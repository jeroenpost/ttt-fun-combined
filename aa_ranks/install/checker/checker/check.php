<?php


$service = "srcds";
define('SQ_SERVER_ADDR', 'fl.ttt-fun.com');
define('SQ_SERVER_PORT', 27065);
define('SQ_TIMEOUT', 15);


require __DIR__ . '/SourceQuery/SourceQuery.class.php';
define('SQ_ENGINE', SourceQuery :: SOURCE);

$Query = new SourceQuery();
$Query->Connect(SQ_SERVER_ADDR, SQ_SERVER_PORT, SQ_TIMEOUT, SQ_ENGINE);

if (!is_array($Query->GetInfo())) {
    sleep(120);
    $Query = new SourceQuery();

    $Query->Connect(SQ_SERVER_ADDR, SQ_SERVER_PORT, SQ_TIMEOUT, SQ_ENGINE);
    if (!is_array($Query->GetInfo())) {
        exec("/usr/sbin/service ".$service." restart");
    }
}

$Query->Disconnect();
