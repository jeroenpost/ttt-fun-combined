--REWARDS Rewards Server Dist
if REWARDS then REWARDS = REWARDS
else REWARDS = {} end
include('modernrewards/sh_rewardsconfig.lua')
include('modernrewards/sv_rewardsdata.lua')

function REWARDS.PlayerInitialSpawn(ply)
    timer.Simple(5,function()
        if not IsValid(ply) then return end

        local rewarded = REWARDS.Database.IsInGroup(ply)

        ply.SteamGroupRewarded = rewarded
        if REWARDS.Settings.ShowOnPlayerConnect and !ply.SteamGroupRewarded then
            REWARDS.CheckPlayerSteamGroup(ply, REWARDS.Settings.GroupName, function(ply)
                if not IsValid(ply) then return end
                if ply.IsInSteamGroup then
                    REWARDS.ApplyPlayerReward(ply)
                else
                    if REWARDS.Settings.ShowOnPlayerConnect then
                        umsg.Start("REWARDS_Open", ply)
                            if REWARDS.Settings.AutoCloseTime and
                            REWARDS.Settings.AutoCloseTime > 0 then
                                umsg.Long(REWARDS.Settings.AutoCloseTime)
                            end
                        umsg.End()
                    end
                end
            end)
        end
    end)
end
hook.Add( "PlayerInitialSpawn", "REWARDS_PlayerInitialSpawn", REWARDS.PlayerInitialSpawn );

function REWARDS.PlayerAttemptJoin(ply, cmd, args)
if not IsValid(ply) then return end
timer.Simple(REWARDS.Settings.JoinCheckDelay or 30, function()
	if not IsValid(ply) or ply.got_group_reward then return end
		REWARDS.CheckPlayerSteamGroup(ply, REWARDS.Settings.GroupName, function(ply)
			if ply.IsInSteamGroup then
				REWARDS.ApplyPlayerReward(ply)
			else
				if REWARDS.Settings.CloseAfterDelayAndNotInGroup then
					SendUserMessage("REWARDS_Close", ply)
				end			
			end
		end)
	end)
end
concommand.Add("rewards_joinsteam", REWARDS.PlayerAttemptJoin)

function REWARDS.ApplyPlayerReward(ply)
	if not IsValid(ply) or ply.SteamGroupRewarded or ply.got_group_reward then SendUserMessage("REWARDS_Close", ply) return end
	REWARDS.Database.GroupJoin(ply)
	ply.SteamGroupRewarded = true
	SendUserMessage("REWARDS_Success", ply)
	--Check DarkRP Reward
	if GAMEMODE.Config and REWARDS.Settings.DarkRPCashReward and REWARDS.Settings.DarkRPCashReward > 0 then
		if ply.AddMoney then ply:AddMoney(REWARDS.Settings.DarkRPCashReward)
		elseif ply.addMoney then ply:addMoney(REWARDS.Settings.DarkRPCashReward) end
	end
	--Check Pointshop Reward
	if PS and REWARDS.Settings.PointshopReward and REWARDS.Settings.PointshopReward > 0 then
		timer.Simple(2, function()
		if not IsValid(ply) or not ply.PS_GivePoints then return end
		    ply:PS_GivePoints(REWARDS.Settings.PointshopReward)
		end)
	end
	--Check Pointshop Item Reward
	if PS and REWARDS.Settings.PointshopItemReward and REWARDS.Settings.PointshopItemReward != "" then
		timer.Simple(2, function()
		if not IsValid(ply) then return end
		--ply:PS_GiveItem(REWARDS.Settings.PointshopItemReward)
		end)
	end
	--Check TTT Karma Reward
	if KARMA and REWARDS.Settings.TTTKarmaReward and REWARDS.Settings.TTTKarmaReward > 0 then
		local config = KARMA.cv
		--ply:SetLiveKarma(math.min(ply:GetLiveKarma() + REWARDS.Settings.TTTKarmaReward, config.max:GetFloat()))
	end
	
	--Run custom reward function
	if REWARDS.Settings.CustomRewardFunction then
		REWARDS.Settings.CustomRewardFunction(ply)
	end
end

function REWARDS.PlayerSay( ply, chattext, pblic )
    if (string.sub(chattext, 1, #chattext) == REWARDS.ChatCommand) then
		SendUserMessage("REWARDS_Open", ply)
    end
end

if REWARDS.ChatCommand and REWARDS.ChatCommand != "" then
	hook.Add( "PlayerSay", "REWARDS_PlayerSay", REWARDS.PlayerSay );
end

function REWARDS.CheckPlayerSteamGroup(ply, group, callback)
	if ply:IsValid() and ply:IsPlayer() and not ply:IsBot() then
	http.Post( REWARDS.Settings.APIURL, {steam = ply:SteamID64(), group = group,
	key = "908w4752opugsdlfijgsqep9r834u5"	},
	function(str, num, tbl, number)

        print("STEAM REWARDS: "..str)

		if str == "true" then
			ply.IsInSteamGroup = true
            print("STEAM REWARDS: "..ply:Nick().." is member")

		elseif str == "steamerror" then
			print("STEAM REWARDS: The Steam Web API's are down for maintenance.")
		elseif str == "apierror" then
			print("STEAM REWARDS: An error occured on your API URL. Try using the default API.")
        else
            print("STEAM REWARDS: WTF error")
        end

	if callback then callback(ply) end
	end
	,function(code) print("STEAM REWARDS: ".. code) end )
	end
end



local function FindGroupNameFromURL()
	local parts = string.Explode( "/" , REWARDS.Settings.SteamGroupPage )
	if (parts[#parts] == "" or parts[#parts]:len() < 3 or #parts < 2) then
		return (parts[#parts - 1])
	else return (parts[#parts]) end
end
REWARDS.Settings.GroupName = FindGroupNameFromURL()