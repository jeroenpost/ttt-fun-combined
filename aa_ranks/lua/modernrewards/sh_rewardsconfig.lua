REWARDS.Theme = {}
--
-- STEAM Rewards Theme
--

REWARDS.Theme.WindowColor = Color(26, 30, 38, 255) --Main window color

REWARDS.Theme.Logo = Material("modernrewards/steamlogo1.png") --Banner logo material
REWARDS.Theme.LogoColor = Color(255, 255, 255, 185) --Banner logo color, default is white
REWARDS.Theme.Font = "Bebas24Font" --Font used for text

REWARDS.Theme.MessageBoxBackColor = Color(51 ,102 ,255 ,255) --Message box back color
REWARDS.Theme.MessageBoxJoinCheckBackColor = Color(51 ,102 ,255 ,255) --Back color when checking
REWARDS.Theme.MessageBoxJoinBarColor = Color(77 ,255 ,121 ,255) --Color of checking progress bar
REWARDS.Theme.MessageBoxSuccessBackColor = Color(77 ,255 ,121 ,255) --Back color when successful
REWARDS.Theme.SuccessColorEffect = true --Color effect when successful

REWARDS.Theme.ButtonColor = Color( 10,200,10 ) --Back color of buttons
REWARDS.Theme.ButtonHoverColor = Color(255,77,77 ) --Hover color of buttons

REWARDS.Settings = {}
--
-- STEAM Rewards Addon Settings
--
--Link to your Steam Group page, simply open your group page in Steam and copy the link
REWARDS.Settings.SteamGroupPage = "https://ttt-fun.com/"
--Name of your Steam Group, used for checking, this name is the last part of the link
 -- e.g. for steamcommunity.com/groups/Facepunch it's just Facepunch
REWARDS.Settings.GroupName = "TTT-FUN"
REWARDS.Settings.ShowOnPlayerConnect = true --Open steam rewards when players not in group connect
REWARDS.Settings.AutoCloseTime = 60 --Auto close steam rewards after certain time (seconds)
REWARDS.Settings.SuccessCloseTime = 15 --Auto close steam rewards after success (seconds)
REWARDS.Settings.PlaySounds = true --Play the menu sounds?
REWARDS.Settings.SuccessSound = "buttons/button9.wav" --Success sound effect
--Change chat command to "" to disable the chat command
REWARDS.ChatCommand = "!joinwebsite"
REWARDS.JoinButtonText = "Join TTT-FUN Website" --Text on join button
REWARDS.DontJoinButtonText = "Not Now, Thanks" --Text on don't join (Close) button
--Message box text (e.g. explain the rewards of joining your group)
REWARDS.Settings.RewardsMessage = "Join our website, and get $25,000 cash bonus on our server! Make sure to login through hybridauth!"
--Message box text on success (e.g. explain what rewards we're given)
REWARDS.Settings.RewardsSuccessMessage = "Thanks for joining the website! We have given you $25,000!"
--Message box text on check (e.g. explain that a check is happening)
REWARDS.Settings.RewardsJoinCheckMessage = "Checking website membership..."
--You can set the FKeyShowCursor to F1,F2,F3 or F4 and change to "" to disable FKeyShowCursor
REWARDS.Settings.FKeyShowCursor = "F4" --Useful for gamemodes with no cursor key
--Delay before check after player presses join button (we recommend at least 30 seconds)
REWARDS.Settings.JoinCheckDelay = 30
REWARDS.Settings.CloseAfterDelayAndNotInGroup = true --Close after check and player not in group?
--API URL, this is the service URL which contacts the steam servers to check memberships
--Default http://api.friendlyplayers.com/groupcheck.php, you can also host this service see readme.txt
REWARDS.Settings.APIURL = "https://ttt-fun.com/ttt_fun_groupcheck"

--
-- STEAM Rewards - Reward Settings
--
--You can enable or disable multiple types of rewards and they will all be given to
--the player as a reward for joining the group. The rewards are given once only.
REWARDS.Settings.DarkRPCashReward = 25000 --DarkRP Cash Reward (Set to 0 to disable)
REWARDS.Settings.PointshopReward = 25000 --Pointshop Points Reward (Set to 0 to disable)
--Pointshop Item Reward (you need the itemid, that's the name of the item lua file)
REWARDS.Settings.PointshopItemReward = "" --Change to "" to disable
--TTT Karma Reward, Karma is applied at the next round start and won't go above the max karma setting
REWARDS.Settings.TTTKarmaReward = 0 -- (Set to 0 to disable)

--Custom Reward Function
--Developers can also add a custom reward function, this will be called
--when a successful reward is being applied to a player.
--Example
--REWARDS.Settings.CustomRewardFunction = function(ply) ply:SetHealth(500) ply:Give("weapon_rpg") end
