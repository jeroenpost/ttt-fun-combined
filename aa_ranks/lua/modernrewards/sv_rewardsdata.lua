--REWARDS Data Server Dist
REWARDS.Database = {}

function REWARDS.Database.GroupJoin(ply)
	if not IsValid(ply) then return end
    ply.isfungroupmember = true
    ply.got_group_reward = true
    hook.Call("steamgroupjoined",GAMEMODE,ply,true)
    fun_api.player_gotgroupreward(ply)
end

function REWARDS.Database.GroupLeave(ply)
	if not IsValid(ply) then return end
    ply.isfungroupmember = false
    ply.got_group_reward = true
    hook.Call("steamgroupjoined",GAMEMODE,ply,false)
    fun_api.player_gotgroupreward(ply)
end

function REWARDS.Database.IsInGroup(ply)
	if not IsValid(ply) then return end
	if ply.isfungroupmember then return true end
	return false
end

