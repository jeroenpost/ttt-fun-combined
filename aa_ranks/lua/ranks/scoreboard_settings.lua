if SERVER then
    AddCSLuaFile()
end
gbscoreboard = {}

local function mergeTable( table1, table2)

        for k,v in ipairs(table2) do
            table.insert(table1, v)
        end

        return table1


end

gbscoreboard.ulxGroups = {
    ['admin'] = { "admin", "Admin", Color(30, 144, 255, 255), true },
    ['hidden_staff'] = { "hidden_staff", "Guest", Color(255, 255, 255), true },
    ['operator'] ={ "operator", "Operator", Color(100, 255, 50) },
    ['user'] = { "user", "Guest", Color(255, 255, 255) },
    ['trusted'] = { "trusted", "Trusted", Color(255, 255, 255), false    },
    ['admin'] = { "admin", "Administrator", Color(255, 230, 0), true    },
    ['member'] = { "member", "Member", Color(240, 240, 240), false    },
    ['super_member'] = { "super_member", "Super Member", Color(220, 220, 220), false    },
    ['mega_member'] = { "mega_member", "Mega Member", Color(200, 200, 200), false    },
    ['legend_member'] = { "legend_member", "Legend Member", Color(180, 180, 180), false    },
    ['jr_mod'] = { "jr_mod", "JR. Moderator", Color(223, 132, 255), true    },
    ['mod'] = { "mod", "Moderator", Color(200, 0, 250), true    },
    ['sr_mod'] = { "sr_mod", "Senior Moderator", Color(135, 5, 182), true    },
    ['trusted_member'] = { "trusted_member", "Trusted Member", Color(223, 132, 255), false    },
    ['super_donator'] = { "super_donator", "Super Donator", Color(101, 96, 253), false    },
    ['super_donator_demoted'] = { "super_donator_demoted", "Super Donator", Color(101, 96, 253), false    },
    ['mega_donator'] = { "mega_donator", "Mega Donator", Color(255, 223, 0, 255), false    },
    ['mega_donator_demoted'] = { "mega_donator_demoted", "Mega Donator Demoted", Color(255, 223, 0, 255), false    },
    ['super_donator_demoted'] = { "super_donator_demoted", "Super Donator*", Color(250, 96, 253), false    },
    ['donator'] = { "donator", "Donator", Color(150, 146, 255), false    },
    ['donatorplus'] = { "donatorplus", "Donator+", Color(170, 166, 255), false    },

    ['custom_rank'] = { "custom_rank", "RMB to Set your rank", Color(240, 240, 240), false    },
    ['custom_rank_demoted'] = { "custom_rank_demoted", "RMB to Set your rank", Color(240, 240, 240), false    },

    ['subway_eatingmachine'] = { "subway_eatingmachine", "Subway Eatingmachine", Color(101, 96, 253), false    },
    ['head_admin'] = { "head_admin", "Head Admin", Color(255, 0, 0), true },
    ['manager'] = { "manager", "Manager", Color(255, 78, 78), true    },
    ['sr_manager'] = { "sr_manager", "Senior Manager", Color(255, 78, 78), true    },
    ['head_of_staff'] = { "head_of_staff", "Head of Staff", Color(19,206,0), true    },
    ['superadmin'] = { "superadmin", "Creator and Owner", "Random", true },
    ['marcopolo'] = { "marcopolo", "Head of Staff", Color(19,206,0), true    },
    ['fear'] = { "fear", "Fear", Color(255, 230, 0), false    },
    ['youtuber'] = { "youtuber", "Youtuber", Color(255, 0, 0), true    },
    ['lordoftime'] = { "lordoftime", "Lord Of Time", Color(255, 0, 0), false    },
    ['yahtzee'] = { "yahtzee", "The Yahtzee", Color(180, 180, 180), false    },
    ['goat'] = { "goat", "G.O.A.T", Color(50, 255, 50), false    },
    ['one'] = { "one", "One", Color(2, 122, 58), true    },
    ['void'] = { "void", "Void", Color(120, 0, 0), false    },
    ['dictator'] = { "dictator", "Dictator", Color(0, 26, 132), false    },
    ['archer'] = { "archer", "Archer's Squad", Color(255, 0, 0), true    },
    ['wheres_bane'] = { "wheres_bane", "Wheres Bane", Color(164, 0, 158), false    },
    ['legit'] = { "legit", "Legit", Color(201, 8, 168), false    },
    ['pikachu'] = { "pikachu", ":3 super doner in 4 the win :3", Color(236, 252, 8), false    },
    ['rigged'] = { "rigged", "[TRUE]ly Rigged", Color(44, 241, 242), true    },
    ['root'] = { "root", "Root Moderator", Color(0, 238, 0), false    },
    ['architect'] = { "architect", "The Architect", Color(50, 205, 50), false    },
    ['turtle'] = { "turtle", "Wheres Turtle", "Random", false    },
    ['fuzzy'] = { "fuzzy", "F.U.Z.Z.Y", Color(240, 0, 7), false    },
    ['hardymen'] = { "hardymen", "Irule's Hardy Men", Color(255, 255, 255), false    },
    ['draco'] = { "draco", "Draconian", Color(78, 11, 210), true    },
    ['stealthy_bitch'] = { "stealthy_bitch", "STEALTHY BITCH", "Random", false    },
    ['sexy_ninja'] = { "sexy_ninja", "Sexy Ninja", "Random", false    },
    ['ihuggle'] = { "ihuggle", "iHuggle", "Random", false    },
    ['sly_archangel'] = { "sly_archangel", "Sly Archangel", Color(39, 241, 215), false    },
    ['doctor'] = { "doctor", "Doctor", "Random", false    },
    ['sealy_haxor'] = { "doctor", "Doctor", "Random", false    },
    ['punishing_ass'] = { "punishing_ass", "Punishing A$$",  Color(159, 0, 197), false    },
    ['insane'] = { "insane", "INSANE", Color(142, 0, 153), false    },
    ['giveupnow'] = { "giveupnow", "GiveUpNow", "Random", false    },
    ['poofer_stop'] = { "poofer_stop", "Poofer, stop", "Random", true    },
    ['god_of_war'] = { "god_of_war", "God of War", Color(128,0,0), false    },
    ['nunumin'] = { "nunumin", "Bad Manager", Color(255, 78, 78), true    },
    ['not_the_owner'] = { "not_the_owner", "Not The Owner", Color(42, 239, 93), false    },
    ['walrus_warrior'] = { "walrus_warrior", "Walrus Warrior", Color(211, 49, 155), false    },
    ['necromancer'] = { "necromancer", "Necromancer", Color(127, 49, 140), false    },
    ['flippity_floppty'] = { "flippity_floppty", "Jungle Girl", Color(0, 0, 126), false    },
    ['sinnamon'] = { "sinnamon", "Sinnamon", Color(188, 45, 1), false    },
    ['best_donator_na'] = { "best_donator_na", "Best Donator NA", Color(69, 240, 20), false    },
    ['alucard'] = { "alucard", "Alucard", Color(46, 217, 234), false    },
    ['the_trusty_guy'] = { "the_trusty_guy", "The Trusty Guy", Color(255, 255, 255), false    },
    ['stealthy_bandit'] = { "stealthy_bandit", "Stealthy Bandit", Color(255, 255, 255), false    },
    ['grandmaster_asshat'] = { "grandmaster_asshat", "Grandmaster Asshat", Color(204, 56, 70), false },
    ['snas_founder'] = { "snas_founder", "S.N.A.S. Founder", "Random", false },
    ['the_one_and_only'] = { "the_one_and_only", "The One And Only", "Random", false },
    ['chairman'] = { "chairman", "Chairman", Color(204, 56, 70), false },
    ['soviet'] = { "soviet", "Soviet", Color(255, 0, 0), true },
    ['system_guardian'] = {"system_guardian","System Guardian", Color(41, 199, 196), true },
    ['pirate_king'] = {"pirate_king","Pirate King", Color(234, 52, 61), false },
    ['minky_monk'] = {"minky_monk","Minky Monk", Color(19,205,203), false },
    ['kvg'] = {"kvg","KVG", Color(38,105,105), false },
    ['quack_kills'] = {"quack_kills","Quack Kills", Color(255, 228, 118), true },
    ['wolf_pup'] = {"wolf_pup","Wolf Pup", Color(255, 228, 118), true },
    ['demigod'] = {"demigod","Neko", Color(255, 228, 118), true },
    ['icycream'] = {"icycream","icycream", Color(255, 228, 118), true },

    ['hollow_wolf'] = {"hollow_wolf","Hollow Wolf", Color(255, 228, 118), true },

    ['blackstar'] = {"blackstar","BlackStar", Color(255, 255, 255), false },
    ['grand_master_wizard'] = {"grand_master_wizard","Grand Master Wizard", Color(205, 10, 175), false },
    ['major_nerd'] = {"major_nerd","Major Nerd", Color(44, 214, 10), false },
    ['juan_the_mexican_man'] = {"juan_the_mexican_man","JUAN THE MEXICAN MAN", Color(255,0,0), false },
    ['necromancer_v2'] = {"necromancer_v2","Necromancer", Color(0, 19, 112), false },
    ['king'] = {"king","King", Color(0, 19, 112), false },
    ['peasant_mod'] = {"peasant_mod","Peasant Mod", Color(0, 19, 112), false },
    ['machxile'] = {"machxile","Machxile", Color(0, 19, 112), false },
    ['fool'] = {"fool","Fool", Color(0, 19, 112), false },
    ['spookyrebel'] = {"spookyrebel","Spookyrebel", Color(0, 19, 112), false },
    ['midnight'] = {"midnight","midnight",Color(0, 19, 112), false },
    ['mute_maniac'] = {"mute_maniac","mute_maniac",Color(0, 19, 112), false },
    ['defender'] = {"defender","defender",Color(0, 19, 112), false },
    ['hashmaster'] = {"hashmaster","hashmaster",Color(0, 19, 112), false },
    ['egg_tinker'] = {"egg_tinker","egg_tinker",Color(0, 19, 112), false },
    ['sodapop'] = {"sodapop","sodapop",Color(0, 19, 112), false },
    ['spectre'] = {"spectre","spectre",Color(0, 19, 112), false },

['custom_rank_jrmod'] = {"custom_rank_jrmod","custom_rank_jrmod",Color(0, 19, 112), false },
['custom_rank_srmod'] = {"custom_rank_srmod","custom_rank_srmod",Color(0, 19, 112), false },
['custom_rank_admin'] = {"custom_rank_admin","custom_rank_admin",Color(0, 19, 112), false },
['custom_rank_mod'] = {"custom_rank_mod","custom_rank_mod",Color(0, 19, 112), false },

};

gbscoreboard.normalranks = {
    "legend_member","mega_member","super_member","members","member","trusted_member","mega_donator_demoted",
    "jr_mod", "mod", "sr_mod", "admin", "guest","user","",
    "super_donator", "super_donator_demoted",  "donatorplus", "donatorplus_demoted","donator","mega_donator",
    "hidden_staff","custom_rank_admin","custom_rank_mod"
}

gbscoreboard.admins = { "sr_manager",  "spectre",  "void","draco", "soviet","admin", "director", "head_admin", "manager", "marcopolo", "nunumin","superadmin", "rigged","grandmaster_asshat","snas_founder","custom_rank_admin" }
gbscoreboard.mods = {
    "mod","sr_mod","god_of_war","root","sinnamon","sly_archangel","walrus_warrior","hidden_staff","quack_kills","sealy_haxor",
    "wolf_pup", "punishing_ass","snas_founder","one","grand_master_wizard","sexy_ninja","doctor","hollow_wolf","peasant_mod", "fool",
    "spookyrebel", "midnight", "mute_maniac","defender","architect","hashmaster","egg_tinker","sodapop","custom_rank_mod","custom_rank_jrmod","custom_rank_srmod",
    "demigod","icycream"
}

gbscoreboard.donators_mega = {
    "superadmin", "mega_donator", "megadonator", "admin", "director", "head_admin", "manager", "mega_donator_demoted",
    "youtuber", "root", "rigged", "marcopolo", "grandmaster_asshat", "draco", "architect", "archer",
    "alucard", "sr_manager",
    "god_of_war","snas_founder","chairman", "nunumin","fuzzy", "walrus_warrior", "void","pirate_king", "minky_monk", "kvg", "quack_kills"
    ,"the_one_and_only","punishing_ass","insane", "blackstar", "juan_the_mexican_man", "major_nerd", "hollow_wolf", "king", "peasant_mod"
    ,"machxile", "spookyrebel","egg_tinker","sodapop","spectre","sealy_haxor","demigod","icycream"
}
gbscoreboard.donators_super = {
    "super_donator", "superdonator", "super_donator_demoted",
    "wheres_bane", "void", "turtle", "the_trusty_guy", "stealthy_bitch",
    "stealthy_bandid", "sly_archangel", "sinnamon", "sexy_ninja",  "one",
    "not_the_owner", "necromancer", "ihuggle",  "giveupnow", "fuzzy", "sr_mod",
    "flippity_floppty", "doctor", "best_donator_na", "alucard", "soviet", "wolf_pup",
    "grand_master_wizard","goat", "necromancer_v2", "fool", "midnight","custom_rank_mod"
}

gbscoreboard.legend_members =  { "legend_member" }
gbscoreboard.mega_members =  { "mega_member" }
gbscoreboard.super_members =  { "super_member" }
gbscoreboard.members =  { "members" }

gbscoreboard.donators_plus = { "donatorplus", "donator_plus", "mod", "donatorplus_demoted" }
gbscoreboard.donators = { "donator", "jr_mod" }

gbscoreboard.customranks = { 'custom_rank',"spectre","custom_rank_demoted","custom_rank_jrmod","custom_rank_mod","custom_rank_srmod","custom_rank_admin","soviet","superadmin" }

-- Merge them
gbscoreboard.mods = mergeTable(gbscoreboard.mods, gbscoreboard.admins)

gbscoreboard.jrmods = mergeTable(gbscoreboard.mods, {'jr_mod'})

gbscoreboard.legend_members = mergeTable(gbscoreboard.legend_members,gbscoreboard.donators_mega)
gbscoreboard.mega_members = mergeTable(gbscoreboard.legend_members, gbscoreboard.mega_members)
gbscoreboard.super_members = mergeTable(gbscoreboard.super_members, gbscoreboard.mega_members )
gbscoreboard.members = mergeTable(gbscoreboard.members, gbscoreboard.super_members)

gbscoreboard.donators_super = mergeTable(gbscoreboard.donators_super, gbscoreboard.donators_mega)
gbscoreboard.donators_plus = mergeTable(gbscoreboard.donators_plus, gbscoreboard.donators_super, gbscoreboard.customranks)
gbscoreboard.donators = mergeTable(gbscoreboard.donators, gbscoreboard.donators_plus)

gbscoreboard.members = mergeTable(gbscoreboard.members, gbscoreboard.donators)


gb = {}

function gb.is_admin( ply )

    if gb.compare_rank(ply,gbscoreboard.admins ) then  ply.isfunadmin = true return true end
    if not table.HasValue(gbscoreboard.admins, ply:PS_GetUsergroup()) and not table.HasValue(gbscoreboard.admins, ply.baseGroup)  then
         ply.isfunadmin = false
         return false
    end
    ply.isfunadmin = true
    return true
end

function gb.is_mod( ply )

    if gb.compare_rank(ply,gbscoreboard.mods ) then  ply.ismod = true return true end
    if not table.HasValue(gbscoreboard.mods, ply:PS_GetUsergroup()) and not table.HasValue(gbscoreboard.mods, ply.baseGroup)  then
         ply.ismod = gb.is_admin( ply )
         return ply.ismod
    end
     ply.ismod = true
     return true
end

function gb.is_jrmod( ply )

    if gb.compare_rank(ply,{"jr_mod"}) then ply.isjrmod = true return true end
     if not table.HasValue(gbscoreboard.jrmods, ply:PS_GetUsergroup()) and not table.HasValue(gbscoreboard.jrmods, ply.baseGroup)  then
             ply.isjrmod = gb.is_mod( ply )
             return ply.isjrmod
        end
    ply.isjrmod = gb.is_mod( ply )
    return ply.isjrmod
end

function gb.is_donator( ply )

    if gb.compare_rank(ply,gbscoreboard.donators ) then ply.isdonator = true return true end
    if not table.HasValue(gbscoreboard.donators, ply:PS_GetUsergroup()) and not table.HasValue(gbscoreboard.donators, ply.baseGroup)  then
        ply.isdonator = gb.has_customrank(ply)
        return  ply.isdonator
    end
     ply.isdonator = true
    return true
end

function gb.is_donatorplus( ply )

    if gb.compare_rank(ply,gbscoreboard.donators_plus ) then  ply.isdonatorplus = true return true end
    if not table.HasValue(gbscoreboard.donators_plus, ply:PS_GetUsergroup()) and not table.HasValue(gbscoreboard.donators_plus, ply.baseGroup)  then
         ply.isdonatorplus = false
         return false
    end
     ply.isdonatorplus = true
end

function gb.is_superdonator( ply )

    if gb.compare_rank(ply,gbscoreboard.donators_super ) then  ply.issuperdonator = true return true end
    if not table.HasValue(gbscoreboard.donators_super, ply:PS_GetUsergroup()) and not table.HasValue(gbscoreboard.donators_super, ply.baseGroup)  then
         ply.issuperdonator = false
         return false
    end
     ply.issuperdonator= true
     return true
end

function gb.is_megadonator( ply )

    if gb.compare_rank(ply,gbscoreboard.donators_mega ) then ply.ismegadonator = true return true end
    if not table.HasValue(gbscoreboard.donators_mega, ply:PS_GetUsergroup()) and not table.HasValue(gbscoreboard.donators_mega, ply.baseGroup)  then
        ply.ismegadonator = false
        return false
    end
    ply.ismegadonator = true
    return true
end

function gb.compare_rank( ply, ranks)
    local rankhash = ""
    for k,v in pairs(ranks) do
        rankhash = rankhash..v
    end
    if not ply.rankhash then ply.rankhash = {} end
    if ply.rankhash[rankhash] then
        if ply.lastspawn and CurTime() - ply.lastspawn > 30 then
            if ply.rankhash[rankhash] == 1 then return true end
            return false
        end
    end

    if ply.ranks and istable(ply.ranks) then
        for k,v in pairs(ply.ranks) do

            if table.HasValue(ranks, v) then
                ply.rankhash[rankhash] = 1
                return true
            end
        end
    end
    ply.rankhash[rankhash] = istable(ranks) and table.HasValue(ranks, ply:PS_GetUsergroup())
    if ply.rankhash[rankhash] then ply.rankhash[rankhash] = 1 else ply.rankhash[rankhash] = 2  end
    if  ply.rankhash[rankhash] == 1 then return true else return false end
end

function gb.compare_rank_name( name, ranks)
    return istable(ranks) and table.HasValue(ranks, name)
end

function gb.has_customrank( ply )
    if gb.compare_rank(ply,gbscoreboard.customranks) or not table.HasValue(gbscoreboard.normalranks, ply:PS_GetUsergroup())  then

        return true
    end

    return false
end

function gb.is_hidden_staff( ply )
    return ( ply:PS_GetUsergroup() == "hidden_staff")
end

function gb.canuse_sg( ply )
    return (gb.is_admin(ply) or ply:PS_GetUsergroup() == "hidden_staff")
end

gb.IsLegendMember = function ( ply )
    if ply:GetUTimeTotalTime() > 1209600 then -- 2 weeks
        return true
    end
    return gb.is_megadonator(ply)
end

gb.IsMegaMember = function ( ply )
    if ply:GetUTimeTotalTime() > 604800 then -- 1 weeks
        return true
    end
    return gb.is_megadonator(ply)
end

gb.IsSuperMember = function ( ply )
    if ply:GetUTimeTotalTime() > 259200 then -- 3  days
        return true
    end
    return gb.is_megadonator(ply)
end

gb.IsMember = function ( ply )
    if ply:GetUTimeTotalTime() > 36000 then -- 10 hours
        return true
    end
    return gb.is_megadonator(ply)
end

gb.isfungroupmember = function ( ply )
    if ply:GetUTimeTotalTime() > 36000 then -- 10 hours
        return true
    end
    return gb.is_megadonator(ply)
end

