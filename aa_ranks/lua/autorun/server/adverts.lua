local function LoadTheAdverts()
    timer.Simple(3,function()

        ulx.adverts = {}
        if server_id == 99 or server_id == 360 or server_id == 361 then
           
         ulx.addAdvert( "4th ANNIVERSARY!!  Ranks and points are on sale for a limited time! 50% OFF  Type !donate! https://ttt-fun.com!", 75, "tttfun", Color(255,255,0,255) )
            
            ulx.addAdvert( "Need a mod? Type !report to make a ticket!", 75, "tttfun", Color(255,255,0,255) )
            ulx.addAdvert( "TTT-FUN hosts other gamemodes too! Type !servers for a list of all servers and gamemodes", 30, "tttfun", Color(0,0,255,255) )
            ulx.addAdvert( "Open your inventory with C or open_inventory in console. Pick up items with CTRL+E.", 30, "tttfun", Color(255,0,0,255) )
            ulx.addAdvert( "Do you want VIP access? Type !donate to check the options!", 30, "tttfun", Color(255,0,255,255) )
            ulx.addAdvert( "Press F4 to open the Job selector and Shop.", 30, "tttfun", Color(0,255,0,255) )
            ulx.addAdvert( "Check out our website: https://ttt-fun.com or type: !forum", 30, "tttfun", Color(255,255,0,255) )
            ulx.addAdvert( "Join our TeamSpeak server! ts.ttt-fun.com ", 30, "tttfun", Color(255,255,0,255) )
            ulx.addAdvert( "Found a bug? Report it in the bugtracker: https://ttt-fun.com/bugtracker", 1200, "tttfun_2", Color(255,255,0,255) )
            //ulx.addAdvert( "Got minecraft? Try the TTT-FUN minecraft server! IP:  minecraft.ttt-fun.com", 1200, "tttfun_2", Color(255,255,0,255) )
        end
        if server_id == 5 or server_id == 1  or server_id == 6 then
         ulx.addAdvert( "4th ANNIVERSARY!!  Ranks and points are on sale for a limited time! 50% OFF  Type !donate! https://ttt-fun.com!", 75, "tttfun", Color(255,255,0,255) )
            ulx.addAdvert( "Need a mod? Type !report to make a ticket!", 75, "tttfun", Color(255,255,0,255) )
            ulx.addAdvert( "Open the pointshop with F3", 30, _, Color(255,0,0,255) )
            ulx.addAdvert( "Do you want donator access? Type !donate to check the options!", 30, "tttfun", Color(255,0,0,255) )
            ulx.addAdvert( "Check out our website: https://ttt-fun.com or type: !forum", 30, "tttfun", Color(255,255,0,255) )
            ulx.addAdvert( "Found a bug? Report it in the bugtracker: https://ttt-fun.com/bugtracker", 1200, "tttfun_2", Color(255,255,0,255) )
            ulx.addAdvert( "Join our TeamSpeak server! ts.ttt-fun.com ", 30, "tttfun", Color(255,255,0,255) )
            ulx.addAdvert( "TTT-FUN hosts other gamemodes and other TTT type servers too! Type !servers for a list of all servers and gamemodes", 30, "tttfun", Color(0,0,255,255) )
            //ulx.addAdvert( "Got minecraft? Try the TTT-FUN minecraft server! IP:  minecraft.ttt-fun.com", 1200, "tttfun_2", Color(255,255,0,255) )
        end
        if server_id == 53 then
         ulx.addAdvert( "4th ANNIVERSARY!!  Ranks and points are on sale for a limited time! 50% OFF  Type !donate! https://ttt-fun.com!", 75, "tttfun", Color(255,255,0,255) )
            ulx.addAdvert( "Need a mod? Type !report to make a ticket!", 75, "tttfun", Color(255,255,0,255) )
            ulx.addAdvert( "Open the pointshop with F4", 30, _, Color(255,0,0,255) )
            ulx.addAdvert( "Do you want donator access? Type !donate to check the options!", 30, "tttfun", Color(255,0,0,255) )
            ulx.addAdvert( "Check out our website: https://ttt-fun.com or type: !forum", 30, "tttfun", Color(255,255,0,255) )
            ulx.addAdvert( "Do you want a fancy custom weapon? Request one on the website! Type !forum", 30, "tttfun", Color(255,255,0,255) )
            ulx.addAdvert( "Found a bug? Report it in the bugtracker: https://ttt-fun.com/bugtracker", 1200, "tttfun_2", Color(255,255,0,255) )
            ulx.addAdvert( "Join our TeamSpeak server! ts.ttt-fun.com ", 30, "tttfun", Color(255,255,0,255) )
            ulx.addAdvert( "Want some more vanilla TTT? Type !servers for a list of all servers and gamemodes", 30, "tttfun", Color(0,0,255,255) )
            //ulx.addAdvert( "Got minecraft? Try the TTT-FUN minecraft server! IP:  minecraft.ttt-fun.com", 1200, "tttfun_2", Color(255,255,0,255) )
        end
        if server_id == 100 then
           ulx.addAdvert( "4th ANNIVERSARY!!  Ranks and points are on sale for a limited time! 50% OFF  Type !donate! https://ttt-fun.com!", 75, "tttfun", Color(255,255,0,255) )
            ulx.addAdvert( "Need a mod? Type !report to make a ticket!", 75, "tttfun", Color(255,255,0,255) )
            ulx.addAdvert( "Open the pointshop with F3", 30, _, Color(255,0,0,255) )
            ulx.addAdvert( "Do you want donator access? Type !donate to check the options!", 30, "tttfun", Color(255,0,0,255) )
            ulx.addAdvert( "Check out our website: https://ttt-fun.com or type: !forum", 30, "tttfun", Color(255,255,0,255) )
            ulx.addAdvert( "Do you want a fancy custom weapon? Request one on the website! Type !forum", 30, "tttfun", Color(255,255,0,255) )
            ulx.addAdvert( "Found a bug? Report it in the bugtracker: https://ttt-fun.com/bugtracker", 1200, "tttfun_2", Color(255,255,0,255) )
            ulx.addAdvert( "Join our TeamSpeak server! ts.ttt-fun.com ", 30, "tttfun", Color(255,255,0,255) )
            ulx.addAdvert( "TTT-FUN hosts other gamemodes too! Type !servers for a list of all servers and gamemodes", 30, "tttfun", Color(0,0,255,255) )
            //ulx.addAdvert( "Got minecraft? Try the TTT-FUN minecraft server! IP:  minecraft.ttt-fun.com", 1200, "tttfun_2", Color(255,255,0,255) )
        end
        if server_id > 129 && server_id < 140 then
          ulx.addAdvert( "4th ANNIVERSARY!!  Ranks and points are on sale for a limited time! 50% OFF  Type !donate! https://ttt-fun.com!", 75, "tttfun", Color(255,255,0,255) )
            ulx.addAdvert( "Need a mod? Type !report to make a ticket!", 75, "tttfun", Color(255,255,0,255) )
            ulx.addAdvert( "Open the pointshop with F3", 30, _, Color(255,0,0,255) )
            ulx.addAdvert( "Do you want donator access? Type !donate to check the options!", 30, "tttfun", Color(255,0,0,255) )
            ulx.addAdvert( "Check out our website: https://ttt-fun.com or type: !forum", 30, "tttfun", Color(255,255,0,255) )
            ulx.addAdvert( "Do you want a fancy custom weapon? Request one on the website! Type !forum", 30, "tttfun", Color(255,255,0,255) )
            ulx.addAdvert( "Found a bug? Report it in the bugtracker: https://ttt-fun.com/bugtracker", 1200, "tttfun_2", Color(255,255,0,255) )
            ulx.addAdvert( "Join our TeamSpeak server! ts.ttt-fun.com ", 30, "tttfun", Color(255,255,0,255) )
            ulx.addAdvert( "We also have very customized TTT server! Type !servers for a list of all servers and gamemodes", 30, "tttfun", Color(0,0,255,255) )
            //ulx.addAdvert( "Got minecraft? Try the TTT-FUN minecraft server! IP:  minecraft.ttt-fun.com", 1200, "tttfun_2", Color(255,255,0,255) )
        end
        if server_id == 300 or server_id == 301 then
          ulx.addAdvert( "4th ANNIVERSARY!!  Ranks and points are on sale for a limited time! 50% OFF  Type !donate! https://ttt-fun.com!", 75, "tttfun", Color(255,255,0,255) )
            ulx.addAdvert( "Need a mod? Type !report to make a ticket!", 75, "tttfun", Color(255,255,0,255) )
            ulx.addAdvert( "Open the pointshop with F4", 30, _, Color(255,0,0,255) )
            ulx.addAdvert( "Do you want donator access? Type !donate to check the options!", 30, "tttfun", Color(255,0,0,255) )
            ulx.addAdvert( "Check out our website: https://ttt-fun.com or type: !forum", 30, "tttfun", Color(255,255,0,255) )
            ulx.addAdvert( "TTT-FUN hosts other gamemodes too! Type !servers for a list of all servers and gamemodes", 30, "tttfun", Color(0,0,255,255) )
            ulx.addAdvert( "Found a bug? Report it in the bugtracker: https://ttt-fun.com/bugtracker", 1200, "tttfun_2", Color(255,255,0,255) )
            ulx.addAdvert( "Join our TeamSpeak server! ts.ttt-fun.com ", 30, "tttfun", Color(255,255,0,255) )
            //ulx.addAdvert( "Got minecraft? Try the TTT-FUN minecraft server! IP:  minecraft.ttt-fun.com", 1200, "tttfun_2", Color(255,255,0,255) )
        end
        if server_id == 500 then

           ulx.addAdvert( "4th ANNIVERSARY!!  Ranks and points are on sale for a limited time! 50% OFF  Type !donate! https://ttt-fun.com!", 75, "tttfun", Color(255,255,0,255) )
            ulx.addAdvert( "Need a mod? Type !report to make a ticket!", 75, "tttfun", Color(255,255,0,255) )
            ulx.addAdvert( "Do you want donator access? Type !donate to check the options!", 30, "tttfun", Color(255,0,0,255) )
            ulx.addAdvert( "Check out our website: https://ttt-fun.com or type: !forum", 30, "tttfun", Color(255,255,0,255) )
            ulx.addAdvert( "TTT-FUN hosts other gamemodes too! Type !servers for a list of all servers and gamemodes", 30, "tttfun", Color(0,0,255,255) )
            ulx.addAdvert( "Found a bug? Report it in the bugtracker: https://ttt-fun.com/bugtracker", 1200, "tttfun_2", Color(255,255,0,255) )
            ulx.addAdvert( "Join our TeamSpeak server! ts.ttt-fun.com ", 30, "tttfun", Color(255,255,0,255) )
            ulx.addAdvert( "Get access to Noclip and Godmode! Get donator, type !donate", 30, "tttfun", Color(255,255,0,255) )
            //ulx.addAdvert( "Got minecraft? Try the TTT-FUN minecraft server! IP:  minecraft.ttt-fun.com", 1200, "tttfun_2", Color(255,255,0,255) )
        end
        if server_id == 132 then
           
            ulx.addAdvert( "4th ANNIVERSARY!! Ranks and points are on sale for a limited time! 50% OFF  Type !donate! https://ttt-fun.com!", 75, "tttfun", Color(255,255,0,255) )
            ulx.addAdvert( "Need a mod? Type !report to make a ticket!", 75, "tttfun", Color(255,255,0,255) )
            ulx.addAdvert( "Do you want donator access? Type !donate to check the options!", 30, "tttfun", Color(255,0,0,255) )
            ulx.addAdvert( "Check out our website: https://ttt-fun.com or type: !forum", 30, "tttfun", Color(255,255,0,255) )
            ulx.addAdvert( "TTT-FUN hosts other gamemodes too! Type !servers for a list of all servers and gamemodes", 30, "tttfun", Color(0,0,255,255) )
            ulx.addAdvert( "Found a bug? Report it in the bugtracker: https://ttt-fun.com/bugtracker", 1200, "tttfun_2", Color(255,255,0,255) )
            ulx.addAdvert( "Join our TeamSpeak server! ts.ttt-fun.com ", 30, "tttfun", Color(255,255,0,255) )
            //ulx.addAdvert( "Got minecraft? Try the TTT-FUN minecraft server! IP:  minecraft.ttt-fun.com", 1200, "tttfun_2", Color(255,255,0,255) )
        end
        if server_id == 700 then
           
            ulx.addAdvert( "4th ANNIVERSARY!!  Ranks and points are on sale for a limited time! 50% OFF  Type !donate! https://ttt-fun.com!", 75, "tttfun", Color(255,255,0,255) )
            ulx.addAdvert( "Need a mod? Type !report to make a ticket!", 75, "tttfun", Color(255,255,0,255) )
            ulx.addAdvert( "Do you want donator access? Type !donate to check the options!", 30, "tttfun", Color(255,0,0,255) )
            ulx.addAdvert( "Check out our website: https://ttt-fun.com or type: !forum", 30, "tttfun", Color(255,255,0,255) )
            ulx.addAdvert( "TTT-FUN hosts other gamemodes too! Type !servers for a list of all servers and gamemodes", 30, "tttfun", Color(0,0,255,255) )
            ulx.addAdvert( "Found a bug? Report it in the bugtracker: https://ttt-fun.com/bugtracker", 1200, "tttfun_2", Color(255,255,0,255) )
            ulx.addAdvert( "Join our TeamSpeak server! ts.ttt-fun.com ", 30, "tttfun", Color(255,255,0,255) )
            ulx.addAdvert( "Open the pointshop with F4", 30, _, Color(255,0,0,255) )
            //ulx.addAdvert( "Got minecraft? Try the TTT-FUN minecraft server! IP:  minecraft.ttt-fun.com", 1200, "tttfun_2", Color(255,255,0,255) )
        end
    end)
end

hook.Add("Initialize","LoadTheAdverts", LoadTheAdverts)
timer.Simple(3,function() LoadTheAdverts() end)