--Fangli's in-game downloads

AddCSLuaFile("autorun/client/figd.lua")
AddCSLuaFile("autorun/client/von.lua")

if VERSION < 160219 then return end

FIGD = FIGD or {}
FIGD.Config = FIGD.Config or {}
FIGD.Config['steamAPI'] = "4D29216FA0573AC56723202DD63B2506" --http://steamcommunity.com/dev/apikey
FIGD.Config['workshopFiles'] = FIGD.Config['workshopFiles'] or {}
FIGD.Config['workshopCollections'] = FIGD.Config['workshopCollections'] or {}
FIGD.Config['menuOptions'] 	 = {
		serverName = "TTT-FUN", --This will show in the menu, please no more than ~25 characters, else it will look really bad
		openMenuCommand = "!addons", --feel free to change it. its used to bring up the menu if the player declined to download the addons on join. it wont bring up anything if they have it all downloaded
		openMenu = true, --if false, the downloads will start without any menu being opened. If true, a menu will appear with a button. If they click the button, the download starts
		showCloseButton = true, --true = Players can choose not to download any addons.    false = Players will be forced to download the addons
		allowToSelectAddons = true, --true = Players can select which addons to download. They can bring up the menu and change their mind anytime on the server. !!!!!!!showCloseButton should be true if allowToSelectAddons is true too!!!!!!
	}

--DO NOT EDIT BELOW THIS UNLESS YOU'RE CERTAIN THAT YOU KNOW WHAT YOU'RE DOING.


util.AddNetworkString("FIGD_Files")

function FIGD.Print(str)
	MsgC(Color(255,255,255), "[", Color(0, 200, 200), "FIGD", Color(255,255,255), "] ", Color(200,200,200), str, "\n")
end

function FIGD.HandleError(sErr)
	MsgC(Color(255,255,255), "[", Color(220,20,20), "FIGD ERROR", Color(255,255,255), "] ", Color(200,200,200), sErr, "\n")
	ErrorNoHalt("[FIGD] ERROR: "..sErr.."\n")
end

function FIGD.InitCollections(callback)
    if #FIGD.Config.workshopCollections > 0 then
        local postData = {
            ["key"] = FIGD.Config.steamAPI,
            ["format"] = "json",
            ["collectioncount"] = tostring(#FIGD.Config.workshopCollections),
        }
        for k,v in pairs(FIGD.Config.workshopCollections) do
            postData["publishedfileids["..(k-1).."]"] = v
        end



        http.Post("https://api.steampowered.com/ISteamRemoteStorage/GetCollectionDetails/v1/", postData,
        function(resp)
            if #resp == 0 then --http.Post doesn't work right after the loading of the map, it's safer like this.
                FIGD.HandleError("(Collections) - There was an error communicating with Steam, trying again...")
                FIGD.InitCollections(callback)
                return
            end

            if resp:find("Bad Request") then
                FIGD.HandleError("(Collections) - There was an error communicating with Steam, please make sure you set your Steam API key right, and also check the Workshop file IDs.")
                return
            end

            local data = util.JSONToTable(resp)

            for _, collection in pairs(data["response"]["collectiondetails"]) do
                for k,item in pairs(collection["children"]) do
                    local id = item["publishedfileid"]

                    if not id then
                        continue
                    end

                    table.insert(FIGD.Config.workshopFiles, tostring(id))
                end
            end

            callback()
        end,
        function()
            FIGD.HandleError("(Collections) - There was an error communicating with Steam, trying again...")
            FIGD.InitCollections(callback)
        end)
    end
end

function FIGD.Init(bIsFirst, nChunk)
	if FIGD.FilesDownloaded and (not nChunk or nChunk == 1) then return end
	if bIsFirst then
		FIGD.Print("Initializing Fangli's In-Game Downloads addon...")
	end
	nChunk = nChunk or 1


	local postData = {		
		["key"] = FIGD.Config.steamAPI,
		["format"] = "json"
	}

	local count = 0
	for k=1, 40, 1 do
		local key = (nChunk-1)*40+k
		if not FIGD.Config.workshopFiles[key] then break end
		count = count + 1
		postData["publishedfileids["..(k-1).."]"] = FIGD.Config.workshopFiles[key]
	end
	postData["itemcount"] = tostring(count)

    if count == 0 then return end


	--Download data about the workshop files
	http.Post("https://api.steampowered.com/ISteamRemoteStorage/GetPublishedFileDetails/v1/", postData,
	function(resp)
		if FIGD.FilesDownloaded and nChunk == 1 then return end

		if #resp == 0 then --http.Post doesn't work right after the loading of the map, it's safer like this.
			FIGD.HandleError("There was an error communicating with Steam, trying again...")
			FIGD.Init(false, nChunk)
			return
		end

		FIGD.FilesDownloaded = true

		if resp:find("Bad Request") then
			--errored out
			--probably wrong workshop ID
			FIGD.HandleError("There was an error communicating with Steam, please make sure you set your Steam API key right, and also check the Workshop file IDs.")
			return
		end

		local data = util.JSONToTable(resp)

		for k,item in pairs(data["response"]["publishedfiledetails"]) do
			local id = item["publishedfileid"]
			local url = item["hcontent_file"]
			local size = item["file_size"]
			local name = item["title"]
			local last_updated = item["time_updated"]

			if not id or not url or not size or not name then 
				continue
			end

			FIGD.AddWorkshopItem(id, {url=url, size=size, name=name, last_updated=tonumber(last_updated)})
		end

		if nChunk*40 < table.Count(FIGD.Config.workshopFiles) then
			FIGD.Init(false, nChunk+1)
		else
			FIGD.Print("Loaded all Workshop items!")
		end			
	end,
	function()
		FIGD.HandleError("There was an error communicating with Steam, trying again...")
		FIGD.Init(false, nChunk)
		return
	end)

end

function FIGD.AddWorkshopItem(id, item)
	FIGD.Print("Adding workshop item '"..item.name.."' with ID "..id.." to in-game downloads")
	FIGD.Files = FIGD.Files or {}
	FIGD.Files[id] = item
end

function FIGD.SendItemsToPlayer(ply)
	if not ply or not IsValid(ply) then return end
	if not FIGD.FilesDownloaded or not FIGD.Files then
		/*timer.Simple(10, function()
			FIGD.Init(false)
			FIGD.SendItemsToPlayer(ply)
		end)*/
		return
	end

	local itemsCount = table.Count(FIGD.Files)
	if itemsCount == 0 then return end

	net.Start("FIGD_Files")
		net.WriteTable(FIGD.Config.menuOptions or {openMenu = false, showMenuCloseButton = false, openMenuCommand = "!figd"})

		net.WriteInt(itemsCount, 16)

		for k,item in pairs(FIGD.Files) do
			net.WriteString(k) --Workshop ID
			net.WriteString(item["url"])
			net.WriteString(item["name"])
			net.WriteInt(item["size"], 32)
			net.WriteInt(item["last_updated"], 32)
		end
	net.Send(ply)
end

concommand.Add("FIGD_request_items", function(ply, cmd, args)
	FIGD.SendItemsToPlayer(ply)
end)


--Gotta use Think hook, we should have http library by now
hook.Add("Think", "FIGD_DownloadWorkshopInfo", function()
	hook.Remove("Think", "FIGD_DownloadWorkshopInfo")
	if not FIGD.Config.workshopCollections or #FIGD.Config.workshopCollections == 0 then
		FIGD.Init(true)
	else
		FIGD.InitCollections(function()
			FIGD.Init(true)
		end)
	end

	timer.Create("CheckFIGD", 5, 0, function()
		if not FIGD.FilesDownloaded then
			FIGD.Init(false)
		else
			timer.Remove("CheckFIGD")
		end
	end)
end)

hook.Add("PlayerSay", "FIGD_Open", function(ply, txt, bTeamChat)
	if txt:sub(1, #FIGD.Config.menuOptions.openMenuCommand) == FIGD.Config.menuOptions.openMenuCommand then
		ply:SendLua([[if FIGD.OpenMenu and FIGD.menuSettings then FIGD.OpenMenu(true) end]])
		return ""
	end
end)