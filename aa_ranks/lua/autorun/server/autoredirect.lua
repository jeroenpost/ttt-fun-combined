hook.Add("InitPostEntity", "gb_add_serverredirection", function()
    local  function checkserverandredirect(ply, steamid, uniqueid)
        if (CurTime() > 300 and server_type and IsValid(ply) and not ply:IsBot())  then
            if noredirect then return end
            local numberOfPlayers = table.Count( player.GetHumans() )
            if (numberOfPlayers < 12 and server_id ~= 13)  then
                http.Post( gb_json.settings.url, {['key'] = gb_json.settings.key,
                    ['action']="get_joinable_servers_minplayers",['servertype'] = server_type or "unknown", ['steam'] = steamid, ['min'] = tostring(numberOfPlayers),['server_id'] = tostring(server_id) },
                    function( body, len, headers, code )
                        if IsValid(ply) then
                            local data = util.JSONToTable( body )
                            if not data then return end
                            if data.server_ip and gb.compare_rank_name(data.admin_group or "nil", gbscoreboard.mods) or gb.compare_rank_name(data.groups or "nil", gbscoreboard.mods) then
                                print("### Not redirecting "..ply:Nick().." to "..data.server_ip );
                                ply:ChatPrint("You are MOd, so you were not automatically redirected to "..data.servername)
                            elseif data.server_ip then
                                timer.Create(ply:SteamID().."redirect",3,10,function()
                                    if IsValid(ply) then
                                        print("### redirecting "..ply:Nick().." to "..data.server_ip );
                                     --   ply:SendLua( 'if IsValid(LocalPlayer()) and GetConVar("ttt_fun_auto_redirect"):GetBool() and LocalPlayer().ConCommand then LocalPlayer():ConCommand("connect '..data.server_ip..'") end' )
                                    end
                                end)

                            end

                        end
                    end)

            end

        end
    end

    hook.Add("PlayerAuthed", "gb_redirecttofullerserver", checkserverandredirect)
end)
