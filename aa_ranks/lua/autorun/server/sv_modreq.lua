ModReq = ModReq or {}
ModReq.ServerIP = game.GetIPAddress()

local InProcess = {}
local OpenTickets = {}
local TimedUsers = {}

local function query( str, values, callback )
    if not values or not istable(values) then values = {} end
    -- yup, im calling those queries in a php script because im too lazy to rewrite properly.
    http.Post( gb_json.settings.url, {['key'] = gb_json.settings.key,
        ['action'] = "modreq_doquery", ["query"] = str, ['values'] = tostring(util.TableToJSON(values)) },
        function( body, len, headers, code )
            local data = util.JSONToTable( body )
            callback( data )
        end);
end


util.AddNetworkString("RefreshTickets")
net.Receive("RefreshTickets", function( len, pl )
    if not ModReq.IsStaff(pl) then return end
	net.Start("RefreshTickets")
	net.WriteTable(OpenTickets)
	net.Send(pl)
	
end)

util.AddNetworkString("CreateTicket")
net.Receive("CreateTicket", function( len, pl )
   -- if not ModReq.IsStaff(pl) then return end

   for k,v in pairs(player.GetAll()) do
       if gb.is_admin(v) then
           pl:ChatPrint("You can only submit a ticket when no admins are on")
        --  return
       end
   end


	local msg = net.ReadString()
	local hname = GetHostName()

	if(TimedUsers[pl:SteamID64()]) then
		if(TimedUsers[pl:SteamID64()] > CurTime() ) then
            pl:ChatPrint("You can only submit one ticket per 10 minutes")
            return
        end
    end

            query( "INSERT into tickets (  player, servip, servname, message, timestamp ) VALUES (  :player, :servip, :servname, :message, :timestamp )",
                {   [':player']=pl:SteamID64(),
                    [':servip']=ModReq.ServerIP,
                    [':servname']=hname,
                    [':message']=msg,
                    [':timestamp']=tostring(os.time())},
                function() print( "[ModReq]: Successfully created new ticket." ) end )

			TimedUsers[pl:SteamID64()] = CurTime() + 600
			pl:ChatPrint("[ModReq]: Your ticket has been submitted and a moderator will be on shortly to assist.")
    ModReq.refresh()


end)

util.AddNetworkString("DeleteTickets")
net.Receive("DeleteTickets", function( len, pl )
    if not ModReq.IsStaff(pl) then return end
	local tbl = net.ReadTable()
	for _, v in pairs(tbl) do
		query( "UPDATE tickets SET claimant = :claim, deleted = 1 WHERE id = :id",{
            [':claim']=pl:SteamID64(),
            [":id"] = v},
            function(data) end)
		        for k, tbl in pairs(OpenTickets) do
			        if(tbl.id == v) then
				    table.remove(OpenTickets, k)
                    end
                end

                query("INSERT into logs (ticketid, claiment, reportee, message) VALUES ( :ticketid, :claiment, :reportee, :message)",
                    {   [':ticketid'] = tostring(v),
                        [':claiment'] = tostring(pl:SteamID64()),
                        [':reportee'] = tostring(pl:SteamID64()),
                        [':message'] = pl:Nick().." Removed ticket #"..v},
                    function( data ) end)
	end
	net.Start("RefreshTickets")
	net.WriteTable(OpenTickets)
	net.Send(pl)
end)

util.AddNetworkString("DeleteBan")
net.Receive("DeleteBan", function( len, pl )
    if not ModReq.IsStaff(pl) then return end
	local tbl = net.ReadTable()
	for k, v in pairs(tbl) do
		query( "DELETE FROM banlist WHERE steamid = :id",{[":id"] = v}, function(data)
		end)
	end
	query("SELECT * FROM banlist",{}, function( data )
		local tbl = {}
		for row = 1, table.Count( data ), 1 do


				table.insert(tbl, data[ row ])

		end
		net.Start("ModReqBans")
		net.WriteTable(tbl)
		net.Send(pl)
	end)
end)

util.AddNetworkString("UpdateTicketStatus")
net.Receive("UpdateTicketStatus", function( len, pl )
    if not ModReq.IsStaff(pl) then return end
	local tbl = net.ReadTable()
	local nick = pl:Nick()
	local id = pl:SteamID64()

    for k, v in pairs(InProcess) do
        if(v.claimant == pl:SteamID64()) then
            pl:ChatPrint("[ModReq]: Open Ticket from "..(v.playername or "unknown")..":")
            pl:ChatPrint(v.message)
            pl:ChatPrint("[ModReq]: You must close your in progress ticket before accepting a new one. Use !closeticket <message>.")

            return ""
        end
    end

	query("SELECT * FROM tickets WHERE id = " .. tonumber(tbl.id),{}, function( data )
		local ticket = data[1]
		if(ticket.inprocess == "true") then
			pl:SendLua([[notification.AddLegacy("That ticket has been claimed already", NOTIFY_HINT, 5)]])
			return
		else
			query("UPDATE tickets SET inprocess = 'true', claimant = :claim WHERE id = :id",
                {   [':claim'] = tostring(id),
                    [':id']=tostring(tbl.id)},
                function(data)
				local msg = "#" .. tbl.id .. " claimed by " .. nick .. ""
				query("INSERT into logs (ticketid, claiment, reportee, message) VALUES ( :ticketid, :claiment, :reportee, :message)",
                    {   [':ticketid'] = tostring((tbl.id)),
                        [':claiment'] = tostring(id),
                        [':reportee'] = tbl.player,
                        [':message'] = msg},
                    function( data ) end)
			end)
			net.Start("UpdateTicketStatus")
			net.WriteString(ticket.servip)
			net.Send(pl)
            pl:ConCommand("modreq_openbar")
        end
        ModReq.refresh()
	end)
end)

util.AddNetworkString("ModReqLogs")
net.Receive("ModReqLogs", function(len, pl)
    if not ModReq.IsStaff(pl) then return end
	query("SELECT * FROM logs ORDER BY id DESC LIMIT 0,50", {}, function( data )
		local tbl = {}
		for row = 1, table.Count( data ), 1 do
			tbl[ row ] = data[ row ]					
		end
		net.Start("ModReqLogs")
		net.WriteTable(tbl)
		net.Send(pl)
	end)
end)

util.AddNetworkString("ModReqBans")
net.Receive("ModReqBans", function(len, pl)
    if not ModReq.IsStaff(pl) then return end
	query("SELECT * FROM banlist", {}, function( data )
		local tbl = {}
		for row = 1, table.Count( data ), 1 do

				table.insert(tbl, data[ row ])

		end
		net.Start("ModReqBans")
		net.WriteTable(tbl)
		net.Send(pl)
	end)
end)

util.AddNetworkString("ModReq")
hook.Add( "PlayerSay", "TicketSystem", function( pl, text, team )
    local text2 = string.lower(text)
	if ( string.sub( text2, 1, 7 ) == "!report"  )  then
		if(pl.ModReqBanned) then pl:ChatPrint("You are banned from using the Mod Request system.") return end
		net.Start("CreateTicket")
		net.Send(pl)
		return ""
	end
	
	if(ModReq.IsStaff(pl)) then

		if( string.sub( text2, 1, 8 ) == "!tickets" ) then
			for k, v in pairs(InProcess) do
				if(v.claimant == pl:SteamID64()) then

                    local playerz = "unknown (left the server!)"
                    for k2,v2 in pairs(player.GetAll()) do
                        if v.player == v2:SteamID64() then
                            playerz = v2:Nick()
                        end
                    end

                    pl:ChatPrint("[ModReq]: Open Ticket from "..(playerz or "unknown")..":")
                    pl:ChatPrint(v.message)
					pl:ChatPrint("[ModReq]: You must close your in progress ticket before accepting a new one. Use !closeticket <message>.")
					return ""
				end
			end
			net.Start("ModReq")
			net.WriteTable(OpenTickets)
			net.Send(pl)
		return ""
		end
		if( string.sub( text2, 1, 12 ) == "!closeticket" ) then
            local text = string.sub( text, 14, 9999 )
            if string.len(text) < 2 then
                pl:ChatPrint("Make sure to type in a short message of how you solved it. Do: !closeticket <message>")
                return
            end

			for k, v in pairs(InProcess) do
				if(v.claimant == pl:SteamID64()) then
					query("UPDATE tickets SET resolved = 'true', resolvedmessage = :resolvedmessage WHERE id = '" .. tonumber(v.id) .. "'",{[':resolvedmessage'] = text}, function(data)
						local msg = "#" .. v.id .. " solved by " .. pl:Nick() .. ": "..text
                        query("INSERT into logs (ticketid, claiment, reportee, message) VALUES ( :ticketid, :claiment, :reportee, :message)",
                            {   [':ticketid'] = (v.id),
                                [':claiment'] = (pl:SteamID64()),
                                [':reportee'] = v.player,
                                [':message'] = msg
                                },
                            function( data ) end)

					end)
					table.remove(InProcess, k)
					pl:ChatPrint("Thank you for resolving ticket #" .. v.id)
                    pl:ConCommand("modreq_closebar")
				end
			end
			return ""
		end
	end
end)


function ModReq.timeToMin( time, boolean )
	if( boolean ) then
		local tmp = time
		tmp =  math.ceil((os.time() - tmp) / 60)
		return tmp
	else
		if(time == "permanent") then return math.huge end
		time = tonumber(time)
		if(os.time() > time) then return 0 end
		local tmp = time
		tmp = math.ceil((tmp - os.time()) / 60)
		return tmp
	end
end

function ModReq.AddBan(ply, target_ply, bantime, reason)
	query("SELECT * FROM banlist WHERE steamid = :steam", {[":steam"] = target_ply:SteamID64()}, function( data )
		if(table.Count( data ) ~= 0 ) then

                ply:ChatPrint("That user is already banned forever. Time altered")

		end


				query("DELETE FROM banlist WHERE steamid = :id",{[':id'] = ply:SteamID64()}, function( data )
                    query("INSERT into banlist ( steamid, bantime, staffid, reason ) VALUES ( :steamid, :bantime, :staffid, :reason)", {
                        [':steamid'] = target_ply:SteamID64(),
                        [':bantime'] = bantime,
                        [':staffid'] = ply:SteamID64(),
                        [':reason'] = reason},
                        function( data )
                            target_ply.ModReqBanned = true
                            local msg = "[ModReq]: " .. target_ply:Nick() .. " has been banned by " .. ply:Nick() .. " for " .. reason
                            query("INSERT into logs (ticketid, claiment, reportee, message) VALUES (  0, :claiment, :reportee, :message)",
                                {   [':claiment'] = ply:SteamID64(),
                                    [':reportee'] = target_ply:SteamID64(),
                                    [':message'] = msg},
                                function( data ) end)
					end)
				end)
    end)
end

function ModReq.CreateDatabase() -- Creates all needed tables in the database.
	query("CREATE TABLE IF NOT EXISTS `tickets` ( `id` int(10) unsigned NOT NULL AUTO_INCREMENT,`ticketid` int(11) NOT NULL,`player` varchar(45) NOT NULL,`claimant` varchar(45) DEFAULT NULL,`servip` varchar(30) NOT NULL,`servname` text NOT NULL,`inprocess` varchar(10) NOT NULL DEFAULT 'false',`resolved` varchar(10) NOT NULL DEFAULT 'false',`message` text NOT NULL,`timestamp` varchar(45) NOT NULL,PRIMARY KEY (`id`));",{}, function(data)
	end)
	
	query("CREATE TABLE IF NOT EXISTS `logs` ( `id` int(11) NOT NULL AUTO_INCREMENT,`ticketid` int(11) NOT NULL,`claiment` varchar(45) NOT NULL,`reportee` varchar(45) NOT NULL,`message` text NOT NULL,PRIMARY KEY (`id`));", {}, function(data)
	end)
	
	query("CREATE TABLE IF NOT EXISTS `banlist` ( `id` int(11) NOT NULL AUTO_INCREMENT,`steamid` varchar(45) NOT NULL,`banned` varchar(10) NOT NULL,PRIMARY KEY (`id`));", {}, function(data)
	end)
	
	ModReq.InitializeShared()
	timer.Create("CheckForTickets", 10, 0, ModReq.InitializeShared)
end

timer.Create("NotifyModReq", 150, 0, function()
	for k, v in pairs(player.GetAll()) do
		v:ChatPrint("[ModReq]: Use !report to submit a ticket, a moderator will respond when available.")
	end
end)

function ModReq.InitializeShared()
	
	
	-- Pull all tickets claimed already for informing the player he has a ticket claimed.
	query( "SELECT * FROM tickets WHERE inprocess = 'true' AND resolved = 'false' AND deleted != 1",{}, function( data )
		if table.Count( data ) > 0 then
			InProcess = {}
			for row = 1, table.Count( data ), 1 do
				local tbl = data[ row ]
				InProcess[ row ] = table.Copy(tbl)
                for k, v in pairs(player.GetAll()) do
                    if tbl.claimant == v:SteamID64() then
                        v:ConCommand("modreq_openbar")
                    end
                end
			end
		else
			InProcess = {}
		end
	end)
	
	-- Grab all tickets not claimed for use client side, also notifies players how many tickets are active and if he has one in progress.
	query( "SELECT * FROM tickets WHERE inprocess = 'false' AND resolved = 'false' AND deleted != 1",{}, function( data )
		if table.Count( data ) > 0 then
			if(OpenTickets[#OpenTickets]) then 
				if(OpenTickets[#OpenTickets].id < data[#data].id) then
					for k, v in pairs(player.GetAll()) do
						if ModReq.IsStaff(v) and v:GetInfoNum("modreq_notify_new", 1) == 1 and ModReq.NotifyNew then
							v:ChatPrint("[ModReq]: There are new tickets in the Mod Request system")
                            v:EmitSound("buttons/blip1.wav")
                            timer.Create("alert"..v:SteamID(),0.1,5,function()
                                if not IsValid(v) then return end
                                v:EmitSound("buttons/blip1.wav")
                            end)
						end
					end
				end
			end
			OpenTickets = {}
			for row = 1, table.Count( data ), 1 do
				
				local tbl = data[ row ]
				OpenTickets[ row ] = table.Copy(tbl)

			end
			for k, v in pairs(player.GetAll()) do


				if ModReq.IsStaff(v) and ModReq.NotifyNumber then
					if not(v.ModReqTimer) then v.ModReqTimer = 0 end

					--if((v.ModReqTimer * 10) >= (time * 60) ) then
						v:ChatPrint("[ModReq]: There are " .. tostring(table.Count( OpenTickets )) .. " tickets unresolved. Type !tickets to see them")
						v.ModReqTimer = 0

					--else
					--	v.ModReqTimer = v.ModReqTimer + 1
					--end
				end
			end
		else
			OpenTickets = {}
		end
	end )
end

hook.Add( "PlayerInitialSpawn", "ModReqInitialSpawn", function(ply)
	query("SELECT * FROM banlist WHERE steamid = :steam",{[':steam'] = ply:SteamID64()}, function( data )
        if not IsValid(ply) then return end
		if(table.Count( data ) > 0 ) then

				ply.ModReqBanned = true

		else
			ply.ModReqBanned = false
		end
	end)
 end)

 ModReq.refresh = function()
    for k,v in pairs(player.GetAll()) do
        if gb.is_jrmod(v) then
            ModReq.InitializeShared()
            return
        end
    end
end

-- Set a timer

timer.Create("CheckForTickets_modreq", 60, 0, function()
    for k,v in pairs(player.GetAll()) do
        if gb.is_jrmod(v) then
            ModReq.InitializeShared()
            return
        end
     end
end)