
hook.Add("InitPostEntity", "gb_ttt_overrides", function()

    local broken_parenting_ents = {
        "move_rope",
        "keyframe_rope",
        "info_target",
        "func_brush"
    }

    if ents and ents.TTT and isfunction(ents.TTT.FixParentedPreCleanup) then
        print("Gb functionoverrides loaded")
        function ents.TTT.FixParentedPreCleanup()
            for _, rcls in pairs(broken_parenting_ents) do
                for k,v in pairs(ents.FindByClass(rcls)) do
                    if v.GetParent and isfunction(v.GetParent) and IsValid(v:GetParent()) then
                        v.CachedParentName = v:GetParent():GetName()
                        v:SetParent(nil)

                        if not v.OrigPos then
                            v.OrigPos = v:GetPos()
                        end
                    end
                end
            end
        end
    end
end)

