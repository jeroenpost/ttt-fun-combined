AddCSLuaFile("afk_config.lua")
AddCSLuaFile("../client/cl_afkmanager.lua")

include('afk_config.lua')

local time = 0

util.AddNetworkString("GivePlayerAFKWarning")
util.AddNetworkString("RemovePlayerAFKWarning")

hook.Add("PlayerAuthed","Add players to the table",function(ply,SID,UID)
	ply.uID = UID
	ply.lastMoved = CurTime()
	ply.hasBeenWarned = false
    ply.totalafktime = 0
    ply.isafk = false
end)

hook.Add("KeyPress","updatePlayerLastMoved_gb",function(ply,key)
    	ply.lastMoved=CurTime()
		if(ply.hasBeenWarned)then
			ply.hasBeenWarned = false
            ply.isafk = false
            hook.Call("addToDamageLog", GAMEMODE, "AFK: "..ply:Nick().." returned from AFK")
            PrintMessage( HUD_PRINTTALK, "[AFK] "..ply:Nick().." returned from AFK" )
			removeAFKWarning(ply)
		end
end)

function giveAFKWarning(ply)
	net.Start("GivePlayerAFKWarning")
	net.Send(ply)
	ply.hasBeenWarned=true
    ply.isafk = true
    hook.Call("addToDamageLog", GAMEMODE, "AFK: "..ply:Nick().." went AFK")
    PrintMessage( HUD_PRINTTALK, "[AFK] "..ply:Nick().." went AFK" )

    local gamemode = GetConVarString("gamemode")
    if gamemode == "terrortown" then
        timer.Create("movetospectator"..ply:SteamID(),15,1,function()
            if IsValid(ply) and ply.isafk then
                ply:ConCommand("ttt_spectator_mode 1")
                hook.Call("addToDamageLog", GAMEMODE, "AFK: "..ply:Nick().." was moved to spectator")

            end
        end)

    end
    if gamemode == "prop_hunt" then
        timer.Create("movetospectator"..ply:SteamID(),30,1,function()
            if IsValid(ply) and ply.isafk and not  ply:Team() == TEAM_SPECTATOR then
                hook.Call("addToDamageLog", GAMEMODE, "AFK: "..ply:Nick().." was moved to spectator")
                ply:Kill()
                ply:SetTeam(TEAM_SPECTATOR)
            end
        end)
    end

    if gamemode == "deathrun" then
        timer.Create("movetospectator"..ply:SteamID(),15,1,function()
            if IsValid(ply) and ply.isafk and not  ply:Team() == TEAM_SPECTATOR then
                hook.Call("addToDamageLog", GAMEMODE, "AFK: "..ply:Nick().." was moved to spectator")
                ply:Kill()
                ply:SetTeam(TEAM_SPECTATOR)
            end
        end)
    end

	return
end

function removeAFKWarning(ply)
	net.Start("RemovePlayerAFKWarning")
	net.Send(ply)
	ply.hasBeenWarned=false
    ply.isafk = false
        if GetConVarString("gamemode") == "terrortown" then
            ply:ConCommand("ttt_spectator_mode 0")
        end
	return
end

hook.Add("Think","checkForAfkPlayers_gb",function()
	if(time<=CurTime())then

      --  PrintTable(afkPlayers)
		--if(table.Count(afkPlayers)>0)then
			for k,v in pairs(player.GetAll()) do
				if(v.lastMoved) then
                    -- Store the total time player has been AFK

					if((v.lastMoved+(AFKCONFIG.kickTime*60))-CurTime()<0)then
						local id = v.uID
						if(AFKCONFIG.souldKickAdmins==false)then
							if(v:IsAdmin()==false)then
								v:Kick(AFKCONFIG.kickReason)
							end
                        else
                            PrintMessage( HUD_PRINTTALK, v:Nick().." was kicked for being AFK" )
                            hook.Call("addToDamageLog", GAMEMODE, "AFK KICK: "..v:Nick().." was kicked for AFK")

							v:Kick(AFKCONFIG.kickReason)
						end
					elseif((v.lastMoved+(AFKCONFIG.warnTime*60))-CurTime()<0)then
                        v.totalafktime = (v.totalafktime or 0) + 5
						if(v.hasBeenWarned==false)then
							giveAFKWarning(v)
						end
                    end

				end
			end
		--end
		time=CurTime()+5
	end
end)
