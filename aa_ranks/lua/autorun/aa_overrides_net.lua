local function gb_ovverrrriides()
    if GetConVarString("gamemode") == "terrortown"  then
        function GAMEMODE:PlayerDeath( victim, infl, attacker)
            -- tell no one
            self:PlayerSilentDeath(victim)

            victim:SetTeam(TEAM_SPEC)
            victim:Freeze(false)
            if victim.Unignite then
            victim:Unignite()
            end
            victim:SetRagdollSpec(true)
            victim:Spectate(OBS_MODE_IN_EYE)

            local rag_ent = victim.server_ragdoll or victim:GetRagdollEntity()
            victim:SpectateEntity(rag_ent)

            victim:Flashlight(false)

            victim:Extinguish()

            net.Start("TTT_PlayerDied") net.Send(victim)

            if HasteMode() and GetRoundState() == ROUND_ACTIVE then
                IncRoundEnd(GetConVar("ttt_haste_minutes_per_death"):GetFloat() * 60)
            end
        end

        -- Traitorchat
        local function RoleChatMsg(sender, role, msg)
            net.Start("TTT_RoleChat")
            net.WriteUInt(role, 2)
            net.WriteEntity(sender)
            net.WriteString(msg)
            net.Send(GetRoleFilter(role))
        end


        local mumbles = {"mumble", "mm", "hmm", "hum", "mum", "mbm", "mble", "ham", "mammaries", "political situation", "mrmm", "hrm",
            "uzbekistan", "mumu", "cheese export", "hmhm", "mmh", "mumble", "mphrrt", "mrh", "hmm", "mumble", "mbmm", "hmml", "mfrrm"}

        -- While a round is active, spectators can only talk among themselves. When they
        -- try to speak to all players they could divulge information about who killed
        -- them. So we mumblify them. In detective mode, we shut them up entirely.
        function GAMEMODE:PlayerSay(ply, text, team_only)
            if not IsValid(ply) then return "" end
            if GetRoundState() == ROUND_ACTIVE then
                local team = ply:Team() == TEAM_SPEC
                if team and not DetectiveMode() then
                    local filtered = {}
                    for k, v in pairs(string.Explode(" ", text)) do
                        -- grab word characters and whitelisted interpunction
                        -- necessary or leetspeek will be used (by trolls especially)
                        local word, interp = string.match(v, "(%a*)([%.,;!%?]*)")
                        if word != "" then
                        table.insert(filtered, mumbles[math.random(1, #mumbles)] .. interp)
                        end
                    end

                    -- make sure we have something to say
                    if table.Count(filtered) < 1 then
                        table.insert(filtered, mumbles[math.random(1, #mumbles)])
                    end

                    table.insert(filtered, 1, "[MUMBLED]")
                    return table.concat(filtered, " ")
                elseif team_only and not team and ply:IsSpecial() then
                    RoleChatMsg(ply, ply:GetRole(), text)
                    return ""
                end
            end

            return text
        end



    end

    if CLIENT then
        timer.Simple(4,function()
            local battery_max = 100
            local battery_min = 10
            if VOICE then
                local function IsTraitorChatting(client)
                    return client:IsActiveTraitor() and (not client.traitor_gvoice)
                end

                local function GetDrainRate()
                   -- if not GetGlobalBool("ttt_voice_drain", false) then return 0 end

                    if GetRoundState() != ROUND_ACTIVE then return 0 end
                    local ply = LocalPlayer()
                    if (not IsValid(ply)) or ply:IsSpec() then return 0 end
                    if  ply:IsDetective() then
                        return 0
                    elseif ply.battery_drain_rate then
                        return ply.battery_drain_rate
                    elseif gb.is_jrmod(ply) then
                        ply.battery_drain_rate = 0
                    elseif gb.is_megadonator(ply) then
                        ply.battery_drain_rate = 0
                    elseif gb.is_superdonator(ply) then
                        ply.battery_drain_rate = 0.01
                    elseif gb.is_donatorplus(ply) then
                        ply.battery_drain_rate = 0.02
                    elseif gb.is_donator(ply) then
                        ply.battery_drain_rate = 0.03
                    elseif ply:GetUTimeTotalTime() > 3600 then
                        ply.battery_drain_rate = 0.06
                    else
                        ply.battery_drain_rate = 0.08
                    end
                    return ply.battery_drain_rate
                end
                function VOICE.Tick()
                    --if not GetGlobalBool("ttt_voice_drain", false) then return end

                    local client = LocalPlayer()
                    if VOICE.IsSpeaking() and (not IsTraitorChatting(client)) then
                        client.voice_battery = client.voice_battery - GetDrainRate()

                        if not VOICE.CanSpeak() then
                            client.voice_battery = 0
                            RunConsoleCommand("-voicerecord")
                        end
                    elseif client.voice_battery < battery_max then
                        client.voice_battery = client.voice_battery + 0.03
                    end
                end
            end
        end)

    end
end

hook.Add("InitPostEntity", "gb_netoverrides", function()
    gb_ovverrrriides()
end)

