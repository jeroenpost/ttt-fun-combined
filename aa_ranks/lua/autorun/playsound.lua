if CLIENT then
    if not LocalPlayer().PlayingSounds then LocalPlayer().PlayingSounds = {} end
    usermessage.Hook("playremotesound", function(faa)
        if not LocalPlayer().PlayingSounds then LocalPlayer().PlayingSounds = {} end
        if not cvars.Bool("tttfun_endroundmusic") then return end

        local e1 = faa:ReadEntity()
        local data = string.Explode( "=@@=",faa:ReadString() )

        local url = data[1]
        local name = data[2]


        -- Check if we have to play 3D or not
        if IsValid(e1) and LocalPlayer() != e1 then

            sound.PlayURL ( url, "3d",
                function( station )
                    if ( IsValid( station ) and IsValid(e1) ) then

                        station:SetPos(e1:GetPos() )

                        station:SetVolume( 1 )
                        station:Set3DFadeDistance( 100, 300 )
                        station:Play()
                        if name then
                            LocalPlayer().PlayingSounds[name] = station
                        end
                        local length = station:GetLength()*10
                        timer.Create("updateSoundPosition"..e1:EntIndex(),0.2,length,function()
                            if( IsValid(e1) and IsValid( station )) then
                                station:SetPos(e1:GetPos() )
                                station:SetVolume( 1 )
                            end
                        end)
                    end
                end )
        else

            sound.PlayURL (
                url, "",
                function( station )
                    if ( IsValid( station ) ) then
                        station:Play()
                        station:SetVolume( 1 )
                        if name then
                            LocalPlayer().PlayingSounds[name] = station
                            station:SetVolume( 1 )
                        end
                    end
                end )

        end



    end)

    usermessage.Hook("stopremotesound", function(faa)
        if not LocalPlayer().PlayingSounds then LocalPlayer().PlayingSounds = {} end
        local name = faa:ReadString()
        local station = LocalPlayer().PlayingSounds[name]
        if IsValid(station) then
            station:SetVolume( 0 )
            station:Stop()
        end
    end)
end

function gb_PlaySoundFromServer( soundfile, ply, nofilter, name )
    if SERVER then
        -- If no originis set, play for all players
        if name then soundfile = soundfile.."=@@="..name gb_StopSoundFromServer(name) end

        if not IsValid(ply) then
            umsg.Start("playremotesound")
            umsg.Entity(nil)
            umsg.String(soundfile)
            umsg.End()
         return
        end
        local orgin_ents = ents.FindInSphere(ply:GetPos(),300)
        local filter = RecipientFilter();
        for k,v in pairs( orgin_ents ) do
            if v:IsPlayer() then
                filter:AddPlayer( v );
            end
        end
        if nofilter then
            umsg.Start("playremotesound")
        else
            umsg.Start("playremotesound", filter)
        end
        umsg.Entity(ply)
        umsg.String(soundfile)
        umsg.End()
    end
end

function gb_StopSoundFromServer( name)
    if SERVER then
    umsg.Start("stopremotesound")
    umsg.String(name)
    umsg.End()
        end
end