local function i (path)
	local side=string.Explode("/",path,false)
	side=string.Left(side[#side],3)
	if side == "cl_" then
		if CLIENT then
			include(path)
		elseif SERVER then
			AddCSLuaFile(path)
		end
	elseif side == "sv_" then
		if SERVER then
			include(path)
		end
	else
		if CLIENT then
			include(path)
		elseif SERVER then
			include(path)
			AddCSLuaFile(path)
		end
	end
end

i "client/cl_modreq.lua"
i "client/cl_modreq_banlist.lua"
i "client/cl_modreq_logs.lua"