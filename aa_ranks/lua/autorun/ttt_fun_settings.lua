CreateClientConVar("tttfun_show_pet_particles", "0", FCVAR_ARCHIVE)
CreateClientConVar("tttfun_number_of_hats", "2", FCVAR_ARCHIVE)
CreateClientConVar("tttfun_show_deathmatch", "1", FCVAR_ARCHIVE)
CreateClientConVar("tttfun_endroundmusic", "1", FCVAR_ARCHIVE)
CreateClientConVar("tttfun_guninfo", "1", FCVAR_ARCHIVE)
CreateClientConVar("ttt_fun_show_veins", "1", FCVAR_ARCHIVE)
CreateClientConVar("ttt_fun_auto_redirect", "0", FCVAR_ARCHIVE)

hook.Add("TTTSettingsTabs", "TTTFUNSETTINGS", function(dtabs)

    local padding = dtabs:GetPadding()

    padding = padding * 2

    local dsettings = vgui.Create("DPanelList", dtabs)
    dsettings:StretchToParent(0,0,padding,0)
    dsettings:EnableVerticalScrollbar(true)
    dsettings:SetPadding(10)
    dsettings:SetSpacing(10)

    do
        local dgui = vgui.Create("DForm", dsettings)
        dgui:SetName("General TTT-FUN Settings")

        local cb = nil

        dgui:CheckBox("Enable RDM Manager popups upon RDM", "ttt_dmglogs_rdmpopups")
        dgui:CheckBox("Enable Deathmatch popups", "tttfun_show_deathmatch")
        dgui:CheckBox("Enable Killcam", "wyozikc_enabled")
        dgui:CheckBox("Enable Music and Custom Sounds", "tttfun_endroundmusic")
        dgui:CheckBox("Enable Playtime Display", "utime_enable")
        dgui:CheckBox("Enable Weapon Info on hover", "tttfun_guninfo")
        dgui:CheckBox("Enable Red Pulsating screen on low health", "ttt_fun_show_veins")
        dgui:CheckBox("Enable Feet", "cl_legs")
        dgui:CheckBox("Enable Auto Redirect", "ttt_fun_auto_redirect")

        local dgui2 = vgui.Create("DForm", dsettings)
        dgui2:SetName("Lag Reduce Settings")
        dgui2:NumSlider("Show number of Hats / Accessoires per person", "tttfun_number_of_hats", 0, 5, 0)
        dgui2:CheckBox("Show Pet Particles", "tttfun_show_pet_particles")

        dsettings:AddItem(dgui)
        dsettings:AddItem(dgui2)

    end

    dtabs:AddSheet("TTT-FUN", dsettings, "gui/ttt-fun_icon.png", false, false, "TTT-FUN Settings")
end)