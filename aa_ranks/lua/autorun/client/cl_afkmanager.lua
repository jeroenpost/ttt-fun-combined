include('afk_config.lua')

surface.CreateFont( "AFKFont", {
	font = "Arial", 
	size = ScreenScale(120), 
	weight = 500, 
	blursize = 0, 
	scanlines = 0, 
	antialias = true, 
	underline = false, 
	italic = false, 
	strikeout = false, 
	symbol = false, 
	rotary = false, 
	shadow = false, 
	additive = false, 
	outline = true, 
} )

surface.CreateFont( "AFKFontTwo", {
	font = "Arial", 
	size = ScreenScale(25), 
	weight = 500, 
	blursize = 0, 
	scanlines = 0, 
	antialias = true, 
	underline = false, 
	italic = false, 
	strikeout = false, 
	symbol = false, 
	rotary = false, 
	shadow = false, 
	additive = false, 
	outline = true, 
} )

local shouldDrawADKWarning = false

net.Receive("GivePlayerAFKWarning",function()
	shouldDrawADKWarning=true
    if not LocalPlayer().totalafktime then
        LocalPlayer().totalafktime = 0
    end
end)

net.Receive("RemovePlayerAFKWarning",function()
	shouldDrawADKWarning=false
end)

local nextTimeAdd = 0
hook.Add("HUDPaint","drawTheAFKWarning",function()



	if(shouldDrawADKWarning)then
        local drawit = true
        -- If darkrp and afk in darkrp
        if LocalPlayer().getDarkRPVar then
            if drawit  then
                if LocalPlayer():getDarkRPVar("AFK") then drawit = false end
            end
        end

        -- Set client AFK time
        if nextTimeAdd < CurTime() then
            LocalPlayer().totalafktime = LocalPlayer().totalafktime + 0.5
            nextTimeAdd = CurTime() + 0.5
        end

        if drawit then
            draw.RoundedBox( 0, 0, (ScrH()/2)-ScreenScale(60), ScrW(), ScreenScale(120), Color(0,0,0,240) )
            draw.DrawText( AFKCONFIG.mainWarnMessage, "AFKFont", ScrW() * 0.5, (ScrH()*0.5)-(ScreenScale(60)), Color( 255, 0 ,0 , 255 ), TEXT_ALIGN_CENTER )
            draw.DrawText( AFKCONFIG.subWarnMessage, "AFKFontTwo", ScrW() * 0.5, (ScrH()*0.5), Color( 255,255,255, 255 ), TEXT_ALIGN_CENTER )
            draw.DrawText( AFKCONFIG.subWarnMessage2, "AFKFontTwo", ScrW() * 0.5, (ScrH()*0.5)+(ScreenScale(25)), Color( 255,255,255, 255 ), TEXT_ALIGN_CENTER )
            draw.DrawText( AFKCONFIG.subWarnMessage3, "AFKFontTwo", ScrW() * 0.5, (ScrH()*0.5)+(ScreenScale(65)), Color( 255,255,255, 255 ), TEXT_ALIGN_CENTER )
        end

	end
	
end)