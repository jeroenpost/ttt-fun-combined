if SERVER then
    AddCSLuaFile('ranks/scoreboard_settings.lua')

    include('ranks/scoreboard_settings.lua')
   -- include('ranks/ranks_to_db.lua')
    include('ranks/ttt_fun_api.lua')
end

if CLIENT then
    include('ranks/scoreboard_settings.lua')
end


local haspointshop = true
if not file.Exists('vgui/DPointShopMenu.lua',"LUA") and not file.Exists('vgui/dpointshopmenu.lua',"LUA") then
    haspointshop = false
end

if SERVER then
	AddCSLuaFile()
    if haspointshop then
        AddCSLuaFile('vgui/DPointShopMenu.lua')
        AddCSLuaFile('vgui/DPointShopItem.lua')
        AddCSLuaFile('vgui/DPointShopPreview.lua')
        AddCSLuaFile('vgui/DPointShopColorChooser.lua')
        AddCSLuaFile('vgui/DPointShopGivePoints.lua')
        AddCSLuaFile('sh_pointshop.lua')
        AddCSLuaFile('sh_config.lua')
        AddCSLuaFile('sh_player_extension.lua')
        AddCSLuaFile('cl_player_extension.lua')
        AddCSLuaFile('cl_pointshop.lua')

        include('sh_pointshop.lua')
        include('sh_config.lua')
        include('sh_player_extension.lua')
        include('sv_player_extension.lua')
        include('sv_pointshop.lua')
    else
        include('providers/mysql.lua')
        include('providers/json.lua')
        PS = {}
        function PS:ValidatePoints(points) return points end
        function PS:ValidateItems(items) return items end
        local Player = FindMetaTable('Player')
        function Player:PS_SendPoints() end
        function Player:PS_SendItems() end
        function Player:PS_SendPoints() end
        function Player:PS_SendItems() end
    end
end

if CLIENT then
    if haspointshop then
        include('vgui/DPointShopMenu.lua')
        include('vgui/DPointShopItem.lua')
        include('vgui/DPointShopPreview.lua')
        include('vgui/DPointShopColorChooser.lua')
        include('vgui/DPointShopGivePoints.lua')
        include('sh_pointshop.lua')
        include('sh_config.lua')
        include('sh_player_extension.lua')
        include('cl_player_extension.lua')
        include('cl_pointshop.lua')
    else
        PS = {}
        function PS:ValidatePoints(points) return points end
        function PS:ValidateItems(items) return items end
        local Player = FindMetaTable('Player')
        function Player:PS_SendPoints() end
        function Player:PS_SendItems() end
        function Player:PS_SendPoints() end
        function Player:PS_SendItems() end
    end
end

if haspointshop then
    PS:Initialize()
    PS:LoadItems()
end

