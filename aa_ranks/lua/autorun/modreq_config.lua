ModReq = ModReq or {}

ModReq.Staff = {"trial_mod", "mod", "sr_mod", "admin", "superadmin"}
ModReq.Admins = {"admin", "superadmin"}

ModReq.NotifyNumber = true -- Whether to notify players every (user defined) minutes about the number of open tickets.
ModReq.NotifyNew = true -- Whether to notify players when a new ticket is detected in the database.

ModReq.IsStaff = function(ply) -- Change to fit your rank system.
	return gb.is_jrmod(ply)
end