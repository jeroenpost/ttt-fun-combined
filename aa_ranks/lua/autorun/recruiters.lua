if SERVER then
	AddCSLuaFile()
	AddCSLuaFile('recruiters/sh_recruiters.lua')
	AddCSLuaFile('recruiters/cl_recruiters.lua')

    include('recruiters/sh_recruiters.lua')
    include('recruiters/sv_recruiters.lua')
end

if CLIENT then
    include('recruiters/sh_recruiters.lua')
    include('recruiters/cl_recruiters.lua')
end