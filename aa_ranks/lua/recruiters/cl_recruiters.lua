if not recruiters then recruiters = {} end

recruiters.drawheight = 35
recruiters.mainWindow = false
recruiters.alertAlpha = 255

function recruiters.PaintMainWindow()
    local ply =  LocalPlayer()
    if not IsValid( ply ) or not recruiters.rewards then return end

    local recruiter =  ply:GetNWBool("Recruiter",false)
    local recruited = ply:GetNWInt("recruiter.recruited",0)
    local goal = ply:GetNWInt("recruiter.goal",10)

    local text = ply:GetNWString("recruiter.toptext","MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM")

    local pointsText = ply:GetNWString("recruiter.lowertext","")
    local alertText = ply:GetNWString("recruiter.alerttext", "")


    local width = REWARDS.FindMessageBoxWidth(text)
    if recruiter then
        local percentWidth = (width/goal) * recruited
        surface.SetDrawColor(Color(50,90,90,240))
        surface.DrawRect(107, 4, width, 27)

        draw.DrawText(text, "Bebas24Font", 114, 7, color_white, 0)

        surface.SetDrawColor(Color(80,140,80,240))
        surface.DrawRect(107, 31, width, 19)
        draw.DrawText(pointsText, "Bebas16Font", 114, 33, color_white, 0)

        surface.SetDrawColor(Color(50,90,90,240))
        surface.DrawRect(107, 50, width, 5)

        surface.SetDrawColor(Color(80,200,80,240))
        surface.DrawRect(107, 50, percentWidth, 5)
    end

    if alertText and alertText != "" then
        surface.SetDrawColor(Color(179,0,255,recruiters.alertAlpha))
        surface.DrawRect(107, 55, width, 38)
        draw.DrawText(alertText, "Bebas32Font", 114, 58, Color(255,255,255,recruiters.alertAlpha), 0)
        recruiters.alertAlpha = recruiters.alertAlpha - 0.5
    else
        recruiters.alertAlpha = 255
    end
end

function recruiters.PaintJoinWindow()
    local ply =  LocalPlayer()
    if not IsValid( ply ) or not recruiters.rewards then return end

    local recruiter =  ply:GetNWBool("Recruiter",false)

    local text = "YOU ARE UPGRADED TO TTT-FUN RECRUITER!"
    if recruiter then

        surface.SetDrawColor(Color(50,90,90,240))
        surface.DrawRect(0, 0, 500, 500)

        surface.SetDrawColor(Color(20,45,45,240))
        surface.DrawRect(5, 5, 490, 490)

        draw.DrawText(text, "Bebas32Font", 50, 40, color_white, 0)
        local text2 = "For every player that joins, you get "..recruiters.singlereward.." points\n"
                     .."Invite your friends to get tons of points and achievements!\n \n"
                     .."The Rewards:\n"
                     .."3 players:  25,000 points\n"
                     .."10 players: 150,000 points\n"
                     .."25 players: 500,000 points\n"
                     .."50 players: 1,000,000 points\n \n"
                     .."Everyone who joins gets you points, they don't have to be your friend!\n"
                     .."Check out https://ttt-fun.com to see the best recruiters of TTT-FUN\n"
                     .."Type: !servers to find other empty servers\n"
                     .."You stay recruiter until you leave for longer than 5 minutes\n \n"
                     .."Have fun!"
        draw.DrawText(text2, "Bebas24Font", 20, 100, color_white, 0)


    end

end

function recruiters.FindMessageBoxWidth(text)
    surface.SetFont("Bebas24Font")
    local tw = surface.GetTextSize(text)
    return tw + 10
end

function recruiters.CloseBar()
    if recruiters.mainWindow then
        recruiters.mainWindow:Remove()
        recruiters.mainWindow = nil
    end
end
usermessage.Hook("recruiters_Close", recruiters.CloseBar)
concommand.Add("recruiter_closebar", recruiters.CloseBar)

function recruiters.OpenBar( settings )
    if not LocalPlayer() then return end

    if not recruiters.mainWindow then
        REWARDS.CurrentAlpha = 0
        recruiters.mainWindow = vgui.Create( "DFrame" )
        recruiters.mainWindow:SetSize( ScrW(), recruiters.drawheight + 25 + 50)
        recruiters.mainWindow:SetPos(200,0)
        recruiters.mainWindow:SetDraggable( false )
        recruiters.mainWindow:ShowCloseButton( true )
        recruiters.mainWindow:SetTitle( "" )
        recruiters.mainWindow.Paint = recruiters.PaintMainWindow
    else
        recruiters.CloseBar()
    end
end

concommand.Add("recruiter_bar", recruiters.OpenBar)


function recruiters.CloseJoin()
    if recruiters.joinWindow then
        recruiters.joinWindow:Remove()
        recruiters.joinWindow = nil
    end

            end
usermessage.Hook("recruiters_Close_Join", recruiters.CloseJoin)
concommand.Add("recruiter_closejoin", recruiters.CloseJoin)

function recruiters.OpenBarAlertJoin( settings )
    if not LocalPlayer() then return end

    if not recruiters.joinWindow then
        REWARDS.CurrentAlpha = 0
        recruiters.joinWindow = vgui.Create( "DFrame" )
        recruiters.joinWindow:SetSize( 500,  500)
        recruiters.joinWindow:SetPos((ScrW()/2)-250,(ScrH()/2)-250)
        recruiters.joinWindow:SetDraggable( false )
        recruiters.joinWindow:ShowCloseButton( true )
        recruiters.joinWindow:SetTitle( "" )
        recruiters.joinWindow.Paint = recruiters.PaintJoinWindow
        recruiters.joinWindow:MakePopup();
    else
        recruiters.CloseJoin()
    end
end

concommand.Add("recruiter_join", recruiters.OpenBarAlertJoin)



