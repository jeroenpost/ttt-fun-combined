CreateClientConVar("modreq_notify_new", "1", true, true)
CreateClientConVar("modreq_update_freq", "5", true, true)

local mainpanel

net.Receive("ModReq", function( len, pl )

		local MarkedForDeletion = {}
		local tickets = net.ReadTable()
		
		local page = 1
		local pages = 0
		for i = 1, #tickets, 13 do
			pages = pages + 1
		end
		
		if(pages == 0) then page = 0 end
		local lessthan = 13 * page
		local greaterthan = 13 * (page - 1)

		
		local MainFrame = vgui.Create("DPanel")
		MainFrame:SetSize( 700, 450 )
		MainFrame:Center()
		MainFrame:MakePopup()
		MainFrame:SetAlpha( 0 )
		MainFrame:AlphaTo( 255, .25, 0, function(animData, pnl)
			mainpanel = MainFrame
		end)
		MainFrame.Paint = function()
			draw.RoundedBox( 8, 0, 0, MainFrame:GetWide(), MainFrame:GetTall(), Color( 0, 0, 0, 210 ) )
			surface.SetTextColor( 255, 255, 255, 255 )
			surface.SetTextPos( MainFrame:GetWide()/2-72, 0 ) 
			surface.SetFont("Trebuchet24")
			surface.DrawText( "  Mod Request" )

			draw.RoundedBoxEx(8, 0, 0, MainFrame:GetWide(), 25, Color(100, 100, 100, 225), true, true, false, false)
		end		
		
		
		local TicketID = vgui.Create( "DButton" )
		TicketID:SetText( "Time" )
		TicketID:SetPos( 50, 48 )
		TicketID:SetSize( 50, 15 )
		TicketID.Paint = function()
			draw.RoundedBoxEx(8, 0, 0, TicketID:GetWide(), TicketID:GetTall(), Color(255, 255, 255, 255), true, true, false, false)
		end
		TicketID:SetParent( MainFrame )
		
		local Messages = vgui.Create( "DButton" )
		Messages:SetText( "Message" )
		Messages:SetPos( 202, 48 )
		Messages:SetSize( 448, 15 )
		Messages.Paint = function()
			draw.RoundedBoxEx(8, 0, 0, Messages:GetWide(), Messages:GetTall(), Color(255, 255, 255, 255), true, true, false, false)
		end
		Messages:SetParent( MainFrame )

		local PlyName = vgui.Create( "DButton" )
		PlyName:SetText( "Player Name" )
		PlyName:SetPos( 101, 48 )
		PlyName:SetSize( 100, 15 )
		PlyName.Paint = function()
			draw.RoundedBoxEx(8, 0, 0, PlyName:GetWide(), PlyName:GetTall(), Color(255, 255, 255, 255), true, true, false, false)
		end
		PlyName:SetParent( MainFrame )
		
		local RightArrow = vgui.Create( "DButton" )
		RightArrow:SetImage("materials/icon16/arrow_right.png")	
		RightArrow:SetText("")
		RightArrow:SetPos( 388, 398 )
		RightArrow:SetSize( 25, 20 )
		RightArrow:SetParent( MainFrame )
		if(pages == 1) then RightArrow:SetDisabled(true) end
		
		local LeftArrow = vgui.Create( "DButton" )
		LeftArrow:SetImage("materials/icon16/arrow_left.png")	
		LeftArrow:SetText("")
		LeftArrow:SetPos( 278, 398 )
		LeftArrow:SetSize( 25, 20 )
		LeftArrow:SetDisabled(true)
		LeftArrow:SetParent( MainFrame )
		
		
		local DeleteTickets = vgui.Create( "DButton" )
		DeleteTickets:SetText("Delete Tickets")
		DeleteTickets:SetPos( 307, 420 )
		DeleteTickets:SetSize( 75, 25 )
		DeleteTickets:SetParent( MainFrame )
		DeleteTickets.Paint = function() 
			draw.RoundedBoxEx(8, 0, 0, DeleteTickets:GetWide(), DeleteTickets:GetTall(), Color(255, 255, 255, 255), false, false, false, false)
		end
		if(LocalPlayer():IsAdmin()) then
			local BanlistButton = vgui.Create( "DButton" )
			BanlistButton:SetText("Banlist")
			BanlistButton:SetPos( 227, 420 )
			BanlistButton:SetSize( 75, 25 )
			BanlistButton:SetParent( MainFrame )
			BanlistButton.Paint = function() 
				draw.RoundedBoxEx(8, 0, 0, BanlistButton:GetWide(), BanlistButton:GetTall(), Color(255, 255, 255, 255), false, false, false, false)
			end
			BanlistButton.DoClick = function ()
				net.Start("ModReqBans")
				net.SendToServer()
			end
		end
		
		if(LocalPlayer():IsAdmin()) then
			local LogButton = vgui.Create( "DButton" )
			LogButton:SetText("Logs")
			LogButton:SetPos( 387, 420 )
			LogButton:SetSize( 75, 25 )
			LogButton:SetParent( MainFrame )
			LogButton.Paint = function() 
				draw.RoundedBoxEx(8, 0, 0, LogButton:GetWide(), LogButton:GetTall(), Color(255, 255, 255, 255), false, false, false, false)
			end
			LogButton.DoClick = function ()
				net.Start("ModReqLogs")
				net.SendToServer()
			end
		end
		
		local CloseButton = vgui.Create( "DButton" )
		CloseButton:SetText( "X" )	
		CloseButton:SetPos( 675, 0 )
		CloseButton:SetSize( 25, 25 )
		CloseButton:SetParent( MainFrame )
		CloseButton.Paint = function() 
			draw.RoundedBoxEx(8, 0, 0, CloseButton:GetWide(), CloseButton:GetTall(), Color(50, 50, 50, 225), false, true, false, false)
		end
		CloseButton.DoClick = function ()
			MainFrame:SetKeyboardInputEnabled( false )
			MainFrame:SetMouseInputEnabled( false )
			MainFrame:AlphaTo( 0, .25, 0, function(animData, pnl)
				MainFrame:Remove()
				timer.Destroy("TicketAutoRefresh")
			end)
		end
		
		local backgroundPanel = vgui.Create( "DPanel", MainFrame )
		backgroundPanel:SetSize( 600, 340 )
		backgroundPanel:SetPos(50, 65)
		backgroundPanel.Paint = function()
		draw.RoundedBoxEx(8, 0, 0, backgroundPanel:GetWide() + 50, backgroundPanel:GetTall() - 20, Color(255, 255, 255, 255))
			surface.SetDrawColor( 0, 0, 0, 255 )
			surface.DrawLine( 50, 0, 50, 321)
			surface.DrawLine( 151, 0, 151, 321)
		
			for k, v in pairs(tickets) do
				if(k > greaterthan) and (k <= lessthan) then
					surface.DrawLine( 0 , 21 + (25 * ((k - greaterthan) - 1)), 600, 21 + (25 * ((k - greaterthan) - 1)) )
				end
			end
		end		
			
		local NumPages = vgui.Create("DLabel", MainFrame)
		NumPages:SetPos( ( MainFrame:GetWide() / 2 ) - 38 , 400 )
		NumPages:SetFont("Trebuchet18")
		NumPages:SetTextColor(Color(255,255,255,255))
		NumPages:SetText( "Page: " .. page .. " of " .. pages )
		NumPages:SizeToContents()
		local SelectButtons = {}
		local DetailsButtons = {}
		for i=1, 13 do
			local Button = vgui.Create("DButton")
			local var = 25 * (i - 1)
			Button:SetPos(2.75, 65 + var)
			Button:SetSize(45, 20)
			Button:SetText( "Claim" )
			Button.Paint = function()
				draw.RoundedBoxEx(8, 0, 0, Button:GetWide(), Button:GetTall(), Color(255, 255, 255, 255), true, true, true, true)
			end
			Button:SetParent(MainFrame)

			table.insert(SelectButtons, Button)
			
			local Details = vgui.Create("DButton")	
			Details:SetPos(652.25, 65 + var)
			Details:SetSize(45, 20)
			Details:SetText( "Details" )
			Details.Paint = function()
				draw.RoundedBoxEx(8, 0, 0, Details:GetWide(), Details:GetTall(), Color(255, 255, 255, 255), true, true, true, true)
			end
			Details:SetParent(MainFrame)
			table.insert(DetailsButtons, Details)
			
			
			if(tickets[i]) then
				
				steamworks.RequestPlayerInfo( tickets[i].player )
				local test = vgui.Create( "DLabel", backgroundPanel )
				test:SetPos( 60, 2 + (25 * (i - 1) ) )
				test:SetText("Loading")
				timer.Simple(1, function()
					if(test:IsValid()) then
						test:SetText(steamworks.GetPlayerName( tickets[i].player ))
					end
				end)
				test:SetSize(90, 20)
				test:SetTextColor(color_black)

                local id
                id = tostring(tickets[i].id)
                if string.len(id) < 4 then
                    for i = 1, 4 - string.len(id) do
                        id = "0" .. id
                    end
                end
				
				local test2 = vgui.Create( "DLabel", backgroundPanel )
				test2:SetPos( 160, 2 + (25 * (i - 1) ) )
				test2:SetText( "#"..id..": "..string.gsub(tickets[i].message, "\n", " - ") )
				test2:SetSize( 425, 20 )
				test2:SetTextColor(color_black)
			
				Button.DoClick = function()
					net.Start("UpdateTicketStatus")
					net.WriteTable(tickets[i])
					net.SendToServer()
                    mainpanel:SetVisible( false )
				end
				
				Details.DoClick = function() 	
					OpenTicketDetails(tickets[i])
				end
				

				
				local Mark = vgui.Create("DButton")
				Mark:SetPos(4, 3 + var)
				Mark:SetSize(45, 20)
				Mark:SetText( os.date("%I:%M%p", tickets[i].timestamp) )
				Mark.Paint = function()
					draw.RoundedBoxEx(8, 0, 0, Mark:GetWide(), Mark:GetTall(), Color(0, 0, 0, 0), true, true, true, true)
				end
				Mark:SetParent(backgroundPanel)
				Mark.DoClick = function()
					if(table.HasValue(MarkedForDeletion, tickets[i].id)) then
						table.RemoveByValue(MarkedForDeletion, tickets[i].id)
						Mark:SetTextColor(color_black)
						notification.AddLegacy( "Ticket: " .. tickets[i].id .. " has been un-marked for deletion.", NOTIFY_HINT, 5 )
						return
					else
						Mark:SetTextColor(Color(255, 0, 0, 255))
						table.insert(MarkedForDeletion, tickets[i].id)
						notification.AddLegacy( "Ticket: " .. tickets[i].id .. " has been marked for deletion.", NOTIFY_HINT, 5 )
					end
				end

			else
				Button:SetDisabled(true)
				Details:SetDisabled(true)
			end
		end
		
		
		DeleteTickets.DoClick = function ()
			if(#MarkedForDeletion == 0) then
				notification.AddLegacy( "You have not marked any tickets for deletion", NOTIFY_HINT, 5)
			else
				net.Start("DeleteTickets")
				net.WriteTable(MarkedForDeletion)
				net.SendToServer()
			end
		end
		
		RightArrow.DoClick = function ()
			page = page + 1
			if(page > 1) then LeftArrow:SetDisabled(false) else LeftArrow:SetDisabled(true) end
			if page >= pages or pages == 0 then RightArrow:SetDisabled(true) else RightArrow:SetDisabled(false) end
			NumPages:SetText("Page: " .. page .. " of " .. pages)
			backgroundPanel:Remove()
			local information = {}
			local lessthan = 13 * page
			local greaterthan = 13 * (page - 1)
			local backgroundPanel = vgui.Create( "DPanel", MainFrame )
			backgroundPanel:SetSize( 600, 340 )
			backgroundPanel:SetPos( 50, 65 )
			backgroundPanel:SetAlpha( 0 )
			backgroundPanel:AlphaTo( 255, 1.25, 0, function(animData, pnl)
			end)
			backgroundPanel.Paint = function()
				draw.RoundedBoxEx(8, 0, 0, backgroundPanel:GetWide() + 50, backgroundPanel:GetTall() - 20, Color(255, 255, 255, 255))		

				surface.SetDrawColor( 0, 0, 0, 255 )
				surface.DrawLine( 50, 0, 50, 321)
				surface.DrawLine( 151, 0, 151, 321)
				for k, v in pairs(tickets) do
					if(k > greaterthan) and (k <= lessthan) then
						surface.DrawLine( 0 , 21 + (25 * ((k - greaterthan) - 1)), 600, 21 + (25 * ((k - greaterthan) - 1)) )
					end
				end
			end
			
			for k, v in pairs(tickets) do
				if(k > greaterthan) and (k <= lessthan) then
					table.insert(information, v)
				end
			end
			
			for i=1, 13 do
					if(information[i]) then
					
						steamworks.RequestPlayerInfo( information[i].player )
						local test = vgui.Create( "DLabel", backgroundPanel )
						test:SetPos( 60, 2 + (25 * (i - 1) ) )
						test:SetText("Loading")
						timer.Simple(1, function()
							if(test:IsValid()) then
								test:SetText(steamworks.GetPlayerName( information[i].player ))
							end
						end)
						test:SetSize(90, 20)
						test:SetTextColor(color_black)


                        local id
                        id = tostring(information[i].id)
                        if string.len(id) < 4 then
                            for i = 1, 4 - string.len(id) do
                                id = "0" .. id
                            end
                        end

						local test2 = vgui.Create( "DLabel", backgroundPanel )
						test2:SetPos( 160, 2 + (25 * (i - 1) ) )
						test2:SetText( "#"..id..": "..string.gsub(information[i].message, "\n", " - ") )
						test2:SetSize( 425, 20 )
						test2:SetTextColor(color_black)
						
						if(DetailsButtons[i]) then
							DetailsButtons[i]:SetDisabled(false)
							DetailsButtons[i].DoClick = function()
								OpenTicketDetails(information[i])
							end
						end
						if(SelectButtons[i]) then
							SelectButtons[i]:SetDisabled(false)
							SelectButtons[i].DoClick = function()
								net.Start("UpdateTicketStatus")
								net.WriteTable(information[i])
								net.SendToServer()
							end
						end

						
						local var = 25 * (i - 1)
						local Mark = vgui.Create("DButton")
						Mark:SetPos(4, 3 + var)
						Mark:SetSize(45, 20)
						if(table.HasValue(MarkedForDeletion, information[i].id)) then
							Mark:SetTextColor(Color(255, 0, 0, 255))
						end
						Mark:SetText( os.date("%I:%M%p", information[i].timestamp) )
						Mark.Paint = function()
							draw.RoundedBoxEx(8, 0, 0, Mark:GetWide(), Mark:GetTall(), Color(0, 0, 0, 0), true, true, true, true)
						end
						Mark:SetParent(backgroundPanel)
						Mark.DoClick = function()
							if(table.HasValue(MarkedForDeletion, information[i].id)) then
								table.RemoveByValue(MarkedForDeletion, information[i].id)
								Mark:SetTextColor(color_black)
								notification.AddLegacy( "Ticket: " .. information[i].id .. " has been un-marked for deletion.", NOTIFY_HINT, 5 )
								return
							else
								Mark:SetTextColor(Color(255, 0, 0, 255))
								table.insert(MarkedForDeletion, information[i].id)
								notification.AddLegacy( "Ticket: " .. information[i].id .. " has been marked for deletion.", NOTIFY_HINT, 5 )
							end
						end
					else
						DetailsButtons[i]:SetDisabled(true)
						SelectButtons[i]:SetDisabled(true)
					end
				end
		end 
		
		LeftArrow.DoClick = function ()
			page = page -1
			NumPages:SetText("Page: " .. page .. " of " .. pages)
			if(page > 1) then LeftArrow:SetDisabled(false) else LeftArrow:SetDisabled(true) end
			if page >= pages or pages == 0 then RightArrow:SetDisabled(true) else RightArrow:SetDisabled(false) end
			backgroundPanel:Remove()
			local information = {}
			local lessthan = 13 * page
			local greaterthan = 13 * (page - 1)
			local backgroundPanel = vgui.Create( "DPanel", MainFrame )
			backgroundPanel:SetSize( 600, 340 )
			backgroundPanel:SetPos(50, 65)
			backgroundPanel:SetAlpha( 0 )
			backgroundPanel:AlphaTo( 255, 1.25, 0, function(animData, pnl)
			end)
			backgroundPanel.Paint = function()
				draw.RoundedBoxEx(8, 0, 0, backgroundPanel:GetWide() + 50, backgroundPanel:GetTall() - 20, Color(255, 255, 255, 255))		

				surface.SetDrawColor( 0, 0, 0, 255 )
				surface.DrawLine( 50, 0, 50, 321)
				surface.DrawLine( 151, 0, 151, 321)
				for k, v in pairs(tickets) do
					if(k > greaterthan) and (k <= lessthan) then
						surface.DrawLine( 0 , 21 + (25 * ((k - greaterthan) - 1)), 600, 21 + (25 * ((k - greaterthan) - 1)) )
					end
				end
			end
			
			for k, v in pairs(tickets) do
				if(k > greaterthan) and (k <= lessthan) then
					table.insert(information, v)
				end
			end
			
			for i=1, 13 do
				if(information[i]) then
				
					steamworks.RequestPlayerInfo( information[i].player )
					local test = vgui.Create( "DLabel", backgroundPanel )
					test:SetPos( 60, 2 + (25 * (i - 1) ) )
					test:SetText("Loading")
					timer.Simple(1, function()
						if(test:IsValid()) then
							test:SetText(steamworks.GetPlayerName( information[i].player ))
						end
					end)
					test:SetSize(90, 20)
					test:SetTextColor(color_black)

                    local id
                    id = tostring(information[i].id)
                    if string.len(id) < 4 then
                        for i = 1, 4 - string.len(id) do
                            id = "0" .. id
                        end
                    end

					local test2 = vgui.Create( "DLabel", backgroundPanel )
					test2:SetPos( 160, 2 + (25 * (i - 1) ) )
					test2:SetText( "#"..id..": "..string.gsub(information[i].message, "\n", " - ") )
					test2:SetSize( 425, 20 )
					test2:SetTextColor(color_black)
					
					if(DetailsButtons[i]) then
						DetailsButtons[i]:SetDisabled(false)
						DetailsButtons[i].DoClick = function()
							OpenTicketDetails(information[i])
						end
					end
					if(SelectButtons[i]) then
						SelectButtons[i]:SetDisabled(false)
						SelectButtons[i].DoClick = function()
							net.Start("UpdateTicketStatus")
							net.WriteTable(information[i])
							net.SendToServer()
						end
					end
					
					local id
					id = tostring(information[i].id)
					if string.len(id) < 4 then
						for i = 1, 4 - string.len(id) do
							id = "0" .. id
						end
					end
					
					local var = 25 * (i - 1)
					local Mark = vgui.Create("DButton")
					Mark:SetPos(4, 3 + var)
					Mark:SetSize(45, 20)
					if(table.HasValue(MarkedForDeletion, information[i].id)) then
						Mark:SetTextColor(Color(255, 0, 0, 255))
					end
					Mark:SetText(  os.date("%I:%M%p", information[i].timestamp) )
					Mark.Paint = function()
						draw.RoundedBoxEx(8, 0, 0, Mark:GetWide(), Mark:GetTall(), Color(0, 0, 0, 0), true, true, true, true)
					end
					Mark:SetParent(backgroundPanel)
					Mark.DoClick = function()
						if(table.HasValue(MarkedForDeletion, information[i].id)) then
							table.RemoveByValue(MarkedForDeletion, information[i].id)
							Mark:SetTextColor(color_black)
							notification.AddLegacy( "Ticket: " .. information[i].id .. " has been un-marked for deletion.", NOTIFY_HINT, 5 )
							return
						else
							Mark:SetTextColor(Color(255, 0, 0, 255))
							table.insert(MarkedForDeletion, information[i].id)
							notification.AddLegacy( "Ticket: " .. information[i].id .. " has been marked for deletion.", NOTIFY_HINT, 5 )
						end
					end
				else
					DetailsButtons[i]:SetDisabled(true)
					SelectButtons[i]:SetDisabled(true)
				end
			end
		end	
		
		timer.Create("TicketAutoRefresh", 10, 0, function()
			net.Start("RefreshTickets")
			net.SendToServer()
		end)
		
		net.Receive("RefreshTickets", function( len, pl )
			tickets = net.ReadTable()
			local information = {}
			local pages = 0
			for i = 1, #tickets, 13 do
				pages = pages + 1
			end
			if(page > pages) then page = page - 1 end
			if(page > 1) then LeftArrow:SetDisabled(false) else LeftArrow:SetDisabled(true) end
			if page >= pages or pages == 0 then RightArrow:SetDisabled(true) else RightArrow:SetDisabled(false) end
			if not (timer.Exists( "TicketAutoRefresh" )) then return end
			NumPages:SetText("Page: " .. page .. " of " .. pages)
			backgroundPanel:Remove()
			local lessthan = 13 * page
			local greaterthan = 13 * (page - 1)
			for k , v in pairs(tickets) do
				if(k > greaterthan) and (k <= lessthan) then
					table.insert(information, v)
				end
			end
			local backgroundPanel = vgui.Create( "DPanel", MainFrame )
			backgroundPanel:SetSize( 600, 340 )
			backgroundPanel:SetPos(50, 65)
			backgroundPanel.Paint = function()
				draw.RoundedBoxEx(8, 0, 0, backgroundPanel:GetWide() + 50, backgroundPanel:GetTall() - 20, Color(255, 255, 255, 255))
				surface.SetDrawColor( 0, 0, 0, 255 )
				surface.DrawLine( 50, 0, 50, 321)
				surface.DrawLine( 151, 0, 151, 321)
				
				for k, v in pairs(tickets) do
					if(k > greaterthan) and (k <= lessthan) then
						surface.DrawLine( 0 , 21 + (25 * ((k - greaterthan) - 1)), 600, 21 + (25 * ((k - greaterthan) - 1)) )
					end
				end
			end
			
			for i=1, 13 do
				if(information[i]) then
					steamworks.RequestPlayerInfo( information[i].player )
					local test = vgui.Create( "DLabel", backgroundPanel )
					test:SetPos( 60, 2 + (25 * (i - 1) ) )
					test:SetText(steamworks.GetPlayerName( tickets[i].player ))
					test:SetSize(90, 20)
					test:SetTextColor(color_black)


                    local id
                    id = tostring(information[i].id)
                    if string.len(id) < 4 then
                        for i = 1, 4 - string.len(id) do
                            id = "0" .. id
                        end
                    end

					local test2 = vgui.Create( "DLabel", backgroundPanel )
					test2:SetPos( 160, 2 + (25 * (i - 1) ) )
					test2:SetText(  "#"..id..": "..string.gsub(information[i].message, "\n", " - ") )
					test2:SetSize( 425, 20 )
					test2:SetTextColor(color_black)
					

					if(DetailsButtons[i]) then
						DetailsButtons[i]:SetDisabled(false)
						DetailsButtons[i].DoClick = function()
							OpenTicketDetails(information[i])
						end
					end
					if(SelectButtons[i]) then
						SelectButtons[i]:SetDisabled(false)
						SelectButtons[i].DoClick = function()
							net.Start("UpdateTicketStatus")
							net.WriteTable(information[i])
							net.SendToServer()
						end
					end

					local Mark = vgui.Create("DButton")
					Mark:SetPos(4, 3 + (25 * (i - 1)))
					Mark:SetSize(45, 20)
					if(table.HasValue(MarkedForDeletion, information[i].id)) then
						Mark:SetTextColor(Color(255, 0, 0, 255))
					end
					Mark:SetText(  os.date("%I:%M%p", information[i].timestamp) )
					Mark.Paint = function()
						draw.RoundedBoxEx(8, 0, 0, Mark:GetWide(), Mark:GetTall(), Color(0, 0, 0, 0), true, true, true, true)
					end
					Mark:SetParent(backgroundPanel)
					Mark.DoClick = function()
						if(table.HasValue(MarkedForDeletion, information[i].id)) then
							table.RemoveByValue(MarkedForDeletion, information[i].id)
							Mark:SetTextColor(color_black)
							notification.AddLegacy( "Ticket: " .. information[i].id .. " has been un-marked for deletion.", NOTIFY_HINT, 5 )
							return
						else
							Mark:SetTextColor(Color(255, 0, 0, 255))
							table.insert(MarkedForDeletion, information[i].id)
							notification.AddLegacy( "Ticket: " .. information[i].id .. " has been marked for deletion.", NOTIFY_HINT, 5 )
						end
					end
				else
					DetailsButtons[i]:SetDisabled(true)
					SelectButtons[i]:SetDisabled(true)
				end
			end
		end)
			
end)

net.Receive("UpdateTicketStatus", function( len )
	local ip = net.ReadString()
    if ip == game.GetIPAddress() then
        LocalPlayer():ChatPrint("Ticket claimed. Player is on this server, no redirect")
        return
    end
    LocalPlayer():ChatPrint("Ticket claimed. You are being redireced in 3 seconds to: "..ip)
	timer.Simple(3, function()

		LocalPlayer():ConCommand('connect ' .. ip)
	end)
end)

function OpenTicketDetails(tbl)
	mainpanel:SetVisible( false )
	local DonateFrame = vgui.Create("DFrame") 
	DonateFrame:SetSize( 400, 275 )
	DonateFrame:Center() 
	DonateFrame:SetTitle("") 
	DonateFrame:SetVisible( true )
	DonateFrame:SetDraggable( false ) 
	DonateFrame:ShowCloseButton( false )
	DonateFrame:SetSizable( false )
	DonateFrame:SetDeleteOnClose( true )
	DonateFrame.Paint = function()
		draw.RoundedBox( 8, 0, 0, DonateFrame:GetWide(), DonateFrame:GetTall(), Color( 0, 0, 0, 210 ) )
		surface.SetTextColor( 255, 255, 255, 255 )
		surface.SetTextPos( DonateFrame:GetWide()/2-62, 0 ) 
		surface.SetFont("Trebuchet24")
		surface.DrawText( "Ticket Details" )
		draw.RoundedBoxEx(8, 0, 0, DonateFrame:GetWide(), 25, Color(100, 100, 100, 225), true, true, false, false)
	end		
	DonateFrame:MakePopup()
	DonateFrame:RequestFocus()
	
	local DScrollPanel = vgui.Create( "DScrollPanel", DonateFrame )
	DScrollPanel:SetSize( 350, 125 )
	DScrollPanel:SetPos( 25, 115 )
	DScrollPanel.Paint = function()
		draw.RoundedBoxEx(8, 0, 0, DScrollPanel:GetWide(), DScrollPanel:GetTall() , Color(100, 100, 100, 225))
	end
	
	local sbar = DScrollPanel:GetVBar()
	function sbar:Paint(w, h)
		draw.RoundedBox(0, 0, 0, w, h, Color(0, 0, 0, 100))
	end
	function sbar.btnUp:Paint(w, h)
		draw.RoundedBox(0, 0, 0, w, h, Color(125, 125, 125, 225))
	end
	function sbar.btnDown:Paint(w, h)
		draw.RoundedBox(0, 0, 0, w, h, Color(125, 125, 125, 225))
	end
	function sbar.btnGrip:Paint(w, h)
		draw.RoundedBox(0, 0, 0, w, h, Color(225, 225, 225, 175))
	end
	
	local NAMEDesc = vgui.Create("DLabel",DonateFrame)
	NAMEDesc:SetTextColor(color_white)
	NAMEDesc:SetText("SERVER: " .. tbl.servname)
	NAMEDesc:SetPos( 25, 30 )
	NAMEDesc:SizeToContents()
	
	local tid
	tid = tostring(tbl.id)
	if string.len(tid) < 4 then
		for i = 1, 4 - string.len(tid) do
			tid = "0" .. tid
		end
    end



	local NAMEDesc = vgui.Create("DLabel",DonateFrame)
	NAMEDesc:SetTextColor(color_white)
	NAMEDesc:SetText("TICKET ID: " .. tid)
	NAMEDesc:SetPos( 25, 50 )
	NAMEDesc:SizeToContents()

    local NAMEDesc = vgui.Create("DLabel",DonateFrame)
    NAMEDesc:SetTextColor(color_white)
    NAMEDesc:SetText("Time: ".. os.date( "%I:%M%p - %d/%m/%Y" ,tbl.timestamp))
    NAMEDesc:SetPos( 25, 70 )
    NAMEDesc:SizeToContents()
		
	local NAMEDesc = vgui.Create("DLabel",DonateFrame)
	NAMEDesc:SetTextColor(color_white)
	NAMEDesc:SetText("MESSAGE: ")
	NAMEDesc:SetPos( 25, 90 )
	NAMEDesc:SizeToContents()
	
	local id = 1
	for a,b in pairs( indent(tbl.message, 60, true)) do
		local NAMEDesc = vgui.Create("DLabel",DScrollPanel)
		NAMEDesc:SetTextColor(color_white)
		NAMEDesc:SetText(b)
		NAMEDesc:SetPos(10 ,14 * id)
		NAMEDesc:SizeToContents()
		id = id + 1
	end
	
	local DermaButton = vgui.Create( "DButton" )
	DermaButton:SetText( "X" )	
	DermaButton:SetPos( 375, 0 )
	DermaButton:SetSize( 25, 25 )
	DermaButton:SetParent( DonateFrame )
	DermaButton.Paint = function() 
		draw.RoundedBoxEx(8, 0, 0, DermaButton:GetWide(), DermaButton:GetTall(), Color(50, 50, 50, 225), false, true, false, false)
	end
	DermaButton.DoClick = function ()
		DonateFrame:Close()
	end
	DonateFrame.OnClose = function ()
		mainpanel:SetVisible( true )
	end

end

function indent( message, nbrChar, first )
    if string.find(message,"<color=" ) then
                nbrChar = nbrChar + 25
        end
    local strTbl = string.Explode( "", message )
    for i=nbrChar,#strTbl,nbrChar do
        if strTbl[i] == " " then
            strTbl[i] = "\n"
        else
            for k=i,1,-1 do             
                if strTbl[k] == " " then
                    strTbl[k] = "\n"
                    break
                end
            end
        end
    end
	if first then
		return string.Explode("\n", table.concat(strTbl))
	else
		return table.concat(strTbl)
	end
 
end


net.Receive("CreateTicket", function(len, pl)
	local DonateFrame = vgui.Create("DFrame") 
	DonateFrame:SetSize( 350, 225 )
	DonateFrame:Center() 
	DonateFrame:SetTitle("") 
	DonateFrame:SetVisible( true )
	DonateFrame:SetDraggable( false ) 
	DonateFrame:ShowCloseButton( false )
	DonateFrame:SetSizable( false )
	DonateFrame.Paint = function()
		draw.RoundedBox( 8, 0, 0, DonateFrame:GetWide(), DonateFrame:GetTall(), Color( 0, 0, 0, 210 ) )
		surface.SetTextColor( 255, 255, 255, 255 )
		surface.SetTextPos( DonateFrame:GetWide()/2-62, 0 ) 
		surface.SetFont("Trebuchet24")
		surface.DrawText( "Create Ticket" )
		draw.RoundedBoxEx(8, 0, 0, DonateFrame:GetWide(), 25, Color(100, 100, 100, 225), true, true, false, false)
	end		
	DonateFrame:MakePopup() 
	
	local NAMEDesc = vgui.Create("DLabel", DonateFrame)
	NAMEDesc:SetPos( 114, 33 )
	NAMEDesc:SetFont("Trebuchet18")
	NAMEDesc:SetTextColor(Color(255,255,255,255))
	NAMEDesc:SetText( "Enter Complaint Here" )
	NAMEDesc:SizeToContents()
	
	local NAMEBox = vgui.Create("DTextEntry", DonateFrame)
	NAMEBox:SetPos( 37, 53 )
	NAMEBox:SetSize( 275, 130 )
	NAMEBox:SetMultiline( true )
	NAMEBox:SetValue("")
	NAMEBox:SetEnterAllowed( true )
	
		local RemoveButton = vgui.Create( "DButton" )
	RemoveButton:SetText( "Cancel Ticket" )	
	RemoveButton:SetPos( 38, 190 )
	RemoveButton:SetSize( 100, 25 )
	RemoveButton.Paint = function()
	draw.RoundedBoxEx(8, 0, 0, RemoveButton:GetWide(), RemoveButton:GetTall(), Color(255, 255, 255, 255), false, false, false, false)
	end
	RemoveButton:SetParent( DonateFrame )
	RemoveButton.DoClick = function ()
		DonateFrame:Remove()
	end
	
	local RightArrow = vgui.Create( "DButton" )
	RightArrow:SetText( "Submit Ticket" )	
	RightArrow:SetPos( 211, 190 )
	RightArrow:SetSize( 100, 25 )
	RightArrow.Paint = function()
	draw.RoundedBoxEx(8, 0, 0, RightArrow:GetWide(), RightArrow:GetTall(), Color(255, 255, 255, 255), false, false, false, false)
	end
	RightArrow:SetParent( DonateFrame )
	RightArrow.DoClick = function ()
		if(string.len(NAMEBox:GetValue()) > 1000) then notification.AddLegacy("That is too many characters, try again. (" .. tostring(string.len(NAMEBox:GetValue() - 1000)) .. " characters over)" , NOTIFY_HINT, 2 ) return end
		net.Start("CreateTicket")
		net.WriteString(NAMEBox:GetValue())
		net.SendToServer()
		DonateFrame:Remove()
	end
	
	local DermaButton = vgui.Create( "DButton" )
	DermaButton:SetText( "X" )	
	DermaButton:SetPos( 325, 0 )
	DermaButton:SetSize( 25, 25 )
	DermaButton:SetParent( DonateFrame )
	DermaButton.Paint = function() 
		draw.RoundedBoxEx(8, 0, 0, DermaButton:GetWide(), DermaButton:GetTall(), Color(50, 50, 50, 225), false, true, false, false)
	end
	DermaButton.DoClick = function ()
		DonateFrame:Remove()
	end
	
end)


local function PaintMainWindow()
    local ply =  LocalPlayer()




    local width = REWARDS.FindMessageBoxWidth("Ticket claimed.")


        surface.SetDrawColor(Color(50,90,90,240))
        surface.DrawRect(107, 50, 500, 50)

    surface.SetDrawColor(Color(255,00,0,240))
    surface.DrawRect(107, 50, 500, 50)
         draw.DrawText("Ticket claimed. !tickets to show. !closeticket <message> to close", "Bebas24Font", 150, 65, color_white, 0)


end

local mainwindow = false
local function modreq_OpenBar( settings )
    if not LocalPlayer() then return end

    if not mainwindow then
        REWARDS.CurrentAlpha = 0
        mainwindow = vgui.Create( "DFrame" )
        mainwindow:SetSize( ScrW(), 170 + 25 + 50)
        mainwindow:SetPos(200,0)
        mainwindow:SetDraggable( false )
        mainwindow:ShowCloseButton( true )
        mainwindow:SetTitle( "" )
        mainwindow.Paint = PaintMainWindow
    end
end
local function modreq_CloseBar( settings )
    if not LocalPlayer() then return end

    if mainwindow then
        mainwindow:Remove()
        mainwindow = false
    end
end

concommand.Add("modreq_openbar", modreq_OpenBar)
concommand.Add("modreq_closebar", modreq_CloseBar)