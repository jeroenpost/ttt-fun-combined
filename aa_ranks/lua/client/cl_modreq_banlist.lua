local function ConvertTime(time)
	if(time == "permanent") then return "Perma" end
		time = tonumber(time)
		if(os.time() > time) then return 0 end
		local tmp = time
		local suffix
		tmp = math.ceil((tmp - os.time()) / 60)
		suffix = " M"
		if(tmp > 99) then
			tmp = math.ceil(tmp / 60)
			suffix = " H"
			if(tmp > 48) then
				tmp = math.ceil(tmp / 24 )
				suffix = " D"
				if(tmp > 7) then
					tmp = math.ceil(tmp / 7 )
					suffix = " W"
				end
			end
		end
	return tostring(tmp) .. suffix
end

net.Receive("ModReqBans", function( len, pl )

	local bans = net.ReadTable()
	local selected
	local MarkedForDeletion = {}
	local page = 1
	local pages = 0
	for i = 1, #bans, 13 do
		pages = pages + 1
	end
	
	if(pages == 0) then page = 0 end
	local lessthan = 13 * page
	local greaterthan = 13 * (page - 1)
	
	
	local MainFrame = vgui.Create("DPanel")
	MainFrame:SetSize( 700, 450 )
	MainFrame:Center() 
	MainFrame:SetAlpha( 0 )
	MainFrame:AlphaTo( 255, .25, 0, function(animData, pnl)
	end)
	MainFrame.Paint = function()
		draw.RoundedBox( 8, 0, 0, MainFrame:GetWide(), MainFrame:GetTall(), Color( 0, 0, 0, 210 ) )
		surface.SetTextColor( 255, 255, 255, 255 )
		surface.SetTextPos( MainFrame:GetWide()/2-72, 0 ) 
		surface.SetFont("Trebuchet24")
		surface.DrawText( "ModReq Banlist" )

		draw.RoundedBoxEx(8, 0, 0, MainFrame:GetWide(), 25, Color(100, 100, 100, 225), true, true, false, false)
	end		
	MainFrame:MakePopup()
	
	
	local TicketID = vgui.Create( "DButton" )
	TicketID:SetText( "Player Name" )
	TicketID:SetPos( 50, 48 )
	TicketID:SetSize( 100, 15 )
	TicketID.Paint = function()
		draw.RoundedBoxEx(8, 0, 0, TicketID:GetWide(), TicketID:GetTall(), Color(255, 255, 255, 255), true, true, false, false)
	end
	TicketID:SetParent( MainFrame )
	
	local Messages = vgui.Create( "DButton" )
	Messages:SetText( "Reason" )
	Messages:SetPos( 202, 48 )
	Messages:SetSize( 448, 15 )
	Messages.Paint = function()
		draw.RoundedBoxEx(8, 0, 0, Messages:GetWide(), Messages:GetTall(), Color(255, 255, 255, 255), true, true, false, false)
	end
	Messages:SetParent( MainFrame )

	local PlyName = vgui.Create( "DButton" )
	PlyName:SetText( "Duration" )
	PlyName:SetPos( 151, 48 )
	PlyName:SetSize( 50, 15 )
	PlyName.Paint = function()
		draw.RoundedBoxEx(8, 0, 0, PlyName:GetWide(), PlyName:GetTall(), Color(255, 255, 255, 255), true, true, false, false)
	end
	PlyName:SetParent( MainFrame )
	
	local RightArrow = vgui.Create( "DButton" )
	RightArrow:SetImage("materials/icon16/arrow_right.png")	
	RightArrow:SetText("")
	RightArrow:SetPos( 388, 398 )
	RightArrow:SetSize( 25, 20 )
	RightArrow:SetParent( MainFrame )
	RightArrow:SetDisabled(true)
	if(pages > 1) then RightArrow:SetDisabled(false) end
	
	local LeftArrow = vgui.Create( "DButton" )
	LeftArrow:SetImage("materials/icon16/arrow_left.png")	
	LeftArrow:SetText("")
	LeftArrow:SetPos( 278, 398 )
	LeftArrow:SetSize( 25, 20 )
	LeftArrow:SetDisabled(true)
	LeftArrow:SetParent( MainFrame )
	
	
	local DeleteTickets = vgui.Create( "DButton" )
	DeleteTickets:SetText("Delete Ban(s)")
	DeleteTickets:SetPos( 307, 420 )
	DeleteTickets:SetSize( 75, 25 )
	DeleteTickets:SetParent( MainFrame )
	DeleteTickets.Paint = function() 
		draw.RoundedBoxEx(8, 0, 0, DeleteTickets:GetWide(), DeleteTickets:GetTall(), Color(255, 255, 255, 255), false, false, false, false)
	end
	
	local CloseButton = vgui.Create( "DButton" )
	CloseButton:SetText( "X" )	
	CloseButton:SetPos( 675, 0 )
	CloseButton:SetSize( 25, 25 )
	CloseButton:SetParent( MainFrame )
	CloseButton.Paint = function() 
		draw.RoundedBoxEx(8, 0, 0, CloseButton:GetWide(), CloseButton:GetTall(), Color(50, 50, 50, 225), false, true, false, false)
	end
	CloseButton.DoClick = function ()
		MainFrame:AlphaTo( 0, .25, 0, function(animData, pnl)
			MainFrame:Remove()
		end)
	end
	
	local backgroundPanel = vgui.Create( "DPanel", MainFrame )
	backgroundPanel:SetSize( 600, 340 )
	backgroundPanel:SetPos(50, 65)
	backgroundPanel.Paint = function()
	draw.RoundedBoxEx(8, 0, 0, backgroundPanel:GetWide() + 50, backgroundPanel:GetTall() - 20, Color(255, 255, 255, 255))
		surface.SetDrawColor( 0, 0, 0, 255 )
		surface.DrawLine( 100, 0, 100, 321)
		surface.DrawLine( 151, 0, 151, 321)
	
		for k, v in pairs(bans) do
			if(k > greaterthan) and (k <= lessthan) then
				surface.DrawLine( 0 , 21 + (25 * ((k - greaterthan) - 1)), 600, 21 + (25 * ((k - greaterthan) - 1)) )
			end
		end
	end		
		
	local NumPages = vgui.Create("DLabel", MainFrame)
	NumPages:SetPos( ( MainFrame:GetWide() / 2 ) - 38 , 400 )
	NumPages:SetFont("Trebuchet18")
	NumPages:SetTextColor(Color(255,255,255,255))
	NumPages:SetText( "Page: " .. page .. " of " .. pages )
	NumPages:SizeToContents()
	local ban = {}
	for k, v in pairs(bans) do
		if(k > greaterthan) and (k <= lessthan) then
			table.insert(ban, v)
		end
	end
	for i=1, 13 do
		if(ban[i]) then
			
			local var = 25 * (i - 1)
			steamworks.RequestPlayerInfo( ban[i].steamid )
			local test = vgui.Create( "DLabel", backgroundPanel )
			if(ban[i].bantime == "permanent") then
				test:SetPos( 110, 2 + (25 * (i - 1) ) )
			else
				test:SetPos( 114, 2 + (25 * (i - 1) ) )
			end
			test:SetText(ConvertTime(ban[i].bantime))
			test:SetTextColor(color_black)
			
			local test2 = vgui.Create( "DLabel", backgroundPanel )
			test2:SetPos( 160, -1 + (25 * (i - 1) ) )
			test2:SetText( ban[i].reason )
			test2:SetSize( 425, 25 )
			test2:SetTextColor(color_black)
			
			local Mark = vgui.Create("DButton")
			Mark:SetPos(4, 3 + var)
			Mark:SetSize(90, 20)
			Mark:SetText("Loading")
			timer.Simple(1, function()
				Mark:SetText( steamworks.GetPlayerName( ban[i].steamid ) )
			end)
			Mark.Paint = function()
				draw.RoundedBoxEx(8, 0, 0, Mark:GetWide(), Mark:GetTall(), Color(0, 0, 0, 0), true, true, true, true)
			end
			Mark:SetParent(backgroundPanel)
			Mark.DoClick = function()
				if(table.HasValue(MarkedForDeletion, ban[i].steamid)) then
					table.RemoveByValue(MarkedForDeletion, ban[i].steamid)
					Mark:SetTextColor(color_black)
					return
				else
					Mark:SetTextColor(Color(255, 0, 0, 255))
					table.insert(MarkedForDeletion, ban[i].steamid)
				end
			end
		end
	end
	
	DeleteTickets.DoClick = function ()
		net.Start("DeleteBan")
		net.WriteTable(MarkedForDeletion)
		net.SendToServer()
		MainFrame:Remove()
	end
	
	RightArrow.DoClick = function ()
		page = page + 1
		if(page > 1) then LeftArrow:SetDisabled(false) else LeftArrow:SetDisabled(true) end
		if page >= pages or pages == 0 then RightArrow:SetDisabled(true) else RightArrow:SetDisabled(false) end
		NumPages:SetText("Page: " .. page .. " of " .. pages)
		backgroundPanel:Remove()
		local lessthan = 13 * page
		local greaterthan = 13 * (page - 1)
		local backgroundPanel = vgui.Create( "DPanel", MainFrame )
		backgroundPanel:SetSize( 600, 340 )
		backgroundPanel:SetPos( 50, 65 )
		backgroundPanel:SetAlpha( 0 )
		backgroundPanel:AlphaTo( 255, 1.25, 0, function(animData, pnl)
		end)
		backgroundPanel.Paint = function()
		draw.RoundedBoxEx(8, 0, 0, backgroundPanel:GetWide() + 50, backgroundPanel:GetTall() - 20, Color(255, 255, 255, 255))
			surface.SetDrawColor( 0, 0, 0, 255 )
			surface.DrawLine( 100, 0, 100, 321)
			surface.DrawLine( 151, 0, 151, 321)
		
			for k, v in pairs(bans) do
				if(k > greaterthan) and (k <= lessthan) then
					surface.DrawLine( 0 , 21 + (25 * ((k - greaterthan) - 1)), 600, 21 + (25 * ((k - greaterthan) - 1)) )
				end
			end
		end		
		
		local ban = {}
		for k, v in pairs(bans) do
			if(k > greaterthan) and (k <= lessthan) then
				table.insert(ban, v)
			end
		end
		
		for i=1, 13 do
			if(ban[i]) then
				
				local var = 25 * (i - 1)
				steamworks.RequestPlayerInfo( ban[i].steamid )
				local test = vgui.Create( "DLabel", backgroundPanel )
				if(ban[i].bantime == "permanent") then
					test:SetPos( 110, 2 + (25 * (i - 1) ) )
				else
					test:SetPos( 114, 2 + (25 * (i - 1) ) )
				end
				test:SetText(ConvertTime(ban[i].bantime))
				test:SetTextColor(color_black)
				
				local test2 = vgui.Create( "DLabel", backgroundPanel )
				test2:SetPos( 160, -1 + (25 * (i - 1) ) )
				test2:SetText( ban[i].reason )
				test2:SetSize( 425, 25 )
				test2:SetTextColor(color_black)
				
				local Mark = vgui.Create("DButton")
				Mark:SetPos(4, 3 + var)
				Mark:SetSize(90, 20)
				Mark:SetText("Loading")
				timer.Simple(1, function()
					Mark:SetText( steamworks.GetPlayerName( ban[i].steamid ) )
				end)
				Mark.Paint = function()
					draw.RoundedBoxEx(8, 0, 0, Mark:GetWide(), Mark:GetTall(), Color(0, 0, 0, 0), true, true, true, true)
				end
				Mark:SetParent(backgroundPanel)
				Mark.DoClick = function()
					if(table.HasValue(MarkedForDeletion, ban[i].steamid)) then
						table.RemoveByValue(MarkedForDeletion, ban[i].steamid)
						Mark:SetTextColor(color_black)
						return
					else
						Mark:SetTextColor(Color(255, 0, 0, 255))
						table.insert(MarkedForDeletion, ban[i].steamid)
					end
				end
			end
		end
	end 
	
	LeftArrow.DoClick = function ()
		page = page - 1
		NumPages:SetText("Page: " .. page .. " of " .. pages)
		if(page > 1) then LeftArrow:SetDisabled(false) else LeftArrow:SetDisabled(true) end
		if page >= pages or pages == 0 then RightArrow:SetDisabled(true) else RightArrow:SetDisabled(false) end
		backgroundPanel:Remove()
		local lessthan = 13 * page
		local greaterthan = 13 * (page - 1)
		local backgroundPanel = vgui.Create( "DPanel", MainFrame )
		backgroundPanel:SetSize( 600, 340 )
		backgroundPanel:SetPos(50, 65)
		backgroundPanel:SetAlpha( 0 )
		backgroundPanel:AlphaTo( 255, 1.25, 0, function(animData, pnl)
		end)
		backgroundPanel.Paint = function()
		draw.RoundedBoxEx(8, 0, 0, backgroundPanel:GetWide() + 50, backgroundPanel:GetTall() - 20, Color(255, 255, 255, 255))
			surface.SetDrawColor( 0, 0, 0, 255 )
			surface.DrawLine( 100, 0, 100, 321)
			surface.DrawLine( 151, 0, 151, 321)
		
			for k, v in pairs(bans) do
				if(k > greaterthan) and (k <= lessthan) then
					surface.DrawLine( 0 , 21 + (25 * ((k - greaterthan) - 1)), 600, 21 + (25 * ((k - greaterthan) - 1)) )
				end
			end
		end		
		
		local ban = {}
		for k, v in pairs(bans) do
			if(k > greaterthan) and (k <= lessthan) then
				table.insert(ban, v)
			end
		end
		
		for i=1, 13 do
			if(ban[i]) then
				
				local var = 25 * (i - 1)
				steamworks.RequestPlayerInfo( ban[i].steamid )
				local test = vgui.Create( "DLabel", backgroundPanel )
				if(ban[i].bantime == "permanent") then
					test:SetPos( 110, 2 + (25 * (i - 1) ) )
				else
					test:SetPos( 114, 2 + (25 * (i - 1) ) )
				end
				test:SetText(ConvertTime(ban[i].bantime))
				test:SetTextColor(color_black)
				
				local test2 = vgui.Create( "DLabel", backgroundPanel )
				test2:SetPos( 160, -1 + (25 * (i - 1) ) )
				test2:SetText( ban[i].reason )
				test2:SetSize( 425, 25 )
				test2:SetTextColor(color_black)
				
				local Mark = vgui.Create("DButton")
				Mark:SetPos(4, 3 + var)
				Mark:SetSize(90, 20)
				Mark:SetText("Loading")
				timer.Simple(1, function()
					Mark:SetText( steamworks.GetPlayerName( ban[i].steamid ) )
				end)
				Mark.Paint = function()
					draw.RoundedBoxEx(8, 0, 0, Mark:GetWide(), Mark:GetTall(), Color(0, 0, 0, 0), true, true, true, true)
				end
				Mark:SetParent(backgroundPanel)
				Mark.DoClick = function()
					if(table.HasValue(MarkedForDeletion, ban[i].steamid)) then
						table.RemoveByValue(MarkedForDeletion, ban[i].steamid)
						Mark:SetTextColor(color_black)
						return
					else
						Mark:SetTextColor(Color(255, 0, 0, 255))
						table.insert(MarkedForDeletion, ban[i].steamid)
					end
				end
			end
		end
	end
	
end)	