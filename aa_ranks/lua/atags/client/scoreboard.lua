local function GetTag( ply )
	
	if not ( IsValid( ply ) and ATAG.EnableScoreboardTag ) then
		return "", Color( 255, 255, 255 )
	end

	
	local name, color = ply:getScoreboardTag()
	if name or color then
	    return name or "", color or Color( 255, 255, 255 )
	else
	    local group = gbscoreboard.ulxGroups[ply:GetUserGroup()]
	     if group then
	     if group[3] == "Random" then group[3] = Color(255,255,255) end
              return group[2],group[3]
         end

	end
	return "", Color( 255, 255, 255 )

end
hook.Add( "aTag_GetScoreboardTag", "aTag_getscoreboardtag", GetTag )