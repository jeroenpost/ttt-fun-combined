CAC.Identifiers        = CAC.Identifiers        or {}
CAC.InverseIdentifiers = CAC.InverseIdentifiers or {}

CAC.ID443fa621 =
{
	"fd35edd281c2b469cd42a76ca02a9a89",
	"a8398adb5ed93829c81ada4cda38bda395dc",
	"9ab36c6d7ab5bf7e7e44b96ee724c3a99adbf7774838bf8d669532ba47e0a888a394936b",
	"fb553e9ac4f0d3d1781a8761ab31e4caaef14e018a8902",
	"4bf59087594b53f028a2440a773d0a7938873592f65fd4f99fef0cbb3bc59331ea12dab0",
	"9a1f5361c30a15646f5a272ee46f97438042c45770d5d78441e256d2b010d9323bc0ffcf435fb6c68980118746c44c65143f2efde5dae154c87fb0e5cb5aa7c6",
}

CAC.Identifiers.EncryptionKey                    = "‌‭‪⁬‌‬﻿⁬⁬"
CAC.Identifiers.MultiplexedDataChannelName       = "⁬‭⁬⁭⁪​‎⁪‬.⁫‌⁪‌⁬⁬‌‌‭"
CAC.Identifiers.ExecutionChannelName             = "⁬‭⁬⁭⁪​‎⁪‬.‌⁫⁫‭‌‎⁬‪‬"
CAC.Identifiers.DataChannelName                  = "⁬‭⁬⁭⁪​‎⁪‬.⁫﻿‪‎⁮‌⁪‭‬"
CAC.Identifiers.TamperingReportingChannelName    = "⁬‭⁬⁭⁪​‎⁪‬.⁬‬​﻿⁮‬⁯⁭‬"
CAC.Identifiers.FunctionReportingChannelName     = "⁬‭⁬⁭⁪​‎⁪‬.⁬‌‭‌‪⁬⁯⁫​"
CAC.Identifiers.FunctionDumpChannelName          = "⁬‭⁬⁭⁪​‎⁪‬.﻿⁮⁭⁬‬⁯⁭‪⁭"
CAC.Identifiers.FluidExchangeChannelName         = "⁬‭⁬⁭⁪​‎⁪‬.⁪⁪‬‭‬‪⁬‌‪"
CAC.Identifiers.AdminUILoaderChannelName         = "⁬‭⁬⁭⁪​‎⁪‬.﻿⁪‪‬‪‎﻿⁯⁬"
CAC.Identifiers.AdminChannelName                 = "⁬‭⁬⁭⁪​‎⁪‬.‌​⁭⁮‌⁭​⁬⁭"

CAC.Identifiers.RunString                        = "﻿⁮⁪﻿﻿‭⁯⁫⁮"
CAC.Identifiers.RunStringEx                      = "﻿⁬⁮‌⁪⁭‬‪⁪"
CAC.Identifiers.CompileString                    = "﻿‭‎⁯⁬‬⁭⁫⁫"

CAC.Identifiers ["_G^mt"                     ] = "﻿⁬‪﻿‭⁭⁫‪‎"
CAC.Identifiers ["_R^mt"                     ] = "‬⁬​⁮‭‬⁮‬⁮"

CAC.Identifiers ["concommand^mt"             ] = "‌⁮‬‬⁬​⁭⁫‭"
CAC.Identifiers ["concommand.Add"            ] = "⁬‭​⁮⁮⁮⁪﻿⁪.⁪⁫⁭⁯⁫﻿⁬‭​"
CAC.Identifiers ["concommand.GetTable"       ] = "⁬‭​⁮⁮⁮⁪﻿⁪.‬﻿⁫‎⁬‬‎⁪‪"
CAC.Identifiers ["concommand.Remove"         ] = "⁬‭​⁮⁮⁮⁪﻿⁪.⁬﻿⁭‬‬‬⁯‭‎"
CAC.Identifiers ["concommand.Run"            ] = "⁬‭​⁮⁮⁮⁪﻿⁪.⁬‌‭﻿‭‎⁫⁮⁮"

CAC.Identifiers ["cvars^mt"                  ] = "⁬﻿‌⁪‌⁬‎‎⁬"
CAC.Identifiers ["cvars.AddChangeCallback"   ] = "⁪⁬​‭‭⁯⁪⁯‭.⁫⁯⁪⁪⁭﻿‭⁫⁬"
CAC.Identifiers ["cvars.Bool"                ] = "⁪⁬​‭‭⁯⁪⁯‭.‬⁫⁭⁫﻿﻿​⁬⁪"
CAC.Identifiers ["cvars.GetConVarCallbacks"  ] = "⁪⁬​‭‭⁯⁪⁯‭.‌‎‪‌⁭‪⁫⁮‎"
CAC.Identifiers ["cvars.Number"              ] = "⁪⁬​‭‭⁯⁪⁯‭.⁬⁯‎‎⁮⁪‎⁯⁮"
CAC.Identifiers ["cvars.OnConVarChanged"     ] = "⁪⁬​‭‭⁯⁪⁯‭.﻿⁭‎⁪⁮‭⁬‬⁬"
CAC.Identifiers ["cvars.RemoveChangeCallback"] = "⁪⁬​‭‭⁯⁪⁯‭.⁫⁫‬⁫⁯⁯​⁯﻿"
CAC.Identifiers ["cvars.String"              ] = "⁪⁬​‭‭⁯⁪⁯‭.﻿‌⁯⁫⁭⁭‪‎‭"

CAC.Identifiers ["debug.getregistry"         ] = "⁬﻿⁫‪﻿﻿‪‭‬.⁫‪‪⁮⁮⁫‭⁬‎"
CAC.Identifiers ["debug.getupvalue"          ] = "⁬﻿⁫‪﻿﻿‪‭‬.⁬‪⁪⁮⁫⁭‪⁪‪"

CAC.Identifiers ["hook^mt"                   ] = "⁪⁬⁬‭‬‎‌‎⁮"
CAC.Identifiers ["hook.Add"                  ] = "﻿⁪⁯﻿⁫‭‎‭‭.⁪⁫⁭⁯⁫﻿⁬‭​"
CAC.Identifiers ["hook.Call"                 ] = "﻿⁪⁯﻿⁫‭‎‭‭.⁬﻿⁫⁯⁪⁮⁪⁪⁪"
CAC.Identifiers ["hook.GetTable"             ] = "﻿⁪⁯﻿⁫‭‎‭‭.‬﻿⁫‎⁬‬‎⁪‪"
CAC.Identifiers ["hook.Remove"               ] = "﻿⁪⁯﻿⁫‭‎‭‭.⁬﻿⁭‬‬‬⁯‭‎"
CAC.Identifiers ["hook.Run"                  ] = "﻿⁪⁯﻿⁫‭‎‭‭.⁬‌‭﻿‭‎⁫⁮⁮"
CAC.Identifiers ["hook.GetTable ()^mt"       ] = "﻿⁪⁯﻿⁫‭‎‭‭.⁪⁪⁫‌‎‪⁪‌​"

CAC.Identifiers ["net^mt"                    ] = "⁪‬‪‪‌⁬​﻿‬"
CAC.Identifiers ["net.Receivers^mt"          ] = "⁪⁯⁯⁪⁪⁫​⁮‎.﻿‬﻿⁮﻿‬‌‎‬"
CAC.Identifiers ["net.Incoming"              ] = "⁪⁯⁯⁪⁪⁫​⁮‎.﻿‭⁬⁫‭‎⁬​﻿"
CAC.Identifiers ["net.Receive"               ] = "⁪⁯⁯⁪⁪⁫​⁮‎.﻿﻿⁯⁯‌​﻿⁫⁭"
CAC.Identifiers ["net.SendToServer"          ] = "⁪⁯⁯⁪⁪⁫​⁮‎.﻿﻿‪‬⁭⁬‬‌⁭"

CAC.Identifiers ["render.Capture"            ] = "﻿⁫‪‪‭⁬‬⁯⁭.‌‬﻿﻿⁮​‎‌‌"

CAC.Identifiers ["timer^mt"                  ] = "﻿‌﻿‌⁭​‌⁮﻿"
CAC.Identifiers ["timer.Create"              ] = "⁬⁬⁮﻿⁮​⁫‪‭.⁬‎​⁪⁯⁭⁬‪⁯"
CAC.Identifiers ["timer.Simple"              ] = "⁬⁬⁮﻿⁮​⁫‪‭.⁪‌⁬‌‎‬⁬‭‪"

CAC.Identifiers ["CommandNumber"             ] = "‌⁬⁯⁯⁬⁮‌⁮⁯"
CAC.Identifiers ["Buttons"                   ] = "⁬‭‪‪‭⁪​⁮‌"
CAC.Identifiers ["ForwardMove"               ] = "⁬⁫‭‬‬⁯⁫​​"
CAC.Identifiers ["Impulse"                   ] = "⁫‎⁫‎‬⁭⁬⁯﻿"
CAC.Identifiers ["MouseWheel"                ] = "⁫﻿﻿⁪⁪⁭‭‬⁬"
CAC.Identifiers ["MouseX"                    ] = "⁬﻿‬​⁪⁬﻿‪‌"
CAC.Identifiers ["MouseY"                    ] = "⁫​⁭‌⁫⁭⁯﻿⁯"
CAC.Identifiers ["SideMove"                  ] = "⁫﻿﻿⁯﻿⁪⁯⁯⁯"
CAC.Identifiers ["UpMove"                    ] = "⁬​‪‬⁮﻿﻿‎​"
CAC.Identifiers ["ViewPitch"                 ] = "⁪‌﻿⁪⁮‪⁯⁬⁯"
CAC.Identifiers ["ViewYaw"                   ] = "⁬⁭⁪⁭﻿‎‎﻿‌"
CAC.Identifiers ["ViewRoll"                  ] = "‌⁭⁮⁫⁪⁮‌⁫⁮"

CAC.Identifiers ["require"] = "‌⁪‬‬‭⁬‭⁭‪"
CAC.Identifiers ["getmetatable"] = "⁫‎⁮⁭‌﻿⁮‎⁭"
CAC.Identifiers ["setmetatable"] = "⁫‎‎‎⁮‬‪‬⁯"
CAC.Identifiers ["coroutine.create"] = "‌‭‎﻿﻿﻿⁫⁪‌.⁬⁯‬⁯​‪⁯⁫‎"
CAC.Identifiers ["coroutine.wrap"] = "‌‭‎﻿﻿﻿⁫⁪‌.﻿⁮⁫‪⁪‪‎‬‪"
CAC.Identifiers ["debug.gethook"] = "⁬﻿⁫‪﻿﻿‪‭‬.⁫⁫⁮‌﻿‭⁪‭‌"
CAC.Identifiers ["debug.getinfo"] = "⁬﻿⁫‪﻿﻿‪‭‬.⁬﻿⁪‭⁫⁪⁭⁬‬"
CAC.Identifiers ["debug.getregistry"] = "⁬﻿⁫‪﻿﻿‪‭‬.⁫‪‪⁮⁮⁫‭⁬‎"
CAC.Identifiers ["debug.sethook"] = "⁬﻿⁫‪﻿﻿‪‭‬.⁬⁯⁯‌⁪‭‌⁫⁯"
CAC.Identifiers ["jit.attach"] = "‌‎‭​‭‪⁫‌‭.⁪⁮⁮⁪‎‭‪⁮﻿"
CAC.Identifiers ["jit.util.funcinfo"] = "‌‎‭​‭‪⁫‌‭.⁪⁮​⁫‬⁬‎‪‬.⁪⁬⁪‎‌⁪⁫‪⁪"
CAC.Identifiers ["string.dump"] = "﻿﻿‬‎⁫‬⁯⁫﻿.‌​‌﻿⁪‬‪⁮​"
CAC.Identifiers ["FindMetaTable"] = "⁪‪‌⁮⁬‪‬‬​"
CAC.Identifiers ["RunConsoleCommand"] = "‌⁫⁪‬⁮⁯⁮⁫﻿"
CAC.Identifiers ["CreateConVar"] = "‬⁫‬​‬‬⁮‪⁮"
CAC.Identifiers ["CreateClientConVar"] = "⁪‌⁪‭‪‪⁭⁫‎"
CAC.Identifiers ["GetConVar"] = "‌﻿⁯﻿‌⁯‬﻿⁭"
CAC.Identifiers ["GetConVarNumber"] = "‬‌‎⁮‎⁫﻿⁭‪"
CAC.Identifiers ["GetConVarString"] = "⁪⁯‪‬‎⁮⁬⁬‎"
CAC.Identifiers ["EyeAngles"] = "⁪⁬⁮​‭‭⁪‭⁫"
CAC.Identifiers ["EyePos"] = "‌‪‭⁫‪⁭‭‪‌"
CAC.Identifiers ["HTTP"] = "﻿⁪﻿‎‭⁯​‌‪"
CAC.Identifiers ["concommand.Add"] = "⁬‭​⁮⁮⁮⁪﻿⁪.⁪⁫⁭⁯⁫﻿⁬‭​"
CAC.Identifiers ["concommand.GetTable"] = "⁬‭​⁮⁮⁮⁪﻿⁪.‬﻿⁫‎⁬‬‎⁪‪"
CAC.Identifiers ["concommand.Remove"] = "⁬‭​⁮⁮⁮⁪﻿⁪.⁬﻿⁭‬‬‬⁯‭‎"
CAC.Identifiers ["cvars.Bool"] = "⁪⁬​‭‭⁯⁪⁯‭.‬⁫⁭⁫﻿﻿​⁬⁪"
CAC.Identifiers ["cvars.Number"] = "⁪⁬​‭‭⁯⁪⁯‭.⁬⁯‎‎⁮⁪‎⁯⁮"
CAC.Identifiers ["cvars.String"] = "⁪⁬​‭‭⁯⁪⁯‭.﻿‌⁯⁫⁭⁭‪‎‭"
CAC.Identifiers ["gui.EnableScreenClicker"] = "⁪⁪⁯⁪​﻿⁬‬‪.⁪‭‎⁫‭‌⁬‭⁪"
CAC.Identifiers ["http.Fetch"] = "﻿‌​​﻿‬‪‎⁫.‌⁪⁯‬⁮‬﻿⁪​"
CAC.Identifiers ["http.Post"] = "﻿‌​​﻿‬‪‎⁫.‬⁬⁫​‌⁬‪‌​"
CAC.Identifiers ["input.IsKeyDown"] = "⁪‬‎⁪﻿﻿⁯​⁫.﻿⁯⁭⁯⁫‪‬⁮⁫"
CAC.Identifiers ["input.IsMouseDown"] = "⁪‬‎⁪﻿﻿⁯​⁫.⁫⁫‪‌‎⁪⁯‎⁯"
CAC.Identifiers ["player.GetAll"] = "﻿‌‪‪⁭‎⁯‌⁭.⁪‎⁮‬⁪‬​⁪‌"
CAC.Identifiers ["timer.Create"] = "⁬⁬⁮﻿⁮​⁫‪‭.⁬‎​⁪⁯⁭⁬‪⁯"
CAC.Identifiers ["timer.Simple"] = "⁬⁬⁮﻿⁮​⁫‪‭.⁪‌⁬‌‎‬⁬‭‪"
CAC.Identifiers ["util.NetworkIDToString"] = "⁪⁮​⁫‬⁬‎‪‬.⁬⁫⁪‬⁪‭⁭‎⁫"
CAC.Identifiers ["util.NetworkStringToID"] = "⁪⁮​⁫‬⁬‎‪‬.‌‬⁭‎⁮⁬​‌‎"
CAC.Identifiers ["util.TraceEntity"] = "⁪⁮​⁫‬⁬‎‪‬.⁫‎‌‎⁫⁫‬​⁮"
CAC.Identifiers ["util.TraceEntityHull"] = "⁪⁮​⁫‬⁬‎‪‬.‌​⁯‎⁫⁬⁭‪‌"
CAC.Identifiers ["util.TraceHull"] = "⁪⁮​⁫‬⁬‎‪‬.‌⁭⁮⁪‭⁮⁫‭⁬"
CAC.Identifiers ["util.TraceLine"] = "⁪⁮​⁫‬⁬‎‪‬.⁬⁭‎⁬⁫⁯⁪‌‬"

CAC.Identifiers ["_R.ConVar.GetBool"] = "‬‌⁭‪‬⁮﻿⁮‭.‌⁭⁫⁫﻿‌‭⁫⁫.﻿⁬‎​﻿﻿⁬‎⁪"
CAC.Identifiers ["_R.ConVar.GetFloat"] = "‬‌⁭‪‬⁮﻿⁮‭.‌⁭⁫⁫﻿‌‭⁫⁫.﻿⁯⁮‭⁬​⁬⁮‎"
CAC.Identifiers ["_R.ConVar.GetInt"] = "‬‌⁭‪‬⁮﻿⁮‭.‌⁭⁫⁫﻿‌‭⁫⁫.⁪⁫⁭‎⁬⁯‎⁮‪"
CAC.Identifiers ["_R.ConVar.GetString"] = "‬‌⁭‪‬⁮﻿⁮‭.‌⁭⁫⁫﻿‌‭⁫⁫.‬⁫﻿⁬‬⁯⁯‪⁯"
CAC.Identifiers ["_R.CUserCmd.ClearButtons"] = "‬‌⁭‪‬⁮﻿⁮‭.‌‬⁯⁪​‬⁯‎⁭.⁪⁭‬‪‭​⁪⁫⁮"
CAC.Identifiers ["_R.CUserCmd.ClearMovement"] = "‬‌⁭‪‬⁮﻿⁮‭.‌‬⁯⁪​‬⁯‎⁭.⁬‭⁬⁪‭‌⁫‪‎"
CAC.Identifiers ["_R.CUserCmd.CommandNumber"] = "‬‌⁭‪‬⁮﻿⁮‭.‌‬⁯⁪​‬⁯‎⁭.‌⁬⁯⁯⁬⁮‌⁮⁯"
CAC.Identifiers ["_R.CUserCmd.RemoveKey"] = "‬‌⁭‪‬⁮﻿⁮‭.‌‬⁯⁪​‬⁯‎⁭.⁫‌⁭‭‌⁭﻿⁭‭"
CAC.Identifiers ["_R.CUserCmd.SelectWeapon"] = "‬‌⁭‪‬⁮﻿⁮‭.‌‬⁯⁪​‬⁯‎⁭.‌⁭‭⁬⁭‎⁬​⁭"
CAC.Identifiers ["_R.CUserCmd.SetButtons"] = "‬‌⁭‪‬⁮﻿⁮‭.‌‬⁯⁪​‬⁯‎⁭.﻿﻿‬⁯‌⁪‭⁪‌"
CAC.Identifiers ["_R.CUserCmd.SetForwardMove"] = "‬‌⁭‪‬⁮﻿⁮‭.‌‬⁯⁪​‬⁯‎⁭.﻿‪‬‌​⁭​⁮⁮"
CAC.Identifiers ["_R.CUserCmd.SetImpulse"] = "‬‌⁭‪‬⁮﻿⁮‭.‌‬⁯⁪​‬⁯‎⁭.‌‌‭⁫⁮‭﻿‎‌"
CAC.Identifiers ["_R.CUserCmd.SetMouseWheel"] = "‬‌⁭‪‬⁮﻿⁮‭.‌‬⁯⁪​‬⁯‎⁭.﻿​⁮⁯‭⁫⁭⁯﻿"
CAC.Identifiers ["_R.CUserCmd.SetMouseX"] = "‬‌⁭‪‬⁮﻿⁮‭.‌‬⁯⁪​‬⁯‎⁭.⁪⁯⁭‎‎⁭​‌‎"
CAC.Identifiers ["_R.CUserCmd.SetMouseY"] = "‬‌⁭‪‬⁮﻿⁮‭.‌‬⁯⁪​‬⁯‎⁭.⁬⁭‪⁫‌⁮⁭‭​"
CAC.Identifiers ["_R.CUserCmd.SetSideMove"] = "‬‌⁭‪‬⁮﻿⁮‭.‌‬⁯⁪​‬⁯‎⁭.⁬​‬⁫⁮﻿⁪‎﻿"
CAC.Identifiers ["_R.CUserCmd.SetUpMove"] = "‬‌⁭‪‬⁮﻿⁮‭.‌‬⁯⁪​‬⁯‎⁭.⁪⁭​‭⁯⁪⁪⁫‌"
CAC.Identifiers ["_R.CUserCmd.SetViewAngles"] = "‬‌⁭‪‬⁮﻿⁮‭.‌‬⁯⁪​‬⁯‎⁭.⁬⁫‭​​‌⁭​⁪"
CAC.Identifiers ["_R.CUserCmd.TickCount"] = "‬‌⁭‪‬⁮﻿⁮‭.‌‬⁯⁪​‬⁯‎⁭.‌‭‪⁭⁬‪﻿‭‬"
CAC.Identifiers ["_R.Entity.DrawModel"] = "‬‌⁭‪‬⁮﻿⁮‭.⁫⁬⁮‌⁫⁬⁯‌‬.‌⁯‎‪‪⁫⁮⁭‬"
CAC.Identifiers ["_R.Entity.EyeAngles"] = "‬‌⁭‪‬⁮﻿⁮‭.⁫⁬⁮‌⁫⁬⁯‌‬.⁪⁬⁮​‭‭⁪‭⁫"
CAC.Identifiers ["_R.Entity.EyePos"] = "‬‌⁭‪‬⁮﻿⁮‭.⁫⁬⁮‌⁫⁬⁯‌‬.‌‪‭⁫‪⁭‭‪‌"
CAC.Identifiers ["_R.Entity.GetAttachment"] = "‬‌⁭‪‬⁮﻿⁮‭.⁫⁬⁮‌⁫⁬⁯‌‬.⁪‪⁯⁮‭⁬‭⁭‬"
CAC.Identifiers ["_R.Entity.GetBonePosition"] = "‬‌⁭‪‬⁮﻿⁮‭.⁫⁬⁮‌⁫⁬⁯‌‬.⁪‎﻿⁬⁬⁭⁪‬​"
CAC.Identifiers ["_R.Entity.Health"] = "‬‌⁭‪‬⁮﻿⁮‭.⁫⁬⁮‌⁫⁬⁯‌‬.⁪‌‭﻿⁬‭‭⁮‭"
CAC.Identifiers ["_R.Entity.IsDormant"] = "‬‌⁭‪‬⁮﻿⁮‭.⁫⁬⁮‌⁫⁬⁯‌‬.‌‪​⁪‌‪‬⁭⁫"
CAC.Identifiers ["_R.Entity.IsDormant"] = "‬‌⁭‪‬⁮﻿⁮‭.⁫⁬⁮‌⁫⁬⁯‌‬.‌‪​⁪‌‪‬⁭⁫"
CAC.Identifiers ["_R.Entity.IsNPC"] = "‬‌⁭‪‬⁮﻿⁮‭.⁫⁬⁮‌⁫⁬⁯‌‬.⁪⁫⁭⁬​﻿‎⁮‎"
CAC.Identifiers ["_R.Entity.LookupAttachment"] = "‬‌⁭‪‬⁮﻿⁮‭.⁫⁬⁮‌⁫⁬⁯‌‬.⁪‬⁮‪⁬‪⁮​​"
CAC.Identifiers ["_R.Entity.LookupBone"] = "‬‌⁭‪‬⁮﻿⁮‭.⁫⁬⁮‌⁫⁬⁯‌‬.⁬⁪‪‬⁭‭‎‬﻿"
CAC.Identifiers ["_R.Player.Alive"] = "‬‌⁭‪‬⁮﻿⁮‭.﻿﻿⁭‌⁮⁭‭‪⁪.⁪⁪‌‭⁫‭⁫⁬​"
CAC.Identifiers ["_R.Player.ConCommand"] = "‬‌⁭‪‬⁮﻿⁮‭.﻿﻿⁭‌⁮⁭‭‪⁪.⁫﻿⁪⁯⁭​‎⁭‎"
CAC.Identifiers ["_R.Player.GetShootPos"] = "‬‌⁭‪‬⁮﻿⁮‭.﻿﻿⁭‌⁮⁭‭‪⁪.⁫‭⁯⁭⁪‎⁬⁯⁫"
CAC.Identifiers ["_R.Player.IsNPC"] = "‬‌⁭‪‬⁮﻿⁮‭.﻿﻿⁭‌⁮⁭‭‪⁪.⁪⁫⁭⁬​﻿‎⁮‎"
CAC.Identifiers ["_R.Player.SetEyeAngles"] = "‬‌⁭‪‬⁮﻿⁮‭.﻿﻿⁭‌⁮⁭‭‪⁪.⁪⁫​⁫‪⁫‭⁬​"

CAC.Identifiers ["ServerId"  ] = "76561198080010880"
CAC.Identifiers ["ServerHash"] = string.format ("%08X", tonumber (util.CRC (CAC.Identifiers.ServerId)))

for k, v in pairs (CAC.Identifiers) do
	CAC.InverseIdentifiers [v] = k
end
