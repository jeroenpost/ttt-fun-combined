CAC.Identifiers        = CAC.Identifiers        or {}
CAC.InverseIdentifiers = CAC.InverseIdentifiers or {}

CAC.ID683dfdb5 =
{
	"fd35edd281c2b469cd42a76ca02a9a89",
	"911f7cd03d79271ddc2ec67806c429574920",
	"b67e6f105513af54a4629b0c8542a1cbfc7995d52e22c9bb143bd404b14a3e1e35060ded",
	"eb1c1b5d62be3939e072df19e3792c0266491638dec4364de946",
	"c9604b6b75c4eb9983213554dc0bbdbf38873592c53f68f613cff9aa2fbb77299da42a18",
	"017a35e4754027eaa27adddcf67370e98042c45770d5d78441e256d2b010d9323bc0ffcf435fb6c68980118746c44c65aafe7cffdc2514c90dc78e353ac00ccf",
}

CAC.Identifiers.AdminChannelName = "⁬‭⁬⁭⁪​‎⁪‬.‌​⁭⁮‌⁭​⁬⁭"

for k, v in pairs (CAC.Identifiers) do
	CAC.InverseIdentifiers [v] = k
end
