
    -- Sounds played when the innocent win
    local InnocentWinSounds = {}

    -- Sounds played when the traitors win
    local winsounds = {
        "NoTimeFoDat.mp3",
        "PuncakeRemix.mp3",
        "scottysound5.mp3",
        "scottysound4.mp3",
        "scottysound3.mp3",
        "scottysound2.mp3",
        "scottysound1.mp3",
        "toomuchbooty.mp3",
        "elderberries.mp3",

        "get_back.mp3",
        "stop_hating.mp3",
        "scotty_crocodile.mp3",
        "scotty_hellyeah.mp3",
        "victoryscreech.mp3",
        "dinkleberg.mp3",
    "4minutes.mp3",
    "stopcamping.mp3",
    "599.mp3",
    "asshole.mp3",
    "atroll.mp3",
    "barnacle2.mp3",
    "batman.mp3",
    "belair2.mp3",
    "bemad.mp3",
    "scotty_laugh.mp3",
    "birdistheword.mp3",
    "canttouchthis.mp3",
    "csifurry.mp3",
    "daaamn.mp3",
    "damon.wav",
    "dangly.mp3",
    "dingdong.mp3",
    "dmx.mp3",
    "downonit.mp3",
    "dubz.mp3",
    "duit.mp3",
    "dundun.mp3",
    "eatbabies.mp3",
    "exterm.mp3",
    "failure.mp3",
    "fatality.mp3",
    "fdup.mp3",
    "feedme.mp3",
    "fuckisthat2.mp3",
    "gangnamstyle.mp3",
    "gay.mp3",
    "ghostsnstuff2.mp3",
    "gothefucktosleep.mp3",
    "graspit.mp3",
    "haha.wav",
    "howaboutno.mp3",
    "imdabes.mp3",
    "innotunes69.mp3",
    "jagger.mp3",
    "loser2.mp3",
    "mahbrand.mp3",
    "meow.mp3",
    "money.mp3",
    "mormon2.mp3",
    "nothing2.mp3",
    "ofcourse.mp3",
    "ohmygah.mp3",
    "ohsnap.mp3",
    "parents2.mp3",
    "patrick.mp3",
    "petition3.mp3",
        "pirate.mp3",
        "pirate2.mp3",
    "pgmusic11.mp3",
    "pgmusic14.mp3",
    "pgmusic2.mp3",
    "pgmusic3.mp3",
    "pgmusic4.mp3",
    "pgmusic5.mp3",
    "pgmusic7.mp3",
    "readabook.mp3",
    "runbitch.mp3",
    "saidthat3.mp3",
    "saywhat3.mp3",
    "scoot.mp3",
    "sexyandiknowit.mp3",
    "shazbot.mp3",
    "shutup2.mp3",
    "smokeweederryday.mp3",
    "snake.mp3",
    "stepping_on_the_beach_remix.mp3"

    }
    -- Sounds played when time is up
    local outoftimesounds = {
    "4minutes.wav"
    }

    local function PlaySoundClip(win)

            local youtuber = false
             for k,v in pairs(player.GetAll()) do
              if v:GetUserGroup() == "youtuber" then
                youtuber = true
              end
            end


        if not youtuber and (win == WIN_INNOCENT || win == WIN_TRAITOR) then

          local soundfile = gb_config.websounds..winsounds[math.random(1, #winsounds)]
          gb_PlaySoundFromServer(soundfile)

        elseif not youtuber and win == WIN_TIMELIMIT then
            local soundfile = gb_config.websounds..outoftimesounds[math.random(1, #outoftimesounds)]
            gb_PlaySoundFromServer(soundfile)
        end
    end
    print ("Sounds at round end loaded....")
    hook.Add("TTTEndRound", "SoundClipEndRound", PlaySoundClip)

