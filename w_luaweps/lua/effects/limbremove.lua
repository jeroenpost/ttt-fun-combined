

/*-----------------------------------------------------------------
	--Initialize Server Files
------------------------------------------------------------------*/

local function OnParticleCollision( particle, Pos, Norm )
	if math.random() < 0.75 then return end
	util.Decal('Blood', Pos + Norm*10, Pos - Norm*10)
end

/*-----------------------------------------------------------------
	Remove Head After...
------------------------------------------------------------------*/
local function RemoveHead( Ent )
	if !IsValid( Ent ) then return end
	
	if Ent:GetClass() == 'prop_ragdoll' && IsValid(Ent.Head) then
		Ent.Head:Remove()
	end
end
hook.Add('EntityRemoved', 'HeadshotDecap.EntityRemoved', RemoveHead)

function EFFECT:Init( data )
	local Ply = data:GetEntity()
	local Pos = data:GetOrigin()
	local Rag = Ply
    local Head = data:GetScale()
	self.Dir = data:GetNormal()

	if !IsValid( Rag ) then return end
	if !IsValid( Ply ) then return end

	local emitter = ParticleEmitter( Pos )
	
		for i=0, 25 do
		
			local vPos = Pos + VectorRand()*5
			local particle = emitter:Add( 'trails/love', vPos )
			
			if (particle) then

				if math.random() > 0.80 then
					particle:SetVelocity( VectorRand()*45 )
				else
					particle:SetVelocity( self.Dir*math.Rand(100,200) + VectorRand()*math.Rand(50,80) )
				end
				
				particle:SetColor( 175, 0, 0 )
				particle:SetLifeTime( 0 )
				particle:SetDieTime( math.Rand( 0.75, 1.5 ) )
				particle:SetStartAlpha( math.Rand( 200, 255 ) )
				particle:SetEndAlpha( 0 )
				particle:SetStartSize( 7.5 )
				particle:SetEndSize( 0.5 )
				particle:SetRoll( math.Rand(0, 360) )
				particle:SetRollDelta( math.Rand(-1,1) )
				
				local StartLength = math.Rand(5, 10)
				particle:SetStartLength( StartLength )
				particle:SetEndLength( StartLength+math.Rand(25,50) )
				
				particle:SetAirResistance( 0 )
				particle:SetGravity( Vector( 0, 0, -300 ) )
				particle:SetCollide( true )
				particle:SetBounce( 0 )
				particle:SetLighting( true )
				particle:SetCollideCallback( OnParticleCollision )
				
			end
			
		end
		
	emitter:Finish()
	
	Rag.Head = self:CreateHead( Ply, Pos, Model( Rag:GetModel() ), Head )
	Rag:CallOnRemove( RemoveHead ) --Remove Head
	
end

/*-----------------------------------------------------------------
	Our Head Material!
------------------------------------------------------------------*/
--local FleshyMat = Material( 'models/decapitation_flesh/decapitation' ) -- I DONT LIKE HEAD GIB

/*-----------------------------------------------------------------
	Render The Decapitation!
------------------------------------------------------------------*/
local function RenderHead( self )

    local normal = self:GetAngles():Up()
	local correctpos = self:LocalToWorld( -self.headposition )
    local distance = normal:Dot( self:LocalToWorld(vector_up*-4) )
	
    local bEnabled = render.EnableClipping( true )
    render.PushCustomClipPlane( normal, distance )
	
	self:SetRenderOrigin( correctpos )
	self:DrawModel()
	
	render.SetBlend( 0.9 )
	render.MaterialOverride( FleshyMat )
		self:SetRenderOrigin( correctpos )
		self:DrawModel()
	render.MaterialOverride(0)
	render.SetBlend( 1 )
	
	render.PopCustomClipPlane()
	render.EnableClipping( bEnabled )
	
	self:SetRenderOrigin()
	
end

/*-----------------------------------------------------------------
	Playermodels Head to be Selected Here.
------------------------------------------------------------------*/
function EFFECT:CreateHead( Ply, Pos, Mdl, Head )

	--Create the entire body model
	local Body = false

    if ( CLIENT ) then
        Body = ents.CreateClientProp( Mdl )
    else
        Body = ents.Create( "prop_ragdoll" )
    end
    Body:SetModel( Mdl )
	Body:SetAngles( Ply:GetAngles() )
	Body:SetPos( Pos )
	Body:Spawn()
    Body:SetSolid( SOLID_VPHYSICS );
	--Initialize its physics
	Body:PhysicsInitBox( Vector( -4, -4, -4 ), Vector( 4, 4, 4 ) )

    timer.Simple(10,function()
        if IsValid(Body) then Body:Remove() end
    end)
	
	local Phys = Body:GetPhysicsObject()
	Phys:Wake()
	Phys:SetVelocity( self.Dir * 375 )
	Phys:AddAngleVelocity( VectorRand()*100 )
	Phys:SetMaterial('flesh')

	--Make it only a head
	local Head = Head
	local Count = Body:GetBoneCount() or -1
	
	if Count < 1 then return end
	for i = 0, Count-1 do
		if i != Head then
			Body:ManipulateBoneScale(i, Vector(0,0,0))
		end
	end
	    if Body:GetBonePosition( Head ) then
        Body.headposition = Body:WorldToLocal( Body:GetBonePosition( Head ) )
	Body.RenderOverride = RenderHead
        end
	
	return Body
end

/*-----------------------------------------------------------------
	Disable Think
------------------------------------------------------------------*/
function EFFECT:Think( )
	return false
end

/*-----------------------------------------------------------------
	END
------------------------------------------------------------------*/
function EFFECT:Render()
end