function EFFECT:Init(data)
	self.EndPos = data:GetOrigin()
	local wep = data:GetEntity()
    local MySelf = LocalPlayer()

	if wep:IsValid() then
		if wep == MySelf:GetActiveWeapon() then
			local attach
			if MySelf:ShouldDrawLocalPlayer() then
				attach = wep:GetAttachment(data:GetAttachment())
			else
				attach = MySelf:GetViewModel():GetAttachment(data:GetAttachment())
			end
			if attach then
				self.StartPos = attach.Pos
			else
				self.StartPos = data:GetStart()
			end
		elseif wep:IsWeapon() then
			local attach = wep:GetAttachment(data:GetAttachment())
			if attach then
				self.StartPos = attach.Pos
			else
				self.StartPos = data:GetStart()
			end
		else
			self.StartPos = data:GetStart()
		end

		local owner = wep:GetOwner()

			self.Col = wep.tracerColor

	else
		self.StartPos = data:GetStart()
    end

    if not self.EndPos then return end

	self.Dir = self.EndPos - self.StartPos

	self.TracerTime = math.max(0.05, math.min(0.35, self.EndPos:Distance(self.StartPos) * 0.0001))

	self.DieTime = CurTime() + self.TracerTime
	self.DieTimeTwo = self.DieTime + math.Rand(0.7, 1)
	self.SpriteSize = math.Rand(14, 20)

	self.Col = self.Col or color_white
	local c = self.Col

	self.Entity:SetRenderBoundsWS(self.StartPos, self.EndPos, Vector(24, 24, 24))
	self.Entity:SetModel("models/Weapons/w_bullet.mdl")
	self.Entity:SetMaterial("models/shiny")
	self.Entity:SetRenderMode(RENDERMODE_TRANSALPHA)
	self.Entity:SetColor(Color(c.r, c.g, c.b, 255))
	self.Entity:SetModelScale(1.5, 0)
	self.Entity:SetAngles(self.Dir:Angle())
end

function EFFECT:Think()
	return CurTime() < self.DieTimeTwo
end

local matBeam = Material("Effects/laser1")
local matGlow = Material("sprites/glow04_noz")
function EFFECT:Render()
	local ct = CurTime()
    if not self.DieTime then self.DieTime = CurTime() end
	if ct < self.DieTime then
		local fDelta = (self.DieTime - ct) / self.TracerTime
		fDelta = math.Clamp(fDelta, 0, 1)

		render.SetMaterial(matBeam)
		local sinWave = math.sin(fDelta * math.pi)
		local endpos = self.EndPos - self.Dir * (fDelta - sinWave * 0.3)
		render.DrawBeam(endpos, self.EndPos - self.Dir * (fDelta + sinWave * 0.3), 2 + sinWave * 8, 1, 0, self.Col)

		render.SetMaterial(matGlow)
		local siz = math.Rand(16, 24)
		render.DrawSprite(endpos, siz, siz, self.Col)
		self.Entity:SetPos(endpos)
	else



	end

	self.Entity:DrawModel()
end
