
if SERVER then
    util.AddNetworkString("resetBonesClientAfterSwitch")
end

if CLIENT then
    function resetBonesClientAfterSwitch()
        if not IsValid( LocalPlayer() ) then return end
        local vm = LocalPlayer():GetViewModel()
       
        if IsValid(vm) then

            if (not vm:GetBoneCount()) then return end
            for i = 0, vm:GetBoneCount() do
                vm:ManipulateBoneScale(i, Vector(1, 1, 1))
                vm:ManipulateBoneAngles(i, Angle(0, 0, 0))
                vm:ManipulateBonePosition(i, Vector(0, 0, 0))
            end
        end
    end
    net.Receive("resetBonesClientAfterSwitch", resetBonesClientAfterSwitch)

end

    hook.Add("PlayerSwitchWeapon", "FixBoneMods", function(ply, oldWeapon, newWeapon)
       if not SERVER then return end
        net.Start("resetBonesClientAfterSwitch")
        net.Send( ply )
    end)

