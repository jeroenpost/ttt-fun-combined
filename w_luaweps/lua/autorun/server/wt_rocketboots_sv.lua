
do --player meta

	local PLY = FindMetaTable("Player")
	
	function PLY:GiveRocketBoots(intTime)
		intTime = intTime or 15
		
		self._wt_RocketBootsEquipped = true
		self._wt_RocketBootsEndTime = CurTime()+3333
        self.rocketFuel = intTime * 100
		
		if not IsValid(self._wt_RocketBootsEffect) then
			local e = ents.Create("wt_rocketboots_thinker")
			e:SetPos(self:GetPos()+Vector(0,0,5))
			e:SetPlayer(self)
			e:Spawn()
			e:SetParent( self )
			self:DeleteOnRemove( e )
			self._wt_RocketBootsEffect = e
		end
		
	end
	function PLY:RemoveRocketBoots()
		self._wt_RocketBootsEquipped = false
		self._wt_RocketBootsEndTime = CurTime()
		
		if IsValid(self._wt_RocketBootsEffect) then
			SafeRemoveEntity(self._wt_RocketBootsEffect)
			self._wt_RocketBootsEffect = nil
		end
	end
	
	function PLY:HasRocketBoots()
		return self._wt_RocketBootsEquipped
    end

    local neztThinkerr = 0
    hook.Add("Think", "RemoveRocketBoots", function()
        if neztThinkerr < CurTime() then
             neztThinkerr = CurTime() + 1
            for k,v in pairs(player.GetAll()) do
                if v:HasRocketBoots() then
                    if v.rocketFuel < 2 or not v:Alive() then
                        v:RemoveRocketBoots()
                    end
                end
            end
        end
    end)
	


end --player meta