if SERVER then
    AddCSLuaFile( 'effects/decapitation_head.lua' )
end

local function gibPlayerHead( Ply, Normal )

    local Head = Ply:LookupBone('valvebiped.bip01_head1')
    if not Head then return end
    local Pos = Ply:GetBonePosition( Head )

    local RagHead = Ply.server_ragdoll:LookupBone('valvebiped.bip01_head1') --HEAD SECTION
    if not RagHead then return end
    Ply.server_ragdoll:ManipulateBoneScale(RagHead, vector_origin)

    local ED = EffectData()
    ED:SetEntity( Ply ) --Player Entity
    ED:SetNormal( Normal )
    ED:SetScale( Ply.server_ragdoll:EntIndex() )
    ED:SetOrigin( Pos )
    util.Effect( 'decapitation_head', ED ) --Effect Gib

end

local function PlayerDeath( Ply, Inflictor, Attacker )
    if Ply.was_headshot then
        if not IsValid(Attacker) or not Attacker:IsPlayer() then return end --If player is attacker then,
        local wep = Attacker:GetActiveWeapon()
        if not IsValid(wep) then return end --get active decap weapon.

        local WeaponState = wep.CanDecapitate --Got weapon state
        if WeaponState then
            local Normal = Attacker:GetForward()
            gibPlayerHead(Ply, Normal)
        end
    end

end
hook.Add('PlayerDeath', 'HeadshotDecap.PlayerDeath', PlayerDeath) --On Player Death