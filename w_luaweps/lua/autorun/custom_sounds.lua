if SERVER then
	AddCSLuaFile()
end

sound.Add(
	{
		name = "WT_RocketBoots.Thrust",
		sound = "^thrusters/rocket04.wav",
		channel = CHAN_BODY,
		pitchstart = 100,
		pitchend = 100,
		level = 1,
		volume = 1,
	}
)


sound.Add(
	{
		name = "WT_RocketBoots.Launch",
		sound = "^thrusters/rocket04.wav",
		channel = CHAN_BODY,
		pitchstart = 100,
		pitchend = 100,
		level = 1,
		volume = 1,
	}
)

sound.Add({
    name = "PTRS-41.Deploy",
    channel = CHAN_ITEM,
    volume = 1.0,
    sound = "weapons/masada/masada_deploy.mp3"
})

sound.Add({
    name = "Weapov_SG550.Clipout",
    channel = CHAN_ITEM,
    volume = 1,
    sound = "weapons/masada/masada_clipout.mp3"
})

sound.Add({
    name = "Weapov_SG550.Clipin",
    channel = CHAN_ITEM,
    volume = 1,
    sound = "weapons/masada/masada_clipin.mp3"
})

sound.Add({
    
    name = "Weapov_SG550.Boltpull",
    channel = CHAN_ITEM,
    volume = 1,
    sound = "weapons/masada/masada_boltpull.mp3"
})


sound.Add({
    name = "watch.single",
    channel = CHAN_WEAPON,
    volume = 1,
    sound = "weapons/watch/watch-1.mp3"
})
sound.Add({
    name = "watch.open",
    channel = CHAN_WEAPON,
    volume = 1,
    sound = "weapons/watch/open.mp3"
})
sound.Add({
    name = "watch.close",
    channel = CHAN_WEAPON,
    volume = 1,
    sound = "weapons/watch/close.mp3"
})
sound.Add({
    name = "watch.fiddle1",
    channel = CHAN_WEAPON,
    volume = 1,
    sound = "weapons/watch/fiddle1.mp3"
})
sound.Add({
    name = "watch.fiddle2",
    channel = CHAN_WEAPON,
    volume = 1,
    sound = "weapons/watch/fiddle2.mp3"
})sound.Add({
    name = "watch.fiddle3",
    channel = CHAN_WEAPON,
    volume = 1,
    sound = "weapons/watch/fiddle3.mp3"
})
sound.Add({
    name = "watch.fiddle4",
    channel = CHAN_WEAPON,
    volume = 1,
    sound = "weapons/watch/fiddle4.mp3"
})
sound.Add({
    name = "watch.draw",
    channel = CHAN_WEAPON,
    volume = 1,
    sound = "weapons/watch/draw.mp3"
})