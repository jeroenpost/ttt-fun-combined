SWEP.PrintName = "Melon GTX2TTT"
SWEP.Slot = 8

if SERVER then // This is where the init.lua stuff goes.
 
	//This makes sure clients download the file
	AddCSLuaFile ("shared.lua")
	--resource.AddFile("materials/vgui/ttt/weapon_melon_gbttt.vtm")
	--resource.AddFile("materials/vgui/ttt/weapon_melon_gbttt.vtf")
    --resource.AddFile("materials/vgui/ttt/melongun.png")
	--resource.AddFile("sound/melon_gtx2ttt/fart.wav")
	//How heavy the SWep is
	SWEP.Weight = 5
 
	//Allow automatic switching to/from this weapon when weapons are picked up
	SWEP.AutoSwitchTo = false
	SWEP.AutoSwitchFrom = false
	

 
elseif CLIENT then // This is where the cl_init.lua stuff goes
 
	//The name of the SWep, as appears in the weapons tab in the spawn menu(Q Menu)
	
	SWEP.Icon = "vgui/ttt_fun_killicons/melongun.png"
	SWEP.EquipMenuData = {
      type = "Weapon",
      desc = "RPG that can shoot melons."
	  };
 
	//Sets the position of the weapon in the switching menu 
	//(appears when you use the scroll wheel or keys 1-6 by default)
	
	SWEP.SlotPos = 4
	
 
	//Sets drawing the ammuntion levels for this weapon
	SWEP.DrawAmmo = true
 
	//Sets the drawing of the crosshair when this weapon is deployed
 
	//Ensures a clean looking notification when a chair is undone. How it works:
	//When you create an undo, you specify the ID:
	//		undo.Create("Some_Identity")
	//By creating an associated language, we can make the undo notification look better:
	//		language.Add("Undone_Some_Identity", "Some message...")
 
	language.Add("Undone_Thrown_SWEP_Entity","Undone Thrown SWEP Entity")
end
 
SWEP.Author = "Thomson2412"
SWEP.Contact = ""
SWEP.Purpose = "Splashing melons"
SWEP.Instructions = "An RPG that shoots melons. Primary fire: One shot. Secondary fire: Fully automatic (Can be laggy)"
SWEP.Base				= "weapon_tttbase"
SWEP.HoldType			= "rpg"
SWEP.ViewModelFlip = false
SWEP.ViewModelFOV 	= 56
function SWEP:Initialize()
	self:SetHoldType( self.HoldType or "pistol" )
end

SWEP.Base = "weapon_tttbase"

 
//The category that you SWep will be shown in, in the Spawn (Q) Menu 
//(This can be anything, GMod will create the categories for you)
SWEP.Category = "Melons"
 
SWEP.Spawnable = true -- Whether regular players can see it
SWEP.AdminSpawnable = true -- Whether Admins/Super Admins can see it
 
SWEP.ViewModel = "models/weapons/v_RPG.mdl" -- This is the model used for clients to see in first person.
SWEP.WorldModel = "models/weapons/w_rocket_launcher.mdl" -- This is the model shown to all other clients and in third-person.
 
 
//This determins how big each clip/magazine for the gun is. You can 
//set it to -1 to disable the ammo system, meaning primary ammo will 
//not be displayed and will not be affected.
SWEP.Primary.ClipSize = 10
 
//This sets the number of rounds in the clip when you first get the gun. Again it can be set to -1.
SWEP.Primary.DefaultClip = 10
 
//Obvious. Determines whether the primary fire is automatic. This should be true/false
SWEP.Primary.Automatic = false
 
//Sets the ammunition type the gun uses, see below for a list of types.
SWEP.Primary.Delay = 3
SWEP.Primary.Ammo = "none"
SWEP.Primary.Damage = 45
SWEP.Primary.Recoil = 2 
SWEP.Primary.NumShots = 1
SWEP.Primary.Cone = 0.03


SWEP.Secondary.ClipSize = 3
SWEP.Secondary.DefaultClip = 3
SWEP.Secondary.Automatic = true
SWEP.Secondary.Ammo = "none"
 

--local ShootSound = Sound("melon_gtx2ttt/fart.wav")
 
function SWEP:Reload()
end
 
function SWEP:Think()
end
 
 
function SWEP:throw_attack (model_file)
	//Get an eye trace. This basically draws an invisible line from
	//the players eye. This SWep makes very little use of the trace, except to 
	//calculate the amount of force to apply to the object thrown.
	local tr = self.Owner:GetEyeTrace()
 
	//Play some noises/effects using the sound we precached earlier
	--self:EmitSound(ShootSound)
	self.BaseClass.ShootEffects(self)
 
	//We now exit if this function is not running serverside
	if (!SERVER) then return end
 
	//The next task is to create a physics prop based on the supplied model
	local ent = ents.Create("prop_physics")
	ent:SetModel(model_file)
 
	//Set the initial position and angles of the object. This might need some fine tuning;
	//but it seems to work for the models I have tried.
	ent:SetPos(self.Owner:EyePos() + (self.Owner:GetAimVector() * 16))
	ent:SetAngles(self.Owner:EyeAngles())
	ent:Spawn()
	ent:SetPhysicsAttacker(self.Owner)
	//ent:SetOwner(self.Owner)
 
	//Now we need to get the physics object for our entity so we can apply a force to it
	local phys = ent:GetPhysicsObject()
 
	//Check if the physics object is valid. If not, remove the entity and stop the function
	if !(phys && IsValid(phys)) then ent:Remove() return end
 
	//Time to apply the force. My method for doing this was almost entirely empirical 
	//and it seems to work fairly intuitively with chairs.
	phys:ApplyForceCenter(self.Owner:GetAimVector():GetNormalized() *  math.pow(tr.HitPos:Length(), 3))
 
	//Now for the important part of adding the spawned objects to the undo and cleanup lists.
	cleanup.Add(self.Owner, "props", ent)
 
	undo.Create ("Thrown_SWEP_Entity")
		undo.AddEntity (ent)
		undo.SetPlayer (self.Owner)
	undo.Finish()
end
 
 
//Throw an office chair on primary attack
function SWEP:PrimaryAttack()
if ( !self:CanPrimaryAttack() ) then return end
	//Call the throw attack function, with the office chair model
	self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
	self:TakePrimaryAmmo(1)
        self:ShootBullet( self.Primary.Damage, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )

	self:throw_attack("models/props_junk/watermelon01.mdl")
end
 
//Throw a wooden chair on secondary attack
function SWEP:SecondaryAttack()
if ( !self:CanSecondaryAttack() ) then return end
	//Call the throw attack function, this time with the wooden chair model
	self:TakeSecondaryAmmo(1)
	self:throw_attack("models/props_junk/watermelon01.mdl")
end


SWEP.Kind = WEAPON_EQUIP1
SWEP.AutoSpawnable = true
SWEP.CanBuy = { ROLE_TRAITOR, ROLE_DETECTIVE }
SWEP.InLoadoutFor = nil
SWEP.LimitedStock = false
SWEP.AllowDrop = true
SWEP.IsSilent = false
SWEP.NoSights = true
SWEP.IronSightsPos = Vector( 6.05, -5, 2.4 )
SWEP.IronSightsAng = Vector( 2.2, -0.1, 0 )
 