if SERVER then
    AddCSLuaFile( "shared.lua" )
    --resource.AddFile("materials/vgui/ttt_fun_killicons/plant-disguiser.png")
end

SWEP.Base				= "gb_base"
SWEP.Kind = WEAPON_EQUIP
SWEP.HoldType = "camera"
SWEP.ViewModelFlip = false
SWEP.Spawnable			= true
SWEP.AdminSpawnable		= true
SWEP.AutoSpawnable      = false 
SWEP.Icon = "vgui/ttt_fun_killicons/plant-disguiser.png"

SWEP.PrintName            = "Plant Disguiser"            
SWEP.Slot                = 6
SWEP.SlotPos            = 1
SWEP.DrawAmmo            = true
SWEP.DrawCrosshair        = true
SWEP.Weight                = 5 

SWEP.EquipMenuData = {
      type  = "item_weapon",
      name  = "Plant Disguiser",
      desc  = "Disguise yourself as a plant for 30 seconds"
      }

SWEP.ViewModel            = "models/props/cs_office/plant01.mdl"
SWEP.WorldModel			= "models/weapons/w_pistol.mdl"
SWEP.Primary.ClipSize        = 30
SWEP.Primary.DefaultClip    = 30
SWEP.Primary.Automatic        = false
SWEP.Primary.Ammo            = "XBowBolt"
SWEP.Primary.Delay            = 30
SWEP.CanBuy = {ROLE_TRAITOR}
SWEP.Used = false 
SWEP.LastOwner = nil
SWEP.Used2 = false 
 
SWEP.VElements = {
	["element_name"] = { type = "Model", model = "models/props/cs_office/plant01.mdl", bone = "static_prop", rel = "", pos = Vector(16.818, 0.455, -14.091), angle = Angle(-5.114, 0, 0), size = Vector(0.5, 0.5, 0.5), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
}
SWEP.WElements = {
	["element_name"] = { type = "Model", model = "models/props/cs_office/plant01.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(5, 5.908, -0.456), angle = Angle(180, -7.159, 0), size = Vector(0.5, 0.5, 0.5), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
}


function SWEP:Initialize() 
    if SERVER then 
        self:SetHoldType( self.HoldType or "pistol" )
    end 
    self:SetNWBool("Used", false) 
    self.BaseClass.Initialize( self )
end


function SWEP:PrimaryAttack()
    
    if self:GetNWBool("Used", false) then 
        self:DryFire(self.SetNextPrimaryFire)
    return false end

    self:SetNWBool("Used", true)
    self.LastOwner = self.Owner

    self.OldModel = self.Owner:GetModel()
    self.LastOwner:SetModel("models/props/cs_office/plant01.mdl")
    if SERVER then
    self.Owner:ChatPrint("You are disguised as a plant now for 30 seconds.")
    self.Owner:ChatPrint("Keep in mind that your weapon is visible!")

    DamageLog("Disguiser: " .. self.Owner:Nick() .. " [" .. self.Owner:GetRoleString() .. "] disguised a a plant")
    end
    timer.Create("takePlantAmmo"..self.LastOwner:SteamID(),1,30,function() if IsValid(self) then 
            self:TakePrimaryAmmo(1)
            if self:Clip1() < 5 && IsValid(self.LastOwner) && not self.Used2 then
                 if SERVER then
                    self.LastOwner:ChatPrint("Your disguise will be gone in "..self:Clip1().." seconds")
                end
            end
            if not self:CanPrimaryAttack() && IsValid(self.LastOwner) then
                self.LastOwner:SetModel( self.OldModel )
                if SERVER && not self.Used2 then
                self.Used2 = true
                self.LastOwner:ChatPrint("You are not disguised anymore")
                end
            end
            
    end end)
    

end

function SWEP:OnDrop()
    if IsValid(self) then
        self.Used2 = true
        self:SetNWBool("Used", false)
    end
end