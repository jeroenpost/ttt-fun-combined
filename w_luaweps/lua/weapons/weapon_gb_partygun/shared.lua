    if SERVER then
       AddCSLuaFile( "shared.lua" )
       --resource.AddFile("materials/vgui/ttt_fun_killicons/partygun.png");
       --resource.AddFile("sound/weapons/gb_weapons/partygun1.mp3");
    end

SWEP.PrintName                       = "Party Gun"
       SWEP.Slot                            = 6
     
    if CLIENT then
       
     
       SWEP.EquipMenuData = {
          type  = "item_weapon",
          name  = "Party Gun",
          desc  = "Makes the victim dance for 30 seconds, takes 30 damage in total"
       };
     
       SWEP.Icon = "vgui/ttt_fun_killicons/partygun.png"
    end

SWEP.VElements = {
	["rainbow2"] = { type = "Sprite", sprite = "trails/rainbow", bone = "Base", rel = "", pos = Vector(3.181, 5.908, 12.272), size = { x = 4.523, y = 4.523 }, color = Color(255, 255, 255, 255), nocull = true, additive = true, vertexalpha = true, vertexcolor = true, ignorez = false},
	["rainbow"] = { type = "Sprite", sprite = "trails/happy", bone = "Base", rel = "", pos = Vector(2.273, -2.274, 4.091), size = { x = 1.514, y = 1.514 }, color = Color(255, 255, 255, 255), nocull = true, additive = true, vertexalpha = true, vertexcolor = true, ignorez = false}
}

SWEP.HoldType = "pistol"
SWEP.ViewModelFOV = 65
SWEP.ViewModelFlip = false
SWEP.ViewModel = "models/weapons/v_irifle.mdl"
SWEP.WorldModel = "models/weapons/w_irifle.mdl"
SWEP.ShowViewModel = true
SWEP.ShowWorldModel = true
SWEP.ViewModelBoneMods = {}
     
    SWEP.Base = "gb_base"
    SWEP.Kind = WEAPON_EQUIP
    SWEP.CanBuy = {ROLE_DETECTIVE, ROLE_TRAITOR}
     
	
    SWEP.Primary.ClipSize = 2
    SWEP.Primary.DefaultClip = 2
    SWEP.Primary.Ammo = "Battery"
    SWEP.Primary.Automatic = false
     
    SWEP.Secondary.ClipSize = -1
    SWEP.Secondary.DefaultClip = -1
    SWEP.Secondary.Ammo = "none"
    SWEP.Secondary.Automatic = true
     
    SWEP.NoSights = true
    SWEP.AllowDrop = true
    SWEP.LimitedStock = true
    SWEP.AutoSpawnable = false
     
    SWEP.StunTime = 8 -- How long is the victim ragdolled?
     
    SWEP.SlowTime = 5 -- How long is the victim "exhausted"?
    SWEP.SlowPercent = 0.33 -- How much they are slowed?
     
    SWEP.Delay = 5
    SWEP.Range = 1024
     
    SWEP.Sounds = {
            "weapons/gb_weapons/partygun1.mp3"
    }
     
    function SWEP:Precache()
            for _, v in pairs (self.Sounds) do
                    util.PrecacheSound(v)
            end
    end
     
    function SWEP:Deploy()
            self:SendWeaponAnim(ACT_VM_DRAW)
            self:SetNextPrimaryFire(CurTime() + 1)
    end
     
    function SWEP:PrimaryAttack()
            if (self:Clip1() <= 0) then return end
           
            self:TakePrimaryAmmo(1)
     
            self:SendWeaponAnim(ACT_VM_PRIMARYATTACK)
            self.Owner:SetAnimation(PLAYER_ATTACK1)
           
            self:SetNextPrimaryFire(CurTime() + self.Delay)
     
            if (IsFirstTimePredicted()) then
                    local snd, i = table.Random(self.Sounds)
                    self:EmitSound("weapons/gb_weapons/partygun1.mp3");
            end
           
            danceMoves = {ACT_GMOD_TAUNT_ROBOT,ACT_GMOD_TAUNT_DANCE,ACT_GMOD_TAUNT_MUSCLE}
               
                    local trace = self.Owner:GetEyeTrace()
                    if (trace.HitNonWorld and trace.HitPos:Distance(trace.StartPos) < self.Range) then
                            local victim = trace.Entity
                            if (!victim:IsPlayer()) then return end
                           
                                danceMove = table.Random(danceMoves)
                                victim:AnimPerformGesture_gb(danceMove);
                                if (SERVER) then    

                                    local soundfile = gb_config.websounds.."birdistheword.mp3"
                                    gb_PlaySoundFromServer(soundfile,victim, false)
                                    
                                 timer.Simple(15,function()
                                    --Start the dance again
                                      if IsValid(victim) and IsValid(self) and IsValid(self.Owner) and danceMove  then        
                                        victim:AnimPerformGesture_gb(danceMove);
                                     end
                                 end)

                                  timer.Create("TakeHealthPartyGun"..victim:UniqueID(),0.5,60, function()
                                      if IsValid(victim) and IsValid(self) and IsValid(self.Owner) then                                         
                                          victim:TakeDamage( 1, self.Owner, self.Owner:GetActiveWeapon() )
                                          local shock1 = math.random(-4200, 4200)
                                          local shock2 = math.random(-4200, 4200)
                                          local shock3 = math.random(-4200, 4200)
                                          victim:GetPhysicsObject():ApplyForceCenter(Vector(shock1, shock2, shock3))
                                      end
                                  end)
                                end

                    end
            
    end

    SWEP.lastReload = 0
     
    function SWEP:SecondaryAttack()
            if (SERVER) and (self.lastReload < (CurTime() - 5)) then    
                   self.lastReload = CurTime()
                   self:EmitSound("weapons/gb_weapons/partygun1.mp3");
            end
    end
     
    hook.Add("CanPlayerSuicide", "CantSuicideIfRagdolled", function(ply)
            return ply.Ragdolled == nil
    end)
