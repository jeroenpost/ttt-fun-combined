if SERVER then
   AddCSLuaFile( "shared.lua" )
   --resource.AddFile("materials/vgui/ttt_fun_killicons/m3.png")
end

SWEP.HoldType = "pistol"
SWEP.PrintName = "Rocketjump"
SWEP.Slot = 7
if CLIENT then
   
   SWEP.Author = "GreenBlack"
   
   SWEP.Icon = "vgui/ttt_fun_killicons/rpg.png"
end

SWEP.Base = "aa_base"
SWEP.Spawnable = true
SWEP.AdminSpawnable = true
SWEP.Kind = WEAPON_EQUIP2

SWEP.Primary.Ammo = "Battery"
SWEP.Primary.Damage = 5
SWEP.Primary.Cone = 0
SWEP.Primary.Delay = 1
SWEP.Primary.ClipSize = 10
SWEP.Primary.ClipMax = 12
SWEP.Primary.DefaultClip = 10
SWEP.Primary.Automatic = false
SWEP.Primary.NumShots = 20
SWEP.HeadshotMultiplier = 1.2
SWEP.AutoSpawnable = false
SWEP.AmmoEnt = "item_box_buckshot_ttt"
SWEP.UseHands			= true
SWEP.ViewModelFlip		= false
SWEP.ViewModelFOV		= 54
SWEP.ViewModel  = "models/weapons/c_rpg.mdl"
SWEP.WorldModel = "models/weapons/w_rocket_launcher.mdl"

SWEP.Primary.Sound = "weapons/rpg/rocketfire1.wav"
SWEP.Primary.Recoil = 0.1
SWEP.reloadtimer = 0


SWEP.IronSightsPos = Vector(5.74, -2.19, 3.28)
SWEP.IronSightsAng = Vector(0.547, 0, 0)


function SWEP:PrimaryAttack(worldsnd)
    self.Weapon:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
    if self:Clip1() < 1 then
        self:EmitSound("weapons/slam/mine_mode.wav")
        return
    end
    self:rFly()
   self:TakePrimaryAmmo(1)
end



-- COLOR
function SWEP:Deploy()
    self:SetColorAndMaterial(Color(255,255,255,255),"models/XQM/LightLinesRed_tool");
    local ply = self.Owner
    if ply:GetNWInt("walkspeed") < 350 then
        ply:SetNWInt("walkspeed",350)
    end
    if ply:GetNWInt("runspeed") < 450 then
        ply:SetNWInt("runspeed", 450)
    end
    self.Owner:SetNWFloat("w_stamina", 1)
end


function SWEP:PreDrop()
    self:ColorReset()
end


function SWEP:Holster()
    self:ColorReset()
    return true
end

function SWEP:OnRemove()
    self:ColorReset()
end
function SWEP:OnDrop()
    self:ColorReset()

end

function SWEP:PreDrawViewModel()

    if SERVER or not IsValid(self.Owner) or not IsValid(self.Owner:GetViewModel()) then return end
    self.Owner:GetViewModel():SetColor(Color(255,255,255,255))
    self.Owner:GetViewModel():SetMaterial("models/XQM/LightLinesRed_tool")

end
-- END COLORD:


-- FLY
SWEP.rflyammo = 10
SWEP.rnextFly = 0
function SWEP:rFly()
    if self.rnextFly > CurTime() or CLIENT then return end
    self.rnextFly = CurTime() + 0.30
    if self.rflyammo < 1 then return end

    self.rflyammo = self.rflyammo - 1
    self.Owner:SetAnimation(PLAYER_ATTACK1)

    if SERVER then self.Owner:EmitSound(Sound(self.Primary.Sound )) end
    --if SERVER then self.Owner:EmitSound(Sound("player/suit_sprint.wav")) end

    local ent = ents.Create( "env_explosion" )
    if( IsValid( self.Owner ) ) then
        ent:SetPos( self.Owner:GetShootPos() )
        ent:SetOwner( self.Owner )
        ent:SetKeyValue( "iMagnitude", "0" )
        ent:Spawn()

        ent:Fire( "Explode", 0, 0 )
        ent:EmitSound( "weapons/big_explosion.mp3", 450, 100 )
    end


    if SERVER then self.Owner:SetVelocity(self.Owner:GetForward() * -900 ) end
    timer.Simple(20, function() if IsValid(self) then self.rflyammo = self.rflyammo + 1 end end)
    --timer.Simple(0.2,self.SecondaryAttackDelay,self)
end