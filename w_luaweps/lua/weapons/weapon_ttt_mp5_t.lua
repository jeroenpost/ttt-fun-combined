
SWEP.Base = "gb_camo_base"
SWEP.ViewModelFOV	= 54
SWEP.HoldType = "ar2"
SWEP.PrintName = "MP5 Former"
SWEP.ViewModel		= "models/weapons/cstrike/c_smg_mp5.mdl"
SWEP.WorldModel		= "models/weapons/w_smg_mp5.mdl"
SWEP.AutoSpawnable = false
SWEP.Camo = 44

SWEP.HoldType = "ar2"
SWEP.Slot = 7

if CLIENT then

    SWEP.Author = "GreenBlack"

    SWEP.Icon = "vgui/ttt_fun_killicons/mp5_t.png"
end

SWEP.EquipMenuData = {
    type = "Weapon",
    desc = "Super fast and powerful MP5"
};
SWEP.CanBuy = { ROLE_TRAITOR }


SWEP.Spawnable = true
SWEP.AdminSpawnable = true
SWEP.Kind = WEAPON_EQUIP2

SWEP.Primary.Delay = 0.09
SWEP.Primary.Recoil = 1.1
SWEP.Primary.Automatic = true
SWEP.Primary.Ammo = "pistol"
SWEP.Primary.Damage = 14
SWEP.Primary.Cone = 0.028
SWEP.Primary.ClipSize = 36
SWEP.Primary.ClipMax = 90
SWEP.Primary.DefaultClip = 36
SWEP.AutoSpawnable = true
SWEP.AmmoEnt = "item_ammo_pistol_ttt"
SWEP.WorldModel = "models/weapons/w_smg_mp5.mdl"
SWEP.Primary.Sound = Sound("Weapon_MP5Navy.Single")

SWEP.HeadshotMultiplier = 1.2

SWEP.IronSightsPos = Vector(-5.3, -3.5823, 2)
SWEP.IronSightsAng = Vector(0.9641, 0.0252, 0)