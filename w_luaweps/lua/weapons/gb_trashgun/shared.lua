
   SWEP.PrintName	= "TrashGun"
   SWEP.Author 		= "GreenBlack"   
   SWEP.Icon = "vgui/ttt_fun_killicons/golden_deagle"

SWEP.Base			= "aa_base"

SWEP.Kind 			= WEAPON_PISTOL
SWEP.Slot = 1
SWEP.SlotPos 	= 1
SWEP.Primary.Recoil	= 1
SWEP.Primary.Damage = 50
SWEP.Primary.Delay 	= 0.45
SWEP.Primary.Cone 	= 0.009
SWEP.Primary.ClipSize 		= 40
SWEP.Primary.ClipMax 		= 120
SWEP.Primary.DefaultClip 	= 40
SWEP.Primary.NumShots		= 1
SWEP.Primary.Automatic 	= true
SWEP.HeadshotMultiplier = 3
SWEP.AmmoEnt 		= "item_ammo_revolver_ttt"

SWEP.AutoSpawnable      = true
SWEP.HoldType = "pistol"
SWEP.ViewModelFOV = 87.322834645669
SWEP.ViewModelFlip = true
SWEP.ViewModel = "models/weapons/v_smg_tmp.mdl"
SWEP.WorldModel = "models/weapons/w_pist_gbagle.mdl"
SWEP.ShowViewModel = true
SWEP.ShowWorldModel = true
SWEP.ViewModelBoneMods = {}

SWEP.WElements = {
	["element_name++++"] = { type = "Model", model = "models/zombie/fast.mdl", bone = "ValveBiped.Bip01_R_Forearm", rel = "", pos = Vector(-18.636, -0.456, -21.365), angle = Angle(27.614, 56.25, 84.886), size = Vector(1,1,1), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["element_name+++"] = { type = "Model", model = "models/phxtended/bar2x45b.mdl", bone = "ValveBiped.Bip01_R_Forearm", rel = "", pos = Vector(6.817, 61.363, -3.182), angle = Angle(138.067, -9.205, 1.023), size = Vector(1,1,1), color = Color(255, 255, 255, 255), surpresslightning = false, material = "camos/camo42", skin = 0, bodygroup = {} },
	["element_name+++++"] = { type = "Model", model = "models/tools/camera/camera.mdl", bone = "ValveBiped.Bip01_R_Forearm", rel = "", pos = Vector(7.727, -7.728, 6.817), angle = Angle(1.023, 7.158, 1.023), size = Vector(1,1,1), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["element_name"] = { type = "Model", model = "models/thrusters/jetpack.mdl", bone = "ValveBiped.Bip01_R_Forearm", rel = "", pos = Vector(6.817, 3.181, -3.182), angle = Angle(0, -5.114, 1.023), size = Vector(1,1,1), color = Color(255, 255, 255, 255), surpresslightning = false, material = "camos/camo43", skin = 0, bodygroup = {} },
	["element_name+"] = { type = "Model", model = "models/characters/hostage_02.mdl", bone = "ValveBiped.Bip01_R_Forearm", rel = "", pos = Vector(-55.91, 4.091, 7.727), angle = Angle(-68.524, -5.114, 13.295), size = Vector(1,1,1), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["element_name++"] = { type = "Model", model = "models/combine_camera/combine_camera.mdl", bone = "ValveBiped.Bip01_R_Forearm", rel = "", pos = Vector(6.817, 3.181, -3.182), angle = Angle(0, -5.114, 1.023), size = Vector(1,1,1), color = Color(255, 255, 255, 255), surpresslightning = false, material = "camos/camo46", skin = 0, bodygroup = {} }
}

SWEP.VElements = {
	["crowsawdddadsddsd"] = { type = "Model", model = "models/dynamite/dynamite.mdl", bone = "v_weapon.TMP_Parent", rel = "", pos = Vector(11.364, -4.092, 0), angle = Angle(-13.296, -35.795, 0), size = Vector(1,1,1), color = Color(255, 255, 255, 255), surpresslightning = false, material = "camos/camo43", skin = 0, bodygroup = {} },
	["crowsawdddads"] = { type = "Model", model = "models/noesis/donut.mdl", bone = "v_weapon.TMP_Parent", rel = "", pos = Vector(16.818, -20.455, 80), angle = Angle(17.385, -172.842, 0), size = Vector(0.018, 0.018, 0.018), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["crowsawdddadsddsdd"] = { type = "Model", model = "models/dav0r/tnt/tnttimed.mdl", bone = "v_weapon.TMP_Parent", rel = "", pos = Vector(-11.365, 0.455, -11.365), angle = Angle(0, 0, 0), size = Vector(1,1,1), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	--["crowsawddd"] = { type = "Model", model = "models/zup/zpm/zpm.mdl", bone = "v_weapon.TMP_Parent", rel = "", pos = Vector(-0, 0,0 ), angle = Angle(0, 0, 0), size = Vector(1,1,1), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["crowsawd"] = { type = "Model", model = "models/zombie/fast.mdl", bone = "v_weapon.TMP_Parent", rel = "", pos = Vector(16.818, -30.455, 0.455), angle = Angle(150.341, -13.296, 95.113), size = Vector(1.343, 1.343, 1.343), color = Color(255, 255, 255, 183), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["crows"] = { type = "Model", model = "models/tools/camera/camera.mdl", bone = "v_weapon.TMP_Parent", rel = "", pos = Vector(-0.456, -1, 0), angle = Angle(0, 0, 0), size = Vector(1,1,1), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	--["crowsawdddadsdds"] = { type = "Model", model = "models/effects/vol_light128x256.mdl", bone = "v_weapon.TMP_Parent", rel = "", pos = Vector(0, 0, 0), angle = Angle(0, 0, 0), size = Vector(0.5, 0.5, 0.5), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["crowsawdddadsddsddd"] = { type = "Model", model = "models/characters/counterterrorist.mdl", bone = "v_weapon.TMP_Parent", rel = "", pos = Vector(-48.637, -7.728, -47.728), angle = Angle(-1.024, 0, 3.068), size = Vector(1.174, 1.174, 1.174), color = Color(255, 255, 255, 255), surpresslightning = false, material = "camos/camo42", skin = 0, bodygroup = {} },
	["crow"] = { type = "Model", model = "models/thrusters/jetpack.mdl", bone = "v_weapon.Right_Hand", rel = "", pos = Vector(-5, 3.5, 0.7), angle = Angle(180, 95.113, -57.841), size = Vector(0.8,0.8,0.8), color = Color(255, 255, 255, 255), surpresslightning = false, material = "camos/camo46", skin = 0, bodygroup = {} }
}


function SWEP:Equip()
    self.Owner.hasProtectionSuit = true
    --self.Owner:SetJumpPower(880)
    self.Owner.HasJumpBoots = true
end

function SWEP:PrimaryAttack(worldsnd) 

   if not self:CanPrimaryAttack() then return end
 self.Weapon:SetNextPrimaryFire(CurTime() + 1.5)
    if SERVER then
    local tr = self.Owner:GetEyeTrace()
local ent = ents.Create( "env_explosion" )
    
	ent:SetPos( tr.HitPos  )
	ent:SetOwner( self.Owner  )
	ent:SetPhysicsAttacker(  self.Owner )
	ent:Spawn()
	ent:SetKeyValue( "iMagnitude", "42" )
	ent:Fire( "Explode", 0, 0 )
	
	util.BlastDamage( self, self:GetOwner(), self:GetPos(), 50, 75 )
	
	ent:EmitSound( "weapons/big_explosion.mp3" )
     end

   self:TakePrimaryAmmo( 1 )
    self.Owner:SetAnimation( PLAYER_ATTACK1 )
   local owner = self.Owner 
   if not IsValid(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

   owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )
end


function SWEP:SecondaryAttack()

         self:Fly()
end