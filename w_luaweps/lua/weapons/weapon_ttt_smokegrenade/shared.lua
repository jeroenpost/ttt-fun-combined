
if SERVER then
   AddCSLuaFile( "shared.lua" )
--resource.AddFile("materials/vgui/ttt_fun_killicons/smokegrenade.png")
end
   
SWEP.HoldType			= "grenade"
SWEP.PrintName = "Smoke Grenade"
 SWEP.Slot = 3
if CLIENT then
   
  

   SWEP.Icon = "vgui/ttt_fun_killicons/smokegrenade.png"
end

SWEP.Base				= "weapon_tttbasegrenade"

SWEP.Spawnable = true
SWEP.AdminSpawnable = true
SWEP.ViewModelFlip = false

SWEP.WeaponID = AMMO_SMOKE
SWEP.Kind = WEAPON_NADE

SWEP.UseHands			= true
SWEP.ViewModelFlip		= false
SWEP.ViewModelFOV		= 54
SWEP.ViewModel			= "models/weapons/cstrike/c_eq_smokegrenade.mdl"
SWEP.WorldModel			= "models/weapons/w_eq_smokegrenade.mdl"
SWEP.Weight			= 5
SWEP.AutoSpawnable      = true
-- really the only difference between grenade weapons: the model and the thrown
-- ent.

function SWEP:GetGrenadeName()
   return "ttt_smokegrenade_proj"
end

