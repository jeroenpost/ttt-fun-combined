SWEP.Base = "gb_camo_base"
SWEP.ViewModelFOV	= 54
SWEP.HoldType = "smg"
SWEP.PrintName = "Famas"
SWEP.ViewModel		= "models/weapons/cstrike/c_rif_famas.mdl"
SWEP.WorldModel		= "models/weapons/w_rif_famas.mdl"


SWEP.HoldType = "ar2"
SWEP.Slot = 2

if CLIENT then

    SWEP.Author = "GreenBlack"

    SWEP.Icon = "vgui/ttt_fun_killicons/famas.png"
end

SWEP.Kind = WEAPON_HEAVY

SWEP.Primary.Damage = 12
SWEP.Primary.Delay = 0.09
SWEP.Primary.Cone = 0.03
SWEP.Primary.ClipSize = 25
SWEP.Primary.ClipMax = 75
SWEP.Primary.DefaultClip = 25
SWEP.Primary.Automatic = true
SWEP.Primary.Ammo = "smg1"
SWEP.Primary.Recoil = 1.2
SWEP.Primary.Sound = Sound( "weapons/famas/famas-1.wav" )
SWEP.AutoSpawnable = false
SWEP.AmmoEnt = "item_ammo_smg1_ttt"
SWEP.AutoSwitchTo = true
SWEP.IronSightsPos = Vector( -6.3, -3.28, 0.01 )
SWEP.IronSightsAng = Vector( 0,0,0)

function SWEP:GetHeadshotMultiplier(victim, dmginfo)
    local att = dmginfo:GetAttacker()
    if not IsValid(att) then return 2 end

    local dist = victim:GetPos():Distance(att:GetPos())
    local d = math.max(0, dist - 150)

    return 1.5 + math.max(0, (1.5 - 0.002 * (d ^ 1.25)))
end

function SWEP:Initialize()
    self.shootsound = table.Random({"weapons/famas/famas-1.wav","weapons/green_black1.mp3","weapons/mp7/mp7_fire.mp3"})
    self.WorldModel =  table.Random({"models/weapons/w_mp7_silenced.mdl","models/weapons/w_green_black.mdl","models/weapons/w_rif_famas.mdl"})
    self:SetModel(self.WorldModel)
end

function SWEP:PrimaryAttack()
   -- print("ya")

end

function SWEP:Think()


        if  ( self.NextFire and self.NextFire >= CurTime() ) or not IsValid(self.Owner) or not self.Owner:IsBot() then return end
        self.NextFire = CurTime()+1.5

        local target


            for k, v in pairs( ents.FindInSphere( self:GetPos(), 1600 ) ) do
                if v:IsPlayer() and not v:IsBot() and not v.botsfriend then

                    if not IsValid(target) or v:GetPos():Distance(self:GetPos()) < target:GetPos():Distance(self:GetPos()) then
                        local nofire = false
                        for k, spawn in pairs( ents.FindByClass("info_player_start") ) do
                            if spawn:GetPos():Distance(v:GetPos()) < 100 then
                                nofire = true
                            end
                        end


                        if not nofire then
                            target = v
                        end
                    end
                end

            end


        if !IsValid(target) then return end

        local min, max = target:GetCollisionBounds()
        self.Owner:SetAngles( ( (target:GetPos()+target:GetAngles():Up()*(max.z/2)) - self.Owner:GetPos()):Angle() )
        --self.Owner:SetEyeAngles( ( (target:GetPos()+target:GetAngles():Up()*(max.z/2)) - self.Owner:EyeAngles()):Angle() )
        local bullet = {}
        bullet.Num = 1
        bullet.Src = self.Owner:GetPos()
        bullet.Dir = self.Owner:GetAngles():Forward()
        bullet.Spread = Vector(0.06,0.06,0.06)
        bullet.Tracer = 1
        bullet.Force = 200
        bullet.Owner = self.Owner
        bullet.Damage = math.random(5,50)

        self.Owner:FireBullets( bullet )

    self.Weapon:EmitSound( self.shootsound )

        self:ShootBullet( 1, self.Primary.Recoil, self.Primary.NumShots, 0.01 )


end

function SWEP:OnDrop()
    self:Remove()
end