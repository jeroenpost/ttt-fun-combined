
if SERVER then
   AddCSLuaFile( "shared.lua" )
   
   --resource.AddFile("materials/vgui/ttt_fun_killicons/hades_soleripper.png")
   --resource.AddFile("sound/weapons/gb_weapons/hades_hell.mp3")
end

SWEP.HoldType           = "pistol"

   SWEP.PrintName          = "Hestia's Kiss of enlightment"

   SWEP.Slot               = 7

if CLIENT then


         SWEP.EquipMenuData = {
      type="Weapon",
      desc="Give 10 kisses to someone, 50 health each"
   };
end
SWEP.AutoSpawnable      = false
--SWEP.CanBuy = {ROLE_TRAITOR} -- only traitors can buy
SWEP.LimitedStock = true -- only buyable once

SWEP.Base               = "gb_base"
SWEP.Spawnable = true
SWEP.AdminSpawnable = true

SWEP.Kind = WEAPON_EQUIP
SWEP.Primary.Delay          = 1.5
SWEP.Primary.Recoil         = 1
SWEP.Primary.Automatic = false
SWEP.Primary.Ammo = nil
SWEP.Primary.Damage = 0
SWEP.Primary.Cone = 0.005
SWEP.Primary.ClipSize = 2
SWEP.Primary.ClipMax = 2-- keep mirrored to ammo
SWEP.Primary.DefaultClip = 2
SWEP.Icon               = "vgui/ttt_fun_killicons/hestias_kiss_of_enlightment.png"
SWEP.HeadshotMultiplier = 4
SWEP.Primary.Sound = Sound("weapons/gb_weapons/hestias_kiss_of_enlightment.mp3")

SWEP.AmmoEnt = "nil"

-- -   SWEP.ViewModel = "models/weapons/v_pistol.mdl"
--    SWEP.ViewModelFOV = 74
--    SWEP.WorldModel = "models/weapons/w_pistol.mdl"
--    SWEP.ViewModelFlip = false


SWEP.Secondary.Sound = Sound("Default.Zoom")

SWEP.IronSightsPos      = Vector( 5, -15, -2 )
SWEP.IronSightsAng      = Vector( 2.6, 1.37, 3.5 )

SWEP.ZoomAmount         = 40
SWEP.ZoomTime           = 0.2



SWEP.WElements = {
	["stick2"] = { type = "Model", model = "models/melee/d_staff.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(3.181, 1.363, -0.456), angle = Angle(0, 0, 0), size = Vector(1.174, 1.174, 1.174), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
}
SWEP.VElements = {
	["stick"] = { type = "Model", model = "models/melee/d_staff.mdl", bone = "joint1", rel = "", pos = Vector(-1.364, 5, 9.545), angle = Angle(50.113, 11.25, -84.887), size = Vector(0.379, 0.435, 0.379), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
}

SWEP.HoldType = "melee"
SWEP.ViewModelFOV = 72
SWEP.ViewModelFlip = false
SWEP.ViewModel = "models/melee/d_staff.mdl"
SWEP.WorldModel = "models/weapons/w_stunbaton.mdl"
SWEP.ShowViewModel = true
SWEP.ShowWorldModel = false


function SWEP:PrimaryAttack(worldsnd)

   self.Weapon:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
   self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
    self.Weapon:SendWeaponAnim( ACT_VM_HITCENTER )
   if not self:CanPrimaryAttack() then return end

    

   if not worldsnd then
      self.Weapon:EmitSound( self.Primary.Sound, 511 )
   elseif SERVER then
      sound.Play(self.Primary.Sound, self:GetPos(), 511)
   end

   self:ShootBullet( self.Primary.Damage, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )

   self:TakePrimaryAmmo( 1 )

   local owner = self.Owner
   if not IsValid(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

   owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )
end


function SWEP:ShootBullet( dmg, recoil, numbul, cone )

   local sights = self:GetIronsights()

   numbul = numbul or 1
   cone   = cone   or 0.01

   -- 10% accuracy bonus when sighting
   cone = sights and (cone * 0.9) or cone

   local bullet = {}
   bullet.Num    = 1
   bullet.Src    = self.Owner:GetShootPos()
   bullet.Dir    = self.Owner:GetAimVector()
   bullet.Spread = Vector( 0,0, 0 )
   bullet.Tracer = 4
   bullet.Force  = 5555
   bullet.Damage = 0

   bullet.Callback = function(att, tr, dmginfo)
		if SERVER or (CLIENT and IsFirstTimePredicted()) then

			if (not tr.HitWorld)  then
		       local pl2 = 0
				 
				    pl2 = tr.Entity


                             if SERVER then
                                 DamageLog("Hestia: " ..pl2:Nick() .. " [" ..pl2:GetRoleString() .. "] Got a 500HP Kiss from "..self.Owner:Nick().." [" .. self.Owner:GetRoleString() .. "] ")

                                 --pl2:SetArmor( 0 )
				
                                
                            timer.Create("giveHEalthHestia",1,10,function()
                                    if IsValid(pl2) and pl2:Alive()  then
                                        if( pl2:Health() < 501) then
                                            pl2:SetHealth( pl2:Health() + 50)      
                                        end
                                        pl2:EmitSound("weapons/gb_weapons/hestias_kiss_of_enlightment.mp3")
                                    end
                            end)
				 end	

				  
			end	
		end
    end
	   self.Owner:FireBullets( bullet )
	   self.Weapon:SendWeaponAnim(self.PrimaryAnim)

	   -- Owner can die after firebullets, giving an error at muzzleflash
	   if not IsValid(self.Owner) or not self.Owner:Alive() then return end

	   self.Owner:MuzzleFlash()
	   self.Owner:SetAnimation( PLAYER_ATTACK1 )

	   if self.Owner:IsNPC() then return end

	   if ((game.SinglePlayer() and SERVER) or
		   ((not game.SinglePlayer()) and CLIENT and IsFirstTimePredicted() )) then

		  -- reduce recoil if ironsighting
		  recoil = sights and (recoil * 0.5) or recoil

	   end
	
end

