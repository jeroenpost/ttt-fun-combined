
AddCSLuaFile(  )
SWEP.HoldType			= "knife"



SWEP.PrintName	                = "Silenced Knife"
SWEP.Author				= "GreenBlack"
SWEP.Category		= "GreenBlack"
SWEP.Slot			        = 0
SWEP.SlotPos		        = 1
SWEP.DrawAmmo                  = false


SWEP.IronSightsPos = Vector(0.804, -4.624, -6.031)
SWEP.IronSightsAng = Vector(30.25, -9.849, 5.627)

if CLIENT then


    SWEP.Slot         = 6

    SWEP.ViewModelFlip = false

    SWEP.EquipMenuData = {
        type = "item_weapon",
        desc = "Silenced Knife. Kill in silence and fly with RMB"
    };

    SWEP.Icon = "vgui/ttt/icon_knife"
end
SWEP.Kind = WEAPON_EQUIP2
SWEP.CanBuy = {ROLE_TRAITOR}
SWEP.Base				= "aa_base"
SWEP.Spawnable			= true
SWEP.AdminSpawnable		= true
SWEP.ViewModel		= "models/weapons/v_knife_t.mdl"
SWEP.WorldModel		= "models/weapons/w_knife_t.mdl"
SWEP.DrawCrosshair   = false
SWEP.Camo = 54
SWEP.CustomCamo = true
SWEP.ViewModelFOV = 80

SWEP.ViewModelFlip = false


SWEP.Weight				= 1
SWEP.AutoSwitchTo		= true
SWEP.AutoSwitchFrom		= false
SWEP.CSMuzzleFlashes		= false
SWEP.AdminSpawnable = true
SWEP.Primary.Damage			= 50
SWEP.Primary.Force			= 0.75
SWEP.Primary.ClipSize		= -1
SWEP.Primary.Delay			= 0.40
SWEP.Primary.DefaultClip	= 1
SWEP.Primary.Automatic		= true
SWEP.Primary.Ammo			= "none"

SWEP.Secondary.ClipSize		= -1
SWEP.Secondary.DefaultClip	= -1
SWEP.Secondary.Damage			= 0
SWEP.Secondary.Automatic		= false
SWEP.Secondary.Ammo			= "none"

function SWEP:Initialize()

    self:SetHoldType( "knife" )
    self.BaseClass.Initialize(self)

end


SWEP.promah 				= Sound("weapons/knife/knife_slash1.wav")
SWEP.popadanie 				= Sound("weapons/usp/usp1.wav")

SWEP.VElements = {
    ["silent"] = { type = "Model", model = "models/mechanics/various/211.mdl", bone = "v_weapon.Right_Index01", rel = "", pos = Vector(0.518, 0.518, -4.676), angle = Angle(180, 148.442, 3.506), size = Vector(0.497, 0.497, 1.21), color = Color(80, 80, 80, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {[2] = 1} }
}
SWEP.WElements = {
    ["silent"] = { type = "Model", model = "models/mechanics/various/211.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(5.714, 2.4, -16.105), angle = Angle(10.519, 1.169, -3.507), size = Vector(0.69, 0.69, 1.014), color = Color(80, 80, 80, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {[2] = 1} }
}



function SWEP:PrimaryAttack()

    local tracet = {}
    tracet.start = self.Owner:GetShootPos()
    tracet.endpos = self.Owner:GetShootPos() + ( self.Owner:GetAimVector() * 150 )
    tracet.filter = self.Owner
    tracet.mask = MASK_SHOT
    local trace = util.TraceLine( tracet )

    self.Weapon:SetNextPrimaryFire(CurTime() + self.Primary.Delay)
    self.Owner:SetAnimation( PLAYER_ATTACK1 )

    if ( trace.Hit ) then

        if trace.Entity:IsPlayer() or string.find(trace.Entity:GetClass(),"npc") or string.find(trace.Entity:GetClass(),"prop_ragdoll") then
            self.Weapon:SendWeaponAnim(ACT_VM_HITCENTER)
            bullet = {}
            bullet.Num    = 5
            bullet.Src    = self.Owner:GetShootPos()
            bullet.Dir    = self.Owner:GetAimVector()
            bullet.Spread = Vector(0.03, 0.03, 0.03)
            bullet.Tracer = 0
            bullet.Force  = 1
            bullet.Damage = self.Primary.Damage / 5
            self.Owner:FireBullets(bullet)
        else
            self.Weapon:SendWeaponAnim(ACT_VM_HITCENTER)
            bullet = {}
            bullet.Num    = 1
            bullet.Src    = self.Owner:GetShootPos()
            bullet.Dir    = self.Owner:GetAimVector()
            bullet.Spread = Vector(0, 0, 0)
            bullet.Tracer = 0
            bullet.Force  = 1000
            bullet.Damage = self.Primary.Damage
            self.Owner:FireBullets(bullet)
            self.Weapon:EmitSound( self.popadanie )
            util.Decal("ManhackCut", trace.HitPos + trace.HitNormal, trace.HitPos - trace.HitNormal)
        end
    else
        self.Weapon:EmitSound(self.promah,100,math.random(90,120))
        self.Weapon:SendWeaponAnim(ACT_VM_MISSCENTER)
    end


end


function SWEP:Reload()
    return false
end


function SWEP:SecondaryAttack(worldsnd)

   self:Fly()
end