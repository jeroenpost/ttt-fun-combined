
include "weapons/weapon_zm_shotgun.lua"
SWEP.Base = "gb_camo_base"
SWEP.ViewModelFOV	= 54
SWEP.HoldType = "smg"
SWEP.PrintName = "The Snake"
SWEP.ViewModel		= "models/weapons/cstrike/c_shot_m3super90.mdl"
SWEP.WorldModel		= "models/weapons/w_shot_m3super90.mdl"
SWEP.AutoSpawnable = false
SWEP.Camo = 20

SWEP.HoldType = "shotgun"
SWEP.Slot = 7
SWEP.Kind = WEAPON_EQUIP2

SWEP.EquipMenuData = {
    type = "Weapon",
    desc = "The Snake. Poisonous and Dangerous"
};


if CLIENT then

    SWEP.Author = "GreenBlack"

    SWEP.Icon = "vgui/ttt_fun_killicons/thesnake.png"
end


SWEP.CanBuy = { ROLE_TRAITOR }

SWEP.Primary.Sound			= Sound( "Weapon_M3.Single" )
SWEP.Primary.Recoil			= 7

SWEP.Primary.Ammo = "Buckshot"
SWEP.Primary.Damage = 11
SWEP.Primary.Cone = 0.085
SWEP.Primary.Delay = 0.8
SWEP.Primary.ClipSize = 9
SWEP.Primary.ClipMax = 24
SWEP.Primary.DefaultClip = 9
SWEP.Primary.Automatic = true
SWEP.Primary.NumShots = 8
SWEP.HeadshotMultiplier = 1.1
SWEP.AutoSpawnable      = true
SWEP.AmmoEnt = "item_box_buckshot_ttt"

function SWEP:Deploy()
    self.dt.reloading = false
    self.reloadtimer = 0
    return self:DeployFunction()
end

SWEP.IronSightsPos = Vector(-7.65, -12.214, 3.2)
SWEP.IronSightsAng = Vector(0, 0, 0)