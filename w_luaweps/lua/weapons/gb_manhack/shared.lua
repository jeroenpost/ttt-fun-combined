if SERVER then
    AddCSLuaFile( "shared.lua" )
    --resource.AddFile("materials/vgui/ttt_fun_killicons/manhackgun.png")
end

SWEP.Base				= "gb_base"
SWEP.Kind = WEAPON_EQUIP
SWEP.HoldType = "pistol"
SWEP.ViewModelFlip = false
SWEP.Spawnable			= true
SWEP.AdminSpawnable		= true
SWEP.AutoSpawnable      = false 
SWEP.Icon = "vgui/ttt_fun_killicons/manhackgun.png"

SWEP.PrintName            = "Manhack Gun"            
SWEP.Slot                = 6
SWEP.SlotPos            = 1
SWEP.DrawAmmo            = true
SWEP.DrawCrosshair        = true
SWEP.Weight                = 5 
SWEP.AutoSwitchTo        = false
SWEP.AutoSwitchFrom        = false
SWEP.Author            = ""
SWEP.Contact        = ""
SWEP.Purpose        = ""
SWEP.EquipMenuData = {
      type  = "item_weapon",
      name  = "Manhack Gun",
      desc  = "Shoot a prop to attach a Manhack.\nRight click to attach a rollermine."
      }

SWEP.Instructions    = "Shoot a prop to attach a Manhack.\nRight click to attach a rollermine."
SWEP.ViewModel            = "models/weapons/v_pistol.mdl"
SWEP.WorldModel            = "models/weapons/w_pistol.mdl"
SWEP.Primary.ClipSize        = 3
SWEP.Primary.DefaultClip    = 3
SWEP.Primary.Automatic        = false
SWEP.Primary.Ammo            = "XBowBolt"
SWEP.Primary.Delay            = 0.5
SWEP.Secondary.Delay            = 0.5
SWEP.Secondary.ClipSize        = 2
SWEP.Secondary.DefaultClip    = 2
SWEP.Secondary.Automatic    = false
SWEP.Secondary.Ammo            = "XBowBolt"
SWEP.AmmoEnt = ""

SWEP.CanBuy = {ROLE_TRAITOR}

local sndPowerUp		= Sound("rope_hit.wav")
local sndPowerDown		= Sound ("shoot_rope.wav")
local sndTooFar			= Sound ("to_far.wav")
local ShootSound = Sound( "Metal.SawbladeStick" )

/*---------------------------------------------------------
Reload does nothing
---------------------------------------------------------*/
function SWEP:Reload()
end

/*---------------------------------------------------------
Think does nothing
---------------------------------------------------------*/
function SWEP:Think()    
end


/*---------------------------------------------------------
PrimaryAttack
---------------------------------------------------------*/
function SWEP:PrimaryAttack()

    if not self:CanPrimaryAttack() then return end
       self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
   self.Weapon:SetNextSecondaryFire( CurTime() + self.Secondary.Delay )

if #ents.FindByClass("npc_manhack") > 4 then
self.Owner:PrintMessage( HUD_PRINTCENTER, "5 manhacks alive is the max on one map" )
return
end
    
    local tr = self.Owner:GetEyeTrace()
    
    local effectdata = EffectData()
    effectdata:SetOrigin( tr.HitPos )
    effectdata:SetNormal( tr.HitNormal )
    effectdata:SetMagnitude( 8 )
    effectdata:SetScale( 1 )
    effectdata:SetRadius( 16 )
    util.Effect( "Sparks", effectdata )
    
    self:EmitSound( ShootSound )


    
    self:ShootEffects( self )
     self:TakePrimaryAmmo( 1 )
    
    // The rest is only done on the server
    if (!SERVER) then return end

DamageLog("Manhack: " .. self.Owner:Nick() .. " [" .. self.Owner:GetRoleString() .. "] spawned a manhack ")

// Make a manhack
    local ent = ents.Create( "npc_manhack" )
    ent:SetPos( tr.HitPos + self.Owner:GetAimVector() * -46 )
    ent:SetAngles( tr.HitNormal:Angle() )
    ent:SetPhysicsAttacker(self.Owner)
    ent:Spawn()
        if  self.Owner:GetRoleString() ~= "traitor" then
        ent:SetKeyValue("sk_manhack_melee_dmg", 0)
        end
    
    // Weld it to the object that we hit
   -- local weld = constraint.Weld( tr.Entity, ent, tr.PhysicsBone, 0, 0 )
    
    undo.Create("Manhack")
    undo.AddEntity( weld )
    undo.AddEntity( nocl )
    undo.AddEntity( ent )
    undo.SetPlayer( self.Owner )
    undo.Finish()
    
end

/*---------------------------------------------------------
SecondaryAttack
---------------------------------------------------------*/
function SWEP:SecondaryAttack()

    if not self:CanPrimaryAttack() or self:Clip1() < 20 then return end
       self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
   self.Weapon:SetNextSecondaryFire( CurTime() + self.Secondary.Delay )
    
    local tr = self.Owner:GetEyeTrace()
    
    self:EmitSound( ShootSound )
    self:ShootEffects( self )
    
    local effectdata = EffectData()
    effectdata:SetOrigin( tr.HitPos )
    effectdata:SetNormal( tr.HitNormal )
    effectdata:SetMagnitude( 8 )
    effectdata:SetScale( 1 )
    effectdata:SetRadius( 16 )
    util.Effect( "Sparks", effectdata )
     self:TakePrimaryAmmo( 1 )
    
    // The rest is only done on the server
    if (!SERVER) then return end
    
    // Make a manhack
    local ent = ents.Create( "npc_rollermine" )
    ent:SetPos( tr.HitPos + self.Owner:GetAimVector() * -46 )
    ent:SetAngles( tr.HitNormal:Angle() )
    ent:SetPhysicsAttacker(self.Owner)
    ent:Spawn()

DamageLog("Rollermine: " .. self.Owner:Nick() .. " [" .. self.Owner:GetRoleString() .. "] spawned a rollermine ")


// Weld it to the object that we hit
    --local weld = constraint.Weld( tr.Entity, ent, tr.PhysicsBone, 0, 0 )
    
    undo.Create("Rollermine")
    undo.AddEntity( weld )
    undo.AddEntity( nocl )
    undo.AddEntity( ent )
    undo.SetPlayer( self.Owner )
    undo.Finish()
    
end


/*---------------------------------------------------------
Name: ShouldDropOnDie
Desc: Should this weapon be dropped when its owner dies?
---------------------------------------------------------*/
function SWEP:ShouldDropOnDie()
    return false
end