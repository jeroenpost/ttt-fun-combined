
    if SERVER then
       AddCSLuaFile( "shared.lua" )
	   --resource.AddFile("materials/vgui/ttt_fun_killicons/tazer.png");
    end
           SWEP.PrintName                       = "Tazer"
       SWEP.Slot                            = 6
    if CLIENT then
 
     
       SWEP.EquipMenuData = {
          type  = "item_weapon",
          name  = "Tazer",
          desc  = "Ragdolls the person for 15 seconds. You only have 2 zaps"
       };
     
       SWEP.Icon = "vgui/ttt_fun_killicons/tazer.png"
    end
     
    SWEP.Base = "weapon_tttbase"
    SWEP.Kind = WEAPON_EQUIP
    SWEP.CanBuy = {ROLE_DETECTIVE, ROLE_TRAITOR}
     
    SWEP.ViewModel = "models/weapons/v_pistol.mdl"
    SWEP.ViewModelFOV = 54
    SWEP.WorldModel = "models/weapons/w_pistol.mdl"
    SWEP.ViewModelFlip = false
	
    SWEP.Primary.ClipSize = 2
    SWEP.Primary.DefaultClip = 2
    SWEP.Primary.Ammo = "Battery"
    SWEP.Primary.Automatic = false
    SWEP.Primary.Cone = 0.16
    SWEP.Primary.NumShots = 50
     SWEP.Primary.Damage = 0

    SWEP.Secondary.ClipSize = -1
    SWEP.Secondary.DefaultClip = -1
    SWEP.Secondary.Ammo = "none"
    SWEP.Secondary.Automatic = true
     
    SWEP.NoSights = true
    SWEP.AllowDrop = true
    SWEP.LimitedStock = true
    SWEP.AutoSpawnable = false
     
    SWEP.StunTime = 8 -- How long is the victim ragdolled?
     
    SWEP.SlowTime = 5 -- How long is the victim "exhausted"?
    SWEP.SlowPercent = 0.33 -- How much they are slowed?
     
    SWEP.Delay = 5
    SWEP.Range = 1024
     
    SWEP.Sounds = {
            "ambient/energy/zap1.wav",
            "ambient/energy/zap2.wav",
            "ambient/energy/zap3.wav",
            "ambient/energy/zap5.wav",
            "ambient/energy/zap6.wav",
            "ambient/energy/zap7.wav",
            "ambient/energy/zap8.wav",
            "ambient/energy/zap9.wav"
    }
     
    function SWEP:Precache()
            for _, v in pairs (self.Sounds) do
                    util.PrecacheSound(v)
            end
    end
     
    function SWEP:Deploy()
            self:SendWeaponAnim(ACT_VM_DRAW)
            self:SetNextPrimaryFire(CurTime() + 1)
    end
     
-- Shooting functions largely copied from weapon_cs_base
function SWEP:PrimaryAttack(worldsnd)

   self.Weapon:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
   self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )

   if not self:CanPrimaryAttack() then return end

   if not worldsnd then
      self.Weapon:EmitSound( self.Primary.Sound, self.Primary.SoundLevel )
   elseif SERVER then
      sound.Play(self.Primary.Sound, self:GetPos(), self.Primary.SoundLevel)
   end

   self:ShootBullet( self.Primary.Damage, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )

   self:TakePrimaryAmmo( 1 )

   local owner = self.Owner
   if not IsValid(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end
    
    if (IsFirstTimePredicted()) then
                    local snd, i = table.Random(self.Sounds)
                    self:EmitSound(snd)
            end

   owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )
end

     
    function SWEP:SecondaryAttack()
            if (SERVER) then       
                    local trace = self.Owner:GetEyeTrace()
                    if (trace.HitNonWorld and trace.HitPos:Distance(trace.StartPos) < self.Range) then
                            local victim = trace.Entity
                            if (victim:GetClass() ~= "prop_ragdoll") then return end
                           
                            local shock1 = math.random(-1200, 1200)
                            local shock2 = math.random(-1200, 1200)
                            local shock3 = math.random(-1200, 1200)
                            victim:GetPhysicsObject():ApplyForceCenter(Vector(shock1, shock2, shock3))  
                    end
            end
    end
     
    hook.Add("CanPlayerSuicide", "CantSuicideIfRagdolled", function(ply)
            return ply.Ragdolled == nil
    end)


function SWEP:ShootBullet(  )

 
    attacker = self.Owner
    local tracedata = {}

    tracedata.start = self.Owner:GetShootPos()
    tracedata.endpos = self.Owner:GetShootPos() + ( self.Owner:GetAimVector() * 450 )
    tracedata.filter = self.Owner
    tracedata.mins = Vector( -3,-3,-3 )
    tracedata.maxs = Vector( 3,3,3 )
    tracedata.mask = MASK_SHOT_HULL
    local trace = util.TraceHull( tracedata )

    victim = trace.Entity

     
  if ( not IsValid(attacker) or !SERVER  or not victim:IsPlayer() or victim:IsWorld() ) then return end
    if  !victim:Alive() or victim.IsDog or specialRound.isSpecialRound or _globals['specialperson'][victim:SteamID()] or (victim.NoTaze && self.Owner:GetRole() != ROLE_DETECTIVE)    then
        self.Owner:PrintMessage( HUD_PRINTCENTER, "Person cannot be tazed" )
            
            return end			
        

    if victim:GetPos():Distance( attacker:GetPos()) > 400 then return end
                        
               StunTime = self.StunTime
 
               local edata = EffectData()
                edata:SetEntity(victim)
                edata:SetMagnitude(3)
                edata:SetScale(3)

                util.Effect("TeslaHitBoxes", edata)

                    self:Taze( victim)
                
                       


end


function SWEP:Taze( victim )

     if not victim:Alive() or victim:IsGhost() then return end

        if victim:InVehicle() then
                local vehicle = victim:GetParent()
                victim:ExitVehicle()
        end

        ULib.getSpawnInfo( victim )

    
            local rag = ents.Create("prop_ragdoll")
                if not (rag:IsValid()) then return end

     if SERVER then DamageLog("Tazer: " .. self.Owner:Nick() .. " [" .. self.Owner:GetRoleString() .. "] Tazed "..victim:Nick().." [" .. victim:GetRoleString() .. "] ")
    end

     rag.BelongsTo = victim
                rag.Weapons = {}
                for _, v in pairs (victim:GetWeapons()) do
                        table.insert(rag.Weapons, v:GetClass())
                end

                rag.Credits = victim:GetCredits()
                rag.OldHealth = victim:Health()
                rag:SetModel(victim:GetModel())
                  rag:SetPos( victim:GetPos() )
                 local velocity = victim:GetVelocity()
        rag:SetAngles( victim:GetAngles() )
        rag:SetModel( victim:GetModel() )
        rag:Spawn()
        rag:Activate()
        victim:SetParent( rag )
                 victim:SetParent( rag )
                victim.Ragdolled = rag
                rag.orgPos = victim:GetPos() 
                rag.orgPos.z = rag.orgPos.z + 10  
                
                victim.orgPos = victim:GetPos()
                victim:StripWeapons()
                victim:Spectate(OBS_MODE_CHASE)
                victim:SpectateEntity(rag) 

                rag:EmitSound(Sound("vo/Citadel/br_ohshit.wav"))
             
                victim:PrintMessage( HUD_PRINTCENTER, "You have been tasered. "..self.StunTime.." seconds till revival" )
                 timer.Create("revivedelay"..victim:UniqueID(), self.StunTime, 1, function() TazerRevive( victim ) end )

end

function TazerRevive( victim)

    if !IsValid( victim ) then return end
    rag = victim:GetParent()

        victim:SetParent()

    if SERVER and victim.IsSlayed then
        victim:Kill();
        return
    end
                       

                        if (rag and rag:IsValid()) then
                                local weps = table.Copy(rag.Weapons)
                                credits = rag.Credits
                                oldHealth = rag.OldHealth
                                rag:DropToFloor()
                                pos = rag:GetPos()  --move up a little
                                 pos.z = pos.z + 15                               
                                
              
                                victim.Ragdolled = nil
                               
                                victim:UnSpectate()
                              --  victim:DrawViewModel(true)
                              --  victim:DrawWorldModel(true)
                                victim:Spawn()
                                --ULib.spawn( victim, true )
                                rag:Remove()
                                victim:SetPos( pos )
                                victim:StripWeapons()

                                UnStuck_stuck(victim)


                                for _, v in pairs (weps) do
                                        victim:Give(v)
                                end

                            if credits > 0 then
                                    victim:AddCredits( credits  )
                            end
                            if oldHealth > 0 then
                                victim:SetHealth( oldHealth  )
                            end
                            victim:DropToFloor()



                             if  !victim:IsInWorld() or !victim:OnGround()    then
                              victim:SetPos( victim.orgPos )
                             end

                              timer.Create("RespawnIfStuck",1,1,function()
                                     if IsValid(victim) and (!victim:IsInWorld() or !victim:OnGround() ) then
                                            victim:SetPos( victim.orgPos   )
                                     end
                              end)

                       else
                            print("Debug: ragdoll or player is invalid")
                            if IsValid( victim) then
                                    ULib.spawn( victim, true )
                           end
                       end
                


end