if SERVER then
   AddCSLuaFile( "shared.lua" )
   --resource.AddFile("materials/vgui/ttt_fun_killicons/m3.png")
end

SWEP.HoldType = "shotgun"
SWEP.PrintName = "The Crippler"
SWEP.Slot = 2
if CLIENT then
   
   SWEP.Author = "GreenBlack"
   
   SWEP.Icon = "vgui/ttt_fun_killicons/m3.png"
end

SWEP.Base = "aa_base"
SWEP.Spawnable = true
SWEP.AdminSpawnable = true
SWEP.Kind = WEAPON_EQUIP
SWEP.WeaponID = AMMO_SHOTGUN

SWEP.Primary.Ammo = "Buckshot"
SWEP.Primary.Damage = 1
SWEP.Primary.Cone = 0.001
SWEP.Primary.Delay = 0.6
SWEP.Primary.ClipSize = 100
SWEP.Primary.ClipMax = 100
SWEP.Primary.DefaultClip = 100
SWEP.Primary.Automatic = true
SWEP.Primary.NumShots = 100
SWEP.HeadshotMultiplier = 2
SWEP.AutoSpawnable = true
SWEP.AmmoEnt = "item_box_buckshot_ttt"
SWEP.ViewModel = "models/weapons/v_shot_m3super90.mdl"
SWEP.WorldModel = "models/weapons/w_shot_m3super90.mdl"
SWEP.Primary.Sound = Sound( "Weapon_M3.Single" )
SWEP.Primary.Recoil = 0.1
SWEP.reloadtimer = 0

SWEP.IronSightsPos = Vector(5.74, -2.19, 3.28)
SWEP.IronSightsAng = Vector(0.547, 0, 0)

SWEP.HoldType = "pistol"
SWEP.ViewModelFOV = 70
SWEP.ViewModelFlip = false
SWEP.ViewModel      = "models/weapons/v_nessbat.mdl"
SWEP.WorldModel   	= "models/weapons/w_nessbat.mdl"
SWEP.ShowViewModel = true
SWEP.ShowWorldModel = true
SWEP.ViewModelBoneMods = {}
SWEP.Sounds = {
    "weapons/gb_weapons/partygun1.mp3"
}


function SWEP:PrimaryAttack(worldsnd)
    self.Weapon:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )

    self:EmitSound("Nessbat/bat_sound.mp3")
    self.Weapon:SendWeaponAnim( ACT_VM_HITCENTER )

    local bullet = {}
    bullet.Num    = 20
    bullet.Src    = self.Owner:GetShootPos()
    bullet.Dir    = self.Owner:GetAimVector()
    bullet.Spread = Vector(0, 0, 0)
    bullet.Tracer = 0
    bullet.Force  = 100000
    bullet.Damage = 0.5
    self.Owner:FireBullets(bullet)

        local tr = self.Owner:GetEyeTrace()
    if IsValid(tr.Entity) and (tr.Entity:IsPlayer() or tr.Entity:IsNPC() or tr.Entity:IsRagdoll() ) then
        local edata = EffectData()
        edata:SetStart(self.Owner:GetShootPos())
        edata:SetOrigin(tr.HitPos)
        edata:SetNormal(tr.Normal)
        edata:SetEntity(tr.Entity)


            util.Effect("BloodImpact", edata)

        timer.Simple(0.01,function()
        tr.Entity:ManipulateBoneScale(math.random(tr.Entity:GetBoneCount()), VectorRand())
        timer.Simple(0.01,function() tr.Entity:ManipulateBoneScale(math.random(tr.Entity:GetBoneCount()), VectorRand()) end)
        timer.Simple(0.02,function() tr.Entity:ManipulateBoneScale(math.random(tr.Entity:GetBoneCount()), VectorRand()) end)
        timer.Simple(0.03,function() tr.Entity:ManipulateBoneScale(math.random(tr.Entity:GetBoneCount()), VectorRand()) end)
        timer.Simple(0.04,function() tr.Entity:ManipulateBoneScale(math.random(tr.Entity:GetBoneCount()), VectorRand()) end)
        end)

    end


end
