if false then

SWEP.Base = "weapon_tttbase"
SWEP.Kind = WEAPON_EQUIP1

SWEP.PrintName = "Running Boots"

SWEP.Icon = "vgui/ttt_fun_killicons/runningshoes.png"
SWEP.EquipMenuData = {
      type  = "item_weapon",
      name  = "Running Shoes",
      desc  = "Makes you run twice as fast! Use Shift to run"
      }
SWEP.CanBuy = {ROLE_DETECTIVE}

SWEP.AutoSwitchTo = true

function SWEP:Equip()
   if IsValid(self.Owner) then
  self.Owner.hasRunningBoots = true
    self.Owner:SetNWInt("runspeed", 650)
    self.Owner:SetNWInt("walkspeed",300)
    self.Owner:SetJumpPower(350)
   -- self.Owner:SetSpeed(false)
    end
    self:Remove()
end

function SWEP:Deploy()
   if IsValid(self.Owner) then
  self.Owner.hasRunningBoots = true
    self.Owner:SetNWInt("runspeed", 650)
    self.Owner:SetNWInt("walkspeed",300)
    self.Owner:SetJumpPower(350)
   -- self.Owner:SetSpeed(false)
    end
    self:Remove()
end


end