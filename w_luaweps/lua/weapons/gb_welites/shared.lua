if SERVER then
   AddCSLuaFile( "shared.lua" )
   --resource.AddFile("materials/vgui/ttt_fun_killicons/elites.png")
end
   
SWEP.HoldType = "duel"
   
 SWEP.PrintName = "Popcorn Elites"
SWEP.Slot = 1
if CLIENT then
   
   
   SWEP.Author = "GreenBlack"
 
   SWEP.Icon = "vgui/ttt_fun_killicons/elites.png"
end
 
SWEP.Base = "aa_base"

SWEP.Kind = WEAPON_PISTOL
SWEP.Primary.Recoil     = 1.5
SWEP.Primary.Damage = 30
SWEP.Primary.Delay = 0.40
SWEP.Primary.Cone = 0.02
SWEP.Primary.ClipSize = 20
SWEP.Primary.Automatic = true
SWEP.Primary.DefaultClip = 20
SWEP.Primary.ClipMax = 60
SWEP.Primary.Ammo = "Pistol"

SWEP.Secondary.Delay = 0.90
SWEP.Secondary.ClipSize     = 20
SWEP.Secondary.DefaultClip  = 20
SWEP.Secondary.Automatic    = true
SWEP.Secondary.Ammo         = "Pistol"
SWEP.Secondary.ClipMax      = 60

SWEP.AutoSpawnable = true
SWEP.AmmoEnt = "item_ammo_pistol_ttt"
SWEP.NoSights = true
 
SWEP.ViewModel  =  "models/weapons/cstrike/c_pist_elite.mdl"
SWEP.WorldModel = "models/weapons/w_pist_elite.mdl"
SWEP.ViewModelFlip = false
SWEP.ViewModelFOV  = 54
SWEP.UseHands = true
 
SWEP.Primary.Sound = Sound( "npc/roller/mine/rmine_predetonate.wav" )
SWEP.Secondary.Sound = Sound( "Weapon_Elite.Single" )


function SWEP:PrimaryAttack( worldsnd )
 
   --self.Weapon:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
   self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )

   if not self:CanPrimaryAttack() then return end

   if not worldsnd then
      self.Weapon:EmitSound( self.Primary.Sound, self.Primary.SoundLevel )
   elseif SERVER then
      sound.Play(self.Primary.Sound, self:GetPos(), self.Primary.SoundLevel)
   end

   self:ShootBullet( self.Primary.Damage, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )
    self:SecondaryAttack()
   self:TakePrimaryAmmo( 1 )

   local owner = self.Owner
   if not IsValid(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

 --  owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )
end

function SWEP:CanSecondaryAttack()
   if not IsValid(self.Owner) then return end

   if self.Weapon:Clip1() <= 0 then
      self:DryFire(self.SetNextSecondaryFire)
      return false
   end
   return true
end




function SWEP:SecondaryAttack()
    local bucket, att, phys, tr

    self.Weapon:SetNextSecondaryFire(CurTime() + 0.8)


    if CLIENT then
        return
    end


    self.Owner:ViewPunch( Angle( math.Rand(-8,8), math.Rand(-8,8), 0 ) )

    bucket = ents.Create( "sent_popcorn_thrown" )
    bucket:SetOwner( self.Owner )

    bucket:SetPos( self.Owner:GetShootPos( ) )
    bucket:Spawn()
    bucket:SetMaterial( "engine/occlusionproxy" )
    bucket:Activate()


    phys = bucket:GetPhysicsObject( )

    if IsValid( phys ) then
        phys:SetVelocity( self.Owner:GetPhysicsObject():GetVelocity() )
        phys:AddVelocity( self.Owner:GetAimVector( ) * 128 * phys:GetMass( ) )
        phys:AddAngleVelocity( VectorRand() * 128 * phys:GetMass( ) )
    end

    net.Start("Popcorn_Explosion")
    net.WriteVector(self:GetPos())
     net.WriteVector(VectorRand())
    net.WriteVector(phys:GetVelocity())
    net.WriteFloat(bucket:EntIndex())
    net.Broadcast()


    --[[
    if !self.Owner:IsAdmin() then
        self.Owner:StripWeapon("weapon_popcorn")
    end
    --]]
end


function SWEP:Initialize()
    self:SetColorAndMaterial(Color(255,255,255,255),"teh_maestro/popcornbuckettexture");
end

-- COLOR
function SWEP:Deploy()
    self.Weapon:SendWeaponAnim(ACT_VM_DRAW)
    self:SetColorAndMaterial(Color(255,255,255,255), "teh_maestro/popcornbuckettexture");
    return true
end


function SWEP:PreDrop()
    self:ColorReset()
end


function SWEP:Holster()
    self:ColorReset()
    return true
end

function SWEP:OnRemove()
    self:ColorReset()
end
function SWEP:OnDrop()
    self:ColorReset()

end

function SWEP:PreDrawViewModel()

    if SERVER or not IsValid(self.Owner) or not IsValid(self.Owner:GetViewModel()) then return end
    self.Owner:GetViewModel():SetColor(Color(255,255,255,255))
    self.Owner:GetViewModel():SetMaterial( "teh_maestro/popcornbuckettexture"   )

end