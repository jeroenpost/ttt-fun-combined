if SERVER then
   AddCSLuaFile( "shared.lua" )
   --resource.AddFile("materials/vgui/ttt_fun_killicons/ak47.png")
end

SWEP.HoldType = "ar2"
   SWEP.Slot = 2
 SWEP.PrintName = "AK-47 Gold"
if CLIENT then

   SWEP.Author = "GreenBlack"

   SWEP.Icon = "vgui/ttt_fun_killicons/ak47.png"
end

SWEP.Base = "aa_base"
SWEP.Spawnable = true
SWEP.AdminSpawnable = true
SWEP.AutoSpawnable = true
SWEP.Kind = WEAPON_HEAVY
SWEP.Primary.Delay = 0.12
SWEP.Primary.Recoil = 2.0
SWEP.Primary.Automatic = true
SWEP.Primary.Ammo = "Pistol"
SWEP.Primary.Damage = 18
SWEP.Primary.Cone = 0.018
SWEP.Primary.ClipSize = 30
SWEP.Primary.ClipMax = 90
SWEP.Primary.DefaultClip = 30
SWEP.AmmoEnt = "item_ammo_pistol_ttt"
SWEP.UseHands = true
SWEP.ViewModel = "models/weapons/cstrike/c_rif_ak47.mdl"
SWEP.WorldModel = "models/weapons/w_rif_ak47.mdl"
SWEP.Primary.Sound = Sound("weapons/ak47/ak47-1.wav")
SWEP.ViewModelFOV		= 54
SWEP.ViewModelFlip = false

SWEP.HeadshotMultiplier = 2

SWEP.IronSightsPos = Vector( 6.1, -3.30, 1.80 )
SWEP.IronSightsAng = Vector( 3.28, 0, 0 )


-- COLOR
function SWEP:Deploy()
    self.Weapon:SendWeaponAnim(ACT_VM_DRAW)
    self:SetColorAndMaterial(Color(255,255,255,255),"models/weapons/v_models/green_deagle/main" );
    return true
end


function SWEP:PreDrop()
    self:ColorReset()
end


function SWEP:Holster()
    self:ColorReset()
    return true
end

function SWEP:OnRemove()
    self:ColorReset()
end
function SWEP:OnDrop()
    self:ColorReset()

end

function SWEP:PreDrawViewModel()

    if SERVER or not IsValid(self.Owner) or not IsValid(self.Owner:GetViewModel()) then return end
    self.Owner:GetViewModel():SetColor(Color(255,255,255,255))
    self.Owner:GetViewModel():SetMaterial( "models/weapons/v_models/green_deagle/main"   )

end
-- END COLORD: