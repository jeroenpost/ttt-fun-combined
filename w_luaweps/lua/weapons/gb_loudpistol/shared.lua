
if SERVER then
   AddCSLuaFile( "shared.lua" )
    --resource.AddFile("materials/vgui/ttt_fun_killicons/usp45.png")
end

SWEP.HoldType			= "pistol"
SWEP.CanDecapitate= true

SWEP.PrintName = "Loud Thingy"
 SWEP.Slot = 6
if CLIENT then
   
  
   SWEP.Author = "GreenBlack"
   SWEP.EquipMenuData = {
      type = "item_weapon",
      desc = "sipistol_desc"
   };

   SWEP.Icon = "vgui/ttt_fun_killicons/usp45.png"
end

SWEP.Base = "aa_base"
SWEP.Primary.Recoil	= 1.35
SWEP.Primary.Damage = 29
SWEP.Primary.Delay = 0.38
SWEP.Primary.Cone = 0.009
SWEP.Primary.ClipSize = 25
SWEP.Primary.Automatic = true
SWEP.Primary.DefaultClip = 25
SWEP.Primary.ClipMax = 80
SWEP.Primary.Ammo = "Pistol"

SWEP.AmmoEnt = "item_ammo_pistol_ttt"

SWEP.IsSilent = false

SWEP.UseHands			= true
SWEP.ViewModelFlip		= false
SWEP.ViewModelFOV		= 54
SWEP.ViewModel			= "models/weapons/cstrike/c_pist_usp.mdl"
SWEP.WorldModel			= "models/weapons/w_pist_usp.mdl"

SWEP.Primary.Sound = Sound( "beams/beamstart5.wav" )
SWEP.Primary.SoundLevel = 100

SWEP.IronSightsPos = Vector( -5.91, -4, 2.84 )
SWEP.IronSightsAng = Vector(-0.5, 0, 0)

SWEP.PrimaryAnim = ACT_VM_PRIMARYATTACK_SILENCED
SWEP.ReloadAnim = ACT_VM_RELOAD_SILENCED
SWEP.AutoSpawnable= true


-- We were bought as special equipment, and we have an extra to give
function SWEP:WasBought(buyer)
   if IsValid(buyer) then -- probably already self.Owner
      buyer:GiveAmmo( 20, "Pistol" )
   end
end

function SWEP:Initialize()
    self:SetColorAndMaterial(Color(255,255,255,255),"phoenix_storms/stripes");
end
-- COLOR
function SWEP:Deploy()
    self.Weapon:SendWeaponAnim(ACT_VM_DRAW)
    self:SetColorAndMaterial(Color(255,255,255,255),"phoenix_storms/stripes");
    return true
end


function SWEP:PreDrop()
    self:ColorReset()
end


function SWEP:Holster()
    self:ColorReset()
    return true
end

function SWEP:OnRemove()
    self:ColorReset()
end
function SWEP:OnDrop()
    self:ColorReset()

end

function SWEP:PreDrawViewModel()

    if SERVER or not IsValid(self.Owner) or not IsValid(self.Owner:GetViewModel()) then return end
    self.Owner:GetViewModel():SetColor(Color(255,255,255,255))
    self.Owner:GetViewModel():SetMaterial("phoenix_storms/stripes")

end
-- END COLORD: