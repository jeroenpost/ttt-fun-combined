

if SERVER then
   AddCSLuaFile( "shared.lua" )
   --resource.AddFile("materials/vgui/ttt_fun_killicons/babyboom.png")
   --resource.AddFile("sound/weapons/gb_crybaby.mp3")
   --resource.AddFile("sound/weapons/big_explosion.mp3")
end

SWEP.Base				= "weapon_tttbasegrenade"

SWEP.Kind = WEAPON_EQUIP2
SWEP.WeaponID = AMMO_MOLOTOV

SWEP.HoldType			= "grenade"

SWEP.CanBuy = { ROLE_TRAITOR }
SWEP.InLoadoutFor = nil
SWEP.LimitedStock = false
SWEP.AllowDrop = true
SWEP.IsSilent = false
SWEP.NoSights = true
SWEP.detonate_timer = 20
SWEP.ViewModelFOV = 90
SWEP.ViewModel			= "models/weapons/v_eq_flashbang.mdl"
SWEP.WorldModel			= "models/props_c17/doll01.mdl"
SWEP.Weight				= 5
SWEP.AutoSpawnable      = false
-- really the only difference between grenade weapons: the model and the thrown
-- ent.
SWEP.PrintName	 = "Baby BOOM"
	SWEP.Slot		 = 7

if CLIENT then
   -- Path to the icon material
   	

		SWEP.Icon = "vgui/ttt_fun_killicons/babyboom.png"
	
   -- Text shown in the equip menu
	SWEP.EquipMenuData = {
   
   
		type = "Weapon",
		desc = "Baby Grenade. \nDoes big BOOM 20 seconds \nafter you bought it.\nSo be quick or you explode yourself!"
   };


    function SWEP:DrawWorldModel()
		--self:DrawModel()
		local ply = self.Owner
		local pos = self.Weapon:GetPos()
		local ang = self.Weapon:GetAngles()
		if ply:IsValid() then
			local bone = ply:LookupBone("ValveBiped.Bip01_R_Hand")
			if bone then
				pos,ang = ply:GetBonePosition(bone)
			end
		else
			self.Weapon:DrawModel() --Draw the actual model when not held.
			return
		end
		if self.ModelEntity:IsValid() == false then
			self.ModelEntity = ClientsideModel(self.WorldModel)
			self.ModelEntity:SetNoDraw(true)
		end
		
		self.ModelEntity:SetModelScale(1,0)
		self.ModelEntity:SetPos(pos)
		self.ModelEntity:SetAngles(ang+Angle(0,0,180))
		self.ModelEntity:DrawModel()
	end
	function SWEP:ViewModelDrawn()
		local ply = self.Owner
		if ply:IsValid() and ply == LocalPlayer() then
			local vmodel = ply:GetViewModel()
			local idParent = vmodel:LookupBone("v_weapon.Flashbang_Parent")
			local idBase = vmodel:LookupBone("v_weapon")
			if not vmodel:IsValid() or not idParent or not idBase then return end --Ensure the model and bones are valid.
			local pos, ang = vmodel:GetBonePosition(idParent)	
			local pos1, ang1 = vmodel:GetBonePosition(idBase) --Rotations were screwy with the parent's angle; use the models baseinstead.

			if self.ModelEntity:IsValid() == false then
				self.ModelEntity = ClientsideModel(self.WorldModel)
				self.ModelEntity:SetNoDraw(true)
			end
			
			self.ModelEntity:SetModelScale(1.25,0)
			self.ModelEntity:SetPos( pos-ang1:Forward()*ang1:Up()*1.25+2.3*ang1:Right() )
			self.ModelEntity:SetAngles(ang1)
			self.ModelEntity:DrawModel()
		end
	end
end

function SWEP:GetGrenadeName()
   return "gb_baby"
end

--Taken from base grenade
function SWEP:Initialize()

  
   if self.SetHoldType then
      self:SetHoldType(self.HoldNormal)
   end
   if SERVER then
   
   timer.Create( "soundBabyecvvv", 0.1, 1, function()
	if self.Weapon then


            local soundfile = gb_config.websounds.."weapons/gb_crybaby.mp3"
            gb_PlaySoundFromServer(soundfile, self)

    end
     end)
    timer.Create( "soundBabyec", 4, 0, function()
	if self.Weapon then

        local soundfile = gb_config.websounds.."weapons/gb_crybaby.mp3"
        gb_PlaySoundFromServer(soundfile, self)

    end
     end)


    timer.Create( "explodeBaby", 20, 1, function()

            if self then
               if self.was_thrown and  isfunction(self.BlowInFace) then return end
                   self:BlowInFace() 
            end 
        end)
   end

   

   self:SetDeploySpeed(self.DeploySpeed)
   self:SetDetTime(CurTime() + self.detonate_timer)
   self:SetThrowTime(0)
   self:SetPin(true)
   
   
   if CLIENT then
	self.ModelEntity = ClientsideModel(self.WorldModel)
	self.ModelEntity:SetNoDraw(true)
   end
end


function SWEP:Deploy()
 
   if self.SetHoldType then
      self:SetHoldType(self.HoldNormal)
   end

   self:SetThrowTime(0)
   self:SetPin(false)
   self:SetDetTime(CurTime() + self.detonate_timer)
   return false
end

function SWEP:OnRemove()
   if CLIENT and IsValid(self.Owner) and self.Owner == LocalPlayer() and self.Owner:Alive() then
      RunConsoleCommand("use", "weapon_ttt_unarmed")
   end
   
   if CLIENT then
   self.ModelEntity:Remove()
   end
end

function SWEP:PullPin()
   --if self:GetPin() then return end

   local ply = self.Owner
   if not IsValid(ply) then return end

   self.Weapon:SendWeaponAnim(ACT_VM_PULLPIN)

   if self.SetHoldType then
      self:SetHoldType(self.HoldReady)
   end

   self:SetPin(false)

  -- self:SetDetTime(CurTime() + self.detonate_timer)
end



function SWEP:Throw()

   timer.Destroy("explodeBaby")
   timer.Destroy("soundBabyec")

   if CLIENT then
      self:SetThrowTime(0)
   elseif SERVER then
      local ply = self.Owner
      if not IsValid(ply) then return end

     -- if self.was_thrown then return end

      self.was_thrown = true
      
      local ang = ply:EyeAngles()

      -- don't even know what this bit is for, but SDK has it
      -- probably to make it throw upward a bit
      if ang.p < 90 then
         ang.p = -10 + ang.p * ((90 + 10) / 90)
      else
         ang.p = 360 - ang.p
         ang.p = -10 + ang.p * -((90 + 10) / 90)
      end

      local vel = math.min(180, (90 - ang.p) * 6)

      local vfw = ang:Forward()
      local vrt = ang:Right()
      --      local vup = ang:Up()

      local src = ply:GetPos() + (ply:Crouching() and ply:GetViewOffsetDucked() or ply:GetViewOffset())
      src = src + (vfw * 120) + (vrt * 10)

      local thr = vfw * vel + ply:GetVelocity()

      self:CreateGrenade(src, Angle(0,0,0), thr, Vector(600, math.random(-1200, 1200), 0), ply)

      self:SetThrowTime(0)
      self:Remove()
   end
end

function SWEP:Think()
   local ply = self.Owner
   if not IsValid(ply) then return end

 if SERVER and self:GetDetTime() < CurTime() then
            self:BlowInFace()
           timer.Destroy("soundBabyec")
         end

   -- pin pulled and attack loose = throw
   --if self:GetPin() then
      -- we will throw now
      if  ply:KeyDown(IN_ATTACK) then
         self:StartThrow()

         self:SetPin(false)
         self.Weapon:SendWeaponAnim(ACT_VM_THROW)

         if SERVER then
            self.Owner:SetAnimation( PLAYER_ATTACK1 )
         end
      
   elseif self:GetThrowTime() > 0 and self:GetThrowTime() < CurTime() then
      self:Throw()
   end


end

function SWEP:BlowInFace()
   local ply = self.Owner
   if not IsValid(ply) then return end

   --if self.was_thrown then return end

   self.was_thrown = true

   -- drop the grenade so it can immediately explode

   local ang = ply:GetAngles()
   local src = ply:GetPos() + (ply:Crouching() and ply:GetViewOffsetDucked() or ply:GetViewOffset())
   src = src + (ang:Right() * 10)

   self:CreateGrenade(src, Angle(0,0,0), Vector(0,0,1), Vector(0,0,1), ply)

   self:SetThrowTime(0)
   self:Remove()
end

function SWEP:CreateGrenade(src, ang, vel, angimp, ply)
   local gren = ents.Create(self:GetGrenadeName())
   if not IsValid(gren) then return end

   gren:SetPos(src)
   gren:SetAngles(ang)

   --   gren:SetVelocity(vel)
   gren:SetOwner(ply)
   gren:SetThrower(ply)

   gren:SetGravity(0.2)
   gren:SetFriction(3)
   gren:SetElasticity(0.9)

   gren:Spawn()

   gren:PhysWake()

   local phys = gren:GetPhysicsObject()
   if IsValid(phys) then
      phys:SetVelocity(vel)
      phys:AddAngleVelocity(angimp)
   end

   -- This has to happen AFTER Spawn() calls gren's Initialize()
   gren:SetDetonateExact(self:GetDetTime())

   return gren
end