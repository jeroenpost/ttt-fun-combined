if SERVER then
   AddCSLuaFile( "shared.lua" )
   --resource.AddFile("materials/vgui/ttt_fun_killicons/m3.png")
end

SWEP.HoldType = "shotgun"
SWEP.PrintName = "HeadPopper"
SWEP.Slot = 2
if CLIENT then
   
   SWEP.Author = "GreenBlack"
   
   SWEP.Icon = "vgui/ttt_fun_killicons/m3.png"
end

SWEP.Base = "aa_base"
SWEP.Spawnable = true
SWEP.AdminSpawnable = true
SWEP.Kind = WEAPON_EQUIP
SWEP.WeaponID = AMMO_SHOTGUN

SWEP.Primary.Ammo = "Buckshot"
SWEP.Primary.Damage = 0
SWEP.Primary.Cone = 0.105
SWEP.Primary.Delay = 0.20
SWEP.Primary.ClipSize = 100
SWEP.Primary.ClipMax = 100
SWEP.Primary.DefaultClip = 100
SWEP.Primary.Automatic = true
SWEP.Primary.NumShots = 100
SWEP.HeadshotMultiplier = 2
SWEP.AutoSpawnable = true
SWEP.AmmoEnt = "item_box_buckshot_ttt"
SWEP.ViewModel = "models/weapons/v_shot_m3super90.mdl"
SWEP.WorldModel = "models/weapons/w_shot_m3super90.mdl"
SWEP.Primary.Sound = Sound( "Weapon_M3.Single" )
SWEP.Primary.Recoil = 0.1
SWEP.reloadtimer = 0

SWEP.IronSightsPos = Vector(5.74, -2.19, 3.28)
SWEP.IronSightsAng = Vector(0.547, 0, 0)

SWEP.HoldType = "pistol"
SWEP.ViewModelFOV = 70
SWEP.ViewModelFlip = false
SWEP.ViewModel = "models/weapons/v_stunstick.mdl"
SWEP.WorldModel = "models/weapons/w_stunbaton.mdl"
SWEP.ShowViewModel = true
SWEP.ShowWorldModel = true
SWEP.ViewModelBoneMods = {}
SWEP.Sounds = {
    "weapons/gb_weapons/partygun1.mp3"
}
SWEP.VElements = {
    ["element_name"] = { type = "Model", model = "models/combine_helicopter/helicopter_bomb01.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(5, 1.363, -15), angle = Angle(0, 0, 0), size = Vector(0.264, 0.264, 0.264), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
}
SWEP.WElements = {
    ["element_name"] = { type = "Model", model = "models/combine_helicopter/helicopter_bomb01.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(5.908, 2.273, -15), angle = Angle(0, 0, 0), size = Vector(0.209, 0.209, 0.209), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
}

function SWEP:PrimaryAttack(worldsnd)
    self.Weapon:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )

    self:EmitSound("weapons/gb_weapons/partygun1.mp3");
    self.Weapon:SendWeaponAnim( ACT_VM_HITCENTER )

        local tr = self.Owner:GetEyeTrace()
    if IsValid(tr.Entity) and (tr.Entity:IsPlayer() or tr.Entity:IsNPC()) then
        local edata = EffectData()
        edata:SetStart(self.Owner:GetShootPos())
        edata:SetOrigin(tr.HitPos)
        edata:SetNormal(tr.Normal)
        edata:SetEntity(tr.Entity)


            util.Effect("BloodImpact", edata)

            local boneid = tr.Entity:LookupBone("ValveBiped.Bip01_Head1")
            if !tr.Entity.biggerhead then tr.Entity.biggerhead = 1 end

            local multi = tr.Entity.biggerhead * 1
            tr.Entity.biggerhead = tr.Entity.biggerhead + 1
           -- print(multi)

            if boneid then
                tr.Entity:ManipulateBoneScale(boneid, Vector(1,1,1))
                tr.Entity:ManipulateBoneScale(boneid, Vector(tr.Entity.biggerhead,tr.Entity.biggerhead,tr.Entity.biggerhead))
            end

        if multi > 6 then
            tr.Entity.biggerhead = 1
            tr.Entity:ManipulateBoneScale(boneid, Vector(0.0001,0.0001,0.0001))
            if SERVER then
                local ent = ents.Create( "env_explosion" )
                if( IsValid( self.Owner ) ) then
                    ent:SetPos( tr.Entity:GetPos() )
                    ent:SetOwner( self.Owner )
                    ent:SetKeyValue( "iMagnitude", "75" )
                    ent:Spawn()
                    ent:Fire( "Explode", 0, 0 )
                    ent:EmitSound( "weapons/big_explosion.mp3", 511, 100 )
                end
            end
        end

    end


end