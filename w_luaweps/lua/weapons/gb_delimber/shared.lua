if( SERVER ) then
	AddCSLuaFile( "shared.lua" )


--resource.AddFile("models/morrowind/daedric/shortsword/w_daedric_shortsword.mdl")
--resource.AddFile("models/morrowind/daedric/shortsword/v_daedric_shortsword.mdl")
--resource.AddFile("materials/vgui/entities/weapon_mor_daedric_shortsword.vmt")
--resource.AddFile("materials/morrowind/daedric/shortsword/daedric6.vmt")
--resource.AddFile("materials/morrowind/daedric/shortsword/daedric4.vmt")
--resource.AddFile("materials/morrowind/daedric/shortsword/daedric3.vmt")
--resource.AddFile("materials/morrowind/daedric/shortsword/daedric2.vmt")
--resource.AddFile("materials/morrowind/daedric/shortsword/daedric5.vmt")
--resource.AddFile("materials/morrowind/daedric/shortsword/daedric1.vmt")
--resource.AddFile("sound/weapons/shortsword/morrowind_shortsword_deploy1.mp3")
--resource.AddFile("sound/weapons/shortsword/morrowind_shortsword_hit.mp3")
--resource.AddFile("sound/weapons/shortsword/morrowind_shortsword_hitwall1.mp3")
--resource.AddFile("sound/weapons/shortsword/morrowind_shortsword_slash.mp3")


end

if( CLIENT ) then
	
	
	SWEP.SlotPos = 1
	SWEP.DrawAmmo = false
	SWEP.DrawCrosshair = false
end


SWEP.PrintName = "Delimber"
SWEP.Slot = 1
SWEP.Kind = WEAPON_MELEE
SWEP.Icon = "vgui/entities/weapon_mor_daedric_shortsword"

SWEP.Category = "Morrowind Shortswords"
SWEP.Author			= "Doug Tyrrell + Mad Cow (Revisions by Neotanks) for LUA (Models, Textures, ect. By: Hellsing/JJSteel)"
SWEP.Base			= "weapon_mor_base_shortsword"
SWEP.Instructions	= "Left click to stab/slash/lunge and right click to throw, also capable of badassery."
SWEP.Contact		= ""
SWEP.Purpose		= ""

SWEP.ViewModelFOV	= 72
SWEP.ViewModelFlip	= false

SWEP.Spawnable			= true
SWEP.AdminSpawnable		= true
  
SWEP.ViewModel      = "models/morrowind/daedric/shortsword/v_daedric_shortsword.mdl"
SWEP.WorldModel   = "models/morrowind/daedric/shortsword/w_daedric_shortsword.mdl"

SWEP.Primary.Damage		= 25
SWEP.Primary.NumShots		= 0
SWEP.Primary.Delay 		= 0.4

SWEP.Primary.ClipSize		= -1					// Size of a clip
SWEP.Primary.DefaultClip	= -1					// Default number of bullets in a clip
SWEP.Primary.Automatic		= true				// Automatic/Semi Auto
SWEP.Primary.Ammo			= "none"

SWEP.Secondary.ClipSize		= -1					// Size of a clip
SWEP.Secondary.DefaultClip	= -1					// Default number of bullets in a clip
SWEP.Secondary.Automatic	= false				// Automatic/Semi Auto
SWEP.Secondary.Ammo		= "none"

SWEP.bones = { "ValveBiped.Bip01_Head1", "ValveBiped.Bip01_R_Hand", "ValveBiped.Bip01_R_Forearm", "ValveBiped.Bip01_R_Foot", "ValveBiped.Bip01_R_Thigh", "ValveBiped.Bip01_R_Calf",  "ValveBiped.Bip01_R_Elbow", "ValveBiped.Bip01_R_Shoulder"
                                       , "ValveBiped.Bip01_L_Hand", "ValveBiped.Bip01_L_Forearm", "ValveBiped.Bip01_L_Foot", "ValveBiped.Bip01_L_Thigh", "ValveBiped.Bip01_L_Calf",  "ValveBiped.Bip01_L_Elbow", "ValveBiped.Bip01_L_Shoulder" }


function SWEP:PrimaryAttack()
    self:SetNextPrimaryFire(CurTime() + self.Primary.Delay)
    self:SetNextSecondaryFire(CurTime() + self.Primary.Delay)
    self.Owner:SetAnimation( PLAYER_ATTACK1 )
    self.Weapon:SendWeaponAnim( ACT_VM_HITCENTER )
    timer.Simple(.05, function()
        if !IsValid(self) or !IsValid(self.Owner) then return end
        local trace = self.Owner:GetEyeTrace()
        if trace.HitPos:Distance(self.Owner:GetShootPos()) <= 75 then
            if( trace.Entity:IsPlayer() or trace.Entity:IsNPC() or trace.Entity:GetClass()=="prop_ragdoll" ) then
                self.Owner:EmitSound( self.FleshHit[math.random(1,#self.FleshHit)] )
                timer.Simple(0.01,function()

                    local bone = table.Random(self.bones)
                    bone = trace.Entity:LookupBone( bone )
                    if bone then
                        self:detachLimb(trace.Entity,bone)
                    end

                end)
            else
                self.Owner:EmitSound( self.Hit[math.random(1,#self.Hit)] )
            end


            local edata = EffectData()
            edata:SetStart(self.Owner:GetShootPos())
            edata:SetOrigin(trace.HitPos)
            edata:SetNormal(trace.Normal)
            edata:SetEntity(trace.Entity)


            util.Effect("BloodImpact", edata)


            local bullet = {}
            bullet.Num    = 1
            bullet.Src    = self.Owner:GetShootPos()
            bullet.Dir    = self.Owner:GetAimVector()
            bullet.Spread = Vector(0, 0, 0)
            bullet.Tracer = 0
            bullet.Force  = 1
            bullet.Damage = 15
            self.Owner:FireBullets(bullet)
            self.Owner:ViewPunch(Angle(7, 0, 0))
        else
            self.Weapon:EmitSound("weapons/shortsword/morrowind_shortsword_slash.mp3")
        end
    end)
end

function SWEP:detachLimb( Ply, Normal )

    Ply:ManipulateBoneScale(Normal, vector_origin)

    local ED = EffectData()
    ED:SetEntity( Ply ) --Player Entity
    ED:SetNormal( self.Owner:GetForward() )
    ED:SetScale( Normal )
    ED:SetOrigin( Ply:GetPos() )
    util.Effect( 'limbremove', ED ) --Effect Gib

end