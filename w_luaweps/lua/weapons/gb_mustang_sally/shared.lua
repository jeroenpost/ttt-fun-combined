if SERVER then
   AddCSLuaFile( "shared.lua" )
   --resource.AddFile("materials/vgui/ttt_fun_killicons/elites.png")
end
   
SWEP.HoldType = "duel"
   
 SWEP.PrintName = "Mustang and Sally"
SWEP.Slot = 7
if CLIENT then
   
   
   SWEP.Author = "GreenBlack"
 
   SWEP.Icon = "vgui/ttt_fun_killicons/elites.png"
end
 
SWEP.Base = "aa_base"

SWEP.EquipMenuData = {
    type  = "item_weapon",
    name  = "Mustang and Sally",
    desc  = "Explosive Greatness"
}
SWEP.CanBuy = {ROLE_TRAITOR,ROLE_DETECTIVE }

SWEP.Kind = WEAPON_EQUIP2
SWEP.Primary.Recoil     = 1.5
SWEP.Primary.Damage = 65
SWEP.Primary.Delay = 0.40
SWEP.Primary.Cone = 0.0002
SWEP.Primary.ClipSize = 20
SWEP.Primary.Automatic = true
SWEP.Primary.DefaultClip = 20
SWEP.Primary.ClipMax = 60
SWEP.Primary.Ammo = "Pistol"

SWEP.Secondary.Delay = 0.3
SWEP.Secondary.ClipSize     = 20
SWEP.Secondary.DefaultClip  = 20
SWEP.Secondary.Automatic    = true
SWEP.Secondary.Ammo         = "Pistol"
SWEP.Secondary.ClipMax      = 60

SWEP.AutoSpawnable = true
SWEP.AmmoEnt = "item_ammo_pistol_ttt"
SWEP.NoSights = true

SWEP.ViewModel  =  "models/weapons/cstrike/c_pist_elite.mdl"
SWEP.WorldModel = "models/weapons/w_pist_elite.mdl"
SWEP.ViewModelFlip = false
SWEP.ViewModelFOV  = 54
SWEP.UseHands = true
 
SWEP.Primary.Sound = Sound( "weapons/rpg/rocketfire1.wav" )
SWEP.Secondary.Sound = Sound( "weapons/rpg/rocket1.wav" )


function SWEP:PrimaryAttack( worldsnd )
 
   --self.Weapon:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
   self.Weapon:SetNextPrimaryFire( CurTime() + 1.2 )

   if not self:CanPrimaryAttack() then return end

   if not worldsnd then
      self.Weapon:EmitSound( self.Primary.Sound, self.Primary.SoundLevel )
   elseif SERVER then
      sound.Play(self.Primary.Sound, self:GetPos(), self.Primary.SoundLevel)
   end

   self:ShootBullet( self.Primary.Damage, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )


   self:TakePrimaryAmmo( 1 )
   --self:ShootFrag()

   local owner = self.Owner
   if not IsValid(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end
   self.Weapon:SendWeaponAnim(ACT_VM_PRIMARYATTACK)
 --  owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )
end

function SWEP:CanSecondaryAttack()
   if not IsValid(self.Owner) then return end

   if self:Clip1() < 1  then

      return false
   end
   return true
end




function SWEP:SecondaryAttack()
    local bucket, att, phys, tr
    if not self:CanSecondaryAttack() then return end

    self.Weapon:SetNextSecondaryFire(CurTime() + 1.2)
    self.Weapon:EmitSound( self.Primary.Sound, self.Primary.SoundLevel )

    if CLIENT then
        return
    end
    self:TakePrimaryAmmo( 1 )
    self:ShootTracer("AirboatGunTracer")
    self:ShootBullet( self.Primary.Damage, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )

    --  self:ShootFrag()

    self.Owner:ViewPunch( Angle( math.Rand(-8,8), math.Rand(-8,8), 0 ) )
    self.Weapon:SendWeaponAnim(ACT_VM_PRIMARYATTACK)
end

-- COLOR
function SWEP:Deploy()
    self.Weapon:SendWeaponAnim(ACT_VM_DRAW)
    self:SetColorAndMaterial(Color(255,255,255,255), "models/grisps/chips");
    return true
end


function SWEP:PreDrop()
    self:ColorReset()
end


function SWEP:Holster()
    self:ColorReset()
    return true
end

function SWEP:OnRemove()
    self:ColorReset()
end
function SWEP:OnDrop()
    self:ColorReset()

end

function SWEP:PreDrawViewModel()

    if SERVER or not IsValid(self.Owner) or not IsValid(self.Owner:GetViewModel()) then return end
    self.Owner:GetViewModel():SetColor(Color(255,255,255,255))
    self.Owner:GetViewModel():SetMaterial( "models/grisps/chips"   )

end