SWEP.Base = "aa_base"
SWEP.HoldType = "slam"
SWEP.PrintName = "Watch"

SWEP.ViewModel				= "models/weapons/v_watch.mdl"
SWEP.WorldModel				= "models/weapons/w_watch.mdl"
SWEP.AutoSpawnable = true
SWEP.Slot = 7

if CLIENT then

    SWEP.Author = "GreenBlack"

    SWEP.Icon = "vgui/ttt_fun_killicons/gb_watch.png"
end

SWEP.CanBuy = { ROLE_TRAITOR, ROLE_DETECTIVE }
SWEP.EquipMenuData = {
    type = "Weapon",
    desc = "Wanna know the time?\nToo bad, this only shoots powerful bullets."
};

SWEP.Kind = WEAPON_EQUIP2

SWEP.Primary.Delay          = 2.5
SWEP.Primary.Recoil         = 7
SWEP.Primary.Automatic = true
SWEP.Primary.Ammo = "357"
SWEP.Primary.Damage = 75
SWEP.Primary.Cone = 0.005
SWEP.Primary.ClipSize = 1
SWEP.Primary.ClipMax = 1 -- keep mirrored to ammo
SWEP.Primary.DefaultClip = 20

SWEP.HeadshotMultiplier = 2

SWEP.AutoSpawnable      = true
SWEP.AmmoEnt = "item_ammo_357_ttt"

SWEP.UseHands			= true
SWEP.ViewModelFlip		= false
SWEP.ViewModelFOV		= 75

--SWEP.Primary.Sound = Sound("watch.single")

SWEP.Secondary.Sound = Sound("Default.Zoom")

SWEP.IronSightsPos      = Vector( 5, -15, -2 )
SWEP.IronSightsAng      = Vector( 2.6, 1.37, 3.5 )

