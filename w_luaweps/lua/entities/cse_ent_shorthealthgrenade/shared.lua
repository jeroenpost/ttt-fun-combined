// raped version of flashbang.

//ENT.Base = "base_entity"
ENT.Type = "anim"

ENT.PrintName		= "gasgrenade"
ENT.Author			= "HLTV Proxy"
ENT.Contact			= ""
ENT.Purpose			= nil
ENT.Instructions	= nil


/*---------------------------------------------------------
   Name: OnRemove
   Desc: Called just before entity is deleted
---------------------------------------------------------*/
function ENT:OnRemove()
end

function ENT:PhysicsUpdate()
end

function ENT:PhysicsCollide(data,phys)

    if self.explodeonimpact then
        for k, v in pairs (ents.FindInSphere(self.Entity:GetPos(), 250)) do
            if v:IsPlayer() then
                v:SetHealth(v:Health()+self.explodeonimpact)
                gb_check_max_health(v)
            end
        end
        self:NextThink(CurTime())
        self.timer = CurTime() -1
        return
    end

	if data.Speed > 50 then
		self.Entity:EmitSound(Sound("Flashbang.Bounce"))
	end
	
	local impulse = -data.Speed * data.HitNormal * .4 + (data.OurOldVelocity * -.6)
	phys:ApplyForceCenter(impulse)
end
