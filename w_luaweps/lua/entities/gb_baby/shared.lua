if SERVER then
   AddCSLuaFile("shared.lua")
end

ENT.Type = "anim"
ENT.Base = "ttt_basegrenade_proj"
ENT.Model = Model("models/props_c17/doll01.mdl")


AccessorFunc( ENT, "radius", "Radius", FORCE_NUMBER )
AccessorFunc( ENT, "dmg", "Dmg", FORCE_NUMBER )

function ENT:Initialize()
   self:SetRadius(850)
   self:SetDmg(300)

   local soundfile = gb_config.websounds.."weapons/gb_crybaby.mp3"
   gb_PlaySoundFromServer(soundfile, self)

   timer.Create( "soundBabyecT", 4, 0, function()
	     if self:IsValid() then

             local soundfile = gb_config.websounds.."weapons/gb_crybaby.mp3"
             gb_PlaySoundFromServer(soundfile, self)

         end
		
     end)
   return self.BaseClass.Initialize(self)
end

function ENT:Explode(tr)
   
   if SERVER then
      self.Entity:SetNoDraw(true)
      self.Entity:SetSolid(SOLID_NONE)

      -- pull out of the surface
      if tr.Fraction != 1.0 then
         self.Entity:SetPos(tr.HitPos + tr.HitNormal * 1.6)
      end

      local pos = self.Entity:GetPos()


      local effect = EffectData()
      effect:SetStart(pos)
      effect:SetOrigin(pos)
      effect:SetScale(self:GetRadius() * 3.3)
      effect:SetRadius(self:GetRadius())
      effect:SetMagnitude(self.dmg)

      if tr.Fraction != 1.0 then
         effect:SetNormal(tr.HitNormal)
      end

      util.Effect("Explosion", effect, true, true)

      util.BlastDamage(self, self:GetThrower(), pos, self:GetRadius(), self:GetDmg())
      self:EmitSound("weapons/big_explosion.mp3")
      timer.Destroy("soundBabyec2T")
      StartFires(pos, tr, 10, 3, false, self:GetThrower())

      self:SetDetonateExact(0)

      self:Remove()
   else
      local spos = self.Entity:GetPos()
      local trs = util.TraceLine({start=spos + Vector(0,0,64), endpos=spos + Vector(0,0,-128), filter=self})
      util.Decal("Scorch", trs.HitPos + trs.HitNormal, trs.HitPos - trs.HitNormal)      

      self:SetDetonateExact(0)
   end
end

