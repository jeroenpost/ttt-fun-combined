ENT.Type = "anim"

if CLIENT then
    function ENT:Draw()
        self.Entity:DrawModel()
    end

    function ENT:IsTranslucent()
        return true
    end
end

function ENT:OnRemove()
end

function ENT:PhysicsUpdate()
end

function ENT:PhysicsCollide(data,phys)
	if data.Speed > 50 then
		self.Entity:EmitSound(Sound("weapons/bugbait/bugbait_impact3.wav"))
	end
	
	local impulse = -data.Speed * data.HitNormal * .4 + (data.OurOldVelocity * -.6)
	phys:ApplyForceCenter(impulse)
end
