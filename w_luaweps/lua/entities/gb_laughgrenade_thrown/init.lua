
AddCSLuaFile( "shared.lua" )

include('shared.lua')

function ENT:Initialize()

	self.Entity:SetModel("models/weapons/w_grenade.mdl")
	self.Entity:PhysicsInit( SOLID_VPHYSICS )
	self.Entity:SetMoveType( MOVETYPE_VPHYSICS )
	self.Entity:SetSolid( SOLID_VPHYSICS )
	self.Entity:DrawShadow( false )

	self.Entity:SetCollisionGroup( COLLISION_GROUP_WEAPON )
	
	local phys = self.Entity:GetPhysicsObject()
	
	if (phys:IsValid()) then
		phys:Sleep()
    end

	self.timer = CurTime() + 14
	self.solidify = CurTime() + 4
	self.Bastardgas = nil
	self.Spammed = false
end

function ENT:Think()
	if (IsValid(self.Owner)==false) then
		self.Entity:Remove()
	end
	if (self.solidify<CurTime()) then
		self.SetOwner(self.Entity)
	end
    if not self.soundplaying and self.solidify<CurTime() then
        self.soundplaying = true
        local soundfile = gb_config.websounds.."nownownow.mp3"
        gb_PlaySoundFromServer(soundfile,self, true)
    end

		local pos = self.Entity:GetPos()
		local maxrange = 200
		local maxstun = 3
		for k,v in pairs(player.GetAll()) do
			local plpos = v:GetPos()
			local dist = -pos:Distance(plpos)+maxrange
			if (pos:Distance(plpos)<=maxrange) then
				local trace = {}
					trace.start = self.Entity:GetPos()
					trace.endpos = v:GetPos()+Vector(0,0,24)
					trace.filter = { v, self.Entity }
					trace.mask = COLLISION_GROUP_PLAYER
				tr = util.TraceLine(trace)
				if (tr.Fraction==1) then
					local stunamount = math.ceil(dist/(maxrange/maxstun))
					 v:ViewPunch( Angle( stunamount*((math.random()*6)-2), stunamount*((math.random()*6)-2), stunamount*((math.random()*4)-1) ) )
                    if not v.laughingg then
                         v:AnimPerformGesture_gb( ACT_GMOD_TAUNT_LAUGH);
                         v:EmitSound("weapons/gb_weapons/laughtergun1.mp3",300,100);
                         v.laughingg = true
                       timer.Simple(5,function() if IsValid(v) then  v.laughingg = false end end)
                    end


				end
			end
		end
		if (self.timer+5<CurTime()) then
			if IsValid(self.Bastardgas) then
				self.Bastardgas:Remove()
                self:ExplodeHard()
			end
		end
		if (self.timer+8<CurTime()) then

            self:ExplodeHard()
		end
		self.Entity:NextThink(CurTime()+0.2)
		return true

end

function ENT:ExplodeHard()

    if not IsValid( self.Owner ) then return end

    local ent = ents.Create( "env_explosion" )

    ent:SetPos( self:GetPos()  )
        ent:SetPhysicsAttacker(  self.Owner )

    ent:Spawn()
    ent:SetKeyValue( "iMagnitude", "50" )
    ent:Fire( "Explode", 0, 0 )

    util.BlastDamage( self,  self.Owner, self:GetPos(), 100, 50 )
    ent:EmitSound( "weapons/big_explosion.mp3" )
    self:Remove()
end
