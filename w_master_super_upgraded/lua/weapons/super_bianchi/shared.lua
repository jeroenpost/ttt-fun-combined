SWEP.Base = "weapon_bianchi"
SWEP.Icon = "vgui/ttt_fun_killicons/bianchi.png"
SWEP.Kind = WEAPON_HEAVY
SWEP.Spawnable			= true
SWEP.AdminSpawnable		= true
SWEP.AutoSpawnable      = false
   SWEP.Slot				= 2
   SWEP.SlotPos			= 0
   SWEP.PrintName = "Super Bianchi FA-6";  
SWEP.AmmoEnt = "item_ammo_smg1_ttt"
SWEP.HoldType = "ar2"

SWEP.ShowViewModel = true
SWEP.ShowWorldModel = true
SWEP.ViewModelBoneMods = {}

SWEP.Primary.Sound 		= Sound("weapons/bianachi/bian-2.mp3")
SWEP.Primary.Recoil		= .2
SWEP.Primary.Damage		= 10
SWEP.Primary.NumShots   = 1
SWEP.Primary.Cone	    = 0.03
SWEP.Primary.Delay 		= .070

SWEP.Primary.ClipSize		= 75					// Size of a clip
SWEP.Primary.DefaultClip	= 75					// Default number of bullets in a clip
SWEP.Primary.ClipMax 		= 120
SWEP.Primary.Automatic		= true				// Automatic/Semi Auto
SWEP.Primary.Ammo			= "smg1"

SWEP.Secondary.ClipSize		= -1					// Size of a clip
SWEP.Secondary.DefaultClip	= -1					// Default number of bullets in a clip
SWEP.Secondary.ClipMax 		= -1
SWEP.Secondary.Automatic	= false				// Automatic/Semi Auto
SWEP.Secondary.Ammo		= "none"

SWEP.IronSightsPos = Vector(-4.56, -5.5, 2.24) 
SWEP.IronSightsAng = Vector(0, 0, 0)
SWEP.RunArmOffset  = Vector(0.381, -1.566, 2.055)
SWEP.RunArmAngle   = Vector(-16.241, 39.08, 0)