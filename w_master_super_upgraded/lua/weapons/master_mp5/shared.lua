if SERVER then
   AddCSLuaFile( "shared.lua" )
   --resource.AddFile("materials/vgui/ttt_fun_killicons/mp5.png")
end

SWEP.HoldType = "ar2"
SWEP.PrintName = "Master MP5"	
SWEP.Slot = 2
if CLIENT then
   		
   SWEP.Author = "GreenBlack"
   
   SWEP.Icon = "vgui/ttt_fun_killicons/mp5.png"
end

SWEP.Base = "weapon_tttbase"
SWEP.Spawnable = true
SWEP.AdminSpawnable = false
SWEP.Kind = WEAPON_HEAVY

SWEP.Primary.Delay = 0.07
SWEP.Primary.Recoil = 0.1
SWEP.Primary.Automatic = true
SWEP.Primary.Ammo = "pistol"
SWEP.Primary.Damage = 6
SWEP.Primary.Cone = 0.028
SWEP.Primary.ClipSize = 65
SWEP.Primary.ClipMax = 160
SWEP.Primary.DefaultClip = 65
SWEP.AutoSpawnable = false
SWEP.AmmoEnt = "item_ammo_pistol_ttt"
SWEP.ViewModel = "models/weapons/v_smg_mp5.mdl"
SWEP.WorldModel = "models/weapons/w_smg_mp5.mdl"
SWEP.Primary.Sound = Sound("Weapon_MP5Navy.Single")

SWEP.HeadshotMultiplier = 2

SWEP.IronSightsPos = Vector(4.7659, -3.0823, 1.8818)
SWEP.IronSightsAng = Vector(0.9641, 0.0252, 0)