if SERVER then
   AddCSLuaFile( "shared.lua" )
	--resource.AddFile("materials/vgui/ttt_fun_killicons/galil.png")
end

SWEP.HoldType = "ar2"

SWEP.PrintName = "Master Galil"
SWEP.Slot = 2

if CLIENT then
   
   SWEP.Author = "GreenBlack"
   
   SWEP.Icon = "vgui/ttt_fun_killicons/galil.png"
   SWEP.ViewModelFlip = false
end

SWEP.Base = "weapon_tttbase"
SWEP.Spawnable = true
SWEP.AdminSpawnable = true
SWEP.Kind = WEAPON_HEAVY
SWEP.Primary.Damage = 16
SWEP.Primary.Delay = 0.12
SWEP.Primary.Cone = 0.00
SWEP.Primary.ClipSize = 85
SWEP.Primary.ClipMax = 105
SWEP.Primary.DefaultClip	= 85
SWEP.Primary.Automatic = true
SWEP.Primary.Ammo = "smg1"
SWEP.AmmoEnt = "item_ammo_smg1_ttt"
SWEP.AutoSpawnable = false
SWEP.Primary.Recoil = 1.2
SWEP.Primary.Sound = Sound("weapons/galil/galil-1.wav")
SWEP.ViewModel = "models/weapons/v_rif_galil.mdl"
SWEP.WorldModel = "models/weapons/w_rif_galil.mdl"

SWEP.HeadshotMultiplier = 2

SWEP.IronSightsPos = Vector( -5.13, -4.38, 2.10 )
SWEP.IronSightsAng = Vector( 0.55, 0.00, -1.09 )