    if SERVER then
       AddCSLuaFile( "shared.lua" )
       --resource.AddFile("materials/vgui/ttt_fun_killicons/partygun.png");
       --resource.AddFile("sound/weapons/gb_weapons/partygun1.mp3");
    end

SWEP.PrintName                       = "Master Party Gun"
       SWEP.Slot                            = 6
     
    if CLIENT then
       
     

       SWEP.Icon = "vgui/ttt_fun_killicons/partygun.png"
    end

SWEP.VElements = {
	["rainbow2"] = { type = "Sprite", sprite = "trails/rainbow", bone = "Base", rel = "", pos = Vector(3.181, 5.908, 12.272), size = { x = 4.523, y = 4.523 }, color = Color(255, 255, 255, 255), nocull = true, additive = true, vertexalpha = true, vertexcolor = true, ignorez = false},
	["rainbow"] = { type = "Sprite", sprite = "trails/happy", bone = "Base", rel = "", pos = Vector(2.273, -2.274, 4.091), size = { x = 1.514, y = 1.514 }, color = Color(255, 255, 255, 255), nocull = true, additive = true, vertexalpha = true, vertexcolor = true, ignorez = false}
}

SWEP.HoldType = "pistol"
SWEP.ViewModelFOV = 65
SWEP.ViewModelFlip = false
SWEP.ViewModel = "models/weapons/v_irifle.mdl"
SWEP.WorldModel = "models/weapons/w_irifle.mdl"
SWEP.ShowViewModel = true
SWEP.ShowWorldModel = true
SWEP.ViewModelBoneMods = {}
     
    SWEP.Base = "gb_base"
    SWEP.Kind = WEAPON_EQUIP
  
     
	
    SWEP.Primary.ClipSize = 1
    SWEP.Primary.DefaultClip = 1
    SWEP.Primary.Ammo = "Battery"
    SWEP.Primary.Automatic = false
     
    SWEP.Secondary.ClipSize = -1
    SWEP.Secondary.DefaultClip = -1
    SWEP.Secondary.Ammo = "none"
    SWEP.Secondary.Automatic = true
     
    SWEP.NoSights = true
    SWEP.AllowDrop = true
    SWEP.LimitedStock = true
    SWEP.AutoSpawnable = false
     
    SWEP.StunTime = 8 -- How long is the victim ragdolled?
     
    SWEP.SlowTime = 5 -- How long is the victim "exhausted"?
    SWEP.SlowPercent = 0.33 -- How much they are slowed?
     
    SWEP.Delay = 5
    SWEP.Range = 1024
     
    SWEP.Sounds = {
            "weapons/gb_weapons/partygun1.mp3"
    }
     
    function SWEP:Precache()
            for _, v in pairs (self.Sounds) do
                    util.PrecacheSound(v)
            end
    end
     
    function SWEP:Deploy()
            self:SendWeaponAnim(ACT_VM_DRAW)
            self:SetNextPrimaryFire(CurTime() + 1)
    end
     
    function SWEP:PrimaryAttack()
            if (self:Clip1() <= 0) then return end
           
            self:TakePrimaryAmmo(1)
     
            self:SendWeaponAnim(ACT_VM_PRIMARYATTACK)
            self.Owner:SetAnimation(PLAYER_ATTACK1)
           
            self:SetNextPrimaryFire(CurTime() + self.Delay)
     
            if (IsFirstTimePredicted()) then
                    local snd, i = table.Random(self.Sounds)
                    self:EmitSound("weapons/gb_weapons/partygun1.mp3");
            end
           


            local soundfile = gb_config.websounds.."birdistheword.mp3"
            gb_PlaySoundFromServer(soundfile,self.Owner, true)
            
            danceMoves = {ACT_GMOD_TAUNT_ROBOT,ACT_GMOD_TAUNT_DANCE,ACT_GMOD_TAUNT_MUSCLE}
               
                    for k,victim in pairs(player.GetAll()) do
                            danceMove = table.Random(danceMoves)
                            if (!victim:IsPlayer()) then return end                                
                           
                                victim:AnimPerformGesture_gb(danceMove);
                                if (SERVER) then                                    

                                    timer.Simple(15,function()
                                       --Start the dance again
                                       if IsValid(victim) and IsValid(self) and IsValid(self.Owner) and danceMove  then    
                                            victim:AnimPerformGesture_gb(danceMove);
                                        end
                                    end)

                                end

                    end
            
    end

    SWEP.lastReload = 0
     
    function SWEP:SecondaryAttack()
            if (SERVER) and (self.lastReload < (CurTime() - 5)) then    
                   self.lastReload = CurTime()
                   self:EmitSound("weapons/gb_weapons/partygun1.mp3");
            end
    end
     
    hook.Add("CanPlayerSuicide", "CantSuicideIfRagdolled", function(ply)
            return ply.Ragdolled == nil
    end)
