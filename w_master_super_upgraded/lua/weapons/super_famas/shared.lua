if SERVER then
   AddCSLuaFile( "shared.lua" )
end

SWEP.HoldType = "ar2"
   SWEP.Slot = 2
SWEP.PrintName = "Super Famas"

if CLIENT then
   
   SWEP.Author = "GreenBlack"

   SWEP.Icon = "vgui/ttt_fun_killicons/famas.png"
   SWEP.ViewModelFlip = false
end

SWEP.Base = "weapon_tttbase"
SWEP.Kind = WEAPON_HEAVY

SWEP.Primary.Damage = 13
SWEP.Primary.Delay = 0.07
SWEP.Primary.Cone = 0.03
SWEP.Primary.ClipSize = 30
SWEP.Primary.ClipMax = 90
SWEP.Primary.DefaultClip = 30
SWEP.Primary.Automatic = true
SWEP.Primary.Ammo = "smg1"
SWEP.Primary.Recoil = 0.8
SWEP.Primary.Sound = Sound( "weapons/famas/famas-1.wav" )
SWEP.AutoSpawnable = false
SWEP.AmmoEnt = "item_ammo_smg1_ttt"

SWEP.ViewModel = "models/weapons/v_rif_famas.mdl"
SWEP.WorldModel = "models/weapons/w_rif_famas.mdl"

SWEP.IronSightsPos = Vector( -4.65, -3.28, 0.01 )
SWEP.IronSightsAng = Vector( 1.09, 0, -2.19 )

function SWEP:GetHeadshotMultiplier(victim, dmginfo)
   local att = dmginfo:GetAttacker()
   if not IsValid(att) then return 2 end

   local dist = victim:GetPos():Distance(att:GetPos())
   local d = math.max(0, dist - 150)
 
   return 1.5 + math.max(0, (1.5 - 0.002 * (d ^ 1.25)))
end