
if SERVER then
   AddCSLuaFile( "shared.lua" )
   --resource.AddFile("materials/vgui/ttt_fun_killicons/glock18.png")
end

SWEP.HoldType = "pistol"

  SWEP.PrintName = "Super Glock18"
   SWEP.Slot = 1
if CLIENT then
 

   SWEP.Icon = "vgui/ttt_fun_killicons/glock18.png"
end

SWEP.Kind = WEAPON_PISTOL
SWEP.WeaponID = AMMO_GLOCK

SWEP.Base = "weapon_tttbase"
SWEP.Primary.Recoil	= 0.5
SWEP.Primary.Damage = 12
SWEP.Primary.Delay = 0.07
SWEP.Primary.Cone = 0.028
SWEP.Primary.ClipSize = 40
SWEP.Primary.Automatic = true
SWEP.Primary.DefaultClip = 40
SWEP.Primary.ClipMax = 120
SWEP.Primary.Ammo = "Pistol"
SWEP.AutoSpawnable = false
SWEP.AmmoEnt = "item_ammo_pistol_ttt"

SWEP.UseHands			= true
SWEP.ViewModelFlip		= false
SWEP.ViewModelFOV		= 55
SWEP.ViewModel  = "models/weapons/cstrike/c_pist_glock18.mdl"
SWEP.WorldModel = "models/weapons/w_pist_glock18.mdl"

SWEP.Primary.Sound = Sound( "Weapon_Glock.Single" )
SWEP.IronSightsPos = Vector( 4.33, -3.7982, 2.8289 )

SWEP.HeadshotMultiplier = 1.75
