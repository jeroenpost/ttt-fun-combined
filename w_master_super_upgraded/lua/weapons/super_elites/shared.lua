if SERVER then
   AddCSLuaFile( "shared.lua" )
   --resource.AddFile("materials/vgui/ttt_fun_killicons/elites.png")
end
   
SWEP.HoldType = "duel"
      SWEP.PrintName = "Super Elites"
   SWEP.Slot = 1
 
if CLIENT then

   SWEP.Author = "GreenBlack"
 
   SWEP.Icon = "vgui/ttt_fun_killicons/elites.png"
end
 
SWEP.Base = "gb_base"

SWEP.Kind = WEAPON_PISTOL 
SWEP.Base = "weapon_tttbase"
SWEP.Primary.Recoil     = 1
SWEP.Primary.Damage = 33
SWEP.Primary.Delay = 0.35
SWEP.Primary.Cone = 0.01
SWEP.Primary.ClipSize = 25
SWEP.Primary.Automatic = true
SWEP.Primary.DefaultClip = 25
SWEP.Primary.ClipMax = 70
SWEP.Primary.Ammo = "Pistol"

SWEP.Secondary.Delay = 0.35
SWEP.Secondary.ClipSize     = 25
SWEP.Secondary.DefaultClip  = 25
SWEP.Secondary.Automatic    = true
SWEP.Secondary.Ammo         = "Pistol"
SWEP.Secondary.ClipMax      = 70

SWEP.AutoSpawnable = false 
SWEP.AmmoEnt = "item_ammo_pistol_ttt"
SWEP.NoSights = true
 
SWEP.ViewModel  = "models/weapons/v_pist_elite.mdl"
SWEP.WorldModel = "models/weapons/w_pist_elite.mdl"
 
SWEP.Primary.Sound = Sound( "Weapon_Elite.Single" )
SWEP.Secondary.Sound = Sound( "Weapon_Elite.Single" )


function SWEP:Think()

	if self.Owner:KeyPressed(IN_ATTACK)  then
	-- When the left click is pressed, then
		self.ViewModelFlip = true
	end

	if self.Owner:KeyPressed(IN_ATTACK2)  then
	-- When the right click is pressed, then
		self.ViewModelFlip = false
	end
end

function SWEP:PrimaryAttack( worldsnd )
 
   --self.Weapon:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
   self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )

   if not self:CanPrimaryAttack() then return end

   if not worldsnd then
      self.Weapon:EmitSound( self.Primary.Sound, self.Primary.SoundLevel )
   elseif SERVER then
      sound.Play(self.Primary.Sound, self:GetPos(), self.Primary.SoundLevel)
   end

   self:ShootBullet( self.Primary.Damage, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )

   self:TakePrimaryAmmo( 1 )

   local owner = self.Owner
   if not IsValid(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

 --  owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )
end

function SWEP:CanSecondaryAttack()
   if not IsValid(self.Owner) then return end

   if self.Weapon:Clip1() <= 0 then
      self:DryFire(self.SetNextSecondaryFire)
      return false
   end
   return true
end

function SWEP:SecondaryAttack(worldsnd)
       

   self.Weapon:SetNextSecondaryFire( CurTime() + self.Secondary.Delay )
   --self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )

   if not self:CanSecondaryAttack() then return end

   if not worldsnd then
      self.Weapon:EmitSound( self.Primary.Sound, self.Primary.SoundLevel )
   elseif SERVER then
      sound.Play(self.Primary.Sound, self:GetPos(), self.Primary.SoundLevel)
   end

   self:ShootBullet( self.Primary.Damage, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )

   self:TakePrimaryAmmo( 1 )

   local owner = self.Owner
   if not IsValid(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

  -- owner:ViewPunch( Angle( math.Rand(-0.6,-0.6) * self.Primary.Recoil, math.Rand(-0.6,0.6) *self.Primary.Recoil, 0 ) )
              
end
