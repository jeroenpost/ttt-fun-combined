-- thrown knife

if SERVER then
   AddCSLuaFile("shared.lua")
else
   ENT.PrintName = "discombobulator_projectile"
   ENT.Icon = "VGUI/ttt/icon_recombobulator"
end


ENT.Type = "anim"
ENT.Model = Model("models/items/combine_rifle_ammo01.mdl")

function ENT:Initialize()
   self.Entity:SetModel(self.Model)

   self.Entity:PhysicsInit(SOLID_VPHYSICS)

   if SERVER then
      self:SetGravity(0.4)
      self:SetFriction(1.0)
      self:SetElasticity(0.45)

      self.StartPos = self:GetPos()

      self.Entity:NextThink(CurTime())
   end
end


if SERVER then
	function ENT:Explode(force2, physforce)
		self.Entity:EmitSound( Sound("npc/assassin/ball_zap1.wav") )
	   local radius = 500
	   local phys_force = physforce or 1500
	   local push_force = force2 or 512
	   local pos = self.Entity:GetPos()
	   -- pull physics objects and push players
	   for k, target in pairs(ents.FindInSphere(pos, radius)) do
		  if IsValid(target) then
			 local tpos = target:LocalToWorld(target:OBBCenter())
			 local dir = (tpos - pos):GetNormal()
			 local phys = target:GetPhysicsObject()

			 if target:IsPlayer() and (not target:IsFrozen()) and ((not target.was_pushed) or target.was_pushed.t != CurTime()) then

				-- always need an upwards push to prevent the ground's friction from
				-- stopping nearly all movement
				dir.z = math.abs(dir.z) + 1

				local push = dir * push_force
				local vel = target:GetVelocity() + push
				vel.z = math.min(vel.z, push_force)
				local pusher = self:GetOwner() or self.Owner or nil

				target:SetVelocity(vel)
				target:SetGroundEntity( nil )
				target.was_pushed = {att=pusher, t=CurTime()}

			 elseif IsValid(phys) then
				phys:ApplyForceCenter(dir * -1 * phys_force)
			 end
		  end
	   end

	   local phexp = ents.Create("env_physexplosion")
	   if IsValid(phexp) then
		  phexp:SetPos(pos)
		  phexp:SetKeyValue("magnitude", 100) --max
		  phexp:SetKeyValue("radius", radius)
		  -- 1 = no dmg, 2 = push ply, 4 = push radial, 8 = los, 16 = viewpunch
		  phexp:SetKeyValue("spawnflags", 1 + 2 + 16)
		  phexp:Spawn()
		  phexp:Fire("Explode", "", 0.2)
		end
	end
	
	
   function ENT:PhysicsCollide(data, phys)
	  self:Explode()
	  self.Entity:Remove()
	end
end
