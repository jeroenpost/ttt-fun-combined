
if SERVER then
   AddCSLuaFile( "shared.lua" )
--   resource.AddSingleFile( "materials/vgui/ttt/icon_recombobulator.vmt" )
--   resource.AddSingleFile( "materials/vgui/ttt/icon_recombobulator.vtf" )
end
    
SWEP.HoldType = "ar2"

if CLIENT then

   SWEP.PrintName    = "Recombobulator"
   SWEP.Slot         = 6
  
   SWEP.ViewModelFlip = false

   SWEP.EquipMenuData = {
      type = "item_weapon",
      desc = "Shoots an on-impact discombobulator."
   };

   SWEP.Icon = "VGUI/ttt/icon_recombobulator"
end

SWEP.Base               = "weapon_tttbase"

SWEP.ViewModel          = "models/weapons/v_rpg.mdl"
SWEP.WorldModel         = "models/weapons/w_rocket_launcher.mdl"
SWEP.ViewModelFlip = false
SWEP.ViewModelFOV = 60

SWEP.DrawCrosshair      = false
SWEP.Primary.Damage         = 0
SWEP.Primary.ClipSize       = 2
SWEP.Primary.DefaultClip    = 2
SWEP.Primary.Automatic      = false
SWEP.Primary.Delay = 3
SWEP.Primary.Ammo       = "AR2AltFire"

SWEP.Kind = WEAPON_EQUIP
SWEP.CanBuy = {ROLE_TRAITOR} -- only traitors can buy
SWEP.LimitedStock = true 


function SWEP:PrimaryAttack()
   if not self.Weapon:CanPrimaryAttack() then return end
   self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
   self.Weapon:EmitSound( Sound( "weapons/grenade_launcher1.wav" ) )
   
   if SERVER then
      self.Owner:SetAnimation( PLAYER_ATTACK1 )
	  self.Weapon:SendWeaponAnim( ACT_VM_PRIMARYATTACK )
      local ply = self.Owner
      if not IsValid(ply) then return end

      local ang = ply:EyeAngles()

      if ang.p < 90 then
         ang.p = -10 + ang.p * ((90 + 10) / 90)
      else
         ang.p = 360 - ang.p
         ang.p = -10 + ang.p * -((90 + 10) / 90)
      end

      local vel = math.Clamp((90 - ang.p) * 13, 1000, 1400)

      local vfw = ang:Forward()
      local vrt = ang:Right()
      
      local src = ply:GetPos() + (ply:Crouching() and ply:GetViewOffsetDucked() or ply:GetViewOffset())

      src = src + (vfw * 1) + (vrt * 3)

      local thr = vfw * vel + ply:GetVelocity()

      local rc_ang = Angle(-10,0,0) + ang
      rc_ang:RotateAroundAxis(rc_ang:Right(), -90)

      local recombob = ents.Create("ttt_recombobulator_proj")
      if not IsValid(recombob) then return end
      recombob:SetPos(src)
      recombob:SetAngles(rc_ang)

      recombob:Spawn()

      recombob.Damage = self.Primary.Damage

      recombob:SetOwner(ply)

      local phys = recombob:GetPhysicsObject()
      if IsValid(phys) then
         phys:SetVelocity(thr)
         phys:Wake()
      end
		
	  self:TakePrimaryAmmo( 1 )
	  self:Reload()
   end
end

function SWEP:Equip()
   self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
end

function SWEP:OnRemove()
   if CLIENT and IsValid(self.Owner) and self.Owner == LocalPlayer() and self.Owner:Alive() then
      RunConsoleCommand("lastinv")
   end
end

