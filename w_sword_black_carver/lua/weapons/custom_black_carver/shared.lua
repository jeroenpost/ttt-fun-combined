if( SERVER ) then
	AddCSLuaFile( "shared.lua" )


--resource.AddFile("models/morrowind/daedric/shortsword/w_daedric_shortsword.mdl")
--resource.AddFile("models/morrowind/daedric/shortsword/v_daedric_shortsword.mdl")
--resource.AddFile("materials/vgui/entities/weapon_mor_daedric_shortsword.vmt")
--resource.AddFile("materials/morrowind/daedric/shortsword/daedric6.vmt")
--resource.AddFile("materials/morrowind/daedric/shortsword/daedric4.vmt")
--resource.AddFile("materials/morrowind/daedric/shortsword/daedric3.vmt")
--resource.AddFile("materials/morrowind/daedric/shortsword/daedric2.vmt")
--resource.AddFile("materials/morrowind/daedric/shortsword/daedric5.vmt")
--resource.AddFile("materials/morrowind/daedric/shortsword/daedric1.vmt")
--resource.AddFile("sound/weapons/shortsword/morrowind_shortsword_deploy1.mp3")
--resource.AddFile("sound/weapons/shortsword/morrowind_shortsword_hit.mp3")
--resource.AddFile("sound/weapons/shortsword/morrowind_shortsword_hitwall1.mp3")
--resource.AddFile("sound/weapons/shortsword/morrowind_shortsword_slash.mp3")


end

if( CLIENT ) then
	
	
	SWEP.SlotPos = 1
	SWEP.DrawAmmo = false
	SWEP.DrawCrosshair = false
end


SWEP.PrintName = "Black Carver"
SWEP.Slot = 1
SWEP.Kind = WEAPON_MELEE
SWEP.Icon = "vgui/entities/weapon_mor_daedric_shortsword"

SWEP.Category = "Morrowind Shortswords"
SWEP.Author			= "Doug Tyrrell + Mad Cow (Revisions by Neotanks) for LUA (Models, Textures, ect. By: Hellsing/JJSteel)"
SWEP.Base			= "weapon_mor_base_shortsword"
SWEP.Instructions	= "Left click to stab/slash/lunge and right click to throw, also capable of badassery."
SWEP.Contact		= ""
SWEP.Purpose		= ""

SWEP.ViewModelFOV	= 72
SWEP.ViewModelFlip	= false

SWEP.Spawnable			= true
SWEP.AdminSpawnable		= true
  
SWEP.ViewModel      = "models/morrowind/daedric/shortsword/v_daedric_shortsword.mdl"
SWEP.WorldModel   = "models/morrowind/daedric/shortsword/w_daedric_shortsword.mdl"

SWEP.Primary.Damage		= 25
SWEP.Primary.NumShots		= 0
SWEP.Primary.Delay 		= .4

SWEP.Primary.ClipSize		= -1					// Size of a clip
SWEP.Primary.DefaultClip	= -1					// Default number of bullets in a clip
SWEP.Primary.Automatic		= true				// Automatic/Semi Auto
SWEP.Primary.Ammo			= "none"

SWEP.Secondary.ClipSize		= -1					// Size of a clip
SWEP.Secondary.DefaultClip	= -1					// Default number of bullets in a clip
SWEP.Secondary.Automatic	= false				// Automatic/Semi Auto
SWEP.Secondary.Ammo		= "none"