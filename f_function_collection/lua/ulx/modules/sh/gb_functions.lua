ulx.nr_roles = {}
local function updatenrroles()
    table.Empty( ulx.nr_roles )
    table.insert(ulx.nr_roles,"traitor")
    table.insert(ulx.nr_roles,"detective")
    table.insert(ulx.nr_roles,"innocent")

end
hook.Add( ULib.HOOK_UCLCHANGED, "ULXNRRolesUpdate", updatenrroles )
updatenrroles() -- Init

function ulx.tttnrforcerole(calling_ply, target_ply, role, rounds)

    role = string.lower(role)
    local tplyname = target_ply:Nick()

    rounds = tonumber(rounds)
    if rounds < 1 then
        rounds = 1
    end

    if (role == "traitor") then
        ULib.tsayColor( calling_ply, false, Color( 255,64,64 ), tplyname, Color( 210,180,140 ), " will be fored to ", Color( 0,154,205 ), "Traitor", Color( 210,180,140 ), " next Round!" )
        target_ply:SetPData( "IPDIforceTnr", 1)

    elseif (role == "detective") then
        ULib.tsayColor( calling_ply, false, Color( 255,64,64 ), tplyname, Color( 210,180,140 ), " will be fored to ", Color( 0,154,205 ), "Detective", Color( 210,180,140 ), " next Round!" )
        target_ply:SetPData( "IPDIforceDnr", 1)

    elseif (role == "innocent") then
        ULib.tsayColor( calling_ply, false, Color( 255,64,64 ), tplyname, Color( 210,180,140 ), " will be fored to ", Color( 0,154,205 ), "Innocent", Color( 210,180,140 ), " for ", Color( 162,205,90 ), ""..rounds.."", Color( 210,180,140 )," Round!" )
        target_ply:SetPData( "IPDIforceInr", rounds)

    else
        ULib.tsayError( calling_ply, "Role unkown..", true )
    end

end
local tttnrforcerole = ulx.command( "TTT Admin", "ulx tttnrforcerole", ulx.tttnrforcerole, "!fnr" )
tttnrforcerole:addParam{ type=ULib.cmds.PlayerArg }
tttnrforcerole:addParam{ type=ULib.cmds.StringArg, completes=ulx.nr_roles, hint="NR Role", error="invalid role \"%s\" specified", ULib.cmds.restrictToCompletes }
tttnrforcerole:addParam{ type=ULib.cmds.StringArg, hint="Rounds (Inno ONLY)" }
tttnrforcerole:defaultAccess( ULib.ACCESS_ADMIN )
tttnrforcerole:help( "Force player to choosen role in follwing round." )





local CATEGORY_NAME = "TTT Admin"

--[[corpse_remove][removes the corpse given.]
@param  {[Ragdoll]} corpse [The corpse to be removed.]
--]]
function corpse_remove(corpse)
    CORPSE.SetFound(corpse, false)
    if string.find(corpse:GetModel(), "zm_", 6, true) then
        corpse:Remove()
    elseif corpse.player_ragdoll then
        corpse:Remove()
    end
end

--[[corpse_identify][identifies the given corpse.]
@param  {[Ragdoll]} corpse [The corpse to be identified.]
--]]
function corpse_identify(corpse)
    if corpse then
        local ply = player.GetByUniqueID(corpse.uqid)
        if IsValid(ply) then
            ply:SetNWBool("body_found", true)
            CORPSE.SetFound(corpse, true)
        end
    end
end
--[End]----------------------------------------------------------------------------------------

function corpse_find(v)
    for _, ent in pairs( ents.FindByClass( "prop_ragdoll" )) do
        if ent.uqid == v:UniqueID() and IsValid(ent) then
            return ent or false
        end
    end
end

------------------------------ Startspecialround ------------------------------
function ulx.startspecialround( calling_ply,  rounde )
    rounde = tonumber(rounde)
    if specialRound.Modes[rounde] ~= nil then
        specialRound.forcedRound =  rounde
        RunConsoleCommand("ttt_roundrestart")
        timer.Simple(10,function() specialRound.forcedRound = false end)
        if IsValid(calling_ply) then
        calling_ply:PS_Notify( "yay, special round" )
            end
    else
        if IsValid(calling_ply) then calling_ply:PS_Notify( "Round doesnt exist" ) end
    end
end
local startspecialround = ulx.command( "TTT Fun", "ulx startspecialround", ulx.startspecialround, "!startspecialround" )
startspecialround:addParam{ type=ULib.cmds.NumArg, min=1, max = 20, default=1, hint="round", ULib.cmds.round }
startspecialround:defaultAccess( ULib.ACCESS_SUPERADMIN )
startspecialround:help( "Starts a special round." )

------------------------------ Snowballs ------------------------------
function ulx.snowballs( calling_ply, target_plys, hours )
    specialRound.snowballs()
    ulx.fancyLogAdmin( calling_ply, "#A Started a snowballfight")
end
local snowballs = ulx.command( "TTT Fun", "ulx snowballs", ulx.snowballs, "!snowballs" )

snowballs:defaultAccess( ULib.ACCESS_SUPERADMIN )
snowballs:help( "Start a snowballfight" )

------------------------------ sent to 13 ------------------------------
function ulx.s13( calling_ply, target_plys )

    for i=1, #target_plys do
        target_plys[ i ]:SendLua("LocalPlayer():ConCommand('connect 184.170.244.10:27069')")
    end

    ulx.fancyLogAdmin( calling_ply, "#A threw #T to server 13", target_plys )
end
local s13 = ulx.command( "TTT Fun", "ulx s13", ulx.s13, "!s13" )
s13:addParam{ type=ULib.cmds.PlayersArg }

s13:defaultAccess( ULib.ACCESS_ADMIN )
s13:help( "Throw person to server 13 (RDMMER)" )



------------------------------ DeathTalking Admin ------------------------------
function ulx.talkwhiledeath( calling_ply, target_plys, seconds )
	 
    for i=1, #target_plys do
		--target_plys[ i ]:ConCommand("setPlayerTime "..amount)
		target_plys[ i ].IsAbleToTalkWhileDeath = true
		target_plys[i]:PS_Notify( "You are able to talk for "..seconds.." seconds while death" )
                timer.Simple(seconds, function()
                    if IsValid(target_plys[ i ]) then
                        target_plys[ i ].IsAbleToTalkWhileDeath = false
                    end
                end)
    end
    
ulx.fancyLogAdmin( calling_ply, "#A set #T to be able to talk to the living for #i seconds", target_plys, seconds )
end
local talkwhiledeath = ulx.command( "TTT Fun", "ulx talkwhiledeath", ulx.talkwhiledeath, "!talkwhiledeath" )
talkwhiledeath:addParam{ type=ULib.cmds.PlayersArg }
talkwhiledeath:addParam{ type=ULib.cmds.NumArg, min=15, max = 300, default=60, hint="Time in seconds", ULib.cmds.round }
talkwhiledeath:defaultAccess( ULib.ACCESS_SUPERADMIN )
talkwhiledeath:help( "Makes you able talk while death" )

------------------------------ Givepointshopitem ------------------------------
function ulx.givepsitem( calling_ply, target_plys, wep )

    for i=1, #target_plys do
        --target_plys[ i ]:ConCommand("setPlayerTime "..amount)
        target_plys[ i ]:PS_GiveItem(wep)
       -- ulx.fancyLogAdmin( calling_ply, "#A gave #T a #i ", target_plys, wep )
    end
end
local givepsitem = ulx.command( "TTT Fun", "ulx givepsitem", ulx.givepsitem, "!givepsitem" )
givepsitem:addParam{ type=ULib.cmds.PlayersArg }
givepsitem:addParam{ type=ULib.cmds.StringArg, hint="jumpboots12p" }
givepsitem:defaultAccess( ULib.ACCESS_SUPERADMIN )
givepsitem:help( "Gives item to person" )

----- DAMAGELOG -----

function ulx.dlog( calling_ply )
    calling_ply:ConCommand("gb_print_damagelog") -- This runs ttt_print_damagelog on the person who called the command (pressed the button or said !dlogs).

    if not gb.is_hidden_staff( calling_ply) then
        ulx.fancyLogAdmin( calling_ply, "#A is viewing the damage logs.", command, target_plys )
    end
end
local dlog= ulx.command( "TTT Fun", "ulx dlog", ulx.dlog, "!dlog" ) -- Makes the command be called dlogs and puts it in the Utility category.
dlog:defaultAccess( ULib.ACCESS_ADMIN )
dlog:help( "Shows the damage logs." )

----- DAMAGELOG -----

function ulx.dlogs( calling_ply )
	--calling_ply:ConCommand("gb_print_damagelog") -- This runs ttt_print_damagelog on the person who called the command (pressed the button or said !dlogs).
    calling_ply:ConCommand("damagelog")

end
local dlogs= ulx.command( "TTT Fun", "ulx dlogs", ulx.dlogs, "!dlogs" ) -- Makes the command be called dlogs and puts it in the Utility category.
dlogs:defaultAccess( ULib.ACCESS_ADMIN )
dlogs:help( "Shows the damage logs." )




 ------------------------------ pspoint ------------------------------
function ulx.pspoint( calling_ply, target_plys, amount )
    
    for i=1, #target_plys do
		target_plys[i]:PS_GivePoints(amount)
	    target_plys[i]:PS_Notify( "An admin gave you "..amount.." points" )
        --target_plys[ i ]:AddCredits(amount)
    end
    
ulx.fancyLogAdmin( calling_ply, true, "#A has given #T #i Points", target_plys, amount )
end
local acred = ulx.command("TTT Fun", "ulx pspoints", ulx.pspoint, "!pspoints")
acred:addParam{ type=ULib.cmds.PlayersArg }
acred:addParam{ type=ULib.cmds.NumArg, hint="Pointshop Points", ULib.cmds.round }
acred:defaultAccess( ULib.ACCESS_ADMIN )
acred:help( "Gives the target(s) Pointshop points." )



---- BigHead
function ulx.bighead( calling_ply, target_plys, size )


    local affected_plys = {}
    for i=1, #target_plys do
        local v = target_plys[ i ]

        if not v:Alive() then
            ULib.tsayError( calling_ply, v:Nick() .. " is dead", true )

        else
            local boneid = v:LookupBone("ValveBiped.Bip01_Head1")
            if boneid then
                local vec = 1
                if size == "big" then vec = 3 end
                if size == "huge" then vec = 6 end
                if size == "nomal" then vec = 1 end
                if size == "small" then vec = 0.5 end

                v:ManipulateBoneScale(boneid, Vector(3, 3, 3))
            end
        end
    end
end


local bighead = ulx.command( "TTT Fun", "ulx bighead", ulx.bighead, "!bighead" )
bighead:addParam{ type=ULib.cmds.PlayersArg }
bighead:addParam{ type=ULib.cmds.StringArg, hint="big", completes={"big","huge","normal","small"} }
bighead:defaultAccess( ULib.ACCESS_ADMIN )
bighead:help( "Give a player a bighead - !bigheadv  " )

---- GIVEWEAPON
function ulx.giveweapon( calling_ply, target_plys, weapon )
	
	
	local affected_plys = {}
	for i=1, #target_plys do
		local v = target_plys[ i ]
		
		if not v:Alive() then
		ULib.tsayError( calling_ply, v:Nick() .. " is dead", true )
		
        else
            v:PS_DropWeaponsSameSlot(weapon)
		v:Give(weapon)
		table.insert( affected_plys, v )
		end
	end
	ulx.fancyLogAdmin( calling_ply, "#A gave #T weapon #s", affected_plys, weapon )
end
	
	
local giveweapon = ulx.command( "TTT Fun", "ulx giveweapon", ulx.giveweapon, "!giveweapon" )
giveweapon:addParam{ type=ULib.cmds.PlayersArg }
giveweapon:addParam{ type=ULib.cmds.StringArg, hint="weapon_nyangun" }
giveweapon:defaultAccess( ULib.ACCESS_ADMIN )
giveweapon:help( "Give a player a weapon - !giveweapon  " )

---- ShowENt
function ulx.reloadpointshop( calling_ply )
    PS:Initialize()
    PS:LoadItems()
    calling_ply:SendLua("PS:Initialize() PS:LoadItems();");
    print("Reloading pointshop")
end

local reloadpointshop = ulx.command( "TTT Admin", "ulx reloadpointshop", ulx.reloadpointshop, "!reloadpointshop" )
reloadpointshop:defaultAccess( ULib.ACCESS_ADMIN )
reloadpointshop:help( "Reload the pointshop." )




---- ShowENt
function ulx.showent( calling_ply )
    print( "## Ent you are looking at")
    local ent = calling_ply:GetEyeTrace()
    if IsValid(ent.Entity) then
     ULib.tsay( calling_ply, "Ent: "..ent.Entity:GetClass(), true )
    else
        ULib.tsay( calling_ply, "Look at an ent", true )
    end

end

local showent = ulx.command( "TTT Admin", "ulx showent", ulx.showent, "!showent" )
showent:defaultAccess( ULib.ACCESS_ADMIN )
showent:help( "Print the entityname you are looking at.)" )


-- Give Ent


function ulx.giveent( calling_ply, target_plys, classname, params, health )
    
    for i=1, #target_plys do
        classname = classname:lower()
		local newEnt = ents.Create( classname )

		-- Make sure it's a valid ent
		if not newEnt or not newEnt:IsValid() then
			print("Ent is not valid")
			return
		end    
    
		local v = target_plys[ i ]

        local trace = v:GetEyeTrace()
		local vector = trace.HitPos
		vector.z = vector.z + 90
        vector.y = vector.y + 90

		newEnt:SetPos( vector ) -- Note that the position can be overridden by the user's flags

		params:gsub( "([%w%p]+)\"?:\"?([%w%p]+)", function( key, value )
			newEnt:SetKeyValue( key, value )
		end ) 

		newEnt:Spawn()
		newEnt:Activate()
        newEnt:SetHealth(health or 100)

		undo.Create( "ulx_ent" )
			undo.AddEntity( newEnt )
			undo.SetPlayer( v )
		undo.Finish()
    end 
	if not params or params == "" then
	--	ulx.fancyLogAdmin( calling_ply, "#A created ent #s", classname )
	else
	--	ulx.fancyLogAdmin( calling_ply, "#A created ent #s with params #s", classname, params )
	end
end

entListgg = {
"weapon_ttt_c4","npc_alyx","npc_antlion","npc_antlion_template_maker","npc_antlionguard","npc_barnacle","npc_barney","npc_breen","npc_citizen","npc_combine_camera","npc_combine_s","npc_combinedropship","npc_combinegunship","npc_crabsynth","npc_cranedriver","npc_crow","npc_cscanner","npc_dog","npc_eli","npc_fastzombie","npc_fisherman (not available in Hammer by default)","npc_gman","npc_headcrab","npc_headcrab_black","npc_headcrab_fast","npc_helicopter","npc_ichthyosaur","npc_kleiner","npc_manhack","npc_metropolice","npc_monk","npc_mortarsynth","npc_mossman","npc_pigeon","npc_poisonzombie","npc_rollermine","npc_seagull","npc_sniper","npc_stalker","npc_strider","npc_turret_ceiling","npc_turret_floor","npc_turret_ground","npc_vortigaunt","npc_zombie","npc_zombie_torso"
}

local giveent = ulx.command(  "TTT Fun", "ulx giveent", ulx.giveent, "!giveent" )
giveent:addParam{ type=ULib.cmds.PlayersArg }
giveent:addParam{ type=ULib.cmds.StringArg, hint="npc_alyx", completes=entListgg }
giveent:addParam{ type=ULib.cmds.StringArg, hint="additionalequipment:weapon_zm_pistol", ULib.cmds.takeRestOfLine, ULib.cmds.optional }
giveent:addParam{ type=ULib.cmds.NumArg, hint="Health", min=1, max = 2500, default=100, ULib.cmds.round }
giveent:defaultAccess( ULib.ACCESS_SUPERADMIN )
giveent:help( "Spawn an ent, separate flag and value with ':'." )


-- Rocket

local directions = {"up", "down", "left", "right", "forward", "back", "u", "d", "l", "r", "f", "b"}
 
function ulx.rocket( calling_ply, target_plys, string_arg )
       
        for _, v in ipairs( target_plys ) do
                if not v:Alive() then
                        ULib.tsay( calling_ply, v:Nick() .. " is dead!", true )
                        return
                end
                if v.jail then
                        ULib.tsay( calling_ply, v:Nick() .. " is in jail", true )
                        return
                end
                if v.ragdoll then
                        ULib.tsay( calling_ply, v:Nick() .. " is a ragdoll", true )
                        return
                end    
 
                if v:InVehicle() then
                        local vehicle = v:GetParent()
                        v:ExitVehicle()
                end
                v:SetMoveType(MOVETYPE_WALK)
                tcolor = team.GetColor( v:Team()  )
                local trail = util.SpriteTrail(v, 0, Color(tcolor.r,tcolor.g,tcolor.b), false, 60, 20, 4, 1/(60+20)*0.5, "trails/smoke.vmt")                           
               
                if( string_arg == "up" or string_arg == "u" ) then
                        v:SetVelocity(Vector(0, 0, 2048))
                elseif ( string_arg == "down" or string_arg == "d" ) then
                        v:SetVelocity(Vector(0, 0, -2048))
                elseif ( string_arg == "left" or string_arg == "l" ) then
                        v:SetVelocity(v:GetLeft() * 2048)
                elseif ( string_arg == "right" or string_arg == "r" ) then
                        v:SetVelocity(v:GetRight() * 2048)
                elseif ( string_arg == "forward" or string_arg == "f" ) then
                        v:SetVelocity(v:GetForward() * 2048)
                elseif ( string_arg == "back" or string_arg == "b" ) then
                        v:SetVelocity(v:GetForward() * -2048)
                end
                       
               
                timer.Simple(2.5, function()
                        local Position = v:GetPos()            
                        local Effect = EffectData()
                        Effect:SetOrigin(Position)
                        Effect:SetStart(Position)
                        Effect:SetMagnitude(512)
                        Effect:SetScale(128)
                        util.Effect("Explosion", Effect)
                        timer.Simple(0.1, function()
                                v:KillSilent()
                                trail:Remove()
                                local corpse = corpse_find(v)
                                if corpse then
                                    corpse_identify(corpse)
                                    corpse_remove(corpse)
                                end
                        end)
                end)
        end
        ulx.fancyLogAdmin( calling_ply, "#A turned #T into a rocket!", target_plys )
end
 
local rocket = ulx.command( "Fun", "ulx rocket", ulx.rocket, "!rocket" )
rocket:addParam{ type=ULib.cmds.PlayersArg }
rocket:addParam{ type=ULib.cmds.StringArg, completes=directions, default="up", hint="direction", error="invalid direction \"%s\" specified", ULib.cmds.restrictToCompletes }
rocket:defaultAccess( ULib.ACCESS_ADMIN )
rocket:help( "Rocket players into the air" )


---[Prevent win]-------------------------------------------------------------------------
function ulx.preventwin( calling_ply, should_prevwin )
    if not GetConVarString("gamemode") == "terrortown" then ULib.tsayError( calling_ply, gamemode_error, true ) else
        if should_prevwin then
            ULib.consoleCommand( "ttt_debug_preventwin 0" .. "\n" )
            ulx.fancyLogAdmin( calling_ply, "#A allowed the round to end as normal." )
        else
            ULib.consoleCommand( "ttt_debug_preventwin 1" .. "\n" )
            ulx.fancyLogAdmin( calling_ply, "#A prevented the round from ending untill timeout." )
        end
    end
end
local preventwin = ulx.command( CATEGORY_NAME, "ulx prevwin", ulx.preventwin )
preventwin:defaultAccess( ULib.ACCESS_SUPERADMIN )
preventwin:addParam{ type=ULib.cmds.BoolArg, invisible=true }
preventwin:setOpposite( "ulx allowwin", {_, true} )
preventwin:help( "Toggles the prevention of winning." )
---[End]----------------------------------------------------------------------------------------


---[Round Restart]-------------------------------------------------------------------------
function ulx.roundrestart( calling_ply )
    if not GetConVarString("gamemode") == "terrortown" then ULib.tsayError( calling_ply, gamemode_error, true ) else
        ULib.consoleCommand( "ttt_roundrestart" .. "\n" )
        ulx.fancyLogAdmin( calling_ply, "#A has restarted the round." )
    end
end
local restartround = ulx.command( CATEGORY_NAME, "ulx roundrestart", ulx.roundrestart )
restartround:defaultAccess( ULib.ACCESS_SUPERADMIN )
restartround:help( "Restarts the round." )
---[End]----------------------------------------------------------------------------------------



---- ShowENt
function ulx.showopenslots( calling_ply )

   table.SortByMember(PS.Items, "Name",true)
   local test = { blawp = {Name = "test"}, bliep = {Name="best"} }
   local test2 = {}
   for k,v in pairs(test) do
       table.insert(test2,v)
   end

   table.SortByMember(test2, "Name",true)
   local i = 0
   for k,v in ipairs(test2) do
       print( v.Name)
       i = i + 1
       if i == 10 then return end
   end

    local weps = calling_ply:GetWeapons()
    local slots = {}
    local openslots = ""
    for k,v in pairs( weps ) do
        if v.Kind then
            slots[v.Kind] = true
        end
    end
    local i = 0
    while i < 10 do
        i = i + 1
        if not slots[i] then
            openslots = openslots.." "..i
        end
    end
    if openslots == "" then
        openslots = "All slots are taken"
    end

        ULib.tsay( calling_ply, "Open slots: "..openslots, true )


end

local showopenslots = ulx.command( "TTT Fun", "ulx showopenslots", ulx.showopenslots, "!showopenslots" )
showopenslots:defaultAccess( ULib.ACCESS_ALL )
showopenslots:help( "Show your open slots" )


---- ShowENt
function ulx.weaponslot( calling_ply, weapon )
    local wep = weapons.GetStored( weapon )
    if not (wep) then
        ULib.tsay( calling_ply, "Weapon doesnt exist", true )
        return
    end
    local kind = 0
    local slot = 0
        if wep.Kind then
            kind = wep.Kind
        end
    if wep.Slot then
        slot = wep.Slot
    end

    ULib.tsay( calling_ply, "You can have only one weapon per type", true )
    ULib.tsay( calling_ply, "Type: "..kind.." Slot: "..slot, true )


end

local weaponslot = ulx.command( "TTT Fun", "ulx weaponslot", ulx.weaponslot, "!weaponslot" )
weaponslot:addParam{ type=ULib.cmds.StringArg, hint="weapon_ttt_ak47" }
weaponslot:defaultAccess( ULib.ACCESS_ALL )
weaponslot:help( "Shows the slot a weapon takes" )


