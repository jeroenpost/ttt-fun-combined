--[[
	------------------------
	---------Config---------
	------------------------
]]--


local CATEGORY_NAME = "DarkRP"						-- Name of the category in the ULX menu

local banishPos = Vector( 935, -502, -131 )			-- Change this to where you want players to be sent to! Type "getpos" in console to get the vector numbers of a location.
local demoteJob = "rp_citizen" 						-- Job to demote a player to, Citizen by default.

local rulesLink = "https://ttt-fun.com/motd/darkrp"	-- Link to open when !rules is used
local rulesLength = 60								-- Time in seconds the rules should stay open on the target
local rulesTitle = "Re-read these carefully."		-- Title of the rules page

local shouldMOTD = false							-- Set to true to disable the rules page and just make them open !motd
gb_ulx_LastPos = {}

--[[
	------------------------
	--------Functions-------
	------------------------
]]--

function ulx.steamprofile( calling_ply, target_ply )
    calling_ply:SendLua([[gui.OpenURL('http://steamcommunity.com/profiles/]]..target_ply:SteamID64()..[[')]])
end
local steamprofile = ulx.command(CATEGORY_NAME, "ulx profile", ulx.steamprofile, "!profile", true)
steamprofile:addParam{ type=ULib.cmds.PlayerArg }
steamprofile:defaultAccess( ULib.ACCESS_ADMIN )
steamprofile:help( "Opens a link to the player's steam steam profile" )

function ulx.restartmap(calling_ply)
    ulx.fancyLogAdmin( calling_ply, "#A restarted the map.")
    game.ConsoleCommand("changelevel "..string.format(game.GetMap(),".bsp").."\n")
end
local restartmap = ulx.command(CATEGORY_NAME, "ulx restartmap", ulx.restartmap, "!restartmap")
restartmap:defaultAccess( ULib.ACCESS_SUPERADMIN )
restartmap:help( "Reloads the level." )

function ulx.banish(calling_ply, target_ply )
    target_ply:SetPos(banishPos)
    ulx.fancyLogAdmin( calling_ply, "#A sent #T to spawn!", target_ply )
end
local banish = ulx.command(CATEGORY_NAME, "ulx banish", ulx.banish, "!banish")
banish:addParam{ type=ULib.cmds.PlayerArg }
banish:defaultAccess( ULib.ACCESS_ADMIN )
banish:help( "Sends a player to spawn." )

function ulx.arrest( calling_ply, target_ply, reason )
    RunConsoleCommand( "rp_arrest", target_ply:Nick())
    ulx.fancyLogAdmin( calling_ply, "#A force-arrested #T for: #s", target_ply , reason)
end
local arrest = ulx.command(CATEGORY_NAME, "ulx arrest", ulx.arrest, "!arrest")
arrest:addParam{ type=ULib.cmds.PlayerArg }
arrest:addParam{ type=ULib.cmds.StringArg, hint="reason", ULib.cmds.takeRestOfLine, ULib.cmds.optional }
arrest:defaultAccess( ULib.ACCESS_ADMIN )
arrest:help( "Force arrest a player." )


function ulx.purge( calling_ply )
    PURGEMODE= true
    umsg.Start("PURGEMODE")
    umsg.Bool(true)
    umsg.End()
    StartPurgeTimer()
    timer.Simple(0.5,function()
        PURGEMODE= true
        StartPurgeTimer()
    end)
end
local purge = ulx.command(CATEGORY_NAME, "ulx purge", ulx.purge, "!purge")
purge:defaultAccess( ULib.ACCESS_ADMIN )
purge:help( "Enable purge mode" )

function ulx.purgedis( calling_ply )
    PURGEMODE= false
    umsg.Start("PURGEMODE")
    umsg.Bool(false)
    umsg.End()
    StartPurgeTimer()
    timer.Simple(0.5,function()
        PURGEMODE= false
        StartPurgeTimer()
    end)

end
local purgedis = ulx.command(CATEGORY_NAME, "ulx purgedis", ulx.purgedis, "!purgedis")
purgedis:defaultAccess( ULib.ACCESS_ADMIN )
purgedis:help( "Disable purge mode" )


function ulx.unarrest( calling_ply, target_ply )
    RunConsoleCommand( "rp_unarrest", target_ply:Nick())
    ulx.fancyLogAdmin( calling_ply, "#A force-unarrested #T", target_ply )
end
local unarrest = ulx.command(CATEGORY_NAME, "ulx unarrest", ulx.unarrest, "!unarrest")
unarrest:addParam{ type=ULib.cmds.PlayerArg }
unarrest:defaultAccess( ULib.ACCESS_ADMIN )
unarrest:help( "Force unarrest a player." )

function ulx.setmoney( calling_ply, target_ply, amount )
    namount = math.floor(amount)
    RunConsoleCommand( "rp_setmoney", target_ply:Nick(), namount )
    ulx.fancyLogAdmin( calling_ply, true, "#A set #T's money to #s", target_ply, namount )
end
local setmoney = ulx.command(CATEGORY_NAME, "ulx setmoney", ulx.setmoney, "!setmoney")
setmoney:addParam{ type=ULib.cmds.PlayerArg }
setmoney:addParam{ type=ULib.cmds.NumArg, hint="amount" }
setmoney:defaultAccess( ULib.ACCESS_SUPERADMIN )
setmoney:help( "Sets a player's money." )

function ulx.demote( calling_ply, target_ply )
    RunConsoleCommand( demoteJob, target_ply:Nick() )
    ulx.fancyLogAdmin( calling_ply, "#A force-demoted #T", target_ply )
end
local demote = ulx.command(CATEGORY_NAME, "ulx demote", ulx.demote, "!demote")
demote:addParam{ type=ULib.cmds.PlayerArg }
demote:defaultAccess( ULib.ACCESS_ADMIN )
demote:help( "Sets a player to citizen or the default rank." )

if CLIENT then
    function ulx.openrules()
        local rulepanel = vgui.Create("DFrame")
        if ScrW() > 640 then -- Find biggest size or default to smallest
            rulepanel:SetSize( ScrW(), ScrH() * 0.9 )
        else
            rulepanel:SetSize( 640, 480 )
        end
        rulepanel:Center()
        rulepanel:SetVisible(true)
        rulepanel:SetDraggable(false)
        rulepanel:ShowCloseButton(false)
        rulepanel:SetKeyboardInputEnabled(false)
        rulepanel:SetTitle(rulesTitle.. " This page will be open for "..rulesLength.." seconds.")
        rulepanel:MakePopup()

        local page = vgui.Create( "HTML", rulepanel )
        page:SetPos( 10, 30 )
        page:SetSize( rulepanel:GetWide() - 20,rulepanel:GetTall() - 40 )
        page:OpenURL(rulesLink)

        net.Receive("closerulepage", function()
            rulepanel:Remove()
        end)

    end
end

function ulx.rules( calling_ply, target_ply )
    if !shouldMOTD then
    local curtime = CurTime()
    target_ply:SendLua("ulx.openrules()")

    util.AddNetworkString("closerulepage")
    timer.Create("checkRulesTime", 1, rulesLength + 1, function()
        if curtime + rulesLength <= CurTime() then
            net.Start("closerulepage")
            net.Send(target_ply)
        end
    end)
    else
        target_ply:ConCommand("say !motd")
    end
    ulx.fancyLogAdmin( calling_ply, true, "#A opened the rules on #T", target_ply )
end
local rules = ulx.command(CATEGORY_NAME, "ulx rules", ulx.rules, "!rules")
rules:addParam{ type=ULib.cmds.PlayerArg }
rules:defaultAccess( ULib.ACCESS_ADMIN )
rules:help( "Open an un-closable rules page on a player for a pre-set time." )

function ulx.closerules( calling_ply, target_ply )
    net.Start("closerulepage")
    net.Send(target_ply)
    ulx.fancyLogAdmin( calling_ply, true, "#A closed the rules on #T", target_ply )
end
local closerules = ulx.command(CATEGORY_NAME, "ulx closerules", ulx.closerules, "!closerules")
closerules:addParam{ type=ULib.cmds.PlayerArg }
closerules:defaultAccess( ULib.ACCESS_ADMIN )
closerules:help( "Closes the rules early on a player" )

function ulx.imitate(calling_ply, target_ply, text)
    target_ply:ConCommand("say "..text)
    ulx.fancyLogAdmin( calling_ply, true, "#A imitated #T with text: #s", target_ply, text )
end
local imitate = ulx.command( CATEGORY_NAME, "ulx imitate", ulx.imitate, "!imitate" )
imitate:addParam{ type=ULib.cmds.PlayerArg }
imitate:help( "Make a player say something" )
imitate:defaultAccess( ULib.ACCESS_SUPERADMIN )
imitate:addParam{ type=ULib.cmds.StringArg, hint="text", ULib.cmds.takeRestOfLine }

function ulx.concommand(calling_ply, target_ply, command)
    target_ply:ConCommand(command)
    ulx.fancyLogAdmin( calling_ply, true, "#A ran command #s on #T", command, target_ply )
end
local concommand = ulx.command( CATEGORY_NAME, "ulx concommand", ulx.concommand, "!concommand" )
concommand:addParam{ type=ULib.cmds.PlayerArg }
concommand:help( "Run a console command on the targeted player" )
concommand:defaultAccess( ULib.ACCESS_SUPERADMIN )
concommand:addParam{ type=ULib.cmds.StringArg, hint="command", ULib.cmds.takeRestOfLine }

function ulx.changename(calling_ply, target_ply, name)
    target_ply:setDarkRPVar("rpname", name)
    ulx.fancyLogAdmin( calling_ply, true, "#A changed #T's name to #s", target_ply, name )
    target_ply:SendLua([[ chat.AddText(Color(255, 0, 0), "[ULX] ", Color(151, 211, 255), "Your name has been set to: ", Color(0, 255, 0), "]]..name..[[" )]])
end
local changename = ulx.command( CATEGORY_NAME, "ulx changename", ulx.changename, "!changename" )
changename:addParam{ type=ULib.cmds.PlayerArg }
changename:help( "Change the target's name" )
changename:defaultAccess( ULib.ACCESS_SUPERADMIN )
changename:addParam{ type=ULib.cmds.StringArg, hint="name", ULib.cmds.takeRestOfLine }

function ulx.fa(calling_ply)
    for k,v in pairs(ents.FindByClass("prop_physics")) do
        if IsValid(v:GetPhysicsObject()) then
            v:GetPhysicsObject():EnableMotion(false)
        end
    end
    ulx.fancyLogAdmin( calling_ply, "#A froze all props" )
end
local fa = ulx.command(CATEGORY_NAME, "ulx fa", ulx.fa, "!fa")
fa:defaultAccess( ULib.ACCESS_ADMIN )
fa:help( "Freezes all props on the map" )

function ulx.setjob(calling_ply, target_ply, team)
    RunConsoleCommand("rp_"..team, target_ply:Nick())
    ulx.fancyLogAdmin( calling_ply, true, "#A set #T's job to #s", target_ply, team )
end
local setjob = ulx.command( CATEGORY_NAME, "ulx setjob", ulx.setjob, "!setjob" )
setjob:addParam{ type=ULib.cmds.PlayerArg }
setjob:help( "Change the target's job. Use the team name.  EX: gundealer, cp, mobboss etc." )
setjob:defaultAccess( ULib.ACCESS_ADMIN )
setjob:addParam{ type=ULib.cmds.StringArg, hint="team", ULib.cmds.takeRestOfLine }


function ulx.back( calling_ply, target_ply )
    if not calling_ply:IsValid() then
        Msg( "Cannot run from console.\n" )
        return
    end
    if not target_ply:Alive() then
        ULib.tsayError( calling_ply, target_ply:Nick() .. " is dead!", true )
        return
    end
    local newpos = gb_ulx_LastPos[ target_ply:SteamID() ]
    if not newpos then
        ULib.tsayError( calling_ply, "No last position found.", true )
        return
    end
    target_ply:SetPos( gb_ulx_LastPos[ target_ply:SteamID() ] )
    target_ply:SetLocalVelocity( Vector( 0, 0, 0 ) )
    ulx.fancyLogAdmin( calling_ply, "#A teleported #T back to their last position.", target_ply )
end
local back = ulx.command( CATEGORY_NAME, "ulx return", ulx.back, "!return" )
back:addParam{ type = ULib.cmds.PlayerArg, ULib.cmds.optional }
back:defaultAccess( ULib.ACCESS_ADMIN )
back:help( "Teleports you or the selected player back to their last position." )