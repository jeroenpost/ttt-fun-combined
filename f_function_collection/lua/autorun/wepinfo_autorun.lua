MsgN("Initializing TTT Weapon Info by Wolf Haley")

if SERVER then
	AddCSLuaFile("cl_wepinfo.lua")
else
	include( "cl_wepinfo.lua" )
end

WN = {}
WNC = {}

local function AddInfoWeapon( e, n )
	table.insert( WN, { ENT = e, NAME = n } )
	--print( "[TTT Weapon Info] Loaded custom info for "..e )
end

/*
	=========================================================================================
								DO NOT CHANGE ANYTHING ABOVE THIS
	=========================================================================================
*/

/*
	ADDING/CHANGING WEAPON NAMES:

	Functions like below (AddInfoWeapon), are used to customize the name of weapons in-game.
	For example, if in-game, a shotgun's name (on the info box) is "shotgun_name" (first one below),
	then I would like to change it.

	This function goes as follows:

	AddInfoWeapon( [Original Name In-Game (On the info box)], [Custom/Preferred Name] )

	You are free to change any of the ones below as you wish.

*/
AddInfoWeapon( "shotgun_name", "Shotgun" )
AddInfoWeapon( "rifle_name", "Rifle" )
AddInfoWeapon( "pistol_name", "Pistol" )
AddInfoWeapon( "grenade_fire", "Incendiary Grenade" )
AddInfoWeapon( "grenade_smoke", "Smoke Grenade" )
AddInfoWeapon( "newton_name", "Newton Launcher" )
AddInfoWeapon( "knife_name", "Knife" )
AddInfoWeapon( "sipistol_name", "Silenced Pistol" )
AddInfoWeapon( "c4", "C4 Explosive" )
AddInfoWeapon( "tele_name", "Teleporter" )
AddInfoWeapon( "flare_name", "Flare Gun" )



// Background color of the info box
WNC.BackgroundColor = Color( 50, 50, 50, 100 )

// Whether or not to color the stats
WNC.EnableColors = false

// Changes the color of the stats only if WNC.EnableColors = true
WNC.DamageColor = Color( 100, 255, 255 )
WNC.ClipColor = Color( 255, 100, 255 )
WNC.AutoColor = Color( 255, 255, 100 )
WNC.SpreadColor = Color( 255, 100, 100 )
WNC.RecoilColor = Color( 255, 150, 100 )