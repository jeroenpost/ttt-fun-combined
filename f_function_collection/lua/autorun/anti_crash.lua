print("Loaded AntiCrash")

hook.Add("Thineek", "AMB_CrashCatcher", function()
    for k, ent in pairs(ents.FindByClass("prop_ragdoll")) do
        if IsValid(ent) then
            if ent.player_ragdoll then
                local velo = ent:GetVelocity():Length()
                if velo >= 1750 and velo <= 2099 then
                    AMB_KillVelocity(ent)
                    ServerLog("[!CRASHCATCHER!] Caught velocity > 1750 on a ragdoll entity, negating velocity and temporarily disabling motion.\n")
                    PrintMessage(HUD_PRINTTALK, "[!TTT-FUN CRASHCHECK!] Please don't spaz bodies around.\n")
                elseif velo >= 2100 then
                    if ent.uqid then
                        ply = player.GetByUniqueID(ent.uqid)
                        if IsValid(ply) then
                        ply:SetNWBool("body_found", true)
                        CORPSE.SetFound(ent, true)
                        end
                    end
                    ent:Remove()
                    ServerLog("[!CRASHCATCHER!] Caught velocity > 2100 on a ragdoll entity, removing offending ragdoll entity from world.\n")
                    PrintMessage(HUD_PRINTTALK, "[!TTT-FUN CRASHCHECK!] Removed a body from the game to prevent a crash.\n")
                end
            end
        end
    end
end)

function AMB_SetSubPhysMotionEnabled(ent, enable)
    if not IsValid(ent) then return end

    for i = 0, ent:GetPhysicsObjectCount() - 1 do
        local subphys = ent:GetPhysicsObjectNum(i)
        if IsValid(subphys) then
            subphys:EnableMotion(enable)
            if enable then
                subphys:Wake()
            end
        end
    end
end

function AMB_KillVelocity(ent)
    ent:SetVelocity(vector_origin)
    AMB_SetSubPhysMotionEnabled(ent, false)

    timer.Simple(0.01, function() AMB_SetSubPhysMotionEnabled(ent, true) end)
end


