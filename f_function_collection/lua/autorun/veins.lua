if CLIENT then

	local RING = surface.GetTextureID( "redring" )

	function PlayerHudEffect()

        if LocalPlayer():GetFunVar("has_delta_hud") == 1 then
            surface.SetMaterial( Material( "camos/delta_hud.png", "noclamp" ) )
            surface.SetDrawColor( color_white )
            local w = surface.ScreenWidth()
            local h = surface.ScreenHeight()
            surface.DrawTexturedRectUV( 0, 0, w ,h+120 , 0, 0, 1,1 )
        end

        if LocalPlayer():GetFunVar("has_hax_hud") == 1 then
            surface.SetMaterial( Material( "camos/hax_hud.png", "noclamp" ) )
            surface.SetDrawColor( color_white )
            local w = surface.ScreenWidth()
            local h = surface.ScreenHeight()
            surface.DrawTexturedRectUV( 0, 0, w-5 ,h , 0, 0, 1,1 )
        end

        if LocalPlayer():GetFunVar("has_penguin_rage") == 1 then
           -- surface.SetMaterial( Material( "camos/camo5", "" ) )
            surface.SetDrawColor( Color(0,0,255,20) )
            local w = surface.ScreenWidth()
            local h = surface.ScreenHeight()
            surface.DrawTexturedRectUV( 0, 0, w ,h+120 , 0, 0, 1,1 )
        end

        local pulse = false -- LocalPlayer():GetFunVar("showpulse") == 1
		if not  GetConVar("ttt_fun_show_veins"):GetBool() and not pulse then return end
        local ply = LocalPlayer()
        if !IsValid(ply) or !ply:Alive() then return end
		local x = 100-ply:Health() -- math.Clamp(100-((ply:Health())),0,40)

		if ply:Health() < 40 then
			surface.SetTexture( RING )
			local speed = x/4
			local mul = math.sin(CurTime()*speed/7 + math.pi/1.5)+1
			--print(mul)
			--draw.SimpleText(tostring(mul),"Default",0,0,Color(255,255,255,255))
			surface.SetDrawColor(Color(255,255,255,mul*(x/4)))
			surface.DrawTexturedRect(0,0,ScrW(),ScrH())
        end



        if pulse then
            surface.SetTexture( RING )
            local speed = 50
            local mul = math.sin(CurTime()*speed/7 + math.pi/1.5)+2
            --print(mul)
            --draw.SimpleText(tostring(mul),"Default",0,0,Color(255,255,255,255))
            surface.SetDrawColor(Color(255,255,255,80/mul))
            surface.DrawTexturedRect(0,0,ScrW(),ScrH())
        end



	end

	hook.Add("HUDPaint","Veins_Effect",PlayerHudEffect)
end
--DrawMotionBlur( 0.1, f1*f2, 0.05)