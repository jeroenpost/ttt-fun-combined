
if SERVER then
    AddCSLuaFile()
    util.AddNetworkString( "gb_fun_networked_vars" )

    -- Networked Vars
    local meta = FindMetaTable("Player")
    function meta:GetFunVar(var)
        if not self.funvars then self.funvars = {} end
        if not self.funvars[var] then return 0 end
        return self.funvars[var]
    end


    function meta:SetFunVar(name,vari)
        if not self.funvars then self.funvars = {} end
        if not vari or not name then return end
        net.Start( "gb_fun_networked_vars" )
        net.WriteString( name )
        net.WriteFloat( vari )
        net.Send( self )
    end
end


if CLIENT then
    local gb_localstorage = {}
    net.Receive( "gb_fun_networked_vars", function( len )
        local name = net.ReadString()
        local var = net.ReadFloat( )
        gb_localstorage[name] = var
    end)

    local meta = FindMetaTable("Player")
    function meta:GetFunVar(var)
        if not gb_localstorage[var] then return 0 end
        return gb_localstorage[var]
    end
end