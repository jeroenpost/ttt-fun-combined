server_id = GetConVarNumber("server_id");


 --Stop picking up weapons
    if SERVER then
        hook.Add("PlayerCanPickupWeapon","CheckIfPlayerIsDog", function(ply,wep)
            if IsValid(ply) and ( ply.IsDog  or ply.CannotPickupWeapons ) then
                return false
            end
        end)
    end

if server_id < 100 then

   

    -- DEATHSOUND OVERRIDE
    function playerDiesSound( victim, weapon, killer )

        if not IsValid(victim) then return end

        if( victim.IsDog ) then
           victim:EmitSound("weapons/dog_howl.mp3", 510)
            if victim.OldModelDog and victim.OldModelScale then
                 victim:SetModel(victim.OldModelDog)
                 victim:SetModelScale(victim.OldModelScale, 0.05 )
            end
             victim.IsDog = false
        end

        -- remove beams
        for k, ent in pairs(ents.FindByClass("trace1")) do
            if IsValid(ent) and ent:GetOwner() == victim then
                    ent:Remove()
            end
        end


    end

    hook.Add( "PlayerDeath", "playerDeathSound", playerDiesSound )





    hook.Add("PlayerDeathSound", "OverrideDeathSoundDoggy", function() 
        return true 
    end)

    -- REMOVE THE DOGGIE VARIABLES ON ROUNDPREPARE
    hook.Add("TTTPrepareRound","NoDoggiesAnymore",function()
             for k,v in pairs(player.GetAll()) do
                    if v.IsDog and CLIENT then
                        v:SetMuted( false)
                    end
                     v.CannotPickupWeapons = false
                     v.IsDog = false
                     v.DogOwner = nil
                     if v.OldModelDog and v.OldModelScale then
                        v:SetModel(v.OldModelDog)
                        v:SetModelScale(v.OldModelScale, 0.5)
                     end
                    if SERVER then
                       v:StripWeapon("dog_bite")
                       v:StripWeapon("dog_sniffer")
                    end
             end
    end)


    -- OVERRIDE CHAT
    hook.Add("PlayerSay", "GreenBlack TTT Doggy", function(ply, text, isTeamChat)
         if IsValid(ply) && ply.IsDog && ply:Alive() then    
                return "Whoof Whoof..." 
            end

    end)



    -- OVERRIDE TALK
    hook.Add( "PlayerStartVoice", "GreenBlack TTT DoggyVoice", function(ply)

        if IsValid(ply) && ply.IsDog then    

                 ply:EmitSound("weapons/bark.mp3", 510)

                return false

        end
    end)

    -- Disable the dog being able to talk
    function playerDogTalk( listener, talker )    
        if IsValid(talker) and talker.IsDog then
             -- talker:EmitSound("weapons/bark.mp3", 510)
            return false
        end 
    end

    hook.Add("PlayerCanHearPlayersVoice","disableVoiceDoggy", playerDogTalk)

end