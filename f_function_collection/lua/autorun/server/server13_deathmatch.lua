


hook.Add("InitPostEntity","server13DeathMatch",function()
    if server_id ~= 13 and server_id ~= 99  then return end

    resource.AddFile("sound/specdm_quakesounds/dominating.mp3")
    resource.AddFile("sound/specdm_quakesounds/doublekill.mp3")
    resource.AddFile("sound/specdm_quakesounds/godlike.mp3")
    resource.AddFile("sound/specdm_quakesounds/holyshit.mp3")
    resource.AddFile("sound/specdm_quakesounds/killingspree.mp3")
    resource.AddFile("sound/specdm_quakesounds/megakill.mp3")
    resource.AddFile("sound/specdm_quakesounds/monsterkill.mp3")
    resource.AddFile("sound/specdm_quakesounds/rampage.mp3")
    resource.AddFile("sound/specdm_quakesounds/triplekill.mp3")
    resource.AddFile("sound/specdm_quakesounds/ultrakill.mp3")
    resource.AddFile("sound/specdm_quakesounds/unstoppable.mp3")
    resource.AddFile("sound/specdm_quakesounds/wickedsick.mp3")

    timer.Simple(10,function()
        RunConsoleCommand("ulx","groupallow","user","ulx respawn");
        RunConsoleCommand("ulx","groupallow","user","ulx giveweapon");
        RunConsoleCommand("ulx","groupallow","user","ulx dlog");
        RunConsoleCommand("ulx","groupallow","user","ulx dlogs");
        Damagelog.RDM_Manager_Enabled = false
        SpecDM.DisplayMessage = false
        SpecDM.EnableJoinMessages = false
        RunConsoleCommand("ttt_roundtime_minutes","5")
        RunConsoleCommand("ttt_debug_preventwin", "1")
    end)

    function gb_auto_respawn()

        RunConsoleCommand("ttt_roundtime_minutes","5")
        RunConsoleCommand("ttt_debug_preventwin", "1")

        timer.Create("gb_Autorespawn",5,0,function()
           if true then return end
            if  GetRoundState() ~= ROUND_ACTIVE then return end

           -- print("Running respawnscript...")

            local traitorCount = 0
            local detectiveCount = 0
            local time = CurTime() + 10

            for k, v in pairs(player.GetAll()) do
                if IsValid(v) and v:Alive() then
                    if v:GetRole() ~= ROLE_TRAITOR then
                        detectiveCount = detectiveCount + 1
                    else
                        traitorCount = traitorCount + 1
                    end
                end
            end

            for k, v in pairs(player.GetAll()) do
                if not IsValid(v) then return end
                v:SetNWBool("deathmatch", true)
                v:SetNWBool("SpecDM_Enabled", false)
                v:SetBaseKarma(9999)
                v:SetLiveKarma( 9999 )
                v.deathmatchenabled = true

                if v:IsBot() and not v:HasWeapon("botweapon") then
                    if server_id ~= 99 then
                    v:StripWeapon("gb_fists")
                    v:Give("botweapon")
                    end
                end

                if not v.dmisalive or (v:Team() ~= TEAM_TERROR and v:Team() ~= TEAM_SPEC)  then

                    if IsValid(v.server_ragdoll) then
                        local doll = v.server_ragdoll
                        timer.Simple(10,function()
                            SafeRemoveEntity(doll)
                        end)

                    end
                    if not v.respawn then
                        v.respawn = CurTime() + 8
                    end
                    if v.respawn < CurTime() then
                        v.dmisalive = true
                        v:SetTeam( TEAM_TERROR )

                        v:Spawn()
                        --if detectiveCount <= traitorCount then
                            v:SetRole(ROLE_DETECTIVE)
                        --else
                        --    v:SetRole(ROLE_TRAITOR)
                        --end
                        v:SetCredits(10)
                        --SendFullStateUpdate()
                        print("Respawned " .. v:Nick())

                        if v:IsBot() then
                            v:StripWeapons()
                            if server_id ~= 99 then
                                v:Give("botweapon")
                                end
                        end
                    else
                        v:ChatPrint("You will be respawned in 15 seconds")
                        v.dmisalive = false
                    end
                elseif v:Team() == TEAM_SPEC then
                    v:ChatPrint("Type: !respawn "..v:Nick().." to respawn yourself")
                    v.dmisalive = false
                elseif( v:Health() < 100) then
                    autohealfunction(v)
                end

            end
        end)

        timer.Create("gb_dm_RestartMap",285,1,function()
            RunConsoleCommand("ttt_roundrestart")
            RunConsoleCommand("ttt_roundtime_minutes","5")
            RunConsoleCommand("ttt_debug_preventwin", "1")
        end)
        timer.Create("gb_dm_ShowStats",275,1,function()
            SetRoundState(ROUND_POST)
            local highest = 0
            local ply = nil
            for k, v in pairs(player.GetAll()) do
                if v.gb_kills and v.gb_kills > highest then
                    ply = v
                    highest =  v.gb_kills
                end
            end
            if IsValid(ply) then
                for k, v in pairs(player.GetAll()) do
                    v:PrintMessage( HUD_PRINTCENTER, "Winner: "..ply:Nick().." ("..highest.." kills)" )
                    v:PrintMessage( HUD_PRINTTALK, "Winner: "..ply:Nick().." ("..highest.." kills)" )
                end
            else
                for k, v in pairs(player.GetAll()) do
                    v:PrintMessage( HUD_PRINTCENTER, "DRAW" )
                    v:PrintMessage( HUD_PRINTTALK, "DRAW" )
                end
            end
            hook.Call("TTTEndRound", GAMEMODE, true)
        end)
    end

    hook.Add("TTTPrepareRound", "gb_auto_respawn", gb_auto_respawn)

    hook.Add("TTTBeginRound","gb_dm_setalldetectives",function()
        for k, v in pairs(player.GetAll()) do
            v:SetRole(ROLE_DETECTIVE)
        end
    end)

    hook.Add("DoPlayerDeath", "setrespawntime", function(ply, killer, dmginfo)
        ply.respawn = CurTime() + 8
        ply.dmisalive = false
        ply:SetNWInt("staminaboost",0.50)
    end)

    hook.Add("PlayerInitialSpawn", "setrespawntim12312e", function(pl)
        pl.respawn = CurTime() + 4
    end)

    -- =============== SWITCH WRONG MAPS ==================

    hook.Add("TTTPrepareRound", "changeMapsIfWrognMap", function()

        -- Reset all stats of all PLayers
        for k, v in pairs(player.GetAll()) do
            v.gb_kills = 0
            v:SetNWInt("gb_kills",0)
        end

        local currentMap = game.GetMap()
        if currentMap == "ttt_space_station" or currentMap == "ttt_lost_temple_v2" then
            timer.Simple(5, function()
                RunConsoleCommand("changelevel", table.Random(Mapvote.maps))
            end)
        end
    end)

    hook.Add("PlayerSpawn","gb_gm_playerspawn",function(v)
        v.dmisalive = true
    end)


-- =============== REMOVE ENTS ==================
    local nextRemoveStuff = 0

    function removeEntsAndThat()



        for k, ent in pairs(ents.FindByClass("prop_ragdoll")) do
            if IsValid(ent) then


                if ent.player_ragdoll then

                    if not ent.NextRemove then
                        ent.NextRemove = CurTime() + 20
                    elseif ent.NextRemove < CurTime() then
                        SafeRemoveEntity(ent);
                    end

                end
            end
        end

        for k,ent in pairs( ents.GetAll() ) do
            if IsValid(ent) and ent.IsWeapon and ent:IsWeapon() and not IsValid(ent.Owner) then

                if not ent.NextRemove then
                    ent.NextRemove = CurTime() + 60
                elseif ent.NextRemove < CurTime() then
                    SafeRemoveEntity(ent);
                end
            end
        end

        for k, ent in pairs(ents.FindByClass("npc_manhack")) do
            if IsValid(ent) then

                if not ent.NextRemove then
                    ent.NextRemove = CurTime() + 20
                elseif ent.NextRemove < CurTime() then
                    SafeRemoveEntity(ent);
                end

            end

        end

        for k, ent in pairs(ents.FindByClass("npc_headcrab")) do
            if IsValid(ent) then

                if not ent.NextRemove then
                    ent.NextRemove = CurTime() + 20
                elseif ent.NextRemove < CurTime() then
                    SafeRemoveEntity(ent);
                end

            end

        end

        for k, ent in pairs(ents.FindByClass("npc_turret_floor")) do
            if IsValid(ent) then

                if not ent.NextRemove then
                    ent.NextRemove = CurTime() + 50
                elseif ent.NextRemove < CurTime() then
                    SafeRemoveEntity(ent);
                end

            end

        end

        for k, ent in pairs(ents.FindByClass("ttt_c4")) do
            if IsValid(ent) then

                if not ent.NextRemove then
                    ent.NextRemove = CurTime() + 40
                elseif ent.NextRemove < CurTime() then
                    SafeRemoveEntity(ent);
                end

            end

        end

    end

    hook.Add("TTTBeginRound", "gb_auto_removeents", function()   timer.Create("AutoDeleteEnts",10,0,function() removeEntsAndThat() end) end)

-- Player Stats

    hook.Add("PlayerDeath", "gb_server13_statsdeathmatch", function(ply, killer, inflictor)
        if not ply:IsBot() then
            ply:SpecDM_CheckKillRows()
        end
        if ply.specdm_killrows then
            ply.specdm_killrows = 0
        end
        if GetRoundState() == ROUND_ACTIVE and IsValid(killer) and killer:IsPlayer()  then
            if not (ply == killer) then
                if not killer.gb_kills then killer.gb_kills = 0 end
                killer.gb_kills = (killer.gb_kills) + 1
                killer:SetNWInt("gb_kills",killer.gb_kills)
                killer.specdm_killrows = (killer.specdm_killrows and killer.specdm_killrows or 0) + 1
                if killer.specdm_stats_new and killer.specdm_stats_new.kills then
                    killer.specdm_stats_new.kills = killer.specdm_stats_new.kills + 1
                    killer:SpecDM_EnableUpdate("kills")
                end
                if ply.specdm_stats_new then
                    if ply.specdm_stats_new.deaths then
                        ply.specdm_stats_new.deaths = ply.specdm_stats_new.deaths + 1
                        ply:SpecDM_EnableUpdate("deaths")
                    end
                    ply:SpecDM_CheckKillRows()
                end

                SpecDM_Quake(ply, killer)
                killer:SetNWBool("SpecDM_Enabled", false)
            end
        end
    end)

end)


