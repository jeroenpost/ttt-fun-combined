

function gb_autobalance(ent, hitgroup, dmginfo)
    if  server_id == 2 or server_id == 1 then
        if ent:IsPlayer() then


                local inflictor = dmginfo:GetInflictor( )
                local weapon = nil
                if inflictor.GetActiveWeapon then
                    weapon = inflictor:GetActiveWeapon()
                end
                local damage = dmginfo:GetDamage()
                local currenttime = CurTime()

                local victimSteam = ent:SteamID()
                if not inflictor.gb_lastdmg then inflictor.gb_lastdmg = {} end
                if inflictor.gb_lastdmg == 0 then inflictor.gb_lastdmg = {} end
                if not inflictor.gb_lastdmg[victimSteam] then inflictor.gb_lastdmg[victimSteam] = 0 end
                if not inflictor.gb_lastdmg[victimSteam.."dmg"] then inflictor.gb_lastdmg[victimSteam.."dmg"] = 0 end

                if ( hitgroup ~= HITGROUP_HEAD  and not dmginfo:IsDamageType(DMG_BLAST)) and not (IsValid(weapon) and weapon:GetClass() == "special_magnum" ) then
                    if inflictor:IsPlayer() and inflictor:Alive() then
                        if inflictor.gb_lastdmg[victimSteam] > currenttime  then
                            if inflictor.gb_lastdmg[victimSteam.."hit"] == currenttime then
                                if inflictor.gb_lastdmg[victimSteam.."hitcurrent"] + damage > 75 then
                                    local dmg = 75 - inflictor.gb_lastdmg[victimSteam.."hitcurrent"]
                                    dmginfo:SetDamage( dmg )
                                    inflictor.gb_lastdmg[victimSteam.."hitcurrent"] = inflictor.gb_lastdmg[victimSteam.."hitcurrent"] + dmg
                                else
                                    inflictor.gb_lastdmg[victimSteam.."hitcurrent"] =  inflictor.gb_lastdmg[victimSteam.."hitcurrent"] + damage
                                end
                            else
                                dmginfo:SetDamage( 0 )

                                if IsValid(weapon) and weapon.SetClip1 then
                                    weapon:SetClip1(weapon:Clip1()+1)
                                end
                            end
                        else
                            if damage > 75 then
                                damage = 75
                                dmginfo:SetDamage( 75 )
                            end
                            inflictor.gb_lastdmg[victimSteam] = currenttime + (damage/115)
                            inflictor.gb_lastdmg[victimSteam.."hit"] = currenttime
                            inflictor.gb_lastdmg[victimSteam.."hitcurrent"] = damage
                        end

                    end
                end

        end
    end
end

hook.Add("ScalePlayerDamage","gb_autobalance", gb_autobalance)

hook.Add("InitPostEntity","gb_autobalance_health",function()
    if server_id == 2 or server_id == 1 then
        timer.Create("gb_autobalance_health",0.2,0,function()
            for a,ply in pairs(player.GetAll()) do
                gb_check_max_health(ply,115)
            end
        end)
        end
end)

