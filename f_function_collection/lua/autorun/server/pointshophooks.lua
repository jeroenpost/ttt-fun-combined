


hook.Add("TTTBeginRound", "TTTBeginRoundPointshopTD", function()
 			 for k,v in pairs(player.GetAll()) do
                 v.burnedBody = false

                                        if v:GetRole() == ROLE_DETECTIVE then
                                             v:SetHealth(  v:Health() + 25 );
                                        end

                 if not gb.IsMember(v) then
                     v:SetHealth( 150 );
                     v:PrintMessage( HUD_PRINTTALK, "Welcome to TTT-FUN! As new player your health is set to 150, and you have a random powerful weapon!")
                     v:Give(table.Random({"custom_tac_nayn","custom_tac_nayn","custom_tac_nayn","custom_tac_nayn","aa_deagle","aa_m3super"}))
                 end

				    --set DoubleHealth
				    if v.GotDoubleHealth or v:GetPData( "GotDoubleHealth",0) == 2 then
				        v.GotDoubleHealth = false
				     	v:SetHealth( v:Health() + 125 );
				     	v:PrintMessage( HUD_PRINTTALK, "[TTT-FUN pointshop] You got extra health")
				     	print(v:Nick().." is set to +125 health (pointshop)")
                        v:SetPData( "GotDoubleHealth", 0)
				    end

                 v.biggerhead = 1

                 -- Dont regive stuff
                 timer.Simple(5,function()
                     if not IsValid(v) then return end
                    local tmr = "give_equipment" .. tostring(v:UniqueID())
                     timer.Destroy( tmr )
                 end)
				
				end
                             
				
			end)



--Remove trails when dead
--function trailremove(ply, ent)
--	if IsValid( ply.TrailPS ) then
--		  SafeRemoveEntity(ply.TrailPS)
--	end
--end
--hook.Add("PlayerDeath", "PS_trailremove", trailremove)

--Make sure person is equipped with all his stuff
function autohealfunction( ply )
    if not IsValid(ply) then return end
    if not ply.NextHeal then ply.NextHeal = 0 end
    if not SERVER or ply.NextHeal > CurTime()   then return end
    ply.NextHeal = CurTime() + 2

    if ply:Health() < 150 then
        ply:SetHealth(ply:Health() + 1 )
    end
end

function canPlayerBuySpecialRound( ply )
    if ply.HadBossRoundMap then
        ply:PS_Notify("You cannot buy a Special round more than one time in a map")
        return false
    end

    if GetGlobalInt("ttt_rounds_left", 6) < 4 then
        ply:PS_Notify("There are less than 3 rounds left on this map")
        ply:PS_Notify("You cannot buy a special round in the last 3 rounds")
        return false
    end
    if GetGlobalInt("special_rounds_this_map", 0) > 1 then
        ply:PS_Notify("Already 2 special rounds where on this map")
        ply:PS_Notify("Only two special rounds are allowed")
        return false
    end


    for k,v in pairs(player.GetAll()) do
        if v.BoughtBossThisRound == true then
            ply:PS_Notify("Someone else bought a special round for next round. Try again next round")
            return false
        end
    end
    return true
end

function refundPlayersWithSpecialStuff()
    for k,v in pairs(player.GetAll()) do
        if v.GotDetective == true then
            v:PS_Notify("Someone bought a special round for next round. You got your detectiveround refunded")
            v:PS_GivePoints(500)
            v:PS_Notify( "An admin gave you 500 points" )
            v.GotDetective = false
            v:SetPData( "lasttraitorbuy", os.time()-900)
        end
        if v:GetPData( "IPDIforceTnr" ) == 1 then
            v:PS_Notify("Someone bought a special round for next round. You got your traitorround refunded")
            v:PS_GivePoints(7500)
            v:SetPData( "lasttraitorbuy", os.time()-900)
            v:SetPData( "IPDIforceTnr", 0)
            v:PS_Notify( "An admin gave you 7500 points" )
        end
    end
end

function checkPersonPSItems() 
	
	for k,ply in pairs(player.GetAll()) do
		if not IsValid(ply) or not ply:PS_CanPerformAction() then return end
		if TEAM_SPECTATOR ~= nil and ply:Team() == TEAM_SPECTATOR then return end
		if TEAM_SPEC ~= nil and ply:Team() == TEAM_SPEC then return end	
		
		for item_id, item in pairs(ply.PS_Items) do
			local ITEM = PS.Items[item_id]
                        if ITEM then
                            local isModel = string.find(ITEM.Category,"odel") 
                            if isModel and isModel > 1 and item.Equipped then
                                if ITEM.WeaponClass then
                                    ply:PS_DropWeaponsSameSlot(ITEM.WeaponClass)
                                   -- ply:PS_HolsterOthers( item_id )
                                end
                                ITEM:OnEquip(ply, item.Modifiers)
                            end

                            if (item.Equipped and not ply.PS_Equipped[ item_id ]) then
                                    ply.PS_Equipped[ item_id ] = true                                    
                                    ITEM:OnEquip(ply, item.Modifiers)
                            end
                        end
		end
	end
		
end


local function PS_setPlayerModel( ply )
    if IsValid(ply) then
        if ply.PlayerModel then
            ply:SetModel( ply.PlayerModel )
        end
        if ply.PS_Color then    
            borkolor = ply.PS_Color
            ply:SetPlayerColor(Vector( borkolor.r / 255, borkolor.g / 255, borkolor.b / 255))
        end
    end
end


hook.Add("PlayerSetModel", "PS_Setplayermodel", function(ply) PS_setPlayerModel(ply) end)


hook.Add("TTTPrepareRound", "PS_GiveItems2", function() timer.Simple(3, checkPersonPSItems) end)
--Fix for playermodels
hook.Add("TTTBeginRound", "PS_GiveItems22", function() timer.Simple(2, checkPersonPSItems) end)
hook.Add("TTTBeginRound", "PS_GiveItems23", function() timer.Simple(4, checkPersonPSItems) end)

