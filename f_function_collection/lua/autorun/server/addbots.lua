local botsLastRun = 1

function addBots( noslay )

    botsLastRun = os.time()

    local Players2 = player.GetHumans()
    local numberOfPlayers = table.Count(Players2)

    local Bots = player.GetBots()
    local numberOfBots = table.Count(Bots)
    local addBots = 2 - numberOfPlayers

    if server_id == 99 or server_id == 13 then
        addBots = 9 - numberOfPlayers
    end

    -- NO idle bots anymore!
    if numberOfPlayers < 1 then return end

    if (numberOfBots > addBots) then

        local kicked = 0
        local kickBots = numberOfBots - addBots
        for k, v in pairs(player.GetBots()) do
            if kicked < kickBots then
                game.ConsoleCommand("kick "..v:Nick().."\n")
                kicked = kicked + 1
                --print("Kicked bot")
            end
        end
    elseif (addBots > numberOfBots) then

        local toAdd = addBots - numberOfBots
        print("Adding "..toAdd.." bots")
        for i=1,toAdd,1 do
            game.ConsoleCommand("bot\n")
            --print("Added bot")
        end
    end

    if not noslay then
        for k, v in pairs(player.GetBots()) do
            v:Kill()
        end
    end

end


function addBotsLowUsers(ply)
    if SERVER and ( os.time() - botsLastRun ) > 10 and (not ply:IsBot()) then
        timer.Simple(10,function()
            addBots(noslay);
        end)

    end
end

function SlayBotsminute()
    addBots(true);
end

concommand.Add("addSomeBots",addBotsLowUsers)


if SERVER then

    hook.Add( "PlayerInitialSpawn", "addBotsIfLowUsers", addBotsLowUsers )
    if server_id == 99 or server_id == 13 then
        timer.Create("minutebotsspawn",10,0,function()
            addBots(true)
        end)
    end

    botsLastRun = os.time() + 30

end

