
local bolt_ents = {

    "crossbow_bolt",
    "npc_grenade_frag",
    "rpg_missile",
    "sent_rpg_missile"

}

function gb_check_max_health(ply,max)
    if not max then max = 150 end
    if ply:Health() > max and not ply.IsSpecialperson then
        ply:SetHealth(max)
    end
end

function protectionSuitDamage(ent, dmginfo)




 	if ent:IsPlayer() then

            local damagetype = dmginfo:GetDamageType()
            local hassuit = ent.hasProtectionSuit	or  ent.hasSuperman or ent.hasProtectionSuitTemp
            local inflictor = dmginfo:GetInflictor( )

            -- Check if player is a specator. If so, strip him and set damage to 0
            if inflictor:IsPlayer() and (inflictor:IsSpec() or not inflictor:Alive()) and not inflictor:IsGhost() then
                dmginfo:SetDamage(0)
                inflictor:StripWeapons()
            end

            if ent.dealextradamage then
                dmginfo:ScaleDamage( tonumber(ent.dealextradamage) )
            end

            if ent:GetActiveWeapon():IsValid() and ent:GetActiveWeapon():GetClass() == "custom_dwpunishment" and inflictor:IsPlayer() then
                local moredamage = math.Rand(1,10);
                if moredamage > 4 then
                    dmginfo:ScaleDamage( 3 )
                    print( "damage x3")
                else
                    if ent:Health() < 175  then
                     ent:SetHealth( ent:Health() + (dmginfo:GetDamage() / 2))
                        if ent:Health() > 175 then
                            ent:SetHealth( 175)
                        end
                    end
                    dmginfo:SetDamage(0)
                   -- print( "extra health")
                end
            end
            if ent.hasDragoScales and damagetype == DMG_BURN then
                if not ent.healthgainbysuit then
                    ent.healthgainbysuit = 0
                end
                if ent:Health() < 175 and (not ent.NextFireDamage or ent.NextFireDamage < CurTime()) and ent.healthgainbysuit < 125  then
                    ent.NextFireDamage = CurTime() + 0.2
                     ent:SetHealth( ent:Health()+2)

                    ent.healthgainbysuit = ent.healthgainbysuit + 2
                    if ent:Health() > 175 then ent:SetHealth(175) end
                end
                dmginfo:SetDamage( 0 )




            elseif hassuit and damagetype == DMG_BLAST  then

                 dmginfo:ScaleDamage( 0.5 )

            elseif  (hassuit) 	and ( damagetype == DMG_FALL or damagetype == DMG_CRUSH or damagetype == DMG_BURN or damagetype == DMG_DROWN ) then			

                dmginfo:SetDamage(0)

            elseif damagetype == DMG_FALL and ent.HasJumpBoots then

                 dmginfo:SetDamage(0)

            elseif damagetype == DMG_FALL then

                     dmginfo:ScaleDamage( 0.7 )      

            elseif GetRoundState() == ROUND_PREP then
                dmginfo:SetDamage(0)

            elseif ent.hasShieldPotato and not ent.hasShieldPotato2   then
                 ent:EmitSound( Sound("weapons/warden/warden1.mp3"))
                 if IsValid(inflictor) and inflictor:IsPlayer() and IsValid(inflictor:GetActiveWeapon())
                    and (inflictor:GetActiveWeapon():GetClass() == "custom_polo_glaive" or
                        inflictor:GetActiveWeapon():GetClass() == "custom_kevins_grenade") then
                         dmginfo:SetDamage(0)
                    end
                 
                 print("potatoshield")

                
                 if IsValid(inflictor) and inflictor:IsPlayer() and not inflictor.hasShieldPotato then
                    paininfo = DamageInfo()
                    local dm = dmginfo:GetDamage( ) * 0.01
                    if dm > 25 then dm = 25 end
					paininfo:SetDamage( dmginfo:GetDamage( ) * 0.01 )
					paininfo:SetDamageType(DMG_SLASH)
		 			paininfo:SetAttacker(ent)
					paininfo:SetInflictor(ent)
					if SERVER then inflictor:TakeDamageInfo(paininfo) end
                 end
                 dmginfo:ScaleDamage( 0.90 )
	        elseif ent:IsPlayer() and ent.hasShieldPotato2 and ent.hasShieldPotato2 > (CurTime() -15)  then
                 ent:EmitSound( Sound("weapons/warden/warden1.mp3"))
                 
                 print("potatoshie;d")
                  if IsValid(inflictor) and inflictor:IsPlayer() and IsValid(inflictor:GetActiveWeapon())
                    and (inflictor:GetActiveWeapon():GetClass() == "custom_polo_glaive" or
                        inflictor:GetActiveWeapon():GetClass() == "custom_kevins_grenade") then
                         dmginfo:SetDamage(0)
                    end
                 inflictor = dmginfo:GetInflictor( )
                 dmginfo:ScaleDamage( 0.5 )
                if IsValid(inflictor) and inflictor:IsPlayer() and not inflictor.hasShieldPotato then
                     paininfo = DamageInfo()
                     local dm = dmginfo:GetDamage( ) * 0.2
                     if dm > 50 then dm = 50 end
					paininfo:SetDamage( dm )
					paininfo:SetDamageType(DMG_SLASH)
					paininfo:SetAttacker(ent)
					paininfo:SetInflictor(ent)

					if SERVER then inflictor:TakeDamageInfo(paininfo) end
                end
            end


	end


    -- Adds bolt damage
    local infl = dmginfo:GetInflictor()
    if IsValid(infl) then
        local pClass 	= infl:GetClass()
        local pOwner 	= infl:GetOwner()
            if (table.HasValue( bolt_ents, pClass )) then
                if (infl.m_iDamage) then
                    dmginfo:SetDamage( infl.m_iDamage )
                end

                if (IsValid(pOwner)) then
                    dmginfo:SetAttacker( pOwner )
                end
            end
    end

end


hook.Add("EntityTakeDamage","removeDamafeIfSuitOn", protectionSuitDamage)

