-- Global Config File
if SERVER then
    AddCSLuaFile("config.lua")
end

if (not ConVarExists("server_id")) then CreateConVar("server_id", "9999", { FCVAR_ARCHIVE }, "The ID of the Server") end
if (not ConVarExists("redirect_to")) then CreateConVar("redirect_to", "3", { FCVAR_ARCHIVE }, "Redirect if server is full") end

server_id = GetConVarNumber("server_id");
server_type = "ttt_custom"

hook.Add("PlayerInitialSpawn", "syncConvars", function()
    RunConsoleCommand("server_id", GetConVar("server_id"):GetString())
end)


_globals = {}
_globals['slayed'] = {}
_globals['specialperson'] = {}

hook.Add("TTTPrepareRound","resetSomeForsLikeSpacialPerson",function()
    _globals['specialperson'] = {}
end)

local looseruleservers = {[1]=1,[2]=1,[5]=1,[6]=1,[53]=1}
strictruleserver = true

if looseruleservers[server_id] then
    strictruleserver = false
end

heavyserver = true


serverSettings = {}


AFKCONFIG = {}
AFKCONFIG.warnTime = 3 --This is the time in minuits it takes before the warning message is giving
AFKCONFIG.kickTime = 30
AFKCONFIG.mainWarnMessage = "You are AFK"-- Message to be displayed when the user is warned, Dont make this too long of it may go off the screen
AFKCONFIG.subWarnMessage = ""-- Sub message to be displayed when the user is warned
AFKCONFIG.subWarnMessage2 = "Move around to be cool again"
AFKCONFIG.subWarnMessage3 = ""
AFKCONFIG.kickReason = "You were kicked for being AFK too long" -- If you dont know this then you are stupid, JK {CODE BLUE} LOVES EVERYONE!
AFKCONFIG.souldKickAdmins = false --Should it kick addmins for being AFK?