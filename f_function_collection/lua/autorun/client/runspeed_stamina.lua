local show_on_right_side = false
local show_label = true

surface.CreateFont("ATStaminaLabel", {
    font = "Roboto",
    size = 14,
    weight = 800
})
hook.Add("PostDrawHUD", "WyoziAdvTTTHUD", function()

    local shouldDraw = !LocalPlayer():KeyDown(IN_ATTACK2)
    if shouldDraw == false then return end

    local client = LocalPlayer()

    if not client:Alive() or client:Team() == TEAM_SPEC then return end
    if not TEAM_SPEC then return end -- not ttt

    local bar_x = 20
    local bar_y = ScrH() - 27
    local bar_width = 230
    local bar_height = 12

    draw.RoundedBox(4, bar_x, bar_y, bar_width, bar_height, Color(25, 100, 25, 222))
    draw.RoundedBox(4, bar_x, bar_y, bar_width * math.max(client:GetNWFloat("w_stamina", 0), 0.02), bar_height, Color(50, 200, 50, 250))

    if show_label then
        draw.SimpleText("Stamina", "ATStaminaLabel", bar_x+5, bar_y+bar_height*0.5+1, Color(0, 0, 0), TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER)
    end

end) 