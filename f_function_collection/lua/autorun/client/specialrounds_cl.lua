
surface.CreateFont("GBEventHeader", {
    font = "Roboto",
    size = 64
})
surface.CreateFont("GBEventTitleHeader", {
    font = "Roboto",
    size = 90
})
surface.CreateFont("GBEventSmallMsg", {
    font = "Roboto",
    size = 45
})

specialRound.Csay = function()
    local data2 = net.ReadTable()
    local msg = data2[1]
    local colorSay = Color( 255, 255, 255, 255 )
    local duration = 3
    local fade = 0.2
	local start = CurTime()
    local data = {
        Start = CurTime(),
        Message = msg,
        SlideInLength = 2,
        FadeOutStart = 5,
        FadeOutLength = 1
    }

    local function ComputeFracs(data)
        local initial_frac = math.Clamp((CurTime() - data.Start) / data.SlideInLength, 0, 1)
        local end_frac = math.Clamp(((data.Start + data.FadeOutStart) - CurTime()) / data.FadeOutLength, 0, 1)
        return initial_frac, end_frac
    end

    local function CubicInterp(t, b, c)
        return c*t*t*t + b
    end
  
	local function drawToScreenForSR()
        if data then
            local initial_frac, end_frac = ComputeFracs(data)

            if end_frac > 0 then
                local alphamul = end_frac
                local y = CubicInterp(initial_frac, -170, 270)

                local scalewidth =  ScreenScale( 250 )
                draw.RoundedBox( 2, ScrW()/2-(scalewidth/2), y, scalewidth, 170, Color( 0, 0, 0, 200*alphamul ) )
                draw.SimpleText("SPECIAL ROUND", "GBEventHeader", ScrW()/2, y+10, Color(255, 255, 255, 255*alphamul), TEXT_ALIGN_CENTER, TEXT_ALIGN_BOTTOM)
                draw.SimpleText(data.Message, "GBEventTitleHeader", ScrW()/2, y+70, Color(52, 152, 219, 255*alphamul), TEXT_ALIGN_CENTER, TEXT_ALIGN_BOTTOM)
            else
                hook.Remove( "HUDPaint", "specialroundsCsay" )
                return
            end
        end




	end

	hook.Add( "HUDPaint", "specialroundsCsay", drawToScreenForSR )
end

net.Receive( "specialroundMessage", specialRound.Csay )




function specialRound.RemoveHatsAndTrails( )
    LocalPlayer():PS_RemoveClientsideModels()
end


net.Receive( "specialroundRemoveHatsAndTrails", specialRound.RemoveHatsAndTrails )



net.Receive( "specialroundopenyoutubeplayer", function()
    local data2 = net.ReadTable()
    local url = data2[1]
    local close = data2[2]
    local dontclose = data2[3]
    local window = vgui.Create( "DFrame" )

        window:SetSize( 130, 42 )

    window:SetPos(10,10)

    window:SetTitle( "Music" )

    window:SetVisible( true )
    --window:SetFocusTopLevel(false)
    --window:MakePopup()
   -- window:SetFocusTopLevel(false)

    local html = vgui.Create( "HTML", window )

    html:SetSize( window:GetWide() - 20, window:GetTall() - 40 - 50 )
    html:SetPos( 10, 30 )
    html:OpenURL( url )

    timer.Simple(close, function() if window and isfunction(window.Close) then window:Close() end end)
    if not dontclose then
        hook.Add("TTTEndRound","CloseYoutubeWindow"..LocalPlayer():SteamID(),function() if window and isfunction(window.Close) then window:Close() end end)
    end
end)




