
if ( CLIENT ) then return end

function gb_loadpersistent( name )

    if not name then name = "" end

	local file = file.Read( "persist_files/" .. game.GetMap() .. "_.txt", true )
	if ( not file ) then return end
	print("Running permaprops")

	local tab = util.JSONToTable( file )

	if ( not tab ) then return end
	if ( not tab.Entities ) then return end
	if ( not tab.Constraints ) then return end

	--local Ents, Constraints = duplicator.Paste( nil, tab.Entities, tab.Constraints )
    for k, v in pairs( ents:GetAll() ) do
        if v.gb_ispermaprop then
            v:Remove()
        end
    end


	for k, v in pairs( tab.Entities ) do
        if v.Class ~= "prop_ragdoll" then

            for k2, v3 in pairs( v.PhysicsObjects )do

                local ent = ents.Create(v.Class)
                ent:SetModel(v.Model)
                ent:SetPos(v3.Pos)
                ent:SetAngles(v3.Angle)
               -- ent:PhysicsInit( SOLID_VPHYSICS )
                ent:SetMoveType(MOVETYPE_NONE)
                ent:SetSolid(SOLID_VPHYSICS)
                ent:Spawn()

                local phys = ent:GetPhysicsObject()

                if phys and phys:IsValid() then
                    phys:EnableMotion(false)
                end

               if v.EntityMods then
                       if v.EntityMods.material then
                           ent:SetMaterial(v.EntityMods.material.MaterialOverride)
                       end
               end
                ent.gb_ispermaprop = true
            end
         end
	end

end

hook.Add( "TTTPrepareRound", "PersistenceInit323_gsb", gb_loadpersistent )


hook.Add( "InitPostEntity", "PersistenceInit", gb_loadpersistent )

    hook.Add( "PostCleanupMap", "PersistenceInit2_gsb", gb_loadpersistent )


   --gb_loadpersistent()
