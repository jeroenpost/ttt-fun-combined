surface.CreateFont( "WeaponFontLarge", {
 font = "Bebas Neue",
 size = 100,
 weight = 0,
 antialias = true
} )

surface.CreateFont( "WeaponFontSmall", {
 font = "Bebas Neue",
 size = 70,
 weight = 0,
 antialias = true
} )

surface.CreateFont( "WeaponFontSmall2", {
 font = "Bebas Neue",
 size = 40,
 weight = 0,
 antialias = true
} )

local function DrawWeaponText()
    if not cvars.Bool("tttfun_guninfo") then return end
	local lpos = LocalPlayer():GetPos()
	local entity = LocalPlayer():GetEyeTrace().Entity
	if entity:IsValid() and lpos:Distance( entity:GetPos() ) < 160 and entity:IsWeapon() then
		local p
		
		local ang = LocalPlayer():EyeAngles()
		
		--if ARMS.BounceOverheadText then
			p = entity:GetPos() + Vector(0,0,90 + math.sin(CurTime()*2)*5)
		--else
		--p = entity:GetPos() + Vector(0,0,90)
		--end
		
	
		for _,yaw in pairs({0, 180}) do
	
			local ang = LocalPlayer():EyeAngles()
			--ang:RotateAroundAxis(a:Forward(), 90)
			--ang:RotateAroundAxis(a:Right(), yaw)
			--if ARMS.RotateOverheadText then
				--a:RotateAroundAxis(a:Right(), CurTime() * ARMS.RotateOverheadSpeed)
			--else
			--end
			
			local offset
			
			local printName = entity:GetPrintName()

		
			
			local damage
			local clip
			local auto
			local spread
			local recoil
			local ammo
			
			local recoilColor
			local spreadColor
			
			if entity.Primary then
				damage = entity.Primary.Damage or "N/A"
				clip = entity.Primary.ClipSize or "N/A"
				auto = tostring(entity.Primary.Delay) or "N/A"
				spread = tostring(entity.Primary.Cone) or "N/A"
				recoil = "N/A"
                slot = entity.Slot
                if slot then
                    slot = tonumber(slot)+1
                end
				ammo = (entity.Kind or "N/A") .. "/".. ((slot) or "N/A")

			else
				damage = "N/A"
				clip = "N/A"
			end
			
			if entity.Primary.Recoil then
				if entity.Primary.Recoil < 1.5 then
					recoil = "Small"
					recoilColor = Color( 0, 255, 0, 255 )
				else
					recoil = "Average"
					recoilColor = Color( 255, 132, 0, 255 )
				end
			end
			
			if entity.Primary.Cone then
				if entity.Primary.Cone >= 0.07 then
					spreadColor = Color( 255, 0, 0, 255 )
				elseif entity.Primary.Cone > 0.04 and entity.Primary.Cone < 0.07 then
					spreadColor = Color( 255, 132, 0, 255 )
				elseif entity.Primary.Cone <= 0.4 then
					spreadColor = Color( 0, 255, 0, 255 )
				end
			end
			
			if entity.Primary.ClipSize == -1 then
				clip = "N/A"
			end
			
			if entity.Primary.Delay then
				auto = entity.Primary.Delay
			else
				auto = "N/A"
			end
			
			if entity.Primary.Damage == 1 then
				damage = "N/A"
			end
			
			for k, v in pairs( WN ) do
				if string.lower(printName) == string.lower(v.ENT) then
					printName = v.NAME
				end
			end
			
			surface.SetFont( "WeaponFontSmall" )
			local wi, hi = surface.GetTextSize(printName)
			if wi > 300 then
				offset = 50
			else
				offset = 0
			end
			
			if WNC.EnableColors then
				dmgcolor = WNC.DamageColor
				clipcolor = WNC.ClipColor
				autocolor = WNC.AutoColor
				spreadcolor = WNC.SpreadColor
				recoilcolor = WNC.RecoilColor
			else
				dmgcolor = Color( 255, 255, 255, 200 )
				clipcolor = Color( 255, 255, 255, 200 )
				autocolor = Color( 255, 255, 255, 200 )
				spreadcolor = Color( 255, 255, 255, 200 )
				recoilcolor = Color( 255, 255, 255, 200 )
			end
		
			cam.Start3D2D(p, Angle( 0, ang.y - 90, 90 ), 0.1)
				draw.RoundedBox( 4,  -250 - offset, 440 + 100, 300 + offset * 2, 240, WNC.BackgroundColor or Color( 50, 50, 50, 100 ) )
				--draw.RoundedBox( 4,  -246, 404, 492, 292, Color( 48, 96, 139, 255 ) )
				draw.RoundedBox( 4,  -182 - offset, 460 + 40 + 100, 160 + offset * 2, 2, Color( 255, 255, 255, 200 ) )
				draw.DrawText(printName, "WeaponFontSmall", -100, 400 + 40 + 100, Color(255, 255, 255, 200), TEXT_ALIGN_CENTER)
				draw.DrawText("Damage: ", "WeaponFontSmall2", -240, 450 + 14 + 40 + 100, dmgcolor or Color(255, 255, 255, 200), TEXT_ALIGN_LEFT)
				draw.DrawText(damage, "WeaponFontSmall2", 40, 450 + 14 + 40 + 100, dmgcolor or Color(255, 255, 255, 200), TEXT_ALIGN_RIGHT)
				draw.DrawText("Clip Size: ", "WeaponFontSmall2", -240, 478 + 14 + 40 + 100, clipcolor or Color(255, 255, 255, 200), TEXT_ALIGN_LEFT)
				draw.DrawText(clip, "WeaponFontSmall2", 40, 478 + 14 + 40 + 100, clipcolor or Color(255, 255, 255, 200), TEXT_ALIGN_RIGHT)
				draw.DrawText("Delay: ", "WeaponFontSmall2", -240, 506 + 14 + 40 + 100, autocolor or Color(255, 255, 255, 200), TEXT_ALIGN_LEFT)
				draw.DrawText(auto or "False", "WeaponFontSmall2", 40, 506 + 14 + 40 + 100, autocolor or Color(255, 255, 255, 200), TEXT_ALIGN_RIGHT)
				draw.DrawText("Spread: ", "WeaponFontSmall2", -240, 534 + 14 + 40 + 100, spreadcolor or Color(255, 255, 255, 200), TEXT_ALIGN_LEFT)
				draw.DrawText(spread, "WeaponFontSmall2", 40, 534 + 14 + 40 + 100, spreadcolor or Color(255, 255, 255, 200), TEXT_ALIGN_RIGHT)
				draw.DrawText("Recoil: ", "WeaponFontSmall2", -240, 534 + 14 + 40 + 28 + 100, recoilcolor or Color(255, 255, 255, 200), TEXT_ALIGN_LEFT)
				draw.DrawText(recoil, "WeaponFontSmall2", 40, 534 + 14 + 40 + 28 + 100, recoilcolor or Color(255, 255, 255, 200), TEXT_ALIGN_RIGHT)
				draw.DrawText("Slot: ", "WeaponFontSmall2", -240, 534 + 14 + 40 + 28 * 5.5, Color(255, 255, 255, 200), TEXT_ALIGN_LEFT)
				draw.DrawText(ammo, "WeaponFontSmall2", 40, 534 + 14 + 40 + 28 * 5.5, Color(255, 255, 255, 200), TEXT_ALIGN_RIGHT)
			cam.End3D2D()
		end
	end
end
hook.Add( "PostDrawOpaqueRenderables", "Draw the gun UI", DrawWeaponText )