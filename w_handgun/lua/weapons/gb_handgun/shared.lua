if ( SERVER ) then

	AddCSLuaFile ()
	SWEP.Weight			= 5;
	SWEP.AutoSwitchTo		= false;
	SWEP.AutoSwitchFrom		= true;
        --resource.AddFile("materials/vgui/ttt_fun_killicons/hand-gun.png")
        --resource.AddFile("models/weapons/v_pist_finger1.mdl")
        --resource.AddFile("sound/weapons/handgun/bang.wav")
end

SWEP.PrintName			= "'Hand' Gun";
SWEP.Slot			= 1;

if ( CLIENT ) then

	
	SWEP.Author			= "DarkWolfInsanity";
	
	SWEP.SlotPos			= 1;
	SWEP.DrawAmmo			= false;
	SWEP.DrawCrosshairs		= true;
	
end

SWEP.Base                      = "weapon_tttbase"
SWEP.NoSights = true
SWEP.Kind = WEAPON_PISTOL
SWEP.Icon = "vgui/ttt_fun_killicons/hand-gun.png";

SWEP.Spawnable			= true;
SWEP.HoldType			= "pistol"
SWEP.Spawnable			= true
SWEP.iconletter			= R
SWEP.ViewModel			= "models/weapons/v_pist_finger1.mdl"
SWEP.WorldModel			= "models/weapons/w_pistol.mdl"
SWEP.ViewModelFlip		= true
SWEP.ViewModelFOV		= 60

SWEP.FiresUnderwater	= true
SWEP.Category			= "Wolf's Guns"

SWEP.ReloadSound			= "weapons/pistol/pistol_reload1.wav"

SWEP.CSMuzzleFlashes	= true

SWEP.Primary.ClipSize = 24;
SWEP.Primary.DefaultClip = 24;
SWEP.Primary.Recoil = 1;
SWEP.Primary.Damage = 5;
SWEP.Primary.NumberofShots = 5;
SWEP.Primary.Spread = 0;
SWEP.Primary.Delay = 0.35;
SWEP.Primary.Automatic = false;
SWEP.Primary.Ammo = "Pistol";
SWEP.Primary.Sound = "weapons/handgun/bang.wav";
SWEP.Primary.Force = 990000;
SWEP.Primary.TakeAmmo = 1;

SWEP.Secondary.Recoil = 1;
SWEP.Secondary.Spread = 0;
SWEP.Secondary.Delay = 1;
SWEP.Secondary.Sound = "weapons/crossbow/fire1.wav";
SWEP.Secondary.TakeAmmo = 1;

function SWEP:throw_attack (model_file)
    local tr = self.Owner:GetEyeTrace();
    self.Weapon:EmitSound (self.Secondary.Sound);
    self.BaseClass.ShootEffects (self);
    if (!SERVER) then return end;
    local ent = ents.Create ("prop_physics");
    ent:SetModel (model_file);
    ent:SetPos (self.Owner:EyePos() + (self.Owner:GetAimVector() * 16));
    ent:SetAngles (self.Owner:EyeAngles());
    ent:SetPhysicsAttacker(self.Owner)
    ent:Spawn();
    local phys = ent:GetPhysicsObject();
    local shot_length = tr.HitPos:Length();
    phys:ApplyForceCenter (self.Owner:GetAimVector():GetNormalized() * math.pow (shot_length, 3));
    cleanup.Add (self.Owner, "props", ent);
    undo.Create ("Thrown model");
    undo.AddEntity (ent);
    undo.SetPlayer (self.Owner);
    undo.Finish();
end

function SWEP:Initialize()
    util.PrecacheSound(self.Primary.Sound)
     util.PrecacheSound(self.Secondary.Sound)
    if ( SERVER ) then
       self:SetHoldType( self.HoldType or "pistol" )
   end
end

function SWEP:PrimaryAttack()
   if ( !self:CanPrimaryAttack() ) then return end
    self:TakePrimaryAmmo(self.Primary.TakeAmmo)
   local bullet = {};
       bullet.Num = self.Primary.NumberofShots;
       bullet.Src = self.Owner:GetShootPos();
       bullet.Dir = self.Owner:GetAimVector();
       bullet.Spread = Vector( self.Primary.Spread * 0.1 , self.Primary.Spread * 0.1, 0);
       bullet.Tracer = 0;
       bullet.Force = self.Primary.Force;
       bullet.Damage = self.Primary.Damage;
       bullet.AmmoType = self.Primary.Ammo;
   local rnda = -self.Primary.Recoil;
   local rndb = self.Primary.Recoil * math.random(-1, 1);
   self:ShootEffects();
   self.Owner:FireBullets( bullet );
   self.Weapon:EmitSound(Sound(self.Primary.Sound))
   self.Owner:ViewPunch( Angle( rnda,rndb,rnda ) )
   self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
   self.Weapon:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
   
end

function SWEP:SecondaryAttack()
   self:TakePrimaryAmmo(self.Secondary.TakeAmmo)
   self:throw_attack ("models/props/cs_italy/bananna.mdl");
  
   self.Weapon:SetNextPrimaryFire( CurTime() + self.Secondary.Delay )
   self.Weapon:SetNextSecondaryFire( CurTime() + self.Secondary.Delay )
end

function SWEP:Think()
end

function SWEP:Reload()
   self.Weapon:DefaultReload(ACT_VM_RELOAD)
   return true
end

function SWEP:Deploy()
   self.Weapon:SendWeaponAnim(ACT_VM_DRAW);
   --GAMEMODE:SetPlayerSpeed(self.Owner,190,350);
   return true
end

function SWEP:Holster()
   return true
end

function SWEP:OnRemove()
end

function SWEP:OnRestore()
end

function SWEP:Precache()
end

function SWEP:OwnerChanged()
end
