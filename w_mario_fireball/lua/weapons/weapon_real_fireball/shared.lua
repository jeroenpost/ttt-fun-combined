-- Read the weapon_real_base if you really want to know what each action does

if (SERVER) then
	AddCSLuaFile("shared.lua")
	--resource.AddFile("sound/weapons/mario_power/mario_fireball.mp3")
	--resource.AddFile("sound/weapons/mario_power/mario_powerup.mp3")
	--resource.AddFile("materials/vgui/ttt/mario_fireball.png")
        --resource.AddFile("materials/vgui/ttt_fun_killicons/mario_fireball.png")
end 
 
SWEP.Base		= "weapon_real_base"
SWEP.PrintName 		= "Super Mario Fireball"
SWEP.Slot 			= 1

if (CLIENT) then
	
	SWEP.ViewModelFOV		= 70
	
	SWEP.SlotPos 		= 1
	SWEP.IconLetter 		= "C"
end

SWEP.Icon = "vgui/ttt_fun_killicons/mario_fireball.png"
SWEP.Slot 			= 1
SWEP.SlotPos 		= 3
SWEP.ViewModel = Model("models/weapons/v_hands.mdl")
SWEP.WorldModel = ""
SWEP.DrawWorldModel = false
SWEP.PrintName		= "Mario Fireball"
SWEP.Author		= "Buu342"
SWEP.Contact		= "buu342@hotmail.com"
SWEP.Purpose		= ""
SWEP.Instructions	= "Shoot with primary fire."
SWEP.CSMuzzleFlashes    = true
SWEP.ViewModelFOV	= 70
SWEP.Drawammo 		= true
SWEP.DrawCrosshair 	= true
SWEP.Category		= "Mario"
SWEP.Kind = WEAPON_PISTOL
SWEP.AllowDrop = true
SWEP.CanBuy = {ROLE_TRAITOR, ROLE_DETECTIVE} 

SWEP.LimitedStock = false
   SWEP.EquipMenuData = {
      name = "SuperMario Fireball",
      type = "item_weapon",
      desc = "Eat a flower and spew FIREBALLS"
   };

SWEP.data = {}
SWEP.data.newclip	= false

SWEP.Spawnable			= true
SWEP.AdminSpawnable		= true

SWEP.HoldType = "fist"
SWEP.UseHands			= true

local overheat = 0

SWEP.Primary.Sound		= "weapons/mario_power/mario_fireball.mp3"
SWEP.Primary.Recoil		= 0.03
SWEP.Primary.Damage		= 35
SWEP.Primary.NumShots		= -1
SWEP.Primary.Cone		= 0.01
SWEP.Primary.ClipSize		= -1
SWEP.Primary.Delay		= 1.1
SWEP.Primary.DefaultClip	= -1
SWEP.Primary.Automatic		= false
SWEP.Primary.Ammo		= "none"
SWEP.Weight = 5

SWEP.Secondary.ClipSize		= -1
SWEP.Secondary.DefaultClip	= -1
SWEP.Secondary.Automatic	= false
SWEP.Secondary.Ammo		= "none"

SWEP.data 				= {}
SWEP.mode 				= "semi"

SWEP.data.semi 			= {}
SWEP.data.semi.Cone 		= 0.015

SWEP.data.burst 			= {}
SWEP.data.burst.Delay 		= 0.9
SWEP.data.burst.Cone 		= 0.03
SWEP.data.burst.BurstDelay 	= 0.05
SWEP.data.burst.Shots 		= 3
SWEP.data.burst.Counter 	= 0
SWEP.data.burst.Timer 		= 0

function SWEP:Initialize()

	self:SetHoldType( "fist" )

end

function SWEP:PrimaryAttack()
	
	if ( !self:CanPrimaryAttack() or !IsValid(self.Owner) ) then return end
	self.Weapon:SetNextPrimaryFire(CurTime() + 2)
	local shoot_angle = Vector(0,0,0)
	if self.Owner:IsNPC() then
		if self.Owner:GetEnemy() != NULL && self.Owner:GetEnemy() != nil then
			shoot_angle = self.Owner:GetEnemy():GetPos() - self.Owner:GetPos() 
			//apply accuracy
			shoot_angle = shoot_angle + Vector(math.random(-85,85),math.random(-85,85),math.random(-85,85))*(shoot_angle:Distance(Vector(0,0,0))/1500)
			//print("Shoot angle is:")
			//print(shoot_angle)
			shoot_angle:Normalize()
			//print("Normalized, shoot angle is:")
			//print(shoot_angle)
		else
			return
		end
	else
		shoot_angle = self.Owner:GetAimVector()
	end
	local shoot_pos   = self.Owner:GetPos() + self.Owner:GetRight() * 5 + self.Owner:GetUp() * 50 + shoot_angle * 35
	
	if self.Owner:IsPlayer() then
		shoot_pos   = self.Owner:GetPos() + self.Owner:GetRight() * 5 + self.Owner:GetUp() * 55 + shoot_angle * 35
	end
	self:EmitSound(self.Primary.Sound)
	if SERVER then
		
		local shot = ents.Create("sent_fireball")
		if (IsValid(shot)) then
		shot:SetPos(shoot_pos)
		shot:SetAngles(shoot_angle:Angle())
		shot:Activate()
		shot:Spawn()
		end
		self.Weapon:SendWeaponAnim( ACT_VM_PRIMARYATTACK )
		
		local phy = shot:GetPhysicsObject()
		if phy:IsValid() then
			phy:ApplyForceCenter((shoot_angle * 1000))
		end
		
		self.Weapon:SendWeaponAnim( ACT_VM_PRIMARYATTACK )
		self.Owner:SetAnimation( PLAYER_ATTACK1 )
		
		//self.Owner:ViewPunch(Angle( -self.Primary.Recoil, 0, 0 ))
		if !self.Owner:IsNPC() then
		if (self.Primary.TakeAmmoPerBullet) then
			self:TakePrimaryAmmo(self.Primary.NumShots)
		else
			self:TakePrimaryAmmo(1)
		end
	end
end
	self:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
	
		//if not self:CanPrimaryAttack() or self.Owner:WaterLevel() > 2 then return end
	-- If your gun have a problem or if you are under water, you'll not be able to fire

	self.Reloadaftershoot = CurTime() + self.Primary.Delay
	-- Set the reload after shoot to be not able to reload when firering

	//self.Weapon:SetNextSecondaryFire(CurTime() + self.Primary.Delay)
	-- Set next secondary fire after your fire delay

	//self.Weapon:SetNextPrimaryFire(CurTime() + self.Primary.Delay)
	-- Set next primary fire after your fire delay

	-- Emit the gun sound when you fire

	//self:RecoilPower()

	//self:TakePrimaryAmmo(1)
	-- Take 1 ammo in you clip

	if ((game.SinglePlayer() and SERVER) or CLIENT) then
		//self.Weapon:SetNWFloat("LastShootTime", CurTime())
	end
end

function SWEP:Deploy()
self.Weapon:EmitSound(Sound("weapons/mario_power/mario_powerup.mp3"))
end




///////////////////////







