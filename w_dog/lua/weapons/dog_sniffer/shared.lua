if SERVER then
    --resource.AddFile("sound/weapons/dog_sniff.mp3")
end

SWEP.Base = "weapon_tttbase"

SWEP.HoldType = "slam"

SWEP.ViewModel = Model("models/weapons/v_hands.mdl")
SWEP.WorldModel = ""

SWEP.Kind = WEAPON_HEAVY
SWEP.AutoSpawnable = false



SWEP.LimitedStock = true
 
function SWEP:OnDrop()
    return false
end

 SWEP.AllowDrop = false

if CLIENT then
    SWEP.PrintName = "Traitor Sniffer"
    SWEP.Slot      = 2
   
    SWEP.ViewModelFOV  = 50
    --SWEP.ViewModelFlip = true
   
   SWEP.Icon = "vgui/ttt_fun_killicons/defibrillator.png"
   


   
   function SWEP:PrimaryAttack()
      local tr = util.TraceLine({start = self.Owner:EyePos(), endpos = self.Owner:EyePos() + self.Owner:EyeAngles():Forward() * 80, filter = self.Owner})
      if tr.HitNonWorld and IsValid(tr.Entity) and tr.Entity:IsPlayer() and tr.Entity:Alive() then
            self.Owner:EmitSound("weapons/dog_sniff.mp3",400)
         self.StartTime = CurTime()
         self.EndTime = CurTime() + 5
         self.Started = true
      end
   end 
   
   function SWEP:DrawHUD()
      if self.Started then
         --print("1")
         if self.Owner:KeyDown(IN_ATTACK) then
         --print("2")
         local tr = util.TraceLine({start = self.Owner:EyePos(), endpos = self.Owner:EyePos() + self.Owner:EyeAngles():Forward() * 80, filter = self.Owner})
            if tr.HitNonWorld and IsValid(tr.Entity) and tr.Entity:IsPlayer() and tr.Entity:Alive() then
               local mins, maxs = self.Owner:OBBMins(), self.Owner:OBBMaxs()
               
                  local traitor = tr.Entity:GetNWString( "halocolor" )

                  fract = math.TimeFraction(self.StartTime, self.EndTime-4, CurTime())
                  surface.SetDrawColor(Color(255, 0, 0, 255))
                  surface.SetDrawColor(Color(120, 120, 120, 130))
                  surface.DrawRect(ScrW() / 2 - (160 / 2), ScrH() / 2 + 10, 160, 10)
                  surface.SetFont("TabLarge")

                  if self.StartTime + 1 < CurTime() then
                    if traitor == "red" then
                        text = "a Traitor!!!"
                        color = Color(255, 0, 0, 255)
                    else
                        text = "an Innocent"
                        color = Color(0, 0, 255, 255)
                    end 
                    local w, h = surface.GetTextSize("Person is "..text)
                    surface.SetTextPos(ScrW() / 2 - (w / 2), ScrH() / 2 - h)
                    surface.SetTextColor( color )
                    surface.DrawText("Person is "..text)
                    surface.SetDrawColor( color )
                    surface.DrawRect(ScrW() / 2 - (160 / 2), ScrH() / 2 + 10, math.Clamp(160 * fract, 0, 160), 10)
                  else
                    local w, h = surface.GetTextSize("Sniffing...... Sniffing......")
                    surface.SetTextPos(ScrW() / 2 - (w / 2), ScrH() / 2 - h)
                    surface.SetTextColor(Color(255, 0, 0, 255))
                    surface.DrawText("Sniffing...... Sniffing......")
                    surface.SetDrawColor(Color(255, 0, 0, 255))
                    surface.DrawRect(ScrW() / 2 - (160 / 2), ScrH() / 2 + 10, math.Clamp(160 * fract, 0, 160), 10)
                  end
               
            else
               self.StartTime = CurTime()
               self.EndTime = CurTime() + 5
            end
         end
      end
   end
end

if SERVER then
   --resource.AddFile("materials/vgui/ttt/defibrillator.png")
   AddCSLuaFile("shared.lua")
   
   function SWEP:Deploy()
      self.Started = false
   end
   
     
   function SWEP:PrimaryAttack()
      local tr = util.TraceLine({start = self.Owner:EyePos(), endpos = self.Owner:EyePos() + self.Owner:EyeAngles():Forward() * 80, filter = self.Owner})
      if tr.HitNonWorld and IsValid(tr.Entity) and tr.Entity:IsPlayer() and tr.Entity:Alive() then
         self.StartTime = CurTime()
         self.Started = true
         
      end
   end
   
   function SWEP:Think()
      if self.Started then
      --print("1")
         if self.Owner:KeyDown(IN_ATTACK) then
            --print("2")
            local tr = util.TraceLine({start = self.Owner:EyePos(), endpos = self.Owner:EyePos() + self.Owner:EyeAngles():Forward() * 80, filter = self.Owner})
            if tr.HitNonWorld and IsValid(tr.Entity) and tr.Entity:IsPlayer() and tr.Entity:Alive() then
               --print("3")
               local mins, maxs = self.Owner:OBBMins(), self.Owner:OBBMaxs()
               local tr2 = util.TraceHull({start = tr.Entity:LocalToWorld(tr.Entity:OBBCenter()) + vector_up, endpos = tr.Entity:LocalToWorld(tr.Entity:OBBCenter()) + Vector(0, 0, 80), filter = {tr.Entity, self.Owner}, mins = Vector(mins.x * 1.6, mins.y * 1.6, 0), maxs = Vector(maxs.x * 1.6, maxs.y * 1.6, 0), mask = MASK_PLAYERSOLID})
               if !tr2.Hit then
                  --print(CurTime() - self.StartTime)
                  if CurTime() - self.StartTime >= 1 then
                     if tr.Entity:GetRole() == ROLE_TRAITOR then
                        self.Owner:PrintMessage( HUD_PRINTTALK, "Sniffed person is a TRAITOR")
                     else
                        self.Owner:PrintMessage( HUD_PRINTTALK, "Sniffed person is an INNOCENT")
                     end
                     self.Started = false
                  end
               else
                  self.StartTime = CurTime()
               end
            else
               self.StartTime = CurTime()
            end
         else
            self.StartTime = CurTime()
            self.Started = false
         end
      end
   end
   
   
end