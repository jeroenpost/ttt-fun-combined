if ( SERVER ) then

	AddCSLuaFile( "shared.lua" )
	
--resource.AddFile("materials/vgui/ttt_fun_killicons/dog_transformer_traitor.png")
end

-------------------------------------------------------------------
SWEP.Author   = "GreenBlack"
SWEP.Contact        = ""
SWEP.Spawnable      = true
SWEP.AdminSpawnable  = true
-----------------------------------------------
SWEP.FiresUnderwater = true
SWEP.HoldType		= "pistol"
SWEP.ViewModel			= "models/weapons/v_pistol.mdl"
    SWEP.ViewModelFOV = 54
    SWEP.WorldModel = "models/weapons/w_pistol.mdl"
SWEP.ViewModelFlip = false
-----------------------------------------------
SWEP.Primary.Delay			= 0.5
SWEP.Primary.Sound			= Sound( "sounds/bite.mp3" )
SWEP.Primary.Recoil			= 0
SWEP.Primary.Damage			= 0
SWEP.Primary.NumShots		= 1		
SWEP.Primary.Cone			= 0
SWEP.Primary.ClipSize		= -1
SWEP.Primary.DefaultClip	= -1
SWEP.Primary.Automatic   	= true
SWEP.Primary.Ammo         	= "none" 
-------------------------------------------------
SWEP.Secondary.ClipSize		= -1
SWEP.Secondary.Sound		= Sound( "sounds/bark.mp3" )
SWEP.Secondary.Delay		= 1.75
SWEP.Secondary.DefaultClip	= -1
SWEP.Secondary.Automatic	= false
SWEP.Secondary.Ammo			= "none"
-------------------------------------------------
SWEP.AllowDrop = true
SWEP.LimitedStock  = true

SWEP.HoldType			= "pistol"
	SWEP.Weight				= 5
	SWEP.AutoSwitchTo 		= false;
	SWEP.AutoSwitchFrom 	= false;
SWEP.PrintName			= "Traitor Dog"
SWEP.Base = "gb_base"
SWEP.Icon = "vgui/ttt_fun_killicons/dog_transformer_traitor.png"
SWEP.Kind = WEAPON_EQUIP2
	
	SWEP.Author				= "GreenBlack"


	SWEP.Slot				= 7
	SWEP.SlotPos			= 1
	SWEP.IconLetter			= "C"
	SWEP.DrawCrosshair		= false

SWEP.IsUsed = false
SWEP.DoggyEnt = false
SWEP.CanBuy = {ROLE_TRAITOR}

SWEP.EquipMenuData = {
      type  = "item_weapon",
      name  = "Dog Transformer",
      desc  = "Transform a buddy traitor in your powerfull dog.\nCannot be used on innocents!"
      }

x = 0



SWEP.nextThink = 0
function SWEP:Think() 

    if( self.nextThink < CurTime() && self.IsUsed ) then
        self.nextThink = CurTime() + 1;
        if IsValid( self ) and IsValid( self.Owner ) and self.Owner:Alive() and IsValid( self.DoggyEnt ) and self.DoggyEnt:Alive() then
              distance =  self.Owner:GetShootPos():Distance( self.DoggyEnt:GetShootPos() )
              if distance > 550 then
                    local dmg = DamageInfo()
                    dmg:SetDamage(5)
                    dmg:SetAttacker(self.DoggyEnt) 
                    dmg:SetInflictor(self.Weapon)
                    dmg:SetDamageForce(self.Owner:GetAimVector() * 1500)
                    dmg:SetDamagePosition(self.Owner:GetPos())
                    dmg:SetDamageType(DMG_CLUB)
                     local spos = self.DoggyEnt:GetShootPos()
                     local sdest = spos + (self.DoggyEnt:GetAimVector() * 70)
  
                    self.DoggyEnt:DispatchTraceAttack(dmg, spos + (self.DoggyEnt:GetAimVector() * 3), sdest)
                    
                    if SERVER and self.DoggyEnt != self.Owner then
                        self.DoggyEnt:PrintMessage( HUD_PRINTTALK, "Get closer to your owner!")
                    end
              end
        end
    end
end


 
function SWEP:Initialize()
end

SWEP.NextAttack = 0

function SWEP:PrimaryAttack()
        

	local trace = self.Owner:GetEyeTrace()
       local hitEnt = trace.Entity
	if !self.IsUsed and trace.HitPos:Distance(self.Owner:GetShootPos()) <= 950 and IsValid( hitEnt ) and hitEnt:IsPlayer() and hitEnt:Alive() then
                
            if self.NextAttack > CurTime() then
               return 
            end

            self.NextAttack = (CurTime() + 1)
                
            if( specialRound.isSpecialRound or _globals['specialperson'][hitEnt:SteamID()] or hitEnt.NoDogProtector) then
                return 
            end

                if SERVER then
                    if hitEnt:GetRole() != ROLE_TRAITOR then
                        self.Owner:TakeDamage( 40, hitEnt, self.Owner:GetActiveWeapon() )
                        return
                    end
                end
                if CLIENT then
                    local traitor = hitEnt:GetNWString( "halocolor" )
                    if traitor != "red" then
                        return
                    end
                end

                if hitEnt.IsDog then
                    return 
                end

    if SERVER then DamageLog("Dog: " .. self.Owner:Nick() .. " [" .. self.Owner:GetRoleString() .. "] turned "..hitEnt:Nick().." [" .. hitEnt:GetRoleString() .. "] into a dog ")
    end
    self.IsUsed = true
                      self.DoggyEnt = hitEnt
                 if SERVER then
                    if !self.Stripped then
                      self.Stripped = true
                      hitEnt:StripWeapons()
                      
                    end
                    
                    hitEnt:Give("dog_bite")
                    hitEnt:Give("dog_bite_rabies")
                    --hitEnt:Give("dog_sniffer")
                 
                  end
                    hitEnt:SetHealth( hitEnt:Health() + 50 )
                   if CLIENT then
                   -- hitEnt:SetMuted( true )
                   end

                    hitEnt.IsDog = true
                    hitEnt.DogOwner = self.Owner
                    hitEnt.OldModelDog = hitEnt:GetModel()
                    hitEnt:SetModel("models/blacky_dog.mdl") 
                    hitEnt:SetModelScale(1, 3)
                    hitEnt.OldModelScale = hitEnt:GetModelScale()
                    --disable the die sound
                    hitEnt.was_headshot = true
                    
                    if SERVER then
                        hitEnt:EmitSound("weapons/bark.mp3",400)
                   end

                    hook.Add("TTTEndRound","RemoveDoggy"..hitEnt:SteamID(), function()
                        if IsValid( hitEnt ) then
                            hitEnt:SetModel(hitEnt.OldModelDog)
                            hitEnt:SetModelScale(hitEnt.OldModelScale, 0.5)
                             if SERVER then
                                hitEnt.IsDog = false
                                hitEnt:StripWeapon("dog_bite")
                                hitEnt:StripWeapon("dog_bite_rabies")
                                 
                            end
                        end
                    end)
                                      
            
	end

end

function toggleNoise()
	--if(
	end

function SWEP:SecondaryAttack()


end
 

