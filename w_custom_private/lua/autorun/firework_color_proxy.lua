if SERVER then
	AddCSLuaFile()
	return
end

matproxy.Add( 
{
	name	=	"FireworkColor", 

	init	=	function( self, mat, values )

		-- Store the name of the variable we want to set
		self.ResultTo = values.resultvar

	end,

	bind	=	function( self, mat, ent )

		if ( !IsValid( ent ) ) then return end

		local Col = ent:GetDTVector(1)
		
		if ( Col ) then
			if ( isvector( Col ) ) then
				mat:SetVector( self.ResultTo, Col )
			end
		else
			mat:SetVector( self.ResultTo, Vector( 1, 0, 0 ) )
		end

	end 
})