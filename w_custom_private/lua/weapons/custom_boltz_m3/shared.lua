

if ( SERVER ) then

	AddCSLuaFile(  )
--resource.AddFile("sound/weapons/m3/s3-1.mp3")
--resource.AddFile("sound/weapons/m3/s3_insertshell.mp3")
--resource.AddFile("sound/weapons/m3/s3_pump.mp3")
--resource.AddFile("models/weapons/w_shot_g3super90.mdl")
--resource.AddFile("models/weapons/v_shot_g3super90.mdl")
--resource.AddFile("materials/models/weapons/v_models/emess-gb/glowingstuff.vmt")
--resource.AddFile("materials/models/weapons/v_models/emess-gb/glass.vmt")
--resource.AddFile("materials/models/weapons/v_models/emess-gb/body.vmt")
--resource.AddFile("materials/models/weapons/w_models/w_emess-gb/w_body.vmt")
--resource.AddFile("materials/vgui/ttt_fun_killicons/emsss.png")


  
	
end
	SWEP.PrintName			= "Boltz M3"
if ( CLIENT ) then

	
	SWEP.Author				= "Babel Industries (Model by Defuse)"
    SWEP.Contact            = "";
    SWEP.Instructions       = "Electro Magnetic Shelless Slug Shotgun"
	SWEP.Category = "Babel Industries"


	
end


SWEP.Base				= "gb_base_shotgun"
SWEP.Spawnable			= true
SWEP.AdminSpawnable		= false
SWEP.Icon 				= "vgui/ttt_fun_killicons/m3.png"
SWEP.HoldType = "shotgun"
SWEP.ViewModelFOV = 71
SWEP.ViewModelFlip = true
SWEP.ViewModel = "models/weapons/v_shot_m3super90.mdl"
SWEP.WorldModel = "models/weapons/w_shot_m3super90.mdl"
SWEP.Primary.Sound = Sound( "Weapon_M3.Single" )
SWEP.Primary.Recoil = 10
SWEP.reloadtimer = 0

SWEP.IronSightsPos = Vector(-10.74, 12.19, -13.28)
SWEP.IronSightsAng = Vector(0.547, 0, 0)

SWEP.ShowViewModel = true
SWEP.ShowWorldModel = true
SWEP.ViewModelBoneMods = {}

function SWEP:Deploy()
   -- self.Owner:EmitSound( "weapons/barret_arm_draw.mp3" ) ;
	self.Weapon:SendWeaponAnim( ACT_VM_DRAW )
	self:SetHoldType( self.HoldType or "pistol" )
	return true;
end 


SWEP.Weight				= 5
SWEP.AutoSwitchTo		= false
SWEP.AutoSwitchFrom		= false
SWEP.AutoSpawnable 		= false
SWEP.Kind				= WEAPON_HEAVY
SWEP.Primary.Sound			= Sound("weapons/mp7/mp7_fire.mp3")
SWEP.Primary.SoundLevel = 10
SWEP.Primary.Recoil			= 1
SWEP.Primary.Damage			= 15
SWEP.Primary.NumShots		= 16
SWEP.Primary.Cone			= 0.035
SWEP.Primary.ClipSize		= 500
SWEP.Primary.Delay			= 0.9
SWEP.Primary.DefaultClip	= 500
SWEP.Primary.Automatic		= true
SWEP.Primary.Ammo			= "buckshot"
SWEP.AmmoEnt = "item_box_buckshot_ttt"

SWEP.Slot				= 2							// Slot in the weapon selection menu
SWEP.SlotPos			= 1							// Position in the slot

SWEP.Secondary.ClipSize		= -1
SWEP.Secondary.DefaultClip	= -1
SWEP.Secondary.Automatic	= false
SWEP.Secondary.Ammo			= "none"
SWEP.HeadshowMultiplier = 1.8



SWEP.RunArmOffset  = Vector(-0.601, -3.935, -1.461)
SWEP.RunArmAngle 	 = Vector(8.032, -41.885, 0)

function SWEP:IsEquipment() 
	return false
end

BOLT_AIR_VELOCITY	= 3500
BOLT_WATER_VELOCITY	= 1500
BOLT_SKIN_NORMAL	= 0
BOLT_SKIN_GLOW		= 1

CROSSBOW_GLOW_SPRITE	= "sprites/light_glow02_noz.vmt"
CROSSBOW_GLOW_SPRITE2	= "sprites/blueflare1.vmt"


SWEP.PrimaryAnim = ACT_VM_PRIMARYATTACK_SILENCED
SWEP.ReloadAnim = ACT_VM_RELOAD_SILENCED

function SWEP:OnDrop()
    self:Remove()
end

function SWEP:Precache()

    util.PrecacheSound( "Weapon_Crossbow.BoltHitBody" );
    util.PrecacheSound( "Weapon_Crossbow.BoltHitWorld" );
    util.PrecacheSound( "Weapon_Crossbow.BoltSkewer" );

    util.PrecacheModel( CROSSBOW_GLOW_SPRITE );
    util.PrecacheModel( CROSSBOW_GLOW_SPRITE2 );

    self.BaseClass:Precache();

end

function SWEP:SecondaryAttack()
    if not self.IronSightsPos then return end
    if self.Weapon:GetNextSecondaryFire() > CurTime() then return end

    bIronsights = not self:GetIronsights()

    self:SetIronsights( bIronsights )

    if SERVER then
        self:SetZoom(bIronsights)
    end

    self.Weapon:SetNextSecondaryFire( CurTime() + 0.3)
end

function SWEP:PrimaryAttack()
    if ( !self:CanPrimaryAttack() ) then return end


    self:FireBolt(0);


    // Signal a reload
    self.m_bMustReload = true;
    self:TakePrimaryAmmo( 1 );

   

    self.Weapon:EmitSound( self.Primary.Sound );
    -- self.Owner:EmitSound( self.Primary.Special2 );

    self.Weapon:SendWeaponAnim( ACT_VM_PRIMARYATTACK );

    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay );
    self.Weapon:SetNextSecondaryFire( CurTime() + self.Primary.Delay );

    // self:DoLoadEffect();
    // self:SetChargerState( CHARGER_STATE_DISCHARGE );

end

function SWEP:Deploy()
    self.Weapon:SendWeaponAnim(ACT_VM_DRAW_SILENCED)
    return true
end

function SWEP:SetZoom(state)
    if CLIENT or not IsValid(self.Owner) then
        return
    else
        if state then
            self.Owner:SetFOV(20, 0.3)
        else
            self.Owner:SetFOV(0, 0.2)
        end
    end
end



function SWEP:PreDrop()
    self:SetIronsights( false )
    self:SetZoom(false)
end

SWEP.Nextreload = 0
function SWEP:Reload()
    if self.Nextreload > CurTime() then return end
    self.Nextreload = CurTime() + 1
    self.Weapon:DefaultReload( ACT_VM_RELOAD );
    self.Owner:SetAnimation(ACT_RELOAD_PISTOL)
    self.Weapon:SendWeaponAnim(ACT_SHOTGUN_RELOAD_FINISH)
    self:SetIronsights( false )
    self:SetZoom(false)
end


function SWEP:Holster()
    self:SetIronsights(false)
    self:SetZoom(false)
    return true
end

if CLIENT then
    local scope = surface.GetTextureID("sprites/scope")
    function SWEP:DrawHUD()
        if self:GetIronsights() then
            surface.SetDrawColor( 0, 0, 0, 255 )

            local x = ScrW() / 2.0
            local y = ScrH() / 2.0
            local scope_size = ScrH()

            -- crosshair
            local gap = 80
            local length = scope_size
            surface.DrawLine( x - length, y, x - gap, y )
            surface.DrawLine( x + length, y, x + gap, y )
            surface.DrawLine( x, y - length, x, y - gap )
            surface.DrawLine( x, y + length, x, y + gap )

            gap = 0
            length = 50
            surface.DrawLine( x - length, y, x - gap, y )
            surface.DrawLine( x + length, y, x + gap, y )
            surface.DrawLine( x, y - length, x, y - gap )
            surface.DrawLine( x, y + length, x, y + gap )


            -- cover edges
            local sh = scope_size / 2
            local w = (x - sh) + 2
            surface.DrawRect(0, 0, w, scope_size)
            surface.DrawRect(x + sh - 2, 0, w, scope_size)

            surface.SetDrawColor(255, 0, 0, 255)
            surface.DrawLine(x, y, x + 1, y + 1)

            -- scope
            surface.SetTexture(scope)
            surface.SetDrawColor(255, 255, 255, 255)

            surface.DrawTexturedRectRotated(x, y, scope_size, scope_size, 0)

        else
            return self.BaseClass.DrawHUD(self)
        end
    end

    function SWEP:AdjustMouseSensitivity()
        return (self:GetIronsights() and 0.2) or nil
    end
end

function SWEP:FireBolt( con )

    if ( self.Weapon:Clip1() <= 0 ) then

    return;
    end

    local pOwner = self.Owner;

    if ( pOwner == NULL ) then
        return;
    end

    if ( SERVER ) then


    local angAiming;


    local count2 = 0
    if con == 1 then cone = 0.001 else cone = 0.00000001 end
   -- while count2 < 10 do
        count2 = count2 + 1


             vecAiming		= pOwner:GetAimVector() ;
             vecSrc		= pOwner:GetShootPos();
            angAiming = vecAiming:Angle() ;
            self:ShootBullet( 90, 1, cone )


        local pBolt = ents.Create (  "crossbow_bolt" );
        pBolt:SetPos( vecSrc );
        pBolt:SetAngles( angAiming );

        pBolt.Damage = 0


        pBolt.AmmoType = self.Primary.Ammo;
        pBolt:SetOwner( pOwner );
        pBolt:Spawn()

        if ( pOwner:WaterLevel() == 3 ) then
            pBolt:SetVelocity( vecAiming * BOLT_WATER_VELOCITY );
        else
            pBolt:SetVelocity( vecAiming * BOLT_AIR_VELOCITY );
        end

    end



end

