if SERVER then
   AddCSLuaFile(  )
end
   
SWEP.HoldType = "duel"
   
 SWEP.PrintName = "Fallen Angels"
SWEP.Slot = 5
if CLIENT then
   
   
   SWEP.Author = "GreenBlack"
 
   SWEP.Icon = "vgui/ttt_fun_killicons/elites.png"
end
 
SWEP.Base = "gb_base"

SWEP.Kind = WEAPON_EQUIP1
SWEP.Base = "weapon_tttbase"
SWEP.Primary.Recoil     = 0.05
SWEP.Primary.Damage = 2
SWEP.Primary.Delay = 0.30
SWEP.Primary.Cone = 0.1
SWEP.Primary.ClipSize = 500
SWEP.Primary.Automatic = true
SWEP.Primary.DefaultClip = 500
SWEP.Primary.ClipMax = 500
SWEP.Primary.Ammo = "Pistol"
SWEP.Primary.NumShots = 16

SWEP.Secondary.Delay = 0.30
SWEP.Secondary.ClipSize     = 40
SWEP.Secondary.DefaultClip  = 40
SWEP.Secondary.Automatic    = true
SWEP.Secondary.Ammo         = "Pistol"
SWEP.Secondary.ClipMax      = 60

SWEP.AutoSpawnable = false
SWEP.AmmoEnt = "item_ammo_pistol_ttt"
SWEP.NoSights = true

SWEP.ViewModel  = "models/weapons/v_pist_elite.mdl"
SWEP.WorldModel = "models/weapons/w_pist_elite.mdl"

SWEP.Primary.Sound = Sound( "Weapon_Elite.Single" )
SWEP.Secondary.Sound = Sound( "Weapon_Elite.Single" )
SWEP.HeadshotMultiplier = 1

function SWEP:Think()

	if self.Owner:KeyPressed(IN_ATTACK)  then
	-- When the left click is pressed, then
		self.ViewModelFlip = true
	end

	if self.Owner:KeyPressed(IN_ATTACK2)  then
	-- When the right click is pressed, then
		self.ViewModelFlip = false
	end
end

function SWEP:PrimaryAttack( worldsnd )
 
   self.Weapon:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
   self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )

   if not self:CanPrimaryAttack() then return end

   if not worldsnd then
      self.Weapon:EmitSound( self.Primary.Sound, self.Primary.SoundLevel )
   elseif SERVER then
      sound.Play(self.Primary.Sound, self:GetPos(), self.Primary.SoundLevel)
   end

   self:ShootBullet( self.Primary.Damage, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )

   self:TakePrimaryAmmo( 0 )

   local owner = self.Owner
   if not IsValid(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

 --  owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )
end

function SWEP:CanSecondaryAttack()
   if not IsValid(self.Owner) then return end

   if self.Weapon:Clip1() <= 0 then
      self:DryFire(self.SetNextSecondaryFire)
      return false
   end
   return true
end

function SWEP:SecondaryAttack(worldsnd)
       

   self.Weapon:SetNextSecondaryFire( CurTime() + self.Secondary.Delay )
   self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )

   if not self:CanSecondaryAttack() then return end

   if not worldsnd then
      self.Weapon:EmitSound( self.Primary.Sound, self.Primary.SoundLevel )
   elseif SERVER then
      sound.Play(self.Primary.Sound, self:GetPos(), self.Primary.SoundLevel)
   end

   self:ShootBullet( self.Primary.Damage, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )

   self:TakePrimaryAmmo( 1 )

   local owner = self.Owner
   if not IsValid(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

  -- owner:ViewPunch( Angle( math.Rand(-0.6,-0.6) * self.Primary.Recoil, math.Rand(-0.6,0.6) *self.Primary.Recoil, 0 ) )
              
end
