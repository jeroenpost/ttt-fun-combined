if SERVER then
   AddCSLuaFile(  )
	--resource.AddFile("sound/gdeagle/gdeagle-1.mp3")
	--resource.AddFile("materials/vgui/ttt_fun_killicons/magnum.png")
end
   
SWEP.HoldType = "pistol"
SWEP.PrintName = "Suicidal's Last Life"
  SWEP.Slot = 6

if CLIENT then
   			
   SWEP.Author = "GreenBlack"
   SWEP.ViewModelFlip = false
 
   SWEP.Icon = "vgui/ttt_fun_killicons/magnum.png"


end

SWEP.Base = "aa_base"
SWEP.Spawnable = true
SWEP.AdminSpawnable = true
SWEP.Kind = WEAPON_EQUIP1
SWEP.WeaponID = AMMO_DEAGLE

SWEP.Primary.Ammo = "AlyxGun"
SWEP.Primary.Recoil = 8
SWEP.Primary.Damage = 50
SWEP.Primary.Delay = 0.38
SWEP.Primary.Cone = 0.02
SWEP.Primary.ClipSize = 18
SWEP.Primary.ClipMax = 40
SWEP.Primary.DefaultClip = 18
SWEP.Primary.Automatic = false

SWEP.HeadshotMultiplier = 3

SWEP.AutoSpawnable = false
SWEP.AmmoEnt = "item_ammo_revolver_ttt"
SWEP.Primary.Sound = Sound("Weapon_357.Single")
SWEP.ViewModel = "models/weapons/v_357.mdl"
SWEP.WorldModel = "models/weapons/w_357.mdl"

SWEP.IronSightsPos = Vector( -5.47, 2.92, 2.55 )
SWEP.IronSightsAng = Vector( 0.55, 0.55, 0.00 )

function SWEP:PrimaryAttack(worldsnd)
    self.Weapon:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
    if not self:CanPrimaryAttack() then return end
    if not worldsnd then
        self.Weapon:EmitSound( self.Primary.Sound, self.Primary.SoundLevel )
    elseif SERVER then
        WorldSound(self.Primary.Sound, self:GetPos(), self.Primary.SoundLevel)
    end

    local bullet = {}
    bullet.Num    = self.Primary.NumShots
    bullet.Src    = self.Owner:GetShootPos()
    bullet.Dir    = self.Owner:GetAimVector()
    bullet.Spread = Vector( self:GetPrimaryCone(), self:GetPrimaryCone(), 0 )
    bullet.Tracer = 1
    bullet.TracerName = "AirboatGunHeavyTracer"
    bullet.Force  = 5000
    bullet.Damage = self.Primary.Damage
    self.Owner:FireBullets( bullet )
    self:ShootBullet( 0, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )

    if ( self.Owner:IsValid() ) then
        local tr = self.Owner:GetEyeTrace()
        local effectdata = EffectData()
        effectdata:SetOrigin(tr.HitPos)
        effectdata:SetNormal(tr.HitNormal)
        effectdata:SetScale(1)
        -- util.Effect("effect_mad_ignition", effectdata)
        util.Decal("FadingScorch", tr.HitPos + tr.HitNormal, tr.HitPos - tr.HitNormal)

        local tracedata = {}
        tracedata.start = tr.HitPos
        tracedata.endpos = Vector(tr.HitPos.x, tr.HitPos.y, tr.HitPos.z - 10)
        tracedata.filter = tr.HitPos
        local tracedata = util.TraceLine(tracedata)
        if SERVER then
            if tracedata.HitWorld  then
                local flame = ents.Create("env_fire");
                flame:SetPos(tr.HitPos + Vector( 0, 0, 1 ));
                flame:SetKeyValue("firesize", "10");
                flame:SetKeyValue("fireattack", "10");
                flame:SetKeyValue("StartDisabled", "0");
                flame:SetKeyValue("health", "10");
                flame:SetKeyValue("firetype", "0");
                flame:SetKeyValue("damagescale", "5");
                flame:SetKeyValue("spawnflags", "128");
                flame:SetPhysicsAttacker(self.Owner)
                flame:SetOwner( self.Owner )
                flame:Spawn();
                flame:Fire("StartFire",0);
            end
        end
        self:TakePrimaryAmmo( 1 )
        local owner = self.Owner
        --if not ValidEntity(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

        owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )
    end
end
SWEP.flyammo = 5
SWEP.nextFly = 0
function SWEP:SecondaryAttack()
    self:Fly()
end

