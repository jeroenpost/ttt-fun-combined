if SERVER then AddCSLuaFile()end
SWEP.Kind = WEAPON_MELEE
SWEP.HoldType = "knife"
   SWEP.PrintName    = "King's Knife"
   SWEP.Slot         = 0

if CLIENT then


  
   SWEP.ViewModelFlip = false

 

   SWEP.Icon = "vgui/ttt_fun_killicons/knife.png"
end

SWEP.Base               = "aa_base"

SWEP.ViewModel          = "models/weapons/v_knife_t.mdl"
SWEP.WorldModel         = ''
SWEP.ShowWorldModel     = false 

SWEP.DrawCrosshair      = false
SWEP.Primary.Damage         = 50
SWEP.Primary.ClipSize       = 1
SWEP.Primary.DefaultClip    = 1
SWEP.Primary.Automatic      = true
SWEP.Primary.Delay = 0.4
SWEP.Primary.Ammo       = "none"
SWEP.Secondary.ClipSize     = -1
SWEP.Secondary.DefaultClip  = -1
SWEP.Secondary.Automatic    = true
SWEP.Secondary.Ammo     = "none"
SWEP.Secondary.Delay = 1.4
SWEP.AllowDrop = false


SWEP.IsSilent = true

-- Pull out faster than standard guns
SWEP.DeploySpeed = 2

function SWEP:PrimaryAttack()
   self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
   self.Weapon:SetNextSecondaryFire( CurTime() + self.Secondary.Delay )

   self.Owner:LagCompensation(true)

   local spos = self.Owner:GetShootPos()
   local sdest = spos + (self.Owner:GetAimVector() * 70)

   local kmins = Vector(1,1,1) * -10
   local kmaxs = Vector(1,1,1) * 10

   local tr = util.TraceHull({start=spos, endpos=sdest, filter=self.Owner, mask=MASK_SHOT_HULL, mins=kmins, maxs=kmaxs})

   -- Hull might hit environment stuff that line does not hit
   if not IsValid(tr.Entity) then
      tr = util.TraceLine({start=spos, endpos=sdest, filter=self.Owner, mask=MASK_SHOT_HULL})
   end

   local hitEnt = tr.Entity

   -- effects
   if IsValid(hitEnt) then
      self.Weapon:SendWeaponAnim( ACT_VM_HITCENTER )

      local edata = EffectData()
      edata:SetStart(spos)
      edata:SetOrigin(tr.HitPos)
      edata:SetNormal(tr.Normal)
      edata:SetEntity(hitEnt)

      if hitEnt:IsPlayer() or hitEnt:GetClass() == "prop_ragdoll" then
         util.Effect("BloodImpact", edata)
      end
   else
      self.Weapon:SendWeaponAnim( ACT_VM_MISSCENTER )
   end

   if SERVER then
      self.Owner:SetAnimation( PLAYER_ATTACK1 )
   end


   if SERVER and tr.Hit and tr.HitNonWorld and IsValid(hitEnt) then
      if hitEnt:IsPlayer() then

        self.Owner:SetHealth(self.Owner:Health()+10)
         -- knife damage is never karma'd, so don't need to take that into
         -- account we do want to avoid rounding error strangeness caused by
         -- other damage scaling, causing a death when we don't expect one, so
         -- when the target's health is close to kill-point we just kill
         if hitEnt:Health() < (self.Primary.Damage + 2) then
            self:StabKill(tr, spos, sdest)
         else
            local dmg = DamageInfo()
            dmg:SetDamage(self.Primary.Damage)
            dmg:SetAttacker(self.Owner)
            dmg:SetInflictor(self.Weapon or self)
            dmg:SetDamageForce(self.Owner:GetAimVector() * 5)
            dmg:SetDamagePosition(self.Owner:GetPos())
            dmg:SetDamageType(DMG_SLASH)

            hitEnt:DispatchTraceAttack(dmg, spos + (self.Owner:GetAimVector() * 3), sdest)
         end
      end
   end

   self.Owner:LagCompensation(false)
end

function SWEP:StabKill(tr, spos, sdest)
   local target = tr.Entity

   local dmg = DamageInfo()
   dmg:SetDamage(49)
   dmg:SetAttacker(self.Owner)
   dmg:SetInflictor(self.Weapon or self)
   dmg:SetDamageForce(self.Owner:GetAimVector())
   dmg:SetDamagePosition(self.Owner:GetPos())
   dmg:SetDamageType(DMG_SLASH)

   -- now that we use a hull trace, our hitpos is guaranteed to be
   -- terrible, so try to make something of it with a separate trace and
   -- hope our effect_fn trace has more luck

   -- first a straight up line trace to see if we aimed nicely
   local retr = util.TraceLine({start=spos, endpos=sdest, filter=self.Owner, mask=MASK_SHOT_HULL})

   -- if that fails, just trace to worldcenter so we have SOMETHING
   if retr.Entity != target then
      local center = target:LocalToWorld(target:OBBCenter())
      retr = util.TraceLine({start=spos, endpos=center, filter=self.Owner, mask=MASK_SHOT_HULL})
   end


   -- create knife effect creation fn
   local bone = retr.PhysicsBone
   local pos = retr.HitPos
   local norm = tr.Normal
   local ang = Angle(-28,0,0) + norm:Angle()
   ang:RotateAroundAxis(ang:Right(), -90)
   pos = pos - (ang:Forward() * 7)

   local prints = self.fingerprints
   local ignore = self.Owner

   target.effect_fn = function(rag)
                         -- we might find a better location
                         local rtr = util.TraceLine({start=pos, endpos=pos + norm * 40, filter=ignore, mask=MASK_SHOT_HULL})

                         if IsValid(rtr.Entity) and rtr.Entity == rag then
                            bone = rtr.PhysicsBone
                            pos = rtr.HitPos
                            ang = Angle(-28,0,0) + rtr.Normal:Angle()
                            ang:RotateAroundAxis(ang:Right(), -90)
                            pos = pos - (ang:Forward() * 10)

                         end

                         local knife = ents.Create("prop_physics")
                         knife:SetModel("models/weapons/w_knife_t.mdl")
                         knife:SetPos(pos)
                         knife:SetCollisionGroup(COLLISION_GROUP_DEBRIS)
                         knife:SetAngles(ang)
                         knife.CanPickup = false

                         knife:Spawn()

                         local phys = knife:GetPhysicsObject()
                         if IsValid(phys) then
                            phys:EnableCollisions(false)
                         end

                         constraint.Weld(rag, knife, bone, 0, 0, true)

                         -- need to close over knife in order to keep a valid ref to it
                         rag:CallOnRemove("ttt_knife_cleanup", function() SafeRemoveEntity(knife) end)
                      end


   -- seems the spos and sdest are purely for effects/forces?
   target:DispatchTraceAttack(dmg, spos + (self.Owner:GetAimVector() * 3), sdest)

   -- target appears to die right there, so we could theoretically get to
   -- the ragdoll in here...

   --self:Remove()      
end

function SWEP:PreDrop()
   -- for consistency, dropped knife should not have DNA/prints
   self.fingerprints = {}
end

function SWEP:OnRemove()
   if CLIENT and IsValid(self.Owner) and self.Owner == LocalPlayer() and self.Owner:Alive() then
      RunConsoleCommand("lastinv")
   end
end

if CLIENT then
   function SWEP:DrawHUD()
      local tr = self.Owner:GetEyeTrace(MASK_SHOT)

      if tr.HitNonWorld and IsValid(tr.Entity) and tr.Entity:IsPlayer()
         and tr.Entity:Health() < (self.Primary.Damage + 10) then

         local x = ScrW() / 2.0
         local y = ScrH() / 2.0

         surface.SetDrawColor(255, 0, 0, 255)

         local outer = 20
         local inner = 10
         surface.DrawLine(x - outer, y - outer, x - inner, y - inner)
         surface.DrawLine(x + outer, y + outer, x + inner, y + inner)

         surface.DrawLine(x - outer, y + outer, x - inner, y + inner)
         surface.DrawLine(x + outer, y - outer, x + inner, y - inner)

         draw.SimpleText("INSTANT KILL", "TabLarge", x, y - 30, COLOR_RED, TEXT_ALIGN_CENTER, TEXT_ALIGN_BOTTOM)
      end         

      return self.BaseClass.DrawHUD(self)
   end
end

SWEP.NextDeploy = 0

function SWEP:SecondaryAttack()
    if self.Owner:KeyDown(IN_USE) then
        if self.NextDeploy < CurTime() then
                self.NextDeploy = CurTime() + 60
            self:throw_attack ( table.Random(self.SecondaryProps) ,nil, 10);
        end
        return
    end
    self:Fly()
end


SWEP.SecondaryProps = {
    "models/props_c17/oildrum001_explosive.mdl",
    "models/props_c17/oildrum001_explosive.mdl",
}
-- BARREL
function SWEP:Reload()
    if self.NextDeploy < CurTime() then
        self.NextDeploy = CurTime() + 1
        local soundfile = gb_config.websounds.."cpzombielustforblood.mp3"
        gb_PlaySoundFromServer(soundfile, self.Owner)

        if SERVER then
            if self.RemoteBoom then self.RemoteBoom:Remove() end
            local ball = ents.Create("kings_knife_trow");

            if (ball) then
                ball:SetPos(self.Owner:GetPos() + Vector(0,0,48));
                ball:SetPhysicsAttacker(self.Owner)
                ball.Owner = self.Owner
                ball:Spawn();
                --ball:SetModelScale( ball:GetModelScale() * 0.1, 0.01 )
                local physicsObject = ball:GetPhysicsObject();
                physicsObject:ApplyForceCenter(self.Owner:GetAimVector() * 1000);
                self.RemoteBoom= ball;
            end;
        end

    end
end



function SWEP:throw_attack (model_file)
    local tr = self.Owner:GetEyeTrace();

    if IsValid( tr.Entity )  then
        tr.Entity.hasProtectionSuit= true
    end

    self.BaseClass.ShootEffects (self);
    if (!SERVER) then return end;
    local ent = ents.Create ("prop_physics");
    ent:SetModel (model_file);
    ent:SetPos (self.Owner:EyePos() + (self.Owner:GetAimVector() * 16));
    ent:SetAngles (self.Owner:EyeAngles());
    ent:SetPhysicsAttacker(self.Owner)
    ent:Spawn();
    ent:SetCollisionGroup( COLLISION_GROUP_DEBRIS );

    ent:SetHealth(25)
    local phys = ent:GetPhysicsObject();
    local shot_length = tr.HitPos:Length()+10;
    phys:SetMass( 1 )
    phys:ApplyForceCenter (self.Owner:GetAimVector():GetNormalized() * math.pow (shot_length, 3));


end



local ENT = {}

ENT.Type = "anim"
ENT.Base = "base_anim"


if SERVER then

    function ENT:Initialize()


        self:SetModel("models/weapons/w_knife_t.mdl")
        self.Entity:PhysicsInit(SOLID_VPHYSICS);
        self.Entity:SetMoveType(MOVETYPE_VPHYSICS);
        self.Entity:SetSolid(SOLID_VPHYSICS);
        self.Entity:SetCollisionGroup(COLLISION_GROUP_PROJECTILE)
        self:SetAngles(self:GetAngles() + Angle(0,0,90))

        local phys = self:GetPhysicsObject()
        if phys:IsValid() then
            phys:Wake()
            phys:EnableGravity(true);
        end

    end

    function ENT:Touch( ent )
        if ent:IsPlayer() and not ent.nextsandtouch or (ent.nextsandtouch and ent.nextsandtouch  < CurTime()) then
            ent.nextsandtouch = CurTime() + 3
            ent:Ignite(0.5,0)
            local dmginfo = DamageInfo()
            local inflictor = nil
            if IsValid(self.Owner) then inflictor = self.Owner else inflictor = self end
            self:SetPhysicsAttacker(inflictor)
            dmginfo:SetDamage( self.damage or 50 ) --50 damage
            dmginfo:SetDamageType( DMG_BULLET ) --Bullet damage
            dmginfo:SetAttacker( inflictor) --First player found gets credit
            dmginfo:SetDamageForce( Vector( 0, 0, 50 ) ) --Launch upwards
            dmginfo:SetInflictor(inflictor)
            ent:TakeDamageInfo( dmginfo )
        end
    end

end

scripted_ents.Register( ENT, "kings_knife_trow", true )
