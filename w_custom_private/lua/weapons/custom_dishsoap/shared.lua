if SERVER then
   AddCSLuaFile(  )
   --resource.AddFile("materials/vgui/ttt_fun_killicons/ak47.png") 
end

SWEP.HoldType = "ar2"
SWEP.PrintName = "Orb's AK"
SWEP.Slot = 2

if CLIENT then
   
   SWEP.Author = "GreenBlack"
   
   SWEP.Icon = "vgui/ttt_fun_killicons/ak47.png"
end
 
SWEP.Base = "weapon_tttbase"
SWEP.Spawnable = true
SWEP.AdminSpawnable = true
SWEP.AutoSpawnable = false
SWEP.Kind = WEAPON_HEAVY
SWEP.Primary.Delay = 0.13
SWEP.Primary.Recoil = 3
SWEP.Primary.Automatic = false
SWEP.Primary.Ammo = "smg1"
SWEP.Primary.Damage = 30
SWEP.Primary.Cone = 0.018
SWEP.Primary.ClipSize = 30
SWEP.Primary.ClipMax = 50
SWEP.Primary.DefaultClip = 50
SWEP.AmmoEnt = "item_ammo_smg1_ttt"
SWEP.ViewModel = "models/weapons/v_rif_ak47.mdl"
SWEP.WorldModel = "models/weapons/w_rif_ak47.mdl"
SWEP.Primary.Sound = Sound("weapons/ak47/ak47-1.wav")

SWEP.HeadshotMultiplier = 2

SWEP.IronSightsPos = Vector( 6.05, -5.30, 1.80 )
SWEP.IronSightsAng = Vector( 3.28, 0, 0 )