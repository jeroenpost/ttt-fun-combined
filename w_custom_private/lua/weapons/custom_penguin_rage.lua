include "weapons/weapon_zm_shotgun.lua"
SWEP.Base = "aa_base"
SWEP.ViewModelFOV	= 65
SWEP.HoldType = "smg"
SWEP.PrintName = "Penguin Rage"
SWEP.ViewModel		= "models/weapons/cstrike/c_shot_m3super90.mdl"
SWEP.WorldModel		= "models/weapons/w_shot_m3super90.mdl"
SWEP.AutoSpawnable = false
SWEP.ViewModelFlip = false
SWEP.UseHands = true

SWEP.HoldType = "shotgun"
SWEP.Slot = 2
SWEP.Kind = WEAPON_HEAVY
SWEP.Camo = 0
if CLIENT then

    SWEP.Author = "GreenBlack"

    SWEP.Icon = "vgui/ttt_fun_killicons/xm1014.png"
end

SWEP.VElements = {
    ["eotech"] = { type = "Model", model = "models/penguin/penguin.mdl", bone = "v_weapon.M3_PARENT", pos = Vector(-0.239, -4.683, -1.27), angle = Angle(90, -90, 0), size = Vector(0.2,0.2,0.2), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
}

function SWEP:PreDrop()
    if CLIENT then return true end
    self.Owner:SetFunVar("has_penguin_rage",0)
    return true
end
function SWEP:Holster()
    if CLIENT or not self.Owner.SetFunVar then return true end
    self.Owner:SetFunVar("has_penguin_rage",0)
    return true
end
function SWEP:Deploy()
    self.Owner:SetFunVar("has_penguin_rage",1)
    return self.BaseClass.Deploy(self)
end

SWEP.Primary.Sound			= Sound( "Weapon_M3.Single" )
SWEP.Primary.Recoil			= 7

SWEP.Primary.Ammo = "Buckshot"
SWEP.Primary.Damage = 18
SWEP.Primary.Cone = 0.085
SWEP.Primary.Delay = 1
SWEP.Primary.ClipSize = 12
SWEP.Primary.ClipMax = 24
SWEP.Primary.DefaultClip = 12
SWEP.Primary.Automatic = true
SWEP.Primary.NumShots = 8
SWEP.AutoSpawnable      = false
SWEP.AmmoEnt = "item_box_buckshot_ttt"


SWEP.IronSightsPos = Vector(-7.65, -12.214, 3.2)
SWEP.IronSightsAng = Vector(0, 0, 0)

function SWEP:SecondaryAttack()
    self:Fly()
end

SWEP.NextBomb = 0
function SWEP:PrimaryAttack()
    if self:Clip1() > 0 and  self.NextBomb < CurTime() then
        self:Disorientate()
        self:Freeze()

            self.NextBomb = CurTime() + 0.6
            self:throw_attack("sandwich_penguin_bomb",35000)

    end

end


local ENT = {}

ENT.Type = "anim"
ENT.Base = "base_anim"


if SERVER then

    function ENT:Initialize()
        self:SetModel("models/penguin/penguin.mdl")
        self:SetModelScale(self:GetModelScale()*1,0)
        self.Entity:PhysicsInit(SOLID_VPHYSICS);
        self.Entity:SetMoveType(MOVETYPE_VPHYSICS);
        self.Entity:SetSolid(SOLID_VPHYSICS);
        self.CanBoom = CurTime() + 3
        self.Angles = -360
        self.damage = 50

        -- self.Entity:SetCollisionGroup(COLLISION_GROUP_WEAPON)
        local phys = self:GetPhysicsObject()
        if phys:IsValid() then
            phys:Wake()
            phys:EnableGravity(true);
        end

        timer.Create(self:EntIndex().."sandboom2",6,0,function()
            if IsValid(self) then
                self:EmitSound( Sound( "C4.PlantSound" ))
                self:SetColor(Color(0,0,math.Rand(50,255),255))
                -- self:SetAngles(Angle(0,math.Rand(0,360),0))
                self:Remove()
            end
        end)

    end



    function ENT:Touch( ent )
        if ent:IsPlayer() and not ent.nextsandtouch or (ent.nextsandtouch and ent.nextsandtouch < CurTime()) then
            ent.hasProtectionSuit = true
            ent.nextsandtouch = CurTime() + 0.5
            ent:Ignite(0.5,0)
            local dmginfo = DamageInfo()
            dmginfo:SetDamage( self.damage ) --50 damage
            dmginfo:SetDamageType( DMG_BULLET ) --Bullet damage
            dmginfo:SetAttacker( self.Owner or ent ) --First player found gets credit
            dmginfo:SetDamageForce( Vector( 0, 0, 50 ) ) --Launch upwards
            dmginfo:SetInflictor(self)
            ent:TakeDamageInfo( dmginfo )
            self:Remove()
        end
    end



end

scripted_ents.Register( ENT, "sandwich_penguin_bomb", true )