////////////////////////////
/////CHAINSAW SWEP /////
/////////by CMasta/////////
////////////////////////////

if ( SERVER ) then

	AddCSLuaFile(  )
	
	
	
--resource.AddFile( "sound/weapons/doom_chainsaw/knife_deploy2.mp3")
--resource.AddFile( "sound/weapons/doom_chainsaw/knife_stab3.mp3")
--resource.AddFile( "sound/weapons/doom_chainsaw/knife_stab1.mp3")
--resource.AddFile( "sound/weapons/doom_chainsaw/knife_deploy1.mp3")
--resource.AddFile( "sound/weapons/doom_chainsaw/knife_slash1.mp3")
--resource.AddFile( "sound/weapons/doom_chainsaw/knife_idle.mp3")
--resource.AddFile( "sound/weapons/doom_chainsaw/knife_stab2.mp3")
--resource.AddFile( "sound/weapons/doom_chainsaw/knife_hitwall1.mp3")
--resource.AddFile( "models/weapons/v_chain_s.mdl")
--resource.AddFile( "materials/models/weapons/v_models/d3_chainsaw/chainsaw_ref.vtf")
--resource.AddFile( "materials/models/weapons/v_models/d3_chainsaw/chainsaw.vmt")
--resource.AddFile( "materials/models/weapons/v_models/d3_chainsaw/chain.vmt")
--resource.AddFile( "materials/vgui/ttt_fun_killicons/chainsaw.png")
	
	SWEP.HoldType			= "shotgun"
	
end
SWEP.PrintName			= "The Redeemer"
SWEP.SlotPos			= 7

if ( CLIENT ) then

				
	SWEP.Author				= "CMasta -ZsN-"

	SWEP.Slot				= 3
	
	SWEP.ViewModelFOV		= 60
	SWEP.IconLetter			= "x"

end



SWEP.Spawnable				= true
SWEP.AdminSpawnable			= true
SWEP.Kind = WEAPON_GRENADE

SWEP.Base   = "weapon_tttbase"
SWEP.Icon = "vgui/ttt_fun_killicons/chainsaw.png"
SWEP.ViewModelFlip = false

-----------------------Main functions----------------------------
 
function SWEP:Think() -- Called every frame
end

sounds = {
	"deploy1",
	"deploy2",
	"hitwall1",
	"idle",
	"slash1", 
	"stab1",
	"stab2",
	"stab3"
	}

function SWEP:Initialize()
	for k, v in pairs ( sounds ) do
		util.PrecacheSound( "weapons/doom_chainsaw/knife_"..v..".mp3" )
	end
end
 
 
function SWEP:PrimaryAttack()
local tr = self.Owner:GetEyeTrace()
	tr.endpos = self.Owner:GetPos() + (self.Owner:GetAimVector() * 175)
local length = (self.Owner:GetShootPos() - tr.Entity:GetPos()):Length()
	
	
	if length <= 285 then
		if tr.Entity:IsValid() and tr.Entity:IsNPC() or tr.Entity:IsPlayer() then	
			self.Weapon:EmitSound("weapons/doom_chainsaw/knife_stab"..math.random( 1, 3 )..".mp3")
			self.Weapon:SendWeaponAnim( ACT_VM_HITCENTER )
			util.BlastDamage(self.Owner, self.Owner, tr.HitPos, 4, math.random( 5, 99 ) )
			local effectdata = EffectData()
				effectdata:SetOrigin( tr.HitPos )
				for i=1, 5 do
					util.Effect( "bloodsplash_effect", effectdata )
				end
		elseif !tr.HitWorld then
			self.Weapon:EmitSound("weapons/doom_chainsaw/knife_hitwall1.mp3")
			self.Weapon:SendWeaponAnim( ACT_VM_HITCENTER )
			util.BlastDamage(self.Owner, self.Owner, tr.HitPos, 4, math.random( 20, 50 ) )			
			
			local Pos1 = tr.HitPos + tr.HitNormal
			local Pos2 = tr.HitPos - tr.HitNormal
				util.Decal( "ManhackCut", Pos1, Pos2 )
		end
	else
		self.Weapon:EmitSound( "weapons/doom_chainsaw/knife_slash1.mp3" )
		self.Weapon:SendWeaponAnim( ACT_VM_MISSCENTER )
	end
	self.Weapon:SetNextPrimaryFire( CurTime() + 0.3 )
end
 
function SWEP:SecondaryAttack()
	return false
end

function SWEP:Reload()
	return false
end

function SWEP:OnDrop()
	self:Remove()
end


function SWEP:Deploy()
	self.Weapon:EmitSound( "weapons/doom_chainsaw/knife_deploy"..math.random( 1, 2 )..".mp3" )
	self.Weapon:SendWeaponAnim( ACT_VM_DRAW )
	timer.Create( "IdleSound", 2.99, 0, function()
	if self.Weapon then
	     	self.Weapon:EmitSound( "weapons/doom_chainsaw/knife_idle.mp3")
			end
     end)
end

function SWEP:Holster()
	timer.Destroy( "IdleSound" )
	self.Weapon:StopSound( "weapons/doom_chainsaw/knife_idle.mp3" )
	return true
end
-------------------------------------------------------------------

------------General Swep Info---------------
SWEP.Author   = "CMasta -ZsN-"
SWEP.Contact        = ""
SWEP.Purpose        = "Cut something... maybe some legs or arms"
SWEP.Instructions   = "Pull out,\naim,\nruuuun cheater cheater cheater cheater ruuuun"
SWEP.Spawnable      = true
SWEP.AdminSpawnable  = true
-----------------------------------------------

------------Models---------------------------
SWEP.ViewModel      = "models/weapons/v_chain_s.mdl"
SWEP.WorldModel		= "models/weapons/w_physics.mdl"
-----------------------------------------------

-------------Primary Fire Attributes----------------------------------------
SWEP.Primary.Delay			= 1 	--In seconds
SWEP.Primary.Recoil			= 0		--Gun Kick
SWEP.Primary.Damage			= 1	--Damage per Bullet
SWEP.Primary.NumShots		= 1		--Number of shots per one fire
SWEP.Primary.Cone			= 10 	--Bullet Spread
SWEP.Primary.ClipSize		= 25	--Use "-1 if there are no clips"
SWEP.Primary.DefaultClip	= 25	-- Number of shots in next clip
SWEP.Primary.Automatic   	= true	--Pistol fire (false) or SMG fire (true)
SWEP.Primary.Ammo         	= "none"	--Ammo Type
-------------End Primary Fire Attributes------------------------------------
 
-------------Secondary Fire Attributes-------------------------------------
SWEP.Secondary.Delay		= 0
SWEP.Secondary.Recoil		= 0
SWEP.Secondary.Damage		= 0
SWEP.Secondary.NumShots		= 0
SWEP.Secondary.Cone			= 0
SWEP.Secondary.ClipSize		= -1
SWEP.Secondary.DefaultClip	= -1
SWEP.Secondary.Automatic   	= false
SWEP.Secondary.Ammo         = "none"
-------------End Secondary Fire Attributes--------------------------------


SWEP.nextBoom = 0
function SWEP:SecondaryAttack(worldsnd) 

   if not self:CanPrimaryAttack() or self.nextBoom > CurTime() then return end
    self.nextBoom = CurTime() + 1.3
    if SERVER then
    local tr = self.Owner:GetEyeTrace()
local ent = ents.Create( "env_explosion" )
    
	ent:SetPos( tr.HitPos  )
	ent:SetOwner( self.Owner  )
	ent:SetPhysicsAttacker(  self.Owner )
	ent:Spawn()
	ent:SetKeyValue( "iMagnitude", "40" )
	ent:Fire( "Explode", 0, 0 )
	
	util.BlastDamage( self, self:GetOwner(), self:GetPos(), 50, 65 )
	
	ent:EmitSound( "weapons/big_explosion.mp3" )
     end

   self:TakePrimaryAmmo( 1 )
    self.Owner:SetAnimation( PLAYER_ATTACK1 )
   local owner = self.Owner 
   if not IsValid(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

   owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )
end