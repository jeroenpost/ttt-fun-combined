include "weapons/weapon_zm_shotgun.lua"
SWEP.Base = "gb_camo_base"
SWEP.ViewModelFOV	= 54
SWEP.HoldType = "smg"
SWEP.PrintName = "Mirana's Sacred Arrow"
SWEP.ViewModel		= "models/weapons/cstrike/c_shot_m3super90.mdl"
SWEP.WorldModel		= "models/weapons/w_shot_m3super90.mdl"
SWEP.AutoSpawnable = false

SWEP.HoldType = "shotgun"
SWEP.Slot = 7
SWEP.Kind = 610

if CLIENT then

    SWEP.Author = "GreenBlack"

    SWEP.Icon = "vgui/ttt_fun_killicons/xm1014.png"
end

SWEP.Primary.Sound			= Sound( "Weapon_M3.Single" )
SWEP.Primary.Recoil			= 0.5

SWEP.Primary.Ammo = "Buckshot"
SWEP.Primary.Damage = 7
SWEP.Primary.Cone = 0.001
SWEP.Primary.Delay = 0.4
SWEP.Primary.ClipSize = 500
SWEP.Primary.ClipMax = 500
SWEP.Primary.DefaultClip = 1000
SWEP.Primary.Automatic = true
SWEP.Primary.NumShots = 11
SWEP.AutoSpawnable      = false
SWEP.AmmoEnt = "item_box_buckshot_ttt"
SWEP.Camo = 30

function SWEP:Deploy()

    self.dt.reloading = false
    self.reloadtimer = 0
    return self:DeployFunction()
end

SWEP.IronSightsPos = Vector(-7.65, -12.214, 3.2)
SWEP.IronSightsAng = Vector(0, 0, 0)

function SWEP:SecondaryAttack()
    self:Fly()
end
SWEP.numshott =0
SWEP.NextBeam = 0
function SWEP:PrimaryAttack()


    if self.Owner:KeyDown(IN_USE) then
        if self.NextBeam > CurTime() then return end
        self.NextBeam = CurTime() + 0.1
        self.Primary.NumShots = 1
        self.Primary.Cone = 0.00001
        self.LaserColor = Color(255,20,255,255)
        self:ShootTracer("LaserTracer_thick")
        self:Freeze()
        self.Weapon:EmitSound("weapons/airboat/airboat_gun_energy2.wav")
        self:ShootBullet( 14, 0.01,1, self:GetPrimaryCone() )
        self.numshott = self.numshott + 1
        if self.numshott > 6 then self.numshott = 0 end

        if self.numshott < 2 then  self:Disorientate()
        elseif self.numshott < 4 then self:Blind()
        elseif self.numshott < 5 then self:Freeze() end
        return
    end

    self.Weapon:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )

    self.Primary.NumShots = 8
    self.Primary.Cone = 0.09
    if not self:CanPrimaryAttack() then return end

    self.numshott = self.numshott + 1
    if self.numshott > 6 then self.numshott = 0 end

    if self.numshott < 2 then  self:Disorientate()
    elseif self.numshott < 4 then self:Blind()
    elseif self.numshott < 5 then self:Freeze() end


    if not worldsnd then
        self.Weapon:EmitSound( self.Primary.Sound, self.Primary.SoundLevel )
    elseif SERVER then
        sound.Play(self.Primary.Sound, self:GetPos(), self.Primary.SoundLevel)
    end

    self:ShootBullet( self.Primary.Damage, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )

    self:TakePrimaryAmmo( 1 )

    local owner = self.Owner
    if not IsValid(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

    owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )
end

function SWEP:OnDrop()
    self:Remove()
end
function SWEP:Reload()
    if self.Owner:KeyDown(IN_USE) then
        self:TrowChips()
        return
    end
    self.BaseClass.Reload(self)
end

SWEP.NextSecondary = 0
function SWEP:TrowChips()
    if not self.Owner.NextHealthMiranas then self.Owner.NextHealthMiranas = 0 end
    if (!SERVER) or self.Owner.NextHealthMiranas > CurTime() then return end
    self.Owner.NextHealthMiranas = CurTime() + 30

    self.Weapon:SendWeaponAnim( ACT_VM_MISSCENTER )
    self.Weapon:EmitSound("weapons/iceaxe/iceaxe_swing1.wav")

    local ent = ents.Create("sacredarrowcrisps")
    local pos = self.Owner:GetShootPos()
    ent:SetPos(pos)
    ent:SetAngles(self.Owner:EyeAngles())
    --ent:SetPhysicsAttacker( self.Owner )
    --ent.owner = self
    ent:Spawn()
    ent:Activate()
    --ent:SetOwner(self.Owner)
    --ent:SetPhysicsAttacker(self.Owner)

    self.Weapon:SetNextSecondaryFire(CurTime() + 2)
    self.Weapon:SetNextPrimaryFire(CurTime() + 1)

    local tr = self.Owner:GetEyeTrace()

    local phys = ent:GetPhysicsObject()
    if (phys:IsValid()) then
        phys:Wake()
        phys:SetMass(1)
        phys:EnableGravity( true )
        phys:ApplyForceCenter( self:GetForward() *2000 )
        phys:SetBuoyancyRatio( 0 )
    end

    local shot_length = tr.HitPos:Length()
    ent:SetVelocity(self.Owner:GetAimVector():GetNormalized() *  math.pow(shot_length, 3))
end

local ENT = {}

ENT.Type = "anim"
ENT.Base = "base_anim"


if SERVER then

    function ENT:Initialize()


        self:SetModel( "models/props_lab/cactus.mdl")
        self:SetModelScale(self:GetModelScale()*1,0)

        self.Entity:PhysicsInit(SOLID_VPHYSICS);
        self.Entity:SetMoveType(MOVETYPE_VPHYSICS);
        self.Entity:SetSolid(SOLID_VPHYSICS);
        self.Entity:SetCollisionGroup(COLLISION_GROUP_PROJECTILE)
        self.Damage = 1
        self:SetUseType(SIMPLE_USE)
        local phys = self:GetPhysicsObject()
        if phys:IsValid() then
            phys:Wake()
            phys:EnableGravity(true);
        end
        timer.Simple(25,function() if IsValid(self) then self:Remove() end end)

    end

    function ENT:Use( activator, caller )

        if ( activator:IsPlayer() ) then
            if activator:Health() < 150 then

                activator:SetHealth(activator:Health()+25)

            end
            activator:EmitSound("crisps/eat.mp3")
        end
        self.Entity:Remove()
    end


end

scripted_ents.Register( ENT, "sacredarrowcrisps", true )