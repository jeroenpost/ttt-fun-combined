if ( SERVER ) then
    AddCSLuaFile(  )
    SWEP.MaxAmmoMod = 5
    SWEP.GiveAmmoMod = 6
end

SWEP.PsID = "cm_g2contender"
SWEP.BulletLength = 7.8
SWEP.CaseLength = 51.8
SWEP.Kind = WEAPON_EQUIP3
SWEP.HeadshotMultiplier = 1
SWEP.MuzVel = 321.521

SWEP.Attachments = {
    [1] = {"kobra", "riflereflex", "eotech", "aimpoint", "elcan", "acog", "ballistic"},
    [2] = {"laser"}}

SWEP.PrintName			= "Blackheart's Boomstick"

if ( CLIENT ) then

    SWEP.PrintName			= "Blackheart's Boomstick"
    SWEP.Author				= "Counter-Strike"
    SWEP.Slot				= 9
    SWEP.SlotPos = 0 //			= 1
    SWEP.IconLetter			= "f"
    SWEP.DifType = true
    SWEP.PitchMod = 0.25
    SWEP.RollMod = 0.5
    SWEP.DrawAngleMod = 0.5
    SWEP.Muzzle = "cstm_muzzle_br"
    SWEP.SparkEffect = "cstm_child_sparks_large"
    SWEP.SmokeEffect = "cstm_child_smoke_large"
    SWEP.ACOGDist = 6
    SWEP.AimpointDist = 11
    SWEP.BallisticDist = 3.5
    SWEP.SafeXMoveMod = 2
    SWEP.NoRail = true


    SWEP.VElements = {
        ["ballistic"] = { type = "Model", model = "models/bunneh/scope01.mdl", bone = "Front", rel = "", pos = Vector(-7.042, -1.65, 1.264), angle = Angle(0, 0, -90), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
        ["acog"] = { type = "Model", model = "models/wystan/attachments/2cog.mdl", bone = "Front", rel = "", pos = Vector(-5, 4.05, 0.333), angle = Angle(-90, 0, -90), size = Vector(0.899, 0.899, 0.899), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
        ["aimpoint"] = { type = "Model", model = "models/wystan/attachments/aimpoint.mdl", bone = "Front", rel = "", pos = Vector(-4.566, 3.5, 0.228), angle = Angle(-90, 0, -90), size = Vector(0.899, 0.899, 0.899), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
        ["elcan"] = { type = "Model", model = "models/bunneh/elcan.mdl", bone = "Front", rel = "", pos = Vector(-4.5, 3.95, 0.321), angle = Angle(-90, 0, -90), size = Vector(0.899, 0.899, 0.899), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
        ["riflereflex"] = { type = "Model", model = "models/attachments/kascope.mdl", bone = "Front", rel = "", pos = Vector(3.045, -1.3, -0.004), angle = Angle(-90, 0, -90), size = Vector(0.6, 0.6, 0.6), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
        ["kobra"] = { type = "Model", model = "models/attachments/cmore.mdl", bone = "Front", rel = "", pos = Vector(1.161, -1.1, -0.004), angle = Angle(-90, 0, -90), size = Vector(0.6, 0.6, 0.6), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
        ["laser"] = { type = "Model", model = "models/props_c17/FurnitureBoiler001a.mdl", bone = "Front", rel = "", pos = Vector(6.551, -0.7, 0.367), angle = Angle(180, -90, 90), size = Vector(0.019, 0.019, 0.019), color = Color(255, 255, 255, 0), surpresslightning = false, material = "models/bunneh/silencer", skin = 0, bodygroup = {} },
        ["eotech"] = { type = "Model", model = "models/wystan/attachments/2otech557sight.mdl", bone = "Front", rel = "", pos = Vector(-9.367, 10.33, -0.264), angle = Angle(0, 4.443, -90), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
    }

    SWEP.WElements = {
        ["ballistic"] = { type = "Model", model = "models/bunneh/scope01.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(-0.783, -0.801, -6.019), angle = Angle(4, 0, 180), size = Vector(1.299, 1.299, 1.299), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
        ["acog"] = { type = "Model", model = "models/wystan/attachments/2cog.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(1.875, 0.523, 0.402), angle = Angle(0, -90, -176), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
        ["riflereflex"] = { type = "Model", model = "models/attachments/kascope.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(12.038, 0.808, -4.783), angle = Angle(0, -90, -176), size = Vector(0.699, 0.699, 0.699), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
        ["elcan"] = { type = "Model", model = "models/bunneh/elcan.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(1.875, 0.523, 0.402), angle = Angle(0, -90, -176), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
        ["aimpoint"] = { type = "Model", model = "models/wystan/attachments/aimpoint.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(2.437, 0.561, 0.437), angle = Angle(0, -90, -176), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
        ["kobra"] = { type = "Model", model = "models/attachments/cmore.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(9.456, 0.8, -4.733), angle = Angle(0, -90, -176), size = Vector(0.699, 0.699, 0.699), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
        ["weapon"] = { type = "Model", model = "models/weapons/w_cstm_g2.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(0.879, 0.958, -0.166), angle = Angle(0, 90, 180), size = Vector(1,1,1), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
        ["eotech"] = { type = "Model", model = "models/wystan/attachments/2otech557sight.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(-3.859, 1.095, 5.251), angle = Angle(2.51, 0, 180), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
    }
end

SWEP.HoldType			= "ar2"
SWEP.Base				= "aa_base"
SWEP.Category = "Extra Pack (Sidearms)"
SWEP.FireModes = {"break"}
SWEP.AmmoTypes = {"hp", "ap", "magnum", "incendiary", "birdshot"}

SWEP.Spawnable			= true
SWEP.AdminSpawnable		= true

SWEP.HoldType = "pistol"
SWEP.ViewModelFOV = 70
SWEP.ViewModelFlip = false
SWEP.ViewModel = "models/weapons/v_cstm_g2.mdl"
SWEP.WorldModel = "models/weapons/w_pist_deagle.mdl"
SWEP.ShowViewModel = true
SWEP.ShowWorldModel = false
SWEP.ViewModelBonescales = {}

SWEP.Weight				= 5
SWEP.AutoSwitchTo		= false
SWEP.AutoSwitchFrom		= false

SWEP.Primary.Sound			= Sound("CSTM_G2CONTENDER")
SWEP.Primary.Recoil			= 3.45
SWEP.Primary.Damage			= 7
SWEP.Primary.NumShots		= 19
SWEP.Primary.Cone			= 0.02
SWEP.Primary.ClipSize		= 1
SWEP.Primary.Delay			= 0.8
SWEP.Primary.DefaultClip	= 60
SWEP.Primary.ClipSize	= 2
SWEP.Primary.Automatic		= false
SWEP.Primary.Ammo			= "7.62x51MM" --".30-30"
SWEP.InitialHoldtype = "pistol"
SWEP.InHoldtype = "pistol"
SWEP.NoBoltAnim = true
SWEP.SilencedSound = Sound("CSTM_SilencedShot2")
SWEP.SilencedVolume = 80
SWEP.UnAimAfterShot = true
SWEP.HeadshotMultiplier = 2.5

-- Animation speed/custom reload function related
SWEP.IsReloading = false
SWEP.AnimPrefix = ""
SWEP.ReloadSpeed = 1
SWEP.ShouldBolt = false
SWEP.ReloadDelay = 0
SWEP.IncAmmoPerc = 0.72 -- Amount of frames required to pass (in percentage) of the reload animation for the weapon to have it's amount of ammo increased
SWEP.CantChamber = true
SWEP.SkipIdle = true
SWEP.PlaybackRate = 1
SWEP.FOVZoom = 85

-- Dynamic accuracy related
SWEP.ShotsAmount 			= 0
SWEP.ConeDecAff				= 0
SWEP.DefRecoil				= 4.5
SWEP.CurCone				= 0.04
SWEP.DecreaseRecoilTime 	= 0
SWEP.ConeAff1 				= 0 -- Crouching/standing
SWEP.ConeAff2 				= 0 -- Using ironsights

SWEP.UnConeTime				= 0 -- Amount of time after firing the last shot that needs to pass until accuracy increases
SWEP.FinalCone				= 0 -- Self explanatory
SWEP.VelocitySensivity		= 1.6 -- Percentage of how much the cone increases depending on the player's velocity (moving speed). Rifles - 100%; SMGs - 80%; Pistols - 60%; Shotguns - 20%
SWEP.HeadbobMul 			= 1
SWEP.IsSilenced 			= false
SWEP.IronsightsCone 		= 0.0045
SWEP.HipCone 				= 0.051
SWEP.ConeInaccuracyAff1 = 0.5
SWEP.PlayFireAnim = true

SWEP.Secondary.Automatic	= false
SWEP.Secondary.Ammo			= "none"

SWEP.IsUsingIronsights 		= false
SWEP.TargetMul = 0
SWEP.SetAndForget			= false
SWEP.CantSilence = true

SWEP.IronSightsPos = Vector(-3, 1, 0.3984)
SWEP.IronSightsAng = Vector(2.5174, -0.0099, 0)

SWEP.AimPos = Vector(-2.994, -1.448, 1.21)
SWEP.AimAng = Vector(0, 0, 0)

function SWEP:OnDrop()
    self:Remove()
end

SWEP.KobraPos = Vector(-2.985, -3.961, 0.592)
SWEP.KobraAng = Vector(0, 0, 0)

SWEP.RReflexPos = Vector(-2.991, -3.961, 0.736)
SWEP.RReflexAng = Vector(0, 0, 0)

SWEP.ReflexPos = Vector(-3, -3.961, 0.216)
SWEP.ReflexAng = Vector(0, 0, 0)

SWEP.ScopePos = Vector(-2.995, -3.961, 0.44)
SWEP.ScopeAng = Vector(0, 0, 0)

SWEP.ACOGPos = Vector(-3, -3.961, 0.80)
SWEP.ACOGAng = Vector(0, 0, 0)

SWEP.ACOG_BackupPos = Vector(-3, -3.961, -0.20)
SWEP.ACOG_BackupAng = Vector(0, 0, 0)

SWEP.ELCANPos = Vector(-2.98, -3.961, 0.78)
SWEP.ELCANAng = Vector(0, 0, 0)

SWEP.ELCAN_BackupPos = Vector(-2.988, -3.961, -0.76)
SWEP.ELCAN_BackupAng = Vector(0, 0, 0)

SWEP.BallisticPos = Vector(-2.98, -4.488, 0.426)
SWEP.BallisticAng = Vector(0, 0, 0)

SWEP.NoFlipOriginsPos = Vector(-1.4, 0, -0.361)
SWEP.NoFlipOriginsAng = Vector(0, 0, 0)

SWEP.MeleePos = Vector(4.487, -1.497, -5.277)
SWEP.MeleeAng = Vector(25.629, -30.039, 26.18)

SWEP.SafePos = Vector(-0.82, 0.165, 0.2)
SWEP.SafeAng = Vector(-9.754, 24.673, 0)


SWEP.IronSightsPos = Vector(-2.994, -1.448, 1.21)
SWEP.IronSightsAng =  Vector(0, 0, 0)

if SERVER then
    AddCSLuaFile()
end
function SWEP:PreDrop()
    self:SetZoom(false)
    self:SetIronsights(false)
    return self.BaseClass.PreDrop(self)
end

function SWEP:Reload()
    self.Weapon:DefaultReload( ACT_VM_RELOAD );
    self:SetIronsights( false )
    self:SetZoom(false)
end

function SWEP:Holster()
    self:SetIronsights(false)
    self:SetZoom(false)
    return true
end

--function SWEP:SecondaryAttack()
--    self:Zoom()
--end

--SWEP.IronSightsPos      = Vector( -100, -100, -100 )
--SWEP.IronSightsAng      = Vector( 2.6, 1.37, 3.5 )
SWEP.Secondary.Sound = Sound("Default.Zoom")
-- Add some zoom to ironsights for this gun
function SWEP:Zoom()
    if not self.IronSightsPos then return end
    if self.Weapon:GetNextSecondaryFire() > CurTime() then return end

    local bIronsights = not self:GetIronsights()

    self:SetIronsights( bIronsights )

    if SERVER then
        self:SetZoom(bIronsights)
    else
        self:EmitSound(self.Secondary.Sound)
    end

    self.Weapon:SetNextSecondaryFire( CurTime() + 0.3)
end




function SWEP:SetZoom(state)
    if CLIENT then
        return
    elseif IsValid(self.Owner) and self.Owner:IsPlayer() then
        if state then
            self.Owner:SetFOV(25, 0.3)
        else
            self.Owner:SetFOV(0, 0.2)
        end
    end
end
