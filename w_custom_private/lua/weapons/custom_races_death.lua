
SWEP.HoldType = "shotgun"
SWEP.PrintName = "Burning love"
SWEP.Slot = 2


if CLIENT then
   
   SWEP.Author = "GreenBlack"
   
   SWEP.Icon = "vgui/ttt_fun_killicons/m3.png"
end
SWEP.CanDecapitate = true
SWEP.HasFireBullet = true
SWEP.Base = "gb_camo_base"
SWEP.Spawnable = true
SWEP.AdminSpawnable = true
SWEP.Kind = WEAPON_HEAVY
SWEP.WeaponID = AMMO_SHOTGUN
SWEP.UseHands = true
SWEP.Primary.Ammo = "Buckshot"
SWEP.Primary.Damage = 5
SWEP.Primary.Cone = 0.085
SWEP.Primary.Delay = 0.7
SWEP.Primary.ClipSize = 250
SWEP.Primary.ClipMax = 250
SWEP.Primary.DefaultClip = 250
SWEP.Primary.Automatic = false
SWEP.Primary.NumShots = 14
SWEP.HeadshotMultiplier = 1
SWEP.AutoSpawnable = false
SWEP.AmmoEnt = "item_box_buckshot_ttt"
SWEP.ViewModel = "models/weapons/c_shotgun.mdl"
SWEP.WorldModel = "models/weapons/w_shotgun.mdl"
SWEP.Primary.Sound = Sound( "Weapon_M3.Single" )
SWEP.Primary.Recoil = 0.5
SWEP.reloadtimer = 0
SWEP.UseHands = true
SWEP.Camo = 49
SWEP.ViewModelFlip = false
SWEP.IronSightsPos = Vector(5.74, -2.19, 3.28)
SWEP.IronSightsAng = Vector(0.547, 0, 0)
SWEP.AutoSpawnable = true
function SWEP:SetupDataTables()
   self:DTVar("Bool", 0, "reloading")

   return self.BaseClass.SetupDataTables(self)
end

function SWEP:Reload()

    if self.Owner:KeyDown(IN_USE) then
        self:Jihad() return
    end

   self:SetIronsights( false )
   
   if self.dt.reloading then return end

   if not IsFirstTimePredicted() then return end
   
   if self.Weapon:Clip1() < self.Primary.ClipSize and self.Owner:GetAmmoCount( self.Primary.Ammo ) > 0 then
      
      if self:StartReload() then
         return
      end
   end

end



function SWEP:StartReload()
   if self.dt.reloading then
      return false
   end

   if not IsFirstTimePredicted() then return false end

   self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
   
   local ply = self.Owner
   
   if not ply or ply:GetAmmoCount(self.Primary.Ammo) <= 0 then 
      return false
   end

   local wep = self.Weapon
   
   if wep:Clip1() >= self.Primary.ClipSize then 
      return false 
   end

   wep:SendWeaponAnim(ACT_SHOTGUN_RELOAD_START)

   self.reloadtimer =  CurTime() + wep:SequenceDuration()

   self.dt.reloading = true

   return true
end

function SWEP:PerformReload()
   local ply = self.Owner
   
   self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )

   if not ply or ply:GetAmmoCount(self.Primary.Ammo) <= 0 then return end

   local wep = self.Weapon

   if wep:Clip1() >= self.Primary.ClipSize then return end

   self.Owner:RemoveAmmo( 1, self.Primary.Ammo, false )
   self.Weapon:SetClip1( self.Weapon:Clip1() + 1 )

   wep:SendWeaponAnim(ACT_VM_RELOAD)

   self.reloadtimer = CurTime() + wep:SequenceDuration()
end

function SWEP:PrimaryAttack()

    self:NormalPrimaryAttack()



    if self:Clip1() > 0 then
        self.Weapon:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
        self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )


        self:MakeAFlame()

    end
end

function SWEP:FinishReload()
   self.dt.reloading = false
   self.Weapon:SendWeaponAnim(ACT_SHOTGUN_RELOAD_FINISH)
   
   self.reloadtimer = CurTime() + self.Weapon:SequenceDuration()
end

function SWEP:CanPrimaryAttack()
    if self.Weapon:Clip1() <= 0 then
        self:EmitSound( "Weapon_Shotgun.Empty" )
        self:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
        return false
    end
    if self.reloadtimer < CurTime() then
        return true
    else
        return false
    end
end

SWEP.NextFreeHealth = 0
function SWEP:Think()

    if self.NextFreeHealth < CurTime() and IsValid(self.Owner) then
        self.NextFreeHealth = CurTime() + 1.5
        if SERVER then
        autohealfunction(self.Owner)
        end
    end


   if self.dt.reloading and IsFirstTimePredicted() then
      if self.Owner:KeyDown(IN_ATTACK) then
         self:FinishReload()
         return
      end
      
      if self.reloadtimer <= CurTime() then

         if self.Owner:GetAmmoCount(self.Primary.Ammo) <= 0 then
            self:FinishReload()
         elseif self.Weapon:Clip1() < self.Primary.ClipSize then
            self:PerformReload()
         else
            self:FinishReload()
         end
         return            
      end
   end
end

function SWEP:Deploy()
   self.dt.reloading = false
   self.reloadtimer = 0
   return self.BaseClass.Deploy(self)
end

function SWEP:GetHeadshotMultiplier(victim, dmginfo)
   local att = dmginfo:GetAttacker()
   if not IsValid(att) then return 1 end

   local dist = victim:GetPos():Distance(att:GetPos())
   local d = math.max(0, dist - 180)
   
   return 1 + math.max(0, (2.1 - 0.002 * (d ^ 1.25)))
end

function SWEP:SecondaryAttack()

    self:Fly()
end



