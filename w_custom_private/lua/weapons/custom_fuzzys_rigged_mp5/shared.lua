if SERVER then
   AddCSLuaFile(  )
   --resource.AddFile("materials/vgui/ttt_fun_killicons/mp5.png")
end

SWEP.HoldType = "ar2"
SWEP.PrintName = "Fuzzy's Rigged MP5"
SWEP.Slot = 7
if CLIENT then
   		
   SWEP.Author = "GreenBlack"
   
   SWEP.Icon = "vgui/ttt_fun_killicons/mp5.png"
end

SWEP.Base = "weapon_tttbase"
SWEP.Spawnable = true
SWEP.AdminSpawnable = true
SWEP.Kind = WEAPON_EQUIP2

SWEP.Primary.Delay = 0.03
SWEP.Primary.Recoil = 0.5
SWEP.Primary.Automatic = true
SWEP.Primary.Ammo = "pistol"
SWEP.Primary.Damage = 3
SWEP.Primary.Cone = 0.028
SWEP.Primary.ClipSize = 100
SWEP.Primary.ClipMax = 300
SWEP.Primary.DefaultClip = 100
SWEP.AutoSpawnable = false
SWEP.AmmoEnt = "item_ammo_pistol_ttt"
SWEP.ViewModel = "models/weapons/v_smg_mp5.mdl"
SWEP.WorldModel = "models/weapons/w_smg_mp5.mdl"
SWEP.Primary.Sound = Sound("Weapon_MP5Navy.Single")

SWEP.HeadshotMultiplier = 1

SWEP.IronSightsPos = Vector(4.7659, -3.0823, 1.8818)
SWEP.IronSightsAng = Vector(0.9641, 0.0252, 0)



function SWEP:PrimaryAttack(worldsnd)
    self.Weapon:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
    if not self:CanPrimaryAttack() then return end
    self:TakePrimaryAmmo( 1 )
    if not worldsnd then
        self.Weapon:EmitSound( self.Primary.Sound, self.Primary.SoundLevel )
    elseif SERVER then
        WorldSound(self.Primary.Sound, self:GetPos(), self.Primary.SoundLevel)
    end
    self:ShootBullet( self.Primary.Damage, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )

    if ( self.Owner:IsValid() and math.random(0,9) == 1 ) then
        local tr = self.Owner:GetEyeTrace()
        local effectdata = EffectData()
        effectdata:SetOrigin(tr.HitPos)
        effectdata:SetNormal(tr.HitNormal)
        effectdata:SetScale(1)
        -- util.Effect("effect_mad_ignition", effectdata)
        util.Decal("FadingScorch", tr.HitPos + tr.HitNormal, tr.HitPos - tr.HitNormal)

        local tracedata = {}
        tracedata.start = tr.HitPos
        tracedata.endpos = Vector(tr.HitPos.x, tr.HitPos.y, tr.HitPos.z - 10)
        tracedata.filter = tr.HitPos
        local tracedata = util.TraceLine(tracedata)
        if SERVER then
            if tracedata.HitWorld then
                local flame = ents.Create("env_fire");
                flame:SetPos(tr.HitPos + Vector( 0, 0, 1 ));
                flame:SetKeyValue("firesize", "10");
                flame:SetKeyValue("fireattack", "10");
                flame:SetKeyValue("StartDisabled", "0");
                flame:SetKeyValue("health", "10");
                flame:SetKeyValue("firetype", "0");
                flame:SetKeyValue("damagescale", "5");
                flame:SetKeyValue("spawnflags", "128");
                flame:SetPhysicsAttacker(self.Owner)
                flame:SetOwner( self.Owner )
                flame:Spawn();
                flame:Fire("StartFire",0);
            end
        end

        local owner = self.Owner
        --if not ValidEntity(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

        owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )
    end
end

SWEP.NextAttacky = 0

function SWEP:SecondaryAttack()

    if (  self.NextAttacky > CurTime()) then return end

    self.Weapon:EmitSound( "weapons/c4/c4_beep1.wav"	)
    self.NextAttacky = CurTime()+ 0.5


    local tr = util.TraceLine({ start = self.Owner:EyePos(), endpos = self.Owner:EyePos() + self.Owner:EyeAngles():Forward() * 80, filter = self.Owner })
    if tr.HitNonWorld and IsValid(tr.Entity) and tr.Entity:GetClass() == "prop_ragdoll" and tr.Entity.player_ragdoll then
        local mins, maxs = self.Owner:OBBMins(), self.Owner:OBBMaxs()
        local tr2 = util.TraceHull({start = tr.Entity:LocalToWorld(tr.Entity:OBBCenter()) + vector_up, endpos = tr.Entity:LocalToWorld(tr.Entity:OBBCenter()) + Vector(0, 0, 80), filter = {tr.Entity, self.Owner}, mins = Vector(mins.x * 1.6, mins.y * 1.6, 0), maxs = Vector(maxs.x * 1.6, maxs.y * 1.6, 0), mask = MASK_PLAYERSOLID})
        if tr2.Hit then
            self.Owner:PrintMessage( HUD_PRINTCENTER, "There is no room to bring him back here" )
            return
        end


        self.NextAttacky = CurTime() + 120
        self:TakePrimaryAmmo( 1 )
        if SERVER then
            rag = tr.Entity
            local ply = player.GetByUniqueID(rag.uqid)
            local credits = CORPSE.GetCredits(rag, 0)
            if not IsValid(ply) then return end
            if SERVER and _globals['slayed'][ply:SteamID()] then self.Owner:PrintMessage( HUD_PRINTCENTER, "Can't revive, person was slayed" )
                ply:Kill();
                return
            end
            if SERVER and ((rag.was_role == ROLE_TRAITOR and self.Owner:GetRole() ~= ROLE_TRAITOR) or (rag.was_role ~= ROLE_TRAITOR and self.Owner:GetRole() == ROLE_TRAITOR))  then
                DamageLog("Defib: " .. self.Owner:Nick() .. " [" .. self.Owner:GetRoleString() .. "] Died defibbing "..ply:Nick().." [" .. ply:GetRoleString() .. "] ")
                self.Owner:PrintMessage( HUD_PRINTCENTER, ply:Nick().." was of the other team,  sucked up your soul and killed you" )
                self.Owner:Kill();

                return
            end
            DamageLog("Defib: " .. self.Owner:Nick() .. " [" .. self.Owner:GetRoleString() .. "] defibbed "..ply:Nick().." [" .. ply:GetRoleString() .. "] ")

            ply:SpawnForRound(true)
            ply:SetCredits(credits)
            ply:SetPos(rag:LocalToWorld(rag:OBBCenter()) + vector_up / 2) --Just in case he gets stuck somewhere
            ply:SetEyeAngles(Angle(0, rag:GetAngles().y, 0))
            ply:SetCollisionGroup(COLLISION_GROUP_WEAPON)
            timer.Simple(2, function() ply:SetCollisionGroup(COLLISION_GROUP_PLAYER) end)

            umsg.Start("_resurrectbody")
            umsg.Entity(ply)
            umsg.Bool(CORPSE.GetFound(rag, false))
            umsg.End()
            rag:Remove()
        end
    end
end

