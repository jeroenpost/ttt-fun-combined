if SERVER then
    AddCSLuaFile(  )
    --resource.AddFile("materials/vgui/ttt_fun_killicons/m3.png")
end

SWEP.HoldType = "shotgun"
SWEP.PrintName = "Auto Shotty"
SWEP.Slot = 5
if CLIENT then

    SWEP.Author = "GreenBlack"

    SWEP.Icon = "vgui/ttt_fun_killicons/m3.png"
end

SWEP.Base = "gb_camo_base"
SWEP.Spawnable = true
SWEP.AdminSpawnable = true
SWEP.Kind = WEAPON_UNARMED
SWEP.WeaponID = AMMO_SHOTGUN

SWEP.Primary.Ammo = "Buckshot"
SWEP.Primary.Damage = 5
SWEP.Primary.Cone = 0.105
SWEP.Primary.Delay = 0.3
SWEP.Primary.ClipSize = 69
SWEP.Primary.ClipMax = 69
SWEP.Primary.DefaultClip = 69
SWEP.Primary.Automatic = true
SWEP.Primary.NumShots = 9
SWEP.HeadshotMultiplier = 2
SWEP.AutoSpawnable = false
SWEP.AmmoEnt = "item_box_buckshot_ttt"
SWEP.UseHands = true
SWEP.ViewModel		= "models/weapons/cstrike/c_smg_p90.mdl"
SWEP.WorldModel		= "models/weapons/w_smg_p90.mdl"
SWEP.Primary.Sound = Sound( "Weapon_M3.Single" )
SWEP.Primary.Recoil = 10
SWEP.reloadtimer = 0
SWEP.Camo = 42
SWEP.IronSightsPos = Vector(5.74, -2.19, 3.28)
SWEP.IronSightsAng = Vector(0.547, 0, 0)

function SWEP:SetupDataTables()
    self:DTVar("Bool", 0, "reloading")

    return self.BaseClass.SetupDataTables(self)
end


function SWEP:PrimaryAttack()
    if ( !self:CanPrimaryAttack() ) then return end


    self:FireBolt(0);
    timer.Simple(0.01,function() self:FireBolt2(1); end)
    timer.Simple(0.05,function() self:FireBolt3(1); end)
    timer.Simple(0.1,function() self:FireBolt4(1); end)
    timer.Simple(0.15,function() self:FireBolt5(1); end)



    // Signal a reload
    self.m_bMustReload = true;
    self:TakePrimaryAmmo( 1 );



    self.Weapon:EmitSound( self.Primary.Sound );
    -- self.Owner:EmitSound( self.Primary.Special2 );

    self.Weapon:SendWeaponAnim( ACT_VM_PRIMARYATTACK );

    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay );
    self.Weapon:SetNextSecondaryFire( CurTime() + self.Primary.Delay );

    // self:DoLoadEffect();
    // self:SetChargerState( CHARGER_STATE_DISCHARGE );

end

function SWEP:OnDrop()
    self:Remove()
end

function SWEP:Reload()
    if self.Owner:KeyDown(IN_USE ) then
    self:SetIronsights( false )

    if self.dt.reloading then return end

    if not IsFirstTimePredicted() then return end

    if self.Weapon:Clip1() < self.Primary.ClipSize and self.Owner:GetAmmoCount( self.Primary.Ammo ) > 0 then

        if self:StartReload() then
            return
        end
    end
        return
    end

    self:Zoom()


end

function SWEP:StartReload()
    if self.dt.reloading then
        return false
    end

    if not IsFirstTimePredicted() then return false end

    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )

    local ply = self.Owner

    if not ply or ply:GetAmmoCount(self.Primary.Ammo) <= 0 then
        return false
    end

    local wep = self.Weapon

    if wep:Clip1() >= self.Primary.ClipSize then
        return false
    end

    wep:SendWeaponAnim(ACT_SHOTGUN_RELOAD_START)

    self.reloadtimer =  CurTime() + wep:SequenceDuration()

    self.dt.reloading = true

    return true
end

function SWEP:PerformReload()
    local ply = self.Owner

    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )

    if not ply or ply:GetAmmoCount(self.Primary.Ammo) <= 0 then return end

    local wep = self.Weapon

    if wep:Clip1() >= self.Primary.ClipSize then return end

    self.Owner:RemoveAmmo( 1, self.Primary.Ammo, false )
    self.Weapon:SetClip1( self.Weapon:Clip1() + 1 )

    wep:SendWeaponAnim(ACT_VM_RELOAD)

    self.reloadtimer = CurTime() + wep:SequenceDuration()
end

function SWEP:FinishReload()
    self.dt.reloading = false
    self.Weapon:SendWeaponAnim(ACT_SHOTGUN_RELOAD_FINISH)

    self.reloadtimer = CurTime() + self.Weapon:SequenceDuration()
end

function SWEP:CanPrimaryAttack()
    if self.Weapon:Clip1() <= 0 then
        self:EmitSound( "Weapon_Shotgun.Empty" )
        self:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
        return false
    end
    if self.reloadtimer < CurTime() then
        return true
    else
        return false
    end
end

function SWEP:Think()
    if self.dt.reloading and IsFirstTimePredicted() then
        if self.Owner:KeyDown(IN_ATTACK) then
            self:FinishReload()
            return
        end

        if self.reloadtimer <= CurTime() then

            if self.Owner:GetAmmoCount(self.Primary.Ammo) <= 0 then
                self:FinishReload()
            elseif self.Weapon:Clip1() < self.Primary.ClipSize then
                self:PerformReload()
            else
                self:FinishReload()
            end
            return
        end
    end
end

function SWEP:Deploy()
    self.dt.reloading = false
    self.reloadtimer = 0
    return self.BaseClass.Deploy(self)
end

function SWEP:GetHeadshotMultiplier(victim, dmginfo)
    local att = dmginfo:GetAttacker()
    if not IsValid(att) then return 3 end

    local dist = victim:GetPos():Distance(att:GetPos())
    local d = math.max(0, dist - 140)

    return 1 + math.max(0, (2.1 - 0.002 * (d ^ 1.25)))
end

function SWEP:SecondaryAttack()
    self:Fly()
end


function SWEP:FireBolt( con )

    if ( self.Weapon:Clip1() <= 0 ) then

        return;
    end

    local pOwner = self.Owner;

    if ( pOwner == NULL ) then
        return;
    end

    if ( SERVER ) then


        local angAiming;


        local count2 = 0
        if con == 1 then cone = 0.020 else cone = 0.00000001 end
        -- while count2 < 10 do
        count2 = count2 + 1


        vecAiming		= pOwner:GetAimVector() ;
        vecSrc		= pOwner:GetShootPos();
        angAiming = vecAiming:Angle() ;
        self:ShootBullet( 35, 1, cone )


        local pBolt = ents.Create (  "crossbow_bolt" );
        pBolt:SetPos( vecSrc );
        pBolt:SetAngles( angAiming );

        pBolt.Damage = 0


        pBolt.AmmoType = self.Primary.Ammo;
        pBolt:SetOwner( pOwner );
        pBolt:Spawn()

        if ( pOwner:WaterLevel() == 3 ) then
            pBolt:SetVelocity( vecAiming * BOLT_WATER_VELOCITY );
        else
            pBolt:SetVelocity( vecAiming * BOLT_AIR_VELOCITY );
        end

    end



end

function SWEP:FireBolt2( con )

    if ( self.Weapon:Clip1() <= 0 ) then

        return;
    end

    local pOwner = self.Owner;

    if ( pOwner == NULL ) then
        return;
    end

    if ( SERVER ) then


        local angAiming;


        local count2 = 0
        cone = 0.025
        vecAiming		= pOwner:GetAimVector() +  Vector( math.Rand(-0.09,0.09), math.Rand(-0.09,0.09), math.Rand(-0.09,0.09) );
        vecSrc		= pOwner:GetShootPos() ;
        angAiming = vecAiming:Angle() ;
        self:ShootBullet( 0, 1, cone )



        local pBolt = ents.Create (  "crossbow_bolt" );
        pBolt:SetPos( vecSrc );
        pBolt:SetAngles( angAiming );
        pBolt.Damage = 0

        pBolt.AmmoType = self.Primary.Ammo;
        pBolt:SetOwner( pOwner );
        pBolt:Spawn()

        if ( pOwner:WaterLevel() == 3 ) then
            pBolt:SetVelocity( vecAiming * BOLT_WATER_VELOCITY );
        else
            pBolt:SetVelocity( vecAiming * BOLT_AIR_VELOCITY );
        end

    end



end


function SWEP:FireBolt3( con )

    if ( self.Weapon:Clip1() <= 0 ) then

        return;
    end

    local pOwner = self.Owner;

    if ( pOwner == NULL ) then
        return;
    end

    if ( SERVER ) then


        local angAiming;


        local count2 = 0
        cone = 0.025

        vecAiming		= pOwner:GetAimVector() +  Vector( math.Rand(-0.10,0.10), math.Rand(-0.10,0.10), math.Rand(-0.10,0.10) );

        vecSrc		= pOwner:GetShootPos() ;
        angAiming = vecAiming:Angle() ;
        self:ShootBullet( 0, 1, cone )



        local pBolt = ents.Create (  "crossbow_bolt" );
        pBolt:SetPos( vecSrc );
        pBolt:SetAngles( angAiming );
        pBolt.Damage = 0

        pBolt.AmmoType = self.Primary.Ammo;
        pBolt:SetOwner( pOwner );
        pBolt:Spawn()

        if ( pOwner:WaterLevel() == 3 ) then
            pBolt:SetVelocity( vecAiming * BOLT_WATER_VELOCITY );
        else
            pBolt:SetVelocity( vecAiming * BOLT_AIR_VELOCITY );
        end

    end



end

function SWEP:FireBolt4( con )

    if ( self.Weapon:Clip1() <= 0 ) then

        return;
    end

    local pOwner = self.Owner;

    if ( pOwner == NULL ) then
        return;
    end

    if ( SERVER ) then


        local angAiming;


        local count2 = 0
        cone = 0.025

        vecAiming		= pOwner:GetAimVector() +  Vector( math.Rand(-0.08,0.08), math.Rand(-0.08,0.08), math.Rand(-0.08,0.08) );

        vecSrc		= pOwner:GetShootPos() ;
        angAiming = vecAiming:Angle() ;
        self:ShootBullet( 0, 1, cone )



        local pBolt = ents.Create (  "crossbow_bolt" );
        pBolt:SetPos( vecSrc );
        pBolt:SetAngles( angAiming );
        pBolt.Damage = 0

        pBolt.AmmoType = self.Primary.Ammo;
        pBolt:SetOwner( pOwner );
        pBolt:Spawn()

        if ( pOwner:WaterLevel() == 3 ) then
            pBolt:SetVelocity( vecAiming * BOLT_WATER_VELOCITY );
        else
            pBolt:SetVelocity( vecAiming * BOLT_AIR_VELOCITY );
        end

    end



end

function SWEP:FireBolt5( con )

    if ( self.Weapon:Clip1() <= 0 ) then

        return;
    end

    local pOwner = self.Owner;

    if ( pOwner == NULL ) then
        return;
    end

    if ( SERVER ) then


        local angAiming;


        local count2 = 0
        cone = 0.025

        vecAiming		= pOwner:GetAimVector() +  Vector( math.Rand(-0.12,0.12), math.Rand(-0.12,0.12), math.Rand(-0.12,0.12) );

        vecSrc		= pOwner:GetShootPos() ;
        angAiming = vecAiming:Angle() ;
        self:ShootBullet( 0, 1, cone )



        local pBolt = ents.Create (  "crossbow_bolt" );
        pBolt:SetPos( vecSrc );
        pBolt:SetAngles( angAiming );
        pBolt.Damage = 0

        pBolt.AmmoType = self.Primary.Ammo;
        pBolt:SetOwner( pOwner );
        pBolt:Spawn()

        if ( pOwner:WaterLevel() == 3 ) then
            pBolt:SetVelocity( vecAiming * BOLT_WATER_VELOCITY );
        else
            pBolt:SetVelocity( vecAiming * BOLT_AIR_VELOCITY );
        end

    end



end


SWEP.IronSightsPos      = Vector( -100, -100, -100 )
SWEP.IronSightsAng      = Vector( 2.6, 1.37, 3.5 )
SWEP.Secondary.Sound = Sound("Default.Zoom")
-- Add some zoom to ironsights for this gun
function SWEP:Zoom()
    if not self.IronSightsPos then return end
    if self.Weapon:GetNextSecondaryFire() > CurTime() then return end

    local bIronsights = not self:GetIronsights()

    self:SetIronsights( bIronsights )

    if SERVER then
        self:SetZoom(bIronsights)
    else
        self:EmitSound(self.Secondary.Sound)
    end

    self.Weapon:SetNextSecondaryFire( CurTime() + 0.3)
end




function SWEP:SetZoom(state)
    if CLIENT then
        return
    elseif IsValid(self.Owner) and self.Owner:IsPlayer() then
        if state then
            self.Owner:SetFOV(30, 0.3)
        else
            self.Owner:SetFOV(0, 0.2)
        end
    end
end

if CLIENT then
    local scope = surface.GetTextureID("sprites/scope")
    function SWEP:DrawHUD()
        if self:GetIronsights() then
            surface.SetDrawColor( 0, 0, 0, 255 )

            local x = ScrW() / 2.0
            local y = ScrH() / 2.0
            local scope_size = ScrH()

            -- crosshair
            local gap = 80
            local length = scope_size
            surface.DrawLine( x - length, y, x - gap, y )
            surface.DrawLine( x + length, y, x + gap, y )
            surface.DrawLine( x, y - length, x, y - gap )
            surface.DrawLine( x, y + length, x, y + gap )

            gap = 0
            length = 50
            surface.DrawLine( x - length, y, x - gap, y )
            surface.DrawLine( x + length, y, x + gap, y )
            surface.DrawLine( x, y - length, x, y - gap )
            surface.DrawLine( x, y + length, x, y + gap )


            -- cover edges
            local sh = scope_size / 2
            local w = (x - sh) + 2
            surface.DrawRect(0, 0, w, scope_size)
            surface.DrawRect(x + sh - 2, 0, w, scope_size)

            surface.SetDrawColor(255, 0, 0, 255)
            surface.DrawLine(x, y, x + 1, y + 1)

            -- scope
            surface.SetTexture(scope)
            surface.SetDrawColor(255, 255, 255, 255)

            surface.DrawTexturedRectRotated(x, y, scope_size, scope_size, 0)

        else
            return self.BaseClass.DrawHUD(self)
        end
    end

    function SWEP:AdjustMouseSensitivity()
        return (self:GetIronsights() and 0.2) or nil
    end
end

function SWEP:PreDrop()
    self:SetZoom(false)
    self:SetIronsights(false)
    return self.BaseClass.PreDrop(self)
end

function SWEP:Holster()
    self:SetIronsights(false)
    self:SetZoom(false)
    return true
end
