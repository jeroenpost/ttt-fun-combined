SWEP.Base = "aa_base_fw"
SWEP.HoldType = "pistol"
SWEP.ViewModelFOV = 70.314960629921
SWEP.ViewModelFlip = false
SWEP.ViewModel = "models/weapons/cstrike/c_pist_deagle.mdl"
SWEP.WorldModel = "models/weapons/w_pistol.mdl"
SWEP.ShowViewModel = true
SWEP.ShowWorldModel = false

SWEP.UseHands = true
SWEP.Kind = WEAPON_MELEE

SWEP.ViewModelBoneMods = {
    ["v_weapon.Deagle_Parent"] = { scale = Vector(0.009, 0.009, 0.009), pos = Vector(0, 0, 0), angle = Angle(0, 0, 0) }
}

SWEP.WElements = {
    ["banannaaa"] = { type = "Model", model = "models/props/cs_italy/bananna.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(8.635, 0.455, -4.092), angle = Angle(3.068, -88.977, 0), size = Vector(1.116, 1.116, 1.116), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
}

SWEP.VElements = {
    ["banana"] = { type = "Model", model = "models/props/cs_italy/bananna.mdl", bone = "v_weapon.Deagle_Parent", rel = "", pos = Vector(0.455, -3.182, -0.456), angle = Angle(166.705, 0, 78.75), size = Vector(0.862, 0.862, 0.862), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
}

SWEP.Sound = { Sound("weapons/nanners/4.mp3"), Sound("weapons/nanners/5.mp3") }
SWEP.Sounds = { Sound("weapons/nanners/8.mp3"), Sound("weapons/nanners/10.mp3"),  Sound("weapons/nanners/13.mp3"),  Sound("weapons/nanners/14.mp3"),  Sound("weapons/nanners/17.mp3"),  Sound("weapons/nanners/18.mp3")}

SWEP.PrintName            = "MelonPistol"
SWEP.Slot                = 0
SWEP.SlotPos            = 1
SWEP.DrawAmmo            = true
SWEP.DrawCrosshair        = true
SWEP.Weight                = 5
SWEP.AutoSwitchTo        = false
SWEP.AutoSwitchFrom        = false
SWEP.Author            = ""
SWEP.Contact        = ""
SWEP.Purpose        = ""

SWEP.AutoSpawnable      = false
SWEP.Primary.Damage        = 40
SWEP.Primary.ClipSize        = 500
SWEP.Primary.DefaultClip    = 500
SWEP.Primary.Automatic        = true
SWEP.Primary.Ammo            = "XBowBolt"
SWEP.Primary.Delay            = 0.8
SWEP.Primary.Recoil = 1
SWEP.Secondary.Delay            = 3
SWEP.Secondary.ClipSize        = 2
SWEP.Secondary.DefaultClip    = 2
SWEP.Secondary.Automatic    = false
SWEP.Secondary.Ammo            = "XBowBolt"
SWEP.AmmoEnt = ""

SWEP.SecondaryProps = {
    "models/props_c17/oildrum001_explosive.mdl",
    "models/Combine_Helicopter/helicopter_bomb01.mdl",
    "models/props_junk/TrafficCone001a.mdl",
    "models/props_junk/metal_paintcan001a.mdl",
    "models/Gibs/HGIBS.mdl",
    "models/Gibs/Fast_Zombie_Legs.mdl",
    "models/props_c17/doll01.mdl",
    "models/props_lab/huladoll.mdl",
    "models/props_lab/huladoll.mdl",
    "models/props_lab/desklamp01.mdl",
    "models/props_junk/garbage_glassbottle003a.mdl",
    "models/weapons/w_shot_gb1014.mdl",

}
SWEP.Icon = "vgui/ttt_fun_killicons/nanners_gun.png"

function SWEP:PrimaryAttack()
    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
    -- self:TakePrimaryAmmo( 1 )
    self:ShootBullet( self.Primary.Damage , self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )
    self:throw_attack ("models/props_junk/watermelon01.mdl", Sound(table.Random( self.Sound) ), 3);
end

SWEP.Headcrabs = 0
SWEP.Headcrabs2 = 0
SWEP.NextSecond = 0

function SWEP:Reload()
    if  self.NextSecond > CurTime() then return end

    if self.Owner:KeyDown(IN_USE) then
    self.Weapon:SetNextSecondaryFire( CurTime() + 5 )

    self.NextSecond = CurTime() + 5
    self.Weapon:SetNextSecondaryFire( CurTime() + self.Secondary.Delay )
    --self:TakePrimaryAmmo( 10 )
    self:throw_attack ( table.Random(self.SecondaryProps) , Sound(table.Random( self.Sounds) ), 10);
        return
    end

    self.NextSecond = CurTime() + 3
    self.Owner.hasMelonShooter = true
    self:FireWorkie(  )

end


SWEP.NextHealthDrop = 0
function SWEP:Think()
    if self.Owner:KeyDown(IN_USE) and self.Owner:KeyDown(IN_ATTACK) and self.NextHealthDrop < CurTime() then
        self.NextHealthDrop = CurTime() + 3

        local healttt = 200

        if self.healththing and SERVER then
            healttt = Entity(self.healththing):GetStoredHealth()
            Entity(self.healththing):Remove()
        end
        self:HealthDrop( healttt )
    end
end

function SWEP:SecondaryAttack()

    self:Fly()

end


local throwsound = Sound( "Weapon_SLAM.SatchelThrow" )

-- ye olde droppe code
function SWEP:HealthDrop( healttt)
    if SERVER then
        local ply = self.Owner
        if not IsValid(ply) then return end

        --if self.Planted then return end

        local vsrc = ply:GetShootPos()
        local vang = ply:GetAimVector()
        local vvel = ply:GetVelocity()

        local vthrow = vvel + vang * 200

        local health = ents.Create("gb_health_station")
        if IsValid(health) then
            health:SetPos(vsrc + vang * 10)
            health:Spawn()
            health:SetModel("models/props_junk/watermelon01.mdl")
            health:SetPlacer(ply)
            health.healsound = "inside_watermelone.mp3"

            health:PhysWake()
            local phys = health:GetPhysicsObject()
            if IsValid(phys) then
                phys:SetVelocity(vthrow)
            end
            health:SetModel("models/props_junk/watermelon01.mdl")
            health:SetStoredHealth(healttt)

            self.healththing = health:EntIndex()

            self.Planted = true
        end
    end

    self.Weapon:EmitSound(throwsound)
end

function SWEP:ReplaySound( ent )
    if not IsValid( ent ) then return end
    ent:EmitSound(  "weapons/nanners/13.mp3"  )
    timer.Simple(16,function()
        if IsValid( ent ) then
            self:ReplaySound( ent )
        end

    end)
end



function SWEP:throw_attack (model_file, sounds, away)
    local tr = self.Owner:GetEyeTrace();

    local orgin_ents = ents.FindInSphere(self.Owner:GetPos(),550)
    for k,v in pairs( orgin_ents ) do
        if v:IsPlayer() then
            v.hasProtectionSuit= true
        end
    end

    self.BaseClass.ShootEffects (self);
    local soundfile = gb_config.websounds.."watermelone.mp3"
    gb_PlaySoundFromServer(soundfile, self.Owner)

    if (!SERVER) then return end;
    local ent = ents.Create ("prop_physics");
    ent:SetModel (model_file);
    ent:SetPos (self.Owner:EyePos() + (self.Owner:GetAimVector() * 16));
    ent:SetAngles (self.Owner:EyeAngles());
    ent:SetPhysicsAttacker(self.Owner)
    ent:Spawn();
    --ent:EmitSound( sound )





    if math.Rand(1,5) == 3 then
        ent:Ignite(100,100)
    end
    local phys = ent:GetPhysicsObject();
    local shot_length = tr.HitPos:Length();
    phys:ApplyForceCenter (self.Owner:GetAimVector():GetNormalized() * math.pow (shot_length, 3));

    timer.Simple(away,function()
        if IsValid(ent) then
            ent:Remove()
        end
    end)

    cleanup.Add (self.Owner, "props", ent);
    undo.Create ("Thrown model");
    undo.AddEntity (ent);
    undo.SetPlayer (self.Owner);
    undo.Finish();
end


function SWEP:Initialize()

    self:SetHoldType( self.HoldType or "pistol" )
    // other initialize code goes here

    if CLIENT then

    // Create a new table for every weapon instance
    self.VElements = table.FullCopy( self.VElements )
    self.WElements = table.FullCopy( self.WElements )
    self.ViewModelBoneMods = table.FullCopy( self.ViewModelBoneMods )

    self:CreateModels(self.VElements) // create viewmodels
    self:CreateModels(self.WElements) // create worldmodels

    // init view model bone build function
    if IsValid(self.Owner) then
        local vm = self.Owner:GetViewModel()
        if IsValid(vm) then
            self:ResetBonePositions(vm)

            // Init viewmodel visibility
            if (self.ShowViewModel == nil or self.ShowViewModel) then
            vm:SetColor(Color(255,255,255,255))
        else
            // we set the alpha to 1 instead of 0 because else ViewModelDrawn stops being called
            vm:SetColor(Color(255,255,255,1))
            // ^ stopped working in GMod 13 because you have to do Entity:SetRenderMode(1) for translucency to kick in
            // however for some reason the view model resets to render mode 0 every frame so we just apply a debug material to prevent it from drawing
            vm:SetMaterial("Debug/hsv")
            end
        end
    end

end

end
