SWEP.Base = "gb_camo_base"
SWEP.ViewModelFOV	= 54
SWEP.HoldType = "pistol"
SWEP.Icon = "vgui/ttt_fun_killicons/p228.png"
SWEP.PrintName = "The Mazer"
SWEP.ViewModel		= "models/weapons/cstrike/c_pist_p228.mdl"
SWEP.WorldModel		= "models/weapons/w_pist_p228.mdl"
SWEP.Kind = WEAPON_CARRY
SWEP.Slot = 4
SWEP.AutoSpawnable = false
SWEP.Primary.Delay       = 0.3
SWEP.Primary.Recoil      = 1.9
SWEP.Primary.Automatic   = true
SWEP.Primary.Damage      = 25
SWEP.Primary.Cone        = 0.005
SWEP.Primary.Ammo        = "smg1"
SWEP.Primary.ClipSize    = 200
SWEP.Primary.ClipMax     = 200
SWEP.Primary.DefaultClip = 200
SWEP.Primary.Sound       = Sound( "Weapon_Glock.Single" )
SWEP.Camo = 53
SWEP.Primary.Sound = Sound( "Weapon_Glock.Single" )
SWEP.IronSightsPos = Vector( -6, -6, 2.6 )

SWEP.HeadshotMultiplier = 1.75


function SWEP:PrimaryAttack()
    self:NormalPrimaryAttack();
    self:ShootTracer("ToolTracer")
end

function SWEP:Deploy()
    self:SetColorAndMaterial( self.WeaponColor, "models/debug/debugwhite" );
end
function SWEP:PreDrawViewModel()
    self.Owner:GetViewModel():SetColor( self.WeaponColor )
    self.Owner:GetViewModel():SetMaterial(  "models/debug/debugwhite"  )
end

function SWEP:SecondaryAttack()
    self:ShootBullet2(0,0,1,0.1)
end


SWEP.NextTaze = 0

function SWEP:ShootBullet2( dmg, recoil, numbul, cone )


    self.Weapon:SendWeaponAnim(self.PrimaryAnim)

    self.Owner:MuzzleFlash()
    self.Owner:SetAnimation( PLAYER_ATTACK1 )

    if not IsFirstTimePredicted() then return end

    local sights = self:GetIronsights()

    numbul = numbul or 1
    cone   = cone   or 0.01

    local bullet = {}
    bullet.Num    = numbul
    bullet.Src    = self.Owner:GetShootPos()
    bullet.Dir    = self.Owner:GetAimVector()
    bullet.Spread = Vector( cone, cone, 0 )
    bullet.Tracer = 1
    bullet.TracerName =  "manatrace"
    bullet.Force  = 10
    bullet.Damage = dmg


    self.Owner:FireBullets( bullet )

    -- Owner can die after firebullets
    if (not IsValid(self.Owner)) or (not self.Owner:Alive()) or self.Owner:IsNPC() then return end

    if ((game.SinglePlayer() and SERVER) or
            ((not game.SinglePlayer()) and CLIENT and IsFirstTimePredicted())) then

        -- reduce recoil if ironsighting
        recoil = sights and (recoil * 0.6) or recoil

        local eyeang = self.Owner:EyeAngles()
        eyeang.pitch = eyeang.pitch - recoil
        self.Owner:SetEyeAngles( eyeang )
    end

    attacker = self.Owner
    local tracedata = {}

    tracedata.start = self.Owner:GetShootPos()
    tracedata.endpos = self.Owner:GetShootPos() + ( self.Owner:GetAimVector() * 450 )
    tracedata.filter = self.Owner
    tracedata.mins = Vector( -3,-3,-3 )
    tracedata.maxs = Vector( 3,3,3 )
    tracedata.mask = MASK_SHOT_HULL
    local trace = util.TraceHull( tracedata )

    local victim = trace.Entity

    timer.Simple(19, function()
        if IsValid(self) and IsValid( self.Owner) and self:Clip1() < 10 then
            self:SetClip1( self:Clip1() + 1)
        end
    end)

    if ( not IsValid(attacker) or !SERVER  or not victim:IsPlayer() or victim:IsWorld() or self.NextTaze > CurTime() ) then return end
    if  !victim:Alive() or victim.IsDog or specialRound.isSpecialRound or _globals['specialperson'][victim:SteamID()] or (victim.NoTaze && self.Owner:GetRole() != ROLE_DETECTIVE)    then
    self.Owner:PrintMessage( HUD_PRINTCENTER, "Person cannot be tazed" )

    return end


    if victim:GetPos():Distance( attacker:GetPos()) > 40000 then return end

    StunTime = self.StunTime

    local edata = EffectData()
    edata:SetEntity(victim)
    edata:SetMagnitude(3)
    edata:SetScale(3)

    util.Effect("TeslaHitBoxes", edata)

    self:Taze( victim)

end

SWEP.StunTime = 5


function SWEP:Taze( victim )

    if not victim:Alive() or victim:IsGhost() then return end

    if victim:InVehicle() then
        local vehicle = victim:GetParent()
        victim:ExitVehicle()
    end

    ULib.getSpawnInfo( victim )

    if SERVER then DamageLog("Tazer: " .. self.Owner:Nick() .. " [" .. self.Owner:GetRoleString() .. "] tazed "..victim:Nick().." [" .. victim:GetRoleString() .. "] ")
    end
    local rag = ents.Create("prop_physics")
    if not (rag:IsValid()) then return end

    self.NextTaze = CurTime() + 19


    rag.BelongsTo = victim
    rag.Weapons = {}
    for _, v in pairs (victim:GetWeapons()) do
        table.insert(rag.Weapons, v:GetClass())
    end

    rag.Credits = victim:GetCredits()
    rag.OldHealth = victim:Health()
    rag:SetModel(victim:GetModel())
    rag:SetPos( victim:GetPos() )
    local velocity = victim:GetVelocity() + Vector(0,0,600)
    rag:SetAngles( victim:GetAngles() )
    rag:SetModel( "models/props_junk/watermelon01.mdl" )
    rag:Spawn()

    rag:Activate()
    rag:SetHealth(500)

    local phys = rag:GetPhysicsObject()
    if (  !IsValid( phys ) ) then rag:Remove() return end


    phys:SetVelocity(self.Owner:GetForward() * 800 + Vector(0, 0, 800))

            victim:SetParent( rag )
    victim:SetParent( rag )
    victim.Ragdolled = rag
    rag.orgPos = victim:GetPos()
    rag.orgPos.z = rag.orgPos.z + 10



    victim.orgPos = victim:GetPos()
    victim:StripWeapons()
    victim:Spectate(OBS_MODE_CHASE)
    victim:SpectateEntity(rag)

    rag:EmitSound(Sound("vo/Citadel/br_ohshit.wav"))

    victim:PrintMessage( HUD_PRINTCENTER, "You have been tasered. "..self.StunTime.." seconds till revival" )
    timer.Create("revivedelay"..victim:UniqueID(), self.StunTime, 1, function() TazerRevive( victim ) end )

end

function TazerRevive( victim)

    if !IsValid( victim ) then return end
    rag = victim:GetParent()

    if SERVER and victim.IsSlayed then
        victim:Kill();
        return
    end
    victim:SetParent()


    if (rag and rag:IsValid()) then
        weps = table.Copy(rag.Weapons)
        credits = rag.Credits
        oldHealth = rag.OldHealth
        rag:DropToFloor()
        pos = rag:GetPos()  --move up a little
        pos.z = pos.z + 15


        victim.Ragdolled = nil

        victim:UnSpectate()
        --  victim:DrawViewModel(true)
        --  victim:DrawWorldModel(true)
        victim:Spawn()
        --ULib.spawn( victim, true )
        rag:Remove()
        victim:SetPos( pos )
        victim:StripWeapons()

        UnStuck_stuck(victim)


        for _, v in pairs (weps) do
            victim:Give(v)
        end

        if credits > 0 then
            victim:AddCredits( credits  )
        end
        if oldHealth > 0 then
            victim:SetHealth( oldHealth  )
        end
        victim:DropToFloor()



        if  !victim:IsInWorld() or !victim:OnGround()    then
        victim:SetPos( victim.orgPos )
        end

        timer.Create("RespawnIfStuck",1,1,function()
            if IsValid(victim) and (!victim:IsInWorld() or !victim:OnGround() ) then
            victim:SetPos( victim.orgPos   )
            end
        end)

    else
        print("Debug: ragdoll or player is invalid")
        if IsValid( victim) then
            ULib.spawn( victim, true )
        end
    end



end