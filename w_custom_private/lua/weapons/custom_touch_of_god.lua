if SERVER then

    CreateConVar("grapple_distance", 10000, false)
	--resource.AddFile("materials/vgui/ttt_fun_killicons/spidergun.png")
end

SWEP.Spawnable			= true
SWEP.AdminSpawnable		= false
SWEP.CanDecapitate= true
SWEP.HasFireBullet = true

SWEP.PrintName			= "The Touch of Gold"
	SWEP.Slot				= 2
	SWEP.SlotPos = 9
	SWEP.Icon = "vgui/ttt_fun_killicons/spidergun.png"  
	
SWEP.Primary.Delay		= 0.1
SWEP.Primary.Damage		    = 13
SWEP.Primary.ClipSize		= 125
SWEP.Primary.DefaultClip	= 250
SWEP.Primary.Automatic		= true
SWEP.Primary.Ammo		= "Battery"
SWEP.UseHands = true

SWEP.DrawAmmo			= true
SWEP.DrawCrosshair		= true
SWEP.ViewModelFOV = 65
SWEP.ViewModelFlip = false
SWEP.ViewModel		= "models/weapons/cstrike/c_smg_mp5.mdl"
SWEP.WorldModel		= "models/weapons/w_smg_mp5.mdl"
SWEP.UseHands = true

  
SWEP.Kind = WEAPON_HEAVY
SWEP.Base   = "gb_camo_base"
SWEP.Camo = 7


function SWEP:GetIronsights( )

	return false

end

if CLIENT then


    function SWEP:CustomAmmoDisplay()

        self.AmmoDisplay = self.AmmoDisplay or {}
                --attempt to remove ammo display
        self.AmmoDisplay.Draw = false

        self.AmmoDisplay.PrimaryClip 	= 1
        self.AmmoDisplay.PrimaryAmmo 	= -1
        self.AmmoDisplay.SecondaryAmmo 	= -1

        return self.AmmoDisplay

    end

    function SWEP:SetHoldType( t )
        -- Just a fake function so we can define
        -- weapon holds in shared files without errors
    end
end



local sndPowerUp		= Sound("rope_hit.mp3")
local sndPowerDown		= Sound ("shoot_rope.mp3")
local sndTooFar			= Sound ("to_far.mp3")

function SWEP:Initialize()

	nextshottime = CurTime()
	self:SetHoldType( "smg" )
	self.BaseClass.Initialize(self)
end


SWEP.NextAmmoGive = 0
SWEP.NextAmmoTake = 0

function SWEP:Think()

    if self.NextAmmoGive < CurTime() && self:Clip1() < 150 then
    self.NextAmmoGive = CurTime() + 3
    self:SetClip1( self:Clip1() + 5 )
    end

	if (!self.Owner || self.Owner == NULL) or self:Clip1() < 1 then

        if ( !self.Beam ) then return end
        if( IsValid( self.Beam) ) then
            self.Beam:Remove()
        end
        self.Beam = nil
        return
    end


	
	if ( self.Owner:KeyPressed( IN_ATTACK2 ) ) then
	
		self:StartAttack()
		
	elseif ( self.Owner:KeyDown( IN_ATTACK2 )  ) then
	
		self:UpdateAttack()
        self.Weapon:SetNextSecondaryFire( CurTime() + 1 )
		
	elseif ( self.Owner:KeyReleased( IN_ATTACK2 )  ) then
	
		self:EndAttack( true )
	
	end
	


end

function SWEP:DoTrace( endpos )
	local trace = {}
		trace.start = self.Owner:GetShootPos()
		trace.endpos = trace.start + (self.Owner:GetAimVector() * 14096) --14096 is length modifier.
		if(endpos) then trace.endpos = (endpos - self.Tr.HitNormal * 7) end
		trace.filter = { self.Owner, self.Weapon }
		
	self.Tr = nil
	self.Tr = util.TraceLine( trace )
end

function SWEP:Reload()
    if self.Owner:KeyDown(IN_USE) then
        self:Jihad("bathroom.mp3")
    end
end


function SWEP:StartAttack()

       
        if not self:CanPrimaryAttack() then return end

	--Get begining and end poins of trace.
	local gunPos = self.Owner:GetShootPos() --Start of distance trace.
	local disTrace = self.Owner:GetEyeTrace() --Store all results of a trace in disTrace.
	local hitPos = disTrace.HitPos --Stores Hit Position of disTrace.
	
	--Calculate Distance
	--Thanks to rgovostes for this code.
	local x = (gunPos.x - hitPos.x)^2;
	local y = (gunPos.y - hitPos.y)^2;
	local z = (gunPos.z - hitPos.z)^2;
	local distance = math.sqrt(x + y + z);
	
	--Only latches if distance is less than distance CVAR
	local distanceCvar = GetConVarNumber("grapple_distance")
	inRange = false
	if distance <= distanceCvar then
		inRange = true
	end
	
	if inRange then
		if (SERVER) then
			
			if (!self.Beam) then --If the beam does not exist, draw the beam.
                            if not self:CanPrimaryAttack() then return end
				--grapple_beam
				self.Beam = ents.Create( "trace1" )
					self.Beam:SetPos( self.Owner:GetShootPos() )
				self.Beam:Spawn()
            self:TakePrimaryAmmo(1)

			end
			
			self.Beam:SetParent( self.Owner )
			self.Beam:SetOwner( self.Owner )
		
		end
		
                
		self:DoTrace()
		self.speed = 10000 --Rope latch speed. Was 3000.
		self.startTime = CurTime()
		self.endTime = CurTime() + self.speed
		self.dtfix = -1
		
		if (SERVER && self.Beam) then
			self.Beam:GetTable():SetEndPos( self.Tr.HitPos )
		end
		
		self:UpdateAttack()
		
		self.Weapon:EmitSound( sndPowerDown )
	else
		--Play A Sound
		self.Weapon:EmitSound( sndTooFar )
	end
end

function SWEP:UpdateAttack()

       -- if not self:CanPrimaryAttack() then return end        
    if !self.Tr or self:Clip1() < 1 then return end
        self.Owner:LagCompensation( true )


	if (!endpos) then endpos = self.Tr.HitPos end
	
	if (SERVER && self.Beam) then
		self.Beam:GetTable():SetEndPos( endpos )
	end

	lastpos = endpos

	
	
			if ( self.Tr.Entity:IsValid() ) then
			
					endpos = self.Tr.Entity:GetPos()
					if ( SERVER and IsValid(self.Beam) ) then
					self.Beam:GetTable():SetEndPos( endpos )
					end
			
			end
			
			local vVel = (endpos - self.Owner:GetPos())
			local Distance = endpos:Distance(self.Owner:GetPos())
			
			local et = (self.startTime + (Distance/self.speed))
			if(self.dtfix != 0) then
				self.dtfix = (et - CurTime()) / (et - self.startTime)
			end
			if(self.dtfix < 0) then
				self.Weapon:EmitSound( sndPowerUp )
				self.dtfix = 0
			end
			
			if(self.dtfix == 0) then
			zVel = self.Owner:GetVelocity().z
			vVel = vVel:GetNormalized()*(math.Clamp(Distance,0,25))
				if( SERVER ) then
                    if  self.NextAmmoTake < CurTime() then
                        self:TakePrimaryAmmo(1)
                        self.NextAmmoTake = CurTime() + 1
                        self.NextAmmoGive = CurTime() + 1.2
                    end

				local gravity = GetConVarNumber("sv_gravity")

                                    vVel:Add(Vector(0,0,(gravity/50)*1.5))

				if(zVel < 0) then
					vVel:Sub(Vector(0,0,zVel/100))
				end
				self.Owner:SetVelocity(vVel)
				end
			end
	
	endpos = nil
	
	self.Owner:LagCompensation( false )
	
end

function SWEP:EndAttack( shutdownsound )
	
	if ( shutdownsound ) then
		self.Weapon:EmitSound( sndPowerDown )
	end

	if ( CLIENT ) then return end
	if ( !self.Beam ) then return end
	if( IsValid( self.Beam) ) then
	  self.Beam:Remove()
	end
	self.Beam = nil
	
end


function SWEP:Holster()
	self:EndAttack( false )
	return true
end

function SWEP:OnRemove()
	self:EndAttack( false )
	return true
end


function SWEP:SecondaryAttack()
end

function SWEP:PrimaryAttack()
self.Weapon:SetNextPrimaryFire( CurTime() + 0.08 )
self:ShootBullet( 10, 1, 0.001 )
self:MakeAFlame()
end

function SWEP:OnDrop()
    self:Remove()
end

function SWEP:ShootBullet( damage, num_bullets, aimcone )

local bullet = {}
bullet.Num 		= num_bullets
bullet.Src 		= self.Owner:GetShootPos()	-- Source
bullet.Dir 		= self.Owner:GetAimVector()	-- Dir of bullet
bullet.Spread 	= Vector( aimcone, aimcone, 0 )		-- Aim Cone
bullet.Tracer	= 5	-- Show a tracer on every x bullets
bullet.TracerName = "Tracer" -- what Tracer Effect should be used
bullet.Force	= 1	-- Amount of force to give to phys objects
bullet.Damage	= damage
bullet.AmmoType = "Pistol"

self.Owner:FireBullets( bullet )
self.Weapon:EmitSound( Sound("weapons/usp/usp1.wav" ) )

self:ShootEffects()

end

function SWEP:GetHeadshotMultiplier(victim, dmginfo)


return 1.2
end
