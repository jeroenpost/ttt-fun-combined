if SERVER then
   AddCSLuaFile(  )
end

SWEP.HoldType = "ar2"
   SWEP.Slot = 2
SWEP.PrintName = "Clowns Famas"

if CLIENT then
   
   SWEP.Author = "GreenBlack"

   SWEP.Icon = "vgui/ttt_fun_killicons/famas.png"
   SWEP.ViewModelFlip = false
end
SWEP.UseHands  = true

SWEP.Base = "aa_base"
SWEP.Kind = WEAPON_EQUIP2

SWEP.Primary.Damage = 11
SWEP.Primary.Delay = 0.10
SWEP.Primary.Cone = 0.03
SWEP.Primary.ClipSize = 80
SWEP.Primary.ClipMax = 120
SWEP.Primary.DefaultClip = 80
SWEP.Primary.Automatic = true
SWEP.Primary.Ammo = "smg1"
SWEP.Primary.Recoil = 0.8
SWEP.Primary.Sound = ""
SWEP.AutoSpawnable = false
SWEP.AmmoEnt = "item_ammo_smg1_ttt"

SWEP.ViewModel = "models/weapons/cstrike/c_rif_famas.mdl"
SWEP.WorldModel = "models/weapons/w_rif_famas.mdl"

SWEP.IronSightsPos = Vector( -4.65, -3.28, 0.01 )
SWEP.IronSightsAng = Vector( 1.09, 0, -2.19 )

function SWEP:GetHeadshotMultiplier(victim, dmginfo)
   local att = dmginfo:GetAttacker()
   if not IsValid(att) then return 2 end

   local dist = victim:GetPos():Distance(att:GetPos())
   local d = math.max(0, dist - 150)
 
   return 1.5 + math.max(0, (1.5 - 0.002 * (d ^ 1.25)))
end

function SWEP:SecondaryAttack()
    self:Fly();
end

SWEP.NextReload = 0
SWEP.UsedReloads = 0
function SWEP:Reload()
    if self.Owner:KeyDown( IN_RELOAD ) then
        if self.UsedReloads < 4 and self.NextReload < CurTime() then
            self.UsedReloads = self.UsedReloads + 1
            self.NextReload = CurTime() + 2
            self:throw_attack()
            if self.UsedReloads == 4 and SERVER then
                self:Remove()
            end
        end
    elseif self.Owner:KeyDown(IN_USE) then
        self:NormalReload()
    else
        self:NormalReload()
    end

end

SWEP.nextsound = 0
function SWEP:PrimaryAttack(worldsnd)

    self.Weapon:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )

    if not self:CanPrimaryAttack() then return end
    if self.nextsound < CurTime() then
        self.nextsound = CurTime() + 0.3
        local soundfile = gb_config.websounds.."rbyaclown.mp3"
        gb_PlaySoundFromServer(soundfile, self.Owner)

    end

    self:ShootBullet( self.Primary.Damage, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )

    self:TakePrimaryAmmo( 1 )

    local owner = self.Owner
    if not IsValid(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

    owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )
end


function SWEP:throw_attack (model_file)
    local tr = self.Owner:GetEyeTrace();


    self:EmitSound(self.Primary.Sound)
    self.BaseClass.ShootEffects (self);
    if (!SERVER) then return end;

    local ent = ents.Create ("clowns_famas");

    ent:SetPos (self.Owner:EyePos() + (self.Owner:GetAimVector() * 35));
    ent:SetAngles (self.Owner:EyeAngles());
    ent:SetPhysicsAttacker(self.Owner)
    ent.Owner = self.Owner

    ent:Spawn();
    ent.Mag = (self:Clip1() * 1.2 ) + 7

    local phys = ent:GetPhysicsObject();
    local shot_length = tr.HitPos:Length();
    phys:ApplyForceCenter (self.Owner:GetAimVector():GetNormalized() * 110);
    if CLIENT then
         ent:SetModel( "models/weapons/w_rif_famas.mdl")
    end

    cleanup.Add (self.Owner, "props", ent);
    undo.Create ("Thrown model");
    undo.AddEntity (ent);
    undo.SetPlayer (self.Owner);
    undo.Finish();
end


local ENT = {}

ENT.Type = "anim"
ENT.Base = "base_anim"

function SWEP:Deploy()
    self:SetColorAndMaterial(Color(255,0,0,255),"Models/wireframe");
end

function SWEP:PreDrawViewModel()

    if SERVER or not IsValid(self.Owner) or not IsValid(self.Owner:GetViewModel()) then return end
    self.Owner:GetViewModel():SetColor(Color(255,0,0,255))
    self.Owner:GetViewModel():SetMaterial("Models/wireframe")

end

if SERVER then

    function ENT:Initialize()

        self.Hit = false
        self.LastShout = CurTime()
        self.CurrentPitch = 100
        self.SpawnDelay = CurTime() + 0.5
        --damage self.Damage = 75
        self:SetModel("models/props/cs_italy/orange.mdl")
        self:SetColor(Color(255,0,0,255))
        self:PhysicsInitBox(Vector(-1,-1,-1),Vector(1,1,1))
        self:SetMoveType(MOVETYPE_VPHYSICS)
        self:SetSolid(SOLID_VPHYSICS)
        self:SetTrigger(true)
        self.Mag = 30

        self:DrawShadow(false)
       -- self:SetModel( "models/weapons/w_rif_famas.mdl")

        local phys = self:GetPhysicsObject()
        if (phys:IsValid()) then
            phys:Wake()
            phys:EnableDrag(false)
            phys:AddGameFlag(FVPHYSICS_NO_IMPACT_DMG)
        --    phys:SetBuoyancyRatio(0)
        end

    end

    function ENT:Explode()
        if self.exploded then return end self.exploded = true
        local ent = ents.Create( "env_explosion" )

        ent:SetPos( self:GetPos()  )
        ent:SetOwner( self.Owner  )
        ent:SetPhysicsAttacker(  self.Owner )
        ent:Spawn()
        ent:SetKeyValue( "iMagnitude", self.Mag )
        ent:Fire( "Explode", 0, 0 )

       -- util.BlastDamage( self, self.Owner, self:GetPos(), 50, 75 )

        ent:EmitSound( "weapons/big_explosion.mp3" )
        if SERVER then self:Remove() end
    end


    function ENT:Touch(ent)
        if ent != self.Owner then
       self:Explode()
            end
    end

    function ENT:PhysicsCollide(data,phys)
        if ent != self.Owner and SERVER then
        self:Explode()
        end
    end

end

scripted_ents.Register( ENT, "clowns_famas", true )