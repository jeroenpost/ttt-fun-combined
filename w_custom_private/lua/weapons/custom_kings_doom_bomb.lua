-- DNA Scanner

AddCSLuaFile()
RADAR_king = {}
SWEP.HoldType = "normal"
SWEP.PrintName = "King's Doom Bomb"
if CLIENT then

    SWEP.Slot = 8

    SWEP.ViewModelFOV = 10

    SWEP.EquipMenuData = {
        type = "item_weapon",
        desc = "dna_desc"
    };

    SWEP.Icon = "vgui/king_ttt/icon_wtester"
end

SWEP.Base = "aa_base"

SWEP.ViewModel  = "models/weapons/v_crowbar.mdl"
SWEP.WorldModel = "models/combine_helicopter/helicopter_bomb01.mdl"

SWEP.DrawCrosshair       = false
SWEP.Primary.ClipSize    = -1
SWEP.Primary.DefaultClip = -1
SWEP.Primary.Automatic   = false
SWEP.Primary.Delay       = 1
SWEP.Primary.Ammo        = "none"

SWEP.Secondary.ClipSize    = -1
SWEP.Secondary.DefaultClip = -1
SWEP.Secondary.Automatic   = false
SWEP.Secondary.Ammo        = "none"
SWEP.Secondary.Delay       = 2

SWEP.Kind = WEAPON_ROLE2
SWEP.CanBuy = nil -- no longer a buyable thing




--SWEP.AllowDrop = false
SWEP.AutoSpawnable = false


SWEP.Nextattack = 0

function SWEP:OnDrop()
    self:Remove()
end

function SWEP:PreDrop()
    if IsValid(self.Owner) then
        self.Owner.scanner_weapon = nil
    end
end
SWEP.nextreload = 0
function SWEP:Reload()
    if self.nextreload > CurTime() then return end
    if self.Owner:KeyDown( IN_USE) then
        local effect = EffectData()
        effect:SetStart(self.Owner:GetPos())
        effect:SetOrigin(self.Owner:GetPos())
        effect:SetScale(1)
        effect:SetRadius(1)
        effect:SetMagnitude(1)
        util.Effect("Explosion", effect, true, true)
        util.BlastDamage(self, self.Owner, self.Owner:GetPos(), 1, 1)

        local pos = self.Owner:GetPos()
        local push_force = 800
       -- self:EmitSound( Sound("weapons/big_explosion.mp3") )
        local radius = 250

        gb_PlaySoundFromServer(gb_config.websounds.."gb_boom.mp3" )
        for k, target in pairs(ents.FindInSphere(self.Owner:GetPos(), 200)) do
            if IsValid(target) then
                local tpos = target:LocalToWorld(target:OBBCenter())
                local dir = (tpos - pos):GetNormal()
                local phys = target:GetPhysicsObject()

                if target:IsPlayer() and (not target:IsFrozen()) and ((not target.was_pushed) or target.was_pushed.t != CurTime()) then

                -- always need an upwards push to prevent the ground's friction from
                -- stopping nearly all movement
                dir.z = math.abs(dir.z) + 1

                local push = dir * push_force
                local vel = target:GetVelocity() + push
                vel.z = math.min(vel.z, push_force)
                local pusher = self:GetOwner() or self.Owner or nil

                target:SetVelocity(vel)
                target:SetGroundEntity( nil )
                target.was_pushed = {att=pusher, t=CurTime()}

                elseif IsValid(phys) then
                    phys:ApplyForceCenter(dir * -1 * phys_force)
                end
                local owner = self.Owner

                timer.Simple(0.2,function()
                    if IsValid(target) and SERVER then
                        local DMG=DamageInfo()
                        DMG:SetDamageType(DMG_DISSOLVE)
                        DMG:SetDamage(125)
                        DMG:SetAttacker(owner)
                        DMG:SetInflictor(self)
                        target:TakeDamageInfo(DMG)
                    end
                end)
            end
        end
        if SERVER then
            local owner = self.Owner
            timer.Simple(0.5,function()
                owner:Kill()
            end)

        local phexp = ents.Create("env_physexplosion")
        if IsValid(phexp) then
            phexp:SetPos(pos)
            phexp:SetKeyValue("magnitude", 100) --max
            phexp:SetKeyValue("radius", radius)
            -- 1 = no dmg, 2 = push ply, 4 = push radial, 8 = los, 16 = viewpunch
            phexp:SetKeyValue("spawnflags", 1 + 2 + 16)
            phexp:Spawn()
            phexp:Fire("Explode", "", 0.2)
        end
        end
        self.nextreload = CurTime() + 5
        return
    end


    if self.Nextattack > CurTime() then return end
    self.Nextattack = CurTime() + 0.2
    if self.Owner:KeyDown(IN_USE) then
        local trace = self.Owner:GetEyeTrace()
        if (trace.HitNonWorld and IsValid(trace.Entity) and trace.Entity:IsPlayer()) then
            local victim = trace.Entity
            self.Owner.trackervictim = victim:UniqueID()
            if SERVER then
                self:Blind(1,victim)
                victim:SetNWFloat("durgz_mushroom_high_start", CurTime())
                victim:SetNWFloat("durgz_mushroom_high_end", CurTime()+5)
            end
        end
    else
        local trace = self.Owner:GetEyeTrace()
        if (trace.HitNonWorld and IsValid(trace.Entity) and trace.Entity:IsPlayer()) then
            local victim = trace.Entity
            self.Owner.trackervictim = victim:UniqueID()
            if SERVER then
                victim:SetVelocity(self.Owner:GetForward() * 200 + Vector(0,0,200))
            end
        end


    end

    return false
end

function SWEP:Deploy()
    if SERVER and IsValid(self.Owner) then
        self.Owner:DrawViewModel(false)
        self.Owner.scanner_weapon = self
    end
    return true
end

function SWEP:OnDrop()
    self:Remove()
end

SWEP.RemoteBoom = false
SWEP.NextFire3 = 0
function SWEP:PrimaryAttack()

        if self.NextFire3 > CurTime() then
            self.Owner:PrintMessage( HUD_PRINTCENTER,  math.floor(self.NextFire3 - CurTime()).." seconds left" )
            return end
        self.NextFire3 = CurTime() + 25
        if SERVER then
            self:EmitSound("Friends/friend_join.wav")
            local ball = ents.Create("doombombexplosive");

            if (ball) then
                ball:SetPos(self.Owner:GetPos() + Vector(0,0,48));
                ball:SetPhysicsAttacker(self.Owner)
                ball.owner = self.Owner
                ball:Spawn();
                --ball:SetModelScale( ball:GetModelScale() * 0.1, 0.01 )
                local physicsObject = ball:GetPhysicsObject();
                physicsObject:ApplyForceCenter(self.Owner:GetAimVector() * 5000);
                self.RemoteBoom= ball;
            end;
        end
        return
end

function SWEP:SecondaryAttack()
    if self.RemoteBoom and IsValid(self.RemoteBoom) then
        self.RemoteBoom:Explode()
        self.RemoteBoom = false
    end
end

local ENT = {}

ENT.Type = "anim"
ENT.Base = "base_anim"


if SERVER then

    function ENT:Initialize()
        self:SetModel("models/combine_helicopter/helicopter_bomb01.mdl")
        self:SetModelScale(self:GetModelScale()*1,0)

        self.Entity:PhysicsInit(SOLID_VPHYSICS);
        self.Entity:SetMoveType(MOVETYPE_VPHYSICS);
        self.Entity:SetSolid(SOLID_VPHYSICS);
        self.CanBoom = CurTime() + 3
        self.Angles = -360

        self.Entity:SetCollisionGroup(COLLISION_GROUP_WEAPON)
        local phys = self:GetPhysicsObject()
        if phys:IsValid() then
            phys:Wake()
            phys:EnableGravity(true);
        end
        timer.Create(self:EntIndex().."sandboom2",3,0,function()
            if IsValid(self) then
                self:EmitSound( Sound( "C4.PlantSound" ))
                self:SetColor(Color(0,0,math.Rand(50,255),255))
                -- self:SetAngles(Angle(0,math.Rand(0,360),0))
            end
        end)

    end

    function ENT:Use( activator, caller )
        if self.CanBoom > CurTime() then return end
        self:EmitSound("common/bugreporter_failed.wav")
        self:Explode()
    end

    function ENT:Touch( ent )
        if ent:IsPlayer() and not ent.nextsandtouch or (ent.nextsandtouch and ent.nextsandtouch < CurTime()) then
            ent.nextsandtouch = CurTime() + 3
            ent:Ignite(0.5,0)
            local dmginfo = DamageInfo()
            dmginfo:SetDamage( 20 ) --50 damage
            dmginfo:SetDamageType( DMG_BULLET ) --Bullet damage
            dmginfo:SetAttacker( self.owner or ent ) --First player found gets credit
            dmginfo:SetDamageForce( Vector( 0, 0, 50 ) ) --Launch upwards
            dmginfo:SetInflictor(self.owner or self)
            ent:TakeDamageInfo( dmginfo )
            self:Explode()
        end
    end

    function ENT:Explode()
        if self.exploded then return end self.exploded = true
        local effect = EffectData()
        effect:SetStart(self:GetPos())
        effect:SetOrigin(self:GetPos())
        effect:SetScale(120)
        effect:SetRadius(220)
        effect:SetMagnitude(250)

        util.Effect("Explosion", effect, true, true)

        util.BlastDamage(self, self.owner, self:GetPos(), 280, 100)
        self:Remove()
    end


end

scripted_ents.Register( ENT, "doombombexplosive", true )
