if( SERVER ) then
	AddCSLuaFile(  )
	--resource.AddFile("materials/vgui/ttt/fists.png")
end


	SWEP.PrintName = "Master Fister"
	SWEP.DrawAmmo = false
	SWEP.DrawCrosshair = false
	
	


SWEP.Base = "gb_base"
SWEP.Kind = WEAPON_UNARMED

SWEP.CanBuy = nil -- no longer a buyable thing
--SWEP.LimitedStock = true

SWEP.AllowDrop = false
SWEP.Icon = "vgui/ttt/fists.png"

SWEP.Author			= "robotboy655"
SWEP.Purpose		= "Well we sure as heck didn't use guns! We would wrestle Hunters to the ground with our bare hands! I would get ten, twenty a day, just using my fists."

SWEP.Spawnable			= true
SWEP.UseHands			= true

SWEP.ViewModel			= "models/weapons/c_arms_cstrike.mdl"

SWEP.WorldModel			= ""
SWEP.DrawWorldModel = false
--SWEP.ViewModel      = "models/weapons/v_punch.mdl"
--SWEP.WorldModel   = "models/weapons/w_fists_t.mdl"

SWEP.ViewModelFOV		= 46

SWEP.Primary.ClipSize		= 1
SWEP.Primary.DefaultClip	= 1
SWEP.Primary.Damage			= 35
SWEP.Primary.Automatic		= true
SWEP.Primary.Ammo			= "none"

SWEP.Secondary.ClipSize		= 1
SWEP.Secondary.DefaultClip	= 1
SWEP.Secondary.Damage			= 35
SWEP.Secondary.Automatic	= true
SWEP.Secondary.Ammo			= "Battery"

SWEP.Weight				= 5
SWEP.AutoSwitchTo		= false
SWEP.AutoSwitchFrom		= false 

SWEP.Slot				= 5
SWEP.SlotPos			= 5
SWEP.DrawAmmo			= false
SWEP.DrawCrosshair		= true
SWEP.secondUsed                 = false


local SwingSound = Sound( "weapons/slam/throw.wav" )
local HitSound = Sound( "Flesh.ImpactHard" )

function SWEP:Initialize()

	self:SetHoldType( "fist" )

end

function SWEP:Think()
   
end

function SWEP:PreDrawViewModel(viewModel, weapon, client)
 
end

SWEP.Distance = 67
SWEP.AttackAnims = { "fists_left", "fists_right", "fists_uppercut" }
SWEP.AttackAnimsRight = { "fists_right", "fists_uppercut" }
function SWEP:PrimaryAttack()
 self:AttackThing( self.AttackAnimsRight[ math.random( 1, #self.AttackAnimsRight ) ] )
end

SWEP.NextReload = 0

function SWEP:Reload()
    if self:Clip1() > 0 and self.NextReload < CurTime() then

        local tracedata = {}
        tracedata.start = self.Owner:GetShootPos()
        tracedata.endpos = self.Owner:GetShootPos() + (self.Owner:GetAimVector() * 100)
        tracedata.filter = self.Owner
        tracedata.mins = Vector(1,1,1) * -10
        tracedata.maxs = Vector(1,1,1) * 10
        tracedata.mask = MASK_SHOT_HULL
        local tr = util.TraceLine({start = self.Owner:EyePos(), endpos = self.Owner:EyePos() + self.Owner:EyeAngles():Forward() * 80, filter = self.Owner})

        local ply = self.Owner
        self.NextReload = CurTime() + 0.8
        if IsValid(tr.Entity) then
            if tr.Entity.player_ragdoll then

                self:SetClip1( self:Clip1() - 1)

                self.NextReload = CurTime() + 120
                timer.Simple(0.1, function()
                    ply:Freeze(true)
                    ply:SetColor(Color(255,0,0,255))
                end)
                self.Weapon:SendWeaponAnim(ACT_VM_PRIMARYATTACK)

                timer.Create("GivePlyHealth_"..self.Owner:UniqueID(),0.5,6,function() if(IsValid(self)) and IsValid(self.Owner) then
                    if self.Owner:Health() < 175 then
                        self.Owner:SetHealth(self.Owner:Health()+5)
                    end
                end end)
                self.Owner:EmitSound("ttt/nomnomnom.mp3")

                timer.Simple(3.1, function()
                    if not IsValid(ply) then return end
                    ply:Freeze(false)
                    ply:SetColor(Color(255,255,255,255))
                    tr.Entity:Remove()
                --self:Remove()
                end )


            end
        end




    elseif self.NextReload < CurTime() then
        self.NextReload = CurTime() + 0.3
        self:AttackThing( "fists_right" )
    end
	
end

SWEP.flyammo = 5

function SWEP:SecondaryAttack()

    self.Weapon:SetNextSecondaryFire(CurTime() + 0.30)
    self.Weapon:SetNextPrimaryFire(CurTime() + 0.3)
    if  self.flyammo < 1 then return end

    if SERVER then self.Owner:EmitSound(Sound("weapons/ls/lightsaber_swing.mp3")) end
    if SERVER then self.Owner:EmitSound(Sound("player/suit_sprint.wav")) end

    self.flyammo = self.flyammo - 1
    self.Owner:SetAnimation( PLAYER_ATTACK1 )

    if SERVER then self.Owner:SetVelocity(self.Owner:GetForward() * 400 + Vector(0,0,400)) end
    timer.Simple(20,function() if IsValid(self) then self.flyammo = self.flyammo + 1 end end)
    --timer.Simple(0.2,self.SecondaryAttackDelay,self)
end

function SWEP:DealDamage( anim )
	local tr = util.TraceLine( {
		start = self.Owner:GetShootPos(),
		endpos = self.Owner:GetShootPos() + self.Owner:GetAimVector() * self.Distance,
		filter = self.Owner
	} )

	if ( not IsValid( tr.Entity ) ) then 
		tr = util.TraceHull( {
			start = self.Owner:GetShootPos(),
			endpos = self.Owner:GetShootPos() + self.Owner:GetAimVector() * self.Distance,
			filter = self.Owner,
			mins = self.Owner:OBBMins() / 3,
			maxs = self.Owner:OBBMaxs() / 3
		} )
	end

	if ( tr.Hit ) then self.Owner:EmitSound( HitSound ) end

	if ( IsValid( tr.Entity ) && ( tr.Entity:IsNPC() || tr.Entity:IsPlayer() ) ) then
		local dmginfo = DamageInfo()
		dmginfo:SetDamage( self.Primary.Damage )
		if ( anim == "fists_left" ) then
			dmginfo:SetDamageForce( self.Owner:GetRight() * 49125 + self.Owner:GetForward() * 99984 ) -- Yes we need those specific numbers
                        dmginfo:SetDamage( self.Secondary.Damage )
                        

                        self.secondUsed = false
		elseif ( anim == "fists_right" ) then
			dmginfo:SetDamageForce( self.Owner:GetRight() * -49124 + self.Owner:GetForward() * 99899 )
                        dmginfo:SetDamage( self.Primary.Damage )
		elseif ( anim == "fists_uppercut" ) then
			dmginfo:SetDamageForce( self.Owner:GetUp() * 51589 + self.Owner:GetForward() * 100128 )
                        dmginfo:SetDamage( self.Primary.Damage )
		end
                tr.Entity:SetVelocity(self.Owner:GetForward() * 600 + Vector(0,0,400))
		dmginfo:SetInflictor( self )
		local attacker = self.Owner
		if ( !IsValid( attacker ) ) then attacker = self end
		dmginfo:SetAttacker( attacker )

		tr.Entity:TakeDamageInfo( dmginfo )
	end
end



function SWEP:AttackThing( anim )
   self.Owner:SetAnimation( PLAYER_ATTACK1 )
   
   
    
	if ( !SERVER ) then return end
   if !IsValid(self.Owner) then return end
	-- We need this because attack sequences won't work otherwise in multiplayer
	local vm = self.Owner:GetViewModel()
	vm:ResetSequence( vm:LookupSequence( "fists_idle_01" ) )

	--local anim = "fists_left" --self.AttackAnims[ math.random( 1, #self.AttackAnims ) ]

	timer.Simple( 0, function()
		if ( !IsValid( self ) || !IsValid( self.Owner ) || !self.Owner:GetActiveWeapon() || self.Owner:GetActiveWeapon() != self ) then return end
	
		local vm = self.Owner:GetViewModel()
		vm:ResetSequence( vm:LookupSequence( anim ) )

		self:Idle()
	end )

	timer.Simple( 0.05, function()
		if ( !IsValid( self ) || !IsValid( self.Owner ) || !self.Owner:GetActiveWeapon() || self.Owner:GetActiveWeapon() != self ) then return end
		if ( anim == "fists_left" ) then
			self.Owner:ViewPunch( Angle( 0, 16, 0 ) )
		elseif ( anim == "fists_right" ) then
			self.Owner:ViewPunch( Angle( 0, -16, 0 ) )
		elseif ( anim == "fists_uppercut" ) then
			self.Owner:ViewPunch( Angle( 16, -8, 0 ) )
		end
	end )

	timer.Simple( 0.2, function()
		if ( !IsValid( self ) || !IsValid( self.Owner ) || !self.Owner:GetActiveWeapon() || self.Owner:GetActiveWeapon() != self ) then return end
		if ( anim == "fists_left" ) then
			self.Owner:ViewPunch( Angle( 4, -16, 0 ) )
		elseif ( anim == "fists_right" ) then
			self.Owner:ViewPunch( Angle( 4, 16, 0 ) )
		elseif ( anim == "fists_uppercut" ) then
			self.Owner:ViewPunch( Angle( -32, 0, 0 ) )
		end
		self.Owner:EmitSound( SwingSound )
		
	end )

	timer.Simple( 0.2, function()
		if ( !IsValid( self ) || !IsValid( self.Owner ) || !self.Owner:GetActiveWeapon() || self.Owner:GetActiveWeapon() != self ) then return end
		self:DealDamage( anim )
	end )

	self.Weapon:SetNextSecondaryFire(CurTime() + 0.6)
    self.Weapon:SetNextPrimaryFire(CurTime() + 0.6)



end

function SWEP:Idle()
    if !IsValid(self.Owner) then return end
	local vm = self.Owner:GetViewModel()
	timer.Create( "fists_idle" .. self:EntIndex(), vm:SequenceDuration(), 1, function()
		vm:ResetSequence( vm:LookupSequence( "fists_idle_0" .. math.random( 1, 2 ) ) )
	end )

end



function SWEP:OnRemove()

	--if ( IsValid( self.Owner ) ) then
	--	local vm = self.Owner:GetViewModel()
		--vm:SetMaterial( "" )
	--end

	timer.Stop( "fists_idle" .. self:EntIndex() )

end

function SWEP:Holster( wep )
if ( !IsValid(self.Owner) ) then
		return
	end



	return true
end
SWEP.modelView = false

function SWEP:Deploy()

    if ( !IsValid(self.Owner) ) then
		return
	end


    local vm = self.Owner:GetViewModel()
	vm:ResetSequence( vm:LookupSequence( "fists_draw" ) )
        
	self:Idle()
	
	return true
end
