if SERVER then
    AddCSLuaFile(  )
    --resource.AddFile("materials/vgui/ttt_fun_killicons/ak47.png")
end

SWEP.HoldType = "ar2"
SWEP.PrintName = "Wepon"
SWEP.Slot = 6

if CLIENT then

    SWEP.Author = "GreenBlack"

    SWEP.Icon = "vgui/ttt_fun_killicons/ak47.png"
end

SWEP.Base = "weapon_tttbase"
SWEP.Spawnable = true
SWEP.AdminSpawnable = true
SWEP.AutoSpawnable = false
SWEP.Kind = WEAPON_EQUIP1
SWEP.Primary.Delay = 0.12
SWEP.Primary.Recoil = 0.05
SWEP.Primary.Automatic = true
SWEP.Primary.Ammo = "Pistol"
SWEP.Primary.Damage = 17
SWEP.Primary.Cone = 0.018
SWEP.Primary.ClipSize = 220
SWEP.Primary.ClipMax = 440
SWEP.Primary.DefaultClip = 220
SWEP.AmmoEnt = "item_ammo_pistol_ttt"
SWEP.ViewModel = "models/weapons/v_smg1.mdl"
SWEP.WorldModel = "models/weapons/w_smg1.mdl"
SWEP.Primary.Sound = Sound("weapons/ak47/ak47-1.wav")
SWEP.ViewModelFlip = false

SWEP.HeadshotMultiplier = 2

SWEP.IronSightsPos = Vector(6.05, -3.30, 1.80)
SWEP.IronSightsAng = Vector(3.28, 0, 0)

SWEP.NextSecond = 0

function SWEP:SecondaryAttack(worldsnd)

    if self.NextSecond > CurTime() then return end
    self.NextSecond = CurTime() + 1.5
    if SERVER then
        local tr = self.Owner:GetEyeTrace()
        local ent = ents.Create("env_explosion")

        ent:SetPos(tr.HitPos)
        ent:SetOwner(self.Owner)
        ent:SetPhysicsAttacker(self.Owner)
        ent:Spawn()
        ent:SetKeyValue("iMagnitude", "40")
        ent:Fire("Explode", 0, 0)

        util.BlastDamage(self, self:GetOwner(), self:GetPos(), 50, 75)

        ent:EmitSound("weapons/big_explosion.mp3")
    end

    self:TakePrimaryAmmo(1)
    self.Owner:SetAnimation(PLAYER_ATTACK1)
    local owner = self.Owner
    if not IsValid(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

    owner:ViewPunch(Angle(math.Rand(-0.2, -0.1) * self.Primary.Recoil, math.Rand(-0.1, 0.1) * self.Primary.Recoil, 0))
end