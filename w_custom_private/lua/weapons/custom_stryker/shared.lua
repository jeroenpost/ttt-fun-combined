if SERVER then

   AddCSLuaFile(  )
   --resource.AddFile("vgui/ttt_fun_killicons/m4a1.png")
end
 -- TODO Add Partyshot

SWEP.HoldType			= "ar2"
  SWEP.PrintName			= "Murasame"
   SWEP.Slot				= 7

if CLIENT then

 

   SWEP.Icon = "vgui/ttt_fun_killicons/m4a1.png"
end


SWEP.Base				= "aa_base_fw"
SWEP.Spawnable = true
SWEP.AdminSpawnable = true

SWEP.Kind = WEAPON_EQUIP3


SWEP.Primary.Delay = 0.13
SWEP.Primary.Recoil = 0.05
SWEP.Primary.Automatic = true
SWEP.Primary.Ammo = "Pistol"
SWEP.Primary.Damage = 13
SWEP.Primary.Cone = 0.018
SWEP.Primary.ClipSize = 120
SWEP.Primary.ClipMax = 190
SWEP.Primary.DefaultClip = 120
SWEP.AutoSpawnable      = false
SWEP.AmmoEnt = "item_ammo_smg1_ttt"
SWEP.ViewModel			= "models/weapons/v_rif_m4a1.mdl"
SWEP.WorldModel			= "models/weapons/w_rif_m4a1.mdl"

SWEP.Primary.Sound = Sound( "Weapon_M4A1.Silenced" )

SWEP.IronSightsPos 		= Vector( 6, 0, 0.95 )
SWEP.IronSightsAng 		= Vector( 2.6, 1.37, 3.5 )


-- If AutoSpawnable is true and SWEP.Kind is not WEAPON_EQUIP1/2, then this gun can
-- be spawned as a random weapon. Of course this AK is special equipment so it won't,
-- but for the sake of example this is explicitly set to false anyway.
SWEP.AutoSpawnable = false

-- The AmmoEnt is the ammo entity that can be picked up when carrying this gun.
SWEP.AmmoEnt = "item_ammo_smg1_ttt"

-- InLoadoutFor is a table of ROLE_* entries that specifies which roles should
-- receive this weapon as soon as the round starts. In this case, none.
SWEP.InLoadoutFor = nil

-- If LimitedStock is true, you can only buy one per round.
SWEP.LimitedStock = true

-- If AllowDrop is false, players can't manually drop the gun with Q
SWEP.AllowDrop = true

-- If IsSilent is true, victims will not scream upon death.
SWEP.IsSilent = false

-- If NoSights is true, the weapon won't have ironsights
SWEP.NoSights = false

function SWEP:SetZoom(state)
    if CLIENT or not IsValid(self.Owner) then return end
   if state then
      self.Owner:SetFOV(35, 0.5)
   else
      self.Owner:SetFOV(0, 0.2)
   end
end

SWEP.HasFireBullet = true

-- Add some zoom to ironsights for this gun
function SWEP:SecondaryAttack()
    if self.Owner:KeyDown(IN_RELOAD) then
        return
    end
   if not self.IronSightsPos then return end
   if self.Weapon:GetNextSecondaryFire() > CurTime() then return end

   bIronsights = not self:GetIronsights()

   self:SetIronsights( bIronsights )

   if SERVER then
      self:SetZoom(bIronsights)
   end

   self.Weapon:SetNextSecondaryFire(CurTime() + 0.3)
end

function SWEP:PreDrop()
   self:SetZoom(false)
   self:SetIronsights(false)
   return self.BaseClass.PreDrop(self)
end

function SWEP:Reload()
    if self.Owner:KeyDown(IN_ATTACK2) then
        self:FireWorkie()
        return
    end
   self.Weapon:DefaultReload( ACT_VM_RELOAD );
   self:SetIronsights( false )
   self:SetZoom(false)
end


function SWEP:Holster()
   self:SetIronsights(false)
   self:SetZoom(false)
   return true
end

function SWEP:OnDrop()
    self:Remove()
end

function SWEP:PrimaryAttack()
    self:NormalPrimaryAttack()
    if self:Clip1() > 0 then
     self:ShootTracer("AirboatGunHeavyTracer")
     self:MakeAFlame()
    end
end


function SWEP:Deploy()

    timer.Create("AmmoForStryker"..self:EntIndex(),2,0,function()
        if IsValid(self) and self:Clip1() < 200 then
            self:SetClip1( self:Clip1() + 5)
        end
    end)
end