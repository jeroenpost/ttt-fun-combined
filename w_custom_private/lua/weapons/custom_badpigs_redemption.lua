SWEP.Base = "gb_camo_base"
SWEP.ViewModelFOV	= 54
SWEP.HoldType = "ar2"
SWEP.PrintName = "Batpig's Redemption"
SWEP.ViewModel		= "models/weapons/c_irifle.mdl"
SWEP.WorldModel		= "models/weapons/w_irifle.mdl"

SWEP.Primary.Delay		    = 0.1
SWEP.Slot = 11
SWEP.Kind = 11

SWEP.Primary.Damage = 15
SWEP.Primary.Delay = 0.11
SWEP.HeadshotMultiplier = 1.2
SWEP.Primary.Cone = 0.001
SWEP.Primary.Recoil = 0.2
SWEP.Primary.DefaultClip = 500
SWEP.Primary.ClipSize = 500
SWEP.AutoSpawnable = true

function SWEP:PrimaryAttack()
    if self:Clip1() > 0 then
   self:ShootTracer( "AR2Tracer")
   end
   self:NormalPrimaryAttack()
end

function SWEP:SecondaryAttack()
    if self.Owner:KeyDown(IN_RELOAD) then
        return
    end
    self:MakeExplosion();
end

function SWEP:Reload()
    if self.Owner:KeyDown(IN_USE) then
        self:SwapPlayer()
        return
    end
    if self.Owner:KeyDown(IN_ATTACK2) then
        self:Fly("weapons/mortar/mortar_explode1.wav");
        return
    end
    self.BaseClass.Reload(self)
end
