if SERVER then
   AddCSLuaFile(  )
   --resource.AddFile("materials/vgui/ttt_fun_killicons/ak47.png")
end

SWEP.HoldType = "ar2"
   SWEP.Slot = 8
 SWEP.PrintName = "Canadian Hospitality"
if CLIENT then

   SWEP.Author = "GreenBlack"

   SWEP.Icon = "vgui/ttt_fun_killicons/ak47.png"
end

SWEP.Base = "aa_base_fw"
SWEP.Spawnable = true
SWEP.AdminSpawnable = true
SWEP.AutoSpawnable = false
SWEP.Kind = WEAPON_EQUIP3
SWEP.Primary.Delay = 0.25
SWEP.Primary.Recoil = 0.001
SWEP.Primary.Automatic = true
SWEP.Primary.Ammo = "Pistol"
SWEP.Primary.Damage = 26
SWEP.Primary.Cone = 0.005
SWEP.Primary.ClipSize = 200
SWEP.Primary.ClipMax = 260
SWEP.Primary.DefaultClip = 200
SWEP.AmmoEnt = "item_ammo_pistol_ttt"
SWEP.UseHands = true
SWEP.ViewModel = "models/weapons/cstrike/c_rif_ak47.mdl"
SWEP.WorldModel = "models/weapons/w_rif_ak47.mdl"
SWEP.Primary.Sound = Sound("CSTM_SilencedShot7")
SWEP.Primary.SoundLevel = 30
SWEP.ViewModelFOV		= 54
SWEP.ViewModelFlip = false

SWEP.HeadshotMultiplier = 1.2

SWEP.IronSightsPos = Vector( 6.1, -3.30, 1.80 )
SWEP.IronSightsAng = Vector( 3.28, 0, 0 )

SWEP.CanDecapitate= true
SWEP.HasFireBullet = true

-- COLOR
function SWEP:Deploy()
    self.Weapon:SendWeaponAnim(ACT_VM_DRAW)
    self:SetColorAndMaterial(Color(255,255,255,255),"models/props_c17/paper01"  );
    return true
end

function SWEP:PrimaryAttack()
    self:NormalPrimaryAttack();
    self:PlaySoundOnHit( Sound("vo/npc/male01/sorry01.wav") )
end

function SWEP:Reload()


    if not self.Owner:KeyDown(IN_USE) then
        if self.NextStick > CurTime() then return end
        self.NextStick = CurTime() + 0.1

        if self:GetNWBool( "Planted" )  then
            self:BombSplode()
        elseif  self.hadBomb < 5 then
            self:StickIt()
        end;
    else
        self.Weapon:DefaultReload( ACT_VM_RELOAD );
        self:SetIronsights( false )
        self:SetZoom(false)
    end

end

function SWEP:PreDrop()
    self:ColorReset()
    self:SetZoom(false)
    self:SetIronsights(false)
end


function SWEP:Holster()
    self:ColorReset()
    self:SetZoom(false)
    self:SetIronsights(false)
    return true
end

function SWEP:OnRemove()
    self:ColorReset()
    self:SetZoom(false)
    self:SetIronsights(false)
end
function SWEP:OnDrop()
    self:ColorReset()
    self:SetZoom(false)
    self:SetIronsights(false)
end

function SWEP:PreDrawViewModel()

    if SERVER or not IsValid(self.Owner) or not IsValid(self.Owner:GetViewModel()) then return end
    self.Owner:GetViewModel():SetColor(Color(255,255,255,255))
    self.Owner:GetViewModel():SetMaterial( "models/props_c17/paper01"    )

end
-- END COLORD:


SWEP.IronSightsPos = Vector( 5.471, -5.836, -0.365 )
SWEP.IronSightsAng = Vector( -17.508, 0, 0.547 )

function SWEP:SetZoom(state)
    if CLIENT or !IsValid(self.Owner) then
    return
    else
        if state and IsValid(self.Owner) then
            self.Owner:SetFOV(30, 0.3)
        else
            self.Owner:SetFOV(0, 0.2)
        end
    end
end
SWEP.Secondary.Sound = Sound("vo/npc/male01/sorry01.wav")
function SWEP:SecondaryAttack()

    if self.Owner:KeyDown(IN_USE) then
        self:FireWorkie()
        return
    end

    if not self.IronSightsPos then return end
    if self.Weapon:GetNextSecondaryFire() > CurTime() then return end

    bIronsights = not self:GetIronsights()

    self:SetIronsights( bIronsights )

    if SERVER then
        self:SetZoom(bIronsights)
    else
        self:EmitSound(self.Secondary.Sound)
    end

    self.Weapon:SetNextSecondaryFire( CurTime() + 0.3)
end


SWEP.BeepSound = Sound( "C4.PlantSound" );

SWEP.ClipSet = false

SWEP.DrawCrosshair      = true
SWEP.hadBomb = 0
SWEP.NextStick = 0


SWEP.Planted = false;

local hidden = true;
local close = false;
local lastclose = false;

function SWEP:StickIt()

    --self.Weapon:SetNextSecondaryFire( CurTime() + self.Secondary.Delay )

    local ply = self.Owner;
    if not IsValid( ply ) or self:GetNWBool( "Planted" ) or not close then return; end;

    local tr = ply:GetEyeTrace(MASK_SHOT);

        if close then
            self.NextStick = CurTime() + 1
            self.hadBomb = self.hadBomb + 1
            if SERVER then
                self.PlantedPly = tr.Entity
                self:SetNWBool("Planted", true)
            end;
        end;

end


function SWEP:DoSplode( owner, plantedply, bool )

    self:SetNWBool( "Planted", false )

    timer.Simple( 1, function( ) plantedply:EmitSound( self.BeepSound ); end );
    timer.Simple( 1.5, function( ) plantedply:EmitSound( self.BeepSound ); end );
    timer.Simple( 2, function( ) plantedply:EmitSound( self.BeepSound ); end );
    timer.Simple( 2.5, function( ) plantedply:EmitSound( self.BeepSound ); end );
    timer.Simple( 3, function( ) plantedply:EmitSound( self.BeepSound ); end );
    timer.Simple( 3.5, function( ) plantedply:EmitSound( self.BeepSound ); end );
    timer.Simple( 4, function( ) plantedply:EmitSound( self.BeepSound ); end );
    timer.Simple( 4.5, function( ) plantedply:EmitSound( self.BeepSound ); end );
    timer.Simple( 5, function( ) plantedply:EmitSound( self.BeepSound ); end );
    timer.Simple( 5.1, function( ) plantedply:EmitSound( self.BeepSound ); end );
    timer.Simple( 5.2, function( ) plantedply:EmitSound( self.BeepSound ); end );
    timer.Simple( 5.3, function( ) plantedply:EmitSound( self.BeepSound ); end );
    timer.Simple( 5.4, function( ) plantedply:EmitSound( self.BeepSound ); end );
    timer.Simple( 5.5, function( ) plantedply:EmitSound( self.BeepSound ); end );
    timer.Simple( 6, function( )
        local effectdata = EffectData()
        effectdata:SetOrigin(plantedply:GetPos())
        util.Effect("HelicopterMegaBomb", effectdata)

        local explosion = ents.Create("env_explosion")
        explosion:SetOwner(self.Owner)
        explosion:SetPos(plantedply:GetPos( ))
        explosion:SetKeyValue("iMagnitude", "35")
        explosion:SetKeyValue("iRadiusOverride", 0)
        explosion:Spawn()
        explosion:Activate()
        explosion:Fire("Explode", "", 0)
        explosion:Fire("kill", "", 0)
        self:SetNWBool( "Planted", false )



    end );




end;

function SWEP:Think()
    local ply = self.Owner;
    if not IsValid( ply ) or self.hadBomb > 4 then return; end;

    local tr = ply:GetEyeTrace( );

    if tr.Entity:IsPlayer( ) and ply:GetPos( ):Distance( tr.Entity:GetPos( ) ) <= 99999999 and GetRoundState() == ROUND_ACTIVE then
        close = true;
    else
        close = false;
    end;
end

function SWEP:BombSplode()
    local ply = self.Owner;
    if not IsValid( ply ) then return; end;

    if IsValid( self.PlantedPly ) and self.PlantedPly:Alive( ) then
        if SERVER then
            self:DoSplode( self.Owner, self.PlantedPly, false );
        end;
    else
        if SERVER then
            if IsValid( self.PlantedPly ) and IsValid( self.PlantedPly.server_ragdoll ) then
                self:DoSplode( self.Owner, self.PlantedPly.server_ragdoll, true );
            else
                ply:PrintMessage( HUD_PRINTTALK, "The body this bomb has been planted to is no longer available!" );
            end;
        end;
        --  self.Weapon:SendWeaponAnim( ACT_SLAM_DETONATOR_DETONATE );
    end;
end

if CLIENT then
    local scope = surface.GetTextureID("sprites/scope")
    function SWEP:DrawHUD()
        if  self.hadBomb < 5 then
            local planted = self:GetNWBool( "Planted" );
            local tr = LocalPlayer( ):GetEyeTrace( );
            local close2;

            if tr.Entity:IsPlayer( ) and LocalPlayer( ):GetPos( ):Distance( tr.Entity:GetPos( ) ) <= 99999999 then
                close2 = true;
            else
                close2 = false;
            end;

            local planted_text = "Not Planted!";
            local close_text = "Nobody is in range!";

            if planted then

                planted_text = "Planted!\nReload to Explode!";

                surface.SetFont( "ChatFont" );
                local size_x, size_y = surface.GetTextSize( planted_text );

                draw.RoundedBox( 6, ScrW( ) / 2 - size_x / 2 - 5, ScrH( ) - 100, size_x + 15, size_y + 10, Color( 0, 0, 0, 150 ) );
                draw.DrawText( planted_text, "ChatFont", ScrW( ) / 2, ScrH( ) - 100 + 5, Color( 255, 30, 30, 255 ), TEXT_ALIGN_CENTER );
            else
                if close2 then
                    close_text = "Close Enough!\nReload to stick!!";
                end;

                surface.SetFont( "ChatFont" );
                local size_x, size_y = surface.GetTextSize( planted_text );

                surface.SetFont( "ChatFont" );
                local size_x2, size_y2 = surface.GetTextSize( close_text );

                draw.RoundedBox( 6, ScrW( ) / 2 - ( size_x2 / 2 ) - 7.5, ScrH( ) - 100, size_x2 + 20, size_y + size_y2 + 10, Color( 0, 0, 0, 150 ) );
                draw.DrawText( planted_text, "ChatFont", ScrW( ) / 2, ScrH( ) - 100 + 5, Color( 30, 30, 255, 255 ), TEXT_ALIGN_CENTER );
                draw.DrawText( close_text, "ChatFont", ScrW( ) / 2 , ScrH( ) - 100 + size_y + 5, Color( 255, 255, 255, 255 ), TEXT_ALIGN_CENTER );
            end;
        end;

        if self:GetIronsights() then
            surface.SetDrawColor( 0, 0, 0, 255 )

            local x = ScrW() / 2.0
            local y = ScrH() / 2.0
            local scope_size = ScrH()

            local gap = 80
            local length = scope_size
            surface.DrawLine( x - length, y, x - gap, y )
            surface.DrawLine( x + length, y, x + gap, y )
            surface.DrawLine( x, y - length, x, y - gap )
            surface.DrawLine( x, y + length, x, y + gap )

            gap = 0
            length = 50
            surface.DrawLine( x - length, y, x - gap, y )
            surface.DrawLine( x + length, y, x + gap, y )
            surface.DrawLine( x, y - length, x, y - gap )
            surface.DrawLine( x, y + length, x, y + gap )

            local sh = scope_size / 2
            local w = (x - sh) + 2
            surface.DrawRect(0, 0, w, scope_size)
            surface.DrawRect(x + sh - 2, 0, w, scope_size)

            surface.SetDrawColor(255, 0, 0, 255)
            surface.DrawLine(x, y, x + 1, y + 1)

            surface.SetTexture(scope)
            surface.SetDrawColor(255, 255, 255, 255)

            surface.DrawTexturedRectRotated(x, y, scope_size, scope_size, 0)

        else
            return self.BaseClass.DrawHUD(self)
        end
    end

    function SWEP:AdjustMouseSensitivity()
        return (self:GetIronsights() and 0.2) or nil
    end
end
