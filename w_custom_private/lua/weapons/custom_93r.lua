
// Variables that are used on both client and server
SWEP.Category				= "Doktor's SWEPs"
SWEP.Author				= "Doktor haus"
SWEP.Contact				= ""
SWEP.Purpose				= ""
SWEP.Instructions				= "Round: 9x19mm Parabellum \nVelocity: ~350 m/s \nSights: Iron \nCapacity: 15 rounds \nRate of Fire: 1000 rpm \nManufacturer: Pietro Beretta \nCountry of origin: Italy"
SWEP.MuzzleAttachment			= "1" 	-- Should be "1" for CSS models or "muzzle" for hl2 models
SWEP.ShellEjectAttachment			= "2" 	-- Should be "2" for CSS models or "1" for hl2 models
SWEP.DrawCrosshair			= true	

SWEP.ViewModelFOV			= 65
SWEP.ViewModelFlip			= false
SWEP.ViewModel				= "models/weapons/v_trh_92fs.mdl"
SWEP.WorldModel				= "models/weapons/w_trh_92fs.mdl"
SWEP.Base 				= "gb_camo_base"
SWEP.Spawnable				= true
SWEP.AdminSpawnable			= true

SWEP.Camo = 7

SWEP.Kind = PISTOL
SWEP.PrintName				= "93R"			// 'Nice' Weapon name (Shown on HUD)
SWEP.Slot				= 2				// Slot in the weapon selection menu
SWEP.SlotPos				= 3				// Position in the slot
SWEP.DrawAmmo				= true				// Should draw the default HL2 ammo counter				// Should draw the default crosshair
SWEP.DrawWeaponInfoBox			= true				// Should draw the weapon info box
SWEP.BounceWeaponIcon   			= true				// Should the weapon icon bounce?


SWEP.Weight				= 30		// Decides whether we should switch from/to this
SWEP.AutoSwitchTo			= true		// Auto switch to if we pick it up
SWEP.AutoSwitchFrom			= true		// Auto switch from if you pick up a better weapon

function SWEP:Initialize()
	self:SetHoldType("pistol")
end

function SWEP:OnRemove()
end

function SWEP:OnDrop()
    self:Remove()
end

SWEP.nextReload = 0
function SWEP:Reload()
    if self.nextReload > CurTime() then return end
    self.nextReload = CurTime() + 0.5
    self.Weapon:DefaultReload(self.ReloadAnim)
    self:SetIronsights( false )
end

 
SWEP.Primary.Sound = Sound( "Weapon_Elite.Single" )

SWEP.Primary.RPM			= 1000						// This is in Rounds Per Minute
SWEP.Primary.ClipSize			= 30						// Size of a clip
SWEP.Primary.DefaultClip		= 60
SWEP.Primary.Damage                     = 33
SWEP.Primary.ConeSpray			= 1.0					// Hip fire accuracy
SWEP.Primary.ConeIncrement		= 1.5					// Rate of innacuracy
SWEP.Primary.ConeMax			= 3.0					// Maximum Innacuracy
SWEP.Primary.ConeDecrement		= 0.1					// Rate of accuracy
SWEP.Primary.KickUp				= 0.4					// Maximum up recoil (rise)
SWEP.Primary.KickDown			= 0.3					// Maximum down recoil (skeet)
SWEP.Primary.KickHorizontal		= 0.3					// Maximum up recoil (stock)
SWEP.Primary.Automatic			= true					// Automatic/Semi Auto
SWEP.Primary.Ammo			= "pistol"
SWEP.Primary.Delay                      = 0.35
SWEP.Secondary.ClipSize			= 1						// Size of a clip
SWEP.Secondary.DefaultClip		= 1						// Default number of bullets in a clip
SWEP.Secondary.Automatic		= false						// Automatic/Semi Auto
SWEP.Secondary.Ammo			= ""
SWEP.Secondary.IronFOV			= 60						// How much you 'zoom' in. Less is more! 	
SWEP.AmmoEnt = "item_ammo_pistol_ttt"
SWEP.data 				= {}						// The starting firemode
SWEP.data.ironsights			= 1

SWEP.IronSightsPos = Vector(-3.961, 0, 0.56)
SWEP.IronSightsAng = Vector(0, 0, 0)
SWEP.SightsPos = Vector(-3.961, 0, 0.56)
SWEP.SightsAng = Vector(0, 0, 0)
SWEP.RunSightsPos = Vector (0.4751, 0, 1.8442)
SWEP.RunSightsAng = Vector (-17.6945, -1.4012, 0)
SWEP.GSightsPos = Vector (0.8672, -2.1202, -2.5784)
SWEP.GSightsAng = Vector (0, 0, 65.0667)

SWEP.Primary.Sound2 = { "weapons/warden/warden1.mp3","weapons/warden/warden2.mp3","weapons/warden/warden3.mp3"}


SWEP.burst = 0

function SWEP:SecondaryAttack(worldsnd)
   self:Fly()
end

function SWEP:PrimaryAttack(worldsnd)
    if self.burst < 3 then
        self.Weapon:SetNextSecondaryFire( CurTime() + 0.15 )
        self.Weapon:SetNextPrimaryFire( CurTime() + 0.15 )
        self.burst = self.burst + 1
        if self.burst == 3 then
            timer.Simple(0.45, function() if IsValid( self ) then
                self.burst = 0
            end end)
        end
    else
       return 
    end

   if not self:CanPrimaryAttack() then return end
   
    --self.Weapon:EmitSound( table.Random(self.Primary.Sound2), 400 )

    sound.Play( self.Primary.Sound, self:GetPos(), 100)

   self:ShootBullet( self.Primary.Damage, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )

	   self:TakePrimaryAmmo( 1 )
	   local owner = self.Owner   
	   --if not ValidEntity(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end
	   
	   owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )
   end

