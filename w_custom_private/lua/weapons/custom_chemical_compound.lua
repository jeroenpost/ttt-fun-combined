if (SERVER) then

    AddCSLuaFile( )
    SWEP.Weight                = 5
    SWEP.AutoSwitchTo        = false
    SWEP.AutoSwitchFrom        = false

end

if ( CLIENT ) then

    SWEP.DrawAmmo            = true
    SWEP.DrawCrosshair        = false
    SWEP.ViewModelFOV        = 65
    SWEP.ViewModelFlip        = false
    SWEP.CSMuzzleFlashes    = true
    
end
SWEP.Base                   = "aa_base"
SWEP.Category                = "EXP 2 Weapons"
SWEP.PrintName                 = "The Chemical Compound"
SWEP.HoldType                 = "rpg"
SWEP.Slot = 0
SWEP.SlotPos = 2

SWEP.Author            = "Mighty Lolrus"
SWEP.Icon = "vgui/ttt_fun_killicons/bow.png"

SWEP.ViewModel                  = "models/weapons/v_gbip_scoub.mdl"
SWEP.WorldModel 				= "models/weapons/w_gbip_scoub.mdl"

SWEP.Kind = WEAPON_MELEE
SWEP.HoldType = "smg"

SWEP.Spawnable            = true
SWEP.AdminSpawnable        = true

SWEP.Primary.Sound			= Sound("weapons/requests_studio/gb2/fire.mp3")
SWEP.Primary.Delay          = 0.75
SWEP.Primary.Recoil         = 1
SWEP.Primary.Automatic = true
SWEP.Primary.Ammo = "357"
SWEP.Primary.Damage = 45
SWEP.Primary.Cone = 0.005
SWEP.Primary.ClipSize = 80
SWEP.Primary.ClipMax = 80 -- keep mirrored to ammo
SWEP.Primary.DefaultClip = 80
SWEP.HeadshotMultiplier = 1
SWEP.CanDecapitate= true
SWEP.Secondary.Sound = Sound("Default.Zoom")

SWEP.IsSilent = true
SWEP.IronSightsPos = Vector(13.41, 13.308, -18.549)
SWEP.IronSightsAng = Vector(1.5, -0.401, -53.741)

SWEP.Offset = {
    Pos = {
        Up = 0,
        Right = -1,
        Forward = -4.5,
    },
    Ang = {
        Up = 0,
        Right = 14,
        Forward = 30,
    }
}

function SWEP:OnDrop()

    local wep = ents.Create(self:GetClass())
    wep:SetPos(self:GetPos()+Vector(0,100,0))
    wep:SetAngles(self:GetAngles())
    wep.IsDropped = true
    self:Remove(self)
    wep:Spawn()

end

function SWEP:DrawWorldModel( )
    local hand, offset, rotate

    if not IsValid( self.Owner ) then
        self:DrawModel( )
        return
    end

    if not self.Hand then
        self.Hand = self.Owner:LookupAttachment( "anim_attachment_rh" )
    end

    hand = self.Owner:GetAttachment( self.Hand )

    if not hand then
        self:DrawModel( )
        return
    end

    offset = hand.Ang:Right( ) * self.Offset.Pos.Right + hand.Ang:Forward( ) * self.Offset.Pos.Forward + hand.Ang:Up( ) * self.Offset.Pos.Up

    hand.Ang:RotateAroundAxis( hand.Ang:Right( ), self.Offset.Ang.Right )
    hand.Ang:RotateAroundAxis( hand.Ang:Forward( ), self.Offset.Ang.Forward )
    hand.Ang:RotateAroundAxis( hand.Ang:Up( ), self.Offset.Ang.Up )

    self:SetRenderOrigin( hand.Pos + offset )
    self:SetRenderAngles( hand.Ang )

    self:DrawModel( )
end


-- COLOR

-- END COLORD:


function SWEP:PrimaryAttack()

        self.Weapon:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
        self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )

        if not self:CanPrimaryAttack() then return end



        if SERVER then
            sound.Play(self.Primary.Sound, self:GetPos(), self.Primary.SoundLevel)
        --    self.Owner:PrintMessage( HUD_PRINTCONSOLE, "DWPunishment Bow Damage: "..damageee.." x "..multiplier )
        end

        self:ShootBullet( 55, 1, 0 )
        self:TakePrimaryAmmo( 1 )

        self:FireBolt(0)
        self:Blind()
        timer.Simple(0.01,function()
        self:FireBolt(1)
        end)
        timer.Simple(0.03, function() self:FireBolt(2) end)
        timer.Simple(0.05,function() self:FireBolt(3) end)
        timer.Simple(0.07, function() self:FireBolt(4) end)

        local owner = self.Owner
        if not IsValid(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

        owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )
end

function SWEP:FireBolt(num)
    local pOwner = self.Owner;

    if ( pOwner == NULL ) then return;   end

    if ( SERVER ) then
        local vecAiming		= pOwner:GetAimVector();
        local vecSrc		= pOwner:GetShootPos();
        if num == 1 then
            vecAiming		= pOwner:GetAimVector() + Vector(0.1,0.00,0.0);

        end
        if num == 2 then
            vecAiming		= pOwner:GetAimVector() + Vector(-0.1,0,0);
        end
        if num == 3 then
            vecAiming		= pOwner:GetAimVector() + Vector(0.00,0.0,0.1);
        end
        if num == 4 then
            vecAiming		= pOwner:GetAimVector() + Vector(0.00,-0.0,-0.1);
        end
        if num ~= 0 then
            self:ShootBullet( 10, 1, 0, vecAiming )
        end

        local angAiming;
        angAiming = vecAiming:Angle();

        local pBolt = ents.Create ( "crossbow_bolt" );
        pBolt:SetPos( vecSrc );
        pBolt:SetAngles( angAiming );
        pBolt.Damage = 0 --self.Primary.Damage;

        pBolt.AmmoType = "crossbow_bolt";
        pBolt:SetOwner( pOwner );
        pBolt:Spawn()


        if ( pOwner:WaterLevel() == 3 ) then
            pBolt:SetVelocity( vecAiming * 1500);
        else
            pBolt:SetVelocity( vecAiming * 3500 );
        end
    end
end

function SWEP:ShootBullet( damage, num_bullets, aimcone, aim )

    local bullet = {}
    bullet.Num 		= num_bullets
    bullet.Src 		= self.Owner:GetShootPos()	// Source
    bullet.Dir 		= aim or self.Owner:GetAimVector()	// Dir of bullet
    bullet.Spread 	= Vector( aimcone, aimcone, 0 )		// Aim Cone
    bullet.Tracer	= 5	// Show a tracer on every x bullets
    bullet.TracerName = "Tracer" // what Tracer Effect should be used
    bullet.Force	= 1	// Amount of force to give to phys objects
    bullet.Damage	= damage
    bullet.AmmoType = "Pistol"

    self.Owner:FireBullets( bullet )

    self:ShootEffects()

end


function SWEP:SetZoom(state)
    if CLIENT then 
       return
    elseif IsValid(self.Owner) and self.Owner:IsPlayer() then
       if state then
          self.Owner:SetFOV(20, 0.3)
          self.Primary.Delay          = 0.95
           self.HeadshotMultiplier = 3
       else
          self.Owner:SetFOV(0, 0.2)
          self.Primary.Delay          = 0.65
          self.HeadshotMultiplier = 1
       end
    end
end

-- Add some zoom to ironsights for this gun
function SWEP:SecondaryAttack()
    self:Fly()
end

function SWEP:PreDrop()
    self:SetZoom(false)
    self:SetIronsights(false)
    self:ColorReset()
    return self.BaseClass.PreDrop(self)
end

SWEP.NextZoom = 0
function SWEP:Reload()
    if self.Owner:KeyDown(IN_USE)  then
        if self.NextZoom > CurTime() then return end
        self.NextZoom = CurTime() + 0.5
        if not self.IronSightsPos then return end
        if self.Weapon:GetNextSecondaryFire() > CurTime() then return end

        bIronsights = not self:GetIronsights()

        self:SetIronsights( bIronsights )

        if SERVER then
            self:SetZoom(bIronsights)
        else
            self:EmitSound(self.Secondary.Sound)
        end

        return
    end
    self.Weapon:DefaultReload( ACT_VM_RELOAD );
    self:SetIronsights( false )
    self:SetZoom(false)
end


function SWEP:Holster()
    self:SetIronsights(false)
    self:SetZoom(false)
    self:ColorReset()
    return true
end

function SWEP:OnRemove()
    self:ColorReset()
end
function SWEP:OnDrop()
    self:ColorReset()
    self:Remove()
end

if CLIENT then
   local scope = surface.GetTextureID("sprites/scope")
   function SWEP:DrawHUD()
      if self:GetIronsights() then
         surface.SetDrawColor( 0, 0, 0, 255 )
         
         local x = ScrW() / 2.0
         local y = ScrH() / 2.0
         local scope_size = ScrH()

         -- crosshair
         local gap = 80
         local length = scope_size
         surface.DrawLine( x - length, y, x - gap, y )
         surface.DrawLine( x + length, y, x + gap, y )
         surface.DrawLine( x, y - length, x, y - gap )
         surface.DrawLine( x, y + length, x, y + gap )

         gap = 0
         length = 50
         surface.DrawLine( x - length, y, x - gap, y )
         surface.DrawLine( x + length, y, x + gap, y )
         surface.DrawLine( x, y - length, x, y - gap )
         surface.DrawLine( x, y + length, x, y + gap )


         -- cover edges
         local sh = scope_size / 2
         local w = (x - sh) + 2
         surface.DrawRect(0, 0, w, scope_size)
         surface.DrawRect(x + sh - 2, 0, w, scope_size)

         surface.SetDrawColor(255, 0, 0, 255)
         surface.DrawLine(x, y, x + 1, y + 1)

         -- scope
         surface.SetTexture(scope)
         surface.SetDrawColor(255, 255, 255, 255)

         surface.DrawTexturedRectRotated(x, y, scope_size, scope_size, 0)

      else
         return self.BaseClass.DrawHUD(self)
      end
   end

   function SWEP:AdjustMouseSensitivity()
      return (self:GetIronsights() and 0.2) or nil
   end
end
