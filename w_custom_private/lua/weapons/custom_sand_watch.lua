SWEP.Base = "aa_base"
SWEP.HoldType = "slam"
SWEP.PrintName = "Watch of Doom"

SWEP.ViewModel				= "models/weapons/v_watch.mdl"
SWEP.WorldModel				= "models/weapons/w_watch.mdl"
SWEP.AutoSpawnable = false
SWEP.Slot = 14

if CLIENT then

    SWEP.Author = "GreenBlack"

    SWEP.Icon = "vgui/ttt_fun_killicons/gb_watch.png"
end



SWEP.Kind = 101

SWEP.Primary.Delay          = 2.5
SWEP.Primary.Recoil         = 7
SWEP.Primary.Automatic = false
SWEP.Primary.Ammo = ".357"
SWEP.Primary.Damage = 5
SWEP.Primary.Cone = 0.005
SWEP.Primary.ClipSize = 20
SWEP.Primary.ClipMax = 20 -- keep mirrored to ammo
SWEP.Primary.DefaultClip = 20

SWEP.HeadshotMultiplier = 1

SWEP.AutoSpawnable      = false
SWEP.AmmoEnt = "item_ammo_357_ttt"

SWEP.UseHands			= true
SWEP.ViewModelFlip		= false
SWEP.ViewModelFOV		= 75

--SWEP.Primary.Sound = Sound("watch.single")

SWEP.Secondary.Sound = Sound("Default.Zoom")

SWEP.IronSightsPos      = Vector( 5, -15, -2 )
SWEP.IronSightsAng      = Vector( 2.6, 1.37, 3.5 )

SWEP.NextShot = 0
function SWEP:PrimaryAttack()
    if self.NextShot > CurTime() then return end
    self.NextShot = CurTime() + self.Primary.Delay
    self.BaseClass.PrimaryAttack(self)

    local tr = self.Owner:GetEyeTrace();

    if IsValid( tr.Entity ) and tr.Entity:IsPlayer()  then

        local edata = EffectData()
        edata:SetStart(self.Owner:GetShootPos())
        edata:SetOrigin(tr.HitPos)
        edata:SetNormal(tr.Normal)
        edata:SetEntity(tr.Entity)
         util.Effect("BloodImpact", edata)

        tr.Entity:SetNWFloat("durgz_mushroom_high_start", CurTime())
        tr.Entity:SetNWFloat("durgz_mushroom_high_end", CurTime()+10)

    local v = tr.Entity
    local owner = self.Owner
        if SERVER then
            timer.Create(v:EntIndex().."poisonknife",2,5,function()
                if IsValid(v) and IsValid(owner) and v:Health() > 1 then
                    v:TakeDamage( 5,
                        owner)
                    v:Ignite(0.2)
                    hook.Add("TTTEndRound",v:EntIndex().."poisonknife",function()

                        timer.Destroy(v:EntIndex().."poisonknife");
                        if IsValid(v) and isfunction( v.Extinguish ) then
                            v:Extinguish()
                        end
                    end)
                end
            end)
        end
    end

if CLIENT then
    local ENT = SWEP
    local TRANSITION_TIME = 5; --transition effect from sober to high, high to sober, in seconds how long it will take etc.
    local HIGH_INTENSITY = 0.77; --1 is max, 0 is nothing at all
    local TIME_TO_GO_ACROSS_SCREEN = 3;
    local whichWay = 2;
    local startawesomefacemove = 0;

    local function DoMushrooms()

        local pl = LocalPlayer();


        local shroom_tab = {}
        shroom_tab[ "$pp_colour_addr" ] = 0
        shroom_tab[ "$pp_colour_addg" ] = 0
        shroom_tab[ "$pp_colour_addb" ] = 0
                //shroom_tab[ "$pp_colour_brightness" ] = 0
                //shroom_tab[ "$pp_colour_contrast" ] = 1
        shroom_tab[ "$pp_colour_mulr" ] = 0
        shroom_tab[ "$pp_colour_mulg" ] = 0
        shroom_tab[ "$pp_colour_mulb" ] = 0


        if( pl:GetNWFloat("durgz_mushroom_high_start") && pl:GetNWFloat("durgz_mushroom_high_end") > CurTime() )then

        if( pl:GetNWFloat("durgz_mushroom_high_start") + TRANSITION_TIME > CurTime() )then

            local s = pl:GetNWFloat("durgz_mushroom_high_start");
            local e = s + TRANSITION_TIME;
            local c = CurTime();
            local pf = (c-s) / (e-s);

            shroom_tab[ "$pp_colour_colour" ] =   1 - pf*0.37
            shroom_tab[ "$pp_colour_brightness" ] = -pf*0.15
            shroom_tab[ "$pp_colour_contrast" ] = 1 + pf*1.57
                    //DrawMotionBlur( 1 - 0.18*pf, 1, 0);
            DrawColorModify( shroom_tab )
            DrawSharpen( 8.32,1.03*pf )

        elseif( pl:GetNWFloat("durgz_mushroom_high_end") - TRANSITION_TIME < CurTime() )then

            local e = pl:GetNWFloat("durgz_mushroom_high_end");
            local s = e - TRANSITION_TIME;
            local c = CurTime();
            local pf = 1 - (c-s) / (e-s);

            shroom_tab[ "$pp_colour_colour" ] = 1 - pf*0.37
            shroom_tab[ "$pp_colour_brightness" ] = -pf*0.15
            shroom_tab[ "$pp_colour_contrast" ] = 1 + pf*1.57
                    //DrawMotionBlur( 1 - 0.18*pf, 1, 0);
            DrawColorModify( shroom_tab )
            DrawSharpen( 8.32,1.03*pf )

        else

            shroom_tab[ "$pp_colour_colour" ] = 0.63
            shroom_tab[ "$pp_colour_brightness" ] = -0.15
            shroom_tab[ "$pp_colour_contrast" ] = 2.57
                    //DrawMotionBlur( 0.82, 1, 0);
            DrawColorModify( shroom_tab )
            DrawSharpen( 8.32,1.03 )

        end


        end
    end
    hook.Add("RenderScreenspaceEffects", "durgz_mushroom_high", DoMushrooms)
end

end

function SWEP:SecondaryAttack()
    self:Zoom()
end

function SWEP:PreDrop()
    self:SetZoom(false)
    self:SetIronsights(false)
    return self.BaseClass.PreDrop(self)
end

SWEP.NextReload = 0
SWEP.LaserColor = Color(math.random(1,255),math.random(1,255),math.random(1,255),255)
function SWEP:Reload()
    if self.Owner:KeyDown(IN_USE) then
        if self.NextReload > CurTime() then return end
        self.NextReload = CurTime() + 0.3
        self.LaserColor = Color(math.random(1,255),math.random(1,255),math.random(1,255),255)
            self.Owner:EmitSound("weapons/gauss/fire1.wav")
            self:ShootTracer( "LaserTracer_thick" )
            self:ShootBullet( 12, 1, 1, 0.05 )
        return
    end
    self.Weapon:DefaultReload( ACT_VM_RELOAD );
    self:SetIronsights( false )
    self:SetZoom(false)
end

function SWEP:Holster()
    self:SetIronsights(false)
    self:SetZoom(false)
    return true
end


function SWEP:Zoom()
    if not self.IronSightsPos then return end
    if self.Weapon:GetNextSecondaryFire() > CurTime() then return end

    local bIronsights = not self:GetIronsights()

    self:SetIronsights( bIronsights )

    if SERVER then
        self:SetZoom(bIronsights)
    else
        self:EmitSound(self.Secondary.Sound)
    end

    self.Weapon:SetNextSecondaryFire( CurTime() + 0.3)
end




function SWEP:SetZoom(state)
    if CLIENT then
        return
    elseif IsValid(self.Owner) and self.Owner:IsPlayer() then
        if state then
            self.Owner:SetFOV(30, 0.3)
        else
            self.Owner:SetFOV(0, 0.2)
        end
    end
end

if CLIENT then
    local scope = surface.GetTextureID("sprites/scope")
    function SWEP:DrawHUD()
        if self:GetIronsights() then
            surface.SetDrawColor( 0, 0, 0, 255 )

            local x = ScrW() / 2.0
            local y = ScrH() / 2.0
            local scope_size = ScrH()

            -- crosshair
            local gap = 80
            local length = scope_size
            surface.DrawLine( x - length, y, x - gap, y )
            surface.DrawLine( x + length, y, x + gap, y )
            surface.DrawLine( x, y - length, x, y - gap )
            surface.DrawLine( x, y + length, x, y + gap )

            gap = 0
            length = 50
            surface.DrawLine( x - length, y, x - gap, y )
            surface.DrawLine( x + length, y, x + gap, y )
            surface.DrawLine( x, y - length, x, y - gap )
            surface.DrawLine( x, y + length, x, y + gap )


            -- cover edges
            local sh = scope_size / 2
            local w = (x - sh) + 2
            surface.DrawRect(0, 0, w, scope_size)
            surface.DrawRect(x + sh - 2, 0, w, scope_size)

            surface.SetDrawColor(255, 0, 0, 255)
            surface.DrawLine(x, y, x + 1, y + 1)

            -- scope
            surface.SetTexture(scope)
            surface.SetDrawColor(255, 255, 255, 255)

            surface.DrawTexturedRectRotated(x, y, scope_size, scope_size, 0)

        else
            return self.BaseClass.DrawHUD(self)
        end
    end

    function SWEP:AdjustMouseSensitivity()
        return (self:GetIronsights() and 0.2) or nil
    end
end
