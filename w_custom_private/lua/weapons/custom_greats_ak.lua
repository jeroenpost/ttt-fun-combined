SWEP.Base = "gb_camo_base"
SWEP.ViewModelFOV	= 54
SWEP.HoldType = "smg"
SWEP.PrintName = "Greats AK"
SWEP.ViewModel		= "models/weapons/cstrike/c_rif_ak47.mdl"
SWEP.WorldModel		= "models/weapons/w_rif_ak47.mdl"
SWEP.AutoSpawnable = false
SWEP.Icon = "vgui/ttt_fun_killicons/ak47.png"
SWEP.Kind = WEAPON_EQUIP1
SWEP.Slot = 5
SWEP.Primary.Delay = 0.11
SWEP.Primary.Recoil = 0.001
SWEP.Primary.Automatic = true
SWEP.Primary.Ammo = "Pistol"
SWEP.Primary.Damage = 15
SWEP.Primary.Cone = 0.018
SWEP.Primary.ClipSize = 200
SWEP.Primary.ClipMax = 200
SWEP.Primary.DefaultClip = 600
SWEP.AmmoEnt = "item_ammo_pistol_ttt"
SWEP.Primary.Sound = Sound("weapons/ak47/ak47-1.wav")

SWEP.HeadshotMultiplier = 1.1

SWEP.IronSightsPos = Vector( -6.6, -8.50, 3.40 )
SWEP.IronSightsAng = Vector( 0, 0, 0 )
SWEP.HasFireBullet = true

function SWEP:PrimaryAttack()
    if self:Clip1() > 0 then
        self:MakeAFlame()
        self:Disorientate()
    end
    self.BaseClass.PrimaryAttack(self)
end