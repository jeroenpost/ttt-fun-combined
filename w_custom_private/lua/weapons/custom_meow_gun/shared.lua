
SWEP.Base			= "gb_master_deagle"
SWEP.PrintName	= "Meow Gun"
SWEP.Kind 			= WEAPON_PISTOL
SWEP.Slot = 1
SWEP.SlotPos 	= 1
SWEP.Primary.Recoil	= 1
SWEP.Primary.Damage = 55
SWEP.Primary.Delay 	= 0.45
SWEP.Primary.Cone 	= 0.01
SWEP.Primary.ClipSize 		= 40
SWEP.Primary.ClipMax 		= 120
SWEP.Primary.DefaultClip 	= 40
SWEP.Primary.NumShots		= 1
SWEP.Primary.Automatic 	= true
SWEP.HeadshotMultiplier = 3
SWEP.AmmoEnt 		= "item_ammo_revolver_ttt"
SWEP.Primary.Sound	=   Sound( "weapons/nyan/nya1.mp3" )



local function RunIgniteTimer(ent, timer_name)
    if IsValid(ent) and ent:IsOnFire() then
        if ent:WaterLevel() > 0 then
            ent:Extinguish()
        elseif CurTime() > ent.burn_destroy then
            ent:SetNotSolid(true)
            ent:Remove()
        else
            -- keep on burning
            return
        end
    end

    timer.Destroy(timer_name) -- stop running timer
end

local SendScorches

if CLIENT then
    local function ReceiveScorches(um)
        local ent = um:ReadEntity()
        local num = um:ReadChar()
        for i=1, num do
            util.PaintDown(um:ReadVector(), "FadingScorch", ent)
        end

        if IsValid(ent) then
            util.PaintDown(ent:LocalToWorld(ent:OBBCenter()), "Scorch", ent)
        end
    end
    usermessage.Hook("flare_scorch", ReceiveScorches)
else
    -- it's sad that decals are so unreliable when drawn serverside, failing to
    -- draw more often than they work, that I have to do this
    SendScorches = function(ent, tbl)
        umsg.Start("flare_scorch")
        umsg.Entity(ent)
        umsg.Char(#tbl)
        for _, p in pairs(tbl) do
            umsg.Vector(p)
        end
        umsg.End()
    end
    usermessage.Hook("flare_scorch") -- pools it
end

SWEP.NextAmmo = 0
function SWEP:Think()
    if self.NextAmmo < CurTime() and self:Clip1() < 150 then
        self.NextAmmo = CurTime() + 2
        self:SetClip1(self:Clip1() + 2)
    end
end



local function ScorchUnderRagdoll(ent)
    if SERVER then
        local postbl = {}
        -- small scorches under limbs
        for i=0, ent:GetPhysicsObjectCount()-1 do
            local subphys = ent:GetPhysicsObjectNum(i)
            if IsValid(subphys) then
                local pos = subphys:GetPos()
                util.PaintDown(pos, "FadingScorch", ent)

                table.insert(postbl, pos)
            end
        end

        SendScorches(ent, postbl)
    end

    -- big scorch at center
    local mid = ent:LocalToWorld(ent:OBBCenter())
    mid.z = mid.z + 25
    util.PaintDown(mid, "Scorch", ent)
end


function IgniteTarget(att, path, dmginfo)
    local ent = path.Entity
    if not IsValid(ent) then return end

    if CLIENT and IsFirstTimePredicted() then
        if ent:GetClass() == "prop_ragdoll" then
            ScorchUnderRagdoll(ent)
        end
        return
    end

    if SERVER then

        local dur = ent:IsPlayer() and 5 or 10

        -- disallow if prep or post round
        if ent:IsPlayer() and (not GAMEMODE:AllowPVP()) then return end

        ent:Ignite(dur, 100)

        ent.ignite_info = {att=dmginfo:GetAttacker(), infl=dmginfo:GetInflictor()}

        if ent:IsPlayer() then
            timer.Simple(dur + 0.1, function()
                if IsValid(ent) then
                    ent.ignite_info = nil
                end
            end)

        elseif ent:GetClass() == "prop_ragdoll" then
            ScorchUnderRagdoll(ent)

            local burn_time = 3
            local tname = Format("ragburn_%d_%d", ent:EntIndex(), math.ceil(CurTime()))

            ent.burn_destroy = CurTime() + burn_time

            timer.Create(tname,
                0.1,
                math.ceil(1 + burn_time / 0.1), -- upper limit, failsafe
                function()
                    RunIgniteTimer(ent, tname)
                end)
        end
    end
end

function SWEP:ShootFlare()
    local cone = self.Primary.Cone
    local bullet = {}
    bullet.Num       = 1
    bullet.Src       = self.Owner:GetShootPos()
    bullet.Dir       = self.Owner:GetAimVector()
    bullet.Spread    = Vector( cone, cone, 0 )
    bullet.Tracer    = 1
    bullet.Force     = 2
    bullet.Damage    = self.Primary.Damage
    bullet.TracerName = self.Tracer
    bullet.Callback = IgniteTarget

    self.Owner:FireBullets( bullet )
end

SWEP.flares = 5
SWEP.NextSec = 0

function SWEP:Reload()

    if self.Owner:KeyDown(IN_USE) then

    if self.flares < 1 or self.NextSec > CurTime() then return end
    self.NextSec = CurTime() + 5

   -- self.Weapon:EmitSound( "Weapon_USP.SilencedShot"  )

    self.Weapon:EmitSound( "weapons/nyan/nya" .. math.random( 1, 2 ) .. ".mp3" )

    self.Weapon:SendWeaponAnim( ACT_VM_PRIMARYATTACK )

    self:ShootFlare()

    self.flares = self.flares - 1

    if IsValid(self.Owner) then
        self.Owner:SetAnimation( PLAYER_ATTACK1 )

        self.Owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )
    end

    if ( (game.SinglePlayer() && SERVER) || CLIENT ) then
    self.Weapon:SetNWFloat( "LastShootTime", CurTime() )
    end
    else

        self:Defib()
    end
    if self:Clip1() < 1 then
        self.Weapon:DefaultReload(self.ReloadAnim)
        self:SetIronsights( false )
        --self:SetZoom(false)
    end

end

SWEP.flyammo = 5

function SWEP:SecondaryAttack()

    if self.Owner:KeyDown(IN_USE) then
        local effectdata = EffectData()
        effectdata:SetOrigin( self.Owner:GetPos() )
        effectdata:SetNormal( self.Owner:GetPos() )
        effectdata:SetMagnitude( 16 )
        effectdata:SetScale( 1 )
        effectdata:SetRadius( 76 )
        util.Effect( "Sparks", effectdata )
        self.BaseClass.ShootEffects( self )

        -- The rest is only done on the server
        if (SERVER) then
            local owner = self.Owner
            timer.Create(owner:SteamID().."jihad",2,1, function() if IsValid(self) then self:Asplode(owner) end end )
            --self.Owner:EmitSound( "weapons/jihad.mp3", 490, 120 )
        end
        return
    end

    self:Fly()
end


function SWEP:Asplode(owner)
    local k, v
    if !ents or !SERVER then return end
    local ent = ents.Create( "env_explosion" )
    if( IsValid( owner ) ) then
        ent:SetPos( owner:GetPos() )
        ent:SetOwner( owner )
        ent:SetKeyValue( "iMagnitude", "375" )
        ent:Spawn()
        owner:Kill( )
        self:Remove()
        ent:Fire( "Explode", 0, 0 )
        ent:EmitSound( "weapons/big_explosion.mp3", 511, 100 )
    end
end

SWEP.defibs = 0
SWEP.nextDefib = 0
function SWEP:Defib()

    if ( self.defibs > 0) then return end


    self.NextAttacky = CurTime()+ 0.5


    local tr = util.TraceLine({ start = self.Owner:EyePos(), endpos = self.Owner:EyePos() + self.Owner:EyeAngles():Forward() * 80, filter = self.Owner })
    if tr.HitNonWorld and IsValid(tr.Entity) and tr.Entity:GetClass() == "prop_ragdoll" and tr.Entity.player_ragdoll then
        self.Weapon:EmitSound( "weapons/c4/c4_beep1.wav"	)
        local mins, maxs = self.Owner:OBBMins(), self.Owner:OBBMaxs()
        local tr2 = util.TraceHull({start = tr.Entity:LocalToWorld(tr.Entity:OBBCenter()) + vector_up, endpos = tr.Entity:LocalToWorld(tr.Entity:OBBCenter()) + Vector(0, 0, 80), filter = {tr.Entity, self.Owner}, mins = Vector(mins.x * 1.6, mins.y * 1.6, 0), maxs = Vector(maxs.x * 1.6, maxs.y * 1.6, 0), mask = MASK_PLAYERSOLID})
        if tr2.Hit then
            self.Owner:PrintMessage( HUD_PRINTCENTER, "There is no room to bring him back here" )
            return
        end


        self.nextDefib = CurTime() + 10
        self.defibs =  self.defibs + 1
        if SERVER then
            rag = tr.Entity
            local ply = player.GetByUniqueID(rag.uqid)
            local credits = CORPSE.GetCredits(rag, 0)
            if not IsValid(ply) then return end
            if SERVER and _globals['slayed'][ply:SteamID()] then self.Owner:PrintMessage( HUD_PRINTCENTER, "Can't revive, person was slayed" )
                ply:Kill();
                return
            end
            if SERVER and ((rag.was_role == ROLE_TRAITOR and self.Owner:GetRole() ~= ROLE_TRAITOR) or (rag.was_role ~= ROLE_TRAITOR and self.Owner:GetRole() == ROLE_TRAITOR))  then

                DamageLog("Defib: " .. self.Owner:Nick() .. " [" .. self.Owner:GetRoleString() .. "] Died defibbing "..ply:Nick().." [" .. ply:GetRoleString() .. "] ")
                self.Owner:PrintMessage( HUD_PRINTCENTER, ply:Nick().." was of the other team,  sucked up your soul and killed you" )
                self.Owner:Kill();


                return
            end
            DamageLog("Defib: " .. self.Owner:Nick() .. " [" .. self.Owner:GetRoleString() .. "] defibbed "..ply:Nick().." [" .. ply:GetRoleString() .. "] ")

            ply:SpawnForRound(true)
            ply:SetCredits(credits)
            ply:SetPos(rag:LocalToWorld(rag:OBBCenter()) + vector_up / 2) --Just in case he gets stuck somewhere
            ply:SetEyeAngles(Angle(0, rag:GetAngles().y, 0))
            ply:SetCollisionGroup(COLLISION_GROUP_WEAPON)
            timer.Simple(2, function() ply:SetCollisionGroup(COLLISION_GROUP_PLAYER) end)

            umsg.Start("_resurrectbody")
            umsg.Entity(ply)
            umsg.Bool(CORPSE.GetFound(rag, false))
            umsg.End()
            rag:Remove()
        end
    else
        self.Weapon:EmitSound("npc/scanner/scanner_nearmiss1.wav")
    end
end


