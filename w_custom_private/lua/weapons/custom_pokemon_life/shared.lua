
if SERVER then
   AddCSLuaFile(  )
   --resource.AddFile("materials/vgui/ttt_fun_killicons/awp.png")
end
SWEP.PrintName = "Breaded's Life or Death"
SWEP.Slot      = 39
   SWEP.SlotPos   = 9
if CLIENT then
   
   
   SWEP.Author = "GreenBlack"

   SWEP.ViewModelFOV  = 72
   SWEP.ViewModelFlip = true

    SWEP.Icon = "vgui/ttt_fun_killicons/awp.png"

end


SWEP.Base				= "gb_camo_base"
SWEP.HoldType			= "ar2"

SWEP.Primary.Delay       = 1
SWEP.Primary.Recoil      = 4.0
SWEP.Primary.Automatic   = false
SWEP.Primary.Damage      = 85
SWEP.Primary.Cone        = 0.0005
SWEP.Primary.Ammo        = "XBowBolt"
SWEP.Primary.ClipSize    = 50
SWEP.Primary.ClipMax     = 50
SWEP.Primary.DefaultClip = 50
SWEP.Primary.Sound       = Sound( "weapons/awp/awp1.wav" )
SWEP.HeadshotMultiplier = 2
SWEP.Camo = 30
SWEP.CustomCamo = 1
SWEP.Tracer = 1

SWEP.Secondary.Sound = Sound("Default.Zoom")
SWEP.ViewModelFlip =false
SWEP.ViewModel  = "models/weapons/cstrike/c_snip_awp.mdl"
SWEP.WorldModel = "models/weapons/w_snip_awp.mdl"

SWEP.IronSightsPos      = Vector( 5, -15, -2 )
SWEP.IronSightsAng      = Vector( 2.6, 1.37, 3.5 )


SWEP.Kind = 1
SWEP.AutoSpawnable = false
SWEP.AmmoEnt = "item_ammo_357_ttt"
SWEP.InLoadoutFor = nil
SWEP.LimitedStock = false
SWEP.AllowDrop = true
SWEP.IsSilent = false
SWEP.NoSights = false
SWEP.Sli = 0

SWEP.Nextreload = 0

function SWEP:Reload()
    if self.Nextreload > CurTime() then return end
    self.Nextreload = CurTime() + 2
        self.Weapon:DefaultReload( ACT_VM_RELOAD );
    self:SetIronsights( false )
    self:SetZoom(false)
   if IsValid( self.Owner) then
        if self.Owner:KeyDown(IN_USE) and self.Sli == 1 then
        self.Sli = 0
        else
        self.Sli = 1
        end
        self.Owner:EmitSound( Sound("Default.Zoom") )
       self:Defib()
   end 
end



SWEP.defibs = 0
SWEP.nextDefib = 0
function SWEP:Defib()

    if ( self.defibs > 0) then return end


    self.NextAttacky = CurTime()+ 0.5


    local tr = util.TraceLine({ start = self.Owner:EyePos(), endpos = self.Owner:EyePos() + self.Owner:EyeAngles():Forward() * 80, filter = self.Owner })
    if tr.HitNonWorld and IsValid(tr.Entity) and tr.Entity:GetClass() == "prop_ragdoll" and tr.Entity.player_ragdoll then
        self.Weapon:EmitSound( "weapons/c4/c4_beep1.wav"	)
        local mins, maxs = self.Owner:OBBMins(), self.Owner:OBBMaxs()
        local tr2 = util.TraceHull({start = tr.Entity:LocalToWorld(tr.Entity:OBBCenter()) + vector_up, endpos = tr.Entity:LocalToWorld(tr.Entity:OBBCenter()) + Vector(0, 0, 80), filter = {tr.Entity, self.Owner}, mins = Vector(mins.x * 1.6, mins.y * 1.6, 0), maxs = Vector(maxs.x * 1.6, maxs.y * 1.6, 0), mask = MASK_PLAYERSOLID})
        if tr2.Hit then
            self.Owner:PrintMessage( HUD_PRINTCENTER, "There is no room to bring him back here" )
            return
        end


        self.nextDefib = CurTime() + 10
        self.defibs =  self.defibs + 1
        if SERVER then
            rag = tr.Entity
            local ply = player.GetByUniqueID(rag.uqid)
            local credits = CORPSE.GetCredits(rag, 0)
            if not IsValid(ply) then return end
            if SERVER and _globals['slayed'][ply:SteamID()] then self.Owner:PrintMessage( HUD_PRINTCENTER, "Can't revive, person was slayed" )
                ply:Kill();
                return
            end
            if SERVER and ((rag.was_role == ROLE_TRAITOR and self.Owner:GetRole() ~= ROLE_TRAITOR) or (rag.was_role ~= ROLE_TRAITOR and self.Owner:GetRole() == ROLE_TRAITOR))  then
                DamageLog("Defib: " .. self.Owner:Nick() .. " [" .. self.Owner:GetRoleString() .. "] Died defibbing "..ply:Nick().." [" .. ply:GetRoleString() .. "] ")
                self.Owner:PrintMessage( HUD_PRINTCENTER, ply:Nick().." was of the other team,  sucked up your soul and killed you" )
                self.Owner:Kill();

                return
            end
            DamageLog("Defib: " .. self.Owner:Nick() .. " [" .. self.Owner:GetRoleString() .. "] defibbed "..ply:Nick().." [" .. ply:GetRoleString() .. "] ")

            ply:SpawnForRound(true)
            ply:SetCredits(credits)
            ply:SetPos(rag:LocalToWorld(rag:OBBCenter()) + vector_up / 2) --Just in case he gets stuck somewhere
            ply:SetEyeAngles(Angle(0, rag:GetAngles().y, 0))
            ply:SetCollisionGroup(COLLISION_GROUP_WEAPON)
            timer.Simple(2, function() ply:SetCollisionGroup(COLLISION_GROUP_PLAYER) end)

            umsg.Start("_resurrectbody")
            umsg.Entity(ply)
            umsg.Bool(CORPSE.GetFound(rag, false))
            umsg.End()
            rag:Remove()
        end
    else
        self.Weapon:EmitSound("npc/scanner/scanner_nearmiss1.wav")
    end
end

function SWEP:PrimaryAttack(worldsnd)

   self.Weapon:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
   self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )

   if not self:CanPrimaryAttack() then return end

   if self.Sli == 1 then
      self.Weapon:EmitSound( Sound( "weapons/usp/usp1.wav" ), self.Primary.SoundLevel )
   else
      self.Weapon:EmitSound( self.Primary.Sound, self.Primary.SoundLevel )
   end

   self:ShootBullet( self.Primary.Damage, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )

   self:TakePrimaryAmmo( 1 )

   local owner = self.Owner
   if not IsValid(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

   owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )
end

function SWEP:DryFire(setnext)
   if CLIENT and LocalPlayer() == self.Owner then
      self:EmitSound( "Weapon_Pistol.Empty" )
   end

   setnext(self, CurTime() +0.05)
end

function SWEP:SetZoom(state)
    if CLIENT then 
       return
    else
       if state then
          self.Owner:SetFOV(20, 0.3)
       else
          self.Owner:SetFOV(0, 0.2)
       end
      end
end

function SWEP:CanPrimaryAttack()
   if not IsValid(self.Owner) then return end

   --If the weapon is out of ammunition, or not scoped in we can't fire.
   if self.Weapon:Clip1() <= 0  then
      self:DryFire(self.SetNextPrimaryFire)
	  self:SetNextSecondaryFire(CurTime() +0.15)
      return false
   end 
   return true
end

-- Add some zoom to ironsights for this gun
function SWEP:SecondaryAttack()
    if not self.IronSightsPos then return end
    if self.Weapon:GetNextSecondaryFire() > CurTime() then return end
    
    bIronsights = not self:GetIronsights()
    
    self:SetIronsights( bIronsights )
    
    if SERVER then
        self:SetZoom(bIronsights)
     else
        self:EmitSound(self.Secondary.Sound)
    end
    
    self.Weapon:SetNextSecondaryFire( CurTime() + 0.3)
	self.Weapon:SetNextPrimaryFire( CurTime() + 0.75) --Set a long delay to prevent people from quickly scoping in and shooting.
end

function SWEP:PreDrop()
    self:SetZoom(false)
    self:SetIronsights(false)
    return self.BaseClass.PreDrop(self)
end

function SWEP:Holster()
    self:SetIronsights(false)
    self:SetZoom(false)
    return true
end

if CLIENT then
   local scope = surface.GetTextureID("sprites/scope")
   function SWEP:DrawHUD()
      if self:GetIronsights() then
         surface.SetDrawColor( 0, 0, 0, 255 )
         
         local x = ScrW() / 2.0
         local y = ScrH() / 2.0
         local scope_size = ScrH()

         -- crosshair
         local gap = 80
         local length = scope_size
         surface.DrawLine( x - length, y, x - gap, y )
         surface.DrawLine( x + length, y, x + gap, y )
         surface.DrawLine( x, y - length, x, y - gap )
         surface.DrawLine( x, y + length, x, y + gap )

         gap = 0
         length = 50
         surface.DrawLine( x - length, y, x - gap, y )
         surface.DrawLine( x + length, y, x + gap, y )
         surface.DrawLine( x, y - length, x, y - gap )
         surface.DrawLine( x, y + length, x, y + gap )


         -- cover edges
         local sh = scope_size / 2
         local w = (x - sh) + 2
         surface.DrawRect(0, 0, w, scope_size)
         surface.DrawRect(x + sh - 2, 0, w, scope_size)

         surface.SetDrawColor(255, 0, 0, 255)
         surface.DrawLine(x, y, x + 1, y + 1)

         -- scope
         surface.SetTexture(scope)
         surface.SetDrawColor(255, 255, 255, 255)

         surface.DrawTexturedRectRotated(x, y, scope_size, scope_size, 0)

      else
         return self.BaseClass.DrawHUD(self)
      end
   end

   function SWEP:AdjustMouseSensitivity()
      return (self:GetIronsights() and 0.2) or nil
   end
end
