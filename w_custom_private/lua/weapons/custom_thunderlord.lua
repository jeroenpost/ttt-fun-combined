SWEP.Base = "gb_camo_base"
SWEP.ViewModelFOV	= 54
SWEP.HoldType = "smg"
SWEP.PrintName = "Thunderlord"
SWEP.ViewModel		= "models/weapons/cstrike/c_mach_m249para.mdl"
SWEP.WorldModel		= "models/weapons/w_mach_m249para.mdl"
SWEP.AutoSpawnable = false
SWEP.Camo = 43
SWEP.Primary.Damage = 10
SWEP.Primary.Delay = 0.06
SWEP.Primary.Cone = 0.11
SWEP.Primary.Automatic		= true
SWEP.Primary.Ammo        = "smg1"
SWEP.AutoSpawnable      = false
SWEP.Primary.Sound			= Sound("Weapon_m249.Single")
SWEP.AmmoEnt = "item_ammo_smg1_ttt"

SWEP.Primary.ClipSize = 250
SWEP.Primary.ClipMax = 500
SWEP.Primary.DefaultClip	= 500
SWEP.Primary.Recoil			= 0.5

SWEP.HoldType = "crossbow"
SWEP.Slot = 8
SWEP.Kind = 9

SWEP.HeadshotMultiplier = 2.2

SWEP.IronSightsPos = Vector(-5.96, -5.119, 2.349)
SWEP.IronSightsAng = Vector(0, 0, 0)

SWEP.tracerColor = Color(0,0,255)
function SWEP:PrimaryAttack()
    if self:Clip1() > 0 then
        self:ShootTracer("manatrace")

        tr = self.Owner:GetEyeTrace()
        if  IsValid( tr.Entity ) then
            local edata = EffectData()

            edata:SetEntity(tr.Entity)
            edata:SetMagnitude(10)
            edata:SetScale(25)
            edata:SetEntity(tr.Entity)
            edata:SetMagnitude(10)
            edata:SetScale(25)
            timer.Create(tr.Entity:EntIndex().."thunderlordpoi",1,3,function()
                if IsValid(tr.Entity) and IsValid(self.Owner) and SERVER then
                tr.Entity:TakeDamage(1, self.Owner)
                local edata = EffectData()

                edata:SetEntity(tr.Entity)
                edata:SetMagnitude(10)
                edata:SetScale(25)
                edata:SetEntity(tr.Entity)
                edata:SetMagnitude(10)
                edata:SetScale(25)
                util.Effect("TeslaHitBoxes", edata)
                end
            end)

            util.Effect("TeslaHitBoxes", edata)
           -- util.Effect("ManhackSparks", edata)
        end

    end
    self.BaseClass.PrimaryAttack(self)
end

SWEP.NextZedPress = 0
function SWEP:Reload()
    if self.Owner:KeyDown(IN_USE) then self.BaseClass.Reload(self) return end
    if CLIENT then return end
    if self.NextZedPress > CurTime() then
        self.Owner:PrintMessage( HUD_PRINTCENTER,  math.floor(self.NextZedPress - CurTime()).." seconds left" )
        return
    end
    self.NextZedPress = CurTime() + 30


    local tr = self.Owner:GetEyeTrace();
    local ent = ents.Create( "cosmic_power_blue" )

    ent:SetPos( tr.HitPos  )
    ent:SetOwner( self.Owner  )
    ent:SetPhysicsAttacker(  self.Owner )
    ent.Owner = self.Owner
    ent:Spawn()
    ent.timer = CurTime() + 0.01
    ent.EndColor = "0 0 0"
    ent.StartColor = "0 0 255"
    timer.Simple(7,function()
        if IsValid(ent) then
            ent.Bastardgas:Remove()
            ent.Entity:Remove()
        end
    end)

end

function SWEP:SecondaryAttack()
    self:Fly()
end