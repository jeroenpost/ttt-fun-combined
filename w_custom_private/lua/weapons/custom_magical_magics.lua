if SERVER then

    CreateConVar("grapple_distance", 10000, false)
	--resource.AddFile("materials/vgui/ttt_fun_killicons/spidergun.png")
end


SWEP.VElements = {
    ["shell++"] = { type = "Model", model = "models/weapons/shotgun_shell.mdl", bone = "ValveBiped.Gun", rel = "back", pos = Vector(0, -2.1, 0.994), angle = Angle(0, 0, 0), size = Vector(0.474, 0.474, 0.474), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
    ["sight"] = { type = "Model", model = "models/props_vehicles/tire001b_truck.mdl", bone = "ValveBiped.Gun", rel = "attach", pos = Vector(1.399, 0, -0.7), angle = Angle(0, 0, 0), size = Vector(0.045, 0.045, 0.045), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
    ["top"] = { type = "Model", model = "models/Items/BoxSRounds.mdl", bone = "ValveBiped.Gun", rel = "", pos = Vector(0, -0.301, 0), angle = Angle(0, 0, -90), size = Vector(0.37, 1.1, 0.103), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
    ["RAIL"] = { type = "Model", model = "models/props_wasteland/prison_heater001a.mdl", bone = "ValveBiped.Gun", rel = "top", pos = Vector(0, 0, -2.201), angle = Angle(90, 180, 0), size = Vector(0.412, 0.6, 0.041), color = Color(255, 255, 255, 255), surpresslightning = false, material =  "camos/custom_camo17", skin = 0, bodygroup = {} },
    ["flashlight"] = { type = "Model", model = "models/props_wasteland/light_spotlight01_lamp.mdl", bone = "ValveBiped.Gun", rel = "barrelunder", pos = Vector(0, 0, -2), angle = Angle(-90, 0, 0), size = Vector(0.4, 0.103, 0.103), color = Color(255, 255, 255, 255), surpresslightning = false, material =  "camos/custom_camo17", skin = 0, bodygroup = {} },
    ["Body"] = { type = "Model", model = "models/props_combine/CombineTrain01a.mdl", bone = "ValveBiped.Pump", rel = "", pos = Vector(0, -1, 1), angle = Angle(90, 0, 90), size = Vector(0.024, 0.026, 0.01), color = Color(255, 255, 255, 255), surpresslightning = false, material =  "camos/custom_camo17", skin = 0, bodygroup = {} },
    ["barrel"] = { type = "Model", model = "models/props_lab/kennel_physics.mdl", bone = "ValveBiped.Gun", rel = "top", pos = Vector(0, 14.906, -2.981), angle = Angle(0, 90, 0), size = Vector(0.25, 0.1, 0.1), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
    ["top2"] = { type = "Model", model = "models/Items/car_battery01.mdl", bone = "ValveBiped.Bip01", rel = "top", pos = Vector(0, -6, 0), angle = Angle(0, 180, 0), size = Vector(0.226, 0.412, 0.289), color = Color(255, 255, 255, 255), surpresslightning = false, material =  "camos/custom_camo17", skin = 0, bodygroup = {} },
    ["shell+"] = { type = "Model", model = "models/weapons/shotgun_shell.mdl", bone = "ValveBiped.Gun", rel = "back", pos = Vector(0, -2.1, 2.98), angle = Angle(0, 180, 0), size = Vector(0.474, 0.474, 0.474), color = Color(255, 255, 255, 255), surpresslightning = false, material =  "camos/custom_camo17", skin = 0, bodygroup = {} },
    ["back"] = { type = "Model", model = "models/props_lab/reciever_cart.mdl", bone = "ValveBiped.Bip01", rel = "top", pos = Vector(0, -16.894, -0.5), angle = Angle(0, 0, 90), size = Vector(0.09, 0.09, 0.226), color = Color(255, 255, 255, 255), surpresslightning = false, material = "camos/custom_camo17", skin = 0, bodygroup = {} },
    ["barrelunder"] = { type = "Model", model = "models/Items/battery.mdl", bone = "ValveBiped.Gun", rel = "barrel", pos = Vector(-1, 0, -0.7), angle = Angle(-90, 0, 0), size = Vector(0.67, 0.67, 0.67), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
    ["sight2"] = { type = "Model", model = "models/props_trainstation/mount_connection001a.mdl", bone = "ValveBiped.Gun", rel = "attach", pos = Vector(0, 0, 0), angle = Angle(0, 0, -180), size = Vector(0.029, 0.029, 0.029), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
    ["attach"] = { type = "Model", model = "models/props_wasteland/laundry_cart001.mdl", bone = "ValveBiped.Gun", rel = "top2", pos = Vector(0, 0, 1.899), angle = Angle(0, 90, 180), size = Vector(0.05, 0.05, 0.019), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
    ["shell"] = { type = "Model", model = "models/weapons/shotgun_shell.mdl", bone = "ValveBiped.Gun", rel = "back", pos = Vector(0, -2.1, 4.968), angle = Angle(0, 0, 0), size = Vector(0.474, 0.474, 0.474), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
    ["front sight"] = { type = "Model", model = "models/Items/combine_rifle_cartridge01.mdl", bone = "ValveBiped.Gun", rel = "barrel", pos = Vector(-2.981, 0, 3.974), angle = Angle(4.472, 180, 0), size = Vector(0.3, 0.3, 0.3), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
}
SWEP.WElements = {
    ["shell++"] = { type = "Model", model = "models/weapons/shotgun_shell.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "back", pos = Vector(0, -2.1, 0.994), angle = Angle(0, 0, 0), size = Vector(0.474, 0.474, 0.474), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
    ["shell"] = { type = "Model", model = "models/weapons/shotgun_shell.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "back", pos = Vector(0, -2.1, 4.968), angle = Angle(0, 0, 0), size = Vector(0.474, 0.474, 0.474), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
    ["flashlight"] = { type = "Model", model = "models/props_wasteland/light_spotlight01_lamp.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "barrelunder", pos = Vector(0, 0, 3.048), angle = Angle(-90, 0, 0), size = Vector(0.4, 0.103, 0.103), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
    ["top"] = { type = "Model", model = "models/Items/BoxSRounds.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(16.761, 0.99, -6), angle = Angle(4.472, -89.442, 174.41), size = Vector(0.37, 0.723, 0.103), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
    ["stock"] = { type = "Model", model = "models/props_c17/computer01_keyboard.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "back", pos = Vector(0, 2.4, -6.2), angle = Angle(-15, 0, -180), size = Vector(0.3, 0.3, 0.3), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
    ["sight"] = { type = "Model", model = "models/props_vehicles/tire001b_truck.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "attach", pos = Vector(1.399, 0, -0.7), angle = Angle(0, 0, 0), size = Vector(0.045, 0.045, 0.045), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
    ["Body"] = { type = "Model", model = "models/props_combine/CombineTrain01a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "top", pos = Vector(0, -1.525, 0.899), angle = Angle(0, 90, 180), size = Vector(0.024, 0.026, 0.014), color = Color(255, 255, 255, 255), surpresslightning = false, material = "camos/custom_camo17", skin = 0, bodygroup = {} },
    ["back"] = { type = "Model", model = "models/props_lab/reciever_cart.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "top", pos = Vector(0, -13.601, -0.5), angle = Angle(0, 0, 90), size = Vector(0.09, 0.09, 0.2), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
    ["top2"] = { type = "Model", model = "models/Items/car_battery01.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "top", pos = Vector(0, -4.572, 0), angle = Angle(0, 180, 0), size = Vector(0.3, 0.342, 0.247), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
    ["shell+"] = { type = "Model", model = "models/weapons/shotgun_shell.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "back", pos = Vector(0, -2.1, 2.98), angle = Angle(0, 180, 0), size = Vector(0.474, 0.474, 0.474), color = Color(255, 255, 255, 255), surpresslightning = false, material =  "camos/custom_camo17", skin = 0, bodygroup = {} },
    ["barrel"] = { type = "Model", model = "models/props_lab/kennel_physics.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "top", pos = Vector(0, 9.5, -4.5), angle = Angle(0, 90, 0), size = Vector(0.15, 0.1, 0.14), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
    ["barrelunder"] = { type = "Model", model = "models/Items/battery.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "barrel", pos = Vector(-3.5, 0, -0.7), angle = Angle(-90, 0, 0), size = Vector(0.67, 0.67, 0.67), color = Color(255, 255, 255, 255), surpresslightning = false, material =  "camos/custom_camo17", skin = 0, bodygroup = {} },
    ["sight2"] = { type = "Model", model = "models/props_trainstation/mount_connection001a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "attach", pos = Vector(0, 0, 0), angle = Angle(0, 0, -180), size = Vector(0.029, 0.029, 0.029), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
    ["attach"] = { type = "Model", model = "models/props_wasteland/laundry_cart001.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "top2", pos = Vector(0, 0, 1.799), angle = Angle(0, 90, 180), size = Vector(0.05, 0.05, 0.019), color = Color(255, 255, 255, 255), surpresslightning = false, material =  "camos/custom_camo17", skin = 0, bodygroup = {} },
    ["RAIL"] = { type = "Model", model = "models/props_wasteland/prison_heater001a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "top", pos = Vector(0, 0.5, -2.201), angle = Angle(90, 180, 0), size = Vector(0.412, 0.439, 0.045), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
    ["front sight"] = { type = "Model", model = "models/Items/combine_rifle_cartridge01.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "barrel", pos = Vector(-2.981, 0, 5), angle = Angle(4.472, 180, 0), size = Vector(0.3, 0.3, 0.3), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
}


function SWEP:Deploy()
    self.BaseClass.Deploy(self)

        gb_StopSoundFromServer("magical_magic")
    if self.Owner.magicalsoundeneabled then
        gb_PlaySoundFromServer(gb_config.websounds.."believeinmagic_0.mp3", owner,true,"magical_magic");
            local owner = self.Owner
            timer.Create(owner:SteamID().."magical",28.1,10,function()
                if IsValid(owner) and owner:Alive() and owner:GetActiveWeapon() and  owner:GetActiveWeapon() == self and not self.stopped then
                    gb_PlaySoundFromServer(gb_config.websounds.."believeinmagic_0.mp3", owner,true,"magical_magic");
                end
            end)
            hook.Add("PlayerDeath","magical_playerdeath",function( victim, inflictor, attacker )
                if victim == owner then
                    gb_StopSoundFromServer("magical_magic")
                end
            end)
     end



end

SWEP.Spawnable			= true
SWEP.AdminSpawnable		= false
SWEP.CanDecapitate= true
SWEP.HasFireBullet = true

SWEP.PrintName			= "Magical Magics"
	SWEP.Slot				= 2
	SWEP.SlotPos = 9
	SWEP.Icon = "vgui/ttt_fun_killicons/spidergun.png"  
	
SWEP.Primary.Delay		= 0
SWEP.Primary.ClipSize		= 10
SWEP.Primary.DefaultClip	= 10
SWEP.Primary.Automatic		= true
SWEP.Primary.Ammo		= "Battery"
SWEP.UseHands = true

SWEP.DrawAmmo			= true
SWEP.DrawCrosshair		= true
SWEP.ViewModelFOV = 71
SWEP.ViewModelFlip = false
SWEP.ViewModelFOV = 70
SWEP.ViewModelFlip = false
SWEP.ViewModel = "models/weapons/v_shotgun.mdl"
SWEP.WorldModel = "models/weapons/w_shotgun.mdl"
SWEP.ShowViewModel = true
SWEP.ShowWorldModel = true
SWEP.ViewModelBoneMods = {
    ["ValveBiped.Pump"] = { scale = Vector(0.009, 0.009, 0.009), pos = Vector(0, 0, 0), angle = Angle(0, 0, 0) },
    ["ValveBiped.Bip01"] = { scale = Vector(1, 1, 1), pos = Vector(-5.37, 1.419, 0.709), angle = Angle(0, 0, 0) }
}
   SWEP.EquipMenuData = {
      type  = "item_weapon",
      name  = "Spiderman Gun",
      desc  = "Fly around, Just make sure not to be seen."
   };
  
SWEP.Kind = WEAPON_HEAVY
SWEP.Base   = "aa_base"
SWEP.Camo = 17
SWEP.CustomCamo = true


function SWEP:GetIronsights( )

	return false

end

if CLIENT then


    function SWEP:CustomAmmoDisplay()

        self.AmmoDisplay = self.AmmoDisplay or {}
                --attempt to remove ammo display
        self.AmmoDisplay.Draw = false

        self.AmmoDisplay.PrimaryClip 	= 1
        self.AmmoDisplay.PrimaryAmmo 	= -1
        self.AmmoDisplay.SecondaryAmmo 	= -1

        return self.AmmoDisplay

    end

    function SWEP:SetHoldType( t )
        -- Just a fake function so we can define
        -- weapon holds in shared files without errors
    end
end



local sndPowerUp		= Sound("rope_hit.mp3")
local sndPowerDown		= Sound ("shoot_rope.mp3")
local sndTooFar			= Sound ("to_far.mp3")

function SWEP:Initialize()

	nextshottime = CurTime()

	self.BaseClass.Initialize(self)
end
SWEP.HoldType = "smg"

SWEP.NextAmmoGive = 0
SWEP.NextAmmoTake = 0
SWEP.NextDisable = 0
SWEP.NextFire2 = 0
function SWEP:Think()

    if self.NextAmmoGive < CurTime() && self:Clip1() < 10 then
    self.NextAmmoGive = CurTime() + 3
    self:SetClip1( self:Clip1() + 1 )
    end

	if (!self.Owner || self.Owner == NULL) or self:Clip1() < 1 then

        if ( !self.Beam ) then return end
        if( IsValid( self.Beam) ) then
            self.Beam:Remove()
        end
        self.Beam = nil
        return
    end



    if self.Owner:KeyDown(IN_USE) and self.Owner:KeyDown(IN_ATTACK2) then

        return
    end

    if self.Owner:KeyDown(IN_USE) and  self.Owner:KeyDown(IN_DUCK) and self.NextDisable < CurTime() then
        self.NextDisable = CurTime() + 0.3
        if self.Owner.magicalsoundeneabled then
            self.Owner.magicalsoundeneabled = false
            self.Owner:PrintMessage( HUD_PRINTCENTER, "Disabled Sound on deploy" )
        else
            self.Owner.magicalsoundeneabled = true
            self.Owner:PrintMessage( HUD_PRINTCENTER, "Enabled Sound on deploy" )
        end
        return
    end



    if ( self.Owner:KeyPressed( IN_ATTACK2 ) ) then
	
		self:StartAttack()
		
	elseif ( self.Owner:KeyDown( IN_ATTACK2 )  ) then
	
		self:UpdateAttack()
        self.Weapon:SetNextSecondaryFire( CurTime() + 1 )
		
	elseif ( self.Owner:KeyReleased( IN_ATTACK2 )  ) then
	
		self:EndAttack( true )
	
    end


    self.BaseClass.Think(self)

end

function SWEP:DoTrace( endpos )
	local trace = {}
		trace.start = self.Owner:GetShootPos()
		trace.endpos = trace.start + (self.Owner:GetAimVector() * 14096) --14096 is length modifier.
		if(endpos) then trace.endpos = (endpos - self.Tr.HitNormal * 7) end
		trace.filter = { self.Owner, self.Weapon }
		
	self.Tr = nil
	self.Tr = util.TraceLine( trace )
end

SWEP.NextReload = 0
function SWEP:Reload()
    if self.NextReload > CurTime() then return end
    self.NextReload = CurTime() + 0.2
    if self.Owner:KeyDown(IN_USE) then
        self:Jihad("bathroom.mp3")
    end
    if self.Owner:KeyDown(IN_ATTACK) then
        gb_StopSoundFromServer("magical_magic")
        self.stopped = true
    end
    if self.Owner:KeyDown(IN_ATTACK2) then
        self.stopped = false
        gb_StopSoundFromServer("magical_magic")
        gb_PlaySoundFromServer(gb_config.websounds.."believeinmagic_0.mp3", owner,true,"magical_magic");
    end

end


function SWEP:StartAttack()

       
        if not self:CanPrimaryAttack() then return end

	--Get begining and end poins of trace.
	local gunPos = self.Owner:GetShootPos() --Start of distance trace.
	local disTrace = self.Owner:GetEyeTrace() --Store all results of a trace in disTrace.
	local hitPos = disTrace.HitPos --Stores Hit Position of disTrace.
	
	--Calculate Distance
	--Thanks to rgovostes for this code.
	local x = (gunPos.x - hitPos.x)^2;
	local y = (gunPos.y - hitPos.y)^2;
	local z = (gunPos.z - hitPos.z)^2;
	local distance = math.sqrt(x + y + z);
	
	--Only latches if distance is less than distance CVAR
	local distanceCvar = GetConVarNumber("grapple_distance")
	inRange = false
	if distance <= distanceCvar then
		inRange = true
	end
	
	if inRange then
		if (SERVER) then
			
			if (!self.Beam) then --If the beam does not exist, draw the beam.
                            if not self:CanPrimaryAttack() then return end
				--grapple_beam
				self.Beam = ents.Create( "trace1" )
					self.Beam:SetPos( self.Owner:GetShootPos() )
				self.Beam:Spawn()
            self:TakePrimaryAmmo(1)

			end
			
			self.Beam:SetParent( self.Owner )
			self.Beam:SetOwner( self.Owner )
		
		end
		
                
		self:DoTrace()
		self.speed = 10000 --Rope latch speed. Was 3000.
		self.startTime = CurTime()
		self.endTime = CurTime() + self.speed
		self.dtfix = -1
		
		if (SERVER && self.Beam) then
			self.Beam:GetTable():SetEndPos( self.Tr.HitPos )
		end
		
		self:UpdateAttack()
		
		self.Weapon:EmitSound( sndPowerDown )
	else
		--Play A Sound
		self.Weapon:EmitSound( sndTooFar )
	end
end

function SWEP:UpdateAttack()

       -- if not self:CanPrimaryAttack() then return end        
    if !self.Tr or self:Clip1() < 1 then return end
        self.Owner:LagCompensation( true )


	if (!endpos) then endpos = self.Tr.HitPos end
	
	if (SERVER && self.Beam) then
		self.Beam:GetTable():SetEndPos( endpos )
	end

	lastpos = endpos

	
	
			if ( self.Tr.Entity:IsValid() ) then
			
					endpos = self.Tr.Entity:GetPos()
					if ( SERVER and IsValid(self.Beam) ) then
					self.Beam:GetTable():SetEndPos( endpos )
					end
			
			end
			
			local vVel = (endpos - self.Owner:GetPos())
			local Distance = endpos:Distance(self.Owner:GetPos())
			
			local et = (self.startTime + (Distance/self.speed))
			if(self.dtfix != 0) then
				self.dtfix = (et - CurTime()) / (et - self.startTime)
			end
			if(self.dtfix < 0) then
				self.Weapon:EmitSound( sndPowerUp )
				self.dtfix = 0
			end
			
			if(self.dtfix == 0) then
			zVel = self.Owner:GetVelocity().z
			vVel = vVel:GetNormalized()*(math.Clamp(Distance,0,25))
				if( SERVER ) then
                    if  self.NextAmmoTake < CurTime() then
                        self:TakePrimaryAmmo(1)
                        self.NextAmmoTake = CurTime() + 1
                        self.NextAmmoGive = CurTime() + 1.2
                    end

				local gravity = GetConVarNumber("sv_gravity")

                                    vVel:Add(Vector(0,0,(gravity/50)*1.5))

				if(zVel < 0) then
					vVel:Sub(Vector(0,0,zVel/100))
				end
				self.Owner:SetVelocity(vVel)
				end
			end
	
	endpos = nil
	
	self.Owner:LagCompensation( false )
	
end

function SWEP:EndAttack( shutdownsound )
	
	if ( shutdownsound ) then
		self.Weapon:EmitSound( sndPowerDown )
	end

	if ( CLIENT ) then return end
	if ( !self.Beam ) then return end
	if( IsValid( self.Beam) ) then
	  self.Beam:Remove()
	end
	self.Beam = nil
	
end


function SWEP:Holster()
	self:EndAttack( false )
    gb_StopSoundFromServer("magical_magic")
	return true
end

function SWEP:OnRemove()
	self:EndAttack( false )
    gb_StopSoundFromServer("magical_magic")
	return true
end

SWEP.nextsecondary = 0
function SWEP:SecondaryAttack()
    if self.Owner:KeyDown(IN_RELOAD) then return end
    if self.Owner:KeyDown(IN_USE) then
        if self.nextsecondary > CurTime() then return end
        self.nextsecondary = CurTime() + 0.4
        if SERVER then
            local ply = self.Owner
            if not IsValid(ply) then return end

            ply:SetAnimation( PLAYER_ATTACK1 )

            local ang = ply:EyeAngles()

            if ang.p < 90 then
                ang.p = -10 + ang.p * ((90 + 10) / 90)
            else
                ang.p = 360 - ang.p
                ang.p = -10 + ang.p * -((90 + 10) / 90)
            end

            local vel = math.Clamp((90 - ang.p) * 5.5, 550, 800)

            local vfw = ang:Forward()
            local vrt = ang:Right()

            local src = ply:GetPos() + (ply:Crouching() and ply:GetViewOffsetDucked() or ply:GetViewOffset())

            src = src + (vfw * 1) + (vrt * 3)

            local thr = vfw * vel + ply:GetVelocity()

            local knife_ang = Angle(-28,0,0) + ang
            knife_ang:RotateAroundAxis(knife_ang:Right(), -90)

            local knife = ents.Create("random_knifething_magic")
            if not IsValid(knife) then return end
            knife:SetPos(src)
            knife:SetAngles(knife_ang)

            knife:Spawn()
            knife.owner = self.Owner

            knife.Damage = self.Primary.Damage

            knife:SetOwner(ply)

            local phys = knife:GetPhysicsObject()
            if IsValid(phys) then
                phys:SetVelocity(thr)
                phys:AddAngleVelocity(Vector(0, 3500, 0))
                phys:Wake()
            end

        end
        return
    end


end

function SWEP:PrimaryAttack()
    if self.Owner:KeyDown(IN_RELOAD) then return end
    if self.Owner:KeyDown(IN_USE) then
    if self.NextFire2 > CurTime() then return end
    self.NextFire2 = CurTime() + 45
    if SERVER then
        local ball = ents.Create("random_healthing_magic");
        self:EmitSound("Friends/friend_join.wav")
        if (ball) then
            ball:SetPos(self.Owner:GetPos() + Vector(0,0,48));
            ball.owner = self.Owner
            ball:Spawn();
            local physicsObject = ball:GetPhysicsObject();
            physicsObject:ApplyForceCenter(self.Owner:GetAimVector() * 1000);
            return ball;
        end;
    end
    return
   end
self.Weapon:SetNextPrimaryFire( CurTime() + 0.85 )
self:ShootBullet( 5, 20, 0.085 )
self:MakeAFlame()
end

function SWEP:OnDrop()
    gb_StopSoundFromServer("magical_magic")
    self:Remove()
end

function SWEP:ShootBullet( damage, num_bullets, aimcone )

local bullet = {}
bullet.Num 		= num_bullets
bullet.Src 		= self.Owner:GetShootPos()	-- Source
bullet.Dir 		= self.Owner:GetAimVector()	-- Dir of bullet
bullet.Spread 	= Vector( aimcone, aimcone, 0 )		-- Aim Cone
bullet.Tracer	= 5	-- Show a tracer on every x bullets
bullet.TracerName = "Tracer" -- what Tracer Effect should be used
bullet.Force	= 1	-- Amount of force to give to phys objects
bullet.Damage	= damage
bullet.AmmoType = "Pistol"

self.Owner:FireBullets( bullet )
self.Weapon:EmitSound( Sound("CSTM_SilencedShot2") )

self:ShootEffects()

end

function SWEP:GetHeadshotMultiplier(victim, dmginfo)
local att = dmginfo:GetAttacker()
if not IsValid(att) then return 3 end

local dist = victim:GetPos():Distance(att:GetPos())
local d = math.max(0, dist - 140)

return 1 + math.max(0, (2.1 - 0.002 * (d ^ 1.25)))
end


local ENT = {}

ENT.Type = "anim"
ENT.Base = "base_anim"


if SERVER then

    function ENT:Initialize()
        local num = math.random(1,10)
        if num < 4 then
            self.heal = 10
            self.model = "models/props/cs_italy/bananna.mdl"
        end
        if num > 3 and num < 6 then
            self.heal = 10
            self.model = "models/props/cs_italy/orange.mdl"
        end
        if num > 5 and num < 9 then
            self.heal = 25
            self.model = "models/teh_maestro/popcorn.mdl"
        end
        if num > 8 then
            self.heal = 45
            self.model = "models/props_junk/watermelon01.mdl"
        end

        self:SetModel(self.model)
        self:SetModelScale(self:GetModelScale()*1,0)
        self:SetColor(Color(128,128,128,255))
        self.Entity:PhysicsInit(SOLID_VPHYSICS);
        self.Entity:SetMoveType(MOVETYPE_VPHYSICS);
        self.Entity:SetSolid(SOLID_VPHYSICS);
        self.active = CurTime() + 2

        self.Entity:SetCollisionGroup(COLLISION_GROUP_WEAPON)
        local phys = self:GetPhysicsObject()
        if phys:IsValid() then
            phys:Wake()
            phys:EnableGravity(true);
        end
    end

    function ENT:Use( activator, caller )
        if self.active > CurTime() then return end
        self.Entity:Remove()
        if ( activator:IsPlayer() ) then
            if activator:Health() < 150 then
                activator:SetHealth(activator:Health()+self.heal)
                if activator:Health() > 150 then
                    activator:SetHealth(150)
                end
            end
            activator:EmitSound("crisps/eat.mp3")
        end
    end


end

scripted_ents.Register( ENT, "random_healthing_magic", true )


local ENT = {}

ENT.Type = "anim"
ENT.Base = "base_anim"


if SERVER then

    function ENT:Initialize()
        local num = math.random(1,10)
        if num < 4 then
            self.heal = 10
            self.model = "models/props/cs_italy/bananna.mdl"
        end
        if num > 3 and num < 6 then
            self.heal = 10
            self.model = "models/props/cs_italy/orange.mdl"
        end
        if num > 5 and num < 9 then
            self.heal = 25
            self.model = "models/teh_maestro/popcorn.mdl"
        end
        if num > 8 then
            self.heal = 45
            self.model = "models/props_junk/watermelon01.mdl"
        end

        self:SetModel(self.model)
        self:SetModelScale(self:GetModelScale()*1,0)
        self:SetColor(Color(128,128,128,255))
        self.Entity:PhysicsInit(SOLID_VPHYSICS);
        self.Entity:SetMoveType(MOVETYPE_VPHYSICS);
        self.Entity:SetSolid(SOLID_VPHYSICS);
        self.active = CurTime() + 0.2

      --  self.Entity:SetCollisionGroup(COLLISION_GROUP_WEAPON)
        local phys = self:GetPhysicsObject()
        if phys:IsValid() then
            phys:Wake()
            phys:EnableGravity(true);
        end

        timer.Simple(3,function()
            if IsValid(self) then self:Remove() end
        end)
    end

    function ENT:Touch( ent )
        if ent:IsPlayer() and not ent.nextsandtouch or (ent.nextsandtouch and ent.nextsandtouch < CurTime()) then
            ent.nextsandtouch = CurTime() + 3
            ent:Ignite(0.5,0)
            local dmginfo = DamageInfo()
            dmginfo:SetDamage( 50 ) --50 damage
            dmginfo:SetDamageType( DMG_BULLET ) --Bullet damage
            dmginfo:SetAttacker( self.owner or ent ) --First player found gets credit
            dmginfo:SetDamageForce( Vector( 0, 0, 50 ) ) --Launch upwards
            dmginfo:SetInflictor(self)
            ent:TakeDamageInfo( dmginfo )
            self:Remove()
        end
    end

    function ENT:PhysicsCollide(data, phys)
        if self.Stuck then return false end

        local other = data.HitEntity
        if IsValid(other) and other:IsPlayer() then
            self:Touch(other)
            self:Remove()
        end
    end


end

scripted_ents.Register( ENT, "random_knifething_magic", true )

