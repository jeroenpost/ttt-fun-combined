if SERVER then AddCSLuaFile() end

SWEP.Base = "aa_base"
SWEP.Kind = WEAPON_NADE
SWEP.HoldType = "melee"
SWEP.ViewModelFOV = 20
SWEP.ViewModelFlip = false
SWEP.ViewModel = "models/weapons/v_crowbar.mdl"
SWEP.WorldModel = "models/food/burger.mdl"
SWEP.ShowViewModel = true
SWEP.ShowWorldModel = false
SWEP.ViewModelBoneMods = {}
SWEP.Slot = 3

SWEP.PrintName = "The Unforgiven Touch"
SWEP.Primary.Delay          = 1.5
SWEP.Primary.Recoil         = 1
SWEP.Primary.Automatic = true
SWEP.Primary.Ammo = nil
SWEP.Primary.Damage = 50
SWEP.Primary.Cone = 0.005
SWEP.Primary.ClipSize = 25
SWEP.Primary.ClipMax = 100-- keep mirrored to ammo
SWEP.Primary.DefaultClip = 25
SWEP.Icon = "vgui/ttt_fun_killicons/poseidons_fork.png"
SWEP.Secondary.Automatic = true

SWEP.WElements = {
    ["element_name"] = { type = "Model", model = "models/staff/staff.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(1, 0.155, 11.454), angle = Angle(190.932, -5.114, 5.113), size = Vector(0.5, 0.5, 0.5), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
}


SWEP.VElements = {
    ["element_nameasd"] = { type = "Model", model = "models/staff/staff.mdl", bone = "ValveBiped.HandControlRotR", rel = "", pos = Vector(29.545, 9.545, 9.545), angle = Angle(178.977, 0, 0), size = Vector(0.5, 0.5, 0.5), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
}

function SWEP:OnDrop()
    self:Remove()
end

function SWEP:Deploy()
  self:SetNWString("shootmode","Dark")
  self:SetNWString("shootmode2","Combustion")
end

SWEP.LaserColor = Color(30,30,30,255)
SWEP.NextReload = 0
function SWEP:PrimaryAttack()

    local mode = self:GetNWString("shootmode")
    self:SendWeaponAnim(self.PrimaryAnim)

    self.Owner:MuzzleFlash()
    self.Owner:SetAnimation( PLAYER_ATTACK1 )

    if self.Owner:KeyDown(IN_USE)  then
        if self.NextReload > CurTime() then return end
        self.NextReload = CurTime() + 0.3

        self:SetIronsights(false)

        if mode == "Dark" then  self:SetNWString("shootmode","Light")  end
        if mode == "Light" then  self:SetNWString("shootmode","Shock") end
        if mode == "Shock" then  self:SetNWString("shootmode","Fire") end
        if mode == "Fire" then  self:SetNWString("shootmode","Ice") end
        if mode == "Ice" then  self:SetNWString("shootmode","Restoration") end
        if mode == "Restoration" then  self:SetNWString("shootmode","Dark") end


        self.Weapon:SetNextPrimaryFire( CurTime() + 0.2 )
        return
    end

   -- self.Weapon:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )

    if mode == "Dark" then
        self:Blind()
        self.LaserColor = Color(30,30,30,255)
        self:ShootTracer("LaserTracer_thick")
        self.Owner:EmitSound( table.Random(  { "weapons/warden/warden1.mp3","weapons/warden/warden2.mp3","weapons/warden/warden3.mp3"} ),100,100)
        self:ShootBullet( 12, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )
        self.Weapon:SetNextPrimaryFire( CurTime() + 0.1 )
    end
    if mode == "Light" then
        self:Disorientate()
        self.LaserColor = Color(255,255,255,255)
        self:ShootTracer("LaserTracer_thick")
        local tr = self.Owner:GetEyeTrace();

        if IsValid( tr.Entity ) and tr.Entity:IsPlayer()  then
            tr.Entity:SetNWFloat("durgz_mushroom_high_start", CurTime())
            tr.Entity:SetNWFloat("durgz_mushroom_high_end", CurTime()+2)
        end
        self.Owner:EmitSound( table.Random(  { "weapons/warden/warden1.mp3","weapons/warden/warden2.mp3","weapons/warden/warden3.mp3"} ),100,100)
        self:ShootBullet( 11, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )
        self.Weapon:SetNextPrimaryFire( CurTime() + 0.09 )
    end
    if mode == "Shock" then
        self:Disorientate()
        self.LaserColor = Color(255,255,255,255)
        self:ShootTracer("LaserTracer_thick")
        local tr = self.Owner:GetEyeTrace();

        if IsValid( tr.Entity ) and tr.Entity:IsPlayer()  then
            if SERVER then
            tr.Entity:Ignite(1,5)
            end
            local ent =  tr.Entity
            local weapon = self.Owner:GetActiveWeapon()
            local edata = EffectData()

            edata:SetEntity(ent)
            edata:SetMagnitude(15)
            edata:SetScale(35)

            util.Effect("TeslaHitBoxes", edata)
            util.Effect("ManhackSparks", edata)
            ent:EmitSound( "weapons/gb_weapons/zeus_thunder.mp3", 500 )
        end
        self.Owner:EmitSound( table.Random(  { "weapons/warden/warden1.mp3","weapons/warden/warden2.mp3","weapons/warden/warden3.mp3"} ),100,100)
        self:ShootBullet( 15, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )
        self.Weapon:SetNextPrimaryFire( CurTime() + 0.12 )
    end

    if mode == "Fire" then
        self:Disorientate()
        self:MakeAFlame()
        self.LaserColor = Color(255,0,0,255)
        self:ShootTracer("LaserTracer_thick")
        local tr = self.Owner:GetEyeTrace();

        if IsValid( tr.Entity ) and tr.Entity:IsPlayer()  then
            if SERVER then
                tr.Entity:Ignite(1,5)
            end
        end
        self.Owner:EmitSound( table.Random(  { "weapons/warden/warden1.mp3","weapons/warden/warden2.mp3","weapons/warden/warden3.mp3"} ),100,100)
        self:ShootBullet( 10, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )
        self.Weapon:SetNextPrimaryFire( CurTime() + 0.1 )
    end
    if mode == "Ice" then
        self:Disorientate()
        self:Freeze()
        self.LaserColor = Color(100,100,255,255)
        self:ShootTracer("LaserTracer_thick")
        local tr = self.Owner:GetEyeTrace();

        if IsValid( tr.Entity ) and tr.Entity:IsPlayer()  then
           -- if SERVER then


                local ent =  tr.Entity
                local weapon = self.Owner:GetActiveWeapon()
                local edata = EffectData()

                edata:SetEntity(ent)
                edata:SetMagnitude(15)
                edata:SetScale(35)

                util.Effect("StunstickImpact", edata)
                util.Effect("TeslaZap", edata)

           -- end
        end
        self.Owner:EmitSound( table.Random(  { "weapons/warden/warden1.mp3","weapons/warden/warden2.mp3","weapons/warden/warden3.mp3"} ),100,100)
        self:ShootBullet(55, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )
        self.Weapon:SetNextPrimaryFire( CurTime() + 0.45 )
    end

    if mode == "Restoration" then

        self.LaserColor = Color(255,0,255,255)
        self:ShootTracer("LaserTracer")
        local tr = self.Owner:GetEyeTrace();

        if IsValid( tr.Entity ) and tr.Entity:IsPlayer()  then
            if SERVER and self.nextheal < CurTime() and tr.Entity:Health() < 150 then
                tr.Entity:SetHealth(tr.Entity:Health()+1)
                self.nextheal = CurTime() + 0.2
            self.Owner:PrintMessage( HUD_PRINTCENTER, "Health: "..tr.Entity:Health() )
            end
            self.Owner:EmitSound( "hl1/fvox/boop.wav", 75, tr.Entity:Health()+105, 0.6, CHAN_AUTO)
        else

            if  (self.Owner.NextZedTimeCustomUse3 && self.Owner.NextZedTimeCustomUse3 > CurTime()) then
            self.Owner:PrintMessage( HUD_PRINTCENTER,  math.floor(self.Owner.NextZedTimeCustomUse3 - CurTime()).." seconds left" )
            return
            end
            local h =  self.Owner:Health()
            self.Owner.NextZedTimeCustomUse3 = CurTime() + 5
            if h < 150 then
                self.Owner:SetHealth(h+5)
                if SERVER then
                self.Owner:EmitSound("vo/npc/Barney/ba_laugh0"..math.random(1,4)..".wav")
                end
                if self.Owner:Health() > 150 then
                    self.Owner:SetHealth(150)
                end
            end


        end
       -- self.Owner:EmitSound( "AlyxEMP.Charge",100,100)
        self.Weapon:SetNextPrimaryFire( CurTime() + 0.01 )
    end

end
SWEP.nextheal = 0
SWEP.nexttracer = 0
function SWEP:ShootTracer( tracername)
    if self.nexttracer > CurTime() then tracername = "AirboatGunTracer" else
    self.nexttracer = CurTime() + 0.3
    end
    local cone = self.Primary.Cone
    local bullet = {}
    bullet.Num = self.Primary.NumShots
    bullet.Src = self.Owner:EyePos()
    bullet.Dir = self.Owner:GetAimVector()
    bullet.Spread = Vector(cone, cone, 0)
    bullet.Tracer = 1
    bullet.Force = 2
    bullet.Damage = 0
    bullet.TracerName = tracername
    self.Owner:FireBullets(bullet)
end

function SWEP:PreDrop(yes) if yes then self:Remove() end end

function SWEP:SecondaryAttack()

    local mode = self:GetNWString("shootmode2")


    if self.Owner:KeyDown(IN_USE) and self.NextReload < CurTime() then
        self.NextReload = CurTime() + 0.3

        self:SetIronsights(false)

        if mode == "Combustion" then  self:SetNWString("shootmode2","Lightning Blast")  end
        if mode == "Lightning Blast" then  self:SetNWString("shootmode2","Self Heal") end
        if mode == "Self Heal" then  self:SetNWString("shootmode2","Combustion") end
        self.Weapon:SetNextSecondaryFire( CurTime() + 0.8 )
        return
    end

    self:SendWeaponAnim(self.PrimaryAnim)

    self.Owner:MuzzleFlash()
    self.Owner:SetAnimation( PLAYER_ATTACK1 )

    self.Weapon:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
    --self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )

    if mode == "Combustion" then
        self:MakeExplosion(NIL,45)
    end

    if mode == "Lightning Blast" then
        local tr = self.Owner:GetEyeTrace();

        if IsValid( tr.Entity ) and tr.Entity:IsPlayer()  then
        local ent = tr.Entity
        local weapon = self.Owner:GetActiveWeapon()
        local edata = EffectData()

        edata:SetEntity(ent)
        edata:SetMagnitude(15)
        edata:SetScale(35)

        util.Effect("TeslaHitBoxes", edata)
        util.Effect("ManhackSparks", edata)
        ent:EmitSound( "weapons/gb_weapons/zeus_thunder.mp3", 500 )

        timer.Create("SparkiesForZeus"..self.EntIndex(),1,3,function()
            if not IsValid( ent ) then  return end
            edata:SetEntity(ent)
            edata:SetMagnitude(10)
            edata:SetScale(25)

            util.Effect("TeslaHitBoxes", edata)
            util.Effect("ManhackSparks", edata)
        end)

        timer.Create("EndBOOM"..self.EntIndex(),4.5,1,function()
            if not IsValid( ent ) or not IsValid(self.Owner) then  return end
            edata:SetEntity(ent)
            edata:SetMagnitude(50)
            edata:SetScale(255)
            ent:EmitSound( "weapons/gb_weapons/zeus_thunder.mp3", 510 )
            util.Effect("TeslaHitBoxes", edata)
            util.Effect("ManhackSparks", edata)

            if SERVER then
                local dmginfo = DamageInfo()
                dmginfo:SetDamageType(DMG_ACID)
                dmginfo:SetDamagePosition(ent:GetPos())
                dmginfo:SetAttacker(self.Owner)
                dmginfo:SetInflictor(self.Owner)
                if self.usedinsta then
                dmginfo:SetDamage(45)
                else
                    dmginfo:SetDamage(150)
                    self.usedinsta = 1
                end
                dmginfo:SetDamageForce(90000 * (ent:GetPos()-self:GetPos()):GetNormal())

                ent:TakeDamageInfo(dmginfo)
                util.Effect("AntlionGib", edata)
             end
        end)
            end
    end

    if mode == "Self Heal" then
        if SERVER then
        self.Owner:EmitSound("vo/npc/Barney/ba_laugh0"..math.random(1,4)..".wav")
        end
        if  (self.Owner.NextZedTimeCustomUse2 && self.Owner.NextZedTimeCustomUse2 > CurTime()) then
        self.Owner:PrintMessage( HUD_PRINTCENTER,  math.floor(self.Owner.NextZedTimeCustomUse2 - CurTime()).." seconds left" )
        return
        end
        local h =  self.Owner:Health()
        self.Owner.NextZedTimeCustomUse2 = CurTime() + 50
        if h < 150 then
            self.Owner:SetHealth(h+50)
            if self.Owner:Health() > 150 then
                self.Owner:SetHealth(150)
            end
        end

    end


end

SWEP.NextSelfHeal = 0

function SWEP:Reload()
    self:Fly()
end


if CLIENT then
    local scope = surface.GetTextureID("sprites/scope")
    function SWEP:DrawHUD( )
        local shotttext = "Mode 1: "..self:GetNWString("shootmode").." | E+LMB to change"
        local shotttext2 = "Mode 2: "..self:GetNWString("shootmode2").." | E+RMB to change"
        surface.SetFont( "ChatFont" );
        local size_x, size_y = surface.GetTextSize( shotttext );
        local size_x2, size_y2 = surface.GetTextSize( shotttext2 );

        draw.RoundedBox( 6, ScrW( ) / 2 - size_x / 2 - 50, ScrH( ) - 100, size_x + 105, size_y + 20 +size_y2, Color( 0, 0, 0, 150 ) );
        draw.DrawText( shotttext , "ChatFont", ScrW( ) / 2, ScrH( ) - 100 + 5, Color( 255, 30, 30, 255 ), TEXT_ALIGN_CENTER );
        draw.DrawText( shotttext2 , "ChatFont", ScrW( ) / 2, ScrH( ) - 80 + 5, Color( 255, 30, 30, 255 ), TEXT_ALIGN_CENTER );
        self.BaseClass.DrawHUD(self)
    end
end