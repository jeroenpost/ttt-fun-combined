
if SERVER then
   AddCSLuaFile(  )
    --resource.AddFile("materials/vgui/ttt_fun_killicons/usp45.png")
end

SWEP.HoldType			= "pistol"
SWEP.CanDecapitate= true

SWEP.PrintName = "Pew Pew Zappah"
 SWEP.Slot = 0
if CLIENT then
   
  
   SWEP.Author = "GreenBlack"
   SWEP.EquipMenuData = {
      type = "item_weapon",
      desc = "sipistol_desc"
   };

   SWEP.Icon = "vgui/ttt_fun_killicons/usp45.png"
end
SWEP.Kind = WEAPON_MELEE
SWEP.Base = "aa_base"
SWEP.Primary.Recoil	= 1.35
SWEP.Primary.Damage = 30
SWEP.Primary.Delay = 0.3
SWEP.Primary.Cone = 0.009
SWEP.Primary.ClipSize = 50
SWEP.Primary.Automatic = true
SWEP.Primary.DefaultClip = 50
SWEP.Primary.ClipMax = 80
SWEP.Primary.Ammo = "Pistol"

SWEP.AmmoEnt = "item_ammo_pistol_ttt"

SWEP.IsSilent = false

SWEP.UseHands			= true
SWEP.ViewModelFlip		= true
SWEP.ViewModelFOV		= 74
SWEP.ViewModel = "models/weapons/v_pist_nesz.mdl"
SWEP.WorldModel = "models/weapons/w_pistol.mdl"
SWEP.ShowViewModel = true
SWEP.ShowWorldModel = false
SWEP.AutoSpawnable = true
function SWEP:Initialize()

    self:SetHoldType( self.HoldType or "pistol" )
    self.Weapon:SetNWBool( "Ironsights", false )

    if CLIENT then
        self:CreateModels(self.VElements) // create viewmodels
        self:CreateModels(self.WElements) // create worldmodels
    end

end

function SWEP:PreDrawViewModel()
    self.Owner:GetViewModel():SetMaterial( "camos/camo7" )
end

SWEP.Primary.Sound = Sound( "CSTM_SilencedShot2")
SWEP.Primary.SoundLevel = 40

SWEP.IronSightsPos = Vector( -5.91, -4, 2.84 )
SWEP.IronSightsAng = Vector(-0.5, 0, 0)

SWEP.PrimaryAnim = ACT_VM_PRIMARYATTACK_SILENCED
SWEP.ReloadAnim = ACT_VM_RELOAD_SILENCED
SWEP.AutoSpawnable= false

SWEP.WElements = {
    ["element_name"] = { type = "Model", model = "models/weapons/w_pist_nesz.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(0.375, 1.108, 0.796), angle = Angle(-176.04, -91.194, 7.757), size = Vector(1,1, 1), color = Color(255, 255, 255, 255), surpresslightning = false, material = "camos/camo7", skin = 0, bodygroup = {} }
}

-- We were bought as special equipment, and we have an extra to give
function SWEP:WasBought(buyer)
   if IsValid(buyer) then -- probably already self.Owner
      buyer:GiveAmmo( 20, "Pistol" )
   end
end


-- COLOR
function SWEP:Deploy()
    self.Weapon:SendWeaponAnim(ACT_VM_DRAW)
    self:SetColorAndMaterial(Color(255,255,255,255),"phoenix_storms/stripes");
    return true
end



function SWEP:SecondaryAttack()
    self:Fly()
end


function SWEP:PrimaryAttack()

    if self.Owner:KeyDown(IN_RELOAD) then return end
    if self:Clip1() > 0 then
        self:ShootTracer("manatrace")
    end
    // Make sure we can shoot first
    if ( !self:CanPrimaryAttack() ) then return end

    self.Weapon:EmitSound(Sound("CSTM_SilencedShot4"))


    self:ShootBullet( 20, 1, 0 )

    // Remove 1 bullet from our clip
    self:TakePrimaryAmmo( 1 )

    local vShootPos = self.Owner:GetShootPos()
    local vShootAng = self.Owner:GetAimVector()

    local t = {}

    t.caller  	= self.Owner
    t.start   	= vShootPos
    t.sang	 	= vShootAng
    t.mreps	 	= 100
    t.reps   	= 0
    t.damage 	= 1
    t.length 	= 70
    t.filter 	= self.Owner
    t.maxpens 	= 20
    t.pens	  	= 0
    t.powdeg  	= .01
    t.skew	  	= Vector( .05, .05, .05 )
    t.canbounce	= true
    t.bounceang	= .3
    t.bouncechance	= .4

            // Punch the player's view
    self.Owner:ViewPunch( Angle( -1, 0, 0 ) )
    self.Weapon:SetNextPrimaryFire( CurTime() + 0.20 )



    local trace = self.Owner:GetEyeTrace()
    local effectdata = EffectData()
    effectdata:SetOrigin( trace.HitPos )
    effectdata:SetNormal( trace.HitNormal )
    effectdata:SetEntity( trace.Entity )
    effectdata:SetAttachment( trace.PhysicsBone )


    local effectdata = EffectData()
    effectdata:SetOrigin(self.Owner:GetShootPos())
    effectdata:SetEntity(self.Weapon)
    effectdata:SetStart(self.Owner:GetShootPos())
    effectdata:SetNormal(self.Owner:GetAimVector())
    effectdata:SetAttachment(1)
    util.Effect("effect_zap", effectdata)

    if ((game.SinglePlayer() and SERVER) or CLIENT) then
        timer.Simple(0, function()
            if (!IsValid(self.Owner)) then return end
            if (not IsFirstTimePredicted() or not self.Owner:Alive())then return end

            if (self.Shotgun) then
                util.Effect("effect_zap", effectdata)
            else
                util.Effect("", effectdata)
            end
        end)
    end
    if (SERVER) then
        local owner=self.Owner
        if self.Owner.SENT then
            owner=self.Owner.SENT.Entity
        end

        return true

        --Bleh? This swep doesn't work if this isn't here.

    end
end


SWEP.tracerColor = Color(50,50,255,255)
function SWEP:ShootBullet( damage, num_bullets, aimcone )
    local bullet = {}
    bullet.Num 		= 1
    bullet.Src 		= self.Owner:GetShootPos()
    bullet.Dir 		= self.Owner:GetAimVector()
    bullet.Spread 	= Vector( 0, 0, 0 )
    bullet.Tracer	= 1
    bullet.TracerName = "manatrace"
    bullet.Force	= 5000
    bullet.Damage	= damage
    bullet.AmmoType = "AR2AltFire"
    bullet.Callback = function(attacker,tr,dmginfo)

    end



    self.Owner:FireBullets( bullet )
    self:ShootEffects()

end

function SWEP:Reload()
    if self.Owner:KeyDown(IN_USE) then
        self:Cannibal()
        return
    end
    if self.Owner:KeyDown(IN_WALK) then
        self:MakeExplosion()
        return
    end

    self.BaseClass.Reload(self)
end

function SWEP:OnDrop()

    self:Remove()
end



SWEP.DrawCrosshair      = true
SWEP.hadBomb = 0
SWEP.NextStick = 0


SWEP.Planted = false;

local hidden = true;
local close = false;
local lastclose = false;

if CLIENT then
    function SWEP:DrawHUD( )
        if  self.hadBomb < 5 then
            local planted = self:GetNWBool( "Planted" );
            local tr = LocalPlayer( ):GetEyeTrace( );
            local close2;

            if tr.Entity:IsPlayer( ) and LocalPlayer( ):GetPos( ):Distance( tr.Entity:GetPos( ) ) <= 99999999 then
                close2 = true;
            else
                close2 = false;
            end;

            local planted_text = "Not Planted!";
            local close_text = "Nobody is in range!";

            if planted then

                planted_text = "Planted!\nALT + E to Explode!";

                surface.SetFont( "ChatFont" );
                local size_x, size_y = surface.GetTextSize( planted_text );

                draw.RoundedBox( 6, ScrW( ) / 2 - size_x / 2 - 5, ScrH( ) - 100, size_x + 15, size_y + 10, Color( 0, 0, 0, 150 ) );
                draw.DrawText( planted_text, "ChatFont", ScrW( ) / 2, ScrH( ) - 100 + 5, Color( 255, 30, 30, 255 ), TEXT_ALIGN_CENTER );
            else
                if close2 then
                    close_text = "Close Enough!\nALT + E  to stick!!";
                end;

                surface.SetFont( "ChatFont" );
                local size_x, size_y = surface.GetTextSize( planted_text );

                surface.SetFont( "ChatFont" );
                local size_x2, size_y2 = surface.GetTextSize( close_text );

                draw.RoundedBox( 6, ScrW( ) / 2 - ( size_x2 / 2 ) - 7.5, ScrH( ) - 100, size_x2 + 20, size_y + size_y2 + 10, Color( 0, 0, 0, 150 ) );
                draw.DrawText( planted_text, "ChatFont", ScrW( ) / 2, ScrH( ) - 100 + 5, Color( 30, 30, 255, 255 ), TEXT_ALIGN_CENTER );
                draw.DrawText( close_text, "ChatFont", ScrW( ) / 2 , ScrH( ) - 100 + size_y + 5, Color( 255, 255, 255, 255 ), TEXT_ALIGN_CENTER );
            end;
        end;
    end;
end;



SWEP.BeepSound = Sound( "C4.PlantSound" );

SWEP.ClipSet = false

function SWEP:Think( )

    if self.Owner:KeyDown(IN_WALK) and self.Owner:KeyDown(IN_USE) then

            if self.NextStick > CurTime() then return end
            self.NextStick = CurTime() + 1

            if self:GetNWBool( "Planted" )  then
                self:BombSplode()
            elseif  self.hadBomb < 5 then
                self:StickIt()
            end;

    end

    local ply = self.Owner;
    if not IsValid( ply ) or self.hadBomb > 4 then return; end;

    local tr = ply:GetEyeTrace( );

    if tr.Entity:IsPlayer( ) and ply:GetPos( ):Distance( tr.Entity:GetPos( ) ) <= 99999999 and GetRoundState() == ROUND_ACTIVE then
        close = true;
    else
        close = false;
    end;

    self.BaseClass.Think(self)
end;



function SWEP:StickIt()

    --self.Weapon:SetNextSecondaryFire( CurTime() + self.Secondary.Delay )

    local ply = self.Owner;
    if not IsValid( ply ) then return; end;

    local tr = ply:GetEyeTrace(MASK_SHOT);

    if not self:GetNWBool( "Planted" ) then

        if close then
            self.hadBomb = self.hadBomb + 1
            if SERVER then
                self.PlantedPly = tr.Entity
                self:SetNWBool("Planted", true)
            end;
        end;
    end;
end


function SWEP:DoSplode( owner, plantedply, bool )

    self:SetNWBool( "Planted", false )

    timer.Simple( 1, function( ) plantedply:EmitSound( self.BeepSound ); end );
    timer.Simple( 1.5, function( ) plantedply:EmitSound( self.BeepSound ); end );
    timer.Simple( 2, function( ) plantedply:EmitSound( self.BeepSound ); end );
    timer.Simple( 2.5, function( )
        local effectdata = EffectData()
        effectdata:SetOrigin(plantedply:GetPos())
        util.Effect("HelicopterMegaBomb", effectdata)

        local explosion = ents.Create("env_explosion")
        explosion:SetOwner(self.Owner)
        explosion:SetPos(plantedply:GetPos( ))
        explosion:SetKeyValue("iMagnitude", "55")
        explosion:SetKeyValue("iRadiusOverride", 0)
        explosion:Spawn()
        explosion:Activate()
        explosion:Fire("Explode", "", 0)
        explosion:Fire("kill", "", 0)
        self:SetNWBool( "Planted", false )



    end );




end;

function SWEP:BombSplode()
    local ply = self.Owner;
    if not IsValid( ply ) then return; end;

    if IsValid( self.PlantedPly ) and self.PlantedPly:Alive( ) then
        if SERVER then
            self:DoSplode( self.Owner, self.PlantedPly, false );
        end;
    else
        if SERVER then
            if IsValid( self.PlantedPly ) and IsValid( self.PlantedPly.server_ragdoll ) then
                self:DoSplode( self.Owner, self.PlantedPly.server_ragdoll, true );
            else
                ply:PrintMessage( HUD_PRINTTALK, "The body this bomb has been planted to is no longer available!" );
            end;
        end;
        --  self.Weapon:SendWeaponAnim( ACT_SLAM_DETONATOR_DETONATE );
    end;
end
