if SERVER then



end
SWEP.PrintName          = "The Last Meal"
SWEP.Slot               = 1
SWEP.Base				= "aa_base"
SWEP.Author             = "GreenBlack"
SWEP.Purpose            = "For the laughs"
SWEP.Instructions       = "Left click to hit other players \nRight click to throw and damage other players \nReload to eat for health"
SWEP.Category			= "Other"
 
SWEP.Spawnable = true
SWEP.AdminSpawnable = true

SWEP.Kind = WEAPON_CARRY
SWEP.Icon = "vgui/ttt_fun_killicons/chips.png"

SWEP.LimitedStock = true

  SWEP.Slot				= 4
   SWEP.SlotPos = 8

   SWEP.EquipMenuData = {
      name = "Crisps 25 health",
      type = "item_weapon",
      desc = "Left click to hit other players \nRight click to throw and damage other players \nReload to eat for health"
   };

SWEP.WElements = {
	["chiips"] = { type = "Model", model = "models/crisps/gaprika.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(1.363, 2.273, -0.456), angle = Angle(-13.296, 0, 0), size = Vector(0.5, 0.5, 0.5), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
}
SWEP.ViewModel				= "models/weapons/v_deat_m249para.mdl"
SWEP.WorldModel				= "models/weapons/w_blopsminigun.mdl"

SWEP.Primary.Clipsize = -1
SWEP.Primary.DefaultClip = -1
SWEP.Primary.Automatic = true
SWEP.Primary.Ammo = "none"
SWEP.ViewModelFlip = false
SWEP.Secondary.Clipsize = -1
SWEP.Secondary.DefaultClip = -1
SWEP.Secondary.Automatic = false
SWEP.Secondary.Ammo = "none"
SWEP.reloaded = false

SWEP.Offset = {
    Pos = {
        Up = 2,
        Right = 32,
        Forward = 0.0,
    },
    Ang = {
        Up = -10,
        Right = 0,
        Forward = 270,
    }
}


function SWEP:DrawWorldModel( )
    local hand, offset, rotate

    if not IsValid( self.Owner ) then
        self:DrawModel( )
        return
    end

    if not self.Hand then
        self.Hand = self.Owner:LookupAttachment( "anim_attachment_rh" )
    end

    hand = self.Owner:GetAttachment( self.Hand )

    if not hand then
        self:DrawModel( )
        return
    end

    offset = hand.Ang:Right( ) * self.Offset.Pos.Right + hand.Ang:Forward( ) * self.Offset.Pos.Forward + hand.Ang:Up( ) * self.Offset.Pos.Up

    hand.Ang:RotateAroundAxis( hand.Ang:Right( ), self.Offset.Ang.Right )
    hand.Ang:RotateAroundAxis( hand.Ang:Forward( ), self.Offset.Ang.Forward )
    hand.Ang:RotateAroundAxis( hand.Ang:Up( ), self.Offset.Ang.Up )

    self:SetRenderOrigin( hand.Pos + offset )
    self:SetRenderAngles( hand.Ang )

    self:DrawModel( )
end

function SWEP:Think()
end

function SWEP:Initialize()
	util.PrecacheSound("weapons/iceaxe/iceaxe_swing1.wav")
	util.PrecacheSound("physics/body/body_medium_impact_hard1.wav")
	util.PrecacheSound("physics/body/body_medium_impact_hard2.wav")
	util.PrecacheSound("physics/body/body_medium_impact_hard3.wav")
	util.PrecacheSound("physics/body/body_medium_impact_hard4.wav")
	util.PrecacheSound("physics/body/body_medium_impact_hard5.wav")
	util.PrecacheSound("physics/body/body_medium_impact_hard6.wav")
	util.PrecacheSound("weapons/knife/knife_slash1.wav")
	util.PrecacheSound("crisps/eat.mp3")
end






function SWEP:Deploy()
end

function SWEP:PrimaryAttack()

    if (!SERVER) then return end

        self.Owner:EmitSound("CSTM_Masada")


    self.Weapon:SendWeaponAnim( ACT_VM_MISSCENTER )
    self.Weapon:EmitSound("weapons/iceaxe/iceaxe_swing1.wav")

    local ent = ents.Create("sent_paprika")
    local pos = self.Owner:GetShootPos()
    ent:SetPos(pos)
    ent:SetModel("models/props_phx/misc/potato.mdl")
    ent:SetAngles(self.Owner:EyeAngles())
    ent:SetPhysicsAttacker( self.Owner )
    --ent.owner = self
    ent:Spawn()
    ent:SetModel("models/props_phx/misc/potato.mdl")
    ent:Activate()
    ent:SetModel("models/props_phx/misc/potato.mdl")
    ent:SetOwner(self.Owner)
    ent:SetPhysicsAttacker(self.Owner)
    ent.owner = self.Owner
    ent.heal = 1
    ent.damage = 30

    self:ShootEffects()

    timer.Simple(4.5,function()
        if IsValid(ent) then ent:Remove() end
    end)


    self.Weapon:SetNextSecondaryFire(CurTime() + 0.5)
    self.Weapon:SetNextPrimaryFire(CurTime() + 0.3)

    local tr = self.Owner:GetEyeTrace()

    local phys = ent:GetPhysicsObject()
    if (phys:IsValid()) then
        phys:Wake()
        phys:SetMass(1)
        phys:EnableGravity( true )
        phys:ApplyForceCenter( self:GetForward() *8000 )
        phys:SetBuoyancyRatio( 0 )
    end

    local shot_length = tr.HitPos:Length()
    ent:SetVelocity(self.Owner:GetAimVector():GetNormalized() *  math.pow(shot_length, 3))

end
function SWEP:OnDrop()
self:Remove()
end

SWEP.Nexthealthingy = 0
SWEP.nextcan = 0
function SWEP:Reload()
   if self.Owner:KeyDown(IN_USE) then
       if self.Weapon.Nexthealthingy and  self.Weapon.Nexthealthingy > CurTime() then
           self.Owner:PrintMessage( HUD_PRINTCENTER,  math.floor(self.Weapon.Nexthealthingy - CurTime()).." seconds left" )
           return
       end
       if (!SERVER) or self.Nexthealthingy > CurTime() then return end
       self.Nexthealthingy = CurTime() + 50
       self.Weapon:SendWeaponAnim( ACT_VM_MISSCENTER )
       self.Weapon:EmitSound("weapons/iceaxe/iceaxe_swing1.wav")

       local ent = ents.Create("sent_paprika")
       local pos = self.Owner:GetShootPos()
       ent:SetPos(pos)
       ent:SetAngles(self.Owner:EyeAngles())
       ent:SetPhysicsAttacker( self.Owner )
       --ent.owner = self
       ent:Spawn()
       ent:Activate()
       ent:SetOwner(self.Owner)
       ent:SetPhysicsAttacker(self.Owner)
       ent.owner = self.Owner


       self.Weapon:SetNextSecondaryFire(CurTime() + 0.5)
       self.Weapon:SetNextPrimaryFire(CurTime() + 1)

       local tr = self.Owner:GetEyeTrace()

       local phys = ent:GetPhysicsObject()
       if (phys:IsValid()) then
           phys:Wake()
           phys:SetMass(1)
           phys:EnableGravity( true )
           phys:ApplyForceCenter( self:GetForward() *200 )
           phys:SetBuoyancyRatio( 0 )
       end

       local shot_length = tr.HitPos:Length()
       ent:SetVelocity(self.Owner:GetAimVector():GetNormalized() *  math.pow(shot_length, 3))


        return
   end

   if self.nextcan < CurTime() and SERVER then
   local ent =self:throw_attack("last_meal_can",500)
   ent.weapon = self

       self.nextcan = CurTime() + 20
       end

end

function SWEP:SecondaryAttack()
if (!SERVER) then return end
self.Weapon:SendWeaponAnim( ACT_VM_MISSCENTER )
self.Owner:EmitSound("weapons/iceaxe/iceaxe_swing1.wav")
self:ShootEffects()
	local ent = ents.Create("last_meal_pizza")
	local pos = self.Owner:GetShootPos()
	ent:SetPos(pos)
	ent:SetAngles(self.Owner:EyeAngles())
	ent:SetPhysicsAttacker( self.Owner )
	--ent.owner = self
	ent:Spawn()
	ent:Activate()
	ent:SetOwner(self.Owner)
ent.owner = self.Owner
	ent:SetPhysicsAttacker(self.Owner)

    timer.Simple(4.5,function()
        if IsValid(ent) then ent:Remove() end
    end)

self.Weapon:SetNextSecondaryFire(CurTime() + 0.8)
self.Weapon:SetNextPrimaryFire(CurTime() + 1)

	local tr = self.Owner:GetEyeTrace()
	
	local phys = ent:GetPhysicsObject()
	if (phys:IsValid()) then
		phys:Wake()
		phys:SetMass(1)
		phys:EnableGravity( true )
		phys:ApplyForceCenter( self:GetForward() *2000 )
		phys:SetBuoyancyRatio( 0 )
	end
 
	local shot_length = tr.HitPos:Length()
	ent:SetVelocity(self.Owner:GetAimVector():GetNormalized() *  math.pow(shot_length, 3))
	

	
end



local ENT = {}

ENT.Type = "anim"
ENT.Base = "base_anim"


if SERVER then

    function ENT:Initialize()
        self:SetModel( "models/workspizza03/workspizza03.mdl" )
        self:SetModelScale(self:GetModelScale()*1,0)
        self.Entity:PhysicsInit(SOLID_VPHYSICS);
        self.Entity:SetMoveType(MOVETYPE_VPHYSICS);
        self.Entity:SetSolid(SOLID_VPHYSICS);
        self.CanBoom = CurTime() + 3
        self.Angles = -360

        --self.Entity:SetCollisionGroup(COLLISION_GROUP_WEAPON)
        local phys = self:GetPhysicsObject()
        if phys:IsValid() then
            phys:Wake()
            phys:EnableGravity(true);
        end


    end


    function ENT:Use( activator, caller )
        if self.CanBoom > CurTime() then return end
        self:EmitSound("common/bugreporter_failed.wav")
        self:Explode()
    end

    function ENT:Touch( ent )
        if ent:IsPlayer() and not ent.nextsandtouch or (ent.nextsandtouch and ent.nextsandtouch < CurTime()) then
            ent.nextsandtouch = CurTime() + 0.2
            ent:Ignite(0.5,0)
            local dmginfo = DamageInfo()
            dmginfo:SetDamage( 75 ) --50 damage
            dmginfo:SetDamageType( DMG_BULLET ) --Bullet damage
            dmginfo:SetAttacker( self.owner or ent ) --First player found gets credit
            dmginfo:SetDamageForce( Vector( 0, 0, 50 ) ) --Launch upwards
            dmginfo:SetInflictor(self)
            ent:TakeDamageInfo( dmginfo )
            self:Explode()
        end
    end

    function ENT:Explode()
        if self.exploded then return end self.exploded = true
        local effect = EffectData()
        effect:SetStart(self:GetPos())
        effect:SetOrigin(self:GetPos())
        effect:SetScale(120)
        effect:SetRadius(120)
        effect:SetMagnitude(250)

        util.Effect("Explosion", effect, true, true)

       -- util.BlastDamage(self, self.owner, self:GetPos(), 100, 100)
        self:Remove()
    end


end

scripted_ents.Register( ENT, "last_meal_pizza", true )


local ENT = {}

ENT.Type = "anim"
ENT.Base = "base_anim"


if SERVER then

    function ENT:Initialize()
        self:SetModel("models/props/cs_office/trash_can_p8.mdl")
        self:SetModelScale(self:GetModelScale()*1,0)
        self.Entity:PhysicsInit(SOLID_VPHYSICS);
        self.Entity:SetMoveType(MOVETYPE_VPHYSICS);
        self.Entity:SetSolid(SOLID_VPHYSICS);
        self.weapon = false

        --self.Entity:SetCollisionGroup(COLLISION_GROUP_WEAPON)
        local phys = self:GetPhysicsObject()
        if phys:IsValid() then
            phys:Wake()
            phys:EnableGravity(true);
        end

        self.activate = CurTime() + 0.03

    end

    function ENT:Explode()
        if self.exploded then return end self.exploded = true
        local effect = EffectData()
        effect:SetStart(self:GetPos())
        effect:SetOrigin(self:GetPos())
        effect:SetScale(1)
        effect:SetRadius(1)
        effect:SetMagnitude(1)

        util.Effect("Explosion", effect, true, true)
        if not self.explodeontouch then
            util.BlastDamage(self, self.Owner, self:GetPos(), 120, 100)
        else
            util.BlastDamage(self, self.Owner, self:GetPos(), 50, 45)
        end
        self:Remove()
    end


    function ENT:PhysicsCollide(data, obj)
     --   self:Touch(obj)
    end

    function ENT:Touch( ent )
        if self.activate < CurTime() and IsValid(ent) and ent:IsPlayer() and not ent.nextsandtouch or (ent.nextsandtouch and ent.nextsandtouch < CurTime())  then

            ent.nextsandtouch = CurTime() + 3

            if not _globals['lastzedtimelastmeal2'] then
                _globals['lastzedtimelastmeal2'] = 0
            end
            if _globals['lastzedtimelastmeal2'] < CurTime() then
                --nt.weapon:ZedTime()
                _globals['lastzedtimelastmeal2'] = CurTime() + 200
                self:ZedTime()
                self:Remove()
                return
            end




            ent:Ignite(0.5,0)
            local dmginfo = DamageInfo()
            dmginfo:SetDamage( 2 ) --50 damage
            dmginfo:SetDamageType( DMG_BULLET ) --Bullet damage
            dmginfo:SetAttacker( self.Owner or self ) --First player found gets credit
            dmginfo:SetDamageForce( Vector( 0, 0, 50 ) ) --Launch upwards
            dmginfo:SetInflictor(self.Owner or self)
            ent:TakeDamageInfo( dmginfo )
            self:Explode()
        end
    end

    function ENT:ZedTime()

        if SERVER then
            self.Owner:GiveEquipmentItem(EQUIP_ZEDTIME)
            hook.Call("SlomoFour", GAMEMODE, self.Owner)
        end

    end


    function ENT:Use( activator, caller )
        self:EmitSound("common/bugreporter_failed.wav")
    end


end

scripted_ents.Register( ENT, "last_meal_can", true )