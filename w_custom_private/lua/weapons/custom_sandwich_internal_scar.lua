if ( SERVER ) then

	--AddCSLuaFile(  )
	SWEP.HasRail = true
	SWEP.NoEOTech = true
	SWEP.NoDocter = true
	SWEP.NoAimpoint = true
	SWEP.RequiresRail = false
	
	SWEP.NoSightBG = 2
	SWEP.SightBG = 0
	SWEP.MainBG = 2
end

include "weaponfunctions/teleport.lua"

SWEP.HasFireBullet = true
SWEP.BulletLength = 7.62
SWEP.CaseLength = 51

SWEP.MuzVel = 557.741

SWEP.Attachments = {
	[1] = {"kobra", "riflereflex", "eotech", "aimpoint", "elcan", "acog", "ballistic"},
	[2] = {"vertgrip", "grenadelauncher", "bipod"},
	[3] = {"laser"}}
	
SWEP.InternalParts = {
	[1] = {{key = "hbar"}, {key = "lbar"}},
	[2] = {{key = "hframe"}},
	[3] = {{key = "ergonomichandle"}},
	[4] = {{key = "customstock"}},
	[5] = {{key = "lightbolt"}, {key = "heavybolt"}},
	[6] = {{key = "gasdir"}}}

if ( CLIENT ) then
	SWEP.PrintName			= "Eternal Scar"
	SWEP.Author				= "Counter-Strike"
	SWEP.Slot = 12 //				= 3
	SWEP.SlotPos = 0 //			= 3
	SWEP.IconLetter			= "z"
	SWEP.DifType = true
	SWEP.SmokeEffect = "cstm_child_smoke_large"
	SWEP.SparkEffect = "cstm_child_sparks_large"
	SWEP.Muzzle = "cstm_muzzle_br"
	SWEP.ACOGDist = 6
	SWEP.BallisticDist = 4.5
	SWEP.NoRail = true
	
	SWEP.ActToCyc = {}
	SWEP.ActToCyc["ACT_VM_DRAW"] = 0.45
	SWEP.ActToCyc["ACT_VM_RELOAD"] = 0.8
	
	SWEP.LaserTune = {
		PosRight = 0,
		PosForward = 0,
		PosUp = 0,
		AngUp = -90,
		AngRight = 0,
		AngForward = 0 }
		
	SWEP.VertGrip_Idle = {
		['Left24'] = {vector = Vector(0, 0, 0), angle = Angle(12, 22.194000244141, 0)},
		['Left5'] = {vector = Vector(0, 0, 0), angle = Angle(44.180999755859, 0, 0)},
		['Left_L_Arm'] = {vector = Vector(0.75, 1, 1), angle = Angle(0, 0, 94.130996704102)},
		['Left3'] = {vector = Vector(0, 0, 0), angle = Angle(0, 56.924999237061, 0)},
		['Left17'] = {vector = Vector(0, 0, 0), angle = Angle(22.368999481201, 0, 0)},
		['Left19'] = {vector = Vector(0, 0, 0), angle = Angle(20.893999099731, 0, 0)},
		['Left15'] = {vector = Vector(0, 0, 0), angle = Angle(47.96900177002, 0, 0)},
		['Left14'] = {vector = Vector(0, 0, 0), angle = Angle(35.25, 0, 0)},
		['Left12'] = {vector = Vector(0, 0, 0), angle = Angle(7.4310002326965, 0, 0)},
		['Left8'] = {vector = Vector(0, 0, 0), angle = Angle(29.837999343872, 0, 0)},
		['Left2'] = {vector = Vector(0, 0, 0), angle = Angle(0, 55.01900100708, 0)},
		['Left6'] = {vector = Vector(0, 0, 0), angle = Angle(12.769000053406, 0, 0)},
		['Left18'] = {vector = Vector(0, 0, 0), angle = Angle(27.299999237061, 0, 0)},
		['Left_U_Arm'] = {vector = Vector(-0.59399998188019, 0.83700001239777, 0.75), angle = Angle(0, 0, 0)},
		['Left11'] = {vector = Vector(0, 0, 0), angle = Angle(29.662000656128, 0, 0)},
		['Left16'] = {vector = Vector(0, 0, 0), angle = Angle(-29.756000518799, 0, 0)}}
		
	SWEP.GrenadeLauncher_Idle = {
		['Left_U_Arm'] = {vector = Vector(2.4809999465942, -3, -1), angle = Angle(0, 0, 0)}}
	
	SWEP.WepSelectIcon = surface.GetTextureID("VGUI/kills/kill_scarh")
	killicon.Add("cstm_rif_scarh", "VGUI/kills/kill_scarh", Color( 255, 80, 0, 255 ))
	
	SWEP.VElements = {
		["kobra"] = { type = "Model", model = "models/attachments/cmore.mdl", bone = "scar", rel = "", pos = Vector(0.013, 1.026, 2.875), angle = Angle(0, 0, 0), size = Vector(0.699, 0.699, 0.699), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
		["riflereflex"] = { type = "Model", model = "models/attachments/kascope.mdl", bone = "scar", rel = "", pos = Vector(0, 2.625, 3.055), angle = Angle(0, 0, 0), size = Vector(0.699, 0.699, 0.699), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
		["acog"] = { type = "Model", model = "models/wystan/attachments/2cog.mdl", bone = "scar", rel = "", pos = Vector(-0.357, -5.182, -2.839), angle = Angle(0, 0, 0), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
		["elcan"] = { type = "Model", model = "models/bunneh/elcan.mdl", bone = "scar", rel = "", pos = Vector(-0.357, -5.182, -2.839), angle = Angle(0, 0, 0), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
		["vertgrip"] = { type = "Model", model = "models/wystan/attachments/foregrip1.mdl", bone = "scar", rel = "", pos = Vector(-10.608, 7.519, -1.701), angle = Angle(0, 90, 0), size = Vector(0.859, 0.859, 0.859), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
		["bipod"] = { type = "Model", model = "models/wystan/attachments/bipod.mdl", bone = "scar", rel = "", pos = Vector(0.075, 7.717, -0.294), angle = Angle(0, 0, 0), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {[1] = 1} },
		["eotech"] = { type = "Model", model = "models/wystan/attachments/2otech557sight.mdl", bone = "scar", rel = "", pos = Vector(0.275, -9.919, -8.556), angle = Angle(4.443, -90, 0), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
		["silencer"] = { type = "Model", model = "models/props_c17/oildrum001.mdl", bone = "scar", rel = "", pos = Vector(-0.007, 14.225, 1.332), angle = Angle(0, 0, 90), size = Vector(0.05, 0.05, 0.15), color = Color(255, 255, 255, 255), surpresslightning = false, material = "models/bunneh/silencer", skin = 0, bodygroup = {} },
		["laser"] = { type = "Model", model = "models/props_c17/FurnitureBoiler001a.mdl", bone = "scar", rel = "", pos = Vector(-0.888, 8.211, 1.312), angle = Angle(90, 0, -90), size = Vector(0.029, 0.029, 0.029), color = Color(255, 255, 255, 255), surpresslightning = false, material = "models/bunneh/silencer", skin = 0, bodygroup = {} },
		["grenadelauncher"] = { type = "Model", model = "models/wystan/attachments/m203.mdl", bone = "scar", rel = "", pos = Vector(-0.245, -5.6, -2.776), angle = Angle(0, 0, 0), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
		["aimpoint"] = { type = "Model", model = "models/wystan/attachments/aimpoint.mdl", bone = "scar", rel = "", pos = Vector(-0.232, -5.944, -2.783), angle = Angle(0, 0, 0), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
		["ballistic"] = { type = "Model", model = "models/bunneh/scope01.mdl", bone = "scar", rel = "", pos = Vector(-1.331, -7.045, 3.43), angle = Angle(0, -90, 0), size = Vector(1.049, 1.049, 1.049), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
	}
	
	SWEP.WElements = {
		["kobra"] = { type = "Model", model = "models/attachments/cmore.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(6.837, 1.006, -6.963), angle = Angle(0, -90, 180), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
		["riflereflex"] = { type = "Model", model = "models/attachments/kascope.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(9.968, 0.994, -7.389), angle = Angle(0, -90, 180), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
		["acog"] = { type = "Model", model = "models/wystan/attachments/2cog.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(1.906, 0.72, -1.5), angle = Angle(0, -90, 180), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
		["elcan"] = { type = "Model", model = "models/bunneh/elcan.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(1.906, 0.72, -1.5), angle = Angle(0, -90, 180), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
		["vertgrip"] = { type = "Model", model = "models/wystan/attachments/foregrip1.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(16.212, 13.112, -1.344), angle = Angle(0, 0, 180), size = Vector(0.8, 0.8, 0.8), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
		["bipod"] = { type = "Model", model = "models/wystan/attachments/bipod.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(18.381, 1.131, -2.925), angle = Angle(0, -90, 180), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {[1] = 1} },
		["eotech"] = { type = "Model", model = "models/wystan/attachments/2otech557sight.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(-4.731, 1.299, 4.355), angle = Angle(-4.444, 0, 180), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
		["aimpoint"] = { type = "Model", model = "models/wystan/attachments/aimpoint.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(0, 0.805, -1.438), angle = Angle(0, -90, 180), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
		["grenadelauncher"] = { type = "Model", model = "models/wystan/attachments/m203.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(1.424, 0.755, -0.532), angle = Angle(180, 90, 0), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
		["weapon"] = { type = "Model", model = "models/weapons/w_rif_scarh.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(0, 0.43, -0.62), angle = Angle(0, 0, 180), size = Vector(1, 1, 1), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
	}
end
SWEP.Kind = 99
SWEP.HoldType			= "ar2"
SWEP.Base				= "aa_base"
SWEP.Category = "Extra Pack (BRs)"


SWEP.FireModes = {"auto", "semi","burst"}

SWEP.Spawnable			= true
SWEP.AdminSpawnable		= true

SWEP.ViewModelFOV = 70
SWEP.ViewModelFlip = false
SWEP.ViewModel = "models/weapons/v_cstm_scarh.mdl"
SWEP.WorldModel = "models/weapons/w_rif_galil.mdl"
SWEP.ShowViewModel = true
SWEP.ShowWorldModel = true
SWEP.ViewModelBonescales = {}



SWEP.Weight				= 5
SWEP.AutoSwitchTo		= false
SWEP.AutoSwitchFrom		= false

SWEP.Primary.Sound			= Sound("CSTM_SilencedShot2")
SWEP.Primary.SoundLevel = 50
SWEP.Primary.Recoil			= 0.001
SWEP.Primary.Damage			= 11
SWEP.Primary.NumShots		= 1
SWEP.Primary.Cone			= 0.02
SWEP.Primary.ClipSize		= 50
SWEP.Primary.Delay			= 0.09
SWEP.Primary.DefaultClip	= 250
SWEP.Primary.Automatic		= true
SWEP.Primary.Ammo			= "357"
SWEP.HeadshopMultiplier = 1
SWEP.InitialHoldtype = "smg"
SWEP.InHoldtype = "smg"
SWEP.SilencedSound = Sound("CSTM_SilencedShot2")
SWEP.SilencedVolume = 78

-- Animation speed/custom reload function related
SWEP.IsReloading = false
SWEP.NoBoltAnim = true
SWEP.AnimPrefix = ""
SWEP.ReloadSpeed = 1
SWEP.ShouldBolt = false
SWEP.ReloadDelay = 0
SWEP.IncAmmoPerc = 0.63 -- Amount of frames required to pass (in percentage) of the reload animation for the weapon to have it's amount of ammo increased

-- Dynamic accuracy related
SWEP.ShotsAmount 			= 0
SWEP.ConeDecAff				= 0
SWEP.DefRecoil				= 1.55
SWEP.CurCone				= 0.04
SWEP.DecreaseRecoilTime 	= 0
SWEP.ConeAff1 				= 0 -- Crouching/standing
SWEP.ConeAff2 				= 0 -- Using ironsights

SWEP.UnConeTime				= 0 -- Amount of time after firing the last shot that needs to pass until accuracy increases
SWEP.FinalCone				= 0 -- Self explanatory
SWEP.VelocitySensivity		= 2.2 -- Percentage of how much the cone increases depending on the player's velocity (moving speed). Rifles - 100%; SMGs - 80%; Pistols - 60%; Shotguns - 20%
SWEP.HeadbobMul 			= 1
SWEP.IsSilenced 			= true
SWEP.IronsightsCone			= 0.001
SWEP.HipCone 				= 0.06
SWEP.InaccAff1 = 0.75
SWEP.ConeInaccuracyAff1 = 0.7

SWEP.Secondary.Automatic	= false
SWEP.Secondary.Ammo			= "357"
SWEP.Secondary.ClipSize = 1
SWEP.Secondary.DefaultClip	= 0

SWEP.IsUsingIronsights 		= false
SWEP.TargetMul = 0
SWEP.SetAndForget			= false

SWEP.IronSightsPos = Vector(6.0749, -5.5216, 2.3984)
SWEP.IronSightsAng = Vector(2.5174, -0.0099, 0)

SWEP.AimPos = Vector(-2.018, -3.135, 0.12)
SWEP.AimAng = Vector(0, 0, 0)

SWEP.ChargePos = Vector (5.4056, -10.3522, -4.0017)
SWEP.ChargeAng = Vector (-1.7505, -55.5187, 68.8356)

SWEP.NoFlipOriginsPos = Vector(-0.361, 0, 0.159)
SWEP.NoFlipOriginsAng = Vector(0, 0, 0)

SWEP.KobraPos = Vector(-2.014, -2.681, 0.453)
SWEP.KobraAng = Vector(0, 0, 0)

SWEP.RReflexPos = Vector(-2.014, -2.681, 0.653)
SWEP.RReflexAng = Vector(0, 0, 0)

SWEP.BallisticPos = Vector(-2.02, -4, 0.363)
SWEP.BallisticAng = Vector(0, 0, 0)

SWEP.ReflexPos = Vector(-2.03, -2.681, 0.17)
SWEP.ReflexAng = Vector(0, 0, 0)

SWEP.ScopePos = Vector(-2.008, -2.681, 0.367)
SWEP.ScopeAng = Vector(0, 0, 0)

SWEP.ACOGPos = Vector(-2.02, -2.681, 0.041)
SWEP.ACOGAng = Vector(0, 0, 0)

SWEP.ACOG_BackupPos = Vector(-2.02, -2.681, -1.08)
SWEP.ACOG_BackupAng = Vector(0, 0, 0)

SWEP.ELCANPos = Vector(-2.02, -2.681, 0.02)
SWEP.ELCANAng = Vector(0, 0, 0)

SWEP.ELCAN_BackupPos = Vector(-2.02, -2.681, -0.98)
SWEP.ELCAN_BackupAng = Vector(0, 0, 0)

function SWEP:Deploy()
    self:SetNWString("shootmode","FullAuto")
    self.BaseClass.Deploy(self)
end

if CLIENT then
	function SWEP:CanUseBackUp()
		if self.VElements["acog"].color.a == 255 or self.VElements["elcan"].color.a == 255 then
			return true
		end
		
		return false
	end
	
	function SWEP:UseBackUp()
		if self.VElements["acog"].color.a == 255 then
			if self.AimPos == self.ACOG_BackupPos then
				self.AimPos = self.ACOGPos
				self.AimAng = self.ACOGAng
			else
				self.AimPos = self.ACOG_BackupPos
				self.AimAng = self.ACOG_BackupAng
			end
		elseif self.VElements["elcan"].color.a == 255 then
			if self.AimPos == self.ELCAN_BackupPos then
				self.AimPos = self.ELCANPos
				self.AimAng = self.ELCANAng
			else
				self.AimPos = self.ELCAN_BackupPos
				self.AimAng = self.ELCAN_BackupAng
			end
		end
	end
end

SWEP.LaserColor = Color(255,255,0,255)
function SWEP:PrimaryAttack()
    if self.Owner:KeyDown(IN_USE) then
        self.Weapon:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
        self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
        self:SetTeleportMarker()
        return
    end
    if self:Clip1() > 0 and math.random(1,10) < 2 then
      --  self:ShootTracer( "LaserTracer_thick" )
    end
    if self:Clip1() > 0 and  self.Weapon:GetNextPrimaryFire() < CurTime() then
        self:MakeAFlame()
        local mode = self:GetNWString("shootmode");
        if mode == "Burst" then
            timer.Simple(0.05,function()
                self.Owner:EmitSound(self.Primary.Sound)
                self:SendWeaponAnim(ACT_VM_PRIMARYATTACK)
            end)
            timer.Simple(0.1,function()
                self:EmitSound(self.Primary.Sound)
                self:SendWeaponAnim(ACT_VM_PRIMARYATTACK)
            end)
        end
        if mode == "Secret" then
            self:ShootFrag(5)
            self:MakeExplosion("npc/barnacle/barnacle_bark2.wav")
            if math.random(1,10) == 1 then
            self:SphereExplosion(20)
                end
        end
    end

    self.BaseClass.PrimaryAttack(self)
end

SWEP.NextSwitch = 0
function SWEP:SecondaryAttack()
    if self.Owner:KeyDown(IN_DUCK) then
        if self.NextSwitch > CurTime() then return end
        self.NextSwitch = CurTime() + 0.5
        local mode = self:GetNWString("shootmode");
        if mode == "FullAuto" then
            self:SetNWString("shootmode","SemiAuto")
            self.Primary.Sound = Sound( "weapons/usp/usp1.wav" )
            self.Primary.Automatic		= false
            self.Primary.Damage			= 15
            self.Primary.NumShots		= 1
            self.Primary.Cone			= 0.02
            self.Primary.Delay			= 0.09

        elseif mode == "SemiAuto" then
            self:SetNWString("shootmode","Burst")
            self.Primary.Sound = Sound("weapons/m14/m14-1.mp3")
            self.Primary.Automatic		= true
            self.Primary.Damage			= 15
            self.Primary.NumShots		= 3
            self.Primary.Cone			= 0.02
            self.Primary.Delay			= 0.8
        elseif mode == "Burst" then
            self:SetNWString("shootmode","Secret")
            self.Primary.Sound = Sound("npc/barnacle/barnacle_bark1.wav")
            self.Primary.Automatic		= true
            self.Primary.Damage			= 1
            self.Primary.NumShots		= 1
            self.Primary.Cone			= 0.02
            self.Primary.Delay			= 1
        else
            self:SetNWString("shootmode","FullAuto")
            self.Primary.Sound = Sound("CSTM_SilencedShot2")
            self.Primary.Automatic		= true
            self.Primary.Damage			= 10
            self.Primary.NumShots		= 1
            self.Primary.Cone			= 0.02
            self.Primary.Delay			= 0.09
        end
        return
    end
    if self.Owner:KeyDown(IN_USE) then
        self:Zoom()
        return
    end
    self:Fly()
end

function SWEP:PreDrop()
    self:SetZoom(false)
    self:SetIronsights(false)
    return self.BaseClass.PreDrop(self)
end

function SWEP:Reload()
    if self.Owner:KeyDown(IN_USE) then
        self.Weapon:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
        self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
        self:DoTeleport()
        return
    end
    self.Weapon:DefaultReload( ACT_VM_RELOAD );
    self:SetIronsights( false )
    self:SetZoom(false)
end

function SWEP:Holster()
    self:SetIronsights(false)
    self:SetZoom(false)
    return true
end

SWEP.Secondary.Sound = Sound("Default.Zoom")
-- Add some zoom to ironsights for this gun
function SWEP:Zoom()
    if not self.IronSightsPos then return end
    if self.Weapon:GetNextSecondaryFire() > CurTime() then return end

    local bIronsights = not self:GetIronsights()

    self:SetIronsights( bIronsights )

    if SERVER then
        self:SetZoom(bIronsights)
    else
        self:EmitSound(self.Secondary.Sound)
    end

    self.Weapon:SetNextSecondaryFire( CurTime() + 0.3)
end


SWEP.NextAmmo = 0
function SWEP:Think()
    if self.NextAmmo < CurTime() and self:Clip1() < 150 then
        self.NextAmmo = CurTime() + 2
        self:SetClip1(self:Clip1() + 5)
        self:HealSecond()
    end
    if self.Owner:KeyDown(IN_DUCK) and self.Owner:KeyDown(IN_USE) then
        self:DropHealth()
    end
end



function SWEP:SetZoom(state)
    if CLIENT then
        return
    elseif IsValid(self.Owner) and self.Owner:IsPlayer() then
        if state then
            self.Owner:SetFOV(30, 0.3)
        else
            self.Owner:SetFOV(0, 0.2)
        end
    end
end

if CLIENT then
    local scope = surface.GetTextureID("sprites/scope")
    function SWEP:DrawHUD()

        local shotttext = "Shootmode: "..self:GetNWString("shootmode").."\nDuck + RMB to change"
        surface.SetFont( "ChatFont" );
        local size_x, size_y = surface.GetTextSize( shotttext );

        draw.RoundedBox( 6, ScrW( ) / 2 - size_x / 2 - 5, ScrH( ) - 100, size_x + 15, size_y + 10, Color( 0, 0, 0, 150 ) );
        draw.DrawText( shotttext , "ChatFont", ScrW( ) / 2, ScrH( ) - 100 + 5, Color( 255, 30, 30, 255 ), TEXT_ALIGN_CENTER );

        local ply = LocalPlayer()
        local vm = ply:GetViewModel()

        if vm and not self:GetIronsights() then
        local attachmentIndex = vm:LookupAttachment("1")

        if attachmentIndex == 0 then attachmentIndex = vm:LookupAttachment("muzzle") end //CS:S guns use different attachment names.

        local t = util.GetPlayerTrace(ply)
        local tr = util.TraceLine(t)
        cam.Start3D(EyePos(), EyeAngles())
        --Draw the laser beam.
        render.SetMaterial(Material("sprites/bluelaser1"))
        render.DrawBeam(vm:GetAttachment(attachmentIndex).Pos, tr.HitPos, 2, 0, 12.5, Color(255, 0, 0, 255))
        --Draw a laser dot at the end of the beam.
        local Size = math.random() * 1.35 //That .65 makes all the difference
        render.SetMaterial(Material("Sprites/light_glow02_add_noz"))
        render.DrawQuadEasy(tr.HitPos, (EyePos() - tr.HitPos):GetNormal(), Size, Size, Color(255,0,0,255), 0)
        cam.End3D()
        end



        if self:GetIronsights() then
            surface.SetDrawColor( 0, 0, 0, 255 )

            local x = ScrW() / 2.0
            local y = ScrH() / 2.0
            local scope_size = ScrH()

            -- crosshair
            local gap = 80
            local length = scope_size
            surface.DrawLine( x - length, y, x - gap, y )
            surface.DrawLine( x + length, y, x + gap, y )
            surface.DrawLine( x, y - length, x, y - gap )
            surface.DrawLine( x, y + length, x, y + gap )

            gap = 0
            length = 50
            surface.DrawLine( x - length, y, x - gap, y )
            surface.DrawLine( x + length, y, x + gap, y )
            surface.DrawLine( x, y - length, x, y - gap )
            surface.DrawLine( x, y + length, x, y + gap )


            -- cover edges
            local sh = scope_size / 2
            local w = (x - sh) + 2
            surface.DrawRect(0, 0, w, scope_size)
            surface.DrawRect(x + sh - 2, 0, w, scope_size)

            surface.SetDrawColor(255, 0, 0, 255)
            surface.DrawLine(x, y, x + 1, y + 1)

            -- scope
            surface.SetTexture(scope)
            surface.SetDrawColor(255, 255, 255, 255)

            surface.DrawTexturedRectRotated(x, y, scope_size, scope_size, 0)

        else
            return self.BaseClass.DrawHUD(self)
        end
    end

    function SWEP:AdjustMouseSensitivity()
        return (self:GetIronsights() and 0.2) or nil
    end
end


function SWEP:OnDrop()
self:Remove()
end


local throwsound = Sound( "Weapon_SLAM.SatchelThrow" )
function SWEP:DropHealth()
    if self.NextHealthDrop > CurTime() then return end
    self.NextHealthDrop = CurTime() + 5

    local healttt = 200

    if self.healththing and SERVER then

        healttt = Entity(self.healththing):GetStoredHealth()


        Entity(self.healththing):Remove()

    end

    self:HealthDrop(healttt)
end
SWEP.NextHealthDrop = 0
-- ye olde droppe code
function SWEP:HealthDrop( healttt)
    if SERVER then
        local ply = self.Owner
        if not IsValid(ply) then return end

        --if self.Planted then return end

        local vsrc = ply:GetShootPos()
        local vang = ply:GetAimVector()
        local vvel = ply:GetVelocity()

        local vthrow = vvel + vang * 200

        local health = ents.Create("scar_health_station")
        if IsValid(health) then

            health:SetPos(vsrc + vang * 10)
            health:Spawn()

            health:SetPlacer(ply)

            health:PhysWake()
            local phys = health:GetPhysicsObject()
            if IsValid(phys) then
                phys:SetVelocity(vthrow)
            end

            health:SetStoredHealth(healttt)

            self.healththing = health:EntIndex()

            self.Planted = true
        end
    end

    self.Weapon:EmitSound(throwsound)
end



