

SWEP.Base = "aa_base"

SWEP.HasFireBullet = true
SWEP.HasFlare = true
SWEP.HasFly = true
SWEP.HasHealshot = true


if SERVER then
    AddCSLuaFile(  )
end


SWEP.PrintName = "Punishers deagle"
SWEP.Kind = WEAPON_PISTOL

SWEP.Icon = "vgui/ttt_fun_killicons/golden_deagle.png"
SWEP.Slot = 1
SWEP.HoldType = "pistol"
SWEP.Spawnable = true
SWEP.AdminSpawnable = true
SWEP.Primary.Ammo = "AlyxGun"
SWEP.Primary.Recoil = 1
SWEP.Primary.Damage = 55
SWEP.Primary.Delay = 0.55
SWEP.Primary.Cone = 0.009
SWEP.Primary.ClipSize = 80
SWEP.Primary.ClipMax = 120
SWEP.Primary.DefaultClip = 80
SWEP.Primary.NumShots = 1
SWEP.Primary.Automatic = true
SWEP.HeadshotMultiplier = 3
SWEP.AutoSpawnable = false
SWEP.AmmoEnt = "item_ammo_revolver_ttt"
SWEP.Primary.Sound = Sound("gdeagle/gdeagle-1.mp3")
SWEP.Primary.SoundLevel = 400
SWEP.ViewModel = "models/weapons/v_pist_gbagle.mdl"
SWEP.WorldModel = "models/weapons/w_pist_gbagle.mdl"
SWEP.ShowWorldModel = true
SWEP.ShowViewModel = true

SWEP.AllowDrop = true
SWEP.IronSightsPos = Vector(-5, -20, -20)
SWEP.IronSightsAng = Vector(0, 0, 0)

function SWEP:PrimaryAttack()
    self.Weapon:SetNextPrimaryFire(CurTime() + self.Primary.Delay)
    if not self:CanPrimaryAttack() then return end

    self.Weapon:EmitSound(self.Primary.Sound, self.Primary.SoundLevel)
    self:ShootBullet(self.Primary.Damage, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone())
    if SERVER then
        self:HealShot();
        self:MakeAFlame();
    end
    self:TakePrimaryAmmo(1)
    self.Owner:ViewPunch(Angle(math.Rand(-0.2, -0.1) * self.Primary.Recoil, math.Rand(-0.1, 0.1) * self.Primary.Recoil, 0))
end


function SWEP:SecondaryAttack()
    if SERVER then
        self:Fly();
    end
end


function SWEP:Reload()

    if self.ReloadingTime and CurTime() <= self.ReloadingTime then return
    end

    if not self.Owner:KeyDown(IN_USE) then
    self:DefaultReload(ACT_VM_RELOAD)
    local AnimationTime = self.Owner:GetViewModel():SequenceDuration()
    self.ReloadingTime = CurTime() + AnimationTime
    self:SetNextPrimaryFire(CurTime() + AnimationTime)
    self:SetNextSecondaryFire(CurTime() + AnimationTime)
    return
    end

    if SERVER then
        self:ShootFlare();
    end
end

function SWEP:Think()
    self:HealSecond()
end