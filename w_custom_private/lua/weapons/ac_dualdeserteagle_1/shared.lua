if ( SERVER ) then

	AddCSLuaFile(  )
	SWEP.HasRail = true
	SWEP.NoAimpoint = true
	SWEP.NoACOG = true
	SWEP.NoEOTech = true
	SWEP.NoDocter = true
	
	SWEP.RequiresRail = false
	SWEP.NoLaser = true
end
SWEP.PsID = "cm_dualdeagles"
SWEP.BulletLength = 12.7
SWEP.CaseLength = 32.6
SWEP.CanDecapitate= true
SWEP.MuzVel = 261.154
SWEP.PrintName			= "Poofer's Double Tap"
if ( CLIENT ) then


	SWEP.Author				= "Counter-Strike"
	SWEP.Slot				= 4
	SWEP.SlotPos = 0 //			= 1
	SWEP.IconLetter			= "f"
	SWEP.Muzzle = "cstm_muzzle_br"
	SWEP.SparkEffect = "cstm_child_sparks_large"
	SWEP.SmokeEffect = "cstm_child_smoke_large"
	SWEP.DifType = true
	SWEP.SafeXMoveMod = 2
	

	SWEP.VElements = {
		["aimpointdot"] = { type = "Sprite", sprite = "effects/redflare", bone = "v_weapon.Deagle_Slide", pos = Vector(0.013, 1.156, 0.386), size = { x = 0.3, y = 0.3 }, color = Color(255, 255, 255, 0), nocull = true, additive = true, vertexalpha = true, vertexcolor = true, ignorez = false},
		["aimpoint"] = { type = "Model", model = "models/wystan/attachments/2octorrds.mdl", bone = "v_weapon.Deagle_Slide", pos = Vector(-0.239, 0.574, 0.924), angle = Angle(0, 0, 90), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
	}
	
	SWEP.WElements = {
		["weapon"] = { type = "Model", model = "models/weapons/w_pist_deagle.mdl", bone = "ValveBiped.Bip01_L_Hand", rel = "", pos = Vector(-0.375, 2.331, 0), angle = Angle(0, 0, 0), size = Vector(1, 1, 1), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
	}
	
	SWEP.NoProperIronsights = true
end

SWEP.Base				= "cstm_base_dual"
SWEP.Category = "Extra Pack (Sidearms)"
SWEP.FireModes = {"semi" }
SWEP.Kind = WEAPON_CARRY

SWEP.Spawnable			= true
SWEP.AdminSpawnable		= true

SWEP.HoldType = "duel"
SWEP.ViewModelFOV = 70
SWEP.ViewModelFlip = true
SWEP.ViewModel = "models/weapons/v_pist_deags.mdl"
SWEP.WorldModel = "models/weapons/w_pist_deagle.mdl"
SWEP.ShowViewModel = true
SWEP.ShowWorldModel = true
SWEP.ViewModelBonescales = {}

SWEP.Weight				= 5
SWEP.AutoSwitchTo		= false
SWEP.AutoSwitchFrom		= false

SWEP.Primary.Sound			= Sound("CSTM_Deagle")
SWEP.Primary.Recoil			= 2
SWEP.Primary.Damage			= 41
SWEP.Primary.NumShots		= 1
SWEP.Primary.Cone			= 0.015
SWEP.Primary.ClipSize		= 900
SWEP.Primary.Delay			= 0.22
SWEP.Primary.DefaultClip	= 900
SWEP.Primary.Automatic		= false
SWEP.Primary.Ammo			= ".50AE"
SWEP.InitialHoldtype = "pistol"
SWEP.InHoldtype = "pistol"
SWEP.CantSilence = true
SWEP.NoBoltAnim = true
SWEP.ChamberAmount = 2
SWEP.SprintAndShoot = true
SWEP.HeadshotMultiplier = 2.5

-- Animation speed/custom reload function related
SWEP.IsReloading = false
SWEP.AnimPrefix = ""
SWEP.ReloadSpeed = 1
SWEP.ShouldBolt = false
SWEP.ReloadDelay = 0
SWEP.IncAmmoPerc = 0.75 -- Amount of frames required to pass (in percentage) of the reload animation for the weapon to have it's amount of ammo increased
SWEP.FOVZoom = 85

-- Dynamic accuracy related
SWEP.ShotsAmount 			= 0
SWEP.ConeDecAff				= 0
SWEP.DefRecoil				= 3
SWEP.CurCone				= 0.04
SWEP.DecreaseRecoilTime 	= 0
SWEP.ConeAff1 				= 0 -- Crouching/standing
SWEP.ConeAff2 				= 0 -- Using ironsights

SWEP.UnConeTime				= 0 -- Amount of time after firing the last shot that needs to pass until accuracy increases
SWEP.FinalCone				= 0 -- Self explanatory
SWEP.VelocitySensivity		= 1 -- Percentage of how much the cone increases depending on the player's velocity (moving speed). Rifles - 100%; SMGs - 80%; Pistols - 60%; Shotguns - 20%
SWEP.HeadbobMul 			= 1
SWEP.IsSilenced 			= false
SWEP.IronsightsCone 		= 0.02
SWEP.HipCone 				= 0.046
SWEP.ConeInaccuracyAff1 = 0.5

SWEP.Secondary.Automatic	= false
SWEP.Secondary.Ammo			= "none"

SWEP.IsUsingIronsights 		= false
SWEP.TargetMul = 0
SWEP.SetAndForget			= false

SWEP.AnimCyc = 1

SWEP.IronSightsPos = Vector(6.0749, -5.5216, 2.3984)
SWEP.IronSightsAng = Vector(2.5174, -0.0099, 0)

SWEP.AimPos = Vector(-0.24, -1.803, 2.319)
SWEP.AimAng = Vector(0, 0, 0)

SWEP.ChargePos = Vector (5.4056, -10.3522, -4.0017)
SWEP.ChargeAng = Vector (-1.7505, -55.5187, 68.8356)

SWEP.MeleePos = Vector(0.843, -0.956, 0.537)
SWEP.MeleeAng = Vector(0, 8.1, 0)

SWEP.SafePos = Vector(0, -7.954, -8.11)
SWEP.SafeAng = Vector(70, 0, 0)

SWEP.FlipOriginsPos = Vector(0, 0, 0)
SWEP.FlipOriginsAng = Vector(0, 0, 0)

SWEP.CustomizePos = Vector(0, -10, -8.11)
SWEP.CustomizeAng = Vector(70, 0, 0)
