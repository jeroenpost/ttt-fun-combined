
SWEP.PsID = "cm_glock18"
SWEP.BulletLength = 9
SWEP.CaseLength = 19

SWEP.MuzVel = 255.905

SWEP.Attachments = {
    [1] = {"reflex"}}

SWEP.InternalParts = {
    [1] = {{key = "ergonomichandle"}}}

if ( CLIENT ) then

    SWEP.PrintName			= "iHuggle's Bust Hug"
    SWEP.Author				= "Counter-Strike"
    SWEP.Slot				= 1
    SWEP.SlotPos = 0 //			= 1
    SWEP.IconLetter			= "c"
    SWEP.ReloadAngleMod = -1
    SWEP.Muzzle = "cstm_muzzle_pistol"
    SWEP.SparkEffect = "cstm_child_sparks_small"
    SWEP.SmokeEffect = "cstm_child_smoke_small"

    killicon.AddFont( "cstm_pistol_glock18", "CSKillIcons", SWEP.IconLetter, Color( 255, 80, 0, 255 ) )

    SWEP.VElements = {
        ["silencer"] = { type = "Model", model = "models/props_c17/oildrum001.mdl", bone = "v_weapon.Glock_Parent", pos = Vector(-2.845, 3.2, -0.557), angle = Angle(92.43, 14.538, 1.187), size = Vector(0.043, 0.043, 0.136), color = Color(255, 255, 255, 0), surpresslightning = false, material = "models/bunneh/silencer", skin = 0, bodygroup = {} },
        ["reflex"] = { type = "Model", model = "models/wystan/attachments/2octorrds.mdl", bone = "v_weapon.Glock_Slide", pos = Vector(0.887, 0.125, 0.312), angle = Angle(-77.087, -89.825, 0), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
    }

    SWEP.WElements = {
        ["silencer"] = { type = "Model", model = "models/props_c17/oildrum001.mdl", pos = Vector(10.404, 1.976, -3.8), angle = Angle(-91.904, 0, 5.664), size = Vector(0.043, 0.043, 0.136), color = Color(255, 255, 255, 0), surpresslightning = false, material = "models/bunneh/silencer", skin = 0, bodygroup = {} },
        ["reflex"] = { type = "Model", model = "models/wystan/attachments/2octorrds.mdl", pos = Vector(2.273, 1.1, -2.3), angle = Angle(180, 85, 0), size = Vector(1.299, 1.299, 1.299), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
    }

    SWEP.SafeXMoveMod = 2
end

SWEP.Category = "Customizable Weaponry"
SWEP.HoldType			= "ar2"
SWEP.Base				= "aa_base"
SWEP.FireModes = {"auto", "3burst", "semi"}

SWEP.Spawnable			= true
SWEP.AdminSpawnable		= true

SWEP.HoldType = "pistol"
SWEP.ViewModelFOV = 70
SWEP.ViewModelFlip = true
SWEP.ViewModel = "models/weapons/v_pist_glock18.mdl"
SWEP.WorldModel = "models/weapons/w_pist_glock18.mdl"
SWEP.ShowViewModel = true
SWEP.ShowWorldModel = true
SWEP.ViewModelBonescales = {}

SWEP.Weight				= 5
SWEP.AutoSwitchTo		= false
SWEP.AutoSwitchFrom		= false

SWEP.Primary.Sound			= Sound("vo/npc/Alyx/gasp02.wav")
SWEP.Primary.Recoil			= 0.3
SWEP.Primary.Damage			= 55
SWEP.Primary.NumShots		= 1
SWEP.Primary.Cone			= 0.02
SWEP.Primary.ClipSize		= 50
SWEP.Primary.Delay			= 0.45
SWEP.Primary.DefaultClip	= 50
SWEP.Primary.Automatic		= true
SWEP.Primary.Ammo			= "9x19MM"
SWEP.InitialHoldtype = "pistol"
SWEP.InHoldtype = "pistol"
SWEP.NoBoltAnim = true
SWEP.DryFireAnim = true
SWEP.CanDecapitate= true
SWEP.SprintAndShoot = true

-- Animation speed/custom reload function related
SWEP.IsReloading = false
SWEP.AnimPrefix = "glock_"
SWEP.ReloadSpeed = 1
SWEP.ShouldBolt = false
SWEP.ReloadDelay = 0
SWEP.IncAmmoPerc = 0.72 -- Amount of frames required to pass (in percentage) of the reload animation for the weapon to have it's amount of ammo increased
SWEP.FOVZoom = 85

-- Dynamic accuracy related
SWEP.ShotsAmount 			= 0
SWEP.ConeDecAff				= 0
SWEP.DefRecoil				= 0.5
SWEP.CurCone				= 0.04
SWEP.DecreaseRecoilTime 	= 0
SWEP.ConeAff1 				= 0 -- Crouching/standing
SWEP.ConeAff2 				= 0 -- Using ironsights

SWEP.UnConeTime				= 0 -- Amount of time after firing the last shot that needs to pass until accuracy increases
SWEP.FinalCone				= 0 -- Self explanatory
SWEP.VelocitySensivity		= 1 -- Percentage of how much the cone increases depending on the player's velocity (moving speed). Rifles - 100%; SMGs - 80%; Pistols - 60%; Shotguns - 20%
SWEP.HeadbobMul 			= 1
SWEP.IsSilenced 			= false
SWEP.IronsightsCone 		= 0.017
SWEP.HipCone 				= 0.045
SWEP.ConeInaccuracyAff1 = 0.5
SWEP.PlayFireAnim = true

SWEP.Secondary.Automatic	= false
SWEP.Secondary.Ammo			= "none"

SWEP.IsUsingIronsights 		= false
SWEP.TargetMul = 0
SWEP.SetAndForget			= false

SWEP.IronSightsPos = Vector( -5.79, -3.9982, 2.8289 )

SWEP.AimPos = Vector(4.335, -2.951, 2.875)
SWEP.AimAng = Vector(0.444, 0.019, 0)

SWEP.ChargePos = Vector (5.4056, -10.3522, -4.0017)
SWEP.ChargeAng = Vector (-1.7505, -55.5187, 68.8356)

SWEP.ReflexPos = Vector(4.335, -3.056, 2.553)
SWEP.ReflexAng = Vector(0, 0, 0)

SWEP.MeleePos = Vector(4.487, -1.497, -5.277)
SWEP.MeleeAng = Vector(25.629, -30.039, 26.18)

SWEP.SafePos = Vector(-0.392, -10.157, -6.064)
SWEP.SafeAng = Vector(70, 0, 0)


function SWEP:PrimaryAttack()
    self.Weapon:SetNextPrimaryFire( CurTime() + 0.5 );
    if SERVER then self.Owner:EmitSound(self.Primary.Sound) end
    self.Weapon:SendWeaponAnim( ACT_VM_PRIMARYATTACK )
    self:FireBolt()
    self:MakeAFlame()


end

function SWEP:Reload()

    self:Fly()

    return self.BaseClass.Reload(self)
end

function SWEP:Think()

    self:HealSecond()
    --return self.BaseClass.Think(self)
end

-- Think HEAL
SWEP.NextHeal = 0
function SWEP:HealSecond()

    if not SERVER or self.NextHeal > CurTime() or not IsValid(self.Owner) then return end
    self.NextHeal = CurTime() + 2

    if self.Owner:Health() < 175 then
        if SERVER then autohealfunction(self.Owner) end
    end

end

function SWEP:FireBolt()

    if ( self.Weapon:Clip1() <= 0 && self.Primary.ClipSize > -1 ) then
        if ( self:Ammo1() > 0 ) then
            self:Reload();
        else
            self.Weapon:SetNextPrimaryFire( 5 );
        end
    return;
    end
    self:ShootBullet( 50, 1, 0.01 )
    local pOwner = self.Owner;

    if ( pOwner == NULL ) then
        return;
    end

    if ( !CLIENT ) then
        local vecAiming		= pOwner:GetAimVector();
        local vecSrc		= pOwner:GetShootPos();

        local angAiming;
        angAiming = vecAiming:Angle();

        local pBolt = ents.Create ( "crossbow_bolt" );
        pBolt:SetPos( vecSrc );
        pBolt:SetAngles( angAiming );
        pBolt.Damage = self.Primary.Damage;

        pBolt.AmmoType = self.Primary.Ammo;
        pBolt:SetOwner( pOwner );
        pBolt:Spawn()

        pBolt:Ignite(30,20)

        if ( pOwner:WaterLevel() == 3 ) then
            pBolt:SetVelocity( vecAiming * BOLT_WATER_VELOCITY );
        else
            pBolt:SetVelocity( vecAiming * BOLT_AIR_VELOCITY );
        end
    end
  --  pOwner:ViewPunch( Angle( -2, 0, 0 ) );
end

-- Flame Shot
function SWEP:MakeAFlame()
    if not self.Owner:IsValid() or CLIENT then return end

    local tr = self.Owner:GetEyeTrace()
    local effectdata = EffectData()
    effectdata:SetOrigin(tr.HitPos)
    effectdata:SetNormal(tr.HitNormal)
    effectdata:SetScale(1)
    -- util.Effect("effect_mad_ignition", effectdata)
    util.Decal("FadingScorch", tr.HitPos + tr.HitNormal, tr.HitPos - tr.HitNormal)

    local tracedata = {}
    tracedata.start = tr.HitPos
    tracedata.endpos = Vector(tr.HitPos.x, tr.HitPos.y, tr.HitPos.z - 10)
    tracedata.filter = tr.HitPos
    local tracedata = util.TraceLine(tracedata)
    if SERVER then
        if tracedata.HitWorld then
            local flame = ents.Create("env_fire");
            flame:SetPos(tr.HitPos + Vector(0, 0, 1));
            flame:SetKeyValue("firesize", "10");
            flame:SetKeyValue("fireattack", "10");
            flame:SetKeyValue("StartDisabled", "0");
            flame:SetKeyValue("health", "10");
            flame:SetKeyValue("firetype", "0");
            flame:SetKeyValue("damagescale", "5");
            flame:SetKeyValue("spawnflags", "128");
            flame:SetPhysicsAttacker(self.Owner)
            flame:SetOwner(self.Owner)
            flame:Spawn();
            flame:Fire("StartFire", 0);
        end
    end
end


-- FLY
SWEP.flyammo = 5
SWEP.nextFly = 0
function SWEP:Fly( soundfile )
    if self.nextFly > CurTime() or CLIENT then return end
    self.nextFly = CurTime() + 0.30
    if self.flyammo < 1 then return end

    if not soundfile then
        if SERVER then self.Owner:EmitSound(Sound("weapons/ls/lightsaber_swing.mp3")) end
        if SERVER then self.Owner:EmitSound(Sound("player/suit_sprint.wav")) end
    else
        if SERVER then self.Owner:EmitSound(Sound(soundfile)) end
    end
    self.flyammo = self.flyammo - 1
    self.Owner:SetAnimation(PLAYER_ATTACK1)
    local power = 400
    if math.random(1,5) < 2 then power = 100
    elseif math.random(1,15) < 2 then power = -500
    elseif math.random(1,15) < 2 then power = 2500
    else power = math.random( 350,450 ) end

    if SERVER then self.Owner:SetVelocity(self.Owner:GetForward() * power + Vector(0, 0, power)) end
    timer.Simple(20, function() if IsValid(self) then self.flyammo = self.flyammo + 1 end end)
    --timer.Simple(0.2,self.SecondaryAttackDelay,self)
end