if SERVER then
    AddCSLuaFile(  );
    --resource.AddFile("sound/weapons/jihad.mp3");
    --resource.AddFile("sound/weapons/big_explosion.mp3");

end

SWEP.HoldType = "camera"
SWEP.PrintName                       = "Picture of Death"
SWEP.Slot                            = 7

if CLIENT then

    SWEP.Icon = "VGUI/ttt/icon_c4"
end

function SWEP:Initialize()
    if SERVER then
        self:SetHoldType( self.HoldType or "pistol" )
    end
    self.BaseClass.Initialize( self )
end

function SWEP:Deploy()
    if SERVER and IsValid(self.Owner) then
    self.Owner:GiveEquipmentItem( EQUIP_PROTECTIONSUIT )
    self.Owner.hasProtectionSuit = true
        end
end
SWEP.Base = "aa_base"


SWEP.ViewModel		= Model("models/weapons/v_c4.mdl")
SWEP.WorldModel		= "models/MaxOfS2D/camera.mdl"



SWEP.Kind = WEAPON_ROLE
SWEP.DrawCrosshair          = false
SWEP.ViewModelFlip          = false
SWEP.Primary.ClipSize       = -1
SWEP.Primary.DefaultClip    = -1
SWEP.Primary.Automatic      = false
SWEP.Primary.Ammo           = "none"
SWEP.Primary.Delay          = 5.0

SWEP.Secondary.ClipSize     = -1
SWEP.Secondary.DefaultClip  = -1
SWEP.Secondary.Automatic    = false
SWEP.Secondary.Ammo         = "none"

SWEP.NoSights               = true



function SWEP:Reload()
end



function SWEP:Think()
end



function SWEP:PrimaryAttack()
    self.Weapon:SetNextPrimaryFire(CurTime() + 2)

    local effectdata = EffectData()
    effectdata:SetOrigin( self.Owner:GetPos() )
    effectdata:SetNormal( self.Owner:GetPos() )
    effectdata:SetMagnitude( 16 )
    effectdata:SetScale( 1 )
    effectdata:SetRadius( 76 )
    util.Effect( "Sparks", effectdata )
    self.BaseClass.ShootEffects( self )

    -- The rest is only done on the server
    if (SERVER) then
        local owner = self.Owner
        timer.Create(owner:SteamID().."jihad",4,1, function() if IsValid(self) then self:Asplode(owner) end end )

        local TauntSound = Sound( "NPC_CScanner.TakePhoto" )
        self.Weapon:EmitSound( TauntSound )

        self.Owner:EmitSound( "asshole.mp3", 490, 100 )
    end
end


function SWEP:Asplode(owner)
    local k, v
    if !ents or !SERVER then return end
    local ent = ents.Create( "env_explosion" )
    if( IsValid( owner ) ) then
        ent:SetPos( owner:GetPos() )
        ent:SetOwner( owner )
        ent:SetKeyValue( "iMagnitude", "375" )
        ent:Spawn()
        owner:Kill( )
        self:Remove()
        ent:Fire( "Explode", 0, 0 )
        ent:EmitSound( "weapons/big_explosion.mp3", 511, 100 )
    end
end



function SWEP:SecondaryAttack()
    self.Weapon:SetNextSecondaryFire( CurTime() + 5 )
    local TauntSound = Sound( "NPC_CScanner.TakePhoto" )
    self.Weapon:EmitSound( TauntSound )

    self.Weapon:SendWeaponAnim( ACT_VM_PRIMARYATTACK )
    self.Owner:SetAnimation( PLAYER_ATTACK1 )

    if ( SERVER && !game.SinglePlayer() ) then

    --
    -- Note that the flash effect is only
    -- shown to other players!
    --

    local vPos = self.Owner:GetShootPos()
    local vForward = self.Owner:GetAimVector()

    local trace = {}
    trace.start = vPos
    trace.endpos = vPos + vForward * 256
    trace.filter = self.Owner

    local tr = util.TraceLine( trace )

    local effectdata = EffectData()
    effectdata:SetOrigin( tr.HitPos )
    util.Effect( "camera_flash", effectdata, true )

    end

    self:ShootSecondary()

end
