 if SERVER then
   AddCSLuaFile( )
end
if CLIENT then
   
   SWEP.Author 		= "GreenBlack"   

   SWEP.SlotPos 	= 1
   SWEP.Icon = "vgui/ttt_fun_killicons/golden_deagle.png"
end
SWEP.PrintName	= "Hanks Credit Card"
SWEP.Slot		= 5
SWEP.HoldType 		= "pistol"
SWEP.Base			= "aa_base"
SWEP.Spawnable 		= true
SWEP.AdminSpawnable = true
SWEP.Kind 			= WEAPON_EQUIP1
SWEP.Primary.Ammo   = "AlyxGun" 
SWEP.Primary.Recoil	= 1
SWEP.Primary.Damage = 55
SWEP.Primary.Delay 	= 0.45
SWEP.Primary.Cone 	= 0.009
SWEP.Primary.ClipSize 		= 40
SWEP.Primary.ClipMax 		= 120
SWEP.Primary.DefaultClip 	= 40
SWEP.Primary.NumShots		= 1
SWEP.Primary.Automatic 	= true
SWEP.HeadshotMultiplier = 1.5
SWEP.AutoSpawnable      = false
SWEP.AmmoEnt 		= "item_ammo_revolver_ttt"
--SWEP.Primary.Sound	= "gdeagle/gdeagle-1.mp3"
SWEP.Primary.Sound	= Sound( "Weapon_Deagle.Single" )
SWEP.Primary.SoundLevel = 400
SWEP.ViewModel= "models/weapons/v_pist_gbagle.mdl"
SWEP.WorldModel= "models/weapons/w_pist_gbagle.mdl"
SWEP.ShowWorldModel = true
SWEP.ShowViewModel = true

SWEP.LimitedStock = true
SWEP.InLoadoutFor = nil
SWEP.AllowDrop 	  = true
SWEP.IronSightsPos = Vector(1.14, -3.951, 0.736)
SWEP.IronSightsAng = Vector(0, 0, 0)
SWEP.IronSightsPos 	= Vector( 3.8, -1, 1.6 )
--SWEP.ViewModelFOV = 49


function SWEP:Deploy()
   self.Weapon:EmitSound("gdeagle/gdeagledeploy.mp3")

end

function SWEP:Reload()

    if self.Owner:KeyDown(IN_USE) then
        self.Weapon:SetNextSecondaryFire( CurTime() + 30 )

        if CLIENT or (self.Owner.NextEatMoney and self.Owner.NextEatMoney > CurTime()) then return end
        self.Owner.NextEatMoney =  CurTime() + 30
        local ent = ents.Create("eat_yo_money_money")
        self:throw_attack ("eat_yo_money_money",  "physics/glass/glass_impact_hard3.wav", 100)
        return
    end
	if self.ReloadingTime and CurTime() <= self.ReloadingTime then return end
	self:SetIronsights( false )
	if ( self:Clip1() < self.Primary.ClipSize and self.Owner:GetAmmoCount( self.Primary.Ammo ) > 0 ) then
 
	self:DefaultReload( ACT_VM_RELOAD )
	local AnimationTime = self.Owner:GetViewModel():SequenceDuration()
	self.ReloadingTime = CurTime() + AnimationTime
	self:SetNextPrimaryFire(CurTime() + AnimationTime)
	self:SetNextSecondaryFire(CurTime() + AnimationTime)
	self.Weapon:EmitSound("gdeagle/gdeaglereload.mp3")
 
	end
 
end
function SWEP:PrimaryAttack(worldsnd)

   self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
   if not self:CanPrimaryAttack() then return end
   if not worldsnd then
      self.Weapon:EmitSound( self.Primary.Sound, self.Primary.SoundLevel )
   elseif SERVER then
      WorldSound(self.Primary.Sound, self:GetPos(), self.Primary.SoundLevel)
   end
   self:ShootBullet( self.Primary.Damage, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )
   
   if ( self.Owner:IsValid() ) then
		local tr = self.Owner:GetEyeTrace()
		local effectdata = EffectData()
		effectdata:SetOrigin(tr.HitPos)
		effectdata:SetNormal(tr.HitNormal)
		effectdata:SetScale(1)
		-- util.Effect("effect_mad_ignition", effectdata)
		util.Decal("FadingScorch", tr.HitPos + tr.HitNormal, tr.HitPos - tr.HitNormal)
		
				local tracedata = {}
				tracedata.start = tr.HitPos
				tracedata.endpos = Vector(tr.HitPos.x, tr.HitPos.y, tr.HitPos.z - 10)
				tracedata.filter = tr.HitPos
				local tracedata = util.TraceLine(tracedata)
				if SERVER then
				if tracedata.HitWorld then
					local flame = ents.Create("env_fire");
					flame:SetPos(tr.HitPos + Vector( 0, 0, 1 ));
					flame:SetKeyValue("firesize", "10");
					flame:SetKeyValue("fireattack", "10");
					flame:SetKeyValue("StartDisabled", "0");
					flame:SetKeyValue("health", "10");
					flame:SetKeyValue("firetype", "0");
					flame:SetKeyValue("damagescale", "5");
					flame:SetKeyValue("spawnflags", "128");
					flame:SetPhysicsAttacker(self.Owner)
					flame:SetOwner( self.Owner )
					flame:Spawn();
					flame:Fire("StartFire",0);
				end
				end
	   self:TakePrimaryAmmo( 1 )
	   local owner = self.Owner   
	   --if not ValidEntity(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end
	   
	   owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )
   end
end

function SWEP:SecondaryAttack()

    self:Fly()
end


 function SWEP:throw_attack (model_file, sound, away)
     local tr = self.Owner:GetEyeTrace();

     if IsValid( tr.Entity )  then
         if not tr.Entity.hasProtectionSuit then
             tr.Entity.hasProtectionSuit= true
             timer.Simple(5, function()
                 if not IsValid( tr.Entity )  then return end
                 tr.Entity.hasProtectionSuit = false
             end)
         end
     end

     self.BaseClass.ShootEffects (self);
     if (!SERVER) then return end;
     local ent = ents.Create ("eat_yo_money_money");
   --  ent:SetModel (model_file);
     ent:SetPos( tr.HitPos + self.Owner:GetAimVector() * -46 )
     ent:SetAngles (self.Owner:EyeAngles());
     ent:SetPhysicsAttacker(self.Owner)
     ent:Spawn();
     ent:EmitSound( sound )
     if math.Rand(1,25) < 2 then
         ent:Ignite(100,100)
     end
     local phys = ent:GetPhysicsObject();
     local shot_length = tr.HitPos:Length();
    -- phys:ApplyForceCenter (self.Owner:GetAimVector():GetNormalized() * math.pow (shot_length, 3) * 2);

     timer.Simple(away,function()
         if IsValid(ent) then
             ent:Remove()
         end
     end)

     cleanup.Add (self.Owner, "props", ent);
     undo.Create ("Thrown model");
     undo.AddEntity (ent);
     undo.SetPlayer (self.Owner);
     undo.Finish();
 end


 local ENT = {}

 ENT.Type = "anim"
 ENT.Base = "base_anim"


 if SERVER then

     function ENT:Initialize()


         self:SetModel("models/props/cs_assault/Money.mdl")
         self:SetModelScale(self:GetModelScale()*1,0)

         self.Entity:PhysicsInit(SOLID_VPHYSICS);
         self.Entity:SetMoveType(MOVETYPE_VPHYSICS);
         self.Entity:SetSolid(SOLID_VPHYSICS);
         self.Entity:SetCollisionGroup(COLLISION_GROUP_PROJECTILE)
         self.Damage = 1
         local phys = self:GetPhysicsObject()
         if phys:IsValid() then
             phys:Wake()
             phys:EnableGravity(true);
         end

     end

     function ENT:Use( activator, caller )
         self.Entity:Remove()
         if ( activator:IsPlayer() ) then
             if activator:Health() < 175 then
                 if self.lower then
                     activator:SetHealth(activator:Health()+10)
                 else
                     activator:SetHealth(activator:Health()+25)
                 end
             end
             activator:EmitSound("crisps/eat.mp3")
         end
     end


end

 scripted_ents.Register( ENT, "eat_yo_money_money", true )