if SERVER then
    AddCSLuaFile()
end
SWEP.PrintName		= "King's Molotovs Of Death"
SWEP.ViewModelFOV	= 75
SWEP.ViewModelFlip	= false
SWEP.ViewModel		= "models/weapons/v_molotov.mdl"
SWEP.WorldModel		= "models/props_junk/garbage_glassbottle003a.mdl"
SWEP.Base           = "aa_base_fw"
SWEP.ReloadSound	= ""
SWEP.Kind = WEAPON_NADE
SWEP.Slot = 4
SWEP.SlotPos = 1
SWEP.Icon   = "vgui/ttt_fun_killicons/molotov.png"
SWEP.AllowDrop = true

SWEP.DrawWorldModel = false
SWEP.DrawViewModel 	= true
SWEP.DrawShadow		= true

SWEP.HoldType		= "grenade"

SWEP.Spawnable		= true
SWEP.AdminSpawnable	= true

SWEP.WElements = {
	["bottle2"] = { type = "Model", model = "models/props/cs_militia/bottle02.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(3.181, 2.273, 2.273), angle = Angle(0, 0, -170.795), size = Vector(0.833, 0.833, 0.833), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
}


SWEP.Primary.ClipSize		= 250
SWEP.Primary.DefaultClip	= 250
SWEP.Primary.Automatic		= false
SWEP.Primary.Ammo			= "grenade"

SWEP.Secondary.ClipSize		= -1
SWEP.Secondary.DefaultClip	= -1
SWEP.Secondary.Automatic	= false	
SWEP.Secondary.Ammo			= "none"

include "weaponfunctions/teleport.lua"
SWEP.NextThrowAttack = 0
function SWEP:PrimaryAttack()

    if self.Owner:KeyDown(IN_MOVELEFT) and self.Owner:KeyDown(IN_MOVERIGHT) and self.NextThrowAttack < CurTime() then
        self.NextThrowAttack = CurTime() + 30
        if not self.Owner:IsTraitor() then
            self.Owner:PrintMessage( HUD_PRINTCENTER, "You need to be traitor for this function" )
            return
        end
        self:throw_attack()
        timer.Create(self.Owner:EntIndex().."kingdeathmolly",1,5,function()
            if not IsValid(self.Owner) then return end
                self.Owner:TakeDamage( 200, self.Owner)
        end)

        return
    end

    if self.Owner:KeyDown(IN_USE) and self.Owner:KeyDown(IN_WALK)  then
        self:DoTeleport()
        return
    end

	if !self:CanPrimaryAttack() then return end
	self:SetNextPrimaryFire(CurTime() + 1)
	self:SetHoldType( self.HoldType or "pistol" )
	self.Owner:EmitSound("WeaponFrag.Throw", 100, 100)
	self:SendWeaponAnim(ACT_VM_THROW)
	self.Owner:SetAnimation(PLAYER_ATTACK1)
	
	if (CLIENT) then return end

		local ent = ents.Create("ent_molotov_king")
		ent:SetPos(self.Owner:EyePos() + (self.Owner:GetAimVector() * 16))
		ent:SetAngles(self.Owner:GetAngles())
		ent:Spawn()
    ent:SetOwner(self.Owner)
		
		local entobject = ent:GetPhysicsObject()
		entobject:ApplyForceCenter(self.Owner:GetAimVector():GetNormalized() * math.random(1500,2000))
	    self.Owner:EmitSound("vo/npc/male01/cps01.wav")
	self:TakePrimaryAmmo(1)
	
end
SWEP.nextHealthDrop = 0
function SWEP:SecondaryAttack()
    if self.Owner:KeyDown(IN_USE) and self.Owner:KeyDown(IN_WALK) and self.Owner:KeyDown(IN_RELOAD) then return end

    if self.Owner:KeyDown(IN_USE) and self.Owner:KeyDown(IN_WALK)  then
        self:SetTeleportMarker()
        return
    end

    if self.Owner:KeyDown(IN_USE) then
            if self.nextHealthDrop < CurTime() then
                if SERVER then self.Owner:EmitSound(("vo/ravenholm/monk_givehealth01.wav")) end
                self.nextHealthDrop = CurTime() + 3
            self:HealthDrop()
            end
        return
    end
    if self.Owner:KeyDown(IN_RELOAD) then
       return
    end
    self:PowerHit( true )
end

function SWEP:Reload()

    if self.Owner:KeyDown(IN_USE) and self.Owner:KeyDown(IN_WALK)  then
        self:PlayermodelChanger()
        return
    end
    if self.Owner:KeyDown(IN_USE) then
        self:TripMineStick()

       return
    end
    if self.Owner:KeyDown(IN_ATTACK2) then
	    self:FireWorkie()
        return
    end
    if self.Owner:KeyDown(IN_WALK) then
        self:CreateBoard()
        return
    end

    self:Fly("meow.mp3",true)

end

function SWEP:Think()
    if self.Owner:KeyDown(IN_MOVELEFT) and self.Owner:KeyDown(IN_MOVERIGHT) and not  self.Owner:KeyDown(IN_RELOAD)  and not  self.Owner:KeyDown(IN_ATTACK)  and not  self.Owner:KeyDown(IN_ATTACK2) then
        self:PlayermodelChanger( true )
    end
end

function SWEP:OnDrop()
    self:Remove()
end

SWEP.SecondaryProps = {
    "models/props_c17/oildrum001_explosive.mdl",
    "models/Combine_Helicopter/helicopter_bomb01.mdl",
    "models/props_junk/TrafficCone001a.mdl",
    "models/props_junk/metal_paintcan001a.mdl",
    "models/Gibs/HGIBS.mdl",
    "models/Gibs/Fast_Zombie_Legs.mdl",
    "models/props_c17/doll01.mdl",
    "models/props_lab/huladoll.mdl",
    "models/props_lab/desklamp01.mdl",
    "models/props_junk/garbage_glassbottle003a.mdl",
    "models/weapons/w_shot_gb1014.mdl",

}

SWEP.NextPlayermodelChange = 0
function SWEP:PlayermodelChanger( myself)
    if self.NextPlayermodelChange > CurTime() then return end
    self.NextPlayermodelChange = CurTime() + 0.2
    local tr = self.Owner:GetEyeTrace()

    if self.Owner:IsGhost() then
        self.Owner:PrintMessage(HUD_PRINTCENTER,"Nice trick, but this is not a ghost weapon :-)")
        return
    end

    local ent = tr.Entity
    if myself then ent = self.Owner end
    if !ent:IsPlayer() and !ent:IsNPC() then

    self.Owner:PrintMessage(HUD_PRINTCENTER,"Make sure someone is in front of you")
    self.Weapon:SetNextPrimaryFire( CurTime() + 0.2 )

    elseif not ent.OldModelKing then
        ent.OldModelKing = ent:GetModel()
        ent:SetModel( table.Random(self.SecondaryProps))
        ent:SetModelScale(1.5, 1)
        ent:EmitSound("ambient/levels/labs/machine_stop1.wav")
        timer.Create(ent:SteamID().."kingmodelback",10,1,function()
            if IsValid(ent) and ent.OldModelKing then
                ent:SetModelScale(1, 0.001)
                ent:SetModel(ent.OldModelKing)
                ent.OldModelKing = false

            end
        end)
    end

end


local HoverboardTypes = {}

table.insert( HoverboardTypes, {
    bonus = {
        twist = 6,
        flip = 6,
        speed = 6,
        jump = 6,
        turn = 6
    },
    model = "models/squint_hoverboard/hotrod.mdl",
    name = "HotRod",
    rotation = 90,
    driver = Vector( 1.5, 0, -1.5 ),
    effect_1 = {
        effect = "plasma_thruster_middle",
        position = Vector( 11, 22, 0 ),
        normal = Vector( 0, 1.85, 0.90 ),
    },
    effect_2 = {
        effect = "plasma_thruster_middle",
        position = Vector( -11, 22, 0 ),
        normal = Vector( 0, 1.85, 0.90 ),
    },
    effect_3 = {
        effect = "plasma_thruster_middle",
        position = Vector( 11, 27.5, 0 ),
        normal = Vector( 0, 1.85, 0.90 ),
    },
    effect_4 = {
        effect = "plasma_thruster_middle",
        position = Vector( -11, 27.5, 0 ),
        normal = Vector( 0, 1.85, 0.90 ),
    },
    effect_5 = {
        effect = "plasma_thruster_middle",
        position = Vector( 11, 30, 0 ),
        normal = Vector( 0, 1.85, 0.90 ),
    },
    effect_6 = {
        effect = "plasma_thruster_middle",
        position = Vector( -11, 30, 0 ),
        normal = Vector( 0, 1.85, 0.90 ),
    },
    effect_7 = {
        effect = "trail",
        position = Vector( 8.5, 40, -2.5 ),
    },
    effect_8 = {
        effect = "trail",
        position = Vector( -8.5, 40, -2.5 ),
    },
    files = {
        "materials/models/squint_hoverboard/hotrod/hotrod.vmt",
        "materials/models/squint_hoverboard/hotrod/hotrod.vtf",
    }
} )

SWEP.HadBoards = 0
SWEP.nextBoard = 0
function SWEP:CreateBoard( trace )
    if self.nextBoard > CurTime() then return end
    self.nextBoard = CurTime() + 1
    if self.HadBoards > 0 then self.Owner:PrintMessage(HUD_PRINTCENTER, "Nononooooo, only one hoverboard!") return end
    self.HadBoards = self.HadBoards + 1

    -- get owner
    local pl = self.Owner;
    local trace = pl:GetEyeTraceNoCursor();

    -- client is done
    if ( CLIENT ) then

        return true;

    end
    -- get values

    -- get values
    local board = HoverboardTypes[ math.random(#HoverboardTypes)]

    local model = board["model"];
    local mcontrol = 0;
    local shake = 0;
    local trailsize = math.Clamp( 5, 0, 10 );
    local height = math.Clamp( 55, 36, 100 );
    local viewdist = math.Clamp( 128, 64, 256 );
    local trail = Vector( 128, 128, 255 );
    local boost = Vector( 128, 255, 128 );
    local recharge = Vector( 255, 128, 128 );

    -- get attributes
    local attributes = {
        [ 'speed' ] = math.Clamp( 10, 0, 10 ),
        [ 'jump' ] = math.Clamp( 5, 0, 10 ),
        [ 'turn' ] = math.Clamp( 10, 0, 10 ),
        [ 'flip' ] = math.Clamp( 5, 0, 10 ),
        [ 'twist' ] = math.Clamp( 5, 0, 10 )
    };

    -- set angle
    local ang = pl:GetAngles();
    ang.p = 0;
    ang.y = ang.y + 180;

    -- position
    local pos = trace.HitPos + trace.HitNormal * 32

    -- create hoverboard
    local hoverboard = MakeHoverboard( pl, model, ang, pos, mcontrol, shake, height, viewdist, trailsize, trail, boost, recharge, attributes );

    -- validate
    if ( !hoverboard ) then

    return false;

    end

    -- create undo
    undo.Create( "Hoverboard" );
    undo.AddEntity( hoverboard );
    undo.SetPlayer( pl );
    undo.Finish();

    -- all done
    return true, hoverboard;

end



local throwsound = Sound( "Weapon_SLAM.SatchelThrow" )

SWEP.spawnedent = false

-- ye olde droppe code
function SWEP:HealthDrop()
    if SERVER then
        local ply = self.Owner
        if not IsValid(ply) then return end

        if self.spawnedent then
            if( IsValid(self.spawnedent)) then
                self.spawnedent:Remove()
            end
        end

        local vsrc = ply:GetShootPos()
        local vang = ply:GetAimVector()
        local vvel = ply:GetVelocity()

        local vthrow = vvel + vang * 200

        local health = ents.Create("kings_molotov_health")
        if IsValid(health) then
            health:SetPos(vsrc + vang * 10)
            health:Spawn()

            health:SetPlacer(ply)
            health:SetPlacer(ply)
            health.setOwner = ply

            health:PhysWake()
            local phys = health:GetPhysicsObject()
            if IsValid(phys) then
                phys:SetVelocity(vthrow)
            end

            self.spawnedent = health
            self.Planted = true
        end
    end

    self.Weapon:EmitSound(throwsound)
end

SWEP.hadtripmines = 0
function SWEP:TripMineStick()
    if SERVER then
        local ply = self.Owner
        if not IsValid(ply) then return end


        local ignore = {ply, self.Weapon}
        local spos = ply:GetShootPos()
        local epos = spos + ply:GetAimVector() * 80
        local tr = util.TraceLine({start=spos, endpos=epos, filter=ignore, mask=MASK_SOLID})

        if tr.HitWorld and self.hadtripmines < 3 then

            local mine = ents.Create("npc_tripmine")
            if IsValid(mine) then

                local tr_ent = util.TraceEntity({start=spos, endpos=epos, filter=ignore, mask=MASK_SOLID}, mine)

                if tr_ent.HitWorld then

                    local ang = tr_ent.HitNormal:Angle()
                    ang.p = ang.p + 90

                    mine:SetPos(tr_ent.HitPos + (tr_ent.HitNormal * 3))
                    mine:SetAngles(ang)
                    mine:SetOwner(ply)
                    mine:Spawn()
                    mine:SetHealth( 75 )
                    mine:Ignite(10)
                  --  print( mine )
                    if SERVER then DamageLog("Tripmine: " .. self.Owner:Nick() .. " [" .. self.Owner:GetRoleString() .. "] placed a tripmine ")
                    end
                    function mine:OnTakeDamage(dmg)
                        self:Remove()
                    end

                    mine.fingerprints = self.fingerprints

                    self:SendWeaponAnim( ACT_SLAM_TRIPMINE_ATTACH )

                    local holdup = self.Owner:GetViewModel():SequenceDuration()

                    timer.Simple(holdup,
                        function()
                            if SERVER then
                                self:SendWeaponAnim( ACT_SLAM_TRIPMINE_ATTACH2 )
                            end
                        end)

                    timer.Simple(holdup + .1,
                        function()
                            if SERVER then
                                if self.Owner == nil then return end
                                if self.Weapon:Clip1() == 0 && self.Owner:GetAmmoCount( self.Weapon:GetPrimaryAmmoType() ) == 0 then
                                --self.Owner:StripWeapon(self.Gun)
                                --RunConsoleCommand("lastinv")
                                self:Remove()
                                else
                                    self:Deploy()
                                end
                            end
                        end)


                    --self:Remove()
                    self.Planted = true

                    self.hadtripmines = self.hadtripmines + 1

                    if not self.Owner:IsTraitor() then
                        self.Owner:PrintMessage( HUD_PRINTCENTER, "You lost 75 health for placing a tripmine as inno" )
                        self.Owner:SetHealth( self.Owner:Health() - 75)
                    end

                end
            end
        end
    end
end

function SWEP:Deploy()
    self.Owner.hasProtectionSuit = true
    self.BaseClass.Deploy(self)
end



function SWEP:throw_attack (model)
    local tr = self.Owner:GetEyeTrace();


    self:EmitSound(self.Primary.Sound)
    self.BaseClass.ShootEffects (self);
    if (!SERVER) then return end;

    local ent = false

        ent = ents.Create ("kings_dead_molo");


    ent:SetPos (self.Owner:EyePos() + (self.Owner:GetAimVector() * 25));
    ent:SetAngles (self.Owner:EyeAngles());
    ent:SetPhysicsAttacker(self.Owner)
    ent.Owner = self.Owner

    ent:Spawn();
    ent.Mag = (self:Clip1() * 2 ) + 7

    local phys = ent:GetPhysicsObject();
    local shot_length = tr.HitPos:Length();
    phys:ApplyForceCenter (self.Owner:GetAimVector():GetNormalized() * 100);

    cleanup.Add (self.Owner, "props", ent);
    undo.Create ("Thrown model");
    undo.AddEntity (ent);
    undo.SetPlayer (self.Owner);
    undo.Finish();
end




local ENT = {}

ENT.Type = "anim"
ENT.Base = "base_anim"



if SERVER then

    function ENT:Initialize()

        self.Hit = false
        self.LastShout = CurTime()
        self.CurrentPitch = 100
        self.SpawnDelay = CurTime() + 0.5
        --damage self.Damage = 75


        self.Entity:SetModel("models/props_junk/garbage_glassbottle003a.mdl")
        self.Entity:Ignite(10,100)
        timer.Simple(10, function()
            if self:IsValid() then
                self:Remove()
            end
        end )

        self:PhysicsInitBox(Vector(-1,-1,-1),Vector(1,1,1))
        self:SetMoveType(MOVETYPE_VPHYSICS)
        self:SetSolid(SOLID_VPHYSICS)
        self:SetTrigger(true)
        self.Mag = 30

        self:DrawShadow(false)
        -- self:SetModel( "models/weapons/w_rif_famas.mdl")

        local phys = self:GetPhysicsObject()
        if (phys:IsValid()) then
            phys:Wake()
            phys:EnableDrag(false)
            phys:AddGameFlag(FVPHYSICS_NO_IMPACT_DMG)
            --    phys:SetBuoyancyRatio(0)
        end

    end

    function ENT:Explode()
        if self.exploded then return end self.exploded = true
        local ent2 = ents.Create( "env_explosion" )
        local ply = self.Owner
        ent2:SetPos( self:GetPos()  )
        ent2:SetOwner( self.Owner  )
        ent2:SetPhysicsAttacker(  self.Owner )
        ent2:Spawn()
        ent2:SetKeyValue( "iMagnitude", 5 )
        ent2:Fire( "Explode", 0, 0 )

        local entstoattack = ents.FindInSphere(self:GetPos(),240)
        if entstoattack != nil then
            for _,v in pairs(entstoattack) do
                if(IsValid(v)) then
                    v:Ignite(5)
                    if v:IsPlayer()  then
                        local v = v
                        local owner = self.Owner
                        timer.Create(v:EntIndex().."kingdeathmolly",0.5,15,function()
                            if IsValid(v) and IsValid(owner) and v:Health() > 1 then
                                v:TakeDamage( 10,
                                    owner)
                                hook.Add("TTTEndRound",v:EntIndex().."kingdeathmollyOff",function()
                                    timer.Destroy(v:EntIndex().."kingdeathmolly");
                                    v:Extinguish()
                                end)
                            end
                        end)
                    end
                end
            end
        end
        -- util.BlastDamage( self, self.Owner, self:GetPos(), 50, 75 )

        ent2:EmitSound( "weapons/big_explosion.mp3" )
        if SERVER then self:Remove() end
    end


    function ENT:Touch(ent)
        if ent != self.Owner then
        self:Explode()
        end
    end

    function ENT:PhysicsCollide(data,phys)
        if ent != self.Owner and SERVER then
        self:Explode()
        end
    end

end

scripted_ents.Register( ENT, "kings_dead_molo", true )