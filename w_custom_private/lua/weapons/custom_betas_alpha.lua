if SERVER then
    AddCSLuaFile()
    CreateConVar("grapple_distance", 10000, false)
	--resource.AddFile("materials/vgui/ttt_fun_killicons/spidergun.png")
end

SWEP.Spawnable			= true
SWEP.AdminSpawnable		= false
SWEP.CanDecapitate= true
SWEP.HasFireBullet = true
SWEP.Primary.Sound = Sound( "weapons/usp/usp1.wav" )
SWEP.Primary.SoundLevel = 50
SWEP.PrintName			= "Beta's Alpha"
	SWEP.Slot				= 2
	SWEP.SlotPos = 9
	SWEP.Icon = "vgui/ttt_fun_killicons/spidergun.png"

SWEP.Primary.Delay		= 0.15
SWEP.Primary.ClipSize		= 60
SWEP.Primary.DefaultClip	= 60
SWEP.Primary.Automatic		= true
SWEP.Primary.Damage = 18
SWEP.Primary.Ammo		= "Battery"
SWEP.UseHands = true

SWEP.DrawAmmo			= true
SWEP.DrawCrosshair		= true
SWEP.ViewModelFOV = 71
SWEP.ViewModelFlip = true
SWEP.ViewModel = "models/weapons/v_snip_sg550.mdl"
SWEP.WorldModel = "models/weapons/w_snip_sg550.mdl"


SWEP.Kind = WEAPON_HEAVY
SWEP.Base   = "aa_base"
SWEP.Camo = 10


function SWEP:GetIronsights( )

	return false

end

if CLIENT then


    function SWEP:CustomAmmoDisplay()

        self.AmmoDisplay = self.AmmoDisplay or {}
                --attempt to remove ammo display
        self.AmmoDisplay.Draw = false

        self.AmmoDisplay.PrimaryClip 	= 1
        self.AmmoDisplay.PrimaryAmmo 	= -1
        self.AmmoDisplay.SecondaryAmmo 	= -1

        return self.AmmoDisplay

    end

    function SWEP:SetHoldType( t )
        -- Just a fake function so we can define
        -- weapon holds in shared files without errors
    end
end



local sndPowerUp		= Sound("rope_hit.mp3")
local sndPowerDown		= Sound ("shoot_rope.mp3")
local sndTooFar			= Sound ("to_far.mp3")

function SWEP:Initialize()

	nextshottime = CurTime()

end


SWEP.NextAmmoGive = 0
SWEP.NextAmmoTake = 0

function SWEP:Think()

    if self.NextAmmoGive < CurTime() && self:Clip1() < 10 then
    self.NextAmmoGive = CurTime() + 3
    self:SetClip1( self:Clip1() + 1 )
    end

	if (!self.Owner || self.Owner == NULL) or self:Clip1() < 1 then

        if ( !self.Beam ) then return end
        if( IsValid( self.Beam) ) then
            self.Beam:Remove()
        end
        self.Beam = nil
        return
    end



	if ( self.Owner:KeyPressed( IN_ATTACK2 ) ) then

		self:StartAttack()

	elseif ( self.Owner:KeyDown( IN_ATTACK2 )  ) then

		self:UpdateAttack()
        self.Weapon:SetNextSecondaryFire( CurTime() + 1 )

	elseif ( self.Owner:KeyReleased( IN_ATTACK2 )  ) then

		self:EndAttack( true )

	end



end

function SWEP:DoTrace( endpos )
	local trace = {}
		trace.start = self.Owner:GetShootPos()
		trace.endpos = trace.start + (self.Owner:GetAimVector() * 14096) --14096 is length modifier.
		if(endpos) then trace.endpos = (endpos - self.Tr.HitNormal * 7) end
		trace.filter = { self.Owner, self.Weapon }

	self.Tr = nil
	self.Tr = util.TraceLine( trace )
end

SWEP.nextreload = 0
function SWEP:Reload()
    if self.Owner:KeyDown(IN_USE) and self.nextreload < CurTime() then
        self.nextreload = CurTime() + 3
       self:HealthDrop()
        return
    end
    if  self.nextreload < CurTime() then
    self:ShootBullet( 5, 15, 0.085 )
    self.nextreload = CurTime() + 0.5
    self:MakeAFlame()
        end
end


function SWEP:StartAttack()


        if not self:CanPrimaryAttack() then return end

	--Get begining and end poins of trace.
	local gunPos = self.Owner:GetShootPos() --Start of distance trace.
	local disTrace = self.Owner:GetEyeTrace() --Store all results of a trace in disTrace.
	local hitPos = disTrace.HitPos --Stores Hit Position of disTrace.

	--Calculate Distance
	--Thanks to rgovostes for this code.
	local x = (gunPos.x - hitPos.x)^2;
	local y = (gunPos.y - hitPos.y)^2;
	local z = (gunPos.z - hitPos.z)^2;
	local distance = math.sqrt(x + y + z);

	--Only latches if distance is less than distance CVAR
	local distanceCvar = GetConVarNumber("grapple_distance")
	inRange = false
	if distance <= distanceCvar then
		inRange = true
	end

	if inRange then
		if (SERVER) then

			if (!self.Beam) then --If the beam does not exist, draw the beam.
                            if not self:CanPrimaryAttack() then return end
				--grapple_beam
				self.Beam = ents.Create( "trace1" )
					self.Beam:SetPos( self.Owner:GetShootPos() )
				self.Beam:Spawn()
            self:TakePrimaryAmmo(1)

			end

			self.Beam:SetParent( self.Owner )
			self.Beam:SetOwner( self.Owner )

		end


		self:DoTrace()
		self.speed = 10000 --Rope latch speed. Was 3000.
		self.startTime = CurTime()
		self.endTime = CurTime() + self.speed
		self.dtfix = -1

		if (SERVER && self.Beam) then
			self.Beam:GetTable():SetEndPos( self.Tr.HitPos )
		end

		self:UpdateAttack()

		self.Weapon:EmitSound( sndPowerDown )
	else
		--Play A Sound
		self.Weapon:EmitSound( sndTooFar )
	end
end

function SWEP:UpdateAttack()

       -- if not self:CanPrimaryAttack() then return end
    if !self.Tr or self:Clip1() < 1 then return end
        self.Owner:LagCompensation( true )


	if (!endpos) then endpos = self.Tr.HitPos end

	if (SERVER && self.Beam) then
		self.Beam:GetTable():SetEndPos( endpos )
	end

	lastpos = endpos



			if ( self.Tr.Entity:IsValid() ) then

					endpos = self.Tr.Entity:GetPos()
					if ( SERVER and IsValid(self.Beam) ) then
					self.Beam:GetTable():SetEndPos( endpos )
					end

			end

			local vVel = (endpos - self.Owner:GetPos())
			local Distance = endpos:Distance(self.Owner:GetPos())

			local et = (self.startTime + (Distance/self.speed))
			if(self.dtfix != 0) then
				self.dtfix = (et - CurTime()) / (et - self.startTime)
			end
			if(self.dtfix < 0) then
				self.Weapon:EmitSound( sndPowerUp )
				self.dtfix = 0
			end

			if(self.dtfix == 0) then
			zVel = self.Owner:GetVelocity().z
			vVel = vVel:GetNormalized()*(math.Clamp(Distance,0,25))
				if( SERVER ) then
                    if  self.NextAmmoTake < CurTime() then
                        self:TakePrimaryAmmo(1)
                        self.NextAmmoTake = CurTime() + 1
                        self.NextAmmoGive = CurTime() + 1.2
                    end

				local gravity = GetConVarNumber("sv_gravity")

                                    vVel:Add(Vector(0,0,(gravity/50)*1.5))

				if(zVel < 0) then
					vVel:Sub(Vector(0,0,zVel/100))
				end
				self.Owner:SetVelocity(vVel)
				end
			end

	endpos = nil

	self.Owner:LagCompensation( false )

end

function SWEP:EndAttack( shutdownsound )

	if ( shutdownsound ) then
		self.Weapon:EmitSound( sndPowerDown )
	end

	if ( CLIENT ) then return end
	if ( !self.Beam ) then return end
	if( IsValid( self.Beam) ) then
	  self.Beam:Remove()
	end
	self.Beam = nil

end


function SWEP:Holster()
	self:EndAttack( false )
	return true
end

function SWEP:OnRemove()
	self:EndAttack( false )
	return true
end


function SWEP:SecondaryAttack()
end

function SWEP:PrimaryAttack()
    if self.Owner:KeyDown(IN_USE) then
        self:Zoom()
        return
    end
self.Weapon:SetNextPrimaryFire( CurTime() + 0.15 )
self:ShootBullet( 20, 1, 0.01)
self:MakeAFlame()
end

function SWEP:OnDrop()
    self:Remove()
end
function SWEP:PreDrop()
    self:SetZoom(false)
    self:SetIronsights(false)
    return self.BaseClass.PreDrop(self)
end



function SWEP:Holster()
    self:SetIronsights(false)
    self:SetZoom(false)
    return true
end
function SWEP:ShootBullet( damage, num_bullets, aimcone )

local bullet = {}
bullet.Num 		= num_bullets
bullet.Src 		= self.Owner:GetShootPos()	-- Source
bullet.Dir 		= self.Owner:GetAimVector()	-- Dir of bullet
bullet.Spread 	= Vector( aimcone, aimcone, 0 )		-- Aim Cone
bullet.Tracer	= 5	-- Show a tracer on every x bullets
bullet.TracerName = "Tracer" -- what Tracer Effect should be used
bullet.Force	= 1	-- Amount of force to give to phys objects
bullet.Damage	= damage
bullet.AmmoType = "Pistol"

self.Owner:FireBullets( bullet )
self.Weapon:EmitSound( self.Primary.Sound  )

self:ShootEffects()

end


function SWEP:DropHealth()
    if self.NextHealthDrop > CurTime() then return end
    self.NextHealthDrop = CurTime() + 5
    local healttt = 200
    if self.healththing and SERVER then
        healttt = Entity(self.healththing):GetStoredHealth()
        Entity(self.healththing):Remove()
    end
    self:HealthDrop(healttt)
end


SWEP.NextHealthDrop = 0

local throwsound = Sound( "Weapon_SLAM.SatchelThrow" )

function SWEP:HealthDrop( healttt)
    if SERVER then
        local ply = self.Owner
        if not IsValid(ply) then return end

        local vsrc = ply:GetShootPos()
        local vang = ply:GetAimVector()
        local vvel = ply:GetVelocity()

        local vthrow = vvel + vang * 200

        local health = ents.Create("normal_health_station")
        if IsValid(health) then
            health:SetPos(vsrc + vang * 10)
            health:Spawn()
            --health:SetModel( "models/props_lab/cactus.mdl")
            health:SetModelScale(health:GetModelScale()*1,0)
            health:SetPlacer(ply)
            --health.healsound = "ginger_cartman.mp3"

            health:PhysWake()
            local phys = health:GetPhysicsObject()
            if IsValid(phys) then
                phys:SetVelocity(vthrow)
            end
            --  health:SetModel( "models/props_lab/cactus.mdl")
            health:SetStoredHealth(200)
            health.setOwner = self.Owner

            self.healththing = health:EntIndex()

            self.Planted = true
        end
    end

    self.Weapon:EmitSound(throwsound)
end

SWEP.IronSightsPos      = Vector( -100, -100, -100 )
SWEP.IronSightsAng      = Vector( 2.6, 1.37, 3.5 )
SWEP.Secondary.Sound = Sound("Default.Zoom")
-- Add some zoom to ironsights for this gun
function SWEP:Zoom()
    if not self.IronSightsPos then return end
    if self.Weapon:GetNextSecondaryFire() > CurTime() then return end

    local bIronsights = not self:GetIronsights()

    self:SetIronsights( bIronsights )

    if SERVER then
        self:SetZoom(bIronsights)
    else
        self:EmitSound(self.Secondary.Sound)
    end

    self.Weapon:SetNextSecondaryFire( CurTime() + 0.3)
end




function SWEP:SetZoom(state)
    if CLIENT then
        return
    elseif IsValid(self.Owner) and self.Owner:IsPlayer() then
        if state then
            self.Owner:SetFOV(30, 0.3)
        else
            self.Owner:SetFOV(0, 0.2)
        end
    end
end

if CLIENT then
    local scope = surface.GetTextureID("sprites/scope")
    function SWEP:DrawHUD()
        if self:GetIronsights() then
            surface.SetDrawColor( 0, 0, 0, 255 )

            local x = ScrW() / 2.0
            local y = ScrH() / 2.0
            local scope_size = ScrH()

            -- crosshair
            local gap = 80
            local length = scope_size
            surface.DrawLine( x - length, y, x - gap, y )
            surface.DrawLine( x + length, y, x + gap, y )
            surface.DrawLine( x, y - length, x, y - gap )
            surface.DrawLine( x, y + length, x, y + gap )

            gap = 0
            length = 50
            surface.DrawLine( x - length, y, x - gap, y )
            surface.DrawLine( x + length, y, x + gap, y )
            surface.DrawLine( x, y - length, x, y - gap )
            surface.DrawLine( x, y + length, x, y + gap )


            -- cover edges
            local sh = scope_size / 2
            local w = (x - sh) + 2
            surface.DrawRect(0, 0, w, scope_size)
            surface.DrawRect(x + sh - 2, 0, w, scope_size)

            surface.SetDrawColor(255, 0, 0, 255)
            surface.DrawLine(x, y, x + 1, y + 1)

            -- scope
            surface.SetTexture(scope)
            surface.SetDrawColor(255, 255, 255, 255)

            surface.DrawTexturedRectRotated(x, y, scope_size, scope_size, 0)

        else
            return self.BaseClass.DrawHUD(self)
        end
    end

    function SWEP:AdjustMouseSensitivity()
        return (self:GetIronsights() and 0.2) or nil
    end
end
