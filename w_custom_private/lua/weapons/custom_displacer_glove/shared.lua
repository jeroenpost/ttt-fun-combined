if( SERVER ) then
	AddCSLuaFile(  )
	--resource.AddFile("materials/vgui/ttt/fists.png")
end


	SWEP.PrintName = "Displacer Glove"
	SWEP.DrawAmmo = false
	SWEP.DrawCrosshair = false
	
	


SWEP.Base = "gb_base"
SWEP.Kind = WEAPON_UNARMED

SWEP.CanBuy = nil -- no longer a buyable thing
--SWEP.LimitedStock = true

SWEP.AllowDrop = false
SWEP.Icon = "vgui/ttt/fists.png"

SWEP.Author			= "robotboy655"
SWEP.Purpose		= "Well we sure as heck didn't use guns! We would wrestle Hunters to the ground with our bare hands! I would get ten, twenty a day, just using my fists."

SWEP.Spawnable			= true
SWEP.UseHands			= true

SWEP.ViewModel			= "models/weapons/c_arms_cstrike.mdl"

SWEP.WorldModel			= ""
SWEP.DrawWorldModel = false
--SWEP.ViewModel      = "models/weapons/v_punch.mdl"
--SWEP.WorldModel   = "models/weapons/w_fists_t.mdl"

SWEP.ViewModelFOV		= 35

SWEP.Primary.ClipSize		= 10
SWEP.Primary.DefaultClip	= 10
SWEP.Primary.Damage			= 0
SWEP.Primary.Automatic		= true
SWEP.Primary.Ammo			= "none"
SWEP.HeadshotMultiplier = 1

SWEP.Secondary.ClipSize		= 1
SWEP.Secondary.DefaultClip	= 1
SWEP.Secondary.Damage			= 0
SWEP.Secondary.Automatic	= true
SWEP.Secondary.Ammo			= "Battery"

SWEP.Weight				= 5
SWEP.AutoSwitchTo		= false
SWEP.AutoSwitchFrom		= false


SWEP.Slot				= 5
SWEP.SlotPos			= 5
SWEP.DrawAmmo			= false
SWEP.DrawCrosshair		= true
SWEP.secondUsed                 = 0


local SwingSound = Sound( "weapons/slam/throw.wav" )
local HitSound = Sound( "Flesh.ImpactHard" )

function SWEP:Initialize()

	self:SetHoldType( "fist" )

end

function SWEP:PreDrawViewModel( vm, wep, ply )

	--vm:SetMaterial( "engine/occlusionproxy" ) -- Hide that view model with hacky material

end


SWEP.damage = 0

SWEP.Distance = 120
SWEP.AttackAnims = { "fists_left", "fists_right", "fists_uppercut" }
SWEP.AttackAnimsRight = { "fists_right", "fists_uppercut" }
function SWEP:PrimaryAttack()
    if  self:Clip1() > 0 then
        self.damage = 75
        self:AttackThing( "fists_right" )
    else
        self.damage = 25
        self:AttackThing( "fists_left" )
    end

end

function SWEP:SecondaryAttack()
    self.damage = 0
    self:AttackThing( self.AttackAnimsRight[ math.random( 1, #self.AttackAnimsRight ) ] )
end


function SWEP:DealDamage( anim )
	local tr = util.TraceLine( {
		start = self.Owner:GetShootPos(),
		endpos = self.Owner:GetShootPos() + self.Owner:GetAimVector() * self.Distance,
		filter = self.Owner
	} )

	if ( not IsValid( tr.Entity ) ) then 
		tr = util.TraceHull( {
			start = self.Owner:GetShootPos(),
			endpos = self.Owner:GetShootPos() + self.Owner:GetAimVector() * self.Distance,
			filter = self.Owner,
			mins = self.Owner:OBBMins() / 3,
			maxs = self.Owner:OBBMaxs() / 3
		} )
	end

	if ( tr.Hit ) then self.Owner:EmitSound( HitSound ) end

	if ( IsValid( tr.Entity ) ) then

                tr.Entity:SetVelocity(self.Owner:GetForward() * 6000 + Vector(0,0,400))
                tr.Entity.hasProtectionSuitTemp = true
                if SERVER and tr.Entity:IsPlayer() then
                    tr.Entity.IsBlinded = true
                    tr.Entity:SetNWBool("isblinded",true)
                    umsg.Start( "ulx_blind", tr.Entity )
                    umsg.Bool( true )
                    umsg.Short( 255 )
                    umsg.End()

                    timer.Create("ResetPLayerAfterBlided".. tr.Entity:UniqueID(), 3,1, function()
                        if not IsValid( tr.Entity)  then  return end

                        if SERVER then
                            tr.Entity:SetNWBool("isblinded",true)
                            tr.Entity.IsBlinded = false
                            umsg.Start( "ulx_blind",  tr.Entity )
                            umsg.Bool( false )
                            umsg.Short( 0 )
                            umsg.End()
                        end
                    end)
                    self:TakePrimaryAmmo(1)
                end

               
                    bullet = {}
                    bullet.Num    = 1
                    bullet.Src    = self.Owner:GetShootPos()
                    bullet.Dir    = self.Owner:GetAimVector()
                    bullet.Spread = Vector(0, 0, 0)
                    bullet.Tracer = 0
                    bullet.Force  = 100000000
                    bullet.Damage =  self.damage
           -- self.Owner:FireBullets(bullet)
    --        	self.Owner:LagCompensation(true)
	--self.Owner:LagCompensation(false)


		local dmginfo = DamageInfo()
		dmginfo:SetDamage( self.Primary.Damage )
		if ( anim == "fists_left" ) then
			dmginfo:SetDamageForce( self.Owner:GetRight() * 49125 + self.Owner:GetForward() * 99984 ) -- Yes we need those specific numbers
                       -- dmginfo:SetDamage( self.Secondary.Damage )
                        

                        self.secondUsed = self.secondUsed + 1
		elseif ( anim == "fists_right" ) then
			dmginfo:SetDamageForce( self.Owner:GetRight() * -49124 + self.Owner:GetForward() * 99899 )
                       -- dmginfo:SetDamage( self.Primary.Damage )
		elseif ( anim == "fists_uppercut" ) then
			dmginfo:SetDamageForce( self.Owner:GetUp() * 51589 + self.Owner:GetForward() * 100128 )
                       -- dmginfo:SetDamage( self.Primary.Damage )
		end
                
		dmginfo:SetInflictor( self )
		local attacker = self.Owner
		if ( !IsValid( attacker ) ) then attacker = self end
		dmginfo:SetAttacker( attacker )

        --if not tr.Entity:IsPlayer() then
                dmginfo:SetDamage( self.damage )
		  tr.Entity:TakeDamageInfo( dmginfo )
       -- end
        end
end



function SWEP:AttackThing( anim )
   self.Owner:SetAnimation( PLAYER_ATTACK1 )
   
   
    
	if ( !SERVER ) then return end
   if !IsValid(self.Owner) then return end
	-- We need this because attack sequences won't work otherwise in multiplayer
	local vm = self.Owner:GetViewModel()
	vm:ResetSequence( vm:LookupSequence( "fists_idle_01" ) )

	--local anim = "fists_left" --self.AttackAnims[ math.random( 1, #self.AttackAnims ) ]

	timer.Simple( 0, function()
		if ( !IsValid( self ) || !IsValid( self.Owner ) || !self.Owner:GetActiveWeapon() || self.Owner:GetActiveWeapon() != self ) then return end
	
		local vm = self.Owner:GetViewModel()
		vm:ResetSequence( vm:LookupSequence( anim ) )

		self:Idle()
	end )

	timer.Simple( 0.05, function()
		if ( !IsValid( self ) || !IsValid( self.Owner ) || !self.Owner:GetActiveWeapon() || self.Owner:GetActiveWeapon() != self ) then return end
		if ( anim == "fists_left" ) then
			--self.Owner:ViewPunch( Angle( 0, 16, 0 ) )
		elseif ( anim == "fists_right" ) then
			--self.Owner:ViewPunch( Angle( 0, -16, 0 ) )
		elseif ( anim == "fists_uppercut" ) then
			--self.Owner:ViewPunch( Angle( 16, -8, 0 ) )
		end
	end )

	timer.Simple( 0.2, function()
		if ( !IsValid( self ) || !IsValid( self.Owner ) || !self.Owner:GetActiveWeapon() || self.Owner:GetActiveWeapon() != self ) then return end
		if ( anim == "fists_left" ) then
			--self.Owner:ViewPunch( Angle( 4, -16, 0 ) )
		elseif ( anim == "fists_right" ) then
			--self.Owner:ViewPunch( Angle( 4, 16, 0 ) )
		elseif ( anim == "fists_uppercut" ) then
			--self.Owner:ViewPunch( Angle( -32, 0, 0 ) )
		end
		self.Owner:EmitSound( SwingSound )
		
	end )

	timer.Simple( 0.2, function()
		if ( !IsValid( self ) || !IsValid( self.Owner ) || !self.Owner:GetActiveWeapon() || self.Owner:GetActiveWeapon() != self ) then return end
		self:DealDamage( anim )
	end )

	self.Weapon:SetNextSecondaryFire(CurTime() + 0.6)
    self.Weapon:SetNextPrimaryFire(CurTime() + 0.6)



end

function SWEP:Idle()
    if !IsValid(self.Owner) then return end
	local vm = self.Owner:GetViewModel()
	timer.Create( "fists_idle" .. self:EntIndex(), vm:SequenceDuration(), 1, function()
		vm:ResetSequence( vm:LookupSequence( "fists_idle_0" .. math.random( 1, 2 ) ) )
	end )

end



function SWEP:OnRemove()

	--if ( IsValid( self.Owner ) ) then
	--	local vm = self.Owner:GetViewModel()
		--vm:SetMaterial( "" )
	--end

	timer.Stop( "fists_idle" .. self:EntIndex() )

end

function SWEP:Holster( wep )

	--if ( IsValid( self.Owner ) ) then
		--local vm = self.Owner:GetViewModel()
		--vm:SetMaterial( "" )
	--end

	timer.Stop( "fists_idle" .. self:EntIndex() )

	return true
end

function SWEP:Deploy()
    if !IsValid(self.Owner) then return end
    local vm = self.Owner:GetViewModel()
	vm:ResetSequence( vm:LookupSequence( "fists_draw" ) )

	self:Idle()

	return true
end

SWEP.NextReload = 0 

