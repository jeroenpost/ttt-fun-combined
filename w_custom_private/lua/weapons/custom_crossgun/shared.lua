

if ( SERVER ) then

	AddCSLuaFile(  )
--resource.AddFile("sound/weapons/m3/s3-1.mp3")
--resource.AddFile("sound/weapons/m3/s3_insertshell.mp3")
--resource.AddFile("sound/weapons/m3/s3_pump.mp3")
--resource.AddFile("models/weapons/w_shot_g3super90.mdl")
--resource.AddFile("models/weapons/v_shot_g3super90.mdl")
--resource.AddFile("materials/models/weapons/v_models/emess-gb/glowingstuff.vmt")
--resource.AddFile("materials/models/weapons/v_models/emess-gb/glass.vmt")
--resource.AddFile("materials/models/weapons/v_models/emess-gb/body.vmt")
--resource.AddFile("materials/models/weapons/w_models/w_emess-gb/w_body.vmt")
--resource.AddFile("materials/vgui/ttt_fun_killicons/emsss.png")


  
	
end
	SWEP.PrintName			= "CrossGun"
if ( CLIENT ) then

	
	SWEP.Author				= "Babel Industries (Model by Defuse)"
    SWEP.Contact            = "";
    SWEP.Instructions       = "Electro Magnetic Shelless Slug Shotgun"
	SWEP.Category = "Babel Industries"


	
end


SWEP.Base				= "gb_base_shotgun"
SWEP.Spawnable			= true
SWEP.AdminSpawnable		= false
SWEP.Icon 				= "vgui/ttt_fun_killicons/emsss.png"
SWEP.HoldType = "shotgun"
SWEP.ViewModelFOV = 71
SWEP.ViewModelFlip = false
SWEP.ViewModel = "models/weapons/v_shotgun.mdl"
SWEP.WorldModel = "models/weapons/w_shotgun.mdl"
SWEP.ShowViewModel = true
SWEP.ShowWorldModel = true
SWEP.ViewModelBoneMods = {}

function SWEP:Deploy()
   -- self.Owner:EmitSound( "weapons/barret_arm_draw.mp3" ) ;
	self.Weapon:SendWeaponAnim( ACT_VM_DRAW )
	self:SetHoldType( self.HoldType or "pistol" )
    self:SetDeploySpeed( self.Weapon:SequenceDuration(0.2) )
	return true;
end 


SWEP.Weight				= 5
SWEP.AutoSwitchTo		= false
SWEP.AutoSwitchFrom		= false
SWEP.AutoSpawnable 		= false
SWEP.Kind				= WEAPON_HEAVY
SWEP.Primary.Sound			= Sound("weapons/mp7/mp7_fire.mp3")
SWEP.Primary.SoundLevel = 10
SWEP.Primary.Recoil			= 1
SWEP.Primary.Damage			= 15
SWEP.Primary.NumShots		= 16
SWEP.Primary.Cone			= 0.035
SWEP.Primary.ClipSize		= 12
SWEP.Primary.Delay			= 0.9
SWEP.Primary.DefaultClip	= 50
SWEP.Primary.Automatic		= true
SWEP.Primary.Ammo			= "buckshot"
SWEP.AmmoEnt = "item_box_buckshot_ttt"

SWEP.Slot				= 2							// Slot in the weapon selection menu
SWEP.SlotPos			= 1							// Position in the slot

SWEP.Secondary.ClipSize		= -1
SWEP.Secondary.DefaultClip	= -1
SWEP.Secondary.Automatic	= false
SWEP.Secondary.Ammo			= "none"

SWEP.IronSightsPos = Vector(3.046, -4.427, 0.834)
SWEP.IronSightsAng = Vector(0, 0, 0)

SWEP.RunArmOffset  = Vector(-0.601, -3.935, -1.461)
SWEP.RunArmAngle 	 = Vector(8.032, -41.885, 0)

function SWEP:IsEquipment() 
	return false
end

BOLT_AIR_VELOCITY	= 3500
BOLT_WATER_VELOCITY	= 1500
BOLT_SKIN_NORMAL	= 0
BOLT_SKIN_GLOW		= 1

CROSSBOW_GLOW_SPRITE	= "sprites/light_glow02_noz.vmt"
CROSSBOW_GLOW_SPRITE2	= "sprites/blueflare1.vmt"


SWEP.PrimaryAnim = ACT_VM_PRIMARYATTACK_SILENCED
SWEP.ReloadAnim = ACT_VM_RELOAD_SILENCED

function SWEP:OnDrop()
    self:Remove()
end

function SWEP:Precache()

    util.PrecacheSound( "Weapon_Crossbow.BoltHitBody" );
    util.PrecacheSound( "Weapon_Crossbow.BoltHitWorld" );
    util.PrecacheSound( "Weapon_Crossbow.BoltSkewer" );

    util.PrecacheModel( CROSSBOW_GLOW_SPRITE );
    util.PrecacheModel( CROSSBOW_GLOW_SPRITE2 );

    self.BaseClass:Precache();

end

function SWEP:PrimaryAttack()
    if ( !self:CanPrimaryAttack() ) then return end


    self:FireBolt(0);
    timer.Simple(0.00001,function() self:FireBolt2(1); end)
    timer.Simple(0.00001,function() self:FireBolt3(1); end)
    timer.Simple(0.00001,function() self:FireBolt4(1); end)
    timer.Simple(0.00001,function() self:FireBolt5(1); end)



    // Signal a reload
    self.m_bMustReload = true;
    self:TakePrimaryAmmo( 1 );

   

    self.Weapon:EmitSound( self.Primary.Sound );
    -- self.Owner:EmitSound( self.Primary.Special2 );

    self.Weapon:SendWeaponAnim( ACT_VM_PRIMARYATTACK );

    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay );
    self.Weapon:SetNextSecondaryFire( CurTime() + self.Primary.Delay );

    // self:DoLoadEffect();
    // self:SetChargerState( CHARGER_STATE_DISCHARGE );

end

function SWEP:Deploy()
    self.Weapon:SendWeaponAnim(ACT_VM_DRAW_SILENCED)
    return true
end

function SWEP:SetZoom(state)
    if CLIENT or not IsValid(self.Owner) then
        return
    else
        if state then
            self.Owner:SetFOV(20, 0.3)
        else
            self.Owner:SetFOV(0, 0.2)
        end
    end
end



function SWEP:PreDrop()
    self:SetIronsights( false )
    self:SetZoom(false)
end

function SWEP:Reload()
    self.Weapon:DefaultReload( ACT_VM_RELOAD );
    self.Owner:SetAnimation(ACT_RELOAD_PISTOL)
    self.Weapon:SendWeaponAnim(ACT_SHOTGUN_RELOAD_FINISH)
    self:SetIronsights( false )
    self:SetZoom(false)
end


function SWEP:Holster()
    self:SetIronsights(false)
    self:SetZoom(false)
    return true
end

if CLIENT then
    local scope = surface.GetTextureID("sprites/scope")
    function SWEP:DrawHUD()
        if self:GetIronsights() then
            surface.SetDrawColor( 0, 0, 0, 255 )

            local x = ScrW() / 2.0
            local y = ScrH() / 2.0
            local scope_size = ScrH()

            -- crosshair
            local gap = 80
            local length = scope_size
            surface.DrawLine( x - length, y, x - gap, y )
            surface.DrawLine( x + length, y, x + gap, y )
            surface.DrawLine( x, y - length, x, y - gap )
            surface.DrawLine( x, y + length, x, y + gap )

            gap = 0
            length = 50
            surface.DrawLine( x - length, y, x - gap, y )
            surface.DrawLine( x + length, y, x + gap, y )
            surface.DrawLine( x, y - length, x, y - gap )
            surface.DrawLine( x, y + length, x, y + gap )


            -- cover edges
            local sh = scope_size / 2
            local w = (x - sh) + 2
            surface.DrawRect(0, 0, w, scope_size)
            surface.DrawRect(x + sh - 2, 0, w, scope_size)

            surface.SetDrawColor(255, 0, 0, 255)
            surface.DrawLine(x, y, x + 1, y + 1)

            -- scope
            surface.SetTexture(scope)
            surface.SetDrawColor(255, 255, 255, 255)

            surface.DrawTexturedRectRotated(x, y, scope_size, scope_size, 0)

        else
            return self.BaseClass.DrawHUD(self)
        end
    end

    function SWEP:AdjustMouseSensitivity()
        return (self:GetIronsights() and 0.2) or nil
    end
end

function SWEP:FireBolt( con )

    if ( self.Weapon:Clip1() <= 0 ) then

    return;
    end

    local pOwner = self.Owner;

    if ( pOwner == NULL ) then
        return;
    end

    if ( SERVER ) then


    local angAiming;


    local count2 = 0
    if con == 1 then cone = 0.020 else cone = 0.00000001 end
   -- while count2 < 10 do
        count2 = count2 + 1


             vecAiming		= pOwner:GetAimVector() ;
             vecSrc		= pOwner:GetShootPos();
            angAiming = vecAiming:Angle() ;
            self:ShootBullet( 75, 1, cone )


        local pBolt = ents.Create (  "crossbow_bolt" );
        pBolt:SetPos( vecSrc );
        pBolt:SetAngles( angAiming );

        pBolt.Damage = 0


        pBolt.AmmoType = self.Primary.Ammo;
        pBolt:SetOwner( pOwner );
        pBolt:Spawn()

        if ( pOwner:WaterLevel() == 3 ) then
            pBolt:SetVelocity( vecAiming * BOLT_WATER_VELOCITY );
        else
            pBolt:SetVelocity( vecAiming * BOLT_AIR_VELOCITY );
        end

    end



end

function SWEP:FireBolt2( con )

    if ( self.Weapon:Clip1() <= 0 ) then

        return;
    end

    local pOwner = self.Owner;

    if ( pOwner == NULL ) then
        return;
    end

    if ( SERVER ) then


        local angAiming;


        local count2 = 0
        cone = 0.025
            vecAiming		= pOwner:GetAimVector() +  Vector( math.Rand(-0.09,0.09), math.Rand(-0.09,0.09), math.Rand(-0.09,0.09) );
            vecSrc		= pOwner:GetShootPos() ;
            angAiming = vecAiming:Angle() ;
            self:ShootBullet( 0, 1, cone )



        local pBolt = ents.Create (  "crossbow_bolt" );
        pBolt:SetPos( vecSrc );
        pBolt:SetAngles( angAiming );
        pBolt.Damage = 0

        pBolt.AmmoType = self.Primary.Ammo;
        pBolt:SetOwner( pOwner );
        pBolt:Spawn()

        if ( pOwner:WaterLevel() == 3 ) then
            pBolt:SetVelocity( vecAiming * BOLT_WATER_VELOCITY );
        else
            pBolt:SetVelocity( vecAiming * BOLT_AIR_VELOCITY );
        end

    end



end


function SWEP:FireBolt3( con )

    if ( self.Weapon:Clip1() <= 0 ) then

        return;
    end

    local pOwner = self.Owner;

    if ( pOwner == NULL ) then
        return;
    end

    if ( SERVER ) then


        local angAiming;


        local count2 = 0
        cone = 0.025

        vecAiming		= pOwner:GetAimVector() +  Vector( math.Rand(-0.10,0.10), math.Rand(-0.10,0.10), math.Rand(-0.10,0.10) );

        vecSrc		= pOwner:GetShootPos() ;
        angAiming = vecAiming:Angle() ;
        self:ShootBullet( 0, 1, cone )



        local pBolt = ents.Create (  "crossbow_bolt" );
        pBolt:SetPos( vecSrc );
        pBolt:SetAngles( angAiming );
        pBolt.Damage = 0

        pBolt.AmmoType = self.Primary.Ammo;
        pBolt:SetOwner( pOwner );
        pBolt:Spawn()

        if ( pOwner:WaterLevel() == 3 ) then
            pBolt:SetVelocity( vecAiming * BOLT_WATER_VELOCITY );
        else
            pBolt:SetVelocity( vecAiming * BOLT_AIR_VELOCITY );
        end

    end



end

function SWEP:FireBolt4( con )

    if ( self.Weapon:Clip1() <= 0 ) then

        return;
    end

    local pOwner = self.Owner;

    if ( pOwner == NULL ) then
        return;
    end

    if ( SERVER ) then


        local angAiming;


        local count2 = 0
        cone = 0.025

        vecAiming		= pOwner:GetAimVector() +  Vector( math.Rand(-0.08,0.08), math.Rand(-0.08,0.08), math.Rand(-0.08,0.08) );

        vecSrc		= pOwner:GetShootPos() ;
        angAiming = vecAiming:Angle() ;
        self:ShootBullet( 0, 1, cone )



        local pBolt = ents.Create (  "crossbow_bolt" );
        pBolt:SetPos( vecSrc );
        pBolt:SetAngles( angAiming );
        pBolt.Damage = 0

        pBolt.AmmoType = self.Primary.Ammo;
        pBolt:SetOwner( pOwner );
        pBolt:Spawn()

        if ( pOwner:WaterLevel() == 3 ) then
            pBolt:SetVelocity( vecAiming * BOLT_WATER_VELOCITY );
        else
            pBolt:SetVelocity( vecAiming * BOLT_AIR_VELOCITY );
        end

    end



end

function SWEP:FireBolt5( con )

    if ( self.Weapon:Clip1() <= 0 ) then

        return;
    end

    local pOwner = self.Owner;

    if ( pOwner == NULL ) then
        return;
    end

    if ( SERVER ) then


        local angAiming;


        local count2 = 0
        cone = 0.025

        vecAiming		= pOwner:GetAimVector() +  Vector( math.Rand(-0.12,0.12), math.Rand(-0.12,0.12), math.Rand(-0.12,0.12) );

        vecSrc		= pOwner:GetShootPos() ;
        angAiming = vecAiming:Angle() ;
        self:ShootBullet( 0, 1, cone )



        local pBolt = ents.Create (  "crossbow_bolt" );
        pBolt:SetPos( vecSrc );
        pBolt:SetAngles( angAiming );
        pBolt.Damage = 0

        pBolt.AmmoType = self.Primary.Ammo;
        pBolt:SetOwner( pOwner );
        pBolt:Spawn()

        if ( pOwner:WaterLevel() == 3 ) then
            pBolt:SetVelocity( vecAiming * BOLT_WATER_VELOCITY );
        else
            pBolt:SetVelocity( vecAiming * BOLT_AIR_VELOCITY );
        end

    end



end

function SWEP:SetDeploySpeed( speed )

    self.m_WeaponDeploySpeed = tonumber( speed / GetConVarNumber( "phys_timescale" ) )

    self.Weapon:SetNextPrimaryFire( CurTime() + speed )
    self.Weapon:SetNextSecondaryFire( CurTime() + speed )

end


function SWEP:SecondaryAttack()
    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
    self.Weapon:SetNextSecondaryFire( CurTime() + 1 )

    self.Owner:LagCompensation(true)



    local spos = self.Owner:GetShootPos()
    local sdest = spos + (self.Owner:GetAimVector() * 80)

    local kmins = Vector(1,1,1) * -10
    local kmaxs = Vector(1,1,1) * 10

    local tr = util.TraceHull({start=spos, endpos=sdest, filter=self.Owner, mask=MASK_SHOT_HULL, mins=kmins, maxs=kmaxs})

    -- Hull might hit environment stuff that line does not hit
    if not IsValid(tr.Entity) then
        tr = util.TraceLine({start=spos, endpos=sdest, filter=self.Owner, mask=MASK_SHOT_HULL})
    end

    local hitEnt = tr.Entity

    -- effects
    if IsValid(hitEnt) then
       -- self.Weapon:SendWeaponAnim( ACT_VM_HITCENTER )

        local edata = EffectData()
        edata:SetStart(spos)
        edata:SetOrigin(tr.HitPos)
        edata:SetNormal(tr.Normal)
        edata:SetEntity(hitEnt)

        if hitEnt:IsPlayer() or hitEnt:GetClass() == "prop_ragdoll" then
            util.Effect("BloodImpact", edata)
        end
    else
      --  self.Weapon:SendWeaponAnim( ACT_VM_MISSCENTER )
      --  self.Owner:ViewPunch( Angle( rnda,rndb,rnda ) )
    end

    if SERVER then
      --  self.Owner:SetAnimation( PLAYER_ATTACK1 )
    end


    if SERVER and tr.Hit and tr.HitNonWorld and IsValid(hitEnt) then
        if hitEnt:IsPlayer() then
            if self.Owner:Health() < 175 then
                self.Owner:SetHealth(self.Owner:Health()+10)
            end
            -- knife damage is never karma'd, so don't need to take that into
            -- account we do want to avoid rounding error strangeness caused by
            -- other damage scaling, causing a death when we don't expect one, so
            -- when the target's health is close to kill-point we just kill

                local dmg = DamageInfo()
                dmg:SetDamage(25)
                dmg:SetAttacker(self.Owner)
                dmg:SetInflictor(self.Weapon or self)
                dmg:SetDamageForce(self.Owner:GetAimVector() * 5)
                dmg:SetDamagePosition(self.Owner:GetPos())
                dmg:SetDamageType(DMG_SLASH)

                hitEnt:DispatchTraceAttack(dmg, spos + (self.Owner:GetAimVector() * 3), sdest)

        end
    end
    local owner = self.Owner
    self.Owner:LagCompensation(false)
    if not IsValid(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end


    self.Owner:SetFOV( 100, 0.2 );
    timer.Simple(0.3, function() if IsValid(self) and IsValid(self.Owner) then self.Owner:SetFOV( 0, 0.5 ) end end)


    owner:ViewPunch( Angle( -10, 0, 0 ) )
end
