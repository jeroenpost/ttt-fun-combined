SWEP.Base = "gb_camo_base"
SWEP.ViewModelFOV	= 54
SWEP.HoldType = "pistol"
SWEP.PrintName = "Buffatron 9000"
SWEP.ViewModel		= "models/weapons/c_357.mdl"
SWEP.WorldModel		= "models/weapons/w_357.mdl"
SWEP.Kind = 991
SWEP.Slot = 8
SWEP.AutoSpawnable = true
SWEP.Primary.Damage		    = 55
SWEP.Primary.Delay		    = 0.5
SWEP.Primary.ClipSize		= 500
SWEP.Primary.DefaultClip	= 500
SWEP.Primary.Cone	        = 0.008
SWEP.Primary.Automatic		= false
SWEP.Primary.Ammo			= "AlyxGun"
SWEP.AmmoEnt = "item_ammo_revolver_ttt"
SWEP.Primary.Sound          = Sound("weapons/357/357_fire2.wav")
SWEP.HeadshotMultiplier = 2.5
SWEP.Camo = 7

SWEP.IronSightsPos = Vector( -4.6, -3, 0.65 )
SWEP.IronSightsAng = Vector( 0, 0, 0.00 )

function SWEP:DrawWorldModel()
end

function SWEP:DrawWorldModelTranslucent()
end


function SWEP:Initialize()
    self:SetHoldType("normal")
    self.BaseClass.Initialize(self)
end

function SWEP:Reload()
    if self.Owner:KeyDown(IN_USE) and (not self.nextreload or self.nextreload < CurTime()) then
        self.nextreload = CurTime() + 0.3
        local mode = self.Owner:GetNWString("buff_mode","PointMan")
        local ply = self.Owner
        if mode == "PointMan" then ply:SetNWString("buff_mode", "Medic") end
        if mode == "Medic" then ply:SetNWString("buff_mode", "PointMan") end
        return
    end
    self.BaseClass.Reload(self)
end

function SWEP:SecondaryAttack()

    if self.Owner:GetNWInt("buff_activate",0) > CurTime() then return end

    if  self.Owner:GetNWInt("buff_defence",0) < 100  then
        self.Owner:PrintMessage( HUD_PRINTCENTER, "Charge first" )
        return
    end

    local defence = math.Round(self.Owner:GetNWInt("buff_defence",0))
    self.Owner:SetNWInt("buff_defence",0)
    self.Owner:SetNWInt("buff_activate",CurTime()+15)

    local mode = self.Owner:GetNWString("buff_mode","PointMan")

    if mode == "PointMan" then
        self.Owner:SetNWInt("buff_boost",50)
        self.Owner:SetNWFloat("buff_speed",1.5)
        self.Owner:SetNWInt("speedmul",self.Owner:GetNWInt("speedmul",220)*1.5)
        timer.Create(self.Owner:SteamID().."buffact"..CurTime(),15,1,function()
            if IsValid(self) and IsValid(self.Owner) then
                self.Owner:SetNWInt("buff_boost",0)
                self.Owner:SetNWFloat("buff_speed",0)
                self.Owner:SetNWInt("speedmul",220)
                timer.Simple(2,function()
                    if IsValid(self) and IsValid(self.Owner) then
                        self.Owner:SetNWInt("buff_boost",0)
                        self.Owner:SetNWFloat("buff_speed",0)
                        self.Owner:SetNWInt("speedmul",220)
                    end
                end)
            end
        end)
    end

    if mode == "Medic" then

        self.Owner:SetNWFloat("buff_speed",1.2)
        self.Owner:SetNWInt("speedmul",self.Owner:GetNWInt("speedmul",220)*1.2)
        timer.Create(self.Owner:SteamID().."buffmedic"..CurTime(),15,1,function()
            if IsValid(self) and IsValid(self.Owner) then
                self.Owner:SetNWFloat("buff_speed",0)
                self.Owner:SetNWInt("speedmul",220)
                timer.Simple(2,function()
                    if IsValid(self) and IsValid(self.Owner) then
                        self.Owner:SetNWFloat("buff_speed",0)
                        self.Owner:SetNWInt("speedmul",220)
                    end
                end)
            end
        end)

        timer.Create(self.Owner:SteamID().."buffmedic_heal"..CurTime(),1,15,function()
            if IsValid(self) and IsValid(self.Owner) then
                self.Owner:SetHealth(self.Owner:Health()+2)
                if self.Owner:Health() > 150 then self.Owner:SetHealth(150) end
            end
        end)
    end


end

function SWEP:Deploy(ply)
    local ply = self.Owner
    if IsValid(ply) then
        ply:SetFunVar("has_buffatron",1)
       if ply:GetNWInt("buff_defence",0) == 0 then ply:SetNWInt("buff_defence",0) end
       if ply:GetNWString("buff_mode", "11") == "11" then ply:SetNWString("buff_mode", "PointMan") end

        hook.Add("EntityTakeDamage", "buffatron_damage",function( ent, dmginfo )
            local inflictor = dmginfo:GetInflictor( )
            local damagetype = dmginfo:GetDamageType()

            if SERVER and IsValid(ply) then

                if SERVER and ent == ply and inflictor ~= ply   then
                    ply:SetNWInt("lasthit", CurTime() )
                end

                if SERVER and inflictor == ply and ent:IsPlayer() and (ply:GetNWInt("lasthit",0)+6) > CurTime()   then
                    local mode = ply:GetNWString("buff_mode", "123")

                    local damage = dmginfo:GetDamage()
                    if damage and damage > 0 then
                        if ply:GetPos():Distance( ent:GetPos() ) > 1599 then
                            damage = damage + ((damage/100)*25)
                        end
                        local setto = math.Round(ply:GetNWInt("buff_defence",0)+damage)
                        if setto > 100 then setto = 100 end
                        ply:SetNWInt("buff_defence", setto )
                    end


                end

                if SERVER and inflictor == ply and ply:GetNWInt("buff_activate",0) > CurTime() then
                    local mode = ply:GetNWString("buff_mode", "123")
                    if mode == "PointMan" then
                         dmginfo:ScaleDamage( 1.5 )
                         ply:SetNWFloat("buff_speed",ply:GetNWFloat("buff_speed",0)+0.25)
                         ply:SetNWInt("speedmul",ply:GetNWInt("speedmul",220)*1.25)
                        timer.Create(ply:SteamID().."buffSpeed"..CurTime(),2,1,function()
                            if IsValid(ply) then
                                ply:SetNWFloat("buff_speed",ply:GetNWFloat("buff_speed",0)-0.25)
                                ply:SetNWInt("speedmul",ply:GetNWInt("speedmul",220)/1.25)
                            end
                        end)
                    end

                    if mode == "Medic" then
                        if (ply:GetNWInt("lasthit",0)+7) > CurTime() then
                            local damage = dmginfo:GetDamage()
                            local distance = ply:GetPos():Distance( ent:GetPos() )
                            if distance < 451 then
                                 ply:SetHealth(ply:Health()+(damage*0.66))
                                if ply:Health() > 150 then ply:SetHealth(150) end
                            elseif distance < 1500 then
                                local dmgmultiplier = (0.66 / distance) * 100
                                print(dmgmultiplier)
                                ply:SetHealth(ply:Health()+(damage*dmgmultiplier))
                            end
                        end


                        -- 15 health lost when hurting someone without getting hurt yourself
                            if (ply:GetNWInt("lasthit",0)+15) < CurTime() then
                                ply:EmitSound("vo/npc/Alyx/hurt08.wav")
                                ply:SetHealth(ply:Health()-14)
                                ply:Ignite(0.1)
                                if ply:Health() < 0 then ply:Kill() end
                            end


                    end
                end

                if SERVER and ent == ply and ply:GetNWInt("buff_activate",0) > CurTime() then
                    local mode = ply:GetNWString("buff_mode", "123")
                    if mode == "PointMan" then
                        if damagetype == DMG_SLASH or damagetype == DMG_CLUB then
                            dmginfo:ScaleDamage( 2 )
                        end
                        local speedbuff =  ply:GetNWFloat("buff_speed",0)
                        local slowspeed = speedbuff-0.5
                        if slowspeed < 1 then slowspeed = 1 end
                        local old_slowspeed = speedbuff - slowspeed
                        ply:SetNWFloat("buff_speed",slowspeed)

                        local speedmul = ply:GetNWInt("speedmul",220)
                        local slowspeed2 = speedmul*0.5
                        if slowspeed2 < 220 then slowspeed2 = 220 end
                        local old_slowspeed2 = speedmul - slowspeed2
                        ply:SetNWInt("speedmul",slowspeed2)
                        timer.Create(ply:SteamID().."buffSpeed"..CurTime(),2,1,function()
                            if IsValid(ply) then
                                ply:SetNWFloat("buff_speed",  ply:GetNWFloat("buff_speed")+old_slowspeed)
                                ply:SetNWInt("speedmul", ply:SetNWInt("speedmul") + old_slowspeed2)
                            end
                        end)
                    end

                    if mode == "Medic" then
                        if damagetype == DMG_ACID or damagetype == DMG_BURN or damagetype == DMG_POISON then
                            dmginfo:ScaleDamage( 1.2 )
                        end
                    end

                end
            end
        end)
    end

    self.BaseClass.Deploy(self)

end

if CLIENT then

    local function Buffatron_DrawHUD( )
        local self = {}
        self.Owner = LocalPlayer()
        if IsValid(self.Owner) and  self.Owner:GetFunVar("has_buffatron") == 1  then
            local defence = math.Round(self.Owner:GetNWInt("buff_defence",0))
            local shotttext = "Charge: "..defence.."/100"
            local mode = "Mode: "..self.Owner:GetNWString("buff_mode","")..""

            local speedboost = "Speed Boost: "..math.Round(self.Owner:GetNWFloat("buff_speed","0"),2).."x"
            local activate = self.Owner:GetNWInt("buff_activate","0") - CurTime()
            local dmgboost = "Dmg Boost: 0%"

            if activate > 0 then
                 dmgboost = "Dmg Boost: "..self.Owner:GetNWInt("buff_boost","0").."%"
            end

            surface.SetFont( "ChatFont" );
            local size_x, size_y = surface.GetTextSize( shotttext );

            draw.RoundedBox( 6, 10, ScrH( ) - 510, 200, 200, Color( 0, 0, 0, 150 ) );
            draw.DrawText( "Buffatron" , "Trebuchet24", 20, ScrH( ) - 500 , Color( 255, 255, 255, 255 ), TEXT_ALIGN_LEFT );

            local barsize = 1.9 * defence
            if barsize > 4 then
                draw.RoundedBox( 6, 15, ScrH( ) - 450, barsize, size_y + 10, Color( 0, 255, 0, 150 ) );
            end
            draw.RoundedBox( 6, 15, ScrH( ) - 450, 190 , size_y + 10, Color( 0, 0, 0, 150 ) );
            draw.DrawText( shotttext , "ChatFont", 20, ScrH( ) - 445 , Color( 255, 255, 255, 255 ), TEXT_ALIGN_LEFT );

            --draw.RoundedBox( 6, 15, ScrH( ) - 420, 190 , size_y + 10, Color( 0, 0, 0, 150 ) );
            if defence == 100 then
                draw.RoundedBox( 6, 15, ScrH( ) - 420, 190 , size_y + 10, Color( 0, 255, 0, 150 ) );
            elseif(activate > 0) then
                draw.RoundedBox( 6, 15, ScrH( ) - 420, 190 , size_y + 10, Color( 255, 0, 0, 150 ) );

                local length = 190 * activate/15
                if length > 6 then
                    if length > 190 then length = 190 end
                    draw.RoundedBox( 6, 15, ScrH( ) - 420, length , size_y + 10, Color( 0, 255, 0, 255 ) );
                end

            else
                draw.RoundedBox( 6, 15, ScrH( ) - 420, 190 , size_y + 10, Color( 255, 0, 0, 150 ) );
            end

            draw.DrawText( mode , "ChatFont", 20, ScrH( ) - 415 , Color( 255, 255, 255, 255 ), TEXT_ALIGN_LEFT );

            draw.DrawText( dmgboost , "ChatFont", 20, ScrH( ) - 385 , Color( 255, 255, 255, 255 ), TEXT_ALIGN_LEFT );
            draw.DrawText( speedboost , "ChatFont", 20, ScrH( ) - 365 , Color( 255, 255, 255, 255 ), TEXT_ALIGN_LEFT );



        end
    end
    hook.Add("HUDPaint","Buffatron_DrawHUD",Buffatron_DrawHUD)
end
