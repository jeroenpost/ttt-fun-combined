SWEP.Base = "gb_camo_base"
SWEP.ViewModelFOV	= 54
SWEP.HoldType = "ar2"
SWEP.PrintName = "THE MEXICANATOR"
SWEP.ViewModel		= "models/weapons/cstrike/c_smg_mp5.mdl"
SWEP.WorldModel		= "models/weapons/w_smg_mp5.mdl"
SWEP.AutoSpawnable = false

SWEP.HoldType = "ar2"
SWEP.Slot = 6

if CLIENT then

    SWEP.Author = "GreenBlack"

    SWEP.Icon = "vgui/ttt_fun_killicons/mp5.png"
end

SWEP.Spawnable = true
SWEP.AdminSpawnable = true
SWEP.Kind = WEAPON_EQUIP2

SWEP.Primary.Delay = 0.11
SWEP.Primary.Recoil = 1.1
SWEP.Primary.Automatic = true
SWEP.Primary.Ammo = "pistol"
SWEP.Primary.Damage = 16
SWEP.Primary.Cone = 0.028
SWEP.Primary.ClipSize = 45
SWEP.Primary.ClipMax = 90
SWEP.Primary.DefaultClip = 45
SWEP.AutoSpawnable = false
SWEP.AmmoEnt = "item_ammo_pistol_ttt"
SWEP.WorldModel = "models/weapons/w_smg_mp5.mdl"
SWEP.Primary.Sound = Sound("Weapon_MP5Navy.Single")

SWEP.HeadshotMultiplier = 1.2

SWEP.IronSightsPos = Vector(-5.3, -3.5823, 2)
SWEP.IronSightsAng = Vector(0.9641, 0.0252, 0)

SWEP.VElements = {
    ["element_name"] = { type = "Model", model = "models/gmod_tower/sombrero.mdl", bone = "v_weapon.MP5_Parent", rel = "", pos = Vector(0.2, -6, -4), angle = Angle(75.973, -90, 0), size = Vector(0.172, 0.172, 0.172), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
}


SWEP.UseHands = true

function SWEP:Initialize()

    if CLIENT then
    self:SetHoldType( self.HoldType or "pistol" )
    self.VElements = table.FullCopy( self.VElements )
    self.WElements = table.FullCopy( self.WElements )
    self.ViewModelBoneMods = table.FullCopy( self.ViewModelBoneMods )

    self:CreateModels(self.VElements)
    self:CreateModels(self.WElements)

        if IsValid(self.Owner) then
            local vm = self.Owner:GetViewModel()
            if IsValid(vm) then
                self:ResetBonePositions(vm)
            end
        end
    end

    self.BaseClass.Initialize(self)

end

function SWEP:Deploy()
    local function playerDiesSound( victim, weapon, killer )
        if( IsValid(killer) and killer:IsPlayer()  ) then
            local wep = killer:GetActiveWeapon()
            if IsValid(wep) then
                wep = wep:GetClass()
                if wep == "custom_the_mexicanator" then
                    gb_PlaySoundFromServer(gb_config.websounds.."mexicanator.mp3", victim)



                    local headturtle = ents.Create("prop_physics")
                    headturtle:SetModel("models/hunter/plates/plate025x1.mdl")
                    headturtle:SetPos(victim:GetPos() + victim:GetUp() * 75)
                    headturtle:SetAngles(Angle(0,-90,0))
                    headturtle:Spawn()
                    local entphys = headturtle:GetPhysicsObject();
                    if entphys:IsValid() then
                        entphys:EnableGravity(true);
                        entphys:Wake();
                    end


                    local turtle = ents.Create("prop_dynamic")
                    turtle:SetModel( "models/gmod_tower/sombrero.mdl")
                    turtle:SetPos(headturtle:GetPos())

                    turtle:SetParent(headturtle)


                    turtle:SetModelScale( turtle:GetModelScale() * 1.75, 0.05)


                    headturtle:SetNoDraw(true)
                    headturtle:SetHealth(1)

                end
            end
        end
    end

    hook.Add( "PlayerDeath", "playerDeathSound_prosniper", playerDiesSound )
end

SWEP.NextSecondary = 0
function SWEP:SecondaryAttack()
    if self.Owner:KeyDown(IN_USE) and self.NextSecondary < CurTime() then
        self.NextSecondary = CurTime() + 10
        gb_PlaySoundFromServer(gb_config.websounds.."mexicanator.mp3", self.Owner)
        return
    end
    self.BaseClass.SecondaryAttack(self)
end

SWEP.NextSecondary2 = 0

function SWEP:Reload()
    if self.Owner:KeyDown(IN_USE) and self.NextSecondary2 < CurTime() and SERVER then
        self.NextSecondary2 = CurTime() + 20
        local headturtle = ents.Create("prop_physics")
        headturtle:SetModel("models/hunter/plates/plate025x1.mdl")
        headturtle:SetPos(self.Owner:GetPos() + self.Owner:GetUp() * 120)
        headturtle:SetAngles(Angle(0,-90,0))
        headturtle:Spawn()
        local entphys = headturtle:GetPhysicsObject();
        if entphys:IsValid() then
            entphys:EnableGravity(true);
            entphys:Wake();
        end


        local turtle = ents.Create("prop_dynamic")
        turtle:SetModel( "models/gmod_tower/sombrero.mdl")
        turtle:SetPos(headturtle:GetPos())

        turtle:SetParent(headturtle)


        turtle:SetModelScale( turtle:GetModelScale() * 1.75, 0.05)


        headturtle:SetNoDraw(true)
        headturtle:SetHealth(1)
        return
    end

    self.BaseClass.Reload(self)
end