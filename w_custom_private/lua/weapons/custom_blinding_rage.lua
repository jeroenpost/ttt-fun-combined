//General Variables\\
SWEP.AdminSpawnable = true
SWEP.ViewModelFOV = 64
SWEP.Kind = 777
SWEP.Base = "aa_base"
SWEP.AutoSpawnable = false

SWEP.VElements = {
	["shell++"] = { type = "Model", model = "models/weapons/shotgun_shell.mdl", bone = "ValveBiped.Gun", rel = "back", pos = Vector(0, -2.1, 0.994), angle = Angle(0, 0, 0), size = Vector(0.474, 0.474, 0.474), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["sight"] = { type = "Model", model = "models/props_vehicles/tire001b_truck.mdl", bone = "ValveBiped.Gun", rel = "attach", pos = Vector(1.399, 0, -0.7), angle = Angle(0, 0, 0), size = Vector(0.045, 0.045, 0.045), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["top"] = { type = "Model", model = "models/Items/BoxSRounds.mdl", bone = "ValveBiped.Gun", rel = "", pos = Vector(0, -0.301, 0), angle = Angle(0, 0, -90), size = Vector(0.37, 1.1, 0.103), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["RAIL"] = { type = "Model", model = "models/props_wasteland/prison_heater001a.mdl", bone = "ValveBiped.Gun", rel = "top", pos = Vector(0, 0, -2.201), angle = Angle(90, 180, 0), size = Vector(0.412, 0.6, 0.041), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["flashlight"] = { type = "Model", model = "models/props_wasteland/light_spotlight01_lamp.mdl", bone = "ValveBiped.Gun", rel = "barrelunder", pos = Vector(0, 0, -2), angle = Angle(-90, 0, 0), size = Vector(0.4, 0.103, 0.103), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["Body"] = { type = "Model", model = "models/props_combine/CombineTrain01a.mdl", bone = "ValveBiped.Pump", rel = "", pos = Vector(0, -1, 1), angle = Angle(90, 0, 90), size = Vector(0.024, 0.026, 0.01), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["barrel"] = { type = "Model", model = "models/props_lab/kennel_physics.mdl", bone = "ValveBiped.Gun", rel = "top", pos = Vector(0, 14.906, -2.981), angle = Angle(0, 90, 0), size = Vector(0.25, 0.1, 0.1), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["top2"] = { type = "Model", model = "models/Items/car_battery01.mdl", bone = "ValveBiped.Bip01", rel = "top", pos = Vector(0, -6, 0), angle = Angle(0, 180, 0), size = Vector(0.226, 0.412, 0.289), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["shell+"] = { type = "Model", model = "models/weapons/shotgun_shell.mdl", bone = "ValveBiped.Gun", rel = "back", pos = Vector(0, -2.1, 2.98), angle = Angle(0, 180, 0), size = Vector(0.474, 0.474, 0.474), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["back"] = { type = "Model", model = "models/props_lab/reciever_cart.mdl", bone = "ValveBiped.Bip01", rel = "top", pos = Vector(0, -16.894, -0.5), angle = Angle(0, 0, 90), size = Vector(0.09, 0.09, 0.226), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["barrelunder"] = { type = "Model", model = "models/Items/battery.mdl", bone = "ValveBiped.Gun", rel = "barrel", pos = Vector(-1, 0, -0.7), angle = Angle(-90, 0, 0), size = Vector(0.67, 0.67, 0.67), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["sight2"] = { type = "Model", model = "models/props_trainstation/mount_connection001a.mdl", bone = "ValveBiped.Gun", rel = "attach", pos = Vector(0, 0, 0), angle = Angle(0, 0, -180), size = Vector(0.029, 0.029, 0.029), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["attach"] = { type = "Model", model = "models/props_wasteland/laundry_cart001.mdl", bone = "ValveBiped.Gun", rel = "top2", pos = Vector(0, 0, 1.899), angle = Angle(0, 90, 180), size = Vector(0.05, 0.05, 0.019), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["shell"] = { type = "Model", model = "models/weapons/shotgun_shell.mdl", bone = "ValveBiped.Gun", rel = "back", pos = Vector(0, -2.1, 4.968), angle = Angle(0, 0, 0), size = Vector(0.474, 0.474, 0.474), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["front sight"] = { type = "Model", model = "models/Items/combine_rifle_cartridge01.mdl", bone = "ValveBiped.Gun", rel = "barrel", pos = Vector(-2.981, 0, 3.974), angle = Angle(4.472, 180, 0), size = Vector(0.3, 0.3, 0.3), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
}
SWEP.WElements = {
	["shell++"] = { type = "Model", model = "models/weapons/shotgun_shell.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "back", pos = Vector(0, -2.1, 0.994), angle = Angle(0, 0, 0), size = Vector(0.474, 0.474, 0.474), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["shell"] = { type = "Model", model = "models/weapons/shotgun_shell.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "back", pos = Vector(0, -2.1, 4.968), angle = Angle(0, 0, 0), size = Vector(0.474, 0.474, 0.474), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["flashlight"] = { type = "Model", model = "models/props_wasteland/light_spotlight01_lamp.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "barrelunder", pos = Vector(0, 0, 3.048), angle = Angle(-90, 0, 0), size = Vector(0.4, 0.103, 0.103), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["top"] = { type = "Model", model = "models/Items/BoxSRounds.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(16.761, 0.99, -6), angle = Angle(4.472, -89.442, 174.41), size = Vector(0.37, 0.723, 0.103), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["stock"] = { type = "Model", model = "models/props_c17/computer01_keyboard.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "back", pos = Vector(0, 2.4, -6.2), angle = Angle(-15, 0, -180), size = Vector(0.3, 0.3, 0.3), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["sight"] = { type = "Model", model = "models/props_vehicles/tire001b_truck.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "attach", pos = Vector(1.399, 0, -0.7), angle = Angle(0, 0, 0), size = Vector(0.045, 0.045, 0.045), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["Body"] = { type = "Model", model = "models/props_combine/CombineTrain01a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "top", pos = Vector(0, -1.525, 0.899), angle = Angle(0, 90, 180), size = Vector(0.024, 0.026, 0.014), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["back"] = { type = "Model", model = "models/props_lab/reciever_cart.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "top", pos = Vector(0, -13.601, -0.5), angle = Angle(0, 0, 90), size = Vector(0.09, 0.09, 0.2), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["top2"] = { type = "Model", model = "models/Items/car_battery01.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "top", pos = Vector(0, -4.572, 0), angle = Angle(0, 180, 0), size = Vector(0.3, 0.342, 0.247), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["shell+"] = { type = "Model", model = "models/weapons/shotgun_shell.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "back", pos = Vector(0, -2.1, 2.98), angle = Angle(0, 180, 0), size = Vector(0.474, 0.474, 0.474), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["barrel"] = { type = "Model", model = "models/props_lab/kennel_physics.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "top", pos = Vector(0, 9.5, -4.5), angle = Angle(0, 90, 0), size = Vector(0.15, 0.1, 0.14), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["barrelunder"] = { type = "Model", model = "models/Items/battery.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "barrel", pos = Vector(-3.5, 0, -0.7), angle = Angle(-90, 0, 0), size = Vector(0.67, 0.67, 0.67), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["sight2"] = { type = "Model", model = "models/props_trainstation/mount_connection001a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "attach", pos = Vector(0, 0, 0), angle = Angle(0, 0, -180), size = Vector(0.029, 0.029, 0.029), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["attach"] = { type = "Model", model = "models/props_wasteland/laundry_cart001.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "top2", pos = Vector(0, 0, 1.799), angle = Angle(0, 90, 180), size = Vector(0.05, 0.05, 0.019), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["RAIL"] = { type = "Model", model = "models/props_wasteland/prison_heater001a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "top", pos = Vector(0, 0.5, -2.201), angle = Angle(90, 180, 0), size = Vector(0.412, 0.439, 0.045), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["front sight"] = { type = "Model", model = "models/Items/combine_rifle_cartridge01.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "barrel", pos = Vector(-2.981, 0, 5), angle = Angle(4.472, 180, 0), size = Vector(0.3, 0.3, 0.3), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
}
SWEP.AutoSwitchTo = false
SWEP.Slot = 776
SWEP.PrintName = "Binary's Blinding Rage"
SWEP.Author = "Spastik"
SWEP.Spawnable = true
SWEP.AutoSwitchFrom = false
SWEP.FiresUnderwater = false
SWEP.Weight = 5
SWEP.DrawCrosshair = true
SWEP.Category = "S&G Munitions"
SWEP.SlotPos = 2
SWEP.DrawAmmo = true
SWEP.ReloadSound = "weapons/shotgun/shotgun_reload1.wav"
SWEP.Instructions = "Pull the trigger to delay an update by five months."
SWEP.Contact = ""
SWEP.Purpose = "For getting everything done at the absolute last minute and being late."
SWEP.Base = "aa_base"
SWEP.HoldType = "shotgun"
SWEP.ViewModelFOV = 70
SWEP.ViewModelFlip = false
SWEP.ViewModel = "models/weapons/v_shotgun.mdl"
SWEP.WorldModel = "models/weapons/w_shotgun.mdl"
SWEP.ShowViewModel = true
SWEP.ShowWorldModel = true
SWEP.ViewModelBoneMods = {
	["ValveBiped.Pump"] = { scale = Vector(0.009, 0.009, 0.009), pos = Vector(0, 0, 0), angle = Angle(0, 0, 0) },
	["ValveBiped.Bip01"] = { scale = Vector(1, 1, 1), pos = Vector(-5.37, 1.419, 0.709), angle = Angle(0, 0, 0) }
}
//General Variables\\

//Primary Fire Variables\\
SWEP.Primary.Sound = "weapons/shotgun/shotgun_dbl_fire7.wav"
SWEP.Primary.Damage = 20
SWEP.Primary.TakeAmmo = 1
SWEP.Primary.ClipSize = 15
SWEP.Primary.Ammo = "Buckshot"
SWEP.Primary.DefaultClip = 77
SWEP.Primary.Spread = .9
SWEP.Primary.NumberofShots = 10
SWEP.Primary.Automatic = true
SWEP.Primary.Recoil = 1.4
SWEP.Primary.Delay = 1.1
SWEP.Primary.Force = 1
        SWEP.HasFireBullet = true
//Primary Fire Variables\\

//Secondary Fire Variables\\
SWEP.Secondary.Ammo = "none"
//Secondary Fire Variables\\

function SWEP:OnDrop()
    self:Remove()
end
//SWEP:Initialize()\\
function SWEP:Initialize()
	util.PrecacheSound(self.Primary.Sound)
	util.PrecacheSound(self.Secondary.Sound)
	if ( SERVER ) then
		self:SetHoldType( self.HoldType or "pistol" )
	end
end
//SWEP:Initialize()\\

//SWEP:PrimaryFire()\\
function SWEP:PrimaryAttack()

    if self.Owner:KeyDown(IN_USE) then
        if self.NextStick > CurTime() then return end
        self.NextStick = CurTime() + 1

        if self:GetNWBool( "Planted" )  then
            self:BombSplode()
        elseif  self.hadBomb < 5 then
            self:StickIt()
        end;
        return
    end
	if ( !self:CanPrimaryAttack() ) then return end

    local mode = self:GetNWString("shootmode")

    if mode == "Normal" then
        self:Blind(3)
        self:MakeAFlame()
        local bullet = {}
            bullet.Num = self.Primary.NumberofShots
            bullet.Src = self.Owner:GetShootPos()
            bullet.Dir = self.Owner:GetAimVector()
            bullet.Spread = Vector( self.Primary.Spread * 0.1 , self.Primary.Spread * 0.1, 0)
            bullet.Tracer = 1
            bullet.Force = self.Primary.Force
            bullet.Damage = self.Primary.Damage
            bullet.AmmoType = self.Primary.Ammo
        local rnda = self.Primary.Recoil * -1
        local rndb = self.Primary.Recoil * math.random(-1, 1)
        self:ShootEffects()
        self.Owner:FireBullets( bullet )
        self.Weapon:EmitSound(Sound("weapons/357/357_fire2.wav"), 500, 90, 1, 0)
        self.Weapon:EmitSound(Sound("weapons/shotgun/shotgun_dbl_fire7.wav"), 500, 90, 1, -1)
        self.Owner:ViewPunch( Angle( rnda,rndb,rnda ) )
        self:TakePrimaryAmmo(self.Primary.TakeAmmo)
        self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
        self.Weapon:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
    end
    if mode == "Precision" then
        self:Blind(3)
        self:MakeAFlame()
        local bullet = {}
        bullet.Num = 1
        bullet.Src = self.Owner:GetShootPos()
        bullet.Dir = self.Owner:GetAimVector()
        bullet.Spread = Vector( 0 , 0, 0)
        bullet.Tracer = 1
        bullet.Force = self.Primary.Force
        bullet.Damage = 40
        bullet.AmmoType = self.Primary.Ammo
        local rnda = self.Primary.Recoil * -1
        local rndb = self.Primary.Recoil * math.random(-1, 1)
        self:ShootEffects()
        self.Owner:FireBullets( bullet )
        self.Weapon:EmitSound(Sound("weapons/357/357_fire2.wav"), 500, 90, 1, 0)
        self:TakePrimaryAmmo(self.Primary.TakeAmmo)
        self.Weapon:SetNextPrimaryFire( CurTime() + 0.5 )
        self.Weapon:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
    end
    if mode == "Blinder" then
        self:YupBlind(0.1)
        self:MakeAFlame()
        local bullet = {}
        bullet.Num = 1
        bullet.Src = self.Owner:GetShootPos()
        bullet.Dir = self.Owner:GetAimVector()
        bullet.Spread = Vector( 0 , 0, 0)
        bullet.Tracer = 1
        bullet.Force = self.Primary.Force
        bullet.Damage = 85
        bullet.AmmoType = self.Primary.Ammo
        local rnda = self.Primary.Recoil * -1
        local rndb = self.Primary.Recoil * math.random(-1, 1)
        self:ShootEffects()
        self.Owner:FireBullets( bullet )
        self.Weapon:EmitSound(Sound("weapons/357/357_fire2.wav"), 500, 90, 1, 0)
        self:TakePrimaryAmmo(self.Primary.TakeAmmo)
        self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
        self.Weapon:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
    end


end
//SWEP:PrimaryFire()\\

-- Blind
SWEP.NumBlinds = 0
SWEP.Nextblind = 0
function SWEP:YupBlind( timerrr, victim, color )
   if not timerrr then timerrr = 0.1 end
    local trace = self.Owner:GetEyeTrace()
    if (trace.HitNonWorld) or victim then

        local victim = victim or trace.Entity
        if ( not victim:IsPlayer()) then return end


        if SERVER then
            victim.IsBlinded = true
            victim:SetNWBool("isblinded",true)
            umsg.Start( "ulx_blind", victim )
            umsg.Bool( true )
            umsg.Short( 255 )
            umsg.End()
        end
        if isfunction(victim.AnimPerformGesture) then
            victim:AnimPerformGesture(ACT_GMOD_TAUNT_PERSISTENCE);
        end
        if (SERVER) then

            timer.Create("ResetPLayerAfterBlided"..victim:SteamID(), timerrr,1, function()
                if not IsValid(victim)  then  return end

                if SERVER then
                    victim.IsBlinded = false
                    victim:SetNWBool("isblinded",false)
                    umsg.Start( "ulx_blind", victim )
                    umsg.Bool( false )
                    umsg.Short( 0 )
                    umsg.End()

                end
            end)
            return true
        end
    end
end

//SWEP:SecondaryFire()\\
function SWEP:SecondaryAttack()
    if self.Owner:KeyDown(IN_USE) then
       self:DropHealth("normal_health_station")
        return
    end
    self:Fly()
end

SWEP.flyammo = 25
SWEP.nextFly = 0
function SWEP:Fly( soundfile, remote )
    if self.nextFly > CurTime() or CLIENT then return end
    self.nextFly = CurTime() + 0.30
    if self.flyammo < 1 then return end

    if not soundfile then
        if SERVER then self.Owner:EmitSound(Sound("weapons/ls/lightsaber_swing.mp3")) end
        if SERVER then self.Owner:EmitSound(Sound("player/suit_sprint.wav")) end
    else
        if SERVER then
            if remote then
                local soundfile = gb_config.websounds..soundfile
                gb_PlaySoundFromServer(soundfile, self.Owner)
            else
                self.Owner:EmitSound(Sound(soundfile))
            end
        end
    end
    self.flyammo = self.flyammo - 1
    self.Owner:SetAnimation(PLAYER_ATTACK1)
    local power = 500

    if SERVER then self.Owner:SetVelocity(self.Owner:GetForward() * power + Vector(0, 0, power)) end
    timer.Simple(20, function() if IsValid(self) then self.flyammo = self.flyammo + 1 end end)
    --timer.Simple(0.2,self.SecondaryAttackDelay,self)
end

--[[---------------------------------------------------------
   Name: SWEP:Reload( )
   Desc: Reload is being pressed
-----------------------------------------------------------]]

SWEP.NextReload = 0
function SWEP:Reload()
    if self.Owner:KeyDown(IN_ATTACK) then
        return
    end
    if self.Owner:KeyDown(IN_USE) and self.NextReload < CurTime() then
        self.NextReload = CurTime() + 0.3
        local mode = self:GetNWString("shootmode")
        self:SetIronsights(false)

        if mode == "Normal" then  self:SetNWString("shootmode","Precision")  end
        if mode == "Precision" then  self:SetNWString("shootmode","Blinder") end
        if mode == "Blinder" then  self:SetNWString("shootmode","Normal") end

        self.Weapon:SetNextPrimaryFire( CurTime() + 0.8 )
        return
    end

	self.Weapon:DefaultReload(ACT_SHOTGUN_PUMP)



	if (self.Weapon:Clip1() < self.Primary.ClipSize) and (self.Owner:GetAmmoCount(self.Primary.Ammo) > 0) then
		self:EmitSound( "weapons/shotgun/shotgun_cock.wav", 500, 100, 1, 1 )
		self.Weapon:SetNextPrimaryFire( CurTime() + 1 )

		if not (CLIENT) then

			self.Owner:DrawViewModel(true)

		end

	end
end

/********************************************************
	SWEP Construction Kit base code
		Created by Clavus
	Available for public use, thread at:
	   facepunch.com/threads/1032378


	DESCRIPTION:
		This script is meant for experienced scripters
		that KNOW WHAT THEY ARE DOING. Don't come to me
		with basic Lua questions.

		Just copy into your SWEP or SWEP base of choice
		and merge with your own code.

		The SWEP.VElements, SWEP.WElements and
		SWEP.ViewModelBoneMods tables are all optional
		and only have to be visible to the client.
********************************************************/

function SWEP:Initialize()

	// other initialize code goes here

	if CLIENT then

		self:SetHoldType( self.HoldType or "pistol" )
		// Create a new table for every weapon instance
		self.VElements = table.FullCopy( self.VElements )
		self.WElements = table.FullCopy( self.WElements )
		self.ViewModelBoneMods = table.FullCopy( self.ViewModelBoneMods )

		self:CreateModels(self.VElements) // create viewmodels
		self:CreateModels(self.WElements) // create worldmodels

		// init view model bone build function
		if IsValid(self.Owner) then
			local vm = self.Owner:GetViewModel()
			if IsValid(vm) then
				self:ResetBonePositions(vm)

				// Init viewmodel visibility
				if (self.ShowViewModel == nil or self.ShowViewModel) then
					vm:SetColor(Color(255,255,255,255))
				else
					// we set the alpha to 1 instead of 0 because else ViewModelDrawn stops being called
					vm:SetColor(Color(255,255,255,1))
					// ^ stopped working in GMod 13 because you have to do Entity:SetRenderMode(1) for translucency to kick in
					// however for some reason the view model resets to render mode 0 every frame so we just apply a debug material to prevent it from drawing
					vm:SetMaterial("Debug/hsv")
				end
			end
		end
	end
end
function SWEP:Holster()

	if CLIENT and IsValid(self.Owner) then
		local vm = self.Owner:GetViewModel()
		if IsValid(vm) then
			self:ResetBonePositions(vm)
		end
	end

	return true
end

function SWEP:OnRemove()
	self:Holster()
end

if CLIENT then

	SWEP.vRenderOrder = nil
	function SWEP:ViewModelDrawn()

		local vm = self.Owner:GetViewModel()
		if !IsValid(vm) then return end

		if (!self.VElements) then return end

		self:UpdateBonePositions(vm)

		if (!self.vRenderOrder) then

			// we build a render order because sprites need to be drawn after models
			self.vRenderOrder = {}

			for k, v in pairs( self.VElements ) do
				if (v.type == "Model") then
					table.insert(self.vRenderOrder, 1, k)
				elseif (v.type == "Sprite" or v.type == "Quad") then
					table.insert(self.vRenderOrder, k)
				end
			end

		end

		for k, name in ipairs( self.vRenderOrder ) do

			local v = self.VElements[name]
			if (!v) then self.vRenderOrder = nil break end
			if (v.hide) then continue end

			local model = v.modelEnt
			local sprite = v.spriteMaterial

			if (!v.bone) then continue end

			local pos, ang = self:GetBoneOrientation( self.VElements, v, vm )

			if (!pos) then continue end

			if (v.type == "Model" and IsValid(model)) then

				model:SetPos(pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z )
				ang:RotateAroundAxis(ang:Up(), v.angle.y)
				ang:RotateAroundAxis(ang:Right(), v.angle.p)
				ang:RotateAroundAxis(ang:Forward(), v.angle.r)

				model:SetAngles(ang)
				//model:SetModelScale(v.size)
				local matrix = Matrix()
				matrix:Scale(v.size)
				model:EnableMatrix( "RenderMultiply", matrix )

				if (v.material == "") then
					model:SetMaterial("")
				elseif (model:GetMaterial() != v.material) then
					model:SetMaterial( v.material )
				end

				if (v.skin and v.skin != model:GetSkin()) then
					model:SetSkin(v.skin)
				end

				if (v.bodygroup) then
					for k, v in pairs( v.bodygroup ) do
						if (model:GetBodygroup(k) != v) then
							model:SetBodygroup(k, v)
						end
					end
				end

				if (v.surpresslightning) then
					render.SuppressEngineLighting(true)
				end

				render.SetColorModulation(v.color.r/255, v.color.g/255, v.color.b/255)
				render.SetBlend(v.color.a/255)
				model:DrawModel()
				render.SetBlend(1)
				render.SetColorModulation(1, 1, 1)

				if (v.surpresslightning) then
					render.SuppressEngineLighting(false)
				end

			elseif (v.type == "Sprite" and sprite) then

				local drawpos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
				render.SetMaterial(sprite)
				render.DrawSprite(drawpos, v.size.x, v.size.y, v.color)

			elseif (v.type == "Quad" and v.draw_func) then

				local drawpos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
				ang:RotateAroundAxis(ang:Up(), v.angle.y)
				ang:RotateAroundAxis(ang:Right(), v.angle.p)
				ang:RotateAroundAxis(ang:Forward(), v.angle.r)

				cam.Start3D2D(drawpos, ang, v.size)
					v.draw_func( self )
				cam.End3D2D()

			end

		end

	end

	SWEP.wRenderOrder = nil
	function SWEP:DrawWorldModel()

		if (self.ShowWorldModel == nil or self.ShowWorldModel) then
			self:DrawModel()
		end

		if (!self.WElements) then return end

		if (!self.wRenderOrder) then

			self.wRenderOrder = {}

			for k, v in pairs( self.WElements ) do
				if (v.type == "Model") then
					table.insert(self.wRenderOrder, 1, k)
				elseif (v.type == "Sprite" or v.type == "Quad") then
					table.insert(self.wRenderOrder, k)
				end
			end

		end

		if (IsValid(self.Owner)) then
			bone_ent = self.Owner
		else
			// when the weapon is dropped
			bone_ent = self
		end

		for k, name in pairs( self.wRenderOrder ) do

			local v = self.WElements[name]
			if (!v) then self.wRenderOrder = nil break end
			if (v.hide) then continue end

			local pos, ang

			if (v.bone) then
				pos, ang = self:GetBoneOrientation( self.WElements, v, bone_ent )
			else
				pos, ang = self:GetBoneOrientation( self.WElements, v, bone_ent, "ValveBiped.Bip01_R_Hand" )
			end

			if (!pos) then continue end

			local model = v.modelEnt
			local sprite = v.spriteMaterial

			if (v.type == "Model" and IsValid(model)) then

				model:SetPos(pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z )
				ang:RotateAroundAxis(ang:Up(), v.angle.y)
				ang:RotateAroundAxis(ang:Right(), v.angle.p)
				ang:RotateAroundAxis(ang:Forward(), v.angle.r)

				model:SetAngles(ang)
				//model:SetModelScale(v.size)
				local matrix = Matrix()
				matrix:Scale(v.size)
				model:EnableMatrix( "RenderMultiply", matrix )

				if (v.material == "") then
					model:SetMaterial("")
				elseif (model:GetMaterial() != v.material) then
					model:SetMaterial( v.material )
				end

				if (v.skin and v.skin != model:GetSkin()) then
					model:SetSkin(v.skin)
				end

				if (v.bodygroup) then
					for k, v in pairs( v.bodygroup ) do
						if (model:GetBodygroup(k) != v) then
							model:SetBodygroup(k, v)
						end
					end
				end

				if (v.surpresslightning) then
					render.SuppressEngineLighting(true)
				end

				render.SetColorModulation(v.color.r/255, v.color.g/255, v.color.b/255)
				render.SetBlend(v.color.a/255)
				model:DrawModel()
				render.SetBlend(1)
				render.SetColorModulation(1, 1, 1)

				if (v.surpresslightning) then
					render.SuppressEngineLighting(false)
				end

			elseif (v.type == "Sprite" and sprite) then

				local drawpos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
				render.SetMaterial(sprite)
				render.DrawSprite(drawpos, v.size.x, v.size.y, v.color)

			elseif (v.type == "Quad" and v.draw_func) then

				local drawpos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
				ang:RotateAroundAxis(ang:Up(), v.angle.y)
				ang:RotateAroundAxis(ang:Right(), v.angle.p)
				ang:RotateAroundAxis(ang:Forward(), v.angle.r)

				cam.Start3D2D(drawpos, ang, v.size)
					v.draw_func( self )
				cam.End3D2D()

			end

		end

	end

	function SWEP:GetBoneOrientation( basetab, tab, ent, bone_override )

		local bone, pos, ang
		if (tab.rel and tab.rel != "") then

			local v = basetab[tab.rel]

			if (!v) then return end

			// Technically, if there exists an element with the same name as a bone
			// you can get in an infinite loop. Let's just hope nobody's that stupid.
			pos, ang = self:GetBoneOrientation( basetab, v, ent )

			if (!pos) then return end

			pos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
			ang:RotateAroundAxis(ang:Up(), v.angle.y)
			ang:RotateAroundAxis(ang:Right(), v.angle.p)
			ang:RotateAroundAxis(ang:Forward(), v.angle.r)

		else

			bone = ent:LookupBone(bone_override or tab.bone)

			if (!bone) then return end

			pos, ang = Vector(0,0,0), Angle(0,0,0)
			local m = ent:GetBoneMatrix(bone)
			if (m) then
				pos, ang = m:GetTranslation(), m:GetAngles()
			end

			if (IsValid(self.Owner) and self.Owner:IsPlayer() and
				ent == self.Owner:GetViewModel() and self.ViewModelFlip) then
				ang.r = -ang.r // Fixes mirrored models
			end

		end

		return pos, ang
	end

	function SWEP:CreateModels( tab )

		if (!tab) then return end

		// Create the clientside models here because Garry says we can't do it in the render hook
		for k, v in pairs( tab ) do
			if (v.type == "Model" and v.model and v.model != "" and (!IsValid(v.modelEnt) or v.createdModel != v.model) and
					string.find(v.model, ".mdl") and file.Exists (v.model, "GAME") ) then

				v.modelEnt = ClientsideModel(v.model, RENDER_GROUP_VIEW_MODEL_OPAQUE)
				if (IsValid(v.modelEnt)) then
					v.modelEnt:SetPos(self:GetPos())
					v.modelEnt:SetAngles(self:GetAngles())
					v.modelEnt:SetParent(self)
					v.modelEnt:SetNoDraw(true)
					v.createdModel = v.model
				else
					v.modelEnt = nil
				end

			elseif (v.type == "Sprite" and v.sprite and v.sprite != "" and (!v.spriteMaterial or v.createdSprite != v.sprite)
				and file.Exists ("materials/"..v.sprite..".vmt", "GAME")) then

				local name = v.sprite.."-"
				local params = { ["$basetexture"] = v.sprite }
				// make sure we create a unique name based on the selected options
				local tocheck = { "nocull", "additive", "vertexalpha", "vertexcolor", "ignorez" }
				for i, j in pairs( tocheck ) do
					if (v[j]) then
						params["$"..j] = 1
						name = name.."1"
					else
						name = name.."0"
					end
				end

				v.createdSprite = v.sprite
				v.spriteMaterial = CreateMaterial(name,"UnlitGeneric",params)

			end
		end

	end

	local allbones
	local hasGarryFixedBoneScalingYet = false

	function SWEP:UpdateBonePositions(vm)

		if self.ViewModelBoneMods then

			if (!vm:GetBoneCount()) then return end

			// !! WORKAROUND !! //
			// We need to check all model names :/
			local loopthrough = self.ViewModelBoneMods
			if (!hasGarryFixedBoneScalingYet) then
				allbones = {}
				for i=0, vm:GetBoneCount() do
					local bonename = vm:GetBoneName(i)
					if (self.ViewModelBoneMods[bonename]) then
						allbones[bonename] = self.ViewModelBoneMods[bonename]
					else
						allbones[bonename] = {
							scale = Vector(1,1,1),
							pos = Vector(0,0,0),
							angle = Angle(0,0,0)
						}
					end
				end

				loopthrough = allbones
			end
			// !! ----------- !! //

			for k, v in pairs( loopthrough ) do
				local bone = vm:LookupBone(k)
				if (!bone) then continue end

				// !! WORKAROUND !! //
				local s = Vector(v.scale.x,v.scale.y,v.scale.z)
				local p = Vector(v.pos.x,v.pos.y,v.pos.z)
				local ms = Vector(1,1,1)
				if (!hasGarryFixedBoneScalingYet) then
					local cur = vm:GetBoneParent(bone)
					while(cur >= 0) do
						local pscale = loopthrough[vm:GetBoneName(cur)].scale
						ms = ms * pscale
						cur = vm:GetBoneParent(cur)
					end
				end

				s = s * ms
				// !! ----------- !! //

				if vm:GetManipulateBoneScale(bone) != s then
					vm:ManipulateBoneScale( bone, s )
				end
				if vm:GetManipulateBoneAngles(bone) != v.angle then
					vm:ManipulateBoneAngles( bone, v.angle )
				end
				if vm:GetManipulateBonePosition(bone) != p then
					vm:ManipulateBonePosition( bone, p )
				end
			end
		else
			self:ResetBonePositions(vm)
		end

	end

	function SWEP:ResetBonePositions(vm)

		if (!vm:GetBoneCount()) then return end
		for i=0, vm:GetBoneCount() do
			vm:ManipulateBoneScale( i, Vector(1, 1, 1) )
			vm:ManipulateBoneAngles( i, Angle(0, 0, 0) )
			vm:ManipulateBonePosition( i, Vector(0, 0, 0) )
		end

	end

	/**************************
		Global utility code
	**************************/

	// Fully copies the table, meaning all tables inside this table are copied too and so on (normal table.Copy copies only their reference).
	// Does not copy entities of course, only copies their reference.
	// WARNING: do not use on tables that contain themselves somewhere down the line or you'll get an infinite loop
	function table.FullCopy( tab )

		if (!tab) then return nil end

		local res = {}
		for k, v in pairs( tab ) do
			if (type(v) == "table") then
				res[k] = table.FullCopy(v) // recursion ho!
			elseif (type(v) == "Vector") then
				res[k] = Vector(v.x, v.y, v.z)
			elseif (type(v) == "Angle") then
				res[k] = Angle(v.p, v.y, v.r)
			else
				res[k] = v
			end
		end

		return res

	end

end

function SWEP:Deploy()
self:SetNWString("shootmode","Normal")
end

if CLIENT then
	function SWEP:DrawHUD( )


        local shotttext = "Shootmode: "..self:GetNWString("shootmode").."\nE+R to change"
        surface.SetFont( "ChatFont" );
        local size_x, size_y = surface.GetTextSize( shotttext );

        draw.RoundedBox( 6, ScrW( ) / 2 - size_x / 2 - 5, ScrH( ) - 150, size_x + 15, size_y + 10, Color( 0, 0, 0, 150 ) );
        draw.DrawText( shotttext , "ChatFont", ScrW( ) / 2, ScrH( ) - 150 + 5, Color( 255, 30, 30, 255 ), TEXT_ALIGN_CENTER );

		local planted = self:GetNWBool( "Planted" );
		local tr = LocalPlayer( ):GetEyeTrace( );
		local close2;

		if tr.Entity:IsPlayer( ) and LocalPlayer( ):GetPos( ):Distance( tr.Entity:GetPos( ) ) <= 99999 then
			close2 = true;
		else
			close2 = false;
		end;

		local planted_text = "Not Planted!";
		local close_text = "Not Close Enough!";

		if planted then
			if hidden == true then
				hidden = false
			end
			planted_text = "Planted!\nSecondary to Explode!";

			surface.SetFont( "ChatFont" );
			local size_x, size_y = surface.GetTextSize( planted_text );

			draw.RoundedBox( 6, ScrW( ) / 2 - size_x / 2 - 5, ScrH( ) - 100, size_x + 15, size_y + 10, Color( 0, 0, 0, 150 ) );
			draw.DrawText( planted_text, "ChatFont", ScrW( ) / 2, ScrH( ) - 100 + 5, Color( 255, 30, 30, 255 ), TEXT_ALIGN_CENTER );
		else
			if close2 then
				close_text = "You are Close Enough!\nPrimary to Plant!";
			end;

			surface.SetFont( "ChatFont" );
			local size_x, size_y = surface.GetTextSize( planted_text );

			surface.SetFont( "ChatFont" );
			local size_x2, size_y2 = surface.GetTextSize( close_text );

			draw.RoundedBox( 6, ScrW( ) / 2 - ( size_x2 / 2 ) - 7.5, ScrH( ) - 100, size_x2 + 20, size_y + size_y2 + 10, Color( 0, 0, 0, 150 ) );
			draw.DrawText( planted_text, "ChatFont", ScrW( ) / 2, ScrH( ) - 100 + 5, Color( 30, 30, 255, 255 ), TEXT_ALIGN_CENTER );
			draw.DrawText( close_text, "ChatFont", ScrW( ) / 2 , ScrH( ) - 100 + size_y + 5, Color( 255, 255, 255, 255 ), TEXT_ALIGN_CENTER );
		end;
	end;
end;
-- =============================================================


SWEP.DrawCrosshair      = true
SWEP.hadBomb = 0
SWEP.NextStick = 0



SWEP.Planted = false;

local hidden = true;
local close = false;
local lastclose = false;

if CLIENT then
    function SWEP:DrawHUD( )

            local shotttext = "Shootmode: "..self:GetNWString("shootmode").."\nE+R to change"
            surface.SetFont( "ChatFont" );
            local size_x, size_y = surface.GetTextSize( shotttext );

            draw.RoundedBox( 6, ScrW( ) / 2 - size_x / 2 - 5, ScrH( ) - 150, size_x + 15, size_y + 10, Color( 0, 0, 0, 150 ) );
            draw.DrawText( shotttext , "ChatFont", ScrW( ) / 2, ScrH( ) - 150 + 5, Color( 255, 30, 30, 255 ), TEXT_ALIGN_CENTER );

        if  self.hadBomb < 5 then
            local planted = self:GetNWBool( "Planted" );
            local tr = LocalPlayer( ):GetEyeTrace( );
            local close2;

            if tr.Entity:IsPlayer( ) and LocalPlayer( ):GetPos( ):Distance( tr.Entity:GetPos( ) ) <= 99999999 then
                close2 = true;
            else
                close2 = false;
            end;

            local planted_text = "Not Planted!";
            local close_text = "Nobody is in range!";

            if planted then

                planted_text = "Planted!\nE+LMB to Explode!";

                surface.SetFont( "ChatFont" );
                local size_x, size_y = surface.GetTextSize( planted_text );

                draw.RoundedBox( 6, ScrW( ) / 2 - size_x / 2 - 5, ScrH( ) - 100, size_x + 15, size_y + 10, Color( 0, 0, 0, 150 ) );
                draw.DrawText( planted_text, "ChatFont", ScrW( ) / 2, ScrH( ) - 100 + 5, Color( 255, 30, 30, 255 ), TEXT_ALIGN_CENTER );
            else
                if close2 then
                    close_text = "Close Enough!\nE+LMB stick!!";
                end;

                surface.SetFont( "ChatFont" );
                local size_x, size_y = surface.GetTextSize( planted_text );

                surface.SetFont( "ChatFont" );
                local size_x2, size_y2 = surface.GetTextSize( close_text );

                draw.RoundedBox( 6, ScrW( ) / 2 - ( size_x2 / 2 ) - 7.5, ScrH( ) - 100, size_x2 + 20, size_y + size_y2 + 10, Color( 0, 0, 0, 150 ) );
                draw.DrawText( planted_text, "ChatFont", ScrW( ) / 2, ScrH( ) - 100 + 5, Color( 30, 30, 255, 255 ), TEXT_ALIGN_CENTER );
                draw.DrawText( close_text, "ChatFont", ScrW( ) / 2 , ScrH( ) - 100 + size_y + 5, Color( 255, 255, 255, 255 ), TEXT_ALIGN_CENTER );
            end;
        end;
    end;
end;



SWEP.BeepSound = Sound( "C4.PlantSound" );

SWEP.ClipSet = false

function SWEP:Think( )
    local ply = self.Owner;
    if not IsValid( ply ) or self.hadBomb > 4 then return; end;

    local tr = ply:GetEyeTrace( );

    if tr.Entity:IsPlayer( ) and ply:GetPos( ):Distance( tr.Entity:GetPos( ) ) <= 99999999 and GetRoundState() == ROUND_ACTIVE then
        close = true;
    else
        close = false;
    end;


end;



function SWEP:StickIt()

    --self.Weapon:SetNextSecondaryFire( CurTime() + self.Secondary.Delay )

    local ply = self.Owner;
    if not IsValid( ply ) then return; end;

    local tr = ply:GetEyeTrace(MASK_SHOT);

    if not self:GetNWBool( "Planted" ) then

        if close then
            self.hadBomb = self.hadBomb + 1
            if SERVER then
                self.PlantedPly = tr.Entity
                self:SetNWBool("Planted", true)
            end;
        end;
    end;
end


function SWEP:DoSplode( owner, plantedply, bool )

    self:SetNWBool( "Planted", false )

    timer.Simple( 1, function( ) plantedply:EmitSound( self.BeepSound ); end );
    timer.Simple( 1.5, function( ) plantedply:EmitSound( self.BeepSound ); end );
    timer.Simple( 2, function( ) plantedply:EmitSound( self.BeepSound ); end );
    timer.Simple( 2.5, function( )
        local effectdata = EffectData()
        effectdata:SetOrigin(plantedply:GetPos())
        util.Effect("HelicopterMegaBomb", effectdata)

        local explosion = ents.Create("env_explosion")
        explosion:SetOwner(self.Owner)
        explosion:SetPos(plantedply:GetPos( ))
        explosion:SetKeyValue("iMagnitude", "45")
        explosion:SetKeyValue("iRadiusOverride", 0)
        explosion:Spawn()
        explosion:Activate()
        explosion:Fire("Explode", "", 0)
        explosion:Fire("kill", "", 0)
        self:SetNWBool( "Planted", false )



    end );




end;

function SWEP:BombSplode()
    local ply = self.Owner;
    if not IsValid( ply ) then return; end;

    if IsValid( self.PlantedPly ) and self.PlantedPly:Alive( ) then
        if SERVER then
            self:Blind(3,self.PlantedPly)
            self:DoSplode( self.Owner, self.PlantedPly, false );
        end;
    else
        if SERVER then
            if IsValid( self.PlantedPly ) and IsValid( self.PlantedPly.server_ragdoll ) then
                self:DoSplode( self.Owner, self.PlantedPly.server_ragdoll, true );
            else
                ply:PrintMessage( HUD_PRINTTALK, "The body this bomb has been planted to is no longer available!" );
            end;
        end;
        --  self.Weapon:SendWeaponAnim( ACT_SLAM_DETONATOR_DETONATE );
    end;
end
