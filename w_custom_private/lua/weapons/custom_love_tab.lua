
SWEP.Spawnable			= true
SWEP.AdminSpawnable		= true
SWEP.ViewModelFOV 	= 70
SWEP.ViewModel			= "models/weapons/v_diamond_mc_pickaxe.mdl"
SWEP.WorldModel			= "models/weapons/w_diamond_mc_pickaxe.mdl"
SWEP.HoldType = "melee"


SWEP.FiresUnderwater = true
SWEP.Primary.Damage         = 30
SWEP.Base					= "aa_base"
SWEP.Primary.ClipSize		= -1
SWEP.Primary.DefaultClip	= -1
SWEP.Primary.Automatic		= true
SWEP.Primary.Ammo			= "none"
SWEP.Primary.Delay = 0.6
SWEP.ViewModelFlip		= false

SWEP.Weight				= 5
SWEP.AutoSwitchTo		= false
SWEP.AutoSwitchFrom		= false

SWEP.Category			= "Minecraft"
SWEP.PrintName			= "Love Tap"
SWEP.Slot				= 0
SWEP.SlotPos			= 1
SWEP.DrawAmmo			= false
SWEP.DrawCrosshair		= true

SWEP.Kind = WEAPON_MELEE

function SWEP:OnDrop()
    self:Remove()
end

SWEP.NextSecond = 0
function SWEP:SecondaryAttack()
    if self.Owner:KeyDown(IN_USE) then
        self.Owner.dontkilllovetab = true
        self.Owner:PrintMessage( HUD_PRINTCENTER, "You turned off your heart" )
    end
    if self.Owner:KeyDown(IN_RELOAD) then
        self.Owner.dontkilllovetab = false
        self.Owner:PrintMessage( HUD_PRINTCENTER, "You enabled your heart" )
    end
    if self.NextSecond > CurTime() or CLIENT then return end
    self.NextSecond = CurTime() + 5

    local number = math.random(0,100)
    if number > 0 and number < 20 then
        self:Invisible()
    end
    if number < 40 and number > 19 then
        self:Playerscale()
    end
    if number < 60 and number > 39 then
        self:Lightningstrike()
    end
    if number < 80 and number > 59 then
        self:Kiss()
    end
    if number < 101 and number > 79 then
        self:Poison()
    end
end

function SWEP:Reload()
    self:ThrowLikeKnife("random_knifething_lovetab", 1, 50)
end

function SWEP:Invisible()
    local owner = self.Owner:GetEyeTrace().Entity
    if not owner then return end
    owner.Material = owner:GetMaterial()
    timer.Create(self.Owner:SteamID().."loveinvisible",3,1,function()
        if IsValid(owner) and owner.Material then
            owner:SetMaterial( owner.Material )
            owner.isdisguised = false
        end
    end)
    owner:SetMaterial("Models/effects/vol_light001")
    owner.isdisguised =true
end
function SWEP:Playerscale()
    local owner = self.Owner:GetEyeTrace().Entity
    if not owner then return end
    owner.Material = owner:GetMaterial()
    timer.Create(self.Owner:SteamID().."loveinscale",5,1,function()
        if IsValid(owner) and owner.Material then
            owner:SetModelScale(1, 4)
            owner:SetViewOffset(Vector(0,0,65))
        end
    end)
    owner:SetModelScale(0.5, 0.1)
    owner:SetViewOffset(Vector(0,0,30))
end
function SWEP:Lightningstrike()
    local owner = self.Owner:GetEyeTrace().Entity
    if not owner then return end

    local edata = EffectData()

    edata:SetEntity(owner)
    edata:SetMagnitude(10)
    edata:SetScale(25)

    util.Effect("TeslaHitBoxes", edata)
    util.Effect("ManhackSparks", edata)
    owner:EmitSound( "weapons/gb_weapons/zeus_thunder.mp3", 500 )

    timer.Create("SparkiesForZeus",1,3,function()
        if not IsValid( owner ) then  return end
        edata:SetEntity(owner)
        edata:SetMagnitude(10)
        edata:SetScale(25)

        util.Effect("TeslaHitBoxes", edata)
        util.Effect("ManhackSparks", edata)
    end)

    timer.Create("EndBOOM",4.5,1,function()
        if not IsValid( owner ) then  return end
        edata:SetEntity(owner)
        edata:SetMagnitude(50)
        edata:SetScale(256)
        ent:EmitSound( "weapons/gb_weapons/zeus_thunder.mp3", 510 )
        util.Effect("TeslaHitBoxes", edata)
        util.Effect("ManhackSparks", edata)
    end)
    local weapon = self.Owner:GetActiveWeapon()
    timer.Create("infectedDamagezeus",0.25,20,function()
        if IsValid( self.Owner ) then
            owner:TakeDamage( 3, self.Owner, weapon or self)
        end
    end)

end

function SWEP:Kiss()
    local owner = self.Owner:GetEyeTrace().Entity
    if not owner then return end
    timer.Create("giveHEalthHestia"..self.Owner:SteamID(),1,10,function()
        if IsValid(owner) and owner:Alive()  then
            if( owner:Health() < 150) then
                owner:SetHealth( owner:Health() + 10)
            end
            owner:EmitSound("weapons/gb_weapons/hestias_kiss_of_enlightment.mp3")
        end
    end)
end

function SWEP:Poison()
    local owner = self.Owner:GetEyeTrace().Entity
    if not owner then return end

    local weapon = self.Owner:GetActiveWeapon()
    timer.Create("infectedDamagezeus"..self.Owner:SteamID(),1,20,function()
        if IsValid( self.Owner ) and IsValid(owner) then
            owner:TakeDamage( 3, self.Owner, weapon or self )
        end
    end)

end


function SWEP:Deploy()
    local function playerDiesSound( victim, weapon, killer )
        if( IsValid(killer) and killer:IsPlayer()  ) then
            local wep = killer:GetActiveWeapon()
            if IsValid(wep) and not killer.dontkilllovetab then
                wep = wep:GetClass()
                if wep == "custom_love_tab" then
                   if math.random(0,100) < 20 then
                       killer:PrintMessage( HUD_PRINTCENTER, "You felt guilty and killed yourself" )
                       killer:Kill()
                   end
                end
            end
        end
    end

    hook.Add( "PlayerDeath", "playerDeathSound_love_tab", playerDiesSound )
    return self.BaseClass.Deploy(self)

end

function SWEP:Initialize()
self:SetHoldType( "melee" )
end
function SWEP:PrimaryAttack()

   self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )




   local spos = self.Owner:GetShootPos()
   local sdest = spos + (self.Owner:GetAimVector() * 70)

   local kmins = Vector(1,1,1) * -10
   local kmaxs = Vector(1,1,1) * 10

   local tr = util.TraceHull({start=spos, endpos=sdest, filter=self.Owner, mask=MASK_SHOT_HULL, mins=kmins, maxs=kmaxs})

   -- Hull might hit environment stuff that line does not hit
   if not IsValid(tr.Entity) then
      tr = util.TraceLine({start=spos, endpos=sdest, filter=self.Owner, mask=MASK_SHOT_HULL})
   end

   local hitEnt = tr.Entity

   -- effects
   if IsValid(hitEnt) then
      self.Weapon:SendWeaponAnim( ACT_VM_HITCENTER )
	  

      local edata = EffectData()
      edata:SetStart(spos)
      edata:SetOrigin(tr.HitPos)
      edata:SetNormal(tr.Normal)
      edata:SetEntity(hitEnt)

      if hitEnt:IsPlayer() or hitEnt:GetClass() == "prop_ragdoll" then
         util.Effect("BloodImpact", edata)
      end
   else
      self.Weapon:SendWeaponAnim( ACT_VM_MISSCENTER )
   end

   if SERVER then
      self.Owner:SetAnimation( PLAYER_ATTACK1 )
   end
self.Weapon:SetNextPrimaryFire(CurTime() + .43)

local trace = self.Owner:GetEyeTrace()

    if trace.HitPos:Distance(self.Owner:GetShootPos()) <= 150 and SERVER then
        if math.random(0,100) < 70 then
                bullet = {}
                bullet.Num    = 1
                bullet.Src    = self.Owner:GetShootPos()
                bullet.Dir    = self.Owner:GetAimVector()
                bullet.Spread = Vector(0, 0, 0)
                bullet.Tracer = 0
                bullet.Force  = 3
                bullet.Damage = 65
                    self.Owner:DoAttackEvent()
                self.Owner:FireBullets(bullet)

                self:EmitSound("Weapon_Crowbar.Melee_Hit")
        else
          if hitEnt:IsPlayer() then
              if hitEnt:Health() < 150 then
                  hitEnt:SetHealth(hitEnt:Health() + 35)
                  if hitEnt:Health() > 150 then hitEnt:SetHealth(150)
                  end
                  self.Owner:EmitSound("NPC_Vortigaunt.SuitOn")
          end
          end
        end
    else
         self.Weapon:EmitSound("Zombie.AttackMiss")
        self.Owner:DoAttackEvent()
    end

end

local ENT = {}

ENT.Type = "anim"
ENT.Base = "base_anim"


if SERVER then

    function ENT:Initialize()

            self.heal = 10
            self.model = "models/weapons/w_diamond_gb_sword.mdl"


        self:SetModel(self.model)
        self:SetModelScale(self:GetModelScale()*1,0)

        self.Entity:PhysicsInit(SOLID_VPHYSICS);
        self.Entity:SetMoveType(MOVETYPE_VPHYSICS);
        self.Entity:SetSolid(SOLID_VPHYSICS);
        self.active = CurTime() + 0.2

        --  self.Entity:SetCollisionGroup(COLLISION_GROUP_WEAPON)
        local phys = self:GetPhysicsObject()
        if phys:IsValid() then
            phys:Wake()
            phys:EnableGravity(true);
        else
            print("Hmm")
        end
            self.model = "models/weapons/w_diamond_mc_pickaxe.mdl"
            self:SetModel(self.model)

        timer.Simple(3,function()
            if IsValid(self) then self:Remove() end
        end)
    end

    function ENT:Touch( ent )
        if ent:IsPlayer() and not ent.nextsandtouch or (ent.nextsandtouch and ent.nextsandtouch < CurTime()) then
            ent.nextsandtouch = CurTime() + 3
            ent:Ignite(0.5,0)
            local dmginfo = DamageInfo()
            dmginfo:SetDamage( 50 ) --50 damage
            dmginfo:SetDamageType( DMG_BULLET ) --Bullet damage
            dmginfo:SetAttacker( self.owner or ent ) --First player found gets credit
            dmginfo:SetDamageForce( Vector( 0, 0, 50 ) ) --Launch upwards
            dmginfo:SetInflictor(self)
            ent:TakeDamageInfo( dmginfo )
            self:Remove()
        end
    end

    function ENT:PhysicsCollide(data, phys)
        if self.Stuck then return false end

        local other = data.HitEntity
        if IsValid(other) and other:IsPlayer() then
            self:Touch(other)
            self:Remove()
        end
    end


end

scripted_ents.Register( ENT, "random_knifething_lovetab", true )


