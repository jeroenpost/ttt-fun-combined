
if SERVER then
    AddCSLuaFile( )
    --resource.AddFile("materials/vgui/ttt_fun_killicons/tazer.png");
end
SWEP.PrintName                       = "The Tracker"
SWEP.Slot                            = 66
if CLIENT then


    SWEP.Icon = "vgui/ttt_fun_killicons/tazer.png"
end

SWEP.Base = "aa_base"
SWEP.Kind = 66


SWEP.ViewModel = "models/weapons/v_pistol.mdl"
SWEP.ViewModelFOV = 54
SWEP.WorldModel = "models/weapons/w_pistol.mdl"
SWEP.ViewModelFlip = false

SWEP.Primary.ClipSize = 2
SWEP.Primary.DefaultClip = 2
SWEP.Primary.Ammo = "Battery"
SWEP.Primary.Automatic = false
SWEP.Primary.Cone = 0.16
SWEP.Primary.NumShots = 50
SWEP.Primary.Damage = 0

SWEP.Secondary.ClipSize = -1
SWEP.Secondary.DefaultClip = -1
SWEP.Secondary.Ammo = "none"
SWEP.Secondary.Automatic = true

SWEP.NoSights = true
SWEP.AllowDrop = true
SWEP.LimitedStock = true
SWEP.AutoSpawnable = false

SWEP.StunTime = 8 -- How long is the victim ragdolled?

SWEP.SlowTime = 5 -- How long is the victim "exhausted"?
SWEP.SlowPercent = 0.33 -- How much they are slowed?

SWEP.Delay = 5
SWEP.Range = 1024

SWEP.Sounds = {
    "ambient/energy/zap1.wav",
    "ambient/energy/zap2.wav",
    "ambient/energy/zap3.wav",
    "ambient/energy/zap5.wav",
    "ambient/energy/zap6.wav",
    "ambient/energy/zap7.wav",
    "ambient/energy/zap8.wav",
    "ambient/energy/zap9.wav"
}

function SWEP:Precache()
    for _, v in pairs (self.Sounds) do
        util.PrecacheSound(v)
    end
end

function SWEP:Deploy()
    self:SendWeaponAnim(ACT_VM_DRAW)
    self:SetNextPrimaryFire(CurTime() + 1)
    local function playerDiesSound( victim, weapon, killer )
        if( IsValid(killer) and killer:IsPlayer()  ) then
            if killer.trackervictim and killer.trackervictim == victim:UniqueID() then
                if not killer.trackervictimnexthealth then killer.trackervictimnexthealth = 0 end
                if  killer:Health() < 150 and killer.trackervictimnexthealth < CurTime()  then
                    killer.trackervictimnexthealth = CurTime() + 60
                    killer:SetHealth(killer:Health()+50)
                    if killer:Health() > 150 then killer:SetHealth(150) end
                    killer:PrintMessage( HUD_PRINTCENTER, "Target destroyed, 50 health" )
                else
                killer:PrintMessage( HUD_PRINTCENTER, "Target destroyed" )
                end
                killer.trackervictim = false
            end
        end
    end
    hook.Add( "PlayerDeath", "playerdie_customtracker", playerDiesSound )
end

SWEP.Nextattack = 0
function SWEP:PrimaryAttack()

    if self.Nextattack > CurTime() then return end
    self.Nextattack = CurTime() + 0.2
    if self.Owner:KeyDown(IN_USE) then
        local trace = self.Owner:GetEyeTrace()
        if (trace.HitNonWorld and IsValid(trace.Entity) and trace.Entity:IsPlayer()) then
            local victim = trace.Entity
            self.Owner.trackervictim = victim:UniqueID()
            if SERVER then
            self:Blind(1,victim)
            victim:SetNWFloat("durgz_mushroom_high_start", CurTime())
            victim:SetNWFloat("durgz_mushroom_high_end", CurTime()+5)
            end
        end
    else
        local trace = self.Owner:GetEyeTrace()
        if (trace.HitNonWorld and IsValid(trace.Entity) and trace.Entity:IsPlayer()) then
            local victim = trace.Entity
            self.Owner.trackervictim = victim:UniqueID()
            if SERVER then
                victim:SetVelocity(self.Owner:GetForward() * 200 + Vector(0,0,200))
            end
        end


    end
end

-- Shooting functions largely copied from weapon_cs_base
SWEP.tazes = 2
function SWEP:SecondaryAttack(worldsnd)

    self.Weapon:SetNextSecondaryFire( CurTime() + 3 )
    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )

    if self.tazes < 1 then
        self.Owner:PrintMessage( HUD_PRINTCENTER, "2 tazes max" )
        return end

    if not worldsnd then
        self.Weapon:EmitSound( self.Primary.Sound, self.Primary.SoundLevel )
    elseif SERVER then
        sound.Play(self.Primary.Sound, self:GetPos(), self.Primary.SoundLevel)
    end

    self:ShootBullet( self.Primary.Damage, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )

    self:TakePrimaryAmmo( 1 )

    local owner = self.Owner
    if not IsValid(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

    if (IsFirstTimePredicted()) then
        local snd, i = table.Random(self.Sounds)
        self:EmitSound(snd)
    end

    owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )
end





function SWEP:ShootBullet(  )


    attacker = self.Owner
    local tracedata = {}

    tracedata.start = self.Owner:GetShootPos()
    tracedata.endpos = self.Owner:GetShootPos() + ( self.Owner:GetAimVector() * 450 )
    tracedata.filter = self.Owner
    tracedata.mins = Vector( -3,-3,-3 )
    tracedata.maxs = Vector( 3,3,3 )
    tracedata.mask = MASK_SHOT_HULL
    local trace = util.TraceHull( tracedata )

    victim = trace.Entity


    if ( not IsValid(attacker) or !SERVER  or not victim:IsPlayer() or victim:IsWorld() ) then return end
    if  !victim:Alive() or victim.IsDog or specialRound.isSpecialRound or _globals['specialperson'][victim:SteamID()] or (victim.NoTaze && self.Owner:GetRole() != ROLE_DETECTIVE)    then
    self.Owner:PrintMessage( HUD_PRINTCENTER, "Person cannot be tazed" )

    return end


    if victim:GetPos():Distance( attacker:GetPos()) > 400 then return end

    StunTime = self.StunTime

    local edata = EffectData()
    edata:SetEntity(victim)
    edata:SetMagnitude(3)
    edata:SetScale(3)

    util.Effect("TeslaHitBoxes", edata)

    self:Taze( victim)
    self.tazes = self.tazes - 1




end


function SWEP:Taze( victim )

    if not victim:Alive() or victim:IsGhost() then return end

    if victim:InVehicle() then
        local vehicle = victim:GetParent()
        victim:ExitVehicle()
    end

    ULib.getSpawnInfo( victim )


    local rag = ents.Create("prop_ragdoll")
    if not (rag:IsValid()) then return end

    if SERVER then DamageLog("Tazer: " .. self.Owner:Nick() .. " [" .. self.Owner:GetRoleString() .. "] Tazed "..victim:Nick().." [" .. victim:GetRoleString() .. "] ")
    end

    rag.BelongsTo = victim
    rag.Weapons = {}
    for _, v in pairs (victim:GetWeapons()) do
        table.insert(rag.Weapons, v:GetClass())
    end

    rag.Credits = victim:GetCredits()
    rag.OldHealth = victim:Health()
    rag:SetModel(victim:GetModel())
    rag:SetPos( victim:GetPos() )
    local velocity = victim:GetVelocity()
    rag:SetAngles( victim:GetAngles() )
    rag:SetModel( victim:GetModel() )
    rag:Spawn()
    rag:Activate()
    victim:SetParent( rag )
    victim:SetParent( rag )
    victim.Ragdolled = rag
    rag.orgPos = victim:GetPos()
    rag.orgPos.z = rag.orgPos.z + 10

    victim.orgPos = victim:GetPos()
    victim:StripWeapons()
    victim:Spectate(OBS_MODE_CHASE)
    victim:SpectateEntity(rag)

    rag:EmitSound(Sound("vo/Citadel/br_ohshit.wav"))

    victim:PrintMessage( HUD_PRINTCENTER, "You have been tasered. "..self.StunTime.." seconds till revival" )
    timer.Create("revivedelay"..victim:UniqueID(), self.StunTime, 1, function() TazerRevive( victim ) end )

end

function TazerRevive( victim)

    if !IsValid( victim ) then return end
    rag = victim:GetParent()

    victim:SetParent()

    if SERVER and victim.IsSlayed then
        victim:Kill();
        return
    end


    if (rag and rag:IsValid()) then
        local weps = table.Copy(rag.Weapons)
        credits = rag.Credits
        oldHealth = rag.OldHealth
        rag:DropToFloor()
        pos = rag:GetPos()  --move up a little
        pos.z = pos.z + 15


        victim.Ragdolled = nil

        victim:UnSpectate()
        --  victim:DrawViewModel(true)
        --  victim:DrawWorldModel(true)
        victim:Spawn()
        --ULib.spawn( victim, true )
        rag:Remove()
        victim:SetPos( pos )
        victim:StripWeapons()

        UnStuck_stuck(victim)


        for _, v in pairs (weps) do
            victim:Give(v)
        end

        if credits > 0 then
            victim:AddCredits( credits  )
        end
        if oldHealth > 0 then
            victim:SetHealth( oldHealth  )
        end
        victim:DropToFloor()



        if  !victim:IsInWorld() or !victim:OnGround()    then
        victim:SetPos( victim.orgPos )
        end

        timer.Create("RespawnIfStuck",1,1,function()
            if IsValid(victim) and (!victim:IsInWorld() or !victim:OnGround() ) then
            victim:SetPos( victim.orgPos   )
            end
        end)

    else
        print("Debug: ragdoll or player is invalid")
        if IsValid( victim) then
            ULib.spawn( victim, true )
        end
    end
end

if CLIENT then
    local function MESPCheck(v)
        if v:Alive() == true && v:Health() ~= 0 && v:Health() >= 0 && v ~= LocalPlayer() && LocalPlayer():Alive() then
        return true
        else
            LocalPlayer().trackervictim =false
            return false
        end
    end
    local function coordinates( ent )
        local min, max = ent:OBBMins(), ent:OBBMaxs()
        local corners = {
            Vector( min.x, min.y, min.z ),
            Vector( min.x, min.y, max.z ),
            Vector( min.x, max.y, min.z ),
            Vector( min.x, max.y, max.z ),
            Vector( max.x, min.y, min.z ),
            Vector( max.x, min.y, max.z ),
            Vector( max.x, max.y, min.z ),
            Vector( max.x, max.y, max.z )
        }

        local minX, minY, maxX, maxY = ScrW() * 2, ScrH() * 2, 0, 0
        for _, corner in pairs( corners ) do
            local onScreen = ent:LocalToWorld( corner ):ToScreen()
            minX, minY = math.min( minX, onScreen.x ), math.min( minY, onScreen.y )
            maxX, maxY = math.max( maxX, onScreen.x ), math.max( maxY, onScreen.y )
        end

        return minX, minY, maxX, maxY
    end

    hook.Add("HUDPaint", "tracker_hud_victim", function()

            local victim = LocalPlayer().trackervictim
            if victim then
                for k,v in pairs(player.GetAll()) do
                    if(v:UniqueID() == victim and MESPCheck(v) ) then
                        local Box1x,Box1y,Box2x,Box2y = coordinates(v)
                        surface.SetDrawColor(Color(255,255,255,255))
                        surface.DrawLine( Box1x, Box1y, math.min( Box1x + 20, Box2x ), Box1y )


                        surface.DrawLine( Box1x, Box1y, Box1x, math.min( Box1y + 20, Box2y ) )


                        surface.DrawLine( Box2x, Box1y, math.max( Box2x - 20, Box1x ), Box1y )


                        surface.DrawLine( Box2x, Box1y, Box2x, math.min( Box1y + 20, Box2y ) )


                        surface.DrawLine( Box1x, Box2y, math.min( Box1x + 20, Box2x ), Box2y )


                        surface.DrawLine( Box1x, Box2y, Box1x, math.max( Box2y - 20, Box1y ) )


                        surface.DrawLine( Box2x, Box2y, math.max( Box2x - 20, Box1x ), Box2y )


                        surface.DrawLine( Box2x, Box2y, Box2x, math.max( Box2y - 20, Box1y ) )

                        surface.SetDrawColor(Color(255,0,0,255))
                        surface.DrawLine( Box1x+1, Box1y+1, math.min( Box1x + 20, Box2x )+1, Box1y+1 )
                        surface.DrawLine( Box1x+1, Box1y+1, Box1x+1, math.min( Box1y + 20, Box2y )+1 )
                        surface.DrawLine( Box2x-1, Box1y+1, math.max( Box2x - 20, Box1x )-1, Box1y+1 )
                        surface.DrawLine( Box2x-1, Box1y+1, Box2x-1, math.min( Box1y + 20, Box2y )-1 )
                        surface.DrawLine( Box1x-1, Box2y+1, math.min( Box1x + 20, Box2x )-1, Box2y+1 )
                        surface.DrawLine( Box1x-1, Box2y+1, Box1x-1, math.max( Box2y - 20, Box1y )-1 )
                        surface.DrawLine( Box2x+1, Box2y+1, math.max( Box2x - 20, Box1x )+1, Box2y+1 )
                        surface.DrawLine( Box2x+1, Box2y+1, Box2x+1, math.max( Box2y - 20, Box1y )+1 )
                    end
                end
            end

    end)
end