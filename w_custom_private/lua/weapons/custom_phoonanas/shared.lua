SWEP.Base = "gb_base"
SWEP.HoldType = "pistol"
SWEP.ViewModelFOV = 70.314960629921
SWEP.ViewModelFlip = false
SWEP.ViewModel = "models/weapons/cstrike/c_pist_deagle.mdl"
SWEP.WorldModel = "models/weapons/w_pistol.mdl"
SWEP.ShowViewModel = true
SWEP.ShowWorldModel = false

SWEP.UseHands = true
SWEP.Kind = WEAPON_EQUIP2

SWEP.ViewModelBoneMods = {
    ["v_weapon.Deagle_Parent"] = { scale = Vector(0.009, 0.009, 0.009), pos = Vector(0, 0, 0), angle = Angle(0, 0, 0) }
}

SWEP.WElements = {
    ["banannaaa"] = { type = "Model", model = "models/props/cs_italy/bananna.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(8.635, 0.455, -4.092), angle = Angle(3.068, -88.977, 0), size = Vector(1.116, 1.116, 1.116), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
}

SWEP.VElements = {
    ["banana"] = { type = "Model", model = "models/props/cs_italy/bananna.mdl", bone = "v_weapon.Deagle_Parent", rel = "", pos = Vector(0.455, -3.182, -0.456), angle = Angle(166.705, 0, 78.75), size = Vector(0.862, 0.862, 0.862), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
}

SWEP.Sound = { Sound("weapons/nanners/4.mp3"), Sound("weapons/nanners/5.mp3") }
SWEP.Sounds = { Sound("weapons/nanners/8.mp3"), Sound("weapons/nanners/10.mp3"),  Sound("weapons/nanners/13.mp3"),  Sound("weapons/nanners/14.mp3"),  Sound("weapons/nanners/17.mp3"),  Sound("weapons/nanners/18.mp3")}

SWEP.PrintName            = "Phoonanas"
SWEP.Slot                = 6
SWEP.SlotPos            = 1
SWEP.DrawAmmo            = true
SWEP.DrawCrosshair        = true
SWEP.Weight                = 5
SWEP.AutoSwitchTo        = false
SWEP.AutoSwitchFrom        = false
SWEP.Author            = ""
SWEP.Contact        = ""
SWEP.Purpose        = ""
SWEP.EquipMenuData = {
    type  = "item_weapon",
    name  = "Nanners Gun",
    desc  = "SAY HELLO TO MY LITTLE FRIEND"
}

SWEP.AutoSpawnable      = false
SWEP.Primary.Damage        = 7
SWEP.Primary.ClipSize        = 500
SWEP.Primary.DefaultClip    = 500
SWEP.Primary.Automatic        = true
SWEP.Primary.Ammo            = "XBowBolt"
SWEP.Primary.Delay            = 0.10
SWEP.Secondary.Delay            = 3
SWEP.Secondary.ClipSize        = 2
SWEP.Secondary.DefaultClip    = 2
SWEP.Secondary.Automatic    = false
SWEP.Secondary.Ammo            = "XBowBolt"
SWEP.AmmoEnt = ""


SWEP.SecondaryProps = {
    "models/props_c17/oildrum001_explosive.mdl",
    "models/Combine_Helicopter/helicopter_bomb01.mdl",
    "models/props_junk/TrafficCone001a.mdl",
    "models/props_junk/metal_paintcan001a.mdl",
    "models/Gibs/HGIBS.mdl",
    "models/Gibs/Fast_Zombie_Legs.mdl",
    "models/props_c17/doll01.mdl",
    "models/props_lab/huladoll.mdl",
    "models/props_lab/huladoll.mdl",
    "models/props_lab/desklamp01.mdl",
    "models/props_junk/garbage_glassbottle003a.mdl",
    "models/weapons/w_shot_gb1014.mdl",

}
SWEP.Icon = "vgui/ttt_fun_killicons/nanners_gun.png"

function SWEP:PrimaryAttack()
    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
    -- self:TakePrimaryAmmo( 1 )
    self:ShootBullet( 6, 0.02, 1, 0.003 )
    self:throw_attack ("models/props/cs_italy/bananna.mdl", Sound(table.Random( self.Sound) ), 3);
end

SWEP.Headcrabs = 0
SWEP.Headcrabs2 = 0
SWEP.NextSecond = 0

function SWEP:Reload()
    if  self.NextSecond > CurTime() then return end


    self.Weapon:SetNextSecondaryFire( CurTime() + 5 )

    self.NextSecond = CurTime() + 5
    self.Weapon:SetNextSecondaryFire( CurTime() + self.Secondary.Delay )
    --self:TakePrimaryAmmo( 10 )
    self:throw_attack ( table.Random(self.SecondaryProps) , Sound(table.Random( self.Sounds) ), 10);
end





function SWEP:SecondaryAttack()
    if( self.Headcrabs > 1 ) then
        self:Reload()
    return end
    if  self.NextSecond > CurTime() then return end
    if not self:CanPrimaryAttack() then return end
    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
    self.Weapon:SetNextSecondaryFire( CurTime() + 5 )

    self.NextSecond = CurTime() + 5






    if  not SERVER then return end


    self.Owner:EmitSound( "weapons/nanners/10.mp3" )

    timer.Simple(3, function()
        if not IsValid(self) or not IsValid(self.Owner) then return end

        local tr = self.Owner:GetEyeTrace()


        if self.Headcrabs2 == 0 then
            local ent = ents.Create( "npc_clawscanner" )
            ent:SetPos( tr.HitPos + self.Owner:GetAimVector() * 1 )
            ent:SetAngles( tr.HitNormal:Angle() )
            ent:SetHealth( 300 )
            ent:SetPhysicsAttacker(self.Owner)
            ent:Spawn()
            ent:Ignite(100,100)
            ent:EmitSound( "weapons/nanners/10.mp3" )

        else

            local ent = ents.Create( "npc_manhack" )
            ent:SetPos( tr.HitPos + self.Owner:GetAimVector() * 1 )
            ent:SetAngles( tr.HitNormal:Angle() )
            ent:SetHealth( 1 )
            ent:SetPhysicsAttacker(self.Owner)
            ent:Spawn()
            ent:EmitSound( "weapons/nanners/13.mp3" )
            if  self.Owner:GetRoleString() ~= "traitor" then
                ent:SetKeyValue("sk_manhack_melee_dmg", 0)
            end

            local turtle = ents.Create("prop_dynamic")
            turtle:SetModel("models/props_c17/doll01.mdl")
            turtle:SetPos(ent:GetPos())
            turtle:SetAngles(Angle(0,-90,0))
            turtle:SetModelScale( turtle:GetModelScale() * 2, 0.05)
            turtle:SetParent(ent)
            turtle:SetHealth(1)

            if math.Rand( 1,5) == 4 then
                turtle:Ignite(100,100)
                turtle:SetHealth(75)
            end

            --headturtle:SetCollisionGroup(COLLISION_GROUP_WEAPON)

            ent:SetNWEntity("Thrower", self.Owner)
            --headturtle:SetName(self:GetThrower():GetName())
            ent:SetNoDraw(true)
            ent:SetHealth(15)
            timer.Simple(16,function()
                if IsValid( ent ) then
                    self:ReplaySound( ent )
                end

            end)

        end
        self.Headcrabs2 = self.Headcrabs2 +1
    end)
    self.Headcrabs = self.Headcrabs +1


end

function SWEP:ReplaySound( ent )
    if not IsValid( ent ) then return end
    ent:EmitSound(  "weapons/nanners/13.mp3"  )
    timer.Simple(16,function()
        if IsValid( ent ) then
            self:ReplaySound( ent )
        end

    end)
end



function SWEP:throw_attack (model_file, sound, away)
    local tr = self.Owner:GetEyeTrace();

    if IsValid( tr.Entity )  then
        if not tr.Entity.hasProtectionSuit then
            tr.Entity.hasProtectionSuit= true
            timer.Simple(5, function()
                if not IsValid( tr.Entity )  then return end
                tr.Entity.hasProtectionSuit = false
            end)
        end
    end

    self.BaseClass.ShootEffects (self);
    if (!SERVER) then return end;
    local ent = ents.Create ("prop_physics");
    ent:SetModel (model_file);
    ent:SetPos (self.Owner:EyePos() + (self.Owner:GetAimVector() * 16));
    ent:SetAngles (self.Owner:EyeAngles());
    ent:SetPhysicsAttacker(self.Owner)
    ent:Spawn();
    ent:EmitSound( sound )
    if math.Rand(1,5) == 3 then
        ent:Ignite(100,100)
    end
    local phys = ent:GetPhysicsObject();
    local shot_length = tr.HitPos:Length();
    phys:ApplyForceCenter (self.Owner:GetAimVector():GetNormalized() * math.pow (shot_length, 3));

    timer.Simple(away,function()
        if IsValid(ent) then
            ent:Remove()
        end
    end)

    cleanup.Add (self.Owner, "props", ent);
    undo.Create ("Thrown model");
    undo.AddEntity (ent);
    undo.SetPlayer (self.Owner);
    undo.Finish();
end