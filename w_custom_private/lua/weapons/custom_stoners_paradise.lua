SWEP.Base = "gb_camo_base"
SWEP.ViewModelFOV	= 54
SWEP.HoldType = "ar2"
SWEP.PrintName = "Stoner's Paradise"
SWEP.ViewModel		= "models/weapons/cstrike/c_snip_scout.mdl"
SWEP.WorldModel		= "models/weapons/w_snip_awp.mdl"


SWEP.HoldType = "ar2"
SWEP.Slot = 1
SWEP.CustomCamo = true
SWEP.Camo = 16

if CLIENT then

    SWEP.Author = "GreenBlack"

    SWEP.Icon = "vgui/ttt_fun_killicons/awp.png"
end

SWEP.HoldType			= "ar2"

SWEP.Primary.Delay       = 1.0
SWEP.Primary.Recoil      = 5.0
SWEP.Primary.Automatic   = false
SWEP.Primary.Damage      = 75
SWEP.Primary.Cone        = 0.0005
SWEP.Primary.Ammo        = "XBowBolt"
SWEP.Primary.ClipSize    = 50
SWEP.Primary.ClipMax     = 50
SWEP.Primary.DefaultClip = 50
SWEP.Primary.Sound       = Sound( "weapons/awp/awp1.wav" )

SWEP.Tracer = 1

SWEP.Secondary.Sound = Sound("Default.Zoom")

SWEP.IronSightsPos      = Vector( 5, -15, -2 )
SWEP.IronSightsAng      = Vector( 2.6, 1.37, 3.5 )


SWEP.Kind = WEAPON_PISTOL
SWEP.AutoSpawnable = false
SWEP.AmmoEnt = "item_ammo_357_ttt"

SWEP.InLoadoutFor = nil
SWEP.LimitedStock = false
SWEP.AllowDrop = true
SWEP.IsSilent = false
SWEP.NoSights = false

SWEP.NextHealth = 0
function SWEP:Reload()

    if self.Owner:KeyDown(IN_USE)then
        if self.NextHealth < CurTime() then
            self:throw_attack ("stoners_health",   100)
            self.NextHealth = CurTime() + 40
        end
        return
    end
    if self.Owner:KeyDown(IN_ATTACK2) then
        self:DropHealth("draino_health_station")
        return
    end

    return false
end

function SWEP:DryFire(setnext)
    if CLIENT and LocalPlayer() == self.Owner then
        self.Owner:EmitSound( "Weapon_Pistol.Empty" )
    end

    setnext(self, CurTime() +0.05)
end

function SWEP:SetZoom(state)
    if CLIENT or not IsValid(self.Owner) then
        return
    else
        if state then
            self.Owner:SetFOV(20, 0.3)
        else
            self.Owner:SetFOV(0, 0.2)
        end
    end
end

SWEP.NextSound = 0
SWEP.NextHealth3 = 0
-- NormalPrimaryAttack
function SWEP:PrimaryAttack(worldsnd)

    if self.Owner:KeyDown(IN_USE)then
        if self.NextHealth3 < CurTime() then
            self:throw_attack ("stoners_healthgrenade",   100)
            self.NextHealth3 = CurTime() + 40
        end
        return
    end

    self.Weapon:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )

    if not self:CanPrimaryAttack() then return end

   self:Disorientate()

    if not worldsnd then
        self.Owner:EmitSound( self.Primary.Sound, self.Primary.SoundLevel )
    elseif SERVER then
        sound.Play(self.Primary.Sound, self.Owner:GetPos(), self.Primary.SoundLevel)
    end

    if self.NextSound < CurTime() then
        local soundfile = gb_config.websounds.."runbitch.mp3"
        gb_PlaySoundFromServer(soundfile, self.Owner)
        self.NextSound = CurTime() + 3
    end

    self.tracerColor = Color(0,255,0,255)

    self:ShootBullet( self.Primary.Damage, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )




    self:TakePrimaryAmmo( 1 )

    local owner = self.Owner
    if not IsValid(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

    owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )
end

SWEP.NextHealth =0
function SWEP:Think()

end


-- Add some zoom to ironsights for this gun
function SWEP:SecondaryAttack()
    if self.Owner:KeyDown(IN_RELOAD) then return end
    if self.Owner:KeyDown(IN_USE) then
        if not self.IronSightsPos then return end
        if self.Weapon:GetNextSecondaryFire() > CurTime() then return end

        bIronsights = not self:GetIronsights()

        self:SetIronsights( bIronsights )

        if SERVER then
            self:SetZoom(bIronsights)
        else
            self:EmitSound(self.Secondary.Sound)
        end
        return
    end

    self.Weapon:SetNextSecondaryFire( CurTime() + 1.1)
    self.Weapon:SetNextPrimaryFire( CurTime() + 1.1) --Set a long delay to prevent people from quickly scoping in and shooting.


    if not worldsnd then
        self.Weapon:EmitSound( self.Primary.Sound, self.Primary.SoundLevel )
    elseif SERVER then
        sound.Play(self.Primary.Sound, self:GetPos(), self.Primary.SoundLevel)
    end

    local soundfile = gb_config.websounds.."runbitch.mp3"
    gb_PlaySoundFromServer(soundfile, self.Owner)

    self:Disorientate()
    self.tracerColor = Color(0,255,0,255)

    self:ShootBullet( 8, 4, 17, 0.09 )


end



function SWEP:ShootBullet( dmg, recoil, numbul, cone )

    self:SendWeaponAnim(self.PrimaryAnim)

    self.Owner:MuzzleFlash()
    self.Owner:SetAnimation( PLAYER_ATTACK1 )

    if not IsFirstTimePredicted() then return end

    local sights = self:GetIronsights()

    numbul = numbul or 1
    cone   = cone   or 0.01

    local bullet = {}
    bullet.Num    = numbul
    bullet.Src    = self.Owner:GetShootPos()
    bullet.Dir    = self.Owner:GetAimVector()
    bullet.Spread = Vector( cone, cone, 0 )
    bullet.Tracer = 1
    bullet.TracerName = "manatrace"
    bullet.Force  = 10
    bullet.Damage = dmg

    self.Owner:FireBullets( bullet )

    -- Owner can die after firebullets
    if (not IsValid(self.Owner)) or (not self.Owner:Alive()) or self.Owner:IsNPC() then return end

    if ((game.SinglePlayer() and SERVER) or
            ((not game.SinglePlayer()) and CLIENT and IsFirstTimePredicted())) then

        -- reduce recoil if ironsighting
        recoil = sights and (recoil * 0.6) or recoil

        local eyeang = self.Owner:EyeAngles()
        eyeang.pitch = eyeang.pitch - recoil
        self.Owner:SetEyeAngles( eyeang )
    end

end

function SWEP:PreDrop()
    self:SetZoom(false)
    self:SetIronsights(false)
    return self.BaseClass.PreDrop(self)
end

function SWEP:Holster()
    self:SetIronsights(false)
    self:SetZoom(false)
    return true
end

if CLIENT then
    local scope = surface.GetTextureID("sprites/scope")
    function SWEP:DrawHUD()
        if self:GetIronsights() then
            surface.SetDrawColor( 0, 0, 0, 255 )

            local x = ScrW() / 2.0
            local y = ScrH() / 2.0
            local scope_size = ScrH()

            -- crosshair
            local gap = 80
            local length = scope_size
            surface.DrawLine( x - length, y, x - gap, y )
            surface.DrawLine( x + length, y, x + gap, y )
            surface.DrawLine( x, y - length, x, y - gap )
            surface.DrawLine( x, y + length, x, y + gap )

            gap = 0
            length = 50
            surface.DrawLine( x - length, y, x - gap, y )
            surface.DrawLine( x + length, y, x + gap, y )
            surface.DrawLine( x, y - length, x, y - gap )
            surface.DrawLine( x, y + length, x, y + gap )


            -- cover edges
            local sh = scope_size / 2
            local w = (x - sh) + 2
            surface.DrawRect(0, 0, w, scope_size)
            surface.DrawRect(x + sh - 2, 0, w, scope_size)

            surface.SetDrawColor(255, 0, 0, 255)
            surface.DrawLine(x, y, x + 1, y + 1)

            -- scope
            surface.SetTexture(scope)
            surface.SetDrawColor(255, 255, 255, 255)

            surface.DrawTexturedRectRotated(x, y, scope_size, scope_size, 0)

        else
            return self.BaseClass.DrawHUD(self)
        end
    end

    function SWEP:AdjustMouseSensitivity()
        return (self:GetIronsights() and 0.2) or nil
    end
end


local ENT = {}

ENT.Type = "anim"
ENT.Base = "base_anim"


if SERVER then

    function ENT:Initialize()


        self:SetModel('models/props_junk/garbage_plasticbottle002a.mdl')
        self:SetModelScale(self:GetModelScale()*1,0)

        self.Entity:PhysicsInit(SOLID_VPHYSICS);
        self.Entity:SetMoveType(MOVETYPE_VPHYSICS);
        self.Entity:SetSolid(SOLID_VPHYSICS);
        self.Entity:SetCollisionGroup(COLLISION_GROUP_PROJECTILE)
        self.Damage = 1
        local phys = self:GetPhysicsObject()
        if phys:IsValid() then
            phys:Wake()
            phys:EnableGravity(true);
        end
        self.spawned = CurTime()



    end

    function ENT:Use( activator, caller )
        if self.spawned + 3 < CurTime() then
            self.Entity:Remove()
            if ( activator:IsPlayer() ) then
                if activator:Health() < 150 then
                    if self.lower then
                        activator:SetHealth(activator:Health()+math.Rand(20,70))
                    else
                        activator:SetHealth(activator:Health()+math.Rand(20,70))
                    end
                    if activator:Health() > 150 then activator:SetHealth(150) end
                end
                activator:EmitSound("crisps/eat.mp3")
            end
        end
    end


end

scripted_ents.Register( ENT, "stoners_health", true )


local ENT = {}

ENT.Type = "anim"
ENT.Base = "base_anim"

function ENT:PhysicsCollide(data,phys)
    if data.Speed > 50 then
        self.Entity:EmitSound(Sound("Flashbang.Bounce"))
    end

    local impulse = -data.Speed * data.HitNormal * .4 + (data.OurOldVelocity * -.6)
    phys:ApplyForceCenter(impulse)
end


function ENT:Initialize()

    self.Entity:SetModel("models/weapons/w_eq_smokegrenade.mdl")
    self.Entity:PhysicsInit( SOLID_VPHYSICS )
    self.Entity:SetMoveType( MOVETYPE_VPHYSICS )
    self.Entity:SetSolid( SOLID_VPHYSICS )
    self.Entity:DrawShadow( false )

    self.Entity:SetCollisionGroup( COLLISION_GROUP_WEAPON )

    local phys = self.Entity:GetPhysicsObject()

    if (phys:IsValid()) then
        phys:Sleep()
    end

    self.timer = CurTime() + 1
    self.solidify = CurTime() + 1
    self.Bastardgas = nil
    self.Spammed = false


end
local function Poison(ply)

end

function ENT:Think()
    if CLIENT then return end
    if (IsValid(self.Owner)==false) and SERVER then
        self.Entity:Remove()
    end
    if (self.solidify<CurTime()) then
        self.SetOwner(self.Entity)
    end
    if self.timer < CurTime() then
        if !IsValid(self.Bastardgas) && !self.Spammed then
        self.Spammed = true
        self.Bastardgas = ents.Create("env_smoketrail")
        self.Bastardgas:SetPos(self.Entity:GetPos())
        self.Bastardgas:SetKeyValue("spawnradius","256")
        self.Bastardgas:SetKeyValue("minspeed","0.5")
        self.Bastardgas:SetKeyValue("maxspeed","2")
        self.Bastardgas:SetKeyValue("startsize","16536")
        self.Bastardgas:SetKeyValue("endsize","256")
        self.Bastardgas:SetKeyValue("endcolor","0 0 0")
        self.Bastardgas:SetKeyValue("startcolor","20 255 20")
        self.Bastardgas:SetKeyValue("opacity","1")
        self.Bastardgas:SetKeyValue("spawnrate","15")
        self.Bastardgas:SetKeyValue("lifetime","10")
        self.Bastardgas:SetParent(self.Entity)
        self.Bastardgas:Spawn()
        self.Bastardgas:Activate()
        self.Bastardgas:Fire("turnon","", 0.1)
        local exp = ents.Create("env_explosion")
        exp:SetKeyValue("spawnflags",461)
        exp:SetPos(self.Entity:GetPos())
        exp:Spawn()
        exp:Fire("explode","",0)
        self.Entity:EmitSound(Sound("BaseSmokeEffect.Sound"))

        end

        local pos = self.Entity:GetPos()
        local maxrange = 256
        local maxstun = 2
        for k,v in pairs(player.GetAll()) do
            --if v.GasProtection then continue end
            local plpos = v:GetPos()
            local dist = -pos:Distance(plpos)+maxrange
            if (pos:Distance(plpos)<=maxrange) and not v:IsGhost() then
                local trace = {}
                trace.start = self.Entity:GetPos()
                trace.endpos = v:GetPos()+Vector(0,0,24)
                trace.filter = { v, self.Entity }
                trace.mask = COLLISION_GROUP_PLAYER
                tr = util.TraceLine(trace)
                if (tr.Fraction==1) then
                    local stunamount = math.ceil(dist/(maxrange/maxstun))
                    v:ViewPunch( Angle( stunamount*((math.random()*6)-2), stunamount*((math.random()*6)-2), stunamount*((math.random()*4)-1) ) )
                    --Poison(v, stunamount, self.Owner, self.Owner:GetWeapon("weapon_gasgrenade"))
                    if v:Health() < 150 then
                        v:SetHealth(v:Health() + 10)
                    end
                end
            end
            end
            if (self.timer+5<CurTime()) then
                if IsValid(self.Bastardgas) then
                    self.Bastardgas:Remove()
                end
            end
            if (self.timer+10<CurTime()) then
                self.Entity:Remove()
            end
            self.Entity:NextThink(CurTime()+0.5)
            return true
        end
    end


scripted_ents.Register( ENT, "stoners_healthgrenade", true )