

if ( SERVER ) then

	

	
end
SWEP.PrintName			= "Royal's Mirana"
if ( CLIENT ) then

				
	SWEP.Author				= "Babel Industries/TS Industries"
    SWEP.Contact            = "";
    SWEP.Instructions       = "After years of developing a trouble-free shotgun with a balance of firerate and lethalness, the TS Industries Bulk Cannon is garunteed to destroy anything in your path*. (*Anything that's organic/breakable.)"
	SWEP.Category =         "Babel Industries"

SWEP.WElements = {
	["coll"] = { type = "Model", model = "models/weapons/w_shot_gb1014.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(0.455, 2.273, 0), angle = Angle(180, 174.886, 0), size = Vector(1,1,1), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	}


	
end

SWEP.Base				= "aa_base"

SWEP.Icon = "vgui/ttt_fun_killicons/spaceshotgun.png"
SWEP.Kind = 601
SWEP.Spawnable			= true
SWEP.AdminSpawnable		= true
SWEP.AutoSpawnable      = false
   SWEP.Slot				= 7
   SWEP.SlotPos			= 0

SWEP.HoldType = "rpg"
SWEP.ViewModelFOV = 60

SWEP.ViewModelFlip = false
SWEP.ViewModel = "models/weapons/v_shot_gb1014.mdl"
SWEP.WorldModel = "models/weapons/w_shotgun.mdl"
SWEP.ShowViewModel = true
SWEP.ShowWorldModel = false
SWEP.ViewModelBoneMods = {}

function SWEP:OnDrop()
    self:Remove()
end

function SWEP:Deploy()
         self.WorldModel = "models/Items/AR2_Grenade.mdl" 
	self.Owner:EmitSound( "weapons/bulkcannon/draw2.mp3" ) ;
	self.Weapon:SendWeaponAnim( ACT_VM_DRAW )
    self:SetDeploySpeed( self.Weapon:SequenceDuration(0.2) )
         self.Owner:SetNWInt("speedmul", 320)
	return true;
end

function SWEP:IsEquipment() 
	return false
end

SWEP.Weight				= 5
SWEP.AutoSwitchTo		= false
SWEP.AutoSwitchFrom		= false
SWEP.Tracer				= 4

SWEP.AmmoEnt = "item_box_buckshot_ttt"
SWEP.Primary.Sound			= Sound( "weapons/bulkcannon/fire.mp3" )
SWEP.Primary.Recoil			= 1
SWEP.Primary.Damage			= 3
SWEP.Primary.NumShots		= 15
SWEP.Primary.Cone			= 0.083
SWEP.Primary.ClipSize		= 5000
SWEP.Primary.ClipMax 		= 5000
SWEP.Primary.Delay			= 0.24
SWEP.Primary.DefaultClip	= 5000
SWEP.Primary.Automatic		= true
SWEP.Primary.Ammo			= "buckshot"

SWEP.Secondary.ClipSize		= -1
SWEP.Secondary.DefaultClip	= -1
SWEP.Secondary.ClipMax = -1
SWEP.Secondary.Automatic	= false
SWEP.Secondary.Ammo			= "none"
SWEP.HeadshotMultiplier = 1

SWEP.IronSightsPos = Vector(-4.711, -4.427, 1.049)
SWEP.IronSightsAng = Vector(0.268, 0.768, 0)
 
SWEP.RunArmOffset = Vector(0.984, -5.246, -3.08)
SWEP.RunArmAngle  = Vector(24.097, 46.474, 4.59) 

SWEP.numshott = 0
SWEP.NExtSHot = 0
function SWEP:PrimaryAttack()

    if self.Owner:KeyDown(IN_USE) then
        if self.NExtSHot > CurTime() then return end
        self.NExtSHot = CurTime() + 0.09
        self:ShootBullet2(0,1,0)
        return
    end

    if self:Clip1() > 0 then
        self.numshott = self.numshott + 1
        if self.numshott > 30 then self.numshott = 0 end

        if self.numshott < 10 then  self:Disorientate()
        elseif self.numshott < 20 then self:Blind()
        elseif self.numshott < 30 then self:Freeze() end

    end
    self.BaseClass.PrimaryAttack(self)
end
SWEP.hideblind = true
SWEP.LaserColor = Color(255,0,0,255)
function SWEP:ShootBullet2( damage, num_bullets, aimcone )

    local bullet = {}
    bullet.Num 		= num_bullets
    bullet.Src 		= self.Owner:GetShootPos()
    bullet.Dir 		= self.Owner:GetAimVector()
    bullet.Spread 	= Vector( 0.001, 0.001, 0 )
    bullet.Tracer	= 1
    bullet.Force	= 10
    bullet.Damage	= 10
    bullet.AmmoType = "Pistol"
    bullet.HullSize = 2
    bullet.TracerName = "LaserTracer_thick"

    self.Owner:FireBullets( bullet )
    self:ShootEffects()

end

function SWEP:SecondaryAttack()
    self:Fly()
end

function SWEP:OnRemove()
	self:Holster()
end

function SWEP:OnDrop()
	self:Holster()
    self:Remove()
end
SWEP.NextThinky = 0
/*---------------------------------------------------------
	Reload does nothing
---------------------------------------------------------*/
SWEP.NextHealthDrop = 0
function SWEP:Reload()
    if CLIENT then return end
    if (!SERVER) then return end
    self.Weapon:SendWeaponAnim( ACT_VM_MISSCENTER )
    if self.NextHealthDrop > CurTime() then return end
    self.NextHealthDrop = CurTime() + 30

    local ent = ents.Create("sent_paprika")
    local pos = self.Owner:GetShootPos()
    ent:SetPos(pos)
    ent:SetAngles(self.Owner:EyeAngles())
    ent:SetPhysicsAttacker( self.Owner )
    --ent.owner = self
    ent:Spawn()
    ent:Activate()
    ent:SetOwner(self.Owner)
    ent:SetPhysicsAttacker(self.Owner)

    ent.damage = self.Primary.Damage
    ent.heal = 50
    ent.poison = false
    ent.poisondamage = 10
    ent.poisontimes = 3
    ent.owner = self.Owner


    local tr = self.Owner:GetEyeTrace()

    local phys = ent:GetPhysicsObject()
    if (phys:IsValid()) then
        phys:Wake()
        phys:SetMass(1)
        phys:EnableGravity( true )
        phys:ApplyForceCenter( self:GetForward() *2000 )
        phys:SetBuoyancyRatio( 0 )
    end

    local shot_length = tr.HitPos:Length()
    ent:SetVelocity(self.Owner:GetAimVector():GetNormalized() *  math.pow(shot_length, 3))



end

/*---------------------------------------------------------
   Think does nothing
---------------------------------------------------------*/
function SWEP:Think()
        if CLIENT then
            if IsValid(self.Owner) && CLIENT && (input.IsKeyDown(KEY_Q))  then
                self.WorldModel = "models/weapons/w_shot_gb1014.mdl"
                self.NextThinky = CurTime() + 2 
            elseif CurTime() > self.NextThinky  && IsValid(self.Owner) && self.Owner:GetActiveWeapon() == self  then
                self.WorldModel = "models/Items/AR2_Grenade.mdl" 
            end
        end
	if self.Weapon:Clip1() > self.Primary.ClipSize then
		self.Weapon:SetClip1(self.Primary.ClipSize)
		
	end
   
	if self.Weapon:GetNWBool( "reloading") == true then
	   
		if self.Weapon:GetNWInt( "reloadtimer") < CurTime() then
			if self.unavailable then return end

			if ( self.Weapon:Clip1() >= self.Primary.ClipSize || self.Owner:GetAmmoCount( self.Primary.Ammo ) <= 0 ) then
				self.Weapon:SetNextPrimaryFire(CurTime() + 0.8)
				self.Weapon:SetNextSecondaryFire(CurTime() + 0.8)
				self.Weapon:SetNWBool( "reloading", false)
				self.Weapon:EmitSound( "weapons/bulkcannon/closelid.mp3" )
				self.Weapon:SendWeaponAnim(ACT_SHOTGUN_RELOAD_FINISH)
			else
			
			self.Weapon:SetNWInt( "reloadtimer", CurTime() + 0.55 )
			self.Weapon:SendWeaponAnim( ACT_VM_RELOAD )
			self.Owner:RemoveAmmo( 1, self.Primary.Ammo, false )
			self.Weapon:SetClip1(  self.Weapon:Clip1() + 1 )
			self.Weapon:SetNextPrimaryFire(CurTime() + 0.8)
			self.Weapon:SetNextSecondaryFire(CurTime() + 0.8)
			self.Weapon:EmitSound( "weapons/bulkcannon/insertshell.mp3" )

				if ( self.Weapon:Clip1() >= self.Primary.ClipSize || self.Owner:GetAmmoCount( self.Primary.Ammo ) <= 0) then
					self.Weapon:SetNextPrimaryFire(CurTime() + 1.5)
					self.Weapon:SetNextSecondaryFire(CurTime() + 1.5)
					self.Weapon:EmitSound( "weapons/bulkcannon/shellout.mp3" )
				else
					self.Weapon:SetNextPrimaryFire(CurTime() + 0.5)
					self.Weapon:SetNextSecondaryFire(CurTime() + 0.5)
					self.Weapon:EmitSound( "weapons/bulkcannon/shellout.mp3" )
				end
			end
		end
	end
end

SWEP.flyammo = 5
SWEP.nextFly = 0
function SWEP:Fly( soundfile, remote )
if self.nextFly > CurTime() or CLIENT then return end
self.nextFly = CurTime() + 0.30
if self.flyammo < 1 then return end

if not soundfile then
if SERVER then self.Owner:EmitSound(Sound("weapons/ls/lightsaber_swing.mp3")) end
if SERVER then self.Owner:EmitSound(Sound("player/suit_sprint.wav")) end
else
if SERVER then
if remote then
local soundfile = gb_config.websounds..soundfile
gb_PlaySoundFromServer(soundfile, self.Owner)
else
self.Owner:EmitSound(Sound(soundfile))
end
end
end
self.flyammo = self.flyammo - 1
self.Owner:SetAnimation(PLAYER_ATTACK1)
local power = 400
if math.random(1,5) < 2 then power = 100
elseif math.random(1,15) < 2 then power = -500
elseif math.random(1,15) < 2 then power = 2500
else power = math.random( 350,450 ) end

if SERVER then self.Owner:SetVelocity(self.Owner:GetForward() * power + Vector(0, 0, power)) end
timer.Simple(20, function() if IsValid(self) then self.flyammo = self.flyammo + 1 end end)
--timer.Simple(0.2,self.SecondaryAttackDelay,self)
end


--Disorentate

function SWEP:Disorientate()
local tr = self.Owner:GetEyeTrace()
if SERVER then
local entz = tr.Entity
if (entz:IsPlayer() or entz:IsNPC()) and  isfunction(entz.SetEyeAngles) then
if (self.Owner.NextDisOrentate and self.Owner.NextDisOrentate > CurTime()) then return end
self.Owner.NextDisOrentate = CurTime() + 4
local eyeang = entz:EyeAngles()
local j = 20
eyeang.pitch = math.Clamp(eyeang.pitch + math.Rand(-j, j), -50, 50)
eyeang.yaw = math.Clamp(eyeang.yaw + math.Rand(-j, j), -50, 50)
entz:SetEyeAngles(eyeang)
end
end

end


function SWEP:PreDrawViewModel()

self.Owner:GetViewModel():SetMaterial(  "camos/camo42"  )
end