

SWEP.Kind = 666
SWEP.HoldType = "knife"
SWEP.PrintName    = "Spooky Knife"
SWEP.Slot         = 665

if CLIENT then



    SWEP.ViewModelFlip = false
    SWEP.ViewModelFOV		= 65


    SWEP.Icon = "vgui/ttt_fun_killicons/knife.png"
end

SWEP.Base               = "gb_camo_base"

SWEP.UseHands			= true
SWEP.ViewModelFlip		= false
SWEP.ViewModelFOV		= 75

SWEP.ViewModelFOV = 70
SWEP.ViewModelFlip = false
SWEP.ViewModel = "models/weapons/v_bf3_acb90.mdl"
SWEP.WorldModel         = "models/weapons/w_knife_t.mdl"

SWEP.ShowWorldModel     = true

SWEP.DrawCrosshair      = false
SWEP.Primary.Damage         = 54
SWEP.Primary.ClipSize       = -1
SWEP.Primary.DefaultClip    = -1
SWEP.Primary.Automatic      = true
SWEP.Primary.Delay = 0.45
SWEP.Primary.Ammo       = "none"
SWEP.Secondary.ClipSize     = -1
SWEP.Secondary.DefaultClip  = -1
SWEP.Secondary.Automatic    = true
SWEP.Secondary.Ammo     = "none"
SWEP.Secondary.Delay = 1.4
SWEP.AllowDrop = true
SWEP.ViewModelFov = 54
SWEP.NoModel = false
SWEP.ModelChecked = false

SWEP.Camo = 0
SWEP.CustomCamo = true

function SWEP:Deploy()
   -- if CLIENT and not file.Exists("models/weapons/a_amerk.mdl", "GAME") then
   --     self.NoModel = true
   --     print("TEST")
   -- end
    local function playerDiesSound( victim, weapon, killer )
        if( IsValid(killer) and killer:IsPlayer()  ) then
            local wep = killer:GetActiveWeapon()
            if IsValid(wep) then
                wep = wep:GetClass()
                if wep == "custom_blackjack" then
                    gb_PlaySoundFromServer(gb_config.websounds.."rattleyourbones.mp3", killer)
                end
            end
        end
    end

    hook.Add( "PlayerDeath", "playerDeathSound_blackjacksniper", playerDiesSound )
    return self.BaseClass.Deploy(self)

end




SWEP.IsSilent = true

-- Pull out faster than standard guns
SWEP.DeploySpeed = 2

function SWEP:OnDrop()
    self:Remove()
end

function SWEP:PrimaryAttack()

    --if CLIENT and not file.Exists("models/weapons/a_amerk.mdl", "GAME") then
    --    self.NoModel = true
    --    print("TEST")
   -- end

    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
    self.Weapon:SetNextSecondaryFire( CurTime() + self.Secondary.Delay )

    self.Owner:LagCompensation(true)

    local spos = self.Owner:GetShootPos()
    local sdest = spos + (self.Owner:GetAimVector() * 140)

    local kmins = Vector(1,1,1) * -10
    local kmaxs = Vector(1,1,1) * 10

    local tr = util.TraceHull({start=spos, endpos=sdest, filter=self.Owner, mask=MASK_SHOT_HULL, mins=kmins, maxs=kmaxs})

    -- Hull might hit environment stuff that line does not hit
    if not IsValid(tr.Entity) then
        tr = util.TraceLine({start=spos, endpos=sdest, filter=self.Owner, mask=MASK_SHOT_HULL})
    end

    self:Disorientate()

    local hitEnt = tr.Entity

    -- effects
    if IsValid(hitEnt) then
        gb_PlaySoundFromServer(gb_config.websounds.."rattleyourbones.mp3", self.Owner)
        if  not self.NoModel then
        self.Weapon:SendWeaponAnim( ACT_VM_MISSCENTER )
        else
            self.Weapon:SendWeaponAnim( ACT_VM_HITCENTER )
        end
        local edata = EffectData()
        edata:SetStart(spos)
        edata:SetOrigin(tr.HitPos)
        edata:SetNormal(tr.Normal)
        edata:SetEntity(hitEnt)

        if hitEnt:IsPlayer() or hitEnt:GetClass() == "prop_ragdoll" then
            util.Effect("BloodImpact", edata)
        end
    else
        if not self.NoModel then
            self.Weapon:SendWeaponAnim( ACT_VM_MISSCENTER )
        else
            self.Weapon:SendWeaponAnim( ACT_VM_MISSCENTER )
        end
    end

    if SERVER then
      --  self.Owner:SetAnimation( ACT_VM_PRIMARYATTACK )

            self.Weapon:SendWeaponAnim( ACT_VM_MISSCENTER )

    end


    if SERVER and tr.Hit and tr.HitNonWorld and IsValid(hitEnt) then
        if hitEnt:IsPlayer() then

            if self.Owner:Health() < 175 then
                self.Owner:SetHealth(self.Owner:Health()+10)
            end
            -- knife damage is never karma'd, so don't need to take that into
            -- account we do want to avoid rounding error strangeness caused by
            -- other damage scaling, causing a death when we don't expect one, so
            -- when the target's health is close to kill-point we just kill
            if hitEnt:Health() < (self.Primary.Damage + 10) then
                self:StabKill(tr, spos, sdest)
            else
                local dmg = DamageInfo()
                dmg:SetDamage(self.Primary.Damage)
                dmg:SetAttacker(self.Owner)
                dmg:SetInflictor(self.Weapon or self)
                dmg:SetDamageForce(self.Owner:GetAimVector() * 5)
                dmg:SetDamagePosition(self.Owner:GetPos())
                dmg:SetDamageType(DMG_SLASH)

                hitEnt:DispatchTraceAttack(dmg, spos + (self.Owner:GetAimVector() * 3), sdest)
            end
        end
    end

    self.Owner:LagCompensation(false)
end

function SWEP:StabKill(tr, spos, sdest)
    local target = tr.Entity

    local dmg = DamageInfo()
    dmg:SetDamage(75)
    dmg:SetAttacker(self.Owner)
    dmg:SetInflictor(self.Weapon or self)
    dmg:SetDamageForce(self.Owner:GetAimVector())
    dmg:SetDamagePosition(self.Owner:GetPos())
    dmg:SetDamageType(DMG_SLASH)

    -- now that we use a hull trace, our hitpos is guaranteed to be
    -- terrible, so try to make something of it with a separate trace and
    -- hope our effect_fn trace has more luck

    -- first a straight up line trace to see if we aimed nicely
    local retr = util.TraceLine({start=spos, endpos=sdest, filter=self.Owner, mask=MASK_SHOT_HULL})

    -- if that fails, just trace to worldcenter so we have SOMETHING
    if retr.Entity != target then
    local center = target:LocalToWorld(target:OBBCenter())
    retr = util.TraceLine({start=spos, endpos=center, filter=self.Owner, mask=MASK_SHOT_HULL})
    end


    -- create knife effect creation fn
    local bone = retr.PhysicsBone
    local pos = retr.HitPos
    local norm = tr.Normal
    local ang = Angle(-28,0,0) + norm:Angle()
    ang:RotateAroundAxis(ang:Right(), -90)
    pos = pos - (ang:Forward() * 7)

    local prints = self.fingerprints
    local ignore = self.Owner

    target.effect_fn = function(rag)
    -- we might find a better location
        local rtr = util.TraceLine({start=pos, endpos=pos + norm * 40, filter=ignore, mask=MASK_SHOT_HULL})

        if IsValid(rtr.Entity) and rtr.Entity == rag then
            bone = rtr.PhysicsBone
            pos = rtr.HitPos
            ang = Angle(-28,0,0) + rtr.Normal:Angle()
            ang:RotateAroundAxis(ang:Right(), -90)
            pos = pos - (ang:Forward() * 10)

        end

        local knife = ents.Create("prop_physics")
        knife:SetModel("models/weapons/w_knife_t.mdl")
        knife:SetPos(pos)
        knife:SetCollisionGroup(COLLISION_GROUP_DEBRIS)
        knife:SetAngles(ang)
        knife.CanPickup = false

        knife:Spawn()

        local phys = knife:GetPhysicsObject()
        if IsValid(phys) then
            phys:EnableCollisions(false)
        end

        constraint.Weld(rag, knife, bone, 0, 0, true)

        -- need to close over knife in order to keep a valid ref to it
        rag:CallOnRemove("ttt_knife_cleanup", function() SafeRemoveEntity(knife) end)
    end


    -- seems the spos and sdest are purely for effects/forces?
    target:DispatchTraceAttack(dmg, spos + (self.Owner:GetAimVector() * 3), sdest)

    -- target appears to die right there, so we could theoretically get to
    -- the ragdoll in here...

    --self:Remove()
end

SWEP.NextReload = 0
function SWEP:Reload()
    self:PowerHit()
    if self.NextReload < CurTime() then
    self.NextReload = CurTime() + 15
    gb_PlaySoundFromServer(gb_config.websounds.."2spooky4me.mp3",self.Weapon)
    end
end

function SWEP:Equip()
    self.Weapon:SetNextPrimaryFire( CurTime() + (self.Primary.Delay * 1.5) )
    self.Weapon:SetNextSecondaryFire( CurTime() + (self.Secondary.Delay * 1.5) )
end

function SWEP:PreDrop()
    -- for consistency, dropped knife should not have DNA/prints
    self.fingerprints = {}
end

function SWEP:OnRemove()
    if CLIENT and IsValid(self.Owner) and self.Owner == LocalPlayer() and self.Owner:Alive() then
        RunConsoleCommand("lastinv")
    end
end

if CLIENT then
    function SWEP:DrawHUD()
        local tr = self.Owner:GetEyeTrace(MASK_SHOT)

        if tr.HitNonWorld and IsValid(tr.Entity) and tr.Entity:IsPlayer()
                and tr.Entity:Health() < (self.Primary.Damage + 10) then

            local x = ScrW() / 2.0
            local y = ScrH() / 2.0

            surface.SetDrawColor(255, 0, 0, 255)

            local outer = 20
            local inner = 10
            surface.DrawLine(x - outer, y - outer, x - inner, y - inner)
            surface.DrawLine(x + outer, y + outer, x + inner, y + inner)

            surface.DrawLine(x - outer, y + outer, x - inner, y + inner)
            surface.DrawLine(x + outer, y - outer, x + inner, y - inner)

            draw.SimpleText("INSTANT KILL", "TabLarge", x, y - 30, COLOR_RED, TEXT_ALIGN_CENTER, TEXT_ALIGN_BOTTOM)
        end

        return self.BaseClass.DrawHUD(self)
    end
end


function SWEP:ShootBullet2( dmg, recoil, numbul, cone )


    self.Weapon:SendWeaponAnim(self.PrimaryAnim)

    self.Owner:MuzzleFlash()
    self.Owner:SetAnimation( PLAYER_ATTACK1 )

    if not IsFirstTimePredicted() then return end

    local sights = self:GetIronsights()

    numbul = numbul or 1
    cone   = cone   or 0.01

    local bullet = {}
    bullet.Num    = numbul
    bullet.Src    = self.Owner:GetShootPos()
    bullet.Dir    = self.Owner:GetAimVector()
    bullet.Spread = Vector( cone, cone, 0 )
    bullet.Tracer = 0
    bullet.TracerName =  "LaserTracer_thick"
    bullet.Force  = 10
    bullet.Damage = 0


    self.Owner:FireBullets( bullet )

    -- Owner can die after firebullets
    if (not IsValid(self.Owner)) or (not self.Owner:Alive()) or self.Owner:IsNPC() then return end

    if ((game.SinglePlayer() and SERVER) or
            ((not game.SinglePlayer()) and CLIENT and IsFirstTimePredicted())) then

        -- reduce recoil if ironsighting
        recoil = sights and (recoil * 0.6) or recoil

        local eyeang = self.Owner:EyeAngles()
        eyeang.pitch = eyeang.pitch - recoil
        self.Owner:SetEyeAngles( eyeang )
    end

    attacker = self.Owner
    local tracedata = {}

    tracedata.start = self.Owner:GetShootPos()
    tracedata.endpos = self.Owner:GetShootPos() + ( self.Owner:GetAimVector() * 150 )
    tracedata.filter = self.Owner
    tracedata.mins = Vector( -3,-3,-3 )
    tracedata.maxs = Vector( 3,3,3 )
    tracedata.mask = MASK_SHOT_HULL
    local trace = util.TraceHull( tracedata )

    victim = trace.Entity

    timer.Simple(15, function()
        if IsValid(self) and IsValid( self.Owner) and self:Clip1() < 10 then
            self:SetClip1( self:Clip1() + 1)
        end
    end)

    if ( not IsValid(attacker) or !SERVER  or not victim:IsPlayer() or victim:IsWorld() ) then return end
    if  !victim:Alive() or victim.IsDog or specialRound.isSpecialRound or _globals['specialperson'][victim:SteamID()] or (victim.NoTaze && self.Owner:GetRole() != ROLE_DETECTIVE)    then
    self.Owner:PrintMessage( HUD_PRINTCENTER, "Person cannot be tazed" )

    return end


    if victim:GetPos():Distance( attacker:GetPos()) > 400 then return end

    self.Weapon:SetNextSecondaryFire( CurTime() + 30 )

    StunTime = self.StunTime

    local edata = EffectData()
    edata:SetEntity(victim)
    edata:SetMagnitude(3)
    edata:SetScale(3)

    util.Effect("TeslaHitBoxes", edata)

    self:Taze( victim)







end
SWEP.Range = 150
SWEP.StunTime = 6
SWEP.HadTazes = 0
function SWEP:SecondaryAttack()
    self.Weapon:SetNextSecondaryFire( CurTime() + 1 )
    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )

    --if not self:CanPrimaryAttack() then return end

    if not worldsnd then
        self.Weapon:EmitSound( self.Primary.Sound, self.Primary.SoundLevel )
    elseif SERVER then
        sound.Play(self.Primary.Sound, self:GetPos(), self.Primary.SoundLevel)
    end

    self:ShootBullet2( 10, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )

    self:TakePrimaryAmmo( 1 )

    local owner = self.Owner
    if not IsValid(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

    owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )

end

hook.Add("CanPlayerSuicide", "CantSuicideIfRagdolled", function(ply)
    return ply.Ragdolled == nil
end)

function SWEP:Taze( victim )

    if not victim:Alive() or victim:IsGhost() then return end

    if victim:InVehicle() then
        local vehicle = victim:GetParent()
        victim:ExitVehicle()
    end

    ULib.getSpawnInfo( victim )

    if SERVER then DamageLog("Tazer: " .. self.Owner:Nick() .. " [" .. self.Owner:GetRoleString() .. "] tazed "..victim:Nick().." [" .. victim:GetRoleString() .. "] ")
    end
    local rag = ents.Create("prop_ragdoll")
    if not (rag:IsValid()) then return end
    --if self.HadTazes > 2 then return end
    self.HadTazes = self.HadTazes + 1


    rag.BelongsTo = victim
    rag.Weapons = {}
    for _, v in pairs (victim:GetWeapons()) do
        table.insert(rag.Weapons, v:GetClass())
    end

    rag.Credits = victim:GetCredits()
    rag.OldHealth = victim:Health()
    rag:SetModel(victim:GetModel())
    rag:SetPos( victim:GetPos() )
    local velocity = victim:GetVelocity()
    rag:SetAngles( victim:GetAngles() )
    rag:SetModel( victim:GetModel() )
    rag:Spawn()
    rag:Activate()
    victim:SetParent( rag )
    victim:SetParent( rag )
    victim.Ragdolled = rag
    rag.orgPos = victim:GetPos()
    rag.orgPos.z = rag.orgPos.z + 10

    victim.orgPos = victim:GetPos()
    victim:StripWeapons()
    victim:Spectate(OBS_MODE_CHASE)
    victim:SpectateEntity(rag)


    gb_PlaySoundFromServer(gb_config.websounds.."2spooky4me.mp3", victim)

    victim:PrintMessage( HUD_PRINTCENTER, "You have been tasered. "..self.StunTime.." seconds till revival" )
    timer.Create("revivedelay"..victim:UniqueID(), self.StunTime, 1, function() TazerRevive( victim ) end )

end

function TazerRevive( victim)

    if !IsValid( victim ) then return end
    rag = victim:GetParent()

    if SERVER and victim.IsSlayed then
        victim:Kill();
        return
    end
    victim:SetParent()


    if (rag and rag:IsValid()) then
        weps = table.Copy(rag.Weapons)
        credits = rag.Credits
        oldHealth = rag.OldHealth
        rag:DropToFloor()
        pos = rag:GetPos()  --move up a little
        pos.z = pos.z + 15


        victim.Ragdolled = nil

        victim:UnSpectate()
        --  victim:DrawViewModel(true)
        --  victim:DrawWorldModel(true)
        victim:Spawn()
        --ULib.spawn( victim, true )
        rag:Remove()
        victim:SetPos( pos )
        victim:StripWeapons()

        UnStuck_stuck(victim)

        for _, v in pairs (weps) do
            victim:Give(v)
        end

        if credits > 0 then
            victim:AddCredits( credits  )
        end
        if oldHealth > 0 then
            victim:SetHealth( oldHealth  )
        end
        victim:DropToFloor()



        if  !victim:IsInWorld() or !victim:OnGround()    then
        victim:SetPos( victim.orgPos )
        end

        timer.Create("RespawnIfStuck",1,1,function()
            if IsValid(victim) and (!victim:IsInWorld() or !victim:OnGround() ) then
            victim:SetPos( victim.orgPos   )
            end
        end)

    else
        print("Debug: ragdoll or player is invalid")
        if IsValid( victim) then
            ULib.spawn( victim, true )
        end
    end



end

