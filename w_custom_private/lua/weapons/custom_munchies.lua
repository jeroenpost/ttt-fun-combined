SWEP.PrintName          = "Munchies"
SWEP.Slot               = 1
SWEP.Base				= "aa_base"
SWEP.Author             = "GreenBlack"

SWEP.Kind = WEAPON_UNARMED
SWEP.Icon = "vgui/ttt_fun_killicons/chips.png"

SWEP.LimitedStock = true

SWEP.Slot				= 5
SWEP.SlotPos = 8

SWEP.WElements = {
    ["chiips"] = { type = "Model", model = "models/crisps/gaprika.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(1.363, 2.273, -0.456), angle = Angle(-13.296, 0, 0), size = Vector(0.5, 0.5, 0.5), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
}

SWEP.ViewModel = Model("models/weapons/v_gbprika.mdl")
SWEP.WorldModel = Model("models/crisps/gaprika.mdl")

SWEP.Primary.Clipsize = -1
SWEP.Primary.DefaultClip = -1
SWEP.Primary.Automatic = false
SWEP.Primary.Ammo = "none"
SWEP.ViewModelFlip = false
SWEP.Secondary.Clipsize = -1
SWEP.Secondary.DefaultClip = -1
SWEP.Secondary.Automatic = false
SWEP.Secondary.Ammo = "none"
SWEP.reloaded = false
SWEP.Primary.Damage = 55
SWEP.Primary.Delay = 0.5
SWEP.AutoSpawnable =  true

function SWEP:OnDrop() self:Remove() end

function SWEP:PrimaryAttack()
    if (!SERVER) then return end
    self.Weapon:SendWeaponAnim( ACT_VM_MISSCENTER )
    self.Weapon:EmitSound("weapons/iceaxe/iceaxe_swing1.wav")

    local ent = ents.Create("sent_paprika")
    local pos = self.Owner:GetShootPos()
    ent:SetPos(pos)
    ent:SetAngles(self.Owner:EyeAngles())
    ent:SetPhysicsAttacker( self.Owner )
    --ent.owner = self
    ent:Spawn()
    ent:Activate()
    ent:SetOwner(self.Owner)
    ent:SetPhysicsAttacker(self.Owner)

    self.Weapon:SetNextSecondaryFire(CurTime() + 0.5)
    self.Weapon:SetNextPrimaryFire(CurTime() + self.Primary.Delay)
    ent.damage = self.Primary.Damage
    ent.heal = 2
    ent.poison = true
    ent.poisondamage = 10
    ent.poisontimes = 3
    ent.owner = self.Owner

    timer.Create(ent:EntIndex().."remover",5,1,function()
        if IsValid(ent) then ent:Remove() end
    end)

    local tr = self.Owner:GetEyeTrace()

    local phys = ent:GetPhysicsObject()
    if (phys:IsValid()) then
        phys:Wake()
        phys:SetMass(1)
        phys:EnableGravity( true )
        phys:ApplyForceCenter( self:GetForward() *2000 )
        phys:SetBuoyancyRatio( 0 )
    end

    local shot_length = tr.HitPos:Length()
    ent:SetVelocity(self.Owner:GetAimVector():GetNormalized() *  math.pow(shot_length, 3))



end

SWEP.usedmodelchange = false
SWEP.nextreload = 0
function SWEP:Reload()
    if self.Owner:KeyDown(IN_USE) then
        if self.nextreload > CurTime() then
            if self.nextreload - CurTime() < 20 then
             self.Owner:PrintMessage( HUD_PRINTCENTER,  math.floor(self.nextreload - CurTime()).." seconds left" )
            end
            return
        end
        self.nextreload = CurTime() + 28
        self.usedmodelchange = true
        self:PlayermodelChanger()
        return
    end
end


SWEP.NextPlayermodelChange = 0
function SWEP:PlayermodelChanger( )
    local model = self.Owner:GetModel()
    local offset =  self.Owner:GetViewOffset()
    self.Owner:SetModel("models/crisps/gaprika.mdl")
    self.Owner:SetViewOffset(Vector(0,0,15))
    local owner =  self.Owner
    timer.Create(self.Owner:EntIndex().."normalmodelhoversand",8,1,function()
        if not IsValid( owner) then return end
        owner:SetModel(model)
        owner:SetViewOffset(offset);
    end)
end

function SWEP:SecondaryAttack()
    self:DropHealth("normal_health_station", "models/crisps/gaprika.mdl")
end