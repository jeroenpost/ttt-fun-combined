

if CLIENT then

   SWEP.Slot            = 4
end

SWEP.Base			= "gb_master_deagle"
SWEP.PrintName       = "The Blitzstick"
SWEP.Kind 			= WEAPON_CARRY
SWEP.Slot = 4
SWEP.SlotPos 	= 1
SWEP.Primary.Recoil	= 1
SWEP.Primary.Damage = 55
SWEP.Primary.Delay 	= 0.45
SWEP.Primary.Cone 	= 0.01
SWEP.Primary.ClipSize 		= 40
SWEP.Primary.ClipMax 		= 120
SWEP.Primary.DefaultClip 	= 40
SWEP.Primary.NumShots		= 1
SWEP.Primary.Automatic 	= true
SWEP.HeadshotMultiplier = 3
SWEP.AmmoEnt 		= "item_ammo_revolver_ttt"

SWEP.AutoSpawnable      = false
SWEP.UseHands = true
SWEP.ViewModel          = Model("models/weapons/c_stunstick.mdl")
SWEP.WorldModel         = Model("models/weapons/w_stunbaton.mdl")
SWEP.ViewModelFlip = false

SWEP.NextSecondary = 0

SWEP.NextHeal = 0

function SWEP:Initialize()
    self:SetColorAndMaterial( Color(255,255,255), "camos/camo7" );
end


SWEP.TracerColor = Color(255,255,255,255)
function SWEP:SuperShot()

    if SERVER then
        local tr = self.Owner:GetEyeTrace()
        local ent = ents.Create("env_explosion")

        ent:SetPos(tr.HitPos)
        ent:SetOwner(self.Owner)
        ent:SetPhysicsAttacker(self.Owner)
        ent:Spawn()
        ent:SetKeyValue("iMagnitude", "160")
        ent:Fire("Explode", 0, 0)

        util.BlastDamage(self, self:GetOwner(), self:GetPos(), 60, 75)

        ent:EmitSound("weapons/big_explosion.mp3")
    end
    self:FireBolt()
    self:ShootTracer( "LaserTracer_thick")
end

-- CHARGING
SWEP.IsHolding = false
SWEP.LastHold = 0
SWEP.Sound = false
function SWEP:Think()

    if self.Owner:KeyDown(IN_USE) and self.Owner:KeyDown(IN_ATTACK2) then

        if not self.IsHolding then
            self.LastHold = CurTime()
            self.IsHolding = true
            self.Sound = CreateSound( self,"weapons/gauss/chargeloop.wav");
            self.Sound:Play();
            self.Sound:ChangePitch(250, 4.5)
        end

        if self.IsHolding and self.LastHold + 5 < CurTime() then
            self:SuperShot()
            self.IsHolding = false
        end
    elseif self.IsHolding then
        self.IsHolding = false
        self.Sound:Stop();
        self.Sound = false
    end

    if not SERVER or self.NextHeal > CurTime() or not IsValid(self.Owner) then return end
    self.NextHeal = CurTime() + 2

    if self.Owner:Health() < 175 then
        if SERVER then autohealfunction(self.Owner) end
    end

end



SWEP.flyammo = 5
SWEP.nextreload = 0

function SWEP:Reload()

    if self:Clip1() < 1 then
        self.Weapon:DefaultReload(self.ReloadAnim)
        self:SetIronsights( false )
        return
    end

   self:Fly()
end


function SWEP:SecondaryAttack()
    if self.NextSecondary > CurTime() or not SERVER then return end
    self.NextSecondary = CurTime() + 20
    self.Owner:SetNWFloat("w_stamina", 1)
    self.Owner:SetNWInt("runspeed", 950)
    self.Owner:SetNWInt("walkspeed",800)
    self.Owner:SetJumpPower(450)
    self.Owner.hasProtectionSuit = true

    timer.Simple(5,function()
        if IsValid(self) and self.Owner:IsValid() then
            self.Owner:SetNWInt("runspeed", 400)
            self.Owner:SetNWInt("walkspeed",250)
            self.Owner:SetJumpPower(250)
        end
    end)

end

function SWEP:PreDrawViewModel()
    self.Owner:GetViewModel():SetMaterial( "camos/camo7"  )
end
