
if SERVER then

    AddCSLuaFile(  )

end

SWEP.HoldType           = "pistol"

BOLT_MODEL			= "models/crossbow_bolt.mdl"

BOLT_AIR_VELOCITY	= 3500
BOLT_WATER_VELOCITY	= 1500
BOLT_SKIN_NORMAL	= 0
BOLT_SKIN_GLOW		= 1

CROSSBOW_GLOW_SPRITE	= "sprites/light_glow02_noz.vmt"
CROSSBOW_GLOW_SPRITE2	= "sprites/blueflare1.vmt"

SWEP.PrintName          = "Creagle V2.0"
SWEP.Slot               = 6

if CLIENT then


    SWEP.Author             = "Urban"

    SWEP.SlotPos            = 7
    SWEP.IconLetter         = "w"
    SWEP.Icon = "vgui/ttt/icon_crossbow"

    SWEP.ViewModelFlip = false
end

if SERVER then
    --resource.AddFile("materials/vgui/ttt/icon_crossbow.vmt")
end

SWEP.Base               = "aa_base"
SWEP.Spawnable = false
SWEP.AdminSpawnable = true

SWEP.Kind = WEAPON_ROLE

SWEP.AutoReload = true

SWEP.Primary.Tracer = 1
SWEP.Primary.Delay          = 1.6
SWEP.Primary.Recoil         = 1
SWEP.Primary.Automatic = false
SWEP.Primary.Ammo = "XBowBolt"
SWEP.Primary.Damage = 100
SWEP.Primary.NumShots		= 1
SWEP.Primary.NumAmmo		= 1
SWEP.Primary.Force = 10000000
SWEP.Primary.Cone = 0.001
SWEP.Primary.ClipSize = 20
SWEP.Primary.DefaultClip = 20
SWEP.Primary.ClipMax = 70
SWEP.AutoReload          = false
SWEP.AmmoEnt = ""
SWEP.Primary.AmmoType		= "crossbow_bolt"
SWEP.AutoSpawnable      = false
SWEP.ViewModel= "models/weapons/v_pist_gbagle.mdl"
SWEP.WorldModel= "models/weapons/w_pist_gbagle.mdl"

SWEP.ShowWorldModel = true
SWEP.ShowViewModel = true
SWEP.ViewModelFlip = true
SWEP.ViewModelFOV = 65

SWEP.Primary.Reload	= Sound( "Weapon_Crossbow.Reload" )
SWEP.Primary.Sound = Sound ("Weapon_Crossbow.Single")
SWEP.Primary.Special1		= Sound( "Weapon_Crossbow.BoltElectrify" )
SWEP.Primary.Special2		= Sound( "Weapon_Crossbow.BoltFly" )

--SWEP.IronSightsPos        = Vector( 5, 0, 1 )
SWEP.IronSightsPos = Vector(0, 0, -15)


SWEP.fingerprints = {}


SWEP.AllowDrop = true
SWEP.IsSilent = true

SWEP.PrimaryAnim = ACT_VM_PRIMARYATTACK_SILENCED
SWEP.ReloadAnim = ACT_VM_RELOAD_SILENCED

function SWEP:Precache()

    util.PrecacheSound( "Weapon_Crossbow.BoltHitBody" );
    util.PrecacheSound( "Weapon_Crossbow.BoltHitWorld" );
    util.PrecacheSound( "Weapon_Crossbow.BoltSkewer" );

    util.PrecacheModel( CROSSBOW_GLOW_SPRITE );
    util.PrecacheModel( CROSSBOW_GLOW_SPRITE2 );

    self.BaseClass:Precache();

end

function SWEP:PrimaryAttack()
    if ( !self:CanPrimaryAttack() ) then return end

    if SERVER then
        local tr = self.Owner:GetEyeTrace()
        local ent = ents.Create( "env_explosion" )

        ent:SetPos( tr.HitPos  )
        ent:SetOwner( self.Owner  )
        ent:SetPhysicsAttacker(  self.Owner )
        ent:Spawn()
        ent:SetKeyValue( "iMagnitude", "10" )
        ent:Fire( "Explode", 0, 0 )

        util.BlastDamage( self, self:GetOwner(), self:GetPos(), 0, 0 )

        ent:EmitSound( "weapons/big_explosion.mp3" )
    end

    if ( self.m_bInZoom && IsMultiplayer() ) then
    //		self:FireSniperBolt();
    self:FireBolt( 80 );
    else
        self:FireBolt( 80 );
    end



    // Signal a reload
    self.m_bMustReload = true;

end

SWEP.HadSecond = false
SWEP.NextReload = 0
function SWEP:Reload()

    if self.Owner:KeyDown(IN_USE) then
        self:Run()
     end

    if ( self.HadSecond ) then
        self:Fly()
        return end
    if self.NextReload < CurTime() then return end
    self.NextReload = CurTime() + 0.9
    if SERVER then
        local tr = self.Owner:GetEyeTrace()
        local ent = ents.Create( "env_explosion" )

        ent:SetPos( tr.HitPos  )
        ent:SetOwner( self.Owner  )
        ent:SetPhysicsAttacker(  self.Owner )
        ent:Spawn()
        ent:SetKeyValue( "iMagnitude", "40" )
        ent:Fire( "Explode", 0, 0 )

        if tr.Entity:IsPlayer() then
            self.HadSecond = true
         if SERVER then tr.Entity:SetVelocity(self.Owner:GetForward() * 400 + Vector(0,0,400)) end
        end
        util.BlastDamage( self, self:GetOwner(), self:GetPos(), 20, 75 )

        ent:EmitSound( "weapons/big_explosion.mp3" )
    end

    if ( self.m_bInZoom && IsMultiplayer() ) then
    //		self:FireSniperBolt();
    self:FireBolt( 95 );
    else
        self:FireBolt( 95 );
    end




    // Signal a reload
    self.m_bMustReload = true;

end

SWEP.NextSecondary = 0
function SWEP:Run()
    if self.NextSecondary > CurTime() or not SERVER then return end
    self.NextSecondary = CurTime() + 20
    self.Owner:SetNWFloat("w_stamina", 1)
    self.Owner:SetNWInt("runspeed", 950)
    self.Owner:SetNWInt("walkspeed",800)
    self.Owner:SetJumpPower(450)
    self.Owner.hasProtectionSuit = true

    timer.Simple(5,function()
        if IsValid(self) and self.Owner:IsValid() then
            self.Owner:SetNWInt("runspeed", 400)
            self.Owner:SetNWInt("walkspeed",250)
            self.Owner:SetJumpPower(480)
        end
    end)

end





function SWEP:Deploy()
    self.Weapon:SendWeaponAnim(ACT_VM_DRAW_SILENCED)
    return true
end

function SWEP:SetZoom(state)
    if CLIENT then
        return
    else
        if state && IsValid(self.Owner) then
        self.Owner:SetFOV(20, 0.3)
        elseif IsValid(self.Owner) then
            self.Owner:SetFOV(0, 0.2)
        end
    end
end

function SWEP:PreDrop()
    self:SetIronsights( false )
    self:SetZoom(false)
end
function SWEP:OnDrop()
    self:Remove()
end


function SWEP:SecondaryAttack()
    self:Fly();
end


function SWEP:Holster()
    self:SetIronsights(false)
    self:SetZoom(false)
    return true
end

if CLIENT then
    local scope = surface.GetTextureID("sprites/scope")
    function SWEP:DrawHUD()
        if self:GetIronsights() then
            surface.SetDrawColor( 0, 0, 0, 255 )

            local x = ScrW() / 2.0
            local y = ScrH() / 2.0
            local scope_size = ScrH()

            -- crosshair
            local gap = 80
            local length = scope_size
            surface.DrawLine( x - length, y, x - gap, y )
            surface.DrawLine( x + length, y, x + gap, y )
            surface.DrawLine( x, y - length, x, y - gap )
            surface.DrawLine( x, y + length, x, y + gap )

            gap = 0
            length = 50
            surface.DrawLine( x - length, y, x - gap, y )
            surface.DrawLine( x + length, y, x + gap, y )
            surface.DrawLine( x, y - length, x, y - gap )
            surface.DrawLine( x, y + length, x, y + gap )


            -- cover edges
            local sh = scope_size / 2
            local w = (x - sh) + 2
            surface.DrawRect(0, 0, w, scope_size)
            surface.DrawRect(x + sh - 2, 0, w, scope_size)

            surface.SetDrawColor(255, 0, 0, 255)
            surface.DrawLine(x, y, x + 1, y + 1)

            -- scope
            surface.SetTexture(scope)
            surface.SetDrawColor(255, 255, 255, 255)

            surface.DrawTexturedRectRotated(x, y, scope_size, scope_size, 0)

        else
            return self.BaseClass.DrawHUD(self)
        end
    end

    function SWEP:AdjustMouseSensitivity()
        return (self:GetIronsights() and 0.2) or nil
    end
end

function SWEP:FireBolt( damage)

    if ( self.Weapon:Clip1() <= 0 && self.Primary.ClipSize > -1 ) then
    if ( self:Ammo1() > 3 ) then
        self:Reload();
        self:ShootBullet( 100, 1, 0.01 )
    else
        self.Weapon:SetNextPrimaryFire( 5 );

    end

    return;
    end

    local pOwner = self.Owner;

    if ( pOwner == NULL ) then
        return;
    end

    if ( !CLIENT ) then
    local vecAiming		= pOwner:GetAimVector();
    local vecSrc		= pOwner:GetShootPos();

    local angAiming;
    angAiming = vecAiming:Angle();

    local pBolt = ents.Create ( self.Primary.AmmoType );
    pBolt:SetPos( vecSrc );
    pBolt:SetAngles( angAiming );
    pBolt.Damage = damage;
    self:ShootBullet( damage, 1, 0.01 )
    pBolt.AmmoType = self.Primary.Ammo;
    pBolt:SetOwner( pOwner );
    pBolt:Spawn()

    if ( pOwner:WaterLevel() == 3 ) then
        pBolt:SetVelocity( vecAiming * BOLT_WATER_VELOCITY );
    else
        pBolt:SetVelocity( vecAiming * BOLT_AIR_VELOCITY );
    end
    local tr = self.Owner:GetEyeTrace()
    local hitEnt = tr.Entity
    if hitEnt:IsPlayer() and self.Owner:Health() < 175 then
        self.Owner:SetHealth( self.Owner:Health() + 10)
    end
    end


    self:TakePrimaryAmmo( self.Primary.NumAmmo );

    if ( !pOwner:IsNPC() ) then
    pOwner:ViewPunch( Angle( -2, 0, 0 ) );
    end

    --self.Weapon:EmitSound( self.Primary.Sound );
    --self.Owner:EmitSound( self.Primary.Special2 );

    self.Weapon:SendWeaponAnim( ACT_VM_PRIMARYATTACK );

    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay );
    self.Weapon:SetNextSecondaryFire( CurTime() + self.Primary.Delay );

    // self:DoLoadEffect();
    // self:SetChargerState( CHARGER_STATE_DISCHARGE );

end

function SWEP:SetDeploySpeed( speed )

    self.m_WeaponDeploySpeed = tonumber( speed / GetConVarNumber( "phys_timescale" ) )

    self.Weapon:SetNextPrimaryFire( CurTime() + speed )
    self.Weapon:SetNextSecondaryFire( CurTime() + speed )

end

function SWEP:WasBought(buyer)
    if IsValid(buyer) then -- probably already self.Owner
        buyer:GiveAmmo( 6, "XBowBolt" )
    end
end 