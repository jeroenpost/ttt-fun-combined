SWEP.Base = "gb_camo_base"
SWEP.ViewModelFOV	= 65
SWEP.HoldType = "duel"
SWEP.PrintName = "Beginners Luck"
SWEP.ViewModel		= "models/weapons/cstrike/c_pist_elite.mdl"
SWEP.WorldModel		= "models/weapons/w_pist_elite.mdl"
SWEP.Kind = WEAPON_PISTOL
SWEP.Slot = 1
SWEP.AutoSpawnable = false
SWEP.Primary.Recoil     = 1.5
SWEP.Primary.Damage = 45
SWEP.Primary.Delay = 0.8
SWEP.Primary.Cone = 0.02
SWEP.Primary.ClipSize = 150
SWEP.Primary.Automatic = true
SWEP.Primary.DefaultClip = 150

SWEP.Secondary.Delay = 0.8
SWEP.Secondary.ClipSize     = 150
SWEP.Secondary.DefaultClip  = 150
SWEP.Secondary.Automatic    = true
SWEP.Secondary.Ammo         = "Pistol"
SWEP.Secondary.ClipMax      = 60

SWEP.Primary.Ammo			= "AlyxGun"
SWEP.AmmoEnt = "item_ammo_pistol_ttt"
SWEP.HeadshotMultiplier = 1

SWEP.Camo = 14

SWEP.Primary.Sound = Sound( "Weapon_Elite.Single" )
SWEP.Secondary.Sound = Sound( "Weapon_Elite.Single" )




function SWEP:PrimaryAttack( worldsnd )

    --self.Weapon:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )

    if not self:CanPrimaryAttack() then return end

    self:PowerHit(true,200,999999)

    if not worldsnd then
        self.Weapon:EmitSound( self.Primary.Sound, self.Primary.SoundLevel )
    elseif SERVER then
        sound.Play(self.Primary.Sound, self:GetPos(), self.Primary.SoundLevel)
    end

    self:ShootBullet( self.Primary.Damage, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )

    --timer.Simple(0.05,function()
       -- if IsValid(self) and self.Sound then
            self:throw_attack ( "models/Gibs/HGIBS.mdl", nil, 3);
        --end
    --end)

    self:TakePrimaryAmmo( 1 )

    local owner = self.Owner
    if not IsValid(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

    --  owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )
end

function SWEP:CanSecondaryAttack()
    if not IsValid(self.Owner) then return end

    if self.Weapon:Clip1() <= 0 then
        self:DryFire(self.SetNextSecondaryFire)
        return false
    end
    return true
end


function SWEP:throw_attack (model_file, sound, away)
    local tr = self.Owner:GetEyeTrace();

    if IsValid( tr.Entity )  then
        tr.Entity.hasProtectionSuit= true
    end


    self.BaseClass.ShootEffects (self);
    if (!SERVER) then return end;
    local ent = ents.Create ("prop_physics");
    ent:SetModel (model_file);
    ent:SetPos (self.Owner:EyePos() + (self.Owner:GetAimVector() * 16));
    ent:SetAngles (self.Owner:EyeAngles());
    ent:SetPhysicsAttacker(self.Owner)
    ent:Spawn();
   -- ent:EmitSound( sound )
    if math.random(1,10) < 2 then
        ent:Ignite(100,100)
    end
    local phys = ent:GetPhysicsObject();
    local shot_length = tr.HitPos:Length()+10;
    phys:SetMass( 1 )
    phys:ApplyForceCenter (self.Owner:GetAimVector():GetNormalized() * math.pow (shot_length, 3));

    timer.Simple(away,function()
        if IsValid(ent) then
            ent:Remove()
        end
    end)

    cleanup.Add (self.Owner, "props", ent);
    undo.Create ("Thrown model");
    undo.AddEntity (ent);
    undo.SetPlayer (self.Owner);
    undo.Finish();
end

function SWEP:SecondaryAttack(worldsnd)


    self.Weapon:SetNextSecondaryFire( CurTime() + self.Secondary.Delay )
    --self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )

    if not self:CanSecondaryAttack() then return end

    if not worldsnd then
        self.Weapon:EmitSound( self.Primary.Sound, self.Primary.SoundLevel )
    elseif SERVER then
        sound.Play(self.Primary.Sound, self:GetPos(), self.Primary.SoundLevel)
    end

    self:ShootBullet( self.Primary.Damage, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )

    self:TakePrimaryAmmo( 1 )
    self:throw_attack ( "models/Gibs/HGIBS.mdl", nil, 3);
    local owner = self.Owner
    if not IsValid(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

    -- owner:ViewPunch( Angle( math.Rand(-0.6,-0.6) * self.Primary.Recoil, math.Rand(-0.6,0.6) *self.Primary.Recoil, 0 ) )

end
