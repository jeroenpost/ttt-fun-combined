SWEP.HoldType = "melee2"
SWEP.ViewModelFOV = 50.944881889764
SWEP.ViewModelFlip = false
SWEP.ViewModel = "models/weapons/v_crowbar.mdl"
SWEP.WorldModel = "models/weapons/w_crowbar.mdl"
SWEP.ShowViewModel = true
SWEP.ShowWorldModel = true
SWEP.ViewModelBoneMods = {
	["ValveBiped.Bip01"] = { scale = Vector(1, 1, 1), pos = Vector(0, -1.588, -0.635), angle = Angle(20.951, 0, -20.952) }
}
SWEP.VElements = {
	["saw"] = { type = "Model", model = "models/props_junk/sawblade001a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(8.635, 1.6, -15), angle = Angle(0, 0, -90), size = Vector(0.009, 0.009, 0.009), color = Color(255, 255, 255, 255), surpresslightning = false, material = "models/shiny", skin = 0, bodygroup = {} },
	["headpiece2"] = { type = "Model", model = "models/props_c17/streetsign005b.mdl", bone = "ValveBiped.Bip01", rel = "saw", pos = Vector(-4.092, -2.274, 0), angle = Angle(-1.024, 54, -88.977), size = Vector(0.379, 8.269, 0.379), color = Color(255, 255, 255, 255), surpresslightning = false, material = "camos/camo7", skin = 0, bodygroup = {} },
	["length"] = { type = "Model", model = "models/props_docks/dock02_pole02a_256.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(3.181, 1.57, -2.274), angle = Angle(6, 0, 0), size = Vector(0.094, 0.094, 0.094), color = Color(255, 255, 255, 255), surpresslightning = false, material = "camos/camo7", skin = 0, bodygroup = {} },
	["saw+"] = { type = "Model", model = "models/props_junk/sawblade001a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(8.635, 1.6, -15), angle = Angle(0, 0, -90), size = Vector(0.264, 0.264, 0.264), color = Color(255, 255, 255, 255), surpresslightning = false, material = "models/shiny", skin = 0, bodygroup = {} },
	["headpiece3"] = { type = "Model", model = "models/props_c17/FurnitureWashingmachine001a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "headpiece2", pos = Vector(1.363, 0, 0.455), angle = Angle(-31.705, -180, 0), size = Vector(0.094, 0.037, 0.264), color = Color(255, 255, 255, 255), surpresslightning = false, material = "camos/camo7", skin = 0, bodygroup = {} },
	["handguard"] = { type = "Model", model = "models/props_c17/playground_teetertoter_stan.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(4.091, 1.399, 0.455), angle = Angle(-86.932, 0, 0), size = Vector(0.151, 0.947, 0.094), color = Color(255, 255, 255, 255), surpresslightning = false, material = "camos/camo7", skin = 0, bodygroup = {} },
	["connector"] = { type = "Model", model = "models/props_c17/tools_wrench01a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "wheel", pos = Vector(-1, -0.456, -4.7), angle = Angle(172.841, -90, -90), size = Vector(0.5, 0.5, 0.5), color = Color(255, 255, 255, 255), surpresslightning = false, material = "camos/camo7", skin = 0, bodygroup = {} },
	["pull"] = { type = "Model", model = "models/props_wasteland/panel_leverHandle001a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "lengthpieces", pos = Vector(2.273, 0, 0), angle = Angle(41.931, -178.978, 0), size = Vector(0.379, 0.379, 0.379), color = Color(255, 255, 255, 255), surpresslightning = false, material = "camos/camo7", skin = 0, bodygroup = {} },
	["lengthpieces"] = { type = "Model", model = "models/gibs/airboat_broken_engine.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(3.181, 1.25, -7.728), angle = Angle(-76.706, 0, -180), size = Vector(0.209, 0.209, 0.209), color = Color(255, 255, 255, 255), surpresslightning = false, material = "camos/camo7", skin = 0, bodygroup = {} },
	["wheel"] = { type = "Model", model = "models/props_c17/pulleywheels_small01.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "headpiece2", pos = Vector(-0.456, 0, 3.181), angle = Angle(0, -90, 0), size = Vector(0.151, 0.151, 0.151), color = Color(255, 255, 255, 255), surpresslightning = false, material = "camos/camo7", skin = 0, bodygroup = {} },
	["holder"] = { type = "Model", model = "models/props_c17/pulleywheels_small01.mdl", bone = "ValveBiped.Bip01", rel = "saw", pos = Vector(0, 0, 0), angle = Angle(90, 0, 0), size = Vector(0.151, 0.151, 0.151), color = Color(255, 255, 255, 255), surpresslightning = false, material = "camos/camo7", skin = 0, bodygroup = {} },
	["part"] = { type = "Model", model = "models/gibs/manhack_gib02.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "lengthpieces", pos = Vector(-1.364, 0, 0.455), angle = Angle(0, 0, 0), size = Vector(0.5, 0.5, 0.5), color = Color(255, 255, 255, 255), surpresslightning = false, material = "camos/camo7", skin = 0, bodygroup = {} },
	["dec"] = { type = "Model", model = "models/props_c17/utilityconnecter003.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(1.899, 1.549, -15), angle = Angle(0, -90, 66.476), size = Vector(0.321, 0.321, 0.321), color = Color(255, 255, 255, 255), surpresslightning = false, material = "camos/camo7", skin = 0, bodygroup = {} }
}
SWEP.WElements = {
	["saw"] = { type = "Model", model = "models/props_junk/sawblade001a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(5.3, 2, -21.365), angle = Angle(-3.069, 20, -93.069), size = Vector(0.009, 0.009, 0.009), color = Color(255, 255, 255, 255), surpresslightning = false, material = "models/shiny", skin = 0, bodygroup = {} },
	["headpiece2"] = { type = "Model", model = "models/props_c17/streetsign005b.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "saw", pos = Vector(-4.092, -2.274, 0), angle = Angle(-1.024, 54, -88.977), size = Vector(0.379, 8.269, 0.379), color = Color(255, 255, 255, 255), surpresslightning = false, material = "camos/camo7", skin = 0, bodygroup = {} },
	["pull"] = { type = "Model", model = "models/props_wasteland/panel_leverHandle001a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "lengthpieces", pos = Vector(2.273, 0, 0), angle = Angle(41.931, -178.978, 0), size = Vector(0.379, 0.379, 0.379), color = Color(255, 255, 255, 255), surpresslightning = false, material = "camos/camo7", skin = 0, bodygroup = {} },
	["saw+"] = { type = "Model", model = "models/props_junk/sawblade001a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(5.3, 2, -21.365), angle = Angle(-3.069, 20, -93.069), size = Vector(0.264, 0.264, 0.264), color = Color(255, 255, 255, 255), surpresslightning = false, material = "models/shiny", skin = 0, bodygroup = {} },
	["dec"] = { type = "Model", model = "models/props_c17/utilityconnecter003.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "headpiece2", pos = Vector(-4.092, 0, -0.456), angle = Angle(0, 90, -58.295), size = Vector(0.321, 0.321, 0.321), color = Color(255, 255, 255, 255), surpresslightning = false, material = "camos/camo7", skin = 0, bodygroup = {} },
	["handguard"] = { type = "Model", model = "models/props_c17/playground_teetertoter_stan.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "length", pos = Vector(0.455, 0, -8.101), angle = Angle(-90, 0, 0), size = Vector(0.119, 0.549, 0.056), color = Color(255, 255, 255, 255), surpresslightning = false, material = "camos/camo7", skin = 0, bodygroup = {} },
	["part"] = { type = "Model", model = "models/gibs/manhack_gib02.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "lengthpieces", pos = Vector(-5, 0, 0.455), angle = Angle(0, 0, 0), size = Vector(0.5, 0.5, 0.5), color = Color(255, 255, 255, 255), surpresslightning = false, material = "camos/camo7", skin = 0, bodygroup = {} },
	["headpiece3"] = { type = "Model", model = "models/props_c17/FurnitureWashingmachine001a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "headpiece2", pos = Vector(1.363, 0, 0.455), angle = Angle(-31.705, -180, 0), size = Vector(0.094, 0.037, 0.264), color = Color(255, 255, 255, 255), surpresslightning = false, material = "camos/camo7", skin = 0, bodygroup = {} },
	["lengthpieces"] = { type = "Model", model = "models/gibs/airboat_broken_engine.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "length", pos = Vector(-0.456, 0, 5), angle = Angle(90, -90, 90), size = Vector(0.209, 0.209, 0.209), color = Color(255, 255, 255, 255), surpresslightning = false, material = "camos/camo7", skin = 0, bodygroup = {} },
	["wheel"] = { type = "Model", model = "models/props_c17/pulleywheels_small01.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "headpiece2", pos = Vector(-0.456, 0, 3.181), angle = Angle(0, -90, 0), size = Vector(0.151, 0.151, 0.151), color = Color(255, 255, 255, 255), surpresslightning = false, material = "camos/camo7", skin = 0, bodygroup = {} },
	["holder"] = { type = "Model", model = "models/props_c17/pulleywheels_small01.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "saw", pos = Vector(0, 0, 0), angle = Angle(90, 0, 0), size = Vector(0.151, 0.151, 0.151), color = Color(255, 255, 255, 255), surpresslightning = false, material = "camos/camo7", skin = 0, bodygroup = {} },
	["connector"] = { type = "Model", model = "models/props_c17/tools_wrench01a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "wheel", pos = Vector(-1, -0.456, -4.7), angle = Angle(172.841, -90, -90), size = Vector(0.5, 0.5, 0.5), color = Color(255, 255, 255, 255), surpresslightning = false, material = "camos/camo7", skin = 0, bodygroup = {} },
	["length"] = { type = "Model", model = "models/props_docks/dock02_pole02a_256.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "headpiece2", pos = Vector(-12, 0, -10), angle = Angle(-50.114, 0, 1), size = Vector(0.094, 0.094, 0.094), color = Color(255, 255, 255, 255), surpresslightning = false, material = "camos/camo7", skin = 0, bodygroup = {} }
}
SWEP.Purpose = "It returns (with a model that looks a thousand times better than the original)!"
SWEP.AutoSwitchTo = true
SWEP.Contact = ""
SWEP.Author = "BFG/Spastik"
SWEP.FiresUnderwater = true
SWEP.Spawnable = true
SWEP.AdminSpawnable = true
SWEP.SlotPos = 0
SWEP.Instructions = "Swing to give free lobotomies."
SWEP.AutoSwitchFrom = false
SWEP.Base = "aa_base"
SWEP.Category = "S&G Munitions"
SWEP.DrawAmmo = false
SWEP.PrintName = "Golden Choppy Choppy"
SWEP.Primary.Recoil				= 0
SWEP.Primary.Damage				= 0
SWEP.Primary.NumShots			= 0
SWEP.Primary.Cone				= 0
SWEP.Primary.ClipSize			= -1
SWEP.Primary.DefaultClip		= -1
SWEP.Primary.Automatic   		= false
SWEP.Primary.Ammo         		= "none"


SWEP.Secondary.Delay			= 0.3
SWEP.Secondary.Recoil			= 0
SWEP.Secondary.Damage			= 0
SWEP.Secondary.NumShots			= 0
SWEP.Secondary.Cone		  		= 0
SWEP.Secondary.ClipSize			= -1
SWEP.Secondary.DefaultClip		= -1
SWEP.Secondary.Automatic   		= true
SWEP.Secondary.Ammo         	= "none"

/*---------------------------------------------------------
---------------------------------------------------------*/

//SWEP:PrimaryFire()\\
function SWEP:PrimaryAttack()
         self.Weapon:SetNextPrimaryFire(CurTime() + self.Primary.Delay)
         local spos = self.Owner:GetShootPos()
         local sdest = spos + (self.Owner:GetAimVector() * 150)

         local kmins = Vector(1,1,1) * -10
         local kmaxs = Vector(1,1,1) * 10

         local tr = util.TraceHull({start=spos, endpos=sdest, filter=self.Owner, mask=MASK_SHOT_HULL, mins=kmins, maxs=kmaxs})

         -- Hull might hit environment stuff that line does not hit
         if not IsValid(tr.Entity) then
             tr = util.TraceLine({start=spos, endpos=sdest, filter=self.Owner, mask=MASK_SHOT_HULL})
         end

         local hitEnt = tr.Entity

         if IsValid(hitEnt) then
		    if SERVER then
                hitEnt:TakeDamage( math.random( 10,30 ),self.Owner or self,self )
			end
		    self.Weapon:EmitSound("npc/manhack/grind1.wav",500,100,1,-1)
			if ( hitEnt:IsPlayer() or hitEnt:IsNPC()  ) then
                 self.Weapon:EmitSound("npc/manhack/grind_flesh3.wav",500,100,1,0)
				else
				 --util.Decal( "ManhackCut",( trace.HitPos - trace.HitNormal ),( trace.HitPos + trace.HitNormal ) )
			end
		    self.Weapon:EmitSound("npc/manhack/grind2.wav", 500, 100, 1, 0)
            self.Weapon:EmitSound("weapons/stunstick/stunstick_fleshhit1.wav", 500, 100 ,1, -1)
           else
	        self.Weapon:EmitSound("npc/zombie/claw_miss2.wav",500,100,1,-1)
	        self.Weapon:EmitSound("npc/vort/claw_swing1.wav",500,100,1,1)
	        self.Weapon:EmitSound("npc/vort/claw_swing2.wav",500,100,1,0)
         end
		 self.Weapon:SendWeaponAnim( ACT_VM_MISSCENTER )
         self.Owner:SetAnimation( PLAYER_ATTACK1 )
		-- self.Owner:ViewPunch( Angle( 0,10,-10 ) )

end


function SWEP:Deploy() --BFG here; this part is supposed to control the motor sound that the lobotomizer makes.
         -- SAGA here; i fixed it
	     if ( !self.SoundObj ) then
		      self.SoundObj = CreateSound( self,"vehicles/v8/v8_idle_loop1.wav" )
		      self.SoundObj:Play()
		 end
end

function SWEP:SecondaryAttack()
    local soundfile = gb_config.websounds.."throw_green.mp3"
    gb_PlaySoundFromServer(soundfile, self.Owner)
    self:ThrowLikeKnife("normal_throw_knife",  5, 200)
    self:Remove()
end

function SWEP:Touch( ent )
if ent:IsPlayer() and not ent.nextsandtouch or (ent.nextsandtouch and ent.nextsandtouch < CurTime()) then
    ent.nextsandtouch = CurTime() + 1
    -- ent:Ignite(0.5,0)
    local dmginfo = DamageInfo()
    if not self.Damage then self.Damage = 50 end
    dmginfo:SetDamage( self.Damage ) --50 damage
    dmginfo:SetDamageType( DMG_BULLET ) --Bullet damage
    dmginfo:SetAttacker( self.owner or ent ) --First player found gets credit
    dmginfo:SetDamageForce( Vector( 0, 0, 50 ) ) --Launch upwards
    dmginfo:SetInflictor(self)
    ent:TakeDamageInfo( dmginfo )
    self:Remove()
end
end



//SWEP:SecondaryFire()\\

function SWEP:Think() -- Called every frame
	self:OnThink() --calls the OnThink function which controls the spinning of the blade
end

local SpinAng = 1



function SWEP:OnThink() -- BFG here; this part is ripped off from Awcmon's Sci Fi but hey don't blame me the code is 2simple4u


	if (CLIENT) then

		SpinAng = SpinAng + 4

		self.VElements["saw+"].angle.x = SpinAng
		self.WElements["saw+"].angle.x = SpinAng --wasn't in Awcmon's code because his doesn't rotate the worldmodel element for some weird reason.

	end



end

function shockEnt(ent, pos, life, radius)
	local b = ents.Create( "point_hurt" )
	b:SetKeyValue("targetname", "fier" )
	b:SetKeyValue("DamageRadius", radius )
	b:SetKeyValue("Damage", "100" )
	b:SetKeyValue("DamageDelay", "1" )
	b:SetKeyValue("DamageType", "256" )
	b:SetPos( pos )
	b:SetParent(ent)
	b:Spawn()
	b:Fire("turnon", "", 0)
	b:Fire("turnoff", "", life)
	b:Fire("kill", "", life)
end

function hitBoxes(ent, ent2)
if ent:IsValid() then
	local effectdata3 = EffectData()
	effectdata3:SetOrigin( ent:GetPos() )
	effectdata3:SetStart( ent2:GetPos() )
	effectdata3:SetMagnitude(10)
	effectdata3:SetEntity( ent )
	util.Effect( "TeslaHitBoxes", effectdata3)
end
end


/********************************************************
	SWEP Construction Kit base code
		Created by Clavus
	Available for public use, thread at:
	   facepunch.com/threads/1032378


	DESCRIPTION:
		This script is meant for experienced scripters
		that KNOW WHAT THEY ARE DOING. Don't come to me
		with basic Lua questions.

		Just copy into your SWEP or SWEP base of choice
		and merge with your own code.

		The SWEP.VElements, SWEP.WElements and
		SWEP.ViewModelBoneMods tables are all optional
		and only have to be visible to the client.
********************************************************/

function SWEP:Initialize()

	// other initialize code goes here

	if CLIENT then

		self:SetHoldType( self.HoldType or "pistol" )
		// Create a new table for every weapon instance
		self.VElements = table.FullCopy( self.VElements )
		self.WElements = table.FullCopy( self.WElements )
		self.ViewModelBoneMods = table.FullCopy( self.ViewModelBoneMods )

		self:CreateModels(self.VElements) // create viewmodels
		self:CreateModels(self.WElements) // create worldmodels

		// init view model bone build function
		if IsValid(self.Owner) then
			local vm = self.Owner:GetViewModel()
			if IsValid(vm) then
				self:ResetBonePositions(vm)

				--[[// Init viewmodel visibility
				if (self.ShowViewModel == nil or self.ShowViewModel) then
					vm:SetColor(Color(255,255,255,255))
				else
					// we set the alpha to 1 instead of 0 because else ViewModelDrawn stops being called
					vm:SetColor(Color(255,255,255,1))
					// ^ stopped working in GMod 13 because you have to do Entity:SetRenderMode(1) for translucency to kick in
					// however for some reason the view model resets to render mode 0 every frame so we just apply a debug material to prevent it from drawing
					vm:SetMaterial("Debug/hsv")
				end]]--
			end
		end
	end
end
function SWEP:Holster()

	if CLIENT and IsValid(self.Owner) then
		local vm = self.Owner:GetViewModel()
		if IsValid(vm) then
			self:ResetBonePositions(vm)
		end
	end
	if ( self.SoundObj ) then
	     self.SoundObj:FadeOut( 0.2 )
		 self.SoundObj = nil
	end

	return true
end

function SWEP:OnRemove()
	self:Holster()
end

if CLIENT then

	SWEP.vRenderOrder = nil
	function SWEP:ViewModelDrawn()

		local vm = self.Owner:GetViewModel()
		if !IsValid(vm) then return end

		if (!self.VElements) then return end

		self:UpdateBonePositions(vm)

		if (!self.vRenderOrder) then

			// we build a render order because sprites need to be drawn after models
			self.vRenderOrder = {}

			for k, v in pairs( self.VElements ) do
				if (v.type == "Model") then
					table.insert(self.vRenderOrder, 1, k)
				elseif (v.type == "Sprite" or v.type == "Quad") then
					table.insert(self.vRenderOrder, k)
				end
			end

		end

		for k, name in ipairs( self.vRenderOrder ) do

			local v = self.VElements[name]
			if (!v) then self.vRenderOrder = nil break end
			if (v.hide) then continue end

			local model = v.modelEnt
			local sprite = v.spriteMaterial

			if (!v.bone) then continue end

			local pos, ang = self:GetBoneOrientation( self.VElements, v, vm )

			if (!pos) then continue end

			if (v.type == "Model" and IsValid(model)) then

				model:SetPos(pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z )
				ang:RotateAroundAxis(ang:Up(), v.angle.y)
				ang:RotateAroundAxis(ang:Right(), v.angle.p)
				ang:RotateAroundAxis(ang:Forward(), v.angle.r)

				model:SetAngles(ang)
				//model:SetModelScale(v.size)
				local matrix = Matrix()
				matrix:Scale(v.size)
				model:EnableMatrix( "RenderMultiply", matrix )

				if (v.material == "") then
					model:SetMaterial("")
				elseif (model:GetMaterial() != v.material) then
					model:SetMaterial( v.material )
				end

				if (v.skin and v.skin != model:GetSkin()) then
					model:SetSkin(v.skin)
				end

				if (v.bodygroup) then
					for k, v in pairs( v.bodygroup ) do
						if (model:GetBodygroup(k) != v) then
							model:SetBodygroup(k, v)
						end
					end
				end

				if (v.surpresslightning) then
					render.SuppressEngineLighting(true)
				end

				render.SetColorModulation(v.color.r/255, v.color.g/255, v.color.b/255)
				render.SetBlend(v.color.a/255)
				model:DrawModel()
				render.SetBlend(1)
				render.SetColorModulation(1, 1, 1)

				if (v.surpresslightning) then
					render.SuppressEngineLighting(false)
				end

			elseif (v.type == "Sprite" and sprite) then

				local drawpos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
				render.SetMaterial(sprite)
				render.DrawSprite(drawpos, v.size.x, v.size.y, v.color)

			elseif (v.type == "Quad" and v.draw_func) then

				local drawpos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
				ang:RotateAroundAxis(ang:Up(), v.angle.y)
				ang:RotateAroundAxis(ang:Right(), v.angle.p)
				ang:RotateAroundAxis(ang:Forward(), v.angle.r)

				cam.Start3D2D(drawpos, ang, v.size)
					v.draw_func( self )
				cam.End3D2D()

			end

		end

	end

	SWEP.wRenderOrder = nil
	function SWEP:DrawWorldModel()

		if (self.ShowWorldModel == nil or self.ShowWorldModel) then
			self:DrawModel()
		end

		if (!self.WElements) then return end

		if (!self.wRenderOrder) then

			self.wRenderOrder = {}

			for k, v in pairs( self.WElements ) do
				if (v.type == "Model") then
					table.insert(self.wRenderOrder, 1, k)
				elseif (v.type == "Sprite" or v.type == "Quad") then
					table.insert(self.wRenderOrder, k)
				end
			end

		end

		if (IsValid(self.Owner)) then
			bone_ent = self.Owner
		else
			// when the weapon is dropped
			bone_ent = self
		end

		for k, name in pairs( self.wRenderOrder ) do

			local v = self.WElements[name]
			if (!v) then self.wRenderOrder = nil break end
			if (v.hide) then continue end

			local pos, ang

			if (v.bone) then
				pos, ang = self:GetBoneOrientation( self.WElements, v, bone_ent )
			else
				pos, ang = self:GetBoneOrientation( self.WElements, v, bone_ent, "ValveBiped.Bip01_R_Hand" )
			end

			if (!pos) then continue end

			local model = v.modelEnt
			local sprite = v.spriteMaterial

			if (v.type == "Model" and IsValid(model)) then

				model:SetPos(pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z )
				ang:RotateAroundAxis(ang:Up(), v.angle.y)
				ang:RotateAroundAxis(ang:Right(), v.angle.p)
				ang:RotateAroundAxis(ang:Forward(), v.angle.r)

				model:SetAngles(ang)
				//model:SetModelScale(v.size)
				local matrix = Matrix()
				matrix:Scale(v.size)
				model:EnableMatrix( "RenderMultiply", matrix )

				if (v.material == "") then
					model:SetMaterial("")
				elseif (model:GetMaterial() != v.material) then
					model:SetMaterial( v.material )
				end

				if (v.skin and v.skin != model:GetSkin()) then
					model:SetSkin(v.skin)
				end

				if (v.bodygroup) then
					for k, v in pairs( v.bodygroup ) do
						if (model:GetBodygroup(k) != v) then
							model:SetBodygroup(k, v)
						end
					end
				end

				if (v.surpresslightning) then
					render.SuppressEngineLighting(true)
				end

				render.SetColorModulation(v.color.r/255, v.color.g/255, v.color.b/255)
				render.SetBlend(v.color.a/255)
				model:DrawModel()
				render.SetBlend(1)
				render.SetColorModulation(1, 1, 1)

				if (v.surpresslightning) then
					render.SuppressEngineLighting(false)
				end

			elseif (v.type == "Sprite" and sprite) then

				local drawpos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
				render.SetMaterial(sprite)
				render.DrawSprite(drawpos, v.size.x, v.size.y, v.color)

			elseif (v.type == "Quad" and v.draw_func) then

				local drawpos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
				ang:RotateAroundAxis(ang:Up(), v.angle.y)
				ang:RotateAroundAxis(ang:Right(), v.angle.p)
				ang:RotateAroundAxis(ang:Forward(), v.angle.r)

				cam.Start3D2D(drawpos, ang, v.size)
					v.draw_func( self )
				cam.End3D2D()

			end

		end

	end

	function SWEP:GetBoneOrientation( basetab, tab, ent, bone_override )

		local bone, pos, ang
		if (tab.rel and tab.rel != "") then

			local v = basetab[tab.rel]

			if (!v) then return end

			// Technically, if there exists an element with the same name as a bone
			// you can get in an infinite loop. Let's just hope nobody's that stupid.
			pos, ang = self:GetBoneOrientation( basetab, v, ent )

			if (!pos) then return end

			pos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
			ang:RotateAroundAxis(ang:Up(), v.angle.y)
			ang:RotateAroundAxis(ang:Right(), v.angle.p)
			ang:RotateAroundAxis(ang:Forward(), v.angle.r)

		else

			bone = ent:LookupBone(bone_override or tab.bone)

			if (!bone) then return end

			pos, ang = Vector(0,0,0), Angle(0,0,0)
			local m = ent:GetBoneMatrix(bone)
			if (m) then
				pos, ang = m:GetTranslation(), m:GetAngles()
			end

			if (IsValid(self.Owner) and self.Owner:IsPlayer() and
				ent == self.Owner:GetViewModel() and self.ViewModelFlip) then
				ang.r = -ang.r // Fixes mirrored models
			end

		end

		return pos, ang
	end

	function SWEP:CreateModels( tab )

		if (!tab) then return end

		// Create the clientside models here because Garry says we can't do it in the render hook
		for k, v in pairs( tab ) do
			if (v.type == "Model" and v.model and v.model != "" and (!IsValid(v.modelEnt) or v.createdModel != v.model) and
					string.find(v.model, ".mdl") and file.Exists (v.model, "GAME") ) then

				v.modelEnt = ClientsideModel(v.model, RENDER_GROUP_VIEW_MODEL_OPAQUE)
				if (IsValid(v.modelEnt)) then
					v.modelEnt:SetPos(self:GetPos())
					v.modelEnt:SetAngles(self:GetAngles())
					v.modelEnt:SetParent(self)
					v.modelEnt:SetNoDraw(true)
					v.createdModel = v.model
				else
					v.modelEnt = nil
				end

			elseif (v.type == "Sprite" and v.sprite and v.sprite != "" and (!v.spriteMaterial or v.createdSprite != v.sprite)
				and file.Exists ("materials/"..v.sprite..".vmt", "GAME")) then

				local name = v.sprite.."-"
				local params = { ["$basetexture"] = v.sprite }
				// make sure we create a unique name based on the selected options
				local tocheck = { "nocull", "additive", "vertexalpha", "vertexcolor", "ignorez" }
				for i, j in pairs( tocheck ) do
					if (v[j]) then
						params["$"..j] = 1
						name = name.."1"
					else
						name = name.."0"
					end
				end

				v.createdSprite = v.sprite
				v.spriteMaterial = CreateMaterial(name,"UnlitGeneric",params)

			end
		end

	end

	local allbones
	local hasGarryFixedBoneScalingYet = false

	function SWEP:UpdateBonePositions(vm)

		if self.ViewModelBoneMods then

			if (!vm:GetBoneCount()) then return end

			// !! WORKAROUND !! //
			// We need to check all model names :/
			local loopthrough = self.ViewModelBoneMods
			if (!hasGarryFixedBoneScalingYet) then
				allbones = {}
				for i=0, vm:GetBoneCount() do
					local bonename = vm:GetBoneName(i)
					if (self.ViewModelBoneMods[bonename]) then
						allbones[bonename] = self.ViewModelBoneMods[bonename]
					else
						allbones[bonename] = {
							scale = Vector(1,1,1),
							pos = Vector(0,0,0),
							angle = Angle(0,0,0)
						}
					end
				end

				loopthrough = allbones
			end
			// !! ----------- !! //

			for k, v in pairs( loopthrough ) do
				local bone = vm:LookupBone(k)
				if (!bone) then continue end

				// !! WORKAROUND !! //
				local s = Vector(v.scale.x,v.scale.y,v.scale.z)
				local p = Vector(v.pos.x,v.pos.y,v.pos.z)
				local ms = Vector(1,1,1)
				if (!hasGarryFixedBoneScalingYet) then
					local cur = vm:GetBoneParent(bone)
					while(cur >= 0) do
						local pscale = loopthrough[vm:GetBoneName(cur)].scale
						ms = ms * pscale
						cur = vm:GetBoneParent(cur)
					end
				end

				s = s * ms
				// !! ----------- !! //

				if vm:GetManipulateBoneScale(bone) != s then
					vm:ManipulateBoneScale( bone, s )
				end
				if vm:GetManipulateBoneAngles(bone) != v.angle then
					vm:ManipulateBoneAngles( bone, v.angle )
				end
				if vm:GetManipulateBonePosition(bone) != p then
					vm:ManipulateBonePosition( bone, p )
				end
			end
		else
			self:ResetBonePositions(vm)
		end

	end

	function SWEP:ResetBonePositions(vm)

		if (!vm:GetBoneCount()) then return end
		for i=0, vm:GetBoneCount() do
			vm:ManipulateBoneScale( i, Vector(1, 1, 1) )
			vm:ManipulateBoneAngles( i, Angle(0, 0, 0) )
			vm:ManipulateBonePosition( i, Vector(0, 0, 0) )
		end

	end

	/**************************
		Global utility code
	**************************/

	// Fully copies the table, meaning all tables inside this table are copied too and so on (normal table.Copy copies only their reference).
	// Does not copy entities of course, only copies their reference.
	// WARNING: do not use on tables that contain themselves somewhere down the line or you'll get an infinite loop
	function table.FullCopy( tab )

		if (!tab) then return nil end

		local res = {}
		for k, v in pairs( tab ) do
			if (type(v) == "table") then
				res[k] = table.FullCopy(v) // recursion ho!
			elseif (type(v) == "Vector") then
				res[k] = Vector(v.x, v.y, v.z)
			elseif (type(v) == "Angle") then
				res[k] = Angle(v.p, v.y, v.r)
			else
				res[k] = v
			end
		end

		return res

	end

end

