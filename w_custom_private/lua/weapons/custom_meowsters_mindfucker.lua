
if ( SERVER ) then
	AddCSLuaFile()

	

end

if ( CLIENT ) then
	
	SWEP.SlotPos = 5

	killicon.Add( "weapon_nyangun", "nyan/killicon", color_white )
	SWEP.WepSelectIcon = Material( "nyan/selection.png" )
	SWEP.BounceWeaponIcon = false
	SWEP.DrawWeaponInfoBox = false
end

   SWEP.Slot = 6968
SWEP.Kind = WEAPON_EQUIP1
SWEP.LimitedStock = false
SWEP.Icon = "vgui/ttt_fun_killicons/nyancat.png" 

SWEP.Base = "aa_base"
SWEP.PrintName = "Greens Trophygun"
SWEP.Category = "Robotboy655's Weapons"
SWEP.ViewModel		= "models/weapons/cstrike/c_shot_m3super90.mdl"
SWEP.WorldModel		= "models/weapons/w_shot_m3super90.mdl"
SWEP.Spawnable = true
SWEP.UseHands = true
SWEP.AdminSpawnable = true
SWEP.AutoSpawnable = false
SWEP.AutoSwitchTo = false
SWEP.AutoSwitchFrom = false
SWEP.HoldType = "smg"

SWEP.ViewModelFlip = false

SWEP.Primary.ClipSize = 999
SWEP.Primary.Delay = 0.6
SWEP.Primary.DefaultClip = 999
SWEP.Primary.Automatic = true
SWEP.Primary.Ammo = "none"

SWEP.Secondary.ClipSize = 999
SWEP.Secondary.Delay = 0.6
SWEP.Secondary.DefaultClip = 999
SWEP.Secondary.Automatic = false
SWEP.Secondary.Ammo = "none"

function SWEP:Initialize()
	self:SetHoldType( self.HoldType or "pistol" )
end

function SWEP:DrawWeaponSelection( x, y, wide, tall, alpha )
	surface.SetDrawColor( 255, 255, 255, alpha )
	surface.SetMaterial( self.WepSelectIcon )
	surface.DrawTexturedRect( x + 10, y, 128, 128 )
end


function SWEP:PrimaryAttack()
	if ( not self:CanSecondaryAttack() ) then return end
	self:EmitSound( "weapons/nyan/nya" .. math.random( 1, 2 ) .. ".mp3", 100, math.random( 85, 100 ) )

	local bullet = {}
	bullet.Num = 12
	bullet.Src = self.Owner:GetShootPos()
	bullet.Dir = self.Owner:GetAimVector()
	bullet.Spread = Vector( 0.11, 0.11, 0 )
	bullet.Tracer = 1
	bullet.Force = 10
	bullet.Damage = 9
	//bullet.AmmoType = "Ar2AltFire"
	bullet.TracerName = "rb655_nyan_tracer"
	self.Owner:FireBullets( bullet )

	self.Weapon:SendWeaponAnim( ACT_VM_SECONDARYATTACK )
	self.Owner:SetAnimation( PLAYER_ATTACK1 )

	self.Weapon:SetNextPrimaryFire( CurTime() + self.Secondary.Delay )
	self.Weapon:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
end

function SWEP:SecondaryAttack()
    self:Fly()
end

function SWEP:Reload()
	if ( !self.Owner:KeyPressed( IN_RELOAD ) ) then return end
	if ( self:GetNextPrimaryFire() > CurTime() ) then return end
    if self.Owner:KeyDown(IN_USE) then
        if ( SERVER ) then
            local ang = self.Owner:EyeAngles()
            local ent = ents.Create( "ent_nyan_bomb" )
            if ( IsValid( ent ) ) then
                ent:SetPos( self.Owner:GetShootPos() + ang:Forward() * 28 + ang:Right() * 24 - ang:Up() * 8 )
                ent:SetAngles( ang )
                ent:SetOwner( self.Owner )
                ent:Spawn()
                ent:Activate()

                local phys = ent:GetPhysicsObject()
                if ( IsValid( phys ) ) then phys:Wake() phys:AddVelocity( ent:GetForward() * 1337 ) end
            end
        end

        self:SendWeaponAnim( ACT_VM_SECONDARYATTACK )
        self.Owner:SetAnimation( PLAYER_ATTACK1 )

        self:EmitSound( "weapons/nyan/nya" .. math.random( 1, 2 ) .. ".mp3", 100, math.random( 60, 80 ) )
        self:SetNextPrimaryFire( CurTime() + 0.5 )
        self:SetNextSecondaryFire( CurTime() + 0.95 )
        return
    end
    self:Freeze()
    self:EmitSound( "weapons/nyan/nya" .. math.random( 1, 2 ) .. ".mp3", 100, math.random( 85, 100 ) )

    local bullet = {}
    bullet.Num = 1
    bullet.Src = self.Owner:GetShootPos()
    bullet.Dir = self.Owner:GetAimVector()
    bullet.Spread = Vector( 0,0, 0 )
    bullet.Tracer = 1
    bullet.Force = 10
    bullet.Damage = 15
            //bullet.AmmoType = "Ar2AltFire"
    bullet.TracerName = "rb655_nyan_tracer"
    self.Owner:FireBullets( bullet )
    self:SetNextPrimaryFire( CurTime() + 1 )
    self:SetNextSecondaryFire( CurTime() + 1 )
end

function SWEP:DoImpactEffect( trace, damageType )
	local effectdata = EffectData()
	effectdata:SetStart( trace.HitPos )
	effectdata:SetOrigin( trace.HitNormal + Vector( math.Rand( -0.5, 0.5 ), math.Rand( -0.5, 0.5 ), math.Rand( -0.5, 0.5 ) ) )
	util.Effect( "rb655_nyan_bounce", effectdata )

	return true
end

function SWEP:FireAnimationEvent( pos, ang, event )
	return true
end

function SWEP:KillSounds()
	if ( self.BeatSound ) then self.BeatSound:Stop() self.BeatSound = nil end
	if ( self.LoopSound ) then self.LoopSound:Stop() self.LoopSound = nil end
end

function SWEP:OnRemove()
	self:KillSounds()
end

function SWEP:OnDrop()
	self:KillSounds()
    self:Remove()
end

function SWEP:Deploy()
	self.Weapon:SendWeaponAnim( ACT_VM_DRAW )
	self.Weapon:SetNextPrimaryFire( CurTime() + self.Weapon:SequenceDuration() )
	
	if ( CLIENT ) then return true end


	return true
end

function SWEP:Holster()
	self:KillSounds()
	return true
end

SWEP.NextRegenAmmo = 0
function SWEP:Think()
	if ( self.Owner:IsPlayer() && self.Owner:KeyReleased( IN_ATTACK ) ) then
		if ( self.LoopSound ) then self.LoopSound:ChangeVolume( 0, 0.1 ) end
		if ( self.BeatSound ) then self.BeatSound:ChangeVolume( 1, 0.1 ) end
    end
    if self.NextRegenAmmo < CurTime() then
        if self:Clip1() < 200 then
            self.NextRegenAmmo = CurTime() + 3
            self:SetClip1(self:Clip1()+2)
        end
    end
end
