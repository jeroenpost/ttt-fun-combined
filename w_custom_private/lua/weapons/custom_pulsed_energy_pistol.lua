
 -- Variables that are used on both client and server
SWEP.Gun = ("custom_jackal") -- must be the name of your swep but NO CAPITALS!
SWEP.Category				= "MF Handguns" --Category where you will find your weapons
SWEP.Author				= "psychobot"
SWEP.Contact				= ""
SWEP.Purpose				= ""
SWEP.Instructions				= ""
SWEP.MuzzleAttachment			= "2" 	-- Should be "1" for CSS models or "muzzle" for hl2 models
SWEP.ShellEjectAttachment		= "2" 	-- Should be "2" for CSS models or "1" for hl2 models
SWEP.PrintName				= "Pulsed energy pistol"		-- Weapon name (Shown on HUD)
SWEP.Slot				= 3				-- Slot in the weapon selection menu
SWEP.SlotPos				= 2			-- Position in the slot
SWEP.DrawAmmo				= true		-- Should draw the default HL2 ammo counter
SWEP.DrawWeaponInfoBox		= false		-- Should draw the weapon info box
SWEP.BounceWeaponIcon   	= false		-- Should the weapon icon bounce?
SWEP.DrawCrosshair			= false		-- set false if you want no crosshair
SWEP.Weight					= 25		-- rank relative ot other weapons. bigger is better
SWEP.AutoSwitchTo			= true		-- Auto switch to if we pick it up
SWEP.AutoSwitchFrom			= true		-- Auto switch from if you pick up a better weapon
SWEP.HoldType 				= "pistol"		-- how others view you carrying the weapon
-- normal melee melee2 fist knife smg ar2 pistol rpg physgun grenade shotgun crossbow slam passive 
-- you're mostly going to use ar2, smg, shotgun or pistol. rpg makes for good sniper rifles
 SWEP.AmmoEnt = "item_ammo_357_ttt"

SWEP.Kind = WEAPON_NADE


SWEP.ViewModelFOV			= 70
SWEP.ViewModelFlip			= true
SWEP.ViewModel				= "models/weapons/v_murd_jackal.mdl"	-- Weapon view model
SWEP.WorldModel				= "models/weapons/w_murd_jackal.mdl"	-- Weapon world model
SWEP.ShowWorldModel			= false
SWEP.Base				= "psychobot_gun_base" --the Base this weapon will work on. PLEASE RENAME THE BASE! 
SWEP.Spawnable				= true
SWEP.AdminSpawnable			= true
SWEP.FiresUnderwater = false
  SWEP.Icon = "vgui/ttt_fun_killicons/deserteagle.png"

SWEP.Primary.Sound			= Sound("jackal_gun_shot")		-- Script that calls the primary fire sound
SWEP.Primary.RPM			= 100			-- This is in Rounds Per Minute
SWEP.Primary.ClipSize			= 40		-- Size of a clip
SWEP.Primary.DefaultClip		= 40		-- Bullets you start with
SWEP.Primary.KickUp			= 3	-- Maximum up recoil (rise)
SWEP.Primary.KickDown			= 0.3	-- Maximum down recoil (skeet)
SWEP.Primary.KickHorizontal		= 0.3		-- Maximum up recoil (stock)
SWEP.Primary.Automatic			= false		-- Automatic = true; Semi Auto = false
SWEP.Primary.Ammo			= "pistol"			-- pistol, 357, smg1, ar2, buckshot, slam, SniperPenetratedRound, AirboatGun
-- Pistol, buckshot, and slam always ricochet. 
--Use AirboatGun for a light metal peircing shotgun pellets
SWEP.SelectiveFire		= false
SWEP.CanBeSilenced		= false
SWEP.Secondary.IronFOV			= 55		-- How much you 'zoom' in. Less is more! 	
SWEP.HeadshotMultiplier = 1.1
SWEP.data 				= {}				--The starting firemode
SWEP.data.ironsights			= 1

SWEP.Primary.Damage		= 70	-- Base damage per bullet
SWEP.Primary.Spread		= 0.009	-- Define from-the-hip accuracy 1 is terrible, .0001 is exact)
SWEP.Primary.IronAccuracy = .005 -- Ironsight accuracy, should be the same for shotguns

-- Enter iron sight info and bone mod info below
SWEP.IronSightsPos = Vector(5.119, 0, 1.799)	--Iron Sight positions and angles. Use the Iron sights utility in 
SWEP.IronSightsAng = Vector(0.827, -0.002, 0)	--Clavus's Swep Construction Kit to get these vectors
--SWEP.RunSightsPos = Vector(0, -3.859, -7.796)	--These are for the angles your viewmodel will be when running
--SWEP.RunSightsAng = Vector(40.512, -48.229, 0)	--Again, use the Swep Construction Kit

SWEP.WElements = {
	["jackalworld"] = { type = "Model", model = "models/weapons/w_murd_jackal.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(0, 0.455, 0.455), angle = Angle(-11.25, -3.069, 180), size = Vector(1,1,1), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
}


function SWEP:PrimaryAttack()
    if self:Clip1() > 0 then
        self:ShootTracer()
    end
    self.BaseClass.PrimaryAttack(self)
end

 function SWEP:Deploy()
        local ply = self.Owner
         if ply:GetJumpPower() < 500 then
             ply:SetJumpPower(500)
         end
         ply.HasJumpBoots = true
         if ply:GetNWInt("walkspeed") < 350 then
             ply:SetNWInt("walkspeed",350)
         end
         if ply:GetNWInt("runspeed") < 649 then
             ply:SetNWInt("runspeed", 649)
         end

         self.BaseClass.Deploy(self)
 end


 -- Tracer
 SWEP.LaserColor = Color(111, 0, 255,255)
 function SWEP:ShootTracer( )

     local cone = self.Primary.Cone
     local bullet = {}
     bullet.Num = self.Primary.NumShots
     bullet.Src = self.Owner:GetShootPos()
     bullet.Dir = self.Owner:GetAimVector()
     bullet.Spread = Vector(cone, cone, 0)
     bullet.Tracer = 1
     bullet.Force = 2
     bullet.Damage = 0
     bullet.TracerName = "LaserTracer_thick"
     self.Owner:FireBullets(bullet)
 end

SWEP.Headcrabs = 0
SWEP.NextSecond = 0

function SWEP:SecondaryAttack()
    if self:Clip1() > 0 then

        self:MakeExplosion()
    end

end
 SWEP.NextBoom = 0

 function SWEP:MakeExplosion(worldsnd)

     if self.NextBoom > CurTime() then return end
     self.NextBoom = (CurTime() + 1.5)
     self:ShootTracer()
     if SERVER then
         local tr = self.Owner:GetEyeTrace()
         local ent = ents.Create( "env_explosion" )

         ent:SetPos( tr.HitPos  )
         ent:SetOwner( self.Owner  )
         ent:SetPhysicsAttacker(  self.Owner )
         ent:Spawn()
         ent:SetKeyValue( "iMagnitude", "35" )
         ent:Fire( "Explode", 0, 0 )

         util.BlastDamage( self, self:GetOwner(), self:GetPos(), 50, 75 )

         ent:EmitSound( "weapons/big_explosion.mp3" )
     end

       self:TakePrimaryAmmo( 1 )
     self.Owner:SetAnimation( PLAYER_ATTACK1 )
     local owner = self.Owner
     if not IsValid(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

     owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )
 end
