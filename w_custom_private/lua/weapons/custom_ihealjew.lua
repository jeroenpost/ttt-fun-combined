SWEP.Base = "gb_camo_base"
SWEP.ViewModelFOV	= 54
SWEP.HoldType = "smg"
SWEP.PrintName = "IHealJew"
SWEP.ViewModel		= "models/weapons/cstrike/c_rif_m4a1.mdl"
SWEP.WorldModel		= "models/weapons/w_rif_m4a1.mdl"
SWEP.AutoSpawnable = false

SWEP.HoldType = "ar2"
SWEP.Slot = 4

if CLIENT then

    SWEP.Author = "GreenBlack"

    SWEP.Icon = "vgui/ttt_fun_killicons/m4a1.png"
end

SWEP.Camo = 0
SWEP.Primary.Delay			= 0.1
SWEP.Primary.Recoil			= 0.001
SWEP.Primary.Automatic = true
SWEP.Primary.Ammo = "Pistol"
SWEP.Primary.Damage = 12
SWEP.Primary.Cone = 0.005
SWEP.Primary.ClipSize = 90
SWEP.Primary.ClipMax = 180
SWEP.Primary.DefaultClip = 90
SWEP.AutoSpawnable      = false
SWEP.AmmoEnt = "item_ammo_pistol_ttt"

SWEP.Primary.Sound = Sound( "Weapon_M4A1.Single" )


SWEP.IronSightsPos = Vector(-4, -6.2, 0.55)
SWEP.IronSightsAng = Vector(2.599, -1.3, -3.6)


function SWEP:SetZoom(state)
    if CLIENT then return end
    if not (IsValid(self.Owner) and self.Owner:IsPlayer()) then return end
    if state then
        self.Owner:SetFOV(35, 0.5)
    else
        self.Owner:SetFOV(0, 0.2)
    end
end

-- Add some zoom to ironsights for this gun
SWEP.used = 0
function SWEP:SecondaryAttack()
    if  CLIENT or not IsValid(self.Owner) then return end
    if not timebetween then timebetween =2 end

    local tr = self.Owner:GetEyeTrace()
    local hitEnt = tr.Entity
    if not hitEnt.lastgothealfromhealshot then hitEnt.lastgothealfromhealshot = 0 end

    if self.used < 25 and hitEnt:IsPlayer() and hitEnt:Health() < 150 and (hitEnt.lastgothealfromhealshot + timebetween) < CurTime()then
                hitEnt:SetHealth(self.Owner:Health() + 10)
        self.used = self.used + 1

                if not worldsnd then
                    self.Weapon:EmitSound( self.Primary.Sound, self.Primary.SoundLevel )
                elseif SERVER then
                    sound.Play(self.Primary.Sound, self:GetPos(), self.Primary.SoundLevel)
                end

                self:ShootBullet( 0, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )
    end


end

function SWEP:PreDrop()
    self:SetZoom(false)
    self:SetIronsights(false)
    return self.BaseClass.PreDrop(self)
end

function SWEP:Reload()
    self.Weapon:DefaultReload( ACT_VM_RELOAD );
    self:SetIronsights( false )
    self:SetZoom(false)
end


function SWEP:Holster()
    self:SetIronsights(false)
    self:SetZoom(false)
    return true
end

function SWEP:Reload()
    if self.Owner:KeyDown(IN_USE) then
        self:ShootFlare()
        return
    end
    self.BaseClass.Reload(self)
end

