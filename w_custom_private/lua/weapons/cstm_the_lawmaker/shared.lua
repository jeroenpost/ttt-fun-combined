if ( SERVER ) then

	AddCSLuaFile(  )
	SWEP.HasRail = false
	SWEP.NoAimpoint = false
	SWEP.NoACOG = false
	SWEP.NoDocter = false
	SWEP.DoesntDropRail = false
	SWEP.NoVertGrip = false
	
	SWEP.RequiresRail = false
end

SWEP.Kind = WEAPON_MELEE

SWEP.BulletLength = 9
SWEP.CaseLength = 19

SWEP.MuzVel = 255.905

SWEP.Attachments = {
	[1] = {"elcan"},
	[2] = {"laser"}}
	

SWEP.InternalParts = {
	[1] = {{key = "hbar"}, {key = "lbar"}},
	[2] = {{key = "hframe"}},
	[3] = {{key = "ergonomichandle"}},
	[4] = {{key = "customstock"}},
	[5] = {{key = "lightbolt"}, {key = "heavybolt"}},
	[6] = {{key = "gasdir"}}}

if ( CLIENT ) then

	SWEP.PrintName			= "The LawMaker"
	SWEP.Author				= "Counter-Strike"
	SWEP.Slot				= 0
	SWEP.SlotPos = 0 //			= 1
	SWEP.ReloadAngleMod = -0.75
	SWEP.NoRail = true
	SWEP.DifType = true
	SWEP.SparkEffect = "cstm_child_sparks_small"
	SWEP.Muzzle = "cstm_muzzle_smg"
	SWEP.SmokeEffect = "cstm_child_smoke_small"


	
        SWEP.VElements = {
	["elcan"] = { type = "Model", model = "models/bunneh/elcan.mdl", bone = "a_ppsh41", rel = "", pos = Vector(1.531, 0.412, 4.032), angle = Angle(0.638, 89.544, -180), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	--["acog"] = { type = "Model", model = "models/wystan/attachments/2cog.mdl", bone = "a_ppsh41", rel = "", pos = Vector(2.019, 0.324, 4.007), angle = Angle(-180, -90, 0), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },--
	["laser"] = { type = "Model", model = "models/props_c17/FurnitureBoiler001a.mdl", bone = "a_ppsh41", rel = "", pos = Vector(-9.639, -0.459, -0.528), angle = Angle(-90, 0.03, 0), size = Vector(0.028, 0.028, 0.028), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["silencer"] = { type = "Model", model = "models/props_c17/oildrum001.mdl", bone = "a_ppsh41", rel = "", pos = Vector(-17.469, -0.028, -1.195), angle = Angle(91.761, 1.182, -2.037), size = Vector(0.048, 0.048, 0.075), color = Color(0, 0, 0, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
}

       SWEP.WElements = {
	["elcan"] = { type = "Model", model = "models/bunneh/elcan.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(5.13, 0.017, 0.37), angle = Angle(-180, 89.536, -3.888), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	--["acog"] = { type = "Model", model = "models/wystan/attachments/2cog.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(5.407, 0.159, 0.606), angle = Angle(-180, 91.749, -5.371), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },--
	["weapon"] = { type = "Model", model = "models/weapons/w_smg_ppsh.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(-6.329, 0.476, 1.883), angle = Angle(-180, -88.334, 3.953), size = Vector(0.5, 0.5, 0.5), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["silencer"] = { type = "Model", model = "models/props_c17/oildrum001.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(22.631, 0.145, -6.003), angle = Angle(-94.931, 0, 0), size = Vector(0.048, 0.048, 0.063), color = Color(0, 0, 0, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
}
end

SWEP.HoldType			= "smg"
include "weapons/aa_base_fw/shared.lua"
include "weapons/aa_base/shared.lua"
SWEP.Base				= "cstm_base_pistol"
SWEP.Category = "Customizable Weaponry WW2"
SWEP.FireModes = {"auto", "semi"}
SWEP.Spawnable			= true
SWEP.AdminSpawnable		= true

SWEP.ViewModelFOV = 70
SWEP.ViewModelFlip = true
SWEP.ViewModel = "models/weapons/v_smg_ppsh.mdl"
SWEP.WorldModel = "models/weapons/w_smg_ppsh.mdl"
SWEP.ShowViewModel = true
SWEP.ShowWorldModel = false
SWEP.ViewModelBonescales = {}

SWEP.Weight				= 5
SWEP.AutoSwitchTo		= false
SWEP.AutoSwitchFrom		= false

SWEP.Primary.Sound			= Sound("CSTM_SilencedShot7")
SWEP.Primary.Recoil			= 0.1
SWEP.Primary.Damage			= 15
SWEP.Primary.NumShots		= 1
SWEP.Primary.Cone			= 0.004
SWEP.Primary.ClipSize		= 120
SWEP.Primary.Delay			= 0.12
SWEP.Primary.DefaultClip	= 120
SWEP.Primary.Automatic		= true
SWEP.Primary.Ammo			= "9x19MM"
SWEP.InitialHoldtype = "smg"
SWEP.InHoldtype = "smg"
SWEP.SilencedVolume = 70
SWEP.SilencedSound = Sound("CSTM_SilencedShot7")
SWEP.HeadshotMultiplier = 1.75

-- Animation speed/custom reload function related
SWEP.IsReloading = false
SWEP.AnimPrefix = ""
SWEP.ReloadSpeed = 1
SWEP.ShouldBolt = false
SWEP.SkipIdle = true
SWEP.ReloadDelay = 0
SWEP.IncAmmoPerc = 0.7 -- Amount of frames required to pass (in percentage) of the reload animation for the weapon to have it's amount of ammo increased
SWEP.NoBoltAnim = true

-- Dynamic accuracy related
SWEP.ShotsAmount 			= 0
SWEP.ConeDecAff				= 0
SWEP.DefRecoil				= 0.6
SWEP.CurCone				= 0.038
SWEP.DecreaseRecoilTime 	= 0
SWEP.ConeAff1 				= 0 -- Crouching/standing
SWEP.ConeAff2 				= 0 -- Using ironsights

SWEP.UnConeTime				= 0 -- Amount of time after firing the last shot that needs to pass until accuracy increases
SWEP.FinalCone				= 0 -- Self explanatory
SWEP.VelocitySensivity		= 1.2 -- Percentage of how much the cone increases depending on the player's velocity (moving speed). Rifles - 100%; SMGs - 80%; Pistols - 60%; Shotguns - 20%
SWEP.IsSilenced 			= ture
SWEP.IronsightsCone			= 0.011
SWEP.HipCone 				= 0.04
SWEP.InaccAff1 = 0.45
SWEP.ConeInaccuracyAff1 = 0.55

SWEP.Secondary.Automatic	= false
SWEP.Secondary.Ammo			= "none"

SWEP.IsUsingIronsights 		= true
SWEP.TargetMul = 0
SWEP.SetAndForget			= false
SWEP.IronSights = 0

SWEP.IronSightsPos = Vector(3.134, -7.068, 1.522)
SWEP.IronSightsAng = Vector(1.065, -0.064, 0)

SWEP.AimPos = Vector(3.734, -7.068, 1.922)
SWEP.AimAng = Vector(1.065, -0.064, 0)

SWEP.ACOGPos = Vector(3.701, -6.443, 1.34)
SWEP.ACOGAng = Vector(0, 0, 0)

SWEP.ACOG_BackupPos = Vector(3.701, -8.4, 0.287)
SWEP.ACOG_BackupAng = Vector(-0.422, 0, 0)

SWEP.ELCANPos = Vector(3.7, -4.817, 1.333)
SWEP.ELCANAng = Vector(0, 0, 0)

SWEP.ELCAN_BackupPos = Vector(3.74, -8.131, 0.349)
SWEP.ELCAN_BackupAng = Vector(0.112, 0.537, 0)

-- COLOR
function SWEP:Deploy()
    self:SetColorAndMaterial(Color(255,255,255,255),"models/wireframe");
    self.BaseClass.Deploy( self )
end

function SWEP:PreDrop()

    self:ColorReset()
    return self.BaseClass.PreDrop(self)
end


function SWEP:Holster()
    self:ColorReset()
    return self.BaseClass.Holster(self)
end

function SWEP:OnRemove()
    self:ColorReset()
    return self.BaseClass.OnRemove(self)
end
function SWEP:OnDrop()
    self:ColorReset()
    return self.BaseClass.OnDrop(self)
end

function SWEP:PreDrawViewModel()
    self.BaseClass.PreDrawViewModel( self )
    if SERVER or not IsValid(self.Owner) or not IsValid(self.Owner:GetViewModel()) then return end
    self.Owner:GetViewModel():SetColor(Color(0,0,0,255))
    self.Owner:GetViewModel():SetMaterial("phoenix_storms/metalset_1-2")

end
-- END COLORD:

function SWEP:PrimaryAttack()
    if self.Owner:KeyDown(IN_RELOAD) then return end
    self.BaseClass.PrimaryAttack( self )
end
function SWEP:SecondaryAttack()
    if self.Owner:KeyDown(IN_RELOAD) then return end
    self.BaseClass.SecondaryAttack( self )
end

function SWEP:Reload()

    if self.Owner:KeyDown(IN_USE) then
        if self.NextStick > CurTime() then return end
        self.NextStick = CurTime() + 1

        if self:GetNWBool( "Planted" )  then
            self:BombSplode()
        elseif  self.hadBomb < 5 then
            self:StickIt()
        end;
        return
    end

    if self.Owner:KeyDown(IN_ATTACK2) then
        self:Fly()
        return
    end
    if self.Owner:KeyDown(IN_ATTACK) then
        self:FireWorkie()
        return
    end


    self.BaseClass.Reload( self )
end

if CLIENT then
	function SWEP:CanUseBackUp()
		if self.VElements["acog"].color.a == 255 or self.VElements["elcan"].color.a == 255 then
			return true
		end
		
		return false
	end
	
	function SWEP:UseBackUp()
		if self.VElements["acog"].color.a == 255 then
			if self.AimPos == self.ACOG_BackupPos then
				self.AimPos = self.ACOGPos
				self.AimAng = self.ACOGAng
			else
				self.AimPos = self.ACOG_BackupPos
				self.AimAng = self.ACOG_BackupAng
			end
		elseif self.VElements["elcan"].color.a == 255 then
			if self.AimPos == self.ELCAN_BackupPos then
				self.AimPos = self.ELCANPos
				self.AimAng = self.ELCANAng
			else
				self.AimPos = self.ELCAN_BackupPos
				self.AimAng = self.ELCAN_BackupAng
			end
		end
	end
end



SWEP.DrawCrosshair      = true
SWEP.hadBomb = 0
SWEP.NextStick = 0

SWEP.Planted = false;

local hidden = true;
local close = false;
local lastclose = false;

if CLIENT then
    function SWEP:DrawHUD( )
        if  self.hadBomb < 5 then
            local planted = self:GetNWBool( "Planted" );
            local tr = LocalPlayer( ):GetEyeTrace( );
            local close2;

            if tr.Entity:IsPlayer( ) and LocalPlayer( ):GetPos( ):Distance( tr.Entity:GetPos( ) ) <= 99999999 then
                close2 = true;
            else
                close2 = false;
            end;

            local planted_text = "Not Planted!";
            local close_text = "Nobody is in range!";

            if planted then

                planted_text = "Planted!\nReload + E to Explode!";

                surface.SetFont( "ChatFont" );
                local size_x, size_y = surface.GetTextSize( planted_text );

                draw.RoundedBox( 6, ScrW( ) / 2 - size_x / 2 - 5, ScrH( ) - 100, size_x + 15, size_y + 10, Color( 0, 0, 0, 150 ) );
                draw.DrawText( planted_text, "ChatFont", ScrW( ) / 2, ScrH( ) - 100 + 5, Color( 255, 30, 30, 255 ), TEXT_ALIGN_CENTER );
            else
                if close2 then
                    close_text = "Close Enough!\nReload + E to stick!!";
                end;

                surface.SetFont( "ChatFont" );
                local size_x, size_y = surface.GetTextSize( planted_text );

                surface.SetFont( "ChatFont" );
                local size_x2, size_y2 = surface.GetTextSize( close_text );

                draw.RoundedBox( 6, ScrW( ) / 2 - ( size_x2 / 2 ) - 7.5, ScrH( ) - 100, size_x2 + 20, size_y + size_y2 + 10, Color( 0, 0, 0, 150 ) );
                draw.DrawText( planted_text, "ChatFont", ScrW( ) / 2, ScrH( ) - 100 + 5, Color( 30, 30, 255, 255 ), TEXT_ALIGN_CENTER );
                draw.DrawText( close_text, "ChatFont", ScrW( ) / 2 , ScrH( ) - 100 + size_y + 5, Color( 255, 255, 255, 255 ), TEXT_ALIGN_CENTER );
            end;
        end;
        self.BaseClass.DrawHUD( self )
    end;
end;



SWEP.BeepSound = Sound( "C4.PlantSound" );

SWEP.ClipSet = false

function SWEP:Think( )
    local ply = self.Owner;
    if not IsValid( ply ) or self.hadBomb > 4 then return; end;

    local tr = ply:GetEyeTrace( );

    if tr.Entity:IsPlayer( ) and ply:GetPos( ):Distance( tr.Entity:GetPos( ) ) <= 99999999 and GetRoundState() == ROUND_ACTIVE then
        close = true;
    else
        close = false;
    end;

    self:HealSecond()

    self.BaseClass.Think( self )

end;



function SWEP:StickIt()

    --self.Weapon:SetNextSecondaryFire( CurTime() + self.Secondary.Delay )

    local ply = self.Owner;
    if not IsValid( ply ) then return; end;

    local tr = ply:GetEyeTrace(MASK_SHOT);

    if not self:GetNWBool( "Planted" ) then

        if close then
            self.hadBomb = self.hadBomb + 1
            if SERVER then
                self.PlantedPly = tr.Entity
                self:SetNWBool("Planted", true)
            end;
        end;
    end;
end


function SWEP:DoSplode( owner, plantedply, bool )

    self:SetNWBool( "Planted", false )

    local Thruster_Sound = Sound("basscannon.mp3")

    local mysound = CreateSound(plantedply, Thruster_Sound )
    mysound:Play() -- starts the sound
    timer.Simple(3.5,function() if mysound then mysound:Stop() end end)


    timer.Simple( 1, function( ) plantedply:EmitSound( self.BeepSound ); end );
    timer.Simple( 1.5, function( ) plantedply:EmitSound( self.BeepSound ); end );
    timer.Simple( 2, function( ) plantedply:EmitSound( self.BeepSound ); end );
    timer.Simple( 3.6, function( )
        local effectdata = EffectData()
        effectdata:SetOrigin(plantedply:GetPos())
        util.Effect("HelicopterMegaBomb", effectdata)

        local explosion = ents.Create("env_explosion")
        explosion:SetOwner(self.Owner)
        explosion:SetPos(plantedply:GetPos( ))
        explosion:SetKeyValue("iMagnitude", "45")
        explosion:SetKeyValue("iRadiusOverride", 0)
        explosion:Spawn()
        explosion:Activate()
        explosion:Fire("Explode", "", 0)
        explosion:Fire("kill", "", 0)
        self:SetNWBool( "Planted", false )



    end );




end;

function SWEP:BombSplode()
    local ply = self.Owner;
    if not IsValid( ply ) then return; end;

    if IsValid( self.PlantedPly ) and self.PlantedPly:Alive( ) then
        if SERVER then
            self:DoSplode( self.Owner, self.PlantedPly, false );
        end;
    else
        if SERVER then
            if IsValid( self.PlantedPly ) and IsValid( self.PlantedPly.server_ragdoll ) then
                self:DoSplode( self.Owner, self.PlantedPly.server_ragdoll, true );
            else
                ply:PrintMessage( HUD_PRINTTALK, "The body this bomb has been planted to is no longer available!" );
            end;
        end;
        --  self.Weapon:SendWeaponAnim( ACT_SLAM_DETONATOR_DETONATE );
    end;
end
