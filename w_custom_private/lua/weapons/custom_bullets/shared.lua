if SERVER then
   AddCSLuaFile(  )
   --resource.AddFile("materials/vgui/ttt_fun_killicons/m249.png")
end

SWEP.HoldType			= "crossbow"

SWEP.PrintName			= "Bullets"
 SWEP.Slot				= 0
if CLIENT then

   
  
   SWEP.Icon = "vgui/ttt_fun_killicons/m249.png"
   SWEP.ViewModelFlip		= false
end


SWEP.Base				= "weapon_tttbase"

SWEP.Spawnable = true
SWEP.AdminSpawnable = true

SWEP.Kind = WEAPON_MELEE



SWEP.Primary.Damage = 6
SWEP.Primary.Delay = 0.015
SWEP.Primary.Cone = 0.11
SWEP.Primary.ClipSize = 75
SWEP.Primary.ClipMax = 75
SWEP.Primary.DefaultClip	= 75
SWEP.Primary.Automatic		= true
SWEP.Primary.Ammo			= "smg1"
SWEP.AmmoEnt = "item_ammo_smg1_ttt"
SWEP.AutoSpawnable      = false
SWEP.Primary.Recoil			= 0.4
SWEP.Primary.Sound			= Sound("Weapon_m249.Single")
SWEP.Primary.SoundLevel = 10
SWEP.UseHands			= true
SWEP.ViewModelFlip		= false
SWEP.ViewModelFOV		= 54
SWEP.ViewModel			= "models/weapons/cstrike/c_mach_m249para.mdl"
SWEP.WorldModel			= "models/weapons/w_crowbar.mdl"

SWEP.HeadshotMultiplier = 2.2

SWEP.IronSightsPos = Vector(-5.96, -5.119, 2.349)
SWEP.IronSightsAng = Vector(0, 0, 0)

SWEP.nextAmmo = 0
function SWEP:Think()
    if SERVER and self.nextAmmo < CurTime() then
        self.nextAmmo = CurTime() + 2
        self:SetClip1( self:Clip1() + 1 )
    end


end

function SWEP:PrimaryAttack(worldsnd)
    self.Weapon:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
    if not self:CanPrimaryAttack() then return end

    self:ShootBullet( self.Primary.Damage, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )

    if ( self.Owner:IsValid() ) then
        local tr = self.Owner:GetEyeTrace()
        local effectdata = EffectData()
        effectdata:SetOrigin(tr.HitPos)
        effectdata:SetNormal(tr.HitNormal)
        effectdata:SetScale(1)
        -- util.Effect("effect_mad_ignition", effectdata)
        util.Decal("FadingScorch", tr.HitPos + tr.HitNormal, tr.HitPos - tr.HitNormal)

        local tracedata = {}
        tracedata.start = tr.HitPos
        tracedata.endpos = Vector(tr.HitPos.x, tr.HitPos.y, tr.HitPos.z - 10)
        tracedata.filter = tr.HitPos
        local tracedata = util.TraceLine(tracedata)
        if SERVER then

            ent = tr.Entity
            if ent:IsPlayer() then
                local eyeang = ent:EyeAngles()

                local j = math.Rand(1,10) --10
                eyeang.pitch = math.Clamp(eyeang.pitch + math.Rand(-j, j), -90, 90)
                eyeang.yaw = math.Clamp(eyeang.yaw + math.Rand(-j, j), -90, 90)
                ent:SetEyeAngles(eyeang)
            end
        end
        self:TakePrimaryAmmo( 1 )
        local owner = self.Owner
        --if not ValidEntity(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

        owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )
    end
end

