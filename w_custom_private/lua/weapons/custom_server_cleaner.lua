

if ( SERVER ) then



    --resource.AddFile("materials/vgui/ttt_fun_killicons/dualspaceshotguns.png")


end
SWEP.PrintName			= "Server Cleaner"

SWEP.ShellDelay			= 0

SWEP.Pistol				= false
SWEP.Rifle				= false
SWEP.Shotgun			= true
SWEP.Sniper				= false

SWEP.Penetration			= false
SWEP.Ricochet			= false
SWEP.ShotgunReloading		= false
SWEP.ShotgunFinish		= 0.5
SWEP.ShotgunBeginReload		= 0.3


if ( CLIENT ) then


    SWEP.Author				= "Babel Industries/TS Industries"
    SWEP.Contact            = "";
    SWEP.Instructions       = "After years of developing a trouble-free shotgun with a balance of firerate and lethalness, the TS Industries Bulk Cannon is garunteed to destroy anything in your path*. (*Anything that's organic/breakable.)"
    SWEP.Category =         "Babel Industries"

    SWEP.WElements = {
        ["some_unique_name"] = { type = "Model", model = "models/weapons/w_shot_gb1014.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(6.193, 1.156, -1.096), angle = Angle(-6.988, 0.137, 175.481), size = Vector(0.899, 0.899, 0.899), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {[2] = 1} }
    }

end

SWEP.Base				= "aa_base"

SWEP.Icon = "vgui/ttt_fun_killicons/dualspaceshotguns.png"
SWEP.Kind = WEAPON_EQUIP3
SWEP.Spawnable			= true
SWEP.AdminSpawnable		= true
SWEP.AutoSpawnable      = false
SWEP.Slot				= 8
SWEP.SlotPos			= 0

SWEP.HoldType = "shotgun"

SWEP.ViewModelFOV = 33
SWEP.ViewModelFlip = false
SWEP.ViewModel		= "models/weapons/v_shot_gb1014.mdl"
SWEP.WorldModel		= "models/weapons/w_shot_xm1014.mdl"
SWEP.ShowViewModel = true
SWEP.ShowWorldModel = true
SWEP.ViewModelBoneMods = {}


SWEP.VElements = {
    ["uadskjbsf"] = { type = "Model", model = "models/weapons/w_shot_gb1014.mdl", bone = "Base", rel = "", pos = Vector(-2.274, 9.545, 3.181), angle = Angle(-91.024, 0, 0), size = Vector(1,1,1), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
    ["uadskjbsf2"] = { type = "Model", model = "models/weapons/w_shot_gb1014.mdl", bone = "Base", rel = "", pos = Vector(-2.274, -11.365, 1.363), angle = Angle(-91.024, 0, 0), size = Vector(1,1,1), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
}

SWEP.WElements = {
    ["coll"] = { type = "Model", model = "models/weapons/w_shot_gb1014.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(0.455, 2.273, 0), angle = Angle(180, 174.886, 0), size = Vector(1,1,1), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
    ["coll+"] = { type = "Model", model = "models/weapons/w_shot_gb1014.mdl", bone = "ValveBiped.Bip01_L_Hand", rel = "", pos = Vector(0.455, 2.273, 0), angle = Angle(-5.114, -5.114, 0), size = Vector(1,1,1), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
}



function SWEP:Deploy()

    self.Owner:EmitSound( "weapons/bulkcannon/draw2.mp3" ) ;
    self.Weapon:SendWeaponAnim( ACT_VM_DRAW )
    -- self:SetDeploySpeed( self.Weapon:SequenceDuration(0.2) )
    self.WorldModel = "models/Items/AR2_Grenade.mdl"
    self.Weapon:SetModel( self.WorldModel)
    self.ShowWorldModel = false
    return true;
end

function SWEP:IsEquipment()
    return false
end

SWEP.Weight				= 5
SWEP.AutoSwitchTo		= true
SWEP.AutoSwitchFrom		= false
SWEP.Tracer				= 4

SWEP.AmmoEnt = "item_box_buckshot_ttt"
SWEP.Primary.Sound			= Sound( "weapons/bulkcannon/fire.mp3" )
SWEP.Primary.Recoil			= 0
SWEP.Primary.Damage			= 5
SWEP.Primary.NumShots		= 14
SWEP.Primary.Cone			= 0.0745
SWEP.Primary.ClipSize		= 50
SWEP.Primary.ClipMax 		= 64
SWEP.Primary.Delay			= 0.40
SWEP.Primary.DefaultClip	= 150
SWEP.Primary.Automatic		= true
SWEP.Primary.Ammo			= "buckshot"
SWEP.Secondary.Delay = 0.40

SWEP.Secondary.ClipSize		= -1
SWEP.Secondary.DefaultClip	= -1
SWEP.Secondary.ClipMax = -1
SWEP.Secondary.Automatic	= true
SWEP.Secondary.Ammo			= "none"

SWEP.IronSightsPos = Vector(-4.711, -4.427, 1.049)
SWEP.IronSightsAng = Vector(0.268, 0.768, 0)

SWEP.RunArmOffset = Vector(0.984, -5.246, -3.08)
SWEP.RunArmAngle  = Vector(24.097, 46.474, 4.59)


function SWEP:SecondaryAttack(worldsnd)
self:Fly()
end

SWEP.tracerColor = Color(90,255,90,255)
function SWEP:ShootBullet( dmg, recoil, numbul, cone )
    self:MakeAFlame()
    self.Owner:MuzzleFlash()

    if not IsFirstTimePredicted() then return end

    local sights = self:GetIronsights()

    numbul = numbul or 1
    cone   = cone   or 0.01

    local bullet = {}
    bullet.Num    = numbul
    bullet.Src    = self.Owner:GetShootPos()
    bullet.Dir    = self.Owner:GetAimVector()
    bullet.Spread = Vector( cone, cone, 0 )
    bullet.Tracer = 1
    bullet.TracerName =  "manatrace"
    bullet.Force  = 10
    bullet.Damage = dmg


    self.Owner:FireBullets( bullet )

    -- Owner can die after firebullets
    if (not IsValid(self.Owner)) or (not self.Owner:Alive()) or self.Owner:IsNPC() then return end

    if ((game.SinglePlayer() and SERVER) or
            ((not game.SinglePlayer()) and CLIENT and IsFirstTimePredicted())) then

        -- reduce recoil if ironsighting
        recoil = sights and (recoil * 0.6) or recoil

        local eyeang = self.Owner:EyeAngles()
        eyeang.pitch = eyeang.pitch - recoil
        self.Owner:SetEyeAngles( eyeang )
    end

    attacker = self.Owner
    local tracedata = {}

    tracedata.start = self.Owner:GetShootPos()
    tracedata.endpos = self.Owner:GetShootPos() + ( self.Owner:GetAimVector() * 450 )
    tracedata.filter = self.Owner
    tracedata.mins = Vector( -3,-3,-3 )
    tracedata.maxs = Vector( 3,3,3 )
    tracedata.mask = MASK_SHOT_HULL
    local trace = util.TraceHull( tracedata )

    local victim = trace.Entity



end


function SWEP:OnRemove()
    self:Holster()
end
function SWEP:CamoPreDrawViewModel()

   -- self.Owner:GetViewModel():SetMaterial(  "camos/camo7" )
end

function SWEP:PreDrawViewModel()
    self:CamoPreDrawViewModel()
end


function SWEP:OnDrop()
    self:Holster()
    self:Remove()
end

SWEP.NextThinky = 0

function SWEP:Think()





    if self.Weapon:Clip1() > self.Primary.ClipSize then
        self.Weapon:SetClip1(self.Primary.ClipSize)

    end

    if self.Weapon:GetNWBool( "reloading") == true then

        if self.Weapon:GetNWInt( "reloadtimer") < CurTime() then
            if self.unavailable then return end

            if ( self.Weapon:Clip1() >= self.Primary.ClipSize || self.Owner:GetAmmoCount( self.Primary.Ammo ) <= 0 ) then
            self.Weapon:SetNextPrimaryFire(CurTime() + 0.8)
            self.Weapon:SetNextSecondaryFire(CurTime() + 0.8)
            self.Weapon:SetNWBool( "reloading", false)
            self.Weapon:EmitSound( "weapons/bulkcannon/closelid.mp3" )
            --self.Weapon:SendWeaponAnim(ACT_SHOTGUN_RELOAD_FINISH)
            else

                self.Weapon:SetNWInt( "reloadtimer", CurTime() + 0.55 )
              --  self.Weapon:SendWeaponAnim( ACT_VM_RELOAD )
                self.Owner:RemoveAmmo( 1, self.Primary.Ammo, false )
                self.Weapon:SetClip1(  self.Weapon:Clip1() + 1 )
                self.Weapon:SetNextPrimaryFire(CurTime() + 0.8)
                self.Weapon:SetNextSecondaryFire(CurTime() + 0.8)
                self.Weapon:EmitSound( "weapons/bulkcannon/insertshell.mp3" )

                if ( self.Weapon:Clip1() >= self.Primary.ClipSize || self.Owner:GetAmmoCount( self.Primary.Ammo ) <= 0) then
                self.Weapon:SetNextPrimaryFire(CurTime() + 1.5)
                self.Weapon:SetNextSecondaryFire(CurTime() + 1.5)
                self.Weapon:EmitSound( "weapons/bulkcannon/shellout.mp3" )
                else
                    self.Weapon:SetNextPrimaryFire(CurTime() + 0.5)
                    self.Weapon:SetNextSecondaryFire(CurTime() + 0.5)
                    self.Weapon:EmitSound( "weapons/bulkcannon/shellout.mp3" )
                end
            end
        end
    end
end
SWEP.ActionDelay = 0
function SWEP:Reload()
    if self.Owner:KeyDown(IN_USE) then
       -- self.Owner:SetVelocity(self.Owner:GetForward() * 5000 + Vector(0, 0, 5000))
        return
    end

    if (self.ActionDelay > CurTime()) then return end

    if (self.Weapon:GetNWBool("Reloading") or self.ShotgunReloading) then return end

    if (self.Weapon:Clip1() < self.Primary.ClipSize and self.Owner:GetAmmoCount(self.Primary.Ammo) > 0) then
        self.ShotgunReloading = true
        self.Weapon:SetNextPrimaryFire(CurTime() + self.ShotgunBeginReload + 0.1)
        self.Weapon:SetNextSecondaryFire(CurTime() + self.ShotgunBeginReload + 0.1)
        self.Weapon:SendWeaponAnim(ACT_SHOTGUN_RELOAD_START)

        timer.Simple(self.ShotgunBeginReload, function()
            if not IsValid(self) then return end
            self.ShotgunReloading = false
            self.Weapon:SetNWBool("Reloading", true)
            self.Weapon:SetVar("ReloadTime", CurTime() + 1)
            self.Weapon:SetNextPrimaryFire(CurTime() + 0.5)
            self.Weapon:SetNextSecondaryFire(CurTime() + 0.5)
        end)

        if (SERVER) then
            self.Owner:SetFOV( 0, 0.15 )
            self:SetIronsights(false)
        end
    end
end

function SWEP:Initialize()
    self:SetHoldType( self.HoldType or "pistol" )
    -- other initialize code goes here

    self.WorldModel = "models/weapons/w_shot_gb1014.mdl"
    self:SetModel( self.WorldModel)

    if CLIENT then

        if !IsValid(self.Owner) then return end

        -- Create a new table for every weapon instance
        self.VElements = table.FullCopy( self.VElements )
        self.WElements = table.FullCopy( self.WElements )
        self.ViewModelBoneMods = table.FullCopy( self.ViewModelBoneMods )

        self:CreateModels(self.VElements) -- create viewmodels
        self:CreateModels(self.WElements) -- create worldmodels

        -- init view model bone build function
        local vm = self.Owner:GetViewModel()
        if IsValid(vm) then
            self:ResetBonePositions(vm)


            -- Init viewmodel visibility
            if (self.ShowViewModel == nil or self.ShowViewModel) then
                vm:SetColor(Color(255,255,255,255))
            else
                -- we set the alpha to 1 instead of 0 because else ViewModelDrawn stops being called
                vm:SetColor(Color(255,255,255,1))
                -- ^ stopped working in GMod 13 because you have to do Entity:SetRenderMode(1) for translucency to kick in
                -- however for some reason the view model resets to render mode 0 every frame so we just apply a debug material to prevent it from drawing
                vm:SetMaterial("Debug/hsv")
            end
        end

    end

end

function SWEP:Holster()


    if IsValid(self) then
        self.ShowWorldModel = true
        self.WorldModel = "models/weapons/w_shot_gb1014.mdl"
        self:SetModel( self.WorldModel)
    end

    if CLIENT then
        if !IsValid(self.Owner) then return end
        local vm = self.Owner:GetViewModel()
        if IsValid(vm) then
            self:ResetBonePositions(vm)
        end
    end
    return true
end

if CLIENT then

    SWEP.vRenderOrder = nil
    function SWEP:ViewModelDrawn()
        if !IsValid(self.Owner) then return end
        local vm = self.Owner:GetViewModel()
        if !IsValid(vm) then return end

        if (!self.VElements) then return end

        self:UpdateBonePositions(vm)

        if (!self.vRenderOrder) then

        -- we build a render order because sprites need to be drawn after models
        self.vRenderOrder = {}

        for k, v in pairs( self.VElements ) do
            if (v.type == "Model") then
                table.insert(self.vRenderOrder, 1, k)
            elseif (v.type == "Sprite" or v.type == "Quad") then
                table.insert(self.vRenderOrder, k)
            end
        end

        end

        for k, name in ipairs( self.vRenderOrder ) do

            local v = self.VElements[name]
            if (!v) then self.vRenderOrder = nil break end
            if (v.hide) then continue end

            local model = v.modelEnt
            local sprite = v.spriteMaterial

            if (!v.bone) then continue end

            local pos, ang = self:GetBoneOrientation( self.VElements, v, vm )

            if (!pos) then continue end

            if (v.type == "Model" and IsValid(model)) then

                model:SetPos(pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z )
                ang:RotateAroundAxis(ang:Up(), v.angle.y)
                ang:RotateAroundAxis(ang:Right(), v.angle.p)
                ang:RotateAroundAxis(ang:Forward(), v.angle.r)

                model:SetAngles(ang)
                --model:SetModelScale(v.size)
                local matrix = Matrix()
                matrix:Scale(v.size)
                model:EnableMatrix( "RenderMultiply", matrix )

                if (v.material == "") then
                    model:SetMaterial("")
                elseif (model:GetMaterial() != v.material) then
                model:SetMaterial( v.material )
                end

                if (v.skin and v.skin != model:GetSkin()) then
                model:SetSkin(v.skin)
                end

                if (v.bodygroup) then
                    for k, v in pairs( v.bodygroup ) do
                        if (model:GetBodygroup(k) != v) then
                        model:SetBodygroup(k, v)
                        end
                    end
                end

                if (v.surpresslightning) then
                    render.SuppressEngineLighting(true)
                end

                render.SetColorModulation(v.color.r/255, v.color.g/255, v.color.b/255)
                render.SetBlend(v.color.a/255)
                model:DrawModel()
                render.SetBlend(1)
                render.SetColorModulation(1, 1, 1)

                if (v.surpresslightning) then
                    render.SuppressEngineLighting(false)
                end

            elseif (v.type == "Sprite" and sprite) then

                local drawpos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
                render.SetMaterial(sprite)
                render.DrawSprite(drawpos, v.size.x, v.size.y, v.color)

            elseif (v.type == "Quad" and v.draw_func) then

                local drawpos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
                ang:RotateAroundAxis(ang:Up(), v.angle.y)
                ang:RotateAroundAxis(ang:Right(), v.angle.p)
                ang:RotateAroundAxis(ang:Forward(), v.angle.r)

                cam.Start3D2D(drawpos, ang, v.size)
                v.draw_func( self )
                cam.End3D2D()

            end

            end

            end

            SWEP.wRenderOrder = nil
            function SWEP:DrawWorldModel()

                if (self.ShowWorldModel == nil or self.ShowWorldModel) then
                    self:DrawModel()
                end

                if (!self.WElements) then return end

                if (!self.wRenderOrder) then

                self.wRenderOrder = {}

                for k, v in pairs( self.WElements ) do
                    if (v.type == "Model") then
                        table.insert(self.wRenderOrder, 1, k)
                    elseif (v.type == "Sprite" or v.type == "Quad") then
                        table.insert(self.wRenderOrder, k)
                    end
                end

                end

                if (IsValid(self.Owner)) then
                    bone_ent = self.Owner
                else
                    -- when the weapon is dropped
                    bone_ent = self
                end

                for k, name in pairs( self.wRenderOrder ) do

                    local v = self.WElements[name]
                    if (!v) then self.wRenderOrder = nil break end
                    if (v.hide) then continue end

                    local pos, ang

                    if (v.bone) then
                        pos, ang = self:GetBoneOrientation( self.WElements, v, bone_ent )
                    else
                        pos, ang = self:GetBoneOrientation( self.WElements, v, bone_ent, "ValveBiped.Bip01_R_Hand" )
                    end

                    if (!pos) then continue end

                    local model = v.modelEnt
                    local sprite = v.spriteMaterial

                    if (v.type == "Model" and IsValid(model)) then

                        model:SetPos(pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z )
                        ang:RotateAroundAxis(ang:Up(), v.angle.y)
                        ang:RotateAroundAxis(ang:Right(), v.angle.p)
                        ang:RotateAroundAxis(ang:Forward(), v.angle.r)

                        model:SetAngles(ang)
                        --model:SetModelScale(v.size)
                        local matrix = Matrix()
                        matrix:Scale(v.size)
                        model:EnableMatrix( "RenderMultiply", matrix )

                        if (v.material == "") then
                            model:SetMaterial("")
                        elseif (model:GetMaterial() != v.material) then
                        model:SetMaterial( v.material )
                        end

                        if (v.skin and v.skin != model:GetSkin()) then
                        model:SetSkin(v.skin)
                        end

                        if (v.bodygroup) then
                            for k, v in pairs( v.bodygroup ) do
                                if (model:GetBodygroup(k) != v) then
                                model:SetBodygroup(k, v)
                                end
                            end
                        end

                        if (v.surpresslightning) then
                            render.SuppressEngineLighting(true)
                        end

                        render.SetColorModulation(v.color.r/255, v.color.g/255, v.color.b/255)
                        render.SetBlend(v.color.a/255)
                        model:DrawModel()
                        render.SetBlend(1)
                        render.SetColorModulation(1, 1, 1)

                        if (v.surpresslightning) then
                            render.SuppressEngineLighting(false)
                        end

                    elseif (v.type == "Sprite" and sprite) then

                        local drawpos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
                        render.SetMaterial(sprite)
                        render.DrawSprite(drawpos, v.size.x, v.size.y, v.color)

                    elseif (v.type == "Quad" and v.draw_func) then

                        local drawpos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
                        ang:RotateAroundAxis(ang:Up(), v.angle.y)
                        ang:RotateAroundAxis(ang:Right(), v.angle.p)
                        ang:RotateAroundAxis(ang:Forward(), v.angle.r)

                        cam.Start3D2D(drawpos, ang, v.size)
                        v.draw_func( self )
                        cam.End3D2D()

                    end

                    end

                    end

                    function SWEP:GetBoneOrientation( basetab, tab, ent, bone_override )

                        local bone, pos, ang
                        if (tab.rel and tab.rel != "") then

                        local v = basetab[tab.rel]

                        if (!v) then return end

                        -- Technically, if there exists an element with the same name as a bone
                        -- you can get in an infinite loop. Let's just hope nobody's that stupid.
                        pos, ang = self:GetBoneOrientation( basetab, v, ent )

                        if (!pos) then return end

                        pos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
                        ang:RotateAroundAxis(ang:Up(), v.angle.y)
                        ang:RotateAroundAxis(ang:Right(), v.angle.p)
                        ang:RotateAroundAxis(ang:Forward(), v.angle.r)

                        else

                            bone = ent:LookupBone(bone_override or tab.bone)

                            if (!bone) then return end

                            pos, ang = Vector(0,0,0), Angle(0,0,0)
                            local m = ent:GetBoneMatrix(bone)
                            if (m) then
                                pos, ang = m:GetTranslation(), m:GetAngles()
                            end

                            if (IsValid(self.Owner) and self.Owner:IsPlayer() and
                                    ent == self.Owner:GetViewModel() and self.ViewModelFlip) then
                                ang.r = -ang.r -- Fixes mirrored models
                            end

                        end

                        return pos, ang
                    end

                    function SWEP:CreateModels( tab )

                        if (!tab) then return end

                        -- Create the clientside models here because Garry says we can't do it in the render hook
                        for k, v in pairs( tab ) do
                            if (v.type == "Model" and v.model and v.model != "" and (!IsValid(v.modelEnt) or v.createdModel != v.model) and
                            string.find(v.model, ".mdl") and file.Exists (v.model, "GAME") ) then

                            v.modelEnt = ClientsideModel(v.model, RENDER_GROUP_VIEW_MODEL_OPAQUE)
                            if (IsValid(v.modelEnt)) then
                                v.modelEnt:SetPos(self:GetPos())
                                v.modelEnt:SetAngles(self:GetAngles())
                                v.modelEnt:SetParent(self)
                                v.modelEnt:SetNoDraw(true)
                                v.createdModel = v.model
                            else
                                v.modelEnt = nil
                            end

                            elseif (v.type == "Sprite" and v.sprite and v.sprite != "" and (!v.spriteMaterial or v.createdSprite != v.sprite)
                            and file.Exists ("materials/"..v.sprite..".vmt", "GAME")) then

                            local name = v.sprite.."-"
                            local params = { ["$basetexture"] = v.sprite }
                            -- make sure we create a unique name based on the selected options
                            local tocheck = { "nocull", "additive", "vertexalpha", "vertexcolor", "ignorez" }
                            for i, j in pairs( tocheck ) do
                                if (v[j]) then
                                    params["$"..j] = 1
                                    name = name.."1"
                                else
                                    name = name.."0"
                                end
                            end

                            v.createdSprite = v.sprite
                            v.spriteMaterial = CreateMaterial(name,"UnlitGeneric",params)

                            end
                        end

                    end

                    local allbones
                    local hasGarryFixedBoneScalingYet = false

                    function SWEP:UpdateBonePositions(vm)

                        if self.ViewModelBoneMods then

                            if (!vm:GetBoneCount()) then return end

                            -- !! WORKAROUND !! --
                            -- We need to check all model names :/
                            local loopthrough = self.ViewModelBoneMods
                            if (!hasGarryFixedBoneScalingYet) then
                            allbones = {}
                            for i=0, vm:GetBoneCount() do
                                local bonename = vm:GetBoneName(i)
                                if (self.ViewModelBoneMods[bonename]) then
                                    allbones[bonename] = self.ViewModelBoneMods[bonename]
                                else
                                    allbones[bonename] = {
                                        scale = Vector(1,1,1),
                                        pos = Vector(0,0,0),
                                        angle = Angle(0,0,0)
                                    }
                                end
                            end

                            loopthrough = allbones
                            end
                            -- !! ----------- !! --

                            for k, v in pairs( loopthrough ) do
                                local bone = vm:LookupBone(k)
                                if (!bone) then continue end

                                -- !! WORKAROUND !! --
                                local s = Vector(v.scale.x,v.scale.y,v.scale.z)
                                local p = Vector(v.pos.x,v.pos.y,v.pos.z)
                                local ms = Vector(1,1,1)
                                if (!hasGarryFixedBoneScalingYet) then
                                local cur = vm:GetBoneParent(bone)
                                while(cur >= 0) do
                                    local pscale = loopthrough[vm:GetBoneName(cur)].scale
                                    ms = ms * pscale
                                    cur = vm:GetBoneParent(cur)
                                end
                                end

                                s = s * ms
                                -- !! ----------- !! --

                                if vm:GetManipulateBoneScale(bone) != s then
                                vm:ManipulateBoneScale( bone, s )
                                end
                                if vm:GetManipulateBoneAngles(bone) != v.angle then
                                vm:ManipulateBoneAngles( bone, v.angle )
                                end
                                if vm:GetManipulateBonePosition(bone) != p then
                                vm:ManipulateBonePosition( bone, p )
                                end
                                end
                        else
                            self:ResetBonePositions(vm)
                        end

                    end

                    function SWEP:ResetBonePositions(vm)

                        if (!vm:GetBoneCount()) then return end
                        for i=0, vm:GetBoneCount() do
                            vm:ManipulateBoneScale( i, Vector(1, 1, 1) )
                            vm:ManipulateBoneAngles( i, Angle(0, 0, 0) )
                            vm:ManipulateBonePosition( i, Vector(0, 0, 0) )
                        end

                    end

                    function table.FullCopy( tab )

                        if (!tab) then return nil end

                        local res = {}
                        for k, v in pairs( tab ) do
                            if (type(v) == "table") then
                                res[k] = table.FullCopy(v) -- recursion ho!
                            elseif (type(v) == "Vector") then
                                res[k] = Vector(v.x, v.y, v.z)
                            elseif (type(v) == "Angle") then
                                res[k] = Angle(v.p, v.y, v.r)
                            else
                                res[k] = v
                            end
                        end

                        return res

                    end

                end

