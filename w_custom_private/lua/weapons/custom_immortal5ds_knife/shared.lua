if SERVER then
   AddCSLuaFile(  )
   --resource.AddFile("materials/vgui/ttt_fun_killicons/knife.png")
end

SWEP.Kind = WEAPON_MELEE
SWEP.HoldType = "knife"
   SWEP.PrintName    = "Immortal5ds Knife"
   SWEP.Slot         = 0

if CLIENT then


  
   SWEP.ViewModelFlip = false

 

   SWEP.Icon = "vgui/ttt_fun_killicons/knife.png"
end

SWEP.Base               = "aa_base"

SWEP.ViewModel          = "models/weapons/v_knife_t.mdl"
SWEP.WorldModel         = ''
SWEP.ShowWorldModel     = false 

SWEP.DrawCrosshair      = false
SWEP.Primary.Damage         = 45
SWEP.Primary.ClipSize       = 1
SWEP.Primary.DefaultClip    = 1
SWEP.Primary.Automatic      = true
SWEP.Primary.Delay = 0.3
SWEP.Primary.Ammo       = "none"
SWEP.Secondary.ClipSize     = -1
SWEP.Secondary.DefaultClip  = -1
SWEP.Secondary.Automatic    = true
SWEP.Secondary.Ammo     = "none"
SWEP.Secondary.Delay = 1.4
SWEP.AllowDrop = false
SWEP.HeadshotMultiplier = 1



SWEP.IsSilent = true

-- Pull out faster than standard guns
SWEP.DeploySpeed = 2

function SWEP:PrimaryAttack()
   self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
   self.Weapon:SetNextSecondaryFire( CurTime() + self.Secondary.Delay )

   self.Owner:LagCompensation(true)

   local spos = self.Owner:GetShootPos()
   local sdest = spos + (self.Owner:GetAimVector() * 70)

   local kmins = Vector(1,1,1) * -10
   local kmaxs = Vector(1,1,1) * 10

   local tr = util.TraceHull({start=spos, endpos=sdest, filter=self.Owner, mask=MASK_SHOT_HULL, mins=kmins, maxs=kmaxs})

   -- Hull might hit environment stuff that line does not hit
   if not IsValid(tr.Entity) then
      tr = util.TraceLine({start=spos, endpos=sdest, filter=self.Owner, mask=MASK_SHOT_HULL})
   end

   local hitEnt = tr.Entity

   -- effects
   if IsValid(hitEnt) then
      self.Weapon:SendWeaponAnim( ACT_VM_HITCENTER )

      local edata = EffectData()
      edata:SetStart(spos)
      edata:SetOrigin(tr.HitPos)
      edata:SetNormal(tr.Normal)
      edata:SetEntity(hitEnt)

      if hitEnt:IsPlayer() or hitEnt:GetClass() == "prop_ragdoll" then
         util.Effect("BloodImpact", edata)
      end
   else
      self.Weapon:SendWeaponAnim( ACT_VM_MISSCENTER )
   end

   if SERVER then
      self.Owner:SetAnimation( PLAYER_ATTACK1 )
   end


   if SERVER and tr.Hit and tr.HitNonWorld and IsValid(hitEnt) then
      if hitEnt:IsPlayer() then

          if self.Owner:Health() < 150 then
            self.Owner:SetHealth(self.Owner:Health()+10)
        end
         -- knife damage is never karma'd, so don't need to take that into
         -- account we do want to avoid rounding error strangeness caused by
         -- other damage scaling, causing a death when we don't expect one, so
         -- when the target's health is close to kill-point we just kill
         if hitEnt:Health() < (self.Primary.Damage + 10) then
            self:StabKill(tr, spos, sdest)
         else
            local dmg = DamageInfo()
            dmg:SetDamage(self.Primary.Damage)
            dmg:SetAttacker(self.Owner)
            dmg:SetInflictor(self.Weapon or self)
            dmg:SetDamageForce(self.Owner:GetAimVector() * 5)
            dmg:SetDamagePosition(self.Owner:GetPos())
            dmg:SetDamageType(DMG_SLASH)

            hitEnt:DispatchTraceAttack(dmg, spos + (self.Owner:GetAimVector() * 3), sdest)
         end
      end
   end

   self.Owner:LagCompensation(false)
end

function SWEP:StabKill(tr, spos, sdest)
   local target = tr.Entity

   local dmg = DamageInfo()
   dmg:SetDamage(49)
   dmg:SetAttacker(self.Owner)
   dmg:SetInflictor(self.Weapon or self)
   dmg:SetDamageForce(self.Owner:GetAimVector())
   dmg:SetDamagePosition(self.Owner:GetPos())
   dmg:SetDamageType(DMG_SLASH)

   -- now that we use a hull trace, our hitpos is guaranteed to be
   -- terrible, so try to make something of it with a separate trace and
   -- hope our effect_fn trace has more luck

   -- first a straight up line trace to see if we aimed nicely
   local retr = util.TraceLine({start=spos, endpos=sdest, filter=self.Owner, mask=MASK_SHOT_HULL})

   -- if that fails, just trace to worldcenter so we have SOMETHING
   if retr.Entity != target then
      local center = target:LocalToWorld(target:OBBCenter())
      retr = util.TraceLine({start=spos, endpos=center, filter=self.Owner, mask=MASK_SHOT_HULL})
   end


   -- create knife effect creation fn
   local bone = retr.PhysicsBone
   local pos = retr.HitPos
   local norm = tr.Normal
   local ang = Angle(-28,0,0) + norm:Angle()
   ang:RotateAroundAxis(ang:Right(), -90)
   pos = pos - (ang:Forward() * 7)

   local prints = self.fingerprints
   local ignore = self.Owner

   target.effect_fn = function(rag)
                         -- we might find a better location
                         local rtr = util.TraceLine({start=pos, endpos=pos + norm * 40, filter=ignore, mask=MASK_SHOT_HULL})

                         if IsValid(rtr.Entity) and rtr.Entity == rag then
                            bone = rtr.PhysicsBone
                            pos = rtr.HitPos
                            ang = Angle(-28,0,0) + rtr.Normal:Angle()
                            ang:RotateAroundAxis(ang:Right(), -90)
                            pos = pos - (ang:Forward() * 10)

                         end

                         local knife = ents.Create("prop_physics")
                         knife:SetModel("models/weapons/w_knife_t.mdl")
                         knife:SetPos(pos)
                         knife:SetCollisionGroup(COLLISION_GROUP_DEBRIS)
                         knife:SetAngles(ang)
                         knife.CanPickup = false

                         knife:Spawn()

                         local phys = knife:GetPhysicsObject()
                         if IsValid(phys) then
                            phys:EnableCollisions(false)
                         end

                         constraint.Weld(rag, knife, bone, 0, 0, true)

                         -- need to close over knife in order to keep a valid ref to it
                         rag:CallOnRemove("ttt_knife_cleanup", function() SafeRemoveEntity(knife) end)
                      end


   -- seems the spos and sdest are purely for effects/forces?
   target:DispatchTraceAttack(dmg, spos + (self.Owner:GetAimVector() * 3), sdest)

   -- target appears to die right there, so we could theoretically get to
   -- the ragdoll in here...

   --self:Remove()      
end

function SWEP:Equip()
   self.Weapon:SetNextPrimaryFire( CurTime() + (self.Primary.Delay * 1.5) )
   self.Weapon:SetNextSecondaryFire( CurTime() + (self.Secondary.Delay * 1.5) )
end

function SWEP:PreDrop()
   -- for consistency, dropped knife should not have DNA/prints
   self.fingerprints = {}
end

function SWEP:OnRemove()
   if CLIENT and IsValid(self.Owner) and self.Owner == LocalPlayer() and self.Owner:Alive() then
      RunConsoleCommand("lastinv")
   end
end

if CLIENT then
   function SWEP:DrawHUD()
      local tr = self.Owner:GetEyeTrace(MASK_SHOT)

      if tr.HitNonWorld and IsValid(tr.Entity) and tr.Entity:IsPlayer()
         and tr.Entity:Health() < (self.Primary.Damage + 10) then

         local x = ScrW() / 2.0
         local y = ScrH() / 2.0

         surface.SetDrawColor(255, 0, 0, 255)

         local outer = 20
         local inner = 10
         surface.DrawLine(x - outer, y - outer, x - inner, y - inner)
         surface.DrawLine(x + outer, y + outer, x + inner, y + inner)

         surface.DrawLine(x - outer, y + outer, x - inner, y + inner)
         surface.DrawLine(x + outer, y - outer, x + inner, y - inner)

         draw.SimpleText("INSTANT KILL", "TabLarge", x, y - 30, COLOR_RED, TEXT_ALIGN_CENTER, TEXT_ALIGN_BOTTOM)
      end         

      return self.BaseClass.DrawHUD(self)
   end
end

SWEP.flyammo = 5
SWEP.nextFly = 0
function SWEP:SecondaryAttack()
    self:Fly()
end


SWEP.NextReload = 0
SWEP.numNomNom = 3

function SWEP:Reload()

    if self.Owner:KeyDown(IN_USE) then
        self:Defib();
        return
    end

    if  self.numNomNom  > 0 and self.NextReload < CurTime() then
        self.numNomNom = self.numNomNom - 1

        local tracedata = {}
        tracedata.start = self.Owner:GetShootPos()
        tracedata.endpos = self.Owner:GetShootPos() + (self.Owner:GetAimVector() * 100)
        tracedata.filter = self.Owner
        tracedata.mins = Vector(1,1,1) * -10
        tracedata.maxs = Vector(1,1,1) * 10
        tracedata.mask = MASK_SHOT_HULL
        local tr = util.TraceLine({start = self.Owner:EyePos(), endpos = self.Owner:EyePos() + self.Owner:EyeAngles():Forward() * 80, filter = self.Owner})

        local ply = self.Owner
        self.NextReload = CurTime() + 0.8
        if IsValid(tr.Entity) then
            if tr.Entity.player_ragdoll then

                self:SetClip1( self:Clip1() - 1)

                self.NextReload = CurTime() + 5
                timer.Simple(0.1, function()
                    ply:Freeze(true)
                    ply:SetColor(Color(255,0,0,255))
                end)
                self.Weapon:SendWeaponAnim(ACT_VM_PRIMARYATTACK)

                timer.Create("GivePlyHealth_"..self.Owner:UniqueID(),0.5,6,function() if(IsValid(self)) and IsValid(self.Owner) then
                    if self.Owner:Health() < 175 then
                        self.Owner:SetHealth(self.Owner:Health()+5)
                    end
                end end)
                self.Owner:EmitSound("ttt/nomnomnom.mp3")
                timer.Simple(3,function()
                    if not IsValid(ply) then return end
                    self.Owner:EmitSound("ttt/nomnomnom.mp3")
                end)
                timer.Simple(6.1, function()
                    if not IsValid(ply) then return end
                    ply:Freeze(false)
                    ply:SetColor(Color(255,255,255,255))
                    tr.Entity:Remove()
                --self:Remove()
                end )


            end
        end

    end

end

SWEP.defibs = 0
SWEP.nextDefib = 0
function SWEP:Defib()

    if ( self.defibs > 0) then return end


    self.NextAttacky = CurTime()+ 0.5


    local tr = util.TraceLine({ start = self.Owner:EyePos(), endpos = self.Owner:EyePos() + self.Owner:EyeAngles():Forward() * 80, filter = self.Owner })
    if tr.HitNonWorld and IsValid(tr.Entity) and tr.Entity:GetClass() == "prop_ragdoll" and tr.Entity.player_ragdoll then
        self.Weapon:EmitSound( "weapons/c4/c4_beep1.wav"	)
        local mins, maxs = self.Owner:OBBMins(), self.Owner:OBBMaxs()
        local tr2 = util.TraceHull({start = tr.Entity:LocalToWorld(tr.Entity:OBBCenter()) + vector_up, endpos = tr.Entity:LocalToWorld(tr.Entity:OBBCenter()) + Vector(0, 0, 80), filter = {tr.Entity, self.Owner}, mins = Vector(mins.x * 1.6, mins.y * 1.6, 0), maxs = Vector(maxs.x * 1.6, maxs.y * 1.6, 0), mask = MASK_PLAYERSOLID})
        if tr2.Hit then
            self.Owner:PrintMessage( HUD_PRINTCENTER, "There is no room to bring him back here" )
            return
        end


        self.nextDefib = CurTime() + 10
        self.defibs =  self.defibs + 1
        if SERVER then
            rag = tr.Entity
            local ply = player.GetByUniqueID(rag.uqid)
            local credits = CORPSE.GetCredits(rag, 0)
            if not IsValid(ply) then return end
            if SERVER and _globals['slayed'][ply:SteamID()] then self.Owner:PrintMessage( HUD_PRINTCENTER, "Can't revive, person was slayed" )
                ply:Kill();
                return
            end
            if SERVER and ((rag.was_role == ROLE_TRAITOR and self.Owner:GetRole() ~= ROLE_TRAITOR) or (rag.was_role ~= ROLE_TRAITOR and self.Owner:GetRole() == ROLE_TRAITOR))  then
                DamageLog("Defib: " .. self.Owner:Nick() .. " [" .. self.Owner:GetRoleString() .. "] Died defibbing "..ply:Nick().." [" .. ply:GetRoleString() .. "] ")
                self.Owner:PrintMessage( HUD_PRINTCENTER, ply:Nick().." was of the other team,  sucked up your soul and killed you" )
                self.Owner:Kill();

                return
            end
            DamageLog("Defib: " .. self.Owner:Nick() .. " [" .. self.Owner:GetRoleString() .. "] defibbed "..ply:Nick().." [" .. ply:GetRoleString() .. "] ")

            ply:SpawnForRound(true)
            ply:SetCredits(credits)
            ply:SetPos(rag:LocalToWorld(rag:OBBCenter()) + vector_up / 2) --Just in case he gets stuck somewhere
            ply:SetEyeAngles(Angle(0, rag:GetAngles().y, 0))
            ply:SetCollisionGroup(COLLISION_GROUP_WEAPON)
            timer.Simple(2, function() ply:SetCollisionGroup(COLLISION_GROUP_PLAYER) end)

            umsg.Start("_resurrectbody")
            umsg.Entity(ply)
            umsg.Bool(CORPSE.GetFound(rag, false))
            umsg.End()
            rag:Remove()
        end
    else
        self.Weapon:EmitSound("npc/scanner/scanner_nearmiss1.wav")
    end
end