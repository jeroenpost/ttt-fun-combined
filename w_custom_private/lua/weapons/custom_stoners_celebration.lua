include "weapons/weapon_zm_shotgun.lua"
SWEP.Base = "gb_camo_base"
SWEP.ViewModelFOV	= 54
SWEP.HoldType = "smg"
SWEP.PrintName = "12th perigee's eve"
SWEP.ViewModel		= "models/weapons/cstrike/c_shot_m3super90.mdl"
SWEP.WorldModel		= "models/weapons/w_shot_m3super90.mdl"
SWEP.AutoSpawnable = false

SWEP.HoldType = "shotgun"
SWEP.Slot = 2
SWEP.Kind = WEAPON_HEAVY
SWEP.CustomCamo = true
SWEP.Camo = 23

if CLIENT then

    SWEP.Author = "GreenBlack"

    SWEP.Icon = "vgui/ttt_fun_killicons/xm1014.png"
end

SWEP.Primary.Sound			= Sound( "Weapon_M3.Single" )
SWEP.Primary.Recoil			= 7

SWEP.Primary.Ammo = "Buckshot"
SWEP.Primary.Damage = 11
SWEP.Primary.Cone = 0.085
SWEP.Primary.Delay = 0.6
SWEP.Primary.ClipSize = 60
SWEP.Primary.ClipMax = 60
SWEP.Primary.DefaultClip = 60
SWEP.Primary.Automatic = true
SWEP.Primary.NumShots = 7
SWEP.AutoSpawnable      = false
SWEP.AmmoEnt = "item_box_buckshot_ttt"

function SWEP:Deploy()
    self.dt.reloading = false
    self.reloadtimer = 0
    return self:DeployFunction()
end

SWEP.IronSightsPos = Vector(-7.65, -12.214, 3.2)
SWEP.IronSightsAng = Vector(0, 0, 0)

SWEP.NextSuperman = 0

function SWEP:SecondaryAttack()
    if self.NextSuperman < CurTime() then
        self.NextSuperman = CurTime() + 5

        self.Owner:SetVelocity(self.Owner:GetForward() * 1000 + Vector(0,0,1000))
        self.hasSuperman = true
        gb_PlaySoundFromServer(gb_config.websounds.."santa3.mp3", self.Owner)
    end
end

SWEP.NextPlaysound2 = 0
function SWEP:PrimaryAttack()
    self.BaseClass.PrimaryAttack(self)
    if self.NextPlaysound2 > CurTime() then return end
    self.NextPlaysound2 = CurTime() + 15
    gb_PlaySoundFromServer(gb_config.websounds.."santa2.mp3", self.Owner, nil, self.Owner:SteamID())
end

SWEP.NextPlaysound = 0
SWEP.NextPlaysound4 = 0
function SWEP:Reload()
    if self.Owner:KeyDown(IN_USE) then

        self:ShootFlare()
        if self.NextPlaysound4 > CurTime() then return end
        self.NextPlaysound4 = CurTime() + 15
        gb_PlaySoundFromServer(gb_config.websounds.."santa4.mp3", self.Owner, nil, self.Owner:SteamID())
        return
    end
    if self.NextPlaysound > CurTime() then return end
    self.NextPlaysound = CurTime() + 30
    gb_PlaySoundFromServer(gb_config.websounds.."santa1.mp3", self.Owner, nil, self.Owner:SteamID())
end

