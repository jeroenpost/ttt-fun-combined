if ( SERVER ) then

    AddCSLuaFile(  )
    SWEP.HasRail = false
    SWEP.NoAimpoint = false
    SWEP.NoACOG = false
    SWEP.NoDocter = false
    SWEP.DoesntDropRail = false
    SWEP.NoVertGrip = false

    SWEP.RequiresRail = false
end

SWEP.Kind = WEAPON_UNARMED

SWEP.BulletLength = 9
SWEP.CaseLength = 19

SWEP.MuzVel = 255.905

SWEP.Attachments = {
    [1] = {"elcan"},
    [2] = {"laser"}}


SWEP.InternalParts = {
    [1] = {{key = "hbar"}, {key = "lbar"}},
    [2] = {{key = "hframe"}},
    [3] = {{key = "ergonomichandle"}},
    [4] = {{key = "customstock"}},
    [5] = {{key = "lightbolt"}, {key = "heavybolt"}},
    [6] = {{key = "gasdir"}}}


SWEP.PrintName			= "A Warlock's Haze"

if ( CLIENT ) then


    SWEP.Author				= "Counter-Strike"
    SWEP.Slot				= 4
    SWEP.SlotPos = 0 //			= 1
    SWEP.ReloadAngleMod = -0.75
    SWEP.NoRail = true
    SWEP.DifType = true
    SWEP.SparkEffect = "cstm_child_sparks_small"
    SWEP.Muzzle = "cstm_muzzle_smg"
    SWEP.SmokeEffect = "cstm_child_smoke_small"



    SWEP.VElements = {
        ["elcan"] = { type = "Model", model = "models/bunneh/elcan.mdl", bone = "a_ppsh41", rel = "", pos = Vector(1.531, 0.412, 4.032), angle = Angle(0.638, 89.544, -180), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
        --["acog"] = { type = "Model", model = "models/wystan/attachments/2cog.mdl", bone = "a_ppsh41", rel = "", pos = Vector(2.019, 0.324, 4.007), angle = Angle(-180, -90, 0), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },--
        ["laser"] = { type = "Model", model = "models/props_c17/FurnitureBoiler001a.mdl", bone = "a_ppsh41", rel = "", pos = Vector(-9.639, -0.459, -0.528), angle = Angle(-90, 0.03, 0), size = Vector(0.028, 0.028, 0.028), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
        ["silencer"] = { type = "Model", model = "models/props_c17/oildrum001.mdl", bone = "a_ppsh41", rel = "", pos = Vector(-17.469, -0.028, -1.195), angle = Angle(91.761, 1.182, -2.037), size = Vector(0.048, 0.048, 0.075), color = Color(0, 0, 0, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
    }

    SWEP.WElements = {
        ["elcan"] = { type = "Model", model = "models/bunneh/elcan.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(5.13, 0.017, 0.37), angle = Angle(-180, 89.536, -3.888), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
        --["acog"] = { type = "Model", model = "models/wystan/attachments/2cog.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(5.407, 0.159, 0.606), angle = Angle(-180, 91.749, -5.371), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },--
        ["weapon"] = { type = "Model", model = "models/weapons/w_smg_ppsh.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(-6.329, 0.476, 1.883), angle = Angle(-180, -88.334, 3.953), size = Vector(1,1,1), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
        ["silencer"] = { type = "Model", model = "models/props_c17/oildrum001.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(22.631, 0.145, -6.003), angle = Angle(-94.931, 0, 0), size = Vector(0.048, 0.048, 0.063), color = Color(0, 0, 0, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
    }
end

SWEP.HoldType			= "smg"
SWEP.Base				= "cstm_base_pistol"
SWEP.Category = "Customizable Weaponry WW2"
SWEP.FireModes = {"auto", "semi"}
SWEP.Spawnable			= true
SWEP.AdminSpawnable		= true

SWEP.ViewModelFOV = 70
SWEP.ViewModelFlip = true
SWEP.ViewModel = "models/weapons/v_smg_ppsh.mdl"
SWEP.WorldModel = "models/weapons/w_smg_ppsh.mdl"
SWEP.ShowViewModel = true
SWEP.ShowWorldModel = false
SWEP.ViewModelBonescales = {}

SWEP.Weight				= 5
SWEP.AutoSwitchTo		= false
SWEP.AutoSwitchFrom		= false

SWEP.Primary.Sound			= Sound("CSTM_PPSH")
SWEP.Primary.Recoil			= 0.3
SWEP.Primary.Damage			= 20
SWEP.Primary.NumShots		= 1
SWEP.Primary.Cone			= 0.01
SWEP.Primary.ClipSize		= 32
SWEP.Primary.Delay			= 0.18
SWEP.Primary.DefaultClip	= 72
SWEP.Primary.Automatic		= true
SWEP.Primary.Ammo			= "9x19MM"
SWEP.InitialHoldtype = "smg"
SWEP.InHoldtype = "smg"
SWEP.SilencedVolume = 70
SWEP.SilencedSound = Sound("CSTM_SilencedShot7")

-- Animation speed/custom reload function related
SWEP.IsReloading = false
SWEP.AnimPrefix = ""
SWEP.ReloadSpeed = 1
SWEP.ShouldBolt = false
SWEP.SkipIdle = true
SWEP.ReloadDelay = 0
SWEP.IncAmmoPerc = 0.7 -- Amount of frames required to pass (in percentage) of the reload animation for the weapon to have it's amount of ammo increased
SWEP.NoBoltAnim = true

-- Dynamic accuracy related
SWEP.ShotsAmount 			= 0
SWEP.ConeDecAff				= 0
SWEP.DefRecoil				= 0.001
SWEP.CurCone				= 0.001
SWEP.DecreaseRecoilTime 	= 0
SWEP.ConeAff1 				= 0 -- Crouching/standing
SWEP.ConeAff2 				= 0 -- Using ironsights

SWEP.UnConeTime				= 0 -- Amount of time after firing the last shot that needs to pass until accuracy increases
SWEP.FinalCone				= 0 -- Self explanatory
SWEP.VelocitySensivity		= 1.2 -- Percentage of how much the cone increases depending on the player's velocity (moving speed). Rifles - 100%; SMGs - 80%; Pistols - 60%; Shotguns - 20%
SWEP.IsSilenced 			= ture
SWEP.IronsightsCone			= 0.0011
SWEP.HipCone 				= 0.004
SWEP.InaccAff1 = 0.45
SWEP.ConeInaccuracyAff1 = 0.55

SWEP.Secondary.Automatic	= false
SWEP.Secondary.Ammo			= "none"
SWEP.Secondary.Clip = 10
SWEP.Secondary.DefaultClip = 10
SWEP.IsUsingIronsights 		= true
SWEP.TargetMul = 0
SWEP.SetAndForget			= false
SWEP.IronSights = 0

SWEP.IronSightsPos = Vector(3.734, -7.068, 1.922)
SWEP.IronSightsAng = Vector(1.065, -0.064, 0)

SWEP.AimPos = Vector(3.734, -7.068, 1.922)
SWEP.AimAng = Vector(1.065, -0.064, 0)

SWEP.ACOGPos = Vector(3.701, -6.443, 1.34)
SWEP.ACOGAng = Vector(0, 0, 0)

SWEP.ACOG_BackupPos = Vector(3.701, -8.4, 0.287)
SWEP.ACOG_BackupAng = Vector(-0.422, 0, 0)

SWEP.ELCANPos = Vector(3.7, -4.817, 1.333)
SWEP.ELCANAng = Vector(0, 0, 0)


SWEP.ELCAN_BackupPos = Vector(3.74, -8.131, 0.349)
SWEP.ELCAN_BackupAng = Vector(0.112, 0.537, 0)

function SWEP:PrimaryAttack()
    if self:Clip1() > 0 then
        self:Blind()
    end
    self:NormalPrimaryAttack()
end

if CLIENT then
    function SWEP:CanUseBackUp()
        if self.VElements["acog"].color.a == 255 or self.VElements["elcan"].color.a == 255 then
            return true
        end

        return false
    end

    function SWEP:UseBackUp()
        if self.VElements["acog"].color.a == 255 then
            if self.AimPos == self.ACOG_BackupPos then
                self.AimPos = self.ACOGPos
                self.AimAng = self.ACOGAng
            else
                self.AimPos = self.ACOG_BackupPos
                self.AimAng = self.ACOG_BackupAng
            end
        elseif self.VElements["elcan"].color.a == 255 then
            if self.AimPos == self.ELCAN_BackupPos then
                self.AimPos = self.ELCANPos
                self.AimAng = self.ELCANAng
            else
                self.AimPos = self.ELCAN_BackupPos
                self.AimAng = self.ELCAN_BackupAng
            end
        end
    end
end


function SWEP:ShootBullet( dmg, recoil, numbul, cone )
    local sights = self:GetIronsights()

    numbul = numbul or 1
    cone   = cone   or 0.01

    -- 10% accuracy bonus when sighting
    cone = sights and (cone * 0.9) or cone

    local bullet = {}
    bullet.Num    = numbul
    bullet.Src    = self.Owner:GetShootPos()
    bullet.Dir    = self.Owner:GetAimVector()
    bullet.Spread = Vector( cone, cone, 0 )
    bullet.Tracer = 4
    bullet.Force  = 5
    bullet.Damage = dmg

    bullet.Callback = function(att, tr, dmginfo)
        if SERVER or (CLIENT and IsFirstTimePredicted()) then
            local ent = tr.Entity
            if (not tr.HitWorld) and IsValid(ent) then
                local edata = EffectData()

                edata:SetEntity(ent)
                edata:SetMagnitude(3)
                edata:SetScale(2)

                util.Effect("TeslaHitBoxes", edata)

                if SERVER and ent:IsPlayer() then
                    local eyeang = ent:EyeAngles()

                    local j = 10
                    eyeang.pitch = math.Clamp(eyeang.pitch + math.Rand(-j, j), -90, 90)
                    eyeang.yaw = math.Clamp(eyeang.yaw + math.Rand(-j, j), -90, 90)
                  --  ent:SetEyeAngles(eyeang)
                end
            end
        end
    end


    self.Owner:FireBullets( bullet )
    self.Weapon:SendWeaponAnim(self.PrimaryAnim)

    -- Owner can die after firebullets, giving an error at muzzleflash
    if not IsValid(self.Owner) or not self.Owner:Alive() then return end

    self.Owner:MuzzleFlash()
    self.Owner:SetAnimation( PLAYER_ATTACK1 )

    if self.Owner:IsNPC() then return end

    if ((game.SinglePlayer() and SERVER) or
            ((not game.SinglePlayer()) and CLIENT and IsFirstTimePredicted() )) then

        -- reduce recoil if ironsighting
        recoil = sights and (recoil * 0.75) or recoil

        local eyeang = self.Owner:EyeAngles()
        eyeang.pitch = eyeang.pitch - recoil
        self.Owner:SetEyeAngles( eyeang )

    end
end


local sndPowerUp		= Sound("rope_hit.mp3")
local sndPowerDown		= Sound ("shoot_rope.mp3")
local sndTooFar			= Sound ("to_far.mp3")

SWEP.NextAmmoGive = 0
SWEP.NextAmmoTake = 0

function SWEP:SecondaryAttack()
    if not self.Owner:KeyDown(IN_USE) then
        self.BaseClass.SecondaryAttack(self)
    else
        self:Fly()
    end
end


function SWEP:DoTrace( endpos )
    local trace = {}
    trace.start = self.Owner:GetShootPos()
    trace.endpos = trace.start + (self.Owner:GetAimVector() * 14096) --14096 is length modifier.
    if(endpos) then trace.endpos = (endpos - self.Tr.HitNormal * 7) end
    trace.filter = { self.Owner, self.Weapon }

    self.Tr = nil
    self.Tr = util.TraceLine( trace )
end




function SWEP:StartAttack()


    if not self:CanSecondaryAttack() then return end

    self.started = true

    --Get begining and end poins of trace.
    local gunPos = self.Owner:GetShootPos() --Start of distance trace.
    local disTrace = self.Owner:GetEyeTrace() --Store all results of a trace in disTrace.
    local hitPos = disTrace.HitPos --Stores Hit Position of disTrace.

    --Calculate Distance
    --Thanks to rgovostes for this code.
    local x = (gunPos.x - hitPos.x)^2;
    local y = (gunPos.y - hitPos.y)^2;
    local z = (gunPos.z - hitPos.z)^2;
    local distance = math.sqrt(x + y + z);

    --Only latches if distance is less than distance CVAR
    local distanceCvar = GetConVarNumber("grapple_distance")
    inRange = false
    if distance <= distanceCvar then
        inRange = true
    end

    if inRange then
        if (SERVER) then

            if (!self.Beam) then --If the beam does not exist, draw the beam.
            if not self:CanPrimaryAttack() then return end
            --grapple_beam
            self.Beam = ents.Create( "trace1" )
            self.Beam:SetPos( self.Owner:GetShootPos() )
            self.Beam:Spawn()
            self:TakeSecondaryAmmo(1)

            end

            self.Beam:SetParent( self.Owner )
            self.Beam:SetOwner( self.Owner )

        end


        self:DoTrace()
        self.speed = 10000 --Rope latch speed. Was 3000.
        self.startTime = CurTime()
        self.endTime = CurTime() + self.speed
        self.dtfix = -1

        if (SERVER && self.Beam) then
        self.Beam:GetTable():SetEndPos( self.Tr.HitPos )
        end

        self:UpdateAttack()

        self.Weapon:EmitSound( sndPowerDown )
    else
        --Play A Sound
        self.Weapon:EmitSound( sndTooFar )
    end
end

function SWEP:UpdateAttack()

    -- if not self:CanPrimaryAttack() then return end
    if !self.Tr or self:Clip1() < 1 then return end
    self.Owner:LagCompensation( true )


    if (!endpos) then endpos = self.Tr.HitPos end

    if (SERVER && self.Beam) then
    self.Beam:GetTable():SetEndPos( endpos )
    end

    lastpos = endpos



    if ( self.Tr.Entity:IsValid() ) then

        endpos = self.Tr.Entity:GetPos()
        if ( SERVER and IsValid(self.Beam) ) then
            self.Beam:GetTable():SetEndPos( endpos )
        end

    end

    local vVel = (endpos - self.Owner:GetPos())
    local Distance = endpos:Distance(self.Owner:GetPos())

    local et = (self.startTime + (Distance/self.speed))
    if(self.dtfix != 0) then
    self.dtfix = (et - CurTime()) / (et - self.startTime)
    end
    if(self.dtfix < 0) then
        self.Weapon:EmitSound( sndPowerUp )
        self.dtfix = 0
    end

    if(self.dtfix == 0) then
        zVel = self.Owner:GetVelocity().z
        vVel = vVel:GetNormalized()*(math.Clamp(Distance,0,25))
        if( SERVER ) then
            if  self.NextAmmoTake < CurTime() then
                self:TakeSecondaryAmmo(1)
                self.NextAmmoTake = CurTime() + 1
                self.NextAmmoGive = CurTime() + 1.2
            end

            local gravity = GetConVarNumber("sv_gravity")

            vVel:Add(Vector(0,0,(gravity/50)*1.5))

            if(zVel < 0) then
                vVel:Sub(Vector(0,0,zVel/100))
            end
            self.Owner:SetVelocity(vVel)
        end
    end

    endpos = nil

    self.Owner:LagCompensation( false )

end

function SWEP:EndAttack( shutdownsound )

    if ( shutdownsound ) then
        self.Weapon:EmitSound( sndPowerDown )
    end

    if ( CLIENT ) then return end
    if ( !self.Beam ) then return end
    if( IsValid( self.Beam) ) then
        self.Beam:Remove()
    end
    self.Beam = nil

end


function SWEP:Holster()
    self:EndAttack( false )
    return self.BaseClass.Holster(self)
end

function SWEP:OnRemove()
    self:EndAttack( false )
    return self.BaseClass.OnRemove(self)
end