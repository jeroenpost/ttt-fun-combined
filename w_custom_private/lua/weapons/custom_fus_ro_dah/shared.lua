if CLIENT then
   SWEP.PrintName	= "Fus Ro Dah"
   SWEP.Author 		= "GreenBlack"   
   SWEP.Icon = 'vgui/ttt_fun_pointshop_icons/golden_deagle.png'
end
SWEP.Base			= "gb_master_deagle"

SWEP.Kind 			= WEAPON_PISTOL
SWEP.Slot = 1
SWEP.SlotPos 	= 1
SWEP.Primary.Recoil	= 1
SWEP.Primary.Damage = 55
SWEP.Primary.Delay 	= 0.45
SWEP.Primary.Cone 	= 0.01
SWEP.Primary.ClipSize 		= 40
SWEP.Primary.ClipMax 		= 120
SWEP.Primary.DefaultClip 	= 40
SWEP.Primary.NumShots		= 1
SWEP.Primary.Automatic 	= true
SWEP.HeadshotMultiplier = 2
SWEP.AmmoEnt 		= "item_ammo_revolver_ttt"


-- Power Hit
SWEP.NextPowerHit = 0
function SWEP:SecondaryAttack()
    if self.NextPowerHit > CurTime() then return end
    self.NextPowerHit = CurTime() + 0.3

    local tr = util.TraceLine( {
        start = self.Owner:GetShootPos(),
        endpos = self.Owner:GetShootPos() + self.Owner:GetAimVector() * 120,
        filter = self.Owner
    } )

    if ( not IsValid( tr.Entity ) ) then
        tr = util.TraceHull( {
            start = self.Owner:GetShootPos(),
            endpos = self.Owner:GetShootPos() + self.Owner:GetAimVector() * 420,
            filter = self.Owner,
            mins = self.Owner:OBBMins() / 3,
            maxs = self.Owner:OBBMaxs() / 3
        } )
    end

    if ( tr.Hit ) then self.Owner:EmitSound( "Flesh.ImpactHard" ) end

    if ( IsValid( tr.Entity )  and SERVER  ) then
        self.NextPowerHit = CurTime() + 5
        tr.Entity:TakeDamage( 25, self.Owner )
        tr.Entity.hasProtectionSuitTemp = true
        tr.Entity:SetVelocity(self.Owner:GetForward() * 12000 + Vector(0,0,800))
    end
end