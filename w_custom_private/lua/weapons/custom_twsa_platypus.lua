
AddCSLuaFile( )

SWEP.ViewModelFOV	= 62
SWEP.ViewModelFlip	= false
SWEP.ViewModel		= "models/weapons/cstrike/c_rif_famas.mdl"
SWEP.WorldModel		= "models/weapons/w_rif_famas.mdl"
SWEP.UseHands = true
SWEP.Spawnable			= true
SWEP.AdminSpawnable		= true
SWEP.AutoSpawnable		= false
SWEP.Slot = 0
SWEP.Primary.ClipSize		= 300000
SWEP.Primary.DefaultClip	= 500000
SWEP.Primary.Cone	= 0.001
SWEP.Primary.Automatic		= true
SWEP.Primary.Ammo			= "none"
SWEP.Primary.Sound          = Sound("Weapon_AR2.Single")

SWEP.Secondary.ClipSize		= 3
SWEP.Secondary.DefaultClip	= 32
SWEP.Secondary.Automatic	= false
SWEP.Secondary.Ammo			= "none"

SWEP.IronSightsPos = Vector(5.74, -2.19, 3.28)
SWEP.IronSightsAng = Vector(0.547, 0, 0)


SWEP.HoldType = "shotgun"
SWEP.PrintName = "Faded Platypus"

SWEP.Base = "aa_base"
SWEP.Kind = WEAPON_MELEE
SWEP.AmmoEnt = "item_ammo_pistol_ttt"
SWEP.InLoadoutFor = nil
SWEP.AllowDrop = true
SWEP.IsSilent = true
SWEP.NoSights = false
SWEP.DrawCrosshair   = true
function SWEP:ShootBullet( damage, num_bullets, aimcone )

    local bullet = {}
    bullet.Num 		= num_bullets
    bullet.Src 		= self.Owner:GetShootPos()			
    bullet.Dir 		= self.Owner:GetAimVector()
    bullet.Spread 	= Vector( 0.001, 0.001, 0 )
    bullet.Tracer	= 1
    bullet.Force	= 10
    bullet.Damage	= damage
    bullet.AmmoType = "Pistol"
    bullet.HullSize = 2
    bullet.TracerName = "LaserTracer_thick"

    self.Owner:FireBullets( bullet )
    self:ShootEffects()

end

SWEP.LaserColor = Color(120,120,0,255)

-- COLOR
function SWEP:Deploy()
    self:SwitchViewModel()
    self:SetNWString("shootmode","Miley Madness")
    return true
end

SWEP.NextSecondary = 0
SWEP.NextReload = 0
function SWEP:Reload()
    if self.Owner:KeyDown(IN_ATTACK2) then return end

    if self.Owner:KeyDown(IN_USE) and self.NextReload < CurTime() then
        self.NextReload = CurTime() + 0.3
        local mode = self:GetNWString("shootmode")
        self:SetIronsights(false)
       -- self:SetZoom(false)
        if mode == "Miley Madness" then  self:SetNWString("shootmode","Powerful Patties") self.Primary.Sound = "weapons/shotgun/shotgun_dbl_fire.wav" end
        if mode == "Powerful Patties" then  self:SetNWString("shootmode","Tar Trap") self.Primary.Sound = "weapons/crossbow/fire1.wav" end
        if mode == "Tar Trap" then  self:SetNWString("shootmode","Miley Madness") self.Primary.Sound = "weapons/stunstick/spark1.wav" end
        self.Weapon:SetNextPrimaryFire( CurTime() + 0.8 )
        return
    end

end


function SWEP:PreDrop()
    self:ColorReset()
end


function SWEP:Holster()
    self:ColorReset()
    return true
end

function SWEP:OnRemove()
 --   self:ColorReset()
end
function SWEP:OnDrop()
    self:ColorReset()

end

function SWEP:throw_attack (entitys, power)
    local tr = self.Owner:GetEyeTrace();
    if not power then power = 100 end

    self:EmitSound(self.Primary.Sound)
    self.BaseClass.ShootEffects (self);
    if (!SERVER) then return end;

    local  ent = ents.Create (entitys);

    ent:SetPos (self.Owner:EyePos() + (self.Owner:GetAimVector() * 75));
    ent:SetAngles (self.Owner:EyeAngles());
    ent:SetPhysicsAttacker(self.Owner)
    ent.Owner = self.Owner

    ent:Spawn();
    ent.Mag = (self:Clip1() * 2 ) + 7

    local phys = ent:GetPhysicsObject();
    local shot_length = tr.HitPos:Length();
    phys:ApplyForceCenter (self.Owner:GetAimVector():GetNormalized() * power);

    timer.Simple(5,function()
        if IsValid(ent) then ent:Remove() end
    end)

    cleanup.Add (self.Owner, "props", ent);
    undo.Create ("Thrown model");
    undo.AddEntity (ent);
    undo.SetPlayer (self.Owner);
    undo.Finish();
end


SWEP.NextBomb = 0
SWEP.NextBombhealth = 0
SWEP.NextBombhealth2 = 0
function SWEP:SecondaryAttack()
    local mode = self:GetNWString("shootmode")
    if mode ==  "Miley Madness" then
        if self.NextBomb < CurTime() then
            self.NextBomb = CurTime() + 8
                self:throw_attack("platypus_bomb",90000)
        end
    end
    if mode ==  "Powerful Patties" then
        if self.NextBombhealth < CurTime() then
            self.NextBombhealth = CurTime() + 50
            self:throw_attack("platypus_patty_heal",900)
        end
    end
    if mode ==  "Tar Trap"  and SERVER then
        if self.NextBombhealth2 < CurTime() then
            self.NextBombhealth2 = CurTime() + 20
            local ent = ents.Create("cse_ent_shorthealthgrenade")
            ent:SetOwner(self.Owner)
            ent.Owner = self.Owner
            ent:SetPos(self.Owner:GetEyeTrace().HitPos + self.Owner:GetAimVector() * -36  )
            ent:SetAngles(Angle(1,0,0))
            ent.healdamage = 10
            ent:Spawn()
          --  ent:SetModel("models/props_junk/garbage_glassbottle003a.mdl")
            ent.timer = CurTime() + 2
         end
    end

end

SWEP.NextMOllie = 0
SWEP.NextBomb4  = 0
function SWEP:PrimaryAttack()
    local mode = self:GetNWString("shootmode")
    if mode ==  "Miley Madness" then
        self:SetNextPrimaryFire(CurTime()+0.1)
        self:ShootBullet(8,1,0.001)
        local trace = self.Owner:GetEyeTrace()
        if (trace.HitNonWorld ) then
            local victim = trace.Entity
            if (not victim:IsPlayer()) then return end
            if math.random(1,2) == 2 then
            victim:AnimPerformGesture_gb(ACT_GMOD_TAUNT_DANCE);
            else
                victim:AnimPerformGesture_gb(ACT_GMOD_TAUNT_MUSCLE);

            end
            self:Freeze(25,2,victim)
            self:Freeze()
            victim:SetHealth(victim:Health() + 1)
        end
    end
    if mode ==  "Powerful Patties" then
        if self.NextBomb4 < CurTime() then
            self.NextBomb4 = CurTime() + 2
            self:throw_attack("platypus_patty",9000)
        end
    end
    if mode ==  "Tar Trap" then
        if self.NextMOllie < CurTime() then
            self.NextMOllie = CurTime() + 30
            if (CLIENT) then return end

            local ent = ents.Create("ent_molotov_king")
            ent:SetPos(self.Owner:EyePos() + (self.Owner:GetAimVector() * 16))
            ent:SetAngles(self.Owner:GetAngles())
            ent:Spawn()
            ent:SetOwner(self.Owner)

            local entobject = ent:GetPhysicsObject()
            entobject:ApplyForceCenter(self.Owner:GetAimVector():GetNormalized() * math.random(3000,8000))

        end
    end
end



SWEP.ViewModels = {
"c_bugbait.mdl",
"c_crossbow.mdl",
"c_crowbar.mdl",
"c_grenade.mdl",
"c_irifle.mdl",
"c_medkit.mdl",
"c_physcannon.mdl",
"c_pistol.mdl",
"c_rpg.mdl",
"c_shotgun.mdl",
"c_slam.mdl",
"c_smg1.mdl",
"c_stunstick.mdl",
"c_superphyscannon.mdl",
"c_toolgun.mdl",
"c_357.mdl",

"c_c4.mdl",
"c_eq_flashbang.mdl"
,"c_eq_fraggrenade.mdl"
,"c_eq_smokegrenade.mdl"
,"c_knife_t.mdl"
,"c_mach_m249para.mdl"
,"c_pist_deagle.mdl"
,"c_pist_elite.mdl"
,"c_pist_fiveseven.mdl"
,"c_pist_glock18.mdl"
,"c_pist_p228.mdl"
,"c_pist_usp.mdl"
,"c_rif_ak47.mdl"
,"c_rif_aug.mdl"
,"c_rif_famas.mdl"
,"c_rif_galil.mdl"
,"c_rif_m4a1.mdl"
,"c_rif_sg552.mdl"
,"c_shot_m3super90.mdl"
,"c_shot_xm1014.mdl"
,"c_smg_mac10.mdl"
,"c_smg_mp5.mdl"
,"c_smg_p90.mdl"
,"c_smg_tmp.mdl"
,"c_smg_ump45.mdl"
,"c_snip_awp.mdl"
,"c_snip_g3sg1.mdl"
,"c_snip_scout.mdl"
,"c_snip_sg550.mdl"
}
SWEP.NumVieModel = 1


function SWEP:SwitchViewModel()
    if  self.NextCamo and self.NextCamo > CurTime() then return end
    self.NextCamo = CurTime() + 0.2
    self.NumVieModel =  math.random(1,#self.ViewModels)
    if self.NumVieModel > 16 then
        if not self.ViewModels[self.NumVieModel] then print("model "..self.NumVieModel.." not found") end
    self.ViewModel		= "models/weapons/cstrike/"..self.ViewModels[self.NumVieModel]
    self.WorldModel		= string.Replace("models/weapons/"..self.ViewModels[self.NumVieModel],"c_","w_")
    else
        self.ViewModel		= "models/weapons/"..self.ViewModels[self.NumVieModel]
        self.WorldModel		= string.Replace("models/weapons/"..self.ViewModels[self.NumVieModel],"c_","w_")
    end
    self:SetModel( self.WorldModel )
    self.Owner:GetViewModel():SetModel(  self.ViewModel  )
  --  self.Owner:PrintMessage(HUD_PRINTCENTER,"Model: "..self.ViewModels[self.NumVieModel])
    if not SERVER then return end
   -- self.Owner:PrintMessage(HUD_PRINTTALK ,"Model: "..self.ViewModels[self.NumVieModel])
end



function SWEP:detachLimb(  )
    if not self.bones then
        self.bones = { "ValveBiped.Bip01_Head1", "ValveBiped.Bip01_R_Hand", "ValveBiped.Bip01_R_Forearm", "ValveBiped.Bip01_R_Foot", "ValveBiped.Bip01_R_Thigh", "ValveBiped.Bip01_R_Calf",  "ValveBiped.Bip01_R_Elbow", "ValveBiped.Bip01_R_Shoulder"
            , "ValveBiped.Bip01_L_Hand", "ValveBiped.Bip01_L_Forearm", "ValveBiped.Bip01_L_Foot", "ValveBiped.Bip01_L_Thigh", "ValveBiped.Bip01_L_Calf",  "ValveBiped.Bip01_L_Elbow", "ValveBiped.Bip01_L_Shoulder" }
    end

    local trace = self.Owner:GetEyeTrace()
    if trace.HitPos:Distance(self.Owner:GetShootPos()) <= 9000 then
        if( trace.Entity:IsPlayer() or trace.Entity:IsNPC() or trace.Entity:GetClass()=="prop_ragdoll" ) then

            timer.Simple(0.01,function()

                local bone = table.Random(self.bones)
                bone = trace.Entity:LookupBone( bone )
                if bone then
                    local Ply = trace.Entity
                    local Normal = bone
                    Ply:ManipulateBoneScale(Normal, vector_origin)
                    Ply:EmitSound( "weapons/shortsword/morrowind_shortsword_hit.mp3" )

                    local ED = EffectData()
                    ED:SetEntity( Ply ) --Player Entity
                    ED:SetNormal( self.Owner:GetForward() )
                    ED:SetScale( Normal )
                    ED:SetOrigin( Ply:GetPos() )
                    util.Effect( 'limbremove', ED ) --Effect Gib
                end

            end)
        end
    end


end


local ENT = {}

ENT.Type = "anim"
ENT.Base = "base_anim"


if SERVER then

    function ENT:Initialize()
        self:SetModel("models/Combine_Helicopter/helicopter_bomb01.mdl")
        self:SetModelScale(self:GetModelScale()*1,0)
        self:SetColor(Color(0,0,128,255))
        self.Entity:PhysicsInit(SOLID_VPHYSICS);
        self.Entity:SetMoveType(MOVETYPE_VPHYSICS);
        self.Entity:SetSolid(SOLID_VPHYSICS);
        self.CanBoom = CurTime() + 3
        self.Angles = -360
        self.damage = 100

       -- self.Entity:SetCollisionGroup(COLLISION_GROUP_WEAPON)
        local phys = self:GetPhysicsObject()
        if phys:IsValid() then
            phys:Wake()
            phys:EnableGravity(true);
        end
        timer.Create(self:EntIndex().."sandboom223",0.2,20,function()
            if IsValid(self) then
                self.damage = self.damage - 10
            end
        end)
        timer.Create(self:EntIndex().."sandboom2",6,0,function()
            if IsValid(self) then
                self:EmitSound( Sound( "C4.PlantSound" ))
                self:SetColor(Color(0,0,math.Rand(50,255),255))
                -- self:SetAngles(Angle(0,math.Rand(0,360),0))
                self:Remove()
            end
        end)

    end


    function ENT:Use( activator, caller )
        if self.CanBoom > CurTime() then return end
        self:EmitSound("common/bugreporter_failed.wav")
        self:Explode()
    end

    function ENT:Touch( ent )
        if ent:IsPlayer() and not ent.nextsandtouch or (ent.nextsandtouch and ent.nextsandtouch < CurTime()) then
            ent.hasProtectionSuit = true
            ent.nextsandtouch = CurTime() + 3
            ent:Ignite(0.5,0)
            local dmginfo = DamageInfo()
            dmginfo:SetDamage( self.damage ) --50 damage
            dmginfo:SetDamageType( DMG_BULLET ) --Bullet damage
            dmginfo:SetAttacker( self.Owner or ent ) --First player found gets credit
            dmginfo:SetDamageForce( Vector( 0, 0, 50 ) ) --Launch upwards
            dmginfo:SetInflictor(self)
            ent:TakeDamageInfo( dmginfo )
            self:Explode()
        end
    end

    function ENT:Explode()
        if self.exploded then return end self.exploded = true
        local effect = EffectData()
        effect:SetStart(self:GetPos())
        effect:SetOrigin(self:GetPos())
        effect:SetScale(self.damage)
        effect:SetRadius(self.damage)
        effect:SetMagnitude(0)

        util.Effect("Explosion", effect, true, true)

        util.BlastDamage(self, self.Owner, self:GetPos(), 0, 100)
        self:Remove()
    end


end

scripted_ents.Register( ENT, "platypus_bomb", true )

local ENT = {}

ENT.Type = "anim"
ENT.Base = "base_anim"


if SERVER then

    function ENT:Initialize()
        self:SetModel("models/food/burger.mdl")
        self:SetModelScale(self:GetModelScale()*1,0)
        self.Entity:PhysicsInit(SOLID_VPHYSICS);
        self.Entity:SetMoveType(MOVETYPE_VPHYSICS);
        self.Entity:SetSolid(SOLID_VPHYSICS);
        self.CanBoom = CurTime() + 3
        self.Angles = -360
        -- self.Entity:SetCollisionGroup(COLLISION_GROUP_WEAPON)
        local phys = self:GetPhysicsObject()
        if phys:IsValid() then
            phys:Wake()
            phys:EnableGravity(true);
        end


    end


    function ENT:Touch( ent )
        if ent:IsPlayer() and not ent.nextsandtouch or (ent.nextsandtouch and ent.nextsandtouch < CurTime()) then
            ent.hasProtectionSuit = true
            ent.nextsandtouch = CurTime() + 3
            ent:Ignite(0.5,0)
            timer.Create(ent:SteamID().."platupuypatty"..math.random(1,9999),1,10,function()
                if IsValid(ent) and IsValid(self.Owner) and ent:Alive() and not ent:IsGhost() then
                    local dmginfo = DamageInfo()
                    dmginfo:SetDamage( 4 ) --50 damage
                    dmginfo:SetDamageType( DMG_BULLET ) --Bullet damage
                    dmginfo:SetAttacker( self.Owner or ent ) --First player found gets credit
                    dmginfo:SetDamageForce( Vector( 0, 0, 50 ) ) --Launch upwards
                    dmginfo:SetInflictor(self)
                    ent:TakeDamageInfo( dmginfo )
                    ent:Ignite(0.1,0)
                end
            end)

        end
    end



end

scripted_ents.Register( ENT, "platypus_patty", true )

local ENT = {}

ENT.Type = "anim"
ENT.Base = "base_anim"


if SERVER then

    function ENT:Initialize()
        self:SetModel("models/food/burger.mdl")
        self:SetModelScale(self:GetModelScale()*1.6,0.1)
        self.Entity:PhysicsInit(SOLID_VPHYSICS);
        self.Entity:SetMoveType(MOVETYPE_VPHYSICS);
        self.Entity:SetSolid(SOLID_VPHYSICS);

        self.Entity:SetCollisionGroup(COLLISION_GROUP_WEAPON)
        local phys = self:GetPhysicsObject()
        if phys:IsValid() then
            phys:Wake()
            phys:EnableGravity(true);
        end
    end

    function ENT:Use( activator, caller )
        self.Entity:Remove()
        if ( activator:IsPlayer() ) then
            if activator:Health() < 150 then
                activator:SetHealth(activator:Health()+50)
                if activator:Health() > 150 then
                    activator:SetHealth(150)
                end
            end
            activator:EmitSound("crisps/eat.mp3")
        end
    end

end

scripted_ents.Register( ENT, "platypus_patty_heal", true )


if CLIENT then
    function SWEP:DrawHUD( )
        local shotttext = "Shootmode: "..self:GetNWString("shootmode").."\nE+R to change"
        surface.SetFont( "ChatFont" );
        local size_x, size_y = surface.GetTextSize( shotttext );

        draw.RoundedBox( 6, ScrW( ) / 2 - size_x / 2 - 5, ScrH( ) - 100, size_x + 15, size_y + 10, Color( 0, 0, 0, 150 ) );
        draw.DrawText( shotttext , "ChatFont", ScrW( ) / 2, ScrH( ) - 100 + 5, Color( 255, 30, 30, 255 ), TEXT_ALIGN_CENTER );
    end
end
