SWEP.Base               = "aa_base"
SWEP.PrintName = "Laser Tag";
    SWEP.HoldType = "ar2"



SWEP.Icon = "vgui/ttt_fun_killicons/bianchi.png"
SWEP.Kind = WEAPON_ROLE
SWEP.Spawnable			= true
SWEP.AdminSpawnable		= true
SWEP.AutoSpawnable      = false
SWEP.Slot				= 8
SWEP.SlotPos			= 0

SWEP.AmmoEnt = "item_ammo_smg1_ttt"
SWEP.HoldType = "ar2"
SWEP.ViewModelFOV = 59
SWEP.ViewModelFlip = false
SWEP.ViewModel = "models/weapons/v_bach_m249para.mdl"
SWEP.WorldModel = "models/weapons/w_bagh_m249para.mdl"

SWEP.ShowViewModel = true
SWEP.ShowWorldModel = true
SWEP.ViewModelBoneMods = {}

SWEP.Primary.Sound 		= Sound("weapons/bianachi/bian-2.mp3")
SWEP.Primary.Recoil		= .2
SWEP.Primary.Damage		= 0
SWEP.Primary.NumShots   = 1
SWEP.Primary.Cone	    = 0.0001
SWEP.Primary.Delay 		= .25

SWEP.Primary.ClipSize		= 600					// Size of a clip
SWEP.Primary.DefaultClip	= 600					// Default number of bullets in a clip
SWEP.Primary.ClipMax 		= 120
SWEP.Primary.Automatic		= true				// Automatic/Semi Auto
SWEP.Primary.Ammo			= "smg1"

SWEP.Secondary.ClipSize		= -1					// Size of a clip
SWEP.Secondary.DefaultClip	= -1					// Default number of bullets in a clip
SWEP.Secondary.ClipMax 		= -1
SWEP.Secondary.Automatic	= false				// Automatic/Semi Auto
SWEP.Secondary.Ammo		= "none"

SWEP.IronSightsPos = Vector(-4.56, -5.5, 2.24)
SWEP.IronSightsAng = Vector(0, 0, 0)
SWEP.RunArmOffset  = Vector(0.381, -1.566, 2.055)
SWEP.RunArmAngle   = Vector(-16.241, 39.08, 0)

SWEP.tracerColor = Color(255,0,0,255)
function SWEP:PrimaryAttack()
    self:NormalPrimaryAttack()
end

function SWEP:OnDrop()
    self:Remove()
end

SWEP.NextTaze = 0

function SWEP:ShootBullet( dmg, recoil, numbul, cone )


    self.Weapon:SendWeaponAnim(self.PrimaryAnim)

    self.Owner:MuzzleFlash()
    self.Owner:SetAnimation( PLAYER_ATTACK1 )

    if not IsFirstTimePredicted() then return end

    local sights = self:GetIronsights()

    numbul = numbul or 1
    cone   = cone   or 0.01

    local bullet = {}
    bullet.Num    = numbul
    bullet.Src    = self.Owner:GetShootPos()
    bullet.Dir    = self.Owner:GetAimVector()
    bullet.Spread = Vector( cone, cone, 0 )
    bullet.Tracer = 1
    bullet.TracerName =  "manatrace"
    bullet.Force  = 10
    bullet.Damage = dmg


    self.Owner:FireBullets( bullet )

    -- Owner can die after firebullets
    if (not IsValid(self.Owner)) or (not self.Owner:Alive()) or self.Owner:IsNPC() then return end

    if ((game.SinglePlayer() and SERVER) or
            ((not game.SinglePlayer()) and CLIENT and IsFirstTimePredicted())) then

        -- reduce recoil if ironsighting
        recoil = sights and (recoil * 0.6) or recoil

        local eyeang = self.Owner:EyeAngles()
        eyeang.pitch = eyeang.pitch - recoil
        self.Owner:SetEyeAngles( eyeang )
    end

    attacker = self.Owner
    local tracedata = {}

    tracedata.start = self.Owner:GetShootPos()
    tracedata.endpos = self.Owner:GetShootPos() + ( self.Owner:GetAimVector() * 450 )
    tracedata.filter = self.Owner
    tracedata.mins = Vector( -3,-3,-3 )
    tracedata.maxs = Vector( 3,3,3 )
    tracedata.mask = MASK_SHOT_HULL
    local trace = util.TraceHull( tracedata )

    local victim = trace.Entity

    timer.Simple(2, function()
        if IsValid(self) and IsValid( self.Owner) and self:Clip1() < 10 then
            self:SetClip1( self:Clip1() + 1)
        end
    end)

    if ( not IsValid(attacker) or !SERVER  or not victim:IsPlayer() or victim:IsWorld() or self.NextTaze > CurTime() ) then return end
    if  !victim:Alive() or victim.IsDog or specialRound.isSpecialRound or _globals['specialperson'][victim:SteamID()] or (victim.NoTaze && self.Owner:GetRole() != ROLE_DETECTIVE)    then
    self.Owner:PrintMessage( HUD_PRINTCENTER, "Person cannot be tazed" )

    return end


    if victim:GetPos():Distance( attacker:GetPos()) > 40000 then return end

    StunTime = self.StunTime

    local edata = EffectData()
    edata:SetEntity(victim)
    edata:SetMagnitude(3)
    edata:SetScale(3)

    util.Effect("TeslaHitBoxes", edata)

    self:Taze( victim)

end


function SWEP:DropHealth()
    if self.NextHealthDrop > CurTime() then return end
    self.NextHealthDrop = CurTime() + 5

    local healttt = 200

    if self.healththing and SERVER then

        healttt = Entity(self.healththing):GetStoredHealth()


                Entity(self.healththing):Remove()

    end

    self:HealthDrop(healttt)

end


SWEP.NextHealthDrop = 0


local throwsound = Sound( "Weapon_SLAM.SatchelThrow" )

-- ye olde droppe code
function SWEP:HealthDrop( healttt)
    if SERVER then
        local ply = self.Owner
        if not IsValid(ply) then return end

        --if self.Planted then return end

        local vsrc = ply:GetShootPos()
        local vang = ply:GetAimVector()
        local vvel = ply:GetVelocity()

        local vthrow = vvel + vang * 200

        local health = ents.Create("laser_health")
        if IsValid(health) then
            health:SetPos(vsrc + vang * 10)
            health:Spawn()
            health:SetModel( "models/props_lab/cactus.mdl")
            health:SetModelScale(health:GetModelScale()*1,0)
            health:SetPlacer(ply)
            health.healsound = "ginger_cartman.mp3"

            health:PhysWake()
            local phys = health:GetPhysicsObject()
            if IsValid(phys) then
                phys:SetVelocity(vthrow)
            end
            health:SetModel( "models/props_lab/cactus.mdl")
            health:SetStoredHealth(healttt)
            health.setOwner = self.Owner

            self.healththing = health:EntIndex()

            self.Planted = true
        end
    end

    self.Weapon:EmitSound(throwsound)
end


SWEP.StunTime = 5
SWEP.HadTaze = 0

function SWEP:Taze( victim )

    if not victim:Alive() or victim:IsGhost() or self.HadTaze > 2 then return end

    if victim:InVehicle() then
        local vehicle = victim:GetParent()
        victim:ExitVehicle()
    end

    ULib.getSpawnInfo( victim )

    if SERVER then DamageLog("Tazer: " .. self.Owner:Nick() .. " [" .. self.Owner:GetRoleString() .. "] tazed "..victim:Nick().." [" .. victim:GetRoleString() .. "] ")
    end
    local rag = ents.Create("prop_ragdoll")
    if not (rag:IsValid()) then return end

    self.NextTaze = CurTime() + 2



    rag.BelongsTo = victim
    rag.Weapons = {}
    for _, v in pairs (victim:GetWeapons()) do
        table.insert(rag.Weapons, v:GetClass())
    end

    rag.Credits = victim:GetCredits()
    rag.OldHealth = victim:Health()
    rag:SetModel(victim:GetModel())
    rag:SetPos( victim:GetPos() )
    local velocity = victim:GetVelocity()
    rag:SetAngles( victim:GetAngles() )
    rag:SetModel( victim:GetModel() )
    rag:Spawn()
    rag:Activate()
    victim:SetParent( rag )
    victim:SetParent( rag )
    victim.Ragdolled = rag
    rag.orgPos = victim:GetPos()
    rag.orgPos.z = rag.orgPos.z + 10

    victim.orgPos = victim:GetPos()
    victim:StripWeapons()
    victim:Spectate(OBS_MODE_CHASE)
    victim:SpectateEntity(rag)

    rag:EmitSound(Sound("vo/Citadel/br_ohshit.wav"))

    victim:PrintMessage( HUD_PRINTCENTER, "You have been tasered. "..self.StunTime.." seconds till revival" )
    timer.Create("revivedelay"..victim:UniqueID(), self.StunTime, 1, function() TazerRevive( victim ) end )

end

function TazerRevive( victim)

    if !IsValid( victim ) then return end
    rag = victim:GetParent()

    if SERVER and victim.IsSlayed then
        victim:Kill();
        return
    end
    victim:SetParent()


    if (rag and rag:IsValid()) then
        weps = table.Copy(rag.Weapons)
        credits = rag.Credits
        oldHealth = rag.OldHealth
        rag:DropToFloor()
        pos = rag:GetPos()  --move up a little
        pos.z = pos.z + 15


        victim.Ragdolled = nil

        victim:UnSpectate()
        --  victim:DrawViewModel(true)
        --  victim:DrawWorldModel(true)
        victim:Spawn()
        --ULib.spawn( victim, true )
        rag:Remove()
        victim:SetPos( pos )
        victim:StripWeapons()

        UnStuck_stuck(victim)

        for _, v in pairs (weps) do
            victim:Give(v)
        end

        if credits > 0 then
            victim:AddCredits( credits  )
        end
        if oldHealth > 0 then
            victim:SetHealth( oldHealth  )
        end
        victim:DropToFloor()



        if  !victim:IsInWorld() or !victim:OnGround()    then
        victim:SetPos( victim.orgPos )
        end

        timer.Create("RespawnIfStuck",1,1,function()
            if IsValid(victim) and (!victim:IsInWorld() or !victim:OnGround() ) then
            victim:SetPos( victim.orgPos   )
            end
        end)

    else
        print("Debug: ragdoll or player is invalid")
        if IsValid( victim) then
            ULib.spawn( victim, true )
        end
    end



end


        function SWEP:SecondaryAttack()

            if self.Owner:KeyDown(IN_USE)  then
                self:DropHealth()
                return
            end

            if (!SERVER) or self.NextSecondary > CurTime() then return end

            self.NextSecondary = CurTime() + 5

            self.Weapon:SendWeaponAnim( ACT_VM_MISSCENTER )
            self.Weapon:EmitSound("weapons/iceaxe/iceaxe_swing1.wav")

            local ent = ents.Create("laserchips")
            local pos = self.Owner:GetShootPos()
            ent:SetPos(pos)
            ent:SetAngles(self.Owner:EyeAngles())
            --ent:SetPhysicsAttacker( self.Owner )
            --ent.owner = self
            ent:Spawn()
            ent:Activate()
            --ent:SetOwner(self.Owner)
            --ent:SetPhysicsAttacker(self.Owner)

            self.Weapon:SetNextSecondaryFire(CurTime() + 2)
            self.Weapon:SetNextPrimaryFire(CurTime() + 1)

            local tr = self.Owner:GetEyeTrace()

            local phys = ent:GetPhysicsObject()
            if (phys:IsValid()) then
                phys:Wake()
                phys:SetMass(1)
                phys:EnableGravity( true )
                phys:ApplyForceCenter( self:GetForward() *2000 )
                phys:SetBuoyancyRatio( 0 )
            end

            local shot_length = tr.HitPos:Length()
            ent:SetVelocity(self.Owner:GetAimVector():GetNormalized() *  math.pow(shot_length, 3))


        end



local ENT = {}

ENT.Type = "anim"
ENT.Base = "base_anim"


if SERVER then

    function ENT:Initialize()


        self:SetModel( "models/props_lab/cactus.mdl")
        self:SetModelScale(self:GetModelScale()*1,0)

        self.Entity:PhysicsInit(SOLID_VPHYSICS);
        self.Entity:SetMoveType(MOVETYPE_VPHYSICS);
        self.Entity:SetSolid(SOLID_VPHYSICS);
        self.Entity:SetCollisionGroup(COLLISION_GROUP_PROJECTILE)
        self.Damage = 1
        self:SetUseType(SIMPLE_USE)
        local phys = self:GetPhysicsObject()
        if phys:IsValid() then
            phys:Wake()
            phys:EnableGravity(true);
        end
        timer.Simple(15,function() if IsValid(self) then self:Remove() end end)

    end

    function ENT:Use( activator, caller )

        if ( activator:IsPlayer() ) then
            if activator:Health() < 150 then

                activator:SetHealth(activator:Health()+6)

            end
            activator:EmitSound("crisps/eat.mp3")
        end
        self.Entity:Remove()
    end


end

scripted_ents.Register( ENT, "laserchips", true )


