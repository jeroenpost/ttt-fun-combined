//General Variables\\
SWEP.AdminSpawnable = true
SWEP.ViewModelFOV = 64
SWEP.Kind = 19
SWEP.Slot = 8

SWEP.VElements = {
	["rail+"] = { type = "Model", model = "models/Items/combine_rifle_cartridge01.mdl", bone = "Python", rel = "", pos = Vector(-0.456, -0.456, 5), angle = Angle(-180, -17.386, -84.887), size = Vector(0.321, 0.321, 0.321), color = Color(255, 255, 255, 255), surpresslightning = false, material = "camos/camo49", skin = 0, bodygroup = {} },
	["Cyl"] = { type = "Model", model = "models/Items/combine_rifle_ammo01.mdl", bone = "Cylinder", rel = "", pos = Vector(0, 0, -2.274), angle = Angle(0, 0, 0), size = Vector(0.379, 0.379, 0.379), color = Color(255, 255, 255, 255), surpresslightning = false, material = "camos/camo49", skin = 0, bodygroup = {} },
	["rail++"] = { type = "Model", model = "models/Items/combine_rifle_cartridge01.mdl", bone = "Python", rel = "", pos = Vector(-0.456, -0.456, 5.908), angle = Angle(-180, -17.386, -84.887), size = Vector(0.321, 0.321, 0.321), color = Color(255, 255, 255, 255), surpresslightning = false, material = "camos/camo49", skin = 0, bodygroup = {} },
	["rail"] = { type = "Model", model = "models/Items/combine_rifle_cartridge01.mdl", bone = "Python", rel = "", pos = Vector(-0.456, -0.456, 6.817), angle = Angle(-180, -17.386, -84.887), size = Vector(0.321, 0.321, 0.321), color = Color(255, 255, 255, 255), surpresslightning = false, material = "camos/camo49", skin = 0, bodygroup = {} },
	["knob"] = { type = "Model", model = "models/props_borealis/door_wheel001a.mdl", bone = "Python", rel = "", pos = Vector(0, -1.3, -2.597), angle = Angle(-104.027, -90, -153.118), size = Vector(0.15, 0.15, 0.15), color = Color(255, 255,255, 255), surpresslightning = false, material = "camos/camo49", skin = 0, bodygroup = {} }
}
SWEP.WElements = {
	["rail+"] = { type = "Model", model = "models/Items/combine_rifle_cartridge01.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(12.272, 0.455, -4.092), angle = Angle(0, 86.931, -180), size = Vector(0.264, 0.264, 0.264), color = Color(255, 255, 255, 255), surpresslightning = false, material = "camos/camo49", skin = 0, bodygroup = {} },
	["rail+++"] = { type = "Model", model = "models/Items/combine_rifle_cartridge01.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(13.182, 0.455, -4.092), angle = Angle(0, 86.931, -180), size = Vector(0.264, 0.264, 0.264), color = Color(255, 255, 255, 255), surpresslightning = false, material = "camos/camo49", skin = 0, bodygroup = {} },
	["Cyl"] = { type = "Model", model = "models/Items/combine_rifle_ammo01.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(5, 0.92, -4.092), angle = Angle(-91.024, -1.024, 0), size = Vector(0.379, 0.379, 0.379), color = Color(255, 255, 255, 255), surpresslightning = false, material = "camos/camo49", skin = 0, bodygroup = {} },
	["rail++"] = { type = "Model", model = "models/Items/combine_rifle_cartridge01.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(11.364, 0.455, -4.092), angle = Angle(0, 86.931, -180), size = Vector(0.264, 0.264, 0.264), color = Color(255, 255, 255, 255), surpresslightning = false, material = "camos/camo49", skin = 0, bodygroup = {} },
	["knob"] = { type = "Model", model = "models/props_borealis/door_wheel001a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(4.4, 0.889, -4.301), angle = Angle(-146.105, 0, 0), size = Vector(0.17, 0.17, 0.17), color = Color(255, 255,255, 255), surpresslightning = false, material = "camos/camo49", skin = 0, bodygroup = {} }
}

SWEP.AutoSwitchTo = false
SWEP.Slot = 2
SWEP.PrintName = "Valve's Fruit Cruncher"
SWEP.Author = "Spastik"
SWEP.Spawnable = true
SWEP.AutoSwitchFrom = false
SWEP.FiresUnderwater = true
SWEP.Weight = 5
SWEP.DrawCrosshair = true
SWEP.Category = "S&G Munitions"
SWEP.SlotPos = 1
SWEP.DrawAmmo = true
SWEP.Instructions = "Point gun hole at other man and squeeze trigger to make boom."
SWEP.Contact = ""
SWEP.Purpose = "To make other man not live"
SWEP.Base = "gb_camo_base"
SWEP.HoldType = "revolver"
SWEP.ViewModelFOV = 54.724409448819
SWEP.ViewModelFlip = false
SWEP.ViewModel = "models/weapons/c_357.mdl"
SWEP.WorldModel = "models/weapons/W_357.mdl"
SWEP.ShowViewModel = true
SWEP.ShowWorldModel = true
SWEP.ViewModelBoneMods = {
	["Base"] = { scale = Vector(1, 1, 1), pos = Vector(0, 2.388, 1.194), angle = Angle(0, 0, 0) },
}

//General Variables\\

//Primary Fire Variables\\
SWEP.Primary.Sound = "weapons/357/357_fire2.wav"
SWEP.Primary.Damage = 55
SWEP.Primary.TakeAmmo = 1
SWEP.Primary.ClipSize = 999
SWEP.Primary.Ammo = "357"
SWEP.Primary.DefaultClip = 9999
SWEP.Primary.Spread = 0.2
SWEP.Primary.NumberofShots = 1
SWEP.Primary.Automatic = true
SWEP.Primary.Recoil = 4
SWEP.Primary.Delay = 0.36
SWEP.Primary.Force = 3
SWEP.Secondary.Automatic = true

//Primary Fire Variables\\

//Secondary Fire Variables\\
SWEP.Secondary.Ammo = "none"
//Secondary Fire Variables\\
SWEP.Camo = 42

//SWEP:Initialize()\\
function SWEP:Initialize()
	util.PrecacheSound(self.Primary.Sound)
	util.PrecacheSound(self.Secondary.Sound)
	if ( SERVER ) then
		self:SetHoldType( self.HoldType or "pistol" )
    end
    self.Baseclass.Initialize(self)
end
//SWEP:Initialize()\\
SWEP.nextreload2 = 0
SWEP.BulletEffectName = "ShellEject"
function SWEP:Reload()
    if self.Owner:KeyDown(IN_USE) then
        if SERVER  then
            if self.nextreload2 > CurTime() then
                if self.nextreload2 - CurTime() < 55 then
                    self.Owner:PrintMessage( HUD_PRINTCENTER,  math.floor(self.nextreload2 - CurTime()).." seconds left" )
                end
                return
            end
            self.nextreload2 = CurTime() + 55
            local tr = self.Owner:GetEyeTrace()
            local ent = ents.Create("fruity_cluster_bomb")
            ent:SetOwner(self.Owner)
            ent.Owner = self.Owner
            ent.Owner = self.Owner

            ent:SetPos( tr.HitPos + self.Owner:GetAimVector() * -46  )
            ent:SetAngles(Angle(1,0,0))

            ent:Spawn()

        end


        return
    end

    if self.Owner:KeyDown(IN_DUCK) then
         self:DefaultReload( ACT_VM_RELOAD )
		 if ( self:Clip1() == self.Primary.ClipSize or self:Ammo1() == 0 ) then return end
		 timer.Simple( 1.92,function()
		       if ( IsValid( self ) and self.Owner:GetActiveWeapon() == self ) then
			        for i = 1,self.Primary.ClipSize do
			            local bullet = EffectData()
					          bullet:SetOrigin( self.Owner:GetPos() + Vector( 0,0,15 ) )
							  bullet:SetAngles( Angle( 90,0,0 ) )
					    util.Effect( self.BulletEffectName,bullet )
					end
			   end
		 end )
		 local vm = self.Owner:GetViewModel()
		 if ( IsValid( vm ) ) then
		      timer.Simple( vm:SequenceDuration(),function()
			        if ( IsValid( self ) and self.Owner:GetActiveWeapon() == self ) then
					     self:SendWeaponAnim( ACT_VM_IDLE )
					end
			  end )
         end
        return
    end

    self:rFly()


end

//SWEP:PrimaryFire()\\
function SWEP:PrimaryAttack()
	if ( !self:CanPrimaryAttack() ) then return end
    self.Owner:SetJumpPower(450)
    self:SetNextPrimaryFire( CurTime() + self.Primary.Delay	)
    if self:Clip1() > 0 then
        self.BaseClass.PrimaryAttack(self)
        self:throw_attack("fruity_ball",100000)

    end
end
//SWEP:PrimaryFire()\\

//SWEP:SecondaryFire()\\
function SWEP:SecondaryAttack()
    if ( !self:CanSecondaryAttack() ) then return end
    local bullet = {}
    bullet.Num = self.Primary.NumberofShots
    bullet.Src = self.Owner:GetShootPos()
    bullet.Dir = self.Owner:GetAimVector()
    bullet.Spread = Vector( 0.08,0.08, 0)
    bullet.Tracer = 5
    bullet.Force = 1
    bullet.Damage = 8
    bullet.AmmoType = self.Primary.Ammo
    local rnda = self.Primary.Recoil * .8
    local rndb = self.Primary.Recoil * math.random(-1, 1)
    self:ShootEffects()
    self.Owner:FireBullets( bullet )
    self.Weapon:EmitSound(Sound(self.Primary.Sound), 500, 100, 1, 0)
    --self.Owner:ViewPunch( Angle( rnda,rndb,rnda ) )
   -- self:TakePrimaryAmmo(self.Primary.TakeAmmo)
    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
    self.Weapon:SetNextSecondaryFire( CurTime() + 0.05 )
    self:throw_attack("fruity_ball_nd",100000)
end



local ENT = {}

ENT.Type = "anim"
ENT.Base = "base_anim"



if SERVER then

    function ENT:Initialize()

        self.Hit = false
        self.LastShout = CurTime()
        self.CurrentPitch = 100
        self.SpawnDelay = CurTime() + 0.5
        --damage self.Damage = 75
        self:SetModel("models/props/cs_italy/orange.mdl")
        self:SetModelScale(1.5,0.01)
        --self:SetMaterial("phoenix_storms/plastic" )
        --self:SetColor(Color(0, 255, 0,255))
        self:PhysicsInitBox(Vector(-4,-4,-4),Vector(4,4,4))
        self:SetMoveType(MOVETYPE_VPHYSICS)
        self:SetSolid(SOLID_VPHYSICS)
        self:SetTrigger(true)
        self.Mag = 30

        self:DrawShadow(false)
        -- self:SetModel( "models/weapons/w_rif_famas.mdl")

        local phys = self:GetPhysicsObject()
        if (phys:IsValid()) then
            phys:Wake()
            phys:EnableDrag(false)
            phys:AddGameFlag(FVPHYSICS_NO_IMPACT_DMG)
            --    phys:SetBuoyancyRatio(0)
        end

    end

    function ENT:Explode()
        if self.exploded then return end self.exploded = true

        local ply = self.Owner


        local guys = ents.FindInSphere( self:GetPos()+Vector(0,0,40), 150 )
        local owner = self.Owner
        for k, guy in pairs(guys) do
            if owner != guy and (guy:IsPlayer() or guy:IsNPC())  then
            local effect = EffectData()
            local origin = guy:GetPos()
            effect:SetOrigin( origin )
            effect:SetScale( 1 )
            util.Effect( "AntlionGib", effect )

            timer.Create(guy:EntIndex().."acid"..math.Rand(1,10),1,5,function()
                if IsValid(owner) and IsValid(guy)  then
                    guy:TakeDamage( 5, owner, owner )
                end
            end)
            end
        end

        -- util.BlastDamage( self, self.Owner, self:GetPos(), 50, 75 )

        self:EmitSound( "ambient/fire/gascan_ignite1.wav" )
        if SERVER then self:Remove() end
    end


    function ENT:Touch(ent)
        if ent != self.Owner then
        self:Explode()
        end
    end

    function ENT:PhysicsCollide(data,phys)
        if ent != self.Owner and SERVER then
        self:Explode()
        end
    end

end

scripted_ents.Register( ENT, "fruity_ball", true )


local ENT = {}

ENT.Type = "anim"
ENT.Base = "fruity_ball"

function ENT:Explode()
    if self.exploded then return end self.exploded = true
    self:Remove()
    end

scripted_ents.Register( ENT, "fruity_ball_nd", true )




local ENT = {}

ENT.Type = "anim"
ENT.Base = "base_anim"


if SERVER then

    function ENT:Initialize()
        if not self.ischild then
        self:SetModel("models/props_junk/watermelon01.mdl")
        else
            self:SetModel("models/props/cs_italy/orange.mdl")
        end
        self:SetModelScale(self:GetModelScale()*1,0)
        self.Entity:PhysicsInit(SOLID_VPHYSICS);
        self.Entity:SetMoveType(MOVETYPE_VPHYSICS);
        self.Entity:SetSolid(SOLID_VPHYSICS);

        self.Entity:SetCollisionGroup(COLLISION_GROUP_WEAPON)
        local phys = self:GetPhysicsObject()
        if phys:IsValid() then
            phys:Wake()
            phys:EnableGravity(true);
        end
        self.timer = CurTime() + 3
    end

    function ENT:Think()
        if self.timer < CurTime() and SERVER then
            self.timer = CurTime() + 90
            self:ExplodeSkulls()
        end

    end


    function ENT:ExplodeSkulls()
        if not self.ischild then
            for i=1,4 do
                local spos = self:GetPos()+Vector(math.random(-75,75),math.random(-75,75),math.random(0,50))
                local bomb = ents.Create("fruity_cluster_bomb")
                bomb.ischild = true
                bomb:SetPos(spos)
                bomb:SetModel("models/props/cs_italy/orange.mdl")
                bomb.Owner = self.Owner
                bomb:Spawn()
            end
            self.ischild = true
            self:ExplodeSkulls()
        else





            local ent = ents.Create( "env_explosion" )

            ent:SetPos( self:GetPos() )
            ent:SetKeyValue( "iMagnitude", "3" )
            ent:Spawn()

            ent:Fire( "Explode",1,1 )
            ent:EmitSound( "physics/body/body_medium_break2.wav", 350, 100 )


            self:EmitSound("weapons/big_explosion.mp3")


            local headturtle = self:SpawnNPC2(self.Owner,self:GetPos(), table.Random({"npc_headcrab_black","npc_fastzombie"}))

            headturtle:SetNPCState(2)
            headturtle.isturtlebomb = false

            local turtle = ents.Create("prop_dynamic")
            if math.random(1,2) == 2 then
            turtle:SetModel("models/props/cs_italy/orange.mdl")
            else
                turtle:SetModel("models/props_junk/watermelon01.mdl")
            end
            turtle:SetPos(self:GetPos())
            turtle:SetAngles(Angle(0,-90,0))
            turtle:SetParent(headturtle)

            --headturtle:SetCollisionGroup(COLLISION_GROUP_WEAPON)

            headturtle:SetNWEntity("Thrower", self.Owner)
            --headturtle:SetName(self:GetThrower():GetName())
            headturtle:SetNoDraw(true)
            headturtle:SetHealth(10)

            self:Remove()

        end
    end
    function ENT:SpawnNPC2( Player, Position, Class )

        local NPCList = list.Get( "NPC" )
        local NPCData = NPCList[ Class ]

        -- Don't let them spawn this entity if it isn't in our NPC Spawn list.
        -- We don't want them spawning any entity they like!
        if ( !NPCData ) then
        if ( IsValid( Player ) ) then
            Player:SendLua( "Derma_Message( \"Sorry! You can't spawn that NPC!\" )" );
        end
        return end

        local bDropToFloor = false

        --
        -- This NPC has to be spawned on a ceiling ( Barnacle )
        --
        if ( NPCData.OnCeiling && Vector( 0, 0, -1 ):Dot( Normal ) < 0.95 ) then
        return nil
        end

        if ( NPCData.NoDrop ) then bDropToFloor = false end

        --
        -- Offset the position
        --


        -- Create NPC
        local NPC = ents.Create( NPCData.Class )
        if ( !IsValid( NPC ) ) then return end

        NPC:SetPos( Position )
        NPC.isturtlebumb = true
        --
        -- This NPC has a special model we want to define
        --
        if ( NPCData.Model ) then
            NPC:SetModel( NPCData.Model )
        end

        --
        -- Spawn Flags
        --
        local SpawnFlags = bit.bor( SF_NPC_FADE_CORPSE, SF_NPC_ALWAYSTHINK)
        if ( NPCData.SpawnFlags ) then SpawnFlags = bit.bor( SpawnFlags, NPCData.SpawnFlags ) end
        if ( NPCData.TotalSpawnFlags ) then SpawnFlags = NPCData.TotalSpawnFlags end
        NPC:SetKeyValue( "spawnflags", SpawnFlags )

        --
        -- Optional Key Values
        --
        if ( NPCData.KeyValues ) then
            for k, v in pairs( NPCData.KeyValues ) do
                NPC:SetKeyValue( k, v )
            end
        end

        --
        -- This NPC has a special skin we want to define
        --
        if ( NPCData.Skin ) then
            NPC:SetSkin( NPCData.Skin )
        end

        --
        -- What weapon should this mother be carrying
        --

        NPC:Spawn()
        NPC:Activate()

        if ( bDropToFloor && !NPCData.OnCeiling ) then
        NPC:DropToFloor()
        end

        return NPC
    end

end

scripted_ents.Register( ENT, "fruity_cluster_bomb", true )









/********************************************************
	SWEP Construction Kit base code
		Created by Clavus
	Available for public use, thread at:
	   facepunch.com/threads/1032378


	DESCRIPTION:
		This script is meant for experienced scripters
		that KNOW WHAT THEY ARE DOING. Don't come to me
		with basic Lua questions.

		Just copy into your SWEP or SWEP base of choice
		and merge with your own code.

		The SWEP.VElements, SWEP.WElements and
		SWEP.ViewModelBoneMods tables are all optional
		and only have to be visible to the client.
********************************************************/

function SWEP:Initialize()
    self.BaseClass.Initialize(self)

	// other initialize code goes here

	if CLIENT then

		self:SetHoldType( self.HoldType or "pistol" )
		// Create a new table for every weapon instance
		self.VElements = table.FullCopy( self.VElements )
		self.WElements = table.FullCopy( self.WElements )
		self.ViewModelBoneMods = table.FullCopy( self.ViewModelBoneMods )

		self:CreateModels(self.VElements) // create viewmodels
		self:CreateModels(self.WElements) // create worldmodels

		// init view model bone build function
		if IsValid(self.Owner) then
			local vm = self.Owner:GetViewModel()
			if IsValid(vm) then
				self:ResetBonePositions(vm)

				--[[// Init viewmodel visibility
				if (self.ShowViewModel == nil or self.ShowViewModel) then
					vm:SetColor(Color(255,255,255,255))
				else
					// we set the alpha to 1 instead of 0 because else ViewModelDrawn stops being called
					vm:SetColor(Color(255,255,255,1))
					// ^ stopped working in GMod 13 because you have to do Entity:SetRenderMode(1) for translucency to kick in
					// however for some reason the view model resets to render mode 0 every frame so we just apply a debug material to prevent it from drawing
					vm:SetMaterial("Debug/hsv")
				end]]--
			end
		end
	end
end
function SWEP:Holster()

	if CLIENT and IsValid(self.Owner) then
		local vm = self.Owner:GetViewModel()
		if IsValid(vm) then
			self:ResetBonePositions(vm)
		end
	end

	return true
end

function SWEP:OnRemove()
	self:Holster()
end

if CLIENT then

	SWEP.vRenderOrder = nil
	function SWEP:ViewModelDrawn()

		local vm = self.Owner:GetViewModel()
		if !IsValid(vm) then return end

		if (!self.VElements) then return end

		self:UpdateBonePositions(vm)

		if (!self.vRenderOrder) then

			// we build a render order because sprites need to be drawn after models
			self.vRenderOrder = {}

			for k, v in pairs( self.VElements ) do
				if (v.type == "Model") then
					table.insert(self.vRenderOrder, 1, k)
				elseif (v.type == "Sprite" or v.type == "Quad") then
					table.insert(self.vRenderOrder, k)
				end
			end

		end

		for k, name in ipairs( self.vRenderOrder ) do

			local v = self.VElements[name]
			if (!v) then self.vRenderOrder = nil break end
			if (v.hide) then continue end

			local model = v.modelEnt
			local sprite = v.spriteMaterial

			if (!v.bone) then continue end

			local pos, ang = self:GetBoneOrientation( self.VElements, v, vm )

			if (!pos) then continue end

			if (v.type == "Model" and IsValid(model)) then

				model:SetPos(pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z )
				ang:RotateAroundAxis(ang:Up(), v.angle.y)
				ang:RotateAroundAxis(ang:Right(), v.angle.p)
				ang:RotateAroundAxis(ang:Forward(), v.angle.r)

				model:SetAngles(ang)
				//model:SetModelScale(v.size)
				local matrix = Matrix()
				matrix:Scale(v.size)
				model:EnableMatrix( "RenderMultiply", matrix )

				if (v.material == "") then
					model:SetMaterial("")
				elseif (model:GetMaterial() != v.material) then
					model:SetMaterial( v.material )
				end

				if (v.skin and v.skin != model:GetSkin()) then
					model:SetSkin(v.skin)
				end

				if (v.bodygroup) then
					for k, v in pairs( v.bodygroup ) do
						if (model:GetBodygroup(k) != v) then
							model:SetBodygroup(k, v)
						end
					end
				end

				if (v.surpresslightning) then
					render.SuppressEngineLighting(true)
				end

				render.SetColorModulation(v.color.r/255, v.color.g/255, v.color.b/255)
				render.SetBlend(v.color.a/255)
				model:DrawModel()
				render.SetBlend(1)
				render.SetColorModulation(1, 1, 1)

				if (v.surpresslightning) then
					render.SuppressEngineLighting(false)
				end

			elseif (v.type == "Sprite" and sprite) then

				local drawpos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
				render.SetMaterial(sprite)
				render.DrawSprite(drawpos, v.size.x, v.size.y, v.color)

			elseif (v.type == "Quad" and v.draw_func) then

				local drawpos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
				ang:RotateAroundAxis(ang:Up(), v.angle.y)
				ang:RotateAroundAxis(ang:Right(), v.angle.p)
				ang:RotateAroundAxis(ang:Forward(), v.angle.r)

				cam.Start3D2D(drawpos, ang, v.size)
					v.draw_func( self )
				cam.End3D2D()

			end

		end

	end

	SWEP.wRenderOrder = nil
	function SWEP:DrawWorldModel()

		if (self.ShowWorldModel == nil or self.ShowWorldModel) then
			self:DrawModel()
		end

		if (!self.WElements) then return end

		if (!self.wRenderOrder) then

			self.wRenderOrder = {}

			for k, v in pairs( self.WElements ) do
				if (v.type == "Model") then
					table.insert(self.wRenderOrder, 1, k)
				elseif (v.type == "Sprite" or v.type == "Quad") then
					table.insert(self.wRenderOrder, k)
				end
			end

		end

		if (IsValid(self.Owner)) then
			bone_ent = self.Owner
		else
			// when the weapon is dropped
			bone_ent = self
		end

		for k, name in pairs( self.wRenderOrder ) do

			local v = self.WElements[name]
			if (!v) then self.wRenderOrder = nil break end
			if (v.hide) then continue end

			local pos, ang

			if (v.bone) then
				pos, ang = self:GetBoneOrientation( self.WElements, v, bone_ent )
			else
				pos, ang = self:GetBoneOrientation( self.WElements, v, bone_ent, "ValveBiped.Bip01_R_Hand" )
			end

			if (!pos) then continue end

			local model = v.modelEnt
			local sprite = v.spriteMaterial

			if (v.type == "Model" and IsValid(model)) then

				model:SetPos(pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z )
				ang:RotateAroundAxis(ang:Up(), v.angle.y)
				ang:RotateAroundAxis(ang:Right(), v.angle.p)
				ang:RotateAroundAxis(ang:Forward(), v.angle.r)

				model:SetAngles(ang)
				//model:SetModelScale(v.size)
				local matrix = Matrix()
				matrix:Scale(v.size)
				model:EnableMatrix( "RenderMultiply", matrix )

				if (v.material == "") then
					model:SetMaterial("")
				elseif (model:GetMaterial() != v.material) then
					model:SetMaterial( v.material )
				end

				if (v.skin and v.skin != model:GetSkin()) then
					model:SetSkin(v.skin)
				end

				if (v.bodygroup) then
					for k, v in pairs( v.bodygroup ) do
						if (model:GetBodygroup(k) != v) then
							model:SetBodygroup(k, v)
						end
					end
				end

				if (v.surpresslightning) then
					render.SuppressEngineLighting(true)
				end

				render.SetColorModulation(v.color.r/255, v.color.g/255, v.color.b/255)
				render.SetBlend(v.color.a/255)
				model:DrawModel()
				render.SetBlend(1)
				render.SetColorModulation(1, 1, 1)

				if (v.surpresslightning) then
					render.SuppressEngineLighting(false)
				end

			elseif (v.type == "Sprite" and sprite) then

				local drawpos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
				render.SetMaterial(sprite)
				render.DrawSprite(drawpos, v.size.x, v.size.y, v.color)

			elseif (v.type == "Quad" and v.draw_func) then

				local drawpos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
				ang:RotateAroundAxis(ang:Up(), v.angle.y)
				ang:RotateAroundAxis(ang:Right(), v.angle.p)
				ang:RotateAroundAxis(ang:Forward(), v.angle.r)

				cam.Start3D2D(drawpos, ang, v.size)
					v.draw_func( self )
				cam.End3D2D()

			end

		end

	end

	function SWEP:GetBoneOrientation( basetab, tab, ent, bone_override )

		local bone, pos, ang
		if (tab.rel and tab.rel != "") then

			local v = basetab[tab.rel]

			if (!v) then return end

			// Technically, if there exists an element with the same name as a bone
			// you can get in an infinite loop. Let's just hope nobody's that stupid.
			pos, ang = self:GetBoneOrientation( basetab, v, ent )

			if (!pos) then return end

			pos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
			ang:RotateAroundAxis(ang:Up(), v.angle.y)
			ang:RotateAroundAxis(ang:Right(), v.angle.p)
			ang:RotateAroundAxis(ang:Forward(), v.angle.r)

		else

			bone = ent:LookupBone(bone_override or tab.bone)

			if (!bone) then return end

			pos, ang = Vector(0,0,0), Angle(0,0,0)
			local m = ent:GetBoneMatrix(bone)
			if (m) then
				pos, ang = m:GetTranslation(), m:GetAngles()
			end

			if (IsValid(self.Owner) and self.Owner:IsPlayer() and
				ent == self.Owner:GetViewModel() and self.ViewModelFlip) then
				ang.r = -ang.r // Fixes mirrored models
			end

		end

		return pos, ang
	end

	function SWEP:CreateModels( tab )

		if (!tab) then return end

		// Create the clientside models here because Garry says we can't do it in the render hook
		for k, v in pairs( tab ) do
			if (v.type == "Model" and v.model and v.model != "" and (!IsValid(v.modelEnt) or v.createdModel != v.model) and
					string.find(v.model, ".mdl") and file.Exists (v.model, "GAME") ) then

				v.modelEnt = ClientsideModel(v.model, RENDER_GROUP_VIEW_MODEL_OPAQUE)
				if (IsValid(v.modelEnt)) then
					v.modelEnt:SetPos(self:GetPos())
					v.modelEnt:SetAngles(self:GetAngles())
					v.modelEnt:SetParent(self)
					v.modelEnt:SetNoDraw(true)
					v.createdModel = v.model
				else
					v.modelEnt = nil
				end

			elseif (v.type == "Sprite" and v.sprite and v.sprite != "" and (!v.spriteMaterial or v.createdSprite != v.sprite)
				and file.Exists ("materials/"..v.sprite..".vmt", "GAME")) then

				local name = v.sprite.."-"
				local params = { ["$basetexture"] = v.sprite }
				// make sure we create a unique name based on the selected options
				local tocheck = { "nocull", "additive", "vertexalpha", "vertexcolor", "ignorez" }
				for i, j in pairs( tocheck ) do
					if (v[j]) then
						params["$"..j] = 1
						name = name.."1"
					else
						name = name.."0"
					end
				end

				v.createdSprite = v.sprite
				v.spriteMaterial = CreateMaterial(name,"UnlitGeneric",params)

			end
		end

	end

	local allbones
	local hasGarryFixedBoneScalingYet = false

	function SWEP:UpdateBonePositions(vm)

		if self.ViewModelBoneMods then

			if (!vm:GetBoneCount()) then return end

			// !! WORKAROUND !! //
			// We need to check all model names :/
			local loopthrough = self.ViewModelBoneMods
			if (!hasGarryFixedBoneScalingYet) then
				allbones = {}
				for i=0, vm:GetBoneCount() do
					local bonename = vm:GetBoneName(i)
					if (self.ViewModelBoneMods[bonename]) then
						allbones[bonename] = self.ViewModelBoneMods[bonename]
					else
						allbones[bonename] = {
							scale = Vector(1,1,1),
							pos = Vector(0,0,0),
							angle = Angle(0,0,0)
						}
					end
				end

				loopthrough = allbones
			end
			// !! ----------- !! //

			for k, v in pairs( loopthrough ) do
				local bone = vm:LookupBone(k)
				if (!bone) then continue end

				// !! WORKAROUND !! //
				local s = Vector(v.scale.x,v.scale.y,v.scale.z)
				local p = Vector(v.pos.x,v.pos.y,v.pos.z)
				local ms = Vector(1,1,1)
				if (!hasGarryFixedBoneScalingYet) then
					local cur = vm:GetBoneParent(bone)
					while(cur >= 0) do
						local pscale = loopthrough[vm:GetBoneName(cur)].scale
						ms = ms * pscale
						cur = vm:GetBoneParent(cur)
					end
				end

				s = s * ms
				// !! ----------- !! //

				if vm:GetManipulateBoneScale(bone) != s then
					vm:ManipulateBoneScale( bone, s )
				end
				if vm:GetManipulateBoneAngles(bone) != v.angle then
					vm:ManipulateBoneAngles( bone, v.angle )
				end
				if vm:GetManipulateBonePosition(bone) != p then
					vm:ManipulateBonePosition( bone, p )
				end
			end
		else
			self:ResetBonePositions(vm)
		end

	end

	function SWEP:ResetBonePositions(vm)

		if (!vm:GetBoneCount()) then return end
		for i=0, vm:GetBoneCount() do
			vm:ManipulateBoneScale( i, Vector(1, 1, 1) )
			vm:ManipulateBoneAngles( i, Angle(0, 0, 0) )
			vm:ManipulateBonePosition( i, Vector(0, 0, 0) )
		end

	end

	/**************************
		Global utility code
	**************************/

	// Fully copies the table, meaning all tables inside this table are copied too and so on (normal table.Copy copies only their reference).
	// Does not copy entities of course, only copies their reference.
	// WARNING: do not use on tables that contain themselves somewhere down the line or you'll get an infinite loop
	function table.FullCopy( tab )

		if (!tab) then return nil end

		local res = {}
		for k, v in pairs( tab ) do
			if (type(v) == "table") then
				res[k] = table.FullCopy(v) // recursion ho!
			elseif (type(v) == "Vector") then
				res[k] = Vector(v.x, v.y, v.z)
			elseif (type(v) == "Angle") then
				res[k] = Angle(v.p, v.y, v.r)
			else
				res[k] = v
			end
		end

		return res

	end

end

