-- Read the weapon_real_base if you really want to know what each action does

if (SERVER) then
    AddCSLuaFile(  )
    --resource.AddFile("sound/weapons/mario_power/mario_fireball.mp3")
    --resource.AddFile("sound/weapons/mario_power/mario_powerup.mp3")
    --resource.AddFile("materials/vgui/ttt/mario_fireball.png")
    --resource.AddFile("materials/vgui/ttt_fun_killicons/mario_fireball.png")
end

SWEP.Base		= "weapon_real_base"
SWEP.PrintName 		= "Jeremy Fireballs"
SWEP.Slot 			= 7

if (CLIENT) then

    SWEP.ViewModelFOV		= 70

    SWEP.SlotPos 		= 1
    SWEP.IconLetter 		= "C"
end

SWEP.Icon = "vgui/ttt_fun_killicons/mario_fireball.png"
SWEP.Slot 			= 1
SWEP.SlotPos 		= 3
SWEP.ViewModel = Model("models/weapons/v_hands.mdl")
SWEP.WorldModel = ""
SWEP.DrawWorldModel = false
SWEP.Author		= "Buu342"
SWEP.Contact		= "buu342@hotmail.com"
SWEP.Purpose		= ""
SWEP.Instructions	= "Shoot with primary fire."
SWEP.CSMuzzleFlashes    = true
SWEP.ViewModelFOV	= 70
SWEP.Drawammo 		= true
SWEP.DrawCrosshair 	= true
SWEP.Category		= "Mario"
SWEP.Kind = WEAPON_EQUIP2



SWEP.data = {}
SWEP.data.newclip	= false

SWEP.Spawnable			= true
SWEP.AdminSpawnable		= true

SWEP.HoldType = "fist"
SWEP.UseHands			= true

local overheat = 0

SWEP.Primary.Sound		= "weapons/mario_power/mario_fireball.mp3"
SWEP.Primary.Recoil		= 0.03
SWEP.Primary.Damage		= 10
SWEP.Primary.NumShots		= -1
SWEP.Primary.Cone		= 0.01
SWEP.Primary.ClipSize		= -1
SWEP.Primary.Delay		= 0.4
SWEP.Primary.DefaultClip	= -1
SWEP.Primary.Automatic		= false
SWEP.Primary.Ammo		= "none"
SWEP.Weight = 5

SWEP.Secondary.ClipSize		= -1
SWEP.Secondary.DefaultClip	= -1
SWEP.Secondary.Automatic	= false
SWEP.Secondary.Ammo		= "none"

SWEP.data 				= {}
SWEP.mode 				= "semi"

SWEP.data.semi 			= {}
SWEP.data.semi.Cone 		= 0.015

SWEP.data.burst 			= {}
SWEP.data.burst.Delay 		= 0.9
SWEP.data.burst.Cone 		= 0.03
SWEP.data.burst.BurstDelay 	= 0.05
SWEP.data.burst.Shots 		= 3
SWEP.data.burst.Counter 	= 0
SWEP.data.burst.Timer 		= 0

function SWEP:Initialize()

    self:SetHoldType( "fist" )

end

function SWEP:PrimaryAttack()

    if ( !self:CanPrimaryAttack() or !IsValid(self.Owner) ) then return end
    self.Weapon:SetNextPrimaryFire(CurTime() + 2)
    local shoot_angle = Vector(0,0,0)
    if self.Owner:IsNPC() then
        if self.Owner:GetEnemy() != NULL && self.Owner:GetEnemy() != nil then
        shoot_angle = self.Owner:GetEnemy():GetPos() - self.Owner:GetPos()
                //apply accuracy
        shoot_angle = shoot_angle + Vector(math.random(-85,85),math.random(-85,85),math.random(-85,85))*(shoot_angle:Distance(Vector(0,0,0))/1500)
                //print("Shoot angle is:")
                //print(shoot_angle)
        shoot_angle:Normalize()
        //print("Normalized, shoot angle is:")
        //print(shoot_angle)
        else
            return
        end
    else
        shoot_angle = self.Owner:GetAimVector()
    end
    local shoot_pos   = self.Owner:GetPos() + self.Owner:GetRight() * 1 + self.Owner:GetUp() * 45 + shoot_angle * 35

    if self.Owner:IsPlayer() then
        shoot_pos   = self.Owner:GetPos() + self.Owner:GetRight() * 1 + self.Owner:GetUp() * 45 + shoot_angle * 35
    end
    self:EmitSound(self.Primary.Sound)
    if SERVER then

        local shot = ents.Create("jeremy_fireball")
        if (IsValid(shot)) then
            shot.Owner = self.Owner
            shot:SetPos(shoot_pos)
            shot:SetAngles(shoot_angle:Angle())
            shot:Activate()
            shot:Spawn()
        end
        self.Weapon:SendWeaponAnim( ACT_VM_PRIMARYATTACK )

        local phy = shot:GetPhysicsObject()
        if phy:IsValid() then
            phy:ApplyForceCenter((shoot_angle * 5999))
        end

        self.Weapon:SendWeaponAnim( ACT_VM_PRIMARYATTACK )
        self.Owner:SetAnimation( PLAYER_ATTACK1 )

        //self.Owner:ViewPunch(Angle( -self.Primary.Recoil, 0, 0 ))
        if !self.Owner:IsNPC() then
        if (self.Primary.TakeAmmoPerBullet) then
            self:TakePrimaryAmmo(self.Primary.NumShots)
        else
            self:TakePrimaryAmmo(1)
        end
        end
    end
    self:SetNextPrimaryFire( CurTime() + self.Primary.Delay )

    //if not self:CanPrimaryAttack() or self.Owner:WaterLevel() > 2 then return end
    -- If your gun have a problem or if you are under water, you'll not be able to fire

    self.Reloadaftershoot = CurTime() + self.Primary.Delay
            -- Set the reload after shoot to be not able to reload when firering

            //self.Weapon:SetNextSecondaryFire(CurTime() + self.Primary.Delay)
            -- Set next secondary fire after your fire delay

            //self.Weapon:SetNextPrimaryFire(CurTime() + self.Primary.Delay)
            -- Set next primary fire after your fire delay

            -- Emit the gun sound when you fire

            //self:RecoilPower()

            //self:TakePrimaryAmmo(1)
    -- Take 1 ammo in you clip

    if ((game.SinglePlayer() and SERVER) or CLIENT) then
        //self.Weapon:SetNWFloat("LastShootTime", CurTime())
    end
end

function SWEP:Deploy()
    self.Weapon:EmitSound(Sound("weapons/mario_power/mario_powerup.mp3"))
end






function SWEP:SecondaryAttack( soundfile )
    self:Fly()
end
-- FLY
SWEP.flyammo = 5
SWEP.nextFly = 0
function SWEP:Fly( soundfile )
    if self.nextFly > CurTime() or CLIENT then return end
    self.nextFly = CurTime() + 0.30
    if self.flyammo < 1 then return end

    if not soundfile then
        if SERVER then self.Owner:EmitSound(Sound("weapons/ls/lightsaber_swing.mp3")) end
        if SERVER then self.Owner:EmitSound(Sound("player/suit_sprint.wav")) end
    else
        if SERVER then self.Owner:EmitSound(Sound(soundfile)) end
    end
    self.flyammo = self.flyammo - 1

    local power = 400
    if math.random(1,5) < 2 then power = 100
    elseif math.random(1,15) < 2 then power = -500
    elseif math.random(1,15) < 2 then power = 2500
    else power = math.random( 350,450 ) end

    if SERVER then self.Owner:SetVelocity(self.Owner:GetForward() * power + Vector(0, 0, power)) end
    timer.Simple(20, function() if IsValid(self) then self.flyammo = self.flyammo + 1 end end)
    --timer.Simple(0.2,self.SecondaryAttackDelay,self)
end


