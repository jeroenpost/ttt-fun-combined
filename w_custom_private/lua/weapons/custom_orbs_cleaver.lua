
if( CLIENT ) then
	
	
	SWEP.SlotPos = 1
	SWEP.DrawAmmo = false
	SWEP.DrawCrosshair = false
end

SWEP.CanDecapitate= true
SWEP.PrintName = "Michael Myers' Axe"
SWEP.Slot = 5
SWEP.Kind = WEAPON_UNARMED
SWEP.Icon = "vgui/entities/weapon_mor_daedric_shortsword"

SWEP.Category = "Morrowind Shortswords"
SWEP.Author			= "Doug Tyrrell + Mad Cow (Revisions by Neotanks) for LUA (Models, Textures, ect. By: Hellsing/JJSteel)"
SWEP.Base			= "weapon_mor_base_shortsword"
SWEP.Instructions	= "Left click to stab/slash/lunge and right click to throw, also capable of badassery."
SWEP.Contact		= ""

SWEP.ViewModelFOV	= 65
SWEP.ViewModelFlip	= false

SWEP.Spawnable			= true
SWEP.AdminSpawnable		= true
SWEP.UseHands = true
  
SWEP.ViewModel      = "models/weapons/v_crowbar.mdl"
SWEP.WorldModel   ="models/props/cs_militia/axe.mdl"

SWEP.VElements = {
    ["element_name"] = { type = "Model", model = "models/props/cs_militia/axe.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(3.596, 1.857, -4.676), angle = Angle(1.169, -8.183, 94.662), size = Vector(0.95, 0.95, 0.95), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
}
SWEP.WElements = {
   -- ["element_name2"] = { type = "Model", model = "models/props/cs_militia/axe.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(3.635, 1.557, -5.715), angle = Angle(-1.17, 10.519, 87.662), size = Vector(1.34, 1.34, 1.34), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
}

SWEP.Primary.Damage		= 51
SWEP.Primary.NumShots		= 0
SWEP.Primary.Delay 		= .4

SWEP.Primary.ClipSize		= -1					// Size of a clip
SWEP.Primary.DefaultClip	= -1					// Default number of bullets in a clip
SWEP.Primary.Automatic		= true				// Automatic/Semi Auto
SWEP.Primary.Ammo			= "none"

SWEP.Secondary.ClipSize		= -1					// Size of a clip
SWEP.Secondary.DefaultClip	= -1					// Default number of bullets in a clip
SWEP.Secondary.Automatic	= false				// Automatic/Semi Auto
SWEP.Secondary.Ammo		= "none"

function SWEP:PreDrop()
    local wep = ents.Create(self:GetClass())
    if IsValid(self.Owner) then
        wep:SetPos(self.Owner:GetPos()+Vector(80,0,0))
        wep:SetAngles(self.Owner:GetAngles())
    else
        wep:SetPos(self:GetPos()+Vector(0,80,0))
        wep:SetAngles(self:GetAngles())
    end

    wep.IsDropped = true
    self:Remove(self)
    wep:Spawn()
end

SWEP.Offset = {
    Pos = {
        Up = 5,
        Right = 0,
        Forward = 0,
    },
    Ang = {
        Up = 0,
        Right = 0,
        Forward = -90,
    }
}


function SWEP:DrawWorldModel( )
    local hand, offset, rotate

    if not IsValid( self.Owner ) then
        self:DrawModel( )
        return
    end

    if not self.Hand then
        self.Hand = self.Owner:LookupAttachment( "anim_attachment_rh" )
    end

    hand = self.Owner:GetAttachment( self.Hand )

    if not hand then
        self:DrawModel( )
        return
    end

    offset = hand.Ang:Right( ) * self.Offset.Pos.Right + hand.Ang:Forward( ) * self.Offset.Pos.Forward + hand.Ang:Up( ) * self.Offset.Pos.Up

    hand.Ang:RotateAroundAxis( hand.Ang:Right( ), self.Offset.Ang.Right )
    hand.Ang:RotateAroundAxis( hand.Ang:Forward( ), self.Offset.Ang.Forward )
    hand.Ang:RotateAroundAxis( hand.Ang:Up( ), self.Offset.Ang.Up )

    self:SetRenderOrigin( hand.Pos + offset )
    self:SetRenderAngles( hand.Ang )

    self:DrawModel( )
end

function SWEP:Initialize()
    self:SetHoldType("melee2")
    // other initialize code goes here
    self:SetNWInt("NextThinky",0)
    self.Hit = {
        Sound( "weapons/shortsword/morrowind_shortsword_hitwall1.mp3" )}
    self.FleshHit = {
        Sound("weapons/shortsword/morrowind_shortsword_hit.mp3") }

    if CLIENT then

        if !IsValid(self.Owner) then return end

        // Create a new table for every weapon instance
        self.VElements = table.FullCopy( self.VElements )
        self.WElements = table.FullCopy( self.WElements )
        self.ViewModelBoneMods = table.FullCopy( self.ViewModelBoneMods )

        self:CreateModels(self.VElements) // create viewmodels
        self:CreateModels(self.WElements) // create worldmodels

end



end

function SWEP:CamoPreDrawViewModel()
    if  not IsValid(self.Owner) or not IsValid(self.Owner:GetViewModel()) then return end
    self.Owner:GetViewModel():SetColor( Color(255,255,255,1) )
    self.Owner:GetViewModel():SetMaterial( "engine/occlusionproxy"  )
end

function SWEP:PreDrawViewModel()
    self:CamoPreDrawViewModel()
end
