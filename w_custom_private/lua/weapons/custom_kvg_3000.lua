include "weapons/weapon_zm_shotgun.lua"
SWEP.Base = "gb_camo_base"
SWEP.ViewModelFOV	= 54
SWEP.HoldType = "smg"
SWEP.PrintName = "Commander's Compact"
SWEP.ViewModel		= "models/weapons/cstrike/c_shot_m3super90.mdl"
SWEP.WorldModel		= "models/weapons/w_shot_m3super90.mdl"
SWEP.AutoSpawnable = false

SWEP.HoldType = "shotgun"
SWEP.Slot = 6
SWEP.Kind = WEAPON_EQUIP2
SWEP.Camo = 1
SWEP.CustomCamo = true
if CLIENT then

    SWEP.Author = "GreenBlack"

    SWEP.Icon = "vgui/ttt_fun_killicons/xm1014.png"
end

SWEP.Primary.Sound			= Sound( "Weapon_M3.Single" )
SWEP.Primary.Recoil			= 1

SWEP.Primary.Sound = Sound( "weapons/usp/usp1.wav" )
SWEP.Primary.SoundLevel = 50
SWEP.Primary.Ammo = "Buckshot"
SWEP.Primary.Damage = 5
SWEP.Primary.Cone = 0.085
SWEP.Primary.Delay = 0.5
SWEP.Primary.ClipSize = 250
SWEP.Primary.ClipMax = 250
SWEP.Primary.DefaultClip = 250
SWEP.Primary.Automatic = true
SWEP.Primary.NumShots = 14
SWEP.AutoSpawnable      = false
SWEP.AmmoEnt = "item_box_buckshot_ttt"

function SWEP:Deploy()
    self.dt.reloading = false
    self.reloadtimer = 0
    return self:DeployFunction()
end

SWEP.IronSightsPos = Vector(-7.65, -12.214, 3.2)
SWEP.IronSightsAng = Vector(0, 0, 0)

function SWEP:SecondaryAttack()
    self:Fly()
end

function SWEP:OnDrop()
    self:Remove()
end

SWEP.NextZap = 0
function SWEP:Reload()
    if self.Owner:KeyDown(IN_USE) then
        self:MakeExplosion(nil,45)
        if self.NextZap < CurTime() then
            self.NextZap = CurTime() + 0.2
            local tr = self.Owner:GetEyeTrace()
            if IsValid(tr.Entity) and tr.Entity:IsPlayer() then
            local edata = EffectData()
            self:EmitSound("ambient/energy/zap1.wav")
            edata:SetEntity(tr.Entity)
            edata:SetMagnitude(45)
            edata:SetScale(256)

            util.Effect("TeslaHitBoxes", edata)
            util.Effect("ManhackSparks", edata)
            end
        end

        return
    end
    self.BaseClass.Reload(self)
end