
if SERVER then
   AddCSLuaFile( )
end

SWEP.HoldType           = "ar2"
   SWEP.PrintName          = "Duckinator"

   SWEP.Slot               = 0

if CLIENT then

   SWEP.SlotPos = 10


		SWEP.Icon = "vgui/ttt_fun_killicons/awp.png"
	
   
end

SWEP.Kind = WEAPON_MELEE
--SWEP.CanBuy = {ROLE_TRAITOR} -- only traitors can buy
SWEP.LimitedStock = false -- only buyable once


SWEP.Base               = "gb_camo_base"
SWEP.Spawnable = true
SWEP.AdminSpawnable = true
SWEP.Camo = 1
SWEP.CustomCamo = true
SWEP.ViewModelFlip = true
SWEP.IsSilent = true
SWEP.Primary.Delay          = 0.045
SWEP.Primary.Recoil         = 0.3
SWEP.Primary.Automatic = true
SWEP.Primary.Ammo = "357"
SWEP.Primary.Damage = 5
SWEP.Primary.NumShots = 0
SWEP.Primary.Cone = 0.04
SWEP.Primary.ClipSize = 10000000
SWEP.Primary.ClipMax = 100000 -- keep mirrored to ammo
SWEP.Primary.DefaultClip = 1000000
SWEP.HeadshotMultiplier = 1.5
SWEP.AutoSpawnable      = false
SWEP.AmmoEnt = "item_ammo_357_ttt"
SWEP.ViewModel  = "models/weapons/cstrike/c_snip_awp.mdl"
SWEP.WorldModel = "models/weapons/w_snip_awp.mdl"
SWEP.Primary.Sound = Sound ("Weapon_M4A1.Silenced")
SWEP.UseHands = true
SWEP.ViewModelFlip = false

SWEP.Secondary.Sound = Sound("Default.Zoom")
SWEP.IronSightsPos      = Vector( 5, -15, -2 )
SWEP.IronSightsAng      = Vector( 2.6, 1.37, 3.5 )

function SWEP:Deploy()
  local function playerDiesSound( victim, weapon, killer )


        if( IsValid(killer) and killer:IsPlayer()  ) then

            local wep = killer:GetActiveWeapon()
            if IsValid(wep) then
                wep = wep:GetClass()
                if wep == "custom_duckinator" then

                  --  gb_PlaySoundFromServer(gb_config.websounds.."ducky_goodbye.mp3", victim)
                    gb_PlaySoundFromServer(gb_config.websounds.."ducky_goodbye.mp3", killer)
                end
            end

        end

    end

    hook.Add( "PlayerDeath", "playerDeathSound_duck", playerDiesSound )

end

function SWEP:PrimaryAttack(worldsnd)
    if self.Owner:KeyDown(IN_USE) then
        self:DropDucky()
        return
    end

    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
    self.Weapon:SetNextSecondaryFire( CurTime() + 0.1)
    self:Disorientate()

    if not worldsnd then
        self.Weapon:EmitSound( self.Primary.Sound, self.Primary.SoundLevel )
    elseif SERVER then
        WorldSound(self.Primary.Sound, self:GetPos(), self.Primary.SoundLevel)
    end

    if self:GetIronsights() then
        self:ShootBullet( 85, 2, 1, 0.001 )
        self.Weapon:SetNextPrimaryFire( CurTime() + 1.25 )
        self.Weapon:SetNextSecondaryFire( CurTime() + 0.4 )
    else
        self:ShootBullet( self.Primary.Damage, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )
    end

    if ( self.Owner:IsValid() ) then
        local tr = self.Owner:GetEyeTrace()
        local effectdata = EffectData()
        effectdata:SetOrigin(tr.HitPos)
        effectdata:SetNormal(tr.HitNormal)
        effectdata:SetScale(1)
        -- util.Effect("effect_mad_ignition", effectdata)
        util.Decal("FadingScorch", tr.HitPos + tr.HitNormal, tr.HitPos - tr.HitNormal)

        local tracedata = {}
        tracedata.start = tr.HitPos
        tracedata.endpos = Vector(tr.HitPos.x, tr.HitPos.y, tr.HitPos.z - 10)
        tracedata.filter = tr.HitPos
        local tracedata = util.TraceLine(tracedata)
        if SERVER and math.random(1,5) == 1 then
            if tracedata.HitWorld  then
                local flame = ents.Create("env_fire");
                flame:SetPos(tr.HitPos + Vector( 0, 0, 1 ));
                flame:SetKeyValue("firesize", "10");
                flame:SetKeyValue("fireattack", "10");
                flame:SetKeyValue("StartDisabled", "0");
                flame:SetKeyValue("health", "10");
                flame:SetKeyValue("firetype", "0");
                flame:SetKeyValue("damagescale", "5");
                flame:SetKeyValue("spawnflags", "128");
                flame:SetPhysicsAttacker(self.Owner)
                flame:SetOwner( self.Owner )
                flame:Spawn();
                flame:Fire("StartFire",0);

            end
        end

        local owner = self.Owner
        --if not ValidEntity(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

        owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )
    end
end

function SWEP:Reload()
    self.Weapon:DefaultReload( ACT_VM_RELOAD );
    self:SetIronsights( false )
    self:SetZoom(false)
end

function SWEP:DropDucky()
    if self.NextHealthDrop > CurTime() then return end
    self.NextHealthDrop = CurTime() + 5

    local healttt = 200

    if self.healththing and SERVER then

        healttt = Entity(self.healththing):GetStoredHealth()
        if self.Owner:IsTraitor()  then
            if  not self.hadhealthexplosion then
                Entity(self.healththing):ExplodeHard()
                self.hadhealthexplosion =true
            else
                Entity(self.healththing):Remove()
            end
        else
            local soundfile = gb_config.websounds.."duck_song.mp3"
            gb_PlaySoundFromServer(soundfile, self)
            local ent2 = self.healththing
            timer.Simple(18, function()
                if not  self.Owner then return end

                local ply = self.Owner
            local ent = ents.Create("cse_ent_shorthealthgrenade")
            ent:SetOwner(ply)
            ent.Owner = ply
            ent:SetPos(Entity(ent2):GetPos())
            ent:SetAngles(Angle(1,0,0))
            ent:Spawn()
            ent.timer = CurTime() + 2
            Entity(ent2):Remove()
                end)
        end
    end
    if not self.healththing and SERVER then
        gb_PlaySoundFromServer(gb_config.websounds.."ducky.mp3", self.Owner)
        self:HealthDrop( healttt )
    else
        self:HealthDrop( healttt )
    end
end


function SWEP:Reload()
    if self.Owner:KeyDown(IN_USE)  then
          self:DropDucky()
          return
    end

    self:ShootFlare()
    self:Disorientate()


end

function SWEP:SetZoom(state)
    if CLIENT then 
       return
    else
       if state then
          self.Owner:SetFOV(20, 0.3)
       else
          self.Owner:SetFOV(0, 0.2)
       end
    end
end

function SWEP:OnDrop()
    self:Remove()
end

-- Add some zoom to ironsights for this gun
function SWEP:SecondaryAttack()
    if self.Owner:KeyDown(IN_USE) then


        if not self.IronSightsPos then return end
        if self.Weapon:GetNextSecondaryFire() > CurTime() then return end

        bIronsights = not self:GetIronsights()

        self:SetIronsights( bIronsights )

        if SERVER then
            self:SetZoom(bIronsights)
         else
            self:EmitSound(self.Secondary.Sound)
        end

        self.Weapon:SetNextSecondaryFire( CurTime() + 0.1)
        self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
        return
    end
    if not self.nextsec or self.nextsec < CurTime() then
    self.nextsec = ( CurTime() + 0.9)
    sound.Play("weapons/shotgun/shotgun_fire6.wav", self:GetPos(), self.Primary.SoundLevel)
    self:ShootBullet( 8, 3, 20, 0.285 )
    self.Weapon:SetNextSecondaryFire( CurTime() + 0.1)
        end
end

function SWEP:PreDrop()
    self:SetZoom(false)
    self:SetIronsights(false)
    return self.BaseClass.PreDrop(self)
end



function SWEP:Holster()
    self:SetIronsights(false)
    self:SetZoom(false)
    return true
end

if CLIENT then
   local scope = surface.GetTextureID("sprites/scope")
   function SWEP:DrawHUD()
      if self:GetIronsights() then
         surface.SetDrawColor( 0, 0, 0, 255 )
         
         local x = ScrW() / 2.0
         local y = ScrH() / 2.0
         local scope_size = ScrH()

         -- crosshair
         local gap = 80
         local length = scope_size
         surface.DrawLine( x - length, y, x - gap, y )
         surface.DrawLine( x + length, y, x + gap, y )
         surface.DrawLine( x, y - length, x, y - gap )
         surface.DrawLine( x, y + length, x, y + gap )

         gap = 0
         length = 50
         surface.DrawLine( x - length, y, x - gap, y )
         surface.DrawLine( x + length, y, x + gap, y )
         surface.DrawLine( x, y - length, x, y - gap )
         surface.DrawLine( x, y + length, x, y + gap )


         -- cover edges
         local sh = scope_size / 2
         local w = (x - sh) + 2
         surface.DrawRect(0, 0, w, scope_size)
         surface.DrawRect(x + sh - 2, 0, w, scope_size)

         surface.SetDrawColor(255, 0, 0, 255)
         surface.DrawLine(x, y, x + 1, y + 1)

         -- scope
         surface.SetTexture(scope)
         surface.SetDrawColor(255, 255, 255, 255)

         surface.DrawTexturedRectRotated(x, y, scope_size, scope_size, 0)

      else
         return self.BaseClass.DrawHUD(self)
      end
   end

   function SWEP:AdjustMouseSensitivity()
      return (self:GetIronsights() and 0.2) or nil
   end
end

SWEP.NextHealthDrop = 0


local throwsound = Sound( "Weapon_SLAM.SatchelThrow" )

-- ye olde droppe code
function SWEP:HealthDrop( healttt)
    if SERVER then
        local ply = self.Owner
        if not IsValid(ply) then return end

        --if self.Planted then return end

        local vsrc = ply:GetShootPos()
        local vang = ply:GetAimVector()
        local vvel = ply:GetVelocity()

        local vthrow = vvel + vang * 200

        local health = ents.Create("duckinator_health")
        if IsValid(health) then
            health:SetPos(vsrc + vang * 10)
            health:Spawn()
            health:SetModel("models/props/cs_office/computer_caseb.mdl")
            health:SetPlacer(ply)
            health.healsound = "ducky.mp3"

            health:PhysWake()
            local phys = health:GetPhysicsObject()
            if IsValid(phys) then
                phys:SetVelocity(vthrow)
            end
            health:SetModel("models/props/cs_office/computer_caseb.mdl")
            health:SetStoredHealth(healttt)
            health.setOwner = self.Owner

            self.healththing = health:EntIndex()

            self.Planted = true
        end
    end

    self.Weapon:EmitSound(throwsound)
end