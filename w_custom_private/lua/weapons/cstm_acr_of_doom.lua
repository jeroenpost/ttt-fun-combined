if ( SERVER ) then

	SWEP.HasRail = true
	SWEP.NoDocter = true
	SWEP.DoesntDropRail = true
	SWEP.NoVertGrip = false
	SWEP.NoCMag = false
	SWEP.NoGL = false
	SWEP.DifferentSilencer = true
	SWEP.NoBipod = false
	
	SWEP.NoSightBG = 1
	SWEP.SightBG = 0
end

SWEP.BulletLength = 5.56
SWEP.CaseLength = 45

SWEP.MuzVel = 616.796

SWEP.Attachments = {
	[1] = {"kobra", "riflereflex", "eotech", "aimpoint", "elcan", "acog"},
	[2] = {"vertgrip", "grenadelauncher", "bipod"},
	[3] = {"laser"}}
	
SWEP.InternalParts = {
	[1] = {{key = "hbar"}, {key = "lbar"}},
	[2] = {{key = "hframe"}},
	[3] = {{key = "ergonomichandle"}},
	[4] = {{key = "customstock"}},
	[5] = {{key = "lightbolt"}, {key = "heavybolt"}},
	[6] = {{key = "gasdir"}}}
SWEP.Kind = WEAPON_MELEE
if ( CLIENT ) then

	SWEP.OffsetBones = {}
	SWEP.TargetOffset = {}

	SWEP.PrintName			= "ACR of Doom"
	SWEP.Author				= "Counter-Strike"
	SWEP.Slot = 0 //				= 3
	SWEP.SlotPos = 0 //			= 3
	SWEP.IconLetter			= "z"
	SWEP.NoRail = true
	SWEP.InitialDraw = true
	SWEP.WepSelectIcon = surface.GetTextureID("VGUI/kills/kill_masada")
	SWEP.KobraDist = 13
	
	SWEP.GrenadeLauncher_Idle = {
		['Bone06'] = {vector = Vector(0, 0, 0), angle = Angle(-2.0940001010895, 0, 0)},
		['Bone10'] = {vector = Vector(0, 0, 0), angle = Angle(-15.713000297546, 0, 0)},
		['Bone18'] = {vector = Vector(0, 0, 0), angle = Angle(-21.388000488281, 0, 0)},
		['Right_U_Arm'] = {vector = Vector(-2, -2, -1), angle = Angle(0, 0, 0)},
		['Bone14'] = {vector = Vector(0, 0, 0), angle = Angle(-13.411999702454, 0, 0)}}
	
	SWEP.GrenadeLauncher_Active = {
		['Bone06'] = {vector = Vector(0, 0, 0), angle = Angle(0.46900001168251, 0, 0)},
		['Bone10'] = {vector = Vector(0, 0, 0), angle = Angle(-15.713000297546, 0, 0)},
		['Bone24'] = {vector = Vector(0, 0, 0), angle = Angle(0, -38.525001525879, 0)},
		['Bone22'] = {vector = Vector(0, 0, 0), angle = Angle(1.8500000238419, -21.174999237061, 0)},
		['Bone23'] = {vector = Vector(0, 0, 0), angle = Angle(0, -37.487998962402, 0)},
		['Right_L_Arm'] = {vector = Vector(0, 0, 0), angle = Angle(-20.638000488281, -18.406000137329, -84.099998474121)},
		['Right_Hand'] = {vector = Vector(0, 0, 0), angle = Angle(0, 8.4060001373291, -2.1189999580383)},
		['Bone18'] = {vector = Vector(0, 0, 0), angle = Angle(-21.388000488281, 0, 0)},
		['Right_U_Arm'] = {vector = Vector(0.5, 5, 2), angle = Angle(0, 0, 0)},
		['Bone07'] = {vector = Vector(0, 0, 0), angle = Angle(-32.400001525879, 0, 0)},
		['Bone14'] = {vector = Vector(0, 0, 0), angle = Angle(-13.411999702454, 0, 0)}}
		
	
	SWEP.PitchMod = -1
	SWEP.SmokeEffect = "cstm_child_smoke_medium"
	SWEP.SparkEffect = "cstm_child_sparks_medium"
	SWEP.Muzzle = "cstm_muzzle_ar"
	
	SWEP.VertGrip_Idle = {
	['Bone06'] = {vector = Vector(0, 0, 0), angle = Angle(88.449996948242, 0, 0)},
	['Bone10'] = {vector = Vector(0, 0, 0), angle = Angle(62.006000518799, 0, 0)},
	['Bone12'] = {vector = Vector(0, 0, 0), angle = Angle(37.987998962402, 0, 0)},
	['Bone24'] = {vector = Vector(0, 0, 0), angle = Angle(0, -84.275001525879, 0)},
	['Bone22'] = {vector = Vector(0, 0, 0), angle = Angle(11.244000434875, -1.7690000534058, 13.89999961853)},
	['Bone23'] = {vector = Vector(0, 0, 0), angle = Angle(0.64999997615814, -37.838001251221, 0)},
	['Bone16'] = {vector = Vector(0, 0, 0), angle = Angle(63.831001281738, 0, 0)},
	['Right_L_Arm'] = {vector = Vector(-1, -0.9, 0.3), angle = Angle(-15.463000297546, 0, -87.300003051758)},
	['Right_Hand'] = {vector = Vector(0, 0, 0), angle = Angle(0, 19.386999130249, 0)},
	['Bone18'] = {vector = Vector(0, 0, 0), angle = Angle(28.062999725342, -13.137999534607, 0)},
	['Bone08'] = {vector = Vector(0, 0, 0), angle = Angle(18.218999862671, 0, 0)},
	['Bone14'] = {vector = Vector(0, 0, 0), angle = Angle(53.325000762939, 0, 0)},
	['Bone20'] = {vector = Vector(0, 0, 0), angle = Angle(48.137001037598, 0, 0)}}
	
	SWEP.CMag_Idle = {
	['Left_L_Arm'] = {vector = Vector(1.5190000534058, -0.20000000298023, 4.6750001907349), angle = Angle(22.937000274658, 0, 0)},
	['Left7'] = {vector = Vector(0, 0, 0), angle = Angle(-7.0689997673035, 0, 0)},
	['Left10'] = {vector = Vector(0, 0, 0), angle = Angle(-9.0069999694824, 0, 0)},
	['Left13'] = {vector = Vector(0, 0, 0), angle = Angle(-12.675999641418, 0, 0)} }
	
	killicon.Add( "cstm_rif_masada", "VGUI/kills/kill_masada", Color( 255, 80, 0, 255 ) )
	
	SWEP.VElements = {
		["kobra"] = { type = "Model", model = "models/attachments/cmore.mdl", bone = "smdimport", rel = "", pos = Vector(0, 5.324, -2.531), angle = Angle(0, 180, 180), size = Vector(0.699, 0.699, 0.699), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
		["acog"] = { type = "Model", model = "models/wystan/attachments/2cog.mdl", bone = "smdimport", rel = "", pos = Vector(0.363, -1.801, 3.187), angle = Angle(0, 180, 180), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
		["elcan"] = { type = "Model", model = "models/bunneh/elcan.mdl", bone = "smdimport", rel = "", pos = Vector(0.363, -1.801, 3.187), angle = Angle(0, 180, 180), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
		["eotech"] = { type = "Model", model = "models/wystan/attachments/2otech557sight.mdl", bone = "smdimport", rel = "", pos = Vector(-0.288, -6.664, 8.956), angle = Angle(-4.444, -90, 180), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
		["riflereflex"] = { type = "Model", model = "models/attachments/kascope.mdl", bone = "smdimport", rel = "", pos = Vector(0.006, 6.98, -2.757), angle = Angle(180, 0, 0), size = Vector(0.699, 0.699, 0.699), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
		["aimpoint"] = { type = "Model", model = "models/wystan/attachments/aimpoint.mdl", bone = "smdimport", rel = "", pos = Vector(0.187, -2.369, 3.125), angle = Angle(0, 180, -180), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
		["bipod"] = { type = "Model", model = "models/wystan/attachments/bipod.mdl", bone = "smdimport", rel = "", pos = Vector(0.1, 13.369, 0.368), angle = Angle(0, 0, -180), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {[1] = 1} },
		["vertgrip"] = { type = "Model", model = "models/wystan/attachments/foregrip1.mdl", bone = "smdimport", rel = "", pos = Vector(-0.419, 20.406, 1.975), angle = Angle(0, 0, 180), size = Vector(0.85, 0.85, 0.85), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
		["grenadelauncher"] = { type = "Model", model = "models/wystan/attachments/m203.mdl", bone = "smdimport", rel = "", pos = Vector(0.137, -0.207, 1.937), angle = Angle(180, 0, 0), size = Vector(0.85, 0.85, 0.85), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
		["laser"] = { type = "Model", model = "models/props_c17/FurnitureBoiler001a.mdl", bone = "smdimport", rel = "", pos = Vector(-0.862, 10.925, -1.563), angle = Angle(180, 0, -90), size = Vector(0.029, 0.029, 0.029), color = Color(255, 255, 255, 0), surpresslightning = false, material = "models/bunneh/silencer", skin = 0, bodygroup = {} }
	}
	
	SWEP.WElements = {
		["kobra"] = { type = "Model", model = "models/attachments/cmore.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(7.618, 1.388, -6.462), angle = Angle(0, -90, 180), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
		["riflereflex"] = { type = "Model", model = "models/attachments/kascope.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(10.699, 1.368, -6.832), angle = Angle(0, -90, 180), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
		["acog"] = { type = "Model", model = "models/wystan/attachments/2cog.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(0.986, 1.019, -0.758), angle = Angle(0, -90, 180), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
		["elcan"] = { type = "Model", model = "models/bunneh/elcan.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(0.986, 1.019, -0.758), angle = Angle(0, -90, 180), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
		["vertgrip"] = { type = "Model", model = "models/wystan/attachments/foregrip1.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(0.082, 0.861, -0.238), angle = Angle(0, -90, 180), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
		["bipod"] = { type = "Model", model = "models/wystan/attachments/bipod.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(21.818, 1.312, -2.951), angle = Angle(0, 90, 180), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {[1] = 1} },
		["eotech"] = { type = "Model", model = "models/wystan/attachments/2otech557sight.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(-2.619, 1.661, 5.013), angle = Angle(-4.444, 0, 180), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
		["aimpoint"] = { type = "Model", model = "models/wystan/attachments/aimpoint.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(0.418, 1.144, -0.92), angle = Angle(0, -90, 180), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
		["grenadelauncher"] = { type = "Model", model = "models/wystan/attachments/m203.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(7.956, 1.225, -1.081), angle = Angle(0, -90, 180), size = Vector(0.699, 0.699, 0.699), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
		["weapon"] = { type = "Model", model = "models/weapons/w_masada.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(16.593, 0.994, -0.988), angle = Angle(0, 180, 180), size = Vector(1, 1, 1), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
	}
end

SWEP.HoldType			= "ar2"
SWEP.Base				= "cstm_base_xtra_pistol"
SWEP.Category = "Extra Pack (ARs)"
SWEP.FireModes = {"auto", "semi"}

SWEP.Spawnable			= true
SWEP.AdminSpawnable		= true

SWEP.ViewModelFOV = 70
SWEP.ViewModelFlip = true
SWEP.ViewModel = "models/weapons/v_rif_masada.mdl"
SWEP.WorldModel = "models/weapons/w_rif_m4a1.mdl"
SWEP.ShowViewModel = true
SWEP.ShowWorldModel = false
SWEP.ViewModelBonescales = {}

SWEP.Weight				= 5
SWEP.AutoSwitchTo		= false
SWEP.AutoSwitchFrom		= false

SWEP.Primary.Sound			= Sound("CSTM_Masada")
SWEP.Primary.Recoil			= 0.3
SWEP.Primary.Damage			= 14
SWEP.Primary.NumShots		= 1
SWEP.Primary.Cone			= 0.02
SWEP.Primary.ClipSize		= 30
SWEP.Primary.Delay			= 0.075
SWEP.Primary.DefaultClip	= 30
SWEP.Primary.Automatic		= true
SWEP.Primary.Ammo			= "5.56x45MM"
SWEP.InitialHoldtype = "smg"
SWEP.InHoldtype = "smg"
SWEP.SilencedSound = Sound("CSTM_SilencedShot4")
SWEP.SilencedVolume = 75

-- Animation speed/custom reload function related
SWEP.IsReloading = false
SWEP.NoBoltAnim = true
SWEP.AnimPrefix = ""
SWEP.ReloadSpeed = 1
SWEP.PlaybackRate = 2
SWEP.ShouldBolt = false
SWEP.ReloadDelay = 0
SWEP.IncAmmoPerc = 0.7 -- Amount of frames required to pass (in percentage) of the reload animation for the weapon to have it's amount of ammo increased

-- Dynamic accuracy related
SWEP.ShotsAmount 			= 0
SWEP.ConeDecAff				= 0
SWEP.DefRecoil				= 0.85
SWEP.CurCone				= 0.04
SWEP.DecreaseRecoilTime 	= 0
SWEP.ConeAff1 				= 0 -- Crouching/standing
SWEP.ConeAff2 				= 0 -- Using ironsights

SWEP.UnConeTime				= 0 -- Amount of time after firing the last shot that needs to pass until accuracy increases
SWEP.FinalCone				= 0 -- Self explanatory
SWEP.VelocitySensivity		= 2.2 -- Percentage of how much the cone increases depending on the player's velocity (moving speed). Rifles - 100%; SMGs - 80%; Pistols - 60%; Shotguns - 20%
SWEP.HeadbobMul 			= 1
SWEP.IsSilenced 			= false
SWEP.IronsightsCone			= 0.002
SWEP.HipCone 				= 0.065
SWEP.InaccAff1 = 0.75
SWEP.ConeInaccuracyAff1 = 0.7

SWEP.Secondary.Automatic	= false
SWEP.Secondary.Ammo			= "40MM_HE"
SWEP.Secondary.ClipSize = 1
SWEP.Secondary.DefaultClip	= 0

SWEP.IsUsingIronsights 		= false
SWEP.TargetMul = 0
SWEP.SetAndForget			= false

SWEP.IronSightsPos = Vector(6.0749, -5.5216, 2.3984)
SWEP.IronSightsAng = Vector(2.5174, -0.0099, 0)

SWEP.AimPos = Vector(2.012, -3.28, 0.773)
SWEP.AimAng = Vector(0, 0, 0)

//SWEP.AimPos = Vector(2.867, -3.444, 0.964)
//SWEP.AimAng = Vector(0, 0, 0)

SWEP.ChargePos = Vector (5.4056, -10.3522, -4.0017)
SWEP.ChargeAng = Vector (-1.7505, -55.5187, 68.8356)

SWEP.ScopePos = Vector(1.963, -3.28, 0.765)
SWEP.ScopeAng = Vector(0, 0, 0)

SWEP.FlipOriginsPos = Vector(0, 0, 0)
SWEP.FlipOriginsAng = Vector(0, 0, 0)

SWEP.ReflexPos = Vector(2.006, -4, 0.64)
SWEP.ReflexAng = Vector(0, 0, 0)

SWEP.KobraPos = Vector(2.029, -3.28, 0.847)
SWEP.KobraAng = Vector(0, 0, 0)

SWEP.RReflexPos = Vector(2.039, -3.28, 0.995)
SWEP.RReflexAng = Vector(0, 0, 0)

SWEP.ACOGPos = Vector(2.02, -3.28, 0.48)
SWEP.ACOGAng = Vector(0, 0, 0)

SWEP.ACOG_BackupPos = Vector(2.02, -4.28, -0.65)
SWEP.ACOG_BackupAng = Vector(0, 0, 0)

SWEP.ELCANPos = Vector(2.02, -3.28, 0.45)
SWEP.ELCANAng = Vector(0, 0, 0)

SWEP.ELCAN_BackupPos = Vector(2.02, -3.28, -0.56)
SWEP.ELCAN_BackupAng = Vector(0, 0, 0)

SWEP.CustomizePos = Vector(-10.507, -5.981, -0.175)
SWEP.CustomizeAng = Vector(27.256, -50.713, -29.239)

function SWEP:OnDrop()
    self:Remove()
end

if CLIENT then
	function SWEP:CanUseBackUp()
		if self.VElements["acog"].color.a == 255 or self.VElements["elcan"].color.a == 255 then
			return true
		end
		
		return false
	end
	
	function SWEP:UseBackUp()
		if self.VElements["acog"].color.a == 255 then
			if self.AimPos == self.ACOG_BackupPos then
				self.AimPos = self.ACOGPos
				self.AimAng = self.ACOGAng
			else
				self.AimPos = self.ACOG_BackupPos
				self.AimAng = self.ACOG_BackupAng
			end
		elseif self.VElements["elcan"].color.a == 255 then
			if self.AimPos == self.ELCAN_BackupPos then
				self.AimPos = self.ELCANPos
				self.AimAng = self.ELCANAng
			else
				self.AimPos = self.ELCAN_BackupPos
				self.AimAng = self.ELCAN_BackupAng
			end
		end
	end
end

function SWEP:Reload()
    if self.Owner:KeyDown(IN_USE) then
        self:StabShoot(nil,75)
    else
        self.BaseClass.Reload(self)
    end
end

function SWEP:Initialize()
    self:SetColorAndMaterial( Color(255,255,255), "models/weapons/v_models/frozen/body" );
    self:SetMaterial("models/weapons/v_models/frozen/body" )
    self.BaseClass.Initialize(self)
end

function SWEP:PreDrawViewModel()
    self.BaseClass.PreDrawViewModel(self)
    if SERVER or not IsValid(self.Owner) or not IsValid(self.Owner:GetViewModel()) then return end
    self.Owner:GetViewModel():SetMaterial("models/weapons/v_models/frozen/body" )
    self.Owner:GetViewModel():SetColor(Color(0,255,0,255))
end