SWEP.HoldType = "melee"
SWEP.ViewModelFOV = 45
SWEP.ViewModelFlip = false
SWEP.ViewModel = "models/weapons/v_stunstick.mdl"
SWEP.WorldModel = "models/weapons/W_stunbaton.mdl"
SWEP.ShowViewModel = true
SWEP.ShowWorldModel = true
SWEP.Kind = 12
SWEP.ViewModelBoneMods = {
    ["ValveBiped.Bip01"] = { scale = Vector(1, 1, 1), pos = Vector(0, 1.899, 2.986), angle = Angle(0, -4.888, -4.888) }
}
SWEP.VElements = {
    ["prong+"] = { type = "Model", model = "models/props_c17/utilityconnecter006d.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "base3", pos = Vector(-0.385, 0, -5.77), angle = Angle(-82.212, 37.212, -37.213), size = Vector(0.129, 0.129, 0.129), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
    ["bottom"] = { type = "Model", model = "models/props_junk/wood_spool01.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "Handle", pos = Vector(0, 0, 5.679), angle = Angle(0, 0, 0), size = Vector(0.039, 0.039, 0.039), color = Color(255, 255, 255, 255), surpresslightning = false, material = "Debug/env_cubemap_model_translucent_fountain", skin = 0, bodygroup = {} },
    ["Handle"] = { type = "Model", model = "models/props_c17/oildrum001.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(3.099, 1.5, -1.894), angle = Angle(-2.131, 95.858, -4.261), size = Vector(0.059, 0.059, 0.119), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
    ["prong++++"] = { type = "Model", model = "models/props_c17/utilityconnecter006d.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "base3", pos = Vector(-0.385, 0.384, -3.462), angle = Angle(-82.212, 0, 0), size = Vector(0.129, 0.129, 0.129), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
    ["part"] = { type = "Model", model = "models/healthvial.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "Handle", pos = Vector(0, 0, -5), angle = Angle(0, 0, 0), size = Vector(0.5, 0.5, 0.5), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
    ["bright"] = { type = "Model", model = "models/props_lab/lab_flourescentlight002b.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "battery+", pos = Vector(0, 2.273, 0.2), angle = Angle(0, 0, 0), size = Vector(0.094, 0.094, 0.094), color = Color(0, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
    ["battery"] = { type = "Model", model = "models/Items/car_battery01.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "part", pos = Vector(0.384, -1.155, 3.461), angle = Angle(0.865, -9.52, -90.865), size = Vector(0.082, 0.082, 0.082), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
    ["prong+++++"] = { type = "Model", model = "models/props_c17/utilityconnecter006d.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "base3", pos = Vector(-0.385, 0, -3.462), angle = Angle(-82.212, 37.212, -37.213), size = Vector(0.129, 0.129, 0.129), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
    ["base2"] = { type = "Model", model = "models/props_lab/labpart.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "part", pos = Vector(0.2, 0.3, -1.155), angle = Angle(-0.866, -132.405, 90.864), size = Vector(0.225, 0.225, 0.225), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
    ["base3"] = { type = "Model", model = "models/props_junk/PopCan01a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "base2", pos = Vector(0, 8.076, 0.15), angle = Angle(16.441, 180, 90), size = Vector(0.801, 0.819, 2.098), color = Color(255, 255, 255, 255), surpresslightning = false, material = "Debug/env_cubemap_model", skin = 0, bodygroup = {} },
    ["prong+++"] = { type = "Model", model = "models/props_c17/utilityconnecter006d.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "base3", pos = Vector(-0.385, 0, -1.155), angle = Angle(-82.212, 37.212, -37.213), size = Vector(0.129, 0.129, 0.129), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
    ["prong"] = { type = "Model", model = "models/props_c17/utilityconnecter006d.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "base3", pos = Vector(-0.385, 0.384, -5.77), angle = Angle(-82.212, 0, 0), size = Vector(0.129, 0.129, 0.129), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
    ["battery+"] = { type = "Model", model = "models/Items/car_battery01.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "part", pos = Vector(0.384, -1.155, -1.923), angle = Angle(0.865, -9.52, -90.865), size = Vector(0.082, 0.082, 0.082), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
    ["prong++"] = { type = "Model", model = "models/props_c17/utilityconnecter006d.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "base3", pos = Vector(-0.385, 0.384, -1.155), angle = Angle(-82.212, 0, 0), size = Vector(0.129, 0.129, 0.129), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
,
    ["cutter"] = { type = "Model", model = "models/rubyscythe.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(2.596, 0, 0), angle = Angle(180, -180, 0), size = Vector(1.149, 1.149, 1.149), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }


    ,["twilysceptre_hl2size"] = { type = "Model", model = "models/freeman/twilysceptre_hl2size.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "Handle", pos = Vector(-0, 0.384, -1.155), angle = Angle(180, 0, 0), size = Vector(1,1,1), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }

}



SWEP.WElements = {
    ["prong+"] = { type = "Model", model = "models/props_c17/utilityconnecter006d.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "base3", pos = Vector(0.455, 0, -5), angle = Angle(-91.024, 0, 0), size = Vector(0.129, 0.129, 0.129), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
    ["bottom"] = { type = "Model", model = "models/props_junk/wood_spool01.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "Handle", pos = Vector(0, 0, 5.908), angle = Angle(0, 0, 0), size = Vector(0.039, 0.039, 0.039), color = Color(255, 255, 255, 255), surpresslightning = false, material = "Debug/env_cubemap_model_translucent_fountain", skin = 0, bodygroup = {} },
    ["Handle"] = { type = "Model", model = "models/props_c17/oildrum001.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(2.7, 1.6, -1.364), angle = Angle(13.295, 11.25, -9.205), size = Vector(0.059, 0.059, 0.119), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
    ["prong++++"] = { type = "Model", model = "models/props_c17/utilityconnecter006d.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "base3", pos = Vector(0.455, 0, -2.274), angle = Angle(-91.024, 0, 0), size = Vector(0.129, 0.129, 0.129), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
    ["bright"] = { type = "Model", model = "models/props_lab/lab_flourescentlight002b.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "battery+", pos = Vector(0, 2.273, 0.2), angle = Angle(95.113, 0, 0), size = Vector(0.094, 0.094, 0.094), color = Color(0, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
    ["battery+"] = { type = "Model", model = "models/Items/car_battery01.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "part", pos = Vector(0.384, -1.155, -1.923), angle = Angle(0.865, -9.52, -90.865), size = Vector(0.082, 0.082, 0.082), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
    ["battery"] = { type = "Model", model = "models/Items/car_battery01.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "part", pos = Vector(0.384, -1.155, 3.461), angle = Angle(0.865, -9.52, -90.865), size = Vector(0.082, 0.082, 0.082), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
    ["prong+++++"] = { type = "Model", model = "models/props_c17/utilityconnecter006d.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "base3", pos = Vector(0.455, 0, 0.455), angle = Angle(-91.024, 0, 0), size = Vector(0.129, 0.129, 0.129), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
    ["base2"] = { type = "Model", model = "models/props_lab/labpart.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "part", pos = Vector(0.2, 0.3, -1.155), angle = Angle(-0.866, -132.405, 90.864), size = Vector(0.225, 0.225, 0.225), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
    ["base3"] = { type = "Model", model = "models/props_junk/PopCan01a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "base2", pos = Vector(0, 8.076, 0.15), angle = Angle(16.441, 180, 90), size = Vector(0.801, 0.819, 2.098), color = Color(255, 255, 255, 255), surpresslightning = false, material = "Debug/env_cubemap_model", skin = 0, bodygroup = {} },
    ["prong+++"] = { type = "Model", model = "models/props_c17/utilityconnecter006d.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "base3", pos = Vector(0, 0.2, 0.455), angle = Angle(-91.024, -93.069, 0), size = Vector(0.129, 0.129, 0.129), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
    ["prong"] = { type = "Model", model = "models/props_c17/utilityconnecter006d.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "base3", pos = Vector(0, 0.2, -5), angle = Angle(-91.024, -93.069, 0), size = Vector(0.129, 0.129, 0.129), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
    ["part"] = { type = "Model", model = "models/healthvial.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "Handle", pos = Vector(0, 0, -5), angle = Angle(0, 0, 0), size = Vector(0.5, 0.5, 0.5), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
    ["prong++"] = { type = "Model", model = "models/props_c17/utilityconnecter006d.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "base3", pos = Vector(0, 0.2, -2.274), angle = Angle(-91.024, -93.069, 0), size = Vector(0.129, 0.129, 0.129), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
    ,
    ["cutter"] = { type = "Model", model = "models/rubyscythe.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "Handle", pos = Vector(-0, 1,0), angle = Angle(180, 0, 0), size = Vector(2,2,2), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
,
["twilysceptre_hl2size"] = { type = "Model", model = "models/freeman/twilysceptre_tf2size.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "Handle", pos = Vector(-1.6, -0.9, -1.155), angle = Angle(180, 0, 0), size = Vector(1,1,1), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }

}

function SWEP:PreDrawViewModel(vm, weapon, ply)
    local mode = self:GetNWString("shootmode")

    if mode == "Bonk" then
        for k,v in pairs(self.VElements) do
            self.VElements[k].color = Color(255, 255, 255, 0)
        end
        self.VElements["cutter"].color = Color(255, 255, 255, 255)
    elseif mode == "Twilight" then
        for k,v in pairs(self.VElements) do
            self.VElements[k].color = Color(255, 255, 255, 0)
        end
        self.VElements["twilysceptre_hl2size"].color = Color(255, 255, 255, 255)
    else
        for k,v in pairs(self.VElements) do
            self.VElements[k].color = Color(255, 255, 255, 255)
        end
        self.VElements["twilysceptre_hl2size"].color = Color(255, 255, 255, 0)
        self.VElements["cutter"].color = Color(255, 255, 255, 0)
    end
    vm:SetMaterial("engine/occlusionproxy")
    return false
end
function SWEP:DrawWorldModel(vm, weapon, ply)
    local mode = self:GetNWString("shootmode")

    if mode == "Bonk" then
        for k,v in pairs(self.VElements) do
            self.WElements[k].color = Color(255, 255, 255, 0)
        end
        self.WElements["cutter"].color = Color(255, 255, 255, 255)
    elseif mode == "Twilight" then
        for k,v in pairs(self.WElements) do
            self.WElements[k].color = Color(255, 255, 255, 0)
        end
        self.WElements["twilysceptre_hl2size"].color = Color(255, 255, 255, 255)
    else
        for k,v in pairs(self.WElements) do
            self.WElements[k].color = Color(255, 255, 255, 255)
        end
        self.WElements["twilysceptre_hl2size"].color = Color(255, 255, 255, 0)
        self.WElements["cutter"].color = Color(255, 255, 255, 0)
    end

    return self.BaseClass.DrawWorldModel(self)
end

SWEP.Purpose = "For feeling like you did when you hit that home run back in the summer of '06"
SWEP.AutoSwitchTo = true
SWEP.Contact = ""
SWEP.Author = "Spastik"
SWEP.FiresUnderwater = true
SWEP.Spawnable = true
SWEP.AdminSpawnable = true
SWEP.SlotPos = 0
SWEP.Instructions = "Legs spread, same width as the shoulders; body tight, then hit the ball like you're defeating the enemy. Here, the pinky finger is the key and then you just hit, hit, hit. Kakin! Bingo!"
SWEP.AutoSwitchFrom = false
SWEP.Base = "aa_base"
SWEP.Category = "S&G Munitions"
SWEP.DrawAmmo = false
SWEP.PrintName = "The Remembered"
SWEP.Primary.Recoil = 0
SWEP.Primary.Damage = 0
SWEP.Primary.NumShots = 0
SWEP.Primary.Cone = 0
SWEP.Primary.ClipSize = -1
SWEP.Primary.DefaultClip = -1
SWEP.Primary.Automatic = false
SWEP.Primary.Ammo = "none"
SWEP.Secondary.Ammo = "none"
SWEP.Slot = 11
SWEP.Secondary.Automatic = true


function SWEP:throw_attack_brock(model_file)
    if CLIENT then return end

    local tr = self.Owner:GetEyeTrace()


    self.BaseClass.ShootEffects(self)




    local ent = ents.Create("remembered_sawblade")
    ent:SetModel(model_file)
    ent.owner = self.Owner

    ent:SetPos(self.Owner:EyePos() + (self.Owner:GetAimVector() * 16))
    ent:SetAngles(self.Owner:EyeAngles())
    ent:Spawn()

    local phys = ent:GetPhysicsObject()


    local shot_length = tr.HitPos:Length()
    phys:ApplyForceCenter(self.Owner:GetAimVector():GetNormalized() * math.pow(shot_length, 5))


    util.SpriteTrail(ent, 1, Color(255, 0, 0, 255), true, 50, 1, 2, 20, "trails/plasma.vmt")

    cleanup.Add(self.Owner, "props", ent)

    undo.Create("Sawblade")
    undo.AddEntity(ent)
    undo.SetPlayer(self.Owner)
    undo.SetCustomUndoText("Undone Sawblade")
    undo.Finish()
    timer.Simple(6, function()
        if IsValid(ent) then ent:Remove() end
    end)
end

function SWEP:throw_attack_brock2(model_file)
    if CLIENT then return end

    local tr = self.Owner:GetEyeTrace()


    self.BaseClass.ShootEffects(self)




    local ent = ents.Create("remembered_can")

    ent.owner = self.Owner

    ent:SetPos(self.Owner:EyePos() + (self.Owner:GetAimVector() * 16))
    ent:SetAngles(self.Owner:EyeAngles())
    ent:Spawn()

    local phys = ent:GetPhysicsObject()


    local shot_length = tr.HitPos:Length()
    phys:ApplyForceCenter(self.Owner:GetAimVector():GetNormalized() * math.pow(shot_length, 5))


    util.SpriteTrail(ent, 1, Color(255, 255, 255, 255), false, 5, 1, 3, 2, "sprites/store/arrows.vmt")

    cleanup.Add(self.Owner, "props", ent)

    undo.Create("Sawblade")
    undo.AddEntity(ent)
    undo.SetPlayer(self.Owner)
    undo.SetCustomUndoText("Undone Sawblade")
    undo.Finish()
    timer.Simple(2, function()
        if IsValid(ent) then ent:Remove() end
    end)
end

function SWEP:OnDrop()
    self:Remove()
end
SWEP.nextreload = 0
function SWEP:Reload()
    if self.Owner:KeyDown(IN_USE) then
        if self.nextreload > CurTime() then return end
        self.nextreload = CurTime() + 0.3
        local mode = self:GetNWString("shootmode","Remembered")
        if mode == "Remembered" then self:SetNWString("shootmode","Bonk")end
        if mode == "Bonk" then self:SetNWString("shootmode","Twilight")  end
        if mode == "Twilight" then self:SetNWString("shootmode","Remembered") end
        return
    end

    if self.NextSecond < CurTime() and self.nextreload < CurTime() then
        self.NextSecond = CurTime() + 0.5
        self:EmitSound("npc/manhack/grind_flesh1.wav")
        self.Weapon:SetNextSecondaryFire(CurTime() + 0.5)
        self.Weapon:SetNextPrimaryFire(CurTime() + 0.5)


        self:TakePrimaryAmmo(1)
        self:HealShot()

        local owner = self.Owner
        if not IsValid(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end


        timer.Simple(0.001, function() self:throw_attack_brock2("models/props_phx/misc/potato_launcher.mdl") owner:ViewPunch(Angle(math.Rand(-0.2, -0.1) * self.Primary.Recoil, math.Rand(-0.1, 0.1) * self.Primary.Recoil, 0)) end)
        return
    end
    return false
end

--SWEP:PrimaryFire()\\
function SWEP:PrimaryAttack()
    self.Weapon:SendWeaponAnim(ACT_VM_MISSCENTER)
    self.Weapon:SetNextPrimaryFire(CurTime() + .3)

    local mode = self:GetNWString("shootmode")

    if mode == "Twilight" then
        if CLIENT then return end
        self:ShootBullet( 15, 0.0001,1, 0.00001)
        local ent =self:throw_attack("hidude_acis_ball",100000)
        --ent:SetModel("models/hunter/misc/sphere1x1.mdl")
       -- ent:SetMaterial("models/debug/debugwhite")
       -- ent:SetColor(Color(math.random(0,255),math.random(0,255),math.random(0,255)))
        self.Trail = util.SpriteTrail( ent, 0, Color(255,255,255,255), false, 3, 1, 1, 0.125, 'trails/rainbow.vmt')

        if self.NextSound < CurTime() then
            self.NextSound = CurTime() + 8
            local soundfile = gb_config.websounds.."brock_pony.mp3"
            gb_PlaySoundFromServer(soundfile, self.Owner)
        end
    return
    end


    local trace = self.Owner:GetEyeTrace()

    if trace.HitPos:Distance(self.Owner:GetShootPos()) <= 75 then
        bullet = {}
        bullet.Num = 1
        bullet.Src = self.Owner:GetShootPos()
        bullet.Dir = self.Owner:GetAimVector()
        bullet.Spread = Vector(0, 0, 0)
        bullet.Tracer = 0
        bullet.Force = 5
        bullet.Damage = 55
        self.Owner:FireBullets(bullet)
        self.Owner:SetAnimation(PLAYER_ATTACK1);
        self.Weapon:EmitSound("weapons/stunstick/stunstick_impact" .. math.random(1, 2) .. ".wav", 500, math.random(90, 110), 1, -1)
        self.Weapon:EmitSound("weapons/stunstick/stunstick_fleshhit" .. math.random(1, 2) .. ".wav", 500, math.random(90, 110), 1, 1)
        self.Weapon:EmitSound("physics/metal/metal_canister_impact_hard" .. math.random(1, 2) .. ".wav", 500, math.random(90, 100), 1, 0)
        if SERVER then
            local hitposent = ents.Create("info_target")
            local trace = self.Owner:GetEyeTrace()
            local hitpos = trace.HitPos

            local hiteffect = ents.Create("point_tesla")
            hiteffect:SetOwner(self:GetOwner())
            hiteffect:SetPos(hitpos)
            hiteffect:Spawn()
            hiteffect:SetKeyValue("beamcount_max", 15)
            hiteffect:SetKeyValue("beamcount_min", 10)
            hiteffect:SetKeyValue("interval_max", 0.5)
            hiteffect:SetKeyValue("interval_min", 0.1)
            hiteffect:SetKeyValue("lifetime_max", 0.3)
            hiteffect:SetKeyValue("lifetime_min", 0.3)
            hiteffect:SetKeyValue("m_Color", 255, 255, 255)
            hiteffect:SetKeyValue("m_flRadius", 50)
            hiteffect:SetKeyValue("m_SoundName", "DoSpark")
            hiteffect:SetKeyValue("texture", "sprites/physcannon_bluelight1b.vmt")
            hiteffect:SetKeyValue("thick_max", 5)
            hiteffect:SetKeyValue("thick_max", 4)
            hiteffect:Fire("DoSpark", "", 0.01)
            hiteffect:Fire("DoSpark", "", 0.1)
            hiteffect:Fire("DoSpark", "", 0.2)
            hiteffect:Fire("DoSpark", "", 0.3)
            hiteffect:Fire("DoSpark", "", 0.4)
            hiteffect:Fire("kill", "", 1.0)
        end
    else
        self.Weapon:EmitSound("weapons/stunstick/stunstick_swing" .. math.random(1, 2) .. ".wav", 500, math.random(90, 110), 1, 0)
        self.Weapon:SendWeaponAnim(ACT_VM_MISSCENTER)
        self.Owner:SetAnimation(PLAYER_ATTACK1)
    end
end


function SWEP:Deploy()
    -- Make sure custom weapons are hidden
    local function playerDiesSound(victim, weapon, killer)


        if (IsValid(killer) and (killer:IsPlayer())) then
            local wep = killer:GetActiveWeapon()
            if IsValid(wep) then
                wep = wep:GetClass()
                if wep == "custom_the_remembered" or weapon:GetClass() == "remembered_sawblade" then
                    for k, v in pairs(ents.FindByClass("prop_ragdoll")) do
                        if v.player_ragdoll and not v.hadtheremembered and v.dmgwep and v.dmgwep == wep then

                            local rag2 = ents.Create("prop_ragdoll")
                            if not IsValid(rag2) then return nil end

                            rag2:SetPos(v:GetPos())
                            rag2:SetModel("models/Gibs/Fast_Zombie_legs.mdl")
                            rag2:SetAngles(v:GetAngles())
                            rag2:SetColor(v:GetColor())

                            rag2:Spawn()
                            rag2:Activate()
                            v.hadtheremembered = true
                            timer.Simple(10, function()
                                if IsValid(rag2) then rag2:Remove() end
                            end)


                            local rag = ents.Create("prop_ragdoll")
                            if not IsValid(rag) then return nil end

                            rag:SetPos(v:GetPos())
                            rag:SetModel("models/Gibs/Fast_Zombie_Torso.mdl")
                            rag:SetAngles(v:GetAngles())
                            rag:SetColor(v:GetColor())

                            rag:Spawn()
                            rag:Activate()
                            rag.player_ragdoll = true
                            rag.uqid = v.uqid
                            rag.hadtheremembered = true
                            rag.equipment = v.equipment
                            rag.was_role = v.was_role
                            rag.bomb_wire = v.bomb_wire
                            rag.dmgtype = v.dmgtype

                            rag.dmgwep = v.dmgwep

                            rag.killer_sample = v.killer_sample

                            -- crime scene data
                            rag.scene = v.scene

                            rag.was_headshot = v.was_headshot
                            rag.time = v.time
                            rag.kills = v.kills

                            CORPSE.SetPlayerNick(rag, victim)

                            v:Remove()
                        end
                    end
                end
            end
        end
    end
    self:SetNWString("shootmode","Remembered")
    hook.Add("PlayerDeath", "gb_hidecustomweapasadson_name", playerDiesSound)
end

--SWEP:PrimaryFire()\\
SWEP.NextSecond = 0
SWEP.NextSound = 0
function SWEP:SecondaryAttack()

        local mode = self:GetNWString("shootmode")
        if mode == "Bonk" then
            self.LaserColor = Color(50,255,50,255)
            self:ShootTracer("LaserTracer_thick")
            self:ShootBullet( 3, 0.0001,1, 0.00001)
            self:Disorientate()
            self:Blind(0.5)
        end

      if self.NextSecond < CurTime() then
        if mode == "Bonk" then
            self:EmitSound("weapons/shotgun/shotgun_fire7.wav")
            self.Weapon:SetNextSecondaryFire( CurTime() + 0.1 )
            self.NextSecond = CurTime()+0.3
            self.Weapon:SetNextPrimaryFire( CurTime() + 0.5 )

            self:ShootBullet( 4, 0.0001,5, 0.085 )
            self:ShootTracer("manatrace")
            self:TakePrimaryAmmo( 1 )

            local owner = self.Owner
            if not IsValid(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end


        end
        if mode == "Twilight" then
            self.Weapon:SetNextSecondaryFire( CurTime() + 0.8)
            if CLIENT then return end
            self:ShootBullet( 70, 0.0001,1, 0.00001)
            local ent =self:throw_attack("hidude_acis_ball",100000)
            ent:SetModel("models/hunter/misc/sphere1x1.mdl")
            ent:SetMaterial("models/debug/debugwhite")
            ent:SetColor(Color(math.random(0,255),math.random(0,255),math.random(0,255)))
            ent.shouldnotremove = true
            --ent:PhysicsInitBox(Vector(-10,-10,-10),Vector(10,10,10))
            timer.Simple(4,function()
                if IsValid(ent) then ent:Remove() end
            end)
            self.Trail = util.SpriteTrail( ent, 0, Color(255,255,255,255), false, 3, 1, 1, 0.125, 'trails/rainbow.vmt')



        end




        if mode == "Remembered" then
            self.NextSecond = CurTime() + 0.5
            self:EmitSound("npc/manhack/grind_flesh1.wav")
            self.Weapon:SetNextSecondaryFire(CurTime() + 0.5)
            self.Weapon:SetNextPrimaryFire(CurTime() + 0.5)


            self:TakePrimaryAmmo(1)
            self:HealShot()

            local owner = self.Owner
            if not IsValid(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end


            timer.Simple(0.001, function() self:throw_attack_brock("models/props_junk/sawblade001a.mdl") owner:ViewPunch(Angle(math.Rand(-0.2, -0.1) * self.Primary.Recoil, math.Rand(-0.1, 0.1) * self.Primary.Recoil, 0)) end)
            return
        end

    end
end

--SWEP:SecondaryFire()\\

SWEP.nextE = 0
function SWEP:Think() -- Called every frame

if self.Owner:KeyDown(IN_WALK) then
        if self.nextE < CurTime() then
            self.nextE = CurTime() + 0.5
            self:EmitSound("player/pl_fallpain3.wav")
            self.Weapon:SetNextSecondaryFire(CurTime() + 0.5)
            self.Weapon:SetNextPrimaryFire(CurTime() + 0.5)

            self:ShootBullet(11, 2, 6, 0.085)
            self:ShootTracer("manatrace")
            self:TakePrimaryAmmo(1)
            self:HealShot()

            local owner = self.Owner
            if not IsValid(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end
            self:MakeExplosion()
            self.NextSecond = CurTime() + 0.5
            owner:ViewPunch(Angle(math.Rand(-0.2, -0.1) * self.Primary.Recoil, math.Rand(-0.1, 0.1) * self.Primary.Recoil, 0))
            return
        end
        return
    end


end

function shockEnt(ent, pos, life, radius)
    local b = ents.Create("point_hurt")
    b:SetKeyValue("targetname", "fier")
    b:SetKeyValue("DamageRadius", radius)
    b:SetKeyValue("Damage", "100")
    b:SetKeyValue("DamageDelay", "1")
    b:SetKeyValue("DamageType", "256")
    b:SetPos(pos)
    b:SetParent(ent)
    b:Spawn()
    b:Fire("turnon", "", 0)
    b:Fire("turnoff", "", life)
    b:Fire("kill", "", life)
end

function hitBoxes(ent, ent2)
    if ent:IsValid() then
        local effectdata3 = EffectData()
        effectdata3:SetOrigin(ent:GetPos())
        effectdata3:SetStart(ent2:GetPos())
        effectdata3:SetMagnitude(10)
        effectdata3:SetEntity(ent)
        util.Effect("TeslaHitBoxes", effectdata3)
    end
end



if CLIENT then
    function SWEP:DrawHUD()
        local shotttext = "Shootmode: " .. self:GetNWString("shootmode", "Remembered") .. "\nE+R to change"
        surface.SetFont("ChatFont");
        local size_x, size_y = surface.GetTextSize(shotttext);

        draw.RoundedBox(6, ScrW() / 2 - size_x / 2 - 5, ScrH() - 150, size_x + 15, size_y + 10, Color(0, 0, 0, 150));
        draw.DrawText(shotttext, "ChatFont", ScrW() / 2, ScrH() - 150 + 5, Color(255, 30, 30, 255), TEXT_ALIGN_CENTER);
        self.BaseClass.DrawHUD(self)
    end
end

local ENT = {}

ENT.Type = "anim"
ENT.Base = "base_anim"


if SERVER then

    function ENT:Initialize()


        self:SetModel('models/props_junk/sawblade001a.mdl')
        self:SetModelScale(self:GetModelScale() * 1, 0)

        self.Entity:PhysicsInit(SOLID_VPHYSICS);
        self.Entity:SetMoveType(MOVETYPE_VPHYSICS);
        self.Entity:SetSolid(SOLID_VPHYSICS);
        self.Entity:SetCollisionGroup(COLLISION_GROUP_PROJECTILE)
        self.Damage = 1
        local phys = self:GetPhysicsObject()
        if phys:IsValid() then
            phys:Wake()
            phys:EnableGravity(true);
        end
    end

    function ENT:Touch(ent)
        if ent:IsPlayer() and not ent.nextsandtouch or (ent.nextsandtouch and ent.nextsandtouch < CurTime()) and self.Damage == 1 then
            ent.nextsandtouch = CurTime() + 0.5
            ent:Ignite(0.5, 0)
            local dmginfo = DamageInfo()
            dmginfo:SetDamage(80) --50 damage
            dmginfo:SetDamageType(DMG_BULLET) --Bullet damage
            dmginfo:SetAttacker(self.owner or ent) --First player found gets credit
            dmginfo:SetDamageForce(Vector(0, 0, 50)) --Launch upwards
            dmginfo:SetInflictor(self.owner or self)
            ent:TakeDamageInfo(dmginfo)
            self.Damage = 0
        end
    end
end

scripted_ents.Register(ENT, "remembered_sawblade", true)

local ENT = {}

ENT.Type = "anim"
ENT.Base = "base_anim"


if SERVER then

    function ENT:Initialize()


        self:SetModel('models/props_phx/misc/potato_launcher_explosive.mdl')
        self:SetMaterial("models/debug/debugwhite")
        self:SetModelScale(self:GetModelScale() * 1, 0)

        self.Entity:PhysicsInit(SOLID_VPHYSICS);
        self.Entity:SetMoveType(MOVETYPE_VPHYSICS);
        self.Entity:SetSolid(SOLID_VPHYSICS);
        self.Entity:SetCollisionGroup(COLLISION_GROUP_PROJECTILE)
        self.Damage = 1
        local phys = self:GetPhysicsObject()
        if phys:IsValid() then
            phys:Wake()
            phys:EnableGravity(true);
        end
    end

    function ENT:Touch(ent)
        if ent:IsWorld() then
            if not self.spawnedstuff and (not self.owner.lastdiskspawned or self.owner.lastdiskspawned < CurTime()) and not self.showedmesage then
                self.spawnedstuff = true
                local ent = ents.Create("remembered_disk")
                self.owner.lastdiskspawned = CurTime() + 15
                ent.owner = self.owner

                ent:SetPos(self:GetPos() + Vector(50, 0, 0))
                ent:Spawn()
                ent.owner = self.owner
            else
                if not self.showedmesage and (self.owner.lastdiskspawned and self.owner.lastdiskspawned > CurTime()) then
                    self.owner:PrintMessage(HUD_PRINTCENTER, "" .. math.Round(self.owner.lastdiskspawned - CurTime()) .. " seconds")
                    self.showedmesage = true
                end
            end
        end

        if ent:IsPlayer() and not ent.nextsandtouch or (ent.nextsandtouch and ent.nextsandtouch < CurTime()) and self.Damage == 1 then
            ent.nextsandtouch = CurTime() + 0.5
            ent:Ignite(0.5, 0)
            local dmginfo = DamageInfo()
            dmginfo:SetDamage(30) --50 damage
            dmginfo:SetDamageType(DMG_BULLET) --Bullet damage
            dmginfo:SetAttacker(self.owner or ent) --First player found gets credit
            dmginfo:SetDamageForce(Vector(0, 0, 50)) --Launch upwards
            dmginfo:SetInflictor(self.owner or self)
            ent:TakeDamageInfo(dmginfo)
            self.Damage = 0
        end
    end

    function ENT:PhysicsCollide(data, phys)


        local other = data.HitEntity
        if not IsValid(other) and not other:IsWorld() then return end
        self:Touch(other)
    end
end

scripted_ents.Register(ENT, "remembered_can", true)


local ENT = {}

ENT.Type = "anim"
ENT.Base = "base_anim"


if SERVER then

    function ENT:Initialize()


        self:SetModel('models/Mechanics/gears2/bevel_36t1.mdl')
        self:SetModelScale(self:GetModelScale() * 0.5, 0)
        --  self:SetMaterial("models/debug/debugwhite")
        self:SetColor(Color(0, 255, 0))

        self.Entity:PhysicsInit(SOLID_VPHYSICS);
        self.Entity:SetMoveType(MOVETYPE_VPHYSICS);
        self.Entity:SetSolid(SOLID_VPHYSICS);
        self.Entity:SetCollisionGroup(COLLISION_GROUP_PROJECTILE)
        self.Damage = 1
        local phys = self:GetPhysicsObject()
        if phys:IsValid() then
            phys:Wake()
            phys:EnableGravity(true);
        end
    end

    function ENT:Touch(ent)
        if ent:IsPlayer() and not ent.nextsandtouch or (ent.nextsandtouch and ent.nextsandtouch < CurTime()) and self.Damage == 1 then
            ent.nextsandtouch = CurTime() + 0.1
            self:SetColor(Color(255, 0, 0))
            timer.Simple(0.7, function()
                if not IsValid(self) then return end
                local dmginfo = DamageInfo()
                dmginfo:SetDamage(80) --50 damage
                dmginfo:SetDamageType(DMG_BULLET) --Bullet damage
                dmginfo:SetAttacker(self.owner or ent) --First player found gets credit
                dmginfo:SetDamageForce(Vector(0, 0, 50)) --Launch upwards
                dmginfo:SetInflictor(self.owner or self)
                ent:TakeDamageInfo(dmginfo)
                self.Damage = 0
                self:Explode()
            end)
        end
    end

    function ENT:Explode()
        if self.exploded then return end self.exploded = true
        local effect = EffectData()
        effect:SetStart(self:GetPos())
        effect:SetOrigin(self:GetPos())
        effect:SetScale(0)
        effect:SetRadius(250)
        effect:SetMagnitude(250)

        util.Effect("Explosion", effect, true, true)

        util.BlastDamage(self, self.owner, self:GetPos(), 0, 0)
        self:Remove()
    end
end

scripted_ents.Register(ENT, "remembered_disk", true)