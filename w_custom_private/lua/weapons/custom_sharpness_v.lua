

if SERVER then
AddCSLuaFile(  )

end

SWEP.EquipMenuData = {
      type  = "item_weapon",
      name  = " Faded Sword",
      desc  = "Mine some heads with this beauty"
      }

SWEP.Contact 		= "ishotz@chillville.net"
SWEP.Author			= "iShotz"
SWEP.Instructions	= "Left Click to attack"
SWEP.ViewModelFlip		= false

SWEP.HoldType			= "melee"
SWEP.Kind = WEAPON_PISTOL


SWEP.Spawnable			= true
SWEP.AdminSpawnable		= true
SWEP.AutoSpawnable		= false
SWEP.ViewModelFOV 	= 70
SWEP.ViewModel			= "models/weapons/v_diamond_gb_sword.mdl"
SWEP.WorldModel			= "models/weapons/w_diamond_gb_sword.mdl"
SWEP.HoldType = "crowbar"
SWEP.Icon = "vgui/ttt_fun_killicons/minecraftsword.png"

SWEP.FiresUnderwater = true
SWEP.Primary.Damage         = 75
SWEP.Base					= "aa_base"
SWEP.Primary.ClipSize		= -1
SWEP.Primary.DefaultClip	= -1
SWEP.Primary.Automatic		= true
SWEP.Primary.Ammo			= "none"
SWEP.Primary.Delay = 0.5


SWEP.Weight				= 5
SWEP.AutoSwitchTo		= false
SWEP.AutoSwitchFrom		= false

SWEP.Category			= "Minecraft"
SWEP.PrintName			= "Aclgolden's Sharpness V"
SWEP.Slot				= 15
SWEP.SlotPos			= 1
SWEP.DrawAmmo			= false
SWEP.DrawCrosshair		= true




function SWEP:Initialize()
self:SetHoldType( "melee" )
end

function SWEP:PrimaryAttack()

   self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )




   local spos = self.Owner:GetShootPos()
   local sdest = spos + (self.Owner:GetAimVector() * 150)

   local kmins = Vector(1,1,1) * -10
   local kmaxs = Vector(1,1,1) * 10

   local tr = util.TraceHull({start=spos, endpos=sdest, filter=self.Owner, mask=MASK_SHOT_HULL, mins=kmins, maxs=kmaxs})

   -- Hull might hit environment stuff that line does not hit
   if not IsValid(tr.Entity) then
      tr = util.TraceLine({start=spos, endpos=sdest, filter=self.Owner, mask=MASK_SHOT_HULL})
   end

   local hitEnt = tr.Entity

   -- effects
   if IsValid(hitEnt) then
      self.Weapon:SendWeaponAnim( ACT_VM_HITCENTER )
	  

      local edata = EffectData()
      edata:SetStart(spos)
      edata:SetOrigin(tr.HitPos)
      edata:SetNormal(tr.Normal)
      edata:SetEntity(hitEnt)

      if hitEnt:IsPlayer() or hitEnt:GetClass() == "prop_ragdoll" then
         util.Effect("BloodImpact", edata)
      end
   else
      self.Weapon:SendWeaponAnim( ACT_VM_MISSCENTER )
   end

   if SERVER then
      self.Owner:SetAnimation( PLAYER_ATTACK1 )
   end
self.Weapon:SetNextPrimaryFire(CurTime() + .43)

local trace = self.Owner:GetEyeTrace()

if trace.HitPos:Distance(self.Owner:GetShootPos()) <= 75 then


        local dmg = DamageInfo()
         if hitEnt:IsPlayer() and hitEnt:Health() < self.Primary.Damage and self.Primary.Damage and not  hitEnt.hitbysharpness then
         dmg:SetDamage( 1)
         hitEnt.hitbysharpness = true
         else
             dmg:SetDamage(self.Primary.Damage)
         end
         dmg:SetAttacker(self.Owner)
         dmg:SetInflictor(self.Weapon)
         dmg:SetDamageForce(self.Owner:GetAimVector() * 1500)
         dmg:SetDamagePosition(self.Owner:GetPos())
         dmg:SetDamageType(DMG_CLUB)
            if not IsValid(hitEnt) then return end
         hitEnt:DispatchTraceAttack(dmg, spos + (self.Owner:GetAimVector() * 3), sdest)


	--bullet = {}
	--bullet.Num    = 1
	--bullet.Src    = self.Owner:GetShootPos()
	--bullet.Dir    = self.Owner:GetAimVector()
	--bullet.Spread = Vector(0, 0, 0)
	--bullet.Tracer = 0
	--bullet.Force  = 3
	--bullet.Damage = 60
       --  dmg:SetDamageType(DMG_CLUB)
		self.Owner:DoAttackEvent()
--self.Owner:FireBullets(bullet) 
self.Weapon:EmitSound("Weapon_Crowbar.Melee_Hit")
else
self.Weapon:EmitSound("Zombie.AttackMiss")

	self.Owner:DoAttackEvent()
end

end
 function SWEP:SecondaryAttack()
     self:ThrowLikeKnife("sharpness_throwsword", 1, 50)
 end

function SWEP:OnDrop()
    self:Remove()
end

SWEP.nextheadcrab = 0
function SWEP:Reload()
    if self.Owner:KeyDown(IN_USE) then
        if self.nextheadcrab > CurTime() or not SERVER then return end
        self.nextheadcrab = CurTime() + 30

        local tr = self.Owner:GetEyeTrace()
        DamageLog("HEADCRAB: " .. self.Owner:Nick() .. " [" .. self.Owner:GetRoleString() .. "] spawned a headcrab ")

        local headturtle = SpawnNPC2(self.Owner,tr.HitPos + self.Owner:GetAimVector() * -46, "npc_headcrab_black")

        headturtle:SetNPCState(2)
        if  self.Owner:GetRoleString() ~= "traitor" then
            headturtle:SetKeyValue("sk_manhack_melee_dmg", 0)
        end

        --  headturtle:SetModel("models/props_lab/cactus.mdl")

        local turtle = ents.Create("prop_dynamic")
        turtle:SetModel("models/weapons/w_diamond_gb_sword.mdl")
        turtle:SetPos(headturtle:GetPos())
        turtle:SetAngles(Angle(0,-180,0))
        turtle:SetModelScale( turtle:GetModelScale() * 2, 0.05)
        turtle:SetParent(headturtle)
        turtle:SetHealth(1)

        --headturtle:SetCollisionGroup(COLLISION_GROUP_WEAPON)

        headturtle:SetNWEntity("Thrower", self.Owner)
        --headturtle:SetName(self:GetThrower():GetName())
        headturtle:SetNoDraw(true)
        headturtle:SetHealth(1)


        return
    end
    self:Fly()
end


local ENT = {}

ENT.Type = "anim"
ENT.Base = "base_anim"


if SERVER then

    function ENT:Initialize()
        self:SetModel("models/weapons/w_diamond_gb_sword.mdl")
        self:SetModelScale(self:GetModelScale()*1,0)
        self:SetColor(Color(128,128,128,255))
        self.Entity:PhysicsInit(SOLID_VPHYSICS);
        self.Entity:SetMoveType(MOVETYPE_VPHYSICS);
        self.Entity:SetSolid(SOLID_VPHYSICS);
        self.active = CurTime() + 0.2

        --  self.Entity:SetCollisionGroup(COLLISION_GROUP_WEAPON)
        local phys = self:GetPhysicsObject()
        if phys:IsValid() then
            phys:Wake()
            phys:EnableGravity(true);
        end

        timer.Simple(3,function()
            if IsValid(self) then self:Remove() end
        end)
    end

    function ENT:Touch( ent )
        if ent:IsPlayer() and not ent.nextsandtouch or (ent.nextsandtouch and ent.nextsandtouch < CurTime()) then
            ent.nextsandtouch = CurTime() + 1
           -- ent:Ignite(0.5,0)
            local dmginfo = DamageInfo()
            dmginfo:SetDamage( 50 ) --50 damage
            dmginfo:SetDamageType( DMG_BULLET ) --Bullet damage
            dmginfo:SetAttacker( self.owner or ent ) --First player found gets credit
            dmginfo:SetDamageForce( Vector( 0, 0, 50 ) ) --Launch upwards
            dmginfo:SetInflictor(self)
            ent:TakeDamageInfo( dmginfo )
            self:Remove()
        end
    end

    function ENT:PhysicsCollide(data, phys)
        if self.Stuck then return false end

        local other = data.HitEntity
        if IsValid(other) and other:IsPlayer() then
            self:Touch(other)
            self:Remove()
        end
    end


end

scripted_ents.Register( ENT, "sharpness_throwsword", true )
