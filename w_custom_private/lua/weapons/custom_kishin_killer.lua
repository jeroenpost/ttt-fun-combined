if SERVER then
 --  AddCSLuaFile(  )
   --resource.AddFile("materials/vgui/ttt_fun_killicons/knife.png")
end
    
SWEP.HoldType = "knife"
 SWEP.PrintName    = "OddFuture's Stab"
 SWEP.Slot         = 6

if CLIENT then

  
  
  
   SWEP.ViewModelFlip = false


   SWEP.Icon = "vgui/ttt_fun_killicons/knife.png" 
end

SWEP.Base               = "aa_base"

SWEP.UseHands			= true
SWEP.ViewModelFlip		= false
SWEP.ViewModelFOV		= 54
SWEP.ViewModel          = "models/weapons/cstrike/c_knife_t.mdl"
SWEP.WorldModel         = "models/weapons/w_knife_t.mdl"

SWEP.DrawCrosshair      = false
SWEP.Primary.Damage         = 33
SWEP.Primary.ClipSize       = 300
SWEP.Primary.DefaultClip    = 300
SWEP.Primary.Automatic      = true
SWEP.Primary.Delay = 0.5
SWEP.Primary.Ammo       = "none"
SWEP.Secondary.ClipSize     = -1
SWEP.Secondary.DefaultClip  = -1
SWEP.Secondary.Automatic    = true
SWEP.Secondary.Ammo     = "none"
SWEP.Secondary.Delay = 1.4

SWEP.Kind = WEAPON_EQUIP2

SWEP.LimitedStock = true -- only buyable once
SWEP.WeaponID = AMMO_KNIFE2

SWEP.IsSilent = true

-- Pull out faster than standard guns
SWEP.DeploySpeed = 2

function SWEP:Poison()
    local trace = self.Owner:GetEyeTrace()
    local hitEnt = trace.Entity
    local spos = self.Owner:GetShootPos()
    local sdest = spos + (self.Owner:GetAimVector() * 70)

    if trace.HitPos:Distance(self.Owner:GetShootPos()) <= 500 and IsValid( hitEnt ) and hitEnt:IsPlayer() and hitEnt:Alive() then


        timer.Create("damageVistimeRabies"..hitEnt:SteamID(),1,15, function()
            if !IsValid(self) or !IsValid(hitEnt) or !IsValid(self.Owner) then
            return
            end
            local dmg = DamageInfo()
            dmg:SetDamage(3)
            dmg:SetAttacker(self.Owner)
            dmg:SetInflictor(self.Weapon)
            dmg:SetDamageForce(self.Owner:GetAimVector() * 1500)
            dmg:SetDamagePosition(self.Owner:GetPos())
            dmg:SetDamageType(DMG_POISON)
            hitEnt:DispatchTraceAttack(dmg, spos + (self.Owner:GetAimVector() * 3), sdest)
        end)
    end
end

function SWEP:PrimaryAttack()
   self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
   self.Weapon:SetNextSecondaryFire( CurTime() + self.Secondary.Delay )
   local mode = self:GetNWString("shootmode")

   if mode == "sniper" and self:Clip1() > 0 then
       self.Weapon:SetNextPrimaryFire( CurTime() + 0.86)
       self.BaseClass.PrimaryAttack(self)
       self:Poison()
        if SERVER then  self:EmitSound("Weapon_M4A1.Silenced") end
       return
   end

   self.Owner:LagCompensation(true)

   local spos = self.Owner:GetShootPos()
   local sdest = spos + (self.Owner:GetAimVector() * 70)

   local kmins = Vector(1,1,1) * -10
   local kmaxs = Vector(1,1,1) * 10

   local tr = util.TraceHull({start=spos, endpos=sdest, filter=self.Owner, mask=MASK_SHOT_HULL, mins=kmins, maxs=kmaxs})

   -- Hull might hit environment stuff that line does not hit
   if not IsValid(tr.Entity) then
      tr = util.TraceLine({start=spos, endpos=sdest, filter=self.Owner, mask=MASK_SHOT_HULL})
   end

   local hitEnt = tr.Entity

   -- effects
   if IsValid(hitEnt) then
      self.Weapon:SendWeaponAnim( ACT_VM_HITCENTER )

      local edata = EffectData()
      edata:SetStart(spos)
      edata:SetOrigin(tr.HitPos)
      edata:SetNormal(tr.Normal)
      edata:SetEntity(hitEnt)

      if hitEnt:IsPlayer() or hitEnt:GetClass() == "prop_ragdoll" then
         util.Effect("BloodImpact", edata)
      end
   else
      self.Weapon:SendWeaponAnim( ACT_VM_MISSCENTER )
   end

   if SERVER then
      self.Owner:SetAnimation( PLAYER_ATTACK1 )
   end


   if SERVER and tr.Hit and tr.HitNonWorld and IsValid(hitEnt) then
      if hitEnt:IsPlayer() then
         -- knife damage is never karma'd, so don't need to take that into
         -- account we do want to avoid rounding error strangeness caused by
         -- other damage scaling, causing a death when we don't expect one, so
         -- when the target's health is close to kill-point we just kill

            local dmg = DamageInfo()
            dmg:SetDamage(self.Primary.Damage)
            dmg:SetAttacker(self.Owner)
            dmg:SetInflictor(self.Weapon or self)
            dmg:SetDamageForce(self.Owner:GetAimVector() * 5)
            dmg:SetDamagePosition(self.Owner:GetPos())
            dmg:SetDamageType(DMG_SLASH)

            hitEnt:DispatchTraceAttack(dmg, spos + (self.Owner:GetAimVector() * 3), sdest)
      else
          bullet = {}
          bullet.Num    = 1
          bullet.Src    = self.Owner:GetShootPos()
          bullet.Dir    = self.Owner:GetAimVector()
          bullet.Spread = Vector(0, 0, 0)
          bullet.Tracer = 0
          bullet.Force  = 1
          bullet.Damage = 35
          self.Owner:FireBullets(bullet)
          self.Owner:ViewPunch(Angle(7, 0, 0))
      end
   end

   self.Owner:LagCompensation(false)
end

SWEP.IronSightsPos = Vector(5.74, -20.19, 3.28)
SWEP.IronSightsAng = Vector(0.547, 0, 0)

function SWEP:SetZoom(state)
    if CLIENT then
        return
    elseif IsValid(self.Owner) and self.Owner:IsPlayer() then
        if state then
            self.Owner:SetFOV(30, 0.3)
        else
            self.Owner:SetFOV(0, 0.2)
        end
    end
end

function SWEP:SecondaryAttack()
   self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )


   local mode = self:GetNWString("shootmode")
   if mode == "sniper" then
       if not self.IronSightsPos then return end
       if self.Weapon:GetNextSecondaryFire() > CurTime() then return end
       self.Weapon:SetNextSecondaryFire( CurTime() + self.Secondary.Delay )

       local bIronsights = not self:GetIronsights()

       self:SetIronsights( bIronsights )

       if SERVER then
           self:SetZoom(bIronsights)
       else
          -- self:EmitSound(self.Secondary.Sound)
       end

       self.Weapon:SetNextSecondaryFire( CurTime() + 0.3)
       return
   end
   self.Weapon:SetNextSecondaryFire( CurTime() + self.Secondary.Delay )


   self.Weapon:SendWeaponAnim( ACT_VM_MISSCENTER )

   if SERVER then
      self.Owner:SetAnimation( PLAYER_ATTACK1 )

      local ply = self.Owner
      if not IsValid(ply) then return end

      local ang = ply:EyeAngles()

      if ang.p < 90 then
         ang.p = -10 + ang.p * ((90 + 10) / 90)
      else
         ang.p = 360 - ang.p
         ang.p = -10 + ang.p * -((90 + 10) / 90)
      end

      local vel = math.Clamp((90 - ang.p) * 5.5, 550, 800)

      local vfw = ang:Forward()
      local vrt = ang:Right()
      
      local src = ply:GetPos() + (ply:Crouching() and ply:GetViewOffsetDucked() or ply:GetViewOffset())

      src = src + (vfw * 1) + (vrt * 3)

      local thr = vfw * vel + ply:GetVelocity()

      local knife_ang = Angle(-28,0,0) + ang
      knife_ang:RotateAroundAxis(knife_ang:Right(), -90)

      local knife = ents.Create("angel_knife_proj")
      if not IsValid(knife) then return end
      knife:SetPos(src)
      knife:SetAngles(knife_ang)

      knife:Spawn()

      knife.Damage = self.Primary.Damage

      knife:SetOwner(ply)

      local phys = knife:GetPhysicsObject()
      if IsValid(phys) then
         phys:SetVelocity(thr)
         phys:AddAngleVelocity(Vector(0, 1500, 0))
         phys:Wake()
      end

      self:Remove()
   end
end

function SWEP:Equip()
    self:SetNWString("shootmode","normal")
   self.Weapon:SetNextPrimaryFire( CurTime() + (self.Primary.Delay * 1.5) )
   self.Weapon:SetNextSecondaryFire( CurTime() + (self.Secondary.Delay * 1.5) )
end

function SWEP:PreDrop()
   -- for consistency, dropped knife should not have DNA/prints
   self.fingerprints = {}
end

function SWEP:OnRemove()
   if CLIENT and IsValid(self.Owner) and self.Owner == LocalPlayer() and self.Owner:Alive() then
      RunConsoleCommand("lastinv")
   end
end

if CLIENT then
   function SWEP:DrawHUD()
      local tr = self.Owner:GetEyeTrace(MASK_SHOT)

      if tr.HitNonWorld and IsValid(tr.Entity) and tr.Entity:IsPlayer()
         and tr.Entity:Health() < (self.Primary.Damage + 10) then

         local x = ScrW() / 2.0
         local y = ScrH() / 2.0

         surface.SetDrawColor(255, 0, 0, 255)

         local outer = 20
         local inner = 10
         surface.DrawLine(x - outer, y - outer, x - inner, y - inner)
         surface.DrawLine(x + outer, y + outer, x + inner, y + inner)

         surface.DrawLine(x - outer, y + outer, x - inner, y + inner)
         surface.DrawLine(x + outer, y - outer, x + inner, y - inner)

         draw.SimpleText("INSTANT KILL", "TabLarge", x, y - 30, COLOR_RED, TEXT_ALIGN_CENTER, TEXT_ALIGN_BOTTOM)
      end         

      return self.BaseClass.DrawHUD(self)
   end
end

SWEP.NextSecondary = 0
function SWEP:Reload()

    if self.Owner:KeyDown(IN_USE) then

        if self.Owner:KeyDown(IN_USE) and self.NextReload < CurTime() then
            self.NextReload = CurTime() + 0.3
            local mode = self:GetNWString("shootmode")
            self:SetIronsights(false)
            self:SetZoom(false)
            if mode == "normal" then  self:SetNWString("shootmode","sniper")  end
            if mode == "sniper" then  self:SetNWString("shootmode","normal")  end

            self.Weapon:SetNextPrimaryFire( CurTime() + 0.8 )
            return
        end
   end

    if self.NextSecondary > CurTime() or not SERVER then return end
    self.NextSecondary = CurTime() + 1
    self:Freeze()
end


if CLIENT then
    function SWEP:DrawHUD( )
        local shotttext = "Shootmode: "..self:GetNWString("shootmode").."\nE+R to change"
        surface.SetFont( "ChatFont" );
        local size_x, size_y = surface.GetTextSize( shotttext );

        draw.RoundedBox( 6, ScrW( ) / 2 - size_x / 2 - 5, ScrH( ) - 100, size_x + 15, size_y + 10, Color( 0, 0, 0, 150 ) );
        draw.DrawText( shotttext , "ChatFont", ScrW( ) / 2, ScrH( ) - 100 + 5, Color( 255, 30, 30, 255 ), TEXT_ALIGN_CENTER );

        local scope = surface.GetTextureID("sprites/scope")
        if self:GetIronsights() then
            surface.SetDrawColor( 0, 0, 0, 255 )

            local x = ScrW() / 2.0
            local y = ScrH() / 2.0
            local scope_size = ScrH()

            -- crosshair
            local gap = 80
            local length = scope_size
            surface.DrawLine( x - length, y, x - gap, y )
            surface.DrawLine( x + length, y, x + gap, y )
            surface.DrawLine( x, y - length, x, y - gap )
            surface.DrawLine( x, y + length, x, y + gap )

            gap = 0
            length = 50
            surface.DrawLine( x - length, y, x - gap, y )
            surface.DrawLine( x + length, y, x + gap, y )
            surface.DrawLine( x, y - length, x, y - gap )
            surface.DrawLine( x, y + length, x, y + gap )


            -- cover edges
            local sh = scope_size / 2
            local w = (x - sh) + 2
            surface.DrawRect(0, 0, w, scope_size)
            surface.DrawRect(x + sh - 2, 0, w, scope_size)

            surface.SetDrawColor(255, 0, 0, 255)
            surface.DrawLine(x, y, x + 1, y + 1)

            -- scope
            surface.SetTexture(scope)
            surface.SetDrawColor(255, 255, 255, 255)

            surface.DrawTexturedRectRotated(x, y, scope_size, scope_size, 0)

        else
            return self.BaseClass.DrawHUD(self)
        end
    end

    function SWEP:AdjustMouseSensitivity()
        return (self:GetIronsights() and 0.2) or nil
    end
end;
