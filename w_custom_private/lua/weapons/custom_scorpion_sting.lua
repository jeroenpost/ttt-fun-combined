
if ( SERVER ) then
	AddCSLuaFile()

end

if ( CLIENT ) then
	
	SWEP.SlotPos = 5

	killicon.Add( "weapon_nyangun", "nyan/killicon", color_white )
	SWEP.WepSelectIcon = Material( "nyan/selection.png" )
	SWEP.BounceWeaponIcon = false
	SWEP.DrawWeaponInfoBox = false
end

   SWEP.EquipMenuData = {
      name = "NyanGun",
      type = "item_weapon",
      desc = "Shoots Nyan Cats"
   };
   SWEP.Slot = 9
SWEP.Kind = 10

SWEP.LimitedStock = false
SWEP.Icon = "vgui/ttt_fun_killicons/nyancat.png" 

SWEP.Base = "gb_camo_base"
SWEP.PrintName = "Valve's Nasty Surprise"
SWEP.Category = "Robotboy655's Weapons"
SWEP.ViewModelFlip		= false
SWEP.ViewModelFOV		= 54
SWEP.ViewModel          = "models/weapons/cstrike/c_knife_t.mdl"
SWEP.WorldModel         = "models/weapons/w_knife_t.mdl"
SWEP.UseHands = true
SWEP.Spawnable = true
SWEP.AdminSpawnable = true
SWEP.AutoSpawnable = false
SWEP.AutoSwitchTo = false
SWEP.AutoSwitchFrom = false
SWEP.HoldType = "smg"
SWEP.Camo = 42

SWEP.ViewModelFlip = false

SWEP.Primary.ClipSize = 1
SWEP.Primary.Delay = 0.4
SWEP.Primary.DefaultClip = 1
SWEP.Primary.Automatic = true
SWEP.Primary.Ammo = "none"

SWEP.Secondary.ClipSize = 1
SWEP.Secondary.Delay = 0.4
SWEP.Secondary.DefaultClip = 1
SWEP.Secondary.Automatic = true
SWEP.Secondary.Ammo = "none"
SWEP.Secondary.Damage = 50

function SWEP:Initialize()
	self:SetHoldType( self.HoldType or "pistol" )
    self.BaseClass.Initialize(self)
end


SWEP.NextSecond= 0
SWEP.NextThird = 0
function SWEP:SecondaryAttack()
    if self.Owner:KeyDown(IN_USE) then
        if not SERVER or self.NextSecond > CurTime() then return end
        self.NextSecond = CurTime() + 25
        local ent = ents.Create("cse_ent_shortgasgrenade")
        ent:SetOwner(ply)
        ent.Owner = self.Owner
        ent:SetPos(self.Owner:GetEyeTrace().HitPos)
        ent:SetAngles(Angle(1,0,0))
        --ent.model = ('models/balloons/balloon_classicheart.mdl')
       -- ent:SetColor(Color(255,0,0,255))

        ent.timer = CurTime() + 5

        if (ent) then
           -- ent:SetPos(self.Owner:GetPos() + Vector(0,0,48));
            ent:SetPhysicsAttacker(self.Owner)
            ent.owner = self.Owner
            ent:NextThink(CurTime() + 5)
            ent:Spawn();
            ent.timer = CurTime() + 1
            --ball:SetModelScale( ball:GetModelScale() * 0.1, 0.01 )
          --  local physicsObject = ent:GetPhysicsObject();
          --  physicsObject:ApplyForceCenter(self.Owner:GetAimVector() * 8000);

        end;
        return
    end

    if ( !self:CanPrimaryAttack() ) then return end
    self:EmitSound( "weapons/nyan/nya" .. math.random( 1, 2 ) .. ".mp3", 100, math.random( 85, 100 ) )

    local bullet = {}
    bullet.Num = 6
    bullet.Src = self.Owner:GetShootPos()
    bullet.Dir = self.Owner:GetAimVector()
    bullet.Spread = Vector( 0.10, 0.1, 0 )
    bullet.Tracer = 1
    bullet.Force = 10
    bullet.Damage = 12
            //bullet.AmmoType = "Ar2AltFire"
    bullet.TracerName = "rb655_nyan_tracer"
    self.Owner:FireBullets( bullet )

    self.Weapon:SendWeaponAnim( ACT_VM_SECONDARYATTACK )
    self.Owner:SetAnimation( PLAYER_ATTACK1 )

    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
    self.Weapon:SetNextSecondaryFire( CurTime() + self.Secondary.Delay )


end


function SWEP:PrimaryAttack()

    if self.Owner:KeyDown(IN_USE) then
        if not SERVER or self.NextThird > CurTime() then return end
        self.NextThird = CurTime() + 20
        local ent = ents.Create("cse_ent_shorthealthgrenade")
        ent:SetOwner(self.Owner)
        ent.Owner = self.Owner
        ent:SetPos(self.Owner:GetEyeTrace().HitPos)
        ent:SetAngles(Angle(1,0,0))
        ent.model = ('models/balloons/balloon_classicheart.mdl')
        ent:SetColor(Color(255,0,0,255))

        ent.timer = CurTime() + 5

        if (ent) then
           -- ent:SetPos(self.Owner:GetPos() + Vector(0,0,48));
            ent:SetPhysicsAttacker(self.Owner)
            ent.owner = self.Owner
            ent:NextThink(CurTime() + 5)
            ent:Spawn();
            ent.timer = CurTime() + 1
            --ball:SetModelScale( ball:GetModelScale() * 0.1, 0.01 )
           -- local physicsObject = ent:GetPhysicsObject();
           -- physicsObject:ApplyForceCenter(self.Owner:GetAimVector() * 8000);

        end;
        return
    end

    if ( !self:CanSecondaryAttack() ) then return end


    local bullet = {}
    bullet.Num = 1
    bullet.Src = self.Owner:GetShootPos()
    bullet.Dir = self.Owner:GetAimVector()
    bullet.Spread = Vector( 0.01, 0.01, 0 )
    bullet.Tracer = 1
    bullet.Force = 5
    bullet.Damage = 60

    bullet.TracerName = "rb655_nyan_tracer"
    self.Owner:FireBullets( bullet )

    self:SendWeaponAnim( ACT_VM_PRIMARYATTACK )
    self.Owner:SetAnimation( PLAYER_ATTACK1 )

    self:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
    self:SetNextSecondaryFire( CurTime() + self.Secondary.Delay )


end



function SWEP:Reload()
	if ( !self.Owner:KeyPressed( IN_RELOAD ) or !self.Owner:KeyPressed( IN_USE ) ) then return end
	if ( self:GetNextPrimaryFire() > CurTime() ) then return end

	if ( SERVER ) then
		local ang = self.Owner:EyeAngles()
		local ent = ents.Create( "ent_nyan_bomb" )
		if ( IsValid( ent ) ) then
			ent:SetPos( self.Owner:GetShootPos() + ang:Forward() * 28 + ang:Right() * 24 - ang:Up() * 8 )
			ent:SetAngles( ang )
			ent:SetOwner( self.Owner )
			ent:Spawn()
			ent:Activate()
			
			local phys = ent:GetPhysicsObject()
			if ( IsValid( phys ) ) then phys:Wake() phys:AddVelocity( ent:GetForward() * 1337 ) end
		end
	end
	
	self:SendWeaponAnim( ACT_VM_SECONDARYATTACK )
	self.Owner:SetAnimation( PLAYER_ATTACK1 )
	
	self:EmitSound( "weapons/nyan/nya" .. math.random( 1, 2 ) .. ".mp3", 100, math.random( 60, 80 ) )
	
	self:SetNextPrimaryFire( CurTime() + 1 )
	self:SetNextSecondaryFire( CurTime() + 1 )
end

function SWEP:DoImpactEffect( trace, damageType )
	local effectdata = EffectData()
	effectdata:SetStart( trace.HitPos )
	effectdata:SetOrigin( trace.HitNormal + Vector( math.Rand( -0.5, 0.5 ), math.Rand( -0.5, 0.5 ), math.Rand( -0.5, 0.5 ) ) )
	util.Effect( "rb655_nyan_bounce", effectdata )

	return true
end

function SWEP:FireAnimationEvent( pos, ang, event )
	return true
end

function SWEP:KillSounds()
	if ( self.BeatSound ) then self.BeatSound:Stop() self.BeatSound = nil end
	if ( self.LoopSound ) then self.LoopSound:Stop() self.LoopSound = nil end
end

function SWEP:OnRemove()
	self:KillSounds()
end

function SWEP:OnDrop()
	self:KillSounds()
end

function SWEP:PreDrop(yes)
    if yes then self:Remove() end
end

function SWEP:Deploy()
	self.Weapon:SendWeaponAnim( ACT_VM_DRAW )
	self.Weapon:SetNextPrimaryFire( CurTime() + self.Weapon:SequenceDuration() )
	
	if ( CLIENT ) then return true end



	return self.BaseClass.Deploy(self)
end

function SWEP:Holster()
	self:KillSounds()
	return true
end

function SWEP:Think()
    if self.Owner:KeyDown(IN_WALK) then
        self:SpartaKick()
    end

    if self.NextAmmoGive < CurTime() && self:Clip1() < 150 then
    self.NextAmmoGive = CurTime() + 3
    self:SetClip1( self:Clip1() + 5 )
    end

    if (!self.Owner || self.Owner == NULL) or self:Clip1() < 1 then

    if ( !self.Beam ) then return end
    if( IsValid( self.Beam) ) then
        self.Beam:Remove()
    end
    self.Beam = nil
    return
    end



    if ( self.Owner:KeyPressed( IN_RELOAD ) ) then

        self:StartAttack()

    elseif ( self.Owner:KeyDown( IN_RELOAD )  ) then

        self:UpdateAttack()
       -- self.Weapon:SetNextSecondaryFire( CurTime() + 1 )

    elseif ( self.Owner:KeyReleased( IN_RELOAD )  ) then

        self:EndAttack( true )

    end
end

SWEP.NextSparta = 0
function SWEP:SpartaKick()
    if self.NextSparta > CurTime() then return end
    self.NextSparta = CurTime() + 5
    self.Weapon:EmitSound("player/kick/foot_swing.mp3");
    if IsValid( self.Owner ) then
        local trace = self.Owner:GetEyeTrace();
        if trace.HitPos:Distance(self.Owner:GetShootPos()) <= 260 then
            if( trace.Entity:IsPlayer() or trace.Entity:IsNPC() or trace.Entity:GetClass()=="prop_ragdoll" ) then
                --	timer.Simple(0, function() game.ConsoleCommand("host_timescale 0.1\n") end)
                --	timer.Simple(0.5, function() game.ConsoleCommand("host_timescale 1\n") end)
                --self.Owner:EmitSound( self.FleshHit[math.random(1,#self.FleshHit)] );
                if SERVER then trace.Entity:SetVelocity(self.Owner:GetForward() * 600 + Vector(0,0,400)) end

            end
            bullet = {}
            bullet.Num    = 6
            bullet.Src    = self.Owner:GetShootPos()
            bullet.Dir    = self.Owner:GetAimVector()
            bullet.Spread = Vector(0.04, 0.04, 0.04)
            bullet.Tracer = 0
            bullet.Force  = 1050
            bullet.Damage = 30
            self.Owner:FireBullets(bullet)
        end


        if IsValid( trace ) and IsValid(trace.Entity) and trace.Entity:GetClass() == "prop_door_rotating" then
            trace.Entity:EmitSound(Sound("physics/wood/wood_box_impact_hard3.mp3"))
            trace.Entity:Fire("open", "", .001)
            trace.Entity:Fire("unlock", "", .001)
            local pos = trace.Entity:GetPos()
            local ang = trace.Entity:GetAngles()
            local model = trace.Entity:GetModel()
            local skin = trace.Entity:GetSkin()
            trace.Entity:SetNotSolid(true)
            trace.Entity:SetNoDraw(true)
            local function ResetDoor(door, fakedoor)
                door:SetNotSolid(false)
                door:SetNoDraw(false)
                fakedoor:Remove()
            end
            local norm = (pos - self.Owner:GetPos()):Normalize()
            local push = 1000000 * norm
            local ent = ents.Create("prop_physics")
            ent:SetPos(pos)
            ent:SetAngles(ang)
            ent:SetModel(model)
            if(skin) then
                ent:SetSkin(skin)
            end
            ent:Spawn()
            timer.Simple(.01, ent.SetVelocity, ent, push)
            timer.Simple(.01, ent:GetPhysicsObject().ApplyForceCenter, ent:GetPhysicsObject(), push)
            timer.Simple(140, ResetDoor, trace.Entity, ent)
        end
    end
end



local sndPowerUp		= Sound("rope_hit.mp3")
local sndPowerDown		= Sound ("shoot_rope.mp3")
local sndTooFar			= Sound ("to_far.mp3")

SWEP.NextAmmoGive = 0
SWEP.NextAmmoTake = 0


function SWEP:DoTrace( endpos )
    local trace = {}
    trace.start = self.Owner:GetShootPos()
    trace.endpos = trace.start + (self.Owner:GetAimVector() * 14096) --14096 is length modifier.
    if(endpos) then trace.endpos = (endpos - self.Tr.HitNormal * 7) end
    trace.filter = { self.Owner, self.Weapon }

    self.Tr = nil
    self.Tr = util.TraceLine( trace )
end


function SWEP:StartAttack()


    if not self:CanPrimaryAttack() then return end

    --Get begining and end poins of trace.
    local gunPos = self.Owner:GetShootPos() --Start of distance trace.
    local disTrace = self.Owner:GetEyeTrace() --Store all results of a trace in disTrace.
    local hitPos = disTrace.HitPos --Stores Hit Position of disTrace.

    --Calculate Distance
    --Thanks to rgovostes for this code.
    local x = (gunPos.x - hitPos.x)^2;
    local y = (gunPos.y - hitPos.y)^2;
    local z = (gunPos.z - hitPos.z)^2;
    local distance = math.sqrt(x + y + z);

    --Only latches if distance is less than distance CVAR
    local distanceCvar = GetConVarNumber("grapple_distance")
    inRange = false
    if distance <= distanceCvar then
        inRange = true
    end

    if inRange then
        if (SERVER) then

            if (!self.Beam) then --If the beam does not exist, draw the beam.
            if not self:CanPrimaryAttack() then return end
            --grapple_beam
            self.Beam = ents.Create( "trace1" )
            self.Beam:SetPos( self.Owner:GetShootPos() )
            self.Beam:Spawn()
            self:TakePrimaryAmmo(1)

            end

            self.Beam:SetParent( self.Owner )
            self.Beam:SetOwner( self.Owner )

        end


        self:DoTrace()
        self.speed = 10000 --Rope latch speed. Was 3000.
        self.startTime = CurTime()
        self.endTime = CurTime() + self.speed
        self.dtfix = -1

        if (SERVER && self.Beam) then
        self.Beam:GetTable():SetEndPos( self.Tr.HitPos )
        end

        self:UpdateAttack()

        self.Weapon:EmitSound( sndPowerDown )
    else
        --Play A Sound
        self.Weapon:EmitSound( sndTooFar )
    end
end

function SWEP:UpdateAttack()

    -- if not self:CanPrimaryAttack() then return end
    if !self.Tr or self:Clip1() < 1 then return end
    self.Owner:LagCompensation( true )


    if (!endpos) then endpos = self.Tr.HitPos end

    if (SERVER && self.Beam) then
    self.Beam:GetTable():SetEndPos( endpos )
    end

    lastpos = endpos



    if ( self.Tr.Entity:IsValid() ) then

        endpos = self.Tr.Entity:GetPos()
        if ( SERVER and IsValid(self.Beam) ) then
            self.Beam:GetTable():SetEndPos( endpos )
        end

    end

    local vVel = (endpos - self.Owner:GetPos())
    local Distance = endpos:Distance(self.Owner:GetPos())

    local et = (self.startTime + (Distance/self.speed))
    if(self.dtfix != 0) then
    self.dtfix = (et - CurTime()) / (et - self.startTime)
    end
    if(self.dtfix < 0) then
        self.Weapon:EmitSound( sndPowerUp )
        self.dtfix = 0
    end

    if(self.dtfix == 0) then
        zVel = self.Owner:GetVelocity().z
        vVel = vVel:GetNormalized()*(math.Clamp(Distance,0,25))
        if( SERVER ) then
            if  self.NextAmmoTake < CurTime() then
                self:TakePrimaryAmmo(1)
                self.NextAmmoTake = CurTime() + 1
                self.NextAmmoGive = CurTime() + 1.2
            end

            local gravity = GetConVarNumber("sv_gravity")

            vVel:Add(Vector(0,0,(gravity/50)*1.5))

            if(zVel < 0) then
                vVel:Sub(Vector(0,0,zVel/100))
            end
            self.Owner:SetVelocity(vVel)
        end
    end

    endpos = nil

    self.Owner:LagCompensation( false )

end

function SWEP:EndAttack( shutdownsound )

    if ( shutdownsound ) then
        self.Weapon:EmitSound( sndPowerDown )
    end

    if ( CLIENT ) then return end
    if ( !self.Beam ) then return end
    if( IsValid( self.Beam) ) then
        self.Beam:Remove()
    end
    self.Beam = nil

end


function SWEP:Holster()
    self:EndAttack( false )
    return true
end

function SWEP:OnRemove()
    self:EndAttack( false )
    return true
end

