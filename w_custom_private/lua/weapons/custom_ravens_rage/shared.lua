
   SWEP.PrintName	= "Ravens Rage"
   SWEP.Author 		= "GreenBlack"   
   SWEP.Icon = 'vgui/ttt_fun_pointshop_icons/golden_deagle.png'

SWEP.Base			= "gb_master_deagle"

SWEP.Kind 			= WEAPON_PISTOL
SWEP.Slot = 1
SWEP.SlotPos 	= 1
SWEP.Primary.Recoil	= 1
SWEP.Primary.Damage = 50
SWEP.Primary.Delay 	= 0.45
SWEP.Primary.Cone 	= 0.009
SWEP.Primary.ClipSize 		= 80
SWEP.Primary.ClipMax 		= 120
SWEP.Primary.DefaultClip 	= 80
SWEP.Primary.NumShots		= 1
SWEP.Primary.Automatic 	= true
SWEP.HeadshotMultiplier = 3
SWEP.AmmoEnt 		= "item_ammo_revolver_ttt"


SWEP.HoldType = "pistol"
SWEP.ViewModelFOV = 87.322834645669
SWEP.ViewModelFlip = true
SWEP.ViewModel = "models/weapons/v_pist_gbagle.mdl"
SWEP.WorldModel = "models/weapons/w_pist_gbagle.mdl"
SWEP.ShowViewModel = true
SWEP.ShowWorldModel = true
SWEP.ViewModelBoneMods = {}

SWEP.WElements = {
	["element_name"] = { type = "Model", model = "models/crow.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(3.181, 0.455, 2.273), angle = Angle(0, -5.114, 180), size = Vector(0.5, 0.5, 0.5), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
}

