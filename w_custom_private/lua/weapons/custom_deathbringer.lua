if SERVER then

   AddCSLuaFile( )
   

	 
	 
end

SWEP.HoldType			= "smg"
SWEP.PrintName			= "DeathBringer"
SWEP.Slot				= 2
if CLIENT then

   
   SWEP.Author				= "Assassin"
   
   SWEP.SlotPos			= 0

   SWEP.Icon = "vgui/ttt_fun_killicons/mp7.png"

	 
	 
   
   SWEP.ViewModelFlip		= true
end


SWEP.Base				= "aa_base"



SWEP.Kind = WEAPON_HEAVY

SWEP.CanDecapitate = true
SWEP.Primary.Damage = 55
SWEP.Primary.Delay = 0.6
SWEP.Primary.Cone = 0.001
SWEP.Primary.ClipSize = 150
SWEP.Primary.ClipMax = 999
SWEP.Primary.DefaultClip	= 200
SWEP.Primary.Automatic		= true
SWEP.Primary.Ammo			= "smg1"
SWEP.AutoSpawnable      = false
SWEP.Primary.Recoil			= 0.8
SWEP.Primary.Sound			= Sound("weapons/mp7/mp7_fire.mp3")
SWEP.ViewModel			= "models/weapons/v_mp7_silenced.mdl"
SWEP.WorldModel			= "models/weapons/w_mp7_silenced.mdl"

SWEP.AmmoEnt = "item_ammo_smg1_ttt"

SWEP.HeadshotMultiplier = 2.5

SWEP.Secondary.Sound = Sound("Default.Zoom")

SWEP.IronSightsPos      = Vector( 5, -15, -2 )
SWEP.IronSightsAng      = Vector( 2.6, 1.37, 3.5 )

function SWEP:SetZoom(state)
    if CLIENT then 
       return
    else
       if state and IsValid(self.Owner) then
          self.Owner:SetFOV(20, 0.3)
       elseif IsValid(self.Owner) then
          self.Owner:SetFOV(0, 0.2)
       end
    end
end
function SWEP:OnDrop()
    self:Remove()
end

SWEP.LaserColor = Color(80,80,80,255)
function SWEP:PrimaryAttack()
    if self.Owner:KeyDown(IN_USE) then return end
    self:NormalPrimaryAttack()
    if self:Clip1() > 0 then
        self:ShootTracer( "manatrace")
    end
end

-- Add some zoom to ironsights for this gun
function SWEP:SecondaryAttack()
    if not self.IronSightsPos then return end
    if self.Weapon:GetNextSecondaryFire() > CurTime() then return end
    
    bIronsights = not self:GetIronsights()
    
    self:SetIronsights( bIronsights )
    
    if SERVER then
        self:SetZoom(bIronsights)
     else
        self:EmitSound(self.Secondary.Sound)
    end
    
    self.Weapon:SetNextSecondaryFire( CurTime() + 0.3)
end

function SWEP:PreDrop()
    self:SetZoom(false)
    self:SetIronsights(false)
    return self.BaseClass.PreDrop(self)
end

function SWEP:Reload()

    if self.Owner:KeyDown(IN_WALK) then
        self:BombStick()
        return
    end
    if self.Owner:KeyDown(IN_USE) then
        if (not self.NextGive or self.NextGive < CurTime()) and SERVER then

            self.NextGive = CurTime() + 10


            local pos, ang =  self.Owner:GetShootPos(),  self.Owner:EyeAngles()
            local dir = (ang:Forward() * 32) + (ang:Right() * 6) + (ang:Up() * -5)

            local tr = util.QuickTrace(pos, dir, self.Owner)
            if tr.HitWorld then return end


            self.Owner:AnimPerformGesture(ACT_ITEM_GIVE)

            local box = ents.Create('item_ammo_smg1_ttt')
            if not IsValid(box) then box:Remove() end

            box:SetPos(pos + dir)
            box:SetOwner(self.Owner)
            box:Spawn()

            box:PhysWake()

            local phys = box:GetPhysicsObject()
            if IsValid(phys) then
                phys:ApplyForceCenter(ang:Forward() * 1000)
                phys:ApplyForceOffset(VectorRand(), vector_origin)
            end

            box.AmmoAmount = 100
            box.AmmoMax = 100

            timer.Simple(2, function()
                if IsValid(box) then
                    box:SetOwner(nil)
                end
            end)


        end
        return
    end
    self.Weapon:DefaultReload( ACT_VM_RELOAD );
    self:SetIronsights( false )
    self:SetZoom(false)
end


function SWEP:Holster()
    self:SetIronsights(false)
    self:SetZoom(false)
    return true
end

if CLIENT then
   local scope = surface.GetTextureID("sprites/scope")
   function SWEP:DrawHUD()
      if self:GetIronsights() then
         surface.SetDrawColor( 0, 0, 0, 255 )
         
         local x = ScrW() / 2.0
         local y = ScrH() / 2.0
         local scope_size = ScrH()

         -- crosshair
         local gap = 80
         local length = scope_size
         surface.DrawLine( x - length, y, x - gap, y )
         surface.DrawLine( x + length, y, x + gap, y )
         surface.DrawLine( x, y - length, x, y - gap )
         surface.DrawLine( x, y + length, x, y + gap )

         gap = 0
         length = 50
         surface.DrawLine( x - length, y, x - gap, y )
         surface.DrawLine( x + length, y, x + gap, y )
         surface.DrawLine( x, y - length, x, y - gap )
         surface.DrawLine( x, y + length, x, y + gap )


         -- cover edges
         local sh = scope_size / 2
         local w = (x - sh) + 2
         surface.DrawRect(0, 0, w, scope_size)
         surface.DrawRect(x + sh - 2, 0, w, scope_size)

         surface.SetDrawColor(255, 0, 0, 255)
         surface.DrawLine(x, y, x + 1, y + 1)

         -- scope
         surface.SetTexture(scope)
         surface.SetDrawColor(255, 255, 255, 255)

         surface.DrawTexturedRectRotated(x, y, scope_size, scope_size, 0)

      else
         return self.BaseClass.DrawHUD(self)
      end
   end

   function SWEP:AdjustMouseSensitivity()
      return (self:GetIronsights() and 0.2) or nil
   end
end

-- again replicating slam, now its attach fn
function SWEP:BombStick()
    if SERVER then
        local ply = self.Owner
        if not IsValid(ply) then return end

        if self.Planted then return end



        local ignore = {ply, self}
        local spos = ply:GetShootPos()
        local epos = spos + ply:GetAimVector() * 80
        local tr = util.TraceLine({start=spos, endpos=epos, filter=ignore, mask=MASK_SOLID})

        if tr.HitWorld then


            local bomb = ents.Create("deathbringer_c4")
            if IsValid(bomb) then
                bomb:PointAtEntity(ply)

                local tr_ent = util.TraceEntity({start=spos, endpos=epos, filter=ignore, mask=MASK_SOLID}, bomb)

                if tr_ent.HitWorld then

                    if not self.Owner:IsTraitor() then
                        self.Owner:SetHealth(self.Owner:Health()-50)
                    end

                    local ang = tr_ent.HitNormal:Angle()
                    ang:RotateAroundAxis(ang:Right(), -0)
                    ang:RotateAroundAxis(ang:Up(), 0)

                    bomb:SetPos(tr_ent.HitPos)
                    bomb:SetAngles(ang)
                    bomb:SetOwner(ply)
                    bomb:SetThrower(ply)
                    bomb:Spawn()

                    bomb.fingerprints = self.fingerprints

                    local phys = bomb:GetPhysicsObject()
                    if IsValid(phys) then
                        phys:EnableMotion(false)
                    end

                    bomb.IsOnWall = true

                    --self:Remove()

                    self.Planted = true

                end
            end

            ply:SetAnimation( PLAYER_ATTACK1 )
        end
    end
end


SWEP.NextAmmoGive = 0
SWEP.NextAmmoTake = 0


local sndPowerUp		= Sound("rope_hit.mp3")
local sndPowerDown		= Sound ("shoot_rope.mp3")
local sndTooFar			= Sound ("to_far.mp3")

function SWEP:Think()

    if self.Owner:KeyDown(IN_MOVELEFT) and self.Owner:KeyDown(IN_MOVERIGHT) then
        self:ZedTime("zt_enter.mp3")
            return
    end
    if self.NextAmmoGive < CurTime() && self:Clip1() < 10 then
    self.NextAmmoGive = CurTime() + 2
    self:SetClip1( self:Clip1() + 1 )
    end

    if (!self.Owner || self.Owner == NULL) or self:Clip1() < 1 then

    if ( !self.Beam ) then return end
    if( IsValid( self.Beam) ) then
        self.Beam:Remove()
    end
    self.Beam = nil
    return
    end



    if ( self.Owner:KeyPressed( IN_USE ) ) then

        self:StartAttack()

    elseif ( self.Owner:KeyDown( IN_ATTACK ) and self.Owner:KeyDown( IN_USE )  ) then

        self:UpdateAttack()

    elseif (  self.Owner:KeyReleased( IN_USE )   ) then

        self:EndAttack( true )

    end



end

function SWEP:DoTrace( endpos )
    local trace = {}
    trace.start = self.Owner:GetShootPos()
    trace.endpos = trace.start + (self.Owner:GetAimVector() * 14096) --14096 is length modifier.
    if(endpos) then trace.endpos = (endpos - self.Tr.HitNormal * 7) end
    trace.filter = { self.Owner, self.Weapon }

    self.Tr = nil
    self.Tr = util.TraceLine( trace )
end




function SWEP:StartAttack()


    if not self:CanPrimaryAttack() then return end

    --Get begining and end poins of trace.
    local gunPos = self.Owner:GetShootPos() --Start of distance trace.
    local disTrace = self.Owner:GetEyeTrace() --Store all results of a trace in disTrace.
    local hitPos = disTrace.HitPos --Stores Hit Position of disTrace.

    --Calculate Distance
    --Thanks to rgovostes for this code.
    local x = (gunPos.x - hitPos.x)^2;
    local y = (gunPos.y - hitPos.y)^2;
    local z = (gunPos.z - hitPos.z)^2;
    local distance = math.sqrt(x + y + z);

    --Only latches if distance is less than distance CVAR
    local distanceCvar = GetConVarNumber("grapple_distance")
    inRange = false
    if distance <= distanceCvar then
        inRange = true
    end

    if inRange then
        if (SERVER) then

            if (!self.Beam) then --If the beam does not exist, draw the beam.
            if not self:CanPrimaryAttack() then return end
            --grapple_beam
            self.Beam = ents.Create( "trace1" )
            self.Beam:SetPos( self.Owner:GetShootPos() )
            self.Beam:Spawn()
            self:TakePrimaryAmmo(1)

            end

            self.Beam:SetParent( self.Owner )
            self.Beam:SetOwner( self.Owner )

        end


        self:DoTrace()
        self.speed = 10000 --Rope latch speed. Was 3000.
        self.startTime = CurTime()
        self.endTime = CurTime() + self.speed
        self.dtfix = -1

        if (SERVER && self.Beam) then
        self.Beam:GetTable():SetEndPos( self.Tr.HitPos )
        end

        self:UpdateAttack()

        self.Weapon:EmitSound( sndPowerDown )
    else
        --Play A Sound
        self.Weapon:EmitSound( sndTooFar )
    end
end

function SWEP:UpdateAttack()

    -- if not self:CanPrimaryAttack() then return end
    if !self.Tr or self:Clip1() < 1 then return end
    self.Owner:LagCompensation( true )


    if (!endpos) then endpos = self.Tr.HitPos end

    if (SERVER && self.Beam) then
    self.Beam:GetTable():SetEndPos( endpos )
    end

    lastpos = endpos



    if ( self.Tr.Entity:IsValid() ) then

        endpos = self.Tr.Entity:GetPos()
        if ( SERVER and IsValid(self.Beam) ) then
            self.Beam:GetTable():SetEndPos( endpos )
        end

    end

    local vVel = (endpos - self.Owner:GetPos())
    local Distance = endpos:Distance(self.Owner:GetPos())

    local et = (self.startTime + (Distance/self.speed))
    if(self.dtfix != 0) then
    self.dtfix = (et - CurTime()) / (et - self.startTime)
    end
    if(self.dtfix < 0) then
        self.Weapon:EmitSound( sndPowerUp )
        self.dtfix = 0
    end

    if(self.dtfix == 0) then
        zVel = self.Owner:GetVelocity().z
        vVel = vVel:GetNormalized()*(math.Clamp(Distance,0,25))
        if( SERVER ) then
            if  self.NextAmmoTake < CurTime() then
                self:TakePrimaryAmmo(1)
                self.NextAmmoTake = CurTime() + 0.2
                self.NextAmmoGive = CurTime() + 1.2
            end

            local gravity = GetConVarNumber("sv_gravity")

            vVel:Add(Vector(0,0,(gravity/50)*1.5))

            if(zVel < 0) then
                vVel:Sub(Vector(0,0,zVel/100))
            end
            self.Owner:SetVelocity(vVel)
        end
    end

    endpos = nil

    self.Owner:LagCompensation( false )

end

function SWEP:EndAttack( shutdownsound )

    if ( shutdownsound ) then
        self.Weapon:EmitSound( sndPowerDown )
    end

    if ( CLIENT ) then return end
    if ( !self.Beam ) then return end
    if( IsValid( self.Beam) ) then
        self.Beam:Remove()
    end
    self.Beam = nil

end
