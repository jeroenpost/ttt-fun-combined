if SERVER then
   AddCSLuaFile(  )
   --resource.AddFile("materials/vgui/ttt_fun_killicons/m249.png")
end

SWEP.HoldType			= "crossbow"

SWEP.PrintName			= "Orb's Shredder"
 SWEP.Slot				= 0
if CLIENT then

   
  
   SWEP.Icon = "vgui/ttt_fun_killicons/m249.png"
   SWEP.ViewModelFlip		= false
end

SWEP.CanDecapitate= true
SWEP.Base				= "gb_camo_base"
SWEP.Camo = 7
SWEP.Spawnable = true
SWEP.AdminSpawnable = true

SWEP.Kind = WEAPON_MELEE
SWEP.WeaponID = AMMO_M249


SWEP.Primary.Damage = 8
SWEP.Primary.Delay = 0.05
SWEP.Primary.Cone = 0.085
SWEP.Primary.ClipSize = 250
SWEP.Primary.ClipMax = 250
SWEP.Primary.DefaultClip	= 250
SWEP.Primary.Automatic		= true
SWEP.Primary.Ammo			= "smg1"
SWEP.AmmoEnt = "item_ammo_smg1_ttt"
SWEP.AutoSpawnable      = false
SWEP.Primary.Recoil			= 0.8
SWEP.Primary.Sound			= Sound("Weapon_m249.Single")
SWEP.UseHands			= true
SWEP.ViewModelFlip		= false

SWEP.ViewModelFOV			= 80
SWEP.ViewModelFlip			= false
SWEP.ViewModel				= "models/weapons/v_deat_m249para.mdl"
SWEP.WorldModel				= "models/weapons/w_blopsminigun.mdl"

SWEP.IronSightsPos = Vector(0, 0, 0)
SWEP.IronSightsAng = Vector(0, 0, 0)
SWEP.SightsPos = Vector(0, 0, 0)
SWEP.SightsAng = Vector(0, 0, 0)
SWEP.RunSightsPos = Vector(0, 1.496, 4.645)
SWEP.RunSightsAng = Vector(-23.425, 0, 0)
SWEP.Offset = {
    Pos = {
        Up = 2,
        Right = 32,
        Forward = 0.0,
    },
    Ang = {
        Up = -10,
        Right = 0,
        Forward = 270,
    }
}

function SWEP:Initialize()

    util.PrecacheSound(self.Primary.Sound)
    self.Reloadaftershoot = 0 				-- Can't reload when firing
    self:SetHoldType("shotgun")
    self.BaseClass.Initialize(self)
end

function SWEP:OnDrop()
    --self:Remove()
end

function SWEP:DrawWorldModel( )
    local hand, offset, rotate

    if not IsValid( self.Owner ) then
        self:DrawModel( )
        return
    end

    if not self.Hand then
        self.Hand = self.Owner:LookupAttachment( "anim_attachment_rh" )
    end

    hand = self.Owner:GetAttachment( self.Hand )

    if not hand then
        self:DrawModel( )
        return
    end

    offset = hand.Ang:Right( ) * self.Offset.Pos.Right + hand.Ang:Forward( ) * self.Offset.Pos.Forward + hand.Ang:Up( ) * self.Offset.Pos.Up

    hand.Ang:RotateAroundAxis( hand.Ang:Right( ), self.Offset.Ang.Right )
    hand.Ang:RotateAroundAxis( hand.Ang:Forward( ), self.Offset.Ang.Forward )
    hand.Ang:RotateAroundAxis( hand.Ang:Up( ), self.Offset.Ang.Up )

    self:SetRenderOrigin( hand.Pos + offset )
    self:SetRenderAngles( hand.Ang )

    self:DrawModel( )
end
SWEP.HeadshotMultiplier = 2.2

SWEP.IronSightsPos = Vector(-5.96, -5.119, 2.349)
SWEP.IronSightsAng = Vector(0, 0, 0)


function SWEP:PreDrawViewModel( vm, wep, ply )
    if not IsValid(vm ) then return end
    vm:SetMaterial( "camos/camo7" )

end
