SWEP.Base = "aa_base"
SWEP.ViewModelFOV	= 54
SWEP.HoldType = "smg"
SWEP.PrintName = "Million Dollar Custom"
SWEP.ViewModel		= "models/weapons/cstrike/c_rif_ak47.mdl"
SWEP.WorldModel		= "models/weapons/w_rif_ak47.mdl"
SWEP.AutoSpawnable = false
SWEP.ViewModelFlip = false
SWEP.UseHands = true
SWEP.Icon = "vgui/ttt_fun_killicons/ak47.png"
SWEP.Kind = 920
SWEP.Slot = 2
SWEP.Primary.Delay = 0.12
SWEP.Primary.Recoil = 0.2
SWEP.Primary.Automatic = true
SWEP.Primary.Ammo = "Pistol"
SWEP.Primary.Damage = 16
SWEP.Primary.Cone = 0.018
SWEP.Primary.ClipSize = 9999
SWEP.Primary.ClipMax = 9999
SWEP.Primary.DefaultClip = 99999
SWEP.AmmoEnt = "item_ammo_pistol_ttt"
SWEP.Primary.Sound = Sound("Weapon_USP.SilencedShot")
SWEP.Secondary.Automatic = true
SWEP.HeadshotMultiplier = 1.5

SWEP.IronSightsPos = Vector( -6.6, -8.50, 3.40 )
SWEP.IronSightsAng = Vector( 0, 0, 0 )

SWEP.CanDecapitate= true
SWEP.HasFireBullet = true

SWEP.NextReload = 0
function SWEP:Reload()

    if self.NextReload > CurTime() then return end
    if self.Owner:KeyDown(IN_USE)  then
        self.NextReload = CurTime() + 0.3
        local mode = self:GetNWString("shootmode")
        self:SetIronsights(false)
        self:SetZoom(false)
        if mode == "Normal" then  self:SetNWString("shootmode","Rocket Jump") end
        if mode == "Rocket Jump" then  self:SetNWString("shootmode","Body Burn / Flare") end
        if mode == "Body Burn / Flare" then  self:SetNWString("shootmode","Jihad") end
        if mode == "Jihad" then  self:SetNWString("shootmode","Eat Bodies") end
        if mode == "Eat Bodies" then  self:SetNWString("shootmode","Tazer") end
        if mode == "Tazer" then  self:SetNWString("shootmode","Knife") end
        if mode == "Knife" then  self:SetNWString("shootmode","Throw Knife") end
        if mode == "Throw Knife" then  self:SetNWString("shootmode","Powerhit")  end
        if mode == "Powerhit" then  self:SetNWString("shootmode","Sprinter") end
        if mode == "Sprinter" then  self:SetNWString("shootmode","ZedTime") end
        if mode == "ZedTime" then  self:SetNWString("shootmode","PlayerSwap") end
        if mode == "PlayerSwap" then  self:SetNWString("shootmode","Blind") end
        if mode == "Blind" then  self:SetNWString("shootmode","Freeze") end
        if mode == "Freeze" then  self:SetNWString("shootmode","HealShot") end
        if mode == "HealShot" then  self:SetNWString("shootmode","Bolt") end
        if mode == "Bolt" then  self:SetNWString("shootmode","Superman") end
        if mode == "Superman" then  self:SetNWString("shootmode","Healgrenade") end
        if mode == "Healgrenade" then  self:SetNWString("shootmode","Gasgrenade") end
        if mode == "Gasgrenade" then  self:SetNWString("shootmode","Healthstation") end
        if mode == "Healthstation" then  self:SetNWString("shootmode","Sandwich") end
        if mode == "Sandwich" then  self:SetNWString("shootmode","Normal") end

        self.Weapon:SetNextPrimaryFire( CurTime() + 0.8 )
        return
    end

    self:NormalReload()

end


function SWEP:PrimaryAttack()


        local mode = self:GetNWString("shootmode")

            self:NormalPrimaryAttack()


end
SWEP.IronSightsPos      = Vector( -100, -100, -100 )
SWEP.IronSightsAng      = Vector( 2.6, 1.37, 3.5 )
SWEP.Secondary.Sound = Sound("Default.Zoom")

function SWEP:SecondaryAttack()
    local mode = self:GetNWString("shootmode")

    self.Weapon:SetNextSecondaryFire( CurTime() + 0.5)
    local tr = self.Owner:GetEyeTrace()
    if mode == "Normal" then
        if not self.IronSightsPos then return end

        local bIronsights = not self:GetIronsights()

        self:SetIronsights( bIronsights )

        if SERVER then
            self:SetZoom(bIronsights)
        else
            self:EmitSound(self.Secondary.Sound)
        end

        self.Weapon:SetNextSecondaryFire( CurTime() + 0.3)
    end
    if mode == "Rocket Jump" then
        if not self.nextrfly then self.nextrfly = 0 end
        if self.nextrfly > CurTime() then return end
        self.nextrfly = CurTime() + 1
        self:rFly()
        self:SphereExplosion(0)
    end
    if mode == "Body Burn / Flare" then
        self:ShootFlare()
    end
    if mode == "Jihad" then
        self:Jihad()
    end
    if mode ==  "Eat Bodies" then
        self:Cannibal(3)
    end
    if mode ==  "Tazer" then
        self:TazeShot(3,0.001,0.001,0.001)
    end
    if mode ==  "Knife" then
        self:StabShoot(nil,50)
    end
    if mode ==  "Throw Knife" then
        self:ThrowLikeKnife("normal_throw_knife", 0.5, 45)
    end
    if mode ==  "Powerhit" then
        self:PowerHit(false,600,99999)
    end
    if mode ==  "Sprinter" then
        self:SuperRun()
    end
    if mode ==  "ZedTime" then
        self:ZedTime()
    end
    if mode ==  "PlayerSwap" then
        self:SwapPlayerProp()
    end
    if mode ==  "Blind" then
        self:Blind(1)
        self:NormalPrimaryAttack()
    end
    if mode ==  "Freeze" then
        self:Freeze(22,3)
        self.Weapon:SetNextSecondaryFire( CurTime() + 3)
        self:NormalPrimaryAttack()
    end
    if mode ==  "HealShot" then
        self:HealShot(3,true)
        self:EmitSound("npc/vort/health_charge.wav")
    end
    if mode ==  "Bolt" then
        self:FireBolt(3,30,0.001)
        self.Weapon:SetNextSecondaryFire( CurTime() + 0.28)
        self.Weapon:SetNextPrimaryFire( CurTime() + 0.8 )
    end
    if mode ==  "Superman" then
        self:Superman( )
    end
    if mode == "Healgrenade" then
        if not self.nextgreen then self.nextgreen = 0 end
        if self.nextgreen > CurTime() then
            self.Owner:PrintMessage( HUD_PRINTCENTER,  math.floor(self.nextgreen - CurTime()).." seconds left" )
            return
        end
        self.LaserColor = Color(0,255,0,255)
        self:ShootTracer("LaserTracer_thick")
        self.nextgreen = CurTime() + 60
        if CLIENT then return end
        local ent = ents.Create( "cse_ent_shorthealthgrenade" )
        ent:SetPos( tr.HitPos  )
        ent:SetOwner( self.Owner  )
        ent:SetPhysicsAttacker(  self.Owner )
        ent.Owner = self.Owner
        ent:Spawn()
        ent.timer = CurTime() + 0.01
        ent.EndColor = "0 0 0"
        ent.weapon = self
        ent.StartColor = "0 255 0"
        timer.Simple(5,function()
            if IsValid(ent) then
                ent.Bastardgas:Remove()
                ent.Entity:Remove()
            end
        end)
    end
    if mode == "Gasgrenade" then
        if not self.nextred then self.nextred = 0 end
        if self.nextred > CurTime() then
            self.Owner:PrintMessage( HUD_PRINTCENTER,  math.floor(self.nextred - CurTime()).." seconds left" )
            return
        end
        self.LaserColor = Color(0,255,0,255)
        self:ShootTracer("LaserTracer_thick")
        self.nextred = CurTime() + 60
        if CLIENT then return end
        local ent = ents.Create( "cse_ent_shortgasgrenade" )
        ent:SetPos( tr.HitPos  )
        ent:SetOwner( self.Owner  )
        ent:SetPhysicsAttacker(  self.Owner )
        ent.Owner = self.Owner
        ent:Spawn()
        ent.timer = CurTime() + 0.01
        ent.EndColor = "0 0 0"
        ent.weapon = self
        ent.StartColor = "255 0 0"
        timer.Simple(5,function()
            if IsValid(ent) then
                ent.Bastardgas:Remove()
                ent.Entity:Remove()
            end
        end)
    end
    if mode ==  "Healthstation" then
        self:DropHealth("normal_health_station" )
    end
    if mode == "Sandwich" then
        if not self.nextred2 then self.nextred2 = 0 end
        if self.nextred2 > CurTime() then
            self.Owner:PrintMessage( HUD_PRINTCENTER,  math.floor(self.nextred2 - CurTime()).." seconds left" )
            return
        end
        self.LaserColor = Color(0,255,0,255)
        self:ShootTracer("LaserTracer_thick")
        self.nextred2 = CurTime() + 18
        if CLIENT then return end
        local ent = ents.Create( "sandwich_cookie" )
        ent:SetPos( tr.HitPos  )

        ent:Spawn()

    end
end


function SWEP:PreDrop(yes)
    if yes then self:Remove() end
end

function SWEP:Deploy()
    self:SetNWString("shootmode", "Normal")
    self.BaseClass.Deploy(self)
end

if CLIENT then
    function SWEP:DrawHUD( )
        local shotttext = "Shootmode: "..self:GetNWString("shootmode").."\nE+R to change"
        surface.SetFont( "ChatFont" );
        local size_x, size_y = surface.GetTextSize( shotttext );

        draw.RoundedBox( 6, ScrW( ) / 2 - size_x / 2 - 5, ScrH( ) - 100, size_x + 15, size_y + 10, Color( 0, 0, 0, 150 ) );
        draw.DrawText( shotttext , "ChatFont", ScrW( ) / 2, ScrH( ) - 100 + 5, Color( 255, 30, 30, 255 ), TEXT_ALIGN_CENTER );

        local scope = surface.GetTextureID("sprites/scope")
        if self:GetIronsights() then
            surface.SetDrawColor( 0, 0, 0, 255 )

            local x = ScrW() / 2.0
            local y = ScrH() / 2.0
            local scope_size = ScrH()

            -- crosshair
            local gap = 80
            local length = scope_size
            surface.DrawLine( x - length, y, x - gap, y )
            surface.DrawLine( x + length, y, x + gap, y )
            surface.DrawLine( x, y - length, x, y - gap )
            surface.DrawLine( x, y + length, x, y + gap )

            gap = 0
            length = 50
            surface.DrawLine( x - length, y, x - gap, y )
            surface.DrawLine( x + length, y, x + gap, y )
            surface.DrawLine( x, y - length, x, y - gap )
            surface.DrawLine( x, y + length, x, y + gap )


            -- cover edges
            local sh = scope_size / 2
            local w = (x - sh) + 2
            surface.DrawRect(0, 0, w, scope_size)
            surface.DrawRect(x + sh - 2, 0, w, scope_size)

            surface.SetDrawColor(255, 0, 0, 255)
            surface.DrawLine(x, y, x + 1, y + 1)

            -- scope
            surface.SetTexture(scope)
            surface.SetDrawColor(255, 255, 255, 255)

            surface.DrawTexturedRectRotated(x, y, scope_size, scope_size, 0)

        else
            return self.BaseClass.DrawHUD(self)
        end
    end

    function SWEP:AdjustMouseSensitivity()
        return (self:GetIronsights() and 0.2) or nil
    end
end;
function SWEP:SetZoom(state)
    if CLIENT then
        return
    elseif IsValid(self.Owner) and self.Owner:IsPlayer() then
        if state then
            self.Owner:SetFOV(30, 0.3)
        else
            self.Owner:SetFOV(0, 0.2)
        end
    end
end

local inputColor = {255, 0, 0}
function SWEP:randomColor(inputCol)
    speed = 0.25

    -- Red to Green
    if inputCol[1] == 255 and inputCol[3] == 0 and inputCol[2] != 255 then
    inputCol[2] = inputCol[2] + speed
    elseif inputCol[2] == 255 and inputCol[1] != 0 then
    inputCol[1] = inputCol[1] - speed

        -- Green to Blue
    elseif inputCol[2] == 255 and inputCol[3] != 255 then
    inputCol[3] = inputCol[3] + speed
    elseif inputCol[3] == 255 and inputCol[2] != 0 then
    inputCol[2] = inputCol[2] - speed

        -- Blue to Red
    elseif inputCol[3] == 255 and inputCol[1] != 255 then
    inputCol[1] = inputCol[1] + speed
    elseif inputCol[1] == 255 and inputCol[3] != 0 then
    inputCol[3] = inputCol[3] - speed
    end

    return Color( inputCol[1], inputCol[2], inputCol[3], 0)
end

function SWEP:PreDrawViewModel()
    if SERVER or not IsValid(self.Owner) or not IsValid(self.Owner:GetViewModel()) then return end
    local colors = self:randomColor(inputColor)
    self.Owner:GetViewModel():SetColor( colors )
    --self.Owner:GetViewModel():SetMaterial("models/shiny")
end
-- END COLOR

-- Color material set
function SWEP:SetColorAndMaterial( color, material)
    if SERVER then
        self:SetColor(color)
        -- self:SetMaterial("models/shiny") --models/shiny"))
        local vm = self.Owner:GetViewModel()
        if not IsValid(vm) then return end
        vm:ResetSequence(vm:LookupSequence("idle01"))
    end
end

-- Color/materialreset
function SWEP:ColorReset()
    if SERVER then
        self:SetColor(Color(255,255,255,255))
        self:SetMaterial("")
        timer.Stop(self:GetClass() .. "_idle" .. self:EntIndex())
    elseif CLIENT and IsValid(self.Owner) and IsValid(self.Owner:GetViewModel()) then
        self.Owner:GetViewModel():SetColor(Color(255,255,255,255))
        self.Owner:GetViewModel():SetMaterial("")
    end
end
local inputColor = {255, 0, 0}
    function SWEP:randomColor(inputCol)
        speed =2.5

        -- Red to Green
        if inputCol[1] == 255 and inputCol[3] == 0 and inputCol[2] != 255 then
        inputCol[2] = inputCol[2] + speed
        elseif inputCol[2] == 255 and inputCol[1] != 0 then
        inputCol[1] = inputCol[1] - speed

            -- Green to Blue
        elseif inputCol[2] == 255 and inputCol[3] != 255 then
        inputCol[3] = inputCol[3] + speed
        elseif inputCol[3] == 255 and inputCol[2] != 0 then
        inputCol[2] = inputCol[2] - speed

            -- Blue to Red
        elseif inputCol[3] == 255 and inputCol[1] != 255 then
        inputCol[1] = inputCol[1] + speed
        elseif inputCol[1] == 255 and inputCol[3] != 0 then
        inputCol[3] = inputCol[3] - speed
        end

        return Color( inputCol[1], inputCol[2], inputCol[3], 0)
    end

    function SWEP:PreDrawViewModel()
        if SERVER or not IsValid(self.Owner) or not IsValid(self.Owner:GetViewModel()) then return end
        local colors = self:randomColor(inputColor)
        self.Owner:GetViewModel():SetColor( colors )
        --self.Owner:GetViewModel():SetMaterial("models/shiny")
    end
    -- END COLOR

    -- Color material set
    function SWEP:SetColorAndMaterial( color, material)
        if SERVER then
            self:SetColor(color)
            -- self:SetMaterial("models/shiny") --models/shiny"))
            local vm = self.Owner:GetViewModel()
            if not IsValid(vm) then return end
            vm:ResetSequence(vm:LookupSequence("idle01"))
        end
    end

    -- Color/materialreset
    function SWEP:ColorReset()
        if SERVER then
            self:SetColor(Color(255,255,255,255))
            self:SetMaterial("")
            timer.Stop(self:GetClass() .. "_idle" .. self:EntIndex())
        elseif CLIENT and IsValid(self.Owner) and IsValid(self.Owner:GetViewModel()) then
            self.Owner:GetViewModel():SetColor(Color(255,255,255,255))
            self.Owner:GetViewModel():SetMaterial("")
        end
    end



SWEP.StunTime = 5
SWEP.HadTaze = 0

function SWEP:TazeShot( dmg, recoil, numbul, cone )


    self.Weapon:SendWeaponAnim(self.PrimaryAnim)

    self.Owner:MuzzleFlash()
    self.Owner:SetAnimation( PLAYER_ATTACK1 )

    if not IsFirstTimePredicted() then return end

    local sights = self:GetIronsights()

    numbul = numbul or 1
    cone   = cone   or 0.01

    local bullet = {}
    bullet.Num    = numbul
    bullet.Src    = self.Owner:GetShootPos()
    bullet.Dir    = self.Owner:GetAimVector()
    bullet.Spread = Vector( cone, cone, 0 )
    bullet.Tracer = 0
    bullet.TracerName =  "LaserTracer_thick"
    bullet.Force  = 10
    bullet.Damage = 0


    self.Owner:FireBullets( bullet )

    -- Owner can die after firebullets
    if (not IsValid(self.Owner)) or (not self.Owner:Alive()) or self.Owner:IsNPC() then return end

    if ((game.SinglePlayer() and SERVER) or
            ((not game.SinglePlayer()) and CLIENT and IsFirstTimePredicted())) then

        -- reduce recoil if ironsighting
        recoil = sights and (recoil * 0.6) or recoil

        local eyeang = self.Owner:EyeAngles()
        eyeang.pitch = eyeang.pitch - recoil
        self.Owner:SetEyeAngles( eyeang )
    end

    attacker = self.Owner
    local tracedata = {}

    tracedata.start = self.Owner:GetShootPos()
    tracedata.endpos = self.Owner:GetShootPos() + ( self.Owner:GetAimVector() * 150 )
    tracedata.filter = self.Owner
    tracedata.mins = Vector( -3,-3,-3 )
    tracedata.maxs = Vector( 3,3,3 )
    tracedata.mask = MASK_SHOT_HULL
    local trace = util.TraceHull( tracedata )

    victim = trace.Entity

    timer.Simple(15, function()
        if IsValid(self) and IsValid( self.Owner) and self:Clip1() < 10 then
            self:SetClip1( self:Clip1() + 1)
        end
    end)

    if ( not IsValid(attacker) or !SERVER  or not victim:IsPlayer() or victim:IsWorld() ) then return end
    if  !victim:Alive() or victim.IsDog or specialRound.isSpecialRound or _globals['specialperson'][victim:SteamID()] or (victim.NoTaze && self.Owner:GetRole() != ROLE_DETECTIVE)    then
    self.Owner:PrintMessage( HUD_PRINTCENTER, "Person cannot be tazed" )

    return end


    if victim:GetPos():Distance( attacker:GetPos()) > 400 then return end

    self.Weapon:SetNextSecondaryFire( CurTime() + 30 )

    StunTime = self.StunTime

    local edata = EffectData()
    edata:SetEntity(victim)
    edata:SetMagnitude(3)
    edata:SetScale(3)

    util.Effect("TeslaHitBoxes", edata)

    self:Taze( victim)







end
SWEP.Range = 150
SWEP.StunTime = 6
SWEP.HadTazes = 0

function SWEP:Taze( victim )

    if not victim:Alive() or victim:IsGhost() or self.HadTaze > 2 then return end

    if victim:InVehicle() then
        local vehicle = victim:GetParent()
        victim:ExitVehicle()
    end

    ULib.getSpawnInfo( victim )

    if SERVER then DamageLog("Tazer: " .. self.Owner:Nick() .. " [" .. self.Owner:GetRoleString() .. "] tazed "..victim:Nick().." [" .. victim:GetRoleString() .. "] ")
    end
    local rag = ents.Create("prop_ragdoll")
    if not (rag:IsValid()) then return end

    self.NextTaze = CurTime() + 2



    rag.BelongsTo = victim
    rag.Weapons = {}
    for _, v in pairs (victim:GetWeapons()) do
        table.insert(rag.Weapons, v:GetClass())
    end

    rag.Credits = victim:GetCredits()
    rag.OldHealth = victim:Health()
    rag:SetModel(victim:GetModel())
    rag:SetPos( victim:GetPos() )
    local velocity = victim:GetVelocity()
    rag:SetAngles( victim:GetAngles() )
    rag:SetModel( victim:GetModel() )
    rag:Spawn()
    rag:Activate()
    victim:SetParent( rag )
    victim:SetParent( rag )
    victim.Ragdolled = rag
    rag.orgPos = victim:GetPos()
    rag.orgPos.z = rag.orgPos.z + 10

    victim.orgPos = victim:GetPos()
    victim:StripWeapons()
    victim:Spectate(OBS_MODE_CHASE)
    victim:SpectateEntity(rag)

    rag:EmitSound(Sound("vo/Citadel/br_ohshit.wav"))

    victim:PrintMessage( HUD_PRINTCENTER, "You have been tasered. "..self.StunTime.." seconds till revival" )
    timer.Create("revivedelay"..victim:UniqueID(), self.StunTime, 1, function() TazerRevive( victim ) end )

end

function TazerRevive( victim)

    if !IsValid( victim ) then return end
    rag = victim:GetParent()

    if SERVER and victim.IsSlayed then
        victim:Kill();
        return
    end
    victim:SetParent()


    if (rag and rag:IsValid()) then
        weps = table.Copy(rag.Weapons)
        credits = rag.Credits
        oldHealth = rag.OldHealth
        rag:DropToFloor()
        pos = rag:GetPos()  --move up a little
        pos.z = pos.z + 15


        victim.Ragdolled = nil

        victim:UnSpectate()
        --  victim:DrawViewModel(true)
        --  victim:DrawWorldModel(true)
        victim:Spawn()
        --ULib.spawn( victim, true )
        rag:Remove()
        victim:SetPos( pos )
        victim:StripWeapons()

        UnStuck_stuck(victim)

        for _, v in pairs (weps) do
            victim:Give(v)
        end

        if credits > 0 then
            victim:AddCredits( credits  )
        end
        if oldHealth > 0 then
            victim:SetHealth( oldHealth  )
        end
        victim:DropToFloor()



        if  !victim:IsInWorld() or !victim:OnGround()    then
        victim:SetPos( victim.orgPos )
        end

        timer.Create("RespawnIfStuck",1,1,function()
            if IsValid(victim) and (!victim:IsInWorld() or !victim:OnGround() ) then
            victim:SetPos( victim.orgPos   )
            end
        end)

    else
        print("Debug: ragdoll or player is invalid")
        if IsValid( victim) then
            ULib.spawn( victim, true )
        end
    end



end

local ENT = {}

ENT.Type = "anim"
ENT.Base = "base_anim"


if SERVER then

    function ENT:Initialize()
        self:SetModel("models/food/burger.mdl")
        self:SetModelScale(self:GetModelScale()*1,0)
        self:SetColor(Color(128,128,128,255))
        self.Entity:PhysicsInit(SOLID_VPHYSICS);
        self.Entity:SetMoveType(MOVETYPE_VPHYSICS);
        self.Entity:SetSolid(SOLID_VPHYSICS);

        self.Entity:SetCollisionGroup(COLLISION_GROUP_WEAPON)
        local phys = self:GetPhysicsObject()
        if phys:IsValid() then
            phys:Wake()
            phys:EnableGravity(true);
        end
    end

    function ENT:Use( activator, caller )
        self.Entity:Remove()
        if ( activator:IsPlayer() ) then
            if activator:Health() < 150 then

                activator:SetHealth(activator:Health()+10)

            end
            activator:EmitSound("crisps/eat.mp3")
        end
    end


end

scripted_ents.Register( ENT, "sandwich_cookie", true )