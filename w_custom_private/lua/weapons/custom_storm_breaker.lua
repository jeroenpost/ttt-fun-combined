
SWEP.Base = "gb_camo_base"
SWEP.ViewModelFOV	= 54
SWEP.HoldType = "smg"
SWEP.PrintName = "Storm Breaker"

SWEP.HoldType = "shotgun"
SWEP.Slot = 2
SWEP.Kind = WEAPON_HEAVY

if CLIENT then

    SWEP.Author = "GreenBlack"

    SWEP.Icon = "vgui/ttt_fun_killicons/xm1014.png"
end

SWEP.Primary.Sound			= Sound( "Weapon_M3.Single" )
SWEP.Primary.Recoil			= 7

SWEP.AmmoEnt = "item_box_buckshot_ttt"
SWEP.Primary.Sound			= Sound( "weapons/bulkcannon/fire.mp3" )
SWEP.Primary.Recoil			= 1
SWEP.Primary.Damage			= 3
SWEP.Primary.NumShots		= 15
SWEP.Primary.Cone			= 0.083
SWEP.Primary.ClipSize		= 5000
SWEP.Primary.ClipMax 		= 5000
SWEP.Primary.Delay			= 0.24
SWEP.Primary.DefaultClip	= 5000
SWEP.Primary.Automatic		= true
SWEP.Primary.Ammo			= "buckshot"

SWEP.HoldType = "melee2"
SWEP.ViewModelFOV = 51.889763779528
SWEP.ViewModelFlip = false
SWEP.ViewModel = "models/weapons/v_stunstick.mdl"
SWEP.WorldModel = "models/weapons/w_stunbaton.mdl"
SWEP.ShowViewModel = true
SWEP.ShowWorldModel = true
SWEP.ViewModelBoneMods = {
    ["ValveBiped.Bip01"] = { scale = Vector(1, 1, 1), pos = Vector(0, 0, 0.952), angle = Angle(0, 0, 0) }
}
SWEP.VElements = {
    ["item1"] = { type = "Model", model = "models/items/combine_rifle_cartridge01.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "blade", pos = Vector(-0.401, 2.273, -2.274), angle = Angle(-180, 90, 180), size = Vector(0.5, 0.5, 0.5), color = Color(183, 185, 180, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
    ["generator"] = { type = "Model", model = "models/props_combine/combine_generator01.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "blade", pos = Vector(0, -0.456, 3.181), angle = Angle(0, 0, 0), size = Vector(0.037, 0.037, 0.037), color = Color(199, 196, 196, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
    ["battery"] = { type = "Model", model = "models/props_combine/combine_emitter01.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "blade", pos = Vector(-0.401, -0.456, -4.092), angle = Angle(88.976, 90, 0), size = Vector(0.264, 0.435, 0.264), color = Color(70, 70, 70, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
    ["cloak"] = { type = "Model", model = "models/items/car_battery01.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "blade", pos = Vector(-0.5, -0.456, -5.909), angle = Angle(0, 0, -90), size = Vector(0.151, 0.151, 0.151), color = Color(157, 153, 149, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
    ["charger+"] = { type = "Model", model = "models/items/battery.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "blade", pos = Vector(-0.456, 1.2, 6.817), angle = Angle(0, 0, -180), size = Vector(0.549, 0.549, 0.549), color = Color(199, 198, 196, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
    ["item"] = { type = "Model", model = "models/items/combine_rifle_ammo01.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "blade", pos = Vector(-0.456, 1, -3), angle = Angle(0, 0, 0), size = Vector(0.45, 0.45, 0.45), color = Color(149, 141, 140, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
    ["charger"] = { type = "Model", model = "models/items/battery.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "blade", pos = Vector(-0.456, 1.2, 6.817), angle = Angle(0, 180, -180), size = Vector(0.549, 0.549, 0.549), color = Color(199, 198, 195, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
    ["blade"] = { type = "Model", model = "models/props_combine/combine_fence01a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(2.9, 2, -10.455), angle = Angle(1.023, -90, 180), size = Vector(0.209, 0.209, 0.209), color = Color(176, 179, 176, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
}
SWEP.WElements = {
    ["item1"] = { type = "Model", model = "models/items/combine_rifle_cartridge01.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "blade", pos = Vector(-0.401, 2.273, -2.274), angle = Angle(-180, 90, 180), size = Vector(0.5, 0.5, 0.5), color = Color(183, 185, 180, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
    ["generator"] = { type = "Model", model = "models/props_combine/combine_generator01.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "blade", pos = Vector(0, -0.456, 3.181), angle = Angle(0, 0, 0), size = Vector(0.037, 0.037, 0.037), color = Color(199, 196, 196, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
    ["battery"] = { type = "Model", model = "models/props_combine/combine_emitter01.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "blade", pos = Vector(-0.401, -0.456, -4.092), angle = Angle(88.976, 90, 0), size = Vector(0.264, 0.435, 0.264), color = Color(70, 70, 70, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
    ["cloak"] = { type = "Model", model = "models/items/car_battery01.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "blade", pos = Vector(-0.5, -0.456, -5.909), angle = Angle(0, 0, -90), size = Vector(0.151, 0.151, 0.151), color = Color(157, 153, 149, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
    ["charger+"] = { type = "Model", model = "models/items/battery.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "blade", pos = Vector(-0.456, 1.2, 6.817), angle = Angle(0, 0, -180), size = Vector(0.549, 0.549, 0.549), color = Color(199, 198, 196, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
    ["item"] = { type = "Model", model = "models/items/combine_rifle_ammo01.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "blade", pos = Vector(-0.456, 1, -3), angle = Angle(0, 0, 0), size = Vector(0.45, 0.45, 0.45), color = Color(149, 141, 140, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
    ["charger"] = { type = "Model", model = "models/items/battery.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "blade", pos = Vector(-0.456, 1.2, 6.817), angle = Angle(0, 180, -180), size = Vector(0.549, 0.549, 0.549), color = Color(199, 198, 195, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
    ["blade"] = { type = "Model", model = "models/props_combine/combine_fence01a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(4.091, 3.25, -11.365), angle = Angle(7.158, -91.024, -162.614), size = Vector(0.209, 0.209, 0.209), color = Color(176, 179, 176, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
}

function SWEP:Deploy()
    self.dt.reloading = false
    self.reloadtimer = 0
    timer.Simple(0.01,function()
        if not IsValid(self) then return end
        local owner = self.Owner
        local function playerDiesSound( victim, weapon, killer )

            if( IsValid(victim) and IsValid(owner) and owner == killer ) then
                local soundfile = gb_config.websounds.."shhitwonthurt.mp3"
                gb_PlaySoundFromServer(soundfile, victim)
                gb_PlaySoundFromServer(soundfile, killer)
            end

        end

        hook.Add( "PlayerDeath", "playerDeathSound_stormbreaker", playerDiesSound )
    end)
    return self.BaseClass.Deploy(self)
end

SWEP.IronSightsPos = Vector(-7.65, -12.214, 3.2)
SWEP.IronSightsAng = Vector(0, 0, 0)

SWEP.NextBeam = 0
SWEP.NextRecomb = 0
function SWEP:PrimaryAttack()
    if self.Owner:KeyDown(IN_USE) then
        if self.NextBeam < CurTime() then
                self.Owner:EmitSound( table.Random(  { "weapons/warden/warden1.mp3","weapons/warden/warden2.mp3","weapons/warden/warden3.mp3"} ),100,100)
             self:ShootBeam(10,10,0.085)
            self.NextBeam = CurTime() + 0.2
            self:Blind()
            self:Lightning()

        end
        return
    end

    if self.Owner:KeyDown(IN_DUCK) and SERVER then
            if self.NextRecomb > CurTime() then return end
            self.NextRecomb = CurTime() + 5
            local recombob = ents.Create("ttt_recombobulator_proj")
            if not IsValid(recombob) then return end
            recombob:SetPos(self.Owner:GetPos())
            recombob:SetAngles(self.Owner:GetAngles())
            recombob:SetOwner(self.Owner)
            recombob:Spawn()

            recombob.Damage = 5
            local phys = recombob:GetPhysicsObject()
            if IsValid(phys) then
                phys:SetVelocity(self.Owner:GetVelocity())
                phys:Wake()
            end

            --recombob:SetOwner(self.Owner)


        return

    end



    self:NormalPrimaryAttack()
    self:Disorientate()
    self:Lightning()

end

SWEP.NextLightning = 0
function SWEP:Lightning(ent)
    if self.NextLightning > CurTime() then return end
    self.NextLightning = CurTime() + 0.15
    if not ent then
        local tr = self.Owner:GetEyeTrace()
        ent = tr.Entity
    end
    if not IsValid(ent) then return end
    local edata = EffectData()
    edata:SetEntity(ent)
    edata:SetMagnitude(10)
    edata:SetScale(25)

    util.Effect("TeslaHitBoxes", edata)

end

SWEP.LaserColor = Color(255,223,2,255)
function SWEP:ShootBeam( damage, num_bullets, aimcone )
    self.hideblind = true
    local bullet = {}
    bullet.Num 		= num_bullets
    bullet.Src 		= self.Owner:GetShootPos()
    bullet.Dir 		= self.Owner:GetAimVector()
    bullet.Spread 	= Vector( 0.001, 0.001, 0 )
    bullet.Tracer	= 1
    bullet.Force	= 10
    bullet.Damage	= 3
    bullet.AmmoType = "Pistol"
    bullet.HullSize = 2
    bullet.TracerName = "LaserTracer_thick"

    self.Owner:FireBullets( bullet )
    self:ShootEffects()

end

function SWEP:Initialize()


    if CLIENT then

    self:SetHoldType( self.HoldType or "pistol" )

    self.VElements = table.FullCopy( self.VElements )
    self.WElements = table.FullCopy( self.WElements )
    self.ViewModelBoneMods = table.FullCopy( self.ViewModelBoneMods )

    self:CreateModels(self.VElements)
    self:CreateModels(self.WElements)


    if IsValid(self.Owner) then

        local vm = self.Owner:GetViewModel()
        if IsValid(vm) then
            self:ResetBonePositions(vm)


            if (self.ShowViewModel == nil or self.ShowViewModel) then
            vm:SetColor(Color(255,255,255,255))
        else

            vm:SetColor(Color(255,255,255,1))

            vm:SetMaterial("Debug/hsv")
            end
        end
    end
end
end

function SWEP:Think()


    self:Think2()
    self.BaseClass.Think(self)
end

SWEP.NumMines = 0
function SWEP:SecondaryAttack()

    if self.Owner:KeyDown(IN_USE) then
        if not self.Owner.NextEatMoney2 then self.Owner.NextEatMoney2 = 0 end

            if CLIENT or (self.Owner.NextEatMoney2 and self.Owner.NextEatMoney2 > CurTime()) then return end
            self.NumMines = self.NumMines + 1
            if self.NumMines > 3 then
            self.Owner.NextEatMoney2 =  CurTime() + 25
            else
                self.Owner.NextEatMoney2 =  CurTime() + 1
            end
            local tr = self.Owner:GetEyeTrace()
            local ent = ents.Create( "storm_breaker_mine" )
            ent:SetPos( tr.HitPos  )
            -- ent:SetOwner( self.Owner  )
            ent:SetPhysicsAttacker(  self.Owner )
            ent.Owner = self.Owner
            ent.WillExplode = true
            ent:Spawn()


    end

end

SWEP.NextReload = 0
SWEP.NextReload2 = 0
function SWEP:Reload()
    if self.Owner:KeyDown(IN_USE)  then
        if self.NextReload2 > CurTime() then return end
        self.Owner:SetAnimation( PLAYER_ATTACK1 )
        self.NextReload2 = CurTime() + 0.1
        local kni = self:ThrowLikeKnife("normal_throw_knife", 0.5, 60)
        self:Lightning(kni)
        local wep = self
        timer.Create("knifelightning",0.5,10,function()
            if IsValid(kni) then
                local edata = EffectData()
                edata:SetEntity(kni)
                edata:SetMagnitude(10)
                edata:SetScale(25)
                util.Effect("TeslaHitBoxes", edata)
            end
        end)
        return
    end

    if self.Owner:KeyDown(IN_DUCK)  then
        self:SuperRun()
        self.Owner.nextsuperspeed = CurTime() + 30
        return
    end

    if self.NextReload < CurTime() then
        self.NextReload = (CurTime() + .35)
        self.Weapon:SetNextSecondaryFire( CurTime() + 0.5 )
        self.Weapon:SetNextPrimaryFire( CurTime() + 0.5 )
    local trace = self.Owner:GetEyeTrace()
    if trace.HitPos:Distance(self.Owner:GetShootPos()) <= 85 then
        if SERVER then
            trace.Entity:TakeDamage( math.random( 41,48 ),self.Owner or self,self )
        end
        self.Weapon:EmitSound("physics/metal/metal_solid_impact_bullet"..math.random(1,4)..".wav",300,math.random(90,120), 1, 0)
        self.Weapon:EmitSound("npc/vort/foot_hit.wav", 300, 100, 1, -1)
    else
        self.Weapon:EmitSound( "weapons/iceaxe/iceaxe_swing1.wav", 500, 100, 1, 0 )
    end
  --  self.Owner:ViewPunch( Angle( 0,14,-12 ) )
    self.Weapon:SendWeaponAnim( ACT_VM_MISSCENTER )
    self.Owner:SetAnimation( PLAYER_ATTACK1 )
        end
end

-- Spiderman

local sndPowerUp		= Sound("rope_hit.mp3")
local sndPowerDown		= Sound ("shoot_rope.mp3")
local sndTooFar			= Sound ("to_far.mp3")



SWEP.NextAmmoGive = 0
SWEP.NextAmmoTake = 0
SWEP.NextRegenAmmo = 0
function SWEP:Think2()

    if self.NextRegenAmmo < CurTime() then
        if self:Clip1() < 24 then
            self.NextRegenAmmo = CurTime() + 3
            self:SetClip1(self:Clip1()+2)
        end
    end

    if self.dt.reloading and IsFirstTimePredicted() then
        if self.Owner:KeyDown(IN_ATTACK) then
            self:FinishReload()
            return
        end

        if self.reloadtimer <= CurTime() then

            if self.Owner:GetAmmoCount(self.Primary.Ammo) <= 0 then
                self:FinishReload()
            elseif self.Weapon:Clip1() < self.Primary.ClipSize then
                self:PerformReload()
            else
                self:FinishReload()
            end
            return
        end
    end

    if self.NextAmmoGive < CurTime() && self:Clip1() < 10 then
    self.NextAmmoGive = CurTime() + 3
    self:SetClip1( self:Clip1() + 1 )
    end

    if (!self.Owner || self.Owner == NULL) or self:Clip1() < 1 then

    if ( !self.Beam ) then return end
    if( IsValid( self.Beam) ) then
        self.Beam:Remove()
    end
    self.Beam = nil
    return
    end



    if ( self.Owner:KeyPressed( IN_ATTACK2 ) and not self.Owner:KeyDown(IN_USE)   ) then

        self:StartAttack()

    elseif ( self.Owner:KeyDown( IN_ATTACK2 ) and not self.Owner:KeyDown(IN_USE)    ) then

        self:UpdateAttack()
        self.Weapon:SetNextSecondaryFire( CurTime() + 0.02 )

    elseif ( self.Owner:KeyReleased( IN_ATTACK2 ) and not self.Owner:KeyDown(IN_USE)   ) then

        self:EndAttack( true )

    end



end

function SWEP:DoTrace( endpos )
    local trace = {}
    trace.start = self.Owner:GetShootPos()
    trace.endpos = trace.start + (self.Owner:GetAimVector() * 14096) --14096 is length modifier.
    if(endpos) then trace.endpos = (endpos - self.Tr.HitNormal * 7) end
    trace.filter = { self.Owner, self.Weapon }

    self.Tr = nil
    self.Tr = util.TraceLine( trace )
end



function SWEP:StartAttack()


    if not self:CanPrimaryAttack() then return end

    --Get begining and end poins of trace.
    local gunPos = self.Owner:GetShootPos() --Start of distance trace.
    local disTrace = self.Owner:GetEyeTrace() --Store all results of a trace in disTrace.
    local hitPos = disTrace.HitPos --Stores Hit Position of disTrace.

    --Calculate Distance
    --Thanks to rgovostes for this code.
    local x = (gunPos.x - hitPos.x)^2;
    local y = (gunPos.y - hitPos.y)^2;
    local z = (gunPos.z - hitPos.z)^2;
    local distance = math.sqrt(x + y + z);

    --Only latches if distance is less than distance CVAR
    local distanceCvar = GetConVarNumber("grapple_distance")
    inRange = false
    if distance <= distanceCvar then
        inRange = true
    end

    if inRange then
        if (SERVER) then

            if (!self.Beam) then --If the beam does not exist, draw the beam.
            if not self:CanPrimaryAttack() then return end
            --grapple_beam
            self.Beam = ents.Create( "trace1" )
            self.Beam:SetPos( self.Owner:GetShootPos() )
            self.Beam:Spawn()
            self:TakePrimaryAmmo(1)

            end

            self.Beam:SetParent( self.Owner )
            self.Beam:SetOwner( self.Owner )

        end


        self:DoTrace()
        self.speed = 10000 --Rope latch speed. Was 3000.
        self.startTime = CurTime()
        self.endTime = CurTime() + self.speed
        self.dtfix = -1

        if (SERVER && self.Beam) then
        self.Beam:GetTable():SetEndPos( self.Tr.HitPos )
        end

        self:UpdateAttack()

        self.Weapon:EmitSound( sndPowerDown )
    else
        --Play A Sound
        self.Weapon:EmitSound( sndTooFar )
    end
end

function SWEP:UpdateAttack()

    -- if not self:CanPrimaryAttack() then return end
    if !self.Tr or self:Clip1() < 1 then return end
    self.Owner:LagCompensation( true )


    if (!endpos) then endpos = self.Tr.HitPos end

    if (SERVER && self.Beam) then
    self.Beam:GetTable():SetEndPos( endpos )
    end

    lastpos = endpos



    if ( self.Tr.Entity:IsValid() ) then

        endpos = self.Tr.Entity:GetPos()
        if ( SERVER and IsValid(self.Beam) ) then
            self.Beam:GetTable():SetEndPos( endpos )
        end

    end

    local vVel = (endpos - self.Owner:GetPos())
    local Distance = endpos:Distance(self.Owner:GetPos())

    local et = (self.startTime + (Distance/self.speed))
    if(self.dtfix != 0) then
    self.dtfix = (et - CurTime()) / (et - self.startTime)
    end
    if(self.dtfix < 0) then
        self.Weapon:EmitSound( sndPowerUp )
        self.dtfix = 0
    end

    if(self.dtfix == 0) then
        zVel = self.Owner:GetVelocity().z
        vVel = vVel:GetNormalized()*(math.Clamp(Distance,0,25))
        if( SERVER ) then
            if  self.NextAmmoTake < CurTime() then
                self:TakePrimaryAmmo(1)
                self.NextAmmoTake = CurTime() + 1
                self.NextAmmoGive = CurTime() + 1.2
            end

            local gravity = GetConVarNumber("sv_gravity")

            vVel:Add(Vector(0,0,(gravity/50)*1.5))

            if(zVel < 0) then
                vVel:Sub(Vector(0,0,zVel/100))
            end
            self.Owner:SetVelocity(vVel)
        end
    end

    endpos = nil

    self.Owner:LagCompensation( false )

end

function SWEP:EndAttack( shutdownsound )

    if ( shutdownsound ) then
        self.Weapon:EmitSound( sndPowerDown )
    end

    if ( CLIENT ) then return end
    if ( !self.Beam ) then return end
    if( IsValid( self.Beam) ) then
        self.Beam:Remove()
    end
    self.Beam = nil

end

function SWEP:OnDrop()
    self:Remove()
end
 
-- MINE ENTITY

local ENT = {}

ENT.Type = "anim"
ENT.Base = "base_anim"


if SERVER then

    function ENT:Initialize()

        self.WillExplode = true
        self:SetModel("models/items/combine_rifle_cartridge01.mdl")
        self:SetModelScale(self:GetModelScale()*1,0)
        self.Deployed = CurTime()
        self.Entity:PhysicsInit(SOLID_VPHYSICS);
        self.Entity:SetMoveType(MOVETYPE_VPHYSICS);
        self.Entity:SetSolid(SOLID_VPHYSICS);
        self.Entity:SetCollisionGroup(COLLISION_GROUP_PROJECTILE)
        self.Damage = 1
        self.health =50
        local phys = self:GetPhysicsObject()
        if phys:IsValid() then
            phys:Wake()
            phys:EnableGravity(true);
            phys:AddGameFlag(FVPHYSICS_NO_PLAYER_PICKUP)
        end


        self:EmitSound("npc/manhack/grind5.wav")


    end
    ENT.NextSpark = 0
    function ENT:Think()
        if self.NextSpark < CurTime() then
            local edata = EffectData()
            edata:SetEntity(self)
            edata:SetMagnitude(15)
            edata:SetScale(25)

            util.Effect("TeslaHitBoxes", edata)
            self.NextSpark = CurTime() + 2
        end

    end

    function ENT:Touch( hitent )
        self:Explode()
    end

    function ENT:TakeDamage(damageAmount)
        self:Explode()
    end

    function ENT:Explode()
        if self.exploded then return end self.exploded = true
        if not ents or not SERVER then return end

        if self.Deployed + 3 > CurTime() or not IsValid(self.Owner ) then return end
        local ent = ents.Create( "env_explosion" )
        ent:SetPos( self:GetPos() )
        ent:SetOwner( self.Owner )
        ent:SetKeyValue( "iMagnitude", "65" )
        ent:Spawn()

        self:Remove()
        ent:Fire( "Explode", 0, 0 )
        ent:EmitSound( "weapons/big_explosion.mp3", 511, 100 )

    end



end

scripted_ents.Register( ENT, "storm_breaker_mine", true )