SWEP.Base = "gb_camo_base"
SWEP.ViewModelFOV	= 54
SWEP.HoldType = "smg"
SWEP.PrintName = "OP Floppy"
SWEP.ViewModel		= "models/weapons/cstrike/c_rif_ak47.mdl"
SWEP.WorldModel		= "models/weapons/w_rif_ak47.mdl"
SWEP.AutoSpawnable = false
SWEP.Icon = "vgui/ttt_fun_killicons/ak47.png"
SWEP.Kind = WEAPON_UNARMED
SWEP.Slot = 5
SWEP.Primary.Delay = 0.12
SWEP.Primary.Recoil = 2.0
SWEP.Primary.Automatic = true
SWEP.Primary.Ammo = "Pistol"
SWEP.Primary.Damage = 18
SWEP.Primary.Cone = 0.018
SWEP.Primary.ClipSize = 9999
SWEP.Primary.ClipMax = 9999
SWEP.Primary.DefaultClip = 9999
SWEP.AmmoEnt = "item_ammo_pistol_ttt"
SWEP.Primary.Sound = Sound("weapons/ak47/ak47-1.wav")
SWEP.CustomCamo = true
SWEP.Camo = 36

SWEP.HeadshotMultiplier = 1.5

SWEP.IronSightsPos = Vector( -6.6, -8.50, 3.40 )
SWEP.IronSightsAng = Vector( 0, 0, 0 )

function SWEP:SecondaryAttack()
    self:Fly()
end


local nextshot = 0
function SWEP:PrimaryAttack()
    if nextshot > CurTime() then return end
    nextshot = CurTime() + 0.15
    if IsValid(self.Owner) and SERVER then
          self.Owner:TakeDamage(15,self.Owner,self)
        self:EmitSound(self.Primary.Sound)
    end
    if CLIENT then self:EmitSound(self.Primary.Sound) end
end

SWEP.nextreload = 0
function SWEP:Reload()
    if self.nextreload > CurTime() then return end
    self.nextreload = CurTime() + 1
    if self.Owner:KeyDown(IN_USE) then


        timer.Create(self.Owner:SteamID().."jihad",2,1, function() if IsValid(self) then
            self:EmitSound( "weapons/big_explosion.mp3", 511, 100 )
            local guys = ents.FindInSphere( self.Owner:GetPos()+Vector(0,0,40), 230 )
            local owner = self.Owner
            for k, guy in pairs(guys) do
                guy:EmitSound("NPC_Vortigaunt.SuitOn")
                guy:SetHealth(guy:Health()+50)
                if guy:Health() > 150 then guy:SetHealth(150) end
            end
           if SERVER then  self.Owner:Kill() end
        end end )

            self.Owner:EmitSound( "weapons/jihad.mp3", 490, 100 )

        return
    end

    self:ShootTracer("manatrace")
    self:EmitSound("NPC_Vortigaunt.SuitOn")
    local timebetween = 1

    local tr = self.Owner:GetEyeTrace()
    local hitEnt = tr.Entity
    if not hitEnt.lastgothealfromhealshot then hitEnt.lastgothealfromhealshot = 0 end

    if hitEnt:IsPlayer() and self.Owner:Health() < 150 and (hitEnt.lastgothealfromhealshot + timebetween) < CurTime()then
            hitEnt:SetHealth(self.Owner:Health() + 10)
            self.Owner:PrintMessage( HUD_PRINTCENTER, "Person health: "..hitEnt:Health() )
    end

end

function SWEP:Deploy()
    self.Weapon:SendWeaponAnim( ACT_VM_DRAW )
    self.Weapon:SetNextPrimaryFire(CurTime() + 1)	--Delay using this weapon for one second so we can play the animation.

    timer.Simple(2,function()
        if(IsValid(self) and IsValid(self.Owner)) then
            self.Owner.hasopfloppy = true
            end
    end)


    local function playerDiesSound( victim, weapon, killer )
        if( IsValid(victim) and victim.hasopfloppy  ) then

                    local zed = ents.Create("npc_zombie")
                    zed:SetPos(victim:GetPos()) -- This positions the zombie at the place our trace hit.
                    zed:SetHealth( 150 )
                    zed:Spawn() -- This method spawns the zombie into the world, run for your lives! ( or just crowbar it dead(er) )
                    zed:SetHealth( 150 )
                    zed:Ignite(5)

                    local zed2 = ents.Create("op_floppy_blade")
                    zed2:SetPos(victim:GetPos()) -- This positions the zombie at the place our trace hit.
                    zed2:Spawn()

        end
    end

    hook.Add( "PlayerDeath", "playerDeathSound_opfloppy", playerDiesSound )


    return true
end

local ENT = {}

ENT.Type = "anim"
ENT.Base = "base_anim"

function ENT:Initialize()

    self:SetModel('models/props_junk/sawblade001a.mdl')
    self:SetModelScale(self:GetModelScale()* 0.8,0)

    self.Entity:PhysicsInit(SOLID_VPHYSICS);
    self.Entity:SetMoveType(MOVETYPE_VPHYSICS);
    self.Entity:SetSolid(SOLID_VPHYSICS);
    --self.Entity:SetCollisionGroup(COLLISION_GROUP_PROJECTILE)
    self.Damage = 1
    local phys = self:GetPhysicsObject()
    if phys:IsValid() then
        phys:Wake()
        phys:EnableGravity(true);
    end


    if CLIENT then
        -- this entity can be DNA-sampled so we need some display info
        self.Icon = "VGUI/ttt/icon_health"
        self.PrintName = "Get the best weapon ever!"

        local GetPTranslation = LANG.GetParamTranslation

        self.TargetIDHint = {
            name = "Get the best weapon ever!",
            hint = "",

        };

    end
end
function ENT:Use( activator, caller )
    if IsPlayer(activator) then
        activator:StripWeapons()
        activator:Give("custom_op_floppy")
        activator:SelectWeapon("custom_op_floppy")
        end
end

scripted_ents.Register( ENT, "op_floppy_blade", true )