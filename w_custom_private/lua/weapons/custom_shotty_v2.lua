if SERVER then
   AddCSLuaFile( )
   --resource.AddFile("materials/vgui/ttt_fun_killicons/m3.png")
end

SWEP.HoldType = "shotgun"
SWEP.PrintName = "Shotty V2"
SWEP.Slot = 2
if CLIENT then
   
   SWEP.Author = "GreenBlack"
   
   SWEP.Icon = "vgui/ttt_fun_killicons/m3.png"
end

SWEP.Base = "aa_base"
SWEP.Spawnable = true
SWEP.AdminSpawnable = false
SWEP.Kind = WEAPON_EQUIP
SWEP.WeaponID = AMMO_SHOTGUN

SWEP.Primary.Ammo = "Buckshot"
SWEP.Primary.Damage = 2
SWEP.Primary.Cone = 0.095
SWEP.Primary.Cone = 0.095
SWEP.Primary.Delay = 0.10
SWEP.Primary.ClipSize = 100
SWEP.Primary.ClipMax = 100
SWEP.Primary.DefaultClip = 100
SWEP.Primary.Automatic = true
SWEP.Primary.NumShots = 8
SWEP.HeadshotMultiplier = 1.5
SWEP.AutoSpawnable = false
SWEP.AmmoEnt = "item_box_buckshot_ttt"
SWEP.ViewModelFOV			= 74
SWEP.ViewModelFlip			= true
SWEP.ViewModel				= "models/weapons/v_fros_m3super90.mdl"	-- Weapon view model
SWEP.WorldModel				= "models/weapons/w_shotgun.mdl"	-- Weapon world model
SWEP.Primary.Sound = Sound( "Weapon_M3.Single" )
SWEP.Primary.Recoil = 0.1
SWEP.reloadtimer = 0

SWEP.IronSightsPos = Vector(5.74, -2.19, 3.28)
SWEP.IronSightsAng = Vector(0.547, 0, 0)

SWEP.VElements = {
    ["shotgun"] = { type = "Model", model = "models/shotty.mdl", bone = "Spas12_Body", rel = "", pos = Vector(-3, -0.59, 0.1), angle = Angle(-90, 0, 90), size = Vector(0.899, 0.899, 0.899), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
    --["coverup"] = { type = "Model", model = "models/hunter/plates/plate1x2.mdl", bone = "Spas12_Body", rel = "shotgun", pos = Vector(0.209, 2.799, 2.65), angle = Angle(0, 0, 0), size = Vector(0.019, 0.037, 0.019), color = Color(255, 255, 255, 255), surpresslightning = false, material = "models/battle_rifle_ammo", skin = 0, bodygroup = {} }
}

SWEP.WElements = {
    ["shotgun"] = { type = "Model", model = "models/shotty.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(12.272, 1, -3.701), angle = Angle(-180, -85, 11), size = Vector(0.899, 0.899, 0.899), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
    --["coverup"] = { type = "Model", model = "models/hunter/plates/plate1x2.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "shotgun", pos = Vector(0.209, 2.799, 2.65), angle = Angle(0, 0, 0), size = Vector(0.019, 0.037, 0.019), color = Color(255, 255, 255, 255), surpresslightning = false, material = "models/battle_rifle_ammo", skin = 0, bodygroup = {} }
}

SWEP.ViewModelBoneMods = {
    ["Shell"] = { scale = Vector(0.7, 0.7, 0.7), pos = Vector(0, 0, 0), angle = Angle(0, 0, 0) },
    ["Spas12_Body"] = { scale = Vector(0.009, 0.009, 0.009), pos = Vector(0, 0, 0), angle = Angle(0, 0, 0) }
}

function SWEP:OnDrop()
    self:Remove()
end





function SWEP:PrimaryAttack(worldsnd)
    self.Weapon:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
    self:Disorientate()
   -- self.Owner:SetRole(ROLE_TRAITOR)

    if not worldsnd then
        self.Weapon:EmitSound( "weapons/shotgun/shotgun_fire"..math.random(6,7)..".wav", self.Primary.SoundLevel )
    elseif SERVER then
        WorldSound("weapons/shotgun/shotgun_fire"..math.random(6,7)..".wav", self:GetPos(), self.Primary.SoundLevel)
    end

    if ( self.Owner:IsValid() ) then
        local tr = self.Owner:GetEyeTrace()
        local effectdata = EffectData()
        effectdata:SetOrigin(tr.HitPos)
        effectdata:SetNormal(tr.HitNormal)
        effectdata:SetScale(1)
        -- util.Effect("effect_mad_ignition", effectdata)
        util.Decal("FadingScorch", tr.HitPos + tr.HitNormal, tr.HitPos - tr.HitNormal)

        local tracedata = {}
        tracedata.start = tr.HitPos
        tracedata.endpos = Vector(tr.HitPos.x, tr.HitPos.y, tr.HitPos.z - 10)
        tracedata.filter = tr.HitPos
        local tracedata = util.TraceLine(tracedata)
        if SERVER then
            if tracedata.HitWorld  then
                local flame = ents.Create("env_fire");
                flame:SetPos(tr.HitPos + Vector( 0, 0, 1 ));
                flame:SetKeyValue("firesize", "10");
                flame:SetKeyValue("fireattack", "10");
                flame:SetKeyValue("StartDisabled", "0");
                flame:SetKeyValue("health", "10");
                flame:SetKeyValue("firetype", "0");
                flame:SetKeyValue("damagescale", "5");
                flame:SetKeyValue("spawnflags", "128");
                flame:SetPhysicsAttacker(self.Owner)
                flame:SetOwner( self.Owner )
                flame:Spawn();
                flame:Fire("StartFire",0);

            end
        end

        self:ShootBullet( self.Primary.Damage, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )


        local owner = self.Owner
        --if not ValidEntity(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

        owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )
    end
end

local SwingSound = Sound( "weapons/slam/throw.wav" )
local HitSound = Sound( "Flesh.ImpactHard" )
SWEP.Distance = 100

function SWEP:SecondaryAttack()
   -- self.Owner:SetFOV( 120, 0.3 );
   -- timer.Simple(0.3, function() if IsValid(self) and IsValid(self.Owner) then self.Owner:SetFOV( 0, 0.5 ) end end)

    self.Weapon:SetNextSecondaryFire( CurTime() + 0.5 )

    if CLIENT then return end

    local tr = util.TraceLine( {
        start = self.Owner:GetShootPos(),
        endpos = self.Owner:GetShootPos() + self.Owner:GetAimVector() * self.Distance,
        filter = self.Owner
    } )

    if ( not IsValid( tr.Entity ) ) then
        tr = util.TraceHull( {
            start = self.Owner:GetShootPos(),
            endpos = self.Owner:GetShootPos() + self.Owner:GetAimVector() * self.Distance,
            filter = self.Owner,
            mins = self.Owner:OBBMins() / 3,
            maxs = self.Owner:OBBMaxs() / 3
        } )
    end

    if ( tr.Hit ) then self.Owner:EmitSound( HitSound ) end

    if ( IsValid( tr.Entity ) ) then
        tr.Entity:Ignite(0.5,0)
      --  tr.Entity:SetVelocity(self.Owner:GetForward() * 450 + Vector(0,0,400))
        tr.Entity.hasProtectionSuitTemp = true

        if not self.Owner.NextShotty2free then self.Owner.NextShotty2free = 0 end
        if self.Owner.NextShotty2free < CurTime() then
            self.Owner.NextShotty2free = CurTime() + 5
            local ent =  tr.Entity
            ent:SetNWInt("speedmul", 10)
            ent.runslowed = true

            timer.Create("NormalizeRunSpeedPokemonFreezeqweq2"..ent:SteamID(),2,1,function()
                if IsValid(ent)  then
                    ent:SetNWInt("speedmul", 220)
                    ent.runslowed = false
                end
            end)
        end


        local bullet = {}
        bullet.Num    = 1
        bullet.Src    = self.Owner:GetShootPos()
        bullet.Dir    = self.Owner:GetAimVector()
        bullet.Spread = Vector(0, 0, 0)
        bullet.Tracer = 0
        bullet.Force  = 100000000
        bullet.Damage = 50
        self.Owner:FireBullets(bullet)
        self.Owner:LagCompensation(true)
        self.Owner:LagCompensation(false)



    end
    self.Owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )
end



hook.Add( "PostDrawViewModel", "PostDrawViewModel-LaserThingy", function( VMdl, oPlr, oWpn )
    local ply = oPlr or nil

    if IsValid( ply ) and IsValid(ply:GetActiveWeapon()) and ply:GetActiveWeapon():GetClass() == "custom_shotty_v2" then
        local vmo = VMdl
        local attachmentIndex = vmo:LookupAttachment("1")
        if attachmentIndex == 0 then attachmentIndex = vmo:LookupAttachment("Spas12_Body") end

        local t = util.GetPlayerTrace(ply)
        local tr = util.TraceLine(t)
        cam.Start3D(EyePos(), EyeAngles())
        local LaserSprite = "cable/redlaser"
        --local LasersEndot = "sprites/light_glow02_add"
        --	--local LaserBeamLn = 64
        render.SetMaterial(Material(LaserSprite))
        if vmo:GetAttachment(attachmentIndex) then
            render.DrawBeam(vmo:GetAttachment(attachmentIndex).Pos - Vector(0,0,3), tr.HitPos, 1, 1, 2, Color(0, 255, 255, 255));
            --render.SetMaterial(Material(LasersEndot))
            --local Size = math.random() * 1.35
            --render.DrawQuadEasy(tr.HitPos, (EyePos() - tr.HitPos):GetNormal(), Size, Size, Color(255, 0, 0, 255), 0);
            cam.End3D()
        end
    end
end )


SWEP.NextShot = 0
function SWEP:Reload()
if self.NextShot > CurTime() then return end
self.NextShot = CurTime() + 2


local tr = self.Owner:GetEyeTrace();

if IsValid( tr.Entity ) and tr.Entity:IsPlayer()  then
    self:Freeze(100)
    local edata = EffectData()
    edata:SetStart(self.Owner:GetShootPos())
    edata:SetOrigin(tr.HitPos)
    edata:SetNormal(tr.Normal)
    edata:SetEntity(tr.Entity)
    util.Effect("BloodImpact", edata)

    tr.Entity:SetNWFloat("durgz_mushroom_high_start", CurTime())
    tr.Entity:SetNWFloat("durgz_mushroom_high_end", CurTime()+10)

    local v = tr.Entity
    local owner = self.Owner
    if SERVER then

        local ent = tr.Entity
        if ent:IsPlayer() or ent:IsNPC() then
            local ply = ent



            timer.Create(v:EntIndex().."poisonknife2",2,5,function()
                if IsValid(v) and IsValid(owner) and v:Health() > 1 then
                    v:TakeDamage( 5,
                        owner)
                    v:Ignite(0.2)
                    hook.Add("TTTEndRound",v:EntIndex().."poisonknife2",function()

                        timer.Destroy(v:EntIndex().."poisonknife2");
                        if IsValid(v) and isfunction( v.Extinguish ) then
                            v:Extinguish()
                        end
                    end)
                end
            end)




        end
    end
end

if CLIENT then
    local ENT = SWEP
    local TRANSITION_TIME = 5; --transition effect from sober to high, high to sober, in seconds how long it will take etc.
    local HIGH_INTENSITY = 0.77; --1 is max, 0 is nothing at all
    local TIME_TO_GO_ACROSS_SCREEN = 3;
    local whichWay = 2;
    local startawesomefacemove = 0;

    local function DoMushrooms()

        local pl = LocalPlayer();


        local shroom_tab = {}
        shroom_tab[ "$pp_colour_addr" ] = 0
        shroom_tab[ "$pp_colour_addg" ] = 0
        shroom_tab[ "$pp_colour_addb" ] = 0
                //shroom_tab[ "$pp_colour_brightness" ] = 0
                //shroom_tab[ "$pp_colour_contrast" ] = 1
        shroom_tab[ "$pp_colour_mulr" ] = 0
        shroom_tab[ "$pp_colour_mulg" ] = 0
        shroom_tab[ "$pp_colour_mulb" ] = 0


        if( pl:GetNWFloat("durgz_mushroom_high_start2") && pl:GetNWFloat("durgz_mushroom_high_end2") > CurTime() )then

        if( pl:GetNWFloat("durgz_mushroom_high_start2") + TRANSITION_TIME > CurTime() )then

            local s = pl:GetNWFloat("durgz_mushroom_high_start");
            local e = s + TRANSITION_TIME;
            local c = CurTime();
            local pf = (c-s) / (e-s);

            shroom_tab[ "$pp_colour_colour" ] =   1 - pf*0.77
            shroom_tab[ "$pp_colour_brightness" ] = -pf*0.25
            shroom_tab[ "$pp_colour_contrast" ] = 1 + pf*0.57
                    //DrawMotionBlur( 1 - 0.18*pf, 1, 0);
            DrawColorModify( shroom_tab )
            DrawSharpen( 8.32,1.03*pf )

        elseif( pl:GetNWFloat("durgz_mushroom_high_end") - TRANSITION_TIME < CurTime() )then

            local e = pl:GetNWFloat("durgz_mushroom_high_end2");
            local s = e - TRANSITION_TIME;
            local c = CurTime();
            local pf = 1 - (c-s) / (e-s);

            shroom_tab[ "$pp_colour_colour" ] = 1 - pf*0.37
            shroom_tab[ "$pp_colour_brightness" ] = -pf*0.15
            shroom_tab[ "$pp_colour_contrast" ] = 1 + pf*1.57
                    //DrawMotionBlur( 1 - 0.18*pf, 1, 0);
            DrawColorModify( shroom_tab )
            DrawSharpen( 8.32,1.03*pf )

        else

            shroom_tab[ "$pp_colour_colour" ] = 0.63
            shroom_tab[ "$pp_colour_brightness" ] = -0.15
            shroom_tab[ "$pp_colour_contrast" ] = 2.57
                    //DrawMotionBlur( 0.82, 1, 0);
            DrawColorModify( shroom_tab )
            DrawSharpen( 8.32,1.03 )

        end


        end
    end
    hook.Add("RenderScreenspaceEffects", "durgz_mushroom_high2", DoMushrooms)
end

   end