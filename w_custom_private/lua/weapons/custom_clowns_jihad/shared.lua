

if SERVER then
   AddCSLuaFile(  )
   --resource.AddFile("materials/vgui/ttt_fun_killicons/babyboom.png")
   --resource.AddFile("sound/weapons/gb_crybaby.mp3")
   --resource.AddFile("sound/weapons/big_explosion.mp3")
end

SWEP.Base				= "weapon_tttbasegrenade"
SWEP.DetTime = 20
SWEP.Kind = WEAPON_EQUIP2
SWEP.WeaponID = AMMO_MOLOTOV

SWEP.HoldType			= "grenade"
SWEP.InLoadoutFor = nil
SWEP.LimitedStock = false
SWEP.AllowDrop = true
SWEP.IsSilent = false
SWEP.NoSights = true
SWEP.detonate_timer = 20
SWEP.ViewModelFOV = 90
SWEP.ViewModel			= "models/weapons/v_c4.mdl"
SWEP.WorldModel			= "models/weapons/w_c4.mdl"
SWEP.Weight				= 5
SWEP.AutoSpawnable      = false
-- really the only difference between grenade weapons: the model and the thrown
-- ent.
SWEP.PrintName	 = "BABY THROW BOOM"
	SWEP.Slot		 = 7

SWEP.NextDetTime = 0
SWEP.DetTime = 20
function SWEP:SetDetTimer()
    if self.NextDetTime > CurTime() then
        return end

    self.NextDetTime = CurTime() + 0.2

    if self.DetTime == 10 then
        self.DetTime = 20
    elseif self.DetTime == 20 then
        self.DetTime = 30
    elseif self.DetTime == 30 then
        self.DetTime = 45
    elseif self.DetTime == 45 then
        self.DetTime = 60
    elseif self.DetTime == 60 then
        self.DetTime = 90
    elseif self.DetTime == 90 then
        self.DetTime = 180
    else
        self.DetTime = 10
    end

    self.Owner:PrintMessage(HUD_PRINTCENTER,"Detonation Time: "..self.DetTime.." seconds")

end

function SWEP:SecondaryAttack()
    self:SetDetTimer()
end

SWEP.Icon = "vgui/ttt_fun_killicons/babyboom.png"

function SWEP:GetGrenadeName()
   return "clown_baby"
end

--Taken from base grenade
function SWEP:Initialize()

  
   if self.SetHoldType then
      self:SetHoldType(self.HoldNormal)
   end

   self:SetDeploySpeed(self.DeploySpeed)
   self:SetDetTime(CurTime() + self.DetTime)
   self:SetThrowTime(0)
   self:SetPin(true)
   
   
   if CLIENT then
	self.ModelEntity = ClientsideModel(self.WorldModel)
	self.ModelEntity:SetNoDraw(true)
   end
end


function SWEP:Deploy()
 
   if self.SetHoldType then
      self:SetHoldType(self.HoldNormal)
   end

   self:SetThrowTime(0)
   self:SetPin(false)
   self:SetDetTime(CurTime() + 9999)
   hook.Add("TTTRoundEnd","givenewawepoansjihad",function()
       for k,v in pairs(player.GetAll()) do
           v.gotseconds = false
       end
   end)
   return false
end

function SWEP:GiveNewOne()
   if SERVER and IsValid(self.Owner) and self.Owner:Alive() and !self.Owner.getsecondclownjihad then
   local Owner = self.Owner
   timer.Simple(0.2,function()
        Owner.getsecondclownjihad = true
        Owner:Give("custom_clowns_jihad")
   end)
   end
   
   if CLIENT then
   self.ModelEntity:Remove()
   end
end

function SWEP:PullPin()
   --if self:GetPin() then return end

   local ply = self.Owner
   if not IsValid(ply) then return end

   self.Weapon:SendWeaponAnim(ACT_VM_PULLPIN)
   self:SetDetTime(CurTime() + self.DetTime)
   if self.SetHoldType then
      self:SetHoldType(self.HoldReady)
   end

   self:SetPin(false)
   self:SetDetTime(CurTime() + self.DetTime)
  -- self:SetDetTime(CurTime() + self.detonate_timer)
end



function SWEP:Throw()

   self:SetDetTime(CurTime() + self.DetTime)
   if CLIENT then
      self:SetThrowTime(0)
   elseif SERVER then
      local ply = self.Owner
      if not IsValid(ply) then return end

     -- if self.was_thrown then return end

      self.was_thrown = true
      
      local ang = ply:EyeAngles()

      -- don't even know what this bit is for, but SDK has it
      -- probably to make it throw upward a bit
      if ang.p < 90 then
         ang.p = -10 + ang.p * ((90 + 10) / 90)
      else
         ang.p = 360 - ang.p
         ang.p = -10 + ang.p * -((90 + 10) / 90)
      end

      local vel = math.min(180, (90 - ang.p) * 6)

      local vfw = ang:Forward()
      local vrt = ang:Right()
      --      local vup = ang:Up()

      local src = ply:GetPos() + (ply:Crouching() and ply:GetViewOffsetDucked() or ply:GetViewOffset())
      src = src + (vfw * 120) + (vrt * 10)

      local thr = vfw * vel + ply:GetVelocity()

      self:CreateGrenade(src, Angle(0,0,0), thr, Vector(600, math.random(-1200, 1200), 0), ply)

      self:SetThrowTime(0)

      self:GiveNewOne()
      self:Remove()

      if SERVER and IsValid(self.Owner) and self.Owner:Alive() and !self.Owner.getsecondclownjihad then
        self.Owner.getsecondclownjihad = true
        self.Owner:Give("custom_clowns_jihad")
      end
   end
end

function SWEP:Think()
   local ply = self.Owner
   if not IsValid(ply) then return end



   -- pin pulled and attack loose = throw
   --if self:GetPin() then
      -- we will throw now
      if  ply:KeyDown(IN_ATTACK) then
         self:StartThrow()

         self:SetPin(false)
         self.Weapon:SendWeaponAnim(ACT_VM_THROW)

         if SERVER then
            self.Owner:SetAnimation( PLAYER_ATTACK1 )
         end
      
   elseif self:GetThrowTime() > 0 and self:GetThrowTime() < CurTime() then
      self:Throw()
   end


end



function SWEP:CreateGrenade(src, ang, vel, angimp, ply)
   local gren = ents.Create(self:GetGrenadeName())
   if not IsValid(gren) then return end

   gren:SetPos(src)
   gren:SetAngles(ang)

   --   gren:SetVelocity(vel)
   gren:SetOwner(ply)
   gren:SetThrower(ply)

   gren:SetGravity(0.2)
   gren:SetFriction(3)
   gren:SetElasticity(0.9)

   gren:Spawn()

   gren:PhysWake()

   local phys = gren:GetPhysicsObject()
   if IsValid(phys) then
      phys:SetVelocity(vel)
      phys:AddAngleVelocity(angimp)
   end

   -- This has to happen AFTER Spawn() calls gren's Initialize()
   self:SetDetTime(CurTime() + self.DetTime)
   gren:SetDetonateExact(CurTime() + self.DetTime)

   return gren
end