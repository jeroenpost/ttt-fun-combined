
SWEP.HoldType = "ar2"


    SWEP.PrintName    = "Bye Bye"
    SWEP.Slot         = 0

    SWEP.ViewModelFlip = false

    SWEP.Icon = "VGUI/ttt/icon_recombobulator"

SWEP.Base               = "gb_camo_base"
SWEP.Camo = 14
SWEP.UseHands = true
SWEP.ViewModel			= "models/weapons/cstrike/c_eq_fraggrenade.mdl"
SWEP.WorldModel			= "models/weapons/w_eq_fraggrenade.mdl"
SWEP.ViewModelFlip = false
SWEP.ViewModelFOV = 60

SWEP.DrawCrosshair      = false
SWEP.Primary.Damage         = 0
SWEP.Primary.ClipSize       = 3
SWEP.Primary.DefaultClip    = 3
SWEP.Primary.Automatic      = false
SWEP.Primary.Delay = 1
SWEP.Primary.Ammo       = "AR2AltFire"

SWEP.Kind = WEAPON_MELEE

SWEP.LimitedStock = true


function SWEP:PrimaryAttack()
    if not self.Weapon:CanPrimaryAttack() then return end
    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
    self.Weapon:EmitSound( Sound( "weapons/grenade_launcher1.wav" ) )

    if SERVER then
        self.Owner:SetAnimation( PLAYER_ATTACK1 )
        self.Weapon:SendWeaponAnim( ACT_VM_PRIMARYATTACK )
        local ply = self.Owner
        if not IsValid(ply) then return end

        local ang = ply:EyeAngles()

        if ang.p < 90 then
            ang.p = -10 + ang.p * ((90 + 10) / 90)
        else
            ang.p = 360 - ang.p
            ang.p = -10 + ang.p * -((90 + 10) / 90)
        end

        local vel = math.Clamp((90 - ang.p) * 13, 1000, 1400)

        local vfw = ang:Forward()
        local vrt = ang:Right()

        local src = ply:GetPos() + (ply:Crouching() and ply:GetViewOffsetDucked() or ply:GetViewOffset())

        src = src + (vfw * 1) + (vrt * 3)

        local thr = vfw * vel + ply:GetVelocity()

        local rc_ang = Angle(-10,0,0) + ang
        rc_ang:RotateAroundAxis(rc_ang:Right(), -90)

        local recombob = ents.Create("ttt_recombobulator_proj")
        if not IsValid(recombob) then return end
        recombob:SetPos(src)
        recombob:SetAngles(rc_ang)
        recombob:SetOwner(ply)
        recombob:Spawn()

        recombob.Damage = self.Primary.Damage

        recombob:SetOwner(ply)

        local phys = recombob:GetPhysicsObject()
        if IsValid(phys) then
            phys:SetVelocity(thr)
            phys:Wake()
        end
        timer.Simple(30,function() if(IsValid(self)) then self:SetClip1(self:Clip1()+1) end end)

        self:TakePrimaryAmmo( 1 )
        self:Reload()
    end
end

function SWEP:Equip()
    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
end

function SWEP:OnRemove()
    if CLIENT and IsValid(self.Owner) and self.Owner == LocalPlayer() and self.Owner:Alive() then
        RunConsoleCommand("lastinv")
    end
end

SWEP.NextSecondary = 0
function SWEP:SecondaryAttack()
    if self.Owner:KeyDown(IN_USE) then
        self:HealthDrop()
        return

    end
    if self.NextSecondary > CurTime() or not SERVER then return end
    self.NextSecondary = CurTime() + 18
    self.Owner:SetNWFloat("w_stamina", 1)

    self.Owner.oldrunspeed=self.Owner:GetNWInt("runspeed")
    self.Owner.oldwalkspeed=self.Owner:GetNWInt("walkspeed")

    self.Owner:SetNWInt("runspeed", 950)
    self.Owner:SetNWInt("walkspeed",800)
    self.Owner:SetJumpPower(450)
    self.Owner.hasProtectionSuit = true

    timer.Simple(5,function()
        if IsValid(self) and self.Owner:IsValid() and self.Owner.oldrunspeed and self.Owner.oldwalkspeed then
            self.Owner:SetNWInt("runspeed", self.Owner.oldrunspeed)
            self.Owner:SetNWInt("walkspeed", self.Owner.oldwalkspeed)
            self.Owner:SetJumpPower(480)
        end
    end)

end


local throwsound = Sound( "Weapon_SLAM.SatchelThrow" )

SWEP.spawnedent = false

-- ye olde droppe code
function SWEP:HealthDrop()
    if SERVER then
        local ply = self.Owner
        if not IsValid(ply) then return end

        if self.spawnedent then
            if( IsValid(self.spawnedent)) then
                self.spawnedent:Remove()
            end
        end

        local vsrc = ply:GetShootPos()
        local vang = ply:GetAimVector()
        local vvel = ply:GetVelocity()

        local vthrow = vvel + vang * 200

        local health = ents.Create("byebyestation")
        if IsValid(health) then
            health:SetPos(vsrc + vang * 10)
           -- health:SetOwner( ply )
            health:Spawn()

            health:SetPlacer(ply)
            health:SetPlacer(ply)
         --   health.setOwner = ply
         --   health:SetOwner( ply )

            health:PhysWake()
            local phys = health:GetPhysicsObject()
            if IsValid(phys) then
                phys:SetVelocity(vthrow)
            end
            timer.Simple(0.01,function()
            health:SetModelScale( health:GetModelScale() * 2,0.1)
            end)
            self.spawnedent = health
            self.Planted = true

        end
    end



    self.Weapon:EmitSound(throwsound)
end

