include "weapons/weapon_zm_shotgun.lua"
SWEP.Base = "gb_camo_base"
SWEP.ViewModelFOV	= 90
SWEP.HoldType = "smg"
SWEP.PrintName = "Balance Breaker"
SWEP.ViewModel		= "models/weapons/c_shotgun.mdl"
SWEP.WorldModel		= "models/weapons/w_shotgun.mdl"


SWEP.HoldType = "shotgun"
SWEP.Slot = 8
SWEP.Kind = 9

if CLIENT then

    SWEP.Author = "GreenBlack"

    SWEP.Icon = "vgui/ttt_fun_killicons/xm1014.png"
end
function SWEP:OnDrop() self:Remove() end

SWEP.Primary.Sound			= Sound( "weapons/green_black1.mp3")
SWEP.Primary.Recoil			= 3

SWEP.Primary.Ammo = "Buckshot"
SWEP.Primary.Damage = 12
SWEP.Primary.Cone = 0.035
SWEP.Primary.Delay = 0.95
SWEP.Primary.ClipSize = 16
SWEP.Primary.ClipMax = 64
SWEP.Primary.DefaultClip = 64
SWEP.Primary.Automatic = true
SWEP.Primary.NumShots = 10
SWEP.AutoSpawnable      = false
SWEP.AmmoEnt = "item_box_buckshot_ttt"
SWEP.Camo = 999

function SWEP:OnDrop()
    self:Remove()
end
function SWEP:Deploy()
    self.dt.reloading = false
    self.reloadtimer = 0
    return true
end

function SWEP:PreDrop(yes)
    if yes then self:Remove() end
end

function SWEP:PrimaryAttack()
    if self:Clip1() > 0 then
    self:FireBolt()
    end
    self.BaseClass.PrimaryAttack(self)
end

