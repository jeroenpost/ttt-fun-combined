if SERVER then

    AddCSLuaFile( )




end

SWEP.HoldType			= "smg"
SWEP.PrintName			= "ACL's Eternal Rest"
SWEP.Slot				= 6
if CLIENT then


    SWEP.Author				= "Assassin"

    SWEP.SlotPos			= 0

    SWEP.Icon = "vgui/ttt_fun_killicons/mp7.png"




    SWEP.ViewModelFlip		= true
end


SWEP.Base				= "gb_camo_base"

SWEP.Spawnable = true
SWEP.AdminSpawnable = true

SWEP.Kind = 99
SWEP.Camo = 7

SWEP.Primary.Damage = 15
SWEP.Primary.Delay = 0.18
SWEP.Primary.Cone = 0.0001
SWEP.Primary.ClipSize = 200
SWEP.Primary.ClipMax = 200
SWEP.Primary.DefaultClip	= 200
SWEP.Primary.Automatic		= true
SWEP.Primary.Ammo			= "smg1"
SWEP.AutoSpawnable      = false
SWEP.Primary.Recoil			= 0.8
SWEP.Primary.Sound			= Sound("weapons/mp7/mp7_fire.mp3")
SWEP.ViewModel			= "models/weapons/v_mp7_silenced.mdl"
SWEP.WorldModel			= "models/weapons/w_mp7_silenced.mdl"

SWEP.AmmoEnt = "item_ammo_smg1_ttt"

SWEP.HeadshotMultiplier = 1.2

SWEP.Secondary.Sound = Sound("Default.Zoom")

SWEP.IronSightsPos      = Vector( 5, -15, -2 )
SWEP.IronSightsAng      = Vector( 2.6, 1.37, 3.5 )

function SWEP:SetZoom(state)
    if CLIENT then
        return
    else
        if state then
            self.Owner:SetFOV(20, 0.3)
        else
            self.Owner:SetFOV(0, 0.2)
        end
    end
end

-- Add some zoom to ironsights for this gun
function SWEP:SecondaryAttack()
    if not self.IronSightsPos then return end
    if self.Weapon:GetNextSecondaryFire() > CurTime() then return end

    bIronsights = not self:GetIronsights()

    self:SetIronsights( bIronsights )

    if SERVER then
        self:SetZoom(bIronsights)
    else
        self:EmitSound(self.Secondary.Sound)
    end

    self.Weapon:SetNextSecondaryFire( CurTime() + 0.3)
end

function SWEP:PreDrop()
    self:SetZoom(false)
    self:SetIronsights(false)
    return self.BaseClass.PreDrop(self)
end

function SWEP:Reload()
    if self.Owner:KeyDown(IN_USE) then self:Fly() return end
    self.Weapon:DefaultReload( ACT_VM_RELOAD );
    self:SetIronsights( false )
    self:SetZoom(false)
end


function SWEP:Holster()
    self:SetIronsights(false)
    self:SetZoom(false)
    return true
end

if CLIENT then
    local scope = surface.GetTextureID("sprites/scope")
    function SWEP:DrawHUD()
        if self:GetIronsights() then
            surface.SetDrawColor( 0, 0, 0, 255 )

            local x = ScrW() / 2.0
            local y = ScrH() / 2.0
            local scope_size = ScrH()

            -- crosshair
            local gap = 80
            local length = scope_size
            surface.DrawLine( x - length, y, x - gap, y )
            surface.DrawLine( x + length, y, x + gap, y )
            surface.DrawLine( x, y - length, x, y - gap )
            surface.DrawLine( x, y + length, x, y + gap )

            gap = 0
            length = 50
            surface.DrawLine( x - length, y, x - gap, y )
            surface.DrawLine( x + length, y, x + gap, y )
            surface.DrawLine( x, y - length, x, y - gap )
            surface.DrawLine( x, y + length, x, y + gap )


            -- cover edges
            local sh = scope_size / 2
            local w = (x - sh) + 2
            surface.DrawRect(0, 0, w, scope_size)
            surface.DrawRect(x + sh - 2, 0, w, scope_size)

            surface.SetDrawColor(255, 0, 0, 255)
            surface.DrawLine(x, y, x + 1, y + 1)

            -- scope
            surface.SetTexture(scope)
            surface.SetDrawColor(255, 255, 255, 255)

            surface.DrawTexturedRectRotated(x, y, scope_size, scope_size, 0)

        else
            return self.BaseClass.DrawHUD(self)
        end
    end

    function SWEP:AdjustMouseSensitivity()
        return (self:GetIronsights() and 0.2) or nil
    end
end


SWEP.NextShot = 0
function SWEP:PrimaryAttack()
    if self.NextShot > CurTime() then return end
    self.NextShot = CurTime() + self.Primary.Delay

    self.BaseClass.PrimaryAttack(self)
    local tr = self.Owner:GetEyeTrace();

    if IsValid( tr.Entity ) and tr.Entity:IsPlayer()  then

        local edata = EffectData()
        edata:SetStart(self.Owner:GetShootPos())
        edata:SetOrigin(tr.HitPos)
        edata:SetNormal(tr.Normal)
        edata:SetEntity(tr.Entity)
        util.Effect("BloodImpact", edata)

        tr.Entity:SetNWFloat("durgz_mushroom_high_start", CurTime())
        tr.Entity:SetNWFloat("durgz_mushroom_high_end", CurTime()+5)

        local v = tr.Entity
        local owner = self.Owner
        if SERVER then

            local ent = tr.Entity
            if ent:IsPlayer() or ent:IsNPC() then
                local ply = ent



                timer.Create(v:EntIndex().."poisonknife2"..math.Rand(9,99999),2,5,function()
                    if IsValid(v) and IsValid(owner) and v:Health() > 1 then
                        v:TakeDamage( 5,
                            owner)
                        v:Ignite(0.1)
                        hook.Add("TTTEndRound",v:EntIndex().."poisonknife2",function()

                            timer.Destroy(v:EntIndex().."poisonknife2");
                            if IsValid(v) and isfunction( v.Extinguish ) then
                                v:Extinguish()
                            end
                        end)
                    end
                end)




            end
        end
    end
    end