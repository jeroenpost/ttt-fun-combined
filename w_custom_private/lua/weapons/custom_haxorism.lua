SWEP.Base = "gb_camo_base"
SWEP.ViewModelFOV	= 90
SWEP.HoldType = "pistol"
SWEP.PrintName = "Haxorism"



SWEP.ShowViewModel = true
SWEP.ShowWorldModel = false
SWEP.AutoSpawnable = false
SWEP.UseHands = true
SWEP.ViewModelFlip = false
SWEP.HoldType = "shotgun"
SWEP.Slot = 9

if CLIENT then

    SWEP.Author = "GreenBlack"

    SWEP.Icon = "vgui/ttt_fun_killicons/mp5.png"
end

SWEP.Spawnable = true
SWEP.AdminSpawnable = true
SWEP.Kind = 55
SWEP.Camo =21
SWEP.CustomCamo = true

SWEP.Primary.Delay = 1.2
SWEP.Primary.Recoil = 1.1
SWEP.Primary.Automatic = true
SWEP.Secondary.Automatic = true
SWEP.Primary.Ammo = "pistol"
SWEP.Primary.Damage = 95
SWEP.Primary.Cone = 0.0001
SWEP.Primary.ClipSize = 350
SWEP.Primary.ClipMax = 900
SWEP.Primary.DefaultClip = 350
SWEP.AutoSpawnable = false
SWEP.AmmoEnt = "item_ammo_pistol_ttt"
SWEP.WorldModel = "models/weapons/w_smg_mp5.mdl"
SWEP.ViewModelFOV	= 54
SWEP.Primary.Sound = Sound("Weapon_MP5Navy.Single")

SWEP.HeadshotMultiplier = 1.5

SWEP.IronSightsPos = Vector(-5.3, -3.5823, 2)
SWEP.IronSightsAng = Vector(0.9641, 0.0252, 0)



SWEP.ViewModelFlip = false
SWEP.ViewModel		= "models/weapons/cstrike/c_pist_usp.mdl"
SWEP.WorldModel		= "models/weapons/w_pist_usp.mdl"
function SWEP:OnDrop()
    self:Remove()
end

SWEP.nextSecond = 0
function SWEP:SecondaryAttack()
    if self.Owner:KeyDown(IN_USE) then
        self:Zoom()
        return
    end

    if self.Aimbot.Target ~= nil then
        self.Owner:SetEyeAngles((self:GetHeadPos(self.Aimbot.Target) - self.Owner:GetShootPos()):Angle())
    end
    if self.nextSecond > CurTime() then return end
    self.nextSecond = CurTime() + 0.15

    self:FireBolt()
    if not worldsnd then
        self.Weapon:EmitSound( self.Primary.Sound, self.Primary.SoundLevel )
    elseif SERVER then
        sound.Play(self.Primary.Sound, self:GetPos(), self.Primary.SoundLevel)
    end

    self:ShootBullet( 20, 0.8, 1, 0.001 )

    self:TakePrimaryAmmo( 1 )
end

-- AIMBOT

SWEP.Aimbot = {}
SWEP.Aimbot.Target = nil
SWEP.Aimbot.DeathSequences = {
    ["models/barnacle.mdl"]            = {4,15},
    ["models/antlion_guard.mdl"]    = {44},
    ["models/hunter.mdl"]            = {124,125,126,127,128},
}

function SWEP:GetHeadPos(ent)
    local model = ent:GetModel() or ""
    if model:find("crow") or model:find("seagull") or model:find("pigeon") then
        return ent:LocalToWorld(ent:OBBCenter() + Vector(0,0,-5))
    elseif ent:GetAttachment(ent:LookupAttachment("eyes")) ~= nil then
        return ent:GetAttachment(ent:LookupAttachment("eyes")).Pos
    else
        return ent:LocalToWorld(ent:OBBCenter())
    end
end

function SWEP:Visible(ent)
    local trace = {}
    trace.start = self.Owner:GetShootPos()
    trace.endpos = self:GetHeadPos(ent)
    trace.filter = {self.Owner,ent}
    trace.mask = MASK_SHOT
    local tr = util.TraceLine(trace)
    return tr.Fraction >= 0.99 and true or false
end

function SWEP:CheckTarget(ent)
    if ent:IsPlayer() or ent:IsNPC() then
        if not IsValid(ent) then return false end
        if ent:Health() < 1 then return false end
        if ent == self.Owner then return false end
        return true
    end

    return false
end

function SWEP:GetTargets()
    local tbl = {}
    for k,ent in pairs(ents.GetAll()) do
        if self:CheckTarget(ent) == true then
            table.insert(tbl,ent)
        end
    end
    return tbl
end

function SWEP:GetClosestTarget()
    local pos = self.Owner:GetPos()
    local ang = self.Owner:GetAimVector()
    local closest = {0,0}
    for k,ent in pairs(self:GetTargets()) do
        local diff = (ent:GetPos()-pos)
        diff:Normalize()
        diff = diff - ang
        diff = diff:Length()
        diff = math.abs(diff)
        if (diff < closest[2]) or (closest[1] == 0) then
            closest = {ent,diff}
        end
    end
    return closest[1]
end

SWEP.activated = false

function SWEP:Think()
    if not self.Owner.usedwallhack and self.Owner:KeyDown(IN_USE) and self.Owner:KeyDown(IN_ATTACK) and not self.activatedwall     then
        self.activatedwall = true
        self.Owner.wallhackactivated = true
        self.Owner:PrintMessage( HUD_PRINTCENTER, "Wallhack activated" )
        local owner = self.Owner

        timer.Create("aimbotcounterstop2"..owner:SteamID(),15,1,function()
            owner.usedwallhack = true
            self.activatedwall = false
            owner.wallhackactivated = false
            self.Aimbot.Target = nil
        end)
        hook.Add("TTTPrepareRound","sandwichstuffaimbot2"..owner:SteamID(),function()
            if IsValid(owner) and IsValid(self) then

                owner.usedwallhack = false
                owner.wallhackactivated = false
                if not self.Aimbot then self.Aimbot = {} end
                self.Aimbot.Target = nil
            end
        end)
    end

    if not self.Owner.usedaimbot and self.Owner:KeyDown(IN_USE) and self.Owner:KeyDown(IN_RELOAD) and self.activated == false  then
        self.activated = true
        self.Owner:PrintMessage( HUD_PRINTCENTER, "Aimbot activated" )
        local owner = self.Owner
        timer.Create("aimbotcounterdfg"..owner:SteamID(),1,16,function()
            self:SetNWInt("iambotcountdown", (tonumber(self:GetNWInt("iambotcountdown",15)) or 15) -1 )
        end)
        timer.Create("aimbotcounterstopsdfg"..owner:SteamID(),15,1,function()
            if not IsValid(self) or not IsValid(owner) or not self.Aimbot then return end
            owner.usedaimbot = true
            self.activated = false
            self.Aimbot.Target = nil
        end)
        hook.Add("TTTPrepareRound","sandwichstuffaimbotzdfzdf"..owner:SteamID(),function()
            if IsValid(owner) then
                if not IsValid(self) or not IsValid(owner) or not self.Aimbot then return end
                owner.usedaimbot = false
                if not self.Aimbot then
                    self.Aimbot = {}
                end
                self.Aimbot.Target = nil
            end
        end)
    end
    if self.activated then
        local ent = self:GetClosestTarget()
        self.Aimbot.Target = ent ~= 0 and ent or nil
    end
end
-- end aimbot
function SWEP:Deploy()
    self.Aimbot = {}

    self:SendWeaponAnim(ACT_VM_DRAW_SILENCED)

    return self.BaseClass.Deploy(self)
end

SWEP.nextsound = 0
function SWEP:PrimaryAttack()
    if self.Owner:KeyDown(IN_RELOAD) then
        self:SwapPlayerProp()
        return
    end
    if self.Aimbot.Target ~= nil then
        self.Owner:SetEyeAngles((self:GetHeadPos(self.Aimbot.Target) - self.Owner:GetShootPos()):Angle())
    end
    self.Weapon:SetNextPrimaryFire(CurTime() + self.Primary.Delay)
    if (!self:CanPrimaryAttack()) then return end
    self.Weapon:EmitSound(self.Primary.Sound)

    local bullet = {}
    bullet.Num = self.Primary.NumShots
    bullet.Src = self.Owner:GetShootPos()
    bullet.Dir = self.Owner:GetAimVector()
    bullet.Spread = Vector(self.Primary.Cone,self.Primary.Cone,0)
    bullet.Tracer = 1
    bullet.Force = 10
    bullet.Damage = self.Primary.Damage

    if SERVER and self.nextsound < CurTime() then
        self.nextsound = CurTime() + 5
      --  gb_PlaySoundFromServer(gb_config.websounds.."sandhax4.mp3", self.Owner)
    end

    self.Owner:FireBullets(bullet)
    self.Weapon:SendWeaponAnim(ACT_VM_PRIMARYATTACK)
    self.Owner:MuzzleFlash()
    self.Owner:SetAnimation(PLAYER_ATTACK1)

    self:TakePrimaryAmmo(1) -- I know I broke my promise, but I want this gun to be a wee bit less dissapointing..
end

function SWEP:PreDrop()
    self:SetZoom(false)
    self:SetIronsights(false)
    return self.BaseClass.PreDrop(self)
end

SWEP.NextReload = 0
function SWEP:Reload()

    if self.Owner:KeyDown(IN_WALK) then
        self.NextReload = CurTime() + 5
        self.Weapon:DefaultReload( ACT_VM_RELOAD );
        self:SetIronsights( false )
        self:SetZoom(false)
        return end

    if self.Owner:KeyDown(IN_USE) then return end
    self:Fly()
    if self.NextReload > CurTime() or self.Owner:KeyDown(IN_USE) then return end
    self.NextReload = CurTime() + 5
    if SERVER then
        gb_PlaySoundFromServer(gb_config.websounds.."sandhax"..math.random(1,4)..".mp3", self.Owner)
    end
end

function SWEP:Holster()
    self:SetIronsights(false)
    self:SetZoom(false)
    return true
end

SWEP.IronSightsPos      = Vector( -100, -100, -100 )
SWEP.IronSightsAng      = Vector( 2.6, 1.37, 3.5 )
SWEP.Secondary.Sound = Sound("Default.Zoom")
-- Add some zoom to ironsights for this gun
function SWEP:Zoom()
    if not self.IronSightsPos then return end
    if self.Weapon:GetNextSecondaryFire() > CurTime() then return end

    local bIronsights = not self:GetIronsights()

    self:SetIronsights( bIronsights )

    if SERVER then
        self:SetZoom(bIronsights)
    else
        self:EmitSound(self.Secondary.Sound)
    end

    self.Weapon:SetNextSecondaryFire( CurTime() + 0.3)
end




function SWEP:SetZoom(state)
    if CLIENT then
        return
    elseif IsValid(self.Owner) and self.Owner:IsPlayer() then
        if state then
            self.Owner:SetFOV(30, 0.3)
        else
            self.Owner:SetFOV(0, 0.2)
        end
    end
end

if CLIENT then
    local scope = surface.GetTextureID("sprites/scope")
    function SWEP:DrawHUD()
        if self:GetIronsights() then
            surface.SetDrawColor( 0, 0, 0, 255 )

            local x = ScrW() / 2.0
            local y = ScrH() / 2.0
            local scope_size = ScrH()

            -- crosshair
            local gap = 80
            local length = scope_size
            surface.DrawLine( x - length, y, x - gap, y )
            surface.DrawLine( x + length, y, x + gap, y )
            surface.DrawLine( x, y - length, x, y - gap )
            surface.DrawLine( x, y + length, x, y + gap )

            gap = 0
            length = 50
            surface.DrawLine( x - length, y, x - gap, y )
            surface.DrawLine( x + length, y, x + gap, y )
            surface.DrawLine( x, y - length, x, y - gap )
            surface.DrawLine( x, y + length, x, y + gap )


            -- cover edges
            local sh = scope_size / 2
            local w = (x - sh) + 2
            surface.DrawRect(0, 0, w, scope_size)
            surface.DrawRect(x + sh - 2, 0, w, scope_size)

            surface.SetDrawColor(255, 0, 0, 255)
            surface.DrawLine(x, y, x + 1, y + 1)

            -- scope
            surface.SetTexture(scope)
            surface.SetDrawColor(255, 255, 255, 255)

            surface.DrawTexturedRectRotated(x, y, scope_size, scope_size, 0)

        else
            return self.BaseClass.DrawHUD(self)
        end
    end

    function SWEP:AdjustMouseSensitivity()
        return (self:GetIronsights() and 0.2) or nil
    end
end


function SWEP:DrawRotatingCrosshair(x,y,time,length,gap)
    surface.DrawLine(
        x + (math.sin(math.rad(time)) * length),
        y + (math.cos(math.rad(time)) * length),
        x + (math.sin(math.rad(time)) * gap),
        y + (math.cos(math.rad(time)) * gap)
    )
end

function SWEP:GetCoordiantes(ent)
    local min,max = ent:OBBMins(),ent:OBBMaxs()
    local corners = {
        Vector(min.x,min.y,min.z),
        Vector(min.x,min.y,max.z),
        Vector(min.x,max.y,min.z),
        Vector(min.x,max.y,max.z),
        Vector(max.x,min.y,min.z),
        Vector(max.x,min.y,max.z),
        Vector(max.x,max.y,min.z),
        Vector(max.x,max.y,max.z)
    }

    local minx,miny,maxx,maxy = ScrW() * 2,ScrH() * 2,0,0
    for _,corner in pairs(corners) do
        local screen = ent:LocalToWorld(corner):ToScreen()
        minx,miny = math.min(minx,screen.x),math.min(miny,screen.y)
        maxx,maxy = math.max(maxx,screen.x),math.max(maxy,screen.y)
    end
    return minx,miny,maxx,maxy
end

function SWEP:FixName(ent)
    if ent:IsPlayer() then return ent:Name() end
    if ent:IsNPC() then return ent:GetClass():sub(5,-1) end
    return ""
end

function SWEP:DrawHUD()
    local x,y = ScrW(),ScrH()
    local w,h = x/2,y/2

    surface.SetDrawColor(Color(0,0,0,235))
    surface.DrawRect(w - 1, h - 3, 3, 7)
    surface.DrawRect(w - 3, h - 1, 7, 3)

    surface.SetDrawColor(Color(0,255,10,230))
    surface.DrawLine(w, h - 2, w, h + 3)
    surface.DrawLine(w - 2, h, w + 3, h)

    local time = CurTime() * -180
    local scale = 10 * 0.02 -- self.Cone
    local gap = 40 * scale
    local length = gap + 20 * scale

    surface.SetDrawColor(250,10,0,150)






        self:DrawRotatingCrosshair(w,h,time,      length,gap)
        self:DrawRotatingCrosshair(w,h,time + 90, length,gap)
        self:DrawRotatingCrosshair(w,h,time + 180,length,gap)
        self:DrawRotatingCrosshair(w,h,time + 270,length,gap)
    if self.Aimbot.Target ~= nil and self.activated then
        local shotttext = "Target locked... ("..self:FixName(self.Aimbot.Target).."). Timer: "..self:GetNWInt("iambotcountdown",15)
        surface.SetFont( "ChatFont" );
        local size_x, size_y = surface.GetTextSize( shotttext );

        draw.RoundedBox( 6, ScrW( ) / 2 - size_x / 2 - 5, ScrH( ) - 100, size_x + 15, size_y + 10, Color( 0, 0, 0, 150 ) );
        draw.DrawText( shotttext , "ChatFont", ScrW( ) / 2, ScrH( ) - 100 + 5, Color( 255, 30, 30, 255 ), TEXT_ALIGN_CENTER );

    end
    if not self.activated and not self.Owner.usedaimbot then
        local shotttext = "Press E+R for 10 sec aimbot"
        surface.SetFont( "ChatFont" );
        local size_x, size_y = surface.GetTextSize( shotttext );

        draw.RoundedBox( 6, ScrW( ) / 2 - size_x / 2 - 5, ScrH( ) - 100, size_x + 15, size_y + 10, Color( 0, 0, 0, 150 ) );
        draw.DrawText( shotttext , "ChatFont", ScrW( ) / 2, ScrH( ) - 100 + 5, Color( 255, 30, 30, 255 ), TEXT_ALIGN_CENTER );
    end
    if  not self.Owner.usedwallhack then
        local shotttext = "Press E+LMB for 10 sec wallhack"
        surface.SetFont( "ChatFont" );
        local size_x, size_y = surface.GetTextSize( shotttext );

        draw.RoundedBox( 6, ScrW( ) / 2 - size_x / 2 - 5, ScrH( ) - 150, size_x + 15, size_y + 10, Color( 0, 0, 0, 150 ) );
        draw.DrawText( shotttext , "ChatFont", ScrW( ) / 2, ScrH( ) - 150 + 5, Color( 255, 30, 30, 255 ), TEXT_ALIGN_CENTER );
    end
end

local maxrange = 10000

local math = math

-- Returns if an entity is a valid physhammer punching target. Does not take
-- distance into account.
local function ValidTarget(ent)
    return IsValid(ent) and ent:IsPlayer() and not ent:IsGhost()
    -- NOTE: cannot check for motion disabled on client
end

if CLIENT then
    local surface = surface

    local linex = 0
    local liney = 0
    local laser = Material("trails/laser")
    function SWEP:ViewModelDrawn()
        local client = LocalPlayer()
        local vm = client:GetViewModel()
        if not IsValid(vm) then return end

        local plytr = client:GetEyeTrace(MASK_SHOT)

        local muzzle_angpos = vm:GetAttachment(1)
        local spos = muzzle_angpos.Pos  + muzzle_angpos.Ang:Up() * 15
        local epos = client:GetShootPos() + client:GetAimVector() * maxrange

        -- Painting beam
        local tr = util.TraceLine({start=spos, endpos=epos, filter=client, mask=MASK_ALL})

        local c = COLOR_RED
        local a = 150
        local d = (plytr.StartPos - plytr.HitPos):Length()
        if plytr.HitNonWorld then
            if ValidTarget(plytr.Entity) then
                if d < maxrange then
                    c = COLOR_GREEN
                    a = 255
                else
                    c = COLOR_RED
                end
            end
        end


        render.SetMaterial(laser)
        render.DrawBeam(spos, tr.HitPos, 5, 0, 0, c)
    end



        local function MESPCheck(v)
            if v:Alive() == true && v:Health() ~= 0 && v:Health() >= 0 && v ~= LocalPlayer() && LocalPlayer():Alive() then
            return true
            else
                return false
            end
        end

        local function coordinates( ent )
            local min, max = ent:OBBMins(), ent:OBBMaxs()
            local corners = {
                Vector( min.x, min.y, min.z ),
                Vector( min.x, min.y, max.z ),
                Vector( min.x, max.y, min.z ),
                Vector( min.x, max.y, max.z ),
                Vector( max.x, min.y, min.z ),
                Vector( max.x, min.y, max.z ),
                Vector( max.x, max.y, min.z ),
                Vector( max.x, max.y, max.z )
            }

            local minX, minY, maxX, maxY = ScrW() * 2, ScrH() * 2, 0, 0
            for _, corner in pairs( corners ) do
                local onScreen = ent:LocalToWorld( corner ):ToScreen()
                minX, minY = math.min( minX, onScreen.x ), math.min( minY, onScreen.y )
                maxX, maxY = math.max( maxX, onScreen.x ), math.max( maxY, onScreen.y )
            end

            return minX, minY, maxX, maxY
        end

        hook.Add("HUDPaint", "BoxEdddSP", function()
            if LocalPlayer().wallhackactivated != true then return end
            for k,v in pairs(player.GetAll()) do
                if(v ~= LocalPlayer() and MESPCheck(v)) then
                    local Box1x,Box1y,Box2x,Box2y = coordinates(v)
                    surface.SetDrawColor(team.GetColor(v:Team()))
                    surface.DrawLine( Box1x, Box1y, math.min( Box1x + 5, Box2x ), Box1y )
                    surface.DrawLine( Box1x, Box1y, Box1x, math.min( Box1y + 5, Box2y ) )
                    surface.DrawLine( Box2x, Box1y, math.max( Box2x - 5, Box1x ), Box1y )
                    surface.DrawLine( Box2x, Box1y, Box2x, math.min( Box1y + 5, Box2y ) )
                    surface.DrawLine( Box1x, Box2y, math.min( Box1x + 5, Box2x ), Box2y )
                    surface.DrawLine( Box1x, Box2y, Box1x, math.max( Box2y - 5, Box1y ) )
                    surface.DrawLine( Box2x, Box2y, math.max( Box2x - 5, Box1x ), Box2y )
                    surface.DrawLine( Box2x, Box2y, Box2x, math.max( Box2y - 5, Box1y ) )
                end
            end

        end)

        hook.Add("HUDPaint", "WallHaxsdddandwich", function()
            if LocalPlayer().wallhackactivated != true then return end
            for k,v in pairs(player.GetAll()) do
                if MESPCheck(v) then
                    cam.Start3D(EyePos(), EyeAngles())
                    render.SetBlend( 0.7 )
                    render.SetColorModulation( 3, 5, 3 )
                    v:DrawModel()
                    cam.End3D()
                end
            end
        end)

end