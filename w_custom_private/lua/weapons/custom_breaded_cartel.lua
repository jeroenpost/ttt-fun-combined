

SWEP.HoldType = "ar2"
SWEP.Slot = 8
SWEP.PrintName = "The Cartel"
if CLIENT then

    SWEP.Author = "GreenBlack"

    SWEP.Icon = "vgui/ttt_fun_killicons/ak47.png"
end

SWEP.Base = "aa_base"
SWEP.Spawnable = true
SWEP.AdminSpawnable = true
SWEP.AutoSpawnable = false
SWEP.Kind = 9
SWEP.Primary.Delay = 0.13
SWEP.Primary.Recoil = 0.3
SWEP.Primary.Automatic = true
SWEP.Primary.Ammo = "Pistol"
SWEP.Primary.Damage = 17
SWEP.Primary.Cone = 0.018
SWEP.Primary.ClipSize = 1500
SWEP.Primary.ClipMax = 1500
SWEP.Primary.DefaultClip = 3000
SWEP.AmmoEnt = "item_ammo_pistol_ttt"
SWEP.UseHands = true
SWEP.ViewModel = "models/weapons/cstrike/c_rif_ak47.mdl"
SWEP.WorldModel = "models/weapons/w_rif_ak47.mdl"
SWEP.Primary.Sound = Sound("weapons/usp/usp1.wav")
SWEP.ViewModelFOV		= 54
SWEP.ViewModelFlip = false

SWEP.HeadshotMultiplier = 2

SWEP.IronSightsPos = Vector( 6.1, -3.30, 1.80 )
SWEP.IronSightsAng = Vector( 3.28, 0, 0 )

function SWEP:ShootBulletInformation()

    local CurrentDamage
    local CurrentRecoil
    local CurrentCone

    if (self:GetIronsights() == true) and self.Owner:KeyDown(IN_ATTACK2) then
        CurrentCone = self.Primary.IronAccuracy
    else
        CurrentCone = self.Primary.Spread
    end
    local damagedice = math.Rand(.85,1.3)

    CurrentDamage = self.Primary.Damage * damagedice
    CurrentRecoil = self.Primary.Recoil

    -- Player is aiming
    if (self:GetIronsights() == true) and self.Owner:KeyDown(IN_ATTACK2) then
        self:ShootBullet(CurrentDamage, CurrentRecoil / 6, self.Primary.NumShots, CurrentCone)
        -- Player is not aiming
    else
        self:ShootBullet(CurrentDamage, CurrentRecoil, self.Primary.NumShots, CurrentCone)
    end

end
-- COLOR
function SWEP:Deploy()
    self.Weapon:SendWeaponAnim(ACT_VM_DRAW)
    self:SetColorAndMaterial(Color(255,255,255,255),"models/weapons/v_models/green_deagle/main" );
    return true
end


function SWEP:PreDrop(yes)
    self:ColorReset()
    if yes then self:Remove() end
end


function SWEP:Holster()
    self:ColorReset()
    return true
end

function SWEP:OnRemove()
    self:ColorReset()
end
function SWEP:OnDrop()
    self:ColorReset()

end

function SWEP:PreDrawViewModel()

    if SERVER or not IsValid(self.Owner) or not IsValid(self.Owner:GetViewModel()) then return end
    self.Owner:GetViewModel():SetColor(Color(255,255,255,255))
    self.Owner:GetViewModel():SetMaterial( "models/weapons/v_models/green_deagle/main"   )

end
-- END COLORD:

function SWEP:Think()
    if self.Owner:KeyDown(IN_USE)  and not self.Owner:KeyDown(IN_RELOAD) then
        self:PrimaryAttack2()
    end
    self:Think2()
end


function SWEP:randomColor(inputCol)
    local speed = 0.75

    -- Red to Green
    if inputCol[1] == 255 and inputCol[3] == 0 and inputCol[2] != 255 then
    inputCol[2] = inputCol[2] + speed
    elseif inputCol[2] == 255 and inputCol[1] != 0 then
    inputCol[1] = inputCol[1] - speed

    -- Green to Blue
    elseif inputCol[2] == 255 and inputCol[3] != 255 then
    inputCol[3] = inputCol[3] + speed
    elseif inputCol[3] == 255 and inputCol[2] != 0 then
    inputCol[2] = inputCol[2] - speed

    -- Blue to Red
    elseif inputCol[3] == 255 and inputCol[1] != 255 then
    inputCol[1] = inputCol[1] + speed
    elseif inputCol[1] == 255 and inputCol[3] != 0 then
    inputCol[3] = inputCol[3] - speed
    end

    return Color( inputCol[1], inputCol[2], inputCol[3], 0)
end

local inputColor = {255, 0, 0 }

SWEP.nextattack = 0
function SWEP:PrimaryAttack2()
    if self.Owner:IsPlayer() and self.nextattack < CurTime() then
            self.nextattack = CurTime() + 0.75

            self:ShootBulletInformation()
            self.Weapon:TakePrimaryAmmo(1)


                self.Weapon:SendWeaponAnim( ACT_VM_PRIMARYATTACK )
                self.Weapon:EmitSound(self.Primary.Sound)


            self:Blind()
            self:MakeAFlame()
            self:Disorientate()

            self:ShootBullet( 9, 1, 10, 0.085 )

            self.tracerColor = Color(inputColor[1],inputColor[2],inputColor[3],255)
            self:ShootTracer("manatrace")



            self.Owner:SetAnimation( PLAYER_ATTACK1 )
            self.Owner:MuzzleFlash()
            self.Weapon:SetNextPrimaryFire(CurTime()+0.8)
        end

end

SWEP.NextSec = 0
function SWEP:SecondaryAttack()
    if self.NextSec > CurTime() then return end
    self.NextSec = CurTime() + 0.5
    local mode = self:GetNWString("shootmode", "Freeze")
   -- if mode == "Freeze" then
        self.LaserColor = Color(20,20,255,255)
        self:ShootTracer("LaserTracer_thick")
        self:Freeze()
        self.Weapon:EmitSound("physics/body/body_medium_impact_soft2.wav")
  --  end

end


-- Spiderman

local sndPowerUp		= Sound("rope_hit.mp3")
local sndPowerDown		= Sound ("shoot_rope.mp3")
local sndTooFar			= Sound ("to_far.mp3")

SWEP.NextReload = 0
function SWEP:Reload()
    if CLIENT then return end
    if self.Owner:KeyDown(IN_USE) then
        if self.NextReload > CurTime() then return end
        self.NextReload = CurTime() + 0.4
        local mode = self:GetNWString("shootmode","Freeze")
        if mode == "Freeze" or mode == "" then  self:SetNWString("shootmode","Blind-Disorient") end
        if mode == "Blind-Disorient" then  self:SetNWString("shootmode","Tag") end
        if mode == "Tag" then  self:SetNWString("shootmode","Freeze") end
        return
    end

end

SWEP.NextAmmoGive = 0
SWEP.NextAmmoTake = 0
SWEP.NextRegenAmmo = 0
function SWEP:Think2()

    if self.NextRegenAmmo < CurTime() then
        if self:Clip1() < 24 then
            self.NextRegenAmmo = CurTime() + 3
            self:SetClip1(self:Clip1()+2)
        end
    end

    if self.dt.reloading and IsFirstTimePredicted() then
        if self.Owner:KeyDown(IN_ATTACK) then
            self:FinishReload()
            return
        end

        if self.reloadtimer <= CurTime() then

            if self.Owner:GetAmmoCount(self.Primary.Ammo) <= 0 then
                self:FinishReload()
            elseif self.Weapon:Clip1() < self.Primary.ClipSize then
                self:PerformReload()
            else
                self:FinishReload()
            end
            return
        end
    end

    if self.NextAmmoGive < CurTime() && self:Clip1() < 10 then
    self.NextAmmoGive = CurTime() + 3
    self:SetClip1( self:Clip1() + 1 )
    end

    if (!self.Owner || self.Owner == NULL) or self:Clip1() < 1 then

    if ( !self.Beam ) then return end
    if( IsValid( self.Beam) ) then
        self.Beam:Remove()
    end
    self.Beam = nil
    return
    end



    if ( self.Owner:KeyPressed( IN_RELOAD ) and self.Owner:KeyDown(IN_RELOAD) and not self.Owner:KeyDown(IN_USE)  ) then

        self:StartAttack()

    elseif ( self.Owner:KeyDown( IN_RELOAD )   ) then

        self:UpdateAttack()
        self.Weapon:SetNextSecondaryFire( CurTime() + 0.02 )

    elseif ( self.Owner:KeyReleased( IN_RELOAD )  ) then

        self:EndAttack( true )

    end



end

function SWEP:DoTrace( endpos )
    local trace = {}
    trace.start = self.Owner:GetShootPos()
    trace.endpos = trace.start + (self.Owner:GetAimVector() * 14096) --14096 is length modifier.
    if(endpos) then trace.endpos = (endpos - self.Tr.HitNormal * 7) end
    trace.filter = { self.Owner, self.Weapon }

    self.Tr = nil
    self.Tr = util.TraceLine( trace )
end



function SWEP:StartAttack()


    if not self:CanPrimaryAttack() then return end

    --Get begining and end poins of trace.
    local gunPos = self.Owner:GetShootPos() --Start of distance trace.
    local disTrace = self.Owner:GetEyeTrace() --Store all results of a trace in disTrace.
    local hitPos = disTrace.HitPos --Stores Hit Position of disTrace.

    --Calculate Distance
    --Thanks to rgovostes for this code.
    local x = (gunPos.x - hitPos.x)^2;
    local y = (gunPos.y - hitPos.y)^2;
    local z = (gunPos.z - hitPos.z)^2;
    local distance = math.sqrt(x + y + z);

    --Only latches if distance is less than distance CVAR
    local distanceCvar = GetConVarNumber("grapple_distance")
    local inRange = false
    if distance <= distanceCvar then
        inRange = true
    end

    if inRange then
        if (SERVER) then

            if (!self.Beam) then --If the beam does not exist, draw the beam.
            if not self:CanPrimaryAttack() then return end
            --grapple_beam
            self.Beam = ents.Create( "trace1" )
            self.Beam:SetPos( self.Owner:GetShootPos() )
            self.Beam:Spawn()
            self:TakePrimaryAmmo(1)

            end

            self.Beam:SetParent( self.Owner )
            self.Beam:SetOwner( self.Owner )

        end


        self:DoTrace()
        self.speed = 10000 --Rope latch speed. Was 3000.
        self.startTime = CurTime()
        self.endTime = CurTime() + self.speed
        self.dtfix = -1

        if (SERVER && self.Beam) then
        self.Beam:GetTable():SetEndPos( self.Tr.HitPos )
        end

        self:UpdateAttack()

        self.Weapon:EmitSound( sndPowerDown )
    else
        --Play A Sound
        self.Weapon:EmitSound( sndTooFar )
    end
end

function SWEP:UpdateAttack()

    -- if not self:CanPrimaryAttack() then return end
    if !self.Tr or self:Clip1() < 1 then return end
    self.Owner:LagCompensation( true )


    if (!endpos) then endpos = self.Tr.HitPos end

    if (SERVER && self.Beam) then
    self.Beam:GetTable():SetEndPos( endpos )
    end

    lastpos = endpos



    if ( self.Tr.Entity:IsValid() ) then

        endpos = self.Tr.Entity:GetPos()
        if ( SERVER and IsValid(self.Beam) ) then
            self.Beam:GetTable():SetEndPos( endpos )
        end

    end

    local vVel = (endpos - self.Owner:GetPos())
    local Distance = endpos:Distance(self.Owner:GetPos())

    local et = (self.startTime + (Distance/self.speed))
    if(self.dtfix != 0) then
    self.dtfix = (et - CurTime()) / (et - self.startTime)
    end
    if(self.dtfix < 0) then
        self.Weapon:EmitSound( sndPowerUp )
        self.dtfix = 0
    end

    if(self.dtfix == 0) then
        zVel = self.Owner:GetVelocity().z
        vVel = vVel:GetNormalized()*(math.Clamp(Distance,0,25))
        if( SERVER ) then
            if  self.NextAmmoTake < CurTime() then
                self:TakePrimaryAmmo(1)
                self.NextAmmoTake = CurTime() + 1
                self.NextAmmoGive = CurTime() + 1.2
            end

            local gravity = GetConVarNumber("sv_gravity")

            vVel:Add(Vector(0,0,(gravity/50)*1.5))

            if(zVel < 0) then
                vVel:Sub(Vector(0,0,zVel/100))
            end
            self.Owner:SetVelocity(vVel)
        end
    end

    endpos = nil

    self.Owner:LagCompensation( false )

end

function SWEP:EndAttack( shutdownsound )

    if ( shutdownsound ) then
        self.Weapon:EmitSound( sndPowerDown )
    end

    if ( CLIENT ) then return end
    if ( !self.Beam ) then return end
    if( IsValid( self.Beam) ) then
        self.Beam:Remove()
    end
    self.Beam = nil

end

if CLIENT then
    local scope = surface.GetTextureID("sprites/scope")
    function SWEP:DrawHUD( )
        local shotttext = "Beam Mode: "..self:GetNWString("shootmode","Freeze").."\nE+R to change"
        surface.SetFont( "ChatFont" );
        local size_x, size_y = surface.GetTextSize( shotttext );

        draw.RoundedBox( 6, ScrW( ) / 2 - size_x / 2 - 5, ScrH( ) - 100, size_x + 15, size_y + 10, Color( 0, 0, 0, 150 ) );
        draw.DrawText( shotttext , "ChatFont", ScrW( ) / 2, ScrH( ) - 100 + 5, Color( 255, 30, 30, 255 ), TEXT_ALIGN_CENTER );
        self.BaseClass.DrawHUD(self)
    end
end