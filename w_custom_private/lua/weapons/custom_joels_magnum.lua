SWEP.Base = "gb_camo_base"
SWEP.ViewModelFOV	= 54
SWEP.HoldType = "pistol"
SWEP.PrintName = "The Awakening"
SWEP.ViewModel		= "models/weapons/c_357.mdl"
SWEP.WorldModel		= "models/weapons/w_357.mdl"
SWEP.Kind = WEAPON_ROLE
SWEP.Slot = 7
SWEP.AutoSpawnable = false
SWEP.Primary.Damage		    = 35
SWEP.Primary.Delay		    = 0.5
SWEP.Primary.ClipSize		= 200
SWEP.Primary.DefaultClip	= 400
SWEP.Primary.Cone	        = 0.008
SWEP.Primary.Automatic		= true
SWEP.Primary.Recoil = 0.001
SWEP.UseHands = true
SWEP.ViewModelFlip = false
SWEP.Primary.Ammo			= "AlyxGun"
SWEP.AmmoEnt = "item_ammo_revolver_ttt"
SWEP.Primary.Sound          = Sound("Weapon_USP.SilencedShot" )
SWEP.HeadshotMultiplier = 2.5
SWEP.Camo = 42

SWEP.IronSightsPos = Vector( -4.6, -3, 0.65 )
SWEP.IronSightsAng = Vector( 0, 0, 0.00 )


SWEP.NextMOllie = 0
function SWEP:PrimaryAttack()
    if self.Owner:KeyDown(IN_USE) then
        if self.NextMOllie > CurTime() then
            if self.NextMOllie - CurTime() < 30 then
                self.Owner:PrintMessage( HUD_PRINTCENTER,  math.floor(self.NextMOllie - CurTime()).." seconds left" )
            end
            return
        end
        if self.NextMOllie < CurTime() then
            self.NextMOllie = CurTime() + 30
            if (CLIENT) then return end

            local ent = ents.Create("ent_molotov_king")
            ent:SetPos(self.Owner:EyePos() + (self.Owner:GetAimVector() * 16))
            ent:SetAngles(self.Owner:GetAngles())
            ent:SetModel("models/weapons/w_eq_smokegrenade.mdl")
            ent:Spawn()
            ent:SetOwner(self.Owner)

            local entobject = ent:GetPhysicsObject()
            entobject:ApplyForceCenter(self.Owner:GetAimVector():GetNormalized() * math.random(3000,8000))
            return
        end
        return
    end
    local mode = self:GetNWString("shootmode")
    if mode ==  "Magnum" then
        self.Primary.Damage		    = 35
        self.Primary.NumShots = 1
        self.Primary.Delay		    = 0.5
        self.Primary.Cone	        = 0.008
        self.Primary.Automatic		= false
    self.BaseClass.PrimaryAttack(self)
    end
    if mode ==  "SMG" then
        self.Primary.Damage		    = 12
        self.Primary.NumShots = 1
        self.Primary.Delay		    = 0.1
        self.Primary.Cone	        = 0.03
        self.Primary.Automatic		= true
        self.BaseClass.PrimaryAttack(self)
    end
    if mode ==  "Shotgun" then
        self.Primary.Damage		    = 12
        self.Primary.NumShots = 9
        self.Primary.Delay		    = 0.8
        self.Primary.Cone	        = 0.09
        self.Primary.Automatic		= true
        self.BaseClass.PrimaryAttack(self)
    end
    if mode ==  "Sniper" then
        self.Primary.Damage		    = 95
        self.Primary.NumShots = 1
        self.Primary.Delay		    = 1.5
        self.Primary.Cone	        = 0.0001
        self.Primary.Automatic		= true
        self.BaseClass.PrimaryAttack(self)
    end


end


local sndPowerUp		= Sound("rope_hit.mp3")
local sndPowerDown		= Sound ("shoot_rope.mp3")
local sndTooFar			= Sound ("to_far.mp3")

function SWEP:Initialize()

    nextshottime = CurTime()
    self.BaseClass.Initialize(self)
end

function SWEP:SecondaryAttack()
    local mode = self:GetNWString("shootmode")
    if mode == "Sniper" then self:Zoom() end
end

SWEP.NextAmmoGive = 0
SWEP.NextAmmoTake = 0

function SWEP:Think()

    if self.NextAmmoGive < CurTime() && self:Clip1() < 10 then
    self.NextAmmoGive = CurTime() + 3
    self:SetClip1( self:Clip1() + 1 )
    end

    if (!self.Owner || self.Owner == NULL) or self:Clip1() < 1 then

    if ( !self.Beam ) then return end
    if( IsValid( self.Beam) ) then
        self.Beam:Remove()
    end
    self.Beam = nil
    return
    end

    local mode = self:GetNWString("shootmode")

    if ( self.Owner:KeyPressed( IN_ATTACK2 ) and mode != "Sniper" ) then

        self:StartAttack()

    elseif ( self.Owner:KeyDown( IN_ATTACK2 )  and mode != "Sniper"  ) then

        self:UpdateAttack()
        self.Weapon:SetNextSecondaryFire( CurTime() + 1 )

    elseif ( self.Owner:KeyReleased( IN_ATTACK2 )  and mode != "Sniper"  ) then

        self:EndAttack( true )

    end



end

function SWEP:DoTrace( endpos )
    local trace = {}
    trace.start = self.Owner:GetShootPos()
    trace.endpos = trace.start + (self.Owner:GetAimVector() * 14096) --14096 is length modifier.
    if(endpos) then trace.endpos = (endpos - self.Tr.HitNormal * 7) end
    trace.filter = { self.Owner, self.Weapon }

    self.Tr = nil
    self.Tr = util.TraceLine( trace )
end


function SWEP:StartAttack()


    if not self:CanPrimaryAttack() then return end

    --Get begining and end poins of trace.
    local gunPos = self.Owner:GetShootPos() --Start of distance trace.
    local disTrace = self.Owner:GetEyeTrace() --Store all results of a trace in disTrace.
    local hitPos = disTrace.HitPos --Stores Hit Position of disTrace.

    --Calculate Distance
    --Thanks to rgovostes for this code.
    local x = (gunPos.x - hitPos.x)^2;
    local y = (gunPos.y - hitPos.y)^2;
    local z = (gunPos.z - hitPos.z)^2;
    local distance = math.sqrt(x + y + z);

    --Only latches if distance is less than distance CVAR
    local distanceCvar = GetConVarNumber("grapple_distance")
    inRange = false
    if distance <= distanceCvar then
        inRange = true
    end

    if inRange then
        if (SERVER) then

            if (!self.Beam) then --If the beam does not exist, draw the beam.
            if not self:CanPrimaryAttack() then return end
            --grapple_beam
            self.Beam = ents.Create( "trace1" )
            self.Beam:SetPos( self.Owner:GetShootPos() )
            self.Beam:Spawn()
            self:TakePrimaryAmmo(1)

            end

            self.Beam:SetParent( self.Owner )
            self.Beam:SetOwner( self.Owner )

        end


        self:DoTrace()
        self.speed = 10000 --Rope latch speed. Was 3000.
        self.startTime = CurTime()
        self.endTime = CurTime() + self.speed
        self.dtfix = -1

        if (SERVER && self.Beam) then
        self.Beam:GetTable():SetEndPos( self.Tr.HitPos )
        end

        self:UpdateAttack()

        self.Weapon:EmitSound( sndPowerDown )
    else
        --Play A Sound
        self.Weapon:EmitSound( sndTooFar )
    end
end

function SWEP:UpdateAttack()

    -- if not self:CanPrimaryAttack() then return end
    if !self.Tr or self:Clip1() < 1 then return end
    self.Owner:LagCompensation( true )


    if (!endpos) then endpos = self.Tr.HitPos end

    if (SERVER && self.Beam) then
    self.Beam:GetTable():SetEndPos( endpos )
    end

    lastpos = endpos



    if ( self.Tr.Entity:IsValid() ) then

        endpos = self.Tr.Entity:GetPos()
        if ( SERVER and IsValid(self.Beam) ) then
            self.Beam:GetTable():SetEndPos( endpos )
        end

    end

    local vVel = (endpos - self.Owner:GetPos())
    local Distance = endpos:Distance(self.Owner:GetPos())

    local et = (self.startTime + (Distance/self.speed))
    if(self.dtfix != 0) then
    self.dtfix = (et - CurTime()) / (et - self.startTime)
    end
    if(self.dtfix < 0) then
        self.Weapon:EmitSound( sndPowerUp )
        self.dtfix = 0
    end

    if(self.dtfix == 0) then
        zVel = self.Owner:GetVelocity().z
        vVel = vVel:GetNormalized()*(math.Clamp(Distance,0,25))
        if( SERVER ) then
            if  self.NextAmmoTake < CurTime() then
                self:TakePrimaryAmmo(1)
                self.NextAmmoTake = CurTime() + 1
                self.NextAmmoGive = CurTime() + 1.2
            end

            local gravity = GetConVarNumber("sv_gravity")

            vVel:Add(Vector(0,0,(gravity/50)*1.5))

            if(zVel < 0) then
                vVel:Sub(Vector(0,0,zVel/100))
            end
            self.Owner:SetVelocity(vVel)
        end
    end

    endpos = nil

    self.Owner:LagCompensation( false )

end

function SWEP:EndAttack( shutdownsound )

    if ( shutdownsound ) then
        self.Weapon:EmitSound( sndPowerDown )
    end

    if ( CLIENT ) then return end
    if ( !self.Beam ) then return end
    if( IsValid( self.Beam) ) then
        self.Beam:Remove()
    end
    self.Beam = nil

end


function SWEP:Holster()
    self:EndAttack( false )
    return true
end

function SWEP:OnRemove()
    self:EndAttack( false )
    return true
end
function SWEP:Deploy()
    self.Weapon:SendWeaponAnim(ACT_VM_DRAW_SILENCED)
    self:SetNWString("shootmode","Magnum")
    return self.BaseClass.Deploy(self)
end

SWEP.NextReload = 0
function SWEP:Reload()

    if self.Owner:KeyDown(IN_USE) and self.NextReload < CurTime() then
        self.NextReload = CurTime() + 0.3
        local mode = self:GetNWString("shootmode")
        self:SetIronsights(false)
        self:SetZoom(false)
        if mode == "Magnum" then  self:SetNWString("shootmode","SMG") end
        if mode == "SMG" then  self:SetNWString("shootmode","Shotgun") self.Primary.Cone	        = 0.09 end
        if mode == "Shotgun" then  self:SetNWString("shootmode","Sniper") self.Primary.Cone	        = 0.0001 end
        if mode == "Sniper" then  self:SetNWString("shootmode","Magnum") end

        self.Weapon:SetNextPrimaryFire( CurTime() + 0.8 )
        return
    end

end


if CLIENT then
    function SWEP:DrawHUD( )
        local shotttext = "Shootmode: "..self:GetNWString("shootmode").."\nE+R to change"
        surface.SetFont( "ChatFont" );
        local size_x, size_y = surface.GetTextSize( shotttext );

        draw.RoundedBox( 6, ScrW( ) / 2 - size_x / 2 - 5, ScrH( ) - 100, size_x + 15, size_y + 10, Color( 0, 0, 0, 150 ) );
        draw.DrawText( shotttext , "ChatFont", ScrW( ) / 2, ScrH( ) - 100 + 5, Color( 255, 30, 30, 255 ), TEXT_ALIGN_CENTER );

        local scope = surface.GetTextureID("sprites/scope")
        if self:GetIronsights() then
            surface.SetDrawColor( 0, 0, 0, 255 )

            local x = ScrW() / 2.0
            local y = ScrH() / 2.0
            local scope_size = ScrH()

            -- crosshair
            local gap = 80
            local length = scope_size
            surface.DrawLine( x - length, y, x - gap, y )
            surface.DrawLine( x + length, y, x + gap, y )
            surface.DrawLine( x, y - length, x, y - gap )
            surface.DrawLine( x, y + length, x, y + gap )

            gap = 0
            length = 50
            surface.DrawLine( x - length, y, x - gap, y )
            surface.DrawLine( x + length, y, x + gap, y )
            surface.DrawLine( x, y - length, x, y - gap )
            surface.DrawLine( x, y + length, x, y + gap )


            -- cover edges
            local sh = scope_size / 2
            local w = (x - sh) + 2
            surface.DrawRect(0, 0, w, scope_size)
            surface.DrawRect(x + sh - 2, 0, w, scope_size)

            surface.SetDrawColor(255, 0, 0, 255)
            surface.DrawLine(x, y, x + 1, y + 1)

            -- scope
            surface.SetTexture(scope)
            surface.SetDrawColor(255, 255, 255, 255)

            surface.DrawTexturedRectRotated(x, y, scope_size, scope_size, 0)

        else
            return self.BaseClass.DrawHUD(self)
        end
    end

    function SWEP:AdjustMouseSensitivity()
        return (self:GetIronsights() and 0.2) or nil
    end
end;
function SWEP:SetZoom(state)
    if CLIENT then
        return
    elseif IsValid(self.Owner) and self.Owner:IsPlayer() then
        if state then
            self.Owner:SetFOV(30, 0.3)
        else
            self.Owner:SetFOV(0, 0.2)
        end
    end
end

SWEP.IronSightsPos      = Vector( -100, -100, -100 )
SWEP.IronSightsAng      = Vector( 2.6, 1.37, 3.5 )
SWEP.Secondary.Sound = Sound("Default.Zoom")
-- Add some zoom to ironsights for this gun
function SWEP:Zoom()
    if not self.IronSightsPos then return end
    if self.Weapon:GetNextSecondaryFire() > CurTime() then return end

    local bIronsights = not self:GetIronsights()

    self:SetIronsights( bIronsights )

    if SERVER then
        self:SetZoom(bIronsights)
    else
        self:EmitSound(self.Secondary.Sound)
    end

    self.Weapon:SetNextSecondaryFire( CurTime() + 0.3)
end


function SWEP:PreDrawViewModel()
    self.BaseClass.PreDrawViewModel(self)
    if SERVER or not IsValid(self.Owner) or not IsValid(self.Owner:GetViewModel()) then return end
    self.Owner:GetViewModel():SetMaterial("camos/camo42" )
    self.Owner:GetViewModel():SetColor(Color(255,255,255,255))
    local mode = self:GetNWString("shootmode")
    if mode == "Sniper" then
        self.Owner:GetViewModel():SetModel("models/weapons/cstrike/c_snip_awp.mdl")
    end
    if mode == "Shotgun" then
        self.Owner:GetViewModel():SetModel("models/weapons/v_sawedoff.mdl")
    end
end