

SWEP.Base = "gb_base"
SWEP.Kind = WEAPON_EQUIP2
SWEP.HoldType = "melee2"
SWEP.ViewModelFOV = 20
SWEP.ViewModelFlip = false
SWEP.ViewModel = "models/weapons/v_crowbar.mdl"
SWEP.WorldModel = "models/food/burger.mdl"
SWEP.ShowViewModel = true
SWEP.ShowWorldModel = false
SWEP.ViewModelBoneMods = {}

SWEP.PrintName = "The Warlock"
SWEP.Primary.Delay          = 1.5
SWEP.Primary.Recoil         = 1
SWEP.Primary.Automatic = false
SWEP.Primary.Ammo = nil
SWEP.Primary.Damage = 0
SWEP.Primary.Cone = 0.005
SWEP.Primary.ClipSize = 25
SWEP.Primary.ClipMax = 100-- keep mirrored to ammo
SWEP.Primary.DefaultClip = 25
SWEP.Icon               = "vgui/ttt_fun_killicons/poseidons_fork.png"


SWEP.WElements = {
	["element_name"] = { type = "Model", model = "models/staff/staff.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(5, 0.455, 10.454), angle = Angle(176.932, -5.114, 5.113), size = Vector(0.5, 0.5, 0.5), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
}


SWEP.VElements = {
	["element_nameasd"] = { type = "Model", model = "models/staff/staff.mdl", bone = "ValveBiped.HandControlRotR", rel = "", pos = Vector(29.545, 9.545, 9.545), angle = Angle(178.977, 0, 0), size = Vector(0.5, 0.5, 0.5), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
}


function SWEP:PrimaryAttack(worldsnd)

   self.Weapon:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
   self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
   
   if not self:CanPrimaryAttack() then return end

    

   if not worldsnd then
      self.Weapon:EmitSound( self.Primary.Sound, 511 )
   elseif SERVER then
      sound.Play(self.Primary.Sound, self:GetPos(), 511)
   end

   self:ShootBullet( self.Primary.Damage, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )

   self:TakePrimaryAmmo( 1 )

   local owner = self.Owner
   if not IsValid(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

   owner:ViewPunch( Angle( math.Rand(-3,-3) * self.Primary.Recoil, math.Rand(-3,3) *self.Primary.Recoil, 0 ) )
    
    if CLIENT then return end
   if not (IsValid(self.Owner) and self.Owner:IsPlayer()) then return end
 
      self.Owner:SetFOV(95, 0.2)
   timer.Simple(0.4,function() if IsValid( self.Owner) then
      self.Owner:SetFOV(0, 0.2)
    end end)
   

end



function SWEP:ShootBullet( dmg, recoil, numbul, cone )

   local sights = self:GetIronsights()

   numbul = numbul or 1
   cone   = cone   or 0.01

   -- 10% accuracy bonus when sighting
   cone = sights and (cone * 0.9) or cone

   local bullet = {}
   bullet.Num    = numbul
   bullet.Src    = self.Owner:GetShootPos()
   bullet.Dir    = self.Owner:GetAimVector()
   bullet.Spread = Vector( cone, cone, 0 )
   bullet.Tracer = 4
   bullet.Force  = 5
   bullet.Damage = dmg

   bullet.Callback = function(att, tr, dmginfo)
  if SERVER or (CLIENT and IsFirstTimePredicted()) then

	    if (not tr.HitWorld)  then
		       local pl2 = 0
				 
                       if SERVER then
                        local ent = ents.Create( "env_explosion" )
    
                            ent:SetPos( tr.Entity:GetPos() )
                            ent:SetOwner( self.Owner  )
                            ent:SetPhysicsAttacker(  self.Owner )
                            ent:Spawn()
                            ent:SetKeyValue( "iMagnitude", "40" )
                            ent:Fire( "Explode", 0, 0 )

                            util.BlastDamage( self, self:GetOwner(), self:GetPos(), 50, 5 )

                            ent:EmitSound( "weapons/big_explosion.mp3" )
                         
                       end
				    pl2 = tr.Entity
				 
					
				 if SERVER then pl2:SetVelocity(self.Owner:GetForward() * 1800 + Vector(0,0,400)) end
			
		end
    end
    end
	   self.Owner:FireBullets( bullet )
	   self.Weapon:SendWeaponAnim(self.PrimaryAnim)

	   -- Owner can die after firebullets, giving an error at muzzleflash
	   if not IsValid(self.Owner) or not self.Owner:Alive() then return end

	   self.Owner:MuzzleFlash()
	   self.Owner:SetAnimation( PLAYER_ATTACK1 )

	 
end


SWEP.SecAm = 5

function SWEP:SecondaryAttack()
    


    self:SetNWBool("Used", true)
    self.LastOwner = self.Owner

    self.OldModel = self.Owner:GetModel()
     
    if SERVER && self.SecAm > 0 then
    self.Owner:ChatPrint("You are invisible")
    self.Owner:SetModel("models/shells/shell_9mm.mdl")
    self.Owner:SetColor( Color(255,255,255,1) )
     
    elseif SERVER then
        self.Owner:ChatPrint("Out of ammo")
        return 
    end
    
    timer.Create("GivedisguiseammoChazza",20,100,function()
        if IsValid(self) && self.SecAm < 5 then
             self.SecAm = 5
              self.Used2 = false
        end
    end)

    timer.Create("takePlantAmmo2"..self.LastOwner:SteamID(),1,30,function() if IsValid(self) then 
            self.SecAm = self.SecAm - 1
            if self.SecAm < 5 && IsValid(self.LastOwner) && not self.Used2 then
                 if SERVER then
                    self.LastOwner:ChatPrint("Your invisibility will be gone in "..self.SecAm.." seconds")
                end 
            end
            if self.SecAm < 1 && IsValid(self.LastOwner) and SERVER then
                ULib.invisible( self.LastOwner, false, 0 )
                if SERVER && not self.Used2 then
                self.Used2 = true
                self.Owner:SetModel(self.OldModel)
                self.Owner:SetColor( Color(255,255,255,255) )
                self.LastOwner:ChatPrint("You are not invisible anymore")
                end
            end
            
    end end)

    
    

end

function SWEP:OnDrop()
    if IsValid(self) then
        self.Used2 = true
        self:SetNWBool("Used", false)
    end
end