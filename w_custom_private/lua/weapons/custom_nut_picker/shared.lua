if( SERVER ) then
	AddCSLuaFile(  )
	--resource.AddFile("materials/vgui/ttt/fists.png")
end


	SWEP.PrintName = "Nut Picker"
	SWEP.DrawAmmo = false
	SWEP.DrawCrosshair = false
	
	


SWEP.Base = "aa_base"
SWEP.Kind = WEAPON_UNARMED

SWEP.CanBuy = nil -- no longer a buyable thing
--SWEP.LimitedStock = true

SWEP.AllowDrop = false
SWEP.Icon = "vgui/ttt_fun_killicons/fists.png"

SWEP.Author			= "robotboy655"
SWEP.Purpose		= "Well we sure as heck didn't use guns! We would wrestle Hunters to the ground with our bare hands! I would get ten, twenty a day, just using my fists."


SWEP.Spawnable			= true
SWEP.UseHands			= true
SWEP.DrawAmmo			= false

SWEP.ViewModel			= "models/weapons/c_arms_citizen.mdl"
SWEP.WorldModel			= ""

SWEP.ViewModelFOV		= 52
SWEP.ViewModelFlip		= false

SWEP.Primary.ClipSize		= -1
SWEP.Primary.DefaultClip	= -1
SWEP.Primary.Damage			= 35
SWEP.Primary.Automatic		= true
SWEP.Primary.Ammo			= "none"

SWEP.Secondary.ClipSize		= 1
SWEP.Secondary.DefaultClip	= 1
SWEP.Secondary.Damage			= 35
SWEP.Secondary.Automatic	= true
SWEP.Secondary.Ammo			= "Battery"

SWEP.Weight				= 5
SWEP.AutoSwitchTo		= false
SWEP.AutoSwitchFrom		= false 

SWEP.Slot				= 5
SWEP.SlotPos			= 5
SWEP.DrawAmmo			= false
SWEP.DrawCrosshair		= true
SWEP.secondUsed                 = false


local SwingSound = Sound( "weapons/slam/throw.wav" )
local HitSound = Sound( "Flesh.ImpactHard" )

function SWEP:Initialize()

	self:SetHoldType( "fist" )

end

function SWEP:Think()
   
end

function SWEP:PreDrawViewModel(viewModel, weapon, client)
    if SERVER or not IsValid(self.Owner) or not IsValid(viewModel) then return end
    viewModel:SetMaterial( "engine/occlusionproxy" )
end

SWEP.Distance = 67
SWEP.AttackAnimsRight = { "fists_left", "fists_right", "fists_uppercut" }
SWEP.AttackAnims = { "fists_right", "fists_uppercut" }
function SWEP:PrimaryAttack()
 self:AttackThing( self.AttackAnimsRight[ math.random( 1, #self.AttackAnimsRight ) ] )
end

function SWEP:SecondaryAttack()
    self:Fly();

    --if not self.secondUsed then
    --     self:AttackThing( "fists_left"  )
   -- else
    --    self:AttackThing( "fists_right")
   -- end
	
end

SWEP.NextReload = 0
function SWEP:Reload()
    if self.NextReload > CurTime() then return end
    self.NextReload = CurTime() + 0.5
    if not self.reloadused then
        self:GrapGun();
    else
        self.Owner:PrintMessage( HUD_PRINTCENTER, "You already duplicated a weapon" )
    end
end

function SWEP:GrapGun()
    if CLIENT then return end
    local tracedata = {}
    tracedata.start = self.Owner:GetShootPos()
    tracedata.endpos = self.Owner:GetShootPos() + (self.Owner:GetAimVector() * 100000)
    tracedata.filter = self.Owner
    tracedata.mins = Vector(1,1,1) * -10
    tracedata.maxs = Vector(1,1,1) * 10
    tracedata.mask = MASK_SHOT_HULL
    local tr = util.TraceLine({start = self.Owner:EyePos(), endpos = self.Owner:EyePos() + self.Owner:EyeAngles():Forward() * 80000, filter = self.Owner})

    local ply = self.Owner
    if IsValid(tr.Entity) and ( tr.Entity:IsPlayer() or tr.Entity:IsNPC() ) then
        if tr.Entity.NoNutpicker then
            ply:PrintMessage( HUD_PRINTCENTER, "Person has anti-nutpicker enabled" )
            return
        end

        local wep = tr.Entity:GetActiveWeapon()
        if IsValid(wep) and wep:GetClass() then
            wep:GetClass()
            local wepclass = wep:GetClass()

            for k,v in pairs(self.Owner:GetWeapons())do
                wep = weapons.GetStored(wepclass)
                if wep && wep.Kind && v.Kind == wep.Kind then
                if v.Kind  then
                WEPS.DropNotifiedWeapon(self.Owner,v, true)
                else
                    self.Owner:StripWeapon( v:GetClass() )
                end
                end
            end

            ply:Give( wepclass )
            ply:PrintMessage( HUD_PRINTCENTER, "Duplicated "..wepclass )
            ply:SelectWeapon(wepclass)
            self.reloadused = true
            return
        end
    else
    ply:PrintMessage( HUD_PRINTCENTER, "You missed " )
        end
end


function SWEP:DealDamage( anim )
	local tr = util.TraceLine( {
		start = self.Owner:GetShootPos(),
		endpos = self.Owner:GetShootPos() + self.Owner:GetAimVector() * self.Distance,
		filter = self.Owner
	} )

	if ( not IsValid( tr.Entity ) ) then 
		tr = util.TraceHull( {
			start = self.Owner:GetShootPos(),
			endpos = self.Owner:GetShootPos() + self.Owner:GetAimVector() * self.Distance,
			filter = self.Owner,
			mins = self.Owner:OBBMins() / 3,
			maxs = self.Owner:OBBMaxs() / 3
		} )
	end

	if ( tr.Hit ) then self.Owner:EmitSound( HitSound ) end

	if ( IsValid( tr.Entity ) && ( tr.Entity:IsNPC() || tr.Entity:IsPlayer() ) ) then
		local dmginfo = DamageInfo()
		dmginfo:SetDamage( self.Primary.Damage )
		if ( anim == "fists_left" ) then
			dmginfo:SetDamageForce( self.Owner:GetRight() * 49125 + self.Owner:GetForward() * 99984 ) -- Yes we need those specific numbers
                        dmginfo:SetDamage( self.Secondary.Damage )
                        

                        self.secondUsed = false
		elseif ( anim == "fists_right" ) then
			dmginfo:SetDamageForce( self.Owner:GetRight() * -49124 + self.Owner:GetForward() * 99899 )
                        dmginfo:SetDamage( self.Primary.Damage )
		elseif ( anim == "fists_uppercut" ) then
			dmginfo:SetDamageForce( self.Owner:GetUp() * 51589 + self.Owner:GetForward() * 100128 )
                        dmginfo:SetDamage( self.Primary.Damage )
		end
                tr.Entity:SetVelocity(self.Owner:GetForward() * 600 + Vector(0,0,400))
		dmginfo:SetInflictor( self )
		local attacker = self.Owner
		if ( !IsValid( attacker ) ) then attacker = self end
		dmginfo:SetAttacker( attacker )

		tr.Entity:TakeDamageInfo( dmginfo )
	end
end



function SWEP:AttackThing( anim )
   self.Owner:SetAnimation( PLAYER_ATTACK1 )
   
   
    
	if ( !SERVER ) then return end
   if !IsValid(self.Owner) then return end
	-- We need this because attack sequences won't work otherwise in multiplayer
	local vm = self.Owner:GetViewModel()
	vm:ResetSequence( vm:LookupSequence( "fists_idle_01" ) )

	--local anim = "fists_left" --self.AttackAnims[ math.random( 1, #self.AttackAnims ) ]

	timer.Simple( 0, function()
		if ( !IsValid( self ) || !IsValid( self.Owner ) || !self.Owner:GetActiveWeapon() || self.Owner:GetActiveWeapon() != self ) then return end
	
		local vm = self.Owner:GetViewModel()
		vm:ResetSequence( vm:LookupSequence( anim ) )

		self:Idle()
	end )

	timer.Simple( 0.05, function()
		if ( !IsValid( self ) || !IsValid( self.Owner ) || !self.Owner:GetActiveWeapon() || self.Owner:GetActiveWeapon() != self ) then return end
		if ( anim == "fists_left" ) then
			self.Owner:ViewPunch( Angle( 0, 16, 0 ) )
		elseif ( anim == "fists_right" ) then
			self.Owner:ViewPunch( Angle( 0, -16, 0 ) )
		elseif ( anim == "fists_uppercut" ) then
			self.Owner:ViewPunch( Angle( 16, -8, 0 ) )
		end
	end )

	timer.Simple( 0.2, function()
		if ( !IsValid( self ) || !IsValid( self.Owner ) || !self.Owner:GetActiveWeapon() || self.Owner:GetActiveWeapon() != self ) then return end
		if ( anim == "fists_left" ) then
			self.Owner:ViewPunch( Angle( 4, -16, 0 ) )
		elseif ( anim == "fists_right" ) then
			self.Owner:ViewPunch( Angle( 4, 16, 0 ) )
		elseif ( anim == "fists_uppercut" ) then
			self.Owner:ViewPunch( Angle( -32, 0, 0 ) )
		end
		self.Owner:EmitSound( SwingSound )
		
	end )

	timer.Simple( 0.2, function()
		if ( !IsValid( self ) || !IsValid( self.Owner ) || !self.Owner:GetActiveWeapon() || self.Owner:GetActiveWeapon() != self ) then return end
		self:DealDamage( anim )
	end )

	self.Weapon:SetNextSecondaryFire(CurTime() + 0.6)
    self.Weapon:SetNextPrimaryFire(CurTime() + 0.6)



end

function SWEP:Idle()

	local vm = self.Owner:GetViewModel()
	timer.Create( "fists_idle" .. self:EntIndex(), vm:SequenceDuration(), 1, function()
		vm:ResetSequence( vm:LookupSequence( "fists_idle_0" .. math.random( 1, 2 ) ) )
	end )

end



function SWEP:OnRemove()

	if ( IsValid( self.Owner ) ) then
		local vm = self.Owner:GetViewModel()
        if IsValid(vm) then
		vm:SetMaterial( "" )
        end
	end

	timer.Stop( "fists_idle" .. self:EntIndex() )

end

function SWEP:Holster( wep )
    if ( IsValid( self.Owner ) ) then
        local vm = self.Owner:GetViewModel()
        if IsValid(vm) then
            vm:SetMaterial( "" )
        end
    end

	return true
end
SWEP.modelView = false

function SWEP:Deploy()

    local vm = self.Owner:GetViewModel()
    vm:ResetSequence( vm:LookupSequence( "fists_draw" ) )


    self:Idle()

    return true

end