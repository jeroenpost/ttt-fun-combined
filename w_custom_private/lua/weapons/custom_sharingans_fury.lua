

SWEP.PrintName				= "Sharingan's Fury"
SWEP.Slot				= 2
SWEP.SlotPos				= 3
SWEP.DrawAmmo				= true
SWEP.DrawWeaponInfoBox			= false
SWEP.BounceWeaponIcon   		= 	false
SWEP.DrawCrosshair			= true
SWEP.Weight				= 30
SWEP.AutoSwitchTo			= true
SWEP.AutoSwitchFrom			= true
SWEP.HoldType 				= "ar2"

SWEP.ViewModelFOV			= 70
SWEP.ViewModelFlip			= true
SWEP.ViewModel				= "models/weapons/v_rif_fury.mdl"
SWEP.WorldModel				= "models/weapons/w_frozen_acr.mdl"
SWEP.Base				= "gb_camo_base"
SWEP.Spawnable				= true
SWEP.AdminSpawnable			= true

SWEP.Kind = WEAPON_HEAVY
SWEP.Primary.SoundLevel  = 33
SWEP.Primary.Sound			= Sound( "Weapon_M4A1.Silenced")
SWEP.Primary.Delay			= 0.15
SWEP.Primary.ClipSize			= 100
SWEP.Primary.DefaultClip		= 150
SWEP.Primary.Automatic			= true
SWEP.Primary.Ammo			= "ar2"
SWEP.AmmoEnt = "item_ammo_smg1_ttt"
SWEP.Primary.ClipMax = 140
SWEP.Secondary.IronFOV			= 55


SWEP.Primary.NumShots	= 1
SWEP.Primary.Damage		= 20
SWEP.Primary.Cone		= .025
SWEP.Primary.IronAccuracy = .015


SWEP.IronSightsPos = Vector(2.668, 0, 0.675)
SWEP.IronSightsAng = Vector(0, 0, 0)

SWEP.Camo = 25
SWEP.CustomCamo = true

SWEP.HasFireBullet = true
SWEP.LaserColor = Color(255,0,0,255)
SWEP.nextreload = 0
function SWEP:PrimaryAttack()
    local mode = self:GetNWString("shootmode")

    if mode == "Mangekyo Sharingan" then
        self.Primary.Delay			= 0.05
        self:Flamethrower()
        self:MakeAFlame()
    end

    if mode == "Rinnegan" then
        self.Primary.Delay			= 1.25
        self.Primary.Damage			= 90
        self:MakeAFlame()
        self.Primary.Cone		= .0001
        self.BaseClass.PrimaryAttack(self)
        if self:Clip1() > 0 then
            self.LaserColor = Color(255,0,0,255)
            self:ShootTracer( "LaserTracer_thick" )
        end
    end

    if mode == "Fury" then
        self:ShootBullet2()
        self:Disorientate()
        self:MakeAFlame()
        self.Weapon:EmitSound(Sound(self.Primary.Sound))
        --self:TakePrimaryAmmo(self.Primary.TakeAmmo)
        self.Weapon:SetNextPrimaryFire( CurTime() + 0.12 )
        self:SetNextPrimaryFire(CurTime()+0.15)
    end

    if mode == "Wood Release: Wood Dragon Jutsu" then
        local traceres=util.QuickTrace(self.Owner:EyePos(),self.Owner:GetAimVector()*9999,self.Owner)
        self:ShootEffect( "vortigaunt_beam",self.Owner:EyePos(),traceres.HitPos)	//shoop da whoop
        self.Weapon:EmitSound("npc/vort/vort_foot"..math.random(1,4)..".wav")
        local bullet = {}
        bullet.Num 		= 1
        bullet.Src 		= self.Owner:GetShootPos()			// Source
        bullet.Dir 		= self.Owner:GetAimVector()			// Dir of bullet
        bullet.Spread 	= Vector(0.01 * 0.1 , 0.2  * 0.1, 0)			// Aim Cone
        bullet.Tracer	= 0						// Show a tracer on every x bullets
        bullet.TracerName = "manatrace"
        bullet.Force	= 1					// Amount of force to give to phys objects
        bullet.Damage	= 23
        bullet.damage	= 23
        self:SetNextPrimaryFire(CurTime()+0.20)

        self.Owner:FireBullets(bullet)
    end

    if mode == "Fire Release: Great Fireball Jutsu" and SERVER then
        if self.nextreload > CurTime() then
            if self.nextreload - CurTime() < 50 then
                self.Owner:PrintMessage( HUD_PRINTCENTER,  math.floor(self.nextreload - CurTime()).." seconds left" )
            end
            return
        end
        self.nextreload = CurTime() + 50

        local tr = self.Owner:GetEyeTrace();
        local ent = ents.Create( "sharingans_fireball" )

        ent:SetPos( tr.HitPos  )
        ent:SetOwner( self.Owner  )
        ent:SetPhysicsAttacker(  self.Owner )
        ent.Owner = self.Owner
        ent:Spawn()
        ent.timer = CurTime() + 0.01
        ent.EndColor = "0 0 0"
        ent.StartColor = "255 102 0"
        timer.Simple(7,function()
            if IsValid(ent) then
                ent.Bastardgas:Remove()
                ent.Entity:Remove()
            end
        end)

    end

end
SWEP.CanDecapitate= true
SWEP.tracerColor =Color(255,0,0,255)
SWEP.Primary.Spread = 0.001
function SWEP:ShootBullet2(damage, recoil, num_bullets, aimcone)

    num_bullets 		= 1

    local bullet = {}
    bullet.Num 		= 5
    bullet.Src 		= self.Owner:GetShootPos()			// Source
    bullet.Dir 		= self.Owner:GetAimVector()			// Dir of bullet
    bullet.Spread 	= Vector(0.2 * 0.1 , 0.2  * 0.1, 0)			// Aim Cone
    bullet.Tracer	= 4						// Show a tracer on every x bullets
    bullet.TracerName = "manatrace"
    bullet.Force	= 10					// Amount of force to give to phys objects
    bullet.Damage	= 3
    bullet.damage	= 3


    self.Owner:FireBullets(bullet)


end

function SWEP:Deploy()
    self.Weapon:SendWeaponAnim(ACT_VM_DRAW_SILENCED)
    self:SetNWString("shootmode","Fury")
    return self.BaseClass.Deploy(self)
end

function SWEP:OnDrop()
    self:Remove()
end

SWEP.NextReload = 0
function SWEP:Reload()

    if self.Owner:KeyDown(IN_USE) and self.NextReload < CurTime() then
        self.NextReload = CurTime() + 0.3
        local mode = self:GetNWString("shootmode")
        self:SetIronsights(false)
      --  self:SetZoom(false)
        if mode == "Mangekyo Sharingan" then  self:SetNWString("shootmode","Rinnegan") self.Primary.Sound = "weapons/shotgun/shotgun_dbl_fire.wav" end
        if mode == "Rinnegan" then  self:SetNWString("shootmode","Fury") self.Primary.Sound = "weapons/crossbow/fire1.wav" end
        if mode == "Fury" then  self:SetNWString("shootmode","Wood Release: Wood Dragon Jutsu") self.Primary.Sound = "weapons/crossbow/fire1.wav" end
        if mode == "Wood Release: Wood Dragon Jutsu" then self:SetNWString("shootmode","Fire Release: Great Fireball Jutsu") self.Primary.Sound = "weapons/crossbow/fire1.wav" end
        if mode == "Fire Release: Great Fireball Jutsu" then self:SetNWString("shootmode","Mangekyo Sharingan") self.Primary.Sound = "weapons/crossbow/fire1.wav" end

        self.Weapon:SetNextPrimaryFire( CurTime() + 0.8 )
        return
    end

end

    function SWEP:ShootEffect(EFFECTSTR,startpos,endpos)
        local pPlayer=self.Owner;
        if !pPlayer then return end
        local view;
        if CLIENT then view=GetViewEntity() else view=pPlayer:GetViewEntity() end
      --  if ( !pPlayer:IsNPC() && view:IsPlayer() ) then
       -- util.ParticleTracerEx( EFFECTSTR, self.Weapon:GetAttachment( self.Weapon:LookupAttachment( "muzzle" ) ).Pos,endpos, true, pPlayer:GetViewModel():EntIndex(), pPlayer:GetViewModel():LookupAttachment( "muzzle" ) );
      --  else
            util.ParticleTracerEx( EFFECTSTR, pPlayer:GetAttachment( pPlayer:LookupAttachment( "anim_attachment_rh" ) ).Pos,endpos, true,pPlayer:EntIndex(), pPlayer:LookupAttachment( "anim_attachment_rh" ) );
       -- end


      -- local rand=math.random(1,1.5);
      -- self:CreateBlast(rand,endpos)
     --   self:CreateBlast(rand,endpos)
    end

    function SWEP:CreateBlast(scale,pos)
        if CLIENT then return end
        local blastspr = ents.Create("env_sprite");			//took me hours to understand how this damn
        blastspr:SetPos( pos );								//entity works
        blastspr:SetKeyValue( "model", "sprites/vortring1.vmt")//the damn vortigaunt beam ring
        blastspr:SetKeyValue( "scale",tostring(scale))
        blastspr:SetKeyValue( "framerate",30)
        blastspr:SetKeyValue( "spawnflags","1")
        blastspr:SetKeyValue( "brightness","255")
        blastspr:SetKeyValue( "angles","0 0 0")
        blastspr:SetKeyValue( "rendermode","9")
        blastspr:SetKeyValue( "renderamt","255")
        blastspr:SetKeyValue( "color","255 0 0")
        blastspr:Spawn()
        blastspr:Fire("kill","",0.45)							//remove it after 0.45 seconds
        end

        function SWEP:DispatchEffect(EFFECTSTR)
        local pPlayer=self.Owner;
        if !pPlayer then return end
        local view;
        if CLIENT then view=GetViewEntity() else view=pPlayer:GetViewEntity() end
        if ( !pPlayer:IsNPC() && view:IsPlayer() ) then
        ParticleEffectAttach( EFFECTSTR, PATTACH_POINT_FOLLOW, pPlayer:GetViewModel(), pPlayer:GetViewModel():LookupAttachment( "muzzle" ) );
        else
            ParticleEffectAttach( EFFECTSTR, PATTACH_POINT_FOLLOW, pPlayer, pPlayer:LookupAttachment( "anim_attachment_rh" ) );
        end
    end

if CLIENT then
    function SWEP:DrawHUD( )
        local shotttext = "Shootmode: "..self:GetNWString("shootmode").."\nE+R to change"
        surface.SetFont( "ChatFont" );
        local size_x, size_y = surface.GetTextSize( shotttext );

        draw.RoundedBox( 6, ScrW( ) / 2 - size_x / 2 - 5, ScrH( ) - 100, size_x + 15, size_y + 10, Color( 0, 0, 0, 150 ) );
        draw.DrawText( shotttext , "ChatFont", ScrW( ) / 2, ScrH( ) - 100 + 5, Color( 255, 30, 30, 255 ), TEXT_ALIGN_CENTER );
end end



local sndPowerUp		= Sound("rope_hit.mp3")
local sndPowerDown		= Sound ("shoot_rope.mp3")
local sndTooFar			= Sound ("to_far.mp3")

function SWEP:Initialize()

    nextshottime = CurTime()

    self.BaseClass.Initialize(self)
end
SWEP.HoldType = "smg"

SWEP.NextAmmoGive = 0
SWEP.NextAmmoTake = 0
SWEP.NextDisable = 0
SWEP.NextFire2 = 0
function SWEP:Think()

    if self.NextAmmoGive < CurTime() && self:Clip1() < 10 then
    self.NextAmmoGive = CurTime() + 3
    self:SetClip1( self:Clip1() + 1 )
    end

    if (!self.Owner || self.Owner == NULL) or self:Clip1() < 1 then

    if ( !self.Beam ) then return end
    if( IsValid( self.Beam) ) then
        self.Beam:Remove()
    end
    self.Beam = nil
    return
    end



    if self.Owner:KeyDown(IN_USE) and self.Owner:KeyDown(IN_ATTACK2) then

        return
    end

    if self.Owner:KeyDown(IN_USE) and  self.Owner:KeyDown(IN_DUCK) and self.NextDisable < CurTime() then
        self.NextDisable = CurTime() + 0.3
        if self.Owner.magicalsoundeneabled then
            self.Owner.magicalsoundeneabled = false
            self.Owner:PrintMessage( HUD_PRINTCENTER, "Disabled Sound on deploy" )
        else
            self.Owner.magicalsoundeneabled = true
            self.Owner:PrintMessage( HUD_PRINTCENTER, "Enabled Sound on deploy" )
        end
        return
    end



    if ( self.Owner:KeyPressed( IN_ATTACK2 ) ) then

        self:StartAttack()

    elseif ( self.Owner:KeyDown( IN_ATTACK2 )  ) then

        self:UpdateAttack()
        self.Weapon:SetNextSecondaryFire( CurTime() + 1 )

    elseif ( self.Owner:KeyReleased( IN_ATTACK2 )  ) then

        self:EndAttack( true )

    end


    self.BaseClass.Think(self)

end

function SWEP:DoTrace( endpos )
    local trace = {}
    trace.start = self.Owner:GetShootPos()
    trace.endpos = trace.start + (self.Owner:GetAimVector() * 14096) --14096 is length modifier.
    if(endpos) then trace.endpos = (endpos - self.Tr.HitNormal * 7) end
    trace.filter = { self.Owner, self.Weapon }

    self.Tr = nil
    self.Tr = util.TraceLine( trace )
end


function SWEP:StartAttack()


    if not self:CanPrimaryAttack() then return end

    --Get begining and end poins of trace.
    local gunPos = self.Owner:GetShootPos() --Start of distance trace.
    local disTrace = self.Owner:GetEyeTrace() --Store all results of a trace in disTrace.
    local hitPos = disTrace.HitPos --Stores Hit Position of disTrace.

    --Calculate Distance
    --Thanks to rgovostes for this code.
    local x = (gunPos.x - hitPos.x)^2;
    local y = (gunPos.y - hitPos.y)^2;
    local z = (gunPos.z - hitPos.z)^2;
    local distance = math.sqrt(x + y + z);

    --Only latches if distance is less than distance CVAR
    local distanceCvar = GetConVarNumber("grapple_distance")
    inRange = false
    if distance <= distanceCvar then
        inRange = true
    end

    if inRange then
        if (SERVER) then

            if (!self.Beam) then --If the beam does not exist, draw the beam.
            if not self:CanPrimaryAttack() then return end
            --grapple_beam
            self.Beam = ents.Create( "trace1" )
            self.Beam:SetPos( self.Owner:GetShootPos() )
            self.Beam:Spawn()
            self:TakePrimaryAmmo(1)

            end

            self.Beam:SetParent( self.Owner )
            self.Beam:SetOwner( self.Owner )

        end


        self:DoTrace()
        self.speed = 10000 --Rope latch speed. Was 3000.
        self.startTime = CurTime()
        self.endTime = CurTime() + self.speed
        self.dtfix = -1

        if (SERVER && self.Beam) then
        self.Beam:GetTable():SetEndPos( self.Tr.HitPos )
        end

        self:UpdateAttack()

        self.Weapon:EmitSound( sndPowerDown )
    else
        --Play A Sound
        self.Weapon:EmitSound( sndTooFar )
    end
end

function SWEP:UpdateAttack()

    -- if not self:CanPrimaryAttack() then return end
    if !self.Tr or self:Clip1() < 1 then return end
    self.Owner:LagCompensation( true )


    if (!endpos) then endpos = self.Tr.HitPos end

    if (SERVER && self.Beam) then
    self.Beam:GetTable():SetEndPos( endpos )
    end

    lastpos = endpos



    if ( self.Tr.Entity:IsValid() ) then

        endpos = self.Tr.Entity:GetPos()
        if ( SERVER and IsValid(self.Beam) ) then
            self.Beam:GetTable():SetEndPos( endpos )
        end

    end

    local vVel = (endpos - self.Owner:GetPos())
    local Distance = endpos:Distance(self.Owner:GetPos())

    local et = (self.startTime + (Distance/self.speed))
    if(self.dtfix != 0) then
    self.dtfix = (et - CurTime()) / (et - self.startTime)
    end
    if(self.dtfix < 0) then
        self.Weapon:EmitSound( sndPowerUp )
        self.dtfix = 0
    end

    if(self.dtfix == 0) then
        zVel = self.Owner:GetVelocity().z
        vVel = vVel:GetNormalized()*(math.Clamp(Distance,0,25))
        if( SERVER ) then
            if  self.NextAmmoTake < CurTime() then
                self:TakePrimaryAmmo(1)
                self.NextAmmoTake = CurTime() + 1
                self.NextAmmoGive = CurTime() + 1.2
            end

            local gravity = GetConVarNumber("sv_gravity")

            vVel:Add(Vector(0,0,(gravity/50)*1.5))

            if(zVel < 0) then
                vVel:Sub(Vector(0,0,zVel/100))
            end
            self.Owner:SetVelocity(vVel)
        end
    end

    endpos = nil

    self.Owner:LagCompensation( false )

end

function SWEP:EndAttack( shutdownsound )

    if ( shutdownsound ) then
        self.Weapon:EmitSound( sndPowerDown )
    end

    if ( CLIENT ) then return end
    if ( !self.Beam ) then return end
    if( IsValid( self.Beam) ) then
        self.Beam:Remove()
    end
    self.Beam = nil

end

function SWEP:SecondaryAttack()
end

function SWEP:Holster()
    self:EndAttack( false )
    gb_StopSoundFromServer("magical_magic")
    return true
end

function SWEP:OnRemove()
    self:EndAttack( false )
    gb_StopSoundFromServer("magical_magic")
    return true
end


function SWEP:Flamethrower()
    if ( !self:CanPrimaryAttack() ) then return end



    if ( self.LoopSound ) then
        self.LoopSound:ChangeVolume( 2, 0.1 )
    else
        self.LoopSound = CreateSound( self.Owner, Sound( "weapons/gb_flamethrower/flame_start.mp3" ) )
        if ( self.LoopSound ) then self.LoopSound:Play() end
    end
    if ( self.BeatSound ) then self.BeatSound:ChangeVolume( 0, 0.1 ) end




    local bullet = {}
    bullet.Num = 1
    bullet.Src = self.Owner:GetShootPos()
    bullet.Dir = self.Owner:GetAimVector()
    bullet.Owner = self.Owner
    bullet.Spread = Vector(0.004, 0.0004, 0 )
    bullet.Tracer = 1
    bullet.Force = 5
    bullet.Damage = 8
    --bullet.AmmoType = "Ar2AltFire" -- For some extremely stupid reason this breaks the tracer effect
    bullet.TracerName = "gb_flames"





    --self:SendWeaponAnim( ACT_VM_PRIMARYATTACK )
    --self.Owner:SetAnimation( PLAYER_ATTACK1 )

    if ( self.Owner:IsValid()  ) then


        local tr = self.Owner:GetEyeTrace()
        local effectdata = EffectData()
        effectdata:SetOrigin(tr.HitPos)
        effectdata:SetNormal(tr.HitNormal)
        effectdata:SetScale(0.5)
        -- util.Effect("effect_mad_ignition", effectdata)
        util.Decal("FadingScorch", tr.HitPos + tr.HitNormal, tr.HitPos - tr.HitNormal)

        local tracedata = {}
        tracedata.start = tr.HitPos
        tracedata.endpos = Vector(tr.HitPos.x, tr.HitPos.y, tr.HitPos.z - 10)
        tracedata.filter = tr.HitPos
        local tracedata2 = util.TraceLine(tracedata)



        if tracedata2.HitWorld && self.Owner:GetPos():Distance( tracedata.endpos ) < 500 then
        if SERVER then
            local flame = ents.Create("env_fire");
            flame:SetPos(tr.HitPos + Vector( 0, 0, 1 ));
            flame:SetKeyValue("firesize", "10");
            flame:SetKeyValue("fireattack", "10");
            flame:SetKeyValue("StartDisabled", "0");
            flame:SetKeyValue("health", "1");
            flame:SetKeyValue("firetype", "0");
            flame:SetKeyValue("damagescale", "1");
            flame:SetKeyValue("spawnflags", 128 + 2);
            flame:SetPhysicsAttacker(self.Owner)
            flame:SetOwner( self.Owner )
            flame:Spawn();
            flame.DieTime = 0.1
            flame:Fire("StartFire",0);

        end


        elseif self.Owner:GetPos():Distance( tracedata.endpos ) > 450 then
            bullet.Damage = 0
        end

        self.Owner:FireBullets( bullet )


        local owner = self.Owner
        --if not ValidEntity(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

        owner:ViewPunch( Angle( math.Rand(-0.01,-0.05) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )
    end


    self:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
    self:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
end

        -- Laser zap lightning grenade
        local ENT = {}

        ENT.Type = "anim"
        ENT.Base = "cse_ent_shortgasgrenade"

        function ENT:Poison(ply,dmg,owner)
            if (ply:Health() >= 0) then
                ply:TakeDamage(dmg or 5, owner or self)
            elseif (ply:Alive()) and (ply:Health() <= 0)then
                ply:Kill()
            else
            end
        end

        function ENT:Think()
            if not SERVER then return end
            if not self.timer then self.timer = CurTime() + 5 end
            if self.timer < CurTime() then
                if !IsValid(self.Bastardgas) && !self.Spammed then
                self.Spammed = true
                self.Bastardgas = ents.Create("env_smoketrail")
                self.Bastardgas:SetPos(self.Entity:GetPos())
                self.Bastardgas:SetKeyValue("spawnradius","25")
                self.Bastardgas:SetKeyValue("minspeed","0.5")
                self.Bastardgas:SetKeyValue("maxspeed","2")
                self.Bastardgas:SetKeyValue("startsize","16536")
                self.Bastardgas:SetKeyValue("endsize","256")
                self.Bastardgas:SetKeyValue("endcolor",self.EndColor)
                self.Bastardgas:SetKeyValue("startcolor",self.StartColor)
                self.Bastardgas:SetKeyValue("opacity","10")
                self.Bastardgas:SetKeyValue("spawnrate","15")
                self.Bastardgas:SetKeyValue("lifetime","10")
                self.Bastardgas:SetParent(self.Entity)
                self.Bastardgas:Spawn()
                self.Bastardgas:Activate()
                self.Bastardgas:Fire("turnon","", 0.1)
                local exp = ents.Create("env_explosion")
                exp:SetKeyValue("spawnflags",461)
                exp:SetPos(self.Entity:GetPos())
                exp:Spawn()
                exp:Fire("explode","",0)
                self.Entity:EmitSound(Sound("BaseSmokeEffect.Sound"))
                end

                local pos = self.Entity:GetPos()
                local maxrange = 256
                local maxstun = 10
                for k,v in pairs(player.GetAll()) do
                    if v.GasProtection then continue end
                    local plpos = v:GetPos()
                    local dist = -pos:Distance(plpos)+maxrange
                    if (pos:Distance(plpos)<=maxrange) and not v:IsGhost() then
                        local trace = {}
                        trace.start = self.Entity:GetPos()
                        trace.endpos = v:GetPos()+Vector(0,0,24)
                        trace.filter = { v, self.Entity }
                        trace.mask = COLLISION_GROUP_PLAYER
                        tr = util.TraceLine(trace)
                        if (tr.Fraction==1) then
                            local stunamount = math.ceil(dist/(maxrange/maxstun))
                            v:ViewPunch( Angle( stunamount*((math.random()*6)-2), stunamount*((math.random()*6)-2), stunamount*((math.random()*4)-1) ) )
                            self:Poison(v, stunamount, self.Owner, self.Owner:GetWeapon("weapon_gasgrenade"))
                            v:Ignite(0.1)
                            local edata = EffectData()
                            self:EmitSound("ambient/energy/zap1.wav")
                            edata:SetEntity(v)
                            edata:SetMagnitude(50)
                            edata:SetScale(256)

                            util.Effect("TeslaHitBoxes", edata)
                            util.Effect("ManhackSparks", edata)
                        end
                    end
                    end
                    if (self.timer+60<CurTime()) then
                        if IsValid(self.Bastardgas) then
                            self.Bastardgas:Remove()
                        end
                    end
                    if (self.timer+65<CurTime()) then
                        self.Entity:Remove()
                    end
                    self.Entity:NextThink(CurTime()+0.5)
                    return true
                end
            end


            scripted_ents.Register( ENT, "sharingans_fireball", true )