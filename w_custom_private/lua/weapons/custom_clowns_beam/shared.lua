if SERVER then

   AddCSLuaFile(  )
   --resource.AddFile("vgui/ttt_fun_killicons/m4a1.png")
end
 

SWEP.HoldType			= "ar2"
  SWEP.PrintName			= "Sollux's Fury"
   SWEP.Slot				= 6

if CLIENT then

 

   SWEP.Icon = "vgui/ttt_fun_killicons/m4a1.png"
end


SWEP.Base				= "aa_base_fw"
SWEP.Spawnable = true
SWEP.AdminSpawnable = true

SWEP.Kind = WEAPON_EQUIP1

SWEP.Primary.Delay			= 0.15
SWEP.Primary.Recoil			= 0.5
SWEP.Primary.Automatic = true
SWEP.Primary.Ammo = "smg1"
SWEP.Primary.Damage = 22
SWEP.Primary.Cone = 0.018
SWEP.Primary.ClipSize = 150
SWEP.Primary.ClipMax = 150
SWEP.Primary.DefaultClip = 150
SWEP.AutoSpawnable      = false
SWEP.AmmoEnt = "item_ammo_smg1_ttt"
SWEP.ViewModel			= "models/weapons/v_rif_m4a1.mdl"
SWEP.WorldModel			= "models/weapons/w_rif_m4a1.mdl"

SWEP.Primary.Sound = Sound( "Weapon_M4A1.Silenced" )

SWEP.IronSightsPos 		= Vector( 6, 0, 0.95 )
SWEP.IronSightsAng 		= Vector( 2.6, 1.37, 3.5 )

--- TTT config values


-- If AutoSpawnable is true and SWEP.Kind is not WEAPON_EQUIP1/2, then this gun can
-- be spawned as a random weapon. Of course this AK is special equipment so it won't,
-- but for the sake of example this is explicitly set to false anyway.
SWEP.AutoSpawnable = false

-- The AmmoEnt is the ammo entity that can be picked up when carrying this gun.
SWEP.AmmoEnt = "item_ammo_smg1_ttt"

-- InLoadoutFor is a table of ROLE_* entries that specifies which roles should
-- receive this weapon as soon as the round starts. In this case, none.
SWEP.InLoadoutFor = nil

-- If LimitedStock is true, you can only buy one per round.
SWEP.LimitedStock = true

-- If AllowDrop is false, players can't manually drop the gun with Q
SWEP.AllowDrop = true

-- If IsSilent is true, victims will not scream upon death.
SWEP.IsSilent = false

-- If NoSights is true, the weapon won't have ironsights
SWEP.NoSights = false

function SWEP:Equip()
    timer.Create("giveAmmoWaterBottleGreen",2,0,function()
        if IsValid(self) then
            self:SetClip1( self:Clip1() + 1 )
        end
    end)
end

function SWEP:SetZoom(state)
   if CLIENT or not IsValid(self.Owner) then return end
   if state  then
      self.Owner:SetFOV(35, 0.5)
   else
      self.Owner:SetFOV(0, 0.2)
   end
end

-- Add some zoom to ironsights for this gun
function SWEP:SecondaryAttack()
   if not self.IronSightsPos then return end
   if self.Weapon:GetNextSecondaryFire() > CurTime() then return end

   bIronsights = not self:GetIronsights()

   self:SetIronsights( bIronsights )

   if SERVER then
      self:SetZoom(bIronsights)
   end

   self.Weapon:SetNextSecondaryFire(CurTime() + 0.3)
end

function SWEP:PreDrop()
   self:SetZoom(false)
   self:SetIronsights(false)
   return self.BaseClass.PreDrop(self)
end

SWEP.NextReload = 0
function SWEP:Reload()
    if self.Owner:KeyDown(IN_USE) then
        self:FireWorkie()
        return
    end
    if self:Clip1() < 1 then
        self.Weapon:DefaultReload( ACT_VM_RELOAD );
        self:SetIronsights( false )
        self:SetZoom(false)
        return
    end
   if self.NextReload < CurTime() then
        self.NextReload = CurTime() + 10
        self:EmitSound(Sound("scotty_laugh.mp3"))
   end
end


function SWEP:Holster()
   self:SetIronsights(false)
   self:SetZoom(false)
   return true
end


SWEP.Primary.Tracer = 1;
SWEP.Primary.Force = 160;
SWEP.Primary.ExplosionShot = false;
SWEP.Primary.BulletShot = true;
SWEP.Primary.ModelShot = false;
SWEP.Primary.BreakAll = false;
SWEP.Primary.Ignite = false;
SWEP.Primary.Zap = true;
SWEP.Primary.ZapRadius = 150;
SWEP.Primary.ZapDamage = 0;
SWEP.Primary.Spread = 0.003;
function SWEP:PrimaryAttack()
    if ( !self:CanPrimaryAttack() ) then return end
    --self.Owner:EmitSound( "wunderwaffe.mp3" )
    self:SetNextPrimaryFire( CurTime( ) + 0.5 )

    --Default stuff, normally used in any condition.
    local tr = self.Owner:GetEyeTrace();
    if SERVER then self.Owner:EmitSound ( self.Primary.Sound ); end
    self:ShootEffects( "ToolTracer" );
    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay );
    self.Weapon:SetNextSecondaryFire( CurTime() + self.Primary.Delay );   self:TakePrimaryAmmo(1);
    local rnda = -self.Primary.Recoil;
    local rndb = self.Primary.Recoil * math.random(-1, 1);
    self.Owner:ViewPunch( Angle( rnda,rndb,rnda ) );

    --The attack mode, can vary from the user settings
    if ( self.Primary.ModelShot ) then
        if (!SERVER) then return end;
        local ent = ents.Create ("prop_physics");
        ent:SetModel ( self.Primary.Model );
        if ( self.Primary.FireFromHip ) then
            ent:SetPos (self.Owner:EyePos() + (self.Owner:GetAimVector() * 32));
        else
            ent:SetPos (tr.HitPos + self.Owner:GetAimVector() * -16);
        end
        ent:SetAngles (self.Owner:EyeAngles());
        ent:Spawn();
        local phys = ent:GetPhysicsObject();
        local shot_length = tr.HitPos:Length();
        if ( self.Primary.Weld ) then
            local weld = constraint.Weld( tr.Entity, ent, tr.PhysicsBone, 0, 0 );
        end
        phys:ApplyForceCenter (self.Owner:GetAimVector():GetNormalized() * math.pow (shot_length, 3));
        cleanup.Add (self.Owner, "props", ent);
        undo.Create ("Thrown model");
        undo.AddEntity (ent);
        undo.SetPlayer (self.Owner);
        undo.Finish();
    end    if ( self.Primary.BulletShot ) then
        local bullet = {};
        bullet.Num = self.Primary.NumberofShots;
        bullet.Src = self.Owner:GetShootPos();
        bullet.Dir = self.Owner:GetAimVector();
        bullet.Spread = Vector( self.Primary.Spread * 0.1 , self.Primary.Spread * 0.1, 0);
        bullet.Tracer = self.Primary.Tracer;
        bullet.TracerName = "ToolTracer"
        bullet.Force = self.Primary.Force;
        bullet.Damage = self.Primary.Damage;
        bullet.AmmoType = self.Primary.Ammo;
        self.Owner:FireBullets( bullet );
    end
    if ( self.Primary.BreakAll ) then
        if( tr.Entity:IsValid() and tr.Entity:GetClass() == "prop_physics" ) then
            constraint.RemoveAll( tr.Entity );
            local physobject = tr.Entity:GetPhysicsObject();
            physobject:EnableMotion( true );
            physobject:SetVelocity( self.Owner:GetAimVector() * 2 );
        end
    end
    if ( self.Primary.Zap ) and SERVER then
        local vaporize = ents.Create ( "point_hurt" )
        vaporize:SetPhysicsAttacker( self.Owner )
        vaporize:SetKeyValue ( "DamageRadius", self.Primary.ZapRadius )
        vaporize:SetKeyValue ( "Damage" , 0 )
        vaporize:SetKeyValue ( "DamageDelay", 0 )
        vaporize:SetKeyValue ( "DamageType" ,"67108864" )
        vaporize:SetPos ( tr.HitPos )
        vaporize:Fire ( "TurnOn" , "", 0 )
        vaporize:Fire ( "TurnOff" , "", 0.5 )



        local effectdata = EffectData()
        effectdata:SetOrigin(self.Owner:GetShootPos())
        effectdata:SetEntity(self.Weapon)
        effectdata:SetStart(self.Owner:GetShootPos())
        effectdata:SetNormal(self.Owner:GetAimVector())
        effectdata:SetAttachment(1)
        util.Effect("effect_zap", effectdata)

        if ((game.SinglePlayer() and SERVER) or CLIENT) then
            timer.Simple(0, function()
                if (!IsValid(self.Owner)) then return end
                if (not IsFirstTimePredicted() or not self.Owner:Alive())then return end

                util.Effect("effect_zap", effectdata)

            end)
        end

    end
end