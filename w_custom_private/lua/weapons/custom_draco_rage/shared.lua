if( SERVER ) then
	AddCSLuaFile(  )
	--resource.AddFile("materials/vgui/ttt/fists.png")
end


	SWEP.PrintName = "Drake's Rage"
	SWEP.DrawAmmo = false
	SWEP.DrawCrosshair = false
	
	


SWEP.Base = "gb_base"
SWEP.Kind = WEAPON_UNARMED

SWEP.CanBuy = nil -- no longer a buyable thing
--SWEP.LimitedStock = true

SWEP.AllowDrop = false
SWEP.Icon = "vgui/ttt_fun_killicons/fists.png"

SWEP.Author			= "robotboy655"
SWEP.Purpose		= "Well we sure as heck didn't use guns! We would wrestle Hunters to the ground with our bare hands! I would get ten, twenty a day, just using my fists."

SWEP.Spawnable			= true
SWEP.UseHands			= true

SWEP.ViewModel			= "models/weapons/c_arms_cstrike.mdl"

SWEP.WorldModel			= ""
SWEP.DrawWorldModel = false
--SWEP.ViewModel      = "models/weapons/v_punch.mdl"
--SWEP.WorldModel   = "models/weapons/w_fists_t.mdl"

SWEP.ViewModelFOV		= 46

SWEP.Primary.ClipSize		= -1
SWEP.Primary.DefaultClip	= -1
SWEP.Primary.Damage			= 35
SWEP.Primary.Automatic		= true
SWEP.Primary.Ammo			= "none"

SWEP.Secondary.ClipSize		= 1
SWEP.Secondary.DefaultClip	= 1
SWEP.Secondary.Damage			= 35
SWEP.Secondary.Automatic	= true
SWEP.Secondary.Ammo			= "Battery"

SWEP.Weight				= 5
SWEP.AutoSwitchTo		= false
SWEP.AutoSwitchFrom		= false 

SWEP.PrintName			= "Raging Russian"
SWEP.Slot				= 5
SWEP.SlotPos			= 5
SWEP.DrawAmmo			= false
SWEP.DrawCrosshair		= true
SWEP.secondUsed                 = false


local SwingSound = Sound( "weapons/slam/throw.wav" )
local HitSound = Sound( "Flesh.ImpactHard" )

function SWEP:Initialize()

	self:SetHoldType( "fist" )

end

function SWEP:Think()
   
end

function SWEP:PreDrawViewModel(viewModel, weapon, client)
 
end

SWEP.Distance = 67
SWEP.AttackAnims = { "fists_left", "fists_right", "fists_uppercut" }
SWEP.AttackAnimsRight = { "fists_right", "fists_uppercut" }

SWEP.OnFire = false

function SWEP:PrimaryAttack()
    if not SERVER then return end
    if self.Owner.OnFire then
        self.Owner.OnFire = false
        self.Owner:Extinguish()

        timer.Destroy("dracohealthrage_andsuch"..self.Owner:SteamID())
    else
        self.Owner.OnFire = true

        self.Owner:Ignite(120,0)
        timer.Create("dracohealthrage_andsuch"..self.Owner:SteamID(),0.2,0,function()
            if IsValid(self.Owner) and self.Owner:Health() < 175 then
                self.Owner:SetHealth(self.Owner:Health()+2);
                self.Owner.hasProtectionSuit = true;
            end
        end)

    end
 self:AttackThing( self.AttackAnimsRight[ math.random( 1, #self.AttackAnimsRight ) ] )
end

function SWEP:SecondaryAttack()
    if not self.secondUsed then
         self:AttackThing( "fists_left" )
    else
        self:AttackThing( "fists_right" )
    end
    self:Fly()
end

SWEP.flyammo = 5
SWEP.nextFly = 0
function SWEP:Fly()
    if self.nextFly > CurTime() then return end
    self.nextFly = CurTime() + 0.30
    if  self.flyammo < 1 then return end

    if SERVER then self.Owner:EmitSound(Sound("weapons/ls/lightsaber_swing.mp3")) end
    if SERVER then self.Owner:EmitSound(Sound("player/suit_sprint.wav")) end

    self.flyammo = self.flyammo - 1
    self.Owner:SetAnimation( PLAYER_ATTACK1 )

    if SERVER then self.Owner:SetVelocity(self.Owner:GetForward() * 400 + Vector(0,0,400)) end
    timer.Simple(20,function() if IsValid(self) then self.flyammo = self.flyammo + 1 end end)
    --timer.Simple(0.2,self.SecondaryAttackDelay,self)
end


function SWEP:Reload()
end

function SWEP:DealDamage( anim )
	local tr = util.TraceLine( {
		start = self.Owner:GetShootPos(),
		endpos = self.Owner:GetShootPos() + self.Owner:GetAimVector() * self.Distance,
		filter = self.Owner
	} )

	if ( not IsValid( tr.Entity ) ) then 
		tr = util.TraceHull( {
			start = self.Owner:GetShootPos(),
			endpos = self.Owner:GetShootPos() + self.Owner:GetAimVector() * self.Distance,
			filter = self.Owner,
			mins = self.Owner:OBBMins() / 3,
			maxs = self.Owner:OBBMaxs() / 3
		} )
	end

	if ( tr.Hit ) then self.Owner:EmitSound( HitSound ) end

	if ( IsValid( tr.Entity ) && ( tr.Entity:IsNPC() || tr.Entity:IsPlayer() ) ) then
		local dmginfo = DamageInfo()
		dmginfo:SetDamage( self.Primary.Damage )
		if ( anim == "fists_left" ) then
			dmginfo:SetDamageForce( self.Owner:GetRight() * 49125 + self.Owner:GetForward() * 99984 ) -- Yes we need those specific numbers
                        dmginfo:SetDamage( self.Secondary.Damage )
                        

                        self.secondUsed = false
		elseif ( anim == "fists_right" ) then
			dmginfo:SetDamageForce( self.Owner:GetRight() * -49124 + self.Owner:GetForward() * 99899 )
                        dmginfo:SetDamage( self.Primary.Damage )
		elseif ( anim == "fists_uppercut" ) then
			dmginfo:SetDamageForce( self.Owner:GetUp() * 51589 + self.Owner:GetForward() * 100128 )
                        dmginfo:SetDamage( self.Primary.Damage )
		end
                tr.Entity:SetVelocity(self.Owner:GetForward() * 600 + Vector(0,0,400))
		dmginfo:SetInflictor( self )
		local attacker = self.Owner
		if ( !IsValid( attacker ) ) then attacker = self end
		dmginfo:SetAttacker( attacker )

		tr.Entity:TakeDamageInfo( dmginfo )
	end
end



function SWEP:AttackThing( anim )
   self.Owner:SetAnimation( PLAYER_ATTACK1 )
   
   
    
	if ( !SERVER ) then return end
   if !IsValid(self.Owner) then return end
	-- We need this because attack sequences won't work otherwise in multiplayer
	local vm = self.Owner:GetViewModel()
	vm:ResetSequence( vm:LookupSequence( "fists_idle_01" ) )

	--local anim = "fists_left" --self.AttackAnims[ math.random( 1, #self.AttackAnims ) ]

	timer.Simple( 0, function()
		if ( !IsValid( self ) || !IsValid( self.Owner ) || !self.Owner:GetActiveWeapon() || self.Owner:GetActiveWeapon() != self ) then return end
	
		local vm = self.Owner:GetViewModel()
		vm:ResetSequence( vm:LookupSequence( anim ) )

		self:Idle()
	end )

	timer.Simple( 0.05, function()
		if ( !IsValid( self ) || !IsValid( self.Owner ) || !self.Owner:GetActiveWeapon() || self.Owner:GetActiveWeapon() != self ) then return end
		if ( anim == "fists_left" ) then
			self.Owner:ViewPunch( Angle( 0, 16, 0 ) )
		elseif ( anim == "fists_right" ) then
			self.Owner:ViewPunch( Angle( 0, -16, 0 ) )
		elseif ( anim == "fists_uppercut" ) then
			self.Owner:ViewPunch( Angle( 16, -8, 0 ) )
		end
	end )

	timer.Simple( 0.2, function()
		if ( !IsValid( self ) || !IsValid( self.Owner ) || !self.Owner:GetActiveWeapon() || self.Owner:GetActiveWeapon() != self ) then return end
		if ( anim == "fists_left" ) then
			self.Owner:ViewPunch( Angle( 4, -16, 0 ) )
		elseif ( anim == "fists_right" ) then
			self.Owner:ViewPunch( Angle( 4, 16, 0 ) )
		elseif ( anim == "fists_uppercut" ) then
			self.Owner:ViewPunch( Angle( -32, 0, 0 ) )
		end
		self.Owner:EmitSound( SwingSound )
		
	end )

	timer.Simple( 0.2, function()
		if ( !IsValid( self ) || !IsValid( self.Owner ) || !self.Owner:GetActiveWeapon() || self.Owner:GetActiveWeapon() != self ) then return end
		self:DealDamage( anim )
	end )

	self.Weapon:SetNextSecondaryFire(CurTime() + 0.6)
    self.Weapon:SetNextPrimaryFire(CurTime() + 0.6)



end

function SWEP:Idle()

	local vm = self.Owner:GetViewModel()
	timer.Create( "fists_idle" .. self:EntIndex(), vm:SequenceDuration(), 1, function()
		vm:ResetSequence( vm:LookupSequence( "fists_idle_0" .. math.random( 1, 2 ) ) )
	end )

end



function SWEP:OnRemove()

	--if ( IsValid( self.Owner ) ) then
	--	local vm = self.Owner:GetViewModel()
		--vm:SetMaterial( "" )
	--end

	timer.Stop( "fists_idle" .. self:EntIndex() )
    if( IsValid(self.Owner)) then
    timer.Destroy("dracohealthrage_andsuch"..self.Owner:SteamID())
        end

end

function SWEP:Holster( wep )
if ( !IsValid(self.Owner) ) then
		return
end
timer.Destroy("dracohealthrage_andsuch"..self.Owner:SteamID())



	return true
end
SWEP.modelView = false

