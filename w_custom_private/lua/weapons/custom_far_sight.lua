if ( SERVER ) then

    SWEP.MaxAmmoMod = 5
    SWEP.GiveAmmoMod = 6
end

SWEP.PsID = "cm_g2contender2"
SWEP.BulletLength = 7.8
SWEP.CaseLength = 51.8

SWEP.MuzVel = 321.521

SWEP.Attachments = {
    [1] = {"kobra", "riflereflex", "eotech", "aimpoint", "elcan", "acog", "ballistic"},
    [2] = {"laser"}}

if ( CLIENT ) then

    SWEP.PrintName			= "Far Sight"
    SWEP.Author				= "Counter-Strike"
    SWEP.Slot				= 1
    SWEP.SlotPos = 0 //			= 1
    SWEP.IconLetter			= "f"
    SWEP.DifType = true
    SWEP.PitchMod = 0.25
    SWEP.RollMod = 0.5
    SWEP.DrawAngleMod = 0.5
    SWEP.Muzzle = "cstm_muzzle_br"
    SWEP.SparkEffect = "cstm_child_sparks_large"
    SWEP.SmokeEffect = "cstm_child_smoke_large"
    SWEP.ACOGDist = 6
    SWEP.AimpointDist = 11
    SWEP.BallisticDist = 3.5
    SWEP.SafeXMoveMod = 2
    SWEP.NoRail = true


    SWEP.VElements = {
        ["ballistic"] = { type = "Model", model = "models/bunneh/scope01.mdl", bone = "Front", rel = "", pos = Vector(-12.042, -2.0, 1.664), angle = Angle(0, 0, -90), size = Vector(1.5,1.5,1.5), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
 }

    SWEP.WElements = {
        ["ballistic"] = { type = "Model", model = "models/bunneh/scope01.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(-26.783, -6.801, -10.019), angle = Angle(4, 0, 180), size = Vector(5,5,5), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
     ["weapon"] = { type = "Model", model = "models/weapons/w_cstm_g2.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(0.879, 0.958, -0.166), angle = Angle(0, 90, 180), size = Vector(1, 1, 1), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
 }
end

SWEP.HoldType			= "ar2"
SWEP.Base				= "aa_base"
SWEP.Category = "Extra Pack (Sidearms)"
SWEP.FireModes = {"break"}
SWEP.AmmoTypes = {"hp", "ap", "magnum", "incendiary", "birdshot"}

SWEP.Spawnable			= true
SWEP.AdminSpawnable		= true

SWEP.HoldType = "pistol"
SWEP.ViewModelFOV = 95
SWEP.ViewModelFlip = false
SWEP.ViewModel = "models/weapons/v_cstm_g2.mdl"
SWEP.WorldModel = "models/weapons/w_pist_deagle.mdl"
SWEP.ShowViewModel = true
SWEP.ShowWorldModel = false
SWEP.ViewModelBonescales = {}

SWEP.Weight				= 5
SWEP.AutoSwitchTo		= false
SWEP.AutoSwitchFrom		= false

SWEP.Primary.Sound			= Sound("CSTM_G2CONTENDER")
SWEP.Primary.Recoil			= 0.45
SWEP.Primary.Damage			= 75
SWEP.Primary.NumShots		= 1
SWEP.Primary.Cone			= 0.0001
SWEP.Primary.ClipSize		= 50
SWEP.Primary.Delay			= 0.45
SWEP.Primary.DefaultClip	= 50
SWEP.Primary.Automatic		= true
SWEP.Primary.Ammo			= "7.62x51MM" --".30-30"
SWEP.InitialHoldtype = "pistol"
SWEP.InHoldtype = "pistol"
SWEP.NoBoltAnim = true
SWEP.SilencedSound = Sound("CSTM_SilencedShot2")
SWEP.SilencedVolume = 80
SWEP.UnAimAfterShot = true
SWEP.HeadshotMultiplier = 3.5

-- Animation speed/custom reload function related
SWEP.IsReloading = false
SWEP.AnimPrefix = ""
SWEP.ReloadSpeed = 1
SWEP.ShouldBolt = false
SWEP.ReloadDelay = 0
SWEP.IncAmmoPerc = 0.72 -- Amount of frames required to pass (in percentage) of the reload animation for the weapon to have it's amount of ammo increased
SWEP.CantChamber = true
SWEP.SkipIdle = true
SWEP.PlaybackRate = 1
SWEP.FOVZoom = 85

-- Dynamic accuracy related
SWEP.ShotsAmount 			= 0
SWEP.ConeDecAff				= 0
SWEP.DefRecoil				= 4.5
SWEP.CurCone				= 0.04
SWEP.DecreaseRecoilTime 	= 0
SWEP.ConeAff1 				= 0 -- Crouching/standing
SWEP.ConeAff2 				= 0 -- Using ironsights

SWEP.UnConeTime				= 0 -- Amount of time after firing the last shot that needs to pass until accuracy increases
SWEP.FinalCone				= 0 -- Self explanatory
SWEP.VelocitySensivity		= 1.6 -- Percentage of how much the cone increases depending on the player's velocity (moving speed). Rifles - 100%; SMGs - 80%; Pistols - 60%; Shotguns - 20%
SWEP.HeadbobMul 			= 1
SWEP.IsSilenced 			= false
SWEP.IronsightsCone 		= 0.0045
SWEP.HipCone 				= 0.051
SWEP.ConeInaccuracyAff1 = 0.5
SWEP.PlayFireAnim = true

SWEP.Secondary.Automatic	= false
SWEP.Secondary.Ammo			= "none"

SWEP.IsUsingIronsights 		= false
SWEP.TargetMul = 0
SWEP.SetAndForget			= false
SWEP.CantSilence = true

SWEP.IronSightsPos = Vector(6.0749, -5.5216, 2.3984)
SWEP.IronSightsAng = Vector(2.5174, -0.0099, 0)

SWEP.AimPos = Vector(-2.994, -1.448, 1.21)
SWEP.AimAng = Vector(0, 0, 0)

SWEP.KobraPos = Vector(-2.985, -3.961, 0.592)
SWEP.KobraAng = Vector(0, 0, 0)

SWEP.RReflexPos = Vector(-2.991, -3.961, 0.736)
SWEP.RReflexAng = Vector(0, 0, 0)

SWEP.ReflexPos = Vector(-3, -3.961, 0.216)
SWEP.ReflexAng = Vector(0, 0, 0)

SWEP.ScopePos = Vector(-2.995, -3.961, 0.44)
SWEP.ScopeAng = Vector(0, 0, 0)

SWEP.ACOGPos = Vector(-3, -3.961, 0.80)
SWEP.ACOGAng = Vector(0, 0, 0)

SWEP.ACOG_BackupPos = Vector(-3, -3.961, -0.20)
SWEP.ACOG_BackupAng = Vector(0, 0, 0)

SWEP.ELCANPos = Vector(-2.98, -3.961, 0.78)
SWEP.ELCANAng = Vector(0, 0, 0)

SWEP.ELCAN_BackupPos = Vector(-2.988, -3.961, -0.76)
SWEP.ELCAN_BackupAng = Vector(0, 0, 0)

SWEP.BallisticPos = Vector(-2.98, -4.488, 0.426)
SWEP.BallisticAng = Vector(0, 0, 0)

SWEP.NoFlipOriginsPos = Vector(-1.4, 0, -0.361)
SWEP.NoFlipOriginsAng = Vector(0, 0, 0)

SWEP.MeleePos = Vector(4.487, -1.497, -5.277)
SWEP.MeleeAng = Vector(25.629, -30.039, 26.18)

SWEP.SafePos = Vector(-0.82, 0.165, 0.2)
SWEP.SafeAng = Vector(-9.754, 24.673, 0)

if CLIENT then
    function SWEP:CanUseBackUp()
        if self.VElements["acog"].color.a == 255 or self.VElements["elcan"].color.a == 255 then
            return true
        end

        return false
    end

    function SWEP:UseBackUp()
        if self.VElements["acog"].color.a == 255 then
            if self.AimPos == self.ACOG_BackupPos then
                self.AimPos = self.ACOGPos
                self.AimAng = self.ACOGAng
            else
                self.AimPos = self.ACOG_BackupPos
                self.AimAng = self.ACOG_BackupAng
            end
        elseif self.VElements["elcan"].color.a == 255 then
            if self.AimPos == self.ELCAN_BackupPos then
                self.AimPos = self.ELCANPos
                self.AimAng = self.ELCANAng
            else
                self.AimPos = self.ELCAN_BackupPos
                self.AimAng = self.ELCAN_BackupAng
            end
        end
    end
end

function SWEP:Initialize()


    if CLIENT then

        self:SetHoldType( self.HoldType or "pistol" )

        self.VElements = table.FullCopy( self.VElements )
        self.WElements = table.FullCopy( self.WElements )
        self.ViewModelBoneMods = table.FullCopy( self.ViewModelBoneMods )

        self:CreateModels(self.VElements)
        self:CreateModels(self.WElements)


        if IsValid(self.Owner) then

            local vm = self.Owner:GetViewModel()
            if IsValid(vm) then
                self:ResetBonePositions(vm)


                if (self.ShowViewModel == nil or self.ShowViewModel) then
                    vm:SetColor(Color(255,255,255,255))
                else

                    vm:SetColor(Color(255,255,255,1))

                    vm:SetMaterial("Debug/hsv")
                end
            end
        end
    end
end

function SWEP:Deploy()
    local owner = self.Owner
    local function playerDiesSound( victim, weapon, killer )
        if( IsValid(killer) and IsValid(owner) and owner == killer and IsValid(victim)) then
            local soundfile = gb_config.websounds.."deadnowhaha.mp3"
            gb_PlaySoundFromServer(soundfile, victim)
        end
    end
    hook.Add( "PlayerDeath", "playerDeathSound_asdasdd"..self.Owner:SteamID(), playerDiesSound )
    self.BaseClass.Deploy(self)

end

SWEP.nextreload = 0
SWEP.NextReload = 0
function SWEP:Reload()

    if self.Owner:KeyDown(IN_USE) and self.NextReload < CurTime() then
        self.NextReload = CurTime() + 0.3
        local mode = self:GetNWString("shootmode", "Snipzor")
        self:SetIronsights(false)

        if mode == "Snipzor" then  self:SetNWString("shootmode","Barfer")  end
        if mode == "Barfer" then  self:SetNWString("shootmode","Poppzer") end
        if mode == "Poppzer" then  self:SetNWString("shootmode","Love Me") end
        if mode == "Love Me" then  self:SetNWString("shootmode","Cannibal") end
        if mode == "Cannibal" then  self:SetNWString("shootmode","Snipzor") end

        self.Weapon:SetNextPrimaryFire( CurTime() + 0.8 )
        return
    end

    if self.nextreload < CurTime() then
        self.nextreload = CurTime() + 0.3
        if self:IsTargetingCorpse() then
        self.nextreload = self.nextreload + 0.3
            self:IdentifyCorpse()
        end
    end


end
SWEP.nextmiget = 0
function SWEP:PrimaryAttack()

    if self.Owner:KeyDown(IN_USE) then
        if self.nextmiget > CurTime() then return end
        self.nextmiget = CurTime() + 60
        local ply = self.Owner
        local offset =  ply:GetViewOffset()
        if not ply.oldspeedmul then
            ply.oldspeedmul =  ply:GetNWInt("speedmul")
            ply.oldjumppower = ply:GetJumpPower()
        end

        ply.runslowed =true
        ply:SetNWInt("speedmul", 850)
        ply:SetNWInt("speedmul",850)
        ply:SetRunSpeed(850)
        ply:SetWalkSpeed(850)
        ply:SetJumpPower(900)
        ply:SetModelScale(0.5, 0.1)
        ply:SetViewOffset(Vector(0,0,30))

        timer.Create("NormalizeRunSpeedFreeze"..ply:UniqueID() ,5,1,function()
            if IsValid(ply) and  ply.oldspeedmul  then
                if tonumber(ply.oldspeedmul) < 220 then ply.OldRunSpeed = 260 end
                ply.runslowed =false
                ply:SetNWInt("speedmul", ply.oldspeedmul)
                ply.oldspeedmul = false
                ply:SetJumpPower(ply.oldjumppower)
                ply:SetViewOffset(Vector(0,0,65))
                ply:SetModelScale(1, 2)

            end
        end)
    end

    local mode = self:GetNWString("shootmode", "Snipzor")
    if mode == "Snipzor" then
    self:NormalPrimaryAttack()
    end

    if mode == "Barfer" then
        self.Weapon:SetNextPrimaryFire(CurTime() + 0.1)
        self:EmitSound("npc/antlion/shell_impact4.wav")



        self:ShootBullet( 13.5, 0.2, 1, 0.001 )
        if CLIENT then return end


       local bucket = ents.Create( "sent_popcorn_thrown" )
        bucket:SetOwner( self.Owner )

        bucket:SetPos( self.Owner:GetShootPos( ) )
        bucket:Spawn()
        bucket:SetMaterial( "engine/occlusionproxy" )
        bucket:Activate()


       local phys = bucket:GetPhysicsObject( )

        if IsValid( phys ) then
            phys:SetVelocity( self.Owner:GetPhysicsObject():GetVelocity() )
            phys:AddVelocity( self.Owner:GetAimVector( ) * 128 * phys:GetMass( ) )
            phys:AddAngleVelocity( VectorRand() * 128 * phys:GetMass( ) )
        end

        net.Start("Popcorn_Explosion")
        net.WriteVector(self:GetPos())
        net.WriteVector(VectorRand())
        net.WriteVector(phys:GetVelocity())
        net.WriteFloat(bucket:EntIndex())
        net.Broadcast()
    end
    if mode == "Poppzer" then
        self.Weapon:SetNextSecondaryFire( CurTime() + 0.35 )
        self.Weapon:SetNextPrimaryFire( CurTime() + 0.35 )

        self:EmitSound("weapons/gb_weapons/partygun1.mp3");
        self.Weapon:SendWeaponAnim( ACT_VM_HITCENTER )

        local tr = self.Owner:GetEyeTrace()
        if IsValid(tr.Entity) and (tr.Entity:IsPlayer() or tr.Entity:IsNPC()) then
            local edata = EffectData()
            edata:SetStart(self.Owner:GetShootPos())
            edata:SetOrigin(tr.HitPos)
            edata:SetNormal(tr.Normal)
            edata:SetEntity(tr.Entity)


            util.Effect("BloodImpact", edata)

            local boneid = tr.Entity:LookupBone("ValveBiped.Bip01_Head1")
            if !tr.Entity.biggerhead then tr.Entity.biggerhead = 1 end

            local multi = tr.Entity.biggerhead * 1
            tr.Entity.biggerhead = tr.Entity.biggerhead + 1
            -- print(multi)

            if boneid then
                tr.Entity:ManipulateBoneScale(boneid, Vector(1,1,1))
                tr.Entity:ManipulateBoneScale(boneid, Vector(tr.Entity.biggerhead,tr.Entity.biggerhead,tr.Entity.biggerhead))
            end

            if multi > 6 then
                tr.Entity.biggerhead = 1
                tr.Entity:ManipulateBoneScale(boneid, Vector(0.0001,0.0001,0.0001))
                if SERVER then
                    local ent = ents.Create( "env_explosion" )
                    if( IsValid( self.Owner ) ) then
                        ent:SetPos( tr.Entity:GetPos() )
                        ent:SetOwner( self.Owner )
                        ent:SetKeyValue( "iMagnitude", "75" )
                        ent:Spawn()
                        ent:Fire( "Explode", 0, 0 )
                        ent:EmitSound( "weapons/big_explosion.mp3", 511, 100 )
                    end
                end
            end

        end
    end

    if mode == "Love Me" then
        self.Weapon:SetNextPrimaryFire( CurTime() + 1 )
        self.Owner:EmitSound("weapons/gb_weapons/hestias_kiss_of_enlightment.mp3")

        local tr = self.Owner:GetEyeTrace()
        local victim = tr.Entity
        if not IsValid(victim) then return end
        local danceMove = ACT_GMOD_TAUNT_MUSCLE
        if (!victim:IsPlayer()) then return end

        victim:AnimPerformGesture_gb(danceMove);

        if victim:Health() < 145 then
            victim:EmitSound("items/medshot4.wav")
            victim:SetHealth(victim:Health()+10)
        end


    end
    if mode == "Cannibal" then
        self:Cannibal(4,9999)
    end


end

function SWEP:IsTargetingCorpse()
    local tr = self.Owner:GetEyeTrace(MASK_SHOT)
    local ent = tr.Entity

    return (IsValid(ent) and ent:GetClass() == "prop_ragdoll" and
            CORPSE.GetPlayerNick(ent, false) ~= false)
end

local confirm = Sound("npc/turret_floor/click1.wav")
function SWEP:IdentifyCorpse()
    if SERVER then
        local tr = self.Owner:GetEyeTrace(MASK_SHOT)
        CORPSE.ShowSearch(self.Owner, tr.Entity, false, true)
    elseif IsFirstTimePredicted() then
        LocalPlayer():EmitSound(confirm)
    end
end


function SWEP:SecondaryAttack()
    if self.Owner:KeyDown(IN_USE) then self:Fly() return end
    self:Zoom()
end

function SWEP:PreDrop(yes)
    self:SetZoom(false)
    self:SetIronsights(false)
    if yes then self:Remove() end

end



function SWEP:Holster()
    self:SetIronsights(false)
    self:SetZoom(false)
    return true
end


SWEP.IronSightsPos      = Vector( -100, -100, -100 )
SWEP.IronSightsAng      = Vector( 2.6, 1.37, 3.5 )
SWEP.Secondary.Sound = Sound("Default.Zoom")
-- Add some zoom to ironsights for this gun
function SWEP:Zoom()
    if not self.IronSightsPos then return end
    if self.Weapon:GetNextSecondaryFire() > CurTime() then return end

    local bIronsights = not self:GetIronsights()

    self:SetIronsights( bIronsights )

    if SERVER then
        self:SetZoom(bIronsights)
    else
        self:EmitSound(self.Secondary.Sound)
    end

    self.Weapon:SetNextSecondaryFire( CurTime() + 0.3)
end




function SWEP:SetZoom(state)
    if CLIENT then
        return
    elseif IsValid(self.Owner) and self.Owner:IsPlayer() then
        if state then
            self.Owner:SetFOV(8, 0.3)
        else
            self.Owner:SetFOV(0, 0.2)
        end
    end
end



if CLIENT then
    local scope = surface.GetTextureID("sprites/scope")
    function SWEP:DrawHUD()

        local shotttext = "Shootmode: "..self:GetNWString("shootmode", "Snipzor").."\nE+R to change"
        surface.SetFont( "ChatFont" );
        local size_x, size_y = surface.GetTextSize( shotttext );

        draw.RoundedBox( 6, ScrW( ) / 2 - size_x / 2 - 5, ScrH( ) - 100, size_x + 15, size_y + 10, Color( 0, 0, 0, 150 ) );
        draw.DrawText( shotttext , "ChatFont", ScrW( ) / 2, ScrH( ) - 100 + 5, Color( 255, 30, 30, 255 ), TEXT_ALIGN_CENTER );


        if self:GetIronsights() then
            surface.SetDrawColor( 0, 0, 0, 255 )

            local x = ScrW() / 2.0
            local y = ScrH() / 2.0
            local scope_size = ScrH()

            -- crosshair
            local gap = 80
            local length = scope_size
            surface.DrawLine( x - length, y, x - gap, y )
            surface.DrawLine( x + length, y, x + gap, y )
            surface.DrawLine( x, y - length, x, y - gap )
            surface.DrawLine( x, y + length, x, y + gap )

            gap = 0
            length = 50
            surface.DrawLine( x - length, y, x - gap, y )
            surface.DrawLine( x + length, y, x + gap, y )
            surface.DrawLine( x, y - length, x, y - gap )
            surface.DrawLine( x, y + length, x, y + gap )


            -- cover edges
            local sh = scope_size / 2
            local w = (x - sh) + 2
            surface.DrawRect(0, 0, w, scope_size)
            surface.DrawRect(x + sh - 2, 0, w, scope_size)

            surface.SetDrawColor(255, 0, 0, 255)
            surface.DrawLine(x, y, x + 1, y + 1)

            -- scope
            surface.SetTexture(scope)
            surface.SetDrawColor(255, 255, 255, 255)

            surface.DrawTexturedRectRotated(x, y, scope_size, scope_size, 0)

        else
            return self.BaseClass.DrawHUD(self)
        end
    end

    function SWEP:AdjustMouseSensitivity()
        return (self:GetIronsights() and 0.05) or nil
    end
end
