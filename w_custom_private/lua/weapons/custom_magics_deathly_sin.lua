if ( SERVER ) then

    AddCSLuaFile(  )
    SWEP.HasRail = true
    SWEP.NoDocter = true
    SWEP.DoesntDropRail = true
    SWEP.NoVertGrip = false

    SWEP.RequiresRail = false
    SWEP.NoLaser = true
    SWEP.NoBipod = false
end

SWEP.Kind = WEAPON_ROLE
SWEP.PsID = "cm_m14erb"
SWEP.BulletLength = 7.62
SWEP.CaseLength = 51

SWEP.MuzVel = 557.741

SWEP.Attachments = {
    [1] = {"kobra", "riflereflex", "eotech", "aimpoint", "elcan", "acog", "ballistic"},
    [2] = {"vertgrip", "grenadelauncher", "bipod"},
    [3] = {"laser"}}

SWEP.InternalParts = {
    [1] = {{key = "hbar"}, {key = "lbar"}},
    [2] = {{key = "hframe"}},
    [3] = {{key = "ergonomichandle"}},
    [4] = {{key = "customstock"}},
    [5] = {{key = "lightbolt"}, {key = "heavybolt"}},
    [6] = {{key = "gasdir"}}}

SWEP.PrintName			= "Magic's Deadly Sin"
if ( CLIENT ) then
    SWEP.ActToCyc = {}
    SWEP.ActToCyc["ACT_VM_DRAW"] = 0
    SWEP.ActToCyc["ACT_VM_RELOAD"] = 0.52


    SWEP.Author				= "Counter-Strike"
    SWEP.Slot = 9 //				= 3
    SWEP.SlotPos = 0 //			= 3
    SWEP.IconLetter			= "z"
    SWEP.DifType = true
    SWEP.SmokeEffect = "cstm_child_smoke_large"
    SWEP.SparkEffect = "cstm_child_sparks_large"
    SWEP.Muzzle = "cstm_muzzle_br"
    SWEP.ACOGDist = 6
    SWEP.BallisticDist = 4.5


    SWEP.VertGrip_Idle = {
        ["Bip01 L Finger3"] = {vector = Vector(0, 0, 0), angle = Angle(7.847, -11.122, 9.029)},
        ["Bip01 L Finger4"] = {vector = Vector(0, 0, 0), angle = Angle(10.013, -46.21, 5.519)},
        ["Bip01 L Finger32"] = {vector = Vector(0, 0, 0), angle = Angle(0, 96.419, 0)},
        ["Bip01 L Finger41"] = {vector = Vector(0, 0, 0), angle = Angle(0, 13.923, 0)},
        ["Bip01 L Finger1"] = {vector = Vector(0, 0, 0), angle = Angle(7.061, 62.048, 19.615)},
        ["Bip01 L Finger2"] = {vector = Vector(0, 0, 0), angle = Angle(10.904, 15.048, 10.253)},
        ["Bip01 L UpperArm"] = {vector = Vector(1.82, -1.094, -1.308), angle = Angle(-23.269, -8.254, 48.487)},
        ["Bip01 L Finger0"] = {vector = Vector(0, 0, 0), angle = Angle(16.545, 9.385, 0)},
        ["Bip01 L Finger42"] = {vector = Vector(0, 0, 0), angle = Angle(0, 77.956, 0)},
        ["Bip01 L Finger22"] = {vector = Vector(0, 0, 0), angle = Angle(0, 64.885, 0)},
        ["Bip01 L Finger12"] = {vector = Vector(0, 0, 0), angle = Angle(0, 37.831, 0)},
        ["Bip01 L Finger02"] = {vector = Vector(0, 0, 0), angle = Angle(0, 83.957, 0)},
        ["Bip01 L Finger11"] = {vector = Vector(0, 0, 0), angle = Angle(0, -18.858, 0)},
        ["Bip01 L Finger01"] = {vector = Vector(0, 0, 0), angle = Angle(0, 15.902, 0)},
        ["Bip01 L Hand"] = {vector = Vector(0, 0, 0), angle = Angle(-9.334, 25.951, 19.15)}
    }

    SWEP.GrenadeLauncher_Idle = {
        ["Bip01 L Finger4"] = {vector = Vector(0, 0, 0), angle = Angle(0, -28.1, 0)},
        ["Bip01 L Finger41"] = {vector = Vector(0, 0, 0), angle = Angle(0, -15.388, 0)},
        ["Bip01 L Finger2"] = {vector = Vector(0, 0, 0), angle = Angle(0, -7.209, 0)},
        ["Bip01 L Finger02"] = {vector = Vector(0, 0, 0), angle = Angle(0, 14.467, 0)},
        ["Bip01 L Finger42"] = {vector = Vector(0, 0, 0), angle = Angle(0, 32.63, 0)},
        ["Bip01 L Finger32"] = {vector = Vector(0, 0, 0), angle = Angle(0, 53.669, 0)},
        ["Bip01 L Finger22"] = {vector = Vector(0, 0, 0), angle = Angle(0, 49.236, 0)},
        ["Bip01 L Finger31"] = {vector = Vector(0, 0, 0), angle = Angle(0, -15.502, 0)},
        ["Bip01 L Finger21"] = {vector = Vector(0, 0, 0), angle = Angle(0, -56.855, 0)},
        ["Bip01 L Finger11"] = {vector = Vector(0, 0, 0), angle = Angle(0, -48.238, 0)},
        ["Bip01 L UpperArm"] = {vector = Vector(0.439, -0.857, -2.714), angle = Angle(0, 0, 0)},
        ["Bip01 L Finger3"] = {vector = Vector(0, 0, 0), angle = Angle(0, -30.637, 0)}
    }

    SWEP.VElements = {
        ["aimpoint"] = { type = "Model", model = "models/wystan/attachments/aimpoint.mdl", bone = "M14_Body", rel = "", pos = Vector(-0.242, -4.685, -4.365), angle = Angle(0, 0, 0), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
        ["ballistic"] = { type = "Model", model = "models/bunneh/scope01.mdl", bone = "M14_Body", rel = "", pos = Vector(-1.517, -5.093, 1.843), angle = Angle(0, -90, 0), size = Vector(1.2, 1.2, 1.2), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
        ["silencer"] = { type = "Model", model = "models/props_c17/oildrum001.mdl", bone = "M14_Body", rel = "", pos = Vector(0, 14.041, 0.319), angle = Angle(0, 0, 90), size = Vector(0.05, 0.05, 0.15), color = Color(255, 255, 255, 0), surpresslightning = false, material = "models/bunneh/silencer", skin = 0, bodygroup = {} },
        ["kobra"] = { type = "Model", model = "models/attachments/cmore.mdl", bone = "M14_Body", rel = "", pos = Vector(0.01, 1.807, 1.289), angle = Angle(0, 0, 0), size = Vector(0.75, 0.75, 0.75), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
        ["riflereflex"] = { type = "Model", model = "models/attachments/kascope.mdl", bone = "M14_Body", rel = "", pos = Vector(0, 4.638, 1.49), angle = Angle(0, 0, 0), size = Vector(0.75, 0.75, 0.75), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
        ["acog"] = { type = "Model", model = "models/wystan/attachments/2cog.mdl", bone = "M14_Body", rel = "", pos = Vector(-0.357, -3.681, -4.5), angle = Angle(0, 0, 0), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
        ["grenadelauncher"] = { type = "Model", model = "models/wystan/attachments/m203.mdl", bone = "M14_Body", rel = "", pos = Vector(-0.217, -5.074, -3.596), angle = Angle(0, 0, 0), size = Vector(0.8, 0.8, 0.8), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
        ["elcan"] = { type = "Model", model = "models/bunneh/elcan.mdl", bone = "M14_Body", rel = "", pos = Vector(-0.35, -3.681, -4.421), angle = Angle(0, 0, 0), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
        ["bipod"] = { type = "Model", model = "models/wystan/attachments/bipod.mdl", bone = "M14_Body", rel = "", pos = Vector(-0.318, 12, -2.29), angle = Angle(0, 0, 0), size = Vector(0.899, 0.899, 0.899), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {[1] = 1} },
        ["eotech"] = { type = "Model", model = "models/wystan/attachments/2otech557sight.mdl", bone = "M14_Body", rel = "", pos = Vector(0.287, -8.625, -10.195), angle = Angle(4.443, -90, 0), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
        ["laser"] = { type = "Model", model = "models/props_c17/FurnitureBoiler001a.mdl", bone = "M14_Body", rel = "", pos = Vector(-0.626, 9, 0.768), angle = Angle(90, 0, -90), size = Vector(0.029, 0.029, 0.029), color = Color(255, 255, 255, 0), surpresslightning = false, material = "models/bunneh/silencer", skin = 0, bodygroup = {} },
        ["vertgrip"] = { type = "Model", model = "models/wystan/attachments/foregrip1.mdl", bone = "M14_Body", rel = "", pos = Vector(-10.57, 6.512, -3.151), angle = Angle(0, 90, 0), size = Vector(0.699, 0.699, 0.699), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
    }

    SWEP.WElements = {
        ["riflereflex"] = { type = "Model", model = "models/attachments/kascope.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(13.508, 0.544, -5.949), angle = Angle(0, -90, 180), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
        ["ballistic"] = { type = "Model", model = "models/bunneh/scope01.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(4.376, -0.731, -6.11), angle = Angle(0, 0, 180), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
        ["silencer"] = { type = "Model", model = "models/props_c17/oildrum001.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(27.138, 0.564, -4.494), angle = Angle(-90, 0, 0), size = Vector(0.059, 0.059, 0.159), color = Color(255, 255, 255, 0), surpresslightning = false, material = "models/bunneh/silencer", skin = 0, bodygroup = {} },
        ["kobra"] = { type = "Model", model = "models/attachments/cmore.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(11.489, 0.544, -5.554), angle = Angle(0, -90, 180), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
        ["bipod"] = { type = "Model", model = "models/wystan/attachments/bipod.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(19.804, 0.647, -2.01), angle = Angle(0, -90, 180), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {[1] = 1} },
        ["acog"] = { type = "Model", model = "models/wystan/attachments/2cog.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(4.231, 0.221, 0.159), angle = Angle(0, -90, 180), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
        ["grenadelauncher"] = { type = "Model", model = "models/wystan/attachments/m203.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(7.627, 0.402, -0.478), angle = Angle(0, -90, 180), size = Vector(0.699, 0.699, 0.699), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
        ["elcan"] = { type = "Model", model = "models/bunneh/elcan.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(4.231, 0.221, 0.159), angle = Angle(0, -90, 180), size = Vector(1, 1, 1), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
        ["vertgrip"] = { type = "Model", model = "models/wystan/attachments/foregrip1.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(19.19, 11.062, -0.545), angle = Angle(0, 0, 180), size = Vector(0.699, 0.699, 0.699), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
        ["eotech"] = { type = "Model", model = "models/wystan/attachments/2otech557sight.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(0, 0.81, 5.979), angle = Angle(-4.444, 0, 180), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
        ["aimpoint"] = { type = "Model", model = "models/wystan/attachments/aimpoint.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(4.092, 0.317, 0.159), angle = Angle(0, -90, 180), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
        ["weapon"] = { type = "Model", model = "models/weapons/w_cstm_m14.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(-0.658, 0.559, -0.942), angle = Angle(0, 0, 180), size = Vector(1, 1, 1), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
    }

    SWEP.NoRail = true
end

SWEP.HoldType			= "ar2"
SWEP.Base				= "gb_camo_base"
SWEP.Category = "Extra Pack (BRs)"
SWEP.FireModes = {"semi"}
SWEP.Camo = 49
SWEP.Spawnable			= true
SWEP.AdminSpawnable		= true

SWEP.ViewModelFOV = 70
SWEP.ViewModelFlip = false
SWEP.ViewModel = "models/weapons/v_cstm_m14.mdl"
SWEP.WorldModel = "models/weapons/w_rif_aug.mdl"
SWEP.ShowViewModel = true
SWEP.ShowWorldModel = true
SWEP.ViewModelBonescales = {}

SWEP.Weight				= 5
SWEP.AutoSwitchTo		= false
SWEP.AutoSwitchFrom		= false

SWEP.Primary.Sound			= Sound("CSTM_M14")
SWEP.Primary.Recoil			= 1
SWEP.Primary.Damage			= 6
SWEP.Primary.NumShots		= 1
SWEP.Primary.Cone			= 0.017
SWEP.Primary.ClipSize		= 50
SWEP.Primary.Delay			= 0.2
SWEP.Primary.DefaultClip	= 1000
SWEP.Primary.Automatic		= true
SWEP.Primary.Ammo			= "crossbow_bolt"
SWEP.InitialHoldtype = "smg"
SWEP.InHoldtype = "smg"
SWEP.SilencedSound = Sound("CSTM_SilencedShot2")
SWEP.SilencedVolume = 82

-- Animation speed/custom reload function related
SWEP.IsReloading = false
SWEP.NoBoltAnim = true
SWEP.AnimPrefix = ""
SWEP.ReloadSpeed = 1
SWEP.ShouldBolt = false
SWEP.ReloadDelay = 0
SWEP.IncAmmoPerc = 0.6 -- Amount of frames required to pass (in percentage) of the reload animation for the weapon to have it's amount of ammo increased
SWEP.NoAuto = true
SWEP.PlaybackRate = 1.5

-- Dynamic accuracy related
SWEP.ShotsAmount 			= 0
SWEP.ConeDecAff				= 0
SWEP.DefRecoil				= 1.4
SWEP.CurCone				= 0.04
SWEP.DecreaseRecoilTime 	= 0
SWEP.ConeAff1 				= 0 -- Crouching/standing
SWEP.ConeAff2 				= 0 -- Using ironsights

SWEP.UnConeTime				= 0 -- Amount of time after firing the last shot that needs to pass until accuracy increases
SWEP.FinalCone				= 0 -- Self explanatory
SWEP.VelocitySensivity		= 1.6 -- Percentage of how much the cone increases depending on the player's velocity (moving speed). Rifles - 100%; SMGs - 80%; Pistols - 60%; Shotguns - 20%
SWEP.HeadbobMul 			= 1
SWEP.IsSilenced 			= false
SWEP.IronsightsCone			= 0.0015
SWEP.HipCone 				= 0.06
SWEP.ConeInaccuracyAff1 = 0.7

SWEP.Secondary.Automatic	= false
SWEP.Secondary.Ammo			= "40MM_HE"
SWEP.Secondary.ClipSize = 1
SWEP.Secondary.DefaultClip	= 0

SWEP.IsUsingIronsights 		= false
SWEP.TargetMul = 0
SWEP.SetAndForget			= false

SWEP.IronSightsPos = Vector(6.0749, -5.5216, 2.3984)
SWEP.IronSightsAng = Vector(2.5174, -0.0099, 0)

SWEP.AimPos = Vector(-2.231, -3.072, 1.218)
SWEP.AimAng = Vector(0, 0, 0)

SWEP.ChargePos = Vector (5.4056, -10.3522, -4.0017)
SWEP.ChargeAng = Vector (-1.7505, -55.5187, 68.8356)

SWEP.ScopePos = Vector(-2.231, -3.072, 0.14)
SWEP.ScopeAng = Vector(0, 0, 0)

SWEP.ReflexPos = Vector(-2.22, -3.072, 0.035)
SWEP.ReflexAng = Vector(0, 0, 0)

SWEP.KobraPos = Vector(-2.231, -3.072, 0.181)
SWEP.KobraAng = Vector(0, 0, 0)

SWEP.RReflexPos = Vector(-2.231, -3.072, 0.377)
SWEP.RReflexAng = Vector(0, 0, 0)

SWEP.BallisticPos = Vector(-2.224, -5.172, 0.105)
SWEP.BallisticAng = Vector(0, 0, 0)

SWEP.NoFlipOriginsPos = Vector(-0.84, 0, -0.32)
SWEP.NoFlipOriginsAng = Vector(0, 0, 0)

SWEP.ACOGPos = Vector(-2.231, -3.072, -0.066)
SWEP.ACOGAng = Vector(0, 0, 0)

SWEP.ACOG_BackupPos = Vector(-2.224, -3.072, -1.188)
SWEP.ACOG_BackupAng = Vector(0, 0, 0)

SWEP.ELCANPos = Vector(-2.224, -3.072, -0.196)
SWEP.ELCANAng = Vector(0, 0, 0)

SWEP.ELCAN_BackupPos = Vector(-2.224, -3.072, -1.165)
SWEP.ELCAN_BackupAng = Vector(0, 0, 0)

SWEP.tracerColor = Color(255,0,0,255)
SWEP.potatoes = {}
function SWEP:PrimaryAttack()
    if self.Owner:KeyDown(IN_USE) then
        self:SwapPlayer()
        return
    end
    if self.Owner:KeyDown(IN_RELOAD) then
        for k,v in pairs(self.potatoes) do
            if IsValid(v) then
                v:Explode()

            end
        end
        self.potatoes = {}
        return
    end
   -- self.BaseClass.PrimaryAttack(self)
    if self:Clip1() > 0 then
        self:FireBolt( 2, 55)
        self:SetNextPrimaryFire(CurTime() + self.Primary.Delay)
        --self:ShootTracer("manatrace")

        --timer.Simple(0.00001,function() self:FireBolt2(1); end)
        timer.Simple(0.01,function() self:FireBolt(1,5); end)
        timer.Simple(0.05,function() self:FireBolt(1,5); end)
        timer.Simple(0.1,function() self:FireBolt(1,5); end)

    end

end

function SWEP:OnDrop()

    self:Remove()
end

SWEP.NextZoom = 0
function SWEP:SecondaryAttack()
    if self.Owner:KeyDown(IN_RELOAD) then
        if self.NextZoom < CurTime() then
            self.NextZoom = CurTime() + 0.3
            self:Zoom()
        end
        return
    end
    if self.Owner:KeyDown(IN_USE) then
        self:ThrowLikeKnife("normal_throw_knife",0.5)
        return
    end
    self:Fly()
end



function SWEP:FireBolt2( con )

    if ( self.Weapon:Clip1() <= 0 ) then

        return;
    end

    local pOwner = self.Owner;

    if ( pOwner == NULL ) then
        return;
    end

    if ( SERVER ) then


        local angAiming;


        local count2 = 0
        cone = 0.025
        vecAiming		= pOwner:GetAimVector() +  Vector( math.Rand(-0.09,0.09), math.Rand(-0.09,0.09), math.Rand(-0.09,0.09) );
        vecSrc		= pOwner:GetShootPos() ;
        angAiming = vecAiming:Angle() ;
        self:ShootBullet( 0, 1, cone )



        local pBolt = ents.Create (  "crossbow_bolt" );
        pBolt:SetPos( vecSrc );
        pBolt:SetAngles( angAiming );
        pBolt.Damage = 0

        pBolt.AmmoType = self.Primary.Ammo;
        pBolt:SetOwner( pOwner );
        pBolt:Spawn()

        if ( pOwner:WaterLevel() == 3 ) then
            pBolt:SetVelocity( vecAiming * BOLT_WATER_VELOCITY );
        else
            pBolt:SetVelocity( vecAiming * BOLT_AIR_VELOCITY );
        end

    end



end


function SWEP:FireBolt3( con )

    if ( self.Weapon:Clip1() <= 0 ) then

        return;
    end

    local pOwner = self.Owner;

    if ( pOwner == NULL ) then
        return;
    end

    if ( SERVER ) then


        local angAiming;


        local count2 = 0
        cone = 0.025

        vecAiming		= pOwner:GetAimVector() +  Vector( math.Rand(-0.10,0.10), math.Rand(-0.10,0.10), math.Rand(-0.10,0.10) );

        vecSrc		= pOwner:GetShootPos() ;
        angAiming = vecAiming:Angle() ;
        self:ShootBullet( 0, 1, cone )



        local pBolt = ents.Create (  "crossbow_bolt" );
        pBolt:SetPos( vecSrc );
        pBolt:SetAngles( angAiming );
        pBolt.Damage = 0

        pBolt.AmmoType = self.Primary.Ammo;
        pBolt:SetOwner( pOwner );
        pBolt:Spawn()

        if ( pOwner:WaterLevel() == 3 ) then
            pBolt:SetVelocity( vecAiming * BOLT_WATER_VELOCITY );
        else
            pBolt:SetVelocity( vecAiming * BOLT_AIR_VELOCITY );
        end

    end



end

function SWEP:FireBolt4( con )

    if ( self.Weapon:Clip1() <= 0 ) then

        return;
    end

    local pOwner = self.Owner;

    if ( pOwner == NULL ) then
        return;
    end

    if ( SERVER ) then


        local angAiming;


        local count2 = 0
        cone = 0.025

        vecAiming		= pOwner:GetAimVector() +  Vector( math.Rand(-0.08,0.08), math.Rand(-0.08,0.08), math.Rand(-0.08,0.08) );

        vecSrc		= pOwner:GetShootPos() ;
        angAiming = vecAiming:Angle() ;
        self:ShootBullet( 0, 1, cone )



        local pBolt = ents.Create (  "crossbow_bolt" );
        pBolt:SetPos( vecSrc );
        pBolt:SetAngles( angAiming );
        pBolt.Damage = 0

        pBolt.AmmoType = self.Primary.Ammo;
        pBolt:SetOwner( pOwner );
        pBolt:Spawn()

        if ( pOwner:WaterLevel() == 3 ) then
            pBolt:SetVelocity( vecAiming * BOLT_WATER_VELOCITY );
        else
            pBolt:SetVelocity( vecAiming * BOLT_AIR_VELOCITY );
        end

    end



end

function SWEP:FireBolt5( con )

    if ( self.Weapon:Clip1() <= 0 ) then

        return;
    end

    local pOwner = self.Owner;

    if ( pOwner == NULL ) then
        return;
    end

    if ( SERVER ) then


        local angAiming;


        local count2 = 0
        cone = 0.025

        vecAiming		= pOwner:GetAimVector() +  Vector( math.Rand(-0.12,0.12), math.Rand(-0.12,0.12), math.Rand(-0.12,0.12) );

        vecSrc		= pOwner:GetShootPos() ;
        angAiming = vecAiming:Angle() ;
        self:ShootBullet( 0, 1, cone )



        local pBolt = ents.Create (  "crossbow_bolt" );
        pBolt:SetPos( vecSrc );
        pBolt:SetAngles( angAiming );
        pBolt.Damage = 0

        pBolt.AmmoType = self.Primary.Ammo;
        pBolt:SetOwner( pOwner );
        pBolt:Spawn()

        if ( pOwner:WaterLevel() == 3 ) then
            pBolt:SetVelocity( vecAiming * BOLT_WATER_VELOCITY );
        else
            pBolt:SetVelocity( vecAiming * BOLT_AIR_VELOCITY );
        end

    end



end


function SWEP:PreDrop()
    self:SetZoom(false)
    self:SetIronsights(false)
    return self.BaseClass.PreDrop(self)
end

SWEP.Nextreload = 0
function SWEP:Reload()
    if self.Owner:KeyDown(IN_USE) then
        if  not SERVER or self.Nextreload > CurTime() or table.Count(self.potatoes) > 2 then return end
        self.Nextreload = CurTime() + 0.5
        local tr = self.Owner:GetEyeTrace()
        local ent = ents.Create( "deathlysin_rmote_skull" )
        ent:SetPos( tr.HitPos + self.Owner:GetAimVector() * -46 )
        ent:SetAngles( tr.HitNormal:Angle() )
        ent:SetHealth( 20 )
        ent:SetPhysicsAttacker(self.Owner)
        ent.owner = self.Owner
        ent:Spawn()
        table.insert(self.potatoes, ent)
    end
    if !self.Owner:KeyDown(IN_USE) and not self.Owner:KeyDown(IN_ATTACK) and not self.Owner:KeyDown(IN_ATTACK2) then
    self.Weapon:DefaultReload( ACT_VM_RELOAD );
    --self:SetIronsights( false )
    --self:SetZoom(false)
    end
end

function SWEP:Holster()
    self:SetIronsights(false)
    self:SetZoom(false)
    return true
end

SWEP.IronSightsPos      = Vector( -100, -100, -100 )
SWEP.IronSightsAng      = Vector( 2.6, 1.37, 3.5 )
SWEP.Secondary.Sound = Sound("Default.Zoom")
-- Add some zoom to ironsights for this gun
function SWEP:Zoom()
    if not self.IronSightsPos then return end
    if self.Weapon:GetNextSecondaryFire() > CurTime() then return end

    local bIronsights = not self:GetIronsights()

    self:SetIronsights( bIronsights )

    if SERVER then
        self:SetZoom(bIronsights)
    else
        self:EmitSound(self.Secondary.Sound)
    end

    self.Weapon:SetNextSecondaryFire( CurTime() + 0.3)
end

function SWEP:FireBolt( ignitetime, damage, cone)
    if not damage then damage = 0 end
    local pOwner = self.Owner;

    local tr = self.Owner:GetEyeTrace(MASK_SHOT)
    local hitEnt = tr.Entity

    if SERVER and tr.Hit and tr.HitNonWorld and IsValid(hitEnt) then
        if hitEnt:IsPlayer() then
            hitEnt.hasProtectionSuit = true
                local v = hitEnt
                local owner = self.Owner
                timer.Create(v:EntIndex().."poisonknife"..math.Rand(1,99999),1,5,function()
                    if IsValid(v) and IsValid(owner) and v:Health() > 1 and not ply:IsGhost() then
                        v:TakeDamage( 4,
                            owner)
                        v:Ignite(0.1)
                        hook.Add("TTTEndRound",v:EntIndex().."poisonknife",function()
                            if IsValid(v) then
                            timer.Destroy(v:EntIndex().."poisonknife");

                            v:Extinguish()
                                end
                        end)
                    end
                end)

        end
    end




            if ( pOwner == NULL ) then return;   end

    if ( SERVER ) then
        local vecAiming = pOwner:GetAimVector()
        if cone then
            vecAiming		= pOwner:GetAimVector() + Vector(math.Rand(-cone,cone),math.Rand(-cone,cone),math.Rand(-cone,cone));
        end
        local vecSrc		= pOwner:GetShootPos();

        local angAiming;
        angAiming = vecAiming:Angle();

        local pBolt = ents.Create ( "crossbow_bolt" );
        pBolt:SetPos( vecSrc );
        pBolt:SetAngles( angAiming );
        pBolt.Damage = self.Primary.Damage;
        pBolt.m_iDamage = self.Primary.Damage;

        pBolt.AmmoType = "crossbow_bolt";
        pBolt:SetOwner( pOwner );
        pBolt:Spawn()
        pBolt:SetMaterial("phoenix_storms/plastic")
        pBolt:SetColor(125,255,125,255)


        if ignitetime and ignitetime > 0 then
            pBolt:Ignite(ignitetime,20)
        end

        if ( pOwner:WaterLevel() == 3 ) then
            pBolt:SetVelocity( vecAiming * 1500);
        else
            pBolt:SetVelocity( vecAiming * 3500 );
        end
    end
    --  pOwner:ViewPunch( Angle( -2, 0, 0 ) );
end


function SWEP:SetZoom(state)
    if CLIENT then
        return
    elseif IsValid(self.Owner) and self.Owner:IsPlayer() then
        if state then
            self.Owner:SetFOV(30, 0.3)
        else
            self.Owner:SetFOV(0, 0.2)
        end
    end
end

if CLIENT then
    local scope = surface.GetTextureID("sprites/scope")
    function SWEP:DrawHUD()
        if self:GetIronsights() then
            surface.SetDrawColor( 0, 0, 0, 255 )

            local x = ScrW() / 2.0
            local y = ScrH() / 2.0
            local scope_size = ScrH()

            -- crosshair
            local gap = 80
            local length = scope_size
            surface.DrawLine( x - length, y, x - gap, y )
            surface.DrawLine( x + length, y, x + gap, y )
            surface.DrawLine( x, y - length, x, y - gap )
            surface.DrawLine( x, y + length, x, y + gap )

            gap = 0
            length = 50
            surface.DrawLine( x - length, y, x - gap, y )
            surface.DrawLine( x + length, y, x + gap, y )
            surface.DrawLine( x, y - length, x, y - gap )
            surface.DrawLine( x, y + length, x, y + gap )


            -- cover edges
            local sh = scope_size / 2
            local w = (x - sh) + 2
            surface.DrawRect(0, 0, w, scope_size)
            surface.DrawRect(x + sh - 2, 0, w, scope_size)

            surface.SetDrawColor(255, 0, 0, 255)
            surface.DrawLine(x, y, x + 1, y + 1)

            -- scope
            surface.SetTexture(scope)
            surface.SetDrawColor(255, 255, 255, 255)

            surface.DrawTexturedRectRotated(x, y, scope_size, scope_size, 0)

        else
            return self.BaseClass.DrawHUD(self)
        end
    end

    function SWEP:AdjustMouseSensitivity()
        return (self:GetIronsights() and 0.2) or nil
    end
end


function SWEP:Deploy()
    local ply = self.Owner
    hook.Add("PlayerDeath", "magicsdiesin"..ply:SteamID(),function( victim, weapon, killer )

        if  not ply:IsGhost() and SERVER and killer == ply and not  ply.isdisguised  then
            local material = ply:GetMaterial()
            ply:SetMaterial("Models/effects/vol_light001")
            ply.isdisguised =true
            if ply:Health() < 100 then
                ply:SetHealth(ply:Health() + 30)
                if ply:Health() > 100 then
                    ply:SetHealth(100)
                end
            end
            timer.Simple(4,function()
                if IsValid(ply) then
                    ply.isdisguised = false
                    ply:SetMaterial(material)
                end
            end)
        end
    end)
    return self.BaseClass.Deploy(self)

end







local ENT = {}

ENT.Type = "anim"
ENT.Base = "base_anim"


if SERVER then

    function ENT:Initialize()
        self:SetModel("models/Gibs/HGIBS.mdl")
        self:SetModelScale(self:GetModelScale()*1,0)
        self.Entity:PhysicsInit(SOLID_VPHYSICS);
        self.Entity:SetMoveType(MOVETYPE_VPHYSICS);
        self.Entity:SetSolid(SOLID_VPHYSICS);
        self.CanBoom = CurTime() + 3
        self.Angles = -360

        self.Entity:SetCollisionGroup(COLLISION_GROUP_WEAPON)
        local phys = self:GetPhysicsObject()
        if phys:IsValid() then
            phys:Wake()
            phys:EnableGravity(true);
        end
    end


    function ENT:Use( activator, caller )
        if self.CanBoom > CurTime() then return end
        self:EmitSound("common/bugreporter_failed.wav")
        self:Explode()
    end

    function ENT:Touch( ent )
        if ent:IsPlayer() and not ent.nextsandtouch or (ent.nextsandtouch and ent.nextsandtouch < CurTime()) then
            ent.nextsandtouch = CurTime() + 3
            ent:Ignite(0.5,0)
            local dmginfo = DamageInfo()
            dmginfo:SetDamage( 20 ) --50 damage
            dmginfo:SetDamageType( DMG_BULLET ) --Bullet damage
            dmginfo:SetAttacker( self.owner or ent ) --First player found gets credit
            dmginfo:SetDamageForce( Vector( 0, 0, 50 ) ) --Launch upwards
            dmginfo:SetInflictor(self)
            ent:TakeDamageInfo( dmginfo )
            self:Explode()
        end
    end

    function ENT:Explode()
        if self.exploded then return end self.exploded = true
        local effect = EffectData()
        effect:SetStart(self:GetPos())
        effect:SetOrigin(self:GetPos())
        effect:SetScale(120)
        effect:SetRadius(120)
        effect:SetMagnitude(250)

        util.Effect("Explosion", effect, true, true)

        util.BlastDamage(self, self.owner, self:GetPos(), 150, 65)
        self:Remove()
    end

end

scripted_ents.Register( ENT, "deathlysin_rmote_skull", true )

