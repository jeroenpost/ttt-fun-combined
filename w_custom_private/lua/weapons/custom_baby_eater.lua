	SWEP.PrintName = "The Baby Eater"
	SWEP.DrawAmmo = false
	SWEP.DrawCrosshair = false
	
	


SWEP.Base = "gb_base"
SWEP.Kind = WEAPON_UNARMED

SWEP.CanBuy = nil -- no longer a buyable thing
--SWEP.LimitedStock = true

SWEP.AllowDrop = false
SWEP.Icon = "vgui/ttt_fun_killicons/fists.png"

SWEP.Author			= "robotboy655"
SWEP.Purpose		= "Well we sure as heck didn't use guns! We would wrestle Hunters to the ground with our bare hands! I would get ten, twenty a day, just using my fists."


SWEP.Spawnable			= true
SWEP.UseHands			= true
SWEP.DrawAmmo			= false

SWEP.ViewModel			= "models/weapons/c_arms_citizen.mdl"
SWEP.WorldModel			= ""

SWEP.ViewModelFOV		= 52
SWEP.ViewModelFlip		= false

SWEP.Primary.ClipSize		= -1
SWEP.Primary.DefaultClip	= -1
SWEP.Primary.Damage			= 49
SWEP.Primary.Automatic		= true
SWEP.Primary.Ammo			= "none"

SWEP.Secondary.ClipSize		= 1
SWEP.Secondary.DefaultClip	= 1
SWEP.Secondary.Damage			= 50
SWEP.Secondary.Automatic	= true
SWEP.Secondary.Ammo			= "Battery"

SWEP.Weight				= 5
SWEP.AutoSwitchTo		= false
SWEP.AutoSwitchFrom		= false

    SWEP.PrintName = "The Baby Eater"
SWEP.Slot				= 5
SWEP.SlotPos			= 5
SWEP.DrawAmmo			= false
SWEP.DrawCrosshair		= true
SWEP.secondUsed                 = false


local SwingSound = Sound( "weapons/slam/throw.wav" )
local HitSound = Sound( "Flesh.ImpactHard" )

function SWEP:Initialize()

	self:SetHoldType( "fist" )

end

function SWEP:Think()
   
end

function SWEP:PreDrawViewModel(viewModel, weapon, client)
    if SERVER or not IsValid(self.Owner) or not IsValid(viewModel) then return end
    viewModel:SetMaterial( "engine/occlusionproxy" )
end

SWEP.Distance = 67
SWEP.AttackAnimsRight = { "fists_left", "fists_right", "fists_uppercut" }
SWEP.AttackAnims = { "fists_right", "fists_uppercut" }
function SWEP:PrimaryAttack()
 self:AttackThing( self.AttackAnimsRight[ math.random( 1, #self.AttackAnimsRight ) ] )
end

function SWEP:SecondaryAttack()
    if not self.secondUsed then
         self:AttackThing( "fists_left"  )
    else
        self:AttackThing( "fists_right")
    end
	
end

SWEP.NextReload = 0
    SWEP.numNomNom = 1
function SWEP:Reload()
    if self.NextReload > CurTime() then return end


    if  self.numNomNom  > 0 and self.NextReload < CurTime() then


        local tracedata = {}
        tracedata.start = self.Owner:GetShootPos()
        tracedata.endpos = self.Owner:GetShootPos() + (self.Owner:GetAimVector() * 100)
        tracedata.filter = self.Owner
        tracedata.mins = Vector(1,1,1) * -10
        tracedata.maxs = Vector(1,1,1) * 10
        tracedata.mask = MASK_SHOT_HULL
        local tr = util.TraceLine({start = self.Owner:EyePos(), endpos = self.Owner:EyePos() + self.Owner:EyeAngles():Forward() * 80, filter = self.Owner})

        local ply = self.Owner
        self.NextReload = CurTime() + 0.8
        if IsValid(tr.Entity) then
            self.NextReload = CurTime() + 0.5
            if tr.Entity.player_ragdoll then
                self.numNomNom = 0

                local soundfile = gb_config.websounds.."baby_eater.mp3"
                gb_PlaySoundFromServer(soundfile, self.Owner)

                self:SetClip1( self:Clip1() - 1)

                self.NextReload = CurTime() + 5
                timer.Simple(0.1, function()
                    ply:Freeze(true)
                    ply:SetColor(Color(255,0,0,255))
                end)
                self.Weapon:SendWeaponAnim(ACT_VM_PRIMARYATTACK)


              --  self.Owner:EmitSound("ambient/sheep.wav")

                timer.Simple(6.1, function()
                    if not IsValid(ply) then return end
                    ply:Freeze(false)
                    ply:SetColor(Color(255,255,255,255))

                    local ent = ents.Create ("eatable_baby");
                    --  ent:SetModel (model_file);
                    ent:SetPos(  tr.Entity:GetPos() + Vector(10,10,10) )
                    ent:SetAngles (self.Owner:EyeAngles());
                    ent:SetPhysicsAttacker(self.Owner)
                    ent:Spawn();


                    tr.Entity:Remove()
                --self:Remove()
                end )
            end
        end

    end

end

function SWEP:DealDamage( anim )
	local tr = util.TraceLine( {
		start = self.Owner:GetShootPos(),
		endpos = self.Owner:GetShootPos() + self.Owner:GetAimVector() * self.Distance,
		filter = self.Owner
	} )

	if ( not IsValid( tr.Entity ) ) then 
		tr = util.TraceHull( {
			start = self.Owner:GetShootPos(),
			endpos = self.Owner:GetShootPos() + self.Owner:GetAimVector() * self.Distance,
			filter = self.Owner,
			mins = self.Owner:OBBMins() / 3,
			maxs = self.Owner:OBBMaxs() / 3
		} )
	end

	if ( tr.Hit ) then self.Owner:EmitSound( HitSound ) end

	if ( IsValid( tr.Entity ) && ( tr.Entity:IsNPC() || tr.Entity:IsPlayer() ) ) then
		local dmginfo = DamageInfo()
		dmginfo:SetDamage( self.Primary.Damage )
		if ( anim == "fists_left" ) then
			dmginfo:SetDamageForce( self.Owner:GetRight() * 49125 + self.Owner:GetForward() * 99984 ) -- Yes we need those specific numbers
                        dmginfo:SetDamage( self.Secondary.Damage )
                        

                        self.secondUsed = false
		elseif ( anim == "fists_right" ) then
			dmginfo:SetDamageForce( self.Owner:GetRight() * -49124 + self.Owner:GetForward() * 99899 )
                        dmginfo:SetDamage( self.Primary.Damage )
		elseif ( anim == "fists_uppercut" ) then
			dmginfo:SetDamageForce( self.Owner:GetUp() * 51589 + self.Owner:GetForward() * 100128 )
                        dmginfo:SetDamage( self.Primary.Damage )
		end
                tr.Entity:SetVelocity(self.Owner:GetForward() * 600 + Vector(0,0,400))
		dmginfo:SetInflictor( self )
		local attacker = self.Owner
		if ( !IsValid( attacker ) ) then attacker = self end
		dmginfo:SetAttacker( attacker )

		tr.Entity:TakeDamageInfo( dmginfo )
	end
end



function SWEP:AttackThing( anim )
   self.Owner:SetAnimation( PLAYER_ATTACK1 )
   
   
    
	if ( !SERVER ) then return end
   if !IsValid(self.Owner) then return end
	-- We need this because attack sequences won't work otherwise in multiplayer
	local vm = self.Owner:GetViewModel()
	vm:ResetSequence( vm:LookupSequence( "fists_idle_01" ) )

	--local anim = "fists_left" --self.AttackAnims[ math.random( 1, #self.AttackAnims ) ]

	timer.Simple( 0, function()
		if ( !IsValid( self ) || !IsValid( self.Owner ) || !self.Owner:GetActiveWeapon() || self.Owner:GetActiveWeapon() != self ) then return end
	
		local vm = self.Owner:GetViewModel()
		vm:ResetSequence( vm:LookupSequence( anim ) )

		self:Idle()
	end )

	timer.Simple( 0.05, function()
		if ( !IsValid( self ) || !IsValid( self.Owner ) || !self.Owner:GetActiveWeapon() || self.Owner:GetActiveWeapon() != self ) then return end
		if ( anim == "fists_left" ) then
			self.Owner:ViewPunch( Angle( 0, 16, 0 ) )
		elseif ( anim == "fists_right" ) then
			self.Owner:ViewPunch( Angle( 0, -16, 0 ) )
		elseif ( anim == "fists_uppercut" ) then
			self.Owner:ViewPunch( Angle( 16, -8, 0 ) )
		end
	end )

	timer.Simple( 0.2, function()
		if ( !IsValid( self ) || !IsValid( self.Owner ) || !self.Owner:GetActiveWeapon() || self.Owner:GetActiveWeapon() != self ) then return end
		if ( anim == "fists_left" ) then
			self.Owner:ViewPunch( Angle( 4, -16, 0 ) )
		elseif ( anim == "fists_right" ) then
			self.Owner:ViewPunch( Angle( 4, 16, 0 ) )
		elseif ( anim == "fists_uppercut" ) then
			self.Owner:ViewPunch( Angle( -32, 0, 0 ) )
		end
		self.Owner:EmitSound( SwingSound )
		
	end )

	timer.Simple( 0.2, function()
		if ( !IsValid( self ) || !IsValid( self.Owner ) || !self.Owner:GetActiveWeapon() || self.Owner:GetActiveWeapon() != self ) then return end
		self:DealDamage( anim )
	end )

	self.Weapon:SetNextSecondaryFire(CurTime() + 0.5)
    self.Weapon:SetNextPrimaryFire(CurTime() + 0.5)



end

function SWEP:Idle()

	local vm = self.Owner:GetViewModel()
	timer.Create( "fists_idle" .. self:EntIndex(), vm:SequenceDuration(), 1, function()
		vm:ResetSequence( vm:LookupSequence( "fists_idle_0" .. math.random( 1, 2 ) ) )
	end )

end



function SWEP:OnRemove()

	if ( IsValid( self.Owner ) ) then
		local vm = self.Owner:GetViewModel()
        if IsValid(vm) then
		vm:SetMaterial( "" )
        end
	end

	timer.Stop( "fists_idle" .. self:EntIndex() )

end

function SWEP:Holster( wep )
    if ( IsValid( self.Owner ) ) then
        local vm = self.Owner:GetViewModel()
        if IsValid(vm) then
            vm:SetMaterial( "" )
        end
    end

	return true
end
SWEP.modelView = false

function SWEP:Deploy()

    local vm = self.Owner:GetViewModel()
    vm:ResetSequence( vm:LookupSequence( "fists_draw" ) )


    self:Idle()

    return true

end

    function SWEP:SecondaryAttack()
        self.Weapon:SetNextSecondaryFire( CurTime() + 30 )

        if CLIENT or (self.Owner.NextEatMoney and self.Owner.NextEatMoney > CurTime()) then return end
        self.Owner.NextEatMoney =  CurTime() + 30
        local ent = ents.Create("eatable_baby")
        self:throw_attack ("eatable_baby",  "physics/glass/glass_impact_hard3.wav", 100)
        local soundfile = gb_config.websounds.."baby_eater.mp3"
        gb_PlaySoundFromServer(soundfile, self.Owner)

    end


    function SWEP:throw_attack (model_file, sound, away)
        local tr = self.Owner:GetEyeTrace();

        if IsValid( tr.Entity )  then
            if not tr.Entity.hasProtectionSuit then
                tr.Entity.hasProtectionSuit= true
                timer.Simple(5, function()
                    if not IsValid( tr.Entity )  then return end
                    tr.Entity.hasProtectionSuit = false
                end)
            end
        end

        self.BaseClass.ShootEffects (self);
        if (!SERVER) then return end;
        local ent = ents.Create ("eatable_baby");
        --  ent:SetModel (model_file);
        ent:SetPos( tr.HitPos + self.Owner:GetAimVector() * -46 )
        ent:SetAngles (self.Owner:EyeAngles());
        ent:SetPhysicsAttacker(self.Owner)
        ent:Spawn();
        ent:EmitSound( sound )
        if math.Rand(1,25) < 2 then
            ent:Ignite(100,100)
        end
        local phys = ent:GetPhysicsObject();
        local shot_length = tr.HitPos:Length();
        -- phys:ApplyForceCenter (self.Owner:GetAimVector():GetNormalized() * math.pow (shot_length, 3) * 2);

        timer.Simple(away,function()
            if IsValid(ent) then
                ent:Remove()
            end
        end)

        cleanup.Add (self.Owner, "props", ent);
        undo.Create ("Thrown model");
        undo.AddEntity (ent);
        undo.SetPlayer (self.Owner);
        undo.Finish();
    end

    local ENT = {}

    ENT.Type = "anim"
    ENT.Base = "base_anim"


    if SERVER then

        function ENT:Initialize()


            self:SetModel("models/props_c17/doll01.mdl")
            self:SetModelScale(self:GetModelScale()*1,0)

            self.Entity:PhysicsInit(SOLID_VPHYSICS);
            self.Entity:SetMoveType(MOVETYPE_VPHYSICS);
            self.Entity:SetSolid(SOLID_VPHYSICS);
            self.Entity:SetCollisionGroup(COLLISION_GROUP_PROJECTILE)
            self.Damage = 1
            local phys = self:GetPhysicsObject()
            if phys:IsValid() then
                phys:Wake()
                phys:EnableGravity(true);
            end

            local soundfile = gb_config.websounds.."weapons/gb_crybaby.mp3"
            gb_PlaySoundFromServer(soundfile, self)


        end

        function ENT:Use( activator, caller )
            self.Entity:Remove()
            if ( activator:IsPlayer() ) then
                if activator:Health() < 175 then
                    if self.lower then
                        activator:SetHealth(activator:Health()+10)
                    else
                        activator:SetHealth(activator:Health()+25)
                    end
                end
                activator:EmitSound("crisps/eat.mp3")
            end
        end


    end

    scripted_ents.Register( ENT, "eatable_baby", true )