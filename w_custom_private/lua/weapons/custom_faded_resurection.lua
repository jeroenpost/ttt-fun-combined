if SERVER then
    AddCSLuaFile(  )
    --resource.AddFile("materials/vgui/gb_weapon_icons/sticky_bomb.png")
    --resource.AddFile("materials/vgui/ttt_fun_killicons/sticky_bomb.png")
end

SWEP.HoldType			= "normal"

SWEP.Slot				= 6
if CLIENT then

    SWEP.Instructions		= ""
    SWEP.Slot				= 11
    SWEP.SlotPos			= 0
    SWEP.IconLetter			= "u"

    SWEP.EquipMenuData = {
        type="Weapon",
        name="Sticky bomb",
        desc="Bomb that can be placed on a player\nwithout them knowing it.\nCan be detonated on a dead player!"
    };
end
SWEP.Icon = "vgui/ttt_fun_killicons/sticky_bomb.png"


SWEP.PrintName			= "Faded Resurrection"

SWEP.Base = "aa_base"

SWEP.Spawnable          = false
SWEP.AdminSpawnable     = true
SWEP.ViewModel			= "models/weapons/v_eq_flashbang.mdl"
SWEP.WorldModel			= "models/dynamite/dynamite.mdl"
SWEP.Weight         = 50
SWEP.AutoSwitchTo       = false
SWEP.AutoSwitchFrom     = false
SWEP.DrawCrosshair      = false
SWEP.Primary.ClipSize       = -1
SWEP.Primary.DefaultClip    = -1
SWEP.Primary.Automatic      = true
SWEP.Primary.Ammo       = "none"
SWEP.Primary.Delay = 0.5

SWEP.Secondary.ClipSize     = -1
SWEP.Secondary.DefaultClip  = -1
SWEP.Secondary.Automatic    = true
SWEP.Secondary.Ammo     = "none"
SWEP.Secondary.Delay = 1.0

SWEP.Planted = false;

SWEP.Kind = 122

SWEP.LimitedStock = true
SWEP.WeaponID = AMMO_PBOMB

SWEP.AllowDrop = true;
SWEP.ModelEntity = false

SWEP.VElements = {
    ["clip"] = { type = "Model", model ="models/dynamite/dynamite.mdl", bone = "v_weapon.Flashbang_Parent", rel = "", pos = Vector(0, -5, -0.9), angle = Angle(90, 1.023, 93.068), size = Vector(0.5, 0.5, 0.5), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
}

if CLIENT then
    -- Path to the icon material



    SWEP.Icon = "vgui/ttt_fun_killicons/turtlegrenade.png"

    function SWEP:DrawWorldModel()
        --self:DrawModel()
        local ply = self.Owner
        local pos = self.Weapon:GetPos()
        local ang = self.Weapon:GetAngles()
        if ply:IsValid() then
            local bone = ply:LookupBone("ValveBiped.Bip01_R_Hand")
            if bone then
                pos,ang = ply:GetBonePosition(bone)
            end
        else
            self.Weapon:DrawModel() --Draw the actual model when not held.
            return
        end
        if not self.ModelEntity or not IsValid(self.ModelEntity)then
            self.ModelEntity = ClientsideModel(self.WorldModel)
            self.ModelEntity:SetNoDraw(true)
        end

        self.ModelEntity:SetModelScale(0.8,0)
        self.ModelEntity:SetPos(pos+Vector(1,4,0))
        self.ModelEntity:SetAngles(ang+Angle(0,0,190))
        self.ModelEntity:DrawModel()
    end

end

SWEP.BeepSound = Sound( "C4.PlantSound" );

local hidden = true;
local close = false;
local lastclose = false;

if CLIENT then
    function SWEP:DrawHUD( )
        local planted = self:GetNWBool( "Planted" );
        local tr = LocalPlayer( ):GetEyeTrace( );
        local close2;

        if tr.Entity:IsPlayer( ) and LocalPlayer( ):GetPos( ):Distance( tr.Entity:GetPos( ) ) <= 99999 then
            close2 = true;
        else
            close2 = false;
        end;

        local planted_text = "Not Planted!";
        local close_text = "Not Close Enough!";

        if planted then
            if hidden == true then
                hidden = false
            end
            planted_text = "Planted!\nSecondary to Explode!";

            surface.SetFont( "ChatFont" );
            local size_x, size_y = surface.GetTextSize( planted_text );

            draw.RoundedBox( 6, ScrW( ) / 2 - size_x / 2 - 5, ScrH( ) - 100, size_x + 15, size_y + 10, Color( 0, 0, 0, 150 ) );
            draw.DrawText( planted_text, "ChatFont", ScrW( ) / 2, ScrH( ) - 100 + 5, Color( 255, 30, 30, 255 ), TEXT_ALIGN_CENTER );
        else
            if close2 then
                close_text = "You are Close Enough!\nPrimary to Plant! RMB to plant special bomb";
            end;

            surface.SetFont( "ChatFont" );
            local size_x, size_y = surface.GetTextSize( planted_text );

            surface.SetFont( "ChatFont" );
            local size_x2, size_y2 = surface.GetTextSize( close_text );

            draw.RoundedBox( 6, ScrW( ) / 2 - ( size_x2 / 2 ) - 7.5, ScrH( ) - 100, size_x2 + 20, size_y + size_y2 + 10, Color( 0, 0, 0, 150 ) );
            draw.DrawText( planted_text, "ChatFont", ScrW( ) / 2, ScrH( ) - 100 + 5, Color( 30, 30, 255, 255 ), TEXT_ALIGN_CENTER );
            draw.DrawText( close_text, "ChatFont", ScrW( ) / 2 , ScrH( ) - 100 + size_y + 5, Color( 255, 255, 255, 255 ), TEXT_ALIGN_CENTER );
        end;
    end;
end;

function SWEP:OnDrop()
    if SERVER then
    end
    self:Remove()
end

function SWEP:Initialize()
    self:SetHoldType( self.HoldType or "pistol" )
    self.BaseClass.Initialize(self)
end;

function SWEP:Think( )
    local ply = self.Owner;
    if not IsValid( ply ) then return; end;

    local tr = ply:GetEyeTrace( );

    if tr.Entity:IsPlayer( ) and ply:GetPos( ):Distance( tr.Entity:GetPos( ) ) <= 99999 and GetRoundState() == ROUND_ACTIVE then
        close = true;
    else
        close = false;
    end;

    if self:GetNWBool( "Planted" ) == false then
        if lastclose == false and close then
            self.Weapon:SendWeaponAnim( ACT_SLAM_STICKWALL_ATTACH );
        elseif lastclose == true and not close then
            self.Weapon:SendWeaponAnim( ACT_SLAM_STICKWALL_IDLE );
        end;
    end;

    lastclose = close;

    if not hidden then
        if close and not self:GetNWBool( "Planted" ) then
            self.Weapon:SendWeaponAnim( ACT_SLAM_STICKWALL_ATTACH );
        elseif not close and not self:GetNWBool( "Planted" ) then
            self.Weapon:SendWeaponAnim( ACT_SLAM_STICKWALL_IDLE );
        elseif self:GetNWBool( "Planted" ) then
            self.Weapon:SendWeaponAnim( ACT_SLAM_DETONATOR_DRAW );
        end;
    end;
    if SERVER and not hidden and IsValid(self.Owner) and self.Owner:GetActiveWeapon() == self.Weapon then
        self.Owner:DrawViewModel(false)
        self.Owner:DrawWorldModel(false)
        hidden = true
    end

    if self.Owner:KeyDown(IN_USE) and not self.Owner:KeyDown(IN_ATTACK) and not self.Owner:KeyDown(IN_RELOAD) and not self.Owner:KeyDown(IN_ATTACK2) and self.Owner:KeyDown(IN_WALK)then
        self:TripMineStick()
    end

    self.BaseClass.Think()
end;

function SWEP:PrimaryAttack()
    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
    self.Weapon:SetNextSecondaryFire( CurTime() + self.Secondary.Delay )

    local ply = self.Owner;
    if not IsValid( ply ) then return; end;

    local tr = ply:GetEyeTrace(MASK_SHOT);

    if not self:GetNWBool( "Planted" ) then
        if close then
            if SERVER then
                self.PlantedPly = tr.Entity
                self:SetNWBool("Planted", true)
                tr.Entity.lastfadedhat = CurTime() + 600


            end;
            self.Weapon:SendWeaponAnim( ACT_SLAM_STICKWALL_ATTACH2 );
        end;
        if hidden == true and self:GetNWBool( "Planted" ) == true then
            hidden = false
        end
    end;
end

SWEP.JustSploded = 0
function SWEP:SecondaryAttack()

    if self.Owner:KeyDown(IN_USE)then
        self:Jihad()
        return
    end

    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
    self.Weapon:SetNextSecondaryFire( CurTime() + self.Secondary.Delay )
    if self:GetNWBool( "Planted" ) and  self.JustSploded < CurTime() then
        self:BombSplode()
    else

        local ply = self.Owner;
        if not IsValid( ply ) then return; end;

        local tr = ply:GetEyeTrace(MASK_SHOT);

        hook.Add("TTTPrepareRound","faded_reset"..ply:SteamID(),function()
            if IsValid(ply) then
            ply.used_faded_res = false
            end
        end)

        if not self:GetNWBool( "Planted" ) then
            if close then
                if SERVER and tr.Entity:IsPlayer()and not self.Owner.used_faded_res then
                  self.Owner.faded_timer = CurTime() +10
                  self.Owner.faded_target = tr.Entity
                  self.Owner.used_faded_res = true
                    timer.Create("faded_timer_bomb",0.5,20,function()
                        if IsValid(self) and IsValid(self.Owner) and IsValid(tr.Entity) and tr.Entity:Alive() then
                            self.Owner:PrintMessage( HUD_PRINTCENTER, "Kill "..tr.Entity:Nick().." within ".. math.floor(self.Owner.faded_timer - CurTime()).." seconds" )
                        end
                    end)

                  local ply = self.Owner




                  hook.Add("PlayerDeath", "faded_thingy"..self.Owner:SteamID(),function( victim, weapon, killer )
                      if IsValid(ply) and not ply:IsGhost() and SERVER and killer == ply and ply.faded_timer and ply.faded_timer > CurTime()  then
                          if IsValid(ply.faded_target) and victim == ply.faded_target then
                              ply.faded_revive = true
                              self.Owner:PrintMessage( HUD_PRINTCENTER, "SUCCESS. You will be revived when ID'd" )

                              hook.Add("TTTBodyFound", "faded_thingy_revive"..self.Owner:SteamID(),function(ply2, deadply, rag )
                                  if IsValid(ply) and ply == deadply and ply.faded_revive then
                                      ply.faded_revive = false
                                      deadply:SpawnForRound(true)
                                      deadply:SetCredits(1)
                                      deadply:SetGhost(false)
                                      --ply:SetTeam( TEAM_TERROR )
                                      deadply:Spawn()

                                      umsg.Start("_resurrectbody")
                                      umsg.Entity(deadply)
                                      umsg.Bool(CORPSE.GetFound(rag, false))
                                      umsg.End()
                                      rag:Remove()
                                  end
                              end)

                          end
                      end
                  end)

                end;
                self.Weapon:SendWeaponAnim( ACT_SLAM_STICKWALL_ATTACH2 );
            end;
            if hidden == true and self:GetNWBool( "Planted" ) == true then
                hidden = false
            end
        end;

    end;
end

function SWEP:DoSplode( owner, plantedply, bool )
    self:SetNWBool( "Planted", false )
    if not bool and self and self.BeepSound and owner then
        self:SetNWBool( "Planted", false )
        timer.Simple( 1, function( ) plantedply:EmitSound( self.BeepSound ); end );
        timer.Simple( 2, function( ) plantedply:EmitSound( self.BeepSound ); end );
        timer.Simple( 3, function( ) plantedply:EmitSound( self.BeepSound ); end );
        timer.Simple( 3, function( )
            local effectdata = EffectData()
            effectdata:SetOrigin(plantedply:GetPos())
            util.Effect("HelicopterMegaBomb", effectdata)

            plantedply.PlayerBombDie = true;
            plantedply.lastfadedhat= false

            local explosion = ents.Create("env_explosion")
            explosion:SetOwner(owner)
            explosion:SetPos(plantedply:GetPos( ) + Vector( 0, 0, 10 ))
            if plantedply.hasProtectionSuit then
              explosion:SetKeyValue("iMagnitude", "80")
            else
                explosion:SetKeyValue("iMagnitude", "40")
            end
            explosion:SetKeyValue("iRadiusOverride", 0)
            explosion:Spawn()
            explosion:Activate()
            explosion:Fire("Explode", "", 0)
            explosion:Fire("kill", "", 0)



            if not self.Owner.numberOfStickies then
                self.Owner.numberOfStickies = 1
            end
            --if self.Owner.numberOfStickies < 4 then
                self.Owner.numberOfStickies = self.Owner.numberOfStickies + 1

           -- end
        end );
    else
        timer.Simple( 1, function( ) plantedply:EmitSound( self.BeepSound ); end );
        timer.Simple( 2, function( ) plantedply:EmitSound( self.BeepSound ); end );
        timer.Simple( 3, function( ) plantedply:EmitSound( self.BeepSound ); end );
        timer.Simple( 4, function( )
            local effectdata = EffectData()
            effectdata:SetOrigin(plantedply:GetPos())
            util.Effect("HelicopterMegaBomb", effectdata)

            local explosion = ents.Create("env_explosion")
            explosion:SetOwner(owner)
            explosion:SetPos(plantedply:GetPos( ))
            explosion:SetKeyValue("iMagnitude", "55")
            explosion:SetKeyValue("iRadiusOverride", 0)
            explosion:Spawn()
            explosion:Activate()
            explosion:Fire("Explode", "", 0)
            explosion:Fire("kill", "", 0)
            plantedply.lastfadedhat= false



        end );
    end;
end;

function SWEP:BombSplode()
    local ply = self.Owner;
    if not IsValid( ply ) then return; end;

    if IsValid( self.PlantedPly ) and self.PlantedPly:Alive( ) then
        if SERVER then
            self:DoSplode( self.Owner, self.PlantedPly, false );
            self.JustSploded = CurTime()+5;
        end;
        self.Weapon:SendWeaponAnim( ACT_SLAM_DETONATOR_DETONATE );
    else
        if SERVER then
            if IsValid( self.PlantedPly ) and IsValid( self.PlantedPly.server_ragdoll ) then
                self:DoSplode( self.Owner, self.PlantedPly.server_ragdoll, true );
                self.JustSploded = CurTime()+5;
            else
                ply:PrintMessage( HUD_PRINTTALK, "The body this bomb has been planted to is no longer available!" );
            end;
        end;
        self.Weapon:SendWeaponAnim( ACT_SLAM_DETONATOR_DETONATE );
    end;
end;

function SWEP:Deploy( )
    //hidden = false;
    if self:GetNWBool( "Planted" ) == true then
       -- self:SetHoldType("normal")
        hidden = false
    end
    hook.Add("PlayerDeath","fadedhat_playerdeath",function(ply)
        ply.lastfadedhat = false
    end)
    return self.BaseClass.Deploy(self)
end;

SWEP.bombammo=5
SWEP.nextboms=0
SWEP.nextboms2=0
function SWEP:Reload()
    if self.Owner:KeyDown(IN_USE) then
        if self.bombammo > 0 and self.nextboms < CurTime()and SERVER then
            self.nextboms = CurTime() + 35
            local tr = self.Owner:GetEyeTrace();
            local ent = ents.Create( "cse_ent_shorthealthgrenade" )
            ent:SetModel("models/dynamite/dynamite.mdl")

            ent:SetPos( tr.HitPos  )
            ent:SetOwner( self.Owner  )
            ent:SetPhysicsAttacker(  self.Owner )
            ent.Owner = self.Owner
            ent:Spawn()
            ent:SetModel("models/dynamite/dynamite.mdl")
            ent.maxhealth = 150
            ent.healdamage = 5
            ent.timer = CurTime() + 0.01
            ent.EndColor = "0 255 0"
            ent.StartColor = "0 255 0"
            timer.Simple(10,function()
                if IsValid(ent) then
                    ent.Bastardgas:Remove()
                    ent.Entity:Remove()
                end
            end)
        elseif self.nextboms > CurTime() then
            self.Owner:PrintMessage( HUD_PRINTCENTER,  math.floor(self.nextboms - CurTime()).." seconds left" )
        end
        return
    end

    if self.nextboms2 > CurTime() then
        self.Owner:PrintMessage( HUD_PRINTCENTER,  math.floor(self.nextboms2 - CurTime()).." seconds left" )
        return
    end
    if CLIENT then return end
    local ball = ents.Create("faded_res_bomb");
    self.nextboms2 = CurTime() + 35
    if (ball) then
        ball:SetPos(self.Owner:GetPos() + Vector(0,0,48));
        ball:SetPhysicsAttacker(self.Owner)
        ball.owner = self.Owner
        ball:Spawn();
        --ball:SetModelScale( ball:GetModelScale() * 0.1, 0.01 )
        local physicsObject = ball:GetPhysicsObject();
        physicsObject:ApplyForceCenter(self.Owner:GetAimVector() * 1000);
        self.RemoteBoom= ball;
    end;

end

function SWEP:OnDrop()
    self:Remove()
end

SWEP.hadtripmines = 0
function SWEP:TripMineStick()
    if SERVER then
        local ply = self.Owner
        if not IsValid(ply) then return end


        local ignore = {ply, self.Weapon}
        local spos = ply:GetShootPos()
        local epos = spos + ply:GetAimVector() * 80
        local tr = util.TraceLine({start=spos, endpos=epos, filter=ignore, mask=MASK_SOLID})

        if tr.HitWorld and self.hadtripmines < 3 then

            local mine = ents.Create("npc_tripmine")
            if IsValid(mine) then


                local tr_ent = util.TraceEntity({start=spos, endpos=epos, filter=ignore, mask=MASK_SOLID}, mine)

                if tr_ent.HitWorld then

                    local ang = tr_ent.HitNormal:Angle()
                    ang.p = ang.p + 90

                    mine:SetPos(tr_ent.HitPos + (tr_ent.HitNormal * 3))
                    mine:SetAngles(ang)
                    mine:SetModel("models/dynamite/dynamite.mdl")

                    mine:SetOwner(ply)
                    mine:Spawn()
                    mine:SetModel("models/dynamite/dynamite.mdl")
                    -- mine:SetHealth( 10 )

                    function mine:OnTakeDamage(dmg)
                        self:Remove()
                    end

                    -- mine:Ignite(100)
                    --  print( mine )
                    if SERVER then DamageLog("Tripmine: " .. self.Owner:Nick() .. " [" .. self.Owner:GetRoleString() .. "] placed a tripmine ")
                    end

                    mine.fingerprints = self.fingerprints

                    self:SendWeaponAnim( ACT_SLAM_TRIPMINE_ATTACH )

                    local holdup = self.Owner:GetViewModel():SequenceDuration()

                    timer.Simple(holdup,
                        function()
                            if SERVER then
                                self:SendWeaponAnim( ACT_SLAM_TRIPMINE_ATTACH2 )
                            end
                        end)

                    timer.Simple(holdup + .1,
                        function()
                            if SERVER then
                                if self.Owner == nil then return end
                                if self.Weapon:Clip1() == 0 && self.Owner:GetAmmoCount( self.Weapon:GetPrimaryAmmoType() ) == 0 then
                                --self.Owner:StripWeapon(self.Gun)
                                --RunConsoleCommand("lastinv")
                                self:Remove()
                                else
                                    self:Deploy()
                                end
                            end
                        end)


                    --self:Remove()
                    self.Planted = true

                    self.hadtripmines = self.hadtripmines + 1

                    if not self.Owner:IsTraitor() then
                        self.Owner:PrintMessage( HUD_PRINTCENTER, "You lost 75 health for placing a tripmine as inno" )
                        self.Owner:SetHealth( self.Owner:Health() - 75)
                    end

                end
            end
        end
    end
end


local ENT = {}

ENT.Type = "anim"
ENT.Base = "base_anim"


if SERVER then

    function ENT:Initialize()
        self:SetModel("models/dynamite/dynamite.mdl")
        self:SetModelScale(self:GetModelScale()*1,0)

        self.Entity:PhysicsInit(SOLID_VPHYSICS);
        self.Entity:SetMoveType(MOVETYPE_VPHYSICS);
        self.Entity:SetSolid(SOLID_VPHYSICS);
        self.CanBoom = CurTime() + 3
        self.timer = CurTime() + 10

        self.Entity:SetCollisionGroup(COLLISION_GROUP_WEAPON)
        local phys = self:GetPhysicsObject()
        if phys:IsValid() then
            phys:Wake()
            phys:EnableGravity(true);
        end
    end

    function ENT:Think()
        if self.timer < CurTime() then
            self:Explode()
        end
    end
    ENT.nextuse = 0
    function ENT:Use( activator, caller )
        if self.nextuse < CurTime() then
            if activator == self.owner and self.nextuse < CurTime() then
                self.nextuse = CurTime() + 0.15
                self.timer = self.timer + 5
                self.owner:PrintMessage( HUD_PRINTCENTER, "5 seconds added to timer, bomb will explode in "..math.floor(self.timer-CurTime()).. "seconds" )
                return
            end
            self.nextuse = CurTime() + 0.15
            self:EmitSound("common/bugreporter_failed.wav")
            self:Explode(true)
        end
    end


    function ENT:Explode(nodamage)
        if self.exploded then return end self.exploded = true
        local effect = EffectData()
        effect:SetStart(self:GetPos())
        effect:SetOrigin(self:GetPos())
        if nodamage then
            effect:SetScale(1)
            effect:SetRadius(1)
            effect:SetMagnitude(1)

            util.Effect("Explosion", effect, true, true)

            util.BlastDamage(self, self.owner, self:GetPos(), 1, 1)
            self:Remove()
            return
        end
        effect:SetScale(120)
        effect:SetRadius(120)
        effect:SetMagnitude(250)

        util.Effect("Explosion", effect, true, true)

        util.BlastDamage(self, self.owner, self:GetPos(), 250, 100)
        self:Remove()
    end


end

scripted_ents.Register( ENT, "faded_res_bomb", true )
