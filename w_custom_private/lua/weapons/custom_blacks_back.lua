SWEP.Base = "aa_base"
SWEP.ViewModelFOV	= 74
SWEP.HoldType = "duel"
SWEP.PrintName = "Black's Back"
SWEP.ViewModel		= "models/weapons/v_pist_deags.mdl"
SWEP.WorldModel		= "models/weapons/w_pist_deagle.mdl"
SWEP.Kind = WEAPON_NADE
SWEP.Slot = 3
SWEP.AutoSpawnable = false
SWEP.Primary.Recoil     = 2.5
SWEP.Primary.Damage = 49
SWEP.Primary.Delay = 0.45
SWEP.Primary.Cone = 0.01
SWEP.Primary.ClipSize = 80
SWEP.Primary.Automatic = true
SWEP.Primary.DefaultClip = 80
SWEP.Camo = 999
SWEP.Secondary.Delay = 0.40
SWEP.Secondary.ClipSize     = 20
SWEP.Secondary.DefaultClip  = 20
SWEP.Secondary.Automatic    = true
SWEP.Secondary.Ammo         = "Pistol"
SWEP.Secondary.ClipMax      = 60

SWEP.Primary.Ammo			= "AlyxGun"
SWEP.AmmoEnt = "item_ammo_pistol_ttt"
SWEP.HeadshotMultiplier = 2.5

function SWEP:OnDrop()
    self:Remove()
end

SWEP.Primary.Sound = Sound("CSTM_Deagle")
SWEP.Secondary.Sound = Sound("CSTM_Deagle")

SWEP.WElements = {
    ["weapon"] = { type = "Model", model = "models/weapons/w_pist_deagle.mdl", bone = "ValveBiped.Bip01_L_Hand", rel = "", pos = Vector(3.6, 0.631, -2.5), angle = Angle(0, 0, 0), size = Vector(1, 1, 1), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
}


function SWEP:PrimaryAttack( worldsnd )

    --self.Weapon:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )

    if not self:CanPrimaryAttack() then return end

    if not worldsnd then
        self.Weapon:EmitSound( self.Primary.Sound, self.Primary.SoundLevel )
    elseif SERVER then
        sound.Play(self.Primary.Sound, self:GetPos(), self.Primary.SoundLevel)
    end

    self:ShootBullet( self.Primary.Damage, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )

    self:TakePrimaryAmmo( 1 )
    self:Disorientate()

    local owner = self.Owner
    if not IsValid(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

    --  owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )
end

function SWEP:Think()

    if self.Owner:KeyPressed(IN_ATTACK)  then
        -- When the left click is pressed, then
        self.ViewModelFlip = false
    end

    if self.Owner:KeyPressed(IN_ATTACK2)  then
        -- When the right click is pressed, then
        self.ViewModelFlip = true
    end
end

function SWEP:CanSecondaryAttack()
    if not IsValid(self.Owner) then return end

    if self.Weapon:Clip1() <= 0 then
        self:DryFire(self.SetNextSecondaryFire)
        return false
    end
    return true
end

function SWEP:SecondaryAttack(worldsnd)


    self.Weapon:SetNextSecondaryFire( CurTime() + self.Secondary.Delay )
    --self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )

    if not self:CanSecondaryAttack() then return end

    if not worldsnd then
        self.Weapon:EmitSound( self.Primary.Sound, self.Primary.SoundLevel )
    elseif SERVER then
        sound.Play(self.Primary.Sound, self:GetPos(), self.Primary.SoundLevel)
    end

    self:ShootBullet( self.Primary.Damage, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )

    self:TakePrimaryAmmo( 1 )
    self:Disorientate()

    local owner = self.Owner
    if not IsValid(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

    -- owner:ViewPunch( Angle( math.Rand(-0.6,-0.6) * self.Primary.Recoil, math.Rand(-0.6,0.6) *self.Primary.Recoil, 0 ) )

end
