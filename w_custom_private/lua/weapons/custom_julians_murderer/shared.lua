
if SERVER then
   AddCSLuaFile(  )
    --resource.AddFile("materials/vgui/ttt_fun_killicons/xm1014.png")
end

SWEP.HoldType			= "shotgun"
SWEP.PrintName = "Julian's Assassin"
SWEP.Slot = 4
if CLIENT then
   
   SWEP.Author = "GreenBlack"
   
   SWEP.Icon = "vgui/ttt_fun_killicons/xm1014.png"
end


SWEP.Base				= "weapon_tttbase"
SWEP.Spawnable = true
SWEP.AdminSpawnable = true

SWEP.Kind = WEAPON_NADE
SWEP.WeaponID = AMMO_SHOTGUN

SWEP.Primary.Ammo = "Buckshot"
SWEP.Primary.Damage = 10
SWEP.Primary.Cone = 0.085
SWEP.Primary.Delay = 0.65
SWEP.Primary.ClipSize = 24
SWEP.Primary.ClipMax = 96
SWEP.Primary.DefaultClip = 24
SWEP.Primary.Automatic = true
SWEP.Primary.NumShots = 9
SWEP.AutoSpawnable      = false
SWEP.AmmoEnt = "item_box_buckshot_ttt"

SWEP.UseHands			= true
SWEP.ViewModel          = Model("models/weapons/v_stunbaton.mdl")
SWEP.WorldModel         = Model("models/weapons/w_stunbaton.mdl")
SWEP.ViewModelFlip = false

SWEP.Primary.Sound			= Sound( "Weapon_XM1014.Single" )
SWEP.Primary.SoundLevel = 10
SWEP.Primary.Recoil			= 7

SWEP.IronSightsPos = Vector(-6.881, -9.214, 2.66)
SWEP.IronSightsAng = Vector(-0.101, -0.7, -0.201)


SWEP.reloadtimer = 0


-- Shooting functions largely copied from weapon_cs_base
function SWEP:PrimaryAttack(worldsnd)

    self.Weapon:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )

    if not self:CanPrimaryAttack() then return end



    self:ShootBullet( self.Primary.Damage, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )

    self:TakePrimaryAmmo( 1 )

    local owner = self.Owner
    if not IsValid(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

    owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )
end

function SWEP:SetupDataTables()
   self:DTVar("Bool", 0, "reloading")

   return self.BaseClass.SetupDataTables(self)
end

function SWEP:Reload()
   self:SetIronsights( false )
   
   --if self.Weapon:GetNWBool( "reloading", false ) then return end
   if self.dt.reloading then return end

   if not IsFirstTimePredicted() then return end
   
   if self.Weapon:Clip1() < self.Primary.ClipSize and self.Owner:GetAmmoCount( self.Primary.Ammo ) > 0 then
      
      if self:StartReload() then
         return
      end
   end

end

function SWEP:StartReload()
   --if self.Weapon:GetNWBool( "reloading", false ) then
   if self.dt.reloading then
      return false
   end

   if not IsFirstTimePredicted() then return false end

   self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
   
   local ply = self.Owner
   
   if not ply or ply:GetAmmoCount(self.Primary.Ammo) <= 0 then 
      return false
   end

   local wep = self.Weapon
   
   if wep:Clip1() >= self.Primary.ClipSize then 
      return false 
   end

   wep:SendWeaponAnim(ACT_SHOTGUN_RELOAD_START)

   self.reloadtimer =  CurTime() + wep:SequenceDuration()

   --wep:SetNWBool("reloading", true)
   self.dt.reloading = true

   return true
end

function SWEP:PerformReload()
   local ply = self.Owner
   
   -- prevent normal shooting in between reloads
   self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )

   if not ply or ply:GetAmmoCount(self.Primary.Ammo) <= 0 then return end

   local wep = self.Weapon

   if wep:Clip1() >= self.Primary.ClipSize then return end

   self.Owner:RemoveAmmo( 1, self.Primary.Ammo, false )
   self.Weapon:SetClip1( self.Weapon:Clip1() + 1 )

   wep:SendWeaponAnim(ACT_VM_RELOAD)

   self.reloadtimer = CurTime() + wep:SequenceDuration()
end

function SWEP:FinishReload()
   self.dt.reloading = false
   self.Weapon:SendWeaponAnim(ACT_SHOTGUN_RELOAD_FINISH)
   
   self.reloadtimer = CurTime() + self.Weapon:SequenceDuration()
end

function SWEP:CanPrimaryAttack()
    if self.Weapon:Clip1() <= 0 then
        self:EmitSound( "Weapon_Shotgun.Empty" )
        self:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
        return false
    end
    if self.reloadtimer < CurTime() then
        return true
    else
        return false
    end
end

function SWEP:Think()
   if self.dt.reloading and IsFirstTimePredicted() then
      if self.Owner:KeyDown(IN_ATTACK) then
         self:FinishReload()
         return
      end
      
      if self.reloadtimer <= CurTime() then

         if self.Owner:GetAmmoCount(self.Primary.Ammo) <= 0 then
            self:FinishReload()
         elseif self.Weapon:Clip1() < self.Primary.ClipSize then
            self:PerformReload()
         else
            self:FinishReload()
         end
         return            
      end
   end
end

function SWEP:Deploy()
   self.dt.reloading = false
   self.reloadtimer = 0
   return self.BaseClass.Deploy(self)
end

-- The shotgun's headshot damage multiplier is based on distance. The closer it
-- is, the more damage it does. This reinforces the shotgun's role as short
-- range weapon by reducing effectiveness at mid-range, where one could score
-- lucky headshots relatively easily due to the spread.
function SWEP:GetHeadshotMultiplier(victim, dmginfo)
   local att = dmginfo:GetAttacker()
   if not IsValid(att) then return 3 end

   local dist = victim:GetPos():Distance(att:GetPos())
   local d = math.max(0, dist - 140)
   
   -- decay from 3.1 to 1 slowly as distance increases
   return 1 + math.max(0, (2.1 - 0.002 * (d ^ 1.25)))
end



SWEP.NumBlinds = 0
function SWEP:SecondaryAttack()
    self.Weapon:SetNextSecondaryFire( CurTime() + 0.14 )
    self.Weapon:SetNextPrimaryFire( CurTime() + 0.14 )

    if  self.NumBlinds > 4 then return end

    self:ShootBullet( 15, self.Primary.Recoil, 1, self:GetPrimaryCone() )


    local owner = self.Owner
    if not IsValid(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

    owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )

    local trace = self.Owner:GetEyeTrace()
    if (trace.HitNonWorld) then
        local victim = trace.Entity
        if ( not victim:IsPlayer()) then return end

        self.NumBlinds = self.NumBlinds + 1
        if SERVER then

            victim.IsBlinded = true
            victim:SetNWBool("isblinded",true)
            umsg.Start( "ulx_blind", victim )
            umsg.Bool( true )
            umsg.Short( 255 )
            umsg.End()
        end
        victim:AnimPerformGesture(ACT_GMOD_TAUNT_PERSISTENCE);
        if (SERVER) then

            timer.Create("ResetPLayerAfterBlided"..victim:UniqueID(), 1,1, function()
                if not IsValid(victim)  then  return end

                if SERVER then
                    victim.IsBlinded = false
                    victim:SetNWBool("isblinded",false)
                    umsg.Start( "ulx_blind", victim )
                    umsg.Bool( false )
                    umsg.Short( 0 )
                    umsg.End()

                end
            end)
        end
    end


end
