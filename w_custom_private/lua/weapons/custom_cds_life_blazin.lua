SWEP.Base = "gb_camo_base"
SWEP.ViewModelFOV	= 54
SWEP.HoldType = "smg"
SWEP.PrintName = "C&D's Life Blazin"
SWEP.ViewModel		= "models/weapons/cstrike/c_rif_m4a1.mdl"
SWEP.WorldModel		= "models/weapons/w_rif_m4a1.mdl"
SWEP.AutoSpawnable = false
SWEP.CanDecapitate= true
SWEP.HasFireBullet = true

SWEP.HoldType = "ar2"
SWEP.Slot = 2
SWEP.Camo = 20

if CLIENT then

    SWEP.Author = "GreenBlack"

    SWEP.Icon = "vgui/ttt_fun_killicons/m4a1.png"
end

SWEP.Primary.Delay			= 0.20
SWEP.Primary.Recoil			= 0.5
SWEP.Primary.Automatic = true
SWEP.Primary.Ammo = "Pistol"
SWEP.Primary.Damage = 27
SWEP.Primary.Cone = 0.001
SWEP.Primary.ClipSize = 100
SWEP.Primary.ClipMax = 100
SWEP.Primary.DefaultClip = 100

SWEP.AmmoEnt = "item_ammo_pistol_ttt"

SWEP.Primary.Sound = Sound( "Weapon_M4A1.Silenced"  )
SWEP.HeadshotMultiplier = 1.2

SWEP.IronSightsPos = Vector(-4, -6.2, 0.55)
SWEP.IronSightsAng = Vector(2.599, -1.3, -3.6)


function SWEP:SetZoom(state)
    if CLIENT then return end
    if not (IsValid(self.Owner) and self.Owner:IsPlayer()) then return end
    if state then
        self.Owner:SetFOV(35, 0.5)
    else
        self.Owner:SetFOV(0, 0.2)
    end
end

function SWEP:PrimaryAttack()
    if self:Clip1() > 0 then
    self:MakeAFlame()
    end
    self.BaseClass.PrimaryAttack(self)
end

-- Add some zoom to ironsights for this gun
function SWEP:SecondaryAttack()

    if self.Owner:KeyDown(IN_USE) then
        self:ShootFlare()
        return
    end
    if self.Owner:KeyDown(IN_RELOAD) then
        if not self.IronSightsPos then return end
        if self.Weapon:GetNextSecondaryFire() > CurTime() then return end

        bIronsights = not self:GetIronsights()

        self:SetIronsights( bIronsights )

        if SERVER then
            self:SetZoom(bIronsights)
        end

        self.Weapon:SetNextSecondaryFire(CurTime() + 0.3)
        return
    end

    self:Fly()


end

function SWEP:PreDrop()
    self:SetZoom(false)
    self:SetIronsights(false)
    return self.BaseClass.PreDrop(self)
end

function SWEP:Reload()
    if self.Owner:KeyDown(IN_USE) then
        self:DropHealth()
        return
    end
    self.Weapon:DefaultReload( ACT_VM_RELOAD );
    self:SetIronsights( false )
    self:SetZoom(false)
end


function SWEP:Holster()
    self:SetIronsights(false)
    self:SetZoom(false)
    return true
end


SWEP.NextRegenAmmo = 0
function SWEP:Think()

    if self.NextRegenAmmo < CurTime() then
        if self:Clip1() < 200 then
            self.NextRegenAmmo = CurTime() + 3
            self:SetClip1(self:Clip1()+2)
        end
    end

    self.BaseClass.Think(self)
end


function SWEP:DropHealth()
    if self.NextHealthDrop > CurTime() then return end
    self.NextHealthDrop = CurTime() + 5
    local healttt = 200
    if self.healththing and SERVER then
        healttt = Entity(self.healththing):GetStoredHealth()
        Entity(self.healththing):Remove()
    end
    self:HealthDrop(healttt)
end


SWEP.NextHealthDrop = 0

local throwsound = Sound( "Weapon_SLAM.SatchelThrow" )

function SWEP:HealthDrop( healttt)
    if SERVER then
        local ply = self.Owner
        if not IsValid(ply) then return end

        local vsrc = ply:GetShootPos()
        local vang = ply:GetAimVector()
        local vvel = ply:GetVelocity()

        local vthrow = vvel + vang * 200

        local health = ents.Create("normal_health_station")
        if IsValid(health) then
            health:SetPos(vsrc + vang * 10)
            health:Spawn()
            --health:SetModel( "models/props_lab/cactus.mdl")
            health:SetModelScale(health:GetModelScale()*1,0)
            health:SetPlacer(ply)
            --health.healsound = "ginger_cartman.mp3"

            health:PhysWake()
            local phys = health:GetPhysicsObject()
            if IsValid(phys) then
                phys:SetVelocity(vthrow)
            end
            --  health:SetModel( "models/props_lab/cactus.mdl")
            health:SetStoredHealth(healttt)
            health.setOwner = self.Owner

            self.healththing = health:EntIndex()

            self.Planted = true
        end
    end

    self.Weapon:EmitSound(throwsound)
end