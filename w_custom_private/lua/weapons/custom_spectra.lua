SWEP.Base = "gb_camo_base"
SWEP.ViewModelFOV	= 54
SWEP.HoldType = "ar2"
SWEP.PrintName = "Spectra"
SWEP.ViewModel		= "models/weapons/cstrike/c_smg_tmp.mdl"
SWEP.WorldModel		= "models/weapons/w_smg_tmp.mdl"
SWEP.AutoSpawnable = false
SWEP.CanDecapitate= true
SWEP.HasFireBullet = true

SWEP.HoldType = "smg"
SWEP.Slot = 10

if CLIENT then
    SWEP.Author = "GreenBlack"
    SWEP.Icon = "vgui/ttt_fun_killicons/tmp.png"
end

SWEP.Primary.Damage      = 71
SWEP.Primary.Delay       = 0.7
SWEP.Primary.Cone        = 0.0001
SWEP.Primary.ClipSize    = 250
SWEP.Primary.ClipMax     = 250
SWEP.Primary.DefaultClip = 500
SWEP.Primary.Automatic   = true
SWEP.Primary.Ammo        = "smg1"
SWEP.Primary.Recoil      = 1.7
SWEP.AutoSpawnable      = true
SWEP.Kind = 11
SWEP.Slot = 10
SWEP.Camo = 999

SWEP.AmmoEnt = "item_ammo_smg1_ttt"

SWEP.IsSilent = true

SWEP.Primary.Sound = Sound( 'weapons/dsr-50/single.mp3')
SWEP.Primary.SoundLevel = 50

SWEP.IronSightsPos = Vector( -6.9, -8, 2.6091 )
SWEP.IronSightsAng = Vector( 0, 0, 0 )

function SWEP:SecondaryAttack()
    self:Fly()
end

function SWEP:PreDrop(yes)
    if yes then self:Remove() end
end

function SWEP:Deploy()
    self:SetNWString("shootmode","Snip")
    local owner = self.Owner
    hook.Add("PlayerDeath", "phoenixDiesdf"..self.Owner:SteamID(),function( victim, weapon, killer )
        if IsValid(owner) and not owner:IsGhost() and SERVER and IsValid(killer) and killer == owner and owner:Health() < 150 then
            owner:SetHealth(owner:Health() +25)
            if owner:Health() > 150 then
                owner:SetHealth(150)
                end
        end
    end)
end

SWEP.NextFreeze = 0
SWEP.tracerColor = Color(255,0,0,255)
SWEP.LaserColor = Color(110,88,20,255)
function SWEP:PrimaryAttack()

    self.Weapon:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )

    if self.Owner:KeyDown(IN_USE)then
        if self.Rockets < 1 then
            self.Owner:PrintMessage( HUD_PRINTCENTER, "Out of rockets" )
            return
        end

        self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay + 1)
        self:FireRocket()
        self:SetClip1( self:Clip1() -1 )
        self.Weapon:SendWeaponAnim( ACT_VM_SECONDARYATTACK)
        self.Owner:SetAnimation( PLAYER_ATTACK1 )
        self.Weapon:EmitSound("weapons/grenade_launcher1.wav", 500, 80, 1, 0)
        self.Weapon:SetNWFloat( "LastShootTime", CurTime() )
        return
    end

    local mode = self:GetNWString("shootmode")
    if mode == "Snip" then

        if not self:CanPrimaryAttack() then return end

        if not worldsnd then
            self.Weapon:EmitSound( self.Primary.Sound, self.Primary.SoundLevel )
        elseif SERVER then
            sound.Play(self.Primary.Sound, self:GetPos(), self.Primary.SoundLevel)
        end

        self:ShootBullet( self.Primary.Damage, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )
        self:Freeze()
        self:Poison()
      --  self:Disorientate()
        self:Blind()
        self:TakePrimaryAmmo( 1 )

        local owner = self.Owner
        if not IsValid(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

        owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )
    end

    if mode == "Orange Laser" then
        if not self:CanPrimaryAttack() then return end
        self.Weapon:SetNextPrimaryFire( CurTime() + 0.3 )
        if not worldsnd then
            self.Weapon:EmitSound( Sound("jackal_gun_shot"), self.Primary.SoundLevel )
        elseif SERVER then
            sound.Play(Sound("jackal_gun_shot"), self:GetPos(), self.Primary.SoundLevel)
        end

        self:ShootBullet( 45, self.Primary.Recoil,1, self:GetPrimaryCone() )
        self:Freeze()
        self:Poison()
        --  self:Disorientate()
        self:Blind()
        self:ShootTracer("LaserTracer_thick")
        self:TakePrimaryAmmo( 1 )

        local owner = self.Owner
        if not IsValid(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

        owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )

    end
end

SWEP.Rockets = 20
function SWEP:FireRocket()
    local aim = self.Owner:GetAimVector()
    local side = aim:Cross(Vector(0,0,1))
    local up = side:Cross(aim)
    local pos = self.Owner:GetShootPos() + side * 10 + up * 0


    self.Rockets = self.Rockets - 1
    if SERVER then

        self.Owner:PrintMessage( HUD_PRINTCENTER, "Rockets left: "..self.Rockets )
        local rocket = ents.Create("shot_nade")
        if !rocket:IsValid() then return false end
        rocket:SetAngles(self.Owner:GetAimVector():Angle(90,90,0))
        rocket:SetPos(pos)
        rocket.damage = 120
        rocket:SetOwner(self.Owner)
        rocket:Spawn()
        rocket.Owner = self.Owner
        rocket:Activate()
        eyes = self.Owner:EyeAngles()
        local phys = rocket:GetPhysicsObject()
        phys:SetVelocity(self.Owner:GetAimVector() * 1500)
    end
    if SERVER and !self.Owner:IsNPC() then
    local anglo = Angle(-3, 0, 0)
    self.Owner:ViewPunch(anglo)
    end

end

function SWEP:Poison()
    owner = self.Owner
    local trace = self.Owner:GetEyeTrace()

        local guy = trace.Entity
    if not guy:IsPlayer() then return end
    timer.Create(guy:EntIndex().."poison",2,5,function()
        if not IsValid(guy) or not IsValid(owner) then return end

        if SERVER and guy != owner  then
        guy:EmitSound("physics/plastic/plastic_barrel_impact_soft5.wav")
        guy.nextcharnobil = CurTime() + 5
        local dmginfo = DamageInfo()
        dmginfo:SetDamage( 3 ) --50 damage
        dmginfo:SetDamageType( DMG_POISON ) --Bullet damage
        if IsValid(owner) then
            dmginfo:SetAttacker(owner ) --First player found gets credit
        else
            dmginfo:SetAttacker( guy )
        end
        dmginfo:SetDamageForce( Vector( 0, 0, 25 ) )
        guy:TakeDamageInfo( dmginfo )
        end
    end)
end

SWEP.NextReload = 0
function SWEP:Reload()
    if self.Owner:KeyDown(IN_ATTACK) then
        return
    end
    if self.Owner:KeyDown(IN_USE) and self.NextReload < CurTime() then
        self.NextReload = CurTime() + 0.3
        local mode = self:GetNWString("shootmode")
        self:SetIronsights(false)

        if mode == "Snip" then  self:SetNWString("shootmode","Orange Laser")  end
        if mode == "Orange Laser" then  self:SetNWString("shootmode","Snip") end

        self.Weapon:SetNextPrimaryFire( CurTime() + 0.8 )
        return
    end


end

if CLIENT then
    local scope = surface.GetTextureID("sprites/scope")
    function SWEP:DrawHUD( )
        local shotttext = "Shootmode: "..self:GetNWString("shootmode").."\nE+R to change"
        surface.SetFont( "ChatFont" );
        local size_x, size_y = surface.GetTextSize( shotttext );

        draw.RoundedBox( 6, ScrW( ) / 2 - size_x / 2 - 5, ScrH( ) - 100, size_x + 15, size_y + 10, Color( 0, 0, 0, 150 ) );
        draw.DrawText( shotttext , "ChatFont", ScrW( ) / 2, ScrH( ) - 100 + 5, Color( 255, 30, 30, 255 ), TEXT_ALIGN_CENTER );
        self.BaseClass.DrawHUD(self)
    end
end