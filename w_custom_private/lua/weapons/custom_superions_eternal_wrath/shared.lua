SWEP.Base = "aa_base"
SWEP.HoldType = "pistol"
SWEP.ViewModelFOV = 70.314960629921
SWEP.ViewModelFlip = false
SWEP.ViewModel = "models/weapons/cstrike/c_pist_deagle.mdl"
SWEP.WorldModel = "models/weapons/w_pistol.mdl"
SWEP.ShowViewModel = true
SWEP.ShowWorldModel = false

SWEP.UseHands = true
SWEP.Kind = WEAPON_EQUIP2

SWEP.ViewModelBoneMods = {
    ["v_weapon.Deagle_Parent"] = { scale = Vector(0.009, 0.009, 0.009), pos = Vector(0, 0, 0), angle = Angle(0, 0, 0) }
}

SWEP.WElements = {
    ["banannaaa"] = { type = "Model", model = "models/props/cs_italy/bananna.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(8.635, 0.455, -4.092), angle = Angle(3.068, -88.977, 0), size = Vector(1.116, 1.116, 1.116), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
}

SWEP.VElements = {
    ["banana"] = { type = "Model", model = "models/props/cs_italy/bananna.mdl", bone = "v_weapon.Deagle_Parent", rel = "", pos = Vector(0.455, -3.182, -0.456), angle = Angle(166.705, 0, 78.75), size = Vector(0.862, 0.862, 0.862), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
}

SWEP.Sound = { Sound("weapons/nanners/4.mp3"), Sound("weapons/nanners/5.mp3") }
SWEP.Sounds = { Sound("weapons/nanners/8.mp3"), Sound("weapons/nanners/10.mp3"),  Sound("weapons/nanners/13.mp3"),  Sound("weapons/nanners/14.mp3"),  Sound("weapons/nanners/17.mp3"),  Sound("weapons/nanners/18.mp3")}

SWEP.PrintName            =  "Banana Fight!"
SWEP.Slot                = 6
SWEP.SlotPos            = 1
SWEP.DrawAmmo            = true
SWEP.DrawCrosshair        = true
SWEP.Weight                = 5
SWEP.AutoSwitchTo        = false
SWEP.AutoSwitchFrom        = false
SWEP.Author            = ""
SWEP.Contact        = ""
SWEP.Purpose        = ""


SWEP.AutoSpawnable      = false
SWEP.Primary.Ammo = "Buckshot"
SWEP.Primary.Damage = 95
SWEP.Primary.Cone = 0.001
SWEP.Primary.Delay = 0.6
SWEP.Primary.ClipSize = 120
SWEP.Primary.ClipMax = 120
SWEP.Primary.DefaultClip = 120
SWEP.Primary.Automatic = false
SWEP.Primary.NumShots = 1
SWEP.HeadshotMultiplier = 0.6
SWEP.Primary.Automatic        = true
SWEP.Primary.Ammo            = "XBowBolt"
SWEP.Secondary.Delay            = 3
SWEP.Secondary.ClipSize        = 3
SWEP.Secondary.DefaultClip    = 3
SWEP.Secondary.Automatic    = false
SWEP.Secondary.Ammo            = "XBowBolt"
SWEP.AmmoEnt = ""
SWEP.Recoil = 0

SWEP.SecondaryProps = {
    "models/props_c17/oildrum001_explosive.mdl",
    "models/Combine_Helicopter/helicopter_bomb01.mdl",
    "models/props_junk/TrafficCone001a.mdl",
    "models/props_junk/metal_paintcan001a.mdl",
    "models/Gibs/HGIBS.mdl",
    "models/Gibs/Fast_Zombie_Legs.mdl",
    "models/props_c17/doll01.mdl",
    "models/props_lab/huladoll.mdl",
    "models/props_lab/huladoll.mdl",
    "models/props_lab/desklamp01.mdl",
    "models/props_junk/garbage_glassbottle003a.mdl",
    "models/weapons/w_shot_gb1014.mdl",

}
SWEP.Icon = "vgui/ttt_fun_killicons/nanners_gun.png"


function SWEP:PrimaryAttack(worldsnd)

    if self.Owner:KeyDown(IN_USE) then
        self:Fly()
        return
    end

    self.Weapon:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )


    if not self:CanPrimaryAttack() then return end

    if not worldsnd then
        self.Weapon:EmitSound( self.Primary.Sound, self.Primary.SoundLevel )
    elseif SERVER then
        sound.Play(self.Primary.Sound, self:GetPos(), self.Primary.SoundLevel)
    end

    self:ShootBullet( self.Primary.Damage, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )

    self:TakePrimaryAmmo( 1 )

    local owner = self.Owner
    if not IsValid(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end


    local tr = self.Owner:GetEyeTrace()
    local effectdata = EffectData()
    effectdata:SetOrigin(tr.HitPos)
    effectdata:SetNormal(tr.HitNormal)
    effectdata:SetScale(1)
    -- util.Effect("effect_mad_ignition", effectdata)
    util.Decal("FadingScorch", tr.HitPos + tr.HitNormal, tr.HitPos - tr.HitNormal)

    local tracedata = {}
    tracedata.start = tr.HitPos
    tracedata.endpos = Vector(tr.HitPos.x, tr.HitPos.y, tr.HitPos.z - 10)
    tracedata.filter = tr.HitPos
    local tracedata = util.TraceLine(tracedata)
    if SERVER then
        hitEnt = tr.Entity
        if IsValid(hitEnt) and (hitEnt:IsPlayer()) and hitEnt:Health() < 25 and hitEnt:Alive() then

            self.deathsound = CreateSound(self.Owner, "dubstep.s1" )
            self.deathsound:Play() -- starts the sound
            timer.Simple(15,function()
                if self.deathsound then
                    self.deathsound:Stop()
                end
            end)
        end

        if tracedata.HitWorld then
            local flame = ents.Create("env_fire");
            flame:SetPos(tr.HitPos + Vector( 0, 0, 1 ));
            flame:SetKeyValue("firesize", "10");
            flame:SetKeyValue("fireattack", "10");
            flame:SetKeyValue("StartDisabled", "0");
            flame:SetKeyValue("health", "10");
            flame:SetKeyValue("firetype", "0");
            flame:SetKeyValue("damagescale", "5");
            flame:SetKeyValue("spawnflags", "128");
            flame:SetPhysicsAttacker(self.Owner)
            flame:SetOwner( self.Owner )
            flame:Spawn();
            flame:Fire("StartFire",0);
        end
    end


    owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )
end





SWEP.Headcrabs = 0
SWEP.Headcrabs2 = 0
SWEP.NextSecond = 0

function SWEP:Reload()
    if  self.NextSecond > CurTime() then return end

    if SERVER then
        self.Owner:EmitSound( Sound("weapons/knives/hitwall.wav") )
        self.Owner:EmitSound( "weapons/nanners/10.mp3" )
    end
    self.Weapon:SetNextSecondaryFire( CurTime() + 5 )

    self.NextSecond = CurTime() + 3

    if !self.Throwing && !self.StartThrow then
    self.StartThrow = true

    self.Weapon:SendWeaponAnim(ACT_VM_PULLBACK_HIGH)
    if IsValid(self.Owner:GetViewModel()) then
        self.Weapon:SetNextPrimaryFire(CurTime() + self.Owner:GetViewModel():SequenceDuration())
        self.Weapon:SetNextSecondaryFire(CurTime() + self.Owner:GetViewModel():SequenceDuration())
        self.NextThrow = CurTime() + 0.05 --self.Owner:GetViewModel():SequenceDuration()
    end
    end
end

SWEP.NextHeal = 0

function SWEP:Think()



    if self.Owner:KeyDown(IN_RELOAD) then
        self.ThrowVel = 2400
        self.Damage = 70
        self.Tossed = true
    end

    if self.StartThrow && !self.Owner:KeyDown(IN_RELOAD) && !self.Owner:KeyDown(IN_RELOAD) && self.NextThrow < CurTime() then

    self.StartThrow = false
    self.Throwing = true
    if self.Tossed then
        self.Weapon:SendWeaponAnim(ACT_VM_SECONDARYATTACK)
    else
        self.Weapon:SendWeaponAnim(ACT_VM_THROW)
    end
    self.Owner:SetAnimation(PLAYER_ATTACK1)
    self:CreateGrenade(self.Damage)
    self:TakePrimaryAmmo(1)
    self.NextAnimation = CurTime() + self.Primary.Delay
    self.ResetThrow = true

    end

    if self.Throwing && self.ResetThrow && self.NextAnimation < CurTime() then

    if self.Owner:GetAmmoCount(self.Primary.Ammo) > 0 && self:Clip1() <= 0 then

    self.Owner:RemoveAmmo(1, self.Primary.Ammo)
    self:SetClip1(self:Clip1()+1)
    self.Weapon:SendWeaponAnim(ACT_VM_DRAW)
    if IsValid(self.Owner:GetViewModel()) then
        self.NextThrow = CurTime() + 0.01
        self.Weapon:SetNextPrimaryFire(CurTime() + self.Owner:GetViewModel():SequenceDuration())
        self.Weapon:SetNextSecondaryFire(CurTime() + self.Owner:GetViewModel():SequenceDuration())
    end

    else
        -- self.Owner:ConCommand("lastinv")
    end

    self.ResetThrow = false
    self.Throwing = false

    end

    if not SERVER or self.NextHeal > CurTime() or not IsValid(self.Owner) then return end
    self.NextHeal = CurTime() + 2

    if self.Owner:Health() < 175 then
        if SERVER then autohealfunction(self.Owner) end
    end

end

function SWEP:throw_attack (model_file, sound, away)
    local tr = self.Owner:GetEyeTrace();

    if IsValid( tr.Entity )  then
        if not tr.Entity.hasProtectionSuit then
            tr.Entity.hasProtectionSuit= true
            timer.Simple(5, function()
                if not IsValid( tr.Entity )  then return end
                tr.Entity.hasProtectionSuit = false
            end)
        end
    end

    self.BaseClass.ShootEffects (self);
    if (!SERVER) then return end;
    local ent = ents.Create ("prop_physics");
    ent:SetModel (model_file);
    ent:SetPos (self.Owner:EyePos() + (self.Owner:GetAimVector() * 16));
    ent:SetAngles (self.Owner:EyeAngles());
    ent:SetPhysicsAttacker(self.Owner)
    ent:Spawn();
    ent:EmitSound( sound )
    if math.Rand(1,5) == 3 then
        ent:Ignite(100,100)
    end
    local phys = ent:GetPhysicsObject();
    local shot_length = tr.HitPos:Length();
    phys:ApplyForceCenter (self.Owner:GetAimVector():GetNormalized() * math.pow (shot_length, 3));

    timer.Simple(away,function()
        if IsValid(ent) then
            ent:Remove()
        end
    end)

    cleanup.Add (self.Owner, "props", ent);
    undo.Create ("Thrown model");
    undo.AddEntity (ent);
    undo.SetPlayer (self.Owner);
    undo.Finish();
end

SWEP.knifes = 0
SWEP.deathsound = false

function SWEP:SecondaryAttack()

    if self.Owner:KeyDown(IN_ATTACK) then
        return
    end

    self.Weapon:SetNextSecondaryFire( CurTime() + 3 )

    if (self:Clip1() <= 0 or self.knifes > 4 ) then
        self:StabSecond()
        return end



    self.Owner:LagCompensation(true)

    local spos = self.Owner:GetShootPos()
    local sdest = spos + (self.Owner:GetAimVector() * 7000000000)

    self.Owner:EmitSound( Sound("weapons/knives/hitwall.wav") )




    local kmins = Vector(1,1,1) * -10000000
    local kmaxs = Vector(1,1,1) * 10000000

    local tr = util.TraceHull({start=spos, endpos=sdest, filter=self.Owner, mask=MASK_SHOT_HULL, mins=kmins, maxs=kmaxs})

    -- Hull might hit environment stuff that line does not hit
    if not IsValid(tr.Entity) then
        tr = util.TraceLine({start=spos, endpos=sdest, filter=self.Owner, mask=MASK_SHOT_HULL})
    end

    local hitEnt = tr.Entity

    -- effects
    if IsValid(hitEnt) then
        self.knifes = self.knifes + 1
        self:TakePrimaryAmmo(1)
        self:SendWeaponAnim(ACT_VM_PRIMARYATTACK)
        self.Owner:SetAnimation(PLAYER_ATTACK1)

        local edata = EffectData()
        edata:SetStart(spos)
        edata:SetOrigin(tr.HitPos)
        edata:SetNormal(tr.Normal)
        edata:SetEntity(hitEnt)

        if (hitEnt:IsPlayer() or hitEnt:IsNPC() or hitEnt:GetClass() == "prop_ragdoll") and hitEnt:Health() < 96 then
            util.Effect("BloodImpact", edata)
            self.deathsound = CreateSound(self.Owner, "dubstep.s1" )
            self.deathsound:Play() -- starts the sound
            timer.Simple(15,function()
                if self.deathsound then
                    self.deathsound:Stop()
                end
            end)
        end
    else
        self.Weapon:SendWeaponAnim( ACT_VM_MISSCENTER )
        self.Weapon:SetNextSecondaryFire( CurTime() + 1 )
    end

    if SERVER then
        self.Owner:SetAnimation( PLAYER_ATTACK1 )
    end



    if SERVER and tr.Hit and tr.HitNonWorld and IsValid(hitEnt) then
        if hitEnt:IsPlayer() or hitEnt:IsNPC()  then
            -- knife damage is never karma'd, so don't need to take that into
            -- account we do want to avoid rounding error strangeness caused by
            -- other damage scaling, causing a death when we don't expect one, so
            -- when the target's health is close to kill-point we just kill

            local dmg = DamageInfo()
            dmg:SetDamage( 75 )
            dmg:SetAttacker(self.Owner)
            dmg:SetInflictor(self.Weapon or self)
            dmg:SetDamageForce(self.Owner:GetAimVector() * 5)
            dmg:SetDamagePosition(self.Owner:GetPos())
            dmg:SetDamageType(DMG_SLASH)

            hitEnt:DispatchTraceAttack(dmg, spos + (self.Owner:GetAimVector() * 3), sdest)

        end
    end

    self.Owner:LagCompensation(false)
end

function SWEP:StabSecond()
    self.Weapon:SetNextPrimaryFire( CurTime() + 1 )
    self.Weapon:SetNextSecondaryFire( CurTime() + 1 )

    self.Owner:LagCompensation(true)



    local spos = self.Owner:GetShootPos()
    local sdest = spos + (self.Owner:GetAimVector() * 80)

    local kmins = Vector(1,1,1) * -10
    local kmaxs = Vector(1,1,1) * 10

    local tr = util.TraceHull({start=spos, endpos=sdest, filter=self.Owner, mask=MASK_SHOT_HULL, mins=kmins, maxs=kmaxs})

    -- Hull might hit environment stuff that line does not hit
    if not IsValid(tr.Entity) then
        tr = util.TraceLine({start=spos, endpos=sdest, filter=self.Owner, mask=MASK_SHOT_HULL})
    end

    local hitEnt = tr.Entity

    -- effects
    if IsValid(hitEnt) then
        self.Weapon:SendWeaponAnim( ACT_VM_HITCENTER )

        local edata = EffectData()
        edata:SetStart(spos)
        edata:SetOrigin(tr.HitPos)
        edata:SetNormal(tr.Normal)
        edata:SetEntity(hitEnt)

        if hitEnt:IsPlayer() or hitEnt:GetClass() == "prop_ragdoll" then
            util.Effect("BloodImpact", edata)
        end
    else
        self.Weapon:SendWeaponAnim( ACT_VM_MISSCENTER )
        self.Owner:ViewPunch( Angle( rnda,rndb,rnda ) )
    end

    if SERVER then
        self.Owner:SetAnimation( PLAYER_ATTACK1 )
    end


    if SERVER and tr.Hit and tr.HitNonWorld and IsValid(hitEnt) then
        if hitEnt:IsPlayer() then
            if self.Owner:Health() < 175 then
                self.Owner:SetHealth(self.Owner:Health()+10)
            end
            -- knife damage is never karma'd, so don't need to take that into
            -- account we do want to avoid rounding error strangeness caused by
            -- other damage scaling, causing a death when we don't expect one, so
            -- when the target's health is close to kill-point we just kill

            local dmg = DamageInfo()
            dmg:SetDamage(25)
            dmg:SetAttacker(self.Owner)
            dmg:SetInflictor(self.Weapon or self)
            dmg:SetDamageForce(self.Owner:GetAimVector() * 5)
            dmg:SetDamagePosition(self.Owner:GetPos())
            dmg:SetDamageType(DMG_SLASH)

            hitEnt:DispatchTraceAttack(dmg, spos + (self.Owner:GetAimVector() * 3), sdest)

        end
    end
    local owner = self.Owner
    self.Owner:LagCompensation(false)
    if not IsValid(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end


    self.Owner:SetFOV( 100, 0.2 );
    timer.Simple(0.3, function() if IsValid(self) and IsValid(self.Owner) then self.Owner:SetFOV( 0, 0.5 ) end end)

    self.Owner:SetAnimation(ACT_RELOAD_PISTOL)
    self.Weapon:SendWeaponAnim(ACT_SHOTGUN_RELOAD_FINISH)
    owner:ViewPunch( Angle( -10, 0, 0 ) )
end


function SWEP:Deploy()

    if game.SinglePlayer() then
        self:CallOnClient("Deploy", "")
    end

    self.StartThrow = false
    self.Throwing = false
    self.ResetThrow = false

    if !self.Throwing then

    self.Weapon:SendWeaponAnim(ACT_VM_DRAW)
    if IsValid(self.Owner:GetViewModel()) then
        self.Weapon:SetNextPrimaryFire(CurTime() + self.Owner:GetViewModel():SequenceDuration())
        self.Weapon:SetNextSecondaryFire(CurTime() + self.Owner:GetViewModel():SequenceDuration())
        self.NextThrow = CurTime() + 0.01
    end

    if self.Owner:GetAmmoCount(self.Primary.Ammo) > 0 && self:Clip1() <= 0 then
    self.Owner:RemoveAmmo(1, self.Primary.Ammo)
    self:SetClip1(self:Clip1()+1)
    end

    end

    return true

end


function SWEP:CreateGrenade( damage )

    if IsValid(self.Owner) && IsValid(self.Weapon) then

    if (SERVER) then

        local ent = ents.Create("wrathbanana")
        if !ent then return end
        ent.Owner = self.Owner
        ent.Inflictor = self.Weapon
        ent.Damage = self.Damage
        ent:SetOwner(self.Owner)
        local eyeang = self.Owner:GetAimVector():Angle()
        local right = eyeang:Right()
        local up = eyeang:Up()
        if self.Tossed then
            ent:SetPos(self.Owner:GetShootPos()+right*4-up*4)
        else
            ent:SetPos(self.Owner:GetShootPos()+right*4+up*4)
        end
        ent:SetAngles(self.Owner:GetAngles())
        ent:SetPhysicsAttacker(self.Owner)
        ent:Spawn()

        local phys = ent:GetPhysicsObject()
        if phys:IsValid() then
            phys:SetVelocity(self.Owner:GetAimVector() * 2800 )
        end

    end

    end

end


local ENT = {}

ENT.Type = "anim"
ENT.Base = "base_anim"

if SERVER then

    function ENT:Initialize()

        self.Hit = false
        self.LastShout = CurTime()
        self.CurrentPitch = 100
        self.SpawnDelay = CurTime() + 0.01
        --damage self.Damage = 75
        self:SetModel("models/props/cs_italy/bananna.mdl")
        self:PhysicsInitBox(Vector(-30,-30,-30),Vector(30,30,30))
        self:SetMoveType(MOVETYPE_VPHYSICS)
        self:SetSolid(SOLID_VPHYSICS)
        self:SetTrigger(true)

        self:SetModelScale(self:GetModelScale()*1,0)
        --self:SetColor(0,200,255,255)
        self:DrawShadow(false)


        local phys = self:GetPhysicsObject()
        if (phys:IsValid()) then
            phys:Wake()
            phys:EnableDrag(false)
            phys:AddGameFlag(FVPHYSICS_NO_IMPACT_DMG)
            phys:SetBuoyancyRatio(0)
        end



        self:Fire("kill", 1, 60)

    end

    function ENT:Think()




        if self.Hit && self.Splodetimer && self.Splodetimer < CurTime() then

        self:EmitSound("weapons/explode"..math.random(3,5)..".wav",90,85)
        local ent = ents.Create( "env_explosion" )

        ent:SetPos( self:GetPos()  )
        ent:SetOwner( self.Owner  )
        ent:SetPhysicsAttacker(  self.Owner )
        ent:Spawn()
        ent:SetKeyValue( "iMagnitude", "30" )
        ent:Fire( "Explode", 0, 0 )

        util.BlastDamage( self, self:GetOwner(), self:GetPos(), 35, 75 )

        ent:EmitSound( "weapons/big_explosion.mp3" )
        self:Remove()

        end

    end


    function ENT:PhysicsUpdate(phys)
        if !self.Hit then
        self:SetLocalAngles(phys:GetVelocity():Angle())
        else
            phys:SetVelocity(Vector(phys:GetVelocity().x*0.95,phys:GetVelocity().y*0.95,phys:GetVelocity().z))
        end
    end

    function ENT:Touch(ent)
        if IsValid(ent) && !self.Stuck    then
        if ent:IsNPC() || (ent:IsPlayer() && ent != self:GetOwner())  || ent:IsVehicle() then
        self:SetSolid(SOLID_NONE)
        self:SetMoveType(MOVETYPE_NONE)
        self:SetParent(ent)
        if !self.Splodetimer then
        self.Splodetimer = CurTime() + 2
        end
        self.Stuck = true
        self.Hit = true
        end
        end
    end

    function ENT:PhysicsCollide(data,phys)
        if self:IsValid() && !self.Hit then
        self:SetCollisionGroup(COLLISION_GROUP_DEBRIS)
        if !self.Splodetimer then
        self.Splodetimer = CurTime() + 2
        end
        self.Hit = true
        end
    end

end

scripted_ents.Register( ENT, "wrathbanana", true )