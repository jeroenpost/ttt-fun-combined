SWEP.ViewModel = "models/Teh_Maestro/popcorn.mdl"
SWEP.WorldModel = "models/Teh_Maestro/popcorn.mdl"
SWEP.Spawnable = true
SWEP.AdminSpawnable = false
SWEP.ViewModelFlip = false
SWEP.ViewModelFOV = 80
SWEP.AdminSpawnable = true
SWEP.AutoSpawnable = false

SWEP.Base = "aa_base"
SWEP.Kind = 19

SWEP.Author = "Antimony51"
SWEP.Instructions = "Left Click: Eat Popcorn\nRight Click: Throw Bucket"

SWEP.Primary.Clipsize = -1
SWEP.Primary.DefaultClip = -1
SWEP.Primary.Automatic = false
SWEP.Primary.Ammo = "none"

SWEP.Secondary.Clipsize = -1
SWEP.Secondary.DefaultClip = -1
SWEP.Secondary.Automatic = false
SWEP.Secondary.Ammo = "none"

SWEP.DrawAmmo = false
SWEP.Slot = 1
SWEP.SlotPos = 1
SWEP.HoldType = "shotgun"

SWEP.PrintName = "Can I haz butterz"
SWEP.Slot = 8
SWEP.SlotPos = 1

function SWEP:Deploy()
end

function SWEP:Think()
end

function SWEP:Initialize()
    util.PrecacheSound("crisps/eat.mp3")
end

function SWEP:Ondrop()
    self:Remove()
end

SWEP.nextP = 0

function SWEP:PrimaryAttack()
    if self.nextP > CurTime() then return end
    self.nextP = CurTime() + 3.5
    if SERVER then
        local sound = CreateSound(self.Owner, "crisps/eat.mp3")
        sound:Play()
        sound:ChangeVolume(0.2, 0)
        if self.Owner:Health() < 175 then
            self.Owner:SetHealth(self.Owner:Health() + 10)
        end
    end
   -- self.Weapon:SetNextPrimaryFire(CurTime() + 3.5)
end


SWEP.flyammo = 10

function SWEP:SecondaryAttack()

    if self.Owner:KeyDown(IN_USE) then
        local bucket, att, phys, tr

        self.Weapon:SetNextSecondaryFire(CurTime() + 1)
        if (self.Owner:IsAdmin()) then
            self.Weapon:SetNextSecondaryFire(CurTime())
        end

        if CLIENT then
            return
        end

        self.Owner:EmitSound("weapons/slam/throw.wav")

        self.Owner:ViewPunch(Angle(math.Rand(-8, 8), math.Rand(-8, 8), 0))

        bucket = ents.Create("sent_popcorn_thrown")
        bucket:SetOwner(self.Owner)
        bucket:SetPos(self.Owner:GetShootPos())
        bucket:Spawn()
        bucket:Activate()

        phys = bucket:GetPhysicsObject()

        if IsValid(phys) then
            phys:SetVelocity(self.Owner:GetPhysicsObject():GetVelocity())
            phys:AddVelocity(self.Owner:GetAimVector() * 128 * phys:GetMass())
            phys:AddAngleVelocity(VectorRand() * 128 * phys:GetMass())
        end

        --[[
        if !self.Owner:IsAdmin() then
            self.Owner:StripWeapon("weapon_popcorn")
        end
        --]]


    else

       self:Fly()
    end
end

SWEP.nextR = 0

function SWEP:Reload(worldsnd)

    if self.nextR > CurTime() then return end
    self.nextR = CurTime() + 1.5


    if SERVER then
        local tr = self.Owner:GetEyeTrace()
        local ent = ents.Create( "env_explosion" )

        ent:SetPos( tr.HitPos  )
        ent:SetOwner( self.Owner  )
        ent:SetPhysicsAttacker(  self.Owner )
        ent:Spawn()
        ent:SetKeyValue( "iMagnitude", "40" )
        ent:Fire( "Explode", 0, 0 )

        util.BlastDamage( self, self:GetOwner(), self:GetPos(), 50, 75 )

        ent:EmitSound( "weapons/big_explosion.mp3" )
    end

    self.Owner:SetAnimation( PLAYER_ATTACK1 )
    local owner = self.Owner
    if not IsValid(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

    owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )
end