SWEP.HoldType           = "knife"
SWEP.PrintName          = "Harry Potter's Cloak"
SWEP.Slot               = 7

SWEP.Icon = "vgui/ttt_fun_killicons/g3sg1.png"
SWEP.Base               = "gb_camo_base"
SWEP.Spawnable = true
SWEP.AdminSpawnable = true
SWEP.Kind = WEAPON_ROLE
SWEP.WeaponID = AMMO_RIFLE
SWEP.Primary.Delay          = 1
SWEP.Primary.Recoil         = 4
SWEP.Primary.Automatic = true
SWEP.Primary.Ammo = "357"
SWEP.Primary.Damage = 0
SWEP.Primary.Cone = 0.006
SWEP.Primary.ClipSize = 20
SWEP.Primary.ClipMax = 20 -- keep mirrored to ammo
SWEP.Primary.DefaultClip = 40
SWEP.HeadshotMultiplier = 1
SWEP.IsSilent = false
SWEP.AutoSpawnable      = false
SWEP.AmmoEnt = "item_ammo_357_ttt"
SWEP.ViewModel		= "models/weapons/c_stunstick.mdl"
SWEP.WorldModel		= "models/weapons/w_stunbaton.mdl"
SWEP.Primary.Sound = Sound ("Weapon_M4A1.Silenced")
SWEP.CanDecapitate = true
SWEP.Camo = 43

SWEP.Secondary.Sound = Sound("Default.Zoom")

function SWEP:Deploy()
    self:DeployFunction()
    self.NextAmmoFill = CurTime() + 60
end
SWEP.NextReload = 0
function SWEP:Think()



        if self.Owner:KeyDown(IN_USE) and self.Owner:KeyDown(IN_WALK) and self.NextReload < CurTime() then
            self.NextReload = CurTime() + 30
            local soundfile = gb_config.websounds.."swiggity_swag.mp3"
            gb_PlaySoundFromServer(soundfile, self.Owner)
        end

    if CLIENT or self.NextThinky and self.NextThinky > CurTime() then return end
    self.NextThinky = CurTime() + 1;

    if self.NextAmmoFill and self.NextAmmoFill < CurTime() then
        self:SetClip1( 20 );
        self.NextAmmoFill = CurTime() + 60
    end

    if self.Activated then
        local clip = self:Clip1()

        if clip < 1 then
            self.Activated = false
            self.NextAmmoFill = CurTime() + 60
            self.Owner:PrintMessage( HUD_PRINTCENTER, "Cloak deactivated" )
            return
        end
        self:SetClip1( clip - 1 );
        if clip == 19 and IsValid(self.Owner) and self.Owner:Health() < 150 then
            self.Owner:SetHealth( self.Owner:Health() + 25)
        end



        local mat = ""
        local name = ""
        if clip > 12 then
            mat = "models/rendertarget"
            name = "Black"
        elseif clip > 0 then
             mat = "Models/effects/vol_light001"
             name = "shinethrough (only shadow visible)"
        else
            mat = ""
            name = "nothing"
            self.Activated = false
        end

        self.Owner:PrintMessage( HUD_PRINTCENTER, "Cloaked with "..name )

        self.Owner:SetMaterial(mat)

    else
        self.Owner:SetMaterial("");
    end

end




-- Add some zoom to ironsights for this gun
function SWEP:PrimaryAttack()
    if self.Owner.IsSpecialPerson then
        self:EmitSound("vo/Citadel/al_fail_no.wav")
        return end
    if self.Weapon:GetNextSecondaryFire() > CurTime() then return end
    self.Activated = true
    self:EmitSound("ambient/energy/zap"..math.random(5,9)..".wav")
    if SERVER then self.Owner:SetNWFloat("w_stamina",1) end
    self.Weapon:SetNextSecondaryFire( CurTime() + 1)
end

-- Add some zoom to ironsights for this gun
function SWEP:SecondaryAttack()

    if self.Weapon:GetNextSecondaryFire() > CurTime() then return end
    self.Activated = false
        self:EmitSound("ambient/energy/zap"..math.random(5,9)..".wav")
    self.Owner:PrintMessage( HUD_PRINTCENTER, "Cloak deactivated" )
    gb_PlaySoundFromServer(gb_config.websounds.."wazaap.mp3", self.Owner)
    self.Weapon:SetNextSecondaryFire( CurTime() + 1)
end
