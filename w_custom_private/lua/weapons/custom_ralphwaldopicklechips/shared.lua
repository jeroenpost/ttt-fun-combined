if SERVER then
   AddCSLuaFile(  )
   --resource.AddFile("materials/vgui/ttt_fun_killicons/m3.png")
end

SWEP.HoldType = "shotgun"
SWEP.PrintName = "The RalphWaldoPickleChips"
SWEP.Slot = 2
if CLIENT then
   
   SWEP.Author = "GreenBlack"
   
   SWEP.Icon = "vgui/ttt_fun_killicons/m3.png"
end

SWEP.Base = "aa_base"
SWEP.Spawnable = true
SWEP.AdminSpawnable = true
SWEP.Kind = WEAPON_HEAVY
SWEP.WeaponID = AMMO_SHOTGUN

SWEP.Primary.Ammo = "Buckshot"
SWEP.Primary.Damage = 5
SWEP.Primary.Cone = 0.085

SWEP.Primary.Delay = 0.75

SWEP.Primary.ClipSize = 16
SWEP.Primary.ClipMax = 24
SWEP.Primary.DefaultClip = 16
SWEP.Primary.Automatic = false
SWEP.Primary.NumShots = 18
SWEP.HeadshotMultiplier = 1
SWEP.AutoSpawnable = false
SWEP.AmmoEnt = "item_box_buckshot_ttt"
SWEP.ViewModelFOV = 70
SWEP.ViewModelFlip = true
SWEP.ViewModel  = "models/weapons/v_snip_awp.mdl"
SWEP.WorldModel = "models/weapons/w_snip_awp.mdl"
SWEP.Primary.Sound = Sound( "Weapon_M3.Single" )
SWEP.Primary.Recoil = 7
SWEP.reloadtimer = 0

SWEP.IronSightsPos = Vector(5.74, -2.19, 3.28)
SWEP.IronSightsAng = Vector(0.547, 0, 0)

SWEP.CanDecapitate = true

function SWEP:SecondaryAttack()
    if self.Owner:KeyDown( IN_USE ) then
        self:PowerHit()
        return
    end
    self:Fly()
end

function SWEP:OnDrop()
    self:Remove()
end


function SWEP:GetHeadshotMultiplier(victim, dmginfo)
   local att = dmginfo:GetAttacker()
   if not IsValid(att) then return 3 end

   local dist = victim:GetPos():Distance(att:GetPos())
   local d = math.max(0, dist - 140)
   
   return 1 + math.max(0, (2.1 - 0.002 * (d ^ 1.25)))
end
