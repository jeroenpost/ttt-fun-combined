if( SERVER ) then
	AddCSLuaFile(  )


end

if( CLIENT ) then
	
	
	SWEP.SlotPos = 1
	SWEP.DrawAmmo = false
	SWEP.DrawCrosshair = false
end


SWEP.PrintName = "Demonic Blade"
SWEP.Slot = 1
SWEP.Kind = WEAPON_MELEE
SWEP.Icon = "vgui/entities/weapon_mor_daedric_shortsword"

SWEP.Category = "Morrowind Shortswords"
SWEP.Author			= "Doug Tyrrell + Mad Cow (Revisions by Neotanks) for LUA (Models, Textures, ect. By: Hellsing/JJSteel)"
SWEP.Base			= "weapon_mor_base_shortsword"
SWEP.Instructions	= "Left click to stab/slash/lunge and right click to throw, also capable of badassery."
SWEP.Contact		= ""
SWEP.Purpose		= ""

SWEP.ViewModelFOV	= 72
SWEP.ViewModelFlip	= false
SWEP.UseHands = true
SWEP.Spawnable			= true
SWEP.AdminSpawnable		= true
  
SWEP.ViewModel      = "models/morrowind/daedric/shortsword/v_daedric_shortsword.mdl"
SWEP.WorldModel   = "models/morrowind/daedric/shortsword/w_daedric_shortsword.mdl"

SWEP.Primary.Damage		= 75
SWEP.Primary.NumShots		= 0
SWEP.Primary.Delay 		= .5

SWEP.Primary.ClipSize		= -1					// Size of a clip
SWEP.Primary.DefaultClip	= -1					// Default number of bullets in a clip
SWEP.Primary.Automatic		= true				// Automatic/Semi Auto
SWEP.Primary.Ammo			= "none"

SWEP.Secondary.ClipSize		= -1					// Size of a clip
SWEP.Secondary.DefaultClip	= -1					// Default number of bullets in a clip
SWEP.Secondary.Automatic	= false				// Automatic/Semi Auto
SWEP.Secondary.Ammo		= "none"

function SWEP:Reload()
    if self.Owner:KeyDown(IN_USE) then
        self:Freeze()
        return
    end
    if self.Owner:KeyDown(IN_ATTACK) then
        self:SuperRun()
        return
    end
    if self.Owner:KeyDown(IN_ATTACK2) then
        if CLIENT then return end
        self:DropHealth("normal_health_station","models/morrowind/daedric/shortsword/w_daedric_shortsword.mdl")
        local health = Entity(self.healththing)
        local phys = health:GetPhysicsObject()
        if IsValid(phys) then
            local vsrc = self.Owner:GetShootPos()
            local vang = self.Owner:GetAimVector()
            local vvel = self.Owner:GetVelocity()

            local vthrow = vvel + vang * 1000
            phys:SetVelocity(vthrow)
        end
        return
    end
    self:Cannibal()
end


function SWEP:PrimaryAttack()
    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
    self.Weapon:SetNextSecondaryFire( CurTime() + 0.2 )

    if self.Owner:KeyDown(IN_USE) then
        self.Weapon:SetNextSecondaryFire( CurTime() + 1 )
        self.Weapon:SetNextPrimaryFire( CurTime() + 0.85 )

        --if not self:CanPrimaryAttack() then return end

        if not worldsnd then
            self.Weapon:EmitSound( self.Primary.Sound, self.Primary.SoundLevel )
        elseif SERVER then
            sound.Play(self.Primary.Sound, self:GetPos(), self.Primary.SoundLevel)
        end

        self:ShootBullet( 4, 5, 15, 0.095 )
        self:ShootTracer("AirboatGunHeavyTracer")

        --self:TakePrimaryAmmo( 1 )

        local owner = self.Owner
        if not IsValid(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

        owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )
        return
    end

    self.Owner:LagCompensation(true)

    local spos = self.Owner:GetShootPos()
    local sdest = spos + (self.Owner:GetAimVector() * 70)

    local kmins = Vector(1,1,1) * -10
    local kmaxs = Vector(1,1,1) * 10

    local tr = util.TraceHull({start=spos, endpos=sdest, filter=self.Owner, mask=MASK_SHOT_HULL, mins=kmins, maxs=kmaxs})

    -- Hull might hit environment stuff that line does not hit
    if not IsValid(tr.Entity) then
        tr = util.TraceLine({start=spos, endpos=sdest, filter=self.Owner, mask=MASK_SHOT_HULL})
    end

    local hitEnt = tr.Entity

    -- effects
    if IsValid(hitEnt) then
        self.Weapon:SendWeaponAnim( ACT_VM_HITCENTER )

        local edata = EffectData()
        edata:SetStart(spos)
        edata:SetOrigin(tr.HitPos)
        edata:SetNormal(tr.Normal)
        edata:SetEntity(hitEnt)

        if hitEnt:IsPlayer() or hitEnt:GetClass() == "prop_ragdoll" then
            util.Effect("BloodImpact", edata)
        end
    else
        self.Weapon:SendWeaponAnim( ACT_VM_MISSCENTER )
    end

    if SERVER then
        self.Owner:SetAnimation( PLAYER_ATTACK1 )
    end


    if SERVER and tr.Hit and tr.HitNonWorld and IsValid(hitEnt) then
        if hitEnt:IsPlayer() then
            -- knife damage is never karma'd, so don't need to take that into
            -- account we do want to avoid rounding error strangeness caused by
            -- other damage scaling, causing a death when we don't expect one, so
            -- when the target's health is close to kill-point we just kill

            local dmg = DamageInfo()
            dmg:SetDamage(self.Primary.Damage)
            dmg:SetAttacker(self.Owner)
            dmg:SetInflictor(self.Weapon or self)
            dmg:SetDamageForce(self.Owner:GetAimVector() * 5)
            dmg:SetDamagePosition(self.Owner:GetPos())
            dmg:SetDamageType(DMG_SLASH)

            hitEnt:DispatchTraceAttack(dmg, spos + (self.Owner:GetAimVector() * 3), sdest)
        else
            bullet = {}
            bullet.Num    = 1
            bullet.Src    = self.Owner:GetShootPos()
            bullet.Dir    = self.Owner:GetAimVector()
            bullet.Spread = Vector(0, 0, 0)
            bullet.Tracer = 0
            bullet.Force  = 1
            bullet.Damage = 25
            self.Owner:FireBullets(bullet)
            self.Owner:ViewPunch(Angle(7, 0, 0))
        end
    end

    self.Owner:LagCompensation(false)
end


function SWEP:SecondaryAttack()
    if self.Owner:KeyDown(IN_RELOAD) then return end
    if self.Owner:KeyDown(IN_USE) then
        self:rFly()
        return
    end
    self.Weapon:EmitSound("weapons/shortsword/morrowind_shortsword_slash.mp3")
    self.Weapon:SetNextPrimaryFire(CurTime() + 1)
    self.Weapon:SetNextSecondaryFire(CurTime() + 0.8)
    self.Weapon:SendWeaponAnim(ACT_VM_HITCENTER)
    if !IsValid(self.Owner) then return end
    if (SERVER) then
        timer.Simple(.25, function()
            if !IsValid(self.Owner) then return end
            local knife = ents.Create("ent_mor_shortsword")
            knife:SetAngles(self.Owner:EyeAngles())
            local pos = self.Owner:GetShootPos()
            pos = pos + self.Owner:GetForward() * 5
            pos = pos + self.Owner:GetRight() * 9
            pos = pos + self.Owner:GetUp() * -5
            knife:SetPos(pos)
            knife:SetOwner(self.Owner)
            knife.Weapon = self		// Used to se the axe's model and the weapon it gives when used.
            knife.damage = 250
            knife.weapon = self
            knife.turnintohealth = true
            knife:Spawn()
            knife:Activate()

            self.Owner:SetAnimation(PLAYER_ATTACK1)

            local phys = knife:GetPhysicsObject()
            phys:SetVelocity(self.Owner:GetAimVector() * 1500)
            phys:AddAngleVelocity(Vector(0, 500, 0))
            self:Remove()
        end)
    end
end
