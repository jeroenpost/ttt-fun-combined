SWEP.Base = "gb_camo_base"
SWEP.ViewModelFOV	= 54
SWEP.HoldType = "pistol"
SWEP.Icon = "vgui/ttt_fun_killicons/fiveseven.png"
SWEP.PrintName = "GreenBlack Core"
SWEP.ViewModel		= "models/weapons/cstrike/c_pist_fiveseven.mdl"
SWEP.WorldModel		= "models/weapons/w_pist_fiveseven.mdl"
SWEP.Kind = WEAPON_PISTOL
SWEP.Slot = 1
SWEP.AutoSpawnable = false
SWEP.Primary.Recoil	= 1.5
SWEP.Primary.Damage = 25
SWEP.Primary.Delay = 0.38
SWEP.Primary.Cone = 0.001
SWEP.Primary.ClipSize = 40
SWEP.Primary.Automatic = true
SWEP.Primary.DefaultClip = 40
SWEP.Primary.ClipMax = 60
SWEP.Primary.Ammo = "Pistol"
SWEP.AutoSpawnable = false
SWEP.AmmoEnt = "item_ammo_pistol_ttt"
SWEP.Camo = 56

SWEP.Primary.Sound = Sound( "physics/body/body_medium_break3.wav" )
SWEP.IronSightsPos = Vector(-5.95, -4, 2.799)
SWEP.IronSightsAng = Vector(0, 0, 0)

function SWEP:SecondaryAttack()
    self:NormalPrimaryAttack()
end

SWEP.NextFire3 = 0
function SWEP:PrimaryAttack()
    if self.NextFire3 > CurTime() then return end
    self.NextFire3 = CurTime() + 3
    if SERVER then
        self:EmitSound("Friends/friend_join.wav")
        local ball = ents.Create("greenblack_core_ball");

        if (ball) then
            ball:SetPos(self.Owner:GetPos() + Vector(0,0,48));
            ball:SetPhysicsAttacker(self.Owner)
            ball.Owner = self.Owner
            ball:Spawn();
            --ball:SetModelScale( ball:GetModelScale() * 0.1, 0.01 )
            local physicsObject = ball:GetPhysicsObject();
            physicsObject:ApplyForceCenter(self.Owner:GetAimVector() * 2500);
            self.RemoteBoom= ball;
        end;
    end
end


local ENT = {}

ENT.Type = "anim"
ENT.Base = "base_anim"


if SERVER then

    function ENT:Initialize()
        self:SetModel("models/Gibs/HGIBS.mdl")
        self:SetModelScale(self:GetModelScale()*1,0)
        self:SetMaterial("camos/camo56")
        self.Entity:PhysicsInit(SOLID_VPHYSICS);
        self.Entity:SetMoveType(MOVETYPE_VPHYSICS);
        self.Entity:SetSolid(SOLID_VPHYSICS);
        self.CanBoom = CurTime() + 3
        self.Angles = -360
        self.allow_gb_pickup = true
        self:SetUseType( SIMPLE_USE )

        self.Entity:SetCollisionGroup(COLLISION_GROUP_WEAPON)
        local phys = self:GetPhysicsObject()
        if phys:IsValid() then
            phys:Wake()
            phys:EnableGravity(true);
        end

        local soundfile = gb_config.websounds.."gb_core_"..math.random(1,12)..".mp3"
        gb_PlaySoundFromServer(soundfile, self)

        timer.Create(self:EntIndex().."makesoundcore",10,100,function()
            if IsValid(self) then
                local soundfile = gb_config.websounds.."gb_core_"..math.random(1,12)..".mp3"
                gb_PlaySoundFromServer(soundfile, self)
                local physicsObject = self:GetPhysicsObject();
                physicsObject:ApplyForceCenter(Vector(0,0,1400));
                self.istalking = true
                timer.Simple(2,function()
                    if IsValid(self) then self.istalking = false end

                  end)
             end
        end)

        timer.Create(self:EntIndex().."colorchangedads",0.2,0,function()
            if IsValid(self) and self:IsPlayerHolding() or self.istalking then
                self:SetColor(Color(math.Rand(50,255),math.Rand(50,255),math.Rand(50,255),255))
            elseif IsValid(self) then
                self:SetColor(Color(255,255,255))
            end
        end)

    end

    function ENT:Use(ply)
        if ( self:IsPlayerHolding() ) then
            ply:DropObject(  )
         return end
        ply:PickupObject( self )

    end



    function ENT:OnTakeDamage(  )
            timer.Simple(0.1, function()
                if IsValid(self) then
                self:Explode()
                    end
            end)
    end

    function ENT:Explode()
        if self.exploded then return end self.exploded = true
        local effect = EffectData()
        effect:SetStart(self:GetPos())
        effect:SetOrigin(self:GetPos())
        effect:SetScale(120)
        effect:SetRadius(120)
        effect:SetMagnitude(200)

        util.Effect("Explosion", effect, true, true)
        if IsValid(self.Owner) then
        util.BlastDamage(self, self.Owner, self:GetPos(), 200, 100)
        end
        self:Remove()
    end


end

scripted_ents.Register( ENT, "greenblack_core_ball", true )