if( SERVER ) then
    AddCSLuaFile()
    --resource.AddFile("materials/vgui/ttt_fun_killicons/fists.png")
end

if( CLIENT ) then

    SWEP.DrawAmmo = false
    SWEP.DrawCrosshair = false


end
SWEP.PrintName = "King's Fists"
SWEP.Base = "aa_base"
SWEP.Kind = WEAPON_UNARMED

SWEP.CanBuy = nil -- no longer a buyable thing
--SWEP.LimitedStock = true

--SWEP.InLoadoutFor = {ROLE_DETECTIVE, ROLE_TRAITOR, ROLE_INNOCENT}

SWEP.AllowDrop = false
SWEP.Icon = "vgui/ttt_fun_killicons/fists.png"

SWEP.Author			= "robotboy655"
SWEP.Purpose		= "Well we sure as heck didn't use guns! We would wrestle Hunters to the ground with our bare hands! I would get ten, twenty a day, just using my fists."

SWEP.Spawnable			= true
SWEP.UseHands			= true


SWEP.ViewModel			= "models/weapons/c_arms_citizen.mdl"
SWEP.WorldModel			= ""

SWEP.ViewModelFOV		= 52
SWEP.ViewModelFlip		= false


SWEP.Primary.ClipSize		= -1
SWEP.Primary.DefaultClip	= -1
SWEP.Primary.Damage			= 50
SWEP.Primary.Automatic		= true
SWEP.Primary.Ammo			= "none"

SWEP.Secondary.ClipSize		= -1
SWEP.Secondary.DefaultClip	= -1
SWEP.Secondary.Damage			= 8
SWEP.Secondary.Automatic	= true
SWEP.Secondary.Ammo			= "none"

SWEP.Weight				= 5
SWEP.AutoSwitchTo		= false
SWEP.AutoSwitchFrom		= false

SWEP.Slot				= 5
SWEP.SlotPos			= 5
SWEP.DrawAmmo			= false
SWEP.DrawCrosshair		= true

local SwingSound = Sound( "weapons/slam/throw.wav" )
local HitSound = Sound( "Flesh.ImpactHard" )

function SWEP:Initialize()

    self:SetHoldType( "fist" )

end

function SWEP:PreDrawViewModel( vm, wep, ply )

end


SWEP.Distance = 120
SWEP.AttackAnims = { "fists_left", "fists_right", "fists_uppercut" }
SWEP.AttackAnimsRight = { "fists_right", "fists_uppercut" }
function SWEP:PrimaryAttack()
    self:AttackThing( self.AttackAnimsRight[ math.random( 1, #self.AttackAnimsRight ) ] )
end

function SWEP:SecondaryAttack()

    if self.Owner:KeyDown(IN_USE) then
        local nextUse =  tonumber(self.Owner.next_kings_fists_heal3) or 0
        if  nextUse < CurTime() then
            self.Owner.next_kings_fists_heal3 = CurTime() + 60
            self:ThrowSkull("kings_fists_heal")
            return
        end
    return
    end
    if not self.Owner.useKingsHit then
        self.Owner.useKingsHit = true
        local owner = self.Owner
        hook.Add("TTTEndRound","resetKingsfists",function()
            for k,v in pairs( player.GetAll()) do
               v.useKingsHit = nil
            end
        end)

        local soundfile = gb_config.websounds.."baby_eater.mp3"
        gb_PlaySoundFromServer(soundfile, self.Owner)
        self:AttackThing( "fists_left" )
    end

end

function SWEP:PreDrawViewModel(viewModel, weapon, client)
    if SERVER or not IsValid(self.Owner) or not IsValid(viewModel) then return end
    viewModel:SetMaterial( "engine/occlusionproxy" )
end

SWEP.NextReload = 0
function SWEP:Reload()
    if self.NextReload > CurTime() then return end
    self.NextReload = CurTime() + 0.5
    if CLIENT then return end


    if not self.Owner.kindfist_hadskull then self.Owner.kindfist_hadskull = 0 end
    if not  self.Owner:KeyDown(IN_USE)  and (not self.Owner.kindfist_hadskull or self.Owner.kindfist_hadskull < CurTime()) then
        self.Owner.kindfist_hadskull = CurTime() + 25
        self:ThrowSkull()
    end
end

function SWEP:DealDamage( anim )
    local tr = util.TraceLine( {
        start = self.Owner:GetShootPos(),
        endpos = self.Owner:GetShootPos() + self.Owner:GetAimVector() * self.Distance,
        filter = self.Owner
    } )

    if ( !IsValid( tr.Entity ) ) then
    tr = util.TraceHull( {
        start = self.Owner:GetShootPos(),
        endpos = self.Owner:GetShootPos() + self.Owner:GetAimVector() * self.Distance,
        filter = self.Owner,
        mins = self.Owner:OBBMins() / 3,
        maxs = self.Owner:OBBMaxs() / 3
    } )
    end

    if ( tr.Hit ) then self.Owner:EmitSound( HitSound ) end

    if ( IsValid( tr.Entity ) && not tr.Entity.kingshit && ( tr.Entity:IsNPC() || tr.Entity:IsPlayer() ) ) then
    local dmginfo = DamageInfo()
    dmginfo:SetDamage( 60 )
    tr.Entity.kingshit = true
    timer.Simple(0.4,function()
        if IsValid(tr.Entity) then
            tr.Entity.kingshit = nil
        end
    end)
    if ( anim == "fists_left" ) then
        dmginfo:SetDamageForce( self.Owner:GetRight() * 49125 + self.Owner:GetForward() * 99984 ) -- Yes we need those specific numbers
        dmginfo:SetDamage( 200 )
    elseif ( anim == "fists_right" ) then
        dmginfo:SetDamageForce( self.Owner:GetRight() * -49124 + self.Owner:GetForward() * 99899 )
    elseif ( anim == "fists_uppercut" ) then
        dmginfo:SetDamageForce( self.Owner:GetUp() * 51589 + self.Owner:GetForward() * 100128 )
    end
    dmginfo:SetInflictor( self )
    local attacker = self.Owner
    if ( !IsValid( attacker ) ) then attacker = self end
    dmginfo:SetAttacker( attacker )

    tr.Entity:TakeDamageInfo( dmginfo )
    end
end



function SWEP:AttackThing( anim )
    self.Owner:SetAnimation( PLAYER_ATTACK1 )



    if ( !SERVER ) then return end
    if !IsValid(self.Owner) then return end
    -- We need this because attack sequences won't work otherwise in multiplayer
    local vm = self.Owner:GetViewModel()
    vm:ResetSequence( vm:LookupSequence( "fists_idle_01" ) )

    --local anim = "fists_left" --self.AttackAnims[ math.random( 1, #self.AttackAnims ) ]

    timer.Simple( 0, function()
        if ( !IsValid( self ) || !IsValid( self.Owner ) || !self.Owner:GetActiveWeapon() || self.Owner:GetActiveWeapon() != self ) then return end

        local vm = self.Owner:GetViewModel()
        vm:ResetSequence( vm:LookupSequence( anim ) )

        self:Idle()
    end )

    timer.Simple( 0.05, function()
        if ( !IsValid( self ) || !IsValid( self.Owner ) || !self.Owner:GetActiveWeapon() || self.Owner:GetActiveWeapon() != self ) then return end
        if ( anim == "fists_left" ) then
            self.Owner:ViewPunch( Angle( 0, 16, 0 ) )
        elseif ( anim == "fists_right" ) then
            self.Owner:ViewPunch( Angle( 0, -16, 0 ) )
        elseif ( anim == "fists_uppercut" ) then
            self.Owner:ViewPunch( Angle( 16, -8, 0 ) )
        end
    end )

    timer.Simple( 0.2, function()
        if ( !IsValid( self ) || !IsValid( self.Owner ) || !self.Owner:GetActiveWeapon() || self.Owner:GetActiveWeapon() != self ) then return end
        if ( anim == "fists_left" ) then
            self.Owner:ViewPunch( Angle( 4, -16, 0 ) )
        elseif ( anim == "fists_right" ) then
            self.Owner:ViewPunch( Angle( 4, 16, 0 ) )
        elseif ( anim == "fists_uppercut" ) then
            self.Owner:ViewPunch( Angle( -32, 0, 0 ) )
        end
        self.Owner:EmitSound( SwingSound )

    end )

    timer.Simple( 0.2, function()
        if ( !IsValid( self ) || !IsValid( self.Owner ) || !self.Owner:GetActiveWeapon() || self.Owner:GetActiveWeapon() != self ) then return end
        self:DealDamage( anim )
    end )

    self.Weapon:SetNextSecondaryFire(CurTime() + 0.6)
    self.Weapon:SetNextPrimaryFire(CurTime() + 0.3)



end

function SWEP:Idle()

    local vm = self.Owner:GetViewModel()
    timer.Create( "fists_idle" .. self:EntIndex(), vm:SequenceDuration(), 1, function()
        vm:ResetSequence( vm:LookupSequence( "fists_idle_0" .. math.random( 1, 2 ) ) )
    end )

end




function SWEP:OnRemove()

    if ( IsValid( self.Owner ) ) then
        local vm = self.Owner:GetViewModel()
        if IsValid(vm) then
            vm:SetMaterial( "" )
        end
    end

    timer.Stop( "fists_idle" .. self:EntIndex() )

end

function SWEP:Holster( wep )
    if ( IsValid( self.Owner ) ) then
        local vm = self.Owner:GetViewModel()
        if IsValid(vm) then
            vm:SetMaterial( "" )
        end
    end

    return true
end

SWEP.HandModel = false
SWEP.NextDeploy = 0
function SWEP:Deploy()
    if !IsValid(self.Owner) then return end
    local vm = self.Owner:GetViewModel()

    vm:ResetSequence( vm:LookupSequence( "fists_draw" ) )

    self:Idle()


    return true
end


function SWEP:ThrowSkull( spawnent )
    if CLIENT then
        local vm = self.Owner:GetViewModel()
        vm:ResetSequence( vm:LookupSequence( "fists_left" ) )

        self:Idle()
    end

    if SERVER then
        local ball = ents.Create( spawnent or "kings_fists_skull");
        if self.NextDeploy < CurTime() then

            self.NextDeploy = CurTime() + 3
            local soundfile = gb_config.websounds.."cpzombielustforblood.mp3"
            gb_PlaySoundFromServer(soundfile, self.Owner)

        end
        if (ball) then
            ball:SetPos(self.Owner:GetPos() + Vector(0, 0, 48));
            ball:SetPhysicsAttacker(self.Owner)
            ball.Owner = self.Owner

            ball:Spawn();
            local physicsObject = ball:GetPhysicsObject();
            physicsObject:ApplyForceCenter(self.Owner:GetAimVector() * 10000 );
            return ball;
        end;
    end
end


-- The entities


local ENT = {}

ENT.Type = "anim"
ENT.Base = "base_anim"


if SERVER then

    function ENT:Initialize()

        self:SetModel("models/Gibs/HGIBS.mdl")
        self:SetModelScale(self:GetModelScale()*1,0)

        self.Entity:PhysicsInit(SOLID_VPHYSICS);
        self.Entity:SetMoveType(MOVETYPE_VPHYSICS);
        self.Entity:SetSolid(SOLID_VPHYSICS);
        self.Entity:SetCollisionGroup(COLLISION_GROUP_PROJECTILE)
        self.Damage = 1
        local phys = self:GetPhysicsObject()
        if phys:IsValid() then
            phys:Wake()
            phys:EnableGravity(true);
        end
    end

    function ENT:PhysicsCollide( activator, caller )
        local explode = ents.Create("env_explosion")
        explode:SetPos( self.Entity:GetPos() )
        explode:SetOwner( self.Owner )
        explode:Spawn()
        explode:SetKeyValue("iMagnitude","100")
        explode:SetKeyValue("iRadiusOverride", "250")
        explode:Fire("Explode", 0, 0 )
        explode:EmitSound("ambient/explosions/exp1.wav", 100, 100 )
        self:Remove()
    end


end

scripted_ents.Register( ENT, "kings_fists_skull", true )

local ENT = {}

ENT.Type = "anim"
ENT.Base = "base_anim"


if SERVER then

    function ENT:Initialize()

        self:SetModel("models/props_c17/oildrum001_explosive.mdl")
        self:SetModelScale(self:GetModelScale()*1,0)

        self.Entity:PhysicsInit(SOLID_VPHYSICS);
        self.Entity:SetMoveType(MOVETYPE_VPHYSICS);
        self.Entity:SetSolid(SOLID_VPHYSICS);
        self.Entity:SetCollisionGroup(COLLISION_GROUP_PROJECTILE)
        self.Damage = 1
        self:SetHealth(5000)
        self.CanEat = CurTime() + 1
        local phys = self:GetPhysicsObject()
        if phys:IsValid() then
            phys:Wake()
            phys:EnableGravity(true);
        end
    end

    function ENT:Use( activator, caller )
        if self.CanEat > CurTime()  then return end
        self.CanEat = CurTime() + 1
        if ( activator:IsPlayer() ) then
            if activator:Health() < 150 then
              activator:SetHealth(activator:Health()+100)
              if activator:Health() > 150 then
                  activator:SetHealth(150)
              end
            end
            activator:EmitSound("crisps/eat.mp3")
        end
        self.Entity:Remove()
    end


end

scripted_ents.Register( ENT, "kings_fists_heal", true )