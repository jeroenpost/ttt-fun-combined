SWEP.Base			= "aa_base"
SWEP.PrintName       = "Pokemons/Sfox's freeze ray"
SWEP.Kind 			= WEAPON_EQUIP2
SWEP.Slot = 8
SWEP.SlotPos 	= 1
SWEP.Primary.Recoil	= 1
SWEP.Primary.Damage = 19
SWEP.Primary.Delay 	= 0.35
SWEP.Primary.Cone 	= 0.01
SWEP.Primary.ClipSize 		= 10
SWEP.Primary.ClipMax 		= 10
SWEP.Primary.DefaultClip 	= 10
SWEP.Primary.NumShots		= 1
SWEP.Primary.Automatic 	= true
SWEP.HeadshotMultiplier = 3
SWEP.AmmoEnt 		= "item_ammo_revolver_ttt"

SWEP.AutoSpawnable      = false

SWEP.ViewModel = Model("models/weapons/v_hands.mdl")
SWEP.ViewModelFlip = false

SWEP.NextSecondary = 0


SWEP.flyammo = 5
SWEP.nextreload = 0

function SWEP:SecondaryAttack()

self:Fly()
end




SWEP.nextreload = 0

function SWEP:PrimaryAttack(worldsnd)
    self.Weapon:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
    if self.nextreload > CurTime() or self:Clip1() < 1 then return end
    self.nextreload = CurTime() + 1
    self:ShootBullet( self.Primary.Damage, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )
    self:Freeze()

    if ( self.Owner:IsValid() ) then
        local tr = self.Owner:GetEyeTrace()
        local effectdata = EffectData()
        effectdata:SetOrigin(tr.HitPos)
        effectdata:SetNormal(tr.HitNormal)
        effectdata:SetScale(1)
        -- util.Effect("effect_mad_ignition", effectdata)
        util.Decal("FadingScorch", tr.HitPos + tr.HitNormal, tr.HitPos - tr.HitNormal)

        local tracedata = {}
        tracedata.start = tr.HitPos
        tracedata.endpos = Vector(tr.HitPos.x, tr.HitPos.y, tr.HitPos.z - 10)
        tracedata.filter = tr.HitPos
        local tracedata = util.TraceLine(tracedata)
        if SERVER then

            ent = tr.Entity
            if ent:IsPlayer() or ent:IsNPC() then
                ply = ent
                if not ply.OldRunSpeed and not ply.OldWalkSpeed then
                    ply.OldRunSpeed = ply:GetNWInt("runspeed")
                    ply.OldWalkSpeed =  ply:GetNWInt("walkspeed")
                end

                    local eyeang = ent:EyeAngles()

                    local j = math.Rand(1,10) --10
                    eyeang.pitch = math.Clamp(eyeang.pitch + math.Rand(-j, j), -90, 90)
                    eyeang.yaw = math.Clamp(eyeang.yaw + math.Rand(-j, j), -90, 90)
                    ent:SetEyeAngles(eyeang)


                ply:SetNWInt("runspeed", 25)
                ply:SetNWInt("walkspeed", 25)

                timer.Create("NormalizeRunSpeedPokemonFreeze2"..math.random(0,9999999999),10,1,function()
                    if IsValid(ply) and  ply.OldRunSpeed and ply.OldWalkSpeed then
                        ply:SetNWInt("runspeed",  ply.OldRunSpeed)
                        ply:SetNWInt("walkspeed",ply.OldWalkSpeed)
                        ply.OldRunSpeed = false
                        ply.OldWalkSpeed = false
                    end
                end)

            end
        end
        self:TakePrimaryAmmo( 1 )
        local owner = self.Owner
        --if not ValidEntity(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

        owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )
    end
end
