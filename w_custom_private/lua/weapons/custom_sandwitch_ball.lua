if ( CLIENT ) then
    SWEP.PrintName			= "Sandwich's Jiggly Balls"
    SWEP.DrawAmmo 			= false
    SWEP.DrawCrosshair 		= false
    SWEP.ViewModelFOV			= 60
    SWEP.ViewModelFlip		= false
    SWEP.CSMuzzleFlashes		= true

    SWEP.Slot				= 2
    SWEP.SlotPos			= 0
end
SWEP.Base = "aa_base"
SWEP.Category			= "HunterFP"

SWEP.Spawnable				= true
SWEP.AdminSpawnable			= true

SWEP.UseHands			= true

SWEP.ViewModel			= "models/weapons/c_arms_citizen.mdl"

function SWEP:Idle()
    if !IsValid(self.Owner) then return end
    local vm = self.Owner:GetViewModel()
    timer.Create( "fists_idle" .. self:EntIndex(), vm:SequenceDuration(), 1, function()
        vm:ResetSequence( vm:LookupSequence( "fists_idle_0" .. math.random( 1, 2 ) ) )
    end )

end

SWEP.DrawWeaponInfoBox  	= false

SWEP.Weight					= 5
SWEP.AutoSwitchTo				= true
SWEP.AutoSwitchFrom			= false

SWEP.Primary.ClipSize			= -1
SWEP.Primary.Damage			= -1
SWEP.Primary.DefaultClip		= -1
SWEP.Primary.Automatic			= true
SWEP.Primary.Ammo			="none"


SWEP.Secondary.ClipSize			= -1
SWEP.Secondary.DefaultClip		= -1
SWEP.Secondary.Damage			= -1
SWEP.Secondary.Automatic		= true
SWEP.Secondary.Ammo			="none"

        /*---------------------------------------------------------
        Think
---------------------------------------------------------*/
function SWEP:Think()
end

/*---------------------------------------------------------
Initialize
---------------------------------------------------------*/
function SWEP:Initialize()
self:SetHoldType( "rpg" )
end


function SWEP:Deploy()
if !IsValid(self.Owner) then return end
local vm = self.Owner:GetViewModel()



vm:ResetSequence( vm:LookupSequence( "fists_draw" ) )

self:Idle()

return true
end

function SWEP:PreDrawViewModel(viewModel, weapon, client)
if SERVER or not IsValid(self.Owner) or not IsValid(viewModel) then return end
viewModel:SetMaterial( "engine/occlusionproxy" )
end


/*---------------------------------------------------------
CreateBall
---------------------------------------------------------*/
function SWEP:CreateBall(position, client, power, damage,shouldexplode)
if CLIENT then
local vm = self.Owner:GetViewModel()
vm:ResetSequence( vm:LookupSequence( "fists_left" ) )

self:Idle()
end

if SERVER then
local ball = ents.Create("dodge_ball");

if (ball) then
ball:SetPos(position);
ball:SetPhysicsAttacker(self.Owner)
ball.Owner = self.Owner
ball.damage = damage
ball.shouldexplode = shouldexplode or false
ball:Spawn();
ball:SetModelScale( ball:GetModelScale() * 0.1, 0.01 )
local physicsObject = ball:GetPhysicsObject();
physicsObject:ApplyForceCenter(client:GetAimVector() * power);
return ball;
end;
end
end;
/*---------------------------------------------------------
PrimaryAttack
---------------------------------------------------------*/
function SWEP:PrimaryAttack()
self.Weapon:SetNextPrimaryFire(CurTime() + 3)
self.Weapon:SetNextSecondaryFire(CurTime() + 1.5)
if self.Owner:KeyDown(IN_USE) then
    self.Owner:SetAnimation( PLAYER_ATTACK1 )
    if SERVER then
    local client = self.Owner
    local ball = self:CreateBall(client:GetPos() + Vector(0, 0, 48) + client:GetAimVector()*64, client,1000, 55, true)
    timer.Simple(4,function() if IsValid(ball)then ball:Remove() end end)
    end
    return
end
self.Owner:SetAnimation( PLAYER_ATTACK1 )
if SERVER then
local client = self.Owner
local ball = self:CreateBall(client:GetPos() + Vector(0, 0, 48) + client:GetAimVector()*64, client,1000, 25)
timer.Simple(4,function() if IsValid(ball)then ball:Remove() end end)
end
end

function SWEP:SecondaryAttack()
if self.Owner:KeyDown(IN_USE) then
self.Weapon:SetNextSecondaryFire( CurTime() + 0.15 )
self.Weapon:SetNextPrimaryFire( CurTime() + 0.15 )


if not worldsnd then
self.Weapon:EmitSound( "weapons/smg1/switch_burst.wav", self.Primary.SoundLevel )
elseif SERVER then
sound.Play("weapons/smg1/switch_burst.wav", self:GetPos(), self.Primary.SoundLevel)
end

self:ShootBullet( 7, 1, 1, 0.009 )



local owner = self.Owner
if not IsValid(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )
return
end
self.Weapon:SetNextPrimaryFire(CurTime() + 1.5)
self.Weapon:SetNextSecondaryFire(CurTime() + 3)
self.Owner:SetAnimation( PLAYER_ATTACK1 )
if SERVER then
local client = self.Owner
local ball = self:CreateBall(client:GetPos() + Vector(0, 0, 48) + client:GetAimVector()*64, client, 10000, 40)
timer.Simple(4,function() if IsValid(ball)then ball:Remove() end end)
end
end

function SWEP:EntityFaceBack(ent)
local angle = self.Owner:GetAngles().y -ent:GetAngles().y
if angle < -180 then angle = 360 +angle end
if angle <= 90 and angle >= -90 then return true end
return false
end

/*---------------------------------------------------------
Reload
---------------------------------------------------------*/
SWEP.NextReload = 0
function SWEP:Reload()
if self.NextReload > CurTime() then return end
self.NextReload = CurTime() + 15
   local model = self.Owner:GetModel()
local offset =  self.Owner:GetViewOffset()
    self.Owner:SetModel("models/dav0r/hoverball.mdl")
    self.Owner:SetViewOffset(Vector(0,0,10))
local owner =  self.Owner
    timer.Create(self.Owner:EntIndex().."normalmodelhoversand",5,1,function()
    if not IsValid( owner) then return end
        owner:SetModel(model)
        owner:SetViewOffset(offset);
    end)
return false
end

/*---------------------------------------------------------
OnRemove
---------------------------------------------------------*/
function SWEP:OnRemove()

return true
end

/*---------------------------------------------------------
Holster
---------------------------------------------------------*/
function SWEP:Holster()

return true
end

/*---------------------------------------------------------
DrawWeaponSelection
---------------------------------------------------------*/
function SWEP:DrawWeaponSelection(x, y, wide, tall, alpha)

end
