if( SERVER ) then
	AddCSLuaFile(  )


end

if( CLIENT ) then
	
	
	SWEP.SlotPos = 0
	SWEP.DrawAmmo = false
	SWEP.DrawCrosshair = false
end


SWEP.PrintName = "inSANE medic"
SWEP.Slot = 3
SWEP.Kind = WEAPON_NADE
SWEP.Icon = "vgui/entities/weapon_mor_daedric_shortsword"

SWEP.Category = "Morrowind Shortswords"
SWEP.Author			= "Doug Tyrrell + Mad Cow (Revisions by Neotanks) for LUA (Models, Textures, ect. By: Hellsing/JJSteel)"
SWEP.Base			= "weapon_mor_base_shortsword"
SWEP.Instructions	= "Left click to stab/slash/lunge and right click to throw, also capable of badassery."
SWEP.Contact		= ""
SWEP.Purpose		= ""

SWEP.ViewModelFOV	= 72
SWEP.ViewModelFlip	= false

SWEP.Spawnable			= true
SWEP.AdminSpawnable		= true
  
SWEP.ViewModel      = "models/morrowind/daedric/shortsword/v_daedric_shortsword.mdl"
SWEP.WorldModel   = "models/morrowind/daedric/shortsword/w_daedric_shortsword.mdl"

SWEP.Primary.Damage		= 40
SWEP.Primary.NumShots		= 1
SWEP.Primary.Delay 		= .6

SWEP.Primary.ClipSize		= -1					// Size of a clip
SWEP.Primary.DefaultClip	= -1					// Default number of bullets in a clip
SWEP.Primary.Automatic		= true				// Automatic/Semi Auto
SWEP.Primary.Ammo			= "none"

SWEP.Secondary.ClipSize		= -1					// Size of a clip
SWEP.Secondary.DefaultClip	= -1					// Default number of bullets in a clip
SWEP.Secondary.Automatic	= false				// Automatic/Semi Auto
SWEP.Secondary.Ammo		= "none"
SWEP.HeadshotMultiplier = 1

function SWEP:PrimaryAttack()
    self:SetNextPrimaryFire(CurTime() + self.Primary.Delay)
    self:SetNextSecondaryFire(CurTime() + self.Primary.Delay)
    self.Owner:SetAnimation( PLAYER_ATTACK1 )
    self.Weapon:SendWeaponAnim( ACT_VM_HITCENTER )
    timer.Simple(.01, function()
        if !IsValid(self) or !IsValid(self.Owner) then return end
        local trace = self.Owner:GetEyeTrace()
        if trace.HitPos:Distance(self.Owner:GetShootPos()) <= 120 then
            if( trace.Entity:IsPlayer() or trace.Entity:IsNPC() or trace.Entity:GetClass()=="prop_ragdoll" ) then
                self.Owner:EmitSound( self.FleshHit[math.random(1,#self.FleshHit)] )
            else
                self.Owner:EmitSound( self.Hit[math.random(1,#self.Hit)] )
            end
            bullet = {}
            bullet.Num    = 1
            bullet.Src    = self.Owner:GetShootPos()
            bullet.Dir    = self.Owner:GetAimVector()
            bullet.Spread = Vector(0, 0, 0)
            bullet.Tracer = 0
            bullet.Force  = 1
            bullet.Damage = 40
            self.Owner:FireBullets(bullet)
            self.Owner:ViewPunch(Angle(7, 0, 0))
        else
            self.Weapon:EmitSound("weapons/shortsword/morrowind_shortsword_slash.mp3")
        end
    end)
end

function SWEP:SecondaryAttack()
    if self.Owner:KeyDown(IN_USE) then
        self:Fly()
        return
    end
    self:SetNextPrimaryFire(CurTime() + self.Primary.Delay)
    self:SetNextSecondaryFire(CurTime() + 5)
    self.Owner:SetAnimation( PLAYER_ATTACK1 )
    self.Weapon:SendWeaponAnim( ACT_VM_HITCENTER )
    timer.Simple(.01, function()
        if !IsValid(self) or !IsValid(self.Owner) then return end
        local trace = self.Owner:GetEyeTrace()
        if trace.HitPos:Distance(self.Owner:GetShootPos()) <= 75 then
            if( trace.Entity:IsPlayer() or trace.Entity:IsNPC() ) then
                self.Owner:EmitSound( "items/medshot4.wav" )
            else
                self.Owner:EmitSound( "items/medshotno1.wav" )
            end
            if trace.Entity:Health() < 175 then
                trace.Entity:SetHealth(trace.Entity:Health()+15)
            else
                self.Owner:EmitSound( "items/medshotno1.wav" )
            end

            self.Owner:ViewPunch(Angle(7, 0, 0))
        else
            self.Weapon:EmitSound("weapons/shortsword/morrowind_shortsword_slash.mp3")
        end
    end)
end

-- FLY
SWEP.flyammo = 5
SWEP.nextFly = 0
function SWEP:Fly( soundfile )
    if self.nextFly > CurTime() or CLIENT then return end
    self.nextFly = CurTime() + 0.30
    if self.flyammo < 1 then return end

    if not soundfile then
        if SERVER then self.Owner:EmitSound(Sound("weapons/ls/lightsaber_swing.mp3")) end
        if SERVER then self.Owner:EmitSound(Sound("player/suit_sprint.wav")) end
    else
        if SERVER then self.Owner:EmitSound(Sound(soundfile)) end
    end
    self.flyammo = self.flyammo - 1
    self.Owner:SetAnimation(PLAYER_ATTACK1)
    local power = 400
    if math.random(1,5) < 2 then power = 100
    elseif math.random(1,15) < 2 then power = -500
    elseif math.random(1,15) < 2 then power = 2500
    else power = math.random( 350,450 ) end

    if SERVER then self.Owner:SetVelocity(self.Owner:GetForward() * power + Vector(0, 0, power)) end
    timer.Simple(20, function() if IsValid(self) then self.flyammo = self.flyammo + 1 end end)
    --timer.Simple(0.2,self.SecondaryAttackDelay,self)
end