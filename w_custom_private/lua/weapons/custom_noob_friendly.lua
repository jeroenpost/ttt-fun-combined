

SWEP.HoldType                   = "shotgun"

if CLIENT then

    SWEP.ViewModelFlip = false
    SWEP.Slot = 4
    SWEP.ViewModelFOV = 70
    SWEP.Icon = "VGUI/ttt/icon_shotgun"
end
SWEP.PrintName = "Noob Friendly"

SWEP.Base                               = "aa_base"
SWEP.Spawnable = true
SWEP.AdminSpawnable = true

SWEP.Kind = WEAPON_NADE
SWEP.WeaponID = AMMO_SHOTGUN
SWEP.UseHands = true
SWEP.Primary.Ammo = "Buckshot"
SWEP.Primary.Damage = 12
SWEP.Primary.Cone = 0.075
SWEP.Primary.Delay = 0.9
SWEP.Primary.ClipSize = 12
SWEP.Primary.ClipMax = 24
SWEP.Primary.DefaultClip = 12
SWEP.Primary.Automatic = false
SWEP.Primary.NumShots = 12
SWEP.AutoSpawnable      = false
SWEP.AmmoEnt = "item_box_buckshot_ttt"
SWEP.UseHands = true
SWEP.ViewModel                  = "models/weapons/v_sawedoff.mdl"
SWEP.WorldModel                 = "models/weapons/w_sawedoff.mdl"
SWEP.Primary.Sound                      = ( "CSTM_G2CONTENDER" )
SWEP.Primary.Recoil                     = 7
--SWEP.IronSightsPos            = Vector( 5.7, -3, 3 )

SWEP.IronSightsPos = Vector(-5, -6.951, 4.508)
SWEP.IronSightsAng = Vector(0.15, 0, 0)
SWEP.SightsPos = Vector(5.738, -2.951, 3.378)
SWEP.SightsAng = Vector(0.15, 0, 0)
SWEP.RunSightsPos = Vector(0.328, -6.394, 1.475)
SWEP.RunSightsAng = Vector(-4.591, -48.197, -1.721)

SWEP.reloadtimer = 0

function SWEP:SetupDataTables()
    self:DTVar("Bool", 0, "reloading")

    return self.BaseClass.SetupDataTables(self)
end

function SWEP:Reload()
    if self.Owner:KeyDown(IN_USE) then
        self:DropHealth("normal_health_station")
        return
    end
    self:SetIronsights( false )

    --if self.Weapon:GetNWBool( "reloading", false ) then return end
    if self.dt.reloading then return end

    if not IsFirstTimePredicted() then return end

    if self.Weapon:Clip1() < self.Primary.ClipSize and self.Owner:GetAmmoCount( self.Primary.Ammo ) > 0 then

        if self:StartReload() then
            return
        end
    end

end

function SWEP:StartReload()
    --if self.Weapon:GetNWBool( "reloading", false ) then
    if self.dt.reloading then
        return false
    end

    if not IsFirstTimePredicted() then return false end

    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
    self.Weapon:EmitSound("Weapon_M4CU.Boltpull")

    local ply = self.Owner

    if not ply or ply:GetAmmoCount(self.Primary.Ammo) <= 0 then
        return false
    end

    local wep = self.Weapon

    if wep:Clip1() >= self.Primary.ClipSize then
        return false
    end

    wep:SendWeaponAnim(ACT_SHOTGUN_RELOAD_START)

    self.reloadtimer =  CurTime() + wep:SequenceDuration()

    --wep:SetNWBool("reloading", true)
    self.dt.reloading = true

    return true
end

function SWEP:PerformReload()
    local ply = self.Owner

    -- prevent normal shooting in between reloads
    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )

    if not ply or ply:GetAmmoCount(self.Primary.Ammo) <= 0 then return end

    local wep = self.Weapon

    if wep:Clip1() >= self.Primary.ClipSize then return end

    self.Owner:RemoveAmmo( 1, self.Primary.Ammo, false )
    self.Weapon:SetClip1( self.Weapon:Clip1() + 1 )

    wep:SendWeaponAnim(ACT_VM_RELOAD)

    self.reloadtimer = CurTime() + wep:SequenceDuration()
end

function SWEP:FinishReload()
    self.dt.reloading = false
    self.Weapon:SendWeaponAnim(ACT_SHOTGUN_RELOAD_FINISH)

    self.reloadtimer = CurTime() + self.Weapon:SequenceDuration()
end

function SWEP:CanPrimaryAttack()
    if self.Weapon:Clip1() <= 0 then
        self:EmitSound( "Weapon_Shotgun.Empty" )
        self:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
        return false
    end
    return true
end

SWEP.NextRegenAmmo = 0
function SWEP:Think()

    if self.NextRegenAmmo < CurTime() then
        if self:Clip1() < 200 then
            self.NextRegenAmmo = CurTime() + 3
            self:SetClip1(self:Clip1()+2)
        end
    end
    if self.dt.reloading and IsFirstTimePredicted() then
        if self.Owner:KeyDown(IN_ATTACK) then
            self:FinishReload()
            return
        end

        if self.reloadtimer <= CurTime() then

            if self.Owner:GetAmmoCount(self.Primary.Ammo) <= 0 then
                self:FinishReload()
            elseif self.Weapon:Clip1() < self.Primary.ClipSize then
                self:PerformReload()
            else
                self:FinishReload()
            end
            return
        end
    end
end

function SWEP:Deploy()
    self.dt.reloading = false
    self.reloadtimer = 0

    local ply = self.Owner
    if ply:GetNWInt("walkspeed") < 400 then
        ply:SetNWInt("walkspeed",400)
    end
    if ply:GetNWInt("runspeed") < 550 then
        ply:SetNWInt("runspeed", 550)
    end
    self.Owner:SetNWFloat("w_stamina", 1)
    self.Owner:SetJumpPower(450)

    return self.BaseClass.Deploy(self)
end

function SWEP:OnDrop()
    self:Remove()
end


function SWEP:SecondaryAttack()
    if self.Owner:KeyDown(IN_USE) then
        self:Fly()
        return
    end

    self:StabShoot(nil,65)
end