
if SERVER then
   AddCSLuaFile(  )
        --resource.AddFile("materials/vgui/ttt_fun_killicons/scout.png")
end

SWEP.HoldType           = "ar2"
SWEP.PrintName          = "Boom Scout"
 SWEP.Slot               = 8
if CLIENT then
   
	SWEP.Author = "GreenBlack"
  

   SWEP.Icon = "vgui/ttt_fun_killicons/scout.png"
end


SWEP.Base               = "aa_base"
SWEP.Spawnable = true
SWEP.AdminSpawnable = true

SWEP.Kind = WEAPON_EQUIP2
--SWEP.WeaponID = AMMO_RIFLE

SWEP.Primary.Delay          = 1
SWEP.Primary.Recoil         = 2
SWEP.Primary.Automatic = true
SWEP.Primary.Ammo = "357"
SWEP.Primary.Damage = 50
SWEP.Primary.Cone = 0.0001
SWEP.Primary.ClipSize = 25
SWEP.Primary.ClipMax = 75 -- keep mirrored to ammo
SWEP.Primary.DefaultClip = 25

SWEP.HeadshotMultiplier = 4

SWEP.AutoSpawnable      = false
SWEP.AmmoEnt = "item_ammo_357_ttt"

SWEP.UseHands			= true
SWEP.ViewModelFlip		= false
SWEP.ViewModelFOV		= 54
SWEP.ViewModel          = Model("models/weapons/cstrike/c_snip_scout.mdl")
SWEP.WorldModel         = Model("models/weapons/w_snip_scout.mdl")

SWEP.Primary.Sound = Sound(")weapons/scout/scout_fire-1.wav")

SWEP.Secondary.Sound = Sound("Default.Zoom")

SWEP.IronSightsPos      = Vector( 5, -15, -2 )
SWEP.IronSightsAng      = Vector( 2.6, 1.37, 3.5 )

function SWEP:SetZoom(state)
    if CLIENT then 
       return
    elseif IsValid(self.Owner) and self.Owner:IsPlayer() then
       if state then
          self.Owner:SetFOV(20, 0.3)
       else
          self.Owner:SetFOV(0, 0.2)
       end
    end
end

SWEP.flyammo = 5

-- Add some zoom to ironsights for this gun
function SWEP:SecondaryAttack()


    if self.Owner:KeyDown(IN_USE) then
        if not self.IronSightsPos then return end
        if self.Weapon:GetNextSecondaryFire() > CurTime() then return end

        bIronsights = not self:GetIronsights()

        self:SetIronsights( bIronsights )

        if SERVER then
            self:SetZoom(bIronsights)
         else
            self:EmitSound(self.Secondary.Sound)
        end
    else
        self:MakeExplosion()
    end


    self.Weapon:SetNextSecondaryFire( CurTime() + 0.3)
end

SWEP.defibs = 0
SWEP.nextDefib = 0
function SWEP:Defib()

    if ( self.defibs > 0) then return end


    self.NextAttacky = CurTime()+ 0.5


    local tr = util.TraceLine({ start = self.Owner:EyePos(), endpos = self.Owner:EyePos() + self.Owner:EyeAngles():Forward() * 80, filter = self.Owner })
    if tr.HitNonWorld and IsValid(tr.Entity) and tr.Entity:GetClass() == "prop_ragdoll" and tr.Entity.player_ragdoll then
        self.Weapon:EmitSound( "weapons/c4/c4_beep1.wav"	)
        local mins, maxs = self.Owner:OBBMins(), self.Owner:OBBMaxs()
        local tr2 = util.TraceHull({start = tr.Entity:LocalToWorld(tr.Entity:OBBCenter()) + vector_up, endpos = tr.Entity:LocalToWorld(tr.Entity:OBBCenter()) + Vector(0, 0, 80), filter = {tr.Entity, self.Owner}, mins = Vector(mins.x * 1.6, mins.y * 1.6, 0), maxs = Vector(maxs.x * 1.6, maxs.y * 1.6, 0), mask = MASK_PLAYERSOLID})
        if tr2.Hit then
            self.Owner:PrintMessage( HUD_PRINTCENTER, "There is no room to bring him back here" )
            return
        end


        self.nextDefib = CurTime() + 10
        self.defibs =  self.defibs + 1
        if SERVER then
            rag = tr.Entity
            local ply = player.GetByUniqueID(rag.uqid)
            local credits = CORPSE.GetCredits(rag, 0)
            if not IsValid(ply) then return end
            if SERVER and _globals['slayed'][ply:SteamID()] then self.Owner:PrintMessage( HUD_PRINTCENTER, "Can't revive, person was slayed" )
                ply:Kill();
                return
            end
            if SERVER and ((rag.was_role == ROLE_TRAITOR and self.Owner:GetRole() ~= ROLE_TRAITOR) or (rag.was_role ~= ROLE_TRAITOR and self.Owner:GetRole() == ROLE_TRAITOR))  then
                DamageLog("Defib: " .. self.Owner:Nick() .. " [" .. self.Owner:GetRoleString() .. "] Died defibbing "..ply:Nick().." [" .. ply:GetRoleString() .. "] ")
                self.Owner:PrintMessage( HUD_PRINTCENTER, ply:Nick().." was of the other team,  sucked up your soul and killed you" )
                self.Owner:Kill();

                return
            end
            DamageLog("Defib: " .. self.Owner:Nick() .. " [" .. self.Owner:GetRoleString() .. "] defibbed "..ply:Nick().." [" .. ply:GetRoleString() .. "] ")

            ply:SpawnForRound(true)
            ply:SetCredits(credits)
            ply:SetPos(rag:LocalToWorld(rag:OBBCenter()) + vector_up / 2) --Just in case he gets stuck somewhere
            ply:SetEyeAngles(Angle(0, rag:GetAngles().y, 0))
            ply:SetCollisionGroup(COLLISION_GROUP_WEAPON)
            timer.Simple(2, function() ply:SetCollisionGroup(COLLISION_GROUP_PLAYER) end)

            umsg.Start("_resurrectbody")
            umsg.Entity(ply)
            umsg.Bool(CORPSE.GetFound(rag, false))
            umsg.End()
            rag:Remove()
        end
    else
        self.Weapon:EmitSound("npc/scanner/scanner_nearmiss1.wav")
    end
end

function SWEP:PreDrop()
    self:SetZoom(false)
    self:SetIronsights(false)
    return self.BaseClass.PreDrop(self)
end

function SWEP:Reload()
    if self.Owner:KeyDown(IN_USE) then
        self:Defib()
      return
    else
       self:Fly()
    end



    if self:Clip1() < 1 then
    self.Weapon:DefaultReload( ACT_VM_RELOAD );
    self:SetIronsights( false )
    self:SetZoom(false)
    end
end


SWEP.NextBoom = 0

function SWEP:MakeExplosion(worldsnd)

    if self.NextBoom > CurTime() then return end
    self.NextBoom = (CurTime() + 1.5)
    if SERVER then
        local tr = self.Owner:GetEyeTrace()
        local ent = ents.Create( "env_explosion" )

        ent:SetPos( tr.HitPos  )
        ent:SetOwner( self.Owner  )
        ent:SetPhysicsAttacker(  self.Owner )
        ent:Spawn()
        ent:SetKeyValue( "iMagnitude", "40" )
        ent:Fire( "Explode", 0, 0 )

        util.BlastDamage( self, self:GetOwner(), self:GetPos(), 50, 75 )

        ent:EmitSound( "weapons/big_explosion.mp3" )
    end

  --  self:TakePrimaryAmmo( 1 )
    self.Owner:SetAnimation( PLAYER_ATTACK1 )
    local owner = self.Owner
    if not IsValid(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

    owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )
end



function SWEP:Holster()
    self:SetIronsights(false)
    self:SetZoom(false)
    return true
end

if CLIENT then
   local scope = surface.GetTextureID("sprites/scope")
   function SWEP:DrawHUD()
      if self:GetIronsights() then
         surface.SetDrawColor( 0, 0, 0, 255 )
         
         local x = ScrW() / 2.0
         local y = ScrH() / 2.0
         local scope_size = ScrH()

         -- crosshair
         local gap = 80
         local length = scope_size
         surface.DrawLine( x - length, y, x - gap, y )
         surface.DrawLine( x + length, y, x + gap, y )
         surface.DrawLine( x, y - length, x, y - gap )
         surface.DrawLine( x, y + length, x, y + gap )

         gap = 0
         length = 50
         surface.DrawLine( x - length, y, x - gap, y )
         surface.DrawLine( x + length, y, x + gap, y )
         surface.DrawLine( x, y - length, x, y - gap )
         surface.DrawLine( x, y + length, x, y + gap )


         -- cover edges
         local sh = scope_size / 2
         local w = (x - sh) + 2
         surface.DrawRect(0, 0, w, scope_size)
         surface.DrawRect(x + sh - 2, 0, w, scope_size)

         surface.SetDrawColor(255, 0, 0, 255)
         surface.DrawLine(x, y, x + 1, y + 1)

         -- scope
         surface.SetTexture(scope)
         surface.SetDrawColor(255, 255, 255, 255)

         surface.DrawTexturedRectRotated(x, y, scope_size, scope_size, 0)

      else
         return self.BaseClass.DrawHUD(self)
      end
   end

   function SWEP:AdjustMouseSensitivity()
      return (self:GetIronsights() and 0.2) or nil
   end
end
