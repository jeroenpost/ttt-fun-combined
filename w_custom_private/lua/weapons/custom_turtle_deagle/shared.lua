
SWEP.Base			= "gb_master_deagle"
SWEP.PrintName	= "TURTLE Deagle"
SWEP.Kind 			= WEAPON_PISTOL
SWEP.Slot = 1
SWEP.SlotPos 	= 1
SWEP.Primary.Recoil	= 1
SWEP.Primary.Damage = 55
SWEP.Primary.Delay 	= 0.45
SWEP.Primary.Cone 	= 0.01
SWEP.Primary.ClipSize 		= 40
SWEP.Primary.ClipMax 		= 120
SWEP.Primary.DefaultClip 	= 40
SWEP.Primary.NumShots		= 1
SWEP.Primary.Automatic 	= true
SWEP.HeadshotMultiplier = 3
SWEP.AmmoEnt 		= "item_ammo_revolver_ttt"
SWEP.Primary.Sound = Sound( "weapons/green_black1.mp3" )


SWEP.Headcrabs = 0
SWEP.NextSecond = 0

function SWEP:SecondaryAttack()
    if  self.NextSecond > CurTime() then return end


    self.NextSecond = CurTime() + 3

    if( self.Headcrabs > 5 ) then
        -- self:EmitSound( "weapons/green_black5.mp3" )
        return end
    self.Headcrabs = self.Headcrabs + 1


    local tr = self.Owner:GetEyeTrace()

    if  not SERVER then return end



    spos = tr.HitPos + self.Owner:GetAimVector() * -46


    local headturtle = self:SpawnNPC2(self.Owner, spos, "npc_headcrab")

    headturtle:SetNPCState(2)

    local turtle = ents.Create("prop_dynamic")
    turtle:SetModel("models/props/de_tides/vending_turtle.mdl")
    turtle:SetPos( spos )
    turtle:SetAngles(Angle(0,-90,0))
    turtle:SetParent(headturtle)

    --headturtle:SetCollisionGroup(COLLISION_GROUP_WEAPON)

    headturtle:SetNWEntity("Thrower", self.Owner)
    --headturtle:SetName(self:GetThrower():GetName())
    headturtle:SetNoDraw(true)
    headturtle:SetHealth(40)




end



function SWEP:SpawnNPC2( Player, Position, Class )

    local NPCList = list.Get( "NPC" )
    local NPCData = NPCList[ Class ]

    -- Don't let them spawn this entity if it isn't in our NPC Spawn list.
    -- We don't want them spawning any entity they like!
    if ( not NPCData ) then
        if ( IsValid( Player ) ) then
            Player:SendLua( "Derma_Message( \"Sorry! You can't spawn that NPC!\" )" );
        end
        return end

    local bDropToFloor = false

    --
    -- This NPC has to be spawned on a ceiling ( Barnacle )
    --
    if ( NPCData.OnCeiling and Vector( 0, 0, -1 ):Dot( Normal ) < 0.95 ) then
        return nil
    end

    if ( NPCData.NoDrop ) then bDropToFloor = false end

    --
    -- Offset the position
    --


    -- Create NPC
    local NPC = ents.Create( NPCData.Class )
    if ( not IsValid( NPC ) ) then return end

    NPC:SetPos( Position )
    --
    -- This NPC has a special model we want to define
    --
    if ( NPCData.Model ) then
        NPC:SetModel( NPCData.Model )
    end

    --
    -- Spawn Flags
    --
    local SpawnFlags = bit.bor( SF_NPC_FADE_CORPSE, SF_NPC_ALWAYSTHINK)
    if ( NPCData.SpawnFlags ) then SpawnFlags = bit.bor( SpawnFlags, NPCData.SpawnFlags ) end
    if ( NPCData.TotalSpawnFlags ) then SpawnFlags = NPCData.TotalSpawnFlags end
    NPC:SetKeyValue( "spawnflags", SpawnFlags )

    --
    -- Optional Key Values
    --
    if ( NPCData.KeyValues ) then
        for k, v in pairs( NPCData.KeyValues ) do
            NPC:SetKeyValue( k, v )
        end
    end

    --
    -- This NPC has a special skin we want to define
    --
    if ( NPCData.Skin ) then
        NPC:SetSkin( NPCData.Skin )
    end

    --
    -- What weapon should this mother be carrying
    --

    NPC:Spawn()
    NPC:Activate()

    if ( bDropToFloor and not NPCData.OnCeiling ) then
        NPC:DropToFloor()
    end

    return NPC
end