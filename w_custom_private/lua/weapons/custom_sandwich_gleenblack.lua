if SERVER then
   AddCSLuaFile(  )

--resource.AddFile("materials/models/weapons/v_models/green_black/greenblack1.vmt")
--resource.AddFile("materials/models/weapons/v_models/green_black/greenblack2.vmt")
--resource.AddFile("materials/models/weapons/v_models/green_black/greenblack.vmt")

--resource.AddFile("models/weapons/v_green_black.mdl")
--resource.AddFile("models/weapons/w_green_black.mdl")
--resource.AddFile("sound/weapons/green_black1.mp3")
--resource.AddFile("sound/weapons/green_black2.mp3")
--resource.AddFile("sound/weapons/green_black3.mp3")
--resource.AddFile("sound/weapons/green_black4.mp3")
--resource.AddFile("sound/weapons/green_black5.mp3")
--resource.AddFile("materials/vgui/ttt_fun_killicons/green_black.png")

end
   
SWEP.HoldType			= "pistol"
SWEP.PrintName			= "Gleen Black"
SWEP.Category			= "TTT-FUN"
SWEP.Slot				= 15

if CLIENT then
   			
   SWEP.Author				= "GreenBlack"
   SWEP.SlotPos			= 2
   SWEP.Icon = "vgui/ttt_fun_killicons/green_black.png"
	killicon.Add( "green_black", "vgui/spawnicons", color_white )
	SWEP.WepSelectIcon = Material( "greenblack.png" )
	SWEP.BounceWeaponIcon = false
	SWEP.DrawWeaponInfoBox = false
end

   SWEP.EquipMenuData = {
      name = "NyanGun",
      type = "item_weapon",
      desc = "Shoots Nyan Cats"
   };
SWEP.Base				= "aa_base"

SWEP.Spawnable = true
SWEP.AdminSpawnable = true

SWEP.Kind = 16
SWEP.WeaponID = AMMO_DEAGLE

SWEP.Primary.Ammo       = "AlyxGun" 
SWEP.Primary.Recoil			= 0.05
SWEP.Primary.Damage = 12
SWEP.Primary.Delay = 0.1
SWEP.Primary.Cone = 0.008
SWEP.Primary.ClipSize = 100
SWEP.Primary.ClipMax = 180
SWEP.Primary.DefaultClip = 100
SWEP.Primary.Automatic = true

SWEP.HeadshotMultiplier = 1.5

SWEP.Secondary.Ammo       = "AlyxGun" 
SWEP.Secondary.Recoil			= 0.05
SWEP.Secondary.Damage = 1
SWEP.Secondary.Delay = 1
SWEP.Secondary.Cone = 0.08
SWEP.Secondary.ClipSize = 3
SWEP.Secondary.ClipMax = 3
SWEP.Secondary.DefaultClip = 3
SWEP.Secondary.Automatic = false

SWEP.AutoSpawnable      = false
SWEP.AmmoEnt = "item_ammo_revolver_ttt"
SWEP.Primary.Sound = Sound( "weapons/green_black1.mp3" )

SWEP.UseHands			= true
SWEP.ViewModelFlip		= true
SWEP.ViewModelFOV		= 75
SWEP.ViewModel			= "models/weapons/v_green_black.mdl"
SWEP.WorldModel			= "models/weapons/w_green_black.mdl"

SWEP.IronSightsPos = Vector(5.14, -2, 2.5)
SWEP.IronSightsAng = Vector(0, 0, 0)
--SWEP.IronSightsPos 	= Vector( 3.8, -1, 3.6 )
--SWEP.ViewModelFOV = 49

function SWEP:PrimaryAttack(worldsnd)

    if self.Owner:KeyDown(IN_USE) then
        self.Weapon:SetNextPrimaryFire( CurTime() + 1.5 )
        self.Weapon:EmitSound( Sound( "weapons/blackgreen1.mp3" ) )
        self:ShootBullet( 90, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )
        return
    end

   self.Weapon:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
   self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )

   if not self:CanPrimaryAttack() then return end

   if not worldsnd then
      self.Weapon:EmitSound( self.Primary.Sound, self.Primary.SoundLevel )
   elseif SERVER then
      sound.Play(self.Primary.Sound, self:GetPos(), self.Primary.SoundLevel)
   end

   self:ShootBullet( self.Primary.Damage, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )

    
   self:TakePrimaryAmmo( 1 )

   local owner = self.Owner 
   if not IsValid(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

   owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )
end

SWEP.Headcrabs = 0
SWEP.NextSecond = 0

function SWEP:SecondaryAttack()
    if self.Owner:KeyDown(IN_USE) then
        self:hax()
        return
    end
    if  self.NextSecond > CurTime() then return end
    if not self:CanPrimaryAttack() then return end
       self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
   self.Weapon:SetNextSecondaryFire( CurTime() + self.Secondary.Delay )

    self.NextSecond = CurTime() + 5

    if( self.Headcrabs > 0 ) then
        self:EmitSound( "weapons/green_black5.mp3" )
    return end
    self.Headcrabs = 1
    self:EmitSound( "weapons/green_black2.mp3" )

    local tr = self.Owner:GetEyeTrace()
   
    if  not SERVER then return end
    
    local ent = ents.Create( "npc_fastzombie" )
    ent:SetPos( tr.HitPos + self.Owner:GetAimVector() * -46 + Vector(0,0,50) )
    --ent:SetAngles( tr.HitNormal:Angle() )
    ent:SetHealth( 1 )
    ent:SetPhysicsAttacker(self.Owner)
   -- ent:SetMaterial("camos/camo7")
    ent:Spawn()
    ent:SetHealth( 1 )
    ent:SetModelScale(ent:GetModelScale() * 2, 10)
    
end

SWEP.NextReload = 0
SWEP.Bottles = 0

function SWEP:Reload()

    if  self.NextReload > CurTime() then return end
       self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
   self.Weapon:SetNextSecondaryFire( CurTime() + self.Secondary.Delay )
    
    timer.Create("giveAmmoWaterBottleGreen",1,0,function()
        if IsValid(self) then
            self:SetClip1( self:Clip1() + 1 )
        end
    end)

     self.NextReload = CurTime() + 3
    
    
    self:ShootEffects( self )
    
    if( self.Bottles > 4 ) then
        self:EmitSound( "weapons/green_black4.mp3" )
    return end

    self:EmitSound( "weapons/green_black3.mp3" )
local tr = self.Owner:GetEyeTrace()
    self.Bottles = self.Bottles + 1


    if (!SERVER) then return end

    local ent1 = ents.Create("prop_physics") 
	local ang = Vector(0,0,1):Angle();
	ang.pitch = ang.pitch + 90;
	ang:RotateAroundAxis(ang:Up(), math.random(0,360))
	ent1:SetAngles(ang)
	ent1:SetModel("models/weapons/w_knife_ct.mdl")
    ent1:SetMaterial("camos/camo7")
	--local pos = position
    ent1:SetNWInt("gleenblack", 1)

	--pos.z = pos.z - ent1:OBBMaxs().z
	ent1:SetPos( tr.HitPos + self.Owner:GetAimVector() * -46  )
	ent1:Spawn()
    ent1:SetModelScale(ent1:GetModelScale() * 1.8, 3)
    local owner = self.Owner
    timer.Create("explodingknife", 5,1,function()
        if IsValid(ent1) and IsValid(owner) then
            local ent = ents.Create( "env_explosion" )

            ent:SetPos( ent1:GetPos()  )
            ent:SetOwner( self.Owner  )
            ent:SetPhysicsAttacker(  self.Owner )
            ent:Spawn()
            ent:SetKeyValue( "iMagnitude", 30 )
            ent:Fire( "Explode", 0, 0 )

        end
    end)
end



-- Power Hit
SWEP.NextPowerHit = 0
function SWEP:hax()
    if self.NextPowerHit > CurTime() then return end
    self.NextPowerHit = CurTime() + 0.3

    local tr = util.TraceLine( {
        start = self.Owner:GetShootPos(),
        endpos = self.Owner:GetShootPos() + self.Owner:GetAimVector() * 120,
        filter = self.Owner
    } )

    if ( not IsValid( tr.Entity ) ) then
        tr = util.TraceHull( {
            start = self.Owner:GetShootPos(),
            endpos = self.Owner:GetShootPos() + self.Owner:GetAimVector() * 420,
            filter = self.Owner,
            mins = self.Owner:OBBMins() / 3,
            maxs = self.Owner:OBBMaxs() / 3
        } )
    end

    if ( tr.Hit ) then self.Owner:EmitSound( "Flesh.ImpactHard" ) end

    if ( IsValid( tr.Entity )  and SERVER  ) then
        self.NextPowerHit = CurTime() + 5
        tr.Entity:TakeDamage( 25, self.Owner )
        tr.Entity.hasProtectionSuitTemp = true
        tr.Entity:SetVelocity(self.Owner:GetForward() * 12000 + Vector(0,0,800))
    end
end