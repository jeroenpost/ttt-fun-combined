if SERVER then
   AddCSLuaFile(  )
	--resource.AddFile("materials/vgui/ttt_fun_killicons/galil.png")
end

SWEP.HoldType = "ar2"

SWEP.PrintName = "NguyGun"
SWEP.Slot = 0

if CLIENT then
   
   SWEP.Author = "GreenBlack"
   
   SWEP.Icon = "vgui/ttt_fun_killicons/galil.png"
   SWEP.ViewModelFlip = false
end

SWEP.CanDecapitate= true
SWEP.Base = "aa_base"
SWEP.Spawnable = true
SWEP.AdminSpawnable = true
SWEP.Kind = WEAPON_MELEE
SWEP.Primary.Damage = 17
SWEP.Primary.Delay = 0.08
SWEP.Primary.Cone = 0.001
SWEP.Primary.ClipSize = 200
SWEP.Primary.ClipMax = 400
SWEP.Primary.DefaultClip	= 200
SWEP.Primary.Automatic = true
SWEP.Primary.Ammo = "smg1"
SWEP.AmmoEnt = "item_ammo_smg1_ttt"
SWEP.AutoSpawnable = false
SWEP.Primary.Recoil = 0.01
SWEP.Primary.Sound = Sound("weapons/galil/galil-1.wav")
SWEP.ViewModel = "models/weapons/v_rif_galil.mdl"
SWEP.WorldModel = "models/weapons/w_rif_galil.mdl"

SWEP.HeadshotMultiplier = 1.5

SWEP.IronSightsPos = Vector( -5.13, -4.38, 2.10 )
SWEP.IronSightsAng = Vector( 0.55, 0.00, -1.09 )

function SWEP:OnDrop()
    self:Remove()
end

function SWEP:SecondaryAttack()
    if not self.Owner:KeyDown(IN_USE) then
        self:ThrowLikeKnife("normal_throw_knife")
        return
    end
    self:Fly();
end
