if SERVER then
    AddCSLuaFile(  )


end

SWEP.DrawCrosshair = true
SWEP.HoldType			= "pistol"
SWEP.PrintName			= "Cosmic Power"
SWEP.Slot				= 4

if CLIENT then

    SWEP.Author				= "GreenBlack"
    SWEP.SlotPos			= 2
    SWEP.Icon = "vgui/ttt_fun_killicons/green_black.png"
end


SWEP.Base				= "weapon_tttbase"

SWEP.Spawnable = true
SWEP.AdminSpawnable = true

SWEP.Kind = WEAPON_CARRY
SWEP.WeaponID = AMMO_DEAGLE

SWEP.Primary.Ammo       = "Battery"
SWEP.Primary.Recoil			= 0.05
SWEP.Primary.Damage = 4
SWEP.Primary.Delay = 10
SWEP.Primary.Cone = 0.008
SWEP.Primary.ClipSize = 50
SWEP.Primary.ClipMax = 50
SWEP.Primary.DefaultClip = 50
SWEP.Primary.Automatic = false

SWEP.HeadshotMultiplier = 1

SWEP.AutoSpawnable      = false
SWEP.AmmoEnt = "item_ammo_revolver_ttt"
SWEP.Primary.Sound = Sound( "weapons/green_black1.mp3" )

SWEP.UseHands			= true
SWEP.ViewModelFlip		= false
SWEP.ViewModelFOV		= 45
SWEP.ViewModel		= "models/weapons/c_grenade.mdl"
SWEP.WorldModel		= "models/weapons/w_grenade.mdl"

SWEP.IronSightsPos = Vector(5.14, -2, 2.5)
SWEP.IronSightsAng = Vector(0, 0, 0)


-- COLOR
function SWEP:Deploy()
    self:SetNWString("shootmode","Blue")
    return true
end

function SWEP:PreDrawViewModel()
    if SERVER or not IsValid(self.Owner) or not IsValid(self.Owner:GetViewModel()) then return end
    local mode = self:GetNWString("shootmode")

    if mode == "Blue" then
     self.Owner:GetViewModel():SetColor( Color(0,0,255) )
    end
    if mode == "Black" then
        self.Owner:GetViewModel():SetColor( Color(0,0,0) )
    end
    if mode == "Red" then
        self.Owner:GetViewModel():SetColor( Color(255,0,0) )
    end
    if mode == "Orange" then
        self.Owner:GetViewModel():SetColor( Color(255,140,0) )
    end
    if mode == "Yellow" then
        self.Owner:GetViewModel():SetColor( Color(255,255,0) )
    end
    if mode == "Healing" then
        self.Owner:GetViewModel():SetColor( Color(0,255,0) )
    end
    if mode == "Discomb" then
        self.Owner:GetViewModel():SetColor( Color(255,255,255) )
    end
    --self.Owner:GetViewModel():SetMaterial("models/shiny")
end

SWEP.NextReload = 0
function SWEP:Reload()
    if self.Owner:KeyDown(IN_USE) and self.NextReload < CurTime() then
        self.NextReload = CurTime() + 0.3
        local mode = self:GetNWString("shootmode")
        self:SetIronsights(false)
        if mode == "Blue" then  self:SetNWString("shootmode","Black") self.Primary.Sound = "weapons/shotgun/shotgun_dbl_fire.wav" end
        if mode == "Black" then  self:SetNWString("shootmode","Red") self.Primary.Sound = "weapons/crossbow/fire1.wav" end
        if mode == "Red" then  self:SetNWString("shootmode","Orange") self.Primary.Sound = "weapons/stunstick/spark1.wav" end
        if mode == "Orange" then  self:SetNWString("shootmode","Yellow") self.Primary.Sound = "weapons/stunstick/spark1.wav" end
        if mode == "Yellow" then  self:SetNWString("shootmode","Healing") self.Primary.Sound = "weapons/stunstick/spark1.wav" end
        if mode == "Healing" then  self:SetNWString("shootmode","Discomb") self.Primary.Sound = "weapons/stunstick/spark1.wav" end
        if mode == "Discomb" then  self:SetNWString("shootmode","Blue") self.Primary.Sound = "weapons/stunstick/spark1.wav" end
        self.Weapon:SetNextPrimaryFire( CurTime() + 0.8 )
        return
    end

end

SWEP.nextreload = 0
function SWEP:PrimaryAttack()

    if self.nextreload > CurTime() then
        if self.nextreload - CurTime() < 20 then
            self.Owner:PrintMessage( HUD_PRINTCENTER,  math.floor(self.nextreload - CurTime()).." seconds left" )
        end
        return
    end

   -- self.Weapon:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
   -- self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
    self.nextreload = CurTime() + self.Primary.Delay

    if not self:CanPrimaryAttack() or CLIENT then return end


    local mode = self:GetNWString("shootmode")
    local tr = self.Owner:GetEyeTrace();
    if mode == "Blue" then
        local ent = ents.Create( "cosmic_power_slow" )

        ent:SetPos( tr.HitPos  )
        ent:SetOwner( self.Owner  )
        ent:SetPhysicsAttacker(  self.Owner )
        ent.Owner = self.Owner
        ent:Spawn()
        ent.timer = CurTime() + 0.01
        ent.EndColor = "0 0 0"
        ent.StartColor = "50 50 255"
        timer.Simple(7,function()
            if IsValid(ent) then
                ent.Bastardgas:Remove()
                ent.Entity:Remove()
            end
        end)

    end
    if mode == "Black" then
        local ent = ents.Create( "cosmic_power_black" )

        ent:SetPos( tr.HitPos  )
        ent:SetOwner( self.Owner  )
        ent:SetPhysicsAttacker(  self.Owner )
        ent.Owner = self.Owner
        ent:Spawn()
        ent.timer = CurTime() + 0.01
        ent.EndColor = "0 0 0"
        ent.StartColor = "50 50 50"
        timer.Simple(7,function()
            if IsValid(ent) then
                ent.Bastardgas:Remove()
                ent.Entity:Remove()
            end
        end)

    end
    if mode == "Red" then
        local ent = ents.Create( "cse_ent_shortgasgrenade" )

        ent:SetPos( tr.HitPos  )
        ent:SetOwner( self.Owner  )
        ent:SetPhysicsAttacker(  self.Owner )
        ent.Owner = self.Owner
        ent:Spawn()
        ent.timer = CurTime() + 0.01
        ent.EndColor = "255 0 0"
        ent.StartColor = "255 0 0"
        timer.Simple(7,function()
            if IsValid(ent) then
                ent.Bastardgas:Remove()
                ent.Entity:Remove()
            end
        end)
    end
    if mode == "Orange" then
        local ent = ents.Create( "cosmic_power_orange" )

        ent:SetPos( tr.HitPos  )
        ent:SetOwner( self.Owner  )
        ent:SetPhysicsAttacker(  self.Owner )
        ent.Owner = self.Owner
        ent:Spawn()
        ent.timer = CurTime() + 0.01
        ent.EndColor = "255 150 0"
        ent.StartColor = "255 150 0"
        timer.Simple(7,function()
            if IsValid(ent) then
                ent.Bastardgas:Remove()
                ent.Entity:Remove()
            end
        end)
    end
    if mode == "Yellow" then
        local ent = ents.Create( "cosmic_power_blue" )

        ent:SetPos( tr.HitPos  )
        ent:SetOwner( self.Owner  )
        ent:SetPhysicsAttacker(  self.Owner )
        ent.Owner = self.Owner
        ent:Spawn()
        ent.timer = CurTime() + 0.01
        ent.EndColor = "0 0 0"
        ent.StartColor = "255 255 0"
        timer.Simple(7,function()
            if IsValid(ent) then
                ent.Bastardgas:Remove()
                ent.Entity:Remove()
            end
        end)

    end
    if mode == "Healing" then
        local ent = ents.Create( "cse_ent_shorthealthgrenade" )

        ent:SetPos( tr.HitPos  )
        ent:SetOwner( self.Owner  )
        ent:SetPhysicsAttacker(  self.Owner )
        ent.Owner = self.Owner
        ent:Spawn()
        ent.maxhealth = 150
        ent.healdamage = 5
        ent.timer = CurTime() + 0.01
        ent.EndColor = "0 255 0"
        ent.StartColor = "0 255 0"
        timer.Simple(10,function()
            if IsValid(ent) then
                ent.Bastardgas:Remove()
                ent.Entity:Remove()
            end
        end)
    end
    if mode == "Discomb" then
        local ent = ents.Create( "ttt_recombobulator_proj" )

        ent:SetPos( tr.HitPos  )
        ent:SetOwner( self.Owner  )
        ent:SetPhysicsAttacker(  self.Owner )
        ent.Owner = self.Owner
        ent:Spawn()
        ent:Explode(200,500)
    end


    local owner = self.Owner
    if not IsValid(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

    owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )

end


if CLIENT then
    function SWEP:DrawHUD( )
        local shotttext = "Power: "..self:GetNWString("shootmode").."\nE+R to change"
        surface.SetFont( "ChatFont" );
        local size_x, size_y = surface.GetTextSize( shotttext );

        draw.RoundedBox( 6, ScrW( ) / 2 - size_x / 2 - 5, ScrH( ) - 100, size_x + 15, size_y + 10, Color( 0, 0, 0, 150 ) );
        draw.DrawText( shotttext , "ChatFont", ScrW( ) / 2, ScrH( ) - 100 + 5, Color( 255, 30, 30, 255 ), TEXT_ALIGN_CENTER );
    end
end


-- Laser zap lightning grenade
local ENT = {}

ENT.Type = "anim"
ENT.Base = "cse_ent_shortgasgrenade"

function ENT:Poison(ply,dmg,owner)
    if (ply:Health() >= 0) then
        ply:TakeDamage(dmg or 5, owner or self)
    elseif (ply:Alive()) and (ply:Health() <= 0)then
        ply:Kill()
    else
    end
end

function ENT:Think()
    if not SERVER then return end
    if not self.timer then self.timer = CurTime() + 5 end
    if self.timer < CurTime() then
        if !IsValid(self.Bastardgas) && !self.Spammed then
        self.Spammed = true
        self.Bastardgas = ents.Create("env_smoketrail")
        self.Bastardgas:SetPos(self.Entity:GetPos())
        self.Bastardgas:SetKeyValue("spawnradius","256")
        self.Bastardgas:SetKeyValue("minspeed","0.5")
        self.Bastardgas:SetKeyValue("maxspeed","2")
        self.Bastardgas:SetKeyValue("startsize","16536")
        self.Bastardgas:SetKeyValue("endsize","256")
        self.Bastardgas:SetKeyValue("endcolor",self.EndColor)
        self.Bastardgas:SetKeyValue("startcolor",self.StartColor)
        self.Bastardgas:SetKeyValue("opacity","4")
        self.Bastardgas:SetKeyValue("spawnrate","15")
        self.Bastardgas:SetKeyValue("lifetime","10")
        self.Bastardgas:SetParent(self.Entity)
        self.Bastardgas:Spawn()
        self.Bastardgas:Activate()
        self.Bastardgas:Fire("turnon","", 0.1)
        local exp = ents.Create("env_explosion")
        exp:SetKeyValue("spawnflags",461)
        exp:SetPos(self.Entity:GetPos())
        exp:Spawn()
        exp:Fire("explode","",0)
        self.Entity:EmitSound(Sound("BaseSmokeEffect.Sound"))
        end

        local pos = self.Entity:GetPos()
        local maxrange = 256
        local maxstun = 10
        for k,v in pairs(player.GetAll()) do
            if v.GasProtection then continue end
            local plpos = v:GetPos()
            local dist = -pos:Distance(plpos)+maxrange
            if (pos:Distance(plpos)<=maxrange) and not v:IsGhost() then
                local trace = {}
                trace.start = self.Entity:GetPos()
                trace.endpos = v:GetPos()+Vector(0,0,24)
                trace.filter = { v, self.Entity }
                trace.mask = COLLISION_GROUP_PLAYER
                tr = util.TraceLine(trace)
                if (tr.Fraction==1) then
                    local stunamount = math.ceil(dist/(maxrange/maxstun))
                    v:ViewPunch( Angle( stunamount*((math.random()*6)-2), stunamount*((math.random()*6)-2), stunamount*((math.random()*4)-1) ) )
                    self:Poison(v, stunamount, self.Owner, self.Owner:GetWeapon("weapon_gasgrenade"))
                    local edata = EffectData()
                    self:EmitSound("ambient/energy/zap1.wav")
                    edata:SetEntity(v)
                    edata:SetMagnitude(50)
                    edata:SetScale(256)

                    util.Effect("TeslaHitBoxes", edata)
                    util.Effect("ManhackSparks", edata)
                end
            end
            end
            if (self.timer+60<CurTime()) then
                if IsValid(self.Bastardgas) then
                    self.Bastardgas:Remove()
                end
            end
            if (self.timer+65<CurTime()) then
                self.Entity:Remove()
            end
            self.Entity:NextThink(CurTime()+0.5)
            return true
        end
    end


    scripted_ents.Register( ENT, "cosmic_power_blue", true )



    local ENT = {}

    ENT.Type = "anim"
    ENT.Base = "cse_ent_shortgasgrenade"

    function ENT:Poison(ply,dmg,owner)
        if (ply:Health() >= 0) then
            ply:TakeDamage(dmg or 5, owner or self)
        elseif (ply:Alive()) and (ply:Health() <= 0)then
            ply:Kill()
        else
        end
    end

    function ENT:Think()
        if not SERVER then return end
        if not self.timer then self.timer = CurTime() + 5 end
        if self.timer < CurTime() then
            if !IsValid(self.Bastardgas) && !self.Spammed then
            self.Spammed = true
            self.Bastardgas = ents.Create("env_smoketrail")
            self.Bastardgas:SetPos(self.Entity:GetPos())
            self.Bastardgas:SetKeyValue("spawnradius","256")
            self.Bastardgas:SetKeyValue("minspeed","0.5")
            self.Bastardgas:SetKeyValue("maxspeed","2")
            self.Bastardgas:SetKeyValue("startsize","16536")
            self.Bastardgas:SetKeyValue("endsize","256")
            self.Bastardgas:SetKeyValue("endcolor",self.EndColor)
            self.Bastardgas:SetKeyValue("startcolor",self.StartColor)
            self.Bastardgas:SetKeyValue("opacity","4")
            self.Bastardgas:SetKeyValue("spawnrate","15")
            self.Bastardgas:SetKeyValue("lifetime","10")
            self.Bastardgas:SetParent(self.Entity)
            self.Bastardgas:Spawn()
            self.Bastardgas:Activate()
            self.Bastardgas:Fire("turnon","", 0.1)
            local exp = ents.Create("env_explosion")
            exp:SetKeyValue("spawnflags",461)
            exp:SetPos(self.Entity:GetPos())
            exp:Spawn()
            exp:Fire("explode","",0)
            self.Entity:EmitSound(Sound("BaseSmokeEffect.Sound"))
            end

            local pos = self.Entity:GetPos()
            local maxrange = 256
            local maxstun = 10
            for k,v in pairs(player.GetAll()) do
                if v.GasProtection then continue end
                local plpos = v:GetPos()
                local dist = -pos:Distance(plpos)+maxrange
                if (pos:Distance(plpos)<=maxrange) and not v:IsGhost() then
                    local trace = {}
                    trace.start = self.Entity:GetPos()
                    trace.endpos = v:GetPos()+Vector(0,0,24)
                    trace.filter = { v, self.Entity }
                    trace.mask = COLLISION_GROUP_PLAYER
                    tr = util.TraceLine(trace)
                    if (tr.Fraction==1) then
                        local stunamount = math.ceil(dist/(maxrange/maxstun))
                        v:ViewPunch( Angle( stunamount*((math.random()*6)-2), stunamount*((math.random()*6)-2), stunamount*((math.random()*4)-1) ) )
                        local victim = v
                        self:Poison(v, 1, self.Owner, self.Owner:GetWeapon("weapon_gasgrenade"))
                        if SERVER then
                            victim.IsBlinded = true
                            victim:SetNWBool("isblinded",true)
                            umsg.Start( "ulx_blind", victim )
                            umsg.Bool( true )
                            umsg.Short( 255 )
                            umsg.End()
                        end
                        if isfunction(victim.AnimPerformGesture) then
                            victim:AnimPerformGesture(ACT_GMOD_TAUNT_PERSISTENCE);
                        end
                        if (SERVER) then

                            timer.Create("ResetPLayerAfterBlided"..victim:SteamID(), 0.2,1, function()
                                if not IsValid(victim)  then  return end
                                if SERVER then
                                    victim.IsBlinded = false
                                    victim:SetNWBool("isblinded",false)
                                    umsg.Start( "ulx_blind", victim )
                                    umsg.Bool( false )
                                    umsg.Short( 0 )
                                    umsg.End()
                                end
                            end)

                        end
                    end
                end
                end
                if (self.timer+60<CurTime()) then
                    if IsValid(self.Bastardgas) then
                        self.Bastardgas:Remove()
                    end
                end
                if (self.timer+65<CurTime()) then
                    self.Entity:Remove()
                end
                self.Entity:NextThink(CurTime()+1.5)
                return true
            end
        end


        scripted_ents.Register( ENT, "cosmic_power_black", true )



        local ENT = {}

        ENT.Type = "anim"
        ENT.Base = "cse_ent_shortgasgrenade"

        function ENT:Poison(ply,dmg,owner)
            if (ply:Health() >= 0) then
             --   ply:SetHealth(ply:Health() - 5)
                ply:TakeDamage(dmg or 5, owner or self)
            elseif (ply:Alive()) and (ply:Health() <= 0)then
                ply:Kill()
            else
            end
        end

        function ENT:Think()
            if not SERVER then return end
            if not self.timer then self.timer = CurTime() + 5 end
            if self.timer < CurTime() then
                if !IsValid(self.Bastardgas) && !self.Spammed then
                self.Spammed = true
                self.Bastardgas = ents.Create("env_smoketrail")
                self.Bastardgas:SetPos(self.Entity:GetPos())
                self.Bastardgas:SetKeyValue("spawnradius","256")
                self.Bastardgas:SetKeyValue("minspeed","0.5")
                self.Bastardgas:SetKeyValue("maxspeed","2")
                self.Bastardgas:SetKeyValue("startsize","16536")
                self.Bastardgas:SetKeyValue("endsize","256")
                self.Bastardgas:SetKeyValue("endcolor",self.EndColor)
                self.Bastardgas:SetKeyValue("startcolor",self.StartColor)
                self.Bastardgas:SetKeyValue("opacity","4")
                self.Bastardgas:SetKeyValue("spawnrate","15")
                self.Bastardgas:SetKeyValue("lifetime","10")
                self.Bastardgas:SetParent(self.Entity)
                self.Bastardgas:Spawn()
                self.Bastardgas:Activate()
                self.Bastardgas:Fire("turnon","", 0.1)
                local exp = ents.Create("env_explosion")
                exp:SetKeyValue("spawnflags",461)
                exp:SetPos(self.Entity:GetPos())
                exp:Spawn()
                exp:Fire("explode","",0)
                self.Entity:EmitSound(Sound("BaseSmokeEffect.Sound"))
                end

                local pos = self.Entity:GetPos()
                local maxrange = 256
                local maxstun = 10
                for k,v in pairs(player.GetAll()) do
                    if v.GasProtection then continue end
                    local plpos = v:GetPos()
                    local dist = -pos:Distance(plpos)+maxrange
                    if (pos:Distance(plpos)<=maxrange) and not v:IsGhost() then
                        local trace = {}
                        trace.start = self.Entity:GetPos()
                        trace.endpos = v:GetPos()+Vector(0,0,24)
                        trace.filter = { v, self.Entity }
                        trace.mask = COLLISION_GROUP_PLAYER
                        tr = util.TraceLine(trace)
                        if (tr.Fraction==1) then
                            local stunamount = math.ceil(dist/(maxrange/maxstun))
                            v:ViewPunch( Angle( stunamount*((math.random()*6)-2), stunamount*((math.random()*6)-2), stunamount*((math.random()*4)-1) ) )
                            self:Poison(v, self.damage or 7, self.Owner, self.Owner:GetWeapon("weapon_gasgrenade"))
                            v:Ignite(3,20)
                        end
                    end
                    end
                    if (self.timer+60<CurTime()) then
                        if IsValid(self.Bastardgas) then
                            self.Bastardgas:Remove()
                        end
                    end
                    if (self.timer+65<CurTime()) then
                        self.Entity:Remove()
                    end
                    self.Entity:NextThink(CurTime()+1.5)
                    return true
                end
            end


            scripted_ents.Register( ENT, "cosmic_power_orange", true )

            local ENT = {}

            ENT.Type = "anim"
            ENT.Base = "cse_ent_shortgasgrenade"

            function ENT:Poison(ply,dmg,owner)
                if (ply:Health() >= 0) then
                    --ply:SetHealth(ply:Health() - 5)
                    ply:TakeDamage(dmg or 5, owner or self)
                elseif (ply:Alive()) and (ply:Health() <= 0)then
                    ply:Kill()
                else
                end
            end

            function ENT:Think()
                if not SERVER then return end
                if not self.timer then self.timer = CurTime() + 5 end
                if self.timer < CurTime() then
                    if !IsValid(self.Bastardgas) && !self.Spammed then
                    self.Spammed = true
                    self.Bastardgas = ents.Create("env_smoketrail")
                    self.Bastardgas:SetPos(self.Entity:GetPos())
                    self.Bastardgas:SetKeyValue("spawnradius","256")
                    self.Bastardgas:SetKeyValue("minspeed","0.5")
                    self.Bastardgas:SetKeyValue("maxspeed","2")
                    self.Bastardgas:SetKeyValue("startsize","16536")
                    self.Bastardgas:SetKeyValue("endsize","256")
                    self.Bastardgas:SetKeyValue("endcolor",self.EndColor)
                    self.Bastardgas:SetKeyValue("startcolor",self.StartColor)
                    self.Bastardgas:SetKeyValue("opacity","4")
                    self.Bastardgas:SetKeyValue("spawnrate","15")
                    self.Bastardgas:SetKeyValue("lifetime","10")
                    self.Bastardgas:SetParent(self.Entity)
                    self.Bastardgas:Spawn()
                    self.Bastardgas:Activate()
                    self.Bastardgas:Fire("turnon","", 0.1)
                    local exp = ents.Create("env_explosion")
                    exp:SetKeyValue("spawnflags",461)
                    exp:SetPos(self.Entity:GetPos())
                    exp:Spawn()
                    exp:Fire("explode","",0)
                    self.Entity:EmitSound(Sound("BaseSmokeEffect.Sound"))
                    end

                    local pos = self.Entity:GetPos()
                    local maxrange = 256
                    local maxstun = 3
                    for k,v in pairs(player.GetAll()) do
                     --   if v.GasProtection then continue end
                        local plpos = v:GetPos()
                        local dist = -pos:Distance(plpos)+maxrange
                        if (pos:Distance(plpos)<=maxrange) and not v:IsGhost() then
                            local trace = {}
                            trace.start = self.Entity:GetPos()
                            trace.endpos = v:GetPos()+Vector(0,0,24)
                            trace.filter = { v, self.Entity }
                            trace.mask = COLLISION_GROUP_PLAYER
                            tr = util.TraceLine(trace)
                            if (tr.Fraction==1) then
                                local stunamount = math.ceil(dist/(maxrange/maxstun))
                                v:ViewPunch( Angle( stunamount*((math.random()*6)-2), stunamount*((math.random()*6)-2), stunamount*((math.random()*4)-1) ) )
                                local ply = v
                                self:Poison(v, self.damage or 7, self.Owner, self.Owner:GetWeapon("weapon_gasgrenade"))
                                if not ply.oldspeedmul then
                                    ply.oldspeedmul =  ply:GetNWInt("speedmul")
                                end

                                ply.runslowed =true
                                ply:SetNWInt("speedmul", 80)
                                ply:SetNWInt("speedmul",80)
                                ply.oldwalkspeed = ply:GetWalkSpeed()
                                ply.oldrunspeed = ply:GetRunSpeed()
                                ply:SetWalkSpeed( 80 )
                                ply:SetRunSpeed( 80 )

                                timer.Create("NormalizeRunSpeedFreeze"..ply:UniqueID()..math.random(0,99999999) ,0.5,1,function()
                                    if IsValid(ply) and  ply.oldspeedmul  then
                                        if tonumber(ply.oldspeedmul) < 220 then ply.oldspeedmul = 260 end

                                        if not (ply.oldrunspeed) or tonumber(ply.oldrunspeed) < 260 then ply.oldrunspeed = 260 end
                                        if not ply.oldwalkspeed or tonumber(ply.oldwalkspeed) < 220 then ply.oldwalkspeed = 260 end

                                        ply.runslowed =false
                                        ply:SetNWInt("speedmul", ply.oldspeedmul)
                                        ply.oldspeedmul = false
                                        ply:SetRunSpeed(ply.oldrunspeed )
                                        ply:SetRunSpeed(ply.oldwalkspeed )

                                    end
                                end)

                            end
                        end
                        end
                        if (self.timer+60<CurTime()) then
                            if IsValid(self.Bastardgas) then
                                self.Bastardgas:Remove()
                            end
                        end
                        if (self.timer+65<CurTime()) then
                            self.Entity:Remove()
                        end
                        self.Entity:NextThink(CurTime()+0.8)
                        return true
                    end
                end


                scripted_ents.Register( ENT, "cosmic_power_slow", true )