

if ( SERVER ) then

	AddCSLuaFile(  )

	
end



	SWEP.PrintName			= "Manbearphoenix's Fury"
	SWEP.Author				= "Babel Industries (Model by Defuse)"
    SWEP.Contact            = "";
    SWEP.Instructions       = "Electro Magnetic Shelless Slug Shotgun"
	SWEP.Category = "Babel Industries"




SWEP.Base				= "aa_base"
SWEP.Spawnable			= true
SWEP.AdminSpawnable		= false
SWEP.Icon 				= "vgui/entities/weapon_emsss"
SWEP.HoldType = "shotgun"
SWEP.ViewModelFOV = 71
SWEP.ViewModel = "models/weapons/v_shot_m3super90.mdl"
SWEP.WorldModel = "models/weapons/w_shot_m3super90.mdl"
SWEP.ShowViewModel = true
SWEP.ShowWorldModel = true
SWEP.ViewModelBoneMods = {}

function SWEP:Deploy()
   -- self.Owner:EmitSound( "weapons/barret_arm_draw.wav" ) ;
	self.Weapon:SendWeaponAnim( ACT_VM_DRAW )
	self:SetHoldType( self.HoldType or "pistol" )
    self:SetDeploySpeed( self.Weapon:SequenceDuration(0.2) )
	return true;
end

SWEP.CanDecapitate= true
SWEP.Weight				= 5
SWEP.AutoSwitchTo		= false
SWEP.AutoSwitchFrom		= false
SWEP.AutoSpawnable 		= false
SWEP.Kind				= WEAPON_ROLE
SWEP.Primary.Sound			= Sound( "weapons/m3/s3-1.mp3" )
SWEP.Primary.Recoil			= 0.1
SWEP.Primary.Damage			= 6
SWEP.Primary.NumShots		= 19
SWEP.Primary.Cone			= 0.09
SWEP.Primary.ClipSize		= 12
SWEP.Primary.Delay			= 0.55
SWEP.Primary.DefaultClip	= 65
SWEP.Primary.Automatic		= true
SWEP.Primary.Ammo			= "buckshot"
SWEP.AmmoEnt = "item_box_buckshot_ttt"

SWEP.Slot				= 7							// Slot in the weapon selection menu
SWEP.SlotPos			= 1							// Position in the slot

SWEP.Secondary.ClipSize		= -1
SWEP.Secondary.DefaultClip	= -1
SWEP.Secondary.Automatic	= false
SWEP.Secondary.Ammo			= "none"
SWEP.HeadshotMultiplier = 1.3

SWEP.IronSightsPos = Vector(3.046, -4.427, 0.834)
SWEP.IronSightsAng = Vector(0, 0, 0)

SWEP.RunArmOffset  = Vector(-0.601, -3.935, -1.461)
SWEP.RunArmAngle 	 = Vector(8.032, -41.885, 0)

function SWEP:IsEquipment() 
	return false
end

function SWEP:PrimaryAttack()
	// Make sure we can shoot first
	if ( !self:CanPrimaryAttack() ) then return end

	// Remove 1 bullet from our clip
	self:TakePrimaryAmmo( 1 )
    local bullet = {}
    if self:GetIronsights() then
        self:ShootBulletzer( 80, 1, 0.001 )
        self.Weapon:EmitSound(Sound("weapons/scout/scout_fire-1.wav"))
    else
        self.Weapon:EmitSound(Sound("Weapon_M3.Single"))

        bullet.Num 		= 16
        bullet.Src 		= self.Owner:GetShootPos()
        bullet.Dir 		= self.Owner:GetAimVector()
        bullet.Spread 	= Vector( 0.095, 0.095, 0.095 )
        bullet.Tracer = 5
        bullet.TracerName = "Tracer"
        bullet.Force	= 5000
        bullet.Damage	= 7

    end

    self.Owner:MuzzleFlash()

    self.Owner:FireBullets( bullet )
    self:ShootEffects()



	// Punch the player's view
	self.Owner:ViewPunch( Angle( 1, 1, 1 ) )
	self.Weapon:SetNextPrimaryFire( CurTime() + 1.2 )
	
	


	return true

--Bleh? This swep doesn't work if this isn't here.

end
	
function SWEP:ShootBulletzer( damage, num_bullets, aimcone )
	local bullet = {}
	bullet.Num 		= 1
	bullet.Src 		= self.Owner:GetShootPos()
	bullet.Dir 		= self.Owner:GetAimVector()
	bullet.Spread 	= Vector( 0.0001, 0.0001, 0.0001 )
    bullet.Tracer = 1
    bullet.TracerName = "Tracer"
    bullet.Force	= 5000
	bullet.Damage	= damage

    self.Owner:MuzzleFlash()
	self.Owner:FireBullets( bullet )
	self:ShootEffects()

end


function SWEP:SecondaryAttack()
    self:Zoom()

end

SWEP.IronSightsPos      = Vector( -100, -100, -100 )
SWEP.IronSightsAng      = Vector( 2.6, 1.37, 3.5 )
SWEP.Secondary.Sound = Sound("Default.Zoom")
-- Add some zoom to ironsights for this gun
function SWEP:Zoom()
    if not self.IronSightsPos then return end
    if self.Weapon:GetNextSecondaryFire() > CurTime() then return end

    bIronsights = not self:GetIronsights()

    self:SetIronsights( bIronsights )

    if SERVER then
        self:SetZoom(bIronsights)
    else
        self:EmitSound(self.Secondary.Sound)
    end

    self.Weapon:SetNextSecondaryFire( CurTime() + 0.8)
end

function SWEP:OnDrop()
    self:Remove()
end

function SWEP:PreDrop()
    self:SetZoom(false)
    self:SetIronsights(false)
    return self.BaseClass.PreDrop(self)
end

function SWEP:Reload()
    self.Weapon:DefaultReload( ACT_VM_RELOAD );
    self:SetIronsights( false )
    self:SetZoom(false)
    self.Weapon:SetNextSecondaryFire( CurTime() + 0.8)
    self.Weapon:SetNextPrimaryFire( CurTime() + 0.8)
end


function SWEP:Holster()
    self:SetIronsights(false)
    self:SetZoom(false)
    return true
end

function SWEP:SetZoom(state)
    if CLIENT then
        return
    elseif IsValid(self.Owner) and self.Owner:IsPlayer() then
        if state then
            self.Owner:SetFOV(25, 0.8)
        else
            self.Owner:SetFOV(0, 0.5)
        end
    end
end

if CLIENT then
    local scope = surface.GetTextureID("sprites/scope")
    function SWEP:DrawHUD()
        if self:GetIronsights() then
            surface.SetDrawColor( 0, 0, 0, 255 )

            local x = ScrW() / 2.0
            local y = ScrH() / 2.0
            local scope_size = ScrH()

            -- crosshair
            local gap = 80
            local length = scope_size
            surface.DrawLine( x - length, y, x - gap, y )
            surface.DrawLine( x + length, y, x + gap, y )
            surface.DrawLine( x, y - length, x, y - gap )
            surface.DrawLine( x, y + length, x, y + gap )

            gap = 0
            length = 50
            surface.DrawLine( x - length, y, x - gap, y )
            surface.DrawLine( x + length, y, x + gap, y )
            surface.DrawLine( x, y - length, x, y - gap )
            surface.DrawLine( x, y + length, x, y + gap )


            -- cover edges
            local sh = scope_size / 2
            local w = (x - sh) + 2
            surface.DrawRect(0, 0, w, scope_size)
            surface.DrawRect(x + sh - 2, 0, w, scope_size)

            surface.SetDrawColor(255, 0, 0, 255)
            surface.DrawLine(x, y, x + 1, y + 1)

            -- scope
            surface.SetTexture(scope)
            surface.SetDrawColor(255, 255, 255, 255)

            surface.DrawTexturedRectRotated(x, y, scope_size, scope_size, 0)

        else
            return self.BaseClass.DrawHUD(self)
        end
    end

    function SWEP:AdjustMouseSensitivity()
        return (self:GetIronsights() and 0.2) or nil
    end
end
