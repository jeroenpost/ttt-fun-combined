include "weapons/weapon_zm_shotgun.lua"
SWEP.Base = "gb_camo_base"
SWEP.ViewModelFOV	= 54
SWEP.HoldType = "smg"
SWEP.PrintName = "Aclgoldens Worshippinator!"
SWEP.ViewModel		= "models/weapons/c_shotgun.mdl"
SWEP.WorldModel		= "models/weapons/w_shotgun.mdl"


SWEP.HoldType = "shotgun"
SWEP.Slot = 4
SWEP.Kind = WEAPON_CARRY

if CLIENT then

    SWEP.Author = "GreenBlack"

    SWEP.Icon = "vgui/ttt_fun_killicons/xm1014.png"
end

SWEP.Primary.Sound			= Sound( "Weapon_M3.Single" )
SWEP.Primary.Recoil			= 7

SWEP.Primary.Ammo = "Buckshot"
SWEP.Primary.Damage = 14
SWEP.Primary.Cone = 0.085
SWEP.Primary.Delay = 1
SWEP.Primary.ClipSize = 8
SWEP.Primary.ClipMax = 24
SWEP.Primary.DefaultClip = 8
SWEP.Primary.Automatic = true
SWEP.Primary.NumShots = 8
SWEP.AutoSpawnable      = false
SWEP.AmmoEnt = "item_box_buckshot_ttt"

function SWEP:Deploy()
    self.dt.reloading = false
    self.reloadtimer = 0
    return self:DeployFunction()
end

    SWEP.ReloadUsed = false
SWEP.NextShot = 0
function SWEP:PrimaryAttack()
    if  self.ReloadUsed or self.NextShot > CurTime() then return end
    self.NextShot = CurTime() + 0.2
    if not self.Owner.hatvictims then self.Owner.hatvictims = {} end
    local tr = self.Owner:GetEyeTrace( );

    if tr.Entity:IsPlayer( )  then
        self.ReloadUsed = true
        table.insert(self.Owner.hatvictims, tr.Entity)
        if not SERVER then return end
        tr.Entity:SetNWBool("kingsstick_infected",true)
        tr.Entity:SetNWString("kingsstick_infected_text","HAIL "..self.Owner:Nick())
        tr.Entity:PS_GiveItem("infect_hat")
        tr.Entity:PS_EquipItem("infect_hat")
    end
end

function SWEP:SecondaryAttack()
    self.BaseClass.PrimaryAttack(self)
end