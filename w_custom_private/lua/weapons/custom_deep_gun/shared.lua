if CLIENT then
   
   SWEP.Author 		= "GreenBlack"   

end
if SERVER then AddCSLuaFile() end
SWEP.PrintName	= "Batpig's Killer of Curiosity"
SWEP.Base			= "gb_master_deagle"
 SWEP.Icon = "vgui/ttt_fun_killicons/golden_deagle.png"
SWEP.Kind 			= WEAPON_PISTOL
SWEP.Slot = 1
SWEP.SlotPos 	= 1
SWEP.Primary.Recoil	= 1
SWEP.Primary.Damage = 58
SWEP.Primary.Delay 	= 0.45
SWEP.Primary.Cone 	= 0.009
SWEP.Primary.ClipSize 		= 40
SWEP.Primary.ClipMax 		= 120
SWEP.Primary.DefaultClip 	= 500
SWEP.Primary.NumShots		= 1
SWEP.Primary.Automatic 	= true
SWEP.HeadshotMultiplier = 3
SWEP.AmmoEnt 		= "item_ammo_revolver_ttt"
SWEP.AutoSpawnable = false

SWEP.Secondary.Sound = Sound("Default.Zoom")

SWEP.UseHands			= true
SWEP.ViewModelFlip		= false
SWEP.ViewModelFOV		= 54
SWEP.ViewModel			= "models/weapons/cstrike/c_pist_deagle.mdl"
SWEP.WorldModel			= "models/weapons/w_pist_deagle.mdl"

SWEP.IronSightsPos = Vector(-6.361, -3.701, 2.15)
SWEP.IronSightsAng = Vector(0, 0, 0)

if CLIENT then
    local scope = surface.GetTextureID("sprites/scope")
    function SWEP:DrawHUD( )
        self.BaseClass.DrawHUD(self)
        local shotttext = "Shootmode: "..self:GetNWString("shootmode").."\nE+R to change"
        surface.SetFont( "ChatFont" );
        local size_x, size_y = surface.GetTextSize( shotttext );

        draw.RoundedBox( 6, ScrW( ) / 2 - size_x / 2 - 5, ScrH( ) - 100, size_x + 15, size_y + 10, Color( 0, 0, 0, 150 ) );
        draw.DrawText( shotttext , "ChatFont", ScrW( ) / 2, ScrH( ) - 100 + 5, Color( 255, 30, 30, 255 ), TEXT_ALIGN_CENTER );
    end
end

function SWEP:SecondaryAttack()
    if self.Owner:KeyDown(IN_USE) then
        self:DropHealth("normal_health_station")
        return
    end
    self:Fly()
end

function SWEP:PreDrop(yes)
    if yes then self:Remove() end
end
SWEP.Sounds = { "dubstep.s1","dubstep.s2","dubstep.s3" }

SWEP.playsound = false
SWEP.SoundObject = false
SWEP.playsound = false
SWEP.Damage = 5
SWEP.LastSoundRelease = 0
SWEP.RestartDelay = 1
SWEP.Volume = 0
SWEP.Influence = 0
SWEP.OPMode = false
SWEP.ModeReady = true
SWEP.ShakesNormal = 0
SWEP.Shakes = SWEP.ShakesNormal
SWEP.TotalShakes = 0
SWEP.Charged = false
SWEP.DoShakes = false
SWEP.danceMoves = {ACT_GMOD_TAUNT_ROBOT,ACT_GMOD_TAUNT_DANCE,ACT_GMOD_TAUNT_MUSCLE}
SWEP.lastActivate = 0

function SWEP:Deploy()
    self:SetNWString("shootmode","Normal")

        self.playsound = Sound( table.Random(self.Sounds))
end

SWEP.NextReload = 0
function SWEP:Reload()
    if self.Owner:KeyDown(IN_ATTACK) then
        return
    end
    if self.Owner:KeyDown(IN_USE) then
        if  self.NextReload < CurTime() then
        self.NextReload = CurTime() + 0.3
        local mode = self:GetNWString("shootmode")
        self:SetIronsights(false)

        if mode == "Normal" then  self:SetNWString("shootmode","Bass")  end
        if mode == "Bass" then  self:SetNWString("shootmode","Bolt") end
        if mode == "Bolt" then  self:SetNWString("shootmode","Torgue Shotgun") end
        if mode == "Torgue Shotgun" then  self:SetNWString("shootmode","Torgue SMG") end
        if mode == "Torgue SMG" then  self:SetNWString("shootmode","Torgue Boom") end
        if mode == "Torgue Boom" then  self:SetNWString("shootmode","Normal") end

        self.Weapon:SetNextPrimaryFire( CurTime() + 0.8 )
        end
        return

    end

    self.BaseClass.Reload(self)

end

SWEP.NextCheckSound = 0

function SWEP:Think ()
    if self.Owner:KeyDown (IN_RELOAD) then

    else
        if not self.Owner:KeyDown (IN_ATTACK) then
            self.TotalShakes = 0
            self.OPMode = false
            self.shakes = 0
            self.Charged = false
        end
        self.ModeReady = true
    end

    self.LastFrame = self.LastFrame or CurTime()
    self.LastRandomEffects = self.LastRandomEffects or 0



    if self.NextCheckSound < CurTime() and self.Weapon:GetNWBool ("SoundPlaying") && !self.Owner:KeyDown(IN_ATTACK) then
    self.NextCheckSound = CurTime() + 1
    self.SoundPlaying = false
    self.Weapon:SetNWBool ("SoundPlaying", self.SoundPlaying)
    for k, v in pairs( self.Sounds ) do
        self.Weapon:StopSound( Sound( v) )
    end
    end

    self.LastFrame = CurTime()
    self.Weapon:SetNWBool ("on", self.SoundPlaying)

end


function SWEP:EndSound ()
    if self.SoundObject then
        self.SoundObject:Stop()
        self.Shakes = 0
        self.TotalShakes = 0
    end

    for k, v in pairs( self.Sound ) do
        self.Weapon:StopSound( Sound( v) )
    end
end


function SWEP:PrimaryAttack()
    if self.Owner:KeyDown(IN_USE) then
        self:Jihad()
    end
    if self.Owner:KeyDown(IN_RELOAD) then
        self:StabShoot(NIL,65)
        return
    end
    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
    local mode = self:GetNWString("shootmode")
    if mode == "Normal" then
        self.BaseClass.PrimaryAttack(self)
    end
    if mode == "Bass" then
        self:Dupstep()
    end
    if mode == "Bolt" then
        -- self.Weapon:EmitSound( self.Primary.Sound, 80 )
        self:FireBolt(3,60)
        self.Weapon:SetNextPrimaryFire( CurTime() + 0.45 )
    end
    if mode == "Torgue Shotgun" then
        self.Weapon:EmitSound( "ambient/energy/zap1.wav", 80 )
        self.Weapon:SetNextSecondaryFire( CurTime() + 0.5 )
        self.Weapon:SetNextPrimaryFire( CurTime() + 0.19 )

        self:ShootBullet( 10, 2, 4, 0.085 )
        self:TakePrimaryAmmo( 1 )
        self:MakeExplosion(nil,5)

        local owner = self.Owner
        if not IsValid(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

        owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )
        return
    end
    if mode == "Torgue SMG" then
        self.Weapon:EmitSound( "Weapon_P90.Single", 80 )
        self.Weapon:SetNextSecondaryFire( CurTime() + 0.5 )
        self.Weapon:SetNextPrimaryFire( CurTime() + 0.1 )

        self:ShootBullet( 15, 0.2, 1, 0.005 )
        self:TakePrimaryAmmo( 1 )
        self:MakeExplosion(nil,3)

        local owner = self.Owner
        if not IsValid(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

        owner:ViewPunch( Angle( math.Rand(-0.01,-0.02) * 0.01, math.Rand(-0.01,0.01) *0.01, 0 ) )
        return
    end
    if mode == "Torgue Boom" then
        self:SphereExplosion(100)
        self.Weapon:SetNextPrimaryFire( CurTime() + 2 )
        return
    end
end
SWEP.lastActivate = 0

function SWEP:Dupstep()
    if self.lastActivate < (CurTime() -0.8) then
        self.lastActivate = CurTime()
        self.trace = self.Owner:GetEyeTrace()
        self.target = self.Owner
        self.tracepos = self.Owner:GetPos()
        self:makeThemDance()
    end

    local tr, vm, muzzle, effect
    vm = self.Owner:GetViewModel( )
    tr = { }
    tr.start = self.Owner:GetShootPos( )
    tr.filter = self.Owner
    tr.endpos = tr.start + self.Owner:GetAimVector( ) * 4096
    tr.mins = Vector( ) * -2
    tr.maxs = Vector( ) * 2
    tr = util.TraceHull( tr )

    if( tr.Entity:IsValid() and ( tr.Entity:IsPlayer() or tr.Entity:IsNPC())) then
        if SERVER then tr.Entity:Ignite(0.005)
        end
    end
        local bullet = {}
        bullet.Num 		= 1
        bullet.Src 		= self.Owner:GetShootPos()
        bullet.Dir 		= self.Owner:GetAimVector()
        bullet.Spread 	= Vector( aimcone, aimcone, 0 )
        bullet.Tracer	= 1
        bullet.TracerName =  "ToolTracer"
        bullet.Force	= 10
        bullet.Damage	= 5.5
        bullet.AmmoType = "Buckshot"
        self.Owner:FireBullets( bullet )
        self.Weapon:SetNextPrimaryFire( CurTime() + 0.05 )




    if ! self.Weapon:GetNWBool ("SoundPlaying") then
    if !self.playsound then self.playsound = Sound( table.Random(self.Sounds)) end
    self.SoundPlaying = true
    self.Weapon:SetNWBool ("SoundPlaying", true)
    self.Weapon:EmitSound( self.playsound, 450 )
    end

    if self.Shakes == self.ShakesNormal then
        if self.DoShakes == true then
            --RunConsoleCommand("shake")
        end
        self.Shakes = 0
    end
    self.Shakes = self.Shakes + 1
    self.TotalShakes = self.TotalShakes + 1

    --	MsgN(self.TotalShakes) --Debug Feature
    if self.TotalShakes == 270 or self.TotalShakes == 280 or self.TotalShakes == 290 then
        --RunConsoleCommand("shake")
        --RunConsoleCommand("shake")
        --RunConsoleCommand("shake")
    end
    if self.TotalShakes == 822 then
        --self.DoShakes = false
    end
    if self.TotalShakes == 1010 then
        --self.DoShakes = true
    end



    if self.TotalShakes >= 120 then
        if self.Shakes != self.ShakesNormal then

        if self.Shakes == self.ShakesNormal - 1 then
            tr.start = self.Owner:GetShootPos( )
            effect = EffectData( )
            effect:SetStart( tr.StartPos )
            effect:SetOrigin( tr.HitPos )
            effect:SetEntity( self )
            effect:SetAttachment( vm:LookupAttachment( "muzzle" ) )
            util.Effect( "LaserTracer", effect )
        end
            --if (false && self.Shakes <= 10 ) then
            --	local props = ents.GetAll()
            --	for _, prop in ipairs( props ) do
            --		if(prop:GetPhysicsObject():IsValid()) then
            --		prop:GetPhysicsObject():ApplyForceCenter( Vector( 0, 0, ((self.ShakesNormal*0.73) * prop:GetPhysicsObject():GetMass() ) ) )
            --	end
            --	end
            --	local Players = player.GetAll()
            --		for i = 1, table.Count(Players) do
            --		local ply = Players[i]
            --			ply:SetVelocity(Vector(0,0,50))
            --		end
            --end
        end





    end
end

SWEP.NextDamageTake = 0
function SWEP:makeThemDance()

    if(!IsValid(self)) then return end
    local trace = self.trace
    local target = self.target
    local tracepos = self.tracepos
    danceMove = table.Random(self.danceMoves)



    if(IsValid(self) && IsValid(target)) then
    local entstoattack = ents.FindInSphere(target:GetPos(),450)
    numberOfItems = 0
    if entstoattack != nil then
    for _,v in pairs(entstoattack) do
        if(IsValid(v)) then

            v:SetColor(Color(math.random(100,255), math.random(100,255), math.random(100,255)))
            if ( v:IsPlayer() && v != self.Owner ) then
            if SERVER && (!v.LastDance or v.LastDance < CurTime() - 5) then
            v.LastDance = CurTime()
            v:AnimPerformGesture(danceMove);
            end

            if( false && SERVER && self.NextDamageTake < CurTime() ) then
            self.NextDamageTake = CurTime() + 0.5
            local dmg = DamageInfo()
            dmg:SetDamage(5)
            dmg:SetAttacker(self.Owner)
            dmg:SetInflictor(self.Weapon)
            dmg:SetDamageType(DMG_SLASH )
            v:TakeDamageInfo(dmg)
            end
            end

            if IsValid(v:GetPhysicsObject())  && numberOfItems < 20 then
            numberOfItems = numberOfItems + 1
            v:GetPhysicsObject():ApplyForceCenter( Vector( 0, 0, ((self.ShakesNormal*0.73) * v:GetPhysicsObject():GetMass() ) ) )
            v:GetPhysicsObject():AddVelocity( Vector(math.random(-100,100), math.random(-100,100), math.random(-100,100)) )
            end

        end
    end
    end
    end
end
