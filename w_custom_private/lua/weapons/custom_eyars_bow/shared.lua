if (SERVER) then

    AddCSLuaFile(  )
    SWEP.Weight                = 5
    SWEP.AutoSwitchTo        = false
    SWEP.AutoSwitchFrom        = false

 
end

if ( CLIENT ) then

    SWEP.DrawAmmo            = true
    SWEP.DrawCrosshair        = true
    SWEP.ViewModelFOV        = 65
    SWEP.ViewModelFlip        = false
    SWEP.CSMuzzleFlashes    = true
    
end
SWEP.Base                   = "weapon_tttbase"
SWEP.Category                = "EXP 2 Weapons"
SWEP.PrintName                 = "Eyars bow"
SWEP.HoldType                 = "rpg"
SWEP.Slot = 1
SWEP.SlotPos = 2

SWEP.Author            = "Mighty Lolrus"
SWEP.Icon = "vgui/ttt_fun_killicons/bow.png"

SWEP.ViewModel                  = "models/weapons/v_gbip_scoub.mdl"
SWEP.WorldModel 				= "models/weapons/w_gbip_scoub.mdl"


SWEP.Kind = WEAPON_PISTOL
SWEP.HoldType = "smg"

SWEP.Spawnable            = true
SWEP.AdminSpawnable        = true

SWEP.Primary.Sound			= Sound("weapons/requests_studio/gb2/fire.mp3")
SWEP.Primary.Delay          = 1.8 
SWEP.Primary.Recoil         = 1
SWEP.Primary.Automatic = true
SWEP.Primary.Ammo = "357"
SWEP.Primary.Damage = 50
SWEP.Primary.Cone = 0.005
SWEP.Primary.ClipSize = 25
SWEP.Primary.ClipMax = 50 -- keep mirrored to ammo
SWEP.Primary.DefaultClip = 25
SWEP.HeadshotMultiplier = 3
SWEP.Secondary.Sound = Sound("Default.Zoom")

SWEP.Secondary.ClipSize     = -1
SWEP.Secondary.DefaultClip  = -1
SWEP.Secondary.Automatic    = true
SWEP.Secondary.Ammo     = "none"
SWEP.Secondary.Delay = 1.0

SWEP.IsSilent = true
SWEP.IronSightsPos = Vector(-13.41, -13.308, 18.549)
SWEP.IronSightsAng = Vector(1.5, -0.401, -53.741)

SWEP.Offset = {
    Pos = {
        Up = 0,
        Right = -1,
        Forward = -4.5,
    },
    Ang = {
        Up = 0,
        Right = 14,
        Forward = 30,
    }
}

function SWEP:OnDrop()

    local wep = ents.Create(self:GetClass())
    wep:SetPos(self:GetPos()+Vector(0,100,0))
    wep:SetAngles(self:GetAngles())
    wep.IsDropped = true
    self:Remove(self)
    wep:Spawn()

end

function SWEP:DrawWorldModel( )
    local hand, offset, rotate

    if not IsValid( self.Owner ) then
        self:DrawModel( )
        return
    end

    if not self.Hand then
        self.Hand = self.Owner:LookupAttachment( "anim_attachment_rh" )
    end

    hand = self.Owner:GetAttachment( self.Hand )

    if not hand then
        self:DrawModel( )
        return
    end

    offset = hand.Ang:Right( ) * self.Offset.Pos.Right + hand.Ang:Forward( ) * self.Offset.Pos.Forward + hand.Ang:Up( ) * self.Offset.Pos.Up

    hand.Ang:RotateAroundAxis( hand.Ang:Right( ), self.Offset.Ang.Right )
    hand.Ang:RotateAroundAxis( hand.Ang:Forward( ), self.Offset.Ang.Forward )
    hand.Ang:RotateAroundAxis( hand.Ang:Up( ), self.Offset.Ang.Up )

    self:SetRenderOrigin( hand.Pos + offset )
    self:SetRenderAngles( hand.Ang )

    self:DrawModel( )
end


SWEP.Planted = false;

SWEP.Kind = WEAPON_EQUIP

SWEP.LimitedStock = true
SWEP.WeaponID = AMMO_PBOMB

SWEP.AllowDrop = true;

SWEP.BeepSound = Sound( "C4.PlantSound" );

local hidden = true;
local close = false;
local lastclose = false;

if CLIENT then
    function SWEP:DrawHUD( )
        local planted = self:GetNWBool( "Planted" );
        local tr = LocalPlayer( ):GetEyeTrace( );
        local close2;

        if tr.Entity:IsPlayer( ) and LocalPlayer( ):GetPos( ):Distance( tr.Entity:GetPos( ) ) <= 99999999 then
            close2 = true;
        else
            close2 = false;
        end;

        local planted_text = "Not Planted!";
        local close_text = "Not Close Enough!";

        if planted then
            if hidden == true then
                hidden = false
            end
            planted_text = "Planted!\nSecondary to Explode!";

            surface.SetFont( "ChatFont" );
            local size_x, size_y = surface.GetTextSize( planted_text );

            draw.RoundedBox( 6, ScrW( ) / 2 - size_x / 2 - 5, ScrH( ) - 100, size_x + 15, size_y + 10, Color( 0, 0, 0, 150 ) );
            draw.DrawText( planted_text, "ChatFont", ScrW( ) / 2, ScrH( ) - 100 + 5, Color( 255, 30, 30, 255 ), TEXT_ALIGN_CENTER );
        else
            if close2 then
                close_text = "You are Close Enough!\nPrimary to Plant!";
            end;

            surface.SetFont( "ChatFont" );
            local size_x, size_y = surface.GetTextSize( planted_text );

            surface.SetFont( "ChatFont" );
            local size_x2, size_y2 = surface.GetTextSize( close_text );

            draw.RoundedBox( 6, ScrW( ) / 2 - ( size_x2 / 2 ) - 7.5, ScrH( ) - 100, size_x2 + 20, size_y + size_y2 + 10, Color( 0, 0, 0, 150 ) );
            draw.DrawText( planted_text, "ChatFont", ScrW( ) / 2, ScrH( ) - 100 + 5, Color( 30, 30, 255, 255 ), TEXT_ALIGN_CENTER );
            draw.DrawText( close_text, "ChatFont", ScrW( ) / 2 , ScrH( ) - 100 + size_y + 5, Color( 255, 255, 255, 255 ), TEXT_ALIGN_CENTER );
        end;
    end;
end;

function SWEP:OnDrop()
    if SERVER then
    end
    self:Remove()
end

function SWEP:Initialize()
    self:SetHoldType( self.HoldType or "pistol" )


end;

SWEP.ClipSet = false

function SWEP:Think( )
    local ply = self.Owner;
    if not IsValid( ply ) then return; end;

    if not self.Owner.numberOfStickies then
        self.Owner.numberOfStickies = 1
    end
    if not self.ClipSet then
        self:SetClip1( 15 - self.Owner.numberOfStickies)
        self.ClipSet = true
    end

    local tr = ply:GetEyeTrace( );

    if tr.Entity:IsPlayer( ) and ply:GetPos( ):Distance( tr.Entity:GetPos( ) ) <= 99999999 and GetRoundState() == ROUND_ACTIVE then
        close = true;
    else
        close = false;
    end;

    if self:GetNWBool( "Planted" ) == false then
        if lastclose == false and close then
            self.Weapon:SendWeaponAnim( ACT_SLAM_STICKWALL_ATTACH );
        elseif lastclose == true and not close then
            self.Weapon:SendWeaponAnim( ACT_SLAM_STICKWALL_IDLE );
        end;
    end;

    lastclose = close;

    if not hidden then
        if close and not self:GetNWBool( "Planted" ) then
            self.Weapon:SendWeaponAnim( ACT_SLAM_STICKWALL_ATTACH );
        elseif not close and not self:GetNWBool( "Planted" ) then
            self.Weapon:SendWeaponAnim( ACT_SLAM_STICKWALL_IDLE );
        elseif self:GetNWBool( "Planted" ) then
            self.Weapon:SendWeaponAnim( ACT_SLAM_DETONATOR_DRAW );
        end;
    end;
    if SERVER and not hidden and IsValid(self.Owner) and self.Owner:GetActiveWeapon() == self.Weapon then
        self.Owner:DrawViewModel(false)
        self.Owner:DrawWorldModel(false)
        hidden = true
    end
end;

function SWEP:PrimaryAttack()
    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
    self.Weapon:SetNextSecondaryFire( CurTime() + self.Secondary.Delay )

    local ply = self.Owner;
    if not IsValid( ply ) then return; end;

    local tr = ply:GetEyeTrace(MASK_SHOT);

    if not self:GetNWBool( "Planted" ) then
        if close then
            if SERVER then
                self.PlantedPly = tr.Entity
                self:SetNWBool("Planted", true)
            end;
            self.Weapon:SendWeaponAnim( ACT_SLAM_STICKWALL_ATTACH2 );
        end;
        if hidden == true and self:GetNWBool( "Planted" ) == true then
            hidden = false
        end
    end;
end

function SWEP:SecondaryAttack()
    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
    self.Weapon:SetNextSecondaryFire( CurTime() + self.Secondary.Delay )
    if self:GetNWBool( "Planted" ) and not self.JustSploded then
        self:BombSplode()




    end;
end

function SWEP:DoSplode( owner, plantedply, bool )
    if not bool then
        self:SetNWBool( "Planted", false )
        timer.Simple( 1, function( ) plantedply:EmitSound( self.BeepSound ); end );
        timer.Simple( 2, function( ) plantedply:EmitSound( self.BeepSound ); end );
        timer.Simple( 3, function( ) plantedply:EmitSound( self.BeepSound ); end );
        timer.Simple( 4, function( )
            local effectdata = EffectData()
            effectdata:SetOrigin(plantedply:GetPos())
            util.Effect("HelicopterMegaBomb", effectdata)

            plantedply.PlayerBombDie = true;

            local explosion = ents.Create("env_explosion")
            explosion:SetOwner(self.Owner)
            explosion:SetPos(plantedply:GetPos( ) + Vector( 0, 0, 10 ))
            explosion:SetKeyValue("iMagnitude", "70")
            explosion:SetKeyValue("iRadiusOverride", 0)
            explosion:Spawn()
            explosion:Activate()
            explosion:Fire("Explode", "", 0)
            explosion:Fire("kill", "", 0)

            --self.Owner:ConCommand( "use weapon_ttt_unarmed" );
           -- self:Remove( );
             hidden = false;
             close = false;
             lastclose = false;
            self:SetNWBool( "Planted", false )

            if not self.Owner.numberOfStickies then
                self.Owner.numberOfStickies = 1
            end
            if self.Owner.numberOfStickies < 15 then
                self.Owner.numberOfStickies = self.Owner.numberOfStickies + 1
                if self.Owner.numberOfStickies < 15 then
                    if SERVER then
                        self.Owner:StripWeapon("custom_eyars_bow");
                        self.Owner:Give("custom_eyars_bow");
                    end
                end
            end
        end );
    else
        timer.Simple( 1, function( ) plantedply:EmitSound( self.BeepSound ); end );
        timer.Simple( 2, function( ) plantedply:EmitSound( self.BeepSound ); end );
        timer.Simple( 3, function( ) plantedply:EmitSound( self.BeepSound ); end );
        timer.Simple( 4, function( )
            local effectdata = EffectData()
            effectdata:SetOrigin(plantedply:GetPos())
            util.Effect("HelicopterMegaBomb", effectdata)

            local explosion = ents.Create("env_explosion")
            explosion:SetOwner(self.Owner)
            explosion:SetPos(plantedply:GetPos( ))
            explosion:SetKeyValue("iMagnitude", "45")
            explosion:SetKeyValue("iRadiusOverride", 0)
            explosion:Spawn()
            explosion:Activate()
            explosion:Fire("Explode", "", 0)
            explosion:Fire("kill", "", 0)

            hidden = false;
            close = false;
            lastclose = false;
            self:SetNWBool( "Planted", false )

            plantedply:Remove( );
           -- self:Remove( );
            if self.Owner.numberOfStickies < 15 then
                if SERVER then
                    self.Owner:StripWeapon("custom_eyars_bow");
                    self.Owner:Give("custom_eyars_bow");
                end
            end
            --self:Remove( );
        end );
    end;
end;

function SWEP:BombSplode()
    local ply = self.Owner;
    if not IsValid( ply ) then return; end;

    if IsValid( self.PlantedPly ) and self.PlantedPly:Alive( ) then
        if SERVER then
            self:DoSplode( self.Owner, self.PlantedPly, false );
            self.JustSploded = true;
        end;
        self.Weapon:SendWeaponAnim( ACT_SLAM_DETONATOR_DETONATE );
    else
        if SERVER then
            if IsValid( self.PlantedPly ) and IsValid( self.PlantedPly.server_ragdoll ) then
                self:DoSplode( self.Owner, self.PlantedPly.server_ragdoll, true );
                self.JustSploded = true;
            else
                ply:PrintMessage( HUD_PRINTTALK, "The body this bomb has been planted to is no longer available!" );
            end;
        end;
        self.Weapon:SendWeaponAnim( ACT_SLAM_DETONATOR_DETONATE );
    end;
end;

function SWEP:Deploy( )
    //hidden = false;
    if self:GetNWBool( "Planted" ) == true then
        self:SetHoldType("normal")
        hidden = false
    end
    return true
end;

function SWEP:Reload()
    return false
end

function SWEP:OnDrop()
    self:Remove()
end

function SWEP:OnRemove()
    if CLIENT and IsValid(self.Owner) and self.Owner == LocalPlayer() and self.Owner:Alive() then
        RunConsoleCommand("lastinv")
    end
end
