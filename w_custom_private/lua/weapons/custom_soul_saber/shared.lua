
SWEP.PrintName			= "King's Stabby Stab"
SWEP.Slot				= 0
SWEP.SlotPos			= 1
if ( CLIENT ) then

				
	SWEP.Author				= "XcoliahX"
	
	
	SWEP.IconLetter			= "f"
	SWEP.DrawCrosshair		= false
	
        killicon.AddFont( "weapon_knife", "CSKillIcons", SWEP.IconLetter, Color( 255, 80, 0, 255 ) )


end

SWEP.Base = "aa_base_fw"
SWEP.Kind = WEAPON_MELEE
SWEP.WeaponID = AMMO_CROWBAR


SWEP.ViewModelFlip	= false
-----------------------Main functions----------------------------
 
-- function SWEP:Reload() --To do when reloading
-- end 
 
function SWEP:JumpReset()
         self.JumpRefire = true
end

function SWEP:Think()
	if self.Owner:KeyDown(IN_JUMP) then
		if self.Owner:IsOnGround() then
			self.Owner:SetVelocity(Vector(0,0,500))
		end
    end

    if self.Owner:KeyDown(IN_USE) and  self.Owner:KeyDown(IN_ATTACK2) then
       self:FireWorkie()
    end
	
         //Jump think
         if self.InAirDmg == 1 then
            local trace = {}
	    trace.start = self.Owner:GetPos()
	    trace.endpos = self.Owner:GetPos() + (self.Owner:GetUp() * -10)
	    trace.filter = self.Owner
	
	    local tr2 = util.TraceLine(trace)

	    self.Owner:SetHealth(self.PrevHP)
	    
	    if tr2.Hit then
	       self.InAirDmg = 0
	       self.JumpRefire = false
               self.Weapon:SetNextSecondaryFire(CurTime() + 0.3)
               self.Weapon:SetNextPrimaryFire(CurTime() + 0.3)

	       if SERVER then self.Owner:EmitSound(Sound("player/pl_pain5.wav")) end
            else
             local ang = self.Owner:GetAimVector()
	     local spos = self.Owner:GetShootPos()

	     local trace = {}
	     trace.start = spos
	     trace.endpos = spos + (ang * 150)
	     trace.filter = self.Owner
	
	     local tr = util.TraceLine(trace)
	 
	     if tr.HitNonWorld and self.JumpRefire == true then
	        self.JumpRefire = false

                local bullet = {}
                bullet.Num=5
                bullet.Src = self.Owner:GetShootPos()
	        bullet.Dir= self.Owner:GetAimVector()
	        bullet.Spread = Vector(0.1,0.1,0.1)
	        bullet.Tracer = 0
	        bullet.Force = 500
	        bullet.Damage = 200
	        self.Owner:FireBullets(bullet)
	        self.Weapon:SendWeaponAnim(ACT_VM_HITCENTER)

	        
	        timer.Simple(0.3,self.JumpReset,self)
	        
                --if SERVER then self.Owner:EmitSound(Sound("npc/vort/claw_swing"..tostring(math.random(1,2))..".wav")) end
             --   if SERVER then self.Owner:EmitSound(Sound("weapons/ls/lightsaber_swing.mp3")) end
				
             end
            end
           end
end

function SWEP:Initialize()
util.PrecacheSound("physics/flesh/flesh_impact_bullet" .. math.random( 3, 5 ) .. ".wav")
util.PrecacheSound("weapons/iceaxe/iceaxe_swing1.wav")
self:SetHoldType("melee")
end
 
function SWEP:PrimaryAttack()
self.Weapon:SetNextPrimaryFire(CurTime() + .45)

local trace = self.Owner:GetEyeTrace()

if trace.HitPos:Distance(self.Owner:GetShootPos()) <= 75 then
self.Weapon:SendWeaponAnim(ACT_VM_HITCENTER)
	bullet = {}
	bullet.Num    = 50
	bullet.Src    = self.Owner:GetShootPos()
	bullet.Dir    = self.Owner:GetAimVector()
	bullet.Spread = Vector(0.1, 0.1, 0.1)
	bullet.Tracer = 0
	bullet.Force  = 0
	bullet.Damage = 1
self.Owner:FireBullets(bullet)
self.Owner:SetAnimation( PLAYER_ATTACK1 );
--self.Weapon:EmitSound("weapons/ls/lightsaber_swing.mp3")
self.Owner:SetAnimation( PLAYER_ATTACK1 )
self.Weapon:SendWeaponAnim(ACT_VM_MISSCENTER)
else
	--self.Weapon:EmitSound("ls/ls_throw")
	self.Weapon:SendWeaponAnim(ACT_VM_MISSCENTER)
	self.Owner:SetAnimation( PLAYER_ATTACK1 )
end

end

/*---------------------------------------------------------
		SecondaryAttack
---------------------------------------------------------*/
SWEP.InAirDmg = 0
function SWEP:SecondaryAttack()
            
        self.Weapon:SetNextSecondaryFire(CurTime() + 0.3)
         self.Weapon:SetNextPrimaryFire(CurTime() + 0.3)


        if not self:CanPrimaryAttack() then return end
         if SERVER then self.Owner:EmitSound(Sound("weapons/ls/lightsaber_swing.mp3")) end
         if SERVER then self.Owner:EmitSound(Sound("player/suit_sprint.wav")) end
         
         

           self:TakePrimaryAmmo( 1 )
         self.PrevHP = self.Owner:Health()
         
         if SERVER then self.Owner:SetVelocity(self.Owner:GetForward() * 600 + Vector(0,0,400)) end
         
         timer.Simple(15, function()
                 if IsValid(self) then self:GivePrimaryAmmo(1) 
            end end)
end
SWEP.HeadshotMultiplier = 1
function SWEP:SecondaryAttackDelay()
         --self.InAirDmg = 1
         --self.JumpRefire = true
end

function SWEP:GivePrimaryAmmo( num )

    self:SetClip1( self:Clip1() + num )
    
end


-------------------------------------------------------------------

------------General Swep Info---------------
SWEP.Author   = "XcoliahX"
SWEP.Contact        = ""
SWEP.Purpose        = ""
SWEP.Instructions   = ""
SWEP.Spawnable      = false
SWEP.AdminSpawnable  = true
-----------------------------------------------

------------Models---------------------------
SWEP.UseHands			= true
SWEP.ViewModelFlip		= false
SWEP.ViewModelFOV		= 54
SWEP.ViewModel          = "models/weapons/cstrike/c_knife_t.mdl"
SWEP.WorldModel         = "models/weapons/w_knife_t.mdl"
-----------------------------------------------

-------------Primary Fire Attributes----------------------------------------
SWEP.Primary.Delay			= 5	--In seconds
SWEP.Primary.Recoil			= 0		--Gun Kick
SWEP.Primary.Damage			= 3	--Damage per Bullet
SWEP.Primary.NumShots		= 1		--Number of shots per one fire
SWEP.Primary.Cone			= 0 	--Bullet Spread
SWEP.Primary.ClipSize		= 5	--Use "-1 if there are no clips"
SWEP.Primary.DefaultClip	= 5	--Number of shots in next clip
SWEP.Primary.Automatic   	= true	--Pistol fire (false) or SMG fire (true)
SWEP.Primary.Ammo         	= "none"	--Ammo Type
-------------End Primary Fire Attributes------------------------------------
 
-------------Secondary Fire Attributes-------------------------------------
SWEP.Secondary.Delay		= 120
SWEP.Secondary.Recoil		= 0
SWEP.Secondary.Damage		= 0
SWEP.Secondary.NumShots		= 1
SWEP.Secondary.Cone			= 0
SWEP.Secondary.ClipSize		= 100
SWEP.Secondary.DefaultClip	= 100
SWEP.Secondary.Automatic   	= true
SWEP.Secondary.Ammo         = "Battery"
-------------End Secondary Fire Attributes--------------------------------
function SWEP:Deploy()
	self.Weapon:EmitSound( "weapons/knife/knife_deploy1.wav" ) --Plays a nifty sound when you pull it out.
end



SWEP.defibs = 0
SWEP.nextDefib = 0
function SWEP:Reload()

    if self.Owner:KeyDown(IN_USE) then
        if ( self.defibs > 0) then return end


        self.NextAttacky = CurTime()+ 0.5


        local tr = util.TraceLine({ start = self.Owner:EyePos(), endpos = self.Owner:EyePos() + self.Owner:EyeAngles():Forward() * 80, filter = self.Owner })
        if tr.HitNonWorld and IsValid(tr.Entity) and tr.Entity:GetClass() == "prop_ragdoll" and tr.Entity.player_ragdoll then
            self.Weapon:EmitSound( "weapons/c4/c4_beep1.wav"	)
            local mins, maxs = self.Owner:OBBMins(), self.Owner:OBBMaxs()
            local tr2 = util.TraceHull({start = tr.Entity:LocalToWorld(tr.Entity:OBBCenter()) + vector_up, endpos = tr.Entity:LocalToWorld(tr.Entity:OBBCenter()) + Vector(0, 0, 80), filter = {tr.Entity, self.Owner}, mins = Vector(mins.x * 1.6, mins.y * 1.6, 0), maxs = Vector(maxs.x * 1.6, maxs.y * 1.6, 0), mask = MASK_PLAYERSOLID})
            if tr2.Hit then
                self.Owner:PrintMessage( HUD_PRINTCENTER, "There is no room to bring him back here" )
                return
            end


            self.nextDefib = CurTime() + 15
            self.defibs =  self.defibs + 1
            if SERVER then
                rag = tr.Entity
                local ply = player.GetByUniqueID(rag.uqid)
                local credits = CORPSE.GetCredits(rag, 0)
                if not IsValid(ply) then return end
                if SERVER and _globals['slayed'][ply:SteamID()] then self.Owner:PrintMessage( HUD_PRINTCENTER, "Can't revive, person was slayed" )
                    ply:Kill();
                    return
                end
                if SERVER and ((rag.was_role == ROLE_TRAITOR and self.Owner:GetRole() ~= ROLE_TRAITOR) or (rag.was_role ~= ROLE_TRAITOR and self.Owner:GetRole() == ROLE_TRAITOR))  then
                    DamageLog("Defib: " .. self.Owner:Nick() .. " [" .. self.Owner:GetRoleString() .. "] Died defibbing "..ply:Nick().." [" .. ply:GetRoleString() .. "] ")
                    self.Owner:PrintMessage( HUD_PRINTCENTER, ply:Nick().." was of the other team,  sucked up your soul and killed you" )
                    self.Owner:Kill();

                    return
                end
                DamageLog("Defib: " .. self.Owner:Nick() .. " [" .. self.Owner:GetRoleString() .. "] defibbed "..ply:Nick().." [" .. ply:GetRoleString() .. "] ")

                ply:SpawnForRound(true)
                ply:SetCredits(credits)
                ply:SetPos(rag:LocalToWorld(rag:OBBCenter()) + vector_up / 2) --Just in case he gets stuck somewhere
                ply:SetEyeAngles(Angle(0, rag:GetAngles().y, 0))
                ply:SetCollisionGroup(COLLISION_GROUP_WEAPON)
                timer.Simple(2, function() ply:SetCollisionGroup(COLLISION_GROUP_PLAYER) end)

                umsg.Start("_resurrectbody")
                umsg.Entity(ply)
                umsg.Bool(CORPSE.GetFound(rag, false))
                umsg.End()
                rag:Remove()
            end
        else
            self.Weapon:EmitSound("npc/scanner/scanner_nearmiss1.wav")
        end
    end
end