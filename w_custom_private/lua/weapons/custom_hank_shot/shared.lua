
    if SERVER then
       AddCSLuaFile(  )
        --resource.AddFile("materials/vgui/ttt_fun_killicons/p228.png")
    end
     SWEP.PrintName = "The 360 Gun"
       SWEP.Slot      = 4
    if CLIENT then
       
       SWEP.Author = "GreenBlack"
       SWEP.ViewModelFOV  = 72
       SWEP.Icon = "vgui/ttt_fun_killicons/p228.png"
    end
     
    SWEP.Base                               = "weapon_tttbase"
     
     
    SWEP.HoldType                   = "pistol"
     
    SWEP.Primary.Delay       = 0.08
    SWEP.Primary.Recoil      = 0.01
    SWEP.Primary.Automatic   = true
    SWEP.Primary.Damage      = 10
    SWEP.Primary.Cone        = 0.025
    SWEP.Primary.Ammo        = "smg1"
    SWEP.Primary.ClipSize    = 120
    SWEP.Primary.ClipMax     = 120
    SWEP.Primary.DefaultClip = 120
SWEP.Primary.Sound = Sound( "Weapon_P90.Single" )

    SWEP.HeadshotMultiplier = 3

    SWEP.Secondary.Delay       = 1
    SWEP.Secondary.Recoil      = 1.01
    SWEP.Secondary.Automatic   = true
    SWEP.Secondary.Damage      = 3
    SWEP.Secondary.Cone        = 1
    SWEP.Secondary.Ammo        = "smg1"

     SWEP.Secondary.NumShots = 100
SWEP.Primary.Sound = Sound( "Weapon_P90.Single" )
     
    SWEP.IronSightsPos = Vector(4.719, 0, 0.839)
	SWEP.IronSightsAng = Vector(0, 0, 0)
     
    SWEP.ViewModel  = "models/weapons/v_pist_p228.mdl"
    SWEP.WorldModel = "models/weapons/w_pist_p228.mdl"
   
       -- Path to the icon material
       SWEP.Icon = "vgui/ttt/icon_glock"
     

     
    --- TTT config values
     
    -- Kind specifies the category this weapon is in. Players can only carry one of
    -- each. Can be: WEAPON_... MELEE, PISTOL, HEAVY, NADE, CARRY, EQUIP1, EQUIP2 or ROLE.
    -- Matching SWEP.Slot values: 0      1       2     3      4      6       7        8
    SWEP.Kind = WEAPON_CARRY
     
    -- If AutoSpawnable is true and SWEP.Kind is not WEAPON_EQUIP1/2, then this gun can
    -- be spawned as a random weapon. Of course this AK is special equipment so it won't,
    -- but for the sake of example this is explicitly set to false anyway.
    SWEP.AutoSpawnable = false
     
    -- The AmmoEnt is the ammo entity that can be picked up when carrying this gun.
    SWEP.AmmoEnt = "item_ammo_pistol_ttt"
     
    -- CanBuy is a table of ROLE_* entries like ROLE_TRAITOR and ROLE_DETECTIVE. If

     
    -- InLoadoutFor is a table of ROLE_* entries that specifies which roles should
    -- receive this weapon as soon as the round starts. In this case, none.
    SWEP.InLoadoutFor = nil
     
    -- If LimitedStock is true, you can only buy one per round.
    SWEP.LimitedStock = false
     
    -- If AllowDrop is false, players can't manually drop the gun with Q
    SWEP.AllowDrop = true
     
    -- If IsSilent is true, victims will not scream upon death.
    SWEP.IsSilent = false
     
    -- If NoSights is true, the weapon won't have ironsights
    SWEP.NoSights = false
     
    -- Equipment menu information is only needed on the client

SWEP.nextFire = CurTime()

function SWEP:SecondaryAttack(worldsnd)

   self.Weapon:SetNextSecondaryFire( CurTime() + self.Secondary.Delay )
   self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
 
   if not self:CanSecondaryAttack() or self.nextFire > CurTime() then return end
 
   self.nextFire = CurTime() + 1

   if not worldsnd then
      self.Weapon:EmitSound( self.Primary.Sound, self.Primary.SoundLevel )
   elseif SERVER then
      sound.Play(self.Primary.Sound, self:GetPos(), self.Primary.SoundLevel)
   end

   self:ShootBullet( self.Secondary.Damage, self.Secondary.Recoil, self.Secondary.NumShots, self.Secondary.Cone )

   self:TakePrimaryAmmo( 50 )

   local owner = self.Owner
   if not IsValid(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

   owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Secondary.Recoil, math.Rand(-0.1,0.1) *self.Secondary.Recoil, 0 ) )
end