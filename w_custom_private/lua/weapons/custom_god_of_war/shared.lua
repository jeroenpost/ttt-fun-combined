if SERVER then
    AddCSLuaFile(  )
    --resource.AddFile("materials/vgui/ttt_fun_killicons/knife.png")
end

SWEP.HoldType = "knife"
SWEP.PrintName    = "The God of War"
SWEP.Slot         = 0

if CLIENT then




    SWEP.ViewModelFlip = false


    SWEP.Icon = "vgui/ttt_fun_killicons/knife.png"
end

SWEP.Base               = "aa_base_fw"

SWEP.UseHands			= true
SWEP.ViewModelFlip		= false
SWEP.ViewModelFOV		= 54
SWEP.ViewModel          = "models/weapons/cstrike/c_knife_t.mdl"
SWEP.WorldModel         = "models/weapons/w_knife_t.mdl"

SWEP.DrawCrosshair      = false
SWEP.Primary.Damage         = 42
SWEP.Primary.ClipSize       = -1
SWEP.Primary.DefaultClip    = -1
SWEP.Primary.Automatic      = true
SWEP.Primary.Delay = 0.6
SWEP.Primary.Ammo       = "none"
SWEP.Secondary.ClipSize     = -1
SWEP.Secondary.DefaultClip  = -1
SWEP.Secondary.Automatic    = true
SWEP.Secondary.Ammo     = "none"
SWEP.Secondary.Delay = 1.4
SWEP.HeadshotMultiplier = 1

SWEP.Kind = WEAPON_MELEE
SWEP.CanDecapitate = true

SWEP.LimitedStock = true -- only buyable once

SWEP.IsSilent = true

-- Pull out faster than standard guns
SWEP.DeploySpeed = 1

function SWEP:PrimaryAttack()
    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
    self.Weapon:SetNextSecondaryFire( CurTime() + self.Secondary.Delay )

    self.Owner:LagCompensation(true)

    local spos = self.Owner:GetShootPos()
    local sdest = spos + (self.Owner:GetAimVector() * 70)

    local kmins = Vector(1,1,1) * -10
    local kmaxs = Vector(1,1,1) * 10

    local tr = util.TraceHull({start=spos, endpos=sdest, filter=self.Owner, mask=MASK_SHOT_HULL, mins=kmins, maxs=kmaxs})

    -- Hull might hit environment stuff that line does not hit
    if not IsValid(tr.Entity) then
        tr = util.TraceLine({start=spos, endpos=sdest, filter=self.Owner, mask=MASK_SHOT_HULL})
    end

    local hitEnt = tr.Entity

    -- effects
    if IsValid(hitEnt) then
        self.Weapon:SendWeaponAnim( ACT_VM_HITCENTER )

        local edata = EffectData()
        edata:SetStart(spos)
        edata:SetOrigin(tr.HitPos)
        edata:SetNormal(tr.Normal)
        edata:SetEntity(hitEnt)

        if hitEnt:IsPlayer() or hitEnt:GetClass() == "prop_ragdoll" then
            util.Effect("BloodImpact", edata)
        end
    else
        self.Weapon:SendWeaponAnim( ACT_VM_MISSCENTER )
    end

    if SERVER then
        self.Owner:SetAnimation( PLAYER_ATTACK1 )
    end


    if SERVER and tr.Hit  and IsValid(hitEnt) then
        self:Disorientate()
        hitEnt:Ignite(1, 0)
            bullet = {}
            bullet.Num    = 1
            bullet.Src    = self.Owner:GetShootPos()
            bullet.Dir    = self.Owner:GetAimVector()
            bullet.Spread = Vector(0, 0, 0)
            bullet.Tracer = 0
            bullet.Force  = 1
            bullet.Damage = 35
            self.Owner:FireBullets(bullet)
            self.Owner:ViewPunch(Angle(7, 0, 0))

    end

    self.Owner:LagCompensation(false)
end


function SWEP:SecondaryAttack()
    if self.Owner:KeyDown( IN_USE) then
        self:Fly()
        return
    end

    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
    self.Weapon:SetNextSecondaryFire( CurTime() + self.Secondary.Delay )

    self:PowerHit()


    self.Weapon:SendWeaponAnim( ACT_VM_MISSCENTER )

    if SERVER and self.Owner:KeyDown(IN_RELOAD)then
        self.Owner:SetAnimation( PLAYER_ATTACK1 )

        local ply = self.Owner
        if not IsValid(ply) then return end

        local ang = ply:EyeAngles()

        if ang.p < 90 then
            ang.p = -10 + ang.p * ((90 + 10) / 90)
        else
            ang.p = 360 - ang.p
            ang.p = -10 + ang.p * -((90 + 10) / 90)
        end

        local vel = math.Clamp((90 - ang.p) * 5.5, 550, 800)

        local vfw = ang:Forward()
        local vrt = ang:Right()

        local src = ply:GetPos() + (ply:Crouching() and ply:GetViewOffsetDucked() or ply:GetViewOffset())

        src = src + (vfw * 1) + (vrt * 3)

        local thr = vfw * vel + ply:GetVelocity()

        local knife_ang = Angle(-28,0,0) + ang
        knife_ang:RotateAroundAxis(knife_ang:Right(), -90)

        local knife = ents.Create("ac_knife_proj")
        if not IsValid(knife) then return end
        knife:SetPos(src)
        knife:SetAngles(knife_ang)
        knife.becomewep = "custom_god_of_war"

        knife:Spawn()

        knife.Damage = self.Primary.Damage

        knife:SetOwner(ply)

        local phys = knife:GetPhysicsObject()
        if IsValid(phys) then
            phys:SetVelocity(thr)
            phys:AddAngleVelocity(Vector(0, 1500, 0))
            phys:Wake()
        end
        knife.becomewep = "custom_god_of_war"
        self:Remove()
    end
end

function SWEP:Equip()
    self.Weapon:SetNextPrimaryFire( CurTime() + (self.Primary.Delay * 1.5) )
    self.Weapon:SetNextSecondaryFire( CurTime() + (self.Secondary.Delay * 1.5) )
end

function SWEP:PreDrop()
    -- for consistency, dropped knife should not have DNA/prints
    self.fingerprints = {}
end

function SWEP:OnRemove()
    if CLIENT and IsValid(self.Owner) and self.Owner == LocalPlayer() and self.Owner:Alive() then
        RunConsoleCommand("lastinv")
    end
end

if CLIENT then
    function SWEP:DrawHUD()
        local tr = self.Owner:GetEyeTrace(MASK_SHOT)

        if tr.HitNonWorld and IsValid(tr.Entity) and tr.Entity:IsPlayer()
                and tr.Entity:Health() < (self.Primary.Damage + 10) then

            local x = ScrW() / 2.0
            local y = ScrH() / 2.0

            surface.SetDrawColor(255, 0, 0, 255)

            local outer = 20
            local inner = 10
            surface.DrawLine(x - outer, y - outer, x - inner, y - inner)
            surface.DrawLine(x + outer, y + outer, x + inner, y + inner)

            surface.DrawLine(x - outer, y + outer, x - inner, y + inner)
            surface.DrawLine(x + outer, y - outer, x + inner, y - inner)

            draw.SimpleText("INSTANT KILL", "TabLarge", x, y - 30, COLOR_RED, TEXT_ALIGN_CENTER, TEXT_ALIGN_BOTTOM)
        end

        return self.BaseClass.DrawHUD(self)
    end
end

SWEP.NextSecondary = 0
function SWEP:Reload()

    if self.Owner:KeyDown( IN_USE ) then
        self:FireWorkie()
        return
    end
    if self.Owner:KeyDown( IN_ATTACK2 ) then
        return
    end
    if ( self.Owner.NextRun and self.Owner.NextRun > CurTime()) or not SERVER then return end
    self.Owner.NextRun  = CurTime() + 40
    self.Owner:SetNWFloat("w_stamina", 1)
    self.Owner:SetNWInt("runspeed", 950)
    self.Owner:SetNWInt("walkspeed",800)
    self.Owner:SetJumpPower(450)
    self.Owner.hasProtectionSuit = true

    timer.Simple(5,function()
        if IsValid(self) and self.Owner:IsValid() then
            self.Owner:SetNWFloat("w_stamina", 1)
            self.Owner:SetNWInt("runspeed", 400)
            self.Owner:SetNWInt("walkspeed",290)
            self.Owner:SetJumpPower(250)
        end
    end)

end

