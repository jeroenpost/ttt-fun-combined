if SERVER then
   AddCSLuaFile(  )
   --resource.AddFile("materials/vgui/ttt_fun_killicons/elites.png")
end
   
SWEP.HoldType = "duel"
   
 SWEP.PrintName = "Alice and Alexia"
SWEP.Slot = 1
if CLIENT then
   
   
   SWEP.Author = "GreenBlack"
 
   SWEP.Icon = "vgui/ttt_fun_killicons/elites.png"
end
 
SWEP.Base = "aa_base"

SWEP.Kind = WEAPON_PISTOL
SWEP.Primary.Recoil     = 0.5
SWEP.Primary.Damage = 45
SWEP.Primary.Delay = 0.40
SWEP.Primary.Cone = 0.0002
SWEP.Primary.ClipSize = 150
SWEP.Primary.Automatic = true
SWEP.Primary.DefaultClip = 150
SWEP.Primary.ClipMax = 150
SWEP.Primary.Ammo = "Pistol"

SWEP.Secondary.Delay = 0.3
SWEP.Secondary.ClipSize     = 150
SWEP.Secondary.DefaultClip  = 150
SWEP.Secondary.Automatic    = true
SWEP.Secondary.Ammo         = "Pistol"
SWEP.Secondary.ClipMax      = 150

SWEP.AutoSpawnable = false
SWEP.AmmoEnt = "item_ammo_pistol_ttt"
SWEP.NoSights = true

SWEP.ViewModel  =  "models/weapons/cstrike/c_pist_elite.mdl"
SWEP.WorldModel = "models/weapons/w_pist_elite.mdl"
SWEP.ViewModelFlip = false
SWEP.ViewModelFOV  = 54
SWEP.UseHands = true
 
SWEP.Primary.Sound = Sound( "weapons/357/357_fire2.wav" )
SWEP.Secondary.Sound = Sound( "weapons/rpg/rocket1.wav" )

SWEP.CanDecapitate= true
SWEP.HasFireBullet = true
function SWEP:PrimaryAttack( worldsnd )
 
   self.Weapon:SetNextSecondaryFire( CurTime() + 0.1 )
   self.Weapon:SetNextPrimaryFire( CurTime() + 0.15 )

   if not self:CanPrimaryAttack() then return end

      sound.Play(self.Primary.Sound, self:GetPos(), self.Primary.SoundLevel)

   self:TakePrimaryAmmo( 1 )
   --self:ShootTracer("AirboatGunHeavyTracer")

   self:ShootBullet( 9, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )

   local owner = self.Owner
   if not IsValid(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end
   self.Weapon:SendWeaponAnim(ACT_VM_PRIMARYATTACK)
 --  owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )
end

function SWEP:CanSecondaryAttack()
   if not IsValid(self.Owner) then return end

   if self:Clip1() < 1  then

      return false
   end
   return true
end




function SWEP:SecondaryAttack()
    local bucket, att, phys, tr
    if not self:CanSecondaryAttack() then return end
    self.Weapon:SetNextPrimaryFire( CurTime() + 0.15 )
    self.Weapon:SetNextSecondaryFire( CurTime() + 0.4 )

    if not self:CanPrimaryAttack() then return end

    self:EmitSound( "weapons/nyan/nya" .. math.random( 1, 2 ) .. ".mp3", 100, math.random( 20, 200 ) )

    self:TakePrimaryAmmo( 1 )
    self:ShootTracer("AirboatGunHeavyTracer")

    self:ShootBullet( 30, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )

    local owner = self.Owner
    if not IsValid(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end
    self.Weapon:SendWeaponAnim(ACT_VM_PRIMARYATTACK)
end

-- COLOR
function SWEP:Deploy()
    self.Weapon:SendWeaponAnim(ACT_VM_DRAW)
    self:SetColorAndMaterial(Color(255,255,255,255), "models/grisps/chips");
    return true
end


function SWEP:PreDrop()
    self:ColorReset()
end


function SWEP:Holster()
    self:ColorReset()
    return true
end

function SWEP:OnRemove()
    self:ColorReset()
end
function SWEP:OnDrop()
    self:ColorReset()

end

function SWEP:PreDrawViewModel()

    if SERVER or not IsValid(self.Owner) or not IsValid(self.Owner:GetViewModel()) then return end
    self.Owner:GetViewModel():SetColor(Color(255,255,255,255))
    self.Owner:GetViewModel():SetMaterial( "models/grisps/chips"   )

end