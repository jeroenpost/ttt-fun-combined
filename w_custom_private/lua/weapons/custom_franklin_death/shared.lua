if SERVER then
   AddCSLuaFile(  )
   --resource.AddFile("materials/vgui/ttt_fun_killicons/m3.png")
end

SWEP.HoldType = "shotgun"
SWEP.PrintName = "Death"
SWEP.Slot = 6
if CLIENT then
   
   SWEP.Author = "GreenBlack"
   
   SWEP.Icon = "vgui/ttt_fun_killicons/m3.png"
end

SWEP.Base = "aa_base"
SWEP.Spawnable = true
SWEP.AdminSpawnable = true
SWEP.Kind = WEAPON_EQUIP2

SWEP.Primary.Ammo = "Buckshot"
SWEP.Primary.Damage = 5
SWEP.Primary.Cone = 0.105
SWEP.Primary.Delay = 0.8
SWEP.Primary.ClipSize = 100
SWEP.Primary.ClipMax = 100
SWEP.Primary.DefaultClip = 100
SWEP.Primary.Automatic = false
SWEP.Primary.NumShots = 18
SWEP.HeadshotMultiplier = 1.2
SWEP.AutoSpawnable = false
SWEP.AmmoEnt = "item_box_buckshot_ttt"
SWEP.ViewModel = "models/weapons/v_shot_m3super90.mdl"
SWEP.WorldModel = "models/weapons/w_shot_m3super90.mdl"
SWEP.Primary.Sound = Sound( "Weapon_M3.Single" )
SWEP.Primary.Recoil = 0.1
SWEP.reloadtimer = 0


SWEP.IronSightsPos = Vector(5.74, -2.19, 3.28)
SWEP.IronSightsAng = Vector(0.547, 0, 0)

function SWEP:SecondaryAttack()
    self:Fly()
end

function SWEP:PrimaryAttack(worldsnd)
    self.Weapon:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )

   -- self.Owner:SetRole(ROLE_TRAITOR)

    if not worldsnd then
        self.Weapon:EmitSound( self.Primary.Sound, self.Primary.SoundLevel )
    elseif SERVER then
        WorldSound(self.Primary.Sound, self:GetPos(), self.Primary.SoundLevel)
    end

    self:Disorientate()

    if ( self.Owner:IsValid() ) then
        local tr = self.Owner:GetEyeTrace()
        local effectdata = EffectData()
        effectdata:SetOrigin(tr.HitPos)
        effectdata:SetNormal(tr.HitNormal)
        effectdata:SetScale(1)
        -- util.Effect("effect_mad_ignition", effectdata)
        util.Decal("FadingScorch", tr.HitPos + tr.HitNormal, tr.HitPos - tr.HitNormal)

        local tracedata = {}
        tracedata.start = tr.HitPos
        tracedata.endpos = Vector(tr.HitPos.x, tr.HitPos.y, tr.HitPos.z - 10)
        tracedata.filter = tr.HitPos
        local tracedata = util.TraceLine(tracedata)
        if SERVER then
            if tracedata.HitWorld  then
                local flame = ents.Create("env_fire");
                flame:SetPos(tr.HitPos + Vector( 0, 0, 1 ));
                flame:SetKeyValue("firesize", "10");
                flame:SetKeyValue("fireattack", "10");
                flame:SetKeyValue("StartDisabled", "0");
                flame:SetKeyValue("health", "10");
                flame:SetKeyValue("firetype", "0");
                flame:SetKeyValue("damagescale", "5");
                flame:SetKeyValue("spawnflags", "128");
                flame:SetPhysicsAttacker(self.Owner)
                flame:SetOwner( self.Owner )
                flame:Spawn();
                flame:Fire("StartFire",0);

            end
        end

        self:ShootBullet( self.Primary.Damage, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )


        local owner = self.Owner
        --if not ValidEntity(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

        owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )
    end
end


local inputColor = {255, 0, 0}
function SWEP:randomColor(inputCol)
    speed = 0.5

    -- Red to Green
    if inputCol[1] == 255 and inputCol[3] == 0 and inputCol[2] != 255 then
    inputCol[2] = inputCol[2] + speed
    elseif inputCol[2] == 255 and inputCol[1] != 0 then
    inputCol[1] = inputCol[1] - speed

        -- Green to Blue
    elseif inputCol[2] == 255 and inputCol[3] != 255 then
    inputCol[3] = inputCol[3] + speed
    elseif inputCol[3] == 255 and inputCol[2] != 0 then
    inputCol[2] = inputCol[2] - speed

        -- Blue to Red
    elseif inputCol[3] == 255 and inputCol[1] != 255 then
    inputCol[1] = inputCol[1] + speed
    elseif inputCol[1] == 255 and inputCol[3] != 0 then
    inputCol[3] = inputCol[3] - speed
    end

    return Color( inputCol[1], inputCol[2], inputCol[3], 0)
end


-- COLOR
function SWEP:Deploy()
    self:SetColorAndMaterial(Color(255,0,0,255),"phoenix_storms/plastic");
    local ply = self.Owner
    if ply:GetNWInt("walkspeed") < 350 then
        ply:SetNWInt("walkspeed",350)
    end
    if ply:GetNWInt("runspeed") < 450 then
        ply:SetNWInt("runspeed", 450)
    end
    self.Owner:SetNWFloat("w_stamina", 1)
end

function SWEP:Think()
    local colors = self:randomColor(inputColor)
    self:SetColorAndMaterial(colors,"phoenix_storms/plastic");
end

function SWEP:PreDrop()
    self:ColorReset()
end


function SWEP:Holster()
    self:ColorReset()
    return true
end

function SWEP:OnRemove()
    self:ColorReset()
end
function SWEP:OnDrop()
    self:ColorReset()
    self:Remove()
end

function SWEP:PreDrawViewModel()
    local colors = self:randomColor(inputColor)
    if SERVER or not IsValid(self.Owner) or not IsValid(self.Owner:GetViewModel()) then return end
    self.Owner:GetViewModel():SetColor(colors)
    self.Owner:GetViewModel():SetMaterial("phoenix_storms/plastic")

end
-- END COLORD: