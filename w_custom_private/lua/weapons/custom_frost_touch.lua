if SERVER then
    AddCSLuaFile(  )
    --resource.AddFile("materials/vgui/ttt_fun_killicons/knife.png")
end

SWEP.HoldType = "knife"
SWEP.PrintName    = "Frost Touch"
SWEP.Slot         = 8

if CLIENT then




    SWEP.ViewModelFlip = false


    SWEP.Icon = "vgui/ttt_fun_killicons/knife.png"
end

SWEP.Base               = "gb_camo_base"

SWEP.UseHands			= true
SWEP.ViewModelFlip		= false
SWEP.ViewModelFOV		= 54
SWEP.ViewModel          = "models/weapons/cstrike/c_knife_t.mdl"
SWEP.WorldModel         = "models/weapons/w_knife_t.mdl"

SWEP.DrawCrosshair      = false
SWEP.Primary.Damage         = 50
SWEP.Primary.ClipSize       = 10
SWEP.Primary.DefaultClip    = 10
SWEP.Primary.Automatic      = true
SWEP.Primary.Delay = 0.2
SWEP.Primary.Ammo       = "none"
SWEP.Secondary.Damage     = 25
SWEP.Secondary.ClipSize     = -1
SWEP.Secondary.DefaultClip  = -1
SWEP.Secondary.Automatic    = true
SWEP.Secondary.Ammo     = "none"
SWEP.Secondary.Delay = 0.5
SWEP.Camo = 37
SWEP.CustomCamo = true
SWEP.Kind = 76

SWEP.LimitedStock = true -- only buyable once

SWEP.IsSilent = true

-- Pull out faster than standard guns
SWEP.DeploySpeed = 1

function SWEP:Deploy()
    local function playerDiesSound( victim, weapon, killer )
        if( IsValid(killer) and killer:IsPlayer()  ) then
            local wep = killer:GetActiveWeapon()
            if IsValid(wep) then
                wep = wep:GetClass()
                if wep == "custom_frost_touch" then
                    gb_PlaySoundFromServer(gb_config.websounds.."stayfrosty.mp3", killer)
                end
            end
        end
    end

    hook.Add( "PlayerDeath", "playerDeathSound_frosttouch", playerDiesSound )
    return self.BaseClass.Deploy(self)

end
SWEP.hadknives = 0
function SWEP:PrimaryAttack()
    if self.Owner:KeyDown(IN_RELOAD) then
        if self.hadknives < 3 then
            self:throw_attack ("frostmine",  "physics/glass/glass_impact_hard3.wav", 500)
            self.hadknives = self.hadknives + 1
            return
        end
    end

    if self.Owner:KeyDown(IN_USE) then self:rFly() return end
    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
    self.Weapon:SetNextSecondaryFire( CurTime() + self.Secondary.Delay )

    self.Owner:LagCompensation(true)

    local spos = self.Owner:GetShootPos()
    local sdest = spos + (self.Owner:GetAimVector() * 70)

    local kmins = Vector(1,1,1) * -10
    local kmaxs = Vector(1,1,1) * 10

    local tr = util.TraceHull({start=spos, endpos=sdest, filter=self.Owner, mask=MASK_SHOT_HULL, mins=kmins, maxs=kmaxs})

    -- Hull might hit environment stuff that line does not hit
    if not IsValid(tr.Entity) then
        tr = util.TraceLine({start=spos, endpos=sdest, filter=self.Owner, mask=MASK_SHOT_HULL})
    end

    local hitEnt = tr.Entity

    if math.Rand(1,5) > 3 then self:Freeze() end

    -- effects
    if IsValid(hitEnt) then
        self.Weapon:SendWeaponAnim( ACT_VM_HITCENTER )

        local edata = EffectData()
        edata:SetStart(spos)
        edata:SetOrigin(tr.HitPos)
        edata:SetNormal(tr.Normal)
        edata:SetEntity(hitEnt)

        if hitEnt:IsPlayer() or hitEnt:GetClass() == "prop_ragdoll" then
            util.Effect("BloodImpact", edata)
        end
    else
        self.Weapon:SendWeaponAnim( ACT_VM_MISSCENTER )
    end

    if SERVER then
        self.Owner:SetAnimation( PLAYER_ATTACK1 )
    end


    if SERVER and tr.Hit and tr.HitNonWorld and IsValid(hitEnt) then
        if hitEnt:IsPlayer() then
            -- knife damage is never karma'd, so don't need to take that into
            -- account we do want to avoid rounding error strangeness caused by
            -- other damage scaling, causing a death when we don't expect one, so
            -- when the target's health is close to kill-point we just kill

            local dmg = DamageInfo()
            dmg:SetDamage(self.Primary.Damage)
            dmg:SetAttacker(self.Owner)
            dmg:SetInflictor(self.Weapon or self)
            dmg:SetDamageForce(self.Owner:GetAimVector() * 5)
            dmg:SetDamagePosition(self.Owner:GetPos())
            dmg:SetDamageType(DMG_SLASH)

            hitEnt:DispatchTraceAttack(dmg, spos + (self.Owner:GetAimVector() * 3), sdest)
        else
            bullet = {}
            bullet.Num    = 1
            bullet.Src    = self.Owner:GetShootPos()
            bullet.Dir    = self.Owner:GetAimVector()
            bullet.Spread = Vector(0, 0, 0)
            bullet.Tracer = 0
            bullet.Force  = 1
            bullet.Damage = 35
            self.Owner:FireBullets(bullet)
            self.Owner:ViewPunch(Angle(7, 0, 0))
        end
    end

    self.Owner:LagCompensation(false)
end

SWEP.NextReload = 0
SWEP.hadknives = 0
SWEP.NextEatMoney = 0
SWEP.hadbommm = 0
function SWEP:Reload()
    if self.Owner:KeyDown(IN_DUCK) then
        if CLIENT or (self.Owner.NextEatMoney and self.Owner.NextEatMoney > CurTime()) and self.hadbommm < 3 then return end
        self.Owner.NextEatMoney =  CurTime() + 5
        self.hadbommm = self.hadbommm + 1
        self:throw_attack2(1,1,15)
        end
    if self.Owner:KeyDown(IN_USE) then
        self:DropHealth("knife_health_station")
        return
    end
    if self.Owner:KeyDown(IN_ATTACK) then return end

    if self.NextReload > CurTime() then return end
    self.NextReload = CurTime() + self.Secondary.Delay
    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
    self.Weapon:SetNextSecondaryFire( CurTime() + self.Secondary.Delay )




    self.Weapon:SendWeaponAnim( ACT_VM_MISSCENTER )

    if SERVER then
        self.Owner:SetAnimation( PLAYER_ATTACK1 )

        local ply = self.Owner
        if not IsValid(ply) then return end

        local ang = ply:EyeAngles()

        if ang.p < 90 then
            ang.p = -10 + ang.p * ((90 + 10) / 90)
        else
            ang.p = 360 - ang.p
            ang.p = -10 + ang.p * -((90 + 10) / 90)
        end

        local vel = math.Clamp((90 - ang.p) * 5.5, 550, 800)

        local vfw = ang:Forward()
        local vrt = ang:Right()

        local src = ply:GetPos() + (ply:Crouching() and ply:GetViewOffsetDucked() or ply:GetViewOffset())

        src = src + (vfw * 1) + (vrt * 3)

        local thr = vfw * vel + ply:GetVelocity()

        local knife_ang = Angle(-28,0,0) + ang
        knife_ang:RotateAroundAxis(knife_ang:Right(), -90)

        local rnum = math.Rand(1,3)

        if rnum < 1.1 then


        elseif rnum < 2.1 then
            self:Freeze()
        elseif rnum < 3.1 then
          --  self:Blind()
        end

        local knife = ents.Create("ac_knife_proj")
        if not IsValid(knife) then return end
        knife:SetPos(src)
        knife:SetAngles(knife_ang)
        knife:Spawn()

        if rnum < 1.1 then

        knife.Damage = self.Secondary.Damage
        elseif rnum < 2.1 then

        elseif rnum < 3.1 then

        end

        knife.becomewep = "custom_dpk_knife"
        knife.removetimer =  0.01

        knife:SetOwner(ply)

        local phys = knife:GetPhysicsObject()
        if IsValid(phys) then
            phys:SetVelocity(thr)
            phys:AddAngleVelocity(Vector(0, 1500, 0))
            phys:Wake()
        end



    end
end

function SWEP:throw_attack2 (model_file, sound, away)
    local tr = self.Owner:GetEyeTrace();

    if IsValid( tr.Entity )  then
        if not tr.Entity.hasProtectionSuit then
            tr.Entity.hasProtectionSuit= true
            timer.Simple(5, function()
                if not IsValid( tr.Entity )  then return end
                tr.Entity.hasProtectionSuit = false
            end)
        end
    end

    self.BaseClass.ShootEffects (self);
    if (!SERVER) then return end;
    local ent = ents.Create ("boommine");
    --  ent:SetModel (model_file);
    ent:SetPos( tr.HitPos + self.Owner:GetAimVector() * -46 )
    ent:SetAngles (self.Owner:EyeAngles());
    ent:SetPhysicsAttacker(self.Owner)
    ent:Spawn();
    ent.WillExplode = self.Owner:IsTraitor()

    self.Mines = ent
    self.TrewMine = CurTime() + 3
    if math.Rand(1,25) < 2 then
        ent:Ignite(100,100)
    end
    local phys = ent:GetPhysicsObject();
    local shot_length = tr.HitPos:Length();
    -- phys:ApplyForceCenter (self.Owner:GetAimVector():GetNormalized() * math.pow (shot_length, 3) * 2);

    timer.Simple(away,function()
        if IsValid(ent) then
            ent:Remove()
        end
    end)

    cleanup.Add (self.Owner, "props", ent);
    undo.Create ("Thrown model");
    undo.AddEntity (ent);
    undo.SetPlayer (self.Owner);
    undo.Finish();
end

function SWEP:Equip()
    self.Weapon:SetNextPrimaryFire( CurTime() + (self.Primary.Delay * 1.5) )
    self.Weapon:SetNextSecondaryFire( CurTime() + (self.Secondary.Delay * 1.5) )
    self.BaseClass.Equip(self)
end

function SWEP:PreDrop()
    -- for consistency, dropped knife should not have DNA/prints
    self.fingerprints = {}
end



if CLIENT then
    function SWEP:DrawHUD()
        local tr = self.Owner:GetEyeTrace(MASK_SHOT)

        if tr.HitNonWorld and IsValid(tr.Entity) and tr.Entity:IsPlayer()
                and tr.Entity:Health() < (self.Secondary.Damage + 10) then

            local x = ScrW() / 2.0
            local y = ScrH() / 2.0

            surface.SetDrawColor(255, 0, 0, 255)

            local outer = 20
            local inner = 10
            surface.DrawLine(x - outer, y - outer, x - inner, y - inner)
            surface.DrawLine(x + outer, y + outer, x + inner, y + inner)

            surface.DrawLine(x - outer, y + outer, x - inner, y + inner)
            surface.DrawLine(x + outer, y - outer, x + inner, y - inner)

            draw.SimpleText("INSTANT KILL", "TabLarge", x, y - 30, COLOR_RED, TEXT_ALIGN_CENTER, TEXT_ALIGN_BOTTOM)
        end

        return self.BaseClass.DrawHUD(self)
    end
end

-- Spiderman



local sndPowerUp		= Sound("rope_hit.mp3")
local sndPowerDown		= Sound ("shoot_rope.mp3")
local sndTooFar			= Sound ("to_far.mp3")

function SWEP:Initialize()

    nextshottime = CurTime()
    self.BaseClass.Initialize(self)
end


SWEP.NextAmmoGive = 0
SWEP.NextAmmoTake = 0
SWEP.NextBeam = 0
function SWEP:SecondaryAttack()
    if self.Owner:KeyDown(IN_USE) then
        if self.NextBeam > CurTime() then return end
        self.NextBeam = CurTime() + 0.35
        self.Primary.NumShots = 1
        self.Primary.Cone = 0.00001
        self.LaserColor = Color(3,255,255,255)
        self:ShootTracer("LaserTracer_thick")
        if math.Rand(1,5) == 5 then self:Freeze() end
        self.Weapon:EmitSound("ambient/explosions/explode_9.wav")
        self:ShootBullet( 45, 0.01,1, self:GetPrimaryCone() )

    end
end
function SWEP:Think()

    if self.NextAmmoGive < CurTime() && self:Clip1() < 10 then
    self.NextAmmoGive = CurTime() + 3
    self:SetClip1( self:Clip1() + 1 )
    end

    if (!self.Owner || self.Owner == NULL) or self:Clip1() < 1 then

    if ( !self.Beam ) then return end
    if( IsValid( self.Beam) ) then
        self.Beam:Remove()
    end
    self.Beam = nil
    return
    end



    if ( self.Owner:KeyPressed( IN_ATTACK2 ) and not self.Owner:KeyDown(IN_USE) ) then

        self:StartAttack()

    elseif ( self.Owner:KeyDown( IN_ATTACK2 ) and not self.Owner:KeyDown(IN_USE)  ) then

        self:UpdateAttack()
        self.Weapon:SetNextSecondaryFire( CurTime() + 1 )

    elseif ( self.Owner:KeyReleased( IN_ATTACK2 )  and not self.Owner:KeyDown(IN_USE)  and not self.Owner:KeyReleased(IN_USE)  ) then

        self:EndAttack( true )

    end

    self.BaseClass.Think(self)


end

function SWEP:OnDrop() self:Remove() end

function SWEP:DoTrace( endpos )
    local trace = {}
    trace.start = self.Owner:GetShootPos()
    trace.endpos = trace.start + (self.Owner:GetAimVector() * 14096) --14096 is length modifier.
    if(endpos) then trace.endpos = (endpos - self.Tr.HitNormal * 7) end
    trace.filter = { self.Owner, self.Weapon }

    self.Tr = nil
    self.Tr = util.TraceLine( trace )
end



function SWEP:StartAttack()


    if not self:CanPrimaryAttack() then return end

    --Get begining and end poins of trace.
    local gunPos = self.Owner:GetShootPos() --Start of distance trace.
    local disTrace = self.Owner:GetEyeTrace() --Store all results of a trace in disTrace.
    local hitPos = disTrace.HitPos --Stores Hit Position of disTrace.

    --Calculate Distance
    --Thanks to rgovostes for this code.
    local x = (gunPos.x - hitPos.x)^2;
    local y = (gunPos.y - hitPos.y)^2;
    local z = (gunPos.z - hitPos.z)^2;
    local distance = math.sqrt(x + y + z);

    --Only latches if distance is less than distance CVAR
    local distanceCvar = GetConVarNumber("grapple_distance")
    inRange = false
    if distance <= distanceCvar then
        inRange = true
    end

    if inRange then
        if (SERVER) then

            if (!self.Beam) then --If the beam does not exist, draw the beam.
            if not self:CanPrimaryAttack() then return end
            --grapple_beam
            self.Beam = ents.Create( "trace1" )
            self.Beam:SetPos( self.Owner:GetShootPos() )
            self.Beam:Spawn()
            self:TakePrimaryAmmo(1)

            end

            self.Beam:SetParent( self.Owner )
            self.Beam:SetOwner( self.Owner )

        end


        self:DoTrace()
        self.speed = 10000 --Rope latch speed. Was 3000.
        self.startTime = CurTime()
        self.endTime = CurTime() + self.speed
        self.dtfix = -1

        if (SERVER && self.Beam) then
        self.Beam:GetTable():SetEndPos( self.Tr.HitPos )
        end

        self:UpdateAttack()

        self.Weapon:EmitSound( sndPowerDown )
    else
        --Play A Sound
        self.Weapon:EmitSound( sndTooFar )
    end
end

function SWEP:UpdateAttack()

    -- if not self:CanPrimaryAttack() then return end
    if !self.Tr or self:Clip1() < 1 then return end
    self.Owner:LagCompensation( true )


    if (!endpos) then endpos = self.Tr.HitPos end

    if (SERVER && self.Beam) then
    self.Beam:GetTable():SetEndPos( endpos )
    end

    lastpos = endpos



    if ( self.Tr.Entity:IsValid() ) then

        endpos = self.Tr.Entity:GetPos()
        if ( SERVER and IsValid(self.Beam) ) then
            self.Beam:GetTable():SetEndPos( endpos )
        end

    end

    local vVel = (endpos - self.Owner:GetPos())
    local Distance = endpos:Distance(self.Owner:GetPos())

    local et = (self.startTime + (Distance/self.speed))
    if(self.dtfix != 0) then
    self.dtfix = (et - CurTime()) / (et - self.startTime)
    end
    if(self.dtfix < 0) then
        self.Weapon:EmitSound( sndPowerUp )
        self.dtfix = 0
    end

    if(self.dtfix == 0) then
        zVel = self.Owner:GetVelocity().z
        vVel = vVel:GetNormalized()*(math.Clamp(Distance,0,25))
        if( SERVER ) then
            if  self.NextAmmoTake < CurTime() then
                self:TakePrimaryAmmo(1)
                self.NextAmmoTake = CurTime() + 1
                self.NextAmmoGive = CurTime() + 1.2
            end

            local gravity = GetConVarNumber("sv_gravity")

            vVel:Add(Vector(0,0,(gravity/50)*1.5))

            if(zVel < 0) then
                vVel:Sub(Vector(0,0,zVel/100))
            end
            self.Owner:SetVelocity(vVel)
        end
    end

    endpos = nil

    self.Owner:LagCompensation( false )

end

function SWEP:EndAttack( shutdownsound )

    if ( shutdownsound ) then
        self.Weapon:EmitSound( sndPowerDown )
    end

    if ( CLIENT ) then return end
    if ( !self.Beam ) then return end
    if( IsValid( self.Beam) ) then
        self.Beam:Remove()
    end
    self.Beam = nil

end


function SWEP:Holster()
    self:EndAttack( false )
    return true
end

function SWEP:OnRemove()
    self:EndAttack( false )
    return true
end

local ENT = {}

ENT.Type = "anim"
ENT.Base = "base_anim"


if SERVER then

    function ENT:Initialize()

        self.WillExplode = true
        self:SetModel("models/weapons/w_knife_t.mdl")
        self:SetModelScale(self:GetModelScale()*1,0)
        self.Deployed = CurTime()
        self.Entity:PhysicsInit(SOLID_VPHYSICS);
        self.Entity:SetMoveType(MOVETYPE_VPHYSICS);
        self.Entity:SetSolid(SOLID_VPHYSICS);
        self.Entity:SetCollisionGroup(COLLISION_GROUP_PROJECTILE)
        self.Damage = 1
        self.health =50
        local phys = self:GetPhysicsObject()
        if phys:IsValid() then
            phys:Wake()
            phys:EnableGravity(true);
            phys:AddGameFlag(FVPHYSICS_NO_PLAYER_PICKUP)
        end


        self:EmitSound("npc/roller/mine/rmine_blades_out3.wav")


    end

    function ENT:Blind(victim)
        if SERVER and IsValid(victim) and victim:IsPlayer() then
            victim.IsBlinded = true
            victim:SetNWBool("isblinded",true)
            umsg.Start( "ulx_blind", victim )
            umsg.Bool( true )
            umsg.Short( 255 )
            umsg.End()
        end

        if (SERVER) and IsValid(victim) and victim:IsPlayer() then

            timer.Create("ResetPLayerAfterBlided"..victim:SteamID(), 2,1, function()
                if not IsValid(victim)  then  return end

                if SERVER then
                    victim.IsBlinded = false
                    victim:SetNWBool("isblinded",false)
                    umsg.Start( "ulx_blind", victim )
                    umsg.Bool( false )
                    umsg.Short( 0 )
                    umsg.End()

                end
            end)
            return true
        end
    end

    function ENT:Touch( hitent )
        if  self.WillExplode then
            if not ents or not SERVER then return end

            if self.Deployed + 3 > CurTime() then return end

            if math.Rand(1,10) != 1 then
            self:Blind(hitent)
            timer.Simple(0.2,function()
                if not IsValid(hitent) then return end
                hitent.hasProtectionSuitTemp = true
                hitent:SetVelocity(hitent:GetUp() * 500 )
            end)
            return end


            local ent = ents.Create( "env_explosion" )
            if( IsValid( hitent ) ) then
                ent:SetPos( hitent:GetPos() )
                ent:SetOwner( self.Owner )
                ent:SetKeyValue( "iMagnitude", "50" )
                ent:Spawn()

                self:Remove()
                ent:Fire( "Explode", 0, 0 )
                ent:EmitSound( "weapons/big_explosion.mp3", 511, 100 )
            end

        end

        if not slowspeed then slowspeed = 25 end
        if not resetafter then resetafter = 3 end

        local ply = hitent
        if ply:IsPlayer() or ply:IsNPC()  then

            if not ply.oldspeedmul then
                ply.oldspeedmul =  ply:GetNWInt("speedmul")
            end
            if ply.lastfreeze and ply.lastfreeze > CurTime() then
                return
            end
            ply.lastfreeze = CurTime() + 5
            ply.runslowed =true
            ply:SetNWInt("speedmul", slowspeed)
            ply:SetNWInt("speedmul",slowspeed)
            ply.oldwalkspeed = ply:GetWalkSpeed()
            ply.oldrunspeed = ply:GetRunSpeed()
            ply:SetWalkSpeed( slowspeed )
            ply:SetRunSpeed( slowspeed )

            timer.Create("NormalizeRunSpeedFreeze"..ply:UniqueID() ,resetafter,1,function()
                if IsValid(ply) and  ply.oldspeedmul  then
                    if tonumber(ply.oldspeedmul) < 220 then ply.oldspeedmul = 260 end

                    if not (ply.oldrunspeed) or tonumber(ply.oldrunspeed) < 260 then ply.oldrunspeed = 260 end
                    if not ply.oldwalkspeed or tonumber(ply.oldwalkspeed) < 220 then ply.oldwalkspeed = 260 end

                    ply.runslowed =false
                    ply:SetNWInt("speedmul", ply.oldspeedmul)
                    ply.oldspeedmul = false
                    ply:SetRunSpeed(ply.oldrunspeed )
                    ply:SetRunSpeed(ply.oldwalkspeed )

                end
            end)
        end
    end

    function ENT:TakeDamage(damageAmount)
        self.health = self.health - damageAmount
        if self.health > 20 then
            local ent = ents.Create( "env_explosion" )
            if( IsValid( self ) ) then
                ent:SetPos( self:GetPos() )
                ent:SetOwner( self.Owner )
                ent:SetKeyValue( "iMagnitude", "50" )
                ent:Spawn()

                self:Remove()
                ent:Fire( "Explode", 0, 0 )
                ent:EmitSound( "weapons/big_explosion.mp3", 511, 100 )
            end
        end


    end

    function ENT:Explode()
        if self.exploded then return end self.exploded = true
        local ent = ents.Create( "env_explosion" )
        ent:SetPos( self:GetPos() )
        ent:SetOwner( self.Owner )
        ent:SetKeyValue( "iMagnitude", "50" )
        ent:Spawn()

        self:Remove()
        ent:Fire( "Explode", 0, 0 )
        ent:EmitSound( "weapons/big_explosion.mp3", 511, 100 )

    end


    function ENT:Use( activator, caller )

        if  self.WillExplode then
            if not ents or not SERVER then return end


            if self.Deployed + 3 > CurTime() then return end

            local ent = ents.Create( "env_explosion" )
            if( IsValid( activator ) ) then
                ent:SetPos( self:GetPos() )
                ent:SetOwner( self.Owner )
                ent:SetKeyValue( "iMagnitude", "50" )
                ent:Spawn()

                self:Remove()
                ent:Fire( "Explode", 0, 0 )
                ent:EmitSound( "weapons/big_explosion.mp3", 511, 100 )
            end
            timer.Simple(0.2,function()
                if not IsValid(activator) then return end
                activator:SetVelocity(activator:GetUp() * 50 )
            end)

        else
            self.Entity:Remove()
            if ( activator:IsPlayer() ) then
                if activator:Health() < 150 then
                    if self.lower then
                        activator:SetHealth(activator:Health()+10)
                    else
                        activator:SetHealth(activator:Health()+25)
                    end
                end
                activator:EmitSound("crisps/eat.mp3")
            end
        end
    end


end

scripted_ents.Register( ENT, "frostmine", true )