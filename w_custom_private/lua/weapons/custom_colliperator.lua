if (SERVER) then

    AddCSLuaFile( )
    SWEP.Weight                = 5
    SWEP.AutoSwitchTo        = false
    SWEP.AutoSwitchFrom        = false


end

if ( CLIENT ) then

    SWEP.DrawAmmo            = true
    SWEP.DrawCrosshair        = false
    SWEP.ViewModelFOV        = 65
    SWEP.ViewModelFlip        = false
    SWEP.CSMuzzleFlashes    = true

end
SWEP.Base                   = "aa_base"
SWEP.Category                = "EXP 2 Weapons"
SWEP.PrintName                 = "The Colliperator Shot"
SWEP.HoldType                 = "rpg"
SWEP.Slot = 65
SWEP.SlotPos = 2

SWEP.Author            = "Mighty Lolrus"
SWEP.Icon = "vgui/ttt_fun_killicons/bow.png"

SWEP.ViewModel                  = "models/weapons/v_gbip_scoub.mdl"
SWEP.WorldModel 				= "models/weapons/w_gbip_scoub.mdl"


SWEP.CanBuy = {ROLE_TRAITOR}
SWEP.EquipMenuData = {
    type  = "item_weapon",
    name  = "Infinite Bow",
    desc  = "Silent Bow."
}

SWEP.Kind = 65
SWEP.HoldType = "smg"

SWEP.Spawnable            = true
SWEP.AdminSpawnable        = true
SWEP.AutoSpawnable        = true
SWEP.Primary.Sound			= Sound("weapons/requests_studio/gb2/fire.mp3")
SWEP.Primary.Delay          = 1.8
SWEP.Primary.Recoil         = 1
SWEP.Primary.Automatic = true
SWEP.Primary.Ammo = "357"
SWEP.Primary.Damage = 50
SWEP.Primary.Cone = 0.005
SWEP.Primary.ClipSize = 25
SWEP.Primary.ClipMax = 50 -- keep mirrored to ammo
SWEP.Primary.DefaultClip = 25
SWEP.HeadshotMultiplier = 3
SWEP.Secondary.Sound = Sound("Default.Zoom")

SWEP.IsSilent = true
SWEP.IronSightsPos = Vector(-13.41, -13.308, 18.549)
SWEP.IronSightsAng = Vector(1.5, -0.401, -53.741)

SWEP.Offset = {
    Pos = {
        Up = 0,
        Right = -1,
        Forward = -4.5,
    },
    Ang = {
        Up = 0,
        Right = 14,
        Forward = 30,
    }
}
function SWEP:OnDrop()

    local wep = ents.Create(self:GetClass())
    wep:SetPos(self:GetPos()+Vector(0,100,0))
    wep:SetAngles(self:GetAngles())
    wep.IsDropped = true
    self:Remove(self)
    wep:Spawn()

end
function SWEP:DrawWorldModel( )
    local hand, offset, rotate

    if not IsValid( self.Owner ) then
        self:DrawModel( )
        return
    end

    if not self.Hand then
        self.Hand = self.Owner:LookupAttachment( "anim_attachment_rh" )
    end

    hand = self.Owner:GetAttachment( self.Hand )

    if not hand then
        self:DrawModel( )
        return
    end

    offset = hand.Ang:Right( ) * self.Offset.Pos.Right + hand.Ang:Forward( ) * self.Offset.Pos.Forward + hand.Ang:Up( ) * self.Offset.Pos.Up

    hand.Ang:RotateAroundAxis( hand.Ang:Right( ), self.Offset.Ang.Right )
    hand.Ang:RotateAroundAxis( hand.Ang:Forward( ), self.Offset.Ang.Forward )
    hand.Ang:RotateAroundAxis( hand.Ang:Up( ), self.Offset.Ang.Up )

    self:SetRenderOrigin( hand.Pos + offset )
    self:SetRenderAngles( hand.Ang )

    self:DrawModel( )
end

function SWEP:PrimaryAttack()

    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
    local mode = self:GetNWString("shootmode","Normal Arrows")
    if mode == "Normal Arrows" then
        self:SetNextPrimaryFire( CurTime() + 0.6 )
        self.Weapon:EmitSound( self.Primary.Sound, 80 )
        self.Primary.Damage = 50
        self.Primary.Recoil = 4
        self.Primary.NumShots = 1
        self.Primary.Cone = 0.00001
        self:ShootBullet( self.Primary.Damage, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )
        -- self:TakePrimaryAmmo( 1 )
    end
    if mode == "Explosive Arrows" then
        self:SetNextPrimaryFire( CurTime() + 0.6 )
        self.Weapon:EmitSound( self.Primary.Sound, 80 )
        self.Primary.Damage = 15
        self.Primary.Recoil = 4
        self.Primary.NumShots = 1
        self.Primary.Cone = 0.00001
        self:ShootBullet( self.Primary.Damage, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )
        --self:TakePrimaryAmmo( 1 )
        if SERVER then self:MakeExplosion(nil,50) end
    end
    if mode == "Fire Arrows" then
        self:SetNextPrimaryFire( CurTime() + 0.6 )
        self.Weapon:EmitSound( self.Primary.Sound, 80 )
        self.Primary.Damage = 45
        self.Primary.Recoil = 4
        self.Primary.NumShots = 1
        self.Primary.Cone = 0.00001
        -- self:ShootBullet( self.Primary.Damage, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )
        --self:TakePrimaryAmmo( 1 )
        if SERVER then self:FireBolt(6,50,0.0001) end
    end
end


function SWEP:SetZoom(state)
    if CLIENT then
        return
    elseif IsValid(self.Owner) and self.Owner:IsPlayer() then
        if state then
            self.Owner:SetFOV(60, 0.3)
        else
            self.Owner:SetFOV(0, 0.2)
        end
    end
end

-- Add some zoom to ironsights for this gun
function SWEP:SecondaryAttack()
    if not self.IronSightsPos then return end
    if self.Weapon:GetNextSecondaryFire() > CurTime() then return end

    bIronsights = not self:GetIronsights()

    self:SetIronsights( bIronsights )

    if SERVER then
        self:SetZoom(bIronsights)
    else
        self:EmitSound(self.Secondary.Sound)
    end

    self.Weapon:SetNextSecondaryFire( CurTime() + 0.3)
end

function SWEP:PreDrop()
    self:SetZoom(false)
    self:SetIronsights(false)
    return self.BaseClass.PreDrop(self)
end

SWEP.NextReload = 0
function SWEP:Reload()
    if self.Owner:KeyDown(IN_ATTACK) or self.NextReload > CurTime() then
        return
    end

    self.NextReload = CurTime() + 0.3
    local mode = self:GetNWString("shootmode",'Normal Arrows')
    self:SetIronsights(false)

    if mode == "Normal Arrows" then  self:SetNWString("shootmode","Explosive Arrows")  end
    if mode == "Explosive Arrows" then  self:SetNWString("shootmode","Fire Arrows") end
    if mode == "Fire Arrows" then  self:SetNWString("shootmode","Normal Arrows") end

    self.Weapon:SetNextPrimaryFire( CurTime() + 0.8 )
    return



end


function SWEP:Holster()
    self:SetIronsights(false)
    self:SetZoom(false)
    return true
end


if CLIENT then
    local scope = surface.GetTextureID("sprites/scope")
    function SWEP:DrawHUD()

        local shotttext = "Shootmode: "..self:GetNWString("shootmode","Normal Arrows").."\nR to change"
        surface.SetFont( "ChatFont" );
        local size_x, size_y = surface.GetTextSize( shotttext );

        draw.RoundedBox( 6, ScrW( ) / 2 - size_x / 2 - 5, ScrH( ) - 100, size_x + 15, size_y + 10, Color( 0, 0, 0, 150 ) );
        draw.DrawText( shotttext , "ChatFont", ScrW( ) / 2, ScrH( ) - 100 + 5, Color( 255, 30, 30, 255 ), TEXT_ALIGN_CENTER );


        if self:GetIronsights() then
            surface.SetDrawColor( 0, 0, 0, 255 )


        else
            return self.BaseClass.DrawHUD(self)
        end
    end

    function SWEP:AdjustMouseSensitivity()
        return (self:GetIronsights() and 0.2) or nil
    end
end
