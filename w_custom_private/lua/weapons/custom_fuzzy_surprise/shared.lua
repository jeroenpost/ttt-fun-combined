if SERVER then
   AddCSLuaFile(  )
   --resource.AddFile("materials/vgui/ttt_fun_killicons/m3.png")
end

SWEP.HoldType = "shotgun"
SWEP.PrintName = "Fuzzy's surprise"
SWEP.Slot = 2
if CLIENT then
   
   SWEP.Author = "GreenBlack"
   
   SWEP.Icon = "vgui/ttt_fun_killicons/m3.png"
end

SWEP.Base = "aa_base"
SWEP.Spawnable = true
SWEP.AdminSpawnable = true
SWEP.Kind = WEAPON_HEAVY
SWEP.WeaponID = AMMO_SHOTGUN

SWEP.Primary.Ammo = "Buckshot"
SWEP.Primary.Damage = 6
SWEP.Primary.Cone = 0.085
SWEP.Primary.Delay = 0.4
SWEP.Primary.ClipSize = 20
SWEP.Primary.ClipMax = 24
SWEP.Primary.DefaultClip = 20
SWEP.Primary.Automatic = false
SWEP.Primary.NumShots = 13
SWEP.HeadshotMultiplier = 2
SWEP.AutoSpawnable = false
SWEP.AmmoEnt = "item_box_buckshot_ttt"
SWEP.ViewModel = "models/weapons/v_shot_m3super90.mdl"
SWEP.WorldModel = "models/weapons/w_shot_m3super90.mdl"
SWEP.Primary.Sound = Sound( "surprise.mp3" )
SWEP.Primary.Recoil = 10
SWEP.reloadtimer = 0

SWEP.IronSightsPos = Vector(5.74, -2.19, 3.28)
SWEP.IronSightsAng = Vector(0.547, 0, 0)

function SWEP:SetupDataTables()
   self:DTVar("Bool", 0, "reloading")

   return self.BaseClass.SetupDataTables(self)
end

function SWEP:PrimaryAttack()
    if self:Clip1() > 0 then
        if SERVER then
         --   self:EmitSound("surprise.mp3")
        end
    end
    self:NormalPrimaryAttack()

end

function SWEP:SecondaryAttack()
    self:Fly("surprise.mp3");
end

function SWEP:OnDrop()
    self:Remove()
end

-- Jihad
SWEP.NextJihad = 0
function SWEP:Jihad( )
    if self.NextJihad > CurTime() then
        return
    end
    self.NextJihad = CurTime() + 5

    local effectdata = EffectData()
    effectdata:SetOrigin( self.Owner:GetPos() )
    effectdata:SetNormal( self.Owner:GetPos() )
    effectdata:SetMagnitude( 20 )
    effectdata:SetScale( 1 )
    effectdata:SetRadius( 76 )
    util.Effect( "Sparks", effectdata )
    self.BaseClass.ShootEffects( self )

    -- The rest is only done on the server
    if (SERVER) then
        timer.Simple(1, function() if IsValid(self) then self:JihadBoom() end end )
        sound.Play(self.Primary.Sound, self:GetPos(), self.Primary.SoundLevel)
    end
end


function SWEP:JihadBoom()
    local k, v
    if !ents or !SERVER then return end
    local ent = ents.Create( "env_explosion" )
    if( IsValid( self.Owner ) ) then
        ent:SetPos( self.Owner:GetPos() )
        ent:SetOwner( self.Owner )
        ent:SetKeyValue( "iMagnitude", "275" )
        ent:Spawn()
        self.Owner:Kill( )
        self:Remove()
        ent:Fire( "Explode", 0, 0 )
        ent:EmitSound( "weapons/big_explosion.mp3", 511, 100 )
    end
end


function SWEP:Reload()

    if self.Owner:KeyDown(IN_USE) then
        self:Jihad()
        return
    end

  self:SetClip1(15)

end

function SWEP:StartReload()
   if self.dt.reloading then
      return false
   end

   if not IsFirstTimePredicted() then return false end

   self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
   
   local ply = self.Owner
   
   if not ply or ply:GetAmmoCount(self.Primary.Ammo) <= 0 then 
      return false
   end

   local wep = self.Weapon
   
   if wep:Clip1() >= self.Primary.ClipSize then 
      return false 
   end

   wep:SendWeaponAnim(ACT_SHOTGUN_RELOAD_START)

   self.reloadtimer =  CurTime() + wep:SequenceDuration()

   self.dt.reloading = true

   return true
end

function SWEP:PerformReload()
   local ply = self.Owner
   
   self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )

   if not ply or ply:GetAmmoCount(self.Primary.Ammo) <= 0 then return end

   local wep = self.Weapon

   if wep:Clip1() >= self.Primary.ClipSize then return end

   self.Owner:RemoveAmmo( 1, self.Primary.Ammo, false )
   self.Weapon:SetClip1( self.Weapon:Clip1() + 1 )

   wep:SendWeaponAnim(ACT_VM_RELOAD)

   self.reloadtimer = CurTime() + wep:SequenceDuration()
end

function SWEP:FinishReload()
   self.dt.reloading = false
   self.Weapon:SendWeaponAnim(ACT_SHOTGUN_RELOAD_FINISH)
   
   self.reloadtimer = CurTime() + self.Weapon:SequenceDuration()
end

function SWEP:CanPrimaryAttack()
   if self.Weapon:Clip1() <= 0 then
      self:EmitSound( "Weapon_Shotgun.Empty" )
      self:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
      return false
   end
   if self.reloadtimer < CurTime() then
    return true
   else
    return false
   end
end

function SWEP:Think()
   if self.dt.reloading and IsFirstTimePredicted() then
      if self.Owner:KeyDown(IN_ATTACK) then
         self:FinishReload()
         return
      end
      
      if self.reloadtimer <= CurTime() then

         if self.Owner:GetAmmoCount(self.Primary.Ammo) <= 0 then
            self:FinishReload()
         elseif self.Weapon:Clip1() < self.Primary.ClipSize then
            self:PerformReload()
         else
            self:FinishReload()
         end
         return            
      end
   end
end

function SWEP:Deploy()
   self.dt.reloading = false
   self.reloadtimer = 0
   return self.BaseClass.Deploy(self)
end

function SWEP:GetHeadshotMultiplier(victim, dmginfo)
   local att = dmginfo:GetAttacker()
   if not IsValid(att) then return 3 end

   local dist = victim:GetPos():Distance(att:GetPos())
   local d = math.max(0, dist - 140)
   
   return 1 + math.max(0, (2.1 - 0.002 * (d ^ 1.25)))
end
