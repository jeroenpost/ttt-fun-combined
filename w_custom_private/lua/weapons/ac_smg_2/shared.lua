if SERVER then
    AddCSLuaFile()
end


SWEP.HoldType = "ar2"
SWEP.PrintName = "Punishers SMG"
if CLIENT then


    SWEP.Slot = 2

    SWEP.ViewModelFlip   = false
    SWEP.ViewModelFOV = 60
    --   SWEP.Icon = "VGUI/ttt/icon_rttt_smg"
end

SWEP.Base				= "weapon_tttbase"

SWEP.Kind = WEAPON_HEAVY
SWEP.CanDecapitate= true

SWEP.Primary.Damage      = 9
SWEP.Primary.Delay       = 0.065
SWEP.Primary.Cone        = 0.03
SWEP.Primary.ClipSize    = 100
SWEP.Primary.ClipMax     = 500
SWEP.Primary.DefaultClip = 100
SWEP.Primary.Automatic   = true
SWEP.Primary.Ammo        = "smg1"
SWEP.Primary.Recoil      = 1.15
SWEP.Primary.Sound 		= Sound("Weapon_SMG1.Single")

SWEP.AutoSpawnable = false

SWEP.AmmoEnt = "item_ammo_smg1_ttt"

SWEP.ViewModel  = "models/weapons/v_smg1.mdl"
SWEP.WorldModel = "models/weapons/w_smg1.mdl"

SWEP.IronSightsPos 		= Vector (-6.4318, -2.0031, 4.5371)
SWEP.IronSightsAng 		= Vector (0, 0, 0)
SWEP.RunArmOffset 		= Vector (9.071, 0, 1.6418)
SWEP.RunArmAngle 	    = Vector (-12.9765, 26.8708, 0)

if SERVER then
    include("weaponfunctions/fly.lua")
end

function SWEP:SetZoom(state)
    if CLIENT then return end
    if state then
        self.Owner:SetFOV(35, 0.5)
    else
        self.Owner:SetFOV(0, 0.2)
    end
end

-- Add some zoom to ironsights for this gun
function SWEP:SecondaryAttack()
    if self.Owner:KeyDown(IN_USE) then
        if not self.IronSightsPos then return end
        if self.Weapon:GetNextSecondaryFire() > CurTime() then return end

        bIronsights = not self:GetIronsights()

        self:SetIronsights( bIronsights )

        if SERVER then
            self:SetZoom(bIronsights)
        end
    elseif SERVER then
        self:Fly()
    end

    self.Weapon:SetNextSecondaryFire(CurTime() + 0.3)

end

function SWEP:PreDrop()
    self:SetZoom(false)
    self:SetIronsights(false)
    return self.BaseClass.PreDrop(self)
end

function SWEP:Reload()
    self.Weapon:DefaultReload( ACT_VM_RELOAD );
    self:SetIronsights( false )
    self:SetZoom(false)
end


function SWEP:Holster()
    self:SetIronsights(false)
    self:SetZoom(false)
    return true
end

function SWEP:Reload()
    if !( ( self.Weapon:Clip1() ) < ( self.Weapon:Ammo1() ) ) then return end
    if !( ( self.Weapon:Clip1() ) < ( self.Primary.ClipSize ) ) then return end
    if !( self.Weapon:Ammo1() >= 0 ) then return end
    if !( self.Weapon:Clip1() >= 0 ) then return end

    self.Weapon:DefaultReload( ACT_VM_RELOAD )
    self:EmitSound( "Weapon_SMG1.Reload" )
end