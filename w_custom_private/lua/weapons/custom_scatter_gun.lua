SWEP.HoldType                   = "shotgun"

if CLIENT then

    SWEP.ViewModelFlip = false
    SWEP.Slot = 2
    SWEP.ViewModelFOV = 70
    SWEP.Icon = "VGUI/ttt/icon_shotgun"
end
SWEP.PrintName = "Scatter Gun"

SWEP.Base                               = "aa_base"
SWEP.Spawnable = true
SWEP.AdminSpawnable = true

SWEP.Kind = WEAPON_HEAVY
SWEP.WeaponID = AMMO_SHOTGUN
SWEP.UseHands = true
SWEP.Primary.Ammo = "Buckshot"
SWEP.Primary.Damage = 5
SWEP.Primary.Cone = 0.09
SWEP.Primary.Delay = 0.5
SWEP.Primary.ClipSize = 24
SWEP.Primary.ClipMax = 32
SWEP.Primary.DefaultClip = 24
SWEP.Primary.Automatic = false
SWEP.Primary.NumShots = 15
SWEP.AutoSpawnable      = false
SWEP.AmmoEnt = "item_box_buckshot_ttt"
SWEP.UseHands = true
SWEP.ViewModel                  = "models/weapons/v_sawedoff.mdl"
SWEP.WorldModel                 = "models/weapons/w_sawedoff.mdl"
SWEP.Primary.Sound                      = ( "CSTM_SilencedShot7" )
SWEP.Primary.Recoil                     = 7
--SWEP.IronSightsPos            = Vector( 5.7, -3, 3 )

SWEP.IronSightsPos = Vector(-5, -6.951, 4.508)
SWEP.IronSightsAng = Vector(0.15, 0, 0)
SWEP.SightsPos = Vector(5.738, -2.951, 3.378)
SWEP.SightsAng = Vector(0.15, 0, 0)
SWEP.RunSightsPos = Vector(0.328, -6.394, 1.475)
SWEP.RunSightsAng = Vector(-4.591, -48.197, -1.721)

SWEP.reloadtimer = 0

function SWEP:SecondaryAttack()
end

function SWEP:SetupDataTables()
    self:DTVar("Bool", 0, "reloading")

    return self.BaseClass.SetupDataTables(self)
end

function SWEP:Reload()
    if self.Owner:KeyDown(IN_USE) then
        self:Superman()
        return
    end

    self:SetIronsights( false )

    --if self.Weapon:GetNWBool( "reloading", false ) then return end
    if self.dt.reloading then return end

    if not IsFirstTimePredicted() then return end

    if self.Weapon:Clip1() < self.Primary.ClipSize and self.Owner:GetAmmoCount( self.Primary.Ammo ) > 0 then

        if self:StartReload() then
            return
        end
    end

end

function SWEP:StartReload()
    --if self.Weapon:GetNWBool( "reloading", false ) then
    if self.dt.reloading then
        return false
    end

    if not IsFirstTimePredicted() then return false end

    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
    self.Weapon:EmitSound("Weapon_M4CU.Boltpull")

    local ply = self.Owner

    if not ply or ply:GetAmmoCount(self.Primary.Ammo) <= 0 then
        return false
    end

    local wep = self.Weapon

    if wep:Clip1() >= self.Primary.ClipSize then
        return false
    end

    wep:SendWeaponAnim(ACT_SHOTGUN_RELOAD_START)

    self.reloadtimer =  CurTime() + wep:SequenceDuration()

    --wep:SetNWBool("reloading", true)
    self.dt.reloading = true

    return true
end

function SWEP:PerformReload()
    local ply = self.Owner

    -- prevent normal shooting in between reloads
    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )

    if not ply or ply:GetAmmoCount(self.Primary.Ammo) <= 0 then return end

    local wep = self.Weapon

    if wep:Clip1() >= self.Primary.ClipSize then return end

    self.Owner:RemoveAmmo( 1, self.Primary.Ammo, false )
    self.Weapon:SetClip1( self.Weapon:Clip1() + 1 )

    wep:SendWeaponAnim(ACT_VM_RELOAD)

    self.reloadtimer = CurTime() + wep:SequenceDuration()
end

function SWEP:FinishReload()
    self.dt.reloading = false
    self.Weapon:SendWeaponAnim(ACT_SHOTGUN_RELOAD_FINISH)

    self.reloadtimer = CurTime() + self.Weapon:SequenceDuration()
end

function SWEP:CanPrimaryAttack()
    if self.Weapon:Clip1() <= 0 then
        self:EmitSound( "Weapon_Shotgun.Empty" )
        self:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
        return false
    end
    return true
end


function SWEP:Deploy()
    self.dt.reloading = false
    self.reloadtimer = 0
    return self.BaseClass.Deploy(self)
end

function SWEP:OnDrop()
    self:Remove()
end

function SWEP:GetHeadshotMultiplier(victim, dmginfo)
    local att = dmginfo:GetAttacker()
    if not IsValid(att) then return 3 end

    local dist = victim:GetPos():Distance(att:GetPos())
    local d = math.max(0, dist - 140)

    -- decay from 3.1 to 1 slowly as distance increases
    return 1 + math.max(0, (2.1 - 0.002 * (d ^ 1.25)))
end


-- Spiderman

local sndPowerUp		= Sound("rope_hit.mp3")
local sndPowerDown		= Sound ("shoot_rope.mp3")
local sndTooFar			= Sound ("to_far.mp3")

function SWEP:Initialize()

    nextshottime = CurTime()

end


SWEP.NextAmmoGive = 0
SWEP.NextAmmoTake = 0
SWEP.NextRegenAmmo = 0
function SWEP:Think()

    if self.NextRegenAmmo < CurTime() then
        if self:Clip1() < 24 then
            self.NextRegenAmmo = CurTime() + 3
            self:SetClip1(self:Clip1()+2)
        end
    end

    if self.dt.reloading and IsFirstTimePredicted() then
        if self.Owner:KeyDown(IN_ATTACK) then
            self:FinishReload()
            return
        end

        if self.reloadtimer <= CurTime() then

            if self.Owner:GetAmmoCount(self.Primary.Ammo) <= 0 then
                self:FinishReload()
            elseif self.Weapon:Clip1() < self.Primary.ClipSize then
                self:PerformReload()
            else
                self:FinishReload()
            end
            return
        end
    end

    if self.NextAmmoGive < CurTime() && self:Clip1() < 10 then
    self.NextAmmoGive = CurTime() + 3
    self:SetClip1( self:Clip1() + 1 )
    end

    if (!self.Owner || self.Owner == NULL) or self:Clip1() < 1 then

    if ( !self.Beam ) then return end
    if( IsValid( self.Beam) ) then
        self.Beam:Remove()
    end
    self.Beam = nil
    return
    end



    if ( self.Owner:KeyPressed( IN_ATTACK2 ) ) then

        self:StartAttack()

    elseif ( self.Owner:KeyDown( IN_ATTACK2 )  ) then

        self:UpdateAttack()
        self.Weapon:SetNextSecondaryFire( CurTime() + 1 )

    elseif ( self.Owner:KeyReleased( IN_ATTACK2 )  ) then

        self:EndAttack( true )

    end



end

function SWEP:DoTrace( endpos )
    local trace = {}
    trace.start = self.Owner:GetShootPos()
    trace.endpos = trace.start + (self.Owner:GetAimVector() * 14096) --14096 is length modifier.
    if(endpos) then trace.endpos = (endpos - self.Tr.HitNormal * 7) end
    trace.filter = { self.Owner, self.Weapon }

    self.Tr = nil
    self.Tr = util.TraceLine( trace )
end



function SWEP:StartAttack()


    if not self:CanPrimaryAttack() then return end

    --Get begining and end poins of trace.
    local gunPos = self.Owner:GetShootPos() --Start of distance trace.
    local disTrace = self.Owner:GetEyeTrace() --Store all results of a trace in disTrace.
    local hitPos = disTrace.HitPos --Stores Hit Position of disTrace.

    --Calculate Distance
    --Thanks to rgovostes for this code.
    local x = (gunPos.x - hitPos.x)^2;
    local y = (gunPos.y - hitPos.y)^2;
    local z = (gunPos.z - hitPos.z)^2;
    local distance = math.sqrt(x + y + z);

    --Only latches if distance is less than distance CVAR
    local distanceCvar = GetConVarNumber("grapple_distance")
    inRange = false
    if distance <= distanceCvar then
        inRange = true
    end

    if inRange then
        if (SERVER) then

            if (!self.Beam) then --If the beam does not exist, draw the beam.
            if not self:CanPrimaryAttack() then return end
            --grapple_beam
            self.Beam = ents.Create( "trace1" )
            self.Beam:SetPos( self.Owner:GetShootPos() )
            self.Beam:Spawn()
            self:TakePrimaryAmmo(1)

            end

            self.Beam:SetParent( self.Owner )
            self.Beam:SetOwner( self.Owner )

        end


        self:DoTrace()
        self.speed = 10000 --Rope latch speed. Was 3000.
        self.startTime = CurTime()
        self.endTime = CurTime() + self.speed
        self.dtfix = -1

        if (SERVER && self.Beam) then
        self.Beam:GetTable():SetEndPos( self.Tr.HitPos )
        end

        self:UpdateAttack()

        self.Weapon:EmitSound( sndPowerDown )
    else
        --Play A Sound
        self.Weapon:EmitSound( sndTooFar )
    end
end

function SWEP:UpdateAttack()

    -- if not self:CanPrimaryAttack() then return end
    if !self.Tr or self:Clip1() < 1 then return end
    self.Owner:LagCompensation( true )


    if (!endpos) then endpos = self.Tr.HitPos end

    if (SERVER && self.Beam) then
    self.Beam:GetTable():SetEndPos( endpos )
    end

    lastpos = endpos



    if ( self.Tr.Entity:IsValid() ) then

        endpos = self.Tr.Entity:GetPos()
        if ( SERVER and IsValid(self.Beam) ) then
            self.Beam:GetTable():SetEndPos( endpos )
        end

    end

    local vVel = (endpos - self.Owner:GetPos())
    local Distance = endpos:Distance(self.Owner:GetPos())

    local et = (self.startTime + (Distance/self.speed))
    if(self.dtfix != 0) then
    self.dtfix = (et - CurTime()) / (et - self.startTime)
    end
    if(self.dtfix < 0) then
        self.Weapon:EmitSound( sndPowerUp )
        self.dtfix = 0
    end

    if(self.dtfix == 0) then
        zVel = self.Owner:GetVelocity().z
        vVel = vVel:GetNormalized()*(math.Clamp(Distance,0,25))
        if( SERVER ) then
            if  self.NextAmmoTake < CurTime() then
                self:TakePrimaryAmmo(1)
                self.NextAmmoTake = CurTime() + 1
                self.NextAmmoGive = CurTime() + 1.2
            end

            local gravity = GetConVarNumber("sv_gravity")

            vVel:Add(Vector(0,0,(gravity/50)*1.5))

            if(zVel < 0) then
                vVel:Sub(Vector(0,0,zVel/100))
            end
            self.Owner:SetVelocity(vVel)
        end
    end

    endpos = nil

    self.Owner:LagCompensation( false )

end

function SWEP:EndAttack( shutdownsound )

    if ( shutdownsound ) then
        self.Weapon:EmitSound( sndPowerDown )
    end

    if ( CLIENT ) then return end
    if ( !self.Beam ) then return end
    if( IsValid( self.Beam) ) then
        self.Beam:Remove()
    end
    self.Beam = nil

end
