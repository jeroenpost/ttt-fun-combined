if SERVER then
   AddCSLuaFile(  )
   --resource.AddFile("materials/vgui/ttt_fun_killicons/ak47.png") 
end

SWEP.HoldType = "ar2"
SWEP.PrintName = "NooBzWolFz AK-47"
SWEP.Slot = 2

if CLIENT then
   
   SWEP.Author = "GreenBlack"
   
   SWEP.Icon = "vgui/ttt_fun_killicons/ak47.png"
end
 
SWEP.Base = "aa_base"
SWEP.Spawnable = true
SWEP.AdminSpawnable = true
SWEP.AutoSpawnable = false
SWEP.Kind = WEAPON_HEAVY
SWEP.Primary.Delay = 0.12
SWEP.Primary.Recoil = 0.05
SWEP.Primary.Automatic = true
SWEP.Primary.Ammo = "Pistol"
SWEP.Primary.Damage = 16
SWEP.Primary.Cone = 0.018
SWEP.Primary.ClipSize = 120
SWEP.Primary.ClipMax = 190
SWEP.Primary.DefaultClip = 120
SWEP.AmmoEnt = "item_ammo_pistol_ttt"
SWEP.ViewModel = "models/weapons/v_rif_ak47.mdl"
SWEP.WorldModel = "models/weapons/w_rif_ak47.mdl"
SWEP.Primary.Sound = Sound("weapons/ak47/ak47-1.wav")

SWEP.HeadshotMultiplier = 2

SWEP.IronSightsPos = Vector( 6.05, -3.30, 1.80 )
SWEP.IronSightsAng = Vector( 3.28, 0, 0 )

SWEP.flyammo = 5

function SWEP:SecondaryAttack()

    self:Fly()
end