if ( SERVER ) then


	SWEP.HasRail = true
	SWEP.NoAimpoint = true
	SWEP.NoACOG = true
	SWEP.NoEOTech = true
	SWEP.NoDocter = true
	
	SWEP.RequiresRail = false
	SWEP.NoLaser = true
end

SWEP.CanDecapitate= true
SWEP.MuzVel = 261.154
SWEP.PrintName			= "Laser Eyes"

	SWEP.SlotPos = 0 //			= 1
SWEP.Slot = 8

SWEP.HasFlare = true
SWEP.Base				= "gb_camo_base"
SWEP.Category = "Extra Pack (Sidearms)"
SWEP.FireModes = {"semi" }
SWEP.Kind = WEAPON_EQUIP2

SWEP.Spawnable			= true
SWEP.AdminSpawnable		= true

SWEP.HoldType = "normal"
SWEP.ViewModelFOV = 80
SWEP.ViewModelFlip = true
SWEP.ViewModel			= "models/weapons/c_arms_citizen.mdl"
SWEP.WorldModel			= 'models/props_wasteland/panel_leverHandle001a.mdl'
SWEP.ShowViewModel = true
SWEP.ShowWorldModel = true
SWEP.ViewModelBonescales = {}

SWEP.Weight				= 5
SWEP.AutoSwitchTo		= false
SWEP.AutoSwitchFrom		= false

SWEP.Primary.Sound			= Sound("default.zoom")
SWEP.Primary.SoundLevel			= 40
SWEP.Primary.Recoil			= 2
SWEP.Primary.Damage			= 0
SWEP.Primary.NumShots		= 1
SWEP.Primary.Cone			= 0.015
SWEP.Primary.ClipSize		= 900
SWEP.Primary.Delay			= 0.18
SWEP.Primary.DefaultClip	= 900
SWEP.Primary.Automatic		= true
SWEP.Primary.Ammo			= ".50AE"
SWEP.InitialHoldtype = "pistol"
SWEP.InHoldtype = "pistol"
SWEP.CantSilence = true
SWEP.NoBoltAnim = true
SWEP.ChamberAmount = 2
SWEP.SprintAndShoot = true
SWEP.HeadshotMultiplier = 1.2

-- Animation speed/custom reload function related
SWEP.IsReloading = false
SWEP.AnimPrefix = ""
SWEP.ReloadSpeed = 1
SWEP.ShouldBolt = false
SWEP.ReloadDelay = 0
SWEP.IncAmmoPerc = 0.75 -- Amount of frames required to pass (in percentage) of the reload animation for the weapon to have it's amount of ammo increased
SWEP.FOVZoom = 85

SWEP.Primary.Sound2 = { "weapons/warden/warden1.mp3","weapons/warden/warden2.mp3","weapons/warden/warden3.mp3"}

-- Dynamic accuracy related
SWEP.ShotsAmount 			= 0
SWEP.ConeDecAff				= 0
SWEP.DefRecoil				= 3
SWEP.CurCone				= 0.04
SWEP.DecreaseRecoilTime 	= 0
SWEP.ConeAff1 				= 0 -- Crouching/standing
SWEP.ConeAff2 				= 0 -- Using ironsights

SWEP.UnConeTime				= 0 -- Amount of time after firing the last shot that needs to pass until accuracy increases
SWEP.FinalCone				= 0 -- Self explanatory
SWEP.VelocitySensivity		= 1 -- Percentage of how much the cone increases depending on the player's velocity (moving speed). Rifles - 100%; SMGs - 80%; Pistols - 60%; Shotguns - 20%
SWEP.HeadbobMul 			= 1
SWEP.IsSilenced 			= false
SWEP.IronsightsCone 		= 0.02
SWEP.HipCone 				= 0.046
SWEP.ConeInaccuracyAff1 = 0.5

SWEP.Secondary.Automatic	= false
SWEP.Secondary.Ammo			= "none"

SWEP.IsUsingIronsights 		= false
SWEP.TargetMul = 0
SWEP.SetAndForget			= false

SWEP.AnimCyc = 1

SWEP.IronSightsPos = Vector(6.0749, -5.5216, 2.3984)
SWEP.IronSightsAng = Vector(2.5174, -0.0099, 0)

SWEP.AimPos = Vector(-0.24, -1.803, 2.319)
SWEP.AimAng = Vector(0, 0, 0)

SWEP.ChargePos = Vector (5.4056, -10.3522, -4.0017)
SWEP.ChargeAng = Vector (-1.7505, -55.5187, 68.8356)

SWEP.MeleePos = Vector(0.843, -0.956, 0.537)
SWEP.MeleeAng = Vector(0, 8.1, 0)

SWEP.SafePos = Vector(0, -7.954, -8.11)
SWEP.SafeAng = Vector(70, 0, 0)

SWEP.FlipOriginsPos = Vector(0, 0, 0)
SWEP.FlipOriginsAng = Vector(0, 0, 0)

SWEP.CustomizePos = Vector(0, -10, -8.11)
SWEP.CustomizeAng = Vector(70, 0, 0)

SWEP.Camo = 9
SWEP.HoldType =  "normal"
SWEP.HeadshotMultiplier = 1

function SWEP:PrimaryAttack()
   if self:Clip1() > 0 and SERVER then
        self.Owner:EmitSound( table.Random(  { "weapons/warden/warden1.mp3","weapons/warden/warden2.mp3","weapons/warden/warden3.mp3"} ),100,100)
    end
    self:NormalPrimaryAttack()

end

function SWEP:PreDrop()
    local wep = ents.Create(self:GetClass())
    if IsValid(self.Owner) then
        wep:SetPos(self.Owner:GetPos()+Vector(80,0,0))
        wep:SetAngles(self.Owner:GetAngles())
    else
        wep:SetPos(self:GetPos()+Vector(0,80,0))
        wep:SetAngles(self:GetAngles())
    end

    wep.IsDropped = true
    self:Remove(self)
    wep:Spawn()
end

function SWEP:SecondaryAttack()
    self:Fly()
end

SWEP.LaserColor = Color(255,0,0,255)
function SWEP:ShootBullet( damage, num_bullets, aimcone )

    local bullet = {}
    bullet.Num 		= num_bullets
    bullet.Src 		= self.Owner:GetShootPos()
    bullet.Dir 		= self.Owner:GetAimVector()
    bullet.Spread 	= Vector( 0.001, 0.001, 0 )
    bullet.Tracer	= 1
    bullet.Force	= 10
    bullet.Damage	= 9
    bullet.AmmoType = "Pistol"
    bullet.HullSize = 2
    bullet.TracerName = "LaserTracer_thick"

    self.Owner:FireBullets( bullet )
    self:ShootEffects()

end

SWEP.Icon = "vgui/ttt_fun_killicons/lasereyes_kill.png"


SWEP.Offset = {
    Pos = {
        Up = 3,
        Right = 0,
        Forward = -7.0,
    },
    Ang = {
        Up = 0,
        Right = -90,
        Forward = 0,
    }
}
SWEP.CanBuy = { ROLE_TRAITOR, ROLE_DETECTIVE }
SWEP.EquipMenuData = {
    type = "Weapon",
    desc = "Shoot lasers with your eyes!"
};


function SWEP:DrawWorldModel( )
    local hand, offset, rotate

    if not IsValid( self.Owner ) then
        self:DrawModel( )
        return
    end

    if not self.Hand then
        self.Hand = self.Owner:LookupAttachment( "eyes" )
    end

    hand = self.Owner:GetAttachment( self.Hand )

    if not hand then
        self:DrawModel( )
        return
    end

    offset = hand.Ang:Right( ) * self.Offset.Pos.Right + hand.Ang:Forward( ) * self.Offset.Pos.Forward + hand.Ang:Up( ) * self.Offset.Pos.Up

    hand.Ang:RotateAroundAxis( hand.Ang:Right( ), self.Offset.Ang.Right )
    hand.Ang:RotateAroundAxis( hand.Ang:Forward( ), self.Offset.Ang.Forward )
    hand.Ang:RotateAroundAxis( hand.Ang:Up( ), self.Offset.Ang.Up )

    self:SetRenderOrigin( hand.Pos + offset )
    self:SetRenderAngles( hand.Ang )

    self:DrawModel( )
end