if ( SERVER ) then

	AddCSLuaFile(  )
	SWEP.HasRail = true
	SWEP.NoDocter = true
	SWEP.DoesntDropRail = true
	SWEP.NoVertGrip = false
	
	SWEP.RequiresRail = false
	SWEP.NoLaser = true
	SWEP.NoBipod = false
end

SWEP.Kind = WEAPON_NADE
SWEP.PsID = "cm_m14erb2"
SWEP.BulletLength = 7.62
SWEP.CaseLength = 51

SWEP.MuzVel = 557.741

SWEP.Attachments = {
	[1] = {"kobra", "riflereflex", "eotech", "aimpoint", "elcan", "acog", "ballistic"},
	[2] = {"vertgrip", "grenadelauncher", "bipod"},
	[3] = {"laser"}}
	
SWEP.InternalParts = {
	[1] = {{key = "hbar"}, {key = "lbar"}},
	[2] = {{key = "hframe"}},
	[3] = {{key = "ergonomichandle"}},
	[4] = {{key = "customstock"}},
	[5] = {{key = "lightbolt"}, {key = "heavybolt"}},
	[6] = {{key = "gasdir"}}}

if ( CLIENT ) then
	SWEP.ActToCyc = {}
	SWEP.ActToCyc["ACT_VM_DRAW"] = 0
	SWEP.ActToCyc["ACT_VM_RELOAD"] = 0.52
	
	SWEP.PrintName			= "Professional Revolver"
	SWEP.Author				= "Counter-Strike"
	SWEP.Slot = 3 //				= 3
	SWEP.SlotPos = 0 //			= 3
	SWEP.IconLetter			= "z"
	SWEP.DifType = true
	SWEP.SmokeEffect = "cstm_child_smoke_large"
	SWEP.SparkEffect = "cstm_child_sparks_large"
	SWEP.Muzzle = "cstm_muzzle_br"
	SWEP.ACOGDist = 6
	SWEP.BallisticDist = 4.5

	
	SWEP.VertGrip_Idle = {
		["Bip01 L Finger3"] = {vector = Vector(0, 0, 0), angle = Angle(7.847, -11.122, 9.029)},
		["Bip01 L Finger4"] = {vector = Vector(0, 0, 0), angle = Angle(10.013, -46.21, 5.519)},
		["Bip01 L Finger32"] = {vector = Vector(0, 0, 0), angle = Angle(0, 96.419, 0)},
		["Bip01 L Finger41"] = {vector = Vector(0, 0, 0), angle = Angle(0, 13.923, 0)},
		["Bip01 L Finger1"] = {vector = Vector(0, 0, 0), angle = Angle(7.061, 62.048, 19.615)},
		["Bip01 L Finger2"] = {vector = Vector(0, 0, 0), angle = Angle(10.904, 15.048, 10.253)},
		["Bip01 L UpperArm"] = {vector = Vector(1.82, -1.094, -1.308), angle = Angle(-23.269, -8.254, 48.487)},
		["Bip01 L Finger0"] = {vector = Vector(0, 0, 0), angle = Angle(16.545, 9.385, 0)},
		["Bip01 L Finger42"] = {vector = Vector(0, 0, 0), angle = Angle(0, 77.956, 0)},
		["Bip01 L Finger22"] = {vector = Vector(0, 0, 0), angle = Angle(0, 64.885, 0)},
		["Bip01 L Finger12"] = {vector = Vector(0, 0, 0), angle = Angle(0, 37.831, 0)},
		["Bip01 L Finger02"] = {vector = Vector(0, 0, 0), angle = Angle(0, 83.957, 0)},
		["Bip01 L Finger11"] = {vector = Vector(0, 0, 0), angle = Angle(0, -18.858, 0)},
		["Bip01 L Finger01"] = {vector = Vector(0, 0, 0), angle = Angle(0, 15.902, 0)},
		["Bip01 L Hand"] = {vector = Vector(0, 0, 0), angle = Angle(-9.334, 25.951, 19.15)}
	}
	
	SWEP.GrenadeLauncher_Idle = {
		["Bip01 L Finger4"] = {vector = Vector(0, 0, 0), angle = Angle(0, -28.1, 0)},
		["Bip01 L Finger41"] = {vector = Vector(0, 0, 0), angle = Angle(0, -15.388, 0)},
		["Bip01 L Finger2"] = {vector = Vector(0, 0, 0), angle = Angle(0, -7.209, 0)},
		["Bip01 L Finger02"] = {vector = Vector(0, 0, 0), angle = Angle(0, 14.467, 0)},
		["Bip01 L Finger42"] = {vector = Vector(0, 0, 0), angle = Angle(0, 32.63, 0)},
		["Bip01 L Finger32"] = {vector = Vector(0, 0, 0), angle = Angle(0, 53.669, 0)},
		["Bip01 L Finger22"] = {vector = Vector(0, 0, 0), angle = Angle(0, 49.236, 0)},
		["Bip01 L Finger31"] = {vector = Vector(0, 0, 0), angle = Angle(0, -15.502, 0)},
		["Bip01 L Finger21"] = {vector = Vector(0, 0, 0), angle = Angle(0, -56.855, 0)},
		["Bip01 L Finger11"] = {vector = Vector(0, 0, 0), angle = Angle(0, -48.238, 0)},
		["Bip01 L UpperArm"] = {vector = Vector(0.439, -0.857, -2.714), angle = Angle(0, 0, 0)},
		["Bip01 L Finger3"] = {vector = Vector(0, 0, 0), angle = Angle(0, -30.637, 0)}
	}

	SWEP.VElements = {
		["aimpoint"] = { type = "Model", model = "models/wystan/attachments/aimpoint.mdl", bone = "M14_Body", rel = "", pos = Vector(-0.242, -4.685, -4.365), angle = Angle(0, 0, 0), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
		["ballistic"] = { type = "Model", model = "models/bunneh/scope01.mdl", bone = "M14_Body", rel = "", pos = Vector(-1.517, -5.093, 1.843), angle = Angle(0, -90, 0), size = Vector(1.2, 1.2, 1.2), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
		["silencer"] = { type = "Model", model = "models/props_c17/oildrum001.mdl", bone = "M14_Body", rel = "", pos = Vector(0, 14.041, 0.319), angle = Angle(0, 0, 90), size = Vector(0.05, 0.05, 0.15), color = Color(255, 255, 255, 0), surpresslightning = false, material = "models/bunneh/silencer", skin = 0, bodygroup = {} },
		["kobra"] = { type = "Model", model = "models/attachments/cmore.mdl", bone = "M14_Body", rel = "", pos = Vector(0.01, 1.807, 1.289), angle = Angle(0, 0, 0), size = Vector(0.75, 0.75, 0.75), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
		["riflereflex"] = { type = "Model", model = "models/attachments/kascope.mdl", bone = "M14_Body", rel = "", pos = Vector(0, 4.638, 1.49), angle = Angle(0, 0, 0), size = Vector(0.75, 0.75, 0.75), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
		["acog"] = { type = "Model", model = "models/wystan/attachments/2cog.mdl", bone = "M14_Body", rel = "", pos = Vector(-0.357, -3.681, -4.5), angle = Angle(0, 0, 0), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
		["grenadelauncher"] = { type = "Model", model = "models/wystan/attachments/m203.mdl", bone = "M14_Body", rel = "", pos = Vector(-0.217, -5.074, -3.596), angle = Angle(0, 0, 0), size = Vector(0.8, 0.8, 0.8), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
		["elcan"] = { type = "Model", model = "models/bunneh/elcan.mdl", bone = "M14_Body", rel = "", pos = Vector(-0.35, -3.681, -4.421), angle = Angle(0, 0, 0), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
		["bipod"] = { type = "Model", model = "models/wystan/attachments/bipod.mdl", bone = "M14_Body", rel = "", pos = Vector(-0.318, 12, -2.29), angle = Angle(0, 0, 0), size = Vector(0.899, 0.899, 0.899), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {[1] = 1} },
		["eotech"] = { type = "Model", model = "models/wystan/attachments/2otech557sight.mdl", bone = "M14_Body", rel = "", pos = Vector(0.287, -8.625, -10.195), angle = Angle(4.443, -90, 0), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
		["laser"] = { type = "Model", model = "models/props_c17/FurnitureBoiler001a.mdl", bone = "M14_Body", rel = "", pos = Vector(-0.626, 9, 0.768), angle = Angle(90, 0, -90), size = Vector(0.029, 0.029, 0.029), color = Color(255, 255, 255, 0), surpresslightning = false, material = "models/bunneh/silencer", skin = 0, bodygroup = {} },
		["vertgrip"] = { type = "Model", model = "models/wystan/attachments/foregrip1.mdl", bone = "M14_Body", rel = "", pos = Vector(-10.57, 6.512, -3.151), angle = Angle(0, 90, 0), size = Vector(0.699, 0.699, 0.699), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
    ,["acog"] = { type = "Model", model = "models/wystan/attachments/2cog.mdl", bone = "gun", rel = "", pos = Vector(3.385, -9.297, -4.967), angle = Angle(0, 180, 0), size = Vector(0.775, 0.775, 0.775), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
    ["laser"] = { type = "Model", model = "models/props_c17/FurnitureBoiler001a.mdl", bone = "gun", rel = "", pos = Vector(3.477, -15.711, -2.152), angle = Angle(0, 0, 90), size = Vector(0.019, 0.019, 0.019), color = Color(255, 255, 255, 0), surpresslightning = false, material = "models/bunneh/silencer", skin = 0, bodygroup = {} },
    ["silencer"] = { type = "Model", model = "models/props_c17/oildrum001.mdl", bone = "gun", rel = "", pos = Vector(3.187, -17.796, -2.077), angle = Angle(0, 0, -90), size = Vector(0.05, 0.05, 0.15), color = Color(255, 255, 255, 0), surpresslightning = false, material = "models/bunneh/silencer", skin = 0, bodygroup = {} }
    ,["weapon"] = { type = "Model", model = "models/weapons/w_pist_magnum.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(-0.16, 0.573, 0.609), angle = Angle(0, 0, 180), size = Vector(1, 1, 1), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }

    }
	
	SWEP.WElements = {
       ["weapon"] = { type = "Model", model = "models/weapons/w_pist_magnum.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(-0.16, 0.873, 0.609), angle = Angle(0, 0, 180), size = Vector(1, 1, 1), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
    }

	SWEP.NoRail = true
end

function SWEP:CamoPreDrawViewModel()
    if  not IsValid(self.Owner) or not IsValid(self.Owner:GetViewModel()) then return end
    self.Owner:GetViewModel():SetColor( Color(255,255,255,1) )
    self.Owner:GetViewModel():SetMaterial( "engine/occlusionproxy"  )
end

function SWEP:PreDrawViewModel()
    self:CamoPreDrawViewModel()
end

SWEP.Base				= "cstm_base_pistol"
SWEP.Category = "Extra Pack (BRs)"
SWEP.FireModes = {"semi"}

SWEP.Spawnable			= true
SWEP.AdminSpawnable		= true

SWEP.HoldType = "pistol"
SWEP.ViewModelFOV = 70
SWEP.ViewModelFlip = false
SWEP.ViewModel = "models/weapons/c_pistol.mdl"
SWEP.WorldModel = "models/weapons/w_pist_deagle.mdl"
SWEP.ShowViewModel = true
SWEP.ShowWorldModel = false
SWEP.ViewModelBonescales = {}

SWEP.Weight				= 5
SWEP.AutoSwitchTo		= false
SWEP.AutoSwitchFrom		= false

SWEP.Primary.Sound			= Sound("CSTM_M14")
SWEP.Primary.Recoil			= 0.3
SWEP.Primary.Damage			= 95
SWEP.Primary.NumShots		= 1
SWEP.Primary.Cone			= 0.0001
SWEP.Primary.ClipSize		= 6
SWEP.Primary.Delay			= 1
SWEP.Primary.DefaultClip	= 100
SWEP.Primary.Automatic		= false
SWEP.Primary.Ammo			= "7.62x51MM"
SWEP.InitialHoldtype = "revolver"
SWEP.InHoldtype = "revolver"
SWEP.SilencedSound = Sound("CSTM_SilencedShot2")
SWEP.SilencedVolume = 82
SWEP.HeadshotMultiplier = 1.5

-- Animation speed/custom reload function related
SWEP.IsReloading = false
SWEP.NoBoltAnim = true
SWEP.AnimPrefix = ""
SWEP.ReloadSpeed = 1
SWEP.ShouldBolt = false
SWEP.ReloadDelay = 0
SWEP.IncAmmoPerc = 0.6 -- Amount of frames required to pass (in percentage) of the reload animation for the weapon to have it's amount of ammo increased
SWEP.NoAuto = true
SWEP.PlaybackRate = 1.5

-- Dynamic accuracy related
SWEP.ShotsAmount 			= 0
SWEP.ConeDecAff				= 0
SWEP.DefRecoil				= 1.4
SWEP.CurCone				= 0.04
SWEP.DecreaseRecoilTime 	= 0
SWEP.ConeAff1 				= 0 -- Crouching/standing
SWEP.ConeAff2 				= 0 -- Using ironsights

SWEP.UnConeTime				= 0 -- Amount of time after firing the last shot that needs to pass until accuracy increases
SWEP.FinalCone				= 0 -- Self explanatory
SWEP.VelocitySensivity		= 1.6 -- Percentage of how much the cone increases depending on the player's velocity (moving speed). Rifles - 100%; SMGs - 80%; Pistols - 60%; Shotguns - 20%
SWEP.HeadbobMul 			= 1
SWEP.IsSilenced 			= false
SWEP.IronsightsCone			= 0.0015
SWEP.HipCone 				= 0.06
SWEP.ConeInaccuracyAff1 = 0.7

SWEP.Secondary.Automatic	= false
SWEP.Secondary.Ammo			= "40MM_HE"
SWEP.Secondary.ClipSize = 1
SWEP.Secondary.DefaultClip	= 0

SWEP.IsUsingIronsights 		= false
SWEP.TargetMul = 0
SWEP.SetAndForget			= false


SWEP.IronSightsPos = Vector( -4.6, -2, 0.65 )
SWEP.IronSightsAng = Vector(0, 0, 0)

SWEP.AimPos = Vector( -1.65, 8, 2.95 )
SWEP.AimAng =  Vector(0, 0, 0)

SWEP.ChargePos =  Vector( -1.65, 8, 2.95 )
SWEP.ChargeAng = Vector (-1.7505, -55.5187, 68.8356)

SWEP.ScopePos =  Vector( -1.65, 8, 2.95 )
SWEP.ScopeAng = Vector(0, 0, 0)

SWEP.ReflexPos =  Vector( -1.65, 8, 2.95 )
SWEP.ReflexAng = Vector(0, 0, 0)

SWEP.KobraPos =  Vector( -1.65, 8, 2.95 )
SWEP.KobraAng = Vector(0, 0, 0)

SWEP.RReflexPos =  Vector( -1.65, 8, 2.95 )
SWEP.RReflexAng = Vector(0, 0, 0)

SWEP.BallisticPos =  Vector( -1.65, 8, 2.95 )
SWEP.BallisticAng = Vector(0, 0, 0)

SWEP.NoFlipOriginsPos =  Vector( -1.65, 8, 2.95 )
SWEP.NoFlipOriginsAng = Vector(0, 0, 0)

SWEP.ACOGPos =  Vector( -1.65, 8, 2.95 )
SWEP.ACOGAng = Vector(0, 0, 0)

SWEP.ACOG_BackupPos =  Vector( -1.65, 8, 2.95 )
SWEP.ACOG_BackupAng = Vector(0, 0, 0)

SWEP.ELCANPos = Vector(-2.224, -3.072, -0.196)
SWEP.ELCANAng = Vector(0, 0, 0)

SWEP.ELCAN_BackupPos = Vector(-2.224, -3.072, -1.165)
SWEP.ELCAN_BackupAng = Vector(0, 0, 0)

function SWEP:PreDrop(yes)
    if yes then self:Remove() end
end

if CLIENT then
	function SWEP:CanUseBackUp()
		if self.VElements["acog"].color.a == 255 or self.VElements["elcan"].color.a == 255 then
			return true
		end
		
		return false
	end
	
	function SWEP:UseBackUp()
		if self.VElements["acog"].color.a == 255 then
			if self.AimPos == self.ACOG_BackupPos then
				self.AimPos = self.ACOGPos
				self.AimAng = self.ACOGAng
			else
				self.AimPos = self.ACOG_BackupPos
				self.AimAng = self.ACOG_BackupAng
			end
		elseif self.VElements["elcan"].color.a == 255 then
			if self.AimPos == self.ELCAN_BackupPos then
				self.AimPos = self.ELCANPos
				self.AimAng = self.ELCANAng
			else
				self.AimPos = self.ELCAN_BackupPos
				self.AimAng = self.ELCAN_BackupAng
			end
		end
	end
end


-- crosshair
if CLIENT then
    local sights_opacity = CreateConVar("ttt_ironsights_crosshair_opacity", "0.8", FCVAR_ARCHIVE)
    local crosshair_brightness = CreateConVar("ttt_crosshair_brightness", "1.0", FCVAR_ARCHIVE)
    local crosshair_size = CreateConVar("ttt_crosshair_size", "1.0", FCVAR_ARCHIVE)
    local disable_crosshair = CreateConVar("ttt_disable_crosshair", "0", FCVAR_ARCHIVE)


    function SWEP:DrawHUD()
        local client = LocalPlayer()
        if disable_crosshair:GetBool() or (not IsValid(client)) then return end

        local sights = (not self.NoSights) and self:GetIronsights()

        local x = ScrW() / 2.0
        local y = ScrH() / 2.0
        local scale = math.max(0.2,  10 * self:GetPrimaryCone())

        local LastShootTime = self:LastShootTime()
        scale = scale * (2 - math.Clamp( (CurTime() - LastShootTime) * 5, 0.0, 1.0 ))

        local alpha = sights and sights_opacity:GetFloat() or 1
        local bright = crosshair_brightness:GetFloat() or 1

        -- somehow it seems this can be called before my player metatable
        -- additions have loaded
        if client.IsTraitor and client:IsTraitor() then
            surface.SetDrawColor(255 * bright,
                50 * bright,
                50 * bright,
                255 * alpha)
        else
            surface.SetDrawColor(0,
                255 * bright,
                0,
                255 * alpha)
        end

        local gap = 20 * scale * (sights and 0.8 or 1)
        local length = gap + (25 * crosshair_size:GetFloat()) * scale
        surface.DrawLine( x - length, y, x - gap, y )
        surface.DrawLine( x + length, y, x + gap, y )
        surface.DrawLine( x, y - length, x, y - gap )
        surface.DrawLine( x, y + length, x, y + gap )

        if self.HUDHelp then
            self:DrawHelp()
        end
    end



end