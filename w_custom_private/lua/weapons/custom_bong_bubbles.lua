if ( SERVER ) then

    AddCSLuaFile( )

end

if ( CLIENT ) then
    SWEP.DrawAmmo		= true
    SWEP.DrawCrosshair	= true
    SWEP.CSMuzzleFlashes	= false
    SWEP.ViewModelFlip	= false
end

SWEP.PrintName			= "Private's Shield of Protection"
SWEP.Author				= "a very sleepy space king"
SWEP.Contact			= ""
SWEP.Purpose			= ""
SWEP.Instructions		= ""
SWEP.Slot				= 3
SWEP.Category			= ""
SWEP.HoldType			= "grenade"

SWEP.Spawnable			= true
SWEP.AdminSpawnable		= true
SWEP.UseHands = true

SWEP.WorldModel	= "models/weapons/w_grenade.mdl"
SWEP.ViewModel	= "models/weapons/c_grenade.mdl"

SWEP.Primary.ClipSize		= -1
SWEP.Primary.DefaultClip	= 8
SWEP.Primary.Automatic   	= false
SWEP.Primary.Ammo         	= "slam"

SWEP.Secondary.ClipSize		= -1
SWEP.Secondary.DefaultClip	= -1
SWEP.Secondary.Automatic   	= false
SWEP.Secondary.Ammo         = "none"
SWEP.Base = "gb_camo_base"
SWEP.CustomCamo = true
SWEP.Camo = 18

local StopNextFallDamage = false

function SWEP:Initialize()
    self:SetHoldType("grenade") --?!?
    self.BaseClass.Initialize(self)
end


function SWEP:PrimaryAttack()
    --if self:Ammo1() == 0 then return end
    --self.Owner:RemoveAmmo( 1, "slam" )
    self:SetNextPrimaryFire( CurTime() + 10 )
    self.Weapon:SendWeaponAnim( ACT_VM_THROW )
    self.Owner:DoAttackEvent()
    local tr = self.Owner:GetEyeTrace();
    if self.Owner.HealShield and IsValid(self.Owner.HealShield) then self.Owner.HealShield:Remove() end
    if ( SERVER ) then
        local ent = ents.Create ( "proj_shieldnade" );
        ent:Spawn();
        ent:SetPos ( self.Owner:EyePos() + ( self.Owner:GetAimVector() * 16 ) );
        ent:SetAngles( self.Owner:EyeAngles() + Angle( 0, 20, 0 ) )
        ent:SetOwner( self.Owner )
        local phys = ent:GetPhysicsObject();
        phys:SetVelocity ( self.Owner:GetAimVector():GetNormalized() * 1000 )
        StopNextFallDamage = true
        self.Ent = ent
        ent.Owner = self.Owner
        self.Weapon:SendWeaponAnim( ACT_VM_DRAW )
    end
end

function SWEP:SecondaryAttack()
end

function SWEP:Reload()
    if not self.Owner.HealShield or not IsValid(self.Owner.HealShield) then return end
    self.Owner.HealShield:Explode()
end