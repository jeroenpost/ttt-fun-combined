SWEP.CanDecapitate= true
if SERVER then
end

SWEP.HoldType           = "ar2"

SWEP.PrintName          = "Turtle's Lightning"

SWEP.Slot               = 0


if CLIENT then

    SWEP.Icon = "VGUI/ttt/icon_scout"
end


SWEP.Base               = "gb_camo_base"
SWEP.Spawnable = true
SWEP.AdminSpawnable = true

SWEP.Kind = 885
SWEP.WeaponID = AMMO_RIFLE

SWEP.InLoadoutFor = nil

SWEP.LimitedStock = true

SWEP.Primary.Delay          = 0.57
SWEP.Primary.Recoil         = 0.3
SWEP.Primary.Automatic = true
SWEP.Primary.Ammo = "357"
SWEP.Primary.Damage = 70
SWEP.Primary.Cone = 0
SWEP.Primary.ClipSize = 80
SWEP.Primary.ClipMax = 80 -- keep mirrored to ammo
SWEP.Primary.DefaultClip = 160

SWEP.HeadshotMultiplier = 3

SWEP.AutoSpawnable      = false
SWEP.AmmoEnt = "item_ammo_357_ttt"

SWEP.UseHands			= true
SWEP.ViewModelFlip		= true
SWEP.ViewModelFOV		= 54
SWEP.ViewModel          = Model("models/weapons/v_snip_god.mdl")
SWEP.WorldModel         = Model("models/weapons/w_snip_god.mdl")

SWEP.Primary.Sound = Sound("weapons/usp/usp1.wav")

SWEP.Secondary.Sound = Sound("Default.Zoom")

SWEP.IronSightsPos 			= Vector(10.2, -10.8, 2)
SWEP.IronSightsAng 			= Vector(2.8, 0, 0)

SWEP.Camo = 43

function SWEP:OnDrop()
    self:Remove()
end

function SWEP:SetZoom(state)
    if CLIENT then
        return
    elseif IsValid(self.Owner) and self.Owner:IsPlayer() then
        if state then
            self.Owner:SetFOV(15, 0.3)
        else
            self.Owner:SetFOV(0, 0.2)
        end
    end
end

function SWEP:PrimaryAttack()
    if self.Owner:KeyDown( IN_USE ) then

        self:Superman()

        return
    end
    self:NormalPrimaryAttack()
end



-- Add some zoom to ironsights for this gun
function SWEP:SecondaryAttack()

    if self.Owner:KeyDown(IN_USE) then
        self:FireBolt(3, 75, 0)
        self.Weapon:SetNextSecondaryFire( CurTime() + 0.5)
        return
    end


    if not self.IronSightsPos then return end
    if self.Weapon:GetNextSecondaryFire() > CurTime() then return end

    local bIronsights = not self:GetIronsights()

    self:SetIronsights( bIronsights )

    if SERVER then
        self:SetZoom(bIronsights)
    else
        self:EmitSound(self.Secondary.Sound)
    end

    self.Weapon:SetNextSecondaryFire( CurTime() + self.Primary.Delay )



    self.Weapon:SetNextSecondaryFire( CurTime() + 0.5)
end

function SWEP:PreDrop()
    self:SetZoom(false)
    self:SetIronsights(false)
    return self.BaseClass.PreDrop(self)
end

function SWEP:Reload()
    if self.Owner:KeyDown( IN_ATTACK2) then
        self:MakeExplosion()
        return
    end
    if self.Owner:KeyDown(IN_USE) then
        self:DropHealth("normal_health_station","models/props/de_tides/vending_turtle.mdl")
        return
    end

    self.Weapon:SetNextSecondaryFire( CurTime() + 3)
    self.Weapon:SetNextPrimaryFire( CurTime() + 3)
    self.Weapon:DefaultReload( ACT_VM_RELOAD );
    self:SetIronsights( false )
    self:SetZoom(false)
end

function SWEP:DropHealth(ent, model)
    if self.NextHealthDrop > CurTime() then return end
    self.NextHealthDrop = CurTime() + 5
    local healttt = 200
    if self.healththing and SERVER then
        if Entity(self.healththing).GetStoredHealth then
            healttt = Entity(self.healththing):GetStoredHealth()
        else
            healttt = 200
        end
        Entity(self.healththing):Remove()
    end
    self:HealthDrop(ent,healttt,model)
end

SWEP.NextHealthDrop = 0

local throwsound = Sound( "Weapon_SLAM.SatchelThrow" )

function SWEP:HealthDrop(ent, healttt, model)
    if SERVER then
        local ply = self.Owner
        if not IsValid(ply) then return end

        local vsrc = ply:GetShootPos()
        local vang = ply:GetAimVector()
        local vvel = ply:GetVelocity()

        local vthrow = vvel + vang * 200

        local health = ents.Create(ent)
        if model then
            health.model = model
            health:SetModel(model)
            health:SetMaterial("camos/camo43")
        end
        if IsValid(health) then
            health:SetPos(vsrc + vang * 10)
            health:Spawn()
            --health:SetModel( "models/props_lab/cactus.mdl")
            health:SetModelScale(health:GetModelScale()*1,0)
            health:SetPlacer(ply)
            --health.healsound = "ginger_cartman.mp3"

            health:PhysWake()
            local phys = health:GetPhysicsObject()
            if IsValid(phys) then
                phys:SetVelocity(vthrow)
            end
            if model then
                health:SetModel(model)
                health:SetMaterial("camos/camo43")
                timer.Simple(0.5,function()
                    if IsValid(health) then
                        health:SetModel(model)
                        health:SetMaterial("camos/camo43")
                    end
                end)
            end
            --  health:SetModel( "models/props_lab/cactus.mdl")
            health:SetStoredHealth(200)
            health.setOwner = self.Owner

            self.healththing = health:EntIndex()

            self.Planted = true
        end
    end

    self.Weapon:EmitSound(throwsound)
end

-- FLY
SWEP.flyammo = 5
SWEP.nextFly = 0
function SWEP:Fly( soundfile )
    if self.nextFly > CurTime() or CLIENT then return end
    self.nextFly = CurTime() + 0.30
    if self.flyammo < 1 then return end

    if not soundfile then
        if SERVER then self.Owner:EmitSound(Sound("weapons/ls/lightsaber_swing.mp3")) end
        if SERVER then self.Owner:EmitSound(Sound("player/suit_sprint.wav")) end
    else
        if SERVER then self.Owner:EmitSound(Sound(soundfile)) end
    end
    self.flyammo = self.flyammo - 1
    self.Owner:SetAnimation(PLAYER_ATTACK1)
    local power = 400
    if math.random(1,5) < 2 then power = 100
    elseif math.random(1,15) < 2 then power = -500
    elseif math.random(1,15) < 2 then power = 2500
    else power = math.random( 350,450 ) end

    if SERVER then self.Owner:SetVelocity(self.Owner:GetForward() * power + Vector(0, 0, power)) end
    timer.Simple(20, function() if IsValid(self) then self.flyammo = self.flyammo + 1 end end)
    --timer.Simple(0.2,self.SecondaryAttackDelay,self)
end
SWEP.NextBoom = 0

function SWEP:MakeExplosion(worldsnd)

    if self.NextBoom > CurTime() then return end
    self.NextBoom = (CurTime() + 1.5)
    if SERVER then
        local tr = self.Owner:GetEyeTrace()
        local ent = ents.Create( "env_explosion" )

        ent:SetPos( tr.HitPos  )
        ent:SetOwner( self.Owner  )
        ent:SetPhysicsAttacker(  self.Owner )
        ent:Spawn()
        ent:SetKeyValue( "iMagnitude", "40" )
        ent:Fire( "Explode", 0, 0 )

        util.BlastDamage( self, self:GetOwner(), self:GetPos(), 50, 75 )

        ent:EmitSound( "weapons/big_explosion.mp3" )
    end

    --  self:TakePrimaryAmmo( 1 )
    self.Owner:SetAnimation( PLAYER_ATTACK1 )
    local owner = self.Owner
    if not IsValid(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

    owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )
end

function SWEP:Holster()
    self:SetIronsights(false)
    self:SetZoom(false)
    return true
end

if CLIENT then
    local scope = surface.GetTextureID("sprites/scope")
    function SWEP:DrawHUD()
        if self:GetIronsights() then
            surface.SetDrawColor( 0, 0, 0, 255 )

            local x = ScrW() / 2.0
            local y = ScrH() / 2.0
            local scope_size = ScrH()

            -- crosshair
            local gap = 80
            local length = scope_size
            surface.DrawLine( x - length, y, x - gap, y )
            surface.DrawLine( x + length, y, x + gap, y )
            surface.DrawLine( x, y - length, x, y - gap )
            surface.DrawLine( x, y + length, x, y + gap )

            gap = 0
            length = 50
            surface.DrawLine( x - length, y, x - gap, y )
            surface.DrawLine( x + length, y, x + gap, y )
            surface.DrawLine( x, y - length, x, y - gap )
            surface.DrawLine( x, y + length, x, y + gap )


            -- cover edges
            local sh = scope_size / 2
            local w = (x - sh) + 2
            surface.DrawRect(0, 0, w, scope_size)
            surface.DrawRect(x + sh - 2, 0, w, scope_size)

            surface.SetDrawColor(255, 0, 0, 255)
            surface.DrawLine(x, y, x + 1, y + 1)

            -- scope
            surface.SetTexture(scope)
            surface.SetDrawColor(255, 255, 255, 255)

            surface.DrawTexturedRectRotated(x, y, scope_size, scope_size, 0)

        else
            return self.BaseClass.DrawHUD(self)
        end
    end

    function SWEP:AdjustMouseSensitivity()
        return (self:GetIronsights() and 0.2) or nil
    end
end
