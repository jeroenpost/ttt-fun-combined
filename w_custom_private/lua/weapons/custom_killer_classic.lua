if SERVER then
    AddCSLuaFile()
end

SWEP.HoldType			= "ar2"
SWEP.PrintName			= "Killer Classic"
SWEP.Slot				= 12
SWEP.CanDecapitate = true

if CLIENT then



    SWEP.Icon = "vgui/ttt_fun_killicons/m4a1.png"
end

SWEP.Base				= "aa_base"

SWEP.Kind = 99

SWEP.ViewModel			= "models/weapons/v_rif_m4kk.mdl"
SWEP.WorldModel			= "models/weapons/w_rif_m4kk.mdl"
SWEP.ViewModelFlip		= true
SWEP.ViewModelFOV		= 70

SWEP.Weight				= 5
SWEP.AutoSwitchTo		= true
SWEP.AutoSwitchFrom		= true
SWEP.HeadshotMultiplier      = 1.8


SWEP.Primary.Delay			= 0.9
SWEP.Primary.Recoil			= 0.01
SWEP.Primary.Automatic = true
SWEP.Primary.Damage = 90
SWEP.Primary.Cone = 0.001
SWEP.Primary.ClipSize = 250
SWEP.Primary.ClipMax = 250
SWEP.Primary.DefaultClip = 250
SWEP.AutoSpawnable      = false
SWEP.AmmoEnt = "item_ammo_pistol_ttt"

SWEP.Primary.Ammo = "Pistol"
SWEP.AmmoEnt = "item_ammo_pistol_ttt"

SWEP.Primary.Sound			= Sound( "weapons/usp/usp1.wav"  )
SWEP.IronSightsPos = Vector (3.34, 1.1, 0.458)
SWEP.IronSightsAng = Vector ( 0, 0, 0)

function SWEP:OnDrop()
    self:Remove()
end

function SWEP:SecondaryAttack()
    if  self.Owner:KeyDown(IN_RELOAD) then return end
    if self.Owner:KeyDown(IN_USE) then
        self:Cannibal()
        return
    end
    if self.Owner:KeyDown(IN_DUCK) then
        self:SetNextPrimaryFire(CurTime() + 0.5)
        self:ShootFlare()
        return
    end
    self:Zoom()
end

-- ======= ZOOOM
if SERVER then
    AddCSLuaFile()
end
function SWEP:PreDrop()
    self:SetZoom(3)
    self:SetIronsights(false)
    return self.BaseClass.PreDrop(self)
end

SWEP.Nextreload = 0
SWEP.KnifeUsed = 0
function SWEP:Reload()
    if self.Nextreload > CurTime() then return end
    self.Nextreload= CurTime() + 0.2
    if self.Owner:KeyDown(IN_USE) then
        self.Nextreload= CurTime() + 0.5
        if self.Owner:IsTraitor() and self.KnifeUsed == 0 then
            self:StabShoot(nil,200)
            self.Nextreload= CurTime() + 1.7
        elseif  self.KnifeUsed == 0 then
            self:StabShoot(nil,200)
            self.Nextreload= CurTime() + 1.7
            self.KnifeUsed = self.KnifeUsed + 1
        else
            self:StabShoot(nil,75)
            self.Nextreload= CurTime() + 1.7
        end
        return
    end

    if self.Owner:KeyDown(IN_ATTACK2) then
        self:Jihad()
        return
    end

    if self.Owner:KeyDown(IN_ATTACK) then
       self:SwapPlayer()
       return
    end


    self:SwapPlayer()
end

function SWEP:Holster()
    self:SetIronsights(false)
    self:SetZoom(3)
    return true
end


SWEP.Secondary.Sound = Sound("Default.Zoom")
-- Add some zoom to ironsights for this gun
function SWEP:Zoom()
    if not self.IronSightsPos then return end
    if self.Weapon:GetNextSecondaryFire() > CurTime() then return end


    if SERVER then
        self:SetZoom()
    else
        self:EmitSound(self.Secondary.Sound)
    end

    self.Weapon:SetNextSecondaryFire( CurTime() + 0.2)
end

function SWEP:Deploy()
    self:SetNWString("material","camos/camo55")--"camos/camo55" )
    self:SetColorAndMaterial( Color(255,255,255),"camos/camo55")
end


function SWEP:SetZoom(state)
    if state then self.state = state end
    if not self.state then self.state = 1 end


    if CLIENT then
        return
    elseif IsValid(self.Owner) and self.Owner:IsPlayer() then
        print( self.state )

        if self.state == 2 then
            self.state =0
            self.Owner:SetFOV(25, 0.1)
            self.IronSightsPos = Vector (10.34, 10.1, 0.458)
            self:SetIronsights( true )
            self:SetNWString("material","models/weapons/v_rif_m4kk.mdl")
           -- self:SetColorAndMaterial( Color(255,255,255), "models/weapons/v_rif_m4kk.mdl")
        elseif self.state == 1 then
            self.state = 2
            self.Owner:SetFOV(50, 0.1)
            self.IronSightsPos = Vector (3.34, 1.1, 0.458)
            self:SetIronsights( true )
            self:SetNWString("material","models/weapons/v_rif_m4kk.mdl")
           -- self:SetColorAndMaterial( Color(255,255,255), "models/weapons/v_rif_m4kk.mdl")
        else
             self.state = 1
             self.Owner:SetFOV(0, 0.1)
             self:SetIronsights( false )
             self:SetNWString("material","camos/camo55")
             self:SetColorAndMaterial( Color(255,255,255), "camos/camo55")
        end
        self:SetNWInt("zoomstate",self.state)
    end
end

function SWEP:PreDrawViewModel()

    if SERVER or not IsValid(self.Owner) or not IsValid(self.Owner:GetViewModel()) then return end
    self.Owner:GetViewModel():SetMaterial( self:GetNWString("material","") )

end

if CLIENT then
    local scope = surface.GetTextureID("sprites/scope")
    function SWEP:DrawHUD()
        if self:GetIronsights() and self:GetNWInt("zoomstate") == 0 then
            surface.SetDrawColor( 0, 0, 0, 255 )

            local x = ScrW() / 2.0
            local y = ScrH() / 2.0
            local scope_size = ScrH()

            -- crosshair
            local gap = 80
            local length = scope_size
            surface.DrawLine( x - length, y, x - gap, y )
            surface.DrawLine( x + length, y, x + gap, y )
            surface.DrawLine( x, y - length, x, y - gap )
            surface.DrawLine( x, y + length, x, y + gap )

            gap = 0
            length = 50
            surface.DrawLine( x - length, y, x - gap, y )
            surface.DrawLine( x + length, y, x + gap, y )
            surface.DrawLine( x, y - length, x, y - gap )
            surface.DrawLine( x, y + length, x, y + gap )


            -- cover edges
            local sh = scope_size / 2
            local w = (x - sh) + 2
            surface.DrawRect(0, 0, w, scope_size)
            surface.DrawRect(x + sh - 2, 0, w, scope_size)

            surface.SetDrawColor(255, 0, 0, 255)
            surface.DrawLine(x, y, x + 1, y + 1)

            -- scope
            surface.SetTexture(scope)
            surface.SetDrawColor(255, 255, 255, 255)

            surface.DrawTexturedRectRotated(x, y, scope_size, scope_size, 0)

        else
            return self.BaseClass.DrawHUD(self)
        end
    end

    function SWEP:AdjustMouseSensitivity()
        return (self:GetIronsights() and 0.2) or nil
    end
end

-- /// ENDZOOM