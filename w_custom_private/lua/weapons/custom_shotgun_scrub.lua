include "weapons/weapon_zm_shotgun.lua"
SWEP.Base = "gb_camo_base"
SWEP.ViewModelFOV	= 54
SWEP.HoldType = "smg"
SWEP.PrintName = "Shotgun Scrub"
SWEP.ViewModel		= "models/weapons/cstrike/c_shot_m3super90.mdl"
SWEP.WorldModel		= "models/weapons/w_shot_m3super90.mdl"
SWEP.AutoSpawnable = false

SWEP.HoldType = "shotgun"
SWEP.Slot = 2
SWEP.Kind = WEAPON_HEAVY
SWEP.Camo = 7

if CLIENT then

    SWEP.Author = "GreenBlack"

    SWEP.Icon = "vgui/ttt_fun_killicons/xm1014.png"
end

SWEP.CanDecapitate= true
SWEP.HasFireBullet = true
SWEP.Primary.Sound			= Sound( "Weapon_M3.Single" )
SWEP.Primary.Recoil			= 7

SWEP.Primary.Ammo = "Buckshot"
SWEP.Primary.Damage = 10
SWEP.Primary.Cone = 0.085
SWEP.Primary.Delay = 0.8
SWEP.Primary.ClipSize = 36
SWEP.Primary.ClipMax = 64
SWEP.Primary.DefaultClip = 36
SWEP.Primary.Automatic = true
SWEP.Primary.NumShots = 10
SWEP.AutoSpawnable      = false
SWEP.AmmoEnt = "item_box_buckshot_ttt"

function SWEP:Deploy()
    self.dt.reloading = false
    self.reloadtimer = 0
    return self:DeployFunction()
end

SWEP.IronSightsPos = Vector(-7.65, -12.214, 3.2)
SWEP.IronSightsAng = Vector(0, 0, 0)

function SWEP:OnDrop()
    self:Remove()
end

function SWEP:PrimaryAttack()
    if self.Owner:KeyDown(IN_USE) then
        self:ShootFlare()
        return
    end
    if self:Clip1() > 0 then
        self:PowerHit(true,600,99999)
    end
    self.BaseClass.PrimaryAttack(self)
end

function SWEP:Reload()
    if self.Owner:KeyDown(IN_USE) then
        self:DropHealth("normal_health_station")
        return
    end

    self.BaseClass.Reload(self)
end

function SWEP:SecondaryAttack()
    if self.Owner:KeyDown(IN_USE) then
        self:Freeze(25)
        self.Weapon:SendWeaponAnim( ACT_VM_HITCENTER )
        self.Owner:SetAnimation(ACT_RELOAD_PISTOL)
        self.Weapon:SendWeaponAnim(ACT_SHOTGUN_RELOAD_FINISH)
        self.Owner:ViewPunch( Angle( -10, 0, 0 ) )
        return
    end
    self:Fly()
end

