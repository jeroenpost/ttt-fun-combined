if SERVER then
   AddCSLuaFile(  )

--resource.AddFile("materials/models/weapons/v_models/darkness001/greenblack1.vmt")
--resource.AddFile("materials/models/weapons/v_models/darkness001/greenblack2.vmt")
--resource.AddFile("materials/models/weapons/v_models/darkness001/greenblack.vmt")

--resource.AddFile("models/weapons/v_darkness001.mdl")
--resource.AddFile("models/weapons/w_darkness001.mdl")

end
   
SWEP.HoldType			= "pistol"
SWEP.PrintName			= "Darkness"
SWEP.Slot				= 2

if CLIENT then
   			
   SWEP.Author				= "TTT"

   
   SWEP.SlotPos			= 2

   SWEP.Icon = "vgui/ttt_fun_killicons/deserteagle.png"
end

SWEP.Base				= "weapon_tttbase"

SWEP.Spawnable = true
SWEP.AdminSpawnable = true

SWEP.Kind = WEAPON_PISTOL
SWEP.WeaponID = AMMO_DEAGLE

SWEP.Primary.Ammo       = "AlyxGun" -- hijack an ammo type we don't use otherwise
SWEP.Primary.Recoil			= 0.05
SWEP.Primary.Damage = 15
SWEP.Primary.Delay = 0.50
SWEP.Primary.Cone = 0.008
SWEP.Primary.ClipSize = 10
SWEP.Primary.ClipMax = 10
SWEP.Primary.DefaultClip = 10
SWEP.Primary.Automatic = true

SWEP.HeadshotMultiplier = 1.5


SWEP.AutoSpawnable      = false
SWEP.AmmoEnt = "item_ammo_revolver_ttt"
SWEP.Primary.Sound = Sound( "Weapon_Deagle.Single" )

SWEP.UseHands			= true
SWEP.ViewModelFlip		= true
SWEP.ViewModelFOV		= 75
SWEP.ViewModel			= "models/weapons/v_darkness001.mdl"
SWEP.WorldModel			= "models/weapons/w_darkness001.mdl"

SWEP.IronSightsPos = Vector(5.14, -2, 2.5)
SWEP.IronSightsAng = Vector(0, 0, 0)
--SWEP.IronSightsPos 	= Vector( 3.8, -1, 3.6 )
--SWEP.ViewModelFOV = 49

function SWEP:PrimaryAttack(worldsnd) 

   self.Weapon:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
   self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )

   if not self:CanPrimaryAttack() then return end

   if not worldsnd then
      self.Weapon:EmitSound( self.Primary.Sound, self.Primary.SoundLevel )
   elseif SERVER then
      sound.Play(self.Primary.Sound, self:GetPos(), self.Primary.SoundLevel)
   end

   self:ShootBullet( self.Primary.Damage, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )
   -- local tr = self.Owner:GetEyeTrace()
   -- local effectdata = EffectData()
   -- effectdata:SetOrigin( tr.HitPos )
   -- effectdata:SetNormal( tr.HitNormal )
  --  effectdata:SetMagnitude( 1 )
  --  effectdata:SetScale( 1 )
  --  effectdata:SetRadius( 1 )
  --  util.Effect( "Sparks", effectdata )
    
   self:TakePrimaryAmmo( 1 )

   local owner = self.Owner 
   if not IsValid(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

   owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )
end



function SWEP:ShootBullet( dmg, recoil, numbul, cone )

   local sights = self:GetIronsights()

   numbul = numbul or 1
   cone   = cone   or 0.01

   -- 10% accuracy bonus when sighting
   cone = sights and (cone * 0.9) or cone

   local bullet = {}
   bullet.Num    = numbul
   bullet.Src    = self.Owner:GetShootPos()
   bullet.Dir    = self.Owner:GetAimVector()
   bullet.Spread = Vector( cone, cone, 0 )
   bullet.Tracer = 4
   bullet.Force  = 5
   bullet.Damage = dmg

   bullet.Callback = function(att, tr, dmginfo)
		if SERVER or (CLIENT and IsFirstTimePredicted()) then

			if (not tr.HitWorld)  then
				 
				    pl2 = tr.Entity
				    pl2.revercedControls = true
				  
				  timer.Simple(10,function()
					if IsValid( pl2 ) then
						pl2.revercedControls = false
					end
				  
				  end)
			end	
		end
    end
	   self.Owner:FireBullets( bullet )
	   self.Weapon:SendWeaponAnim(self.PrimaryAnim)

	   -- Owner can die after firebullets, giving an error at muzzleflash
	   if not IsValid(self.Owner) or not self.Owner:Alive() then return end

	   self.Owner:MuzzleFlash()
	   self.Owner:SetAnimation( PLAYER_ATTACK1 )

	   if self.Owner:IsNPC() then return end

	   if ((game.SinglePlayer() and SERVER) or
		   ((not game.SinglePlayer()) and CLIENT and IsFirstTimePredicted() )) then

		  -- reduce recoil if ironsighting
		  recoil = sights and (recoil * 0.5) or recoil

	   end
	
end


