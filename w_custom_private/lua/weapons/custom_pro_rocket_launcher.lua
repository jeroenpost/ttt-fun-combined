SWEP.Kind = WEAPON_CARRY
SWEP.Base = "aa_base"

SWEP.Author = "Spastik"
SWEP.Contact = ""
SWEP.Purpose = "FOR WHEN YOU NEED TO CLEAR THE WAY THROUGH TONS OF CANNON FODDER REALLY FAST."
SWEP.Instructions = "PULL THE TRIGGER TO TAKE OUT HORDES OF CANNON FODDER. EVERYTHING IS CANNON FODDER WHEN YOU'RE USING THIS RIFLE."

SWEP.Primary.Round 			= ("ent_atr_shell")	--NAME OF ENTITY GOES HERE
SWEP.Spawnable = false
SWEP.AdminSpawnable = true
SWEP.AutoSwitchTo = false
SWEP.Slot = 0
SWEP.PrintName = "Professional Rocket Launcher"
SWEP.Author = "Spastik"
SWEP.Spawnable = true
SWEP.AutoSwitchFrom = false
SWEP.FiresUnderwater = false
SWEP.Weight = 5
SWEP.DrawCrosshair = true
SWEP.Category = "S&G Munitions"
SWEP.SlotPos = 3
SWEP.DrawAmmo = true
SWEP.ReloadSound = "weapons/smg1/smg1_reload.wav"
SWEP.Base = "aa_base"
SWEP.HoldType = "crossbow"
SWEP.ViewModelFOV = 70.787401574803
SWEP.ViewModelFlip = false
SWEP.ViewModel = "models/weapons/c_rpg.mdl"
SWEP.WorldModel = "models/weapons/w_rocket_launcher.mdl"
SWEP.ShowViewModel = true
SWEP.ShowWorldModel = true
SWEP.UseHands = true

SWEP.Primary.ClipSize = 4
SWEP.Primary.DefaultClip = 8
SWEP.Primary.Automatic = false
SWEP.Primary.Ammo = "smg1_grenade"

SWEP.Secondary.ClipSize = -1
SWEP.Secondary.DefaultClip = -1
SWEP.Secondary.Automatic = false
SWEP.Secondary.Ammo = "none"
SWEP.Primary.Delay = 0.8

SWEP.Zoomed = false

function SWEP:PreDrop(yes)
    if yes then self:Remove() end
end
function SWEP:Deploy()
end

 function SWEP:Initialize()
	self:SetHoldType( self.HoldType or "pistol" )
end

function SWEP:PrimaryAttack()
	if self:Clip1() <= 0 then return end
	self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
	self:FireRocket()
    self:TakePrimaryAmmo(1)
	self:SendWeaponAnim(ACT_VM_PRIMARYATTACK)
	self.Owner:SetAnimation( PLAYER_ATTACK1 )
	self:EmitSound( "weapons/mortar/mortar_explode2.wav", 500, 80, 1, 0 )
	self:EmitSound( "weapons/mortar/mortar", 500, 80, 1, -1 )
	self.Weapon:SetNWFloat( "LastShootTime", CurTime() )
end

function SWEP:FireRocket()
	local aim = self.Owner:GetAimVector()
	local side = aim:Cross(Vector(0,0,1))
	local up = side:Cross(aim)
	local pos = self.Owner:GetShootPos() + side * 10 + up * -3

	if SERVER then
	local rocket = ents.Create(self.Primary.Round)
	if !rocket:IsValid() then return false end
	rocket:SetAngles(self.Owner:GetAimVector():Angle(90,90,0))
	rocket:SetPos(pos)
	rocket:SetOwner(self.Owner)
	rocket:Spawn()
	rocket.Owner = self.Owner
	rocket:Activate()
	eyes = self.Owner:EyeAngles()
		local phys = rocket:GetPhysicsObject()
			phys:SetVelocity(self.Owner:GetAimVector() * 7000)
	end
		if SERVER and !self.Owner:IsNPC() then
		local anglo = Angle(-3, 0, 0)
		self.Owner:ViewPunch(anglo)
		end

end

function SWEP:CheckWeaponsAndAmmo()
	if SERVER and self.Weapon != nil then
		timer.Simple(.01, function() if not IsValid(self) then return end
			if not IsValid(self.Owner) then return end
			self.Owner:StripWeapon(self.Gun)
		end)
	end
end




    SWEP.rflyammo = 15
    SWEP.rnextFly = 0
function SWEP:SecondaryAttack()
        if self.rnextFly > CurTime() or CLIENT then return false end
        self.rnextFly = CurTime() + 0.30
        if self.rflyammo < 1 then return end

        self.rflyammo = self.rflyammo - 1
        self.Owner:SetAnimation(PLAYER_ATTACK1)

        if SERVER then self.Owner:EmitSound(Sound(self.Primary.Sound )) end
        --if SERVER then self.Owner:EmitSound(Sound("player/suit_sprint.wav")) end

        local ent = ents.Create( "env_explosion" )
        if( IsValid( self.Owner ) ) then
            ent:SetPos( self.Owner:GetShootPos() )
            ent:SetOwner( self.Owner )
            ent:SetKeyValue( "iMagnitude", "0" )
            ent:Spawn()

            ent:Fire( "Explode", 0, 0 )
            ent:EmitSound( "physics/body/body_medium_break2.wav", 350, 100 )
        end


        if SERVER then self.Owner:SetVelocity(self.Owner:GetForward() * -1400 ) end
        timer.Simple(20, function() if IsValid(self) then self.rflyammo = self.rflyammo + 1 end end)
        return true
        --timer.Simple(0.2,self.SecondaryAttackDelay,self)
    end


SWEP.reloadtimer = 0

function SWEP:SetupDataTables()
    self:DTVar("Bool", 0, "reloading")

    return self.BaseClass.SetupDataTables(self)
end

function SWEP:Reload()
    self:SetIronsights( false )

    if self.Weapon:GetNWBool( "reloading", false ) then return end
    --if self.dt.reloading then return end


    if not IsFirstTimePredicted() then return end

    if self.Weapon:Clip1() < self.Primary.ClipSize and self.Owner:GetAmmoCount( self.Primary.Ammo ) > 0 then


        if self:StartReload() then
            return
        end
    end

end

function SWEP:StartReload()
    if self.Weapon:GetNWBool( "reloading", false ) then
    --if self.dt.reloading then
       return false
   end

    if not IsFirstTimePredicted() then return false end

    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )

    local ply = self.Owner

    if not ply or ply:GetAmmoCount(self.Primary.Ammo) <= 0 then
     --   return false
    end

    local wep = self.Weapon

    if wep:Clip1() >= self.Primary.ClipSize then
        return false
    end



    wep:SendWeaponAnim(ACT_RELOAD)

    self.reloadtimer =  CurTime() + wep:SequenceDuration()

    wep:SetNWBool("reloading", true)
    --self.dt.reloading = true
    self:PerformReload()
    return true
end

function SWEP:PerformReload()
    local ply = self.Owner

    -- prevent normal shooting in between reloads
    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )

    if not ply or ply:GetAmmoCount(self.Primary.Ammo) <= 0 then return end

    local wep = self.Weapon

    if wep:Clip1() >= self.Primary.ClipSize then return end

    self.Owner:RemoveAmmo( 1, self.Primary.Ammo, false )
    self.Weapon:SetClip1( self.Weapon:Clip1() + 1 )

    wep:SendWeaponAnim(ACT_VM_RELOAD)

    self.reloadtimer = CurTime() + wep:SequenceDuration()
    timer.Simple(wep:SequenceDuration(),function()
        if IsValid(self) then
            self:FinishReload()
        end
    end)
end

function SWEP:FinishReload()
   -- self.dt.reloading = false
    self.Weapon:SetNWBool("reloading", false)
    self.Weapon:SendWeaponAnim(ACT_SHOTGUN_RELOAD_FINISH)

    self.reloadtimer = CurTime() + self.Weapon:SequenceDuration()
end