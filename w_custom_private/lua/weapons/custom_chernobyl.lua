SWEP.PrintName			= "Chernobyl"

SWEP.Base				= "aa_base"
SWEP.Spawnable			= true
SWEP.AdminSpawnable		= false
SWEP.Icon 				= "vgui/ttt_fun_killicons/emsss.png"
SWEP.HoldType = "slam"
SWEP.ViewModelFOV = 71
SWEP.ViewModelFlip = true
SWEP.ViewModel  = Model("models/weapons/v_c4.mdl")
SWEP.WorldModel = Model("models/weapons/w_c4.mdl")
SWEP.ShowViewModel = true
SWEP.ShowWorldModel = true
SWEP.ViewModelBoneMods = {}
SWEP.Camo = 44

function SWEP:Deploy()
   -- self.Owner:EmitSound( "weapons/barret_arm_draw.mp3" ) ;
	self.Weapon:SendWeaponAnim( ACT_VM_DRAW )
	self:SetHoldType( self.HoldType or "pistol" )
    self:SetDeploySpeed( 0.1)
    self:SetColorAndMaterial(Color(255,255,255,255),"camos/camo44");
	return true;
end

function SWEP:PreDrawViewModel()

    if SERVER or not IsValid(self.Owner) or not IsValid(self.Owner:GetViewModel()) then return end
    self.Owner:GetViewModel():SetColor(Color(255,255,255,255))
    self.Owner:GetViewModel():SetMaterial("camos/camo44")

end


SWEP.Weight				= 5
SWEP.AutoSwitchTo		= false
SWEP.AutoSwitchFrom		= false
SWEP.AutoSpawnable 		= false
SWEP.Kind				= WEAPON_EQUIP2
SWEP.Primary.Sound			= Sound( "weapons/m3/s3-1.mp3" )
SWEP.Primary.Recoil			= 1
SWEP.Primary.Damage			= 0.05
SWEP.Primary.NumShots		= 16
SWEP.Primary.Cone			= 0.035
SWEP.Primary.ClipSize		= 90
SWEP.Primary.Delay			= 0.9
SWEP.Primary.DefaultClip	= 90
SWEP.Primary.Automatic		= true
SWEP.Primary.Ammo			= "buckshot"
SWEP.AmmoEnt = "item_box_buckshot_ttt"

SWEP.Slot				= 6
SWEP.SlotPos			= 1

SWEP.Secondary.ClipSize		= -1
SWEP.Secondary.DefaultClip	= -1
SWEP.Secondary.Automatic	= false
SWEP.Secondary.Ammo			= "none"

SWEP.IronSightsPos = Vector(3.046, -4.427, 0.834)
SWEP.IronSightsAng = Vector(0, 0, 0)

SWEP.RunArmOffset  = Vector(-0.601, -3.935, -1.461)
SWEP.RunArmAngle 	 = Vector(8.032, -41.885, 0)

function SWEP:IsEquipment() 
	return false
end

SWEP.UsedReloads3 = 0
function SWEP:PrimaryAttack()
    if self.Owner:KeyDown(IN_RELOAD) then return end

    self:SetNextSecondaryFire( CurTime() + 3)
    self:SetNextPrimaryFire( CurTime() + 3)

    if self.Owner:KeyDown(IN_USE) then
        if self.UsedReloads3 < 7 and self.NextReload2 < CurTime() then
            self.UsedReloads3 = self.UsedReloads3 + 1
            self.NextReload3= CurTime() + 2
            self:throw_attack(4)
            if self.UsedReloads3 == 3 and SERVER then
                --self:Remove()
            end
        end
        return
    end


end
SWEP.UsedReloads2 = 0
SWEP.NextReload2 = 0
function SWEP:SecondaryAttack()
    if self.Owner:KeyDown(IN_RELOAD) then return end

    if self.Owner:KeyDown(IN_USE) then
        if self.UsedReloads2 < 3 and self.NextReload2 < CurTime() then
            self.UsedReloads2 = self.UsedReloads2 + 1
            self.NextReload2 = CurTime() + 2
            self:throw_attack(3)
            if self.UsedReloads2 == 3 and SERVER then
                --self:Remove()
            end
        end
    end
    if SERVER and (not self.NextSec or self.NextSec < CurTime()) then
        self.NextSec = CurTime() + 3
        self.Owner:EmitSound("npc/metropolice/vo/nowgetoutofhere.wav")
        end
end


SWEP.NextReload = 0
SWEP.UsedReloads = 0
SWEP.UsedReloads6 = 0
SWEP.NextReload6 = 0
function SWEP:Reload()
    if self.Owner:KeyDown( IN_ATTACK )   then
        if self.UsedReloads < 4 and self.NextReload < CurTime() then
            self.UsedReloads = self.UsedReloads + 1
            self.NextReload = CurTime() + 2
            self:throw_attack(2)
            if self.UsedReloads == 4 and SERVER then
                --self:Remove()
            end
        end
        return
    end

    if self.Owner:KeyDown(IN_ATTACK2) then
        if self.UsedReloads6 < 3 and self.NextReload6 < CurTime() then
            self.UsedReloads6 = self.UsedReloads6 + 1
            self.NextReload6 = CurTime() + 2
            self:throw_attack(6)
            if self.UsedReloads6 == 3 and SERVER then
                --self:Remove()
            end
        end
    end

    if self.Owner:KeyDown(IN_USE) then
        if SERVER then
            self.Owner:EmitSound("npc/attack_helicopter/aheli_damaged_alarm1.wav")
        end
        if self.NextReload > CurTime() then return end
        self.NextReload = CurTime() + 2
        timer.Simple(1.5,function()
            if IsValid(self.Owner) then
                local guys = ents.FindInSphere( self.Owner:GetPos()+Vector(0,0,40), 230 )
                local owner = self.Owner
                for k, guy in pairs(guys) do
                    if isfunction(guy.GetBoneCount) then
                        if SERVER then
                            guy:EmitSound("HL1/fvox/radiation_detected.wav")
                        end
                        timer.Create(guy:EntIndex().."custom_chernobyl",2,12,function()
                            if not IsValid(guy) or not isfunction(guy.GetBoneCount) then return end
                            guy:ManipulateBoneScale(math.random(guy:GetBoneCount()), VectorRand())
                            if SERVER and guy != owner and (not guy.nextcharnobil or guy.nextcharnobil < CurTime()) then
                            guy:EmitSound("physics/plastic/plastic_barrel_impact_soft5.wav")
                            guy.nextcharnobil = CurTime() + 5
                            local dmginfo = DamageInfo()
                            dmginfo:SetDamage( 10 ) --50 damage
                            dmginfo:SetDamageType( DMG_POISON ) --Bullet damage
                            if IsValid(owner) then
                                dmginfo:SetAttacker(owner ) --First player found gets credit
                            else
                                dmginfo:SetAttacker( guy )
                            end
                            dmginfo:SetDamageForce( Vector( 0, 0, 200 ) )
                            guy:TakeDamageInfo( dmginfo )
                            end
                        end)
                    end
                end

                self:SphereExplosion(0)
                if SERVER then
                    self.Owner:Kill( )

                    self:Remove()
                end
            end
        end)
        return
    end


    self:NormalReload()


end


function SWEP:throw_attack (num)
    local tr = self.Owner:GetEyeTrace();


    self:EmitSound(self.Primary.Sound)
    self.BaseClass.ShootEffects (self);
    if (!SERVER) then return end;

    local ent = false
           ent = ents.Create ("clowns_famas"..num);


    ent:SetPos (self.Owner:EyePos() + (self.Owner:GetAimVector() * 25));
    ent:SetAngles (self.Owner:EyeAngles());
    ent:SetPhysicsAttacker(self.Owner)
    ent.Owner = self.Owner

    ent:Spawn();
    ent.Mag = (self:Clip1() * 2 ) + 7

    local phys = ent:GetPhysicsObject();
    local shot_length = tr.HitPos:Length();
    phys:ApplyForceCenter (self.Owner:GetAimVector():GetNormalized() * 100);

    cleanup.Add (self.Owner, "props", ent);
    undo.Create ("Thrown model");
    undo.AddEntity (ent);
    undo.SetPlayer (self.Owner);
    undo.Finish();
end


local ENT = {}

ENT.Type = "anim"
ENT.Base = "base_anim"



if SERVER then

    function ENT:Initialize()

        self.Hit = false
        self.LastShout = CurTime()
        self.CurrentPitch = 100
        self.SpawnDelay = CurTime() + 0.5
        --damage self.Damage = 75
        self:SetModel("models/props/cs_italy/orange.mdl")
        self:SetColor(Color(255,0,0,255))
        self:PhysicsInitBox(Vector(-1,-1,-1),Vector(1,1,1))
        self:SetMoveType(MOVETYPE_VPHYSICS)
        self:SetSolid(SOLID_VPHYSICS)
        self:SetTrigger(true)
        self.Mag = 30

        self:DrawShadow(false)
        -- self:SetModel( "models/weapons/w_rif_famas.mdl")

        local phys = self:GetPhysicsObject()
        if (phys:IsValid()) then
            phys:Wake()
            phys:EnableDrag(false)
            phys:AddGameFlag(FVPHYSICS_NO_IMPACT_DMG)
            --    phys:SetBuoyancyRatio(0)
        end

    end

    function ENT:Explode()
        if self.exploded then return end self.exploded = true
        local ent = ents.Create( "env_explosion" )

        ent:SetPos( self:GetPos()  )
        ent:SetOwner( self.Owner  )
        ent:SetPhysicsAttacker(  self.Owner )
        ent:Spawn()
        ent:SetKeyValue( "iMagnitude", 45 )
        ent:Fire( "Explode", 0, 0 )

        -- util.BlastDamage( self, self.Owner, self:GetPos(), 50, 75 )

        ent:EmitSound( "weapons/big_explosion.mp3" )
        if SERVER then self:Remove() end
    end


    function ENT:Touch(ent)
        if self.exploded then return end self.exploded = true
        if ent != self.Owner then
        self:Explode()
        end
    end

    function ENT:PhysicsCollide(data,phys)
        if ent != self.Owner and SERVER then
        self:Explode()
        end
    end

end

scripted_ents.Register( ENT, "clowns_famas2", true )


local ENT = {}

ENT.Type = "anim"
ENT.Base = "base_anim"



if SERVER then

    function ENT:Initialize()

        self.Hit = false
        self.LastShout = CurTime()
        self.CurrentPitch = 100
        self.SpawnDelay = CurTime() + 0.5
        --damage self.Damage = 75
        self:SetModel("models/props/cs_italy/orange.mdl")
        self:SetMaterial("phoenix_storms/plastic" )
        self:SetColor(Color(255, 0, 255,255))
        self:PhysicsInitBox(Vector(-1,-1,-1),Vector(1,1,1))
        self:SetMoveType(MOVETYPE_VPHYSICS)
        self:SetSolid(SOLID_VPHYSICS)
        self:SetTrigger(true)
        self.Mag = 30

        self:DrawShadow(false)
        -- self:SetModel( "models/weapons/w_rif_famas.mdl")

        local phys = self:GetPhysicsObject()
        if (phys:IsValid()) then
            phys:Wake()
            phys:EnableDrag(false)
            phys:AddGameFlag(FVPHYSICS_NO_IMPACT_DMG)
            --    phys:SetBuoyancyRatio(0)
        end

    end

    function ENT:Explode()

        if self.exploded then return end self.exploded = true
        local ent2 = ents.Create( "env_explosion" )
        local ply = self.Owner
        ent2:SetPos( self:GetPos()  )
        ent2:SetOwner( self.Owner  )
        ent2:SetPhysicsAttacker(  self.Owner )
        ent2:Spawn()
        ent2:SetKeyValue( "iMagnitude", 1 )
        ent2:Fire( "Explode", 0, 0 )

        local ent3 = ents.Create("cse_ent_shortgasgrenade")
        ent3:SetOwner(ply)
        ent3.Owner = ply
        ent3:SetPos(self:GetPos())
        ent3:SetAngles(Angle(1,0,0))
        ent3:Spawn()
        ent3.timer = CurTime() + 0.01
        timer.Simple(7,function()
            if IsValid(ent3) then
            ent3.Bastardgas:Remove()
            ent3.Entity:Remove()
                end
        end)

        -- util.BlastDamage( self, self.Owner, self:GetPos(), 50, 75 )

        ent3:EmitSound( "weapons/big_explosion.mp3" )
        if SERVER then self:Remove() end
    end


    function ENT:Touch(ent)
        if self.exploded then return end self.exploded = true
        if ent != self.Owner then
        self:Explode()
        end
    end

    function ENT:PhysicsCollide(data,phys)
        if ent != self.Owner and SERVER then
        self:Explode()
        end
    end

end

scripted_ents.Register( ENT, "clowns_famas3", true )


local ENT = {}

ENT.Type = "anim"
ENT.Base = "base_anim"



if SERVER then

    function ENT:Initialize()

        self.Hit = false
        self.LastShout = CurTime()
        self.CurrentPitch = 100
        self.SpawnDelay = CurTime() + 0.5
        --damage self.Damage = 75
        self:SetModel("models/props/cs_italy/orange.mdl")
        self:SetMaterial("phoenix_storms/plastic" )
        self:SetColor(Color(255, 153, 0))
        self:PhysicsInitBox(Vector(-1,-1,-1),Vector(1,1,1))
        self:SetMoveType(MOVETYPE_VPHYSICS)
        self:SetSolid(SOLID_VPHYSICS)
        self:SetTrigger(true)
        self.Mag = 30

        self:DrawShadow(false)
        -- self:SetModel( "models/weapons/w_rif_famas.mdl")

        local phys = self:GetPhysicsObject()
        if (phys:IsValid()) then
            phys:Wake()
            phys:EnableDrag(false)
            phys:AddGameFlag(FVPHYSICS_NO_IMPACT_DMG)
            --    phys:SetBuoyancyRatio(0)
        end

    end

    function ENT:Explode()
        if self.exploded then return end self.exploded = true
        local ent2 = ents.Create( "env_explosion" )
        local ply = self.Owner
        ent2:SetPos( self:GetPos()  )
        ent2:SetOwner( self.Owner  )
        ent2:SetPhysicsAttacker(  self.Owner )
        ent2:Spawn()
        ent2:SetKeyValue( "iMagnitude", 35 )
        ent2:Fire( "Explode", 0, 0 )

        for i=1,10 do
            local explfire = ents.Create("env_fire")
            explfire:SetPos(self.Entity:GetPos() + Vector(math.random(-100, 100), math.random(-100, 100), 0))
            explfire:SetKeyValue("health", "10")
            explfire:SetKeyValue("firesize", "20")
            explfire:SetKeyValue("damagescale", "2")
            explfire:SetPhysicsAttacker(self.Owner)
            explfire:SetOwner( self.Owner )
            explfire:Spawn()
            explfire:Fire("StartFire", "", 0)

            timer.Simple(9, function()
                if explfire:IsValid() then
                    explfire:Remove()
                end
            end )

        end


        -- util.BlastDamage( self, self.Owner, self:GetPos(), 50, 75 )

        ent2:EmitSound( "weapons/big_explosion.mp3" )
        if SERVER then self:Remove() end
    end


    function ENT:Touch(ent)
        if ent != self.Owner then
        self:Explode()
        end
    end

    function ENT:PhysicsCollide(data,phys)
        if ent != self.Owner and SERVER then
        self:Explode()
        end
    end

end

scripted_ents.Register( ENT, "clowns_famas4", true )


local ENT = {}

ENT.Type = "anim"
ENT.Base = "base_anim"



if SERVER then

    function ENT:Initialize()

        self.Hit = false
        self.LastShout = CurTime()
        self.CurrentPitch = 100
        self.SpawnDelay = CurTime() + 0.5
        --damage self.Damage = 75
        self:SetModel("models/props/cs_italy/orange.mdl")
        self:SetMaterial("phoenix_storms/plastic" )
        self:SetColor(Color(0, 255, 0,255))
        self:PhysicsInitBox(Vector(-1,-1,-1),Vector(1,1,1))
        self:SetMoveType(MOVETYPE_VPHYSICS)
        self:SetSolid(SOLID_VPHYSICS)
        self:SetTrigger(true)
        self.Mag = 30

        self:DrawShadow(false)
        -- self:SetModel( "models/weapons/w_rif_famas.mdl")

        local phys = self:GetPhysicsObject()
        if (phys:IsValid()) then
            phys:Wake()
            phys:EnableDrag(false)
            phys:AddGameFlag(FVPHYSICS_NO_IMPACT_DMG)
            --    phys:SetBuoyancyRatio(0)
        end

    end

    function ENT:Explode()
        if self.exploded then return end self.exploded = true
        local ent2 = ents.Create( "env_explosion" )
        local ply = self.Owner
        ent2:SetPos( self:GetPos()  )
        ent2:SetOwner( self.Owner  )
        ent2:SetPhysicsAttacker(  self.Owner )
        ent2:Spawn()
        ent2:SetKeyValue( "iMagnitude", 1 )
        ent2:Fire( "Explode", 0, 0 )

        local ent3 = ents.Create("cse_ent_shorthealthgrenade")
        ent3:SetOwner(ply)
        ent3.Owner = ply
        ent3:SetPos(self:GetPos())
        ent3:SetAngles(Angle(1,0,0))
        ent3:Spawn()
        ent3.timer = CurTime() + 0.01
        ent3.healdamage = 3
        ent3.maxhealth = 150
        timer.Simple(10,function()
            if IsValid(ent3) then
                ent3.Bastardgas:Remove()
                ent3.Entity:Remove()
            end
        end)

        -- util.BlastDamage( self, self.Owner, self:GetPos(), 50, 75 )

        ent3:EmitSound( "weapons/big_explosion.mp3" )
        if SERVER then self:Remove() end
    end


    function ENT:Touch(ent)
        if ent != self.Owner then
        self:Explode()
        end
    end

    function ENT:PhysicsCollide(data,phys)
        if ent != self.Owner and SERVER then
        self:Explode()
        end
    end

end

scripted_ents.Register( ENT, "clowns_famas6", true )