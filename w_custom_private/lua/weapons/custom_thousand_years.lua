if( SERVER ) then
	AddCSLuaFile();
	

	
end
SWEP.PrintName = "Thousand Years of Death";
	SWEP.Slot = 5;

if( CLIENT ) then
    SWEP.Category = "Shot846";
	
	SWEP.SlotPos = 4;
	SWEP.DrawAmmo = false;
	SWEP.DrawCrosshair = true;
end


SWEP.Base = "aa_base"
SWEP.Kind = WEAPON_UNARMED



SWEP.ViewModelFOV	= 75
SWEP.ViewModelFlip	= false

SWEP.Spawnable			= true
SWEP.AdminSpawnable		= true

SWEP.NextStrike = 0;
  
SWEP.ViewModel      = "models/weapons/v_kick.mdl"
SWEP.WorldModel   = ""
  
-------------Primary Fire Attributes----------------------------------------
SWEP.Primary.Delay			= 0.4 	--In seconds
SWEP.Primary.Recoil			= 0.01	--Gun Kick
SWEP.Primary.Damage			= 15	--Damage per Bullet
SWEP.Primary.NumShots		= 1		--Number of shots per one fire
SWEP.Primary.Cone			= 0.01 	--Bullet Spread
SWEP.Primary.ClipSize		= -1	--Use "-1 if there are no clips"
SWEP.Primary.DefaultClip	= -1	--Number of shots in next clip
SWEP.Primary.Automatic   	= true	--Pistol fire (false) or SMG fire (true)
SWEP.Primary.Ammo         	= "none"	--Ammo Type
 

util.PrecacheSound("player/skick/madness.mp3")
util.PrecacheSound("player/skick/foot_kickwall.mp3")
util.PrecacheSound("player/skick/foot_kickbody.mp3")
util.PrecacheSound("player/skick/sparta.mp3")

function SWEP:Think()
    if self.Owner:KeyDown(IN_RELOAD) then
        if self.NextSecondary > CurTime() then
            return
        end
        if not damage then damage = 45 end
        self.NextSecondary = CurTime() + 0.75
        self:EmitSound(  "weapons/usp/usp1.wav", 100 )
        self:ShootBullet( damage, 0.02, 1, 0.003 )
        self.Owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )

        self.Weapon:SendWeaponAnim( ACT_VM_IDLE )
    end
end
function SWEP:Initialize()
	if( SERVER ) then
			self:SetHoldType("normal");
	end
	self.Hit = { 
	Sound( "weapons/slam/throw.mp3" )};
	self.FleshHit = {
  	Sound( "Flesh.ImpactHard"  )
	} ;

end

function SWEP:Precache()
end

function SWEP:Deploy()
	self.Weapon:SendWeaponAnim( ACT_VM_IDLE );
	return true;
end

function SWEP:OnDrop()
    self:Remove()
end

function SWEP:PrimaryAttack()
	 if( CurTime() < self.NextStrike ) then return; end
	-- self.Weapon:EmitSound("player/skick/sparta.mp3")
     self.NextStrike = ( CurTime() + 2 );
     local soundfile = gb_config.websounds.."thousandyearsofdeath.mp3"
     gb_PlaySoundFromServer(soundfile, self.Owner)


	 timer.Simple( 0.001, function() if IsValid(self) then self.AttackAnim(self) end end)
	 timer.Simple( 0.002, function() if IsValid(self) then self.Weapon:SendWeaponAnim( ACT_VM_IDLE ) end end)
     timer.Simple( 0.003,  function() if IsValid(self) and SERVER then self.ShootBullets (self) end end)
	 self.Owner:SetAnimation( PLAYER_ATTACK1 );
end	

function SWEP:ShootBullets()
	--self.Weapon:EmitSound("player/kick/foot_swing.mp3");
    local  damage = 25
	if IsValid( self.Owner ) then
		local trace = self.Owner:GetEyeTrace();
		if trace.HitPos:Distance(self.Owner:GetShootPos()) <= 260 then
			if( trace.Entity:IsPlayer() or trace.Entity:IsNPC() or trace.Entity:GetClass()=="prop_ragdoll" ) then
				--	timer.Simple(0, function() game.ConsoleCommand("host_timescale 0.1\n") end)	
				--	timer.Simple(0.5, function() game.ConsoleCommand("host_timescale 1\n") end)
				self.Owner:EmitSound( self.FleshHit[math.random(1,#self.FleshHit)] );
				if SERVER then trace.Entity:SetVelocity(self.Owner:GetForward() * 600 + Vector(0,0,400)) end

                aim = trace.Entity:EyeAngles()
                aim.p = 0
                aim = aim:Forward()
                if aim:DotProduct((self.Owner:GetShootPos() - trace.Entity:GetShootPos()):GetNormal()) <= -0.7 then
                   damage = 65
                end

			else
				self.Owner:EmitSound( self.Hit[math.random(1,#self.Hit)] );
			end

        end

        bullet = {}
        bullet.Num    = 5
        bullet.Src    = self.Owner:GetShootPos()
        bullet.Dir    = self.Owner:GetAimVector()
        bullet.Spread = Vector(0.04, 0.04, 0.04)
        bullet.Tracer = 0
        bullet.Force  = 1050
        bullet.Damage = damage
        self.Owner:FireBullets(bullet)
	
	
			if IsValid( trace ) and IsValid(trace.Entity) and trace.Entity:GetClass() == "prop_door_rotating" then
			 trace.Entity:EmitSound(Sound("physics/wood/wood_box_impact_hard3.mp3"))
			 trace.Entity:Fire("open", "", .001)
			 trace.Entity:Fire("unlock", "", .001)
			 local pos = trace.Entity:GetPos()
			 local ang = trace.Entity:GetAngles()
			 local model = trace.Entity:GetModel()
			 local skin = trace.Entity:GetSkin()
			 trace.Entity:SetNotSolid(true)
			 trace.Entity:SetNoDraw(true)
			 local function ResetDoor(door, fakedoor)
			 door:SetNotSolid(false)
			 door:SetNoDraw(false)
			 fakedoor:Remove()
			 end
			 local norm = (pos - self.Owner:GetPos()):Normalize()
			 local push = 1000000 * norm
			 local ent = ents.Create("prop_physics")
			 ent:SetPos(pos)
			 ent:SetAngles(ang)
			 ent:SetModel(model)
			if(skin) then
			  ent:SetSkin(skin)
			end
			 ent:Spawn()
			 timer.Simple(.01, ent.SetVelocity, ent, push)               
			 timer.Simple(.01, ent:GetPhysicsObject().ApplyForceCenter, ent:GetPhysicsObject(), push)
			 timer.Simple(140, ResetDoor, trace.Entity, ent)
		end
	end

end

function SWEP:SecondaryAttack()
        self:Fly()
end

function SWEP:AttackAnim()
	self.Weapon:SendWeaponAnim( ACT_VM_PRIMARYATTACK )
end

function SWEP:Reload()

end