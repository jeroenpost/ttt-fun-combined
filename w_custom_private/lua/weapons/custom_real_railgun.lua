--General Variables\\
SWEP.AdminSpawnable = false
SWEP.ViewModelFOV = 64

SWEP.VElements = {
	["shell++"] = { type = "Model", model = "models/weapons/shotgun_shell.mdl", bone = "ValveBiped.Bip01", rel = "clip", pos = Vector(-1.9, 0.3, 0), angle = Angle(0, 0, 0), size = Vector(0.264, 0.264, 0.264), color = Color(255, 255, 255, 255), surpresslightning = false, material = "camos/camo17", skin = 0, bodygroup = {} },
	["shell++++"] = { type = "Model", model = "models/weapons/shotgun_shell.mdl", bone = "ValveBiped.Bip01", rel = "clip", pos = Vector(-1.9, -0.9, 0), angle = Angle(0, 0, 0), size = Vector(0.264, 0.264, 0.264), color = Color(255, 255, 255, 255), surpresslightning = false, material ="camos/camo17", skin = 0, bodygroup = {} },
	["sight+"] = { type = "Model", model = "models/props_trainstation/mount_connection001a.mdl", bone = "ValveBiped.Bip01", rel = "bacl", pos = Vector(2.7, 0, 2.273), angle = Angle(90, -90, 90), size = Vector(0.059, 0.059, 0.059), color = Color(255, 255, 255, 255), surpresslightning = false, material = "camos/camo17", skin = 0, bodygroup = {} },
	["shell+++"] = { type = "Model", model = "models/weapons/shotgun_shell.mdl", bone = "ValveBiped.Bip01", rel = "clip", pos = Vector(-1.9, -1.53, -0.401), angle = Angle(0, 0, 0), size = Vector(0.264, 0.264, 0.264), color = Color(255, 255, 255, 255), surpresslightning = false, material = "camos/camo17", skin = 0, bodygroup = {} },
	["shell+++++"] = { type = "Model", model = "models/weapons/shotgun_shell.mdl", bone = "ValveBiped.Bip01", rel = "clip", pos = Vector(-1.9, -0.301, -0.401), angle = Angle(0, 0, 0), size = Vector(0.264, 0.264, 0.264), color = Color(255, 255, 255, 255), surpresslightning = false, material = "camos/camo17", skin = 0, bodygroup = {} },
	["bacl"] = { type = "Model", model = "models/props_lab/binderbluelabel.mdl", bone = "ValveBiped.Bip01", rel = "mid body", pos = Vector(-0.401, 5.599, 0.1), angle = Angle(0, 0, 90), size = Vector(0.321, 0.72, 0.889), color = Color(95, 95, 98, 255), surpresslightning = false, material = "camos/camo17", skin = 0, bodygroup = {} },
	["shell+"] = { type = "Model", model = "models/weapons/shotgun_shell.mdl", bone = "ValveBiped.Bip01", rel = "clip", pos = Vector(-1.9, 0.8, -0.401), angle = Angle(0, 0, 0), size = Vector(0.264, 0.264, 0.264), color = Color(255, 255, 255, 255), surpresslightning = false, material = "camos/camo17", skin = 0, bodygroup = {} },
	["barrel"] = { type = "Model", model = "models/props_junk/propane_tank001a.mdl", bone = "ValveBiped.Gun", rel = "front body", pos = Vector(0, -1.364, 13.182), angle = Angle(180, 0, 0), size = Vector(0.209, 0.209, 0.776), color = Color(255, 255, 255, 255), surpresslightning = false, material = "camos/camo17", skin = 0, bodygroup = {} },
	["mid body"] = { type = "Model", model = "models/props_lab/reciever01a.mdl", bone = "ValveBiped.Bip01", rel = "front body", pos = Vector(0, -1, -11.365), angle = Angle(0, 90, 90), size = Vector(0.259, 0.549, 0.379), color = Color(255, 255, 255, 255), surpresslightning = false, material = "camos/camo17", skin = 0, bodygroup = {} },
	["sight"] = { type = "Model", model = "models/props_trainstation/mount_connection001a.mdl", bone = "ValveBiped.Bip01", rel = "front body", pos = Vector(0, -3.182, 6.817), angle = Angle(-90, 0, 90), size = Vector(0.059, 0.059, 0.059), color = Color(255, 255, 255, 255), surpresslightning = false, material ="camos/camo17", skin = 0, bodygroup = {} },
	["front body"] = { type = "Model", model = "models/props_junk/gascan001a.mdl", bone = "ValveBiped.Gun", rel = "", pos = Vector(0, 0.8, 7.727), angle = Angle(0, 0, 0), size = Vector(0.275, 0.275, 0.72), color = Color(255, 255, 255, 255), surpresslightning = false, material = "camos/camo17", skin = 0, bodygroup = {} },
	["shell"] = { type = "Model", model = "models/weapons/shotgun_shell.mdl", bone = "ValveBiped.Bip01", rel = "clip", pos = Vector(-1.9, 1.363, 0), angle = Angle(0, 0, 0), size = Vector(0.264, 0.264, 0.264), color = Color(255, 255, 255, 255), surpresslightning = false, material = "camos/camo17", skin = 0, bodygroup = {} },
	["clip"] = { type = "Model", model = "models/props_wasteland/cargo_container01.mdl", bone = "ValveBiped.Gun", rel = "mid body", pos = Vector(-4.092, 4.091, 0), angle = Angle(0, 91.023, 0), size = Vector(0.037, 0.009, 0.009), color = Color(255, 255, 255, 255), surpresslightning = false, material = "models/props_canal/metalwall005b", skin = 0, bodygroup = {} }
}
SWEP.WElements = {
	["shell++"] = { type = "Model", model = "models/weapons/shotgun_shell.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "clip", pos = Vector(-1.9, 0.3, 0), angle = Angle(0, 0, 0), size = Vector(0.264, 0.264, 0.264), color = Color(255, 255, 255, 255), surpresslightning = false, material = "camos/camo17", skin = 0, bodygroup = {} },
	["stock"] = { type = "Model", model = "models/props_wasteland/panel_leverHandle001a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "bacl", pos = Vector(0.779, 0, 8.635), angle = Angle(0, 180, 0), size = Vector(0.72, 0.379, 0.889), color = Color(255, 255, 255, 255), surpresslightning = false, material = "camos/camo17", skin = 0, bodygroup = {} },
	["shell++++"] = { type = "Model", model = "models/weapons/shotgun_shell.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "clip", pos = Vector(-1.9, -0.9, 0), angle = Angle(0, 0, 0), size = Vector(0.264, 0.264, 0.264), color = Color(255, 255, 255, 255), surpresslightning = false, material = "camos/camo17", skin = 0, bodygroup = {} },
	["sight+"] = { type = "Model", model = "models/props_trainstation/mount_connection001a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "mid body", pos = Vector(2.5, 0, 0), angle = Angle(0, -90, 90), size = Vector(0.059, 0.059, 0.059), color = Color(255, 255, 255, 255), surpresslightning = false, material = "camos/camo17", skin = 0, bodygroup = {} },
	["shell+++"] = { type = "Model", model = "models/weapons/shotgun_shell.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "clip", pos = Vector(-1.9, -1.53, -0.401), angle = Angle(0, 0, 0), size = Vector(0.264, 0.264, 0.264), color = Color(255, 255, 255, 255), surpresslightning = false, material = "camos/camo17", skin = 0, bodygroup = {} },
	["clip"] = { type = "Model", model = "models/props_wasteland/cargo_container01.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "mid body", pos = Vector(-3.182, 3.181, -0.151), angle = Angle(0, 91.023, 0), size = Vector(0.037, 0.009, 0.009), color = Color(255, 255, 255, 255), surpresslightning = false, material = "camos/camo17", skin = 0, bodygroup = {} },
	["shell+++++"] = { type = "Model", model = "models/weapons/shotgun_shell.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "clip", pos = Vector(-1.9, -0.301, -0.401), angle = Angle(0, 0, 0), size = Vector(0.264, 0.264, 0.264), color = Color(255, 255, 255, 255), surpresslightning = false, material = "camos/camo17", skin = 0, bodygroup = {} },
	["bacl"] = { type = "Model", model = "models/props_lab/binderbluelabel.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "mid body", pos = Vector(-0.12, 2.269, 0.129), angle = Angle(0, 0, 90), size = Vector(0.321, 0.72, 0.889), color = Color(95, 95, 98, 255), surpresslightning = false, material = "camos/camo17", skin = 0, bodygroup = {} },
	["shell+"] = { type = "Model", model = "models/weapons/shotgun_shell.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "clip", pos = Vector(-1.9, 0.8, -0.401), angle = Angle(0, 0, 0), size = Vector(0.264, 0.264, 0.264), color = Color(255, 255, 255, 255), surpresslightning = false, material = "camos/camo17", skin = 0, bodygroup = {} },
	["barrel"] = { type = "Model", model = "models/props_junk/propane_tank001a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "front body", pos = Vector(0, -0.456, 8.635), angle = Angle(180, 0, 0), size = Vector(0.151, 0.151, 0.151), color = Color(255, 255, 255, 255), surpresslightning = false, material ="camos/camo17", skin = 0, bodygroup = {} },
	["mid body"] = { type = "Model", model = "models/props_lab/reciever01a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "front body", pos = Vector(0, -1, -11.365), angle = Angle(0, 90, 90), size = Vector(0.259, 0.379, 0.264), color = Color(255, 255, 255, 255), surpresslightning = false, material = "camos/camo17", skin = 0, bodygroup = {} },
	["front body"] = { type = "Model", model = "models/props_junk/gascan001a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(24.09, 0.899, -5.909), angle = Angle(3.068, -89, -97.159), size = Vector(0.321, 0.264, 0.549), color = Color(255, 255, 255, 255), surpresslightning = false, material = "camos/camo17", skin = 0, bodygroup = {} },
	["shell"] = { type = "Model", model = "models/weapons/shotgun_shell.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "clip", pos = Vector(-1.9, 1.363, 0), angle = Angle(0, 0, 0), size = Vector(0.264, 0.264, 0.264), color = Color(255, 255, 255, 255), surpresslightning = false, material = "camos/camo17", skin = 0, bodygroup = {} },
	["sight"] = { type = "Model", model = "models/props_trainstation/mount_connection001a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "front body", pos = Vector(0, -3.182, 6.817), angle = Angle(-90, 0, 90), size = Vector(0.059, 0.059, 0.059), color = Color(255, 255, 255, 255), surpresslightning = false, material = "camos/camo17", skin = 0, bodygroup = {} }
}

SWEP.HoldType = "shotgun"
SWEP.ViewModelFOV = 59.448818897638
SWEP.ViewModelFlip = false
SWEP.ViewModel = "models/weapons/v_shotgun.mdl"
SWEP.WorldModel = "models/weapons/w_shotgun.mdl"
SWEP.ShowViewModel = true
SWEP.ShowWorldModel = true
SWEP.ViewModelBoneMods = {
    ["ValveBiped.Pump"] = { scale = Vector(0.009, 0.009, 0.009), pos = Vector(0, 0, 0), angle = Angle(0, 0, 0) },
    ["ValveBiped.Bip01"] = { scale = Vector(1, 1, 1), pos = Vector(-2.858, 3.492, 1.269), angle = Angle(0, 0, 0) }
}
SWEP.AutoSwitchTo = false
SWEP.Slot = 0
SWEP.PrintName = "MPKA"
SWEP.Author = "Spastik"
SWEP.Spawnable = true
SWEP.AutoSwitchFrom = false
SWEP.FiresUnderwater = false
SWEP.Weight = 5
SWEP.DrawCrosshair = true
SWEP.Category = "S&G Munitions"
SWEP.SlotPos = 3
SWEP.DrawAmmo = true
SWEP.ReloadSound = "weapons/smg1/smg1_reload.wav"
SWEP.Instructions = "PULL THE TRIGGER TO BRING BACK THE DEAD (JUST TO KILL THEM AGAIN SECONDS LATER)."
SWEP.Contact = ""
SWEP.Purpose = "FOR KEEPING YOUR ENEMIES MORE LIVELY BY CHASING THEM DOWN WITH A STREAM OF ROCKETS."
SWEP.Base = "gb_camo_base"
SWEP.HoldType = "smg"

SWEP.ShowViewModel = true
SWEP.ShowWorldModel = true
SWEP.ViewModelBoneMods = {
	["ValveBiped.handle"] = { scale = Vector(0.009, 0.009, 0.009), pos = Vector(0, 0, 0), angle = Angle(0, 0, 0) },
	["ValveBiped.Bip01"] = { scale = Vector(1, 1, 1), pos = Vector(-6.667, 2.029, 1.159), angle = Angle(0, 0, 0) }
}
--General Variables\\
SWEP.PrimaryReloadSound = Sound("Weapon_SMG1.Reload")
--Primary Fire Variables\\
SWEP.Primary.Round 			= ("ent_rocketshot_spastik")	--NAME OF ENTITY GOES HERE
SWEP.Primary.Damage = 65
SWEP.Primary.TakeAmmo = 1
SWEP.Primary.ClipSize = 50
SWEP.Primary.Ammo = "RPG_round"
SWEP.Primary.DefaultClip = 500
SWEP.Primary.Spread = 0.001
SWEP.Primary.NumberofShots = 1
SWEP.Primary.Automatic = true
SWEP.Primary.Recoil = 1
SWEP.Primary.Delay = 1
SWEP.Primary.Force = 40
--Primary Fire Variables\\

--Secondary Fire Variables\\
SWEP.Secondary.NumberofShots = 1
SWEP.Secondary.Force = 30
SWEP.Secondary.Spread = 0.1
SWEP.Secondary.DefaultClip = 32
SWEP.Secondary.Automatic = "true"
SWEP.Secondary.Ammo = "None"
SWEP.Secondary.Recoil = 1
SWEP.Secondary.Delay = 0.2
SWEP.Secondary.TakeAmmo = 1
SWEP.Secondary.ClipSize = 0
SWEP.Secondary.Damage = 10
--Secondary Fire Variables\\
--Secondary Fire Variables\\
--SWEP:Initialize()\\

--SWEP:Initialize()\\
SWEP.NextShot = 0
function SWEP:PrimaryAttack()
    if self.Owner:KeyDown(IN_USE) and self.Owner:KeyDown(IN_ATTACK)  then return end
    if self:Clip1() <= 0 or self.NextShot > CurTime() then return end
	self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
    self.NextShot = CurTime() + self.Primary.Delay
    self:FireRocket()
	self:SetClip1( self:Clip1() -1 )
	self:SendWeaponAnim(ACT_VM_PRIMARYATTACK)
	self.Owner:SetAnimation( PLAYER_ATTACK1 )
	self.Weapon:EmitSound("weapons/grenade_launcher1.wav", 500, 80, 1, 0)
	self.Weapon:EmitSound("weapons/smg1/npc_smg1_fire1.wav", 500, 80, 1, -1)
	self.Owner:EmitSound("weapons/pistol/pistol_fire2.wav" .. math.random(1, 2) .. ".wav", 90, math.random(95, 75),1 ,1)
	self.Weapon:SetNWFloat( "LastShootTime", CurTime() )
end

function SWEP:FireRocket()
	local aim = self.Owner:GetAimVector()
	local side = aim:Cross(Vector(0,0,1))
	local up = side:Cross(aim)
	local pos = self.Owner:GetShootPos() + side * 10 + up * -10

	if SERVER then
	local rocket = ents.Create(self.Primary.Round)
	if !IsValid(rocket) then return false end
	rocket:SetAngles(self.Owner:GetAimVector():Angle(90,90,0))
	rocket:SetPos(pos)
	rocket:SetOwner(self.Owner)
    rocket.damage = 40
	rocket:Spawn()
	rocket.Owner = self.Owner
	rocket:Activate()
	eyes = self.Owner:EyeAngles()
		local phys = rocket:GetPhysicsObject()
			phys:SetVelocity(self.Owner:GetAimVector() * 4000)
	end
		if SERVER and !self.Owner:IsNPC() then
		local anglo = Angle(-3, 0, 0)
		self.Owner:ViewPunch(anglo)
		end

end

function SWEP:CheckWeaponsAndAmmo()
	if SERVER and self.Weapon != nil then
		timer.Simple(.01, function() if not IsValid(self) then return end
			if not IsValid(self.Owner) then return end
			self.Owner:StripWeapon(self.Gun)
		end)
	end
end

--SWEP:SecondaryFire()\\
SWEP.NextSecond = 0
function SWEP:SecondaryAttack()
    if self.NextSecond > CurTime() then return end
    if self.Owner:KeyDown(IN_USE)  then
        return
    end
    self.NextSecond = CurTime() + 0.8
    self:Disorientate()
    self:EmitSound("weapons/pistol/pistol_fire" .. math.random(1, 2) .. ".wav",100)

    self:NormalShootBullet(5, 5, 20, 0.085)


end
function SWEP:Reload()

    self.Weapon:DefaultReload(ACT_SHOTGUN_PUMP)



    if (self.Weapon:Clip1() < self.Primary.ClipSize) and (self.Owner:GetAmmoCount(self.Primary.Ammo) > 0) then
        self:EmitSound( "weapons/shotgun/shotgun_cock.wav", 500, 100, 1, 1 )
        self.Weapon:SetNextPrimaryFire( CurTime() + 2 )

        if not (CLIENT) then

            self.Owner:DrawViewModel(true)

        end

    end
end



function SWEP:Initialize()

	-- other initialize code goes here
    nextshottime = CurTime()
	if CLIENT then

		self:SetHoldType( self.HoldType or "pistol" )
		-- Create a new table for every weapon instance
		self.VElements = table.FullCopy( self.VElements )
		self.WElements = table.FullCopy( self.WElements )
		self.ViewModelBoneMods = table.FullCopy( self.ViewModelBoneMods )

		self:CreateModels(self.VElements) -- create viewmodels
		self:CreateModels(self.WElements) -- create worldmodels

		-- init view model bone build function
		if IsValid(self.Owner) then
			local vm = self.Owner:GetViewModel()
			if IsValid(vm) then
				self:ResetBonePositions(vm)

				--[[-- Init viewmodel visibility
				if (self.ShowViewModel == nil or self.ShowViewModel) then
					vm:SetColor(Color(255,255,255,255))
				else
					-- we set the alpha to 1 instead of 0 because else ViewModelDrawn stops being called
					vm:SetColor(Color(255,255,255,1))
					-- ^ stopped working in GMod 13 because you have to do Entity:SetRenderMode(1) for translucency to kick in
					-- however for some reason the view model resets to render mode 0 every frame so we just apply a debug material to prevent it from drawing
					vm:SetMaterial("Debug/hsv")
				end]]--
			end
		end
	end
end
function SWEP:Holster()

	if CLIENT and IsValid(self.Owner) then
		local vm = self.Owner:GetViewModel()
		if IsValid(vm) then
			self:ResetBonePositions(vm)
		end
	end

	return true
end

function SWEP:OnRemove()
	self:Holster()
end

function SWEP:OnDrop()
    self:Remove()
end




local sndPowerUp		= Sound("rope_hit.mp3")
local sndPowerDown		= Sound ("shoot_rope.mp3")
local sndTooFar			= Sound ("to_far.mp3")

SWEP.HoldType = "smg"

SWEP.NextAmmoGive = 0
SWEP.NextAmmoTake = 0
SWEP.NextDisable = 0
SWEP.NextFire2 = 0
function SWEP:Think()

    if self.NextAmmoGive < CurTime() && self:Clip1() < 10 then
    self.NextAmmoGive = CurTime() + 3
    self:SetClip1( self:Clip1() + 1 )
    end

    if (!self.Owner || self.Owner == NULL) or self:Clip1() < 1 then

    if ( !self.Beam ) then return end
    if( IsValid( self.Beam) ) then
        self.Beam:Remove()
    end
    self.Beam = nil
    return
    end




    if self.Owner:KeyDown(IN_USE)  and self.Owner:KeyDown(IN_RELOAD) then
        if self.Owner:KeyDown(IN_ATTACK2) then
            self:TrickStomp()
        end

        if self.Owner:KeyDown(IN_ATTACK) then
            self:MakeExplosionthing()
        end
        return
    end
    if self.Owner:KeyDown(IN_USE) and self.Owner:KeyDown(IN_ATTACK2) then

        return
    end





    if ( self.Owner:KeyPressed( IN_JUMP ) ) then

        self:StartAttack()

    elseif ( self.Owner:KeyDown( IN_JUMP )  ) then

        self:UpdateAttack()
        self.Weapon:SetNextSecondaryFire( CurTime() + 1 )

    elseif ( self.Owner:KeyReleased( IN_JUMP )  ) then

        self:EndAttack( true )

    end


    self.BaseClass.Think(self)

end

function SWEP:DoTrace( endpos )
    local trace = {}
    trace.start = self.Owner:GetShootPos()
    trace.endpos = trace.start + (self.Owner:GetAimVector() * 14096) --14096 is length modifier.
    if(endpos) then trace.endpos = (endpos - self.Tr.HitNormal * 7) end
    trace.filter = { self.Owner, self.Weapon }

    self.Tr = nil
    self.Tr = util.TraceLine( trace )
end


function SWEP:StartAttack()


    if not self:CanPrimaryAttack() then return end

    --Get begining and end poins of trace.
    local gunPos = self.Owner:GetShootPos() --Start of distance trace.
    local disTrace = self.Owner:GetEyeTrace() --Store all results of a trace in disTrace.
    local hitPos = disTrace.HitPos --Stores Hit Position of disTrace.

    --Calculate Distance
    --Thanks to rgovostes for this code.
    local x = (gunPos.x - hitPos.x)^2;
    local y = (gunPos.y - hitPos.y)^2;
    local z = (gunPos.z - hitPos.z)^2;
    local distance = math.sqrt(x + y + z);

    --Only latches if distance is less than distance CVAR
    local distanceCvar = GetConVarNumber("grapple_distance")
    inRange = false
    if distance <= distanceCvar then
        inRange = true
    end

    if inRange then
        if (SERVER) then

            if (!self.Beam) then --If the beam does not exist, draw the beam.
            if not self:CanPrimaryAttack() then return end
            --grapple_beam
            self.Beam = ents.Create( "trace1" )
            self.Beam:SetPos( self.Owner:GetShootPos() )
            self.Beam:Spawn()
            self:TakePrimaryAmmo(1)

            end

            self.Beam:SetParent( self.Owner )
            self.Beam:SetOwner( self.Owner )

        end


        self:DoTrace()
        self.speed = 10000 --Rope latch speed. Was 3000.
        self.startTime = CurTime()
        self.endTime = CurTime() + self.speed
        self.dtfix = -1

        if (SERVER && self.Beam) then
        self.Beam:GetTable():SetEndPos( self.Tr.HitPos )
        end

        self:UpdateAttack()

        self.Weapon:EmitSound( sndPowerDown )
    else
        --Play A Sound
        self.Weapon:EmitSound( sndTooFar )
    end
end

function SWEP:UpdateAttack()

    -- if not self:CanPrimaryAttack() then return end
    if !self.Tr or self:Clip1() < 1 then return end
    self.Owner:LagCompensation( true )


    if (!endpos) then endpos = self.Tr.HitPos end

    if (SERVER && self.Beam) then
    self.Beam:GetTable():SetEndPos( endpos )
    end

    lastpos = endpos



    if ( self.Tr.Entity:IsValid() ) then

        endpos = self.Tr.Entity:GetPos()
        if ( SERVER and IsValid(self.Beam) ) then
            self.Beam:GetTable():SetEndPos( endpos )
        end

    end

    local vVel = (endpos - self.Owner:GetPos())
    local Distance = endpos:Distance(self.Owner:GetPos())

    local et = (self.startTime + (Distance/self.speed))
    if(self.dtfix != 0) then
    self.dtfix = (et - CurTime()) / (et - self.startTime)
    end
    if(self.dtfix < 0) then
        self.Weapon:EmitSound( sndPowerUp )
        self.dtfix = 0
    end

    if(self.dtfix == 0) then
        zVel = self.Owner:GetVelocity().z
        vVel = vVel:GetNormalized()*(math.Clamp(Distance,0,25))
        if( SERVER ) then
            if  self.NextAmmoTake < CurTime() then
                self:TakePrimaryAmmo(1)
                self.NextAmmoTake = CurTime() + 1
                self.NextAmmoGive = CurTime() + 1.2
            end

            local gravity = GetConVarNumber("sv_gravity")

            vVel:Add(Vector(0,0,(gravity/50)*1.5))

            if(zVel < 0) then
                vVel:Sub(Vector(0,0,zVel/100))
            end
            self.Owner:SetVelocity(vVel)
        end
    end

    endpos = nil

    self.Owner:LagCompensation( false )

end

function SWEP:EndAttack( shutdownsound )

    if ( shutdownsound ) then
        self.Weapon:EmitSound( sndPowerDown )
    end

    if ( CLIENT ) then return end
    if ( !self.Beam ) then return end
    if( IsValid( self.Beam) ) then
        self.Beam:Remove()
    end
    self.Beam = nil

end














if CLIENT then

	SWEP.vRenderOrder = nil
	function SWEP:ViewModelDrawn()

		local vm = self.Owner:GetViewModel()
		if !IsValid(vm) then return end

		if (!self.VElements) then return end

		self:UpdateBonePositions(vm)

		if (!self.vRenderOrder) then

			-- we build a render order because sprites need to be drawn after models
			self.vRenderOrder = {}

			for k, v in pairs( self.VElements ) do
				if (v.type == "Model") then
					table.insert(self.vRenderOrder, 1, k)
				elseif (v.type == "Sprite" or v.type == "Quad") then
					table.insert(self.vRenderOrder, k)
				end
			end

		end

		for k, name in ipairs( self.vRenderOrder ) do

			local v = self.VElements[name]
			if (!v) then self.vRenderOrder = nil break end
			if (v.hide) then continue end

			local model = v.modelEnt
			local sprite = v.spriteMaterial

			if (!v.bone) then continue end

			local pos, ang = self:GetBoneOrientation( self.VElements, v, vm )

			if (!pos) then continue end

			if (v.type == "Model" and IsValid(model)) then

				model:SetPos(pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z )
				ang:RotateAroundAxis(ang:Up(), v.angle.y)
				ang:RotateAroundAxis(ang:Right(), v.angle.p)
				ang:RotateAroundAxis(ang:Forward(), v.angle.r)

				model:SetAngles(ang)
				--model:SetModelScale(v.size)
				local matrix = Matrix()
				matrix:Scale(v.size)
				model:EnableMatrix( "RenderMultiply", matrix )

				if (v.material == "") then
					model:SetMaterial("")
				elseif (model:GetMaterial() != v.material) then
					model:SetMaterial( v.material )
				end

				if (v.skin and v.skin != model:GetSkin()) then
					model:SetSkin(v.skin)
				end

				if (v.bodygroup) then
					for k, v in pairs( v.bodygroup ) do
						if (model:GetBodygroup(k) != v) then
							model:SetBodygroup(k, v)
						end
					end
				end

				if (v.surpresslightning) then
					render.SuppressEngineLighting(true)
				end

				render.SetColorModulation(v.color.r/255, v.color.g/255, v.color.b/255)
				render.SetBlend(v.color.a/255)
				model:DrawModel()
				render.SetBlend(1)
				render.SetColorModulation(1, 1, 1)

				if (v.surpresslightning) then
					render.SuppressEngineLighting(false)
				end

			elseif (v.type == "Sprite" and sprite) then

				local drawpos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
				render.SetMaterial(sprite)
				render.DrawSprite(drawpos, v.size.x, v.size.y, v.color)

			elseif (v.type == "Quad" and v.draw_func) then

				local drawpos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
				ang:RotateAroundAxis(ang:Up(), v.angle.y)
				ang:RotateAroundAxis(ang:Right(), v.angle.p)
				ang:RotateAroundAxis(ang:Forward(), v.angle.r)

				cam.Start3D2D(drawpos, ang, v.size)
					v.draw_func( self )
				cam.End3D2D()

			end

		end

	end

	SWEP.wRenderOrder = nil
	function SWEP:DrawWorldModel()

		if (self.ShowWorldModel == nil or self.ShowWorldModel) then
			self:DrawModel()
		end

		if (!self.WElements) then return end

		if (!self.wRenderOrder) then

			self.wRenderOrder = {}

			for k, v in pairs( self.WElements ) do
				if (v.type == "Model") then
					table.insert(self.wRenderOrder, 1, k)
				elseif (v.type == "Sprite" or v.type == "Quad") then
					table.insert(self.wRenderOrder, k)
				end
			end

		end

		if (IsValid(self.Owner)) then
			bone_ent = self.Owner
		else
			-- when the weapon is dropped
			bone_ent = self
		end

		for k, name in pairs( self.wRenderOrder ) do

			local v = self.WElements[name]
			if (!v) then self.wRenderOrder = nil break end
			if (v.hide) then continue end

			local pos, ang

			if (v.bone) then
				pos, ang = self:GetBoneOrientation( self.WElements, v, bone_ent )
			else
				pos, ang = self:GetBoneOrientation( self.WElements, v, bone_ent, "ValveBiped.Bip01_R_Hand" )
			end

			if (!pos) then continue end

			local model = v.modelEnt
			local sprite = v.spriteMaterial

			if (v.type == "Model" and IsValid(model)) then

				model:SetPos(pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z )
				ang:RotateAroundAxis(ang:Up(), v.angle.y)
				ang:RotateAroundAxis(ang:Right(), v.angle.p)
				ang:RotateAroundAxis(ang:Forward(), v.angle.r)

				model:SetAngles(ang)
				--model:SetModelScale(v.size)
				local matrix = Matrix()
				matrix:Scale(v.size)
				model:EnableMatrix( "RenderMultiply", matrix )

				if (v.material == "") then
					model:SetMaterial("")
				elseif (model:GetMaterial() != v.material) then
					model:SetMaterial( v.material )
				end

				if (v.skin and v.skin != model:GetSkin()) then
					model:SetSkin(v.skin)
				end

				if (v.bodygroup) then
					for k, v in pairs( v.bodygroup ) do
						if (model:GetBodygroup(k) != v) then
							model:SetBodygroup(k, v)
						end
					end
				end

				if (v.surpresslightning) then
					render.SuppressEngineLighting(true)
				end

				render.SetColorModulation(v.color.r/255, v.color.g/255, v.color.b/255)
				render.SetBlend(v.color.a/255)
				model:DrawModel()
				render.SetBlend(1)
				render.SetColorModulation(1, 1, 1)

				if (v.surpresslightning) then
					render.SuppressEngineLighting(false)
				end

			elseif (v.type == "Sprite" and sprite) then

				local drawpos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
				render.SetMaterial(sprite)
				render.DrawSprite(drawpos, v.size.x, v.size.y, v.color)

			elseif (v.type == "Quad" and v.draw_func) then

				local drawpos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
				ang:RotateAroundAxis(ang:Up(), v.angle.y)
				ang:RotateAroundAxis(ang:Right(), v.angle.p)
				ang:RotateAroundAxis(ang:Forward(), v.angle.r)

				cam.Start3D2D(drawpos, ang, v.size)
					v.draw_func( self )
				cam.End3D2D()

			end

		end

	end

	function SWEP:GetBoneOrientation( basetab, tab, ent, bone_override )

		local bone, pos, ang
		if (tab.rel and tab.rel != "") then

			local v = basetab[tab.rel]

			if (!v) then return end

			-- Technically, if there exists an element with the same name as a bone
			-- you can get in an infinite loop. Let's just hope nobody's that stupid.
			pos, ang = self:GetBoneOrientation( basetab, v, ent )

			if (!pos) then return end

			pos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
			ang:RotateAroundAxis(ang:Up(), v.angle.y)
			ang:RotateAroundAxis(ang:Right(), v.angle.p)
			ang:RotateAroundAxis(ang:Forward(), v.angle.r)

		else

			bone = ent:LookupBone(bone_override or tab.bone)

			if (!bone) then return end

			pos, ang = Vector(0,0,0), Angle(0,0,0)
			local m = ent:GetBoneMatrix(bone)
			if (m) then
				pos, ang = m:GetTranslation(), m:GetAngles()
			end

			if (IsValid(self.Owner) and self.Owner:IsPlayer() and
				ent == self.Owner:GetViewModel() and self.ViewModelFlip) then
				ang.r = -ang.r -- Fixes mirrored models
			end

		end

		return pos, ang
	end

	function SWEP:CreateModels( tab )

		if (!tab) then return end

		-- Create the clientside models here because Garry says we can't do it in the render hook
		for k, v in pairs( tab ) do
			if (v.type == "Model" and v.model and v.model != "" and (!IsValid(v.modelEnt) or v.createdModel != v.model) and
					string.find(v.model, ".mdl") and file.Exists (v.model, "GAME") ) then

				v.modelEnt = ClientsideModel(v.model, RENDER_GROUP_VIEW_MODEL_OPAQUE)
				if (IsValid(v.modelEnt)) then
					v.modelEnt:SetPos(self:GetPos())
					v.modelEnt:SetAngles(self:GetAngles())
					v.modelEnt:SetParent(self)
					v.modelEnt:SetNoDraw(true)
					v.createdModel = v.model
				else
					v.modelEnt = nil
				end

			elseif (v.type == "Sprite" and v.sprite and v.sprite != "" and (!v.spriteMaterial or v.createdSprite != v.sprite)
				and file.Exists ("materials/"..v.sprite..".vmt", "GAME")) then

				local name = v.sprite.."-"
				local params = { ["$basetexture"] = v.sprite }
				-- make sure we create a unique name based on the selected options
				local tocheck = { "nocull", "additive", "vertexalpha", "vertexcolor", "ignorez" }
				for i, j in pairs( tocheck ) do
					if (v[j]) then
						params["$"..j] = 1
						name = name.."1"
					else
						name = name.."0"
					end
				end

				v.createdSprite = v.sprite
				v.spriteMaterial = CreateMaterial(name,"UnlitGeneric",params)

			end
		end

	end

	local allbones
	local hasGarryFixedBoneScalingYet = false

	function SWEP:UpdateBonePositions(vm)

		if self.ViewModelBoneMods then

			if (!vm:GetBoneCount()) then return end

			-- !! WORKAROUND !! --
			-- We need to check all model names :/
			local loopthrough = self.ViewModelBoneMods
			if (!hasGarryFixedBoneScalingYet) then
				allbones = {}
				for i=0, vm:GetBoneCount() do
					local bonename = vm:GetBoneName(i)
					if (self.ViewModelBoneMods[bonename]) then
						allbones[bonename] = self.ViewModelBoneMods[bonename]
					else
						allbones[bonename] = {
							scale = Vector(1,1,1),
							pos = Vector(0,0,0),
							angle = Angle(0,0,0)
						}
					end
				end

				loopthrough = allbones
			end
			-- !! ----------- !! --

			for k, v in pairs( loopthrough ) do
				local bone = vm:LookupBone(k)
				if (!bone) then continue end

				-- !! WORKAROUND !! --
				local s = Vector(v.scale.x,v.scale.y,v.scale.z)
				local p = Vector(v.pos.x,v.pos.y,v.pos.z)
				local ms = Vector(1,1,1)
				if (!hasGarryFixedBoneScalingYet) then
					local cur = vm:GetBoneParent(bone)
					while(cur >= 0) do
						local pscale = loopthrough[vm:GetBoneName(cur)].scale
						ms = ms * pscale
						cur = vm:GetBoneParent(cur)
					end
				end

				s = s * ms
				-- !! ----------- !! --

				if vm:GetManipulateBoneScale(bone) != s then
					vm:ManipulateBoneScale( bone, s )
				end
				if vm:GetManipulateBoneAngles(bone) != v.angle then
					vm:ManipulateBoneAngles( bone, v.angle )
				end
				if vm:GetManipulateBonePosition(bone) != p then
					vm:ManipulateBonePosition( bone, p )
				end
			end
		else
			self:ResetBonePositions(vm)
		end

	end

	function SWEP:ResetBonePositions(vm)

		if (!vm:GetBoneCount()) then return end
		for i=0, vm:GetBoneCount() do
			vm:ManipulateBoneScale( i, Vector(1, 1, 1) )
			vm:ManipulateBoneAngles( i, Angle(0, 0, 0) )
			vm:ManipulateBonePosition( i, Vector(0, 0, 0) )
		end

	end

	/**************************
		Global utility code
	**************************/

	-- Fully copies the table, meaning all tables inside this table are copied too and so on (normal table.Copy copies only their reference).
	-- Does not copy entities of course, only copies their reference.
	-- WARNING: do not use on tables that contain themselves somewhere down the line or you'll get an infinite loop
	function table.FullCopy( tab )

		if (!tab) then return nil end

		local res = {}
		for k, v in pairs( tab ) do
			if (type(v) == "table") then
				res[k] = table.FullCopy(v) -- recursion ho!
			elseif (type(v) == "Vector") then
				res[k] = Vector(v.x, v.y, v.z)
			elseif (type(v) == "Angle") then
				res[k] = Angle(v.p, v.y, v.r)
			else
				res[k] = v
			end
		end

		return res

	end

end

        SWEP.flyammo = 5
        SWEP.nextFly = 0
        function SWEP:TrickStomp( soundfile )
            if self.nextFly > CurTime() or CLIENT then return end
            self.nextFly = CurTime() + 0.30
            if self.flyammo < 1 then return end

            self.flyammo = self.flyammo - 1
            self.Owner:SetAnimation(PLAYER_ATTACK1)
            local power = 600

            if SERVER then self.Owner:SetVelocity(self.Owner:GetUp() * power + Vector(0, 0, power)) end
            timer.Simple(20, function() if IsValid(self) then self.flyammo = self.flyammo + 1 end end)
            --timer.Simple(0.2,self.SecondaryAttackDelay,self)
            timer.Create(self.Owner:SteamID().."jumpstomp",2,1,function()
                if IsValid(self) and IsValid(self.Owner) then
                 if SERVER then self.Owner:SetVelocity(self.Owner:GetUp() * -power + Vector(0, 0, -power)) end
             end
            end)
        end

        SWEP.NextBoom = 0

        function SWEP:MakeExplosionthing(soundy, magnitude)

            if self.NextBoom > CurTime() then return end
            self.NextBoom = (CurTime() + 2.5)
            if SERVER then
                if not magnitude then magnitude = 35 end
                local tr = self.Owner:GetEyeTrace()
                local ent = ents.Create( "env_explosion" )

                ent:SetPos( tr.HitPos  )
                ent:SetOwner( self.Owner  )
                ent:SetPhysicsAttacker(  self.Owner )
                ent:Spawn()
                ent:SetKeyValue( "iMagnitude", magnitude )
                ent:Fire( "Explode", 0, 0 )



                local ent = ents.Create( "realrailgunboom" )

                ent:SetPos( tr.HitPos  )
                ent:SetOwner( self.Owner  )
                ent:SetPhysicsAttacker(  self.Owner )
                ent.Owner = self.Owner
                ent:Spawn()
                ent.EndColor = "0 0 0"
                ent.StartColor = "50 50 255"
                ent:Spawn()
                ent.timer = CurTime() + 0.01
                ent.EndColor = "0 0 0"
                ent.StartColor = "50 50 255"
                timer.Simple(7,function()
                    if IsValid(ent) then
                        ent.Bastardgas:Remove()
                        ent.Entity:Remove()
                    end
                end)

                util.BlastDamage( self, self:GetOwner(), self:GetPos(), 50, 75 )

                    ent:EmitSound("ambient/energy/zap1.wav")

            end

            --  self:TakePrimaryAmmo( 1 )
            self.Owner:SetAnimation( PLAYER_ATTACK1 )
            local owner = self.Owner
            if not IsValid(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

            owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )
        end


        local ENT = {}

        ENT.Type = "anim"
        ENT.Base = "cse_ent_shortgasgrenade"

        function ENT:Poison(ply)
            if (ply:Health() >= 0) then
                ply:SetHealth(ply:Health() - 5)
            elseif (ply:Alive()) and (ply:Health() <= 0)then
                ply:Kill()
            else
            end
        end

        function ENT:Think()
            if not SERVER then return end
            if not self.timer then self.timer = CurTime() + 5 end
            if self.timer < CurTime() then
                if !IsValid(self.Bastardgas) && !self.Spammed then
                self.Spammed = true
                self.Bastardgas = ents.Create("env_smoketrail")
                self.Bastardgas:SetPos(self.Entity:GetPos())
                self.Bastardgas:SetKeyValue("spawnradius","256")
                self.Bastardgas:SetKeyValue("minspeed","0.5")
                self.Bastardgas:SetKeyValue("maxspeed","2")
                self.Bastardgas:SetKeyValue("startsize","16536")
                self.Bastardgas:SetKeyValue("endsize","256")
                self.Bastardgas:SetKeyValue("endcolor",self.EndColor)
                self.Bastardgas:SetKeyValue("startcolor",self.StartColor)
                self.Bastardgas:SetKeyValue("opacity","4")
                self.Bastardgas:SetKeyValue("spawnrate","15")
                self.Bastardgas:SetKeyValue("lifetime","10")
                self.Bastardgas:SetParent(self.Entity)
                self.Bastardgas:Spawn()
                self.Bastardgas:Activate()
                self.Bastardgas:Fire("turnon","", 0.1)
                local exp = ents.Create("env_explosion")
                exp:SetKeyValue("spawnflags",461)
                exp:SetPos(self.Entity:GetPos())
                exp:Spawn()
                exp:Fire("explode","",0)
                self.Entity:EmitSound(Sound("BaseSmokeEffect.Sound"))
                end

                local pos = self.Entity:GetPos()
                local maxrange = 256
                local maxstun = 10
                for k,v in pairs(player.GetAll()) do
                    if v.GasProtection then continue end
                    local plpos = v:GetPos()
                    local dist = -pos:Distance(plpos)+maxrange
                    if (pos:Distance(plpos)<=maxrange) and not v:IsGhost() then
                        local trace = {}
                        trace.start = self.Entity:GetPos()
                        trace.endpos = v:GetPos()+Vector(0,0,24)
                        trace.filter = { v, self.Entity }
                        trace.mask = COLLISION_GROUP_PLAYER
                        tr = util.TraceLine(trace)
                        if (tr.Fraction==1) then
                            local stunamount = math.ceil(dist/(maxrange/maxstun))
                            v:ViewPunch( Angle( stunamount*((math.random()*6)-2), stunamount*((math.random()*6)-2), stunamount*((math.random()*4)-1) ) )
                            self:Poison(v, stunamount, self.Owner, self.Owner:GetWeapon("weapon_gasgrenade"))
                            local edata = EffectData()
                            self:EmitSound("ambient/energy/zap1.wav")
                            edata:SetEntity(v)
                            edata:SetMagnitude(50)
                            edata:SetScale(256)

                            util.Effect("TeslaHitBoxes", edata)
                            util.Effect("ManhackSparks", edata)
                        end
                    end
                    end
                    if (self.timer+60<CurTime()) then
                        if IsValid(self.Bastardgas) then
                            self.Bastardgas:Remove()
                        end
                    end
                    if (self.timer+65<CurTime()) then
                        self.Entity:Remove()
                    end
                    self.Entity:NextThink(CurTime()+0.5)
                    return true
                end
            end


            scripted_ents.Register( ENT, "realrailgunboom", true )