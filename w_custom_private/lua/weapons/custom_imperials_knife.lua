
AddCSLuaFile()

SWEP.HoldType = "knife"

if CLIENT then

    SWEP.PrintName    = "Imperial's Knife"
    SWEP.Slot         = 7

    SWEP.ViewModelFlip = false

    SWEP.EquipMenuData = {
        type = "item_weapon",
        desc = "knife_desc"
    };

    SWEP.Icon = "vgui/ttt/icon_knife"
end

SWEP.Base               = "gb_camo_base"

SWEP.UseHands			= true
SWEP.ViewModelFlip		= false
SWEP.ViewModelFOV		= 54
SWEP.ViewModel          = "models/weapons/cstrike/c_knife_t.mdl"
SWEP.WorldModel         = "models/weapons/w_knife_t.mdl"

SWEP.DrawCrosshair      = false
SWEP.Primary.Damage         = 30
SWEP.Primary.ClipSize       = -1
SWEP.Primary.DefaultClip    = -1
SWEP.Primary.Automatic      = true
SWEP.Primary.Delay = 0.5
SWEP.Primary.Ammo       = "none"
SWEP.Secondary.ClipSize     = -1
SWEP.Secondary.DefaultClip  = -1
SWEP.Secondary.Automatic    = true
SWEP.Secondary.Ammo     = "none"
SWEP.Secondary.Delay = 0.5

SWEP.Kind = WEAPON_EQUIP3

SWEP.LimitedStock = true -- only buyable once
SWEP.WeaponID = AMMO_KNIFE

SWEP.IsSilent = true

-- Pull out faster than standard guns
SWEP.DeploySpeed = 2

function SWEP:Deploy()
    local function playerDiesSound( victim, weapon, killer )
        if( IsValid(killer) and killer:IsPlayer()  ) then
            local wep = killer:GetActiveWeapon()
            if IsValid(wep) then
                wep = wep:GetClass()
                if wep == "custom_imperials_knife" then
                    local model = victim:GetModel()
                    if model then
                        killer:SetModel(model)
                        killer:SetNWBool("disguised", true)
                    end
                end
            end
        end
    end

    hook.Add( "PlayerDeath", "playerDeathSound_imperialknife", playerDiesSound )
    return self.BaseClass.Deploy(self)

end

function SWEP:PrimaryAttack()
    if self.Owner:KeyDown(IN_USE) then
        self:ShootFlare()
        return
    end
    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
    self.Weapon:SetNextSecondaryFire( CurTime() + self.Secondary.Delay )

    if not IsValid(self.Owner) then return end

    self.Owner:LagCompensation(true)

    local spos = self.Owner:GetShootPos()
    local sdest = spos + (self.Owner:GetAimVector() * 70)

    local kmins = Vector(1,1,1) * -10
    local kmaxs = Vector(1,1,1) * 10

    local tr = util.TraceHull({start=spos, endpos=sdest, filter=self.Owner, mask=MASK_SHOT_HULL, mins=kmins, maxs=kmaxs})

    -- Hull might hit environment stuff that line does not hit
    if not IsValid(tr.Entity) then
        tr = util.TraceLine({start=spos, endpos=sdest, filter=self.Owner, mask=MASK_SHOT_HULL})
    end

    local hitEnt = tr.Entity

    -- effects
    if IsValid(hitEnt) then
        self.Weapon:SendWeaponAnim( ACT_VM_HITCENTER )

        local edata = EffectData()
        edata:SetStart(spos)
        edata:SetOrigin(tr.HitPos)
        edata:SetNormal(tr.Normal)
        edata:SetEntity(hitEnt)

        if hitEnt:IsPlayer() or hitEnt:GetClass() == "prop_ragdoll" then
            util.Effect("BloodImpact", edata)
        end
    else
        self.Weapon:SendWeaponAnim( ACT_VM_MISSCENTER )
    end

    if SERVER then
        self.Owner:SetAnimation( PLAYER_ATTACK1 )
    end


    if SERVER and tr.Hit and tr.HitNonWorld and IsValid(hitEnt) then
        if hitEnt:IsPlayer() then
            -- knife damage is never karma'd, so don't need to take that into
            -- account we do want to avoid rounding error strangeness caused by
            -- other damage scaling, causing a death when we don't expect one, so
            -- when the target's health is close to kill-point we just kill

                local dmg = DamageInfo()
                dmg:SetDamage(self.Primary.Damage)
                dmg:SetAttacker(self.Owner)
                dmg:SetInflictor(self.Weapon or self)
                dmg:SetDamageForce(self.Owner:GetAimVector() * 5)
                dmg:SetDamagePosition(self.Owner:GetPos())
                dmg:SetDamageType(DMG_SLASH)

                hitEnt:DispatchTraceAttack(dmg, spos + (self.Owner:GetAimVector() * 3), sdest)

        end
    end

    self.Owner:LagCompensation(false)
end

function SWEP:SecondaryAttack()
    if self.Owner:KeyDown(IN_USE) then
        self:Cannibal()
        return
    end
    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
    self.Weapon:SetNextSecondaryFire( CurTime() + self.Secondary.Delay )


    self.Weapon:SendWeaponAnim( ACT_VM_MISSCENTER )

    if SERVER then
        local ply = self.Owner
        if not IsValid(ply) then return end

        ply:SetAnimation( PLAYER_ATTACK1 )

        local ang = ply:EyeAngles()

        if ang.p < 90 then
            ang.p = -10 + ang.p * ((90 + 10) / 90)
        else
            ang.p = 360 - ang.p
            ang.p = -10 + ang.p * -((90 + 10) / 90)
        end

        local vel = math.Clamp((90 - ang.p) * 5.5, 550, 800)

        local vfw = ang:Forward()
        local vrt = ang:Right()

        local src = ply:GetPos() + (ply:Crouching() and ply:GetViewOffsetDucked() or ply:GetViewOffset())

        src = src + (vfw * 1) + (vrt * 3)

        local thr = vfw * vel + ply:GetVelocity()

        local knife_ang = Angle(-28,0,0) + ang
        knife_ang:RotateAroundAxis(knife_ang:Right(), -90)

        local knife = ents.Create("normal_throw_knife")
        if not IsValid(knife) then return end
        knife:SetPos(src)
        knife:SetAngles(knife_ang)

        knife:Spawn()
        if self.usedknife then

         knife.Damage = self.Primary.Damage
        else
            knife.Damage = 250
            self.usedknife = true
        end

        knife:SetOwner(self.Owner)
        knife.owner = self.Owner

        local phys = knife:GetPhysicsObject()
        if IsValid(phys) then
            phys:SetVelocity(thr)
            phys:AddAngleVelocity(Vector(0, 1500, 0))
            phys:Wake()
        end

        self:Remove()

    end
end

function SWEP:Equip()
    self.Weapon:SetNextPrimaryFire( CurTime() + (self.Primary.Delay * 1.5) )
    self.Weapon:SetNextSecondaryFire( CurTime() + (self.Secondary.Delay * 1.5) )
end

function SWEP:PreDrop()
    -- for consistency, dropped knife should not have DNA/prints
    self.fingerprints = {}
end

function SWEP:OnRemove()
    if CLIENT and IsValid(self.Owner) and self.Owner == LocalPlayer() and self.Owner:Alive() then
        RunConsoleCommand("lastinv")
    end
end

if CLIENT then
    function SWEP:DrawHUD()
        local tr = self.Owner:GetEyeTrace(MASK_SHOT)

        if tr.HitNonWorld and IsValid(tr.Entity) and tr.Entity:IsPlayer()
                and tr.Entity:Health() < (self.Primary.Damage + 0) then

            local x = ScrW() / 2.0
            local y = ScrH() / 2.0

            surface.SetDrawColor(255, 0, 0, 255)

            local outer = 20
            local inner = 10
            surface.DrawLine(x - outer, y - outer, x - inner, y - inner)
            surface.DrawLine(x + outer, y + outer, x + inner, y + inner)

            surface.DrawLine(x - outer, y + outer, x - inner, y + inner)
            surface.DrawLine(x + outer, y - outer, x + inner, y - inner)

            draw.SimpleText("INSTANT KILL", "TabLarge", x, y - 30, COLOR_RED, TEXT_ALIGN_CENTER, TEXT_ALIGN_BOTTOM)
        end

        return self.BaseClass.DrawHUD(self)
    end
end




SWEP.Base               = "gb_camo_base"
SWEP.HoldType = "knife"
SWEP.Camo = 49
