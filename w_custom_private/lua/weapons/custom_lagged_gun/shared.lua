
if ( SERVER ) then
	AddCSLuaFile()

	

end 

if ( CLIENT ) then
	
	SWEP.SlotPos = 5

	killicon.Add( "weapon_nyangun", "nyan/killicon", color_white )
	SWEP.WepSelectIcon = Material( "nyan/selection.png" )
	SWEP.BounceWeaponIcon = false
	SWEP.DrawWeaponInfoBox = false
end

SWEP.Slot = 2
   
SWEP.Kind = WEAPON_HEAVY
SWEP.LimitedStock = false
SWEP.Icon = "vgui/ttt_fun_pointshop_icons/nyancat.png" 

SWEP.Base = "weapon_tttbase"
SWEP.PrintName = "Lagged Gun"
SWEP.Category = "Robotboy655's Weapons"
SWEP.ViewModel = "models/weapons/v_smg1.mdl"
SWEP.WorldModel = "models/weapons/w_smg1.mdl"
SWEP.Spawnable = true
SWEP.AdminSpawnable = true
SWEP.AutoSpawnable = false
SWEP.AutoSwitchTo = false
SWEP.AutoSwitchFrom = false
SWEP.HoldType = "smg"

SWEP.ViewModelFlip = false

SWEP.Primary.ClipSize = 1
SWEP.Primary.Delay = 0.07
SWEP.Primary.DefaultClip = 1
SWEP.Primary.Automatic = true
SWEP.Primary.Ammo = "none"

SWEP.Secondary.ClipSize = 3
SWEP.Secondary.Delay = 0.5
SWEP.Secondary.DefaultClip = 3
SWEP.Secondary.Automatic = false
SWEP.Secondary.Ammo = "none"

SWEP.HeadshotMultiplier = 1.2

function SWEP:Initialize()
	self:SetHoldType( self.HoldType or "pistol" )
end

function SWEP:DrawWeaponSelection( x, y, wide, tall, alpha )
	surface.SetDrawColor( 255, 255, 255, alpha )
	surface.SetMaterial( self.WepSelectIcon )
	surface.DrawTexturedRect( x + 10, y, 128, 128 )
end

function SWEP:PrimaryAttack()
	if ( !self:CanPrimaryAttack() ) then return end

	if (self.Owner:IsNPC()) then
		self:EmitSound( "weapons/nyan/nya" .. math.random( 1, 2 ) .. ".mp3", 100, math.random( 60, 80 ) )
	else
		if ( self.LoopSound ) then
			self.LoopSound:ChangeVolume( 1, 0.1 )
		else
			self.LoopSound = CreateSound( self.Owner, Sound( "weapons/nyan/nyan_loop.mp3" ) )
			if ( self.LoopSound ) then self.LoopSound:Play() end
		end
		if ( self.BeatSound ) then self.BeatSound:ChangeVolume( 0, 0.1 ) end
	end

	local bullet = {}
	bullet.Num = 1
	bullet.Src = self.Owner:GetShootPos()
	bullet.Dir = self.Owner:GetAimVector()
	bullet.Spread = Vector( 0.01, 0.01, 0 )
	bullet.Tracer = 1
	bullet.Force = 5
	bullet.Damage = 5
	//bullet.AmmoType = "Ar2AltFire" -- For some extremely stupid reason this breaks the tracer effect
	bullet.TracerName = "rb655_nyan_tracer"
	self.Owner:FireBullets( bullet )

	self:SendWeaponAnim( ACT_VM_PRIMARYATTACK )
	self.Owner:SetAnimation( PLAYER_ATTACK1 )

	self:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
	self:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
end

function SWEP:SecondaryAttack()
	if ( !self:CanSecondaryAttack() ) then return end
	self:EmitSound( "weapons/nyan/nya" .. math.random( 1, 2 ) .. ".mp3", 100, math.random( 85, 100 ) )

	local bullet = {}
	bullet.Num = 8
	bullet.Src = self.Owner:GetShootPos()
	bullet.Dir = self.Owner:GetAimVector()
	bullet.Spread = Vector( 0.10, 0.1, 0 )
	bullet.Tracer = 1
	bullet.Force = 10
	bullet.Damage = 5
	//bullet.AmmoType = "Ar2AltFire"
	bullet.TracerName = "rb655_nyan_tracer"
	self.Owner:FireBullets( bullet )

	self.Weapon:SendWeaponAnim( ACT_VM_SECONDARYATTACK )
	self.Owner:SetAnimation( PLAYER_ATTACK1 )

	self.Weapon:SetNextPrimaryFire( CurTime() + self.Secondary.Delay )
	self.Weapon:SetNextSecondaryFire( CurTime() + self.Secondary.Delay ) 
end

function SWEP:Reload()
	if ( !self.Owner:KeyPressed( IN_RELOAD ) ) then return end
	if ( self:GetNextPrimaryFire() > CurTime() ) then return end

	if ( SERVER ) then
		local ang = self.Owner:EyeAngles()
		local ent = ents.Create( "ent_nyan_bomb_custom" )
		if ( IsValid( ent ) ) then
			ent:SetPos( self.Owner:GetShootPos() + ang:Forward() * 28 + ang:Right() * 24 - ang:Up() * 8 )
			ent:SetAngles( ang )
			ent:SetOwner( self.Owner )
			ent:Spawn()
			ent:Activate() 
			
			local phys = ent:GetPhysicsObject()
			if ( IsValid( phys ) ) then phys:Wake() phys:AddVelocity( ent:GetForward() * 1337 ) end
		end
	end
	
	self:SendWeaponAnim( ACT_VM_SECONDARYATTACK )
	self.Owner:SetAnimation( PLAYER_ATTACK1 )
	
	self:EmitSound( "weapons/nyan/nya" .. math.random( 1, 2 ) .. ".mp3", 100, math.random( 60, 80 ) )
	
	self:SetNextPrimaryFire( CurTime() + 5 )
	self:SetNextSecondaryFire( CurTime() + 5 )
end

function SWEP:DoImpactEffect( trace, damageType )
	local effectdata = EffectData()
	effectdata:SetStart( trace.HitPos )
	effectdata:SetOrigin( trace.HitNormal + Vector( math.Rand( -0.5, 0.5 ), math.Rand( -0.5, 0.5 ), math.Rand( -0.5, 0.5 ) ) )
	util.Effect( "rb655_nyan_bounce", effectdata )

	return true
end

function SWEP:FireAnimationEvent( pos, ang, event )
	return true
end

function SWEP:KillSounds()
	if ( self.BeatSound ) then self.BeatSound:Stop() self.BeatSound = nil end
	if ( self.LoopSound ) then self.LoopSound:Stop() self.LoopSound = nil end
end

function SWEP:OnRemove()
	self:KillSounds()
end

function SWEP:OnDrop()
	self:KillSounds()
end

function SWEP:Deploy()
	self.Weapon:SendWeaponAnim( ACT_VM_DRAW )
	self.Weapon:SetNextPrimaryFire( CurTime() + self.Weapon:SequenceDuration() )
	
	if ( CLIENT ) then return true end

	self.BeatSound = CreateSound( self.Owner, Sound( "weapons/nyan/nyan_beat.mp3" ) )
	if ( self.BeatSound ) then self.BeatSound:Play() end

	return true
end

function SWEP:Holster()
	self:KillSounds()
	return true
end

function SWEP:Think()
	if ( self.Owner:IsPlayer() && self.Owner:KeyReleased( IN_ATTACK ) ) then
		if ( self.LoopSound ) then self.LoopSound:ChangeVolume( 0, 0.1 ) end
		if ( self.BeatSound ) then self.BeatSound:ChangeVolume( 1, 0.1 ) end
	end
end
