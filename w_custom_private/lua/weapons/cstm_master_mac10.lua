if ( SERVER ) then
	AddCSLuaFile(  )
end
SWEP.Kind = WEAPON_NADE
SWEP.PsID = "cm_mac10"
SWEP.BulletLength = 11.5
SWEP.CaseLength = 22.8

SWEP.MuzVel = 209.973

SWEP.InternalParts = {
	[1] = {{key = "hbar"}, {key = "lbar"}},
	[2] = {{key = "hframe"}},
	[3] = {{key = "ergonomichandle"}},
	[4] = {{key = "customstock"}},
	[5] = {{key = "lightbolt"}, {key = "heavybolt"}},
	[6] = {{key = "gasdir"}}}

SWEP.PrintName			= "Terezi's Blinding Justice"
if ( CLIENT ) then

	SWEP.Author				= "Counter-Strike"
	SWEP.Slot				= 3
	SWEP.SlotPos = 0 //			= 1
	SWEP.IconLetter			= "l"
	SWEP.Muzzle = "cstm_muzzle_smg"
	SWEP.SparkEffect = "cstm_child_sparks_small"
	SWEP.SmokeEffect = "cstm_child_smoke_small"
	
	killicon.AddFont( "cstm_smg_mac10", "CSKillIcons", SWEP.IconLetter, Color( 255, 80, 0, 255 ) )
	
	SWEP.VElements = {
		["silencer"] = { type = "Model", model = "models/props_c17/oildrum001.mdl", bone = "v_weapon.mac10_parent", pos = Vector(-0.101, 3, 3.756), angle = Angle(0, 0, 0), size = Vector(0.05, 0.05, 0.159), color = Color(255, 255, 255, 1), surpresslightning = false, material = "models/bunneh/silencer", skin = 0, bodygroup = {} }
	}
	
	SWEP.WElements = {
		["silencer"] = { type = "Model", model = "models/props_c17/oildrum001.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(11.715, 1.894, -3.86), angle = Angle(-90.667, -5.316, 0), size = Vector(0.05, 0.05, 0.159), color = Color(255, 255, 255, 1), surpresslightning = false, material = "models/bunneh/silencer", skin = 0, bodygroup = {} }
	}
end

SWEP.Category = "Customizable Weaponry"
SWEP.HoldType			= "smg"
SWEP.Base				= "aa_base"
SWEP.FireModes = {"auto", "semi"}

SWEP.Spawnable			= true
SWEP.AdminSpawnable		= true

SWEP.ViewModelFOV = 70
SWEP.ViewModelFlip = false
SWEP.UseHands = true
SWEP.ViewModel		= "models/weapons/cstrike/c_smg_mac10.mdl"
SWEP.WorldModel		= "models/weapons/w_smg_mac10.mdl"
SWEP.ShowViewModel = true
SWEP.ShowWorldModel = true
SWEP.ViewModelBonescales = {}

SWEP.Weight				= 5
SWEP.AutoSwitchTo		= false
SWEP.AutoSwitchFrom		= false

SWEP.Primary.Sound			= Sound("Weapon_MAC10.Single")
SWEP.Primary.Recoil			= 0.0001
SWEP.Primary.Damage			= 14
SWEP.Primary.NumShots		= 1
SWEP.Primary.Cone			= 0.02
SWEP.Primary.ClipSize		= 200
SWEP.Primary.Delay			= 0.08
SWEP.Primary.DefaultClip	= 200
SWEP.Primary.Automatic		= true
SWEP.Primary.Ammo			= ".45ACP"
SWEP.InitialHoldtype = "smg"
SWEP.InHoldtype = "smg"
SWEP.ReloadAct = "reload"

-- Animation speed/custom reload function related
SWEP.IsReloading = false
SWEP.NoBoltAnim = true
SWEP.AnimPrefix = "mac10_"
SWEP.ReloadSpeed = 1
SWEP.ShouldBolt = false
SWEP.ReloadDelay = 0
SWEP.IncAmmoPerc = 0.65 -- Amount of frames required to pass (in percentage) of the reload animation for the weapon to have it's amount of ammo increased

-- Dynamic accuracy related
SWEP.ShotsAmount 			= 0
SWEP.ConeDecAff				= 0
SWEP.DefRecoil				= 0.001
SWEP.CurCone				= 0.05
SWEP.DecreaseRecoilTime 	= 0
SWEP.ConeAff1 				= 0 -- Crouching/standing
SWEP.ConeAff2 				= 0 -- Using ironsights

SWEP.UnConeTime				= 0 -- Amount of time after firing the last shot that needs to pass until accuracy increases
SWEP.FinalCone				= 0 -- Self explanatory
SWEP.VelocitySensivity		= 1.5 -- Percentage of how much the cone increases depending on the player's velocity (moving speed). Rifles - 100%; SMGs - 80%; Pistols - 60%; Shotguns - 20%
SWEP.HeadbobMul 			= 1
SWEP.IsSilenced 			= false
SWEP.IronsightsCone			= 0.013
SWEP.HipCone 				= 0.001
SWEP.InaccAff1 = 0.45
SWEP.ConeInaccuracyAff1 = 0.55

SWEP.Secondary.Automatic	= false
SWEP.Secondary.Ammo			= "none"

SWEP.IsUsingIronsights 		= false
SWEP.TargetMul = 0
SWEP.SetAndForget			= false

SWEP.IronSightsPos = Vector(6.0749, -5.5216, 2.3984)
SWEP.IronSightsAng = Vector(2.5174, -0.0099, 0)

SWEP.AimPos = Vector(6.4874, -5.7857, 2.3743)
SWEP.AimAng = Vector(3.8361, 5.4429, 7.4878)

SWEP.ChargePos = Vector (5.4056, -10.3522, -4.0017)
SWEP.ChargeAng = Vector (-1.7505, -55.5187, 68.8356)

SWEP.Primary.Ammo        = "SMG1"
SWEP.Primary.Sound       = Sound( "Weapon_mac10.Single" )
SWEP.AmmoEnt = "item_ammo_smg1_ttt"

local maxrange = 10000

local math = math

-- Returns if an entity is a valid physhammer punching target. Does not take
-- distance into account.
local function ValidTarget(ent)
    return IsValid(ent) and ent:IsPlayer()
    -- NOTE: cannot check for motion disabled on client
end

-- Laser COlor
if CLIENT then
    local surface = surface

    local linex = 0
    local liney = 0
    local laser = Material("trails/laser")
    function SWEP:ViewModelDrawn()
        local client = LocalPlayer()
        local vm = client:GetViewModel()
        if not IsValid(vm) then return end

        local plytr = client:GetEyeTrace(MASK_SHOT)

        local muzzle_angpos = vm:GetAttachment(1)
        local spos = muzzle_angpos.Pos + muzzle_angpos.Ang:Forward() * -0.5
        local epos = client:GetShootPos() + client:GetAimVector() * maxrange

        -- Painting beam
        local tr = util.TraceLine({start=spos, endpos=epos, filter=client, mask=MASK_ALL})

        local c = COLOR_RED
        local a = 150
        local d = (plytr.StartPos - plytr.HitPos):Length()
        if plytr.HitNonWorld then
            if ValidTarget(plytr.Entity) then
                if d < maxrange then
                    c = COLOR_GREEN
                    a = 255
                else
                    c = COLOR_RED
                end
            end
        end


        render.SetMaterial(laser)
        render.DrawBeam(spos, tr.HitPos, 5, 0, 0, c)
    end
end

function SWEP:PrimaryAttack()
    if self:Clip1() > 0 then
        self:Disorientate()
        if self.NumBlinds < 2 then
            self:Blind(0.5)
        end
    end
    self.BaseClass.PrimaryAttack(self)
end