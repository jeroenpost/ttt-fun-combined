--resource.AddFile("materials/vgui/ttt_fun_killicons/turtlegrenade.png")

if SERVER then
    AddCSLuaFile( )
end

SWEP.Base				= "weapon_tttbasegrenade"

SWEP.Kind = 17
SWEP.WeaponID = AMMO_MOLOTOV

SWEP.HoldType			= "grenade"

SWEP.InLoadoutFor = nil
SWEP.LimitedStock = false
SWEP.AllowDrop = true
SWEP.IsSilent = false
SWEP.NoSights = true
SWEP.Primary.Delay = 1.5

SWEP.ViewModel			= "models/weapons/v_eq_flashbang.mdl"
SWEP.WorldModel			= "models/Gibs/HGIBS.mdl"
SWEP.Weight				= 5
SWEP.AutoSpawnable      = false
-- really the only difference between grenade weapons: the model and the thrown
-- ent.

SWEP.PrintName	 = "Sandwich's Skulls"
SWEP.Slot		 = 17

SWEP.NextFire2 = 0
function SWEP:PrimaryAttack()
    if self.Owner:KeyDown(IN_USE) then
        if self.NextFire2 > CurTime() then return end
        self.NextFire2 = CurTime() + 45
        if SERVER then
            local ball = ents.Create("boom_skull_sand");
            self:EmitSound("Friends/friend_join.wav")
            if (ball) then
                ball:SetPos(self.Owner:GetPos() + Vector(0,0,48));
                ball.owner = self.Owner
                ball:Spawn();
                local physicsObject = ball:GetPhysicsObject();
                physicsObject:ApplyForceCenter(self.Owner:GetAimVector() * 1000);
                return ball;
            end;
        end
        return
    end
    self.BaseClass.PrimaryAttack(self)
end
if CLIENT then
    -- Path to the icon material



    SWEP.Icon = "vgui/ttt_fun_killicons/turtlegrenade.png"

    function SWEP:DrawWorldModel()
        --self:DrawModel()
        local ply = self.Owner
        local pos = self.Weapon:GetPos()
        local ang = self.Weapon:GetAngles()
        if ply:IsValid() then
            local bone = ply:LookupBone("ValveBiped.Bip01_R_Hand")
            if bone then
                pos,ang = ply:GetBonePosition(bone)
            end
        else
            self.Weapon:DrawModel() --Draw the actual model when not held.
            return
        end
        if self.ModelEntity:IsValid() == false then
            self.ModelEntity = ClientsideModel(self.WorldModel)
            self.ModelEntity:SetNoDraw(true)
        end

        self.ModelEntity:SetModelScale(1.2,0)
        self.ModelEntity:SetPos(pos+Vector(4,2,0))
        self.ModelEntity:SetAngles(ang+Angle(0,0,190))
        self.ModelEntity:DrawModel()
    end
    function SWEP:ViewModelDrawn()
        local ply = self.Owner
        if ply:IsValid() and ply == LocalPlayer() then
            local vmodel = ply:GetViewModel()
            local idParent = vmodel:LookupBone("v_weapon.Flashbang_Parent")
            local idBase = vmodel:LookupBone("v_weapon")
            if not vmodel:IsValid() or not idParent or not idBase then return end --Ensure the model and bones are valid.
            local pos, ang = vmodel:GetBonePosition(idParent)
            local pos1, ang1 = vmodel:GetBonePosition(idBase) --Rotations were screwy with the parent's angle; use the models baseinstead.

            if self.ModelEntity:IsValid() == false then
                self.ModelEntity = ClientsideModel(self.WorldModel)
                self.ModelEntity:SetNoDraw(true)
            end

            self.ModelEntity:SetModelScale(0.75,0)
            self.ModelEntity:SetPos(pos-ang1:Forward()*2.25-ang1:Up()*1.25+1*ang1:Right())
            self.ModelEntity:SetAngles(ang1+Angle(-35,180,290))
            self.ModelEntity:DrawModel()
        end
    end
end

function SWEP:GetGrenadeName()
    return "sandwich_skull"
end

SWEP.NextFire = 0
SWEP.NextFire3 = 0
SWEP.RemoteBoom = false
function SWEP:SecondaryAttack()
    if self.Owner:KeyDown(IN_USE) then
        if self.NextFire3 > CurTime() then return end
        self.NextFire3 = CurTime() + 25
        if SERVER then
            self:EmitSound("Friends/friend_join.wav")
            local ball = ents.Create("boom_skull_sand_remote");

            if (ball) then
                ball:SetPos(self.Owner:GetPos() + Vector(0,0,48));
                ball:SetPhysicsAttacker(self.Owner)
                ball.owner = self.Owner
                ball:Spawn();
                --ball:SetModelScale( ball:GetModelScale() * 0.1, 0.01 )
                local physicsObject = ball:GetPhysicsObject();
                physicsObject:ApplyForceCenter(self.Owner:GetAimVector() * 1000);
                self.RemoteBoom= ball;
            end;
        end
        return

    elseif self.NextFire > CurTime() then return end
    self.NextFire = CurTime() + 15
    if SERVER then
        local ball = ents.Create("sandwich_skull");
        self:EmitSound("Friends/friend_join.wav")
        if (ball) then
            ball:SetPos(self.Owner:GetPos() + Vector(0,0,48));
            ball:SetPhysicsAttacker(self.Owner)
            ball.Owner = self.Owner
            ball.damage = 50
            ball.shouldmakefire = true
            ball:Spawn();
            --ball:SetModelScale( ball:GetModelScale() * 0.1, 0.01 )
            local physicsObject = ball:GetPhysicsObject();
            physicsObject:ApplyForceCenter(self.Owner:GetAimVector() * 1000000);
            return ball;
        end;
    end

end

function SWEP:Reload()
    if self.RemoteBoom and IsValid(self.RemoteBoom) then
        self.RemoteBoom:Explode()
        self.RemoteBoom = false
    end
end

--Taken from base grenade
function SWEP:Initialize()
    if self.SetHoldType then
        self:SetHoldType(self.HoldNormal)
    end

    self:SetDeploySpeed(self.DeploySpeed)

    self:SetDetTime(0)
    self:SetThrowTime(0)
    self:SetPin(false)

    self.was_thrown = false
    if CLIENT then
        self.ModelEntity = ClientsideModel(self.WorldModel)
        self.ModelEntity:SetNoDraw(true)
    end
end



function SWEP:Deploy()
    self.Weapon:SendWeaponAnim(ACT_VM_DRAW)
    return true
end

local ENT = {}

ENT.Type = "anim"
ENT.Base = "base_anim"


if SERVER then

    function ENT:Initialize()
        self:SetModel("models/Gibs/HGIBS.mdl")
        self:SetModelScale(self:GetModelScale()*1,0)
        self:SetColor(Color(128,128,128,255))
        self.Entity:PhysicsInit(SOLID_VPHYSICS);
        self.Entity:SetMoveType(MOVETYPE_VPHYSICS);
        self.Entity:SetSolid(SOLID_VPHYSICS);

        self.Entity:SetCollisionGroup(COLLISION_GROUP_WEAPON)
        local phys = self:GetPhysicsObject()
        if phys:IsValid() then
            phys:Wake()
            phys:EnableGravity(true);
        end
    end

    function ENT:Use( activator, caller )
        self.Entity:Remove()
        if ( activator:IsPlayer() ) then
            if activator:Health() < 150 then

                    activator:SetHealth(activator:Health()+10)

            end
            activator:EmitSound("crisps/eat.mp3")
        end
    end


end

scripted_ents.Register( ENT, "eatable_skull_sand", true )


local ENT = {}

ENT.Type = "anim"
ENT.Base = "base_anim"


if SERVER then

    function ENT:Initialize()
        self:SetModel("models/Gibs/HGIBS.mdl")
        self:SetModelScale(self:GetModelScale()*1,0)
        self:SetColor(Color(128,0,0,255))
        self.Entity:PhysicsInit(SOLID_VPHYSICS);
        self.Entity:SetMoveType(MOVETYPE_VPHYSICS);
        self.Entity:SetSolid(SOLID_VPHYSICS);

        self.Entity:SetCollisionGroup(COLLISION_GROUP_WEAPON)
        local phys = self:GetPhysicsObject()
        if phys:IsValid() then
            phys:Wake()
            phys:EnableGravity(true);
        end
        timer.Create(self:EntIndex().."sandboom2",0.5,20,function()
            if IsValid(self) then
             --self:EmitSound( Sound( "C4.PlantSound" ))
             self:SetColor(Color(math.Rand(50,255),0,0,255))
            end
        end)
        timer.Create(self:EntIndex().."sandboom2boom",10,1,function()
            local effect = EffectData()
            effect:SetStart(self:GetPos())
            effect:SetOrigin(self:GetPos())
            effect:SetScale(160)
            effect:SetRadius(220)
            effect:SetMagnitude(250)

            util.Effect("Explosion", effect, true, true)

            util.BlastDamage(self, self.owner, self:GetPos(), 120, 90)
            self:Remove()
        end)
    end

    function ENT:Use( activator, caller )
        self:EmitSound("common/bugreporter_failed.wav")
    end


end

scripted_ents.Register( ENT, "boom_skull_sand", true )


local ENT = {}

ENT.Type = "anim"
ENT.Base = "base_anim"


if SERVER then

    function ENT:Initialize()
        self:SetModel("models/Gibs/HGIBS.mdl")
        self:SetModelScale(self:GetModelScale()*1,0)
        self:SetColor(Color(0,0,128,255))
        self.Entity:PhysicsInit(SOLID_VPHYSICS);
        self.Entity:SetMoveType(MOVETYPE_VPHYSICS);
        self.Entity:SetSolid(SOLID_VPHYSICS);
        self.CanBoom = CurTime() + 3
        self.Angles = -360

        self.Entity:SetCollisionGroup(COLLISION_GROUP_WEAPON)
        local phys = self:GetPhysicsObject()
        if phys:IsValid() then
            phys:Wake()
            phys:EnableGravity(true);
        end
        timer.Create(self:EntIndex().."sandboom2",3,0,function()
            if IsValid(self) then
                self:EmitSound( Sound( "C4.PlantSound" ))
                self:SetColor(Color(0,0,math.Rand(50,255),255))
               -- self:SetAngles(Angle(0,math.Rand(0,360),0))
            end
        end)

    end

    function ENT:Think()
        if not self.Angl then self.Angl =-360 end
        self.Angl = self.Angl + 10
        self:SetAngles(Angle(0,self.Angl,0))
    end

    function ENT:Use( activator, caller )
        if self.CanBoom > CurTime() then return end
        self:EmitSound("common/bugreporter_failed.wav")
        self:Explode()
    end

    function ENT:Touch( ent )
        if ent:IsPlayer() and not ent.nextsandtouch or (ent.nextsandtouch and ent.nextsandtouch < CurTime()) then
            ent.nextsandtouch = CurTime() + 3
            ent:Ignite(0.5,0)
            local dmginfo = DamageInfo()
            dmginfo:SetDamage( 20 ) --50 damage
            dmginfo:SetDamageType( DMG_BULLET ) --Bullet damage
            dmginfo:SetAttacker( self.owner or ent ) --First player found gets credit
            dmginfo:SetDamageForce( Vector( 0, 0, 50 ) ) --Launch upwards
            dmginfo:SetInflictor(self.owner or self)
            ent:TakeDamageInfo( dmginfo )
            self:Explode()
        end
    end

    function ENT:Explode()
        if self.exploded then return end self.exploded = true
        local effect = EffectData()
        effect:SetStart(self:GetPos())
        effect:SetOrigin(self:GetPos())
        effect:SetScale(60)
        effect:SetRadius(60)
        effect:SetMagnitude(100)

        util.Effect("Explosion", effect, true, true)

        util.BlastDamage(self, self.owner, self:GetPos(), 120, 90)
        self:Remove()
    end


end

scripted_ents.Register( ENT, "boom_skull_sand_remote", true )


function SWEP:Throw()
    if CLIENT then
        self:SetThrowTime(0)
    elseif SERVER then
        local ply = self.Owner
        if not IsValid(ply) then return end



        local ang = ply:EyeAngles()

        -- don't even know what this bit is for, but SDK has it
        -- probably to make it throw upward a bit
        if ang.p < 90 then
            ang.p = -10 + ang.p * ((90 + 10) / 90)
        else
            ang.p = 360 - ang.p
            ang.p = -10 + ang.p * -((90 + 10) / 90)
        end

        local vel = math.min(800, (90 - ang.p) * 6)

        local vfw = ang:Forward()
        local vrt = ang:Right()
        --      local vup = ang:Up()

        local src = ply:GetPos() + (ply:Crouching() and ply:GetViewOffsetDucked() or ply:GetViewOffset())
        src = src + (vfw * 8) + (vrt * 10)

        local thr = vfw * vel + ply:GetVelocity()

        self:CreateGrenade(src, Angle(0,0,0), thr, Vector(600, math.random(-1200, 1200), 0), ply)

        self:SetThrowTime(0)
       -- self:Remove()
    end
end