if ( SERVER ) then
	SWEP.HasRail = false
	SWEP.NoEOTech = true
	SWEP.NoDocter = true
	
	SWEP.EoTechWithRail = true
	SWEP.DoesntDropRail = true
end

SWEP.Kind = WEAPON_EQUIP2
SWEP.BulletLength = 8.58
SWEP.CaseLength = 69.20

SWEP.MuzVel = 557.741

SWEP.Attachments = {
	[1] = {"kobra", "eotech", "acog", "ballistic", "aimpoint"},
	[2] = {"bipod"},
	[3] = {"laser"}}
	
SWEP.InternalParts = {
	[1] = {{key = "hbar"}, {key = "lbar"}},
	[2] = {{key = "hframe"}},
	[3] = {{key = "ergonomichandle"}},
	[4] = {{key = "customstock"}},
	[5] = {{key = "lightbolt"}, {key = "heavybolt"}},
	[6] = {{key = "gasdir"}}}

if ( CLIENT ) then
	SWEP.ActToCyc = {}
	SWEP.ActToCyc["ACT_VM_DRAW"] = 0
	SWEP.ActToCyc["ACT_VM_RELOAD"] = 0.8
	
	SWEP.PrintName			= "Mongers 'Get rekt' snipah"
	SWEP.Author				= "Counter-Strike"
	SWEP.Slot = 6 //				= 3
	SWEP.SlotPos = 0 //			= 3
	SWEP.IconLetter			= "z"
	SWEP.SmokeEffect = "cstm_child_smoke_large"
	SWEP.SparkEffect = "cstm_child_sparks_large"
	SWEP.Muzzle = "cstm_muzzle_br"
	SWEP.ACOGDist = 5
	SWEP.BallisticDist = 4.3
	SWEP.SemiText = "Bolt-action"
	SWEP.ViewMoveWhenFiring = true

	SWEP.DifType = true
	

	SWEP.LaserTune = {
		PosRight = 0,
		PosForward = 0,
		PosUp = 0,
		AngUp = 90,
		AngRight = 0,
		AngForward = 0 }
		
	SWEP.VElements = {
	["bipod"] = { type = "Model", model = "models/wystan/attachments/bipod.mdl", bone = "v_weapon.scout_Parent", rel = "", pos = Vector(0.165, 0.524, 12.399), angle = Angle(0, 0, 93.293), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {[1] = 1} },
	["acog"] = { type = "Model", model = "models/wystan/attachments/2cog.mdl", bone = "v_weapon.scout_Parent", rel = "", pos = Vector(0.241, -1.132, 1.177), angle = Angle(-180, 0, 90), size = Vector(0.75, 0.75, 0.75), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["aimpoint"] = { type = "Model", model = "models/wystan/attachments/aimpoint.mdl", bone = "v_weapon.scout_Parent", rel = "", pos = Vector(0.284, -2.503, -0.357), angle = Angle(-180, 0, 90), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["eotech"] = { type = "Model", model = "models/wystan/attachments/2otech557sight.mdl", bone = "v_weapon.scout_Parent", rel = "", pos = Vector(-0.341, -7.446, -5.237), angle = Angle(91.023, 0.972, 90.507), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["ballistic"] = { type = "Model", model = "models/bunneh/scope01.mdl", bone = "v_weapon.scout_Parent", rel = "", pos = Vector(1.258, 3.701, -4.291), angle = Angle(90, 90, 0), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["kobra"] = { type = "Model", model = "models/attachments/cmore.mdl", bone = "v_weapon.scout_Parent", rel = "", pos = Vector(-0.013, 3.148, 6.651), angle = Angle(180, 0, 90), size = Vector(0.75, 0.75, 0.75), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["laser"] = { type = "Model", model = "models/props_c17/FurnitureBoiler001a.mdl", bone = "v_weapon.scout_Parent", rel = "", pos = Vector(-0.165, 1.307, 14.847), angle = Angle(180, 0, 180), size = Vector(0.028, 0.028, 0.028), color = Color(0, 0, 0, 0), surpresslightning = false, material = "models/bunneh/silencer", skin = 0, bodygroup = {} },
	["silencer"] = { type = "Model", model = "models/props_c17/oildrum001.mdl", bone = "v_weapon.scout_Parent", rel = "", pos = Vector(-0.068, 2.601, 19.705), angle = Angle(0, 0, 0), size = Vector(0.03, 0.03, 0.081), color = Color(0, 0, 0, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
}
	SWEP.WElements = {
	["bipod"] = { type = "Model", model = "models/wystan/attachments/bipod.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(22.993, 0.319, -4.699), angle = Angle(0, -88.581, -180), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {[1] = 1} },
	["acog"] = { type = "Model", model = "models/wystan/attachments/2cog.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(4.377, 0.949, 0.697), angle = Angle(-0.8, -87.74, 174.554), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
    ["aimpoint"] = { type = "Model", model = "models/wystan/attachments/aimpoint.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(4.067, 1.059, 0.76), angle = Angle(180, 93.314, -6.79), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["eotech"] = { type = "Model", model = "models/wystan/attachments/2otech557sight.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(0.01, 1.731, 6.576), angle = Angle(-8.804, 2.894, 180), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["ballistic"] = { type = "Model", model = "models/bunneh/scope01.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(-0.983, 0.134, -5.087), angle = Angle(-6.032, 2.755, 180), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["kobra"] = { type = "Model", model = "models/attachments/cmore.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(10.152, 0.952, -5.535), angle = Angle(3.775, -86.807, 172.07), size = Vector(0.699, 0.699, 0.699), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["weapon"] = { type = "Model", model = "models/weapons/w_gbip_scoutrifle.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(0, 1.49, 0.782), angle = Angle(-180, -86.806, 7.754), size = Vector(1,1,1), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["silencer"] = { type = "Model", model = "models/props_c17/oildrum001.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(32.578, -0.188, -7.88), angle = Angle(-142.764, -81.183, -82.175), size = Vector(0.045, 0.045, 0.059), color = Color(0, 0, 0, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
}

	SWEP.NoRail = true
end

SWEP.HoldType			= "ar2"
SWEP.Base				= "cstm_base_sniper3"
SWEP.Category = "Customizable Weaponry"
SWEP.FireModes = {"bolt"}

SWEP.Spawnable			= true
SWEP.AdminSpawnable		= true

SWEP.ViewModelFOV = 70
SWEP.ViewModelFlip = true
SWEP.ViewModel = "models/weapons/v_gbip_scoutrifle.mdl"
SWEP.WorldModel = "models/weapons/w_gbip_scoutrifle.mdl"
SWEP.ShowViewModel = true
SWEP.ShowWorldModel = false
SWEP.ViewModelBonescales = {}

SWEP.Weight				= 5
SWEP.AutoSwitchTo		= false
SWEP.AutoSwitchFrom		= false

SWEP.Primary.Sound			= Sound("CSTM_SCOUT")
SWEP.Primary.Recoil			= 3
SWEP.Primary.Damage			= 95
SWEP.Primary.NumShots		= 1
SWEP.Primary.Cone			= 0.001
SWEP.Primary.ClipSize		= 10
SWEP.Primary.Delay			= 2
SWEP.Primary.DefaultClip	= 10
SWEP.Primary.Automatic		= false
SWEP.Primary.Ammo			= "7.62x51MM"
SWEP.InitialHoldtype = "smg"
SWEP.InHoldtype = "smg"
SWEP.SilencedSound = Sound("CSTM_SilencedShot2")
SWEP.SilencedVolume = 78
SWEP.NoAuto = true
SWEP.UnAimAfterShot = true
SWEP.HeadshotMultiplier = 1.6

-- Animation speed/custom reload function related
SWEP.IsReloading = false
SWEP.NoBoltAnim = true
SWEP.AnimPrefix = ""
SWEP.ReloadSpeed = 1.15
SWEP.ShouldBolt = false
SWEP.ReloadDelay = 0
SWEP.IncAmmoPerc = 0.55 -- Amount of frames required to pass (in percentage) of the reload animation for the weapon to have it's amount of ammo increased
SWEP.PlaybackRate = 1

-- Dynamic accuracy related
SWEP.ShotsAmount 			= 0
SWEP.ConeDecAff				= 0
SWEP.DefRecoil				= 6
SWEP.CurCone				= 0.04
SWEP.DecreaseRecoilTime 	= 0
SWEP.ConeAff1 				= 0 -- Crouching/standing
SWEP.ConeAff2 				= 0 -- Using ironsights

SWEP.UnConeTime				= 0 -- Amount of time after firing the last shot that needs to pass until accuracy increases
SWEP.FinalCone				= 0 -- Self explanatory
SWEP.VelocitySensivity		= 2.2 -- Percentage of how much the cone increases depending on the player's velocity (moving speed). Rifles - 100%; SMGs - 80%; Pistols - 60%; Shotguns - 20%
SWEP.HeadbobMul 			= 1
SWEP.IsSilenced 			= false
SWEP.IronsightsCone			= 0.0001
SWEP.HipCone 				= 0.045
SWEP.InaccAff1 = 0.75
SWEP.ConeInaccuracyAff1 = 0.7
SWEP.PlayFireAnim = true

SWEP.Secondary.Automatic	= false
SWEP.Secondary.Ammo			= "none"

SWEP.IsUsingIronsights 		= false
SWEP.TargetMul = 0
SWEP.SetAndForget			= false

SWEP.IronSightsPos = Vector(5, -5.139, 2.88)
SWEP.IronSightsAng = Vector(0.87, 0, 0)

SWEP.AimPos = Vector(5, -5.139, 2.88)
SWEP.AimAng = Vector(0.87, 0, 0)

SWEP.KobraPos = Vector(5, -4.789, 2.079)
SWEP.KobraAng = Vector(-0.042, 0, 0)

SWEP.ScopePos = Vector(5.079, -4.094, 2.079)
SWEP.ScopeAng = Vector(0, 0.209, 0)

SWEP.ReflexPos = Vector(5, -3.668, 1.878)
SWEP.ReflexAng = Vector(0, 0, 0)

SWEP.ACOGPos = Vector(5, -4.932, 2.16)
SWEP.ACOGAng = Vector(-0.352, 0.065, 0)

SWEP.ACOG_BackupPos = Vector(4.96, -6.473, 1.289)
SWEP.ACOG_BackupAng = Vector(-0.076, -0.144, 0)

SWEP.ELCANPos = Vector(-2.74, -2.78, 0.33)
SWEP.ELCANAng = Vector(0, 0, 0)

SWEP.ELCAN_BackupPos = Vector(-2.74, -2.78, -0.42)
SWEP.ELCAN_BackupAng = Vector(0, 0, 0)

SWEP.BallisticPos = Vector(5, -2.98, 2.121)
SWEP.BallisticAng = Vector(0, 0, 0)

SWEP.ChargePos = Vector (5.4056, -10.3522, -4.0017)
SWEP.ChargeAng = Vector (-1.7505, -55.5187, 68.8356)

SWEP.FlipOriginsPos = Vector(-1.601, 0, -0.64)
SWEP.FlipOriginsAng = Vector(0, 0, 0)

SWEP.NoFlipOriginsPos = Vector(-1.601, 0, -0.64)
SWEP.NoFlipOriginsAng = Vector(0, 0, 0)

if CLIENT then
	function SWEP:CanUseBackUp()
		if self.VElements["acog"].color.a == 255 or self.VElements["elcan"].color.a == 255 then
			return true
		end
		
		return false
	end
	
	function SWEP:UseBackUp()
		if self.VElements["acog"].color.a == 255 then
			if self.AimPos == self.ACOG_BackupPos then
				self.AimPos = self.ACOGPos
				self.AimAng = self.ACOGAng
			else
				self.AimPos = self.ACOG_BackupPos
				self.AimAng = self.ACOG_BackupAng
			end
		elseif self.VElements["elcan"].color.a == 255 then
			if self.AimPos == self.ELCAN_BackupPos then
				self.AimPos = self.ELCANPos
				self.AimAng = self.ELCANAng
			else
				self.AimPos = self.ELCAN_BackupPos
				self.AimAng = self.ELCAN_BackupAng
			end
		end
	end
end

function SWEP:PrimaryAttack()
    if self:Clip1() > 0 then
        self:MakeExplosion(nil,0)
    end
    self.BaseClass.PrimaryAttack(self)
end

function SWEP:OnDrop()
    self:Remove()
end

function SWEP:Reload()
    if self.Owner:KeyDown(IN_ATTACK2) then
        self:BombStick()
        return
    end
    self.BaseClass.Reload( self )
end
function SWEP:BombStick()
    if SERVER then
        local ply = self.Owner
        if not IsValid(ply) then return end

        if self.Planted then return end



        local ignore = {ply, self}
        local spos = ply:GetShootPos()
        local epos = spos + ply:GetAimVector() * 80
        local tr = util.TraceLine({start=spos, endpos=epos, filter=ignore, mask=MASK_SOLID})

        if tr.HitWorld then


            local bomb = ents.Create("deathbringer_c4")
            if IsValid(bomb) then
                bomb:PointAtEntity(ply)

                local tr_ent = util.TraceEntity({start=spos, endpos=epos, filter=ignore, mask=MASK_SOLID}, bomb)

                if tr_ent.HitWorld then

                    if not self.Owner:IsTraitor() then
                        self.Owner:SetHealth(self.Owner:Health()-50)
                    end

                    local ang = tr_ent.HitNormal:Angle()
                    ang:RotateAroundAxis(ang:Right(), 90)
                    ang:RotateAroundAxis(ang:Up(), 0)

                    bomb:SetPos(tr_ent.HitPos)
                    bomb:SetAngles(ang)
                    bomb:SetOwner(ply)
                    bomb:SetThrower(ply)
                    bomb:SetModel('models/weapons/w_c4.mdl')
                    bomb:Spawn()
                    bomb:SetModel('models/weapons/w_c4.mdl')
                    bomb.fingerprints = self.fingerprints

                    local phys = bomb:GetPhysicsObject()
                    if IsValid(phys) then
                        phys:EnableMotion(false)
                    end

                    bomb.IsOnWall = true

                    --self:Remove()

                    self.Planted = true

                end
            end

            ply:SetAnimation( PLAYER_ATTACK1 )
        end
    end
end
