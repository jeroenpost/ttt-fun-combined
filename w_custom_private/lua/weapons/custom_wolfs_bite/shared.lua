if SERVER then
   AddCSLuaFile(  )
   --resource.AddFile("materials/vgui/ttt_fun_killicons/ak47.png") 
end

SWEP.HoldType = "ar2"
SWEP.PrintName = "Wolf's Bite"
SWEP.Slot = 2

if CLIENT then
   
   SWEP.Author = "GreenBlack"
   
   SWEP.Icon = "vgui/ttt_fun_killicons/ak47.png"
end
 
SWEP.Base = "weapon_tttbase"
SWEP.Spawnable = true
SWEP.AdminSpawnable = true
SWEP.AutoSpawnable = false
SWEP.Kind = WEAPON_HEAVY
SWEP.Primary.Delay = 0.12
SWEP.Primary.Recoil = 0.05
SWEP.Primary.Automatic = true
SWEP.Primary.Ammo = "Pistol"
SWEP.Primary.Damage = 16
SWEP.Primary.Cone = 0.018
SWEP.Primary.ClipSize = 120
SWEP.Primary.ClipMax = 190
SWEP.Primary.DefaultClip = 120
SWEP.AmmoEnt = "item_ammo_pistol_ttt"
SWEP.ViewModel = "models/weapons/v_rif_ak47.mdl"
SWEP.WorldModel = "models/weapons/w_rif_ak47.mdl"
SWEP.Primary.Sound = Sound( "sounds/bite.mp3" )

SWEP.HeadshotMultiplier = 2

SWEP.IronSightsPos = Vector( 6.05, -3.30, 1.80 )
SWEP.IronSightsAng = Vector( 3.28, 0, 0 )

function SWEP:SecondaryAttack( soundfile )
    self:Fly()
end
-- FLY
SWEP.flyammo = 5
SWEP.nextFly = 0
function SWEP:Fly( soundfile )
    if self.nextFly > CurTime() or CLIENT then return end
    self.nextFly = CurTime() + 0.30
    if self.flyammo < 1 then return end

    if not soundfile then
        if SERVER then self.Owner:EmitSound(Sound("weapons/ls/lightsaber_swing.mp3")) end
        if SERVER then self.Owner:EmitSound(Sound("player/suit_sprint.wav")) end
    else
        if SERVER then self.Owner:EmitSound(Sound(soundfile)) end
    end
    self.flyammo = self.flyammo - 1
    self.Owner:SetAnimation(PLAYER_ATTACK1)
    local power = 400
    if math.random(1,5) < 2 then power = 100
    elseif math.random(1,15) < 2 then power = -500
    elseif math.random(1,15) < 2 then power = 2500
    else power = math.random( 350,450 ) end

    if SERVER then self.Owner:SetVelocity(self.Owner:GetForward() * power + Vector(0, 0, power)) end
    timer.Simple(20, function() if IsValid(self) then self.flyammo = self.flyammo + 1 end end)
    --timer.Simple(0.2,self.SecondaryAttackDelay,self)
end