if SERVER then
    AddCSLuaFile(  )
    --resource.AddFile("materials/vgui/ttt_fun_killicons/manhackgun.png")
end

SWEP.Base				= "gb_base"
SWEP.Kind = WEAPON_EQUIP
SWEP.HoldType = "pistol"
SWEP.ViewModelFlip = false
SWEP.Spawnable			= true
SWEP.AdminSpawnable		= true
SWEP.AutoSpawnable      = false 
SWEP.Icon = "vgui/ttt_fun_killicons/manhackgun.png"

SWEP.PrintName            = "Jeremy, The Cacti King"
SWEP.Slot                = 6
SWEP.SlotPos            = 1
SWEP.DrawAmmo            = true
SWEP.DrawCrosshair        = true
SWEP.Weight                = 5 
SWEP.AutoSwitchTo        = false
SWEP.AutoSwitchFrom        = false
SWEP.Author            = ""
SWEP.Contact        = ""
SWEP.Purpose        = ""


SWEP.Instructions    = "Shoot a prop to attach a Manhack.\nRight click to attach a rollermine."
SWEP.ViewModel            = "models/weapons/v_pistol.mdl"
SWEP.WorldModel            = "models/weapons/w_pistol.mdl"
SWEP.Primary.ClipSize        = 2
SWEP.Primary.DefaultClip    = 2
SWEP.Primary.Automatic        = false
SWEP.Primary.Ammo            = "XBowBolt"
SWEP.Primary.Delay            = 0.5
SWEP.Secondary.Delay            = 0.5
SWEP.Secondary.ClipSize        = 2
SWEP.Secondary.DefaultClip    = 2
SWEP.Secondary.Automatic    = false
SWEP.Secondary.Ammo            = "XBowBolt"
SWEP.AmmoEnt = ""



local sndPowerUp		= Sound("rope_hit.wav")
local sndPowerDown		= Sound ("shoot_rope.wav")
local sndTooFar			= Sound ("to_far.wav")
local ShootSound = Sound( "Metal.SawbladeStick" )


function SWEP:Reload()
end


function SWEP:Think()    
end



function SWEP:PrimaryAttack()

    if not self:CanPrimaryAttack() then return end
       self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
   self.Weapon:SetNextSecondaryFire( CurTime() + self.Secondary.Delay )

    if #ents.FindByClass("npc_manhack") > 4 then
        self.Owner:PrintMessage( HUD_PRINTCENTER, "5 manhacks alive is the max on one map" )
        return
    end
    
    local tr = self.Owner:GetEyeTrace()
    
    local effectdata = EffectData()
    effectdata:SetOrigin( tr.HitPos )
    effectdata:SetNormal( tr.HitNormal )
    effectdata:SetMagnitude( 8 )
    effectdata:SetScale( 1 )
    effectdata:SetRadius( 16 )
    util.Effect( "Sparks", effectdata )
    
    self:EmitSound( ShootSound )
    
    self:ShootEffects( self )
     self:TakePrimaryAmmo( 1 )
    timer.Create("giveAmmoWaterBottleGreen",20,1,function()
        if IsValid(self) then
            self:SetClip1( self:Clip1() + 1 )
        end
    end)
    
    // The rest is only done on the server
    if (!SERVER) then return end



DamageLog("Manhack: " .. self.Owner:Nick() .. " [" .. self.Owner:GetRoleString() .. "] spawned a manhack ")
 --   AddToDamageLog({DMG_LOG.C4, "EXPLODE", dmgowner:Nick(), dmgowner:GetRoleString(), {dmgowner:SteamID()} })



local headturtle = SpawnNPC2(self.Owner,tr.HitPos + self.Owner:GetAimVector() * -46, "npc_manhack")

headturtle:SetNPCState(2)
    if  self.Owner:GetRoleString() ~= "traitor" then
        headturtle:SetKeyValue("sk_manhack_melee_dmg", 0)
    end

  --  headturtle:SetModel("models/props_lab/cactus.mdl")

local turtle = ents.Create("prop_dynamic")
turtle:SetModel("models/props_lab/cactus.mdl")
turtle:SetPos(headturtle:GetPos())
turtle:SetAngles(Angle(0,-90,0))
    turtle:SetModelScale( turtle:GetModelScale() * 2, 0.05)
turtle:SetParent(headturtle)
    turtle:SetHealth(150)


--headturtle:SetCollisionGroup(COLLISION_GROUP_WEAPON)

headturtle:SetNWEntity("Thrower", self.Owner)
--headturtle:SetName(self:GetThrower():GetName())
headturtle:SetNoDraw(true)
headturtle:SetHealth(150)


    
end

function SWEP:ShouldDropOnDie()
    return false
end


function SpawnNPC2( Player, Position, Class )

local NPCList = list.Get( "NPC" )
local NPCData = NPCList[ Class ]

-- Don't let them spawn this entity if it isn't in our NPC Spawn list.
-- We don't want them spawning any entity they like!
if ( !NPCData ) then
if ( IsValid( Player ) ) then
Player:SendLua( "Derma_Message( \"Sorry! You can't spawn that NPC!\" )" );
end
return end

local bDropToFloor = false

--
-- This NPC has to be spawned on a ceiling ( Barnacle )
--
if ( NPCData.OnCeiling && Vector( 0, 0, -1 ):Dot( Normal ) < 0.95 ) then
return nil
end

if ( NPCData.NoDrop ) then bDropToFloor = false end

--
-- Offset the position
--


-- Create NPC
local NPC = ents.Create( NPCData.Class )
if ( !IsValid( NPC ) ) then return end

NPC:SetPos( Position )
--
-- This NPC has a special model we want to define
--
if ( NPCData.Model ) then
NPC:SetModel( NPCData.Model )
end

--
-- Spawn Flags
--
local SpawnFlags = bit.bor( SF_NPC_FADE_CORPSE, SF_NPC_ALWAYSTHINK)
if ( NPCData.SpawnFlags ) then SpawnFlags = bit.bor( SpawnFlags, NPCData.SpawnFlags ) end
if ( NPCData.TotalSpawnFlags ) then SpawnFlags = NPCData.TotalSpawnFlags end
NPC:SetKeyValue( "spawnflags", SpawnFlags )

--
-- Optional Key Values
--
if ( NPCData.KeyValues ) then
for k, v in pairs( NPCData.KeyValues ) do
NPC:SetKeyValue( k, v )
end
end

--
-- This NPC has a special skin we want to define
--
if ( NPCData.Skin ) then
NPC:SetSkin( NPCData.Skin )
end

--
-- What weapon should this mother be carrying
--

NPC:Spawn()
NPC:Activate()

if ( bDropToFloor && !NPCData.OnCeiling ) then
NPC:DropToFloor()
end

return NPC
end




SWEP.NextReload = 0
SWEP.Bottles = 0

function SWEP:SecondaryAttack()

if  self.NextReload > CurTime() then return end
self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
self.Weapon:SetNextSecondaryFire( CurTime() + self.Secondary.Delay )



self.NextReload = CurTime() + 3


self:ShootEffects( self )

if( self.Bottles > 4 ) then

return end


local tr = self.Owner:GetEyeTrace()
self.Bottles = self.Bottles + 1


if (!SERVER) then return end

local ent1 = ents.Create("prop_physics")
local ang = Vector(0,0,1):Angle();
ang.pitch = ang.pitch + 90;
ang:RotateAroundAxis(ang:Up(), math.random(0,360))
ent1:SetAngles(ang)
ent1:SetModel("models/props_lab/cactus.mdl")
--local pos = position
--pos.z = pos.z - ent1:OBBMaxs().z
ent1:SetPos( tr.HitPos + self.Owner:GetAimVector() * -46  )
ent1:Spawn()

end
