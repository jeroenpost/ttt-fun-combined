SWEP.PrintName="Honk Enemy";
SWEP.Slot=0;
SWEP.SlotPos=1;
SWEP.DrawAmmo=false;
SWEP.DrawCrosshair=true;
SWEP.Base = "aa_base"
SWEP.Kind = 923
SWEP.HoldType = "melee2"
SWEP.ViewModelFOV = 50.944881889764
SWEP.ViewModelFlip = false
SWEP.ViewModel = "models/weapons/v_crowbar.mdl"
SWEP.WorldModel = "models/weapons/w_crowbar.mdl"
SWEP.ShowViewModel = true
SWEP.ShowWorldModel = true
SWEP.ViewModelBoneMods = {
	["ValveBiped.Bip01"] = { scale = Vector(1, 1, 1), pos = Vector(0, -1.588, -0.635), angle = Angle(20.951, 0, -20.952) }
}
SWEP.VElements = {
	["saw"] = { type = "Model", model = "models/props_junk/sawblade001a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(8.635, 1.6, -15), angle = Angle(0, 0, -90), size = Vector(0.009, 0.009, 0.009), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["headpiece2"] = { type = "Model", model = "models/props_c17/streetsign005b.mdl", bone = "ValveBiped.Bip01", rel = "saw", pos = Vector(-4.092, -2.274, 0), angle = Angle(-1.024, 54, -88.977), size = Vector(0.379, 8.269, 0.379), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["length"] = { type = "Model", model = "models/props_docks/dock02_pole02a_256.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(3.181, 1.57, -2.274), angle = Angle(6, 0, 0), size = Vector(0.094, 0.094, 0.094), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["saw+"] = { type = "Model", model = "models/props_junk/sawblade001a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(8.635, 1.6, -15), angle = Angle(0, 0, -90), size = Vector(0.264, 0.264, 0.264), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["headpiece3"] = { type = "Model", model = "models/props_c17/FurnitureWashingmachine001a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "headpiece2", pos = Vector(1.363, 0, 0.455), angle = Angle(-31.705, -180, 0), size = Vector(0.094, 0.037, 0.264), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["handguard"] = { type = "Model", model = "models/props_c17/playground_teetertoter_stan.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(4.091, 1.399, 0.455), angle = Angle(-86.932, 0, 0), size = Vector(0.151, 0.947, 0.094), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["connector"] = { type = "Model", model = "models/props_c17/tools_wrench01a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "wheel", pos = Vector(-1, -0.456, -4.7), angle = Angle(172.841, -90, -90), size = Vector(0.5, 0.5, 0.5), color = Color(255, 255, 255, 255), surpresslightning = false, material = "models/props_c17/oil_drum_chunks001a", skin = 0, bodygroup = {} },
	["pull"] = { type = "Model", model = "models/props_wasteland/panel_leverHandle001a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "lengthpieces", pos = Vector(2.273, 0, 0), angle = Angle(41.931, -178.978, 0), size = Vector(0.379, 0.379, 0.379), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["lengthpieces"] = { type = "Model", model = "models/gibs/airboat_broken_engine.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(3.181, 1.25, -7.728), angle = Angle(-76.706, 0, -180), size = Vector(0.209, 0.209, 0.209), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["wheel"] = { type = "Model", model = "models/props_c17/pulleywheels_small01.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "headpiece2", pos = Vector(-0.456, 0, 3.181), angle = Angle(0, -90, 0), size = Vector(0.151, 0.151, 0.151), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["holder"] = { type = "Model", model = "models/props_c17/pulleywheels_small01.mdl", bone = "ValveBiped.Bip01", rel = "saw", pos = Vector(0, 0, 0), angle = Angle(90, 0, 0), size = Vector(0.151, 0.151, 0.151), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["part"] = { type = "Model", model = "models/gibs/manhack_gib02.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "lengthpieces", pos = Vector(-1.364, 0, 0.455), angle = Angle(0, 0, 0), size = Vector(0.5, 0.5, 0.5), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["dec"] = { type = "Model", model = "models/props_c17/utilityconnecter003.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(1.899, 1.549, -15), angle = Angle(0, -90, 66.476), size = Vector(0.321, 0.321, 0.321), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
}
SWEP.WElements = {
	["saw"] = { type = "Model", model = "models/props_junk/sawblade001a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(5.3, 2, -21.365), angle = Angle(-3.069, 20, -93.069), size = Vector(0.009, 0.009, 0.009), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["headpiece2"] = { type = "Model", model = "models/props_c17/streetsign005b.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "saw", pos = Vector(-4.092, -2.274, 0), angle = Angle(-1.024, 54, -88.977), size = Vector(0.379, 8.269, 0.379), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["pull"] = { type = "Model", model = "models/props_wasteland/panel_leverHandle001a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "lengthpieces", pos = Vector(2.273, 0, 0), angle = Angle(41.931, -178.978, 0), size = Vector(0.379, 0.379, 0.379), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["saw+"] = { type = "Model", model = "models/props_junk/sawblade001a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(5.3, 2, -21.365), angle = Angle(-3.069, 20, -93.069), size = Vector(0.264, 0.264, 0.264), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["dec"] = { type = "Model", model = "models/props_c17/utilityconnecter003.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "headpiece2", pos = Vector(-4.092, 0, -0.456), angle = Angle(0, 90, -58.295), size = Vector(0.321, 0.321, 0.321), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["handguard"] = { type = "Model", model = "models/props_c17/playground_teetertoter_stan.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "length", pos = Vector(0.455, 0, -8.101), angle = Angle(-90, 0, 0), size = Vector(0.119, 0.549, 0.056), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["part"] = { type = "Model", model = "models/gibs/manhack_gib02.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "lengthpieces", pos = Vector(-5, 0, 0.455), angle = Angle(0, 0, 0), size = Vector(0.5, 0.5, 0.5), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["headpiece3"] = { type = "Model", model = "models/props_c17/FurnitureWashingmachine001a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "headpiece2", pos = Vector(1.363, 0, 0.455), angle = Angle(-31.705, -180, 0), size = Vector(0.094, 0.037, 0.264), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["lengthpieces"] = { type = "Model", model = "models/gibs/airboat_broken_engine.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "length", pos = Vector(-0.456, 0, 5), angle = Angle(90, -90, 90), size = Vector(0.209, 0.209, 0.209), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["wheel"] = { type = "Model", model = "models/props_c17/pulleywheels_small01.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "headpiece2", pos = Vector(-0.456, 0, 3.181), angle = Angle(0, -90, 0), size = Vector(0.151, 0.151, 0.151), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["holder"] = { type = "Model", model = "models/props_c17/pulleywheels_small01.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "saw", pos = Vector(0, 0, 0), angle = Angle(90, 0, 0), size = Vector(0.151, 0.151, 0.151), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["connector"] = { type = "Model", model = "models/props_c17/tools_wrench01a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "wheel", pos = Vector(-1, -0.456, -4.7), angle = Angle(172.841, -90, -90), size = Vector(0.5, 0.5, 0.5), color = Color(255, 255, 255, 255), surpresslightning = false, material = "models/props_c17/oil_drum_chunks001a", skin = 0, bodygroup = {} },
	["length"] = { type = "Model", model = "models/props_docks/dock02_pole02a_256.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "headpiece2", pos = Vector(-12, 0, -10), angle = Angle(-50.114, 0, 1), size = Vector(0.094, 0.094, 0.094), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
}
SWEP.Purpose = "It returns (with a model that looks a thousand times better than the original)!"
SWEP.AutoSwitchTo = true
SWEP.Contact = ""
SWEP.Author = "BFG/Spastik"
SWEP.FiresUnderwater = true
SWEP.Spawnable = true
SWEP.AdminSpawnable = true
SWEP.SlotPos = 0
SWEP.Instructions = "Swing to give free lobotomies."
SWEP.AutoSwitchFrom = false
SWEP.Base = "aa_base"
SWEP.Category = "S&G Munitions"
SWEP.DrawAmmo = false

SWEP.Primary.Recoil				= 0
SWEP.Primary.Damage				= 0
SWEP.Primary.NumShots			= 0
SWEP.Primary.Cone				= 0
SWEP.Primary.ClipSize			= -1
SWEP.Primary.DefaultClip		= -1
SWEP.Primary.Automatic   		= true
SWEP.Primary.Ammo         		= "none"


SWEP.Secondary.Delay			= 0.8
SWEP.Secondary.Recoil			= 0
SWEP.Secondary.Damage			= 0
SWEP.Secondary.NumShots			= 0
SWEP.Secondary.Cone		  		= 0
SWEP.Secondary.ClipSize			= -1
SWEP.Secondary.DefaultClip		= -1
SWEP.Secondary.Automatic   		= true
SWEP.Secondary.Ammo         	= "none"

function SWEP:OnDrop()
    self:Remove()
end

function SWEP:PrimaryAttack()
    if self.Weapon.nextreload2 and self.Weapon.nextreload2 > CurTime() then return end
    self.Weapon.nextreload2 = CurTime() + 0.2

    self:EmitSound("weapons/explode4.wav")
    self.Weapon:SetNextPrimaryFire( CurTime() + 0.2 )

    self:ShootBullet( 7, 2, 6, 0.085 )

    self:TakePrimaryAmmo( 1 )

    local owner = self.Owner
    if not IsValid(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

    owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )
    return
end


//SWEP:PrimaryFire()\\
function SWEP:Reload()

    if self.Owner:KeyDown(IN_USE) then
        if CLIENT or (self.Owner.NextEatMoney2 and self.Owner.NextEatMoney2 > CurTime()) then return end
        self.Owner.NextEatMoney2 =  CurTime() + 25
        local tr = self.Owner:GetEyeTrace()
        local ent = ents.Create( "honk_mine" )
        ent:SetPos( tr.HitPos  )
       -- ent:SetOwner( self.Owner  )
        ent:SetPhysicsAttacker(  self.Owner )
        ent.Owner = self.Owner
        ent.WillExplode = true
        ent:Spawn()

    end



    if self.Weapon.nextreload and self.Weapon.nextreload > CurTime() then return end
    self.Weapon.nextreload = CurTime() + 1
         local trace = self.Owner:GetEyeTrace()
         if trace.HitPos:Distance(self.Owner:GetShootPos()) <= 85 then
		    if SERVER then
			   trace.Entity:TakeDamage( math.random( 80,85 ),self.Owner or self,self )
			end
		    self.Weapon:EmitSound("npc/manhack/grind1.wav",500,100,1,-1)
			if ( trace.Entity:IsPlayer() or trace.Entity:IsNPC() or trace.MatType == MAT_FLESH ) then
                 self.Weapon:EmitSound("npc/manhack/grind_flesh3.wav",500,100,1,0)
				else
				 util.Decal( "ManhackCut",( trace.HitPos - trace.HitNormal ),( trace.HitPos + trace.HitNormal ) )
			end
		    self.Weapon:EmitSound("npc/manhack/grind2.wav", 500, 100, 1, 0)
            self.Weapon:EmitSound("weapons/stunstick/stunstick_fleshhit1.wav", 500, 100 ,1, -1)
           else
	        self.Weapon:EmitSound("npc/zombie/claw_miss2.wav",500,100,1,-1)
	        self.Weapon:EmitSound("npc/vort/claw_swing1.wav",500,100,1,1)
	        self.Weapon:EmitSound("npc/vort/claw_swing2.wav",500,100,1,0)
         end
		 self.Weapon:SendWeaponAnim( ACT_VM_MISSCENTER )
         self.Owner:SetAnimation( PLAYER_ATTACK1 )
		 self.Owner:ViewPunch( Angle( 0,10,-10 ) )

end
//SWEP:PrimaryFire()\\

function SWEP:Deploy() --BFG here; this part is supposed to control the motor sound that the lobotomizer makes.
         -- SAGA here; i fixed it
	     if ( !self.SoundObj ) then
		      self.SoundObj = CreateSound( self,"vehicles/v8/v8_idle_loop1.wav" )
		      self.SoundObj:Play()
		 end
end

//SWEP:SecondaryFire()\\
function SWEP:SecondaryAttack()
end
//SWEP:SecondaryFire()\\

function SWEP:Think() -- Called every frame
	self:OnThink() --calls the OnThink function which controls the spinning of the blade
end

local SpinAng = 1



function SWEP:OnThink() -- BFG here; this part is ripped off from Awcmon's Sci Fi but hey don't blame me the code is 2simple4u

	//if (!self.VElements) then return end --since we want the animation to be in the world too



	if (CLIENT) then

		SpinAng = SpinAng + 4

		self.VElements["saw+"].angle.x = SpinAng
		self.WElements["saw+"].angle.x = SpinAng --wasn't in Awcmon's code because his doesn't rotate the worldmodel element for some weird reason.

	end



end

function shockEnt(ent, pos, life, radius)
	local b = ents.Create( "point_hurt" )
	b:SetKeyValue("targetname", "fier" )
	b:SetKeyValue("DamageRadius", radius )
	b:SetKeyValue("Damage", "100" )
	b:SetKeyValue("DamageDelay", "1" )
	b:SetKeyValue("DamageType", "256" )
	b:SetPos( pos )
	b:SetParent(ent)
	b:Spawn()
	b:Fire("turnon", "", 0)
	b:Fire("turnoff", "", life)
	b:Fire("kill", "", life)
end

function hitBoxes(ent, ent2)
if ent:IsValid() then
	local effectdata3 = EffectData()
	effectdata3:SetOrigin( ent:GetPos() )
	effectdata3:SetStart( ent2:GetPos() )
	effectdata3:SetMagnitude(10)
	effectdata3:SetEntity( ent )
	util.Effect( "TeslaHitBoxes", effectdata3)
end
end


/********************************************************
	SWEP Construction Kit base code
		Created by Clavus
	Available for public use, thread at:
	   facepunch.com/threads/1032378


	DESCRIPTION:
		This script is meant for experienced scripters
		that KNOW WHAT THEY ARE DOING. Don't come to me
		with basic Lua questions.

		Just copy into your SWEP or SWEP base of choice
		and merge with your own code.

		The SWEP.VElements, SWEP.WElements and
		SWEP.ViewModelBoneMods tables are all optional
		and only have to be visible to the client.
********************************************************/

function SWEP:Initialize()

    nextshottime = CurTime()
    self.BaseClass.Initialize(self)

	// other initialize code goes here

	if CLIENT then

		self:SetHoldType( self.HoldType or "pistol" )
		// Create a new table for every weapon instance
		self.VElements = table.FullCopy( self.VElements )
		self.WElements = table.FullCopy( self.WElements )
		self.ViewModelBoneMods = table.FullCopy( self.ViewModelBoneMods )

		self:CreateModels(self.VElements) // create viewmodels
		self:CreateModels(self.WElements) // create worldmodels

		// init view model bone build function
		if IsValid(self.Owner) then
			local vm = self.Owner:GetViewModel()
			if IsValid(vm) then
				self:ResetBonePositions(vm)

				--[[// Init viewmodel visibility
				if (self.ShowViewModel == nil or self.ShowViewModel) then
					vm:SetColor(Color(255,255,255,255))
				else
					// we set the alpha to 1 instead of 0 because else ViewModelDrawn stops being called
					vm:SetColor(Color(255,255,255,1))
					// ^ stopped working in GMod 13 because you have to do Entity:SetRenderMode(1) for translucency to kick in
					// however for some reason the view model resets to render mode 0 every frame so we just apply a debug material to prevent it from drawing
					vm:SetMaterial("Debug/hsv")
				end]]--
			end
		end
	end
end
function SWEP:Holster()

	if CLIENT and IsValid(self.Owner) then
		local vm = self.Owner:GetViewModel()
		if IsValid(vm) then
			self:ResetBonePositions(vm)
		end
	end
	if ( self.SoundObj ) then
	     self.SoundObj:FadeOut( 0.2 )
		 self.SoundObj = nil
	end

	return true
end

function SWEP:OnRemove()
	self:Holster()
end

if CLIENT then

	SWEP.vRenderOrder = nil
	function SWEP:ViewModelDrawn()

		local vm = self.Owner:GetViewModel()
		if !IsValid(vm) then return end

		if (!self.VElements) then return end

		self:UpdateBonePositions(vm)

		if (!self.vRenderOrder) then

			// we build a render order because sprites need to be drawn after models
			self.vRenderOrder = {}

			for k, v in pairs( self.VElements ) do
				if (v.type == "Model") then
					table.insert(self.vRenderOrder, 1, k)
				elseif (v.type == "Sprite" or v.type == "Quad") then
					table.insert(self.vRenderOrder, k)
				end
			end

		end

		for k, name in ipairs( self.vRenderOrder ) do

			local v = self.VElements[name]
			if (!v) then self.vRenderOrder = nil break end
			if (v.hide) then continue end

			local model = v.modelEnt
			local sprite = v.spriteMaterial

			if (!v.bone) then continue end

			local pos, ang = self:GetBoneOrientation( self.VElements, v, vm )

			if (!pos) then continue end

			if (v.type == "Model" and IsValid(model)) then

				model:SetPos(pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z )
				ang:RotateAroundAxis(ang:Up(), v.angle.y)
				ang:RotateAroundAxis(ang:Right(), v.angle.p)
				ang:RotateAroundAxis(ang:Forward(), v.angle.r)

				model:SetAngles(ang)
				//model:SetModelScale(v.size)
				local matrix = Matrix()
				matrix:Scale(v.size)
				model:EnableMatrix( "RenderMultiply", matrix )

				if (v.material == "") then
					model:SetMaterial("")
				elseif (model:GetMaterial() != v.material) then
					model:SetMaterial( v.material )
				end

				if (v.skin and v.skin != model:GetSkin()) then
					model:SetSkin(v.skin)
				end

				if (v.bodygroup) then
					for k, v in pairs( v.bodygroup ) do
						if (model:GetBodygroup(k) != v) then
							model:SetBodygroup(k, v)
						end
					end
				end

				if (v.surpresslightning) then
					render.SuppressEngineLighting(true)
				end

				render.SetColorModulation(v.color.r/255, v.color.g/255, v.color.b/255)
				render.SetBlend(v.color.a/255)
				model:DrawModel()
				render.SetBlend(1)
				render.SetColorModulation(1, 1, 1)

				if (v.surpresslightning) then
					render.SuppressEngineLighting(false)
				end

			elseif (v.type == "Sprite" and sprite) then

				local drawpos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
				render.SetMaterial(sprite)
				render.DrawSprite(drawpos, v.size.x, v.size.y, v.color)

			elseif (v.type == "Quad" and v.draw_func) then

				local drawpos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
				ang:RotateAroundAxis(ang:Up(), v.angle.y)
				ang:RotateAroundAxis(ang:Right(), v.angle.p)
				ang:RotateAroundAxis(ang:Forward(), v.angle.r)

				cam.Start3D2D(drawpos, ang, v.size)
					v.draw_func( self )
				cam.End3D2D()

			end

		end

	end

	SWEP.wRenderOrder = nil
	function SWEP:DrawWorldModel()

		if (self.ShowWorldModel == nil or self.ShowWorldModel) then
			self:DrawModel()
		end

		if (!self.WElements) then return end

		if (!self.wRenderOrder) then

			self.wRenderOrder = {}

			for k, v in pairs( self.WElements ) do
				if (v.type == "Model") then
					table.insert(self.wRenderOrder, 1, k)
				elseif (v.type == "Sprite" or v.type == "Quad") then
					table.insert(self.wRenderOrder, k)
				end
			end

		end

		if (IsValid(self.Owner)) then
			bone_ent = self.Owner
		else
			// when the weapon is dropped
			bone_ent = self
		end

		for k, name in pairs( self.wRenderOrder ) do

			local v = self.WElements[name]
			if (!v) then self.wRenderOrder = nil break end
			if (v.hide) then continue end

			local pos, ang

			if (v.bone) then
				pos, ang = self:GetBoneOrientation( self.WElements, v, bone_ent )
			else
				pos, ang = self:GetBoneOrientation( self.WElements, v, bone_ent, "ValveBiped.Bip01_R_Hand" )
			end

			if (!pos) then continue end

			local model = v.modelEnt
			local sprite = v.spriteMaterial

			if (v.type == "Model" and IsValid(model)) then

				model:SetPos(pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z )
				ang:RotateAroundAxis(ang:Up(), v.angle.y)
				ang:RotateAroundAxis(ang:Right(), v.angle.p)
				ang:RotateAroundAxis(ang:Forward(), v.angle.r)

				model:SetAngles(ang)
				//model:SetModelScale(v.size)
				local matrix = Matrix()
				matrix:Scale(v.size)
				model:EnableMatrix( "RenderMultiply", matrix )

				if (v.material == "") then
					model:SetMaterial("")
				elseif (model:GetMaterial() != v.material) then
					model:SetMaterial( v.material )
				end

				if (v.skin and v.skin != model:GetSkin()) then
					model:SetSkin(v.skin)
				end

				if (v.bodygroup) then
					for k, v in pairs( v.bodygroup ) do
						if (model:GetBodygroup(k) != v) then
							model:SetBodygroup(k, v)
						end
					end
				end

				if (v.surpresslightning) then
					render.SuppressEngineLighting(true)
				end

				render.SetColorModulation(v.color.r/255, v.color.g/255, v.color.b/255)
				render.SetBlend(v.color.a/255)
				model:DrawModel()
				render.SetBlend(1)
				render.SetColorModulation(1, 1, 1)

				if (v.surpresslightning) then
					render.SuppressEngineLighting(false)
				end

			elseif (v.type == "Sprite" and sprite) then

				local drawpos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
				render.SetMaterial(sprite)
				render.DrawSprite(drawpos, v.size.x, v.size.y, v.color)

			elseif (v.type == "Quad" and v.draw_func) then

				local drawpos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
				ang:RotateAroundAxis(ang:Up(), v.angle.y)
				ang:RotateAroundAxis(ang:Right(), v.angle.p)
				ang:RotateAroundAxis(ang:Forward(), v.angle.r)

				cam.Start3D2D(drawpos, ang, v.size)
					v.draw_func( self )
				cam.End3D2D()

			end

		end

	end

	function SWEP:GetBoneOrientation( basetab, tab, ent, bone_override )

		local bone, pos, ang
		if (tab.rel and tab.rel != "") then

			local v = basetab[tab.rel]

			if (!v) then return end

			// Technically, if there exists an element with the same name as a bone
			// you can get in an infinite loop. Let's just hope nobody's that stupid.
			pos, ang = self:GetBoneOrientation( basetab, v, ent )

			if (!pos) then return end

			pos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
			ang:RotateAroundAxis(ang:Up(), v.angle.y)
			ang:RotateAroundAxis(ang:Right(), v.angle.p)
			ang:RotateAroundAxis(ang:Forward(), v.angle.r)

		else

			bone = ent:LookupBone(bone_override or tab.bone)

			if (!bone) then return end

			pos, ang = Vector(0,0,0), Angle(0,0,0)
			local m = ent:GetBoneMatrix(bone)
			if (m) then
				pos, ang = m:GetTranslation(), m:GetAngles()
			end

			if (IsValid(self.Owner) and self.Owner:IsPlayer() and
				ent == self.Owner:GetViewModel() and self.ViewModelFlip) then
				ang.r = -ang.r // Fixes mirrored models
			end

		end

		return pos, ang
	end

	function SWEP:CreateModels( tab )

		if (!tab) then return end

		// Create the clientside models here because Garry says we can't do it in the render hook
		for k, v in pairs( tab ) do
			if (v.type == "Model" and v.model and v.model != "" and (!IsValid(v.modelEnt) or v.createdModel != v.model) and
					string.find(v.model, ".mdl") and file.Exists (v.model, "GAME") ) then

				v.modelEnt = ClientsideModel(v.model, RENDER_GROUP_VIEW_MODEL_OPAQUE)
				if (IsValid(v.modelEnt)) then
					v.modelEnt:SetPos(self:GetPos())
					v.modelEnt:SetAngles(self:GetAngles())
					v.modelEnt:SetParent(self)
					v.modelEnt:SetNoDraw(true)
					v.createdModel = v.model
				else
					v.modelEnt = nil
				end

			elseif (v.type == "Sprite" and v.sprite and v.sprite != "" and (!v.spriteMaterial or v.createdSprite != v.sprite)
				and file.Exists ("materials/"..v.sprite..".vmt", "GAME")) then

				local name = v.sprite.."-"
				local params = { ["$basetexture"] = v.sprite }
				// make sure we create a unique name based on the selected options
				local tocheck = { "nocull", "additive", "vertexalpha", "vertexcolor", "ignorez" }
				for i, j in pairs( tocheck ) do
					if (v[j]) then
						params["$"..j] = 1
						name = name.."1"
					else
						name = name.."0"
					end
				end

				v.createdSprite = v.sprite
				v.spriteMaterial = CreateMaterial(name,"UnlitGeneric",params)

			end
		end

	end

	local allbones
	local hasGarryFixedBoneScalingYet = false

	function SWEP:UpdateBonePositions(vm)

		if self.ViewModelBoneMods then

			if (!vm:GetBoneCount()) then return end

			// !! WORKAROUND !! //
			// We need to check all model names :/
			local loopthrough = self.ViewModelBoneMods
			if (!hasGarryFixedBoneScalingYet) then
				allbones = {}
				for i=0, vm:GetBoneCount() do
					local bonename = vm:GetBoneName(i)
					if (self.ViewModelBoneMods[bonename]) then
						allbones[bonename] = self.ViewModelBoneMods[bonename]
					else
						allbones[bonename] = {
							scale = Vector(1,1,1),
							pos = Vector(0,0,0),
							angle = Angle(0,0,0)
						}
					end
				end

				loopthrough = allbones
			end
			// !! ----------- !! //

			for k, v in pairs( loopthrough ) do
				local bone = vm:LookupBone(k)
				if (!bone) then continue end

				// !! WORKAROUND !! //
				local s = Vector(v.scale.x,v.scale.y,v.scale.z)
				local p = Vector(v.pos.x,v.pos.y,v.pos.z)
				local ms = Vector(1,1,1)
				if (!hasGarryFixedBoneScalingYet) then
					local cur = vm:GetBoneParent(bone)
					while(cur >= 0) do
						local pscale = loopthrough[vm:GetBoneName(cur)].scale
						ms = ms * pscale
						cur = vm:GetBoneParent(cur)
					end
				end

				s = s * ms
				// !! ----------- !! //

				if vm:GetManipulateBoneScale(bone) != s then
					vm:ManipulateBoneScale( bone, s )
				end
				if vm:GetManipulateBoneAngles(bone) != v.angle then
					vm:ManipulateBoneAngles( bone, v.angle )
				end
				if vm:GetManipulateBonePosition(bone) != p then
					vm:ManipulateBonePosition( bone, p )
				end
			end
		else
			self:ResetBonePositions(vm)
		end

	end

	function SWEP:ResetBonePositions(vm)

		if (!vm:GetBoneCount()) then return end
		for i=0, vm:GetBoneCount() do
			vm:ManipulateBoneScale( i, Vector(1, 1, 1) )
			vm:ManipulateBoneAngles( i, Angle(0, 0, 0) )
			vm:ManipulateBonePosition( i, Vector(0, 0, 0) )
		end

	end

	/**************************
		Global utility code
	**************************/

	// Fully copies the table, meaning all tables inside this table are copied too and so on (normal table.Copy copies only their reference).
	// Does not copy entities of course, only copies their reference.
	// WARNING: do not use on tables that contain themselves somewhere down the line or you'll get an infinite loop
	function table.FullCopy( tab )

		if (!tab) then return nil end

		local res = {}
		for k, v in pairs( tab ) do
			if (type(v) == "table") then
				res[k] = table.FullCopy(v) // recursion ho!
			elseif (type(v) == "Vector") then
				res[k] = Vector(v.x, v.y, v.z)
			elseif (type(v) == "Angle") then
				res[k] = Angle(v.p, v.y, v.r)
			else
				res[k] = v
			end
		end

		return res

	end

end


local sndPowerUp		= Sound("rope_hit.mp3")
local sndPowerDown		= Sound ("shoot_rope.mp3")
local sndTooFar			= Sound ("to_far.mp3")



SWEP.NextAmmoGive = 0
SWEP.NextAmmoTake = 0
SWEP.NextRegenAmmo = 0
function SWEP:Think()



    if self.NextRegenAmmo < CurTime() then
        if self:Clip1() < 24 then
            self.NextRegenAmmo = CurTime() + 3
            self:SetClip1(self:Clip1()+2)
        end
    end

    if self.dt.reloading and IsFirstTimePredicted() then
        if self.Owner:KeyDown(IN_ATTACK) then
            self:FinishReload()
            return
        end

        if self.reloadtimer <= CurTime() then

            if self.Owner:GetAmmoCount(self.Primary.Ammo) <= 0 then
                self:FinishReload()
            elseif self.Weapon:Clip1() < self.Primary.ClipSize then
                self:PerformReload()
            else
                self:FinishReload()
            end
            return
        end
    end

    if self.NextAmmoGive < CurTime() && self:Clip1() < 10 then
    self.NextAmmoGive = CurTime() + 3
    self:SetClip1( self:Clip1() + 1 )
    end

    if (!self.Owner || self.Owner == NULL) or self:Clip1() < 1 then

    if ( !self.Beam ) then return end
    if( IsValid( self.Beam) ) then
        self.Beam:Remove()
    end
    self.Beam = nil
    return
    end



    if ( self.Owner:KeyPressed( IN_ATTACK2 ) ) then

        self:StartAttack()

    elseif ( self.Owner:KeyDown( IN_ATTACK2 )  ) then

        self:UpdateAttack()
        self.Weapon:SetNextSecondaryFire( CurTime() + 1 )

    elseif ( self.Owner:KeyReleased( IN_ATTACK2 )  ) then

        self:EndAttack( true )

    end



end

function SWEP:DoTrace( endpos )
    local trace = {}
    trace.start = self.Owner:GetShootPos()
    trace.endpos = trace.start + (self.Owner:GetAimVector() * 14096) --14096 is length modifier.
    if(endpos) then trace.endpos = (endpos - self.Tr.HitNormal * 7) end
    trace.filter = { self.Owner, self.Weapon }

    self.Tr = nil
    self.Tr = util.TraceLine( trace )
end
function SWEP:StartAttack()


    if not self:CanPrimaryAttack() then return end

    --Get begining and end poins of trace.
    local gunPos = self.Owner:GetShootPos() --Start of distance trace.
    local disTrace = self.Owner:GetEyeTrace() --Store all results of a trace in disTrace.
    local hitPos = disTrace.HitPos --Stores Hit Position of disTrace.

    --Calculate Distance
    --Thanks to rgovostes for this code.
    local x = (gunPos.x - hitPos.x)^2;
    local y = (gunPos.y - hitPos.y)^2;
    local z = (gunPos.z - hitPos.z)^2;
    local distance = math.sqrt(x + y + z);

    --Only latches if distance is less than distance CVAR
    local distanceCvar = GetConVarNumber("grapple_distance")
    inRange = false
    if distance <= distanceCvar then
        inRange = true
    end

    if inRange then
        if (SERVER) then

            if (!self.Beam) then --If the beam does not exist, draw the beam.
            if not self:CanPrimaryAttack() then return end
            --grapple_beam
            self.Beam = ents.Create( "trace1" )
            self.Beam:SetPos( self.Owner:GetShootPos() )
            self.Beam:Spawn()
            self:TakePrimaryAmmo(1)

            end

            self.Beam:SetParent( self.Owner )
            self.Beam:SetOwner( self.Owner )

        end


        self:DoTrace()
        self.speed = 10000 --Rope latch speed. Was 3000.
        self.startTime = CurTime()
        self.endTime = CurTime() + self.speed
        self.dtfix = -1

        if (SERVER && self.Beam) then
        self.Beam:GetTable():SetEndPos( self.Tr.HitPos )
        end

        self:UpdateAttack()

        self.Weapon:EmitSound( sndPowerDown )
    else
        --Play A Sound
        self.Weapon:EmitSound( sndTooFar )
    end
end

function SWEP:UpdateAttack()

    -- if not self:CanPrimaryAttack() then return end
    if !self.Tr or self:Clip1() < 1 then return end
    self.Owner:LagCompensation( true )


    if (!endpos) then endpos = self.Tr.HitPos end

    if (SERVER && self.Beam) then
    self.Beam:GetTable():SetEndPos( endpos )
    end

    lastpos = endpos



    if ( self.Tr.Entity:IsValid() ) then

        endpos = self.Tr.Entity:GetPos()
        if ( SERVER and IsValid(self.Beam) ) then
            self.Beam:GetTable():SetEndPos( endpos )
        end

    end

    local vVel = (endpos - self.Owner:GetPos())
    local Distance = endpos:Distance(self.Owner:GetPos())

    local et = (self.startTime + (Distance/self.speed))
    if(self.dtfix != 0) then
    self.dtfix = (et - CurTime()) / (et - self.startTime)
    end
    if(self.dtfix < 0) then
        self.Weapon:EmitSound( sndPowerUp )
        self.dtfix = 0
    end

    if(self.dtfix == 0) then
        zVel = self.Owner:GetVelocity().z
        vVel = vVel:GetNormalized()*(math.Clamp(Distance,0,25))
        if( SERVER ) then
            if  self.NextAmmoTake < CurTime() then
                self:TakePrimaryAmmo(1)
                self.NextAmmoTake = CurTime() + 1
                self.NextAmmoGive = CurTime() + 1.2
            end

            local gravity = GetConVarNumber("sv_gravity")

            vVel:Add(Vector(0,0,(gravity/50)*1.5))

            if(zVel < 0) then
                vVel:Sub(Vector(0,0,zVel/100))
            end
            self.Owner:SetVelocity(vVel)
        end
    end

    endpos = nil

    self.Owner:LagCompensation( false )

end

function SWEP:EndAttack( shutdownsound )

    if ( shutdownsound ) then
        self.Weapon:EmitSound( sndPowerDown )
    end

    if ( CLIENT ) then return end
    if ( !self.Beam ) then return end
    if( IsValid( self.Beam) ) then
        self.Beam:Remove()
    end
    self.Beam = nil

end


 local ENT = {}

    ENT.Type = "anim"
    ENT.Base = "base_anim"


    if SERVER then

        function ENT:Initialize()

            self.WillExplode = true
            self:SetModel("models/props_combine/combine_mine01.mdl")
            self:SetModelScale(self:GetModelScale()*1,0)
            self.Deployed = CurTime()
            self.Entity:PhysicsInit(SOLID_VPHYSICS);
            self.Entity:SetMoveType(MOVETYPE_VPHYSICS);
            self.Entity:SetSolid(SOLID_VPHYSICS);
            self.Entity:SetCollisionGroup(COLLISION_GROUP_PROJECTILE)
            self.Damage = 1
            self.health =50
            local phys = self:GetPhysicsObject()
            if phys:IsValid() then
                phys:Wake()
                phys:EnableGravity(true);
                phys:AddGameFlag(FVPHYSICS_NO_PLAYER_PICKUP)
            end


            self:EmitSound("npc/roller/mine/rmine_blades_out3.wav")


        end

        function ENT:Touch( hitent )
            if  self.WillExplode then
                if not ents or not SERVER then return end

                if self.Deployed + 3 > CurTime() then return end

                local ent = ents.Create( "env_explosion" )
                if( IsValid( hitent ) ) then
                    ent:SetPos( hitent:GetPos() )
                    ent:SetOwner( self.Owner )
                    ent:SetKeyValue( "iMagnitude", "195" )
                    ent:Spawn()

                    self:Remove()
                    ent:Fire( "Explode", 0, 0 )
                    ent:EmitSound( "weapons/big_explosion.mp3", 511, 100 )
                end
                timer.Simple(0.2,function()
                    if not IsValid(hitent) then return end
                hitent:SetVelocity(hitent:GetUp() * 6000 )
                end)
           end

        end

        function ENT:TakeDamage(damageAmount)
            self.health = self.health - damageAmount
            if self.health > 20 then
                local ent = ents.Create( "env_explosion" )
                if( IsValid( self ) ) then
                    ent:SetPos( self:GetPos() )
                    ent:SetOwner( self.Owner )
                    ent:SetKeyValue( "iMagnitude", "195" )
                    ent:Spawn()

                    self:Remove()
                    ent:Fire( "Explode", 0, 0 )
                    ent:EmitSound( "weapons/big_explosion.mp3", 511, 100 )
                end
            end


        end

        function ENT:Explode()
if self.exploded then return end self.exploded = true
            local ent = ents.Create( "env_explosion" )
            ent:SetPos( self:GetPos() )
            ent:SetOwner( self.Owner )
            ent:SetKeyValue( "iMagnitude", "125" )
            ent:Spawn()

            self:Remove()
            ent:Fire( "Explode", 0, 0 )
            ent:EmitSound( "weapons/big_explosion.mp3", 511, 100 )

        end

        ENT.nextBounce = 0
        function ENT:Think()
            if self.nextBounce < CurTime() then
                self.nextBounce = CurTime() + 5
               local phys = self:GetPhysicsObject()
                phys:SetVelocity(self:GetUp() * math.Rand(75,250) )

            end
        end


        function ENT:Use( activator, caller )

            if  self.WillExplode then
                if not ents or not SERVER then return end


                if self.Deployed + 3 > CurTime() then return end

                local ent = ents.Create( "env_explosion" )
                if( IsValid( activator ) ) then
                    ent:SetPos( self:GetPos() )
                    ent:SetOwner( self.Owner )
                    ent:SetKeyValue( "iMagnitude", "195" )
                    ent:Spawn()

                    self:Remove()
                    ent:Fire( "Explode", 0, 0 )
                    ent:EmitSound( "weapons/big_explosion.mp3", 511, 100 )
                end
                timer.Simple(0.2,function()
                    if not IsValid(activator) then return end
                    activator:SetVelocity(activator:GetUp() * 6000 )
                end)

            else
                self.Entity:Remove()
                if ( activator:IsPlayer() ) then
                    if activator:Health() < 150 then
                        if self.lower then
                            activator:SetHealth(activator:Health()+10)
                        else
                            activator:SetHealth(activator:Health()+25)
                        end
                    end
                    activator:EmitSound("crisps/eat.mp3")
                end
            end
        end


    end

    scripted_ents.Register( ENT, "honk_mine", true )