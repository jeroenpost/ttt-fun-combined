//General Variables\\
SWEP.AdminSpawnable = true
SWEP.ViewModelFOV = 64

SWEP.VElements = {
	["detail"] = { type = "Model", model = "models/props_wasteland/panel_leverHandle001a.mdl", bone = "Crossbow_model.Crossbow_base", rel = "back", pos = Vector(0, 3.22, 3.22), angle = Angle(90, 0, -90), size = Vector(0.5, 0.28, 0.899), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["scopetop"] = { type = "Model", model = "models/props_lab/cactus.mdl", bone = "Crossbow_model.Crossbow_base", rel = "scope2", pos = Vector(1.889, 0, 3.22), angle = Angle(90, 0, 180), size = Vector(0.178, 0.178, 0.178), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["break"] = { type = "Model", model = "models/props_lab/kennel_physics.mdl", bone = "Crossbow_model.Crossbow_base", rel = "barrel", pos = Vector(-1.601, 0, 23.624), angle = Angle(-90, 0, 0), size = Vector(0.078, 0.078, 0.078), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["stock"] = { type = "Model", model = "models/props_wasteland/panel_leverHandle001a.mdl", bone = "Crossbow_model.Crossbow_base", rel = "body", pos = Vector(0, 11.612, -3), angle = Angle(90, 0, 90), size = Vector(0.5, 0.349, 1.541), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["clip"] = { type = "Model", model = "models/Items/BoxMRounds.mdl", bone = "Crossbow_model.Crossbow_base", rel = "body", pos = Vector(0, -2.701, 1.7), angle = Angle(0, 0, 180), size = Vector(0.245, 0.245, 0.245), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["scopeadjuster"] = { type = "Model", model = "models/props_vehicles/carparts_tire01a.mdl", bone = "Crossbow_model.Crossbow_base", rel = "scope", pos = Vector(0, 0, -1.701), angle = Angle(0, 0, -90), size = Vector(0.064, 0.064, 0.064), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["scope"] = { type = "Model", model = "models/props_lab/jar01a.mdl", bone = "Crossbow_model.Crossbow_base", rel = "", pos = Vector(0, -4.551, -3.5), angle = Angle(0, 180, 0), size = Vector(0.23, 0.23, 0.23), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["wheelcover"] = { type = "Model", model = "models/props_lab/reciever_cart.mdl", bone = "Crossbow_model.bowl_wheel", rel = "body", pos = Vector(14, -4, -2.5), angle = Angle(90, 0, -90), size = Vector(0.07, 0.07, 0.07), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["back"] = { type = "Model", model = "models/props_lab/citizenradio.mdl", bone = "Crossbow_model.Crossbow_base", rel = "body", pos = Vector(0, 9, -0.401), angle = Angle(0, 0, -180), size = Vector(0.111, 0.3, 0.239), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["wheelcover+"] = { type = "Model", model = "models/props_lab/reciever_cart.mdl", bone = "Crossbow_model.bowl_wheel", rel = "body", pos = Vector(-14, -4, -2.5), angle = Angle(-90, 0, -90), size = Vector(0.07, 0.07, 0.07), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["rails"] = { type = "Model", model = "models/props_wasteland/prison_heater001a.mdl", bone = "Crossbow_model.Crossbow_base", rel = "back", pos = Vector(0, 0, 3.45), angle = Angle(90, 0, 0), size = Vector(0.1, 0.259, 0.043), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["body"] = { type = "Model", model = "models/props_combine/combine_train02b.mdl", bone = "Crossbow_model.Crossbow_base", rel = "", pos = Vector(0, 2, 7.517), angle = Angle(0, 0, 90), size = Vector(0.023, 0.046, 0.029), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["scope2"] = { type = "Model", model = "models/props_wasteland/laundry_basket001.mdl", bone = "Crossbow_model.Crossbow_base", rel = "scope", pos = Vector(0, 0, 6), angle = Angle(180, 90, 0), size = Vector(0.043, 0.043, 0.239), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["barrel"] = { type = "Model", model = "models/props_lab/pipesystem02b.mdl", bone = "Crossbow_model.Crossbow_base", rel = "body", pos = Vector(0, -15.035, -3.221), angle = Angle(0, 0, -90), size = Vector(0.99, 0.99, 0.99), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
}

SWEP.WElements = {
	["detail"] = { type = "Model", model = "models/props_wasteland/panel_leverHandle001a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "back", pos = Vector(0, 3.22, 3.22), angle = Angle(90, 0, -90), size = Vector(0.5, 0.28, 0.899), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["scopetop"] = { type = "Model", model = "models/props_lab/cactus.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "scope", pos = Vector(1.889, 0, 3.22), angle = Angle(90, 0, 180), size = Vector(0.178, 0.178, 0.178), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["break"] = { type = "Model", model = "models/props_lab/kennel_physics.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "barrel", pos = Vector(-1.601, 0, 14.194), angle = Angle(-90, 0, 0), size = Vector(0.078, 0.078, 0.078), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["stock"] = { type = "Model", model = "models/props_wasteland/panel_leverHandle001a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "body", pos = Vector(0, 14.194, -3), angle = Angle(90, 0, 90), size = Vector(0.735, 0.349, 0.899), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["clip"] = { type = "Model", model = "models/Items/BoxMRounds.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "body", pos = Vector(0, -2.701, 1.7), angle = Angle(0, 0, 180), size = Vector(0.245, 0.245, 0.245), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["scopeadjuster"] = { type = "Model", model = "models/props_vehicles/carparts_tire01a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "scope", pos = Vector(0, 0, -1.701), angle = Angle(0, 0, -90), size = Vector(0.064, 0.064, 0.064), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["scope"] = { type = "Model", model = "models/props_lab/jar01a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "back", pos = Vector(-0.201, -2, 6), angle = Angle(90, 0, 90), size = Vector(0.23, 0.23, 0.23), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["barrel"] = { type = "Model", model = "models/props_lab/pipesystem02b.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "body", pos = Vector(0, -11.613, -3.221), angle = Angle(0, 0, -90), size = Vector(0.99, 0.99, 0.6), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["wheelcover"] = { type = "Model", model = "models/props_lab/reciever_cart.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "body", pos = Vector(14, -4, -2.5), angle = Angle(90, 0, -90), size = Vector(0.07, 0.07, 0.07), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["wheelcover+"] = { type = "Model", model = "models/props_lab/reciever_cart.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "body", pos = Vector(-14, -4, -2.5), angle = Angle(-90, 0, -90), size = Vector(0.07, 0.07, 0.07), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["rails"] = { type = "Model", model = "models/props_wasteland/prison_heater001a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "back", pos = Vector(0, 0, 3.45), angle = Angle(90, 0, 0), size = Vector(0.1, 0.259, 0.043), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["body"] = { type = "Model", model = "models/props_combine/combine_train02b.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(12, -0, -0.52), angle = Angle(0, -80, 0), size = Vector(0.023, 0.046, 0.029), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["scope2"] = { type = "Model", model = "models/props_wasteland/laundry_basket001.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "scope", pos = Vector(0, 0, 6), angle = Angle(180, 90, 0), size = Vector(0.043, 0.043, 0.239), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["back"] = { type = "Model", model = "models/props_lab/citizenradio.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "body", pos = Vector(0, 9, -0.401), angle = Angle(0, 0, -180), size = Vector(0.111, 0.3, 0.239), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
}

SWEP.AutoSwitchTo = false
SWEP.Slot = 1
SWEP.PrintName = "Magic's Laughing Gas"
SWEP.Author = "Spastik"
SWEP.Spawnable = true
SWEP.AutoSwitchFrom = false
SWEP.FiresUnderwater = false
SWEP.Weight = 5
SWEP.DrawCrosshair = true
SWEP.Category = "S&G Munitions"
SWEP.SlotPos = 3
SWEP.DrawAmmo = true
SWEP.Instructions = "Pull the trigger to pray."
SWEP.Contact = ""
SWEP.Purpose = "For when you need a miracle in the form of a .338 Lapua round from the heavens."
SWEP.Base = "aa_base"
SWEP.HoldType = "crossbow"
SWEP.ViewModelFOV = 71.649484536082
SWEP.ViewModelFlip = false
SWEP.ViewModel = "models/weapons/v_crossbow.mdl"
SWEP.WorldModel = "models/weapons/w_crossbow.mdl"
SWEP.ShowViewModel = true
SWEP.ShowWorldModel = true
SWEP.ViewModelBoneMods = {
	["Crossbow_model.Base"] = { scale = Vector(1, 1, 1), pos = Vector(-3.27, 0, 0), angle = Angle(0, 0, 0) },
	["Crossbow_model.bolt"] = { scale = Vector(0.009, 0.009, 0.009), pos = Vector(0, 0, 0), angle = Angle(0, 0, 0) }
}
SWEP.Kind = WEAPON_HEAVY
//SWEP.TracerFreq			= 0
SWEP.TracerType                 	= "AR2Tracer"

//General Variables\\

//Primary Fire Variables\\

SWEP.Primary.Damage = 80
SWEP.Primary.TakeAmmo = 1
SWEP.Primary.ClipSize = 99
SWEP.Primary.Ammo = "xbowbolt"
SWEP.Primary.DefaultClip = 99
SWEP.Primary.Spread = 0.04
SWEP.Primary.Cone = 0.0001
SWEP.Primary.NumberofShots = 1
SWEP.Primary.Automatic = true
SWEP.Primary.Recoil = 5
SWEP.Primary.Delay = 1
SWEP.Primary.Force = 0.4
SWEP.Primary.NumShots = 1

        SWEP.Primary.Sound = "Weapon_USP.SilencedShot"
//Primary Fire Variables\\

//Secondary Fire Variables\\
SWEP.Secondary.Ammo = "none"

Zoom=0
//Secondary Fire Variables\\

//SWEP:Initialize()\\
	function SWEP:Initialize()
    self:SetHoldType("crossbow")
    self:SetNWString("shootmode","Crossbow")
    end

SWEP.nextpurple = 0
SWEP.nextgreen = 0
SWEP.nextyellow = 0
SWEP.nextblack = 0
SWEP.IsHolding = false
SWEP.LastHold = 0
SWEP.Sound = false

SWEP.LaserColor = Color(255,0,0,255)
function SWEP:SuperShot()

    if SERVER then
        local tr = self.Owner:GetEyeTrace()
        local ent = ents.Create("env_explosion")

        ent:SetPos(tr.HitPos)
        ent:SetOwner(self.Owner)
        ent:SetPhysicsAttacker(self.Owner)
        ent:Spawn()
        ent:SetKeyValue("iMagnitude", "80")
        ent:Fire("Explode", 0, 0)

        util.BlastDamage(self, self:GetOwner(), self:GetPos(), 40, 75)

        ent:EmitSound("weapons/big_explosion.mp3")
    end
    self:FireBolt()
    self.LaserColor = Color(255,0,0,255)
    self:ShootTracer( "LaserTracer_thick")
end

SWEP.nextPrimary =0
function SWEP:Think()
    local mode = self:GetNWString("shootmode")
    if mode == "Charged Crossbow" then
        if self.Owner:KeyDown(IN_ATTACK) then

            if not self.IsHolding then
                self.LastHold = CurTime()
                self.IsHolding = true
                self.Sound = CreateSound( self,"weapons/gauss/chargeloop.wav");
                self.Sound:Play();
                self.Sound:ChangePitch(250, 4.5)
            end

            if self.IsHolding and self.LastHold + 5 < CurTime() then
                self:SuperShot()
                self.IsHolding = false
            end
        elseif self.IsHolding then
            self.IsHolding = false
            self.Sound:Stop();
            self.Sound = false
            if self.nextPrimary < CurTime() then
                self.nextPrimary = CurTime() + 0.9
                self.BaseClass.PrimaryAttack(self)
                if self:Clip1() > 0 then
                self.LaserColor = Color(0,255,0,255)
                self:ShootTracer( "LaserTracer_thick")
                end
            end
        end
    end

    if self.Owner:KeyDown(IN_USE) and self.Owner.remotemagics then
        local magics = self.Owner.remotemagics
        self.Owner.remotemagics = {}
        for k,v in pairs(magics) do
            if IsValid(v) and v.Explode then
                v:Explode()
            end
        end
    end
    self.BaseClass.Think(self)
end
SWEP.hideblind = true
function SWEP:PrimaryAttack()


    local tr = self.Owner:GetEyeTrace();
    local mode = self:GetNWString("shootmode")
    if mode == "Charged Crossbow" then

        self.Weapon:SetNextPrimaryFire( CurTime() + 0.9 )
    end
    if mode == "Beam" then
        self.BaseClass.PrimaryAttack(self)
        self.Weapon:SetNextPrimaryFire( CurTime() + 0.9 )

    end
    if mode == "Yellow Beam" then
        if self.nextyellow > CurTime() then
                self.Owner:PrintMessage( HUD_PRINTCENTER,  math.floor(self.nextyellow - CurTime()).." seconds left" )
            return
        end
        self.LaserColor = Color(255,255,0,255)
        self:ShootTracer("LaserTracer_thick")
        self.nextyellow = CurTime() + 0.08

        if CLIENT then return end
        --self.BaseClass.PrimaryAttack(self)
        self.Weapon:SetNextPrimaryFire( CurTime() + 0.9 )
        if IsValid(tr.Entity) and tr.Entity:IsPlayer() then
            tr.Entity:AnimPerformGesture_gb(ACT_GMOD_TAUNT_LAUGH);
            tr.Entity:EmitSound("weapons/gb_weapons/laughtergun1.mp3")
            tr.Entity:TakeDamage(11, self.Owner or self)
            timer.Create("TakeHealthLaughGun2"..tr.Entity:UniqueID(),3,3, function()
                if IsValid(tr.Entity) then
                 --   tr.Entity:TakeDamage(5, self.Owner or self)
                    tr.Entity:AnimPerformGesture_gb(ACT_GMOD_TAUNT_LAUGH);
                end
            end)
        end
        self.Weapon:SetNextPrimaryFire( CurTime() + 0.09 )
    end
    if mode == "Pink Gas" then
        if self.nextpurple > CurTime() then
            self.Owner:PrintMessage( HUD_PRINTCENTER,  math.floor(self.nextpurple - CurTime()).." seconds left" )
            return
        end
        self.LaserColor = Color(255,0,255,255)
        self:ShootTracer("LaserTracer_thick")
        self.nextpurple = CurTime() + 60
        if CLIENT then return end
        local ent = ents.Create( "magics_pink_gas" )
        ent:SetPos( tr.HitPos  )
        ent:SetOwner( self.Owner  )
        ent:SetPhysicsAttacker(  self.Owner )
        ent.Owner = self.Owner
        ent:Spawn()
        ent.timer = CurTime() + 0.01
        ent.EndColor = "0 0 0"
        ent.weapon = self
        ent.StartColor = "255 0 255"
        timer.Simple(5,function()
            if IsValid(ent) then
                ent.Bastardgas:Remove()
                ent.Entity:Remove()
            end
        end)
    end
    if mode == "Green Gas" then
        if self.nextgreen > CurTime() then
            self.Owner:PrintMessage( HUD_PRINTCENTER,  math.floor(self.nextgreen - CurTime()).." seconds left" )
            return
        end
        self.LaserColor = Color(0,255,0,255)
        self:ShootTracer("LaserTracer_thick")
        self.nextgreen = CurTime() + 60
        if CLIENT then return end
        local ent = ents.Create( "magics_green_gas" )
        ent:SetPos( tr.HitPos  )
        ent:SetOwner( self.Owner  )
        ent:SetPhysicsAttacker(  self.Owner )
        ent.Owner = self.Owner
        ent:Spawn()
        ent.timer = CurTime() + 0.01
        ent.EndColor = "0 0 0"
        ent.weapon = self
        ent.StartColor = "0 255 0"
        timer.Simple(5,function()
            if IsValid(ent) then
                ent.Bastardgas:Remove()
                ent.Entity:Remove()
            end
        end)
    end

    if mode == "Black Gas" then
        if self.nextblack > CurTime() then
            self.Owner:PrintMessage( HUD_PRINTCENTER, "Already used this round" )
            return
        end
        self.LaserColor = Color(20,20,20,255)
        self:ShootTracer("LaserTracer_thick")
        self.nextblack = CurTime() + 6000
        if CLIENT then return end
        local ent = ents.Create( "magics_black_gas" )
        ent:SetPos( tr.HitPos  )
        ent:SetOwner( self.Owner  )
        ent:SetPhysicsAttacker(  self.Owner )
        ent.Owner = self.Owner
        ent:Spawn()
        ent.timer = CurTime() + 0.01
        ent.EndColor = "0 0 0"
        ent.weapon = self
        ent.StartColor = "20 20 20"
        timer.Simple(5,function()
            if IsValid(ent) then
                ent.Bastardgas:Remove()
                ent.Entity:Remove()
            end
        end)
    end
end


//SWEP:Initialize()\\

function SWEP:OnDrop()
    self:Remove()
end

function SWEP:Deploy()
    --self:SetHoldType("crossbow")
    self:SetNWString("shootmode","Charged Crossbow")
end

function SWEP:SecondaryAttack()
 	self:Zoom()
end

SWEP.NextReload = 0
function SWEP:Reload()
         if ( self.NextReload > CurTime() ) then return end
        self.NextReload = CurTime() + 0.3

         if self.Owner:KeyDown(IN_USE)  then
             self.NextReload = CurTime() + 0.3
             local mode = self:GetNWString("shootmode")
             self:SetIronsights(false)
             self:SetZoom(false)
             if mode == "Pink Gas" then  self:SetNWString("shootmode","Green Gas") self.Primary.Sound = "weapons/shotgun/shotgun_dbl_fire.wav" end
             if mode == "Green Gas" then  self:SetNWString("shootmode","Yellow Beam") self.Primary.Sound = "weapons/crossbow/fire1.wav" end
             if mode == "Yellow Beam" then  self:SetNWString("shootmode","Black Gas") self.Primary.Sound = "weapons/crossbow/fire1.wav" end
             if mode == "Black Gas" then  self:SetNWString("shootmode","Charged Crossbow") self.Primary.Sound = "weapons/crossbow/fire1.wav" end
             if mode == "Charged Crossbow" then  self:SetNWString("shootmode","Pink Gas") self.Primary.Sound = "weapons/crossbow/fire1.wav" end
             self.Weapon:SetNextPrimaryFire( CurTime() + 0.8 )
             return
         end

end


function SWEP:Initialize()

	// other initialize code goes here

	if CLIENT then

		self:SetHoldType( self.HoldType or "pistol" )
		// Create a new table for every weapon instance
		self.VElements = table.FullCopy( self.VElements )
		self.WElements = table.FullCopy( self.WElements )
		self.ViewModelBoneMods = table.FullCopy( self.ViewModelBoneMods )

		self:CreateModels(self.VElements) // create viewmodels
		self:CreateModels(self.WElements) // create worldmodels

		// init view model bone build function
		if IsValid(self.Owner) then
			local vm = self.Owner:GetViewModel()
			if IsValid(vm) then
				self:ResetBonePositions(vm)

				--[[// Init viewmodel visibility
				if (self.ShowViewModel == nil or self.ShowViewModel) then
					vm:SetColor(Color(255,255,255,255))
				else
					// we set the alpha to 1 instead of 0 because else ViewModelDrawn stops being called
					vm:SetColor(Color(255,255,255,1))
					// ^ stopped working in GMod 13 because you have to do Entity:SetRenderMode(1) for translucency to kick in
					// however for some reason the view model resets to render mode 0 every frame so we just apply a debug material to prevent it from drawing
					vm:SetMaterial("Debug/hsv")
				end]]--
			end
		end
	end
end
function SWEP:Holster()

	if CLIENT and IsValid(self.Owner) then
		local vm = self.Owner:GetViewModel()
		if IsValid(vm) then
			self:ResetBonePositions(vm)
		end
	end

	return true
end

function SWEP:OnRemove()
	self:Holster()
end

if CLIENT then

	SWEP.vRenderOrder = nil
	function SWEP:ViewModelDrawn()

		local vm = self.Owner:GetViewModel()
		if !IsValid(vm) then return end

		if (!self.VElements) then return end

		self:UpdateBonePositions(vm)

		if (!self.vRenderOrder) then

			// we build a render order because sprites need to be drawn after models
			self.vRenderOrder = {}

			for k, v in pairs( self.VElements ) do
				if (v.type == "Model") then
					table.insert(self.vRenderOrder, 1, k)
				elseif (v.type == "Sprite" or v.type == "Quad") then
					table.insert(self.vRenderOrder, k)
				end
			end

		end

		for k, name in ipairs( self.vRenderOrder ) do

			local v = self.VElements[name]
			if (!v) then self.vRenderOrder = nil break end
			if (v.hide) then continue end

			local model = v.modelEnt
			local sprite = v.spriteMaterial

			if (!v.bone) then continue end

			local pos, ang = self:GetBoneOrientation( self.VElements, v, vm )

			if (!pos) then continue end

			if (v.type == "Model" and IsValid(model)) then

				model:SetPos(pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z )
				ang:RotateAroundAxis(ang:Up(), v.angle.y)
				ang:RotateAroundAxis(ang:Right(), v.angle.p)
				ang:RotateAroundAxis(ang:Forward(), v.angle.r)

				model:SetAngles(ang)
				//model:SetModelScale(v.size)
				local matrix = Matrix()
				matrix:Scale(v.size)
				model:EnableMatrix( "RenderMultiply", matrix )

				if (v.material == "") then
					model:SetMaterial("")
				elseif (model:GetMaterial() != v.material) then
					model:SetMaterial( v.material )
				end

				if (v.skin and v.skin != model:GetSkin()) then
					model:SetSkin(v.skin)
				end

				if (v.bodygroup) then
					for k, v in pairs( v.bodygroup ) do
						if (model:GetBodygroup(k) != v) then
							model:SetBodygroup(k, v)
						end
					end
				end

				if (v.surpresslightning) then
					render.SuppressEngineLighting(true)
				end

				render.SetColorModulation(v.color.r/255, v.color.g/255, v.color.b/255)
				render.SetBlend(v.color.a/255)
				model:DrawModel()
				render.SetBlend(1)
				render.SetColorModulation(1, 1, 1)

				if (v.surpresslightning) then
					render.SuppressEngineLighting(false)
				end

			elseif (v.type == "Sprite" and sprite) then

				local drawpos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
				render.SetMaterial(sprite)
				render.DrawSprite(drawpos, v.size.x, v.size.y, v.color)

			elseif (v.type == "Quad" and v.draw_func) then

				local drawpos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
				ang:RotateAroundAxis(ang:Up(), v.angle.y)
				ang:RotateAroundAxis(ang:Right(), v.angle.p)
				ang:RotateAroundAxis(ang:Forward(), v.angle.r)

				cam.Start3D2D(drawpos, ang, v.size)
					v.draw_func( self )
				cam.End3D2D()

			end

		end

	end

	SWEP.wRenderOrder = nil
	function SWEP:DrawWorldModel()

		if (self.ShowWorldModel == nil or self.ShowWorldModel) then
			self:DrawModel()
		end

		if (!self.WElements) then return end

		if (!self.wRenderOrder) then

			self.wRenderOrder = {}

			for k, v in pairs( self.WElements ) do
				if (v.type == "Model") then
					table.insert(self.wRenderOrder, 1, k)
				elseif (v.type == "Sprite" or v.type == "Quad") then
					table.insert(self.wRenderOrder, k)
				end
			end

		end

		if (IsValid(self.Owner)) then
			bone_ent = self.Owner
		else
			// when the weapon is dropped
			bone_ent = self
		end

		for k, name in pairs( self.wRenderOrder ) do

			local v = self.WElements[name]
			if (!v) then self.wRenderOrder = nil break end
			if (v.hide) then continue end

			local pos, ang

			if (v.bone) then
				pos, ang = self:GetBoneOrientation( self.WElements, v, bone_ent )
			else
				pos, ang = self:GetBoneOrientation( self.WElements, v, bone_ent, "ValveBiped.Bip01_R_Hand" )
			end

			if (!pos) then continue end

			local model = v.modelEnt
			local sprite = v.spriteMaterial

			if (v.type == "Model" and IsValid(model)) then

				model:SetPos(pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z )
				ang:RotateAroundAxis(ang:Up(), v.angle.y)
				ang:RotateAroundAxis(ang:Right(), v.angle.p)
				ang:RotateAroundAxis(ang:Forward(), v.angle.r)

				model:SetAngles(ang)
				//model:SetModelScale(v.size)
				local matrix = Matrix()
				matrix:Scale(v.size)
				model:EnableMatrix( "RenderMultiply", matrix )

				if (v.material == "") then
					model:SetMaterial("")
				elseif (model:GetMaterial() != v.material) then
					model:SetMaterial( v.material )
				end

				if (v.skin and v.skin != model:GetSkin()) then
					model:SetSkin(v.skin)
				end

				if (v.bodygroup) then
					for k, v in pairs( v.bodygroup ) do
						if (model:GetBodygroup(k) != v) then
							model:SetBodygroup(k, v)
						end
					end
				end

				if (v.surpresslightning) then
					render.SuppressEngineLighting(true)
				end

				render.SetColorModulation(v.color.r/255, v.color.g/255, v.color.b/255)
				render.SetBlend(v.color.a/255)
				model:DrawModel()
				render.SetBlend(1)
				render.SetColorModulation(1, 1, 1)

				if (v.surpresslightning) then
					render.SuppressEngineLighting(false)
				end

			elseif (v.type == "Sprite" and sprite) then

				local drawpos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
				render.SetMaterial(sprite)
				render.DrawSprite(drawpos, v.size.x, v.size.y, v.color)

			elseif (v.type == "Quad" and v.draw_func) then

				local drawpos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
				ang:RotateAroundAxis(ang:Up(), v.angle.y)
				ang:RotateAroundAxis(ang:Right(), v.angle.p)
				ang:RotateAroundAxis(ang:Forward(), v.angle.r)

				cam.Start3D2D(drawpos, ang, v.size)
					v.draw_func( self )
				cam.End3D2D()

			end

		end

	end

	function SWEP:GetBoneOrientation( basetab, tab, ent, bone_override )

		local bone, pos, ang
		if (tab.rel and tab.rel != "") then

			local v = basetab[tab.rel]

			if (!v) then return end

			// Technically, if there exists an element with the same name as a bone
			// you can get in an infinite loop. Let's just hope nobody's that stupid.
			pos, ang = self:GetBoneOrientation( basetab, v, ent )

			if (!pos) then return end

			pos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
			ang:RotateAroundAxis(ang:Up(), v.angle.y)
			ang:RotateAroundAxis(ang:Right(), v.angle.p)
			ang:RotateAroundAxis(ang:Forward(), v.angle.r)

		else

			bone = ent:LookupBone(bone_override or tab.bone)

			if (!bone) then return end

			pos, ang = Vector(0,0,0), Angle(0,0,0)
			local m = ent:GetBoneMatrix(bone)
			if (m) then
				pos, ang = m:GetTranslation(), m:GetAngles()
			end

			if (IsValid(self.Owner) and self.Owner:IsPlayer() and
				ent == self.Owner:GetViewModel() and self.ViewModelFlip) then
				ang.r = -ang.r // Fixes mirrored models
			end

		end

		return pos, ang
	end

	function SWEP:CreateModels( tab )

		if (!tab) then return end

		// Create the clientside models here because Garry says we can't do it in the render hook
		for k, v in pairs( tab ) do
			if (v.type == "Model" and v.model and v.model != "" and (!IsValid(v.modelEnt) or v.createdModel != v.model) and
					string.find(v.model, ".mdl") and file.Exists (v.model, "GAME") ) then

				v.modelEnt = ClientsideModel(v.model, RENDER_GROUP_VIEW_MODEL_OPAQUE)
				if (IsValid(v.modelEnt)) then
					v.modelEnt:SetPos(self:GetPos())
					v.modelEnt:SetAngles(self:GetAngles())
					v.modelEnt:SetParent(self)
					v.modelEnt:SetNoDraw(true)
					v.createdModel = v.model
				else
					v.modelEnt = nil
				end

			elseif (v.type == "Sprite" and v.sprite and v.sprite != "" and (!v.spriteMaterial or v.createdSprite != v.sprite)
				and file.Exists ("materials/"..v.sprite..".vmt", "GAME")) then

				local name = v.sprite.."-"
				local params = { ["$basetexture"] = v.sprite }
				// make sure we create a unique name based on the selected options
				local tocheck = { "nocull", "additive", "vertexalpha", "vertexcolor", "ignorez" }
				for i, j in pairs( tocheck ) do
					if (v[j]) then
						params["$"..j] = 1
						name = name.."1"
					else
						name = name.."0"
					end
				end

				v.createdSprite = v.sprite
				v.spriteMaterial = CreateMaterial(name,"UnlitGeneric",params)

			end
		end

	end

	local allbones
	local hasGarryFixedBoneScalingYet = false

	function SWEP:UpdateBonePositions(vm)

		if self.ViewModelBoneMods then

			if (!vm:GetBoneCount()) then return end

			// !! WORKAROUND !! //
			// We need to check all model names :/
			local loopthrough = self.ViewModelBoneMods
			if (!hasGarryFixedBoneScalingYet) then
				allbones = {}
				for i=0, vm:GetBoneCount() do
					local bonename = vm:GetBoneName(i)
					if (self.ViewModelBoneMods[bonename]) then
						allbones[bonename] = self.ViewModelBoneMods[bonename]
					else
						allbones[bonename] = {
							scale = Vector(1,1,1),
							pos = Vector(0,0,0),
							angle = Angle(0,0,0)
						}
					end
				end

				loopthrough = allbones
			end
			// !! ----------- !! //

			for k, v in pairs( loopthrough ) do
				local bone = vm:LookupBone(k)
				if (!bone) then continue end

				// !! WORKAROUND !! //
				local s = Vector(v.scale.x,v.scale.y,v.scale.z)
				local p = Vector(v.pos.x,v.pos.y,v.pos.z)
				local ms = Vector(1,1,1)
				if (!hasGarryFixedBoneScalingYet) then
					local cur = vm:GetBoneParent(bone)
					while(cur >= 0) do
						local pscale = loopthrough[vm:GetBoneName(cur)].scale
						ms = ms * pscale
						cur = vm:GetBoneParent(cur)
					end
				end

				s = s * ms
				// !! ----------- !! //

				if vm:GetManipulateBoneScale(bone) != s then
					vm:ManipulateBoneScale( bone, s )
				end
				if vm:GetManipulateBoneAngles(bone) != v.angle then
					vm:ManipulateBoneAngles( bone, v.angle )
				end
				if vm:GetManipulateBonePosition(bone) != p then
					vm:ManipulateBonePosition( bone, p )
				end
			end
		else
			self:ResetBonePositions(vm)
		end

	end

	function SWEP:ResetBonePositions(vm)

		if (!vm:GetBoneCount()) then return end
		for i=0, vm:GetBoneCount() do
			vm:ManipulateBoneScale( i, Vector(1, 1, 1) )
			vm:ManipulateBoneAngles( i, Angle(0, 0, 0) )
			vm:ManipulateBonePosition( i, Vector(0, 0, 0) )
		end

	end

	/**************************
		Global utility code
	**************************/

	// Fully copies the table, meaning all tables inside this table are copied too and so on (normal table.Copy copies only their reference).
	// Does not copy entities of course, only copies their reference.
	// WARNING: do not use on tables that contain themselves somewhere down the line or you'll get an infinite loop
	function table.FullCopy( tab )

		if (!tab) then return nil end

		local res = {}
		for k, v in pairs( tab ) do
			if (type(v) == "table") then
				res[k] = table.FullCopy(v) // recursion ho!
			elseif (type(v) == "Vector") then
				res[k] = Vector(v.x, v.y, v.z)
			elseif (type(v) == "Angle") then
				res[k] = Angle(v.p, v.y, v.r)
			else
				res[k] = v
			end
		end

		return res

	end

end

SWEP.IronSightsPos      = Vector( -100, -100, -100 )
SWEP.IronSightsAng      = Vector( 2.6, 1.37, 3.5 )
SWEP.Secondary.Sound = Sound("Default.Zoom")
-- Add some zoom to ironsights for this gun
function SWEP:Zoom()
    if not self.IronSightsPos then return end
    if self.Weapon:GetNextSecondaryFire() > CurTime() then return end

    local bIronsights = not self:GetIronsights()

    self:SetIronsights( bIronsights )

    if SERVER then
        self:SetZoom(bIronsights)
    else
        self:EmitSound(self.Secondary.Sound)
    end

    self.Weapon:SetNextSecondaryFire( CurTime() + 0.3)
end




function SWEP:SetZoom(state)
    if CLIENT then
        return
    elseif IsValid(self.Owner) and self.Owner:IsPlayer() then
        if state then
            self.Owner:SetFOV(30, 0.3)
        else
            self.Owner:SetFOV(0, 0.2)
        end
    end
end

if CLIENT then
    local scope = surface.GetTextureID("sprites/scope")
    function SWEP:DrawHUD()

    local shotttext = "Shootmode: "..self:GetNWString("shootmode").."\nE+R to change"
    surface.SetFont( "ChatFont" );
    local size_x, size_y = surface.GetTextSize( shotttext );

    draw.RoundedBox( 6, ScrW( ) / 2 - size_x / 2 - 5, ScrH( ) - 100, size_x + 15, size_y + 10, Color( 0, 0, 0, 150 ) );
    draw.DrawText( shotttext , "ChatFont", ScrW( ) / 2, ScrH( ) - 100 + 5, Color( 255, 30, 30, 255 ), TEXT_ALIGN_CENTER );


        if self:GetIronsights() then
            surface.SetDrawColor( 0, 0, 0, 255 )

            local x = ScrW() / 2.0
            local y = ScrH() / 2.0
            local scope_size = ScrH()

            -- crosshair
            local gap = 80
            local length = scope_size
            surface.DrawLine( x - length, y, x - gap, y )
            surface.DrawLine( x + length, y, x + gap, y )
            surface.DrawLine( x, y - length, x, y - gap )
            surface.DrawLine( x, y + length, x, y + gap )

            gap = 0
            length = 50
            surface.DrawLine( x - length, y, x - gap, y )
            surface.DrawLine( x + length, y, x + gap, y )
            surface.DrawLine( x, y - length, x, y - gap )
            surface.DrawLine( x, y + length, x, y + gap )


            -- cover edges
            local sh = scope_size / 2
            local w = (x - sh) + 2
            surface.DrawRect(0, 0, w, scope_size)
            surface.DrawRect(x + sh - 2, 0, w, scope_size)

            surface.SetDrawColor(255, 0, 0, 255)
            surface.DrawLine(x, y, x + 1, y + 1)

            -- scope
            surface.SetTexture(scope)
            surface.SetDrawColor(255, 255, 255, 255)

            surface.DrawTexturedRectRotated(x, y, scope_size, scope_size, 0)

        else
            return self.BaseClass.DrawHUD(self)
        end
    end

    function SWEP:AdjustMouseSensitivity()
        return (self:GetIronsights() and 0.2) or nil
    end
end


-- Laser zap lightning grenade
local ENT = {}

ENT.Type = "anim"
ENT.Base = "cse_ent_shortgasgrenade"

function ENT:Poison(ply,dmg,owner)
    if (ply:Health() >= 0) then
        ply:TakeDamage(dmg or 5, owner or self)
    elseif (ply:Alive()) and (ply:Health() <= 0)then
        ply:Kill()
    else
    end
end

function ENT:Think()

    if not SERVER then return end
    if not self.timer then self.timer = CurTime() + 5 end
    if self.timer < CurTime() then
        if !IsValid(self.Bastardgas) && !self.Spammed then
        self.Spammed = true
        self.Bastardgas = ents.Create("env_smoketrail")
        self.Bastardgas:SetPos(self.Entity:GetPos())
        self.Bastardgas:SetKeyValue("spawnradius","256")
        self.Bastardgas:SetKeyValue("minspeed","0.5")
        self.Bastardgas:SetKeyValue("maxspeed","2")
        self.Bastardgas:SetKeyValue("startsize","16536")
        self.Bastardgas:SetKeyValue("endsize","256")
        self.Bastardgas:SetKeyValue("endcolor",self.EndColor)
        self.Bastardgas:SetKeyValue("startcolor",self.StartColor)
        self.Bastardgas:SetKeyValue("opacity","4")
        self.Bastardgas:SetKeyValue("spawnrate","15")
        self.Bastardgas:SetKeyValue("lifetime","10")
        self.Bastardgas:SetParent(self.Entity)
        self.Bastardgas:Spawn()
        self.Bastardgas:Activate()
        self.Bastardgas:Fire("turnon","", 0.1)
        local exp = ents.Create("env_explosion")
        exp:SetKeyValue("spawnflags",461)
        exp:SetPos(self.Entity:GetPos())
        exp:Spawn()
        exp:Fire("explode","",0)
        self.Entity:EmitSound(Sound("BaseSmokeEffect.Sound"))
        end

        local pos = self.Entity:GetPos()
        local maxrange = 256
        local maxstun = 10
        for k,v in pairs(player.GetAll()) do
            if v.GasProtection then continue end
            local plpos = v:GetPos()
            local dist = -pos:Distance(plpos)+maxrange
            if (pos:Distance(plpos)<=maxrange) and not v:IsGhost() then
                local trace = {}
                trace.start = self.Entity:GetPos()
                trace.endpos = v:GetPos()+Vector(0,0,24)
                trace.filter = { v, self.Entity }
                trace.mask = COLLISION_GROUP_PLAYER
                tr = util.TraceLine(trace)
                if (tr.Fraction==1) then
                    local stunamount = math.ceil(dist/(maxrange/maxstun))
                  --  v:ViewPunch( Angle( stunamount*((math.random()*6)-2), stunamount*((math.random()*6)-2), stunamount*((math.random()*4)-1) ) )
                    self:Poison(v, 10, self.Owner, self.Owner:GetWeapon("weapon_gasgrenade"))
                    if IsValid(self.weapon) and self.weapon.Freeze then
                                        self.weapon:Freeze(25,3,v)
                    end
                    v:SetVelocity(v:GetUp() * 100 + Vector(0, 0, 300))
                    v:EmitSound("weapons/big_explosion.mp3")
                end
            end
            end
            if (self.timer+60<CurTime()) then
                if IsValid(self.Bastardgas) then
                    self.Bastardgas:Remove()
                end
            end
            if (self.timer+65<CurTime()) then
                self.Entity:Remove()
            end
            self.Entity:NextThink(CurTime()+0.7)
            return true
        end
    end


    scripted_ents.Register( ENT, "magics_pink_gas", true )


-- Laser zap lightning grenade
local ENT = {}

ENT.Type = "anim"
ENT.Base = "cse_ent_shortgasgrenade"

function ENT:Poison(ply,dmg,owner)
    if (ply:Health() >= 0) then
        ply:TakeDamage(dmg or 7, owner or self)
    elseif (ply:Alive()) and (ply:Health() <= 0)then
        ply:Kill()
    else
    end
end

function ENT:Think()
    if not SERVER then return end
    if not self.timer then self.timer = CurTime() + 5 end
    if self.timer < CurTime() then
        if !IsValid(self.Bastardgas) && !self.Spammed then
        self.Spammed = true
        self.Bastardgas = ents.Create("env_smoketrail")
        self.Bastardgas:SetPos(self.Entity:GetPos())
        self.Bastardgas:SetKeyValue("spawnradius","256")
        self.Bastardgas:SetKeyValue("minspeed","0.5")
        self.Bastardgas:SetKeyValue("maxspeed","2")
        self.Bastardgas:SetKeyValue("startsize","16536")
        self.Bastardgas:SetKeyValue("endsize","256")
        self.Bastardgas:SetKeyValue("endcolor",self.EndColor)
        self.Bastardgas:SetKeyValue("startcolor",self.StartColor)
        self.Bastardgas:SetKeyValue("opacity","4")
        self.Bastardgas:SetKeyValue("spawnrate","15")
        self.Bastardgas:SetKeyValue("lifetime","10")
        self.Bastardgas:SetParent(self.Entity)
        self.Bastardgas:Spawn()
        self.Bastardgas:Activate()
        self.Bastardgas:Fire("turnon","", 0.1)
        local exp = ents.Create("env_explosion")
        exp:SetKeyValue("spawnflags",461)
        exp:SetPos(self.Entity:GetPos())
        exp:Spawn()
        exp:Fire("explode","",0)
        self.Entity:EmitSound(Sound("BaseSmokeEffect.Sound"))
        end

        local pos = self.Entity:GetPos()
        local maxrange = 250
        local maxstun = 1
        for k,v in pairs(player.GetAll()) do
            if v.GasProtection then continue end
            local plpos = v:GetPos()
            local dist = -pos:Distance(plpos)+maxrange
            if (pos:Distance(plpos)<=maxrange) and not v:IsGhost() then
                local trace = {}
                trace.start = self.Entity:GetPos()
                trace.endpos = v:GetPos()+Vector(0,0,24)
                trace.filter = { v, self.Entity }
                trace.mask = COLLISION_GROUP_PLAYER
                tr = util.TraceLine(trace)
                if (tr.Fraction==1) then
                    local stunamount = math.ceil(dist/(maxrange/maxstun))
                    v:ViewPunch( Angle( stunamount*((math.random()*6)-2), stunamount*((math.random()*6)-2), stunamount*((math.random()*4)-1) ) )
                        if IsValid(self.weapon) and self.weapon.Freeze then
                        self.weapon:Freeze(100,3,v)
                        end
                   --
                    self:Poison(v, 10, self.Owner, self.Owner:GetWeapon("weapon_gasgrenade"))

                    v:EmitSound("ambient/voices/cough"..math.random(1,4)..".wav")
                end
            end
            end
            if (self.timer+60<CurTime()) then
                if IsValid(self.Bastardgas) then
                    self.Bastardgas:Remove()
                end
            end
            if (self.timer+65<CurTime()) then
                self.Entity:Remove()
            end
            self.Entity:NextThink(CurTime()+0.7)
            return true
        end
    end


    scripted_ents.Register( ENT, "magics_green_gas", true )

-- Laser zap lightning grenade
local ENT = {}

ENT.Type = "anim"
ENT.Base = "cse_ent_shortgasgrenade"

function ENT:Poison(ply,dmg,owner)
    if (ply:Health() >= 0) then
        ply:TakeDamage(dmg or 7, owner or self)
    elseif (ply:Alive()) and (ply:Health() <= 0)then
        ply:Kill()
    else
    end
end

function ENT:Think()
    if not SERVER then return end
    if not self.timer then self.timer = CurTime() + 5 end
    if self.timer < CurTime() then
      self:SpawnPenguin()
        if !IsValid(self.Bastardgas) && !self.Spammed then
        self.Spammed = true
        self.Bastardgas = ents.Create("env_smoketrail")
        self.Bastardgas:SetPos(self.Entity:GetPos())
        self.Bastardgas:SetKeyValue("spawnradius","256")
        self.Bastardgas:SetKeyValue("minspeed","0.5")
        self.Bastardgas:SetKeyValue("maxspeed","2")
        self.Bastardgas:SetKeyValue("startsize","16536")
        self.Bastardgas:SetKeyValue("endsize","256")
        self.Bastardgas:SetKeyValue("endcolor",self.EndColor)
        self.Bastardgas:SetKeyValue("startcolor",self.StartColor)
        self.Bastardgas:SetKeyValue("opacity","4")
        self.Bastardgas:SetKeyValue("spawnrate","15")
        self.Bastardgas:SetKeyValue("lifetime","10")
        self.Bastardgas:SetParent(self.Entity)
        self.Bastardgas:Spawn()
        self.Bastardgas:Activate()
        self.Bastardgas:Fire("turnon","", 0.1)
        local exp = ents.Create("env_explosion")
        exp:SetKeyValue("spawnflags",461)
        exp:SetPos(self.Entity:GetPos())
        exp:Spawn()
        exp:Fire("explode","",0)
        self.Entity:EmitSound(Sound("BaseSmokeEffect.Sound"))
        end

        local pos = self.Entity:GetPos()
        local maxrange = 256
        local maxstun = 10
        for k,v in pairs(player.GetAll()) do
            if v.GasProtection then continue end
            local plpos = v:GetPos()
            local dist = -pos:Distance(plpos)+maxrange
            if (pos:Distance(plpos)<=maxrange) and not v:IsGhost() then
                local trace = {}
                trace.start = self.Entity:GetPos()
                trace.endpos = v:GetPos()+Vector(0,0,24)
                trace.filter = { v, self.Entity }
                trace.mask = COLLISION_GROUP_PLAYER
                tr = util.TraceLine(trace)
                if (tr.Fraction==1) then
                    local stunamount = math.ceil(dist/(maxrange/maxstun))
                    v:ViewPunch( Angle( stunamount*((math.random()*6)-2), stunamount*((math.random()*6)-2), stunamount*((math.random()*4)-1) ) )
                    self:Poison(v, stunamount, self.Owner, self.Owner:GetWeapon("weapon_gasgrenade"))

                    --v:EmitSound("ambient/voices/cough"..math.random(1,4)..".wav")
                end
            end
            end
            if (self.timer+60<CurTime()) then
                if IsValid(self.Bastardgas) then
                    self.Bastardgas:Remove()
                end
            end
            if (self.timer+10<CurTime()) then
                self.Entity:Remove()
            end
            self.Entity:NextThink(CurTime()+1)
            return true
end


    end
    function ENT:SpawnPenguin()
        if not IsValid(self.Owner) then return end
        if not self.Owner.remotemagics then self.Owner.remotemagics = {} end
        for i=1,1 do
        local spos = self:GetPos()+Vector(math.random(-75,75),math.random(-75,75),math.random(0,50))
        local bomb = ents.Create("magics_black_penguin")
        bomb:SetPos(spos)
        bomb.Owner = self.Owner
        bomb:Spawn()
        table.insert(self.Owner.remotemagics, bomb)
        end
    end

    scripted_ents.Register( ENT, "magics_black_gas", true )


local ENT = {}

ENT.Type = "anim"
ENT.Base = "base_anim"


if SERVER then

    function ENT:Initialize()
        self:SetModel("models/penguin/penguin.mdl")
        self:SetModelScale(self:GetModelScale()*1,0)
        self.Entity:PhysicsInit(SOLID_VPHYSICS);
        self.Entity:SetMoveType(MOVETYPE_VPHYSICS);
        self.Entity:SetSolid(SOLID_VPHYSICS);
        self.CanBoom = CurTime() + 3

        self.Entity:SetCollisionGroup(COLLISION_GROUP_WEAPON)
        local phys = self:GetPhysicsObject()
        if phys:IsValid() then
            phys:Wake()
            phys:EnableGravity(true);
        end
        timer.Create(self:EntIndex().."sandboom2",30,0,function()
            if IsValid(self) then
                self:Remove()
            end
        end)

    end

    function ENT:Explode()
if self.exploded then return end self.exploded = true
    if not IsValid(self.Owner) then return end
        local effect = EffectData()
        effect:SetStart(self:GetPos())
        effect:SetOrigin(self:GetPos())
        effect:SetScale(75)
        effect:SetRadius(150)
        effect:SetMagnitude(75)

        util.Effect("Explosion", effect, true, true)

        util.BlastDamage(self, self.Owner, self:GetPos(), 250, 75)
        self:Remove()
    end


end

scripted_ents.Register( ENT, "magics_black_penguin", true )
