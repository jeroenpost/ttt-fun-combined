AddCSLuaFile()

SWEP.PrintName = "Honk Miracles"

SWEP.Spawnable = true
SWEP.AdminOnly = false

SWEP.ViewModel = "models/weapons/c_m9.mdl"
SWEP.WorldModel = "models/weapons/w_m9.mdl"
SWEP.UseHands = true
SWEP.ViewModelFOV = 50
SWEP.ViewModelFlip = false
SWEP.Camo = 6
SWEP.Slot = 100
SWEP.SlotPos = 3

SWEP.DrawWeaponInfoBox = false
SWEP.Base = "gb_camo_base"
SWEP.Primary.ClipSize = 500
SWEP.Primary.DefaultClip= 500
SWEP.Primary.Automatic = true
SWEP.Primary.Ammo = "357"
SWEP.Kind = 100
SWEP.Secondary.ClipSize	= -1
SWEP.Secondary.DefaultClip = -1
SWEP.Secondary.Automatic = false
SWEP.Secondary.Ammo	= "none"
SWEP.hideblind = true
function SWEP:PrimaryAttack()
    self:SetNextPrimaryFire( CurTime() + 0.5 )
    local mode = self:GetNWString("shootmode","Sniper")

    if mode == "Sniper" then
        self:SetNextPrimaryFire( CurTime() + 1.1 )
        self:ShootBullet( 95, 1, 0 )
        self:EmitSound( "weapons/usp/usp1.wav" )
        self:TakePrimaryAmmo( 1 )
    end
    if mode == "Laser" then
        self:SetNextPrimaryFire( CurTime() + 0.095 )
        self:ShootBullet( 13, 1, 0 )
        self:EmitSound( "weapons/usp/usp1.wav" )
        self:TakePrimaryAmmo( 1 )
        self.LaserColor = Color(255,0,255,255)
        self:ShootTracer("LaserTracer_thick")
    end
    if mode == "Hotdog" then
        self:SetNextPrimaryFire( CurTime() + 3 )
        self:EmitSound( "player/footsteps/slosh4.wav" )
        self:throw_attack ( "models/food/hotdog.mdl","", 15);
    end
end

function SWEP:throw_attack (model_file, sound, away)
    local tr = self.Owner:GetEyeTrace();

    if IsValid( tr.Entity )  then
        tr.Entity.hasProtectionSuit= true
    end

    self.BaseClass.ShootEffects (self);
    if (!SERVER) then return end;
    local ent = ents.Create ("honk_hotdog");
    ent:SetModel (model_file);
    ent:SetPos (self.Owner:EyePos() + (self.Owner:GetAimVector() * 16));
    ent:SetAngles (self.Owner:EyeAngles());
    ent:SetPhysicsAttacker(self.Owner)
    ent.Owner = self.Owner
    ent:Spawn();
    ent:EmitSound( sound )
    if math.random(1,10) < 2 then
        ent:Ignite(100,100)
    end
    local phys = ent:GetPhysicsObject();
    local shot_length = tr.HitPos:Length()+10;
    phys:SetMass( 1 )
    phys:ApplyForceCenter (self.Owner:GetAimVector():GetNormalized() * math.pow (shot_length, 3));

    timer.Simple(away,function()
        if IsValid(ent) then
            ent:Remove()
        end
    end)

    cleanup.Add (self.Owner, "props", ent);
    undo.Create ("Thrown model");
    undo.AddEntity (ent);
    undo.SetPlayer (self.Owner);
    undo.Finish();
end


function SWEP:SecondaryAttack()
    self:rFly()
end

SWEP.NextReload = 0
function SWEP:Reload()
    if self.Owner:KeyDown(IN_ATTACK) then
        return
    end
    if self.Owner:KeyDown(IN_USE) and self.NextReload < CurTime() then
        self.NextReload = CurTime() + 0.3
        local mode = self:GetNWString("shootmode","Sniper")
        self:SetIronsights(false)

        if mode == "Sniper" then  self:SetNWString("shootmode","Laser")  end
        if mode == "Laser" then  self:SetNWString("shootmode","Hotdog") end
        if mode == "Hotdog" then  self:SetNWString("shootmode","Sniper") end

        self.Weapon:SetNextPrimaryFire( CurTime() + 0.8 )
        return
    end
end


if CLIENT then
    function SWEP:DrawHUD()

        local shotttext = "Shootmode: "..self:GetNWString("shootmode", "Sniper").."\nE+R to change"
        surface.SetFont( "ChatFont" );
        local size_x, size_y = surface.GetTextSize( shotttext );
        draw.RoundedBox( 6, ScrW( ) / 2 - size_x / 2 - 5, ScrH( ) - 100, size_x + 15, size_y + 10, Color( 0, 0, 0, 150 ) );
        draw.DrawText( shotttext , "ChatFont", ScrW( ) / 2, ScrH( ) - 100 + 5, Color( 255, 30, 30, 255 ), TEXT_ALIGN_CENTER );
        self.BaseClass.DrawHUD(self)
    end
end


local ENT = {}

ENT.Type = "anim"
ENT.Base = "base_anim"


if SERVER then

    function ENT:Initialize()
        self.Start = CurTime()

        self:SetModel("models/food/hotdog.mdl")
        self:SetModelScale(self:GetModelScale()*1,0)

        self.Entity:PhysicsInit(SOLID_VPHYSICS);
        self.Entity:SetMoveType(MOVETYPE_VPHYSICS);
        self.Entity:SetSolid(SOLID_VPHYSICS);
        self.Entity:SetCollisionGroup(COLLISION_GROUP_PROJECTILE)
        self.Damage = 1
        local phys = self:GetPhysicsObject()
        if phys:IsValid() then
            phys:Wake()
            phys:EnableGravity(true);
        end

    end



    function ENT:PhysicsCollide(data, obj)
        --  self:EmitSound( "Rubber.BulletImpact" )
        if  data.HitEntity:IsPlayer( ) then
            self:Touch(data.HitEntity)
        end

    end
    function ENT:Touch(ent)
        if IsValid(ent) and  ent:IsPlayer( ) then

            if not ent.nextcrecentpain then ent.nextcrecentpain = 0 end
            local target = ent
            local tpos = target:LocalToWorld(target:OBBCenter())
            local dir = (tpos - pos):GetNormal()
            local phys = target:GetPhysicsObject()
            local push_force = 700
            local phys_force = 700
            if target:IsPlayer() and (not target:IsFrozen()) and ((not target.was_pushed) or target.was_pushed.t != CurTime()) then

            -- always need an upwards push to prevent the ground's friction from
            -- stopping nearly all movement
            dir.z = math.abs(dir.z) + 1

            local push = dir * push_force
            local vel = target:GetVelocity() + push
            vel.z = math.min(vel.z, push_force)
            local pusher = self:GetOwner() or self.Owner or nil

            target:SetVelocity(vel)
            target:SetGroundEntity( nil )
            target.was_pushed = {att=pusher, t=CurTime()}

            elseif IsValid(phys) then
                phys:ApplyForceCenter(dir * -1 * phys_force)
            end


        end
    end


end

scripted_ents.Register( ENT, "honk_hotdog", true )