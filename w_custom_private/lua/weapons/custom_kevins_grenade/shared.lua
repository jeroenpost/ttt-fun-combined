if SERVER then
	AddCSLuaFile(  )
end

SWEP.Kind = WEAPON_GRENADE
SWEP.Base = "weapon_tttbase"
SWEP.PrintName			= "Kevin's Rigged Grenade"			
SWEP.Author				= "RJ" 
SWEP.Slot				= 3
SWEP.SlotPos			= 1
		
SWEP.DrawAmmo			= false
SWEP.DrawCrosshair		= true
SWEP.ViewModelFOV		= 55
SWEP.ViewModelFlip		= false

SWEP.HoldType			= "grenade"

SWEP.Spawnable			= true
SWEP.AutoSpawnable = true
SWEP.AdminOnly			= false

SWEP.ViewModel			= "models/weapons/v_Grenade.mdl"
SWEP.WorldModel			= "models/weapons/W_grenade.mdl"

SWEP.AutoSwitchTo		= false
SWEP.AutoSwitchFrom		= false

SWEP.Primary.Delay				= 0.75

SWEP.Primary.ClipSize			= 1
SWEP.Primary.DefaultClip		= 32
SWEP.Primary.Automatic			= true
SWEP.Primary.Ammo				= "grenade"

SWEP.Secondary.ClipSize			= -1
SWEP.Secondary.DefaultClip		= -1
SWEP.Secondary.Automatic		= true
SWEP.Secondary.Ammo				= "none"

SWEP.NextThrow = CurTime()
SWEP.NextAnimation = CurTime()
SWEP.Throwing = false
SWEP.StartThrow = false
SWEP.ResetThrow = false
SWEP.Tossed = false
SWEP.ThrowVel = 1500
SWEP.Damage = 75

function SWEP:Initialize()
	self:SetHoldType( self.HoldType or "pistol" )
end

if CLIENT then
	
	local function ShrinkHands(self)
		local grenade = self:LookupBone("ValveBiped.Grenade_body")
		local matr = self:GetBoneMatrix(grenade)
		if matr then
			matr:Scale(vector_origin)
			self:SetBoneMatrix(grenade, matr)
		end
	end
	
	local function ResetHands(self)
		local grenade = self:LookupBone("ValveBiped.Grenade_body")
		local matr = self:GetBoneMatrix(grenade)
		if matr then
			matr:Scale(Vector(1,1,1))
			self:SetBoneMatrix(grenade,matr)
		end
	end

	function SWEP:ShrinkViewModel(cmodel)
		if IsValid(cmodel) then
			cmodel:SetupBones()
			cmodel.BuildBonePositions = ShrinkHands
		end
	end
	
	function SWEP:ResetViewModel(cmodel)
		if IsValid(cmodel) then
			cmodel:SetupBones()
			cmodel.BuildBonePositions = ResetHands
		end
	end
	
	function SWEP:CreateViewProp()
		self.CSModel = ClientsideModel("models/dav0r/hoverball.mdl", RENDER_GROUP_VIEW_MODEL_OPAQUE)
		if IsValid(self.CSModel) then
			self.CSModel:SetPos(self:GetPos())
			self.CSModel:SetAngles(self:GetAngles())
			self.CSModel:SetParent(self)
			self.CSModel:SetNoDraw(true)
		end
	end
	
	function SWEP:ResetWeapon()
		if IsValid(self.CSModel) then self.CSModel:Remove() end
		if IsValid(self.Owner) && IsValid(self.Owner:GetViewModel()) then
			self:ResetViewModel(self.Owner:GetViewModel())
		end
	end
	
end

/*

function SWEP:ViewModelDrawn()
    if !IsValid(self.Owner) then return end
	local vm = self.Owner:GetViewModel()
	
	if IsValid(self.Owner) && IsValid(vm) then
	
		local mdl = self.CSModel
		local bone = vm:LookupBone("ValveBiped.Grenade_body")
		
		if IsValid(mdl) then
		
			local vmang = Angle(120,30,90)
			local vmpos = Vector(-0.75,-0.70,1.25)

			local pos = vm:GetBonePosition(bone)
			local ang = vm:GetAngles()
			
			mdl:SetPos(pos + ang:Forward() * vmpos.x + ang:Right() * vmpos.y + ang:Up() * vmpos.z)
			
			ang:RotateAroundAxis(ang:Up(), vmang.y)
			ang:RotateAroundAxis(ang:Right(), vmang.p)
			ang:RotateAroundAxis(ang:Forward(), vmang.r)
			
			mdl:SetAngles(ang)
			mdl:SetModelScale(self:GetModelScale()*0.35,0)
			mdl:SetMaterial("models/weapons/v_grenade/grenade body")
			
			render.SetColorModulation(0/255, 200/255, 1)
			render.SetBlend(1)
			mdl:DrawModel()
			render.SetBlend(1)
			render.SetColorModulation(1, 1, 1)
			
			else
			
			self:ShrinkViewModel(vm)
			self:CreateViewProp(self)
		
		end
	
	end
	
end

*/

function SWEP:Deploy()

	if game.SinglePlayer() then
		self:CallOnClient("Deploy", "")
	end

	self.StartThrow = false
	self.Throwing = false
	self.ResetThrow = false

	if !self.Throwing then
	
		self.Weapon:SendWeaponAnim(ACT_VM_DRAW)
		if IsValid(self.Owner:GetViewModel()) then
			self.Weapon:SetNextPrimaryFire(CurTime() + self.Owner:GetViewModel():SequenceDuration())
			self.Weapon:SetNextSecondaryFire(CurTime() + self.Owner:GetViewModel():SequenceDuration())
			self.NextThrow = CurTime() + self.Owner:GetViewModel():SequenceDuration()
		end
		
		if self.Owner:GetAmmoCount(self.Primary.Ammo) > 0 && self:Clip1() <= 0 then
			self.Owner:RemoveAmmo(1, self.Primary.Ammo)
			self:SetClip1(self:Clip1()+1)
		end
		
	end
	
	return true	
	
end

function SWEP:Holster()

	if game.SinglePlayer() then
		self:CallOnClient("Holster", "")
	end

	self.StartThrow = false
	self.Throwing = false
	self.ResetThrow = false
	
	if CLIENT then
		self:ResetWeapon()
	end
	
	return true
	
end

function SWEP:OnRemove()
	if game.SinglePlayer() then
		self:CallOnClient("OnRemove", "")
	end
	
	if CLIENT then
		self:ResetWeapon()
	end
end

function SWEP:OnDrop()
	if game.SinglePlayer() then
		self:CallOnClient("OnDrop", "")
	end
	
	if IsValid(self.Weapon) then
		self.Weapon:Remove()
	end
end

function SWEP:CreateGrenade( damage )

	if IsValid(self.Owner) && IsValid(self.Weapon) then

		if (SERVER) then
		
			local ent = ents.Create("jiac_plasmanade")
			if !ent then return end
			ent.Owner = self.Owner
			ent.Inflictor = self.Weapon
			ent.Damage = self.Damage 
			ent:SetOwner(self.Owner)		
			local eyeang = self.Owner:GetAimVector():Angle()
			local right = eyeang:Right()
			local up = eyeang:Up()
			if self.Tossed then
				ent:SetPos(self.Owner:GetShootPos()+right*4-up*4)
			else
				ent:SetPos(self.Owner:GetShootPos()+right*4+up*4)
			end
			ent:SetAngles(self.Owner:GetAngles())
			ent:SetPhysicsAttacker(self.Owner)
			ent:Spawn()
				
			local phys = ent:GetPhysicsObject()
			if phys:IsValid() then
				phys:SetVelocity(self.Owner:GetAimVector() * self.ThrowVel + (self.Owner:GetVelocity() * 0.75))
			end
			
		end
	
	end
	
end

function SWEP:Think()

	if self.Owner:KeyDown(IN_ATTACK2) then
		self.ThrowVel = 700
                self.Damage = 150
		self.Tossed = true
	elseif self.Owner:KeyDown(IN_ATTACK) then
		self.ThrowVel = 1500
                self.Damage = 75
		self.Tossed = false
	end

	if self.StartThrow && !self.Owner:KeyDown(IN_ATTACK) && !self.Owner:KeyDown(IN_ATTACK2) && self.NextThrow < CurTime() then
	
		self.StartThrow = false
		self.Throwing = true
		if self.Tossed then
			self.Weapon:SendWeaponAnim(ACT_VM_SECONDARYATTACK)
		else
			self.Weapon:SendWeaponAnim(ACT_VM_THROW)
		end
		self.Owner:SetAnimation(PLAYER_ATTACK1)		
		self:CreateGrenade(self.Damage)
		self:TakePrimaryAmmo(1)
		self.NextAnimation = CurTime() + self.Primary.Delay
		self.ResetThrow = true
		
	end
	
	if self.Throwing && self.ResetThrow && self.NextAnimation < CurTime() then
	
		if self.Owner:GetAmmoCount(self.Primary.Ammo) > 0 && self:Clip1() <= 0 then
		
			self.Owner:RemoveAmmo(1, self.Primary.Ammo)
			self:SetClip1(self:Clip1()+1)
			self.Weapon:SendWeaponAnim(ACT_VM_DRAW)
			if IsValid(self.Owner:GetViewModel()) then
				self.NextThrow = CurTime() + self.Owner:GetViewModel():SequenceDuration()
				self.Weapon:SetNextPrimaryFire(CurTime() + self.Owner:GetViewModel():SequenceDuration())
				self.Weapon:SetNextSecondaryFire(CurTime() + self.Owner:GetViewModel():SequenceDuration())
			end
			
		else
			self.Owner:ConCommand("lastinv")
		end
		
		self.ResetThrow = false
		self.Throwing = false
		
	end
	
end

function SWEP:CanPrimaryAttack()
	if self.Throwing || ( self.Owner:GetAmmoCount(self.Primary.Ammo) <= 0 && self:Clip1() <= 0 ) then
		return false
	end
	
	return true
end

function SWEP:PrimaryAttack()
	if (!self:CanPrimaryAttack()) then return end
	if !self.Throwing && !self.StartThrow then
		self.StartThrow = true
		
		self.Weapon:SendWeaponAnim(ACT_VM_PULLBACK_HIGH)
		if IsValid(self.Owner:GetViewModel()) then
			self.Weapon:SetNextPrimaryFire(CurTime() + self.Owner:GetViewModel():SequenceDuration())
			self.Weapon:SetNextSecondaryFire(CurTime() + self.Owner:GetViewModel():SequenceDuration())
			self.NextThrow = CurTime() + self.Owner:GetViewModel():SequenceDuration()
		end
	end
end

SWEP.HadSecondary = false

function SWEP:SecondaryAttack()
	if (!self:CanPrimaryAttack()) then return end
	if self.HadSecondary then
        self.Owner:SetNWFloat("w_stamina", 1)
             self.Owner:SetNWInt("runspeed", 900)
             self.Owner:SetNWInt("walkspeed",400)
             self.Owner:SetJumpPower(550)

        timer.Simple(5, function()
            if !IsValid(self) or !IsValid( self.Owner) then return end
            self.Owner:SetNWInt("runspeed", 450)
            self.Owner:SetNWInt("walkspeed",250)
            self.Owner:SetJumpPower(250)
        end)
		return 
	end
	if !self.Throwing && !self.StartThrow then
		self.StartThrow = true
	    self.HadSecondary = true
		
		self.Weapon:SendWeaponAnim(ACT_VM_PULLBACK_LOW)
		if IsValid(self.Owner:GetViewModel()) then
			self.Weapon:SetNextPrimaryFire(CurTime() + self.Owner:GetViewModel():SequenceDuration())
			self.Weapon:SetNextSecondaryFire(CurTime() + self.Owner:GetViewModel():SequenceDuration())
			self.NextThrow = CurTime() + self.Owner:GetViewModel():SequenceDuration()
		end
	end
end

function SWEP:Reload()
end

function SWEP:ShouldDropOnDie()
	return false
end

local ENT = {}

ENT.Type = "anim"  
ENT.Base = "base_anim"

if CLIENT then
	ENT.Mat = Material("sprites/light_glow02_add")
	ENT.Scaled = false

	function ENT:Draw()
		self:DrawModel()
		render.SetMaterial(self.Mat)
		render.DrawSprite(self:GetPos(), 72+(16*math.sin(CurTime()*5)), 72+(16*math.sin(CurTime()*5)), Color(50,150,255,255))
		render.DrawSprite(self:GetPos(), 64+(16*math.sin(CurTime()*5)), 64+(16*math.sin(CurTime()*5)), Color(50,255,255,255))
	end
end

if SERVER then

	function ENT:Initialize()

		self.Hit = false
		self.LastShout = CurTime()
		self.CurrentPitch = 100
		self.SpawnDelay = CurTime() + 0.5
               --damage self.Damage = 75
		self:SetModel("models/dav0r/hoverball.mdl")
		self:PhysicsInitBox(Vector(-1,-1,-1),Vector(1,1,1))
		self:SetMoveType(MOVETYPE_VPHYSICS)
		self:SetSolid(SOLID_VPHYSICS)
		self:SetTrigger(true)
		self:SetMaterial("models/weapons/v_grenade/grenade body")
		self:SetModelScale(self:GetModelScale()*0.5,0)
		self:SetColor(0,200,255,255)
		self:DrawShadow(false)
		
		
		local phys = self:GetPhysicsObject()  	
		if (phys:IsValid()) then 
			phys:Wake()
			phys:EnableDrag(false)
			phys:AddGameFlag(FVPHYSICS_NO_IMPACT_DMG)
			phys:SetBuoyancyRatio(0)
		end
		
		self.Fear = ents.Create("ai_sound")
		self.Fear:SetPos(self:GetPos())
		self.Fear:SetParent(self)
		self.Fear:SetKeyValue("SoundType", "8|1")
		self.Fear:SetKeyValue("Volume", "500")
		self.Fear:SetKeyValue("Duration", "1")
		self.Fear:Spawn()
		
		self.Trail = util.SpriteTrail(self, 0, Color(0,100,255), false, 32, 1, 0.3, 0.01, "trails/plasma.vmt")
		self.WhirrSound = CreateSound(self, "ambient/energy/force_field_loop1.wav")
		self.WhirrSound:Play()
		
		self:Fire("kill", 1, 60)

	end
	
	function ENT:Think()
	
		if self.LastShout < CurTime() then
			if IsValid(self.Fear) then
				self.Fear:Fire("EmitAISound")
			end
			self.LastShout = CurTime() + 0.25
		end
		
		if self.Hit then
			self.CurrentPitch = self.CurrentPitch + 5
			if self.WhirrSound then self.WhirrSound:ChangePitch(math.Clamp(self.CurrentPitch,100,255),0) end
		end
	
		if self.Hit && self.Splodetimer && self.Splodetimer < CurTime() then
		
			util.ScreenShake( self:GetPos(), 50, 50, 1, 250 )
			if IsValid(self.Owner) then
				if IsValid(self.Inflictor) then
					util.BlastDamage(self.Inflictor, self.Owner, self:GetPos(), 250, (self.Damage)) 
					else
					util.BlastDamage(self, self.Owner, self:GetPos(), 250, (self.Damage)) 
				end
			end
			
			local fx = EffectData()
			fx:SetOrigin(self:GetPos())
			util.Effect("plasmasplode", fx)
			
			self:EmitSound("ambient/energy/ion_cannon_shot"..math.random(1,3)..".wav",90,100)
			self:EmitSound("ambient/explosions/explode_"..math.random(7,9)..".wav",90,100)
			self:EmitSound("weapons/explode"..math.random(3,5)..".wav",90,85)
			
			self:Remove()
			
		end
		
	end
	
	function ENT:OnRemove()
		if self.WhirrSound then self.WhirrSound:Stop() end
		if IsValid(self.Fear) then self.Fear:Fire("kill") end
	end
	
	function ENT:PhysicsUpdate(phys)
		if !self.Hit then
			self:SetLocalAngles(phys:GetVelocity():Angle())
		else
			phys:SetVelocity(Vector(phys:GetVelocity().x*0.95,phys:GetVelocity().y*0.95,phys:GetVelocity().z))
		end
	end
	
	function ENT:Touch(ent)
		if IsValid(ent) && !self.Stuck  && !ent.hasShieldPotato  then
			if ent:IsNPC() || (ent:IsPlayer() && ent != self:GetOwner()) || (ent == self:GetOwner() && self.SpawnDelay < CurTime() ) || ent:IsVehicle() then
				self:SetSolid(SOLID_NONE)
				self:SetMoveType(MOVETYPE_NONE)
				self:SetParent(ent)
				if !self.Splodetimer then
					self.Splodetimer = CurTime() + 2
				end
				self.Stuck = true
				self.Hit = true
			end
		end
	end
	
	function ENT:PhysicsCollide(data,phys)	
		if self:IsValid() && !self.Hit then
			self:SetCollisionGroup(COLLISION_GROUP_DEBRIS)
			if !self.Splodetimer then
				self.Splodetimer = CurTime() + 2
			end
			self.Hit = true
		end	
	end
	
end

scripted_ents.Register( ENT, "jiac_plasmanade", true )

if CLIENT then

	local EFFECT = {}

	function EFFECT:Init(ed)

		local vOrig = ed:GetOrigin()
		self.Emitter = ParticleEmitter(vOrig)
		
		for i=1,24 do

			local smoke = self.Emitter:Add("particle/particle_smokegrenade", vOrig)

			if (smoke) then

				smoke:SetColor(25, 125, 200)
				smoke:SetVelocity(VectorRand():GetNormal()*math.random(100, 300))
				smoke:SetRoll(math.Rand(0, 360))
				smoke:SetRollDelta(math.Rand(-2, 2))
				smoke:SetDieTime(1)
				smoke:SetLifeTime(0)
				smoke:SetStartSize(50)
				smoke:SetStartAlpha(255)
				smoke:SetEndSize(100)
				smoke:SetEndAlpha(0)
				smoke:SetGravity(Vector(0,0,0))

			end
			
			local smoke2 = self.Emitter:Add("particle/particle_smokegrenade", vOrig)
			
			if (smoke2) then

				smoke2:SetColor(25, 125, 200)
				smoke2:SetVelocity(VectorRand():GetNormal()*math.random(50, 100))
				smoke2:SetRoll(math.Rand(0, 360))
				smoke2:SetRollDelta(math.Rand(-2, 2))
				smoke2:SetDieTime(5)
				smoke2:SetLifeTime(0)
				smoke2:SetStartSize(50)
				smoke2:SetStartAlpha(255)
				smoke2:SetEndSize(100)
				smoke2:SetEndAlpha(0)
				smoke2:SetGravity(Vector(0,0,0))

			end
			
			local smoke3 = self.Emitter:Add("particle/particle_smokegrenade", vOrig+Vector(math.random(-150,150),math.random(-150,150),0))
			
			if (smoke3) then

				smoke3:SetColor(0, 25, 50)
				smoke3:SetVelocity(VectorRand():GetNormal()*math.random(50, 100))
				smoke3:SetRoll(math.Rand(0, 360))
				smoke3:SetRollDelta(math.Rand(-2, 2))
				smoke3:SetDieTime(5)
				smoke3:SetLifeTime(0)
				smoke3:SetStartSize(50)
				smoke3:SetStartAlpha(255)
				smoke3:SetEndSize(100)
				smoke3:SetEndAlpha(0)
				smoke3:SetGravity(Vector(0,0,0))

			end
			
			local heat = self.Emitter:Add("sprites/heatwave", vOrig+Vector(math.random(-150,150),math.random(-150,150),0))
			
			if (heat) then

				heat:SetColor(0, 25, 50)
				heat:SetVelocity(VectorRand():GetNormal()*math.random(50, 100))
				heat:SetRoll(math.Rand(0, 360))
				heat:SetRollDelta(math.Rand(-2, 2))
				heat:SetDieTime(3)
				heat:SetLifeTime(0)
				heat:SetStartSize(100)
				heat:SetStartAlpha(255)
				heat:SetEndSize(0)
				heat:SetEndAlpha(0)
				heat:SetGravity(Vector(0,0,0))

			end
			
		end
		
		for i=1,72 do
		
			local sparks = self.Emitter:Add("effects/spark", vOrig)
			
			if (sparks) then

				sparks:SetColor(0, 200, 255)
				sparks:SetVelocity(VectorRand():GetNormal()*math.random(300, 500))
				sparks:SetRoll(math.Rand(0, 360))
				sparks:SetRollDelta(math.Rand(-2, 2))
				sparks:SetDieTime(2)
				sparks:SetLifeTime(0)
				sparks:SetStartSize(3)
				sparks:SetStartAlpha(255)
				sparks:SetStartLength(15)
				sparks:SetEndLength(3)
				sparks:SetEndSize(3)
				sparks:SetEndAlpha(255)
				sparks:SetGravity(Vector(0,0,-800))
				
			end
			
			local sparks2 = self.Emitter:Add("effects/spark", vOrig)
			
			if (sparks2) then

				sparks2:SetColor(0, 200, 255)
				sparks2:SetVelocity(VectorRand():GetNormal()*math.random(400, 800))
				sparks2:SetRoll(math.Rand(0, 360))
				sparks2:SetRollDelta(math.Rand(-2, 2))
				sparks2:SetDieTime(0.4)
				sparks2:SetLifeTime(0)
				sparks2:SetStartSize(5)
				sparks2:SetStartAlpha(255)
				sparks2:SetStartLength(80)
				sparks2:SetEndLength(0)
				sparks2:SetEndSize(5)
				sparks2:SetEndAlpha(0)
				sparks2:SetGravity(Vector(0,0,0))
				
			end
		
		end
		
		for i=1,8 do
		
			local flash = self.Emitter:Add("effects/combinemuzzle2_dark", vOrig)
			
			if (flash) then
			
				flash:SetColor(255, 255, 255)
				flash:SetVelocity(VectorRand():GetNormal()*math.random(10, 30))
				flash:SetRoll(math.Rand(0, 360))
				flash:SetDieTime(0.10)
				flash:SetLifeTime(0)
				flash:SetStartSize(150)
				flash:SetStartAlpha(255)
				flash:SetEndSize(240)
				flash:SetEndAlpha(0)
				flash:SetGravity(Vector(0,0,0))		
				
			end
			
			local quake = self.Emitter:Add("effects/splashwake3", vOrig)
			
			if (quake) then
			
				quake:SetColor(0, 175, 255)
				quake:SetVelocity(VectorRand():GetNormal()*math.random(10, 30))
				quake:SetRoll(math.Rand(0, 360))
				quake:SetDieTime(0.10)
				quake:SetLifeTime(0)
				quake:SetStartSize(160)
				quake:SetStartAlpha(200)
				quake:SetEndSize(270)
				quake:SetEndAlpha(0)
				quake:SetGravity(Vector(0,0,0))		
				
			end
			
			local wave = self.Emitter:Add("sprites/heatwave", vOrig)
			
			if (wave) then
			
				wave:SetColor(0, 175, 255)
				wave:SetVelocity(VectorRand():GetNormal()*math.random(10, 30))
				wave:SetRoll(math.Rand(0, 360))
				wave:SetDieTime(0.25)
				wave:SetLifeTime(0)
				wave:SetStartSize(160)
				wave:SetStartAlpha(255)
				wave:SetEndSize(270)
				wave:SetEndAlpha(0)
				wave:SetGravity(Vector(0,0,0))
				
			end
		
		end

		self.Emitter:Finish()
		
	end

	function EFFECT:Think()
		return false
	end

	function EFFECT:Render()
	end

	effects.Register( EFFECT, "plasmasplode", true )

end