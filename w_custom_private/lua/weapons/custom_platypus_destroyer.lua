SWEP.HoldType = "shotgun"
SWEP.PrintName = "Platypus Destroyer"
SWEP.Slot = 2
if CLIENT then
   
   SWEP.Author = "GreenBlack"
   
   SWEP.Icon = "vgui/ttt_fun_killicons/m3.png"
end

SWEP.Base = "aa_base"
SWEP.Spawnable = true
SWEP.AdminSpawnable = true
SWEP.Kind = WEAPON_HEAVY
SWEP.Camo = 0

SWEP.Primary.Ammo = "Buckshot"
SWEP.Primary.Damage = 12
SWEP.Primary.Cone = 0.13
SWEP.Primary.Delay = 0.9
SWEP.Primary.ClipSize = 72
SWEP.Primary.ClipMax = 72
SWEP.Primary.DefaultClip = 72
SWEP.Primary.Automatic = false
SWEP.Primary.NumShots = 10
SWEP.HeadshotMultiplier = 1.2
SWEP.AutoSpawnable = false
SWEP.AmmoEnt = "item_box_buckshot_ttt"
SWEP.ViewModel = "models/weapons/c_shotgun.mdl"
SWEP.ViewModelFlip = false
SWEP.ViewModelFOV = 54
SWEP.WorldModel = "models/weapons/w_shotgun.mdl"
SWEP.Primary.Sound = Sound( "CSTM_SilencedShot7" )
SWEP.Primary.Recoil = 7
SWEP.reloadtimer = 0
SWEP.UseHands = true

SWEP.IronSightsPos = Vector(5.74, -2.19, 3.28)
SWEP.IronSightsAng = Vector(0.547, 0, 0)
local inputColor = {255, 0, 0 }

function SWEP:SetupDataTables()
   self:DTVar("Bool", 0, "reloading")

   return self.BaseClass.SetupDataTables(self)
end

function SWEP:SecondaryAttack()
    self:Fly()
end


function SWEP:Reload()
    if self.Owner:KeyDown(IN_USE) then


            if CLIENT or (self.Owner.NextEatMoney and self.Owner.NextEatMoney > CurTime()) then return end
            self.Owner.NextEatMoney =  CurTime() + 30
            self:throw_attack ("platypus_destroy_health",  "physics/glass/glass_impact_hard3.wav", 100)
        return
    end
   self:SetIronsights( false )
   
   if self.dt.reloading then return end

   if not IsFirstTimePredicted() then return end
   
   if self.Weapon:Clip1() < self.Primary.ClipSize and self.Owner:GetAmmoCount( self.Primary.Ammo ) > 0 then
      
      if self:StartReload() then
         return
      end
   end

end

function SWEP:OnDrop()
    self:Remove()
end

function SWEP:StartReload()
   if self.dt.reloading then
      return false
   end

   if not IsFirstTimePredicted() then return false end

   self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
   
   local ply = self.Owner
   
   if not ply or ply:GetAmmoCount(self.Primary.Ammo) <= 0 then 
      return false
   end

   local wep = self.Weapon
   
   if wep:Clip1() >= self.Primary.ClipSize then 
      return false 
   end

   wep:SendWeaponAnim(ACT_SHOTGUN_RELOAD_START)

   self.reloadtimer =  CurTime() + wep:SequenceDuration()

   self.dt.reloading = true

   return true
end

function SWEP:PerformReload()
   local ply = self.Owner
   
   self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )

   if not ply or ply:GetAmmoCount(self.Primary.Ammo) <= 0 then return end

   local wep = self.Weapon

   if wep:Clip1() >= self.Primary.ClipSize then return end

   self.Owner:RemoveAmmo( 1, self.Primary.Ammo, false )
   self.Weapon:SetClip1( self.Weapon:Clip1() + 1 )

   wep:SendWeaponAnim(ACT_VM_RELOAD)

   self.reloadtimer = CurTime() + wep:SequenceDuration()
end

function SWEP:FinishReload()
   self.dt.reloading = false
   self.Weapon:SendWeaponAnim(ACT_SHOTGUN_RELOAD_FINISH)
   
   self.reloadtimer = CurTime() + self.Weapon:SequenceDuration()
end

function SWEP:CanPrimaryAttack()
    if self.Weapon:Clip1() <= 0 then
        self:EmitSound( "Weapon_Shotgun.Empty" )
        self:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
        return false
    end
    if self.reloadtimer < CurTime() then
        return true
    else
        return false
    end
end

function SWEP:Think()
    self:HealSecond()
   if self.dt.reloading and IsFirstTimePredicted() then
      if self.Owner:KeyDown(IN_ATTACK) then
         self:FinishReload()
         return
      end
      
      if self.reloadtimer <= CurTime() then

         if self.Owner:GetAmmoCount(self.Primary.Ammo) <= 0 then
            self:FinishReload()
         elseif self.Weapon:Clip1() < self.Primary.ClipSize then
            self:PerformReload()
         else
            self:FinishReload()
         end
         return            
      end
   end
end

local inputColor = {255, 0, 0}



function SWEP:Equip()

        self:SetColorAndMaterial(Color(255,0,0,255),"models/shiny");
        local ply = self.Owner
        if ply:GetNWInt("walkspeed") < 400 then
            ply:SetNWInt("walkspeed",400)
        end
        if ply:GetNWInt("runspeed") < 550 then
            ply:SetNWInt("runspeed", 550)
        end
        self.Owner:SetNWFloat("w_stamina", 1)
        self.Owner:SetJumpPower(450)


   self.dt.reloading = false
   self.reloadtimer = 0
   return self.BaseClass.Equip(self)
end

function SWEP:GetHeadshotMultiplier(victim, dmginfo)
   local att = dmginfo:GetAttacker()
   if not IsValid(att) then return 3 end

   local dist = victim:GetPos():Distance(att:GetPos())
   local d = math.max(0, dist - 140)
   
   return 1 + math.max(0, (2.1 - 0.002 * (d ^ 1.25)))
end


local inputColor = {255, 0, 0}
function SWEP:randomColor(inputCol)
    speed = 0.25

    -- Red to Green
    if inputCol[1] == 255 and inputCol[3] == 0 and inputCol[2] != 255 then
    inputCol[2] = inputCol[2] + speed
    elseif inputCol[2] == 255 and inputCol[1] != 0 then
    inputCol[1] = inputCol[1] - speed

        -- Green to Blue
    elseif inputCol[2] == 255 and inputCol[3] != 255 then
    inputCol[3] = inputCol[3] + speed
    elseif inputCol[3] == 255 and inputCol[2] != 0 then
    inputCol[2] = inputCol[2] - speed

        -- Blue to Red
    elseif inputCol[3] == 255 and inputCol[1] != 255 then
    inputCol[1] = inputCol[1] + speed
    elseif inputCol[1] == 255 and inputCol[3] != 0 then
    inputCol[3] = inputCol[3] - speed
    end

    return Color( inputCol[1], inputCol[2], inputCol[3], 0)
end

function SWEP:PreDrawViewModel()
    if SERVER or not IsValid(self.Owner) or not IsValid(self.Owner:GetViewModel()) then return end
    local colors = self:randomColor(inputColor)
    self.Owner:GetViewModel():SetColor( colors )
    self.Owner:GetViewModel():SetMaterial("models/shiny")
end

function SWEP:throw_attack (model_file, sound, away)
    local tr = self.Owner:GetEyeTrace();

    if IsValid( tr.Entity )  then
        if not tr.Entity.hasProtectionSuit then
            tr.Entity.hasProtectionSuit= true
            timer.Simple(5, function()
                if not IsValid( tr.Entity )  then return end
                tr.Entity.hasProtectionSuit = false
            end)
        end
    end

    self.BaseClass.ShootEffects (self);
    if (!SERVER) then return end;
    local ent = ents.Create ("platypus_destroy_health");
    --  ent:SetModel (model_file);
    ent:SetPos( tr.HitPos + self.Owner:GetAimVector() * -46 )
    ent:SetAngles (self.Owner:EyeAngles());
    ent:SetPhysicsAttacker(self.Owner)
    ent:Spawn();
    ent:EmitSound( sound )
    if math.Rand(1,25) < 2 then
        ent:Ignite(100,100)
    end
    local phys = ent:GetPhysicsObject();
    local shot_length = tr.HitPos:Length();
    -- phys:ApplyForceCenter (self.Owner:GetAimVector():GetNormalized() * math.pow (shot_length, 3) * 2);

    timer.Simple(away,function()
        if IsValid(ent) then
            ent:Remove()
        end
    end)

    cleanup.Add (self.Owner, "props", ent);
    undo.Create ("Thrown model");
    undo.AddEntity (ent);
    undo.SetPlayer (self.Owner);
    undo.Finish();
end


local ENT = {}

ENT.Type = "anim"
ENT.Base = "base_anim"


if SERVER then

    function ENT:Initialize()


        self:SetModel( "models/weapons/w_medkit.mdl")
        self:SetModelScale(self:GetModelScale()*1,0)

        self.Entity:PhysicsInit(SOLID_VPHYSICS);
        self.Entity:SetMoveType(MOVETYPE_VPHYSICS);
        self.Entity:SetSolid(SOLID_VPHYSICS);
        self.Entity:SetCollisionGroup(COLLISION_GROUP_PROJECTILE)
        self.Damage = 1
        local phys = self:GetPhysicsObject()
        if phys:IsValid() then
            phys:Wake()
            phys:EnableGravity(true);
        end

    end

    function ENT:Use( activator, caller )
        self.Entity:Remove()
        if ( activator:IsPlayer() ) then
            if activator:Health() < 149 then
                if self.lower then
                    activator:SetHealth(activator:Health()+25)
                    if activator:Health() > 150 then
                        activator:SetHealth(150)
                    end
                else
                    activator:SetHealth(activator:Health()+50)
                    if activator:Health() > 150 then
                        activator:SetHealth(150)
                    end
                end
            end
            activator:EmitSound("crisps/eat.mp3")
        end
    end


end

scripted_ents.Register( ENT, "platypus_destroy_health", true )