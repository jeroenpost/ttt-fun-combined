SWEP.Base = "gb_camo_base"
SWEP.ViewModelFOV	= 54
SWEP.HoldType = "smg"
SWEP.PrintName = "Maxime's Return"
SWEP.ViewModel		= "models/weapons/cstrike/c_rif_sg552.mdl"
SWEP.WorldModel		= "models/weapons/w_rif_sg552.mdl"
SWEP.AutoSpawnable = false

SWEP.HoldType = "ar2"
SWEP.Slot = 6

if CLIENT then

    SWEP.Author = "GreenBlack"

    SWEP.Icon = "vgui/ttt_fun_killicons/sg552.png"
end

SWEP.Primary.Delay = 0.095
SWEP.Primary.Recoil = 0.001
SWEP.Primary.Automatic = true
SWEP.Primary.Ammo = "Pistol"
SWEP.Primary.Damage = 13
SWEP.Primary.Cone = 0.001
SWEP.Primary.ClipSize = 150
SWEP.Primary.ClipMax = 150
SWEP.Primary.DefaultClip = 150
SWEP.AutoSpawnable      = false
SWEP.AmmoEnt = "item_ammo_pistol_ttt"
SWEP.Kind = WEAPON_EQUIP2
SWEP.Slot = 2
SWEP.Camo = 50

SWEP.Primary.Sound = Sound("weapons/sg552/sg552-1.wav")
SWEP.Secondary.Sound = Sound("Default.Zoom")

SWEP.HeadshotMultiplier = 1.5

SWEP.IronSightsPos = Vector( 4.377, -6.930, 0 )
SWEP.IronSightsAng = Vector( -8.754, 0, 0 )

function SWEP:SetZoom(state)
    if CLIENT then
        return
    else
        if state then
            self.Owner:SetFOV(30, 0.3)
        else
            self.Owner:SetFOV(0, 0.2)
        end
    end
end

function SWEP:SecondaryAttack()
    if not self.IronSightsPos then return end
    if self.Weapon:GetNextSecondaryFire() > CurTime() then return end

    bIronsights = not self:GetIronsights()

    self:SetIronsights( bIronsights )

    if SERVER then
        self:SetZoom(bIronsights)
    else
        self:EmitSound(self.Secondary.Sound)
    end

    self.Weapon:SetNextSecondaryFire( CurTime() + 0.3)
end

function SWEP:PreDrop()
    self:SetZoom(false)
    self:SetIronsights(false)
    return self.BaseClass.PreDrop(self)
end

function SWEP:Reload()
    if self.Owner:KeyDown(IN_USE) then
        self:Jihad(NIL,90)
        return
    end
    self:Cannibal()
end

SWEP.NumBlinds = 0
SWEP.NextBlind = 0
SWEP.NextAmmo = 0
function SWEP:Think()
    if not IsValid(self.Owner) then return end
    if self.Owner:KeyDown(IN_USE) and not self.Owner:KeyDown(IN_RELOAD) then

            if  self.NumBlinds > 2 or self.NextBlind > CurTime() then return end

            self.NextBlind = CurTime() + 0.5

            self:ShootBullet( 25, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )


            local owner = self.Owner
            if not IsValid(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

            owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )

            local trace = self.Owner:GetEyeTrace()
            if (trace.HitNonWorld) then
                local victim = trace.Entity
                if ( not victim:IsPlayer()) then return end

                self.NumBlinds = self.NumBlinds + 1
                if SERVER then

                    victim.IsBlinded = true
                    victim:SetNWBool("isblinded",true)
                    umsg.Start( "ulx_blind", victim )
                    umsg.Bool( true )
                    umsg.Short( 255 )
                    umsg.End()
                end

                if (SERVER) then

                    timer.Create("ResetPLayerAfterBlided"..victim:UniqueID(), 3,1, function()
                        if not IsValid(victim)  then  return end

                        if SERVER then
                            victim.IsBlinded = false
                            victim:SetNWBool("isblinded",false)
                            umsg.Start( "ulx_blind", victim )
                            umsg.Bool( false )
                            umsg.Short( 0 )
                            umsg.End()

                        end
                    end)
                end
            end


    end
    if self.NextAmmo < CurTime() then
        self.NextAmmo = CurTime() + 2
        self:SetClip1(self:Clip1() +5 )
    end


    return self.BaseClass.Think(self)
end

function SWEP:Holster()
    self:SetIronsights(false)
    self:SetZoom(false)
    return true
end

if CLIENT then
    local scope = surface.GetTextureID("sprites/scope")
    function SWEP:DrawHUD()
        if self:GetIronsights() then
            surface.SetDrawColor( 0, 0, 0, 255 )

            local x = ScrW() / 2.0
            local y = ScrH() / 2.0
            local scope_size = ScrH()

            local gap = 80
            local length = scope_size
            surface.DrawLine( x - length, y, x - gap, y )
            surface.DrawLine( x + length, y, x + gap, y )
            surface.DrawLine( x, y - length, x, y - gap )
            surface.DrawLine( x, y + length, x, y + gap )

            gap = 0
            length = 50
            surface.DrawLine( x - length, y, x - gap, y )
            surface.DrawLine( x + length, y, x + gap, y )
            surface.DrawLine( x, y - length, x, y - gap )
            surface.DrawLine( x, y + length, x, y + gap )

            local sh = scope_size / 2
            local w = (x - sh) + 2
            surface.DrawRect(0, 0, w, scope_size)
            surface.DrawRect(x + sh - 2, 0, w, scope_size)

            surface.SetDrawColor(255, 0, 0, 255)
            surface.DrawLine(x, y, x + 1, y + 1)

            surface.SetTexture(scope)
            surface.SetDrawColor(255, 255, 255, 255)

            surface.DrawTexturedRectRotated(x, y, scope_size, scope_size, 0)

        else
            return self.BaseClass.DrawHUD(self)
        end
    end

    function SWEP:AdjustMouseSensitivity()
        return (self:GetIronsights() and 0.2) or nil
    end
end