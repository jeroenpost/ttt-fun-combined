SWEP.Base = "gb_camo_base"
SWEP.ViewModelFOV	= 54
SWEP.HoldType = "pistol"
SWEP.Icon = "vgui/ttt_fun_killicons/fiveseven.png"
SWEP.PrintName = "Ultimatum"
SWEP.ViewModel		= "models/weapons/cstrike/c_pist_fiveseven.mdl"
SWEP.WorldModel		= "models/weapons/w_pist_fiveseven.mdl"
SWEP.Kind = WEAPON_NADE
SWEP.UseHands = true
SWEP.Slot = 3
SWEP.AutoSpawnable = false
SWEP.Primary.Recoil	= 1.5
SWEP.Primary.Damage = 18
SWEP.Primary.Delay = 0.07
SWEP.Primary.Cone = 0.02
SWEP.Primary.ClipSize = 100
SWEP.Primary.Automatic = false
SWEP.Primary.DefaultClip = 100
SWEP.Primary.ClipMax = 60
SWEP.Primary.Ammo = "Pistol"
SWEP.AutoSpawnable = false
SWEP.HeadshotMultiplier= 1.1
SWEP.AmmoEnt = "item_ammo_pistol_ttt"

SWEP.Primary.Sound = Sound( "Weapon_FiveSeven.Single" )
SWEP.IronSightsPos = Vector(-5.95, -4, 2.799)
SWEP.IronSightsAng = Vector(0, 0, 0)

function SWEP:SecondaryAttack()
    self:Fly()
end

function SWEP:Reload()
    if self:Clip1() < 1 then
        self.BaseClass.Reload(self)
    end
    self:rFly()
end