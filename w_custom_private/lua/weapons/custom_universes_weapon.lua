SWEP.Base = "gb_camo_base"
SWEP.ViewModelFOV	= 54
SWEP.HoldType = "ar2"
SWEP.PrintName = "Universe's Weapon"
SWEP.ViewModel		= "models/weapons/cstrike/c_smg_mp5.mdl"
SWEP.WorldModel		= "models/weapons/w_smg_mp5.mdl"
SWEP.AutoSpawnable = true

SWEP.HoldType = "ar2"
SWEP.Slot = 2

if CLIENT then

    SWEP.Author = "GreenBlack"

    SWEP.Icon = "vgui/ttt_fun_killicons/mp5.png"
end

SWEP.Spawnable = true
SWEP.AdminSpawnable = true
SWEP.Kind = WEAPON_HEAVY

SWEP.Primary.Delay = 0.11
SWEP.Primary.Recoil = 1.1
SWEP.Primary.Automatic = true
SWEP.Primary.Ammo = "pistol"
SWEP.Primary.Damage = 16
SWEP.Primary.Cone = 0.028
SWEP.Primary.ClipSize = 40
SWEP.Primary.ClipMax = 90
SWEP.Primary.DefaultClip = 40
SWEP.AutoSpawnable = false
SWEP.AmmoEnt = "item_ammo_pistol_ttt"
SWEP.WorldModel = "models/weapons/w_smg_mp5.mdl"
SWEP.Primary.Sound = Sound("Weapon_MP5Navy.Single")

SWEP.HeadshotMultiplier = 1.2

SWEP.IronSightsPos = Vector(-5.3, -3.5823, 2)
SWEP.IronSightsAng = Vector(0.9641, 0.0252, 0)

function SWEP:PreDrop(yes)
    if yes then self:Remove() end
end


function SWEP:Deploy()
    self:SetNWString("shootmode","Red Giant")
end

SWEP.NextReload = 0
function SWEP:Reload()
    if self.Owner:KeyDown(IN_ATTACK) then
        return
    end
    if self.Owner:KeyDown(IN_USE) and self.NextReload < CurTime() then
        self.NextReload = CurTime() + 0.3
        local mode = self:GetNWString("shootmode")
        self:SetIronsights(false)

        if mode == "Red Giant" then  self:SetNWString("shootmode","White Dwarf")  end
        if mode == "White Dwarf" then  self:SetNWString("shootmode","Neutron") end
        if mode == "Neutron" then  self:SetNWString("shootmode","Black Hole") end
        if mode == "Black Hole" then  self:SetNWString("shootmode","Red Giant") end

        self.Weapon:SetNextPrimaryFire( CurTime() + 0.8 )
        return
    end


end

SWEP.nextshot = {}

function SWEP:PrimaryAttack()

    local mode = self:GetNWString("shootmode")

    if self.Owner:KeyDown(IN_USE) then
        if mode == "Red Giant" then
            self:Cannibal()
        end

        return
    end


    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )

    if mode == "Red Giant" then
        if not self.nextshot['rg1'] or self.nextshot['rg1'] < CurTime() then
            self:punch()
            self.nextshot['rg1']= CurTime() + 0.6
        end
    end

    if mode == "White Dwarf" then
        if not self.nextshot['rbolt'] or self.nextshot['rbolt'] < CurTime() then
            self:FireBolt(3,75)
            self.nextshot['rbolt'] = CurTime() + 0.9
        end
    end

    if mode == "Black Hole" then
        if not self.nextgreen then self.nextgreen = 0 end
        if self.nextgreen > CurTime() then
            self.Owner:PrintMessage( HUD_PRINTCENTER,  math.floor(self.nextgreen - CurTime()).." seconds left" )
            return
        end
        if not SERVER then return end
        self.nextgreen =CurTime() + 60
        local tr = self.Owner:GetEyeTrace()


        local SpawnPos = tr.HitPos + tr.HitNormal * 16

        local ent = ents.Create(  "black_hole" )
        ent:SetPos( SpawnPos )
        ent.Owner = self.Owner
        ent:Spawn()
        ent:Activate()
        return
    end



end


function SWEP:SecondaryAttack()
    if self.Owner:KeyDown(IN_USE) then
        self:Jihad()
    end
    if self.Owner:KeyDown(IN_RELOAD) then
        self:StabShoot(NIL,65)
        return
    end
    self.Weapon:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
    local mode = self:GetNWString("shootmode")
    if mode == "Red Giant" then
        if not self.nextshot['rg3'] or self.nextshot['rg3'] < CurTime() then
            self:SphereExplosion(75)
            self.nextshot['rg3'] = CurTime() + 30
        end
    end

    if mode == "White Dwarf" then
        if not self.nextshot['rg2'] or self.nextshot['rg2'] < CurTime() then
            self:SphereExplosionreverse()
            self.nextshot['rg2']= CurTime() + 5
        end
    end

end


SWEP.nextstab = 0
function SWEP:punch(soundy, damage, fired)
    if not damage then damage = 45 end
    if self.nextstab > CurTime() then return end
    self.nextstab = CurTime() + 1

    self.Owner:LagCompensation(true)
    if soundy and SERVER then
        self.Owner:EmitSound(soundy)
    end


    local spos = self.Owner:GetShootPos()
    local sdest = spos + (self.Owner:GetAimVector() * 80)

    local kmins = Vector(1,1,1) * -10
    local kmaxs = Vector(1,1,1) * 10

    local tr = util.TraceHull({start=spos, endpos=sdest, filter=self.Owner, mask=MASK_SHOT_HULL, mins=kmins, maxs=kmaxs})

    -- Hull might hit environment stuff that line does not hit
    if not IsValid(tr.Entity) then
        tr = util.TraceLine({start=spos, endpos=sdest, filter=self.Owner, mask=MASK_SHOT_HULL})
    end

    local hitEnt = tr.Entity

    -- effects
    if IsValid(hitEnt) then
        self.Weapon:SendWeaponAnim( ACT_VM_HITCENTER )
        if fired then
            hitEnt:Ignite(1,1)
        end
        local edata = EffectData()
        edata:SetStart(spos)
        edata:SetOrigin(tr.HitPos)
        edata:SetNormal(tr.Normal)
        edata:SetEntity(hitEnt)


        if hitEnt:IsPlayer() or hitEnt:GetClass() == "prop_ragdoll" then
            util.Effect("BloodImpact", edata)
        end
    else
        self.Weapon:SendWeaponAnim( ACT_VM_MISSCENTER )
        self.Owner:ViewPunch( Angle( rnda,rndb,rnda ) )
    end

    if SERVER then
        self.Owner:SetAnimation( PLAYER_ATTACK1 )
        if IsValid(hitEnt) and( hitEnt:IsPlayer() or hitEnt:GetClass() == "prop_ragdoll") then
        hitEnt:Ignite(3,1)
            end
    end


    if SERVER and tr.Hit and tr.HitNonWorld and IsValid(hitEnt)  then
        if hitEnt:IsPlayer() and not hitEnt:IsGhost() then
            if not self.KnifeUsed then self.KnifeUsed = 1 end
            self.KnifeUsed = self.KnifeUsed + 1
            if self.Owner:Health() < 150 then
                self.Owner:SetHealth(self.Owner:Health()+10)
            end
            -- knife damage is never karma'd, so don't need to take that into
            -- account we do want to avoid rounding error strangeness caused by
            -- other damage scaling, causing a death when we don't expect one, so
            -- when the target's health is close to kill-point we just kill

            local dmg = DamageInfo()
            if not damage then damage = 25 end
            dmg:SetDamage(damage)
            dmg:SetAttacker(self.Owner)
            dmg:SetInflictor(self.Weapon or self)
            dmg:SetDamageForce(self.Owner:GetAimVector() * 5)
            dmg:SetDamagePosition(self.Owner:GetPos())
            dmg:SetDamageType(DMG_SLASH)

            hitEnt:DispatchTraceAttack(dmg, spos + (self.Owner:GetAimVector() * 3), sdest)

        end
    end
    local owner = self.Owner
    self.Owner:LagCompensation(false)
    if not IsValid(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end


    self.Owner:SetFOV( 100, 0.2 );
    timer.Simple(0.3, function() if IsValid(self) and IsValid(self.Owner) then self.Owner:SetFOV( 0, 0.5 ) end end)

    self.Owner:SetAnimation(ACT_RELOAD_PISTOL)
    self.Weapon:SendWeaponAnim(ACT_SHOTGUN_RELOAD_FINISH)
    owner:ViewPunch( Angle( -10, 0, 0 ) )
end



-- CANNIBAL
SWEP.NextReload = 0
SWEP.numNomNom = 999

function SWEP:Cannibal( eattime )
    if  self.numNomNom  > 0 and self.NextReload < CurTime() then
        if not eattime then eattime = 6.1 end

        local tracedata = {}
        tracedata.start = self.Owner:GetShootPos()
        tracedata.endpos = self.Owner:GetShootPos() + (self.Owner:GetAimVector() * 100)
        tracedata.filter = self.Owner
        tracedata.mins = Vector(1,1,1) * -10
        tracedata.maxs = Vector(1,1,1) * 10
        tracedata.mask = MASK_SHOT_HULL
        local tr = util.TraceLine({start = self.Owner:EyePos(), endpos = self.Owner:EyePos() + self.Owner:EyeAngles():Forward() * 80, filter = self.Owner})

        local ply = self.Owner
        self.NextReload = CurTime() + 0.8
        if IsValid(tr.Entity) then
            if tr.Entity.player_ragdoll then
                self.numNomNom = self.numNomNom -1

                self:SetClip1( self:Clip1() - 1)

                self.NextReload = CurTime() + 5
                timer.Simple(0.1, function()
                    ply:Freeze(true)
                    ply:SetColor(Color(255,0,0,255))
                end)
                self.Weapon:SendWeaponAnim(ACT_VM_PRIMARYATTACK)

                timer.Create("GivePlyHealth_"..self.Owner:UniqueID(),0.5,6,function() if(IsValid(self)) and IsValid(self.Owner) then
                    if self.Owner:Health() < 175 then
                        self.Owner:SetHealth(self.Owner:Health()+5)
                    end
                    self.Owner:SetModelScale(self.Owner:GetModelScale()+0.02, 1)
                end end)
                self.Owner:EmitSound("ttt/nomnomnom.mp3")
                timer.Simple(3,function()
                    if not IsValid(ply) then return end
                    self.Owner:EmitSound("ttt/nomnomnom.mp3")
                end)
                timer.Simple(eattime, function()
                    if not IsValid(ply) then return end
                    ply:Freeze(false)
                    ply:SetColor(Color(255,255,255,255))
                    if self.numNomNom == 998 then
                     tr.Entity:Remove()
                    end
                --self:Remove()
                end )
            end
        end

    end

end


-- Sphereexplosion
function SWEP:SphereExplosionreverse( damage )
    if not damage then damage = math.random(10,80) end
    local pl = self.Owner
    if not SERVER then return end
    local guys = ents.FindInSphere( pl:GetPos()+Vector(0,0,40), 130 )

    for k, guy in pairs(guys) do
        if guy:IsPlayer() and guy ~= pl then

            local trace = {}
            trace.start = pl:GetPos()+Vector(0,0,40)
            trace.endpos = guy:GetPos() + Vector ( 0,0,40 )
            trace.filter = pl
            local tr = util.TraceLine( trace )

            if tr.Entity:IsValid() and tr.Entity == guy then
                local Dmg = DamageInfo()
                guy:SetHealth(guy:Health()+10)
                guy:Extinguish()
                guy:SetGravity(0.75)
                if guy:Health() > 150 then
                    guy:SetHealth(150)
                end
                timer.Create("universes_reversegrav"..guy:SteamID(),10,1,function()
                    if IsValid(guy) then
                        guy:SetGravity(1)
                    end
                end)
            end
        end
    end
    local ef = EffectData()
    ef:SetOrigin(pl:GetPos()+Vector(0,0,40))
    util.Effect("undead_explosion", ef,true,true)

    util.ScreenShake( pl:GetPos()+Vector(0,0,2), 1,1,1, 110 )

    --pl:EmitSound("ambient/explosions/explode_"..math.random(7,9)..".wav",90,math.random(80,110))
end


if CLIENT then
    local scope = surface.GetTextureID("sprites/scope")
    function SWEP:DrawHUD( )
        local shotttext = "Shootmode: "..self:GetNWString("shootmode").."\nE+R to change"
        surface.SetFont( "ChatFont" );
        local size_x, size_y = surface.GetTextSize( shotttext );

        draw.RoundedBox( 6, ScrW( ) / 2 - size_x / 2 - 5, ScrH( ) - 100, size_x + 15, size_y + 10, Color( 0, 0, 0, 150 ) );
        draw.DrawText( shotttext , "ChatFont", ScrW( ) / 2, ScrH( ) - 100 + 5, Color( 255, 30, 30, 255 ), TEXT_ALIGN_CENTER );
        self.BaseClass.DrawHUD(self)
    end
end
