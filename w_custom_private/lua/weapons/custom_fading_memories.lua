include "weapons/weapon_zm_shotgun.lua"
SWEP.Base = "gb_camo_base"
SWEP.ViewModelFOV	= 54
SWEP.HoldType = "smg"
SWEP.PrintName = "Murder of Crows"
SWEP.ViewModel		= "models/weapons/c_shotgun.mdl"
SWEP.WorldModel		= "models/weapons/w_shotgun.mdl"
SWEP.Camo = 0

SWEP.HoldType = "shotgun"
SWEP.Slot = 6
SWEP.Kind = WEAPON_EQUIP

if CLIENT then

    SWEP.Author = "GreenBlack"

    SWEP.Icon = "vgui/ttt_fun_killicons/xm1014.png"
end

SWEP.Primary.Sound			= Sound( "Weapon_M3.Single" )
SWEP.Primary.Recoil			= 2

SWEP.Primary.Ammo = "Buckshot"
SWEP.Primary.Damage = 2
SWEP.Primary.Cone = 0.12
SWEP.Primary.Delay = 0.1
SWEP.Primary.ClipSize = 1000
SWEP.Primary.ClipMax = 1000
SWEP.Primary.DefaultClip = 1000
SWEP.Primary.Automatic = false
SWEP.Primary.NumShots = 12
SWEP.AutoSpawnable      = false
SWEP.AmmoEnt = "item_box_buckshot_ttt"
SWEP.Secondary.Automatic = true
function SWEP:PreDrop(yes)
    if yes then
     self:Remove()
    end
end

function SWEP:Deploy()
    self.dt.reloading = false
    self.reloadtimer = 0
    return self:DeployFunction()
end

function SWEP:SecondaryAttack()

    self.Weapon:SetNextSecondaryFire( CurTime() + 0.4 )
    self.Weapon:SetNextPrimaryFire( CurTime() + 0.4 )

    if not self:CanSecondaryAttack() then return end

    if not worldsnd then
        self.Owner:EmitSound( Sound("weapons/galil/galil-1.wav"), self.Primary.SoundLevel )
    elseif SERVER then
        sound.Play(Sound("weapons/galil/galil-1.wav"), self:GetPos(), self.Primary.SoundLevel)
    end

    self:ShootBullet( 35, self.Primary.Recoil, 1, 0.012)

    self:TakePrimaryAmmo( 1 )

    local owner = self.Owner
    if not IsValid(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

    owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )

end