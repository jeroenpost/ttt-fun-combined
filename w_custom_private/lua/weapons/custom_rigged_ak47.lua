SWEP.Base = "aa_base"
SWEP.ViewModelFOV	= 54
SWEP.HoldType = "smg"
SWEP.PrintName = "AK 47"
SWEP.ViewModel		= "models/weapons/cstrike/c_rif_ak47.mdl"
SWEP.WorldModel		= "models/weapons/w_rif_ak47.mdl"
SWEP.AutoSpawnable = false
SWEP.Icon = "vgui/ttt_fun_killicons/ak47.png"
SWEP.Kind = WEAPON_EQUIP2
SWEP.Slot = 6
SWEP.Primary.Delay = 0.28
SWEP.Primary.Recoil = 1.0
SWEP.Primary.Automatic = true
SWEP.Primary.Ammo = "Pistol"
SWEP.Primary.Damage = 30
SWEP.Primary.Cone = 0.018
SWEP.Primary.ClipSize = 300
SWEP.Primary.ClipMax = 900
SWEP.Primary.DefaultClip = 900
SWEP.AmmoEnt = "item_ammo_pistol_ttt"
SWEP.Primary.Sound = Sound("weapons/ak47/ak47-1.wav")
SWEP.UseHands = true
SWEP.ViewModelFlip= false
SWEP.HeadshotMultiplier = 1.5

SWEP.IronSightsPos = Vector( -6.6, -8.50, 3.40 )
SWEP.IronSightsAng = Vector( 0, 0, 0 )

SWEP.NextReload = 0
function SWEP:Reload()
    if self.Owner:KeyDown(IN_ATTACK)then
        self:ZedTime()
        return
    end
    if self.Owner:KeyDown(IN_USE) and SERVER and self.NextReload < CurTime() then
        self.NextReload = CurTime() + 0.3
        local mode = self:GetNWString("shootmode","Potato Power")
        if mode == "AK" then self:SetNWString("shootmode","Potato Power") end
        if mode == "Potato Power" then self:SetNWString("shootmode","Rats Power") end
        if mode == "Rats Power" then self:SetNWString("shootmode","AK") end
        return
    end

  --  self.BaseClass.Reload(self)
end

function SWEP:Deploy()
    local ply = self.Owner

    hook.Add("PlayerDeath", "sandbootsdie"..ply:SteamID(),function( victim, weapon, killer )
        if  not ply:IsGhost() and SERVER and killer == ply and not  ply.isdisguised  then
            gb_PlaySoundFromServer(gb_config.websounds.."rats_acdc.mp3", killer)
        end
    end)
end

SWEP.NextSuperman= 0
function SWEP:SecondaryAttack()
    self.Weapon:SetNextSecondaryFire( CurTime() + 1 )
    if self.Owner:KeyDown(IN_USE) then
        if  self.NextSuperman < CurTime() then
            self.NextSuperman = CurTime() + 5

            self.Owner:SetVelocity(self.Owner:GetForward() * 1500 + Vector(0,0,1500))
            self.hasSuperman = true

        end
        return
    end
    local mode = self:GetNWString("shootmode","Potato Power")

    if mode ~= "hax" then
        if CLIENT then return end
        self:SetNextSecondaryFire(CurTime()+1)
        self.Owner:EmitSound("weapons/iceaxe/iceaxe_swing1.wav")
        self:ShootEffects()
        local ent = ents.Create("rats_disk")
        local pos = self.Owner:GetEyeTrace().HitPos  + Vector(0,0,48)
        ent:SetPos(pos)
        ent:SetAngles(self.Owner:EyeAngles())
        ent:SetPhysicsAttacker( self.Owner )
        --ent.owner = self
        ent:Spawn()
        ent:Activate()
        ent:SetOwner(self.Owner)
        ent.owner = self.Owner
        ent:SetPhysicsAttacker(self.Owner)

        timer.Simple(30,function()
            if IsValid(ent) then ent:Remove() end
        end)
    end

end
SWEP.NextFire2 = 0
function SWEP:PrimaryAttack()
    if self.Owner:KeyDown(IN_USE) then
            if not self.nextgreen then self.nextgreen = 0 end
            if self.nextgreen > CurTime() then
                self.Owner:PrintMessage( HUD_PRINTCENTER,  math.floor(self.nextgreen - CurTime()).." seconds left" )
                return
            end
            local tr = self.Owner:GetEyeTrace()
            self.LaserColor = Color(0,255,0,255)
            self:ShootTracer("LaserTracer_thick")
            self.nextgreen = CurTime() + 60
            if CLIENT then return end
            local ent = ents.Create( "cse_ent_shorthealthgrenade" )
            ent:SetPos( tr.HitPos  )
            ent:SetOwner( self.Owner  )
            ent:SetPhysicsAttacker(  self.Owner )
            ent.Owner = self.Owner
            ent:Spawn()
            ent.timer = CurTime() + 0.01
            ent.EndColor = "0 0 0"
            ent.weapon = self
            ent.StartColor = "0 255 0"
            gb_PlaySoundFromServer(gb_config.websounds.."rats_acdc.mp3", ent)
            timer.Simple(5,function()
                if IsValid(ent) then
                    ent.Bastardgas:Remove()
                    ent.Entity:Remove()
                end
            end)
        return
    end
    local mode = self:GetNWString("shootmode","Potato Power")
    if mode == "Potato Power" then


        self.BaseClass.PrimaryAttack(self)
        self:throw_attack("riggedak47potato",10000)
        return
    end

    if mode == "Rats Power" then
        self.BaseClass.PrimaryAttack(self)
        local ent = self:throw_attack("riggedak47skull",10000)
        if CLIENT then return end
        ent.Owner = self.Owner
        return
    end


    self.BaseClass.PrimaryAttack(self)
end





local sndPowerUp		= Sound("rope_hit.mp3")
local sndPowerDown		= Sound ("shoot_rope.mp3")
local sndTooFar			= Sound ("to_far.mp3")

function SWEP:Initialize()

    nextshottime = CurTime()

    self.BaseClass.Initialize(self)
end


SWEP.NextAmmoGive = 0
SWEP.NextAmmoTake = 0
SWEP.NextDisable = 0
SWEP.NextFire2 = 0
function SWEP:Think()

    if self.NextAmmoGive < CurTime() && self:Clip1() < 10 then
    self.NextAmmoGive = CurTime() + 3
    self:SetClip1( self:Clip1() + 1 )
    end

    if (!self.Owner || self.Owner == NULL) or self:Clip1() < 1 then

    if ( !self.Beam ) then return end
    if( IsValid( self.Beam) ) then
        self.Beam:Remove()
    end
    self.Beam = nil
    return
    end






    if ( self.Owner:KeyPressed( IN_ATTACK2 ) and self.Owner:KeyDown(IN_USE) ) then

        self:StartAttack()

    elseif ( self.Owner:KeyDown( IN_ATTACK2 ) ) then

        self:UpdateAttack()


    elseif ( self.Owner:KeyReleased( IN_ATTACK2 ) ) then

        self:EndAttack( true )

    end


    self.BaseClass.Think(self)

end

function SWEP:DoTrace( endpos )
    local trace = {}
    trace.start = self.Owner:GetShootPos()
    trace.endpos = trace.start + (self.Owner:GetAimVector() * 14096) --14096 is length modifier.
    if(endpos) then trace.endpos = (endpos - self.Tr.HitNormal * 7) end
    trace.filter = { self.Owner, self.Weapon }

    self.Tr = nil
    self.Tr = util.TraceLine( trace )
end


function SWEP:StartAttack()


    if not self:CanPrimaryAttack() then return end

    --Get begining and end poins of trace.
    local gunPos = self.Owner:GetShootPos() --Start of distance trace.
    local disTrace = self.Owner:GetEyeTrace() --Store all results of a trace in disTrace.
    local hitPos = disTrace.HitPos --Stores Hit Position of disTrace.

    --Calculate Distance
    --Thanks to rgovostes for this code.
    local x = (gunPos.x - hitPos.x)^2;
    local y = (gunPos.y - hitPos.y)^2;
    local z = (gunPos.z - hitPos.z)^2;
    local distance = math.sqrt(x + y + z);

    --Only latches if distance is less than distance CVAR
    local distanceCvar = GetConVarNumber("grapple_distance")
    inRange = false
    if distance <= distanceCvar then
        inRange = true
    end

    if inRange then
        if (SERVER) then

            if (!self.Beam) then --If the beam does not exist, draw the beam.
            if not self:CanPrimaryAttack() then return end
            --grapple_beam
            self.Beam = ents.Create( "trace1" )
            self.Beam:SetPos( self.Owner:GetShootPos() )
            self.Beam:Spawn()
            self:TakePrimaryAmmo(1)

            end

            self.Beam:SetParent( self.Owner )
            self.Beam:SetOwner( self.Owner )

        end


        self:DoTrace()
        self.speed = 10000 --Rope latch speed. Was 3000.
        self.startTime = CurTime()
        self.endTime = CurTime() + self.speed
        self.dtfix = -1

        if (SERVER && self.Beam) then
        self.Beam:GetTable():SetEndPos( self.Tr.HitPos )
        end

        self:UpdateAttack()

        self.Weapon:EmitSound( sndPowerDown )
    else
        --Play A Sound
        self.Weapon:EmitSound( sndTooFar )
    end
end

function SWEP:UpdateAttack()

    -- if not self:CanPrimaryAttack() then return end
    if !self.Tr or self:Clip1() < 1 then return end
    self.Owner:LagCompensation( true )


    if (!endpos) then endpos = self.Tr.HitPos end

    if (SERVER && self.Beam) then
    self.Beam:GetTable():SetEndPos( endpos )
    end

    lastpos = endpos



    if ( self.Tr.Entity:IsValid() ) then

        endpos = self.Tr.Entity:GetPos()
        if ( SERVER and IsValid(self.Beam) ) then
            self.Beam:GetTable():SetEndPos( endpos )
        end

    end

    local vVel = (endpos - self.Owner:GetPos())
    local Distance = endpos:Distance(self.Owner:GetPos())

    local et = (self.startTime + (Distance/self.speed))
    if(self.dtfix != 0) then
    self.dtfix = (et - CurTime()) / (et - self.startTime)
    end
    if(self.dtfix < 0) then
        self.Weapon:EmitSound( sndPowerUp )
        self.dtfix = 0
    end

    if(self.dtfix == 0) then
        zVel = self.Owner:GetVelocity().z
        vVel = vVel:GetNormalized()*(math.Clamp(Distance,0,25))
        if( SERVER ) then
            if  self.NextAmmoTake < CurTime() then
                self:TakePrimaryAmmo(1)
                self.NextAmmoTake = CurTime() + 1
                self.NextAmmoGive = CurTime() + 1.2
            end

            local gravity = GetConVarNumber("sv_gravity")

            vVel:Add(Vector(0,0,(gravity/50)*1.5))

            if(zVel < 0) then
                vVel:Sub(Vector(0,0,zVel/100))
            end
            self.Owner:SetVelocity(vVel)
        end
    end

    endpos = nil

    self.Owner:LagCompensation( false )

end

function SWEP:EndAttack( shutdownsound )

    if ( shutdownsound ) then
        self.Weapon:EmitSound( sndPowerDown )
    end

    if ( CLIENT ) then return end
    if ( !self.Beam ) then return end
    if( IsValid( self.Beam) ) then
        self.Beam:Remove()
    end
    self.Beam = nil

end


function SWEP:Holster()
    self:EndAttack( false )
    gb_StopSoundFromServer("magical_magic")
    return true
end

function SWEP:OnRemove()
    self:EndAttack( false )
    gb_StopSoundFromServer("magical_magic")
    return true
end

local ENT = {}

ENT.Type = "anim"
ENT.Base = "base_anim"



if SERVER then

    function ENT:Initialize()

        self.Hit = false
        self.LastShout = CurTime()
        self.CurrentPitch = 100
        self.SpawnDelay = CurTime() + 0.5
        --damage self.Damage = 75
        self:SetModel("models/props_phx/misc/potato.mdl")
        self:SetModelScale(1.5,0.01)
        --self:SetMaterial("phoenix_storms/plastic" )
        --self:SetColor(Color(0, 255, 0,255))
        self:PhysicsInitBox(Vector(-4,-4,-4),Vector(4,4,4))
        self:SetMoveType(MOVETYPE_VPHYSICS)
        self:SetSolid(SOLID_VPHYSICS)
        self:SetTrigger(true)
        self.Mag = 30

        self:DrawShadow(false)
        -- self:SetModel( "models/weapons/w_rif_famas.mdl")

        local phys = self:GetPhysicsObject()
        if (phys:IsValid()) then
            phys:Wake()
            phys:EnableDrag(false)
            phys:AddGameFlag(FVPHYSICS_NO_IMPACT_DMG)
            --    phys:SetBuoyancyRatio(0)
        end

    end

    function ENT:Explode()
        if self.exploded then return end self.exploded = true

        if SERVER then self:Remove() end
    end


    function ENT:Touch(ent)
        if ent != self.Owner then
        self:Explode()
        end
    end


end

scripted_ents.Register( ENT, "riggedak47potato", true )

local ENT = {}

ENT.Type = "anim"
ENT.Base = "base_anim"



if SERVER then

    function ENT:Initialize()

        self.Hit = false
        self.LastShout = CurTime()
        self.CurrentPitch = 100
        self.SpawnDelay = CurTime() + 0.5
        --damage self.Damage = 75
        self:SetModel("models/Gibs/HGIBS.mdl")
        self:SetModelScale(1.5,0.01)
        --self:SetMaterial("phoenix_storms/plastic" )
        --self:SetColor(Color(0, 255, 0,255))
        self:PhysicsInitBox(Vector(-4,-4,-4),Vector(4,4,4))
        self:SetMoveType(MOVETYPE_VPHYSICS)
        self:SetSolid(SOLID_VPHYSICS)
        self:SetTrigger(true)
        self.Mag = 30

        self:DrawShadow(false)
        -- self:SetModel( "models/weapons/w_rif_famas.mdl")

        local phys = self:GetPhysicsObject()
        if (phys:IsValid()) then
            phys:Wake()
            phys:EnableDrag(false)
            phys:AddGameFlag(FVPHYSICS_NO_IMPACT_DMG)
            --    phys:SetBuoyancyRatio(0)
        end

    end


    function ENT:Touch( ent )
        if self.exploded then return end
        if ent:IsPlayer() and not ent.nextsandtouch or (ent.nextsandtouch and ent.nextsandtouch < CurTime()) then
            ent.nextsandtouch = CurTime() + 3
            ent:Ignite(0.5,0)
            local dmginfo = DamageInfo()
            dmginfo:SetDamage( 10 ) --50 damage
            dmginfo:SetDamageType( DMG_BULLET ) --Bullet damage
            dmginfo:SetAttacker( self.Owner or ent ) --First player found gets credit
            dmginfo:SetDamageForce( Vector( 0, 0, 50 ) ) --Launch upwards
            dmginfo:SetInflictor(self.Owner or self)
            ent:TakeDamageInfo( dmginfo )
            self:Explode()
        end
    end

    ENT.exploded = false
    function ENT:Explode()

        if self.exploded or not IsValid(self.Owner) then return end
        self.exploded = true
        --util.BlastDamage(self, self.Owner, self:GetPos(), 1, 70)
        local effect = EffectData()
        effect:SetStart(self:GetPos())
        effect:SetOrigin(self:GetPos())
        effect:SetScale(1)
        effect:SetRadius(1)
        effect:SetMagnitude(1)

        util.Effect("Explosion", effect, true, true)
        timer.Simple(0.01,function() self:Remove() end)

    end


    function ENT:Touch(ent)
        if IsValid(self.Owner) and  ent != self.Owner then
        self:Explode()
        end
    end


end

scripted_ents.Register( ENT, "riggedak47skull", true )


local ENT = {}

ENT.Type = "anim"
ENT.Base = "base_anim"


if SERVER then

    function ENT:Initialize()


        self:SetModel('models/workspizza03/workspizza03.mdl')
        self:SetModelScale(self:GetModelScale() * 0.5, 0)
        --  self:SetMaterial("models/debug/debugwhite")
       -- self:SetColor(Color(0, 255, 0))

        self.Entity:PhysicsInit(SOLID_VPHYSICS);
        self.Entity:SetMoveType(MOVETYPE_VPHYSICS);
        self.Entity:SetSolid(SOLID_VPHYSICS);
        self.Entity:SetCollisionGroup(COLLISION_GROUP_PROJECTILE)
        self.Damage = 1
        local phys = self:GetPhysicsObject()
        if phys:IsValid() then
            phys:Wake()
            phys:EnableGravity(true);
        end


        self.timer = CurTime()
        self.solidify = CurTime() + 1
        self.Bastardgas = nil
        self.Spammed = false
        self.EndColor = "50 50 50"
        self.StartColor = "100 100 100"
    end

    function ENT:Touch(ent)
        if ent:IsPlayer() and not ent.nextsandtouch or (ent.nextsandtouch and ent.nextsandtouch < CurTime()) and self.Damage == 1 then
            ent.nextsandtouch = CurTime() + 0.1
            local ent = ents.Create( "npc_headcrab" )
            ent:SetPos( self:GetPos() + Vector(20,0,100))

            ent:SetHealth( 200 )
            ent:SetPhysicsAttacker(self.Owner)
            ent:Spawn()
           -- self:SetColor(Color(255, 0, 0))
            timer.Simple(0.01, function()
                if not IsValid(self) then return end
                local dmginfo = DamageInfo()
                dmginfo:SetDamage(80) --50 damage
                dmginfo:SetDamageType(DMG_BULLET) --Bullet damage
                dmginfo:SetAttacker(self.owner or ent) --First player found gets credit
                dmginfo:SetDamageForce(Vector(0, 0, 50)) --Launch upwards
                dmginfo:SetInflictor(self.owner or self)
                ent:TakeDamageInfo(dmginfo)
                self.Damage = 0
                self:Explode()
            end)
        end
    end

    function ENT:Explode()
        if self.exploded then return end self.exploded = true
        local effect = EffectData()
        effect:SetStart(self:GetPos())
        effect:SetOrigin(self:GetPos())
        effect:SetScale(0)
        effect:SetRadius(250)
        effect:SetMagnitude(250)

        util.Effect("Explosion", effect, true, true)

        util.BlastDamage(self, self.owner, self:GetPos(), 0, 0)
        self:Remove()
    end


    function ENT:Think()
        if (IsValid(self.Owner)==false) then
            self.Entity:Remove()
        end
        if (self.solidify<CurTime()) then
            self.SetOwner(self.Entity)
        end
        if self.timer < CurTime() then
            if !IsValid(self.Bastardgas) && !self.Spammed then
            self.Spammed = true
            self.Bastardgas = ents.Create("env_smoketrail")
            self.Bastardgas:SetPos(self.Entity:GetPos())
            self.Bastardgas:SetKeyValue("spawnradius","256")
            self.Bastardgas:SetKeyValue("minspeed","0.5")
            self.Bastardgas:SetKeyValue("maxspeed","2")
            self.Bastardgas:SetKeyValue("startsize","16536")
            self.Bastardgas:SetKeyValue("endsize","256")
            self.Bastardgas:SetKeyValue("endcolor",self.EndColor)
            self.Bastardgas:SetKeyValue("startcolor",self.StartColor)
            self.Bastardgas:SetKeyValue("opacity","4")
            self.Bastardgas:SetKeyValue("spawnrate","15")
            self.Bastardgas:SetKeyValue("lifetime","10")
            self.Bastardgas:SetParent(self.Entity)
            self.Bastardgas:Spawn()
            self.Bastardgas:Activate()
            self.Bastardgas:Fire("turnon","", 0.1)
            local exp = ents.Create("env_explosion")
            exp:SetKeyValue("spawnflags",461)
            exp:SetPos(self.Entity:GetPos())
            exp:Spawn()
            exp:Fire("explode","",0)
            self.Entity:EmitSound(Sound("BaseSmokeEffect.Sound"))
            end

            local pos = self.Entity:GetPos()
            local maxrange = 256
            local maxstun = 40
            for k,v in pairs(player.GetAll()) do
                if v.GasProtection then continue end
                local plpos = v:GetPos()
                local dist = -pos:Distance(plpos)+maxrange
                if (pos:Distance(plpos)<=maxrange) and not v:IsGhost() then
                    local trace = {}
                    trace.start = self.Entity:GetPos()
                    trace.endpos = v:GetPos()+Vector(0,0,24)
                    trace.filter = { v, self.Entity }
                    trace.mask = COLLISION_GROUP_PLAYER
                    tr = util.TraceLine(trace)
                    if (tr.Fraction==1) then
                        local stunamount = math.ceil(dist/(maxrange/maxstun))
                        v:ViewPunch( Angle( stunamount*((math.random()*6)-2), stunamount*((math.random()*6)-2), stunamount*((math.random()*4)-1) ) )
                        Poison(v, stunamount, self.Owner, self.Owner)
                    end
                end
                end
                if (self.timer+60<CurTime()) then
                    if IsValid(self.Bastardgas) then
                        self.Bastardgas:Remove()
                    end
                end
                if (self.timer+65<CurTime()) then
                    self.Entity:Remove()
                end
                self.Entity:NextThink(CurTime()+0.5)
                return true
            end
        end

end

scripted_ents.Register(ENT, "rats_disk", true)

if CLIENT then
    function SWEP:DrawHUD()
        local shotttext = "Shootmode: "..self:GetNWString("shootmode","Potato Power").."\nE+R to change"
        surface.SetFont( "ChatFont" );
        local size_x, size_y = surface.GetTextSize( shotttext );

        draw.RoundedBox( 6, ScrW( ) / 2 - size_x / 2 - 5, ScrH( ) - 150, size_x + 15, size_y + 10, Color( 0, 0, 0, 150 ) );
        draw.DrawText( shotttext , "ChatFont", ScrW( ) / 2, ScrH( ) - 150 + 5, Color( 255, 30, 30, 255 ), TEXT_ALIGN_CENTER );
        self.BaseClass.DrawHUD(self)
    end
end


