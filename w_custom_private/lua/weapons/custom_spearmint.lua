//General Variables\\
SWEP.AdminSpawnable = true
SWEP.ViewModelFOV = 64

SWEP.VElements = {
	["saw"] = { type = "Model", model = "models/props_c17/signpole001.mdl", bone = "Crossbow_model.bolt", rel = "", pos = Vector(-0.101, 0, 26.818), angle = Angle(0, 0, 180), size = Vector(0.435, 0.435, 0.321), color = Color(255, 169, 0, 255), surpresslightning = false, material = "models/props_c17/metalwall085a", skin = 0, bodygroup = {} },
	["weight"] = { type = "Model", model = "models/props_junk/cinderblock01a.mdl", bone = "Crossbow_model.Crossbow_base", rel = "body", pos = Vector(-2, 1.363, 0), angle = Angle(0, 0, 90), size = Vector(0.2, 0.2, 0.2), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["wheel"] = { type = "Model", model = "models/props_c17/pulleywheels_small01.mdl", bone = "Crossbow_model.Crossbow_base", rel = "hframe++++", pos = Vector(0, -5, -0.456), angle = Angle(0, 0, 0), size = Vector(0.17, 0.17, 0.17), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["hframe++"] = { type = "Model", model = "models/gibs/metal_gib2.mdl", bone = "Crossbow_model.Crossbow_base", rel = "", pos = Vector(0, -0.32, 20.454), angle = Angle(0, 90, 180), size = Vector(1.003, 1.83, 1.003), color = Color(255, 255, 255, 255), surpresslightning = false, material = "models/props_c17/oil_drum_chunks001a", skin = 0, bodygroup = {} },
	["hframe+++"] = { type = "Model", model = "models/gibs/metal_gib2.mdl", bone = "Crossbow_model.Crossbow_base", rel = "hframe++", pos = Vector(0.001, 14.09, 4.091), angle = Angle(0, 180, 90), size = Vector(1.003, 0.606, 1.003), color = Color(255, 255, 255, 255), surpresslightning = false, material = "models/props_c17/oil_drum_chunks001a", skin = 0, bodygroup = {} },
	["hframe++++"] = { type = "Model", model = "models/gibs/metal_gib2.mdl", bone = "Crossbow_model.Crossbow_base", rel = "hframe++", pos = Vector(0.001, -14.091, 4.091), angle = Angle(0, 0, 90), size = Vector(1.003, 0.606, 1.003), color = Color(255, 255, 255, 255), surpresslightning = false, material = "models/props_c17/oil_drum_chunks001a", skin = 0, bodygroup = {} },
	["pull"] = { type = "Model", model = "models/props_c17/trappropeller_lever.mdl", bone = "Crossbow_model.pull", rel = "", pos = Vector(1.363, 0, 0), angle = Angle(-180, -90, 0), size = Vector(0.264, 0.264, 0.264), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["wheel+"] = { type = "Model", model = "models/props_c17/pulleywheels_small01.mdl", bone = "Crossbow_model.Crossbow_base", rel = "hframe+++", pos = Vector(0, -5, -0.456), angle = Angle(0, 0, 0), size = Vector(0.17, 0.17, 0.17), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["adj+"] = { type = "Model", model = "models/props_citizen_tech/firetrap_button01a.mdl", bone = "Crossbow_model.Crossbow_base", rel = "scope", pos = Vector(0, 1.1, 5), angle = Angle(0, -90, 0), size = Vector(0.209, 0.209, 0.209), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["scope"] = { type = "Model", model = "models/props_c17/oildrum001.mdl", bone = "Crossbow_model.Crossbow_base", rel = "body", pos = Vector(4.699, -13.94, 0), angle = Angle(0, 0, 90), size = Vector(0.079, 0.079, 0.239), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["body"] = { type = "Model", model = "models/props_junk/wood_crate002a.mdl", bone = "Crossbow_model.Crossbow_base", rel = "hframe++", pos = Vector(-0.5, 0.1, 11.8), angle = Angle(0, 0, 90), size = Vector(0.052, 0.321, 0.052), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["adj"] = { type = "Model", model = "models/props_citizen_tech/firetrap_button01a.mdl", bone = "Crossbow_model.Crossbow_base", rel = "scope", pos = Vector(1.2, 0, 5), angle = Angle(0, 0, 0), size = Vector(0.209, 0.209, 0.209), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
}
SWEP.WElements = {
	["adj+"] = { type = "Model", model = "models/props_citizen_tech/firetrap_button01a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "scope", pos = Vector(0, 1.1, 5), angle = Angle(0, -90, 0), size = Vector(0.209, 0.209, 0.209), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["weight"] = { type = "Model", model = "models/props_junk/cinderblock01a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "body", pos = Vector(2, 1.363, 0.1), angle = Angle(0, 0, 90), size = Vector(0.2, 0.2, 0.321), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["wheel"] = { type = "Model", model = "models/props_c17/pulleywheels_small01.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "hframe++++", pos = Vector(0, -5, 0.455), angle = Angle(0, 0, 0), size = Vector(0.17, 0.17, 0.17), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["hframe++"] = { type = "Model", model = "models/gibs/metal_gib2.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(25, -2.274, -3.182), angle = Angle(89, -3.069, 13.295), size = Vector(1.003, 1.83, 1.003), color = Color(255, 255, 255, 255), surpresslightning = false, material = "models/props_c17/oil_drum_chunks001a", skin = 0, bodygroup = {} },
	["hframe+++"] = { type = "Model", model = "models/gibs/metal_gib2.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "hframe++", pos = Vector(0.001, 14.09, 4.091), angle = Angle(0, 180, 90), size = Vector(1.003, 0.606, 1.003), color = Color(255, 255, 255, 255), surpresslightning = false, material = "models/props_c17/oil_drum_chunks001a", skin = 0, bodygroup = {} },
	["hframe++++"] = { type = "Model", model = "models/gibs/metal_gib2.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "hframe++", pos = Vector(0.001, -14.091, 4.091), angle = Angle(0, 0, 90), size = Vector(1.003, 0.606, 1.003), color = Color(255, 255, 255, 255), surpresslightning = false, material = "models/props_c17/oil_drum_chunks001a", skin = 0, bodygroup = {} },
	["scope"] = { type = "Model", model = "models/props_c17/oildrum001.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(0, 2.059, -7), angle = Angle(-90, 10, 0), size = Vector(0.079, 0.079, 0.239), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["wheel+"] = { type = "Model", model = "models/props_c17/pulleywheels_small01.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "hframe+++", pos = Vector(0, -5, 0.455), angle = Angle(0, 0, 0), size = Vector(0.17, 0.17, 0.17), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["element_name"] = { type = "Model", model = "", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(0, 0, 0), angle = Angle(0, 0, 0), size = Vector(0.5, 0.5, 0.5), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["saw"] = { type = "Model", model = "models/props_c17/signpole001.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(7.727, 0.699, -3.8), angle = Angle(-90.5, 9.699, 0), size = Vector(0.435, 0.435, 0.209), color = Color(237, 182, 0, 255), surpresslightning = false, material = "models/props_c17/metalwall085a", skin = 0, bodygroup = {} },
	["body"] = { type = "Model", model = "models/props_junk/wood_crate002a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "hframe++", pos = Vector(0.455, -0.071, 11.364), angle = Angle(0, 0, 90), size = Vector(0.052, 0.321, 0.052), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["adj"] = { type = "Model", model = "models/props_citizen_tech/firetrap_button01a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "scope", pos = Vector(1.2, 0, 5), angle = Angle(0, 0, 0), size = Vector(0.209, 0.209, 0.209), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["pull"] = { type = "Model", model = "models/props_c17/trappropeller_lever.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(5.908, 2.273, -3.5), angle = Angle(0, 15.34, 180), size = Vector(0.264, 0.264, 0.264), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
}
SWEP.Kind = WEAPON_ROLE
SWEP.AutoSwitchTo = false
SWEP.Slot = 7
SWEP.PrintName = "Spearmint"
SWEP.Author = "Spastik/BFG"
SWEP.Spawnable = true
SWEP.AutoSwitchFrom = false
SWEP.FiresUnderwater = false
SWEP.Weight = 5
SWEP.DrawCrosshair = true
SWEP.Category = "S&G Munitions"
SWEP.SlotPos = 3
SWEP.DrawAmmo = true
SWEP.ReloadSound = "weapons/smg1/smg1_reload.wav"
SWEP.Instructions = "Pull the trigger to....well, this isn't a shark, so you figure out what to do."
SWEP.Contact = ""
SWEP.Purpose = "For confusing your enemies into thinking that you're carrying a shark up until a long rusted rod is plunged through their abdomen."
SWEP.Base = "aa_base"
SWEP.HoldType = "crossbow"
SWEP.ViewModelFOV = 56.614173228346
SWEP.ViewModelFlip = false
SWEP.ViewModel = "models/weapons/v_crossbow.mdl"
SWEP.WorldModel = "models/weapons/w_crossbow.mdl"
SWEP.ShowViewModel = true
SWEP.ShowWorldModel = true
SWEP.ViewModelBoneMods = {
	["Crossbow_model.pull"] = { scale = Vector(0.381, 0.381, 0.381), pos = Vector(0, 0, 0), angle = Angle(0, 0, 0) },
	["Crossbow_model.Base"] = { scale = Vector(1, 1, 1), pos = Vector(0, -2.54, 0), angle = Angle(0, 0, 0) },
	["Crossbow_model.bolt"] = { scale = Vector(0.009, 0.009, 0.009), pos = Vector(0, 0, 0), angle = Angle(0, 0, 0) },
	["Crossbow_model.bowl_wheel"] = { scale = Vector(0.009, 0.009, 0.009), pos = Vector(0, 0, 0), angle = Angle(0, 0, 0) }
}

//SWEP.TracerFreq			= 1
//SWEP.TracerType                 	= "AirboatGunHeavyTracer"

//General Variables\\

//Primary Fire Variables\\

SWEP.Primary.Damage = 95
SWEP.Primary.TakeAmmo = 1
SWEP.Primary.ClipSize = 1
SWEP.Primary.Ammo = "Xbowbolt"
SWEP.Primary.DefaultClip = 20
SWEP.Primary.Spread = 0.001
SWEP.Primary.NumberofShots = 1
SWEP.Primary.Automatic = false
SWEP.Primary.Recoil = 5
SWEP.Primary.Delay = 1
SWEP.Primary.Force = 7
        SWEP.HeadshotMultiplier = 1.5

SWEP.Secondary.Sound=Sound("weapons/sniper/sniper_zoomin.wav")
SWEP.Secondary.Ammo = "none"
Zoom=0


function SWEP:OnDrop()
    self:Remove()
end

//SWEP:PrimaryFire()\\
function SWEP:PrimaryAttack()
	if ( !self:CanPrimaryAttack() ) then return end

	local rnda = self.Primary.Recoil * -1
	local rndb = self.Primary.Recoil * math.random(-1, 1)

	self:FireBolt(0.2,self.Primary.Damage)
    self:ShootEffects()
    self:MakeAFlame()
	self.Weapon:EmitSound("weapons/crossbow/fire1.wav", 500,95, 1, 0)
	self.Owner:ViewPunch( Angle( rnda,rndb,rnda ) )
	self:TakePrimaryAmmo(self.Primary.TakeAmmo)
	self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
	self.Weapon:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
	
	local trace = self.Owner:GetEyeTrace()
		if trace.HitSky then return end
			//EmitSound("Weapon_Crossbow.BoltHitWorld", trace.Hitpos)
			sound.Play("Weapon_Crossbow.BoltHitWorld", trace.HitPos, 200, 100)

		if trace.HitWorld then --this is neccessary, because the bolt will float in mid air if this is not done
   			local effectdata = EffectData() 
			effectdata:SetOrigin( trace.HitPos ) 
			effectdata:SetNormal( trace.HitNormal ) 
			effectdata:SetEntity( trace.Entity ) 
			effectdata:SetAttachment( trace.PhysicsBone ) 
			util.Effect( "Boltimpact", effectdata )
			util.Effect( "StunstickImpact", effectdata )
			util.Effect( "GlassImpact", effectdata )
			util.Effect( "GlassImpact", effectdata )
			util.Effect( "ManhackSparks", effectdata )
		else
			local effectdata = EffectData() 
			effectdata:SetOrigin( trace.HitPos ) 
			effectdata:SetNormal( trace.HitNormal ) 
			effectdata:SetEntity( trace.Entity ) 
			effectdata:SetAttachment( trace.PhysicsBone ) 
			util.Effect( "StunstickImpact", effectdata )
			util.Effect( "ManhackSparks", effectdata )
			util.Effect( "ManhackSparks", effectdata )
			util.Effect( "ManhackSparks", effectdata ) --Fuck you I like calling things multiple times
		end
end



function SWEP:PreDrop()
    self:SetZoom(false)
    self:SetIronsights(false)
    return self.BaseClass.PreDrop(self)
end

//SWEP:PrimaryFire()\\

//SWEP:SecondaryFire()\\
//SWEP:SecondaryFire()\\
function SWEP:SecondaryAttack()
    if self.Owner:KeyDown(IN_USE) then
        self:ShootFlare()
        return
    end
 	self:Zoom()
end

function SWEP:Reload()
	self.Weapon:DefaultReload( ACT_VM_RELOAD )
	// SAGA's edit
	if ( self:Clip1() == self.Primary.ClipSize or self:Ammo1() == 0 ) then return end -- this stops sound spam if we press reload with no ammo or full clip
	if self.Zoomed then
		self.Zoomed = false
		if SERVER then
			self.Owner:SetFOV( 0, 0.3 )
		end
	end
	self.Weapon:EmitSound( "weapons/crossbow/reload1.wav", 500, 100, 1, -1 )
	timer.Simple(0.9, function()
		if ( not IsValid( self ) ) then return end
		self.Weapon:EmitSound( "physics/wood/wood_crate_impact_soft"..math.random( 1,3 )..".wav", 500, 100, 1, 1 )
	end)
end



function SWEP:GetTracerOrigin()

	local pos = self:GetOwner():EyePos()
	return pos

end

SWEP.IronSightsPos      = Vector( -100, -100, -100 )
SWEP.IronSightsAng      = Vector( 2.6, 1.37, 3.5 )
SWEP.Secondary.Sound = Sound("Default.Zoom")
-- Add some zoom to ironsights for this gun
function SWEP:Zoom()
    if not self.IronSightsPos then return end
    if self.Weapon:GetNextSecondaryFire() > CurTime() then return end

    local bIronsights = not self:GetIronsights()

    self:SetIronsights( bIronsights )

    if SERVER then
        self:SetZoom(bIronsights)
    else
        self:EmitSound(self.Secondary.Sound)
    end

    self.Weapon:SetNextSecondaryFire( CurTime() + 0.3)
end



if CLIENT then
    function SWEP:DrawHUD( )

        local scope = surface.GetTextureID("sprites/scope")
        if self:GetIronsights() then
            surface.SetDrawColor( 0, 0, 0, 255 )

            local x = ScrW() / 2.0
            local y = ScrH() / 2.0
            local scope_size = ScrH()

            -- crosshair
            local gap = 80
            local length = scope_size
            surface.DrawLine( x - length, y, x - gap, y )
            surface.DrawLine( x + length, y, x + gap, y )
            surface.DrawLine( x, y - length, x, y - gap )
            surface.DrawLine( x, y + length, x, y + gap )

            gap = 0
            length = 50
            surface.DrawLine( x - length, y, x - gap, y )
            surface.DrawLine( x + length, y, x + gap, y )
            surface.DrawLine( x, y - length, x, y - gap )
            surface.DrawLine( x, y + length, x, y + gap )


            -- cover edges
            local sh = scope_size / 2
            local w = (x - sh) + 2
            surface.DrawRect(0, 0, w, scope_size)
            surface.DrawRect(x + sh - 2, 0, w, scope_size)

            surface.SetDrawColor(255, 0, 0, 255)
            surface.DrawLine(x, y, x + 1, y + 1)

            -- scope
            surface.SetTexture(scope)
            surface.SetDrawColor(255, 255, 255, 255)

            surface.DrawTexturedRectRotated(x, y, scope_size, scope_size, 0)

        else
            return self.BaseClass.DrawHUD(self)
        end
    end

    function SWEP:AdjustMouseSensitivity()
        return (self:GetIronsights() and 0.2) or nil
    end
end;
function SWEP:SetZoom(state)
    if CLIENT then
        return
    elseif IsValid(self.Owner) and self.Owner:IsPlayer() then
        if state then
            self.Owner:SetFOV(30, 0.3)
        else
            self.Owner:SetFOV(0, 0.2)
        end
    end
end
