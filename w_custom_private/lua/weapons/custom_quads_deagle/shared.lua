 if SERVER then
   AddCSLuaFile(  )



end
if CLIENT then
   
   SWEP.Author 		= "GreenBlack"   

   SWEP.SlotPos 	= 1
   SWEP.Icon = "vgui/ttt_fun_killicons/golden_deagle.png"
end
SWEP.PrintName	= "Quad's Deagle"
SWEP.Slot		= 1
SWEP.HoldType 		= "pistol"
SWEP.Base			= "aa_base"
SWEP.Spawnable 		= true
SWEP.AdminSpawnable = true
SWEP.Kind 			= WEAPON_PISTOL
SWEP.Primary.Ammo   = "AlyxGun" 
SWEP.Primary.Recoil	= 1
SWEP.Primary.Damage = 55
SWEP.Primary.Delay 	= 0.55
SWEP.Primary.Cone 	= 0.009
SWEP.Primary.ClipSize 		= 80
SWEP.Primary.ClipMax 		= 120
SWEP.Primary.DefaultClip 	= 40
SWEP.Primary.NumShots		= 1
SWEP.Primary.Automatic 	= true
SWEP.HeadshotMultiplier = 3
SWEP.AutoSpawnable      = false
SWEP.AmmoEnt 		= "item_ammo_revolver_ttt"
--SWEP.Primary.Sound	= "gdeagle/gdeagle-1.mp3"
SWEP.Primary.Sound	= Sound( "Weapon_Deagle.Single" )
SWEP.Primary.SoundLevel = 400
SWEP.ViewModel= "models/weapons/v_pist_gbagle.mdl"
SWEP.WorldModel= "models/weapons/w_pist_gbagle.mdl"
SWEP.ShowWorldModel = true
SWEP.ShowViewModel = true

SWEP.LimitedStock = true
SWEP.InLoadoutFor = nil
SWEP.AllowDrop 	  = true
SWEP.IronSightsPos = Vector(1.14, -3.951, 0.736)
SWEP.IronSightsAng = Vector(0, 0, 0)
SWEP.IronSightsPos 	= Vector( 3.8, -1, 1.6 )
--SWEP.ViewModelFOV = 49


function SWEP:Deploy()
   self.Weapon:EmitSound("gdeagle/gdeagledeploy.mp3")

end

 SWEP.NextReload = 0

function SWEP:Reload()
 
	if self.ReloadingTime and CurTime() <= self.ReloadingTime then return end

	self:SetIronsights( false )
	if ( self:Clip1() < 1 and self.Owner:GetAmmoCount( self.Primary.Ammo ) > 0 ) then
 
	self:DefaultReload( ACT_VM_RELOAD )
	local AnimationTime = self.Owner:GetViewModel():SequenceDuration()
	self.ReloadingTime = CurTime() + AnimationTime
	self:SetNextPrimaryFire(CurTime() + AnimationTime)
	self:SetNextSecondaryFire(CurTime() + AnimationTime)
	self.Weapon:EmitSound("gdeagle/gdeaglereload.wav")
 
    end


    if self.NextReload > CurTime() then

       self.Owner:PrintMessage( HUD_PRINTCENTER, "Wait ".. math.Round(self.NextReload - CurTime()) .." seconds"  )
    return end
    self.ReloadingTime = CurTime() + 0.5

    local tr = util.TraceLine({ start = self.Owner:EyePos(), endpos = self.Owner:EyePos() + self.Owner:EyeAngles():Forward() * 80, filter = self.Owner })
    if tr.HitNonWorld and IsValid(tr.Entity) and tr.Entity:GetClass() == "prop_ragdoll" and tr.Entity.player_ragdoll then

        local mins, maxs = self.Owner:OBBMins(), self.Owner:OBBMaxs()
        local tr2 = util.TraceHull({start = tr.Entity:LocalToWorld(tr.Entity:OBBCenter()) + vector_up, endpos = tr.Entity:LocalToWorld(tr.Entity:OBBCenter()) + Vector(0, 0, 80), filter = {tr.Entity, self.Owner}, mins = Vector(mins.x * 1.6, mins.y * 1.6, 0), maxs = Vector(maxs.x * 1.6, maxs.y * 1.6, 0), mask = MASK_PLAYERSOLID})
        if tr2.Hit then
            self.Owner:PrintMessage( HUD_PRINTCENTER, "There is no room to bring him back here" )
            return
        end


        self.NextReload = CurTime() + 120
        self.Weapon:EmitSound( "weapons/c4/c4_beep1.wav"	)

        if SERVER then
            rag = tr.Entity
            local ply = player.GetByUniqueID(rag.uqid)
            local credits = CORPSE.GetCredits(rag, 0)
            if not IsValid(ply) then return end
            if SERVER and _globals['slayed'][ply:SteamID()] then
                self.Owner:PrintMessage( HUD_PRINTCENTER, "Can't revive, person was slayed" )
                ply:Kill();
                return
            end
            if SERVER and ((rag.was_role == ROLE_TRAITOR and self.Owner:GetRole() ~= ROLE_TRAITOR) or (rag.was_role ~= ROLE_TRAITOR and self.Owner:GetRole() == ROLE_TRAITOR))  then
                DamageLog("Defib: " .. self.Owner:Nick() .. " [" .. self.Owner:GetRoleString() .. "] Died defibbing "..ply:Nick().." [" .. ply:GetRoleString() .. "] ")
                self.Owner:PrintMessage( HUD_PRINTCENTER, ply:Nick().." was of the other team,  sucked up your soul and killed you" )
                self.Owner:Kill();

                return
            end
            DamageLog("Defib: " .. self.Owner:Nick() .. " [" .. self.Owner:GetRoleString() .. "] defibbed "..ply:Nick().." [" .. ply:GetRoleString() .. "] ")

            ply:SpawnForRound(true)
            ply:SetCredits(credits)
            ply:SetPos(rag:LocalToWorld(rag:OBBCenter()) + vector_up / 2) --Just in case he gets stuck somewhere
            ply:SetEyeAngles(Angle(0, rag:GetAngles().y, 0))
            ply:SetCollisionGroup(COLLISION_GROUP_PLAYER)
            if ply:GetNWInt("runspeed") < 450 then
            ply:SetNWInt("runspeed", 450)
            ply:SetNWInt("walkspeed",350)
            end

            umsg.Start("_resurrectbody")
            umsg.Entity(ply)
            umsg.Bool(CORPSE.GetFound(rag, false))
            umsg.End()
            rag:Remove()
        end
    end
 
end
function SWEP:PrimaryAttack(worldsnd)
   self.Weapon:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
   self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
   if not self:CanPrimaryAttack() then return end
   if not worldsnd then
      self.Weapon:EmitSound( self.Primary.Sound, self.Primary.SoundLevel )
   elseif SERVER then
      WorldSound(self.Primary.Sound, self:GetPos(), self.Primary.SoundLevel)
   end
   self:ShootBullet( self.Primary.Damage, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )
   
   if ( self.Owner:IsValid() ) then
		local tr = self.Owner:GetEyeTrace()
		local effectdata = EffectData()
		effectdata:SetOrigin(tr.HitPos)
		effectdata:SetNormal(tr.HitNormal)
		effectdata:SetScale(1)
		-- util.Effect("effect_mad_ignition", effectdata)
		util.Decal("FadingScorch", tr.HitPos + tr.HitNormal, tr.HitPos - tr.HitNormal)
		
				local tracedata = {}
				tracedata.start = tr.HitPos
				tracedata.endpos = Vector(tr.HitPos.x, tr.HitPos.y, tr.HitPos.z - 10)
				tracedata.filter = tr.HitPos
				local tracedata = util.TraceLine(tracedata)
				if SERVER then
				if tracedata.HitWorld then
					local flame = ents.Create("env_fire");
					flame:SetPos(tr.HitPos + Vector( 0, 0, 1 ));
					flame:SetKeyValue("firesize", "80");
					flame:SetKeyValue("fireattack", "10");
					flame:SetKeyValue("StartDisabled", "0");
					flame:SetKeyValue("health", "10");
					flame:SetKeyValue("firetype", "0");
					flame:SetKeyValue("damagescale", "5");
					flame:SetKeyValue("spawnflags", "128");
					flame:SetPhysicsAttacker(self.Owner)
					flame:SetOwner( self.Owner )
					flame:Spawn();
					flame:Fire("StartFire",0);
				end
				end
	   self:TakePrimaryAmmo( 1 )
	   local owner = self.Owner   
	   --if not ValidEntity(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end
	   
	   owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )
   end
end


 SWEP.flyammo = 5

 function SWEP:SecondaryAttack()

     self:Fly()
 end
