SWEP.ViewModel = "models/Teh_Maestro/popcorn.mdl"
SWEP.WorldModel = "models/Teh_Maestro/popcorn.mdl"
SWEP.Spawnable			= true
SWEP.AdminSpawnable		= false
SWEP.ViewModelFlip = false
SWEP.ViewModelFOV		= 80
SWEP.AdminSpawnable = true
SWEP.AutoSpawnable = false

SWEP.Base = "weapon_tttbase"
SWEP.Kind = WEAPON_EQUIP2

SWEP.Author = "Antimony51"
SWEP.Instructions	= "Left Click: Eat Popcorn\nRight Click: Throw Bucket"

SWEP.Primary.Clipsize = -1
SWEP.Primary.DefaultClip = -1
SWEP.Primary.Automatic = false
SWEP.Primary.Ammo = "none"

SWEP.Secondary.Clipsize = -1
SWEP.Secondary.DefaultClip = -1
SWEP.Secondary.Automatic = false
SWEP.Secondary.Ammo = "none"

SWEP.DrawAmmo = false
SWEP.Slot				= 1
SWEP.SlotPos			= 1
SWEP.HoldType 			= "shotgun"

SWEP.PrintName          = "Turtle Flakes"
SWEP.Slot               = 1
SWEP.SlotPos            = 1

function SWEP:Deploy()
end


function SWEP:Initialize()
	util.PrecacheSound("crisps/eat.mp3")
end

function SWEP:OnDrop()
    self:Remove()
end


function SWEP:PrimaryAttack()
	if SERVER then
		local sound = CreateSound(self.Owner, "crisps/eat.mp3")
		sound:Play()
		sound:ChangeVolume(0.2,0)
                self.Owner:SetHealth( self.Owner:Health() + 3 )
	end
	self.Weapon:SetNextPrimaryFire(CurTime() + 2.5 )
end
function SWEP:OnDroP()
    self:Remove()
end
function SWEP:SecondaryAttack()
	local bucket, att, phys, tr

	self.Weapon:SetNextSecondaryFire(CurTime() + 0.5)
	if (self.Owner:IsAdmin()) then
		self.Weapon:SetNextSecondaryFire(CurTime())
	end
    
    if CLIENT then
        return
    end
    
    self.Owner:EmitSound( "weapons/slam/throw.wav" )
	
	self.Owner:ViewPunch( Angle( math.Rand(-8,8), math.Rand(-8,8), 0 ) )
	
    bucket = ents.Create( "sent_popcorn_thrown" )
    bucket:SetOwner( self.Owner )
    bucket:SetPos( self.Owner:GetShootPos( ) )
    bucket:Spawn() 
    bucket:Activate()
	
    phys = bucket:GetPhysicsObject( )
        
    if IsValid( phys ) then
		phys:SetVelocity( self.Owner:GetPhysicsObject():GetVelocity() )
		phys:AddVelocity( self.Owner:GetAimVector( ) * 128 * phys:GetMass( ) )
		phys:AddAngleVelocity( VectorRand() * 128 * phys:GetMass( ) )
    end
	
	--[[
	if !self.Owner:IsAdmin() then
		self.Owner:StripWeapon("weapon_popcorn")
	end
	--]]
end
