
SWEP.PrintName			= "Schrodinger's Cat"
SWEP.Slot				= 0
SWEP.SlotPos			= 1
if ( CLIENT ) then


    SWEP.Author				= "XcoliahX"


    SWEP.IconLetter			= "f"
    SWEP.DrawCrosshair		= false

    killicon.AddFont( "weapon_knife", "CSKillIcons", SWEP.IconLetter, Color( 255, 80, 0, 255 ) )


end
SWEP.Icon = "vgui/ttt_fun_killicons/lightsaber.png"
SWEP.Base = "gb_camo_base"
SWEP.Kind = WEAPON_MELEE
SWEP.Camo = 2
SWEP.CustomCamo = true

-------------------------------------------------------------------

------------General Swep Info---------------
SWEP.Author   = "XcoliahX"
SWEP.Contact        = ""
SWEP.Purpose        = ""
SWEP.Instructions   = ""
SWEP.Spawnable      = false
SWEP.AdminSpawnable  = true
-----------------------------------------------

------------Models---------------------------
SWEP.ViewModel      = "models/weapons/v_knife_t.mdl"
SWEP.WorldModel   = "models/weapons/w_stunbaton.mdl"
-----------------------------------------------

-------------Primary Fire Attributes----------------------------------------
SWEP.Primary.Delay			= 1	--In seconds
SWEP.Primary.Recoil			= 0		--Gun Kick
SWEP.Primary.Damage			= 45	--Damage per Bullet
SWEP.Primary.NumShots		= 1		--Number of shots per one fire
SWEP.Primary.Cone			= 0 	--Bullet Spread
SWEP.Primary.ClipSize		= 5	--Use "-1 if there are no clips"
SWEP.Primary.DefaultClip	= 5	--Number of shots in next clip
SWEP.Primary.Automatic   	= true	--Pistol fire (false) or SMG fire (true)
SWEP.Primary.Ammo         	= "smg1"	--Ammo Type
-------------End Primary Fire Attributes------------------------------------

-------------Secondary Fire Attributes-------------------------------------
SWEP.Secondary.Delay		= 0.6
SWEP.Secondary.Recoil		= 0
SWEP.Secondary.Damage		= 5
SWEP.Secondary.NumShots		= 15
SWEP.Secondary.Cone			= 0
SWEP.Secondary.ClipSize		= 100
SWEP.Secondary.DefaultClip	= 100
SWEP.Secondary.Automatic   	= true
SWEP.Secondary.Ammo         = "Battery"
-------------End Secondary Fire Attributes--------------------------------
SWEP.AmmoEnt = "item_ammo_pistol_ttt"


SWEP.ViewModelFlip	= false
-----------------------Main functions----------------------------

-- function SWEP:Reload() --To do when reloading
-- end 

function SWEP:JumpReset()
    self.JumpRefire = true
end

function SWEP:Think()
    if self.Owner:KeyDown(IN_JUMP) then
        if self.Owner:IsOnGround() then
            self.Owner:SetVelocity(Vector(0,0,500))
        end
    end

end

function SWEP:Initialize()

    self:SetHoldType("melee")
end



SWEP.InAirDmg = 0
function SWEP:SecondaryAttack()

    self:Fly()
end

function SWEP:SecondaryAttackDelay()
    return 0.3
end

function SWEP:GivePrimaryAmmo( num )

    self:SetClip1( self:Clip1() + num )

end



function SWEP:Deploy()
    self.Weapon:EmitSound( "weapons/knife/knife_deploy1.wav" ) --Plays a nifty sound when you pull it out.
end

SWEP.NextReload = 0

function SWEP:Reload()
    if CurTime() < self.NextReload then return end





    local tracedata = {}
    tracedata.start = self.Owner:GetShootPos()
    tracedata.endpos = self.Owner:GetShootPos() + (self.Owner:GetAimVector() * 100)
    tracedata.filter = self.Owner
    tracedata.mins = Vector(1,1,1) * -10
    tracedata.maxs = Vector(1,1,1) * 10
    tracedata.mask = MASK_SHOT_HULL
    local tr = util.TraceLine({start = self.Owner:EyePos(), endpos = self.Owner:EyePos() + self.Owner:EyeAngles():Forward() * 80, filter = self.Owner})

    local ply = self.Owner

    if IsValid(tr.Entity) then
        self.NextReload = CurTime() + 10
        if tr.Entity.player_ragdoll then
            self.NextReload = CurTime() + 120
            timer.Simple(0.1, function()
                ply:Freeze(true)
                ply:SetColor(Color(255,0,0,255))
            end)
            self.Weapon:SendWeaponAnim(ACT_VM_PRIMARYATTACK)

            timer.Create("GivePlyHealth_"..self.Owner:UniqueID(),0.5,6,function() if(IsValid(self)) and IsValid(self.Owner) then
                if self.Owner:Health() < 175 then
                self.Owner:SetHealth(self.Owner:Health()+5)
                    end
            end end)
            self.Owner:EmitSound("ttt/nomnomnom.mp3")

            timer.Simple(3.1, function()
                if not IsValid(ply) then return end
                ply:Freeze(false)
                ply:SetColor(Color(255,255,255,255))
                tr.Entity:Remove()
            --self:Remove()
            end )


        end
    end


end


SWEP.nextAttack = 0


function SWEP:PrimaryAttack()
   -- self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )

    if( self.nextAttack > CurTime() ) then return end

    if self.Owner:KeyDown(IN_USE) then
          self:ShootBullet(5, 15, 15, 0.085)
          self.nextAttack = CurTime() + 0.9
        return
    end

    self.nextAttack = CurTime() + self.Primary.Delay

    self.Owner:LagCompensation(true)

    local spos = self.Owner:GetShootPos()
    local sdest = spos + (self.Owner:GetAimVector() * 70)

    local kmins = Vector(1,1,1) * -10
    local kmaxs = Vector(1,1,1) * 10

    local tr = util.TraceHull({start=spos, endpos=sdest, filter=self.Owner, mask=MASK_SHOT_HULL, mins=kmins, maxs=kmaxs})

    -- Hull might hit environment stuff that line does not hit
    if not IsValid(tr.Entity) then
        tr = util.TraceLine({start=spos, endpos=sdest, filter=self.Owner, mask=MASK_SHOT_HULL})
    end

    local hitEnt = tr.Entity

    -- effects
    if IsValid(hitEnt) then
        self.Weapon:SendWeaponAnim( ACT_VM_HITCENTER )

        local edata = EffectData()
        edata:SetStart(spos)
        edata:SetOrigin(tr.HitPos)
        edata:SetNormal(tr.Normal)
        edata:SetEntity(hitEnt)

        if hitEnt:IsPlayer() or hitEnt:GetClass() == "prop_ragdoll" then
            util.Effect("BloodImpact", edata)
        end
    else
        self.Weapon:SendWeaponAnim( ACT_VM_MISSCENTER )
    end

    if SERVER then
        self.Owner:SetAnimation( PLAYER_ATTACK1 )
    end


    if SERVER and tr.Hit and tr.HitNonWorld and IsValid(hitEnt) then
        if hitEnt:IsPlayer() then
            if  self.Owner:Health() < 175 then
                self.Owner:SetHealth( self.Owner:Health() + 10 )
            end

                local dmg = DamageInfo()
                dmg:SetDamage(self.Primary.Damage)
                dmg:SetAttacker(self.Owner)
                dmg:SetInflictor(self.Weapon or self)
                dmg:SetDamageForce(self.Owner:GetAimVector() * 5)
                dmg:SetDamagePosition(self.Owner:GetPos())
                dmg:SetDamageType(DMG_SLASH)

                hitEnt:DispatchTraceAttack(dmg, spos + (self.Owner:GetAimVector() * 3), sdest)

        end
    end

    self.Owner:LagCompensation(false)
end



if CLIENT then
    function SWEP:DrawHUD()
        local tr = self.Owner:GetEyeTrace(MASK_SHOT)

        if tr.HitNonWorld and IsValid(tr.Entity) and tr.Entity:IsPlayer()
                and tr.Entity:Health() < (self.Primary.Damage ) then

            local x = ScrW() / 2.0
            local y = ScrH() / 2.0

            surface.SetDrawColor(255, 0, 0, 255)

            local outer = 20
            local inner = 10
            surface.DrawLine(x - outer, y - outer, x - inner, y - inner)
            surface.DrawLine(x + outer, y + outer, x + inner, y + inner)

            surface.DrawLine(x - outer, y + outer, x - inner, y + inner)
            surface.DrawLine(x + outer, y - outer, x + inner, y - inner)

            draw.SimpleText("INSTANT KILL", "TabLarge", x, y - 30, COLOR_RED, TEXT_ALIGN_CENTER, TEXT_ALIGN_BOTTOM)
        end

        return self.BaseClass.DrawHUD(self)
    end
end



function SWEP:CreateZombie( ply, owner )

    if not IsValid( ply ) or not IsValid( owner ) then print("Not valid zombie") return end

    local ent = ply
    local weapon = owner:GetActiveWeapon()
    local edata = EffectData()

    edata:SetEntity(ent)
    edata:SetMagnitude(3)
    edata:SetScale(2)

    util.Effect("TeslaHitBoxes", edata)

    if SERVER and ent:IsPlayer() then

        local modelsss = { "npc_zombie", "npc_antlionguard", "npc_fastzombie", "npc_poisonzombie" }
        local randmodel = table.Random( modelsss )

        local oldPos = ent:GetPos()

        timer.Simple(4.9, function()
            if IsValid( ent ) then
                spot = ent:GetPos()
            else
                spot = oldPos
            end
            zed = ents.Create(randmodel)
        end)

        timer.Simple(5,function()
            if SERVER then
                if IsValid( ent ) then
                    ent:TakeDamage( 2000, owner, weapon )
                end
                --local ply = self.Owner -- The first entity is always the first player
                zed:SetPos(spot) -- This positions the zombie at the place our trace hit.
                zed:SetHealth( 2000 )
                zed:Spawn() -- This method spawns the zombie into the world, run for your lives! ( or just crowbar it dead(er) )
                zed:SetHealth( 2000 )
            end
        end) --20
        timer.Create("infectedDamage",0.25,20,function()
            if IsValid( ent ) then
                ent:TakeDamage( 3, owner, weapon )
            end
        end)

    end


end

SWEP.HadSecond = false

function SWEP:ShootBullet2( dmg, recoil, numbul, cone )

    if self.HadSecond then return end


    local sights = self:GetIronsights()

    numbul = numbul or 1
    cone   = cone   or 0.01

    -- 10% accuracy bonus when sighting
    cone = sights and (cone * 0.9) or cone

    local bullet = {}
    bullet.Num    = numbul
    bullet.Src    = self.Owner:GetShootPos()
    bullet.Dir    = self.Owner:GetAimVector()
    bullet.Spread = Vector( cone, cone, 0 )
    bullet.Tracer = 1
    bullet.Force  = 50000
    bullet.Damage = 0

    bullet.Callback = function(att, tr, dmginfo)
        if SERVER or (CLIENT and IsFirstTimePredicted()) then

            if (not tr.HitWorld)  then
                self.HadSecond = true
                pl2 = tr.Entity


               -- self:CreateZombie( pl2, self.Owner )
            end
        end
    end
    self.Owner:FireBullets( bullet )
    self.Weapon:SendWeaponAnim(self.PrimaryAnim)

    -- Owner can die after firebullets, giving an error at muzzleflash
    if not IsValid(self.Owner) or not self.Owner:Alive() then return end

    self.Owner:MuzzleFlash()
    self.Owner:SetAnimation( PLAYER_ATTACK1 )

    if self.Owner:IsNPC() then return end

    if ((game.SinglePlayer() and SERVER) or
            ((not game.SinglePlayer()) and CLIENT and IsFirstTimePredicted() )) then

        -- reduce recoil if ironsighting
        recoil = sights and (recoil * 0.5) or recoil

    end

end
