

if (SERVER) then

    AddCSLuaFile(  )
    SWEP.Weight                = 10
    SWEP.AutoSwitchTo        = true
    SWEP.AutoSwitchFrom        = true

end

if ( CLIENT ) then

    SWEP.DrawAmmo            = true
    SWEP.DrawCrosshair        = false
    SWEP.ViewModelFOV        = 65
    SWEP.ViewModelFlip        = false
    SWEP.CSMuzzleFlashes    = false
    SWEP.WepSelectIcon   = surface.GetTextureID("VGUI/entities/select")
    SWEP.BounceWeaponIcon = false

end


SWEP.Icon = "vgui/ttt_fun_killicons/minigun.png"
SWEP.EquipMenuData = {
    type  = "item_weapon",
    name  = "Minigun",
    desc  = "Shoots. Fast"
}

SWEP.Base = "gb_camo_base"
SWEP.Kind = WEAPON_UNARMED

SWEP.Slot = 3
SWEP.PrintName = "Diamond Destroyer"
SWEP.Author = "Heavy-D"
SWEP.Spawnable = true
SWEP.Category = "Heavy-D's SWEPs"
SWEP.SlotPos = 1
SWEP.Instructions = "Shoot it"
SWEP.Contact = "Don't"
SWEP.Purpose = "Hurt people"
SWEP.HoldType = "physgun"

SWEP.ShowViewModel = true
SWEP.ShowWorldModel = true

SWEP.ViewModelFOV			= 80
SWEP.ViewModelFlip			= false
SWEP.ViewModel				= "models/weapons/v_deat_m249para.mdl"
SWEP.WorldModel				= "models/weapons/w_blopsminigun.mdl"
SWEP.Camo = 53
SWEP.MuzzleAttachment			= "1" 	-- Should be "1" for CSS models or "muzzle" for hl2 models
SWEP.ShellEjectAttachment		= "2" 	-- Should be "2" for CSS models or "1" for hl2 models

        //Primary Fire Variables\\
SWEP.Primary.Sound = "weapons/ar2/fire1.wav"
SWEP.Primary.Damage = 13
SWEP.Primary.TakeAmmo = 1
SWEP.Primary.ClipSize = 400
SWEP.Primary.Ammo = "SMG1"
SWEP.Primary.DefaultClip = 3000
SWEP.Primary.Spread = 0.01
SWEP.Primary.NumberofShots = 1
SWEP.Primary.Automatic = true
SWEP.Primary.Recoil = 0.08
SWEP.Primary.Delay = 0.08
SWEP.Primary.Force = 300
SWEP.BarrelAngle = 0
SWEP.Crosshair = 0.07
        //Primary Fire Variables\\

//Secondary Fire Variables\\
SWEP.Secondary.NumberofShots = 1
SWEP.Secondary.Force = 10
SWEP.Secondary.Spread = 0.1
SWEP.Secondary.DefaultClip = 32
SWEP.Secondary.Recoil = 1
SWEP.Secondary.Automatic = false
SWEP.Secondary.Delay = 0.5
SWEP.Secondary.ClipSize = 0
SWEP.Secondary.Damage = 10
        //Secondary Fire Variables\\

SWEP.Primary.Sound			= Sound("weapons/deathmachine/death-1.wav")
SWEP.Primary.Sound2			= Sound("weapons/deathmachine/death_coverup.wav")

SWEP.IronSightsPos = Vector(0, 0, 0)
SWEP.IronSightsAng = Vector(0, 0, 0)
SWEP.SightsPos = Vector(0, 0, 0)
SWEP.SightsAng = Vector(0, 0, 0)
SWEP.RunSightsPos = Vector(0, 1.496, 4.645)
SWEP.RunSightsAng = Vector(-23.425, 0, 0)
SWEP.Offset = {
    Pos = {
        Up = 2,
        Right = 32,
        Forward = 0.0,
    },
    Ang = {
        Up = -10,
        Right = 0,
        Forward = 270,
    }
}
function SWEP:OnDrop()

    local wep = ents.Create(self:GetClass())
    wep:SetPos(self:GetPos()+Vector(0,100,0))
    wep:SetAngles(self:GetAngles())
    wep.IsDropped = true
    self:Remove(self)
    wep:Spawn()

end

function SWEP:SecondaryAttack()
    self:Fly()
end


function SWEP:DrawWorldModel( )
    local hand, offset, rotate

    if not IsValid( self.Owner ) then
        self:DrawModel( )
        return
    end

    if not self.Hand then
        self.Hand = self.Owner:LookupAttachment( "anim_attachment_rh" )
    end

    hand = self.Owner:GetAttachment( self.Hand )

    if not hand then
        self:DrawModel( )
        return
    end

    offset = hand.Ang:Right( ) * self.Offset.Pos.Right + hand.Ang:Forward( ) * self.Offset.Pos.Forward + hand.Ang:Up( ) * self.Offset.Pos.Up

    hand.Ang:RotateAroundAxis( hand.Ang:Right( ), self.Offset.Ang.Right )
    hand.Ang:RotateAroundAxis( hand.Ang:Forward( ), self.Offset.Ang.Forward )
    hand.Ang:RotateAroundAxis( hand.Ang:Up( ), self.Offset.Ang.Up )

    self:SetRenderOrigin( hand.Pos + offset )
    self:SetRenderAngles( hand.Ang )

    self:DrawModel( )
end

function SWEP:Initialize()

    util.PrecacheSound(self.Primary.Sound)
    self.Reloadaftershoot = 0 				-- Can't reload when firing
    self:SetHoldType("shotgun")
    self.BaseClass.Initialize(self)
end

function SWEP:Deploy()

    if game.SinglePlayer() then self.Single=true
    else
        self.Single=false
    end
    self:SetHoldType("shotgun")                          	// Hold type styles; ar2 pistol shotgun rpg normal melee grenade smg slam fist melee2 passive knife
    self:SetIronsights(false, self.Owner)					// Set the ironsight false
    self.Weapon:SendWeaponAnim( ACT_VM_DRAW )
    if !self.Owner:IsNPC() then self.ResetSights = CurTime() + self.Owner:GetViewModel():SequenceDuration() end

    if IsValid(self) and self.Owner:IsValid() then
        if self.Owner:GetNWInt("runspeed") < 400 then
        self.Owner:SetNWInt("runspeed", 400)
        self.Owner:SetNWInt("walkspeed",350)
        self.Owner:SetJumpPower(480)
        end
    end

    return self.BaseClass.Deploy(self)
end

function SWEP:Think()

    if self.Owner:KeyReleased(IN_ATTACK) then
        self.Weapon:EmitSound( self.Primary.Sound2, self.Primary.SoundLevel )
    end


    if (self.Owner:WaterLevel() > 2) then
        self.Weapon:SetNextPrimaryFire(CurTime() + 0.5)
        return false
    end

end

SWEP.NextReload  = 0
function SWEP:Reload()

    if self.Owner:KeyDown(IN_USE) then
        self.Weapon:DefaultReload(ACT_VM_RELOAD)
        if !self.Owner:IsNPC() then
        self.ResetSights = CurTime() + self.Owner:GetViewModel():SequenceDuration() end

        return
    end

    if self.NextReload < CurTime() then
        timer.Destroy("rpgIdle"); -- Kills timer to stop it from calling idle animation in the middle of fire anim.
        local rpg_vm = self.Owner:GetViewModel()
        local pos2 = self.Owner:GetShootPos()
        self:FireRocket()
        self.Weapon:EmitSound("weapons/rpg/rocketfire1.wav")
        util.ScreenShake(pos2, 10, 2, 0.1, 200)
        self.Weapon:TakePrimaryAmmo(1)
        rpg_vm:SetSequence(rpg_vm:LookupSequence("shoot1"))
        self.Owner:SetAnimation(PLAYER_ATTACK1)
        self.Owner:MuzzleFlash()
        self.NextReload = CurTime() + 5
    end




end

function SWEP:FireRocket() -- Rocket spawn function, refers to outside lua ent (self.primary.round).
    local aim = self.Owner:GetAimVector()
    local side = aim:Cross(Vector(0,0,1))
    local up = side:Cross(aim)
    local pos = self.Owner:GetShootPos() + side * 6 + up * -5

    if SERVER then
        local rocket = ents.Create("ammo_ren_rpg_missle")
        if !rocket:IsValid() then return false end
        rocket:SetAngles(aim:Angle()+Angle(0,0,0))
        rocket:SetPos(pos)
        rocket:SetOwner(self.Owner)
        rocket:Spawn()
        rocket:Activate()
        rocket.damage = 105
    end
end

-- NormalPrimaryAttack
function SWEP:PrimaryAttack(worldsnd)

    self.Weapon:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )

    if not self:CanPrimaryAttack() then return end

    if not worldsnd then
        self.Weapon:EmitSound( self.Primary.Sound, self.Primary.SoundLevel )

    elseif SERVER then
        sound.Play(self.Primary.Sound, self:GetPos(), self.Primary.SoundLevel)
    end

    self:ShootBullet( self.Primary.Damage, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )

    self:TakePrimaryAmmo( 1 )




end

SWEP.NextAnim = 0
SWEP.tracerColor = Color(0,0,255)
function SWEP:ShootBullet( dmg, recoil, numbul, cone )


    self.Owner:SetAnimation( PLAYER_ATTACK1 )
    self.Owner:MuzzleFlash()
    if self.NextAnim < CurTime() then

        self.Weapon:SendWeaponAnim(self.PrimaryAnim)
        self.NextAnim = CurTime() + 0.1
    end

    if not IsFirstTimePredicted() then return end

    local sights = self:GetIronsights()

    numbul = numbul or 1
    cone   = cone   or 0.01

    local bullet = {}
    bullet.Num    = numbul
    bullet.Src    = self.Owner:GetShootPos()
    bullet.Dir    = self.Owner:GetAimVector()
    bullet.Spread = Vector( cone, cone, 0 )
    bullet.Tracer = 1
    bullet.TracerName = "manatrace"
    bullet.Force  = 10
    bullet.Damage = dmg


    self.Owner:FireBullets( bullet )

    -- Owner can die after firebullets
    if (not IsValid(self.Owner)) or (not self.Owner:Alive()) or self.Owner:IsNPC() then return end

    if ((game.SinglePlayer() and SERVER) or
            ((not game.SinglePlayer()) and CLIENT and IsFirstTimePredicted())) then

        -- reduce recoil if ironsighting
        recoil = sights and (recoil * 0.6) or recoil

        local eyeang = self.Owner:EyeAngles()
        eyeang.pitch = eyeang.pitch - recoil
        self.Owner:SetEyeAngles( eyeang )
    end

end