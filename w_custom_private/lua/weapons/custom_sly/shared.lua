if SERVER then
   AddCSLuaFile(  )
end

SWEP.HoldType = "ar2"
   SWEP.Slot = 0
SWEP.PrintName = "Sly"

if CLIENT then
   
   SWEP.Author = "GreenBlack"

   SWEP.Icon = "vgui/ttt_fun_killicons/famas.png"
   SWEP.ViewModelFlip = false
end

SWEP.Base = "aa_base"
SWEP.Kind = WEAPON_MELEE

SWEP.Primary.Damage = 10
SWEP.Primary.Delay = 0.09
SWEP.Primary.Cone = 0.03
SWEP.Primary.ClipSize = 40
SWEP.Primary.ClipMax = 90
SWEP.Primary.DefaultClip = 40
SWEP.Primary.Automatic = true
SWEP.Primary.Ammo = "smg1"
SWEP.Primary.Recoil = 0.1
SWEP.Primary.Sound = Sound( "weapons/famas/famas-1.wav" )
SWEP.Primary.SoundLevel = 1
SWEP.AutoSpawnable = false
SWEP.AmmoEnt = "item_ammo_smg1_ttt"

SWEP.ViewModel = "models/weapons/v_rif_famas.mdl"
SWEP.WorldModel = "models/weapons/w_rif_famas.mdl"

SWEP.IronSightsPos = Vector( -4.65, -3.28, 0.01 )
SWEP.IronSightsAng = Vector( 1.09, 0, -2.19 )


function SWEP:PrimaryAttack(worldsnd)
    self.Weapon:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
    if not self:CanPrimaryAttack() then return end
    if not worldsnd then
        self.Weapon:EmitSound( self.Primary.Sound, self.Primary.SoundLevel )
    elseif SERVER then
        WorldSound(self.Primary.Sound, self:GetPos(), self.Primary.SoundLevel)
    end
    self:ShootBullet( self.Primary.Damage, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )

    if ( self.Owner:IsValid() ) then
        local tr = self.Owner:GetEyeTrace()
        local effectdata = EffectData()
        effectdata:SetOrigin(tr.HitPos)
        effectdata:SetNormal(tr.HitNormal)
        effectdata:SetScale(1)
        -- util.Effect("effect_mad_ignition", effectdata)
        util.Decal("FadingScorch", tr.HitPos + tr.HitNormal, tr.HitPos - tr.HitNormal)

        local tracedata = {}
        tracedata.start = tr.HitPos
        tracedata.endpos = Vector(tr.HitPos.x, tr.HitPos.y, tr.HitPos.z - 10)
        tracedata.filter = tr.HitPos
        local tracedata = util.TraceLine(tracedata)
        if SERVER then
            if tracedata.HitWorld and math.Rand(0,5) == 3 then
                local flame = ents.Create("env_fire");
                flame:SetPos(tr.HitPos + Vector( 0, 0, 1 ));
                flame:SetKeyValue("firesize", "10");
                flame:SetKeyValue("fireattack", "10");
                flame:SetKeyValue("StartDisabled", "0");
                flame:SetKeyValue("health", "10");
                flame:SetKeyValue("firetype", "0");
                flame:SetKeyValue("damagescale", "5");
                flame:SetKeyValue("spawnflags", "128");
                flame:SetPhysicsAttacker(self.Owner)
                flame:SetOwner( self.Owner )
                flame:Spawn();
                flame:Fire("StartFire",0);
            end
        end
        self:TakePrimaryAmmo( 1 )
        local owner = self.Owner
        --if not ValidEntity(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

        owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )
    end
end


function SWEP:GetHeadshotMultiplier(victim, dmginfo)
   local att = dmginfo:GetAttacker()
   if not IsValid(att) then return 2 end

   local dist = victim:GetPos():Distance(att:GetPos())
   local d = math.max(0, dist - 150)
 
   return 1.5 + math.max(0, (1.5 - 0.002 * (d ^ 1.25)))
end

SWEP.flyammo = 5
SWEP.nextFly = 0
function SWEP:SecondaryAttack()
   self:Fly()
end


SWEP.DrawCrosshair      = true
SWEP.hadBomb = 0
SWEP.NextStick = 0
function SWEP:Reload()
    if self.NextStick > CurTime() then return end
    self.NextStick = CurTime() + 1

    if self:Clip1() < 1 then
        self.Weapon:DefaultReload(self.ReloadAnim)
        self:SetIronsights( false )

    else

        if self:GetNWBool( "Planted" )  then
            self:BombSplode()
        elseif  self.hadBomb < 5 then
            self:StickIt()
        end;
    end

end


SWEP.Planted = false;

local hidden = true;
local close = false;
local lastclose = false;

if CLIENT then
    function SWEP:DrawHUD( )
        if  self.hadBomb < 5 then
            local planted = self:GetNWBool( "Planted" );
            local tr = LocalPlayer( ):GetEyeTrace( );
            local close2;

            if tr.Entity:IsPlayer( ) and LocalPlayer( ):GetPos( ):Distance( tr.Entity:GetPos( ) ) <= 99999999 then
                close2 = true;
            else
                close2 = false;
            end;

            local planted_text = "Not Planted!";
            local close_text = "Nobody is in range!";

            if planted then

                planted_text = "Planted!\nReload to Explode!";

                surface.SetFont( "ChatFont" );
                local size_x, size_y = surface.GetTextSize( planted_text );

                draw.RoundedBox( 6, ScrW( ) / 2 - size_x / 2 - 5, ScrH( ) - 100, size_x + 15, size_y + 10, Color( 0, 0, 0, 150 ) );
                draw.DrawText( planted_text, "ChatFont", ScrW( ) / 2, ScrH( ) - 100 + 5, Color( 255, 30, 30, 255 ), TEXT_ALIGN_CENTER );
            else
                if close2 then
                    close_text = "Close Enough!\nReload to stick!!";
                end;

                surface.SetFont( "ChatFont" );
                local size_x, size_y = surface.GetTextSize( planted_text );

                surface.SetFont( "ChatFont" );
                local size_x2, size_y2 = surface.GetTextSize( close_text );

                draw.RoundedBox( 6, ScrW( ) / 2 - ( size_x2 / 2 ) - 7.5, ScrH( ) - 100, size_x2 + 20, size_y + size_y2 + 10, Color( 0, 0, 0, 150 ) );
                draw.DrawText( planted_text, "ChatFont", ScrW( ) / 2, ScrH( ) - 100 + 5, Color( 30, 30, 255, 255 ), TEXT_ALIGN_CENTER );
                draw.DrawText( close_text, "ChatFont", ScrW( ) / 2 , ScrH( ) - 100 + size_y + 5, Color( 255, 255, 255, 255 ), TEXT_ALIGN_CENTER );
            end;
        end;
    end;
end;



SWEP.BeepSound = Sound( "C4.PlantSound" );

SWEP.ClipSet = false

function SWEP:Think( )
    local ply = self.Owner;
    if not IsValid( ply ) or self.hadBomb > 4 then return; end;

    local tr = ply:GetEyeTrace( );

    if tr.Entity:IsPlayer( ) and ply:GetPos( ):Distance( tr.Entity:GetPos( ) ) <= 99999999 and GetRoundState() == ROUND_ACTIVE then
        close = true;
    else
        close = false;
    end;


end;



function SWEP:StickIt()

   --self.Weapon:SetNextSecondaryFire( CurTime() + self.Secondary.Delay )

    local ply = self.Owner;
    if not IsValid( ply ) then return; end;

    local tr = ply:GetEyeTrace(MASK_SHOT);

    if not self:GetNWBool( "Planted" ) then

        if close then
            self.hadBomb = self.hadBomb + 1
            if SERVER then
                self.PlantedPly = tr.Entity
                self:SetNWBool("Planted", true)
            end;
        end;
    end;
end


function SWEP:DoSplode( owner, plantedply, bool )

    self:SetNWBool( "Planted", false )

        timer.Simple( 1, function( ) plantedply:EmitSound( self.BeepSound ); end );
        timer.Simple( 1.5, function( ) plantedply:EmitSound( self.BeepSound ); end );
        timer.Simple( 2, function( ) plantedply:EmitSound( self.BeepSound ); end );
        timer.Simple( 2.5, function( )
            local effectdata = EffectData()
            effectdata:SetOrigin(plantedply:GetPos())
            util.Effect("HelicopterMegaBomb", effectdata)

            local explosion = ents.Create("env_explosion")
            explosion:SetOwner(self.Owner)
            explosion:SetPos(plantedply:GetPos( ))
            explosion:SetKeyValue("iMagnitude", "45")
            explosion:SetKeyValue("iRadiusOverride", 0)
            explosion:Spawn()
            explosion:Activate()
            explosion:Fire("Explode", "", 0)
            explosion:Fire("kill", "", 0)
            self:SetNWBool( "Planted", false )



        end );




end;

function SWEP:BombSplode()
    local ply = self.Owner;
    if not IsValid( ply ) then return; end;

    if IsValid( self.PlantedPly ) and self.PlantedPly:Alive( ) then
        if SERVER then
            self:DoSplode( self.Owner, self.PlantedPly, false );
        end;
    else
        if SERVER then
            if IsValid( self.PlantedPly ) and IsValid( self.PlantedPly.server_ragdoll ) then
                self:DoSplode( self.Owner, self.PlantedPly.server_ragdoll, true );
            else
                ply:PrintMessage( HUD_PRINTTALK, "The body this bomb has been planted to is no longer available!" );
            end;
        end;
        --  self.Weapon:SendWeaponAnim( ACT_SLAM_DETONATOR_DETONATE );
    end;
end
