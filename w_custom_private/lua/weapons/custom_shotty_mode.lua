SWEP.HoldType           = "ar2"
SWEP.PrintName          = "SnipeShot v2"
SWEP.Slot               = 7

SWEP.Icon = "vgui/ttt_fun_killicons/g3sg1.png"
SWEP.Base               = "gb_camo_base"
SWEP.Spawnable = true
SWEP.AdminSpawnable = true
SWEP.Kind = WEAPON_ROLE
SWEP.WeaponID = AMMO_RIFLE
SWEP.Primary.Delay          = 0.9
SWEP.Primary.Recoil         = 4
SWEP.Primary.Automatic = true
SWEP.Primary.Ammo = "smg1"
SWEP.Primary.Damage = 55
SWEP.Primary.Cone = 0.003
SWEP.Primary.ClipSize = 35
SWEP.Primary.ClipMax = 50 -- keep mirrored to ammo
SWEP.Primary.DefaultClip = 35
SWEP.HeadshotMultiplier = 4
SWEP.IsSilent = false
SWEP.AutoSpawnable      = false
SWEP.AmmoEnt = "item_ammo_smg1_ttt"
SWEP.ViewModel		= "models/weapons/cstrike/c_snip_awp.mdl"
SWEP.WorldModel		= "models/weapons/w_snip_awp.mdl"
SWEP.Primary.Sound = Sound ("Weapon_M4A1.Silenced")
SWEP.CanDecapitate = true
SWEP.Camo = 43
SWEP.HasFlare = true

SWEP.Secondary.Sound = Sound("Default.Zoom")

SWEP.IronSightsPos      = Vector( 5, -15, -2 )
SWEP.IronSightsAng      = Vector( 2.6, 1.37, 3.5 )

function SWEP:PrimaryAttack()
    self:NormalPrimaryAttack()
    if self:Clip1() > 0 and (not self.NextFire or self.NextFire < CurTime()) then
        self.NextFire = CurTime() + 2
        self:MakeAFlame()
    end
end

function SWEP:SetZoom(state)
    if CLIENT then 
       return
    else
       if state then
          self.Owner:SetFOV(20, 0.3)
       else
          self.Owner:SetFOV(0, 0.2)
       end
    end
end

function SWEP:Think()
    if self.Owner:KeyDown(IN_USE) then
        self:SwitchMode()
    end
end

function SWEP:SwitchMode()
    if  self.NextSwitch and self.NextSwitch > CurTime() then return end
    self.NextSwitch = CurTime() + 1

    if self.IsShotty then
        self.Primary.Damage = 55
        self.HeadshotMultiplier = 2
        self.Primary.Cone = 0.006
        self.Primary.NumShots = 1
        self.Primary.Sound = Sound ("Weapon_M4A1.Silenced")
        self.IsShotty = false
        self.Owner:PrintMessage( HUD_PRINTCENTER, "Sniper mode activated" )
        if SERVER then
           self.Owner:EmitSound("HL1/fvox/deactivated.wav")
        end
    else
        self.Primary.Damage = 7
        self.HeadshotMultiplier = 1
        self.Primary.Cone = 0.12
        self.Primary.Sound = Sound ("Weapon_M3.Single")
        self.Primary.NumShots = 18
        self.IsShotty = true
        self.Owner:PrintMessage( HUD_PRINTCENTER, "Shotgun mode activated" )
        if SERVER then
            self.Owner:EmitSound("HL1/fvox/activated.wav")
        end
    end
end

-- Add some zoom to ironsights for this gun
function SWEP:SecondaryAttack()
    if not self.IronSightsPos then return end
    if self.Weapon:GetNextSecondaryFire() > CurTime() then return end
    
    bIronsights = not self:GetIronsights()
    
    self:SetIronsights( bIronsights )
    
    if SERVER then
        self:SetZoom(bIronsights)
     else
        self:EmitSound(self.Secondary.Sound)
    end
    
    self.Weapon:SetNextSecondaryFire( CurTime() + 0.3)
end

function SWEP:PreDrop()
    self:SetZoom(false)
    self:SetIronsights(false)
    return self.BaseClass.PreDrop(self)
end

function SWEP:Reload()
    self.Weapon:DefaultReload( ACT_VM_RELOAD );
    self:SetIronsights( false )
    self:SetZoom(false)
end


function SWEP:Holster()
    self:SetIronsights(false)
    self:SetZoom(false)
    return true
end

if CLIENT then
   local scope = surface.GetTextureID("sprites/scope")
   function SWEP:DrawHUD()
      if self:GetIronsights() then
         surface.SetDrawColor( 0, 0, 0, 255 )
         
         local x = ScrW() / 2.0
         local y = ScrH() / 2.0
         local scope_size = ScrH()

         -- crosshair
         local gap = 80
         local length = scope_size
         surface.DrawLine( x - length, y, x - gap, y )
         surface.DrawLine( x + length, y, x + gap, y )
         surface.DrawLine( x, y - length, x, y - gap )
         surface.DrawLine( x, y + length, x, y + gap )

         gap = 0
         length = 50
         surface.DrawLine( x - length, y, x - gap, y )
         surface.DrawLine( x + length, y, x + gap, y )
         surface.DrawLine( x, y - length, x, y - gap )
         surface.DrawLine( x, y + length, x, y + gap )


         -- cover edges
         local sh = scope_size / 2
         local w = (x - sh) + 2
         surface.DrawRect(0, 0, w, scope_size)
         surface.DrawRect(x + sh - 2, 0, w, scope_size)

         surface.SetDrawColor(255, 0, 0, 255)
         surface.DrawLine(x, y, x + 1, y + 1)

         -- scope
         surface.SetTexture(scope)
         surface.SetDrawColor(255, 255, 255, 255)

         surface.DrawTexturedRectRotated(x, y, scope_size, scope_size, 0)

      else
         return self.BaseClass.DrawHUD(self)
      end
   end

   function SWEP:AdjustMouseSensitivity()
      return (self:GetIronsights() and 0.2) or nil
   end
end