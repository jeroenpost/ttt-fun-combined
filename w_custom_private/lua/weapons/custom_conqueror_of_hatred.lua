if SERVER then
    AddCSLuaFile ()
    SWEP.AutoSwitchTo        = true
    SWEP.AutoSwitchFrom        = false
end
SWEP.CanDecapitate= true
--SWEP.HasFireBullet = true
SWEP.DrawCrosshair        = false
SWEP.PrintName            = "CONQUEROR OF HATRED"

SWEP.Spawnable                = true
SWEP.AdminSpawnable            = true

SWEP.Category       		= "uacport Sweps"

SWEP.ViewModel			= "models/weapons/v_mp7_silenced.mdl"
SWEP.WorldModel			= "models/weapons/w_mp7_silenced.mdl"
SWEP.ViewModelFlip = true
SWEP.BobScale            = 2
SWEP.SwayScale            = 2
SWEP.Base = "gb_camo_base"
SWEP.Camo = 15

SWEP.Primary.ClipSize        = -1
SWEP.Primary.DefaultClip    = -1
SWEP.Primary.Automatic        = true
SWEP.Primary.Ammo            = "none"
SWEP.HoldType = "shotgun"
SWEP.Secondary.ClipSize        = -1
SWEP.Secondary.DefaultClip    = -1
SWEP.Secondary.Automatic    = false
SWEP.Secondary.Ammo            = "none"

function SWEP:SecondaryAttack()
    if self.Owner:KeyDown(IN_USE) then
        self:Zoom()
        return
    end

    self:Fly()
end

function SWEP:Holster()
    self:SetIronsights(false)
    self:SetZoom(false)
    self:EndSound()
    return true
end

--actual SWEP details
SWEP.ViewModelDefPos = Vector (-40.9885, -90.7463, -22.2584)
SWEP.ViewModelDefAng = Vector (-178.6234, 40.6472, 100.4833)

SWEP.MoveToPos = Vector (-105, -53, 20)
SWEP.MoveToAng = Vector (-230, 45, 185)

SWEP.Sound = "hatred.mp3"

SWEP.Volume = 500
SWEP.Influence = 0
SWEP.ShakesNormal = 0
SWEP.Shakes = SWEP.ShakesNormal
SWEP.TotalShakes = 0
SWEP.LastSoundRelease = 0
SWEP.TotalShakes = 0
SWEP.lastActivate = 0
SWEP.RestartDelay = 1
SWEP.RandomEffectsDelay = 0.2
SWEP.LaserColor = Color(120,120,0,255)
function SWEP:PreDrop()
    self:SetZoom(false)
    self:SetIronsights(false)
    return self.BaseClass.PreDrop(self)
end


function SWEP:PrimaryAttack()

    if self.Owner:KeyDown(IN_USE) then
        if self.bombs then
            for k,v in pairs(self.bombs) do
                if not v.Explode then continue end
                v:Explode()
            end
            self.bombs = {}
        end

        return
    end

    local mode = self:GetNWString("shootmode")

    if mode == "Beam" then
            self:CreateSound ()
            self.Weapon:SetNextPrimaryFire( CurTime() + 0.02 )


            local tr, vm, muzzle, effect
            vm = self.Owner:GetViewModel( )
            tr = { }
            tr.start = self.Owner:GetShootPos( )
            tr.filter = self.Owner
            tr.endpos = tr.start + self.Owner:GetAimVector( ) * 4096
            tr.mins = Vector( ) * -2
            tr.maxs = Vector( ) * 2
            tr = util.TraceHull( tr )


            -- if self.Shakes != self.ShakesNormal then
            effect = EffectData( )
            effect:SetStart( tr.StartPos + Vector(0,0,-10) )
            effect:SetOrigin( tr.HitPos  )
            effect:SetEntity( self )
            effect:SetAttachment( vm:LookupAttachment( "muzzle" ) )
            util.Effect( "ToolTracer", effect )


            local bullet = {}
            bullet.Num 		= 1
            bullet.Src 		= self.Owner:GetShootPos()
            bullet.Dir 		= self.Owner:GetAimVector()
            bullet.Spread 	= Vector( 0, 0, 0 )
            bullet.Tracer	= 0
            bullet.Force	= 1000
            bullet.Damage	= 3
            bullet.AmmoType = "Buckshot"
            self.Owner:FireBullets( bullet )

    end
    if mode == "SMG" then
        self:FireBolt( 0.1, 15)
        self.Weapon:EmitSound("Weapon_USP.SilencedShot")
        self.Weapon:SetNextPrimaryFire( CurTime() + 0.15 )
    end
    if mode == "Shotty" then
        self:FireBolt( 0.1, 15,0.085)
        timer.Simple(0.01,function()
            self:FireBolt( 0.1, 15,0.085)
        end)
        timer.Simple(0.02,function()
            self:FireBolt( 0.1, 15,0.085)
        end)
        timer.Simple(0.03,function()
            self:FireBolt( 0.1, 15,0.085)
        end)
        timer.Simple(0.04,function()
            self:FireBolt( 0.1, 15,0.085)
        end)

        local bullet = {}
        bullet.Num 		= 5
        bullet.Src 		= self.Owner:GetShootPos()
        bullet.Dir 		= self.Owner:GetAimVector()
        bullet.Spread 	= Vector( 0.085, 0.085, 0.085 )
        bullet.Tracer	= 0
        bullet.Force	= 1000
        bullet.Damage	= 12
        bullet.AmmoType = "Buckshot"
        self.Owner:FireBullets( bullet )
        self.Weapon:EmitSound("Weapon_USP.SilencedShot")
        self.Weapon:SetNextPrimaryFire( CurTime() + 0.9 )
    end
    if mode == "Sniper" then
        local tr, vm, muzzle, effect
        vm = self.Owner:GetViewModel( )
        tr = { }
        tr.start = self.Owner:GetShootPos( )
        tr.filter = self.Owner
        tr.endpos = tr.start + self.Owner:GetAimVector( ) * 4096
        tr.mins = Vector( ) * -2
        tr.maxs = Vector( ) * 2
        tr = util.TraceHull( tr )


        -- if self.Shakes != self.ShakesNormal then
        effect = EffectData( )
        effect:SetStart( tr.StartPos + Vector(0,0,-10) )
        effect:SetOrigin( tr.HitPos  )
        effect:SetEntity( self )
        effect:SetAttachment( vm:LookupAttachment( "muzzle" ) )
        util.Effect( "ToolTracer", effect )


        local bullet = {}
        bullet.Num 		= 1
        bullet.Src 		= self.Owner:GetShootPos()
        bullet.Dir 		= self.Owner:GetAimVector()
        bullet.Spread 	= Vector( 0, 0, 0 )
        bullet.Tracer	= 0
        bullet.Force	= 1000
        bullet.Damage	= 85
        bullet.AmmoType = "Buckshot"
        self.Owner:FireBullets( bullet )
        self.Weapon:EmitSound("Weapon_USP.SilencedShot")
        self.Weapon:SetNextPrimaryFire( CurTime() + 1.2 )
    end
    if mode == "Grenade" then
        self:CreateGrenade(45)
        self.Weapon:SetNextPrimaryFire( CurTime() + 3 )
    end

end

function SWEP:OnDrop()
    self:EndSound()
    self:Remove()
end

function SWEP:Deploy()
    self.Weapon:SendWeaponAnim(ACT_VM_DRAW_SILENCED)
    self:SetNWString("shootmode","Beam")
    return true
end

SWEP.NextReload = 0
function SWEP:Reload()

    if self.Owner:KeyDown(IN_USE) and self.NextReload < CurTime() then
        self.NextReload = CurTime() + 0.3
        local mode = self:GetNWString("shootmode")
        self:SetIronsights(false)
        self:SetZoom(false)
        if mode == "Beam" then  self:SetNWString("shootmode","SMG") self.Primary.Sound = "weapons/shotgun/shotgun_dbl_fire.wav" end
        if mode == "SMG" then  self:SetNWString("shootmode","Grenade") self.Primary.Sound = "weapons/crossbow/fire1.wav" end
        if mode == "Grenade" then  self:SetNWString("shootmode","Shotty") self.Primary.Sound = "weapons/crossbow/fire1.wav" end
        if mode == "Shotty" then  self:SetNWString("shootmode","Sniper") self.Primary.Sound = "weapons/crossbow/fire1.wav" end
        if mode == "Sniper" then  self:SetNWString("shootmode","Beam") self.Primary.Sound = "weapons/crossbow/fire1.wav" end

         self.Weapon:SetNextPrimaryFire( CurTime() + 0.8 )
        return
    end

end


function SWEP:CreateGrenade( damage )

    if IsValid(self.Owner) && IsValid(self.Weapon) then

    if self.bombs and #self.bombs > 2 then
        self.Owner:PrintMessage( HUD_PRINTCENTER, "3 bombs max" )
        return
    end

    if (SERVER) then

        local ent = ents.Create("hatrednade")
        if !ent then return end
        ent.Owner = self.Owner
        ent.Inflictor = self.Weapon
        ent.Damage = damage
        ent:SetOwner(self.Owner)
        local eyeang = self.Owner:GetAimVector():Angle()
        local right = eyeang:Right()
        local up = eyeang:Up()
        if self.Tossed then
            ent:SetPos(self.Owner:GetShootPos()+right*4-up*4)
        else
            ent:SetPos(self.Owner:GetShootPos()+right*4+up*4)
        end
        ent:SetAngles(self.Owner:GetAngles())
        ent:SetPhysicsAttacker(self.Owner)
        ent:Spawn()

        if not self.bombs then self.bombs = {} end
        table.insert(self.bombs,ent)

        local phys = ent:GetPhysicsObject()
        if phys:IsValid() then
            phys:SetVelocity(self.Owner:GetAimVector() * self.ThrowVel + (self.Owner:GetVelocity() * 0.75))
        end

    end

    end

end

function SWEP:Think()

    if !self.Owner:KeyDown( IN_ATTACK ) and self.Weapon:GetNWBool("on") then
    self:EndSound ()
    end

    if self.Owner:KeyDown(IN_ATTACK2) then
        self.ThrowVel = 700
        self.Damage = 150
        self.Tossed = true
    elseif self.Owner:KeyDown(IN_ATTACK) then
        self.ThrowVel = 1500
        self.Damage = 75
        self.Tossed = false
    end

    if self.StartThrow && !self.Owner:KeyDown(IN_ATTACK) && !self.Owner:KeyDown(IN_ATTACK2) && self.NextThrow < CurTime() then

    self.StartThrow = false
    self.Throwing = true
    if self.Tossed then
        self.Weapon:SendWeaponAnim(ACT_VM_SECONDARYATTACK)
    else
        self.Weapon:SendWeaponAnim(ACT_VM_THROW)
    end
    self.Owner:SetAnimation(PLAYER_ATTACK1)
    self:CreateGrenade(self.Damage)
    self:TakePrimaryAmmo(1)
    self.NextAnimation = CurTime() + self.Primary.Delay
    self.ResetThrow = true

    end

    if self.Throwing && self.ResetThrow && self.NextAnimation < CurTime() then

    if self.Owner:GetAmmoCount(self.Primary.Ammo) > 0 && self:Clip1() <= 0 then

    self.Owner:RemoveAmmo(1, self.Primary.Ammo)
    self:SetClip1(self:Clip1()+1)
    self.Weapon:SendWeaponAnim(ACT_VM_DRAW)
    if IsValid(self.Owner:GetViewModel()) then
        self.NextThrow = CurTime() + self.Owner:GetViewModel():SequenceDuration()
        self.Weapon:SetNextPrimaryFire(CurTime() + self.Owner:GetViewModel():SequenceDuration())
        self.Weapon:SetNextSecondaryFire(CurTime() + self.Owner:GetViewModel():SequenceDuration())
    end

    else
        self.Owner:ConCommand("lastinv")
    end

    self.ResetThrow = false
    self.Throwing = false

    end

end


SWEP.RandomEffects = {

}
SWEP.SoundObject = false
SWEP.LastFrame = false
SWEP.RestartDelay = 0
SWEP.SoundPlaying = false
SWEP.LastSoundRelease = 0


SWEP.NextSoundCheck = 0
function SWEP:CreateSound ()
    if not self.Weapon:GetNWBool("on") and ( not self.NextSoundCheck2 or self.NextSoundCheck2 < CurTime()) then
        self.NextSoundCheck2 = CurTime() + 1

        gb_PlaySoundFromServer(gb_config.websounds.."hatred.mp3", self.Owner,true,"hatredsound");
        self.Weapon:SetNWBool ("on", true)
        timer.Create("hatredplay",29,10,function()
            if not IsValid(self) then return end
            self.Weapon:SetNWBool ("on", true)
            gb_PlaySoundFromServer(gb_config.websounds.."hatred.mp3", self.Owner,true,"hatredsound");
        end)


    end

end


function SWEP:OwnerChanged() self:EndSound() end


SWEP.IronSightsPos      = Vector( -100, -100, -100 )
SWEP.IronSightsAng      = Vector( 2.6, 1.37, 3.5 )
SWEP.Secondary.Sound = Sound("Default.Zoom")
-- Add some zoom to ironsights for this gun
function SWEP:Zoom()
    if not self.IronSightsPos then return end
    if self.Weapon:GetNextSecondaryFire() > CurTime() then return end

    local bIronsights = not self:GetIronsights()

    self:SetIronsights( bIronsights )

    if SERVER then
        self:SetZoom(bIronsights)
    else
        self:EmitSound(self.Secondary.Sound)
    end

    self.Weapon:SetNextSecondaryFire( CurTime() + 0.3)
end


function SWEP:EndSound()
    self.Weapon:SetNWBool ("on", false)
    timer.Destroy("hatredplay")
    gb_StopSoundFromServer("hatredsound")
end


if CLIENT then
    function SWEP:DrawHUD( )
        local shotttext = "Shootmode: "..self:GetNWString("shootmode").."\nE+R to change"
        surface.SetFont( "ChatFont" );
        local size_x, size_y = surface.GetTextSize( shotttext );

        draw.RoundedBox( 6, ScrW( ) / 2 - size_x / 2 - 5, ScrH( ) - 100, size_x + 15, size_y + 10, Color( 0, 0, 0, 150 ) );
        draw.DrawText( shotttext , "ChatFont", ScrW( ) / 2, ScrH( ) - 100 + 5, Color( 255, 30, 30, 255 ), TEXT_ALIGN_CENTER );

        local scope = surface.GetTextureID("sprites/scope")
        if self:GetIronsights() then
            surface.SetDrawColor( 0, 0, 0, 255 )

            local x = ScrW() / 2.0
            local y = ScrH() / 2.0
            local scope_size = ScrH()

            -- crosshair
            local gap = 80
            local length = scope_size
            surface.DrawLine( x - length, y, x - gap, y )
            surface.DrawLine( x + length, y, x + gap, y )
            surface.DrawLine( x, y - length, x, y - gap )
            surface.DrawLine( x, y + length, x, y + gap )

            gap = 0
            length = 50
            surface.DrawLine( x - length, y, x - gap, y )
            surface.DrawLine( x + length, y, x + gap, y )
            surface.DrawLine( x, y - length, x, y - gap )
            surface.DrawLine( x, y + length, x, y + gap )


            -- cover edges
            local sh = scope_size / 2
            local w = (x - sh) + 2
            surface.DrawRect(0, 0, w, scope_size)
            surface.DrawRect(x + sh - 2, 0, w, scope_size)

            surface.SetDrawColor(255, 0, 0, 255)
            surface.DrawLine(x, y, x + 1, y + 1)

            -- scope
            surface.SetTexture(scope)
            surface.SetDrawColor(255, 255, 255, 255)

            surface.DrawTexturedRectRotated(x, y, scope_size, scope_size, 0)

        else
            return self.BaseClass.DrawHUD(self)
        end
    end

    function SWEP:AdjustMouseSensitivity()
        return (self:GetIronsights() and 0.2) or nil
    end
end;
function SWEP:SetZoom(state)
    if CLIENT then
        return
    elseif IsValid(self.Owner) and self.Owner:IsPlayer() then
        if state then
            self.Owner:SetFOV(30, 0.3)
        else
            self.Owner:SetFOV(0, 0.2)
        end
    end
end



local ENT = {}

ENT.Type = "anim"
ENT.Base = "base_anim"

if CLIENT then
    ENT.Mat = Material("sprites/light_glow02_add")
    ENT.Scaled = false

    function ENT:Draw()
        self:DrawModel()
        render.SetMaterial(self.Mat)
        render.DrawSprite(self:GetPos(), 72+(16*math.sin(CurTime()*5)), 72+(16*math.sin(CurTime()*5)), Color(50,150,255,255))
        render.DrawSprite(self:GetPos(), 64+(16*math.sin(CurTime()*5)), 64+(16*math.sin(CurTime()*5)), Color(50,255,255,255))
    end
end

if SERVER then

    function ENT:Initialize()

        self.Hit = false
        self.LastShout = CurTime()
        self.CurrentPitch = 100
        self.SpawnDelay = CurTime() + 0.5
        --damage self.Damage = 75
        self:SetModel("models/dav0r/hoverball.mdl")
        self:PhysicsInitBox(Vector(-1,-1,-1),Vector(1,1,1))
        self:SetMoveType(MOVETYPE_VPHYSICS)
        self:SetSolid(SOLID_VPHYSICS)
        self:SetTrigger(true)
        self:SetMaterial("models/weapons/v_grenade/grenade body")
        self:SetModelScale(self:GetModelScale()*0.5,0)
        self:SetColor(0,200,255,255)
        self:DrawShadow(false)


        local phys = self:GetPhysicsObject()
        if (phys:IsValid()) then
            phys:Wake()
            phys:EnableDrag(false)
            phys:AddGameFlag(FVPHYSICS_NO_IMPACT_DMG)
            phys:SetBuoyancyRatio(0)
        end

        self.Fear = ents.Create("ai_sound")
        self.Fear:SetPos(self:GetPos())
        self.Fear:SetParent(self)
        self.Fear:SetKeyValue("SoundType", "8|1")
        self.Fear:SetKeyValue("Volume", "0.2")
        self.Fear:SetKeyValue("Duration", "1")
        self.Fear:Spawn()

        self.Trail = util.SpriteTrail(self, 0, Color(0,100,255), false, 32, 1, 0.3, 0.01, "trails/plasma.vmt")
        self.WhirrSound = CreateSound(self, "ambient/energy/force_field_loop1.wav")
        self.WhirrSound:Play()
        self.WhirrSound:ChangeVolume(0.2, 0.1)


        self:Fire("kill", 1, 60)

    end

    function ENT:Think()

        if self.LastShout < CurTime() then
            if IsValid(self.Fear) then
                self.Fear:Fire("EmitAISound")
            end
            self.LastShout = CurTime() + 0.25
        end

        if self.Hit then
            self.CurrentPitch = self.CurrentPitch + 5
            if self.WhirrSound then self.WhirrSound:ChangePitch(math.Clamp(self.CurrentPitch,100,255),0) end
        end



    end

    function ENT:OnRemove()
        if self.WhirrSound then self.WhirrSound:Stop() end
        if IsValid(self.Fear) then self.Fear:Fire("kill") end
    end

    function ENT:PhysicsUpdate(phys)
        if !self.Hit then
        self:SetLocalAngles(phys:GetVelocity():Angle())
        else
            phys:SetVelocity(Vector(phys:GetVelocity().x*0.95,phys:GetVelocity().y*0.95,phys:GetVelocity().z))
        end
    end

    function ENT:Explode()
        if self.exploded then return end self.exploded = true
        util.ScreenShake( self:GetPos(), 50, 50, 1, 250 )
        if IsValid(self.Owner) then
            if IsValid(self.Inflictor) then
                util.BlastDamage(self.Inflictor, self.Owner, self:GetPos(), 250, (self.Damage))
            else
                util.BlastDamage(self, self.Owner, self:GetPos(), 250, (self.Damage))
            end
        end

        local fx = EffectData()
        fx:SetOrigin(self:GetPos())
        util.Effect("plasmasplode", fx)

        self:EmitSound("ambient/energy/ion_cannon_shot"..math.random(1,3)..".wav",90,100)
        self:EmitSound("ambient/explosions/explode_"..math.random(7,9)..".wav",90,100)
        self:EmitSound("weapons/explode"..math.random(3,5)..".wav",90,85)

        self:Remove()
    end

    function ENT:Touch(ent)
        if IsValid(ent) && !self.Stuck  && !ent.hasShieldPotato  then
        if ent:IsNPC() || (ent:IsPlayer() && ent != self:GetOwner()) || (ent == self:GetOwner() && self.SpawnDelay < CurTime() ) || ent:IsVehicle() then
        self:SetSolid(SOLID_NONE)
        self:SetMoveType(MOVETYPE_NONE)
        self:SetParent(ent)
        if !self.Splodetimer then
        self.Splodetimer = CurTime() + 2
        end
        self.Stuck = true
        self.Hit = true
        end
        end
    end

    function ENT:PhysicsCollide(data,phys)
        if self:IsValid() && !self.Hit then
        self:SetCollisionGroup(COLLISION_GROUP_DEBRIS)
        self:SetMoveType( MOVETYPE_NONE );
        if !self.Splodetimer then
        self.Splodetimer = CurTime() + 2
        end
        self.Hit = true
        end
    end

end

scripted_ents.Register( ENT, "hatrednade", true )


    local ENT = {}

    ENT.Type = "anim"
    ENT.Base = "base_anim"



    if SERVER then

        function ENT:Initialize()

            self.Hit = false
            self.LastShout = CurTime()
            self.CurrentPitch = 100
            self.SpawnDelay = CurTime() + 0.5
            --damage self.Damage = 75
            self:SetModel(table.Random({"models/props_phx/misc/egg.mdl","models/props_phx/misc/potato.mdl"}))
            self:SetModelScale(1.5,0.01)
            --self:SetMaterial("phoenix_storms/plastic" )
            --self:SetColor(Color(0, 255, 0,255))
            self:PhysicsInitBox(Vector(-4,-4,-4),Vector(4,4,4))
            self:SetMoveType(MOVETYPE_VPHYSICS)
            self:SetSolid(SOLID_VPHYSICS)
            self:SetTrigger(true)
            self.Mag = 30

            self:DrawShadow(false)
            -- self:SetModel( "models/weapons/w_rif_famas.mdl")

            local phys = self:GetPhysicsObject()
            if (phys:IsValid()) then
                phys:Wake()
                phys:EnableDrag(false)
                phys:AddGameFlag(FVPHYSICS_NO_IMPACT_DMG)
                --    phys:SetBuoyancyRatio(0)
            end

        end

        function ENT:Explode()
            if self.exploded then return end self.exploded = true
            local ply = self.Owner


            local guys = ents.FindInSphere( self:GetPos()+Vector(0,0,40), 50 )
            local owner = self.Owner
            for k, guy in pairs(guys) do
                if owner != guy and (guy:IsPlayer() or guy:IsNPC())  then


                if IsValid(owner) and IsValid(guy)  then
                    guy:TakeDamage( 4, owner, owner )
                    guy:Ignite(0.5)
                    local tr = self.Owner:GetEyeTrace()
                    if SERVER then
                        local entz = guy
                        if (entz:IsPlayer() or entz:IsNPC()) and  isfunction(entz.SetEyeAngles) then
                            if (self.Owner.NextDisOrentate and self.Owner.NextDisOrentate > CurTime()) then return end
                            self.Owner.NextDisOrentate = CurTime() + 4
                            local eyeang = entz:EyeAngles()
                            local j = 20
                            eyeang.pitch = math.Clamp(eyeang.pitch + math.Rand(-j, j), -50, 50)
                            eyeang.yaw = math.Clamp(eyeang.yaw + math.Rand(-j, j), -50, 50)
                            entz:SetEyeAngles(eyeang)
                        end
                    end
                end

                end
            end

            -- util.BlastDamage( self, self.Owner, self:GetPos(), 50, 75 )

            self:EmitSound( "ambient/fire/gascan_ignite1.wav" )
            if SERVER then self:Remove() end
        end


        function ENT:Touch(ent)
            if ent != self.Owner then
            self:Explode()
            end
        end

        function ENT:PhysicsCollide(data,phys)
            if ent != self.Owner and SERVER then
            self:Explode()
            end
        end

    end

    scripted_ents.Register( ENT, "hatredpotato", true )