if SERVER then
   AddCSLuaFile(  )
end
   
SWEP.HoldType = "physgun"
SWEP.PrintName = "Energy Rifle"
   SWEP.Slot = 3

SWEP.HasFireBullet = true
SWEP.CanDecapitate = true

if CLIENT then
   

   SWEP.Icon = "VGUI/ttt/icon_launch"

   SWEP.ViewModelFlip = true
   SWEP.ViewModelFOV = 54
end
 
SWEP.Base				= "aa_base"
SWEP.Kind = WEAPON_GRENADE
SWEP.Primary.Ammo   = "none"

SWEP.Primary.DefaultClip	= 5
SWEP.Primary.Automatic		= true
SWEP.Primary.Delay = 0.1
SWEP.Primary.ClipSize= 10
SWEP.Primary.ClipMax= 10
SWEP.Primary.Ammo  = "none"
SWEP.Primary.Cone  = 0.005
SWEP.Primary.Damage = 10
SWEP.Primary.Recoil = 0
SWEP.Secondary.ClipSize		= -1
SWEP.Secondary.DefaultClip	= -1
SWEP.Secondary.Automatic	= true
SWEP.Secondary.Ammo  = "none"
SWEP.Secondary.Delay = 0.8
SWEP.Secondary.Recoil = 0

SWEP.HeadshotMultiplier = 1

SWEP.AutoSpawnable = false
SWEP.NoSights = true

SWEP.Primary.Sound = Sound( "weapons/m3/s3-1.mp3" )
SWEP.Primary.SoundLevel = 70


SWEP.ViewModel = "models/weapons/v_snip_g3sg1.mdl"
SWEP.WorldModel = "models/weapons/w_snip_g3sg1.mdl"

SWEP.nextRefill = 0
function SWEP:Think()

    if  self.nextRefill < CurTime() and self:Clip1() < 10 then
         self:SetClip1( 10 )
         self.nextRefill = CurTime() + 2
    end

end
 
function SWEP:Reload()
     self.Weapon:SetNextSecondaryFire( CurTime() + 0.14 )
   self.Weapon:SetNextPrimaryFire( CurTime() + 0.14 )

   if  self:Clip1() < 5 then return end

   if not worldsnd then
      self.Weapon:EmitSound( self.Primary.Sound, self.Primary.SoundLevel )
   elseif SERVER then
      sound.Play(self.Primary.Sound, self:GetPos(), self.Primary.SoundLevel)
   end

   self:ShootBullet3( 0, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone(), 30,75 )

   self:TakePrimaryAmmo( 5 )

   local owner = self.Owner
   if not IsValid(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

   owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )

     local trace = self.Owner:GetEyeTrace()
                    if (trace.HitNonWorld) then
                            local victim = trace.Entity
                            if (!victim:IsPlayer()) then return end


                             if SERVER then  
                                        victim.IsBlinded = true
                                        victim:SetNWBool("isblinded",true)
                                        umsg.Start( "ulx_blind", victim )
                                                umsg.Bool( true )
                                                umsg.Short( 255 )
                                        umsg.End()
                             end
                                victim:AnimPerformGesture(ACT_GMOD_TAUNT_PERSISTENCE);
                                if (SERVER) then    
                                    --victim:EmitSound("weapons/gb_weapons/laughtergun1.mp3",480,100);
                                    
                                 
                                    timer.Create("TakeHealthLightGun2"..victim:UniqueID(),5,3, function()
                                       if IsValid(victim) then      
                                        
                                           victim:AnimPerformGesture(ACT_GMOD_TAUNT_PERSISTENCE);
                                       end
                                    end)

                                    timer.Create("ResetPLayerAfterBlided"..victim:UniqueID(), 0.5,1, function()
                                         if !IsValid(victim)  then  return end 
                                 
                                            if SERVER then
                                                victim.IsBlinded = false
                                                victim:SetNWBool("isblinded",false)
                                                 umsg.Start( "ulx_blind", victim )
                                                        umsg.Bool( false )
                                                        umsg.Short( 0 )
                                                 umsg.End()
                                               
                                            end
                                     end)
                            end
                    end


end
function SWEP:OnDrop()
    self:Remove()
end


SWEP.NextFlame = 0
function SWEP:PrimaryAttack(worldsnd)

   self.Weapon:SetNextSecondaryFire( CurTime() + 0.1 )
   self.Weapon:SetNextPrimaryFire( CurTime() + 0.1 )

   if not self:CanPrimaryAttack() then return end

   self:Disorientate()

   if not worldsnd then
      self.Weapon:EmitSound( self.Primary.Sound, self.Primary.SoundLevel )
   elseif SERVER then
      sound.Play(self.Primary.Sound, self:GetPos(), self.Primary.SoundLevel)
   end

   self:ShootBullet( self.Primary.Damage, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone(), 8, 10 )
   if self.NextFlame < CurTime() then
       self.NextFlame = CurTime() + 0.25
       self:MakeAFlame()
   end

   --self:TakePrimaryAmmo( 1 )
   self:Disorientate()

   local owner = self.Owner
   if not IsValid(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

   owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )
end

function SWEP:SecondaryAttack(worldsnd)

   self.Weapon:SetNextSecondaryFire( CurTime() + 0.07 )
   self.Weapon:SetNextPrimaryFire( CurTime() + 0.07 )

   if not self:CanPrimaryAttack() then return end

   if not worldsnd then
      self.Weapon:EmitSound( self.Primary.Sound, self.Primary.SoundLevel )
   elseif SERVER then
      sound.Play(self.Primary.Sound, self:GetPos(), self.Primary.SoundLevel)
   end

   self:ShootBullet2( 9, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone(), 18, 25 )
   self:MakeAFlame()

   self:TakePrimaryAmmo( 1 )

   local owner = self.Owner
   if not IsValid(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

   owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )
end


function SWEP:ShootBullet( dmg, recoil, numbul, cone, scale, magnitude )
   local sights = self:GetIronsights()

   numbul = numbul or 1
   cone   = cone   or 0.01

   -- 10% accuracy bonus when sighting
   cone = sights and (cone * 0.9) or cone

   local bullet = {}
   bullet.Num    = numbul
   bullet.Src    = self.Owner:GetShootPos()
   bullet.Dir    = self.Owner:GetAimVector()
   bullet.Spread = Vector( cone, cone, 0 )
   bullet.Tracer = 1
   bullet.TracerName = "AirboatGunHeavyTracer"
   bullet.Force  = 5
   bullet.Damage = dmg


      bullet.Callback = function(att, tr, dmginfo)
                        if SERVER or (CLIENT and IsFirstTimePredicted()) then
                           local ent = tr.Entity
                           if (not tr.HitWorld) and IsValid(ent) then
                              local edata = EffectData()

                              edata:SetEntity(ent)
                              edata:SetMagnitude(1)
                              edata:SetScale(1)

                              util.Effect("TeslaHitBoxes", edata)

                              if SERVER and ent:IsPlayer() then
                                 local eyeang = ent:EyeAngles()

                                 local j = math.Rand(1,5) --10
                                 eyeang.pitch = math.Clamp(eyeang.pitch + math.Rand(-j, j), -90, 90)
                                 eyeang.yaw = math.Clamp(eyeang.yaw + math.Rand(-j, j), -90, 90)
                                 ent:SetEyeAngles(eyeang)
                              end
                           end
                        end
                     end

   self.Owner:FireBullets( bullet )
   self.Weapon:SendWeaponAnim(self.PrimaryAnim)

   -- Owner can die after firebullets, giving an error at muzzleflash
   if not IsValid(self.Owner) or not self.Owner:Alive() then return end

   self.Owner:MuzzleFlash()
   self.Owner:SetAnimation( PLAYER_ATTACK1 )

   if self.Owner:IsNPC() then return end

   if ((game.SinglePlayer() and SERVER) or
       ((not game.SinglePlayer()) and CLIENT and IsFirstTimePredicted() )) then

      -- reduce recoil if ironsighting
      recoil = sights and (recoil * 0.75) or recoil

      local eyeang = self.Owner:EyeAngles()
      eyeang.pitch = eyeang.pitch - recoil
      self.Owner:SetEyeAngles( eyeang )

   end
end



function SWEP:ShootBullet2( dmg, recoil, numbul, cone, scale, magnitude )
   local sights = self:GetIronsights()

   numbul = numbul or 1
   cone   = cone   or 0.01

   -- 10% accuracy bonus when sighting
   cone = sights and (cone * 0.9) or cone

   local bullet = {}
   bullet.Num    = numbul
   bullet.Src    = self.Owner:GetShootPos()
   bullet.Dir    = self.Owner:GetAimVector()
   bullet.Spread = Vector( cone, cone, 0 )
   bullet.Tracer = 1
   bullet.TracerName = "AirboatGunHeavyTracer"
   bullet.Force  = 5
   bullet.Damage = dmg


      bullet.Callback = function(att, tr, dmginfo)
                        if SERVER or (CLIENT and IsFirstTimePredicted()) then
                           local ent = tr.Entity
                           if (not tr.HitWorld) and IsValid(ent) then
                              local edata = EffectData()

                              edata:SetEntity(ent)
                              edata:SetMagnitude(3)
                              edata:SetScale(2)

                              util.Effect("TeslaHitBoxes", edata)

                              if SERVER and ent:IsPlayer() then
                                 local eyeang = ent:EyeAngles()

                                 local j = math.Rand(1,25) --10
                                 eyeang.pitch = math.Clamp(eyeang.pitch + math.Rand(-j, j), -90, 90)
                                 eyeang.yaw = math.Clamp(eyeang.yaw + math.Rand(-j, j), -90, 90)
                                 ent:SetEyeAngles(eyeang)
                              end
                           end
                        end
                     end

   self.Owner:FireBullets( bullet )
   self.Weapon:SendWeaponAnim(self.PrimaryAnim)

   -- Owner can die after firebullets, giving an error at muzzleflash
   if not IsValid(self.Owner) or not self.Owner:Alive() then return end

   self.Owner:MuzzleFlash()
   self.Owner:SetAnimation( PLAYER_ATTACK1 )

   if self.Owner:IsNPC() then return end

   if ((game.SinglePlayer() and SERVER) or
       ((not game.SinglePlayer()) and CLIENT and IsFirstTimePredicted() )) then

      -- reduce recoil if ironsighting
      recoil = sights and (recoil * 0.75) or recoil

      local eyeang = self.Owner:EyeAngles()
      eyeang.pitch = eyeang.pitch - recoil
      self.Owner:SetEyeAngles( eyeang )

   end
end


function SWEP:ShootBullet3( dmg, recoil, numbul, cone, scale, magnitude )
   local sights = self:GetIronsights()

   numbul = numbul or 1
   cone   = cone   or 0.01

   -- 10% accuracy bonus when sighting
   cone = sights and (cone * 0.9) or cone

   local bullet = {}
   bullet.Num    = numbul
   bullet.Src    = self.Owner:GetShootPos()
   bullet.Dir    = self.Owner:GetAimVector()
   bullet.Spread = Vector( cone, cone, 0 )
   bullet.Tracer = 1
   bullet.TracerName = "AirboatGunHeavyTracer"
   bullet.Force  = 5
   bullet.Damage = dmg


      bullet.Callback = function(att, tr, dmginfo)
                        if SERVER or (CLIENT and IsFirstTimePredicted()) then
                           local ent = tr.Entity
                           if (not tr.HitWorld) and IsValid(ent) then
                              local edata = EffectData()

                              edata:SetEntity(ent)
                              edata:SetMagnitude(4)
                              edata:SetScale(3)

                              util.Effect("TeslaHitBoxes", edata)

                              if SERVER and ent:IsPlayer() then
                                 local eyeang = ent:EyeAngles()

                                 local j = math.Rand(10,30) --10
                                 eyeang.pitch = math.Clamp(eyeang.pitch + math.Rand(-j, j), -90, 90)
                                 eyeang.yaw = math.Clamp(eyeang.yaw + math.Rand(-j, j), -90, 90)
                                 ent:SetEyeAngles(eyeang)
                              end
                           end
                        end
                     end

   self.Owner:FireBullets( bullet )
   self.Weapon:SendWeaponAnim(self.PrimaryAnim)

   -- Owner can die after firebullets, giving an error at muzzleflash
   if not IsValid(self.Owner) or not self.Owner:Alive() then return end

   self.Owner:MuzzleFlash()
   self.Owner:SetAnimation( PLAYER_ATTACK1 )

   if self.Owner:IsNPC() then return end

   if ((game.SinglePlayer() and SERVER) or
       ((not game.SinglePlayer()) and CLIENT and IsFirstTimePredicted() )) then

      -- reduce recoil if ironsighting
      recoil = sights and (recoil * 0.75) or recoil

      local eyeang = self.Owner:EyeAngles()
      eyeang.pitch = eyeang.pitch - recoil
      self.Owner:SetEyeAngles( eyeang )

   end
end