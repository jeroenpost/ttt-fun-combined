SWEP.VElements = {
    ["leftclaw+"] = { type = "Model", model = "models/mechanics/robotics/claw.mdl", bone = "ValveBiped.base", rel = "", pos = Vector(-1.395, -1.005, 5.638), angle = Angle(-180, 0, 179.968), size = Vector(0.086, 0.086, 0.086), color = Color(255,255,255, 255), surpresslightning = false, material = "camos/custom_camo17", skin = 0, bodygroup = {} },
    ["pipe1+++++"] = { type = "Model", model = "models/props_canal/mattpipe.mdl", bone = "ValveBiped.Bip01", rel = "barrel", pos = Vector(1.129, -0.058, 5.25), angle = Angle(-0.508, 180, 0.261), size = Vector(1,1,1), color = Color(255,255,255, 255), surpresslightning = false, material = "camos/custom_camo17", skin = 0, bodygroup = {} },
    ["pipe1"] = { type = "Model", model = "models/props_canal/mattpipe.mdl", bone = "ValveBiped.Bip01", rel = "barrel", pos = Vector(0, -1.264, 5.144), angle = Angle(0.1, -83.37, 0), size = Vector(1,1,1), color = Color(255,255,255, 255), surpresslightning = false, material = "camos/custom_camo17", skin = 0, bodygroup = {} },
    ["leftclaw"] = { type = "Model", model = "models/mechanics/robotics/claw.mdl", bone = "ValveBiped.base", rel = "", pos = Vector(1.404, -1.005, 5.638), angle = Angle(0, 0, 179.968), size = Vector(0.086, 0.086, 0.086), color = Color(255,255,255, 255), surpresslightning = false, material = "camos/custom_camo17", skin = 0, bodygroup = {} },
    ["train"] = { type = "Model", model = "models/props_combine/CombineTrain01a.mdl", bone = "ValveBiped.base", rel = "", pos = Vector(0, 2.194, -4.843), angle = Angle(-90, 91.099, 0), size = Vector(0.01, 0.016, 0.01), color = Color(255, 255, 255, 254), surpresslightning = false, material = "camos/custom_camo17", skin = 0, bodygroup = {} },
    ["barrel"] = { type = "Model", model = "models/props_c17/oildrum001.mdl", bone = "ValveBiped.base", rel = "", pos = Vector(0.068, -1.114, 4.362), angle = Angle(0, 0, 0), size = Vector(0.065, 0.065, 0.065), color = Color(255, 255, 255, 255), surpresslightning = false, material = "models/Combine_Mine/combine_mine03", skin = 0, bodygroup = {} },
    ["pipe1+"] = { type = "Model", model = "models/props_canal/mattpipe.mdl", bone = "ValveBiped.Bip01", rel = "barrel", pos = Vector(-1.264, -0.132, 5.156), angle = Angle(-0.475, 1.394, 0.056), size = Vector(1,1,1), color = Color(255,255,255, 255), surpresslightning = false, material = "camos/custom_camo17", skin = 0, bodygroup = {} },
    ["pipe1++++"] = { type = "Model", model = "models/props_canal/mattpipe.mdl", bone = "ValveBiped.Bip01", rel = "barrel", pos = Vector(-0.889, 0.686, 5.25), angle = Angle(-0.508, 45.93, 0.261), size = Vector(1,1,1), color = Color(255,255,255, 255), surpresslightning = false, material = "camos/custom_camo17", skin = 0, bodygroup = {} },
    ["pipe1+++++++"] = { type = "Model", model = "models/props_canal/mattpipe.mdl", bone = "ValveBiped.Bip01", rel = "barrel", pos = Vector(0.885, 0.742, 5.25), angle = Angle(-0.508, 139.669, 0.261), size = Vector(1,1,1), color = Color(255,255,255, 255), surpresslightning = false, material = "camos/custom_camo17", skin = 0, bodygroup = {} },
    ["pipe1++++++"] = { type = "Model", model = "models/props_canal/mattpipe.mdl", bone = "ValveBiped.Bip01", rel = "barrel", pos = Vector(-0.884, -0.988, 5.25), angle = Angle(-0.508, -45.445, 0.261), size = Vector(1,1,1), color = Color(255,255,255, 255), surpresslightning = false, material = "camos/custom_camo17", skin = 0, bodygroup = {} },
    ["pipe1+++"] = { type = "Model", model = "models/props_canal/mattpipe.mdl", bone = "ValveBiped.Bip01", rel = "barrel", pos = Vector(0.836, -0.939, 5.25), angle = Angle(-0.508, -138.976, 0.261), size = Vector(1,1,1), color = Color(255,255,255, 255), surpresslightning = false, material = "camos/custom_camo17", skin = 0, bodygroup = {} },
    ["pipe1++"] = { type = "Model", model = "models/props_canal/mattpipe.mdl", bone = "ValveBiped.Bip01", rel = "barrel", pos = Vector(-0.12, 1.131, 5.193), angle = Angle(-0.288, 90.207, -0.958), size = Vector(1,1,1), color = Color(255,255,255, 255), surpresslightning = false, material = "camos/custom_camo17", skin = 0, bodygroup = {} }
}


SWEP.WElements = {
    ["pipe1+++++++"] = { type = "Model", model = "models/props_canal/mattpipe.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "barrel", pos = Vector(0.885, 0.742, 5.25), angle = Angle(-0.508, 139.669, 0.261), size = Vector(1,1,1), color = Color(255,255,255, 255), surpresslightning = false, material = "camos/custom_camo17", skin = 0, bodygroup = {} },
    ["pipe1+++++"] = { type = "Model", model = "models/props_canal/mattpipe.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "barrel", pos = Vector(1.129, -0.058, 5.25), angle = Angle(-0.508, 180, 0.261), size = Vector(1,1,1), color = Color(255,255,255, 255), surpresslightning = false, material = "camos/custom_camo17", skin = 0, bodygroup = {} },
    ["leftclaw"] = { type = "Model", model = "models/mechanics/robotics/claw.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(9.55, 2.868, -6.949), angle = Angle(2.03, -88.962, 77.336), size = Vector(0.098, 0.098, 0.098), color = Color(255,255,255, 255), surpresslightning = false, material = "camos/custom_camo17", skin = 0, bodygroup = {} },
    ["pipe1+"] = { type = "Model", model = "models/props_canal/mattpipe.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "barrel", pos = Vector(-1.264, -0.132, 5.156), angle = Angle(-0.475, 1.394, 0.056), size = Vector(1,1,1), color = Color(255,255,255, 255), surpresslightning = false, material = "camos/custom_camo17", skin = 0, bodygroup = {} },
    ["pipe1++++++"] = { type = "Model", model = "models/props_canal/mattpipe.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "barrel", pos = Vector(-0.884, -0.988, 5.25), angle = Angle(-0.508, -45.445, 0.261), size = Vector(1,1,1), color = Color(255,255,255, 255), surpresslightning = false, material = "camos/custom_camo17", skin = 0, bodygroup = {} },
    ["pipe1++"] = { type = "Model", model = "models/props_canal/mattpipe.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "barrel", pos = Vector(-0.12, 1.131, 5.193), angle = Angle(-0.288, 90.207, -0.958), size = Vector(1,1,1), color = Color(255,255,255, 254), surpresslightning = false, material = "camos/custom_camo17", skin = 0, bodygroup = {} },
    ["leftclaw+"] = { type = "Model", model = "models/mechanics/robotics/claw.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(9.557, -0.269, -6.987), angle = Angle(-1.081, 91.973, 104.18), size = Vector(0.098, 0.098, 0.098), color = Color(255,255,255, 255), surpresslightning = false, material = "camos/custom_camo17", skin = 0, bodygroup = {} },
    ["pipe1"] = { type = "Model", model = "models/props_canal/mattpipe.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "barrel", pos = Vector(0, -1.264, 5.144), angle = Angle(0.1, -83.37, 0), size = Vector(1,1,1), color = Color(255,255,255, 255), surpresslightning = false, material = "camos/custom_camo17", skin = 0, bodygroup = {} },
    ["can"] = { type = "Model", model = "models/props_junk/PopCan01a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(5.543, 1.83, -5.277), angle = Angle(-10.325, 1.25, -180), size = Vector(0.5, 0.5, 0.5), color = Color(255, 255, 255, 0), surpresslightning = false, material = "camos/custom_camo17", skin = 0, bodygroup = {} },
    ["barrel"] = { type = "Model", model = "models/props_c17/oildrum001.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "can", pos = Vector(2.663, 0.193, 1.156), angle = Angle(0, 91.168, 90), size = Vector(0.065, 0.065, 0.065), color = Color(255, 255, 255, 255), surpresslightning = false, material = "models/Combine_Mine/combine_mine03", skin = 0, bodygroup = {} },
    ["pipe1+++"] = { type = "Model", model = "models/props_canal/mattpipe.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "barrel", pos = Vector(0.836, -0.939, 5.25), angle = Angle(-0.508, -138.976, 0.261), size = Vector(1,1,1), color = Color(255,255,255, 255), surpresslightning = false, material = "camos/custom_camo17", skin = 0, bodygroup = {} },
    ["pipe1++++"] = { type = "Model", model = "models/props_canal/mattpipe.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "barrel", pos = Vector(-0.889, 0.686, 5.25), angle = Angle(-0.508, 45.93, 0.261), size = Vector(1,1,1), color = Color(255,255,255, 255), surpresslightning = false, material = "camos/custom_camo17", skin = 0, bodygroup = {} },
    ["train"] = { type = "Model", model = "models/props_combine/CombineTrain01a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(3.25, 1.425, -2.375), angle = Angle(167.511, 0.368, 0.919), size = Vector(0.016, 0.021, 0.016), color = Color(255, 255, 255, 255), surpresslightning = false, material = "camos/custom_camo17", skin = 0, bodygroup = {} }
}
SWEP.HasHealshot = true

if (SERVER) then

    AddCSLuaFile(  )
    SWEP.Weight                = 10
    SWEP.AutoSwitchTo        = true
    SWEP.AutoSwitchFrom        = true

end

if ( CLIENT ) then

    SWEP.DrawAmmo            = true
    SWEP.DrawCrosshair        = false
    SWEP.ViewModelFOV        = 65
    SWEP.ViewModelFlip        = false
    SWEP.CSMuzzleFlashes    = false
    SWEP.WepSelectIcon   = surface.GetTextureID("VGUI/entities/select")
    SWEP.BounceWeaponIcon = false

end

function SWEP:Deploy()
    local ply = self.Owner
    hook.Add("PlayerDeath", "healshot"..ply:SteamID(),function( victim, weapon, killer )
        if IsValid(ply) and not ply:IsGhost() and SERVER and killer == ply and ply:Health() < 150 then
            ply:SetHealth(ply:Health() +25)
            if ply:Health() > 150 then ply:SetHealth(150) end
        end
    end)
    return true
end



SWEP.Base = "aa_base"
SWEP.Kind = WEAPON_UNARMED

SWEP.Slot = 7
SWEP.PrintName = "Haze's Magical Hatred"
SWEP.Author = "Heavy-D"
SWEP.Spawnable = true
SWEP.Category = "Heavy-D's SWEPs"
SWEP.SlotPos = 1
SWEP.Instructions = "Shoot it"
SWEP.Contact = "Don't"
SWEP.Purpose = "Hurt people"
SWEP.HoldType = "physgun"
SWEP.ViewModel = "models/weapons/v_smg1.mdl"
SWEP.WorldModel = "models/weapons/w_smg1.mdl"
SWEP.ShowViewModel = true
SWEP.ShowWorldModel = true
SWEP.ViewModelBoneMods = {
    ["ValveBiped.base"] = { scale = Vector(0.777, 0.777, 0.777), pos = Vector(0, 0, 0), angle = Angle(0, 0, 0) },
    ["ValveBiped.clip"] = { scale = Vector(0.009, 0.009, 0.009), pos = Vector(0, 0, 0), angle = Angle(0, 0, 0) }
}
        //Primary Fire Variables\\
SWEP.Primary.Sound = "weapons/ar2/fire1.wav"
SWEP.Primary.Damage = 1
SWEP.Primary.TakeAmmo = 0
SWEP.Primary.ClipSize = 400
SWEP.Primary.Ammo = "SMG1"
SWEP.Primary.DefaultClip = 3000
SWEP.Primary.Spread = 0.085
SWEP.Primary.NumberofShots = 14
SWEP.Primary.Automatic = true
SWEP.Primary.Recoil = 0.01
SWEP.Primary.Delay = 0.1
SWEP.Primary.Force = 10
SWEP.BarrelAngle = 0
SWEP.Crosshair = 0.07
        SWEP.Primary.Automatic = true
        //Primary Fire Variables\\

//Secondary Fire Variables\\
SWEP.Secondary.NumberofShots = 1
SWEP.Secondary.Force = 10
SWEP.Secondary.Spread = 0.1
SWEP.Secondary.DefaultClip = 32
SWEP.Secondary.Recoil = 0.001
SWEP.Secondary.Automatic = false
SWEP.Secondary.Delay = 0.001
SWEP.Secondary.ClipSize = 0
SWEP.Secondary.Damage = 10
        //Secondary Fire Variables\\

Zoom = 0

function SWEP:Initialize()

    util.PrecacheSound(self.Primary.Sound)

    if ( SERVER ) then
        self:SetNPCMinBurst( 20 )
        self:SetNPCMaxBurst( 50 )
        self:SetNPCFireRate( 0.07 )
    end


    if CLIENT then

        // Create a new table for every weapon instance
        self.VElements = table.FullCopy( self.VElements )
        self.WElements = table.FullCopy( self.WElements )
        self.ViewModelBoneMods = table.FullCopy( self.ViewModelBoneMods )

        self:CreateModels(self.VElements) // create viewmodels
        self:CreateModels(self.WElements) // create worldmodels

        // init view model bone build function
        if IsValid(self.Owner) then
            if self.Owner:IsNPC() then return end
            local vm = self.Owner:GetViewModel()
            if IsValid(vm) then
                self:ResetBonePositions(vm)
            end

            // Init viewmodel visibility
            if (self.ShowViewModel == nil or self.ShowViewModel) then
            --vm:SetColor(Color(255,255,255,255))
        else
            // we set the alpha to 1 instead of 0 because else ViewModelDrawn stops being called
            vm:SetColor(Color(255,255,255,1))
            // ^ stopped working in GMod 13 because you have to do Entity:SetRenderMode(1) for translucency to kick in
            // however for some reason the view model resets to render mode 0 every frame so we just apply a debug material to prevent it from drawing
            vm:SetMaterial("Debug/hsv")
            end
        end

    end

    self.BarrelAngle = 0

end

function SWEP:DrawHUD()

    if self:GetNWBool("Reloading") == true then return end

    local x, y

    if ( self.Owner == LocalPlayer() && self.Owner:ShouldDrawLocalPlayer() ) then

    local tr = util.GetPlayerTrace( self.Owner )
    tr.mask = ( CONTENTS_SOLID || CONTENTS_MOVEABLE || CONTENTS_MONSTER || CONTENTS_WINDOW || CONTENTS_DEBRIS || CONTENTS_GRATE || CONTENTS_AUX )
    local trace = util.TraceLine( tr )

    local coords = trace.HitPos:ToScreen()
    x, y = coords.x, coords.y

    else
        x, y = ScrW() / 2.0, ScrH() / 2.0
    end


    local scale = 10*self.Crosshair



    surface.SetDrawColor( 0, 255, 0, 255 )

    local gap = 40 * scale
    local length = gap + 20 * scale
    surface.DrawLine( x - length, y, x - gap, y )
    surface.DrawLine( x + length, y, x + gap, y )
    surface.DrawLine( x, y - length, x, y - gap )
    surface.DrawLine( x, y + length, x, y + gap )

end

SWEP.nextexplodemoney = 0
function SWEP:Reload()


        if self.healththing and self.spawnedwewe < CurTime() and self.nextexplodemoney < CurTime() then
            Entity(self.healththing):Explode()
            Entity(self.healththing):Remove()
            self.nextexplodemoney = CurTime() + 60
            self.healththing = nil
                return
        end



    if self.Owner:KeyDown(IN_ATTACK) or self.Owner:KeyDown(IN_ATTACK2) or self.Weapon:Ammo1()==0 or self.Weapon:Clip1() >399 or self:GetNWBool("Reloading") == true then return false end
    self.Weapon:DefaultReload(ACT_VM_RELOAD)
    self:SetNWBool("Reloading", true)


    timer.Simple(1.3, function()
        self:SetNWBool("Reloading", false)
    end)
end


SWEP.LastAtt = CurTime()



function SWEP:GetViewModelPosition( origin, angles )
    if not self.Owner or not self.Owner.KeyDown then return end

    if self:GetNWBool("Reloading") == true or self.Weapon:Clip1() == 0 then return end


    local targetOffset = 0;
    local wiggle = VectorRand();
    wiggle:Mul( 0.0001 )

    if self.Owner:KeyDown(IN_ATTACK) then

        wiggle:Mul( 1000 );
        targetOffset = 1;

    end

    if self.Owner:KeyReleased(IN_ATTACK) then

        wiggle = vector_origin;

    end

    -- self.VMOffset = Lerp( FrameTime() * 20, self.VMOffset, targetOffset );

    local offset = angles:Forward();
    offset:Mul( self.VMOffset );
    offset:Add( wiggle );

    origin:Sub( offset );

    return origin, angles;

end

SWEP.tracerColor = Color(255,0,255,255)
function SWEP:PrimaryAttack()

    if self:GetNWBool("Reloading") == true then return end

    self.Weapon:SendWeaponAnim(ACT_VM_PRIMARYATTACK)
    local rnda = self.Primary.Recoil * -1
    local rndb = self.Primary.Recoil * math.random(-1, 1)
    self:Disorientate()

    self:ShootBullet()

    self.Weapon:EmitSound(Sound(self.Primary.Sound))
    --self:TakePrimaryAmmo(self.Primary.TakeAmmo)
    self.Weapon:SetNextPrimaryFire( CurTime() + 0.1 )
    self:SetNextSecondaryFire(CurTime()+0.3)


end


function SWEP:DrawWeaponSelection(x, y, wide, tall, alpha)

    surface.SetDrawColor(255, 255, 255, alpha)
    surface.SetTexture(self.WepSelectIcon)

    local fsin = 0

    if (self.BounceWeaponIcon == true) then
        fsin = math.sin(CurTime() * 10) * 5
    end

    y = y + 10
    x = x + 10
    wide = wide - 20

    surface.DrawTexturedRect(x + (fsin), y - (fsin), wide - fsin * 2, (wide / 2) + (fsin))

    self:PrintWeaponInfo(x + wide + 20, y + tall * 0.95, alpha)
end
SWEP.Primary.Spread = 0.001
function SWEP:ShootBullet(damage, recoil, num_bullets, aimcone)

    num_bullets 		= 1

    local bullet = {}
    bullet.Num 		= 14
    bullet.Src 		= self.Owner:GetShootPos()			// Source
    bullet.Dir 		= self.Owner:GetAimVector()			// Dir of bullet
    bullet.Spread 	= Vector(self.Primary.Spread * 0.1 , self.Primary.Spread * 0.1, 0)			// Aim Cone
    bullet.Tracer	= 4						// Show a tracer on every x bullets
    bullet.TracerName = "manatrace"
    bullet.Force	= 10					// Amount of force to give to phys objects
    bullet.Damage	= 1
    bullet.damage	= 1


    self.Owner:FireBullets(bullet)

    if self.Weapon:Clip1() >0 and self.Owner:KeyDown(IN_ATTACK) and self:GetNWBool("Reloading") == false then
        self.BarrelAngle = self.BarrelAngle + 10
    end

    self.VElements["barrel"].angle.y = math.Approach(self.VElements["barrel"].angle.y, self.BarrelAngle, FrameTime()*500)


end


//SWEP:SecondaryFire()\\
function SWEP:SecondaryAttack()
    self:SetNextSecondaryFire(CurTime()+0.3)
    self:SetNextPrimaryFire(CurTime()+0.3)
    self:MakeExplosion()
    self:FireBolt(0)


end

function SWEP:AdjustMouseSensitivity()

    if ( self.Owner:KeyDown( IN_ATTACK2 )  ) then

        return 0.1

    end
end

function SWEP:Think()

    if self.Owner:KeyDown(IN_USE) and not self.Owner:KeyDown(IN_RELOAD)then
        self:Fly()
    end

    if self.Owner:KeyDown(IN_USE) and  self.Owner:KeyDown(IN_RELOAD)then
        self:DropHealth("hatred_health_magic")
        self.spawnedwewe = CurTime() + 1
    end

    if self:GetNWBool("Zoomed") == true then
        self.Primary.Spread = 0.1
        self.Crosshair = 0.05
    end

    if self:GetNWBool("Zoomed") == false then
        self.Primary.Spread = 0.2
        self.Crosshair = 0.07
    end

    if self.Weapon:Clip1() >0 and self.Owner:KeyDown(IN_ATTACK) then
        self.BarrelAngle = self.BarrelAngle + 10
    end

    if CLIENT then
        self.VElements["barrel"].angle.y = math.Approach(self.VElements["barrel"].angle.y, self.BarrelAngle, FrameTime()*150)

        self.WElements["barrel"].angle.p = math.Approach(self.WElements["barrel"].angle.p, self.BarrelAngle, FrameTime()*150)
    end


    if not self.Owner:KeyDown(IN_ATTACK) and self.Primary.Delay<0.3 then
        self.Primary.Delay = self.Primary.Delay + 0.001
    end


end


function SWEP:Holster()

    if CLIENT and IsValid(self.Owner) then
        local vm = self.Owner:GetViewModel()
        if IsValid(vm) then
            self:ResetBonePositions(vm)
        end
    end

    return true
end

if CLIENT then

    SWEP.vRenderOrder = nil
    function SWEP:ViewModelDrawn()
        if !IsValid(self.Owner) then return end
        local vm = self.Owner:GetViewModel()
        if !IsValid(vm) then return end

        if (!self.VElements) then return end

        self:UpdateBonePositions(vm)

        if (!self.vRenderOrder) then

        // we build a render order because sprites need to be drawn after models
        self.vRenderOrder = {}

        for k, v in pairs( self.VElements ) do
            if (v.type == "Model") then
                table.insert(self.vRenderOrder, 1, k)
            elseif (v.type == "Sprite" or v.type == "Quad") then
                table.insert(self.vRenderOrder, k)
            end
        end

        end

        for k, name in ipairs( self.vRenderOrder ) do

            local v = self.VElements[name]
            if (!v) then self.vRenderOrder = nil break end
            if (v.hide) then continue end

            local model = v.modelEnt
            local sprite = v.spriteMaterial

            if (!v.bone) then continue end

            local pos, ang = self:GetBoneOrientation( self.VElements, v, vm )

            if (!pos) then continue end

            if (v.type == "Model" and IsValid(model)) then

                model:SetPos(pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z )
                ang:RotateAroundAxis(ang:Up(), v.angle.y)
                ang:RotateAroundAxis(ang:Right(), v.angle.p)
                ang:RotateAroundAxis(ang:Forward(), v.angle.r)

                model:SetAngles(ang)
                //model:SetModelScale(v.size)
                local matrix = Matrix()
                matrix:Scale(v.size)
                model:EnableMatrix( "RenderMultiply", matrix )

                if (v.material == "") then
                    model:SetMaterial("")
                elseif (model:GetMaterial() != v.material) then
                model:SetMaterial( v.material )
                end

                if (v.skin and v.skin != model:GetSkin()) then
                model:SetSkin(v.skin)
                end

                if (v.bodygroup) then
                    for k, v in pairs( v.bodygroup ) do
                        if (model:GetBodygroup(k) != v) then
                        model:SetBodygroup(k, v)
                        end
                    end
                end

                if (v.surpresslightning) then
                    render.SuppressEngineLighting(true)
                end

                render.SetColorModulation(v.color.r/255, v.color.g/255, v.color.b/255)
                render.SetBlend(v.color.a/255)
                model:DrawModel()
                render.SetBlend(1)
                render.SetColorModulation(1, 1, 1)

                if (v.surpresslightning) then
                    render.SuppressEngineLighting(false)
                end

            elseif (v.type == "Sprite" and sprite) then

                local drawpos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
                render.SetMaterial(sprite)
                render.DrawSprite(drawpos, v.size.x, v.size.y, v.color)

            elseif (v.type == "Quad" and v.draw_func) then

                local drawpos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
                ang:RotateAroundAxis(ang:Up(), v.angle.y)
                ang:RotateAroundAxis(ang:Right(), v.angle.p)
                ang:RotateAroundAxis(ang:Forward(), v.angle.r)

                cam.Start3D2D(drawpos, ang, v.size)
                v.draw_func( self )
                cam.End3D2D()

            end

            end

            end

            SWEP.wRenderOrder = nil
            function SWEP:DrawWorldModel()

                if (self.ShowWorldModel == nil or self.ShowWorldModel) then
                    self:DrawModel()
                end

                if (!self.WElements) then return end

                if (!self.wRenderOrder) then

                self.wRenderOrder = {}

                for k, v in pairs( self.WElements ) do
                    if (v.type == "Model") then
                        table.insert(self.wRenderOrder, 1, k)
                    elseif (v.type == "Sprite" or v.type == "Quad") then
                        table.insert(self.wRenderOrder, k)
                    end
                end

                end

                if (IsValid(self.Owner)) then
                    bone_ent = self.Owner
                else
                    // when the weapon is dropped
                    bone_ent = self
                end

                for k, name in pairs( self.wRenderOrder ) do

                    local v = self.WElements[name]
                    if (!v) then self.wRenderOrder = nil break end
                    if (v.hide) then continue end

                    local pos, ang

                    if (v.bone) then
                        pos, ang = self:GetBoneOrientation( self.WElements, v, bone_ent )
                    else
                        pos, ang = self:GetBoneOrientation( self.WElements, v, bone_ent, "ValveBiped.Bip01_R_Hand" )
                    end

                    if (!pos) then continue end

                    local model = v.modelEnt
                    local sprite = v.spriteMaterial

                    if (v.type == "Model" and IsValid(model)) then

                        model:SetPos(pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z )
                        ang:RotateAroundAxis(ang:Up(), v.angle.y)
                        ang:RotateAroundAxis(ang:Right(), v.angle.p)
                        ang:RotateAroundAxis(ang:Forward(), v.angle.r)

                        model:SetAngles(ang)
                        //model:SetModelScale(v.size)
                        local matrix = Matrix()
                        matrix:Scale(v.size)
                        model:EnableMatrix( "RenderMultiply", matrix )

                        if (v.material == "") then
                            model:SetMaterial("")
                        elseif (model:GetMaterial() != v.material) then
                        model:SetMaterial( v.material )
                        end

                        if (v.skin and v.skin != model:GetSkin()) then
                        model:SetSkin(v.skin)
                        end

                        if (v.bodygroup) then
                            for k, v in pairs( v.bodygroup ) do
                                if (model:GetBodygroup(k) != v) then
                                model:SetBodygroup(k, v)
                                end
                            end
                        end

                        if (v.surpresslightning) then
                            render.SuppressEngineLighting(true)
                        end

                        render.SetColorModulation(v.color.r/255, v.color.g/255, v.color.b/255)
                        render.SetBlend(v.color.a/255)
                        model:DrawModel()
                        render.SetBlend(1)
                        render.SetColorModulation(1, 1, 1)

                        if (v.surpresslightning) then
                            render.SuppressEngineLighting(false)
                        end

                    elseif (v.type == "Sprite" and sprite) then

                        local drawpos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
                        render.SetMaterial(sprite)
                        render.DrawSprite(drawpos, v.size.x, v.size.y, v.color)

                    elseif (v.type == "Quad" and v.draw_func) then

                        local drawpos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
                        ang:RotateAroundAxis(ang:Up(), v.angle.y)
                        ang:RotateAroundAxis(ang:Right(), v.angle.p)
                        ang:RotateAroundAxis(ang:Forward(), v.angle.r)

                        cam.Start3D2D(drawpos, ang, v.size)
                        v.draw_func( self )
                        cam.End3D2D()

                    end

                    end

                    end

                    function SWEP:GetBoneOrientation( basetab, tab, ent, bone_override )

                        local bone, pos, ang
                        if (tab.rel and tab.rel != "") then

                        local v = basetab[tab.rel]

                        if (!v) then return end

                        // Technically, if there exists an element with the same name as a bone
                        // you can get in an infinite loop. Let's just hope nobody's that stupid.
                        pos, ang = self:GetBoneOrientation( basetab, v, ent )

                        if (!pos) then return end

                        pos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
                        ang:RotateAroundAxis(ang:Up(), v.angle.y)
                        ang:RotateAroundAxis(ang:Right(), v.angle.p)
                        ang:RotateAroundAxis(ang:Forward(), v.angle.r)

                        else

                            bone = ent:LookupBone(bone_override or tab.bone)

                            if (!bone) then return end

                            pos, ang = Vector(0,0,0), Angle(0,0,0)
                            local m = ent:GetBoneMatrix(bone)
                            if (m) then
                                pos, ang = m:GetTranslation(), m:GetAngles()
                            end

                            if (IsValid(self.Owner) and self.Owner:IsPlayer() and
                                    ent == self.Owner:GetViewModel() and self.ViewModelFlip) then
                                ang.r = -ang.r // Fixes mirrored models
                            end

                        end

                        return pos, ang
                    end

                    function SWEP:CreateModels( tab )

                        if (!tab) then return end

                        // Create the clientside models here because Garry says we can't do it in the render hook
                        for k, v in pairs( tab ) do
                            if (v.type == "Model" and v.model and v.model != "" and (!IsValid(v.modelEnt) or v.createdModel != v.model) and
                            string.find(v.model, ".mdl") and file.Exists (v.model, "GAME") ) then

                            v.modelEnt = ClientsideModel(v.model, RENDER_GROUP_VIEW_MODEL_OPAQUE)
                            if (IsValid(v.modelEnt)) then
                                v.modelEnt:SetPos(self:GetPos())
                                v.modelEnt:SetAngles(self:GetAngles())
                                v.modelEnt:SetParent(self)
                                v.modelEnt:SetNoDraw(true)
                                v.createdModel = v.model
                            else
                                v.modelEnt = nil
                            end

                            elseif (v.type == "Sprite" and v.sprite and v.sprite != "" and (!v.spriteMaterial or v.createdSprite != v.sprite)
                            and file.Exists ("materials/"..v.sprite..".vmt", "GAME")) then

                            local name = v.sprite.."-"
                            local params = { ["$basetexture"] = v.sprite }
                                    // make sure we create a unique name based on the selected options
                            local tocheck = { "nocull", "additive", "vertexalpha", "vertexcolor", "ignorez" }
                            for i, j in pairs( tocheck ) do
                                if (v[j]) then
                                    params["$"..j] = 1
                                    name = name.."1"
                                else
                                    name = name.."0"
                                end
                            end

                            v.createdSprite = v.sprite
                            v.spriteMaterial = CreateMaterial(name,"UnlitGeneric",params)

                            end
                        end

                    end

                    local allbones
                    local hasGarryFixedBoneScalingYet = false

                    function SWEP:UpdateBonePositions(vm)

                        if self.ViewModelBoneMods then

                            if (!vm:GetBoneCount()) then return end

                            // !! WORKAROUND !! //
                            // We need to check all model names :/
                            local loopthrough = self.ViewModelBoneMods
                            if (!hasGarryFixedBoneScalingYet) then
                            allbones = {}
                            for i=0, vm:GetBoneCount() do
                                local bonename = vm:GetBoneName(i)
                                if (self.ViewModelBoneMods[bonename]) then
                                    allbones[bonename] = self.ViewModelBoneMods[bonename]
                                else
                                    allbones[bonename] = {
                                        scale = Vector(1,1,1),
                                        pos = Vector(0,0,0),
                                        angle = Angle(0,0,0)
                                    }
                                end
                            end

                            loopthrough = allbones
                            end
                            // !! ----------- !! //

                            for k, v in pairs( loopthrough ) do
                                local bone = vm:LookupBone(k)
                                if (!bone) then continue end

                                // !! WORKAROUND !! //
                                local s = Vector(v.scale.x,v.scale.y,v.scale.z)
                                local p = Vector(v.pos.x,v.pos.y,v.pos.z)
                                local ms = Vector(1,1,1)
                                if (!hasGarryFixedBoneScalingYet) then
                                local cur = vm:GetBoneParent(bone)
                                while(cur >= 0) do
                                    local pscale = loopthrough[vm:GetBoneName(cur)].scale
                                    ms = ms * pscale
                                    cur = vm:GetBoneParent(cur)
                                end
                                end

                                s = s * ms
                                        // !! ----------- !! //

                                if vm:GetManipulateBoneScale(bone) != s then
                                vm:ManipulateBoneScale( bone, s )
                                end
                                if vm:GetManipulateBoneAngles(bone) != v.angle then
                                vm:ManipulateBoneAngles( bone, v.angle )
                                end
                                if vm:GetManipulateBonePosition(bone) != p then
                                vm:ManipulateBonePosition( bone, p )
                                end
                                end
                        else
                            self:ResetBonePositions(vm)
                        end

                    end

                    function SWEP:ResetBonePositions(vm)

                        if (!vm:GetBoneCount()) then return end
                        for i=0, vm:GetBoneCount() do
                            vm:ManipulateBoneScale( i, Vector(1, 1, 1) )
                            vm:ManipulateBoneAngles( i, Angle(0, 0, 0) )
                            vm:ManipulateBonePosition( i, Vector(0, 0, 0) )
                        end

                    end

                    /**************************
                    Global utility code
                    **************************/

                    // Fully copies the table, meaning all tables inside this table are copied too and so on (normal table.Copy copies only their reference).
                    // Does not copy entities of course, only copies their reference.
                    // WARNING: do not use on tables that contain themselves somewhere down the line or you'll get an infinite loop
                    function table.FullCopy( tab )

                        if (!tab) then return nil end

                        local res = {}
                        for k, v in pairs( tab ) do
                            if (type(v) == "table") then
                                res[k] = table.FullCopy(v) // recursion ho!
                            elseif (type(v) == "Vector") then
                                res[k] = Vector(v.x, v.y, v.z)
                            elseif (type(v) == "Angle") then
                                res[k] = Angle(v.p, v.y, v.r)
                            else
                                res[k] = v
                            end
                        end

                        return res

                    end

                end



                local ENT = {}

                ENT.Type = "anim"
                ENT.Base = "normal_health_station"

                function ENT:Initialize()

                    self:SetModel("models/props/cs_office/plant01.mdl" )
                    self.healsound = "items/medshot4.wav"
                    self:PhysicsInit(SOLID_VPHYSICS)
                    self:SetMoveType(MOVETYPE_VPHYSICS)
                    self:SetSolid(SOLID_BBOX)
                    self:SetCollisionGroup(COLLISION_GROUP_WEAPON)
                    if SERVER then
                        self:SetMaxHealth(200)

                        local phys = self:GetPhysicsObject()
                        if IsValid(phys) then
                            phys:SetMass(200)
                        end

                        self:SetUseType(CONTINUOUS_USE)
                    end
                    self:SetHealth(200)

                    self:SetColor(Color(180, 180, 250, 255))

                    self:SetStoredHealth(200)

                    self:SetPlacer(nil)

                    self.NextHeal = 0

                    self.fingerprints = {}
                end

                function ENT:Explode()
                    if self.exploded then return end self.exploded = true
                    for i=1,4 do
                        local spos = self:GetPos()+Vector(math.random(-75,75),math.random(-75,75),math.random(0,50))
                        local bomb = ents.Create("eatable_money_sand")
                        bomb.ischild = true
                        bomb:SetPos(spos)
                        bomb.Owner = self.Owner
                        bomb:Spawn()
                    end
                end

                scripted_ents.Register( ENT, "hatred_health_magic", true )

                local ENT = {}

                ENT.Type = "anim"
                ENT.Base = "base_anim"


                if SERVER then

                    function ENT:Initialize()
                        self:SetModel("models/props/cs_assault/money.mdl")
                        self:SetModelScale(self:GetModelScale()*1,0)
                        self.Entity:PhysicsInit(SOLID_VPHYSICS);
                        self.Entity:SetMoveType(MOVETYPE_VPHYSICS);
                        self.Entity:SetSolid(SOLID_VPHYSICS);

                        self.Entity:SetCollisionGroup(COLLISION_GROUP_WEAPON)
                        local phys = self:GetPhysicsObject()
                        if phys:IsValid() then
                            phys:Wake()
                            phys:EnableGravity(true);
                        end
                    end

                    function ENT:Use( activator, caller )
                        self.Entity:Remove()
                        if ( activator:IsPlayer() ) then
                            if activator:Health() < 150 then

                                activator:SetHealth(activator:Health()+10)

                            end
                            activator:EmitSound("crisps/eat.mp3")
                        end
                    end


                end

                scripted_ents.Register( ENT, "eatable_money_sand", true )