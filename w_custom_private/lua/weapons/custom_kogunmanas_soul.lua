SWEP.Base = "gb_camo_base"
SWEP.ViewModelFOV	= 54
SWEP.HoldType = "ar2"
SWEP.PrintName = "Kogunmana's Soul"
SWEP.HoldType = "grenade"
SWEP.ViewModelFOV = 74
SWEP.ViewModelFlip = false
SWEP.ViewModel = "models/weapons/c_grenade.mdl"
SWEP.WorldModel = "models/weapons/w_grenade.mdl"
SWEP.ShowViewModel = true
SWEP.Camo = 0
SWEP.CustomCamo = true
SWEP.ShowWorldModel = false
SWEP.ViewModelBoneMods = {
    ["ValveBiped.Grenade_body"] = { scale = Vector(0.009, 0.009, 0.009), pos = Vector(0, 0, 0), angle = Angle(0, 0, 0) }
}
SWEP.VElements = {
    ["pancake"] = { type = "Model", model = "models/pancake.mdl", bone = "ValveBiped.Grenade_body", rel = "", pos = Vector(0.455, 2.273, -1.364), angle = Angle(0, 90.158, 60.476), size = Vector(1.003, 1.003, 1.003), color = Color(255, 255, 255, 255), surpresslightning = false, material = "camos/custom_camo5", skin = 0, bodygroup = {} }
}
SWEP.WElements = {
    ["pancake"] = { type = "Model", model = "models/pancake.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(5, 5, -0.456), angle = Angle(1.023, -54.206, 91.023), size = Vector(1.003, 1.003, 1.003), color = Color(255, 255, 255, 255), surpresslightning = false, material = "camos/custom_camo5", skin = 0, bodygroup = {} }
}

SWEP.UseHands = true

function SWEP:Initialize()

    // other initialize code goes here

    if CLIENT then
    self:SetHoldType( self.HoldType or "pistol" )
    self.VElements = table.FullCopy( self.VElements )
    self.WElements = table.FullCopy( self.WElements )
    self.ViewModelBoneMods = table.FullCopy( self.ViewModelBoneMods )

    self:CreateModels(self.VElements)
    self:CreateModels(self.WElements)

    if IsValid(self.Owner) then
        local vm = self.Owner:GetViewModel()
        if IsValid(vm) then
            self:ResetBonePositions(vm)

            if (self.ShowViewModel == nil or self.ShowViewModel) then
                vm:SetColor(Color(255,255,255,255))
            else
                // we set the alpha to 1 instead of 0 because else ViewModelDrawn stops being called
                vm:SetColor(Color(255,255,255,1))
                // ^ stopped working in GMod 13 because you have to do Entity:SetRenderMode(1) for translucency to kick in
                // however for some reason the view model resets to render mode 0 every frame so we just apply a debug material to prevent it from drawing
                vm:SetMaterial("Debug/hsv")
                end
            end
        end

    end

 self.BaseClass.Initialize(self)

end

SWEP.Slot = 7

if CLIENT then

    SWEP.Author = "GreenBlack"

    SWEP.Icon = "vgui/ttt_fun_killicons/mp5.png"
end


SWEP.Spawnable = true
SWEP.AdminSpawnable = true
SWEP.Kind = 566

SWEP.Primary.Delay = 0.11
SWEP.Primary.Recoil = 1.1
SWEP.Primary.Automatic = true
SWEP.Primary.Ammo = "pistol"
SWEP.Primary.Damage = 16
SWEP.Primary.Cone = 0.028
SWEP.Primary.ClipSize = 30
SWEP.Primary.ClipMax = 90
SWEP.Primary.DefaultClip = 30
SWEP.AutoSpawnable = false
SWEP.AmmoEnt = "item_ammo_pistol_ttt"

SWEP.Primary.Sound = Sound("Weapon_MP5Navy.Single")

SWEP.HeadshotMultiplier = 1.2

SWEP.IronSightsPos = Vector(-5.3, -3.5823, 2)
SWEP.IronSightsAng = Vector(0.9641, 0.0252, 0)

function SWEP:DoImpactEffect( trace, damageType )
    local effectdata = EffectData()
    effectdata:SetStart( trace.HitPos )
    effectdata:SetOrigin( trace.HitNormal + Vector( math.Rand( -0.5, 0.5 ), math.Rand( -0.5, 0.5 ), math.Rand( -0.5, 0.5 ) ) )
    util.Effect( "kogunman_bounce", effectdata )

    return true
end

function SWEP:PrimaryAttack()
    if ( !self:CanPrimaryAttack() ) then return end


    self:CreateSound ()


    local bullet = {}
    bullet.Num = 1
    bullet.Src = self.Owner:GetShootPos()
    bullet.Dir = self.Owner:GetAimVector()
    bullet.Spread = Vector( 0.01, 0.01, 0 )
    bullet.Tracer = 1
    bullet.Force = 5
    bullet.Damage = 9
            //bullet.AmmoType = "Ar2AltFire" -- For some extremely stupid reason this breaks the tracer effect
    bullet.TracerName = "rb655_nyan_tracer"
    self.Owner:FireBullets( bullet )

    self:SendWeaponAnim( ACT_VM_PRIMARYATTACK )
    self.Owner:SetAnimation( PLAYER_ATTACK1 )

    self:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
    self:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
end

SWEP.NextReload = 0
SWEP.NextReload2 = 0
function SWEP:Reload()

    if self.Owner:KeyDown(IN_USE) then
        if self.NextStick > CurTime() then return end
        self.NextStick = CurTime() + 0.5

        if self:GetNWBool( "Planted" )  then
            self:BombSplode()
        elseif  self.hadBomb < 5 then
            self:StickIt()
        end;
        return
    end

    if self.NextReload < CurTime() then
        self.NextReload = CurTime() + 0.2
        local tr = self.Owner:GetEyeTrace()
        local hitEnt = tr.Entity


        if hitEnt:IsPlayer()  then
            local pos =  self.Owner:GetPos()
            self.Owner:SetPos(hitEnt:GetPos())
            hitEnt:SetPos(pos)
            local ef = EffectData()
            ef:SetEntity(self.Owner)
            util.Effect("swap_effect", ef,true,true)

            gb_PlaySoundFromServer(gb_config.websounds.."weapons/kogun_tp"..math.random(1,3)..".mp3", self.Owner,true,"kon_tp");
            self.NextReload = CurTime() + 11
        end

    end
end
SWEP.NextThrow = 0
function SWEP:SecondaryAttack()
    if self.Owner:KeyDown(IN_USE) and self.NextThrow < CurTime() then
        self.NextThrow = CurTime() + 15
        self:throw_attack ("kogun_doll",  "physics/glass/glass_impact_hard3.wav", 100)
         return
    end
    if  self.NextReload2 < CurTime() and SERVER then

        self.NextReload2 = CurTime() + 0.2
        local tr = self.Owner:GetEyeTrace()
        if tr.Hit and tr.Entity and tr.Entity:IsPlayer() and self.Owner:GetPos():Distance(tr.Entity:GetPos()) < 600 then

            local spell = ents.Create ("spell_paralyze")
            if spell ~= nil and spell:IsValid() then
                spell:SetPos(tr.Entity:GetPos())
                spell:SetOwner(self.Owner)
                spell:SetParent(tr.Entity)
                spell:Spawn()
                spell:Activate()
                gb_PlaySoundFromServer(gb_config.websounds.."weapons/kogun_stuck.mp3", self.Owner,true,"kon_lock");
            end
            self.NextReload2 = CurTime() + 15
        end
        return
    end
end

function SWEP:Think ()
    if !self.Owner:KeyDown( IN_ATTACK ) and self.Weapon:GetNWBool("on") then
    self:EndSound ()
    end

    local ply = self.Owner;
    if not IsValid( ply ) or self.hadBomb > 4 then return; end;

    local tr = ply:GetEyeTrace( );

    if tr.Entity:IsPlayer( ) and ply:GetPos( ):Distance( tr.Entity:GetPos( ) ) <= 99999999 and GetRoundState() == ROUND_ACTIVE then
        self.close = true;
    else
        self.close = false;
    end;
end

SWEP.NextSoundCheck = 0
function SWEP:CreateSound ()
    if not self.Weapon:GetNWBool("on") and ( not self.NextSoundCheck2 or self.NextSoundCheck2 < CurTime()) then
        self.NextSoundCheck2 = CurTime() + 1

        gb_PlaySoundFromServer(gb_config.websounds.."weapons/kogun_audio.mp3", self.Owner,true,"kogun_aud");
        self.Weapon:SetNWBool ("on", true)
        timer.Create("kogunplay"..self:EntIndex(),57,2,function()
            gb_PlaySoundFromServer(gb_config.websounds.."weapons/kogun_audio.mp3", self.Owner,true,"kogun_aud");
        end)


    end

end

function SWEP:Holster() self:EndSound() return true end
function SWEP:OwnerChanged() self:EndSound() end

function SWEP:EndSound()
    self.Weapon:SetNWBool ("on", false)
    timer.Destroy("kogunplay"..self:EntIndex())
    gb_StopSoundFromServer("kogun_aud")
end






function SWEP:throw_attack (model_file, sound, away)
local tr = self.Owner:GetEyeTrace();

if IsValid( tr.Entity )  then
if not tr.Entity.hasProtectionSuit then
tr.Entity.hasProtectionSuit= true
timer.Simple(5, function()
if not IsValid( tr.Entity )  then return end
tr.Entity.hasProtectionSuit = false
end)
end
end

self.BaseClass.ShootEffects (self);
if (!SERVER) then return end;
local ent = ents.Create ("kogun_doll");
--  ent:SetModel (model_file);
ent:SetPos( tr.HitPos + self.Owner:GetAimVector() * -46 )
ent:SetAngles (self.Owner:EyeAngles());
ent:SetPhysicsAttacker(self.Owner)
ent:Spawn();
ent:EmitSound( sound )
if math.Rand(1,25) < 2 then
ent:Ignite(100,100)
end
local phys = ent:GetPhysicsObject();
local shot_length = tr.HitPos:Length();
-- phys:ApplyForceCenter (self.Owner:GetAimVector():GetNormalized() * math.pow (shot_length, 3) * 2);

timer.Simple(away,function()
if IsValid(ent) then
ent:Remove()
end
end)

cleanup.Add (self.Owner, "props", ent);
undo.Create ("Thrown model");
undo.AddEntity (ent);
undo.SetPlayer (self.Owner);
undo.Finish();
end


local ENT = {}

ENT.Type = "anim"
ENT.Base = "base_anim"

function SWEP:OnDrop()
    timer.Destroy("kogunplay"..self:EntIndex())
    gb_StopSoundFromServer("kogun_aud")
end

if SERVER then

    function ENT:Initialize()


        self:SetModel("models/maxofs2d/companion_doll.mdl")
        self:SetModelScale(self:GetModelScale()*math.random(0.5,2.5),0)

        self.Entity:PhysicsInit(SOLID_VPHYSICS);
        self.Entity:SetMoveType(MOVETYPE_VPHYSICS);
        self.Entity:SetSolid(SOLID_VPHYSICS);
        self.Entity:SetCollisionGroup(COLLISION_GROUP_PROJECTILE)
        self.Damage = 1
        self.Activated = CurTime() + 2
        local phys = self:GetPhysicsObject()
        if phys:IsValid() then
            phys:Wake()
            phys:EnableGravity(true);
        end

    end

    function ENT:Use( activator, caller )
        if self.Activated > CurTime() then return end
        self.Entity:Remove()
        if ( activator:IsPlayer() ) then
            if activator:Health() < 175 then
                if self.lower then
                    activator:SetHealth(activator:Health()+10)
                else
                    activator:SetHealth(activator:Health()+10)
                end
            end
            gb_PlaySoundFromServer(gb_config.websounds.."weapons/kogun_tp1.mp3", activator,true,"kon_stuck");
            timer.Simple(0.05,function()            gb_PlaySoundFromServer(gb_config.websounds.."weapons/kogun_tp1.mp3", activator,true,"kon_stuck");
            end)
            timer.Simple(0.1,function()
                gb_PlaySoundFromServer(gb_config.websounds.."weapons/kogun_tp1.mp3", activator,true,"kon_stuck");
                end)
        end
    end


end

scripted_ents.Register( ENT, "kogun_doll", true )












SWEP.DrawCrosshair      = true
SWEP.hadBomb = 0
SWEP.NextStick = 0


SWEP.Planted = false;

local hidden = true;
SWEP.close = false;
local lastclose = false;

if CLIENT then
    function SWEP:DrawHUD( )
        if  self.hadBomb < 5 then
            local planted = self:GetNWBool( "Planted" );
            local tr = LocalPlayer( ):GetEyeTrace( );
            local close2;

            if tr.Entity:IsPlayer( ) and LocalPlayer( ):GetPos( ):Distance( tr.Entity:GetPos( ) ) <= 99999999 then
                close2 = true;
            else
                close2 = false;
            end;

            local planted_text = "Not Planted!";
            local close_text = "Nobody is in range!";

            if planted then

                planted_text = "Planted!\nR+E to Explode!";

                surface.SetFont( "ChatFont" );
                local size_x, size_y = surface.GetTextSize( planted_text );

                draw.RoundedBox( 6, ScrW( ) / 2 - size_x / 2 - 5, ScrH( ) - 100, size_x + 15, size_y + 10, Color( 0, 0, 0, 150 ) );
                draw.DrawText( planted_text, "ChatFont", ScrW( ) / 2, ScrH( ) - 100 + 5, Color( 255, 30, 30, 255 ), TEXT_ALIGN_CENTER );
            else
                if close2 then
                    close_text = "Close Enough!\nR+E to stick!!";
                end;

                surface.SetFont( "ChatFont" );
                local size_x, size_y = surface.GetTextSize( planted_text );

                surface.SetFont( "ChatFont" );
                local size_x2, size_y2 = surface.GetTextSize( close_text );

                draw.RoundedBox( 6, ScrW( ) / 2 - ( size_x2 / 2 ) - 7.5, ScrH( ) - 100, size_x2 + 20, size_y + size_y2 + 10, Color( 0, 0, 0, 150 ) );
                draw.DrawText( planted_text, "ChatFont", ScrW( ) / 2, ScrH( ) - 100 + 5, Color( 30, 30, 255, 255 ), TEXT_ALIGN_CENTER );
                draw.DrawText( close_text, "ChatFont", ScrW( ) / 2 , ScrH( ) - 100 + size_y + 5, Color( 255, 255, 255, 255 ), TEXT_ALIGN_CENTER );
            end;
        end;
    end;
end;



SWEP.BeepSound = Sound( "C4.PlantSound" );

SWEP.ClipSet = false

function SWEP:StickIt()

    --self.Weapon:SetNextSecondaryFire( CurTime() + self.Secondary.Delay )

    local ply = self.Owner;
    if not IsValid( ply ) then return; end;

    local tr = ply:GetEyeTrace(MASK_SHOT);

    if not self:GetNWBool( "Planted" ) then

        if self.close then
            self.hadBomb = self.hadBomb + 1
            if SERVER then
                self.PlantedPly = tr.Entity
                self:SetNWBool("Planted", true)
            end;
        end;
    end;
end


function SWEP:DoSplode( owner, plantedply, bool )

    self:SetNWBool( "Planted", false )

    local sound = "sil"..math.random(1,5)..".mp3";
    if math.random(1,5) == 5 then
         sound = "silent_pia.mp3"
    end

    gb_PlaySoundFromServer(gb_config.websounds..sound, self.Owner,true,"kon_tp");


    timer.Simple( 1, function( ) plantedply:EmitSound( self.BeepSound ); end );
    timer.Simple( 1.5, function( ) plantedply:EmitSound( self.BeepSound ); end );
    timer.Simple( 2, function( ) plantedply:EmitSound( self.BeepSound ); end );
    timer.Simple( 3, function( )
        local effectdata = EffectData()
        effectdata:SetOrigin(plantedply:GetPos())
        util.Effect("HelicopterMegaBomb", effectdata)

        local explosion = ents.Create("env_explosion")
        explosion:SetOwner(self.Owner)
        explosion:SetPos(plantedply:GetPos( ))
        explosion:SetKeyValue("iMagnitude", "42")
        explosion:SetKeyValue("iRadiusOverride", 0)
        explosion:Spawn()
        explosion:Activate()
        explosion:Fire("Explode", "", 0)
        explosion:Fire("kill", "", 0)
        self:SetNWBool( "Planted", false )



    end );




end;

function SWEP:BombSplode()
    local ply = self.Owner;
    if not IsValid( ply ) then return; end;

    if IsValid( self.PlantedPly ) and self.PlantedPly:Alive( ) then
        if SERVER then
            self:DoSplode( self.Owner, self.PlantedPly, false );
        end;
    else
        if SERVER then
            if IsValid( self.PlantedPly ) and IsValid( self.PlantedPly.server_ragdoll ) then
                self:DoSplode( self.Owner, self.PlantedPly.server_ragdoll, true );
            else
                ply:PrintMessage( HUD_PRINTTALK, "The body this bomb has been planted to is no longer available!" );
            end;
        end;
        --  self.Weapon:SendWeaponAnim( ACT_SLAM_DETONATOR_DETONATE );
    end;
end
