include "weapons/weapon_zm_shotgun.lua"
SWEP.Base = "gb_camo_base"
SWEP.ViewModelFOV	= 54
SWEP.HoldType = "smg"
SWEP.PrintName = "Aclgolden's Apple Slicer"
SWEP.ViewModel		= "models/weapons/c_shotgun.mdl"
SWEP.WorldModel		= "models/weapons/w_shotgun.mdl"
SWEP.AutoSpawnable = false

SWEP.HoldType = "shotgun"
SWEP.Slot = 0
SWEP.Kind = WEAPON_MELEE

if CLIENT then

    SWEP.Author = "GreenBlack"

    SWEP.Icon = "vgui/ttt_fun_killicons/xm1014.png"
end

SWEP.Primary.Sound			= Sound( "Weapon_M3.Single" )
SWEP.Primary.Recoil			= 7
SWEP.Camo = 7
SWEP.Primary.Ammo = "Buckshot"
SWEP.Primary.Damage = 10
SWEP.Primary.Cone = 0.085
SWEP.Primary.Delay = 0.8
SWEP.Primary.ClipSize = 40
SWEP.Primary.ClipMax = 80
SWEP.Primary.DefaultClip = 40
SWEP.Primary.Automatic = true
SWEP.Primary.NumShots = 8
SWEP.AutoSpawnable      = false
SWEP.AmmoEnt = "item_box_buckshot_ttt"

function SWEP:Deploy()
    self.dt.reloading = false
    self.reloadtimer = 0
    return self:DeployFunction()
end

SWEP.IronSightsPos = Vector(-7.65, -12.214, 3.2)
SWEP.IronSightsAng = Vector(0, 0, 0)

function SWEP:SecondaryAttack()
    self.Owner.hasProtectionSuit = true
    self:Fly()
end

function SWEP:Reload()
    if self.Owner:KeyDown(IN_USE) then
        self:Cannibal()
        return
    end
    if self.Owner:KeyDown(IN_ATTACK) then
        self:Jihad()
        return
    end
    self.BaseClass.Reload(self)
end