
if ( SERVER ) then

    AddCSLuaFile( )

end
SWEP.WElements = {
  --  ["world1"] = { type = "Model", model = "models/weapons/w_pist_m9g.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(0.157, 1.128, 0.266), angle = Angle(-5.875, 0, 179.442), size = Vector(1.2,1.2,1.2), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
  --  ["world2"] = { type = "Model", model = "models/weapons/w_pist_m9f_silencer.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(0.157, 1.128, 0.266), angle = Angle(-5.875, 0, 179.442), size = Vector(1.2,1.2,1.2), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
}
SWEP.CanDecapitate= true
SWEP.PrintName = "oSecres's Terminator of Justice"
SWEP.Slot				= 1
if ( CLIENT ) then

    SWEP.Category                = "Grub's Weapons"

    SWEP.Author 		="Residual Grub"
    SWEP.Instructions ="Hold E and right click for silencer"

    SWEP.SlotPos			= 4
    SWEP.ViewModelFOV      = 70
    SWEP.CSMuzzleFlashes    = true
end

SWEP.Kind				= 11
SWEP.Base				= "gb_camo_base" -- what is the base of the weapon
SWEP.Camo = -1

SWEP.Spawnable			= true
SWEP.AdminSpawnable		= true

SWEP.HoldType 			= "pistol" -- the animation in thirdperson
SWEP.UseHands			= true
SWEP.ViewModelFlip		= false
SWEP.ViewModelFOV		= 54
SWEP.ViewModel          = "models/weapons/cstrike/c_knife_t.mdl"
SWEP.WorldModel         = "models/weapons/w_knife_t.mdl"

SWEP.ShowWorldModel = true
SWEP.Weight				= 5
SWEP.AutoSwitchTo		= true
SWEP.AutoSwitchFrom		= true
SWEP.AutoSpawnable = true
SWEP.Primary.Sound			= Sound("weapons/ak47/ak47-1.wav")
SWEP.Primary.Recoil			= 0.5
SWEP.Primary.Damage			= 80
SWEP.Primary.NumShots		= 1
SWEP.Primary.Cone			= .03 -- the spread
SWEP.Primary.ClipSize		= 500
SWEP.Primary.RPM			= 0.3-- ROF
SWEP.Primary.DefaultClip	= 500 -- set it the same value as the ClipSize to prevent some problem
SWEP.Primary.Automatic		= true
SWEP.Primary.Ammo			= "pistol"
SWEP.Primary.Delay = 0.95

SWEP.Secondary.ClipSize		= -1
SWEP.Secondary.DefaultClip	= -1
SWEP.Secondary.Automatic	= false
SWEP.Secondary.Ammo			= "none"
SWEP.ShellDelay = 0
SWEP.ShellEffect = "none"
--SWEP.MuzzleEffect            = "muzzle_lee_silenced"
SWEP.MuzzleAttachment        = "1"
SWEP.ShellEjectAttachment    = "1"


SWEP.IronSightsPos = Vector(-2.56, -3.658, 0.159)
SWEP.IronSightsAng = Angle(0, 0, 0)
SWEP.RunSightsPos = Vector (0, 0, 0)
SWEP.RunSightsAng = Angle (-13.572, 21.82, 0)



local inputColor = {255, 0, 0}
function SWEP:randomColor(inputCol)
    speed = 0.25

    -- Red to Green
    if inputCol[1] == 255 and inputCol[3] == 0 and inputCol[2] != 255 then
    inputCol[2] = inputCol[2] + speed
    elseif inputCol[2] == 255 and inputCol[1] != 0 then
    inputCol[1] = inputCol[1] - speed

    -- Green to Blue
    elseif inputCol[2] == 255 and inputCol[3] != 255 then
    inputCol[3] = inputCol[3] + speed
    elseif inputCol[3] == 255 and inputCol[2] != 0 then
    inputCol[2] = inputCol[2] - speed

    -- Blue to Red
    elseif inputCol[3] == 255 and inputCol[1] != 255 then
    inputCol[1] = inputCol[1] + speed
    elseif inputCol[1] == 255 and inputCol[3] != 0 then
    inputCol[3] = inputCol[3] - speed
    end

    return Color( inputCol[1], inputCol[2], inputCol[3], 0)
end

function SWEP:PreDrawViewModel()
    if SERVER or not IsValid(self.Owner) or not IsValid(self.Owner:GetViewModel()) then return end
    local colors = self:randomColor(inputColor)
    self.Owner:GetViewModel():SetColor( colors )
    self.Owner:GetViewModel():SetMaterial("models/debug/debugwhite")

end
-- END COLOR

-- Color material set
function SWEP:SetColorAndMaterial( color, material)
    if not IsValid(self.Owner) then return end
    if SERVER then
        self:SetColor(color)
        self:SetMaterial("models/shiny") --models/shiny"))
        local vm = self.Owner:GetViewModel()
        if not IsValid(vm) then return end
        vm:ResetSequence(vm:LookupSequence("idle01"))
    end

    self.Owner:GetViewModel():SetMaterial("models/debug/debugwhite")
end

-- Color/materialreset
function SWEP:ColorReset()
    if SERVER then
        self:SetColor(Color(255,255,255,255))
        self:SetMaterial("")
        timer.Stop(self:GetClass() .. "_idle" .. self:EntIndex())
    elseif CLIENT and IsValid(self.Owner) and IsValid(self.Owner:GetViewModel()) then
        self.Owner:GetViewModel():SetColor(Color(255,255,255,255))
        self.Owner:GetViewModel():SetMaterial("")
    end
end




function SWEP:Deploy()
    self:SetNWString("shootmode","Terminate")
    self:SetColorAndMaterial(Color(255,255,255))

end


function SWEP:PrimaryAttack()
    if self.Owner:KeyDown(IN_RELOAD) then
        self:TripMineStick()
    end
    if self.Owner:KeyDown(IN_RELOAD) then return end
    if self.Owner:KeyDown(IN_USE) then
        if not self.nextgreen then self.nextgreen = 0 end
        if self.nextgreen > CurTime() then
            self.Owner:PrintMessage( HUD_PRINTCENTER,  math.floor(self.nextgreen - CurTime()).." seconds left" )
            return
        end

        local tr = self.Owner:GetEyeTrace()
        self.LaserColor = Color(50,255,50,255)
        self:ShootTracer("LaserTracer_thick")
        self.nextgreen = CurTime() + 30
        if CLIENT then return end
        local ent = ents.Create( "cse_ent_shorthealthgrenade" )
        ent:SetPos( tr.HitPos  )
        ent:SetOwner( self.Owner  )
        ent:SetPhysicsAttacker(  self.Owner )
        ent.Owner = self.Owner
        ent:Spawn()
        ent.timer = CurTime() + 0.01
        ent.EndColor = "0 0 0"
        ent.weapon = self
        ent.StartColor = "0 255 0"
        ent.healdamage = 3
        timer.Simple(8,function()
            if IsValid(ent) then
                ent.Bastardgas:Remove()
                ent.Entity:Remove()
            end
        end)
        return
    end


    local mode = self:GetNWString("shootmode")

    if mode == "Terminate" then

    --    self:ShootTracer("LaserTracer_thick")

    --self.Primary.Damage = 75

    self.BaseClass.PrimaryAttack(self)
   -- self:SetNextPrimaryFire( CurTime() + 1)
      --  self:EmitSound( table.Random(  { "weapons/warden/warden1.mp3","weapons/warden/warden2.mp3","weapons/warden/warden3.mp3"} ),100,100)
        return
    end

    if mode == "Shotgun" then

        self.Weapon:SetNextSecondaryFire( CurTime() + 0.5 )
        self.Weapon:SetNextPrimaryFire( CurTime() + 0.5 )

        self:ShootBullet( 11, 2, 6, 0.085 )
        self:ShootTracer("manatrace")
        self:TakePrimaryAmmo( 1 )
        self:EmitSound("Weapon_XM1014.Single")

        local owner = self.Owner
        if not IsValid(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

        owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )
        return
    end
    if mode == "Beam" then
        self.Primary.Damage = 16
        self:ShootTracer("LaserTracer_thick")
        if self:Clip1() > 0 then
            self:ShootTracer("manatrace")

        end
        self.BaseClass.PrimaryAttack(self)
        self:EmitSound( table.Random(  { "weapons/warden/warden1.mp3","weapons/warden/warden2.mp3","weapons/warden/warden3.mp3"} ),100,100)
        return
    end


end

SWEP.hideblind = true
SWEP.NextReload = 0
function SWEP:Reload()
    if self.Owner:KeyDown(IN_ATTACK) then
        return
    end
    if self.Owner:KeyDown(IN_USE) and self.NextReload < CurTime() then
        self.NextReload = CurTime() + 0.3
        local mode = self:GetNWString("shootmode")
        self:SetIronsights(false)

        if mode == "Terminate" then  self:SetNWString("shootmode","Beam")  end
        if mode == "Beam" then  self:SetNWString("shootmode","Shotgun") end
        if mode == "Shotgun" then  self:SetNWString("shootmode","Terminate") end

        self.Weapon:SetNextPrimaryFire( CurTime() + 0.8 )
        return
    end
end



SWEP.NextThink2 = 0
SWEP.thinks = 0
function SWEP:Think()
        if CLIENT and  IsValid(self) and self.NextThink2 < CurTime() and IsValid(self.Owner) and IsValid(self.Owner:GetViewModel()) then
            if self.thinks > 100 then
                self.NextThink2 = CurTime() + 3
            end
            self.thinks = self.thinks + 1
            self.Owner:GetViewModel():SetSubMaterial(1,"models/debug/white")

        end
end

SWEP.NextBomb = 0
function SWEP:SecondaryAttack()
    if self.Owner:KeyDown(IN_RELOAD) then
    local healt =self:DropHealth("normal_health_station",'models/balloons/balloon_classicheart.mdl')

        return
    end
    if self.Owner:KeyDown(IN_USE) then
        if CLIENT then return end
        if self:Clip1() > 0 and  self.NextBomb < CurTime() then

            self.NextBomb = CurTime() + 2
            local ent = ents.Create("thrown_penguin")
            ent:SetPos(self.Owner:GetShootPos())
            ent:SetAngles(Angle(math.random(1, 360), math.random(1, 360), math.random(1, 360)))
            ent:SetOwner(self.Weapon:GetOwner())
            ent:SetModel("models/weapons/w_knife_t.mdl")
            ent:Spawn()
            ent:SetModel("models/weapons/w_knife_t.mdl")
            ent:Activate()
            ent:SetModel("models/weapons/w_knife_t.mdl")

            ent:GetPhysicsObject():ApplyForceCenter(self.Owner:GetAimVector() * 12800)

            self.Weapon:SendWeaponAnim(ACT_VM_THROW)
            self.Owner:SetAnimation( PLAYER_ATTACK1 )
            timer.Simple(3.5, function()
             --   if not IsValid(ent) then return end
                local effectdata = EffectData()
                effectdata:SetOrigin(ent:GetPos())

                local effect = EffectData()
                effect:SetStart(ent:GetPos())
                effect:SetOrigin(ent:GetPos())
                effect:SetScale(90)
                effect:SetRadius(90)
                effect:SetMagnitude(100)

             sound.Play( "ambient/explosions/exp1.wav", Vector( ent:GetPos() ) )

                util.Effect("Explosion", effect, true, true)
             util.BlastDamage(self, self.Owner, self:GetPos(), 100, 50)


            end)

            timer.Simple(1, function()
                if (self.Weapon:GetOwner():GetActiveWeapon() == self.Weapon) then
                    self.Weapon:SendWeaponAnim(ACT_VM_DRAW)
                end
            end)

        end
        return
    end


    self:Fly()
end


function SWEP:HealthDrop(ent, healttt, model)
    if SERVER then
        local ply = self.Owner
        if not IsValid(ply) then return end

        local vsrc = ply:GetShootPos()
        local vang = ply:GetAimVector()
        local vvel = ply:GetVelocity()

        local vthrow = vvel + vang * 200

        local health = ents.Create(ent)
        if model then
            health.model = 'models/balloons/balloon_classicheart.mdl'
            health:SetModel('models/balloons/balloon_classicheart.mdl')
            health:SetMaterial("camos/camo7")
        end
        if IsValid(health) then
            health:SetPos(vsrc + vang * 10)
            health:Spawn()
            --health:SetModel( "models/props_lab/cactus.mdl")
            health:SetModelScale(health:GetModelScale()*1,0)
            health:SetPlacer(ply)
            --health.healsound = "ginger_cartman.mp3"

            health:PhysWake()
            local phys = health:GetPhysicsObject()
            if IsValid(phys) then
                phys:SetVelocity(vthrow)
            end
            if model then
                health:SetModel('models/balloons/balloon_classicheart.mdl')
                timer.Simple(0.5,function()
                    if IsValid(health) then
                        health:SetModel('models/balloons/balloon_classicheart.mdl')
                    end
                end)
            end
            --  health:SetModel( "models/props_lab/cactus.mdl")
            health:SetStoredHealth(200)
            health.setOwner = self.Owner

            self.healththing = health:EntIndex()

            self.Planted = true
        end
    end


end

if CLIENT then
    function SWEP:DrawHUD( )
    local shotttext = "Shootmode: "..self:GetNWString("shootmode","").."\nE+R to change"
    surface.SetFont( "ChatFont" );
    local size_x, size_y = surface.GetTextSize( shotttext );

    draw.RoundedBox( 6, ScrW( ) / 2 - size_x / 2 - 5, ScrH( ) - 150, size_x + 15, size_y + 10, Color( 0, 0, 0, 150 ) );
    draw.DrawText( shotttext , "ChatFont", ScrW( ) / 2, ScrH( ) - 150 + 5, Color( 255, 30, 30, 255 ), TEXT_ALIGN_CENTER );
    self.BaseClass.DrawHUD(self)

    end

end


