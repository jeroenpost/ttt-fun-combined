if SERVER then
    AddCSLuaFile(  )
end
if CLIENT then

    SWEP.Author 		= "GreenBlack"

    SWEP.SlotPos 	= 1
    SWEP.Icon = "vgui/ttt_fun_killicons/magnum.png"
end
SWEP.CanDecapitate= true
SWEP.PrintName	= "The Ruletska"
SWEP.Slot		= 1
SWEP.HoldType 		= "pistol"
SWEP.Base			= "aa_base"
SWEP.Spawnable 		= true
SWEP.AdminSpawnable = true
SWEP.Kind 			= WEAPON_PISTOL
SWEP.Primary.Ammo   = "Battery"
SWEP.Primary.Recoil	= 1
SWEP.Primary.Damage = 35
SWEP.Primary.Delay 	= 0.35
SWEP.Primary.Cone 	= 0.009
SWEP.Primary.ClipSize 		= 6
SWEP.Primary.ClipMax 		= 6
SWEP.Primary.DefaultClip 	= 6
SWEP.Primary.NumShots		= 1
SWEP.Primary.Automatic 	= true
SWEP.HeadshotMultiplier = 1
SWEP.AutoSpawnable      = false
SWEP.AmmoEnt 		= "item_ammo_revolver_ttt"
SWEP.Primary.Sound	= "gdeagle/gdeagle-1.mp3"
SWEP.Primary.SoundLevel = 400
SWEP.ViewModel = "models/weapons/v_357.mdl"
SWEP.WorldModel = "models/weapons/w_357.mdl"
SWEP.ShowWorldModel = true
SWEP.ShowViewModel = true
SWEP.ViewModelFlip = false

SWEP.LimitedStock = true
SWEP.InLoadoutFor = nil
SWEP.AllowDrop 	  = true
SWEP.IronSightsPos = Vector(1.14, -3.951, 0.736)
SWEP.IronSightsAng = Vector(0, 0, 0)
SWEP.IronSightsPos 	= Vector( 3.8, -1, 1.6 )
--SWEP.ViewModelFOV = 49


function SWEP:PrimaryAttack(worldsnd)


    self.Weapon:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
    if not self:CanPrimaryAttack() then return end

    self.Weapon:EmitSound( self.Primary.Sound, self.Primary.SoundLevel )
    local damage =50
    local numbul =  self:Clip1()
    if numbul == 6 then
        damage = 50
    elseif numbul == 5 then
        damage = 70
    elseif numbul == 4 then
        damage = 90
    elseif numbul == 3 then
        damage = 110
    elseif numbul == 2 then
        damage = 125
    elseif numbul == 1 then
        if not SERVER then return end
        self.Owner:PrintMessage(HUD_PRINTCENTER,"The Ruletska's last bullet killed you")

        self.Owner:Kill()
        self:TakePrimaryAmmo( 1 )
        return
    end

    if math.Rand(1,9) < 2 then
        if not SERVER then return end
        self.Owner:PrintMessage(HUD_PRINTCENTER,"The Ruletska killed you")
        self.Owner:Kill()
    end

    self:ShootBullet( damage, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )

        self:TakePrimaryAmmo( 1 )
        self.Owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )

end
