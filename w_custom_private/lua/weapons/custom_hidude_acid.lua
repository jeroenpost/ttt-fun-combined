SWEP.Base = "gb_camo_base"
SWEP.ViewModelFOV	= 75
SWEP.HoldType = "pistol"
SWEP.PrintName = "Hidude's Acid Balloons"
SWEP.ViewModel		= "models/weapons/v_pist_deags.mdl"
SWEP.WorldModel		= "models/weapons/w_pist_deagle.mdl"
SWEP.Kind = WEAPON_EQUIP2
SWEP.Slot = 6
SWEP.AutoSpawnable = false
SWEP.Primary.Damage		    = 1
SWEP.Primary.Delay		    = 0.35
SWEP.Primary.ClipSize		= 250
SWEP.Primary.DefaultClip	= 250
SWEP.Primary.Cone	        = 0.008
SWEP.Primary.Automatic		= true
SWEP.Primary.Ammo = "AlyxGun"
SWEP.AmmoEnt = "item_ammo_revolver_ttt"
SWEP.Primary.Sound          = Sound("CSTM_SilencedShot7")
SWEP.HeadshotMultiplier = 2.5
SWEP.Camo = 7

SWEP.IronSightsPos = Vector(-6.361, -3.701, 2.15)
SWEP.IronSightsAng = Vector(0, 0, 0)


function SWEP:PrimaryAttack()
    self.Owner:SetJumpPower(450)
    self:SetNextPrimaryFire( CurTime() + self.Primary.Delay	)
    if self:Clip1() > 0 then
        self.BaseClass.PrimaryAttack(self)
        self:throw_attack("hidude_acis_ball",100000)

    end
end

function SWEP:Deploy()
    self.Owner:SetJumpPower(450)
    return self.BaseClass.Deploy(self)
end


function SWEP:SecondaryAttack()
    if self.Owner:KeyDown(IN_USE) then
        self:Fly()
        return
    end
    self.Owner:SetJumpPower(450)
    self:SetNextPrimaryFire( CurTime() + self.Primary.Delay	)
    if self:Clip1() > 0 then
        self.BaseClass.PrimaryAttack(self)
        self:throw_attack("hidude_acis_ball",100000)

    end

end

local ENT = {}

ENT.Type = "anim"
ENT.Base = "base_anim"



if SERVER then

    function ENT:Initialize()

        self.Hit = false
        self.LastShout = CurTime()
        self.CurrentPitch = 100
        self.SpawnDelay = CurTime() + 0.5
        --damage self.Damage = 75
        self:SetModel("models/props/cs_italy/orange.mdl")
        self:SetModelScale(1.5,0.01)
        self:SetMaterial("phoenix_storms/plastic" )
        self:SetColor(Color(0, 255, 0,255))
        self:PhysicsInitBox(Vector(-4,-4,-4),Vector(4,4,4))
        self:SetMoveType(MOVETYPE_VPHYSICS)
      --  self:SetSolid(SOLID_VPHYSICS)
        self:SetTrigger(true)
        self.Mag = 30

        self:DrawShadow(false)
        -- self:SetModel( "models/weapons/w_rif_famas.mdl")

        timer.Simple(10,function()
            if IsValid(self) then self:Remove() end
        end)

        local phys = self:GetPhysicsObject()
        if (phys:IsValid()) then
            phys:Wake()
            phys:EnableDrag(false)
            phys:AddGameFlag(FVPHYSICS_NO_IMPACT_DMG)
            --    phys:SetBuoyancyRatio(0)
        end

    end
    ENT.exploded = false

    function ENT:Explode()
        if self.exploded then return end self.exploded = true
        local ply = self.Owner


        local guys = ents.FindInSphere( self:GetPos()+Vector(0,0,40), 150 )
        local owner = self.Owner
        for k, guy in pairs(guys) do
            if owner != guy and (guy:IsPlayer() or guy:IsNPC())  then
                local effect = EffectData()
                local origin = guy:GetPos()
                effect:SetOrigin( origin )
                effect:SetScale( 1 )
                util.Effect( "AntlionGib", effect )

                timer.Create(guy:EntIndex().."acid"..math.Rand(1,10),1,5,function()
                    if IsValid(owner) and IsValid(guy)  then
                     guy:TakeDamage( 5, owner, owner )
                    end
                 end)
            end
        end

        -- util.BlastDamage( self, self.Owner, self:GetPos(), 50, 75 )

        self:EmitSound( "ambient/fire/gascan_ignite1.wav" )
        if SERVER and not self.shouldnotremove then self:Remove() end
    end


    function ENT:Touch(ent)
        if ent != self.Owner then
        self:Explode()
        end
    end



end

scripted_ents.Register( ENT, "hidude_acis_ball", true )