if SERVER then

   AddCSLuaFile(  )
   
	 
	 
end

SWEP.HoldType			= "smg"
SWEP.PrintName			= "Spooky Mp7"
SWEP.Slot				= 5

if CLIENT then

   
   SWEP.Author				= "Assassin"
   
   SWEP.SlotPos			= 0

   SWEP.Icon = "vgui/ttt_fun_killicons/mp7.png"

	 
	 
   
   SWEP.ViewModelFlip		= true
end


SWEP.Base				= "aa_base"

SWEP.Spawnable = true
SWEP.AdminSpawnable = true

SWEP.Kind = WEAPON_EQUIP3


BOLT_AIR_VELOCITY	= 3500
BOLT_WATER_VELOCITY	= 1500
BOLT_SKIN_NORMAL	= 0
BOLT_SKIN_GLOW		= 1

CROSSBOW_GLOW_SPRITE	= "sprites/light_glow02_noz.vmt"
CROSSBOW_GLOW_SPRITE2	= "sprites/blueflare1.vmt"


SWEP.PrimaryAnim = ACT_VM_PRIMARYATTACK_SILENCED
SWEP.ReloadAnim = ACT_VM_RELOAD_SILENCED

SWEP.Primary.Damage = 35
SWEP.Primary.Delay = 0.3
SWEP.Primary.Cone = 0.001
SWEP.Primary.ClipSize = 80
SWEP.Primary.ClipMax = 160
SWEP.Primary.DefaultClip	= 80
SWEP.Primary.Automatic		= true
SWEP.Primary.Ammo			= "smg1"
SWEP.AutoSpawnable      = false
SWEP.Primary.Recoil			= 0.08
SWEP.Primary.Sound			= Sound("weapons/mp7/mp7_fire.mp3")
SWEP.ViewModel			= "models/weapons/v_mp7_silenced.mdl"
SWEP.WorldModel			= "models/weapons/w_mp7_silenced.mdl"

SWEP.AmmoEnt = "item_ammo_smg1_ttt"

SWEP.HeadshotMultiplier = 1

SWEP.Secondary.Sound = Sound("Default.Zoom")

SWEP.IronSightsPos      = Vector( 5, -15, -2 )
SWEP.IronSightsAng      = Vector( 2.6, 1.37, 3.5 )

function SWEP:SetZoom(state)
    if CLIENT or not IsValid(self.Owner) then return end
       if state then
          self.Owner:SetFOV(20, 0.3)
       else
          self.Owner:SetFOV(0, 0.2)
       end

end

function SWEP:OnDrop()
    self:Remove()
end

SWEP.nextAmmo = 0
function SWEP:Think()
    if SERVER and self.nextAmmo < CurTime() then
        self.nextAmmo = CurTime() + 2
        self:SetClip1( self:Clip1() + 1 )
    end


end

-- Add some zoom to ironsights for this gun
SWEP.flyammo = 5

function SWEP:SecondaryAttack()

    self:Fly()
end

function SWEP:PreDrop()
    self:SetZoom(false)
    self:SetIronsights(false)
    return self.BaseClass.PreDrop(self)
end


    SWEP.nextreload = 0

    function SWEP:Reload(worldsnd)
        if  self.Owner:KeyDown(IN_USE) then
            self:Defib();
            return
        end

        self.Weapon:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
        self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
        if self.nextreload > CurTime() or self:Clip1() < 15 then return end
        self.nextreload = CurTime() + 1
        self:ShootBullet( self.Primary.Damage, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )

        if ( self.Owner:IsValid() ) then
            local tr = self.Owner:GetEyeTrace()
            local effectdata = EffectData()
            effectdata:SetOrigin(tr.HitPos)
            effectdata:SetNormal(tr.HitNormal)
            effectdata:SetScale(1)
            -- util.Effect("effect_mad_ignition", effectdata)
            util.Decal("FadingScorch", tr.HitPos + tr.HitNormal, tr.HitPos - tr.HitNormal)

            local tracedata = {}
            tracedata.start = tr.HitPos
            tracedata.endpos = Vector(tr.HitPos.x, tr.HitPos.y, tr.HitPos.z - 10)
            tracedata.filter = tr.HitPos
            local tracedata = util.TraceLine(tracedata)
            if SERVER then

                ent = tr.Entity
                if ent:IsPlayer() or ent:IsNPC() then
                    ply = ent
                    if not ply.OldRunSpeed and not ply.OldWalkSpeed then
                        ply.OldRunSpeed = ply:GetNWInt("runspeed")
                        ply.OldWalkSpeed =  ply:GetNWInt("walkspeed")
                    end

                    ply:SetNWInt("runspeed", 25)
                    ply:SetNWInt("walkspeed", 25)

                    timer.Create("NormalizeRunSpeedPokemonFreeze2"..math.random(0,9999999999),3,1,function()
                        if IsValid(ply) and  ply.OldRunSpeed and ply.OldWalkSpeed then
                            ply:SetNWInt("runspeed",  ply.OldRunSpeed)
                            ply:SetNWInt("walkspeed",ply.OldWalkSpeed)
                            ply.OldRunSpeed = false
                            ply.OldWalkSpeed = false
                        end
                    end)

                end
            end
            self:TakePrimaryAmmo( 15 )
            local owner = self.Owner
            --if not ValidEntity(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

            owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )
        end
    end


function SWEP:Holster()
    self:SetIronsights(false)
    self:SetZoom(false)
    return true
end

if CLIENT then
   local scope = surface.GetTextureID("sprites/scope")
   function SWEP:DrawHUD()
      if self:GetIronsights() then
         surface.SetDrawColor( 0, 0, 0, 255 )
         
         local x = ScrW() / 2.0
         local y = ScrH() / 2.0
         local scope_size = ScrH()

         -- crosshair
         local gap = 80
         local length = scope_size
         surface.DrawLine( x - length, y, x - gap, y )
         surface.DrawLine( x + length, y, x + gap, y )
         surface.DrawLine( x, y - length, x, y - gap )
         surface.DrawLine( x, y + length, x, y + gap )

         gap = 0
         length = 50
         surface.DrawLine( x - length, y, x - gap, y )
         surface.DrawLine( x + length, y, x + gap, y )
         surface.DrawLine( x, y - length, x, y - gap )
         surface.DrawLine( x, y + length, x, y + gap )


         -- cover edges
         local sh = scope_size / 2
         local w = (x - sh) + 2
         surface.DrawRect(0, 0, w, scope_size)
         surface.DrawRect(x + sh - 2, 0, w, scope_size)

         surface.SetDrawColor(255, 0, 0, 255)
         surface.DrawLine(x, y, x + 1, y + 1)

         -- scope
         surface.SetTexture(scope)
         surface.SetDrawColor(255, 255, 255, 255)

         surface.DrawTexturedRectRotated(x, y, scope_size, scope_size, 0)

      else
         return self.BaseClass.DrawHUD(self)
      end
   end

   function SWEP:AdjustMouseSensitivity()
      return (self:GetIronsights() and 0.2) or nil
   end
end


SWEP.defibs = 0
SWEP.nextDefib = 0
function SWEP:Defib()

    if ( self.defibs > 0 or self.nextDefib > CurTime()) then return end


    self.NextAttacky = CurTime()+ 0.5


    local tr = util.TraceLine({ start = self.Owner:EyePos(), endpos = self.Owner:EyePos() + self.Owner:EyeAngles():Forward() * 80, filter = self.Owner })
    if tr.HitNonWorld and IsValid(tr.Entity) and tr.Entity:GetClass() == "prop_ragdoll" and tr.Entity.player_ragdoll then
        self.Weapon:EmitSound( "weapons/c4/c4_beep1.wav"	)
        local mins, maxs = self.Owner:OBBMins(), self.Owner:OBBMaxs()
        local tr2 = util.TraceHull({start = tr.Entity:LocalToWorld(tr.Entity:OBBCenter()) + vector_up, endpos = tr.Entity:LocalToWorld(tr.Entity:OBBCenter()) + Vector(0, 0, 80), filter = {tr.Entity, self.Owner}, mins = Vector(mins.x * 1.6, mins.y * 1.6, 0), maxs = Vector(maxs.x * 1.6, maxs.y * 1.6, 0), mask = MASK_PLAYERSOLID})
        if tr2.Hit then
            self.Owner:PrintMessage( HUD_PRINTCENTER, "There is no room to bring him back here" )
            return
        end


        self.nextDefib = CurTime() + 10
        self.defibs =  self.defibs + 1
        if SERVER then
            rag = tr.Entity
            local ply = player.GetByUniqueID(rag.uqid)
            local credits = CORPSE.GetCredits(rag, 0)
            if not IsValid(ply) then return end
            if SERVER and _globals['slayed'][ply:SteamID()] then self.Owner:PrintMessage( HUD_PRINTCENTER, "Can't revive, person was slayed" )
                ply:Kill();
                return
            end
            if SERVER and ((rag.was_role == ROLE_TRAITOR and self.Owner:GetRole() ~= ROLE_TRAITOR) or (rag.was_role ~= ROLE_TRAITOR and self.Owner:GetRole() == ROLE_TRAITOR))  then
                DamageLog("Defib: " .. self.Owner:Nick() .. " [" .. self.Owner:GetRoleString() .. "] Died defibbing "..ply:Nick().." [" .. ply:GetRoleString() .. "] ")
                self.Owner:PrintMessage( HUD_PRINTCENTER, ply:Nick().." was of the other team,  sucked up your soul and killed you" )
                self.Owner:Kill();

                return
            end
            DamageLog("Defib: " .. self.Owner:Nick() .. " [" .. self.Owner:GetRoleString() .. "] defibbed "..ply:Nick().." [" .. ply:GetRoleString() .. "] ")

            ply:SpawnForRound(true)
            ply:SetCredits(credits)
            ply:SetPos(rag:LocalToWorld(rag:OBBCenter()) + vector_up / 2) --Just in case he gets stuck somewhere
            ply:SetEyeAngles(Angle(0, rag:GetAngles().y, 0))
            ply:SetCollisionGroup(COLLISION_GROUP_WEAPON)
            timer.Simple(2, function() ply:SetCollisionGroup(COLLISION_GROUP_PLAYER) end)

            umsg.Start("_resurrectbody")
            umsg.Entity(ply)
            umsg.Bool(CORPSE.GetFound(rag, false))
            umsg.End()
            rag:Remove()
        end
    else
        self.Weapon:EmitSound("npc/scanner/scanner_nearmiss1.wav")
        self.nextDefib = CurTime() + 0.5
    end
end

function SWEP:PrimaryAttack()
    if ( !self:CanPrimaryAttack() ) then return end
    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay );
    self.Weapon:SetNextSecondaryFire( CurTime() + self.Primary.Delay );

    self:FireBolt(1);


    // Signal a reload
    self.m_bMustReload = true;
    self:TakePrimaryAmmo( 1 );



    self.Weapon:EmitSound( self.Primary.Sound );
    -- self.Owner:EmitSound( self.Primary.Special2 );

    self.Weapon:SendWeaponAnim( ACT_VM_PRIMARYATTACK );



    // self:DoLoadEffect();
    // self:SetChargerState( CHARGER_STATE_DISCHARGE );

end

function SWEP:FireBolt( con )

    if ( self.Weapon:Clip1() <= 0 ) then

        return;
    end

    local pOwner = self.Owner;

    if ( pOwner == NULL ) then
        return;
    end

    if ( SERVER ) then


        local angAiming;


        local count2 = 0
        if con == 1 then cone = 0.030 else cone = 0.00000001 end
        -- while count2 < 10 do
        count2 = count2 + 1


        vecAiming		= pOwner:GetAimVector() ;
        vecSrc		= pOwner:GetShootPos();
        angAiming = vecAiming:Angle() ;
        self:ShootBullet( 35, 1, cone )
        local bullet = {}
        bullet.Num 		= 15
        bullet.Src 		= self.Owner:GetShootPos()	-- Source
        bullet.Dir 		= self.Owner:GetAimVector()	-- Dir of bullet
        bullet.Spread 	= Vector( 0.5, 0.5, 0 )		-- Aim Cone
        bullet.Tracer	= 1	-- Show a tracer on every x bullets
        bullet.TracerName = "Tracer" -- what Tracer Effect should be used
        bullet.Force	= 1	-- Amount of force to give to phys objects
        bullet.Damage	= 0
        bullet.AmmoType = "Pistol"

        self.Owner:FireBullets( bullet )

        self:ShootEffects()



        local pBolt = ents.Create (  "crossbow_bolt" );
        pBolt:SetPos( vecSrc );
        pBolt:SetAngles( angAiming );

        pBolt.Damage = 0


        pBolt.AmmoType = self.Primary.Ammo;
        pBolt:SetOwner( pOwner );
        pBolt:Spawn()

        if ( pOwner:WaterLevel() == 3 ) then
            pBolt:SetVelocity( vecAiming * BOLT_WATER_VELOCITY );
        else
            pBolt:SetVelocity( vecAiming * BOLT_AIR_VELOCITY );
        end

    end



end