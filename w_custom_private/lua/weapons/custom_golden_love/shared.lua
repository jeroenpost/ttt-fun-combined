if SERVER then
   AddCSLuaFile(  )
   --resource.AddFile("materials/vgui/ttt_fun_killicons/m3.png")
end
SWEP.CanDecapitate= true
SWEP.HoldType = "shotgun"
SWEP.PrintName = "Golden Love"
SWEP.Slot = 1
if CLIENT then
   
   SWEP.Author = "GreenBlack"
   
   SWEP.Icon = "vgui/ttt_fun_killicons/m3.png"
end

SWEP.Base = "aa_base"
SWEP.Spawnable = true
SWEP.AdminSpawnable = true
SWEP.Kind = WEAPON_PISTOL
SWEP.WeaponID = AMMO_SHOTGUN

SWEP.Primary.Ammo = "Buckshot"
SWEP.Primary.Damage = 7
SWEP.Primary.Cone = 0.085
SWEP.Primary.Delay = 0.2
SWEP.Primary.ClipSize = 200
SWEP.Primary.ClipMax = 200
SWEP.Primary.DefaultClip = 200
SWEP.Primary.Automatic = false
SWEP.Primary.NumShots = 8
SWEP.HeadshotMultiplier = 1.5
SWEP.AutoSpawnable = false
SWEP.AmmoEnt = "item_box_buckshot_ttt"
SWEP.HoldType = "shotgun"
SWEP.ViewModelFOV = 65
SWEP.ViewModelFlip = false
SWEP.ViewModel  = "models/weapons/cstrike/c_smg_mac10.mdl"
SWEP.WorldModel = "models/weapons/w_smg_mac10.mdl"
SWEP.ShowViewModel = true
SWEP.ShowWorldModel = true
SWEP.UseHands = true
SWEP.Primary.Sound = Sound( "weapons/fx/nearmiss/bulletLtoR03.wav")
SWEP.Primary.Recoil = 0.5
SWEP.reloadtimer = 0

SWEP.IronSightsPos = Vector(5.74, -2.19, 3.28)
SWEP.IronSightsAng = Vector(0.547, 0, 0)

function SWEP:PreDrop(yes)
    if yes then self:Remove() end
end

function SWEP:SetupDataTables()
   self:DTVar("Bool", 0, "reloading")

   return self.BaseClass.SetupDataTables(self)
end




function SWEP:CanPrimaryAttack()
    if self.Weapon:Clip1() <= 0 then
        self:EmitSound( "Weapon_Shotgun.Empty" )
        self:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
        return false
    end
    if self.reloadtimer < CurTime() then
        return true
    else
        return false
    end
end


function SWEP:GetHeadshotMultiplier(victim, dmginfo)
   local att = dmginfo:GetAttacker()
   if not IsValid(att) then return 3 end

   local dist = victim:GetPos():Distance(att:GetPos())
   local d = math.max(0, dist - 140)
   
   return 1 + math.max(0, (2.1 - 0.002 * (d ^ 1.25)))
end

SWEP.reloadtimer = 0

function SWEP:SetupDataTables()
    self:DTVar("Bool", 0, "reloading")

    return self.BaseClass.SetupDataTables(self)
end

function SWEP:Reload()
    self:SetIronsights( false )

    --if self.Weapon:GetNWBool( "reloading", false ) then return end
    if self.dt.reloading then return end

    if not IsFirstTimePredicted() then return end

    if self.Weapon:Clip1() < self.Primary.ClipSize and self.Owner:GetAmmoCount( self.Primary.Ammo ) > 0 then

        if self:StartReload() then
            return
        end
    end

end

function SWEP:StartReload()
    --if self.Weapon:GetNWBool( "reloading", false ) then
    if self.dt.reloading then
        return false
    end

    if not IsFirstTimePredicted() then return false end

    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )

    local ply = self.Owner

    if not ply or ply:GetAmmoCount(self.Primary.Ammo) <= 0 then
        return false
    end

    local wep = self.Weapon

    if wep:Clip1() >= self.Primary.ClipSize then
        return false
    end

    wep:SendWeaponAnim(ACT_SHOTGUN_RELOAD_START)

    self.reloadtimer =  CurTime() + wep:SequenceDuration()

    --wep:SetNWBool("reloading", true)
    self.dt.reloading = true

    return true
end

function SWEP:PerformReload()
    local ply = self.Owner

    -- prevent normal shooting in between reloads
    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )

    if not ply or ply:GetAmmoCount(self.Primary.Ammo) <= 0 then return end

    local wep = self.Weapon

    if wep:Clip1() >= self.Primary.ClipSize then return end

    self.Owner:RemoveAmmo( 1, self.Primary.Ammo, false )
    self.Weapon:SetClip1( self.Weapon:Clip1() + 1 )

    wep:SendWeaponAnim(ACT_VM_RELOAD)

    self.reloadtimer = CurTime() + wep:SequenceDuration()
end

function SWEP:FinishReload()
    self.dt.reloading = false
    self.Weapon:SendWeaponAnim(ACT_SHOTGUN_RELOAD_FINISH)

    self.reloadtimer = CurTime() + self.Weapon:SequenceDuration()
end


function SWEP:Think()
    self:HealSecond()
    if self.dt.reloading and IsFirstTimePredicted() then
        if self.Owner:KeyDown(IN_ATTACK) then
            self:FinishReload()
            return
        end

        if self.reloadtimer <= CurTime() then

            if self.Owner:GetAmmoCount(self.Primary.Ammo) <= 0 then
                self:FinishReload()
            elseif self.Weapon:Clip1() < self.Primary.ClipSize then
                self:PerformReload()
            else
                self:FinishReload()
            end
            return
        end
    end
end

function SWEP:SecondaryAttack()
    self:Fly()
end

-- COLOR
function SWEP:Deploy()
    self.Weapon:SendWeaponAnim(ACT_VM_DRAW)
    self:SetColorAndMaterial(Color(255,255,255,255), "gb/gold/gold_player");
    return true
end


function SWEP:PreDrop()
    self:ColorReset()
end


function SWEP:Holster()
    self:ColorReset()
    return true
end

function SWEP:OnRemove()
    self:ColorReset()
end
function SWEP:OnDrop()
    self:ColorReset()

end

function SWEP:PreDrawViewModel()

    if SERVER or not IsValid(self.Owner) or not IsValid(self.Owner:GetViewModel()) then return end
    self.Owner:GetViewModel():SetColor(Color(255,255,255,255))
    self.Owner:GetViewModel():SetMaterial( "gb/gold/gold_player");

end
