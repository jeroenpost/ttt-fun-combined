if SERVER then
   AddCSLuaFile(  )
   --resource.AddFile("materials/vgui/ttt_fun_killicons/famas.png")
end

SWEP.HoldType = "ar2"

SWEP.PrintName = "A$$"
SWEP.Slot = 2

if CLIENT then
   
   SWEP.Author = "GreenBlack"
   
   SWEP.Icon = "vgui/ttt_fun_killicons/famas.png"
   SWEP.ViewModelFlip = false
end

SWEP.Base = "weapon_tttbase"
SWEP.Kind = WEAPON_HEAVY

SWEP.Primary.Damage = 14
SWEP.Primary.Delay = 0.08
SWEP.Primary.Cone = 0.04
SWEP.Primary.ClipSize = 9999
SWEP.Primary.ClipMax = 9999
SWEP.Primary.DefaultClip = 9999
SWEP.Primary.Automatic = true
SWEP.Primary.Ammo = "smg1"
SWEP.Primary.Recoil = 1.2
SWEP.Primary.Sound = Sound( "weapons/famas/famas-1.wav" )
SWEP.Primary.SoundLevel = 0
SWEP.AutoSpawnable = false
SWEP.AmmoEnt = "item_ammo_smg1_ttt"

SWEP.ViewModel = "models/weapons/v_rif_famas.mdl"
SWEP.WorldModel = "models/weapons/w_rif_famas.mdl"

SWEP.IronSightsPos = Vector( -4.65, -3.28, 0.01 )
SWEP.IronSightsAng = Vector( 1.09, 0, -2.19 )

function SWEP:GetHeadshotMultiplier(victim, dmginfo)
   local att = dmginfo:GetAttacker()
   if not IsValid(att) then return 2 end

   local dist = victim:GetPos():Distance(att:GetPos())
   local d = math.max(0, dist - 150)
 
   return 1.5 + math.max(0, (1.5 - 0.002 * (d ^ 1.25)))
end


SWEP.nextBolty = 0

function SWEP:PrimaryAttack()
    if ( self.nextBolty > CurTime() ) then return end
    self.nextBolty = CurTime() + 0.4

    if ( self.m_bInZoom && IsMultiplayer() ) then
    //		self:FireSniperBolt();
    self:FireBolt();
    else
        self:FireBolt();
    end

    // Signal a reload
    self.m_bMustReload = true;

end


function SWEP:FireBolt()

    if ( self.Weapon:Clip1() <= 0 && self.Primary.ClipSize > -1 ) then
    if ( self:Ammo1() > 3 ) then
        self:Reload();
        self:ShootBullet( 40, 1, 0.01 )
    else
        self.Weapon:SetNextPrimaryFire( 5 );

    end

    return;
    end

    local pOwner = self.Owner;

    if ( pOwner == NULL ) then
        return;
    end

    if ( !CLIENT ) then
    local vecAiming		= pOwner:GetAimVector();
    local vecSrc		= pOwner:GetShootPos();

    local angAiming;
    angAiming = vecAiming:Angle();

    local pBolt = ents.Create ( "crossbow_bolt" );
    pBolt:SetPos( vecSrc );
    pBolt:SetAngles( angAiming );
    pBolt.Damage = self.Primary.Damage;
    self:ShootBullet( 50, 1, 0.01 )
    pBolt.AmmoType = self.Primary.Ammo;
    pBolt:SetOwner( pOwner );
    pBolt:Spawn()

    if ( pOwner:WaterLevel() == 3 ) then
        pBolt:SetVelocity( vecAiming * BOLT_WATER_VELOCITY );
    else
        pBolt:SetVelocity( vecAiming * BOLT_AIR_VELOCITY );
    end
    local tr = self.Owner:GetEyeTrace()
    local hitEnt = tr.Entity
    if hitEnt:IsPlayer() and self.Owner:Health() < 175 then
        self.Owner:SetHealth( self.Owner:Health() + 5)
    end
    end


    self:TakePrimaryAmmo(1 );

    if ( !pOwner:IsNPC() ) then
    pOwner:ViewPunch( Angle( -2, 0, 0 ) );
    end

    self.Weapon:EmitSound( self.Primary.Sound );
    -- self.Owner:EmitSound( self.Primary.Special2 );

    self.Weapon:SendWeaponAnim( ACT_VM_PRIMARYATTACK );

    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay );
    self.Weapon:SetNextSecondaryFire( CurTime() + self.Primary.Delay );

    // self:DoLoadEffect();
    // self:SetChargerState( CHARGER_STATE_DISCHARGE );

end



function SWEP:SetDeploySpeed( speed )

    self.m_WeaponDeploySpeed = tonumber( speed / GetConVarNumber( "phys_timescale" ) )

    self.Weapon:SetNextPrimaryFire( CurTime() + speed )
    self.Weapon:SetNextSecondaryFire( CurTime() + speed )

end

function SWEP:WasBought(buyer)
    if IsValid(buyer) then -- probably already self.Owner
        buyer:GiveAmmo( 6, "XBowBolt" )
    end
end

SWEP.NextSecondary = 0
function SWEP:Reload()
    if self.NextSecondary > CurTime() or not SERVER then return end
    self.NextSecondary = CurTime() + 20
    self.Owner:SetNWFloat("w_stamina", 1)
    self.Owner:SetNWInt("runspeed", 950)
    self.Owner:SetNWInt("walkspeed",800)
    self.Owner:SetJumpPower(450)
    self.Owner.hasProtectionSuit = true

    timer.Simple(5,function()
        if IsValid(self) and self.Owner:IsValid() then
            self.Owner:SetNWInt("runspeed", 400)
            self.Owner:SetNWInt("walkspeed",250)
            self.Owner:SetJumpPower(480)
        end
    end)

end


function SWEP:SetZoom(state)
    if CLIENT then
        return
    elseif IsValid(self.Owner) and self.Owner:IsPlayer() then
        if state then
            self.Owner:SetFOV(20, 0.3)
        else
            self.Owner:SetFOV(0, 0.2)
        end
    end
end

SWEP.Secondary.Sound = Sound("Default.Zoom")
-- Add some zoom to ironsights for this gun
function SWEP:SecondaryAttack()
    if not self.IronSightsPos then return end
    if self.Weapon:GetNextSecondaryFire() > CurTime() then return end

    bIronsights = not self:GetIronsights()

    self:SetIronsights( bIronsights )

    if SERVER then
        self:SetZoom(bIronsights)
    else
        self:EmitSound(self.Secondary.Sound)
    end

    self.Weapon:SetNextSecondaryFire( CurTime() + 0.3)
end

function SWEP:PreDrop()
    self:SetZoom(false)
    self:SetIronsights(false)
    return self.BaseClass.PreDrop(self)
end

function SWEP:Reload()
    self.Weapon:DefaultReload( ACT_VM_RELOAD );
    self:SetIronsights( false )
    self:SetZoom(false)
end


function SWEP:Holster()
    self:SetIronsights(false)
    self:SetZoom(false)
    return true
end

if CLIENT then
    local scope = surface.GetTextureID("sprites/scope")
    function SWEP:DrawHUD()
        if self:GetIronsights() then
            surface.SetDrawColor( 0, 0, 0, 255 )

            local x = ScrW() / 2.0
            local y = ScrH() / 2.0
            local scope_size = ScrH()

            -- crosshair
            local gap = 80
            local length = scope_size
            surface.DrawLine( x - length, y, x - gap, y )
            surface.DrawLine( x + length, y, x + gap, y )
            surface.DrawLine( x, y - length, x, y - gap )
            surface.DrawLine( x, y + length, x, y + gap )

            gap = 0
            length = 50
            surface.DrawLine( x - length, y, x - gap, y )
            surface.DrawLine( x + length, y, x + gap, y )
            surface.DrawLine( x, y - length, x, y - gap )
            surface.DrawLine( x, y + length, x, y + gap )


            -- cover edges
            local sh = scope_size / 2
            local w = (x - sh) + 2
            surface.DrawRect(0, 0, w, scope_size)
            surface.DrawRect(x + sh - 2, 0, w, scope_size)

            surface.SetDrawColor(255, 0, 0, 255)
            surface.DrawLine(x, y, x + 1, y + 1)

            -- scope
            surface.SetTexture(scope)
            surface.SetDrawColor(255, 255, 255, 255)

            surface.DrawTexturedRectRotated(x, y, scope_size, scope_size, 0)

        else
            return self.BaseClass.DrawHUD(self)
        end
    end

    function SWEP:AdjustMouseSensitivity()
        return (self:GetIronsights() and 0.2) or nil
    end
end
