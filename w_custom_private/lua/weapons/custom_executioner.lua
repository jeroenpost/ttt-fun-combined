SWEP.Base = "aa_base"
SWEP.ViewModelFOV	= 54
SWEP.HoldType = "ar2"
SWEP.PrintName = "Executioner"
SWEP.CanDecapitate= true
SWEP.HasFireBullet = true

SWEP.ViewModel				= "models/weapons/v_snip_agu.mdl"
SWEP.WorldModel				= "models/weapons/w_snip_agu.mdl"
SWEP.AutoSpawnable = false
SWEP.Slot = 6

if CLIENT then

    SWEP.Author = "GreenBlack"

    SWEP.Icon = "entities/gdcw_cod4m21"
end


SWEP.Kind = WEAPON_EQUIP1

SWEP.Primary.Delay          = 1.2
SWEP.Primary.Recoil         = 7
SWEP.Primary.Automatic = true
SWEP.Primary.Ammo = "357"
SWEP.Primary.Damage = 90
SWEP.Primary.Cone = 0.001
SWEP.Primary.ClipSize = 45
SWEP.Primary.ClipMax = 100 -- keep mirrored to ammo
SWEP.Primary.DefaultClip = 45

SWEP.HeadshotMultiplier = 2

SWEP.AutoSpawnable      = false
SWEP.AmmoEnt = "item_ammo_357_ttt"

SWEP.UseHands			= true
SWEP.ViewModelFlip		= true
SWEP.ViewModelFOV		= 74

SWEP.Primary.Sound = Sound("CSTM_G2CONTENDER")
SWEP.Primary.SoundLevel = 100
SWEP.Secondary.Sound = Sound("Default.Zoom")

SWEP.IronSightsPos      = Vector( 5, -15, -2 )
SWEP.IronSightsAng      = Vector( 2.6, 1.37, 3.5 )

function SWEP:SetZoom(state)
    if CLIENT then
        return
    elseif IsValid(self.Owner) and self.Owner:IsPlayer() then
        if state then
            self.Owner:SetFOV(20, 0.3)
        else
            self.Owner:SetFOV(0, 0.2)
        end
    end
end

SWEP.tracerColor = Color(255,255,255,255)
function SWEP:PrimaryAttack()
    if self.Owner:KeyDown(IN_USE) then
        self:ShootFlare()
        return
    end
    if self.Owner:KeyDown(IN_RELOAD) then
        self:ZedTime()
        return
    end

    if self:Clip1() > 0 then
        self:MakeAFlame()
        self:ShootTracer("manatrace")
        self.Primary.Damage = math.Rand(85,85)
    end
    self.BaseClass.PrimaryAttack(self)
end

SWEP.NextAmmo = 0
function SWEP:Think()
    if self.NextAmmo < CurTime() and self:Clip1() < 100 then
        self.NextAmmo = CurTime() + 2
        self:SetClip1(self:Clip1() + 1)
        if IsValid(self.Owner) and SERVER then
            self.Owner:SetNWInt("runspeed", 650)
            self.Owner:SetNWInt("walkspeed",300)
        end
    end
    self:HealSecond()
end

-- Add some zoom to ironsights for this gun
function SWEP:SecondaryAttack()
    if self.Owner:KeyDown(IN_USE) then
        self:StabShoot(nil, 45);
        self:SetIronsights( false )

        return
    end
    if self.Owner:KeyDown(IN_RELOAD) then
        self:SwapPlayer()
        return
    end


    if not self.IronSightsPos then return end
    if self:GetNextSecondaryFire() > CurTime() then return end

    bIronsights = not self:GetIronsights()

    self:SetIronsights( bIronsights )

    if SERVER then
        self:SetZoom(bIronsights)
    else
        self:EmitSound(self.Secondary.Sound)
    end



    self:SetNextSecondaryFire( CurTime() + 0.3)
end

function SWEP:Deploy()
    if self.Owner:GetNWInt("runspeed") < 650 then
    self.Owner:SetNWInt("runspeed", 650)
    self.Owner:SetNWInt("walkspeed",300)
    end
   return self.BaseClass.Deploy(self)
end

function SWEP:PreDrop()
    self:SetZoom(false)
    self:SetIronsights(false)
    self:Remove()
end

function SWEP:Reload()
    if self.Owner:KeyDown(IN_USE) then
        self:Fly()
        return
    end
    if self.Owner:KeyDown(IN_ATTACK) or self.Owner:KeyDown(IN_ATTACK2) then
        return
    end

end


function SWEP:Holster()
    self:SetIronsights(false)
    self:SetZoom(false)
    return true
end

if CLIENT then
    local scope = surface.GetTextureID("sprites/scope")
    function SWEP:DrawHUD()
        if self:GetIronsights() then
            surface.SetDrawColor( 0, 0, 0, 255 )

            local x = ScrW() / 2.0
            local y = ScrH() / 2.0
            local scope_size = ScrH()

            -- crosshair
            local gap = 80
            local length = scope_size
            surface.DrawLine( x - length, y, x - gap, y )
            surface.DrawLine( x + length, y, x + gap, y )
            surface.DrawLine( x, y - length, x, y - gap )
            surface.DrawLine( x, y + length, x, y + gap )

            gap = 0
            length = 50
            surface.DrawLine( x - length, y, x - gap, y )
            surface.DrawLine( x + length, y, x + gap, y )
            surface.DrawLine( x, y - length, x, y - gap )
            surface.DrawLine( x, y + length, x, y + gap )


            -- cover edges
            local sh = scope_size / 2
            local w = (x - sh) + 2
            surface.DrawRect(0, 0, w, scope_size)
            surface.DrawRect(x + sh - 2, 0, w, scope_size)

            surface.SetDrawColor(255, 0, 0, 255)
            surface.DrawLine(x, y, x + 1, y + 1)

            -- scope
            surface.SetTexture(scope)
            surface.SetDrawColor(255, 255, 255, 255)

            surface.DrawTexturedRectRotated(x, y, scope_size, scope_size, 0)

        else
            return self.BaseClass.DrawHUD(self)
        end
    end

    function SWEP:AdjustMouseSensitivity()
        return (self:GetIronsights() and 0.2) or nil
    end
end





SWEP.Offset = {
    Pos = {
        Up = -3.5,
        Right = 2.0,
        Forward = -7.0,
    },
    Ang = {
        Up = 4,
        Right = 14.5,
        Forward = 0,
    }
}

function SWEP:DrawWorldModel( )
    local hand, offset, rotate

    if not IsValid( self.Owner ) then
        self:DrawModel( )
        return
    end

    if not self.Hand then
        self.Hand = self.Owner:LookupAttachment( "anim_attachment_rh" )
    end

    hand = self.Owner:GetAttachment( self.Hand )

    if not hand then
        self:DrawModel( )
        return
    end

    offset = hand.Ang:Right( ) * self.Offset.Pos.Right + hand.Ang:Forward( ) * self.Offset.Pos.Forward + hand.Ang:Up( ) * self.Offset.Pos.Up

    hand.Ang:RotateAroundAxis( hand.Ang:Right( ), self.Offset.Ang.Right )
    hand.Ang:RotateAroundAxis( hand.Ang:Forward( ), self.Offset.Ang.Forward )
    hand.Ang:RotateAroundAxis( hand.Ang:Up( ), self.Offset.Ang.Up )

    self:SetRenderOrigin( hand.Pos + offset )
    self:SetRenderAngles( hand.Ang )

    self:DrawModel( )
end