if SERVER then
   AddCSLuaFile(  )
end
   
SWEP.HoldType = "duel"
SWEP.DrawCrosshair      = true
 SWEP.PrintName = "Outlaws"
SWEP.Slot = 6
if CLIENT then
   
   
   SWEP.Author = "GreenBlack"
 
   SWEP.Icon = "vgui/ttt_fun_killicons/elites.png"
end
 
SWEP.Base = "gb_base"

SWEP.Kind = WEAPON_EQUIP1
SWEP.Base = "weapon_tttbase"
SWEP.Primary.Recoil     = 0.05
SWEP.Primary.Damage = 29
SWEP.Primary.Delay = 0.30
SWEP.Primary.Cone = 0.001
SWEP.Primary.ClipSize = 60
SWEP.Primary.Automatic = true
SWEP.Primary.DefaultClip = 60
SWEP.Primary.ClipMax = 120
SWEP.Primary.Ammo = "Pistol"

SWEP.Secondary.Delay = 0.30
SWEP.Secondary.ClipSize     = 40
SWEP.Secondary.DefaultClip  = 40
SWEP.Secondary.Automatic    = true
SWEP.Secondary.Ammo         = "Pistol"
SWEP.Secondary.ClipMax      = 60

SWEP.AutoSpawnable = false
SWEP.AmmoEnt = "item_ammo_pistol_ttt"
SWEP.NoSights = true

SWEP.ViewModel  = "models/weapons/v_pist_elite.mdl"
SWEP.WorldModel = "models/weapons/w_pist_elite.mdl"

SWEP.Primary.Sound = Sound( "Weapon_Elite.Single" )
SWEP.Secondary.Sound = Sound( "Weapon_Elite.Single" )
SWEP.HeadshotMultiplier = 1


function SWEP:Think()

	if self.Owner:KeyPressed(IN_ATTACK)  then
	-- When the left click is pressed, then
		self.ViewModelFlip = true
	end

	if self.Owner:KeyPressed(IN_ATTACK2)  then
	-- When the right click is pressed, then
		self.ViewModelFlip = false
	end
end

function SWEP:PrimaryAttack( worldsnd )
 
   --self.Weapon:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
   self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )

   if not self:CanPrimaryAttack() then return end

   if not worldsnd then
      self.Weapon:EmitSound( self.Primary.Sound, self.Primary.SoundLevel )
   elseif SERVER then
      sound.Play(self.Primary.Sound, self:GetPos(), self.Primary.SoundLevel)
   end

   self:ShootBullet( self.Primary.Damage, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )

   self:TakePrimaryAmmo( 1 )

   local owner = self.Owner
   if not IsValid(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

 --  owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )
end

function SWEP:CanSecondaryAttack()
   if not IsValid(self.Owner) then return end

   if self.Weapon:Clip1() <= 0 then
      self:DryFire(self.SetNextSecondaryFire)
      return false
   end
   return true
end

function SWEP:SecondaryAttack(worldsnd)
       

   self.Weapon:SetNextSecondaryFire( CurTime() + self.Secondary.Delay )
   --self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )

   if not self:CanSecondaryAttack() then return end

   if not worldsnd then
      self.Weapon:EmitSound( self.Primary.Sound, self.Primary.SoundLevel )
   elseif SERVER then
      sound.Play(self.Primary.Sound, self:GetPos(), self.Primary.SoundLevel)
   end

   self:ShootBullet( self.Primary.Damage, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )

   self:TakePrimaryAmmo( 1 )

   local owner = self.Owner
   if not IsValid(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

  -- owner:ViewPunch( Angle( math.Rand(-0.6,-0.6) * self.Primary.Recoil, math.Rand(-0.6,0.6) *self.Primary.Recoil, 0 ) )
              
end

SWEP.hadBomb = false

function SWEP:Reload()
    if self:GetNWBool( "Planted" ) and not self.JustSploded then
        self:BombSplode()
    elseif not self.hadBomb then
        self:StickIt()
     end;

end


SWEP.Planted = false;

local hidden = true;
local close = false;
local lastclose = false;

if CLIENT then
    function SWEP:DrawHUD( )
        if not self.hadBomb then
        local planted = self:GetNWBool( "Planted" );
        local tr = LocalPlayer( ):GetEyeTrace( );
        local close2;

        if tr.Entity:IsPlayer( ) and LocalPlayer( ):GetPos( ):Distance( tr.Entity:GetPos( ) ) <= 99999999 then
            close2 = true;
        else
            close2 = false;
        end;

        local planted_text = "Not Planted!";
        local close_text = "Nobody is in range!";

        if planted then
            if hidden == true then
                hidden = false
            end
            planted_text = "Planted!\nReload to Explode!";

            surface.SetFont( "ChatFont" );
            local size_x, size_y = surface.GetTextSize( planted_text );

            draw.RoundedBox( 6, ScrW( ) / 2 - size_x / 2 - 5, ScrH( ) - 100, size_x + 15, size_y + 10, Color( 0, 0, 0, 150 ) );
            draw.DrawText( planted_text, "ChatFont", ScrW( ) / 2, ScrH( ) - 100 + 5, Color( 255, 30, 30, 255 ), TEXT_ALIGN_CENTER );
        else
            if close2 then
                close_text = "Close Enough!\nReload to stick!!";
            end;

            surface.SetFont( "ChatFont" );
            local size_x, size_y = surface.GetTextSize( planted_text );

            surface.SetFont( "ChatFont" );
            local size_x2, size_y2 = surface.GetTextSize( close_text );

            draw.RoundedBox( 6, ScrW( ) / 2 - ( size_x2 / 2 ) - 7.5, ScrH( ) - 100, size_x2 + 20, size_y + size_y2 + 10, Color( 0, 0, 0, 150 ) );
            draw.DrawText( planted_text, "ChatFont", ScrW( ) / 2, ScrH( ) - 100 + 5, Color( 30, 30, 255, 255 ), TEXT_ALIGN_CENTER );
            draw.DrawText( close_text, "ChatFont", ScrW( ) / 2 , ScrH( ) - 100 + size_y + 5, Color( 255, 255, 255, 255 ), TEXT_ALIGN_CENTER );
        end;
    end;
        end;
end;




SWEP.ClipSet = false

function SWEP:Think( )
    local ply = self.Owner;
    if not IsValid( ply ) or self.hadBomb then return; end;


    local tr = ply:GetEyeTrace( );

    if tr.Entity:IsPlayer( ) and ply:GetPos( ):Distance( tr.Entity:GetPos( ) ) <= 99999999 and GetRoundState() == ROUND_ACTIVE then
        close = true;
    else
        close = false;
    end;

    if self:GetNWBool( "Planted" ) == false then
        if lastclose == false and close then
            --  self.Weapon:SendWeaponAnim( ACT_SLAM_STICKWALL_ATTACH );
        elseif lastclose == true and not close then
            --   self.Weapon:SendWeaponAnim( ACT_SLAM_STICKWALL_IDLE );
        end;
    end;

    lastclose = close;

    if not hidden then
        if close and not self:GetNWBool( "Planted" ) then
            --    self.Weapon:SendWeaponAnim( ACT_SLAM_STICKWALL_ATTACH );
        elseif not close and not self:GetNWBool( "Planted" ) then
            --    self.Weapon:SendWeaponAnim( ACT_SLAM_STICKWALL_IDLE );
        elseif self:GetNWBool( "Planted" ) then
            --   self.Weapon:SendWeaponAnim( ACT_SLAM_DETONATOR_DRAW );
        end;
    end;
    if SERVER and not hidden and IsValid(self.Owner) and self.Owner:GetActiveWeapon() == self.Weapon then
        -- self.Owner:DrawViewModel(false)
        -- self.Owner:DrawWorldModel(false)
        hidden = true
    end
end;

function SWEP:StickIt()
    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
    self.Weapon:SetNextSecondaryFire( CurTime() + self.Secondary.Delay )

    local ply = self.Owner;
    if not IsValid( ply ) then return; end;

    local tr = ply:GetEyeTrace(MASK_SHOT);

    if not self:GetNWBool( "Planted" ) then
        if close then
            if SERVER then
                self.PlantedPly = tr.Entity
                self:SetNWBool("Planted", true)
            end;
            --   self.Weapon:SendWeaponAnim( ACT_SLAM_STICKWALL_ATTACH2 );
        end;
        if hidden == true and self:GetNWBool( "Planted" ) == true then
            hidden = false
        end
    end;
end


function SWEP:DoSplode( owner, plantedply, bool )
    if not bool then
        self:SetNWBool( "Planted", false )
        self.hadBomb = true
        timer.Simple( 1, function( ) plantedply:EmitSound( self.BeepSound ); end );
        timer.Simple( 1.5, function( ) plantedply:EmitSound( self.BeepSound ); end );
        timer.Simple( 2, function( ) plantedply:EmitSound( self.BeepSound ); end );
        timer.Simple( 2.5, function( )
            local effectdata = EffectData()
            effectdata:SetOrigin(plantedply:GetPos())
            util.Effect("HelicopterMegaBomb", effectdata)

            plantedply.PlayerBombDie = true;

            local explosion = ents.Create("env_explosion")
            explosion:SetOwner(self.Owner)
            explosion:SetPos(plantedply:GetPos( ) + Vector( 0, 0, 10 ))
            explosion:SetKeyValue("iMagnitude", "75")
            explosion:SetKeyValue("iRadiusOverride", 0)
            explosion:Spawn()
            explosion:Activate()
            explosion:Fire("Explode", "", 0)
            explosion:Fire("kill", "", 0)


        end );
    else
        self:SetNWBool( "Planted", false )
        timer.Simple( 1, function( ) plantedply:EmitSound( self.BeepSound ); end );
        timer.Simple( 1.5, function( ) plantedply:EmitSound( self.BeepSound ); end );
        timer.Simple( 2, function( ) plantedply:EmitSound( self.BeepSound ); end );
        timer.Simple( 2.5, function( )
            local effectdata = EffectData()
            effectdata:SetOrigin(plantedply:GetPos())
            util.Effect("HelicopterMegaBomb", effectdata)

            local explosion = ents.Create("env_explosion")
            explosion:SetOwner(self.Owner)
            explosion:SetPos(plantedply:GetPos( ))
            explosion:SetKeyValue("iMagnitude", "45")
            explosion:SetKeyValue("iRadiusOverride", 0)
            explosion:Spawn()
            explosion:Activate()
            explosion:Fire("Explode", "", 0)
            explosion:Fire("kill", "", 0)

            hidden = false;
            close = false;
            lastclose = false;
            self:SetNWBool( "Planted", false )

            plantedply:Remove( );

        end );
    end;
end;

function SWEP:BombSplode()
    local ply = self.Owner;
    if not IsValid( ply ) then return; end;

    if IsValid( self.PlantedPly ) and self.PlantedPly:Alive( ) then
        if SERVER then
            self:DoSplode( self.Owner, self.PlantedPly, false );
            self.JustSploded = true;
        end;
        --  self.Weapon:SendWeaponAnim( ACT_SLAM_DETONATOR_DETONATE );
    else
        if SERVER then
            if IsValid( self.PlantedPly ) and IsValid( self.PlantedPly.server_ragdoll ) then
                self:DoSplode( self.Owner, self.PlantedPly.server_ragdoll, true );
                self.JustSploded = true;
            else
                ply:PrintMessage( HUD_PRINTTALK, "The body this bomb has been planted to is no longer available!" );
            end;
        end;
        --  self.Weapon:SendWeaponAnim( ACT_SLAM_DETONATOR_DETONATE );
    end;
end;
