SWEP.Base = "gb_camo_base"
SWEP.ViewModelFOV	= 54
SWEP.HoldType = "pistol"
SWEP.PrintName = "Faded Sidearm"
SWEP.ViewModel		= "models/weapons/c_357.mdl"
SWEP.WorldModel		= "models/weapons/w_357.mdl"
SWEP.Kind = WEAPON_NADE
SWEP.Slot = 3
SWEP.AutoSpawnable = false
SWEP.Primary.Damage		    = 75
SWEP.Primary.Delay		    = 0.7
SWEP.Primary.Sound          = Sound("weapons/357/357_fire2.wav")
SWEP.HeadshotMultiplier = 1.5
SWEP.Primary.Cone	        = 0.001

SWEP.Primary.ClipSize		= 35
SWEP.Primary.DefaultClip	= 35
SWEP.Primary.Automatic		= false
SWEP.Primary.Ammo			= "AlyxGun"
SWEP.AmmoEnt = "item_ammo_revolver_ttt"
SWEP.Primary.Sound          = Sound("weapons/357/357_fire2.wav")


SWEP.IronSightsPos = Vector( -4.6, -3, 0.65 )
SWEP.IronSightsAng = Vector( 0, 0, 0.00 )
SWEP.Camo = 46

function SWEP:Initialize()
    self:SetNWString("shootmode", "pistol")
    self.BaseClass.Initialize(self)
end

function SWEP:OnDrop()
    self:Remove()
end

SWEP.NextReload = 0
function SWEP:Reload()
    if self.Owner:KeyDown(IN_USE) and self.NextReload < CurTime() then
        self.NextReload = CurTime() + 0.3
        local mode = self:GetNWString("shootmode")
        self:SetIronsights(false)
        self:SetZoom(false)
        if mode == "pistol" then
            self:SetNWString("shootmode","sniper")
            self.Primary.Damage		    = 85
            self.Primary.Delay		    = 1.5
            self.Primary.Sound = "weapons/crossbow/fire1.wav"
         end
        if mode == "sniper" then
            self:SetNWString("shootmode","pistol")
            self.Primary.Damage		    = 60
            self.Primary.Delay		    = 0.65
            self.Primary.Sound = Sound("weapons/357/357_fire2.wav")
        end
        return
    end
    if self.NextReload < CurTime() then
    self.BaseClass.Reload(self)
    end
end

function SWEP:PreDrop()
    self:SetZoom(false)
    self:SetIronsights(false)
    return self.BaseClass.PreDrop(self)
end

SWEP.IronSightsPos      = Vector( -100, -100, -100 )
SWEP.IronSightsAng      = Vector( 2.6, 1.37, 3.5 )
SWEP.Secondary.Sound = Sound("Default.Zoom")
function SWEP:SecondaryAttack()

    if self.Owner:KeyDown(IN_USE) then
        self:Jihad()
        return
    end

    local mode = self:GetNWString("shootmode")
    if mode == "sniper" then
        if not self.IronSightsPos then return end
        if self.Weapon:GetNextSecondaryFire() > CurTime() then return end

        local bIronsights = not self:GetIronsights()

        self:SetIronsights( bIronsights )

        if SERVER then
            self:SetZoom(bIronsights)
        else
            self:EmitSound(self.Secondary.Sound)
        end

        self.Weapon:SetNextSecondaryFire( CurTime() + 0.3)
        return
    end


    self:Fly()
end


function SWEP:SetZoom(state)
    if CLIENT then
        return
    elseif IsValid(self.Owner) and self.Owner:IsPlayer() then
        if state then
            self.Owner:SetFOV(30, 0.3)
        else
            self.Owner:SetFOV(0, 0.2)
        end
    end
end


if CLIENT then
    function SWEP:DrawHUD( )
        local shotttext = "Shootmode: "..self:GetNWString("shootmode").."\nE+R to change"
        surface.SetFont( "ChatFont" );
        local size_x, size_y = surface.GetTextSize( shotttext );

        draw.RoundedBox( 6, ScrW( ) / 2 - size_x / 2 - 5, ScrH( ) - 100, size_x + 15, size_y + 10, Color( 0, 0, 0, 150 ) );
        draw.DrawText( shotttext , "ChatFont", ScrW( ) / 2, ScrH( ) - 100 + 5, Color( 255, 30, 30, 255 ), TEXT_ALIGN_CENTER );

        local scope = surface.GetTextureID("sprites/scope")
        if self:GetIronsights() then
            surface.SetDrawColor( 0, 0, 0, 255 )

            local x = ScrW() / 2.0
            local y = ScrH() / 2.0
            local scope_size = ScrH()

            -- crosshair
            local gap = 80
            local length = scope_size
            surface.DrawLine( x - length, y, x - gap, y )
            surface.DrawLine( x + length, y, x + gap, y )
            surface.DrawLine( x, y - length, x, y - gap )
            surface.DrawLine( x, y + length, x, y + gap )

            gap = 0
            length = 50
            surface.DrawLine( x - length, y, x - gap, y )
            surface.DrawLine( x + length, y, x + gap, y )
            surface.DrawLine( x, y - length, x, y - gap )
            surface.DrawLine( x, y + length, x, y + gap )


            -- cover edges
            local sh = scope_size / 2
            local w = (x - sh) + 2
            surface.DrawRect(0, 0, w, scope_size)
            surface.DrawRect(x + sh - 2, 0, w, scope_size)

            surface.SetDrawColor(255, 0, 0, 255)
            surface.DrawLine(x, y, x + 1, y + 1)

            -- scope
            surface.SetTexture(scope)
            surface.SetDrawColor(255, 255, 255, 255)

            surface.DrawTexturedRectRotated(x, y, scope_size, scope_size, 0)

        else
            return self.BaseClass.DrawHUD(self)
        end
    end

    function SWEP:AdjustMouseSensitivity()
        return (self:GetIronsights() and 0.2) or nil
    end
end;
