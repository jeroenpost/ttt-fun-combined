SWEP.HoldType = "melee"
SWEP.ViewModelFOV = 65
SWEP.ViewModelFlip = false
SWEP.ViewModel = "models/weapons/v_crowbar.mdl"
SWEP.WorldModel = "models/weapons/w_crowbar.mdl"
SWEP.ShowViewModel = true
SWEP.ShowWorldModel = true
SWEP.Kind = WEAPON_EQUIP
SWEP.ViewModelBoneMods = {
	["ValveBiped.Bip01"] = { scale = Vector(1, 1, 1), pos = Vector(-6.985, -0.953, 1.587), angle = Angle(3.809, 0, 0) }
}
SWEP.VElements = {
	["thrusterholder"] = { type = "Model", model = "models/Items/combine_rifle_ammo01.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "backthruster", pos = Vector(3.181, 0, 0), angle = Angle(180, 90, -90), size = Vector(0.833, 0.833, 0.833), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["backthruster"] = { type = "Model", model = "models/weapons/w_missile.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "head", pos = Vector(0, 0, 4.091), angle = Angle(-90, 0, 0), size = Vector(0.776, 0.776, 0.776), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["handle"] = { type = "Model", model = "models/props_c17/GasPipes006a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(7.099, 1.299, 5.908), angle = Angle(-5.114, 180, -178.978), size = Vector(1.003, 1.003, 0.947), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["engine"] = { type = "Model", model = "models/props_c17/TrapPropeller_Engine.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "head", pos = Vector(-0.456, 4.091, 2.273), angle = Angle(-90, 180, 90), size = Vector(0.209, 0.209, 0.209), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["head+"] = { type = "Model", model = "models/props_junk/CinderBlock01a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "handle", pos = Vector(4.092, 0, 25), angle = Angle(90, 90, 90), size = Vector(0.662, 0.662, 0.662), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["warhead"] = { type = "Model", model = "models/weapons/w_missile_closed.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "head", pos = Vector(0, 0, -5.909), angle = Angle(-90, 0, 0), size = Vector(1.174, 1.174, 1.174), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["head"] = { type = "Model", model = "models/props_junk/CinderBlock01a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "handle", pos = Vector(4.091, 0, 25), angle = Angle(0, 90, 90), size = Vector(0.662, 0.662, 0.662), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
}
SWEP.WElements = {
	["backthruster"] = { type = "Model", model = "models/weapons/w_missile.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "head", pos = Vector(0, 0, 4.091), angle = Angle(-90, 0, 0), size = Vector(0.776, 0.776, 0.776), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["topper"] = { type = "Model", model = "models/props_c17/oildrum001.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "loweredge", pos = Vector(0, 0, 0), angle = Angle(0, 0, 0), size = Vector(0.129, 0.129, 0.009), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["warhead"] = { type = "Model", model = "models/weapons/w_missile_closed.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "head", pos = Vector(0, 0, -2.274), angle = Angle(-90, 0, 0), size = Vector(1.003, 1.003, 1.003), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["head"] = { type = "Model", model = "models/props_junk/CinderBlock01a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "handle", pos = Vector(4.091, 0, 18), angle = Angle(0, 90, 90), size = Vector(0.662, 0.662, 0.662), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["head+"] = { type = "Model", model = "models/props_junk/CinderBlock01a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "handle", pos = Vector(4.092, 0, 18), angle = Angle(90, 90, 90), size = Vector(0.662, 0.662, 0.662), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["engine"] = { type = "Model", model = "models/props_c17/TrapPropeller_Engine.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "head", pos = Vector(0, 4.091, 0.455), angle = Angle(-90, 180, 90), size = Vector(0.209, 0.209, 0.209), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["thrusterholder"] = { type = "Model", model = "models/Items/combine_rifle_ammo01.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "backthruster", pos = Vector(3.181, 0, 0), angle = Angle(180, 90, -90), size = Vector(0.833, 0.833, 0.833), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["handle"] = { type = "Model", model = "models/props_c17/GasPipes006a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(7.4, 0.699, -7.901), angle = Angle(174, 17.385, 3.068), size = Vector(1.003, 1.003, 0.72), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["loweredge"] = { type = "Model", model = "models/props_trainstation/trainstation_post001.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "handle", pos = Vector(4.599, 0, -13.183), angle = Angle(0, 0, 180), size = Vector(0.72, 0.72, 0.151), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
}
SWEP.Purpose = "TO SMASH EVERYTHING WITH A SLEDGEHAMMER HOLDING AN EXPLOSIVE WARHEAD."
SWEP.AutoSwitchTo = true
SWEP.Contact = ""
SWEP.Author = "Spastik/SAGA_"
SWEP.FiresUnderwater = true
SWEP.Spawnable = true
SWEP.AdminSpawnable = true
SWEP.SlotPos = 0
SWEP.Slot = 6
SWEP.Instructions = "SWING TO SMASH THINGS. SECONDARY TO THROW A ROCKET FROM A GOD DAMN SLEDGEHAMMER."
SWEP.AutoSwitchFrom = false
SWEP.Base = "aa_base"
SWEP.Category = "S&G Munitions"
SWEP.DrawAmmo = false
SWEP.PrintName = "Ragnarök"
SWEP.Primary.Round 			= ("shot_nade")	--NAME OF ENTITY GOES HERE

SWEP.Primary.Damage = 15
SWEP.Primary.TakeAmmo = 0
SWEP.Primary.ClipSize = 1
SWEP.Primary.Ammo = "RPG_round"
SWEP.Primary.DefaultClip = 13
SWEP.Primary.Spread = 1.2
SWEP.Primary.NumberofShots = 1
SWEP.Primary.Automatic = false
SWEP.Primary.Recoil = 1
SWEP.Primary.Delay = 0.2
SWEP.Primary.Force = 40

SWEP.Secondary.Delay			= 0.8
SWEP.Secondary.Recoil			= 0
SWEP.Secondary.Damage			= 0
SWEP.Secondary.NumShots			= 0
SWEP.Secondary.Cone		  		= 0
SWEP.Secondary.ClipSize			= 1
SWEP.Secondary.DefaultClip		= 10
SWEP.Secondary.Automatic   		= false
SWEP.Secondary.Ammo         	= "RPG_round"
SWEP.Secondary.Round 			= ("ent_rocketshot_spastik")	--NAME OF ENTITY GOES HERE


function SWEP:PrimaryAttack()
    if self.Owner:KeyDown(IN_USE) then
        self.Weapon:EmitSound("weapons/shortsword/morrowind_shortsword_slash.mp3")
        self.Weapon:SetNextPrimaryFire(CurTime() + 1)
        self.Weapon:SetNextSecondaryFire(CurTime() + 1)
        self.Weapon:SendWeaponAnim(ACT_VM_HITCENTER)
        if !IsValid(self.Owner) then return end
        if (SERVER) then
            timer.Simple(.25, function()
                if !IsValid(self.Owner) then return end
                local knife = ents.Create("ent_mor_shortsword")
                knife:SetAngles(self.Owner:EyeAngles())
                local pos = self.Owner:GetShootPos()
                pos = pos + self.Owner:GetForward() * 5
                pos = pos + self.Owner:GetRight() * 9
                pos = pos + self.Owner:GetUp() * -5
                knife:SetPos(pos)
                knife:SetOwner(self.Owner)
                knife.Weapon = self		// Used to se the axe's model and the weapon it gives when used.
                knife.damage = 150
                knife:Spawn()
                knife:Activate()

                self.Owner:SetAnimation(PLAYER_ATTACK1)

                local phys = knife:GetPhysicsObject()
                phys:SetVelocity(self.Owner:GetAimVector() * 1500)
                phys:AddAngleVelocity(Vector(0, 500, 0))
                self:Remove()
            end)
        end
        return

    end

    local spos = self.Owner:GetShootPos()
    local sdest = spos + (self.Owner:GetAimVector() * 120)

    local kmins = Vector(1,1,1) * -10
    local kmaxs = Vector(1,1,1) * 10

    local tr = util.TraceHull({start=spos, endpos=sdest, filter=self.Owner, mask=MASK_SHOT_HULL, mins=kmins, maxs=kmaxs})

    -- Hull might hit environment stuff that line does not hit
    if not IsValid(tr.Entity) then
        tr = util.TraceLine({start=spos, endpos=sdest, filter=self.Owner, mask=MASK_SHOT_HULL})
    end

    local hitEnt = tr.Entity

         self.Weapon:SetNextPrimaryFire(CurTime() + self.Primary.Delay)
         local trace = self.Owner:GetEyeTrace()
         if IsValid(hitEnt) and hitEnt:GetPos():Distance(self.Owner:GetShootPos()) <= 120 then
		    if SERVER then
                hitEnt:TakeDamage( 55,self.Owner or self,self )
			end
		    self.Weapon:EmitSound("physics/concrete/concrete_block_impact_hard"..math.random(1,3)..".wav",75,math.random(500,110,1,-1))
		    self.Weapon:EmitSound("physics/body/body_medium_impact_hard"..math.random(1,6)..".wav",75,math.random(500,100,1, 1))

		   else
	        self.Weapon:EmitSound("npc/zombie/claw_miss1.wav")
	        self.Weapon:EmitSound("npc/vort/claw_swing2.wav")
         end
		 self.Owner:ViewPunch( Angle( 0,5,-6 ) )
		 self.Weapon:SendWeaponAnim( ACT_VM_MISSCENTER )
         self.Owner:SetAnimation( PLAYER_ATTACK1 )

end

-- GOOMBA
function SWEP:TrickStomp( soundfile )
    self.Owner.doestrickstomp = CurTime() + 5
    hook.Add("GetFallDamage","trickstomp",function(ply)
            if ply.doestrickstomp and ply.doestrickstomp > CurTime() then
                local ef = EffectData()
                ef:SetOrigin(ply:GetPos()+Vector(0,0,40))
                util.Effect("undead_explosion", ef,true,true)
                util.ScreenShake( ply:GetPos()+Vector(0,0,40), math.random(3,6), math.random(3,4), math.random(2,3), 110 )
                ply:EmitSound("ambient/explosions/explode_"..math.random(7,9)..".wav",90,math.random(80,110))
            end
    end)

    if self.nextFly > CurTime() or CLIENT then return end
    self.nextFly = CurTime() + 0.30
    if self.flyammo < 1 then return end

    self.flyammo = self.flyammo - 1
    self.Owner:SetAnimation(PLAYER_ATTACK1)
    local power = 600

    if SERVER then self.Owner:SetVelocity(self.Owner:GetUp() * power + Vector(0, 0, power)) end
    timer.Simple(20, function() if IsValid(self) then self.flyammo = self.flyammo + 1 end end)
    --timer.Simple(0.2,self.SecondaryAttackDelay,self)
    timer.Create(self.Owner:SteamID().."jumpstomp",2,1,function()
        if IsValid(self) and IsValid(self.Owner) then
            if SERVER then self.Owner:SetVelocity(self.Owner:GetUp() * -power + Vector(0, 0, -power)) end
        end
    end)
end

SWEP.flyammo = 5
SWEP.nextFly = 0

function SWEP:Reload()
    if self.Owner:KeyDown(IN_USE) then

        self:TrickStomp()
    end
         self.NextReload = self.NextReload or 0
         if ( self.NextReload > CurTime() ) then return end
         if ( self:Clip1() ~= self.Primary.ClipSize and self:Ammo1() ~= 0 ) then
		      self:SendWeaponAnim( ACT_VM_HOLSTER )
			  self:SetNextPrimaryFire( CurTime() + 1.25 )
			  self:SetNextSecondaryFire( CurTime() + 1.25 )
			  self.NextReload = CurTime() + 1.25
			  timer.Simple( 1.2,function()
			        if ( IsValid( self ) ) then
                         self:DefaultReload( ACT_VM_DRAW )
						 self:SetClip1( 1 )
						 if ( IsValid( self.Owner ) ) then
						      self.Owner:RemoveAmmo( 1,self.Primary.Ammo )
						 end
					end
			  end )
		 end
end

function SWEP:SecondaryAttack()
	if self:Clip1() <= 0 then return end
	self.Weapon:SetNextSecondaryFire( CurTime() + self.Secondary.Delay )
	self:FireFrag()
	self:SetClip1( self:Clip1() -1 )
	self:SendWeaponAnim(ACT_VM_MISSCENTER)
	self.Owner:SetAnimation( PLAYER_ATTACK1 )
	self.Weapon:EmitSound( "weapons/rpg/rocketfire1.wav" , 500, 80, 1, 0)
	self.Weapon:SetNWFloat( "LastShootTime", CurTime() )
end

function SWEP:FireFrag()
	local aim = self.Owner:GetAimVector()
	local side = aim:Cross(Vector(0,0,1))
	local up = side:Cross(aim)
	local pos = self.Owner:GetShootPos() + side * 10 + up * 0


	if SERVER then
	local Frag = ents.Create(self.Secondary.Round)
	if !Frag:IsValid() then return false end
	Frag:SetAngles(self.Owner:GetAimVector():Angle(90,90,0))
	Frag:SetPos(pos)
	Frag:SetOwner(self.Owner)
	Frag:Spawn()
    Frag.damage = 40
	Frag.Owner = self.Owner
	Frag:Activate()
	eyes = self.Owner:EyeAngles()
		local phys = Frag:GetPhysicsObject()
			phys:SetVelocity(self.Owner:GetAimVector() * 1500)
	end
		if SERVER and !self.Owner:IsNPC() then
		local anglo = Angle(-3, 0, 0)		
		self.Owner:ViewPunch(anglo)
		end

end

function SWEP:CheckWeaponsAndAmmo()
	if SERVER and self.Weapon != nil then 
		timer.Simple(.01, function() if not IsValid(self) then return end 
			if not IsValid(self.Owner) then return end
			self.Owner:StripWeapon(self.Gun)
		end)	
	end
end

local inputColor = {255, 0, 0}
function SWEP:randomColor(inputCol)
    speed = 0.25

    -- Red to Green
    if inputCol[1] == 255 and inputCol[3] == 0 and inputCol[2] != 255 then
    inputCol[2] = inputCol[2] + speed
    elseif inputCol[2] == 255 and inputCol[1] != 0 then
    inputCol[1] = inputCol[1] - speed

        -- Green to Blue
    elseif inputCol[2] == 255 and inputCol[3] != 255 then
    inputCol[3] = inputCol[3] + speed
    elseif inputCol[3] == 255 and inputCol[2] != 0 then
    inputCol[2] = inputCol[2] - speed

        -- Blue to Red
    elseif inputCol[3] == 255 and inputCol[1] != 255 then
    inputCol[1] = inputCol[1] + speed
    elseif inputCol[1] == 255 and inputCol[3] != 0 then
    inputCol[3] = inputCol[3] - speed
    end

    return Color( inputCol[1], inputCol[2], inputCol[3], 0)
end

function SWEP:PreDrawViewModel()
    if SERVER or not IsValid(self.Owner) or not IsValid(self.Owner:GetViewModel()) then return end
    local colors = self:randomColor(inputColor)
    self.Owner:GetViewModel():SetColor( colors )
    self.Owner:GetViewModel():SetMaterial("models/shiny")
end

function SWEP:Think() -- BFG here; this part is ripped off from Awcmon's Sci Fi but hey don't blame me the code is 2simple4u

	     if ( self:Clip1() == 1 ) then
		      self.VElements["warhead"].color = Color( 255,255,255,255 )
			  self.VElements["backthruster"].color = Color( 255,255,255,255 )
			  
			  self.WElements["warhead"].color = Color( 255,255,255,255 )
			  self.WElements["backthruster"].color = Color( 255,255,255,255 )
			 else
			  self.VElements["warhead"].color = Color( 255,255,255,0 )
			  self.VElements["backthruster"].color = Color( 255,255,255,0 )
			  
			  self.WElements["warhead"].color = Color( 255,255,255,0 )
			  self.WElements["backthruster"].color = Color( 255,255,255,0 )
		 end


end



function SWEP:Initialize()

	// other initialize code goes here

	if CLIENT then

		self:SetHoldType( self.HoldType or "pistol" )
		// Create a new table for every weapon instance
		self.VElements = table.FullCopy( self.VElements )
		self.WElements = table.FullCopy( self.WElements )
		self.ViewModelBoneMods = table.FullCopy( self.ViewModelBoneMods )

		self:CreateModels(self.VElements) // create viewmodels
		self:CreateModels(self.WElements) // create worldmodels
		
		// init view model bone build function
		if IsValid(self.Owner) then
			local vm = self.Owner:GetViewModel()
			if IsValid(vm) then
				self:ResetBonePositions(vm)
				
				--[[// Init viewmodel visibility
				if (self.ShowViewModel == nil or self.ShowViewModel) then
					vm:SetColor(Color(255,255,255,255))
				else
					// we set the alpha to 1 instead of 0 because else ViewModelDrawn stops being called
					vm:SetColor(Color(255,255,255,1))
					// ^ stopped working in GMod 13 because you have to do Entity:SetRenderMode(1) for translucency to kick in
					// however for some reason the view model resets to render mode 0 every frame so we just apply a debug material to prevent it from drawing
					vm:SetMaterial("Debug/hsv")			
				end]]--
			end
		end
	end
end
function SWEP:Holster()
	
	if CLIENT and IsValid(self.Owner) then
		local vm = self.Owner:GetViewModel()
		if IsValid(vm) then
			self:ResetBonePositions(vm)
		end
	end
	
		//CSoundPatch:Stop( ambient/levels/canals/generator_ambience_loop1.wav )

	return true
end

function SWEP:OnRemove()
	self:Holster()
end

if CLIENT then

	SWEP.vRenderOrder = nil
	function SWEP:ViewModelDrawn()
		
		local vm = self.Owner:GetViewModel()
		if !IsValid(vm) then return end
		
		if (!self.VElements) then return end
		
		self:UpdateBonePositions(vm)

		if (!self.vRenderOrder) then
			
			// we build a render order because sprites need to be drawn after models
			self.vRenderOrder = {}

			for k, v in pairs( self.VElements ) do
				if (v.type == "Model") then
					table.insert(self.vRenderOrder, 1, k)
				elseif (v.type == "Sprite" or v.type == "Quad") then
					table.insert(self.vRenderOrder, k)
				end
			end
			
		end

		for k, name in ipairs( self.vRenderOrder ) do
		
			local v = self.VElements[name]
			if (!v) then self.vRenderOrder = nil break end
			if (v.hide) then continue end
			
			local model = v.modelEnt
			local sprite = v.spriteMaterial
			
			if (!v.bone) then continue end
			
			local pos, ang = self:GetBoneOrientation( self.VElements, v, vm )
			
			if (!pos) then continue end
			
			if (v.type == "Model" and IsValid(model)) then

				model:SetPos(pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z )
				ang:RotateAroundAxis(ang:Up(), v.angle.y)
				ang:RotateAroundAxis(ang:Right(), v.angle.p)
				ang:RotateAroundAxis(ang:Forward(), v.angle.r)

				model:SetAngles(ang)
				//model:SetModelScale(v.size)
				local matrix = Matrix()
				matrix:Scale(v.size)
				model:EnableMatrix( "RenderMultiply", matrix )
				
				if (v.material == "") then
					model:SetMaterial("")
				elseif (model:GetMaterial() != v.material) then
					model:SetMaterial( v.material )
				end
				
				if (v.skin and v.skin != model:GetSkin()) then
					model:SetSkin(v.skin)
				end
				
				if (v.bodygroup) then
					for k, v in pairs( v.bodygroup ) do
						if (model:GetBodygroup(k) != v) then
							model:SetBodygroup(k, v)
						end
					end
				end
				
				if (v.surpresslightning) then
					render.SuppressEngineLighting(true)
				end

colors = self:randomColor(inputColor)
				render.SetColorModulation(colors.r/255, colors.g/255, colors.b/255)
				render.SetBlend(v.color.a/255)
				model:DrawModel()
				render.SetBlend(1)
				render.SetColorModulation(1, 1, 1)
				
				if (v.surpresslightning) then
					render.SuppressEngineLighting(false)
				end
				
			elseif (v.type == "Sprite" and sprite) then
				
				local drawpos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
				render.SetMaterial(sprite)
				render.DrawSprite(drawpos, v.size.x, v.size.y, v.color)
				
			elseif (v.type == "Quad" and v.draw_func) then
				
				local drawpos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
				ang:RotateAroundAxis(ang:Up(), v.angle.y)
				ang:RotateAroundAxis(ang:Right(), v.angle.p)
				ang:RotateAroundAxis(ang:Forward(), v.angle.r)
				
				cam.Start3D2D(drawpos, ang, v.size)
					v.draw_func( self )
				cam.End3D2D()

			end
			
		end
		
	end

	SWEP.wRenderOrder = nil
	function SWEP:DrawWorldModel()
		
		if (self.ShowWorldModel == nil or self.ShowWorldModel) then
			self:DrawModel()
		end
		
		if (!self.WElements) then return end
		
		if (!self.wRenderOrder) then

			self.wRenderOrder = {}

			for k, v in pairs( self.WElements ) do
				if (v.type == "Model") then
					table.insert(self.wRenderOrder, 1, k)
				elseif (v.type == "Sprite" or v.type == "Quad") then
					table.insert(self.wRenderOrder, k)
				end
			end

		end
		
		if (IsValid(self.Owner)) then
			bone_ent = self.Owner
		else
			// when the weapon is dropped
			bone_ent = self
		end
		
		for k, name in pairs( self.wRenderOrder ) do
		
			local v = self.WElements[name]
			if (!v) then self.wRenderOrder = nil break end
			if (v.hide) then continue end
			
			local pos, ang
			
			if (v.bone) then
				pos, ang = self:GetBoneOrientation( self.WElements, v, bone_ent )
			else
				pos, ang = self:GetBoneOrientation( self.WElements, v, bone_ent, "ValveBiped.Bip01_R_Hand" )
			end
			
			if (!pos) then continue end
			
			local model = v.modelEnt
			local sprite = v.spriteMaterial
			
			if (v.type == "Model" and IsValid(model)) then

				model:SetPos(pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z )
				ang:RotateAroundAxis(ang:Up(), v.angle.y)
				ang:RotateAroundAxis(ang:Right(), v.angle.p)
				ang:RotateAroundAxis(ang:Forward(), v.angle.r)

				model:SetAngles(ang)
				//model:SetModelScale(v.size)
				local matrix = Matrix()
				matrix:Scale(v.size)
				model:EnableMatrix( "RenderMultiply", matrix )
				
				if (v.material == "") then
					model:SetMaterial("")
				elseif (model:GetMaterial() != v.material) then
					model:SetMaterial( v.material )
				end
				
				if (v.skin and v.skin != model:GetSkin()) then
					model:SetSkin(v.skin)
				end
				
				if (v.bodygroup) then
					for k, v in pairs( v.bodygroup ) do
						if (model:GetBodygroup(k) != v) then
							model:SetBodygroup(k, v)
						end
					end
				end
				
				if (v.surpresslightning) then
					render.SuppressEngineLighting(true)
				end
				
				render.SetColorModulation(v.color.r/255, v.color.g/255, v.color.b/255)
				render.SetBlend(v.color.a/255)
				model:DrawModel()
				render.SetBlend(1)
				render.SetColorModulation(1, 1, 1)
				
				if (v.surpresslightning) then
					render.SuppressEngineLighting(false)
				end
				
			elseif (v.type == "Sprite" and sprite) then
				
				local drawpos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
				render.SetMaterial(sprite)
				render.DrawSprite(drawpos, v.size.x, v.size.y, v.color)
				
			elseif (v.type == "Quad" and v.draw_func) then
				
				local drawpos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
				ang:RotateAroundAxis(ang:Up(), v.angle.y)
				ang:RotateAroundAxis(ang:Right(), v.angle.p)
				ang:RotateAroundAxis(ang:Forward(), v.angle.r)
				
				cam.Start3D2D(drawpos, ang, v.size)
					v.draw_func( self )
				cam.End3D2D()

			end
			
		end
		
	end

	function SWEP:GetBoneOrientation( basetab, tab, ent, bone_override )
		
		local bone, pos, ang
		if (tab.rel and tab.rel != "") then
			
			local v = basetab[tab.rel]
			
			if (!v) then return end
			
			// Technically, if there exists an element with the same name as a bone
			// you can get in an infinite loop. Let's just hope nobody's that stupid.
			pos, ang = self:GetBoneOrientation( basetab, v, ent )
			
			if (!pos) then return end
			
			pos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
			ang:RotateAroundAxis(ang:Up(), v.angle.y)
			ang:RotateAroundAxis(ang:Right(), v.angle.p)
			ang:RotateAroundAxis(ang:Forward(), v.angle.r)
				
		else
		
			bone = ent:LookupBone(bone_override or tab.bone)

			if (!bone) then return end
			
			pos, ang = Vector(0,0,0), Angle(0,0,0)
			local m = ent:GetBoneMatrix(bone)
			if (m) then
				pos, ang = m:GetTranslation(), m:GetAngles()
			end
			
			if (IsValid(self.Owner) and self.Owner:IsPlayer() and 
				ent == self.Owner:GetViewModel() and self.ViewModelFlip) then
				ang.r = -ang.r // Fixes mirrored models
			end
		
		end
		
		return pos, ang
	end

	function SWEP:CreateModels( tab )

		if (!tab) then return end

		// Create the clientside models here because Garry says we can't do it in the render hook
		for k, v in pairs( tab ) do
			if (v.type == "Model" and v.model and v.model != "" and (!IsValid(v.modelEnt) or v.createdModel != v.model) and 
					string.find(v.model, ".mdl") and file.Exists (v.model, "GAME") ) then
				
				v.modelEnt = ClientsideModel(v.model, RENDER_GROUP_VIEW_MODEL_OPAQUE)
				if (IsValid(v.modelEnt)) then
					v.modelEnt:SetPos(self:GetPos())
					v.modelEnt:SetAngles(self:GetAngles())
					v.modelEnt:SetParent(self)
					v.modelEnt:SetNoDraw(true)
					v.createdModel = v.model
				else
					v.modelEnt = nil
				end
				
			elseif (v.type == "Sprite" and v.sprite and v.sprite != "" and (!v.spriteMaterial or v.createdSprite != v.sprite) 
				and file.Exists ("materials/"..v.sprite..".vmt", "GAME")) then
				
				local name = v.sprite.."-"
				local params = { ["$basetexture"] = v.sprite }
				// make sure we create a unique name based on the selected options
				local tocheck = { "nocull", "additive", "vertexalpha", "vertexcolor", "ignorez" }
				for i, j in pairs( tocheck ) do
					if (v[j]) then
						params["$"..j] = 1
						name = name.."1"
					else
						name = name.."0"
					end
				end

				v.createdSprite = v.sprite
				v.spriteMaterial = CreateMaterial(name,"UnlitGeneric",params)
				
			end
		end
		
	end
	
	local allbones
	local hasGarryFixedBoneScalingYet = false

	function SWEP:UpdateBonePositions(vm)
		
		if self.ViewModelBoneMods then
			
			if (!vm:GetBoneCount()) then return end
			
			// !! WORKAROUND !! //
			// We need to check all model names :/
			local loopthrough = self.ViewModelBoneMods
			if (!hasGarryFixedBoneScalingYet) then
				allbones = {}
				for i=0, vm:GetBoneCount() do
					local bonename = vm:GetBoneName(i)
					if (self.ViewModelBoneMods[bonename]) then 
						allbones[bonename] = self.ViewModelBoneMods[bonename]
					else
						allbones[bonename] = { 
							scale = Vector(1,1,1),
							pos = Vector(0,0,0),
							angle = Angle(0,0,0)
						}
					end
				end
				
				loopthrough = allbones
			end
			// !! ----------- !! //
			
			for k, v in pairs( loopthrough ) do
				local bone = vm:LookupBone(k)
				if (!bone) then continue end
				
				// !! WORKAROUND !! //
				local s = Vector(v.scale.x,v.scale.y,v.scale.z)
				local p = Vector(v.pos.x,v.pos.y,v.pos.z)
				local ms = Vector(1,1,1)
				if (!hasGarryFixedBoneScalingYet) then
					local cur = vm:GetBoneParent(bone)
					while(cur >= 0) do
						local pscale = loopthrough[vm:GetBoneName(cur)].scale
						ms = ms * pscale
						cur = vm:GetBoneParent(cur)
					end
				end
				
				s = s * ms
				// !! ----------- !! //
				
				if vm:GetManipulateBoneScale(bone) != s then
					vm:ManipulateBoneScale( bone, s )
				end
				if vm:GetManipulateBoneAngles(bone) != v.angle then
					vm:ManipulateBoneAngles( bone, v.angle )
				end
				if vm:GetManipulateBonePosition(bone) != p then
					vm:ManipulateBonePosition( bone, p )
				end
			end
		else
			self:ResetBonePositions(vm)
		end
		   
	end
	 
	function SWEP:ResetBonePositions(vm)
		
		if (!vm:GetBoneCount()) then return end
		for i=0, vm:GetBoneCount() do
			vm:ManipulateBoneScale( i, Vector(1, 1, 1) )
			vm:ManipulateBoneAngles( i, Angle(0, 0, 0) )
			vm:ManipulateBonePosition( i, Vector(0, 0, 0) )
		end
		
	end

	/**************************
		Global utility code
	**************************/

	// Fully copies the table, meaning all tables inside this table are copied too and so on (normal table.Copy copies only their reference).
	// Does not copy entities of course, only copies their reference.
	// WARNING: do not use on tables that contain themselves somewhere down the line or you'll get an infinite loop
	function table.FullCopy( tab )

		if (!tab) then return nil end
		
		local res = {}
		for k, v in pairs( tab ) do
			if (type(v) == "table") then
				res[k] = table.FullCopy(v) // recursion ho!
			elseif (type(v) == "Vector") then
				res[k] = Vector(v.x, v.y, v.z)
			elseif (type(v) == "Angle") then
				res[k] = Angle(v.p, v.y, v.r)
			else
				res[k] = v
			end
		end
		
		return res
		
	end
	
end

