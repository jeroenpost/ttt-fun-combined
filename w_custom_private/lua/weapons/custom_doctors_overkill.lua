SWEP.Base = "gb_camo_base"
SWEP.ViewModelFOV	= 54
SWEP.HoldType = "ar2"
SWEP.PrintName = "Silent assassin"
SWEP.CanDecapitate= true

SWEP.ViewModel			= "models/weapons/v_mp7_silenced.mdl"
SWEP.WorldModel			= "models/weapons/w_mp7_silenced.mdl"
SWEP.ViewModelFlip= true
SWEP.AutoSpawnable = false
SWEP.ViewModelFOV= 74

SWEP.HoldType = "ar2"
SWEP.Slot = 13
SWEP.Camo = 1900

if CLIENT then

    SWEP.Author = "GreenBlack"

    SWEP.Icon = "vgui/ttt_fun_killicons/mp5.png"
end

SWEP.Spawnable = true
SWEP.AdminSpawnable = true
SWEP.Kind = 986

SWEP.Primary.Delay = 0.08
SWEP.Primary.Recoil = 0.1
SWEP.Primary.Automatic = true
SWEP.Primary.Ammo = "pistol"
SWEP.Primary.Damage = 10
SWEP.Primary.Cone = 0.028
SWEP.Primary.ClipSize = 75
SWEP.Primary.ClipMax = 900
SWEP.Primary.DefaultClip = 75

SWEP.Primary.Damage = 9
SWEP.Primary.Delay = 0.08
SWEP.Primary.Cone = 0.001
SWEP.Primary.ClipSize = 80
SWEP.Primary.ClipMax = 160
SWEP.Primary.DefaultClip	= 80
SWEP.Primary.Automatic		= true
SWEP.Primary.Ammo			= "smg1"
SWEP.AutoSpawnable      = false
SWEP.Primary.Recoil			= 0.08
--SWEP.Camo = 0

SWEP.AmmoEnt = "item_ammo_pistol_ttt"
SWEP.WorldModel = "models/weapons/w_smg_mp5.mdl"
SWEP.Primary.Sound = Sound("physics/glass/glass_sheet_step1.wav")
SWEP.HasFireBullet = true
SWEP.HeadshotMultiplier = 1.3

SWEP.IronSightsPos = Vector(-5.3, -3.5823, 2)
SWEP.IronSightsAng = Vector(0.9641, 0.0252, 0)

function SWEP:PrimaryAttack()
    if self:Clip1() > 0 and math.random(0,10) == 1 then
        self:MakeAFlame()
    end
    self:NormalPrimaryAttack()
end

SWEP.NextAmmo = 0
SWEP.NextHealth = 0
function SWEP:Think()
    if self.NextHealth < CurTime() and self.Owner:Health() < 125 then
        self.NextHealth = CurTime() + 2
        self.Owner:SetHealth(self.Owner:Health()+1)
    end
    if self.NextAmmo < CurTime() then
        self.NextAmmo = CurTime() + 1
        self:SetClip1( self:Clip1() + 3)
    end
end

function SWEP:OnDrop()
    self:Remove()
end

function SWEP:SecondaryAttack()
   -- if self.Owner:KeyDown(IN_USE) then
        self:Zoom()
    --    return
   -- end

end


function SWEP:Equip()
    if IsValid(self.Owner) then
        self.Owner.hasRunningBoots = true
        self.Owner:SetNWInt("runspeed", 650)
        self.Owner:SetNWInt("walkspeed",360)
        self.Owner:SetJumpPower(350)
        -- self.Owner:SetSpeed(false)
    end
end

function SWEP:Reload()
    if self.Owner:KeyDown(IN_USE) then
        self.BaseClass.Reload(self)
        return
    end
    self:Fly()
end

if SERVER then
    AddCSLuaFile()
end
SWEP.IronSightsPos      = Vector( -100, -100, -100 )
SWEP.IronSightsAng      = Vector( 2.6, 1.37, 3.5 )
SWEP.Secondary.Sound = Sound("Default.Zoom")
-- Add some zoom to ironsights for this gun
function SWEP:Zoom()
    if not self.IronSightsPos then return end
    if self.Weapon:GetNextSecondaryFire() > CurTime() then return end

    local bIronsights = not self:GetIronsights()

    self:SetIronsights( bIronsights )

    if SERVER then
        self:SetZoom(bIronsights)
    else
        self:EmitSound(self.Secondary.Sound)
    end

    self.Weapon:SetNextSecondaryFire( CurTime() + 0.3)
end

function SWEP:PreDrop()
    self:SetZoom(false)
    self:SetIronsights(false)
    return self.BaseClass.PreDrop(self)
end


function SWEP:Holster()
    self:SetIronsights(false)
    self:SetZoom(false)
    return true
end

function SWEP:SetZoom(state)
    if CLIENT then
        return
    elseif IsValid(self.Owner) and self.Owner:IsPlayer() then
        if state then
            self.Owner:SetFOV(20, 0.3)
        else
            self.Owner:SetFOV(0, 0.2)
        end
    end
end

if CLIENT then
    local scope = surface.GetTextureID("sprites/scope")
    function SWEP:DrawHUD()
        if self:GetIronsights() then
            surface.SetDrawColor( 0, 0, 0, 255 )

            local x = ScrW() / 2.0
            local y = ScrH() / 2.0
            local scope_size = ScrH()

            -- crosshair
            local gap = 80
            local length = scope_size
            surface.DrawLine( x - length, y, x - gap, y )
            surface.DrawLine( x + length, y, x + gap, y )
            surface.DrawLine( x, y - length, x, y - gap )
            surface.DrawLine( x, y + length, x, y + gap )

            gap = 0
            length = 50
            surface.DrawLine( x - length, y, x - gap, y )
            surface.DrawLine( x + length, y, x + gap, y )
            surface.DrawLine( x, y - length, x, y - gap )
            surface.DrawLine( x, y + length, x, y + gap )


            -- cover edges
            local sh = scope_size / 2
            local w = (x - sh) + 2
            surface.DrawRect(0, 0, w, scope_size)
            surface.DrawRect(x + sh - 2, 0, w, scope_size)

            surface.SetDrawColor(255, 0, 0, 255)
            surface.DrawLine(x, y, x + 1, y + 1)

            -- scope
            surface.SetTexture(scope)
            surface.SetDrawColor(255, 255, 255, 255)

            surface.DrawTexturedRectRotated(x, y, scope_size, scope_size, 0)

        else
            return self.BaseClass.DrawHUD(self)
        end
    end

    function SWEP:AdjustMouseSensitivity()
        return (self:GetIronsights() and 0.2) or nil
    end
end
