SWEP.Base = "gb_camo_base"
SWEP.ViewModelFOV	= 54
SWEP.HoldType = "pistol"
SWEP.PrintName = "Loq's De-spawner"

SWEP.ViewModel			= "models/weapons/c_toolgun.mdl"
SWEP.WorldModel			= "models/weapons/w_toolgun.mdl"
SWEP.AutoSpawnable = false
SWEP.Slot = 7

if CLIENT then

    SWEP.Author = "GreenBlack"

    SWEP.Icon = "vgui/ttt_fun_killicons/scout.png"
end

function SWEP:GetToolObject( tool )
    return "Loq's Despawner"
end

SWEP.Kind = WEAPON_EQUIP3

SWEP.Primary.Delay          = 1.5
SWEP.Primary.Recoil         = 7
SWEP.Primary.Automatic = true
SWEP.Primary.Ammo = "357"
SWEP.Primary.Damage = 9
SWEP.Primary.Cone = 0.001
SWEP.Primary.ClipSize = 250
SWEP.Primary.ClipMax = 250 -- keep mirrored to ammo
SWEP.Primary.DefaultClip = 250
SWEP.Primary.Automatic		= true
SWEP.Primary.Damage			= 7
SWEP.Primary.NumShots		= 1
SWEP.Primary.Cone			= 0.02
SWEP.Primary.Delay			= 0.06
SWEP.HeadshotMultiplier = 1
SWEP.Primary.Recoil = 0.01

SWEP.HeadshotMultiplier = 4

SWEP.AutoSpawnable      = false
SWEP.AmmoEnt = "item_ammo_357_ttt"

SWEP.UseHands			= true
SWEP.ViewModelFlip		= false
SWEP.ViewModelFOV		= 54
SWEP.Camo = -1

SWEP.Primary.Sound = Sound("physics/rubber/rubber_tire_impact_bullet1.wav")

SWEP.Secondary.Sound = Sound("Default.Zoom")

SWEP.IronSightsPos      = Vector( 5, -15, -2 )
SWEP.IronSightsAng      = Vector( 2.6, 1.37, 3.5 )

function SWEP:Deploy()
    self:SetNWString("shootmode","SMG")
end
function SWEP:OnDrop()
    if SERVER then
        self:EmitSound("vo/Citadel/br_ohshit.wav")
    end
end

SWEP.NextFire = 0
SWEP.NextSniper = 0
function SWEP:PrimaryAttack()
    local mode = self:GetNWString("shootmode");
    if mode == "SMG"  then
        if self.NextFire > CurTime() then return end
        self.NextFire = CurTime() + 0.09
    end
    if mode == "Sniper"  then
        if self.NextSniper > CurTime() then return end
        self.NextSniper = CurTime() + 7
    end



    if not worldsnd then
        self.Weapon:EmitSound( self.Primary.Sound, self.Primary.SoundLevel )
    elseif SERVER then
        sound.Play(self.Primary.Sound, self:GetPos(), self.Primary.SoundLevel)
    end

    self:ShootBullet( self.Primary.Damage, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )

    self:TakePrimaryAmmo( 1 )

    local owner = self.Owner
    if not IsValid(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

    owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )
end

SWEP.NextSwitch = 0
function SWEP:Reload()
    if self.Owner:KeyDown(IN_USE) then
        if self.NextSwitch > CurTime() then return end
        self.NextSwitch = CurTime() + 0.5
        local mode = self:GetNWString("shootmode");
        if mode == "SMG" then
            self:SetNWString("shootmode","Sniper")
            self.Primary.Automatic		= false
            self.Primary.Damage			= 80
            self.Primary.NumShots		= 1
            self.Primary.Cone			= 0.001
            self.Primary.Delay			= 1.5
            self.HeadshotMultiplier = 2
            self.Primary.Recoil = 7
        else
            self:SetNWString("shootmode","SMG")
            self.Primary.Automatic		= true
            self.Primary.Damage			= 7
            self.Primary.NumShots		= 1
            self.Primary.Cone			= 0.001
            self.Primary.Delay			= 0.06
            self.HeadshotMultiplier = 1
            self.Primary.Recoil = 0.01
        end
        return
    end
    if ( self:Clip1() == self.Primary.ClipSize or self.Owner:GetAmmoCount( self.Primary.Ammo ) <= 0 ) then return end
    self:DefaultReload( ACT_VM_RELOAD )
    self:SetIronsights( false )
    self:SetZoom( false )
end

function SWEP:SetZoom(state)
    if CLIENT then
        return
    elseif IsValid(self.Owner) and self.Owner:IsPlayer() then
        if state then
            self.Owner:SetFOV(20, 0.3)
        else
            self.Owner:SetFOV(0, 0.2)
        end
    end
end

-- Add some zoom to ironsights for this gun
function SWEP:SecondaryAttack()
    if not self.IronSightsPos then return end
    if self:GetNextSecondaryFire() > CurTime() then return end

    bIronsights = not self:GetIronsights()

    self:SetIronsights( bIronsights )

    if SERVER then
        self:SetZoom(bIronsights)
    else
        self:EmitSound(self.Secondary.Sound)
    end

    self:SetNextSecondaryFire( CurTime() + 0.3)
end

function SWEP:PreDrop()
    self:SetZoom(false)
    self:SetIronsights(false)
    return self.BaseClass.PreDrop(self)
end



function SWEP:Holster()
    self:SetIronsights(false)
    self:SetZoom(false)
    return true
end
SWEP.ToolNameHeight		= 0
SWEP.InfoBoxHeight		= 0



if CLIENT then

    SWEP.WepSelectIcon		= surface.GetTextureID( "vgui/gmod_tool" )
    SWEP.Gradient			= surface.GetTextureID( "gui/gradient" )
    SWEP.InfoIcon			= surface.GetTextureID( "gui/info" )

    surface.CreateFont( "GModToolName",
        {
            font		= "Roboto Bk",
            size		= 80,
            weight		= 1000
        })

    surface.CreateFont( "GModToolSubtitle",
        {
            font		= "Roboto Bk",
            size		= 24,
            weight		= 1000
        })

    surface.CreateFont( "GModToolHelp",
        {
            font		= "Roboto Bk",
            size		= 17,
            weight		= 1000
        })

    local scope = surface.GetTextureID("sprites/scope")
    function SWEP:DrawHUD()



        local shotttext = "Shootmode: "..self:GetNWString("shootmode").."\nE+R to change"
        surface.SetFont( "ChatFont" );
        local size_x, size_y = surface.GetTextSize( shotttext );

        draw.RoundedBox( 6, ScrW( ) / 2 - size_x / 2 - 5, ScrH( ) - 100, size_x + 15, size_y + 10, Color( 0, 0, 0, 150 ) );
        draw.DrawText( shotttext , "ChatFont", ScrW( ) / 2, ScrH( ) - 100 + 5, Color( 255, 30, 30, 255 ), TEXT_ALIGN_CENTER );

        if self:GetIronsights() then
            surface.SetDrawColor( 0, 0, 0, 255 )

            local x = ScrW() / 2.0
            local y = ScrH() / 2.0
            local scope_size = ScrH()

            -- crosshair
            local gap = 80
            local length = scope_size
            surface.DrawLine( x - length, y, x - gap, y )
            surface.DrawLine( x + length, y, x + gap, y )
            surface.DrawLine( x, y - length, x, y - gap )
            surface.DrawLine( x, y + length, x, y + gap )

            gap = 0
            length = 50
            surface.DrawLine( x - length, y, x - gap, y )
            surface.DrawLine( x + length, y, x + gap, y )
            surface.DrawLine( x, y - length, x, y - gap )
            surface.DrawLine( x, y + length, x, y + gap )


            -- cover edges
            local sh = scope_size / 2
            local w = (x - sh) + 2
            surface.DrawRect(0, 0, w, scope_size)
            surface.DrawRect(x + sh - 2, 0, w, scope_size)

            surface.SetDrawColor(255, 0, 0, 255)
            surface.DrawLine(x, y, x + 1, y + 1)

            -- scope
            surface.SetTexture(scope)
            surface.SetDrawColor(255, 255, 255, 255)

            surface.DrawTexturedRectRotated(x, y, scope_size, scope_size, 0)

        else
            return self.BaseClass.DrawHUD(self)
        end
    end

    function SWEP:AdjustMouseSensitivity()
        return (self:GetIronsights() and 0.2) or nil
    end





    local matScreen 	= Material( "models/weapons/v_toolgun/screen" )
    local txBackground	= surface.GetTextureID( "models/weapons/v_toolgun/screen_bg" )

    -- GetRenderTarget returns the texture if it exists, or creates it if it doesn't
    local RTTexture 	= GetRenderTarget( "GModToolgunScreen", 256, 256 )

    surface.CreateFont( "GModToolScreen", {
        font	= "Helvetica",
        size	= 60,
        weight	= 900
    } )


    local function DrawScrollingText( text, y, texwide )

        local w, h = surface.GetTextSize( text  )
        w = w + 64

        local x = math.fmod( CurTime() * 400, w ) * -1

        while ( x < texwide ) do

            surface.SetTextColor( 0, 0, 0, 255 )
            surface.SetTextPos( x + 3, y + 3 )
            surface.DrawText( text )

            surface.SetTextColor( 255, 255, 255, 255 )
            surface.SetTextPos( x, y )
            surface.DrawText( text )

            x = x + w

        end

    end

    --[[---------------------------------------------------------
        We use this opportunity to draw to the toolmode
            screen's rendertarget texture.
    -----------------------------------------------------------]]
    function SWEP:RenderScreen()

        local TEX_SIZE = 256
        local mode = GetConVarString( "gmod_toolmode" )
        local oldW = ScrW()
        local oldH = ScrH()

        -- Set the material of the screen to our render target
        matScreen:SetTexture( "$basetexture", RTTexture )

        local OldRT = render.GetRenderTarget()

        -- Set up our view for drawing to the texture
        render.SetRenderTarget( RTTexture )
        render.SetViewPort( 0, 0, TEX_SIZE, TEX_SIZE )
        cam.Start2D()

        -- Background
        surface.SetDrawColor( 255, 255, 255, 255 )
        surface.SetTexture( txBackground )
        surface.DrawTexturedRect( 0, 0, TEX_SIZE, TEX_SIZE )

        -- Give our toolmode the opportunity to override the drawing


            surface.SetFont( "GModToolScreen" )
            DrawScrollingText("Loq's Despawner", 64, TEX_SIZE )



        cam.End2D()
        render.SetRenderTarget( OldRT )
        render.SetViewPort( 0, 0, oldW, oldH )

    end
end
