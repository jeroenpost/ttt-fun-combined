if SERVER then
   AddCSLuaFile(  )
   --resource.AddFile("materials/vgui/ttt_fun_killicons/elites.png")
end
   
SWEP.HoldType = "duel"
   
 SWEP.PrintName = "Professional Elites"
SWEP.Slot = 1
if CLIENT then
   
   
   SWEP.Author = "GreenBlack"
 
   SWEP.Icon = "vgui/ttt_fun_killicons/elites.png"
end
 
SWEP.Base = "aa_base"


SWEP.Kind = WEAPON_PISTOL
SWEP.Primary.Recoil     = 0.5
SWEP.Primary.Damage = 35
SWEP.Primary.Delay = 0.35
SWEP.Primary.Cone = 0.001
SWEP.Primary.ClipSize = 60
SWEP.Primary.Automatic = true
SWEP.Primary.DefaultClip = 60
SWEP.Primary.ClipMax = 120
SWEP.Primary.Ammo = "Pistol"

SWEP.Secondary.Delay = 0.30
SWEP.Secondary.ClipSize     = 40
SWEP.Secondary.DefaultClip  = 40
SWEP.Secondary.Automatic    = true
SWEP.Secondary.Ammo         = "Pistol"
SWEP.Secondary.ClipMax      = 60

SWEP.AutoSpawnable = false
SWEP.AmmoEnt = "item_ammo_pistol_ttt"
SWEP.NoSights = true

SWEP.ViewModel  =  "models/weapons/cstrike/c_pist_elite.mdl"
SWEP.WorldModel = "models/weapons/w_pist_elite.mdl"
SWEP.ViewModelFlip = false
SWEP.ViewModelFOV  = 54
SWEP.UseHands = true

SWEP.HeadshotMultiplier = 1
SWEP.Primary.Sound = Sound( "weapons/fx/rics/ric4.wav" )
SWEP.Primary.SoundLevel = 55
SWEP.Secondary.Sound = Sound( "vo/npc/female01/itsamanhack01.wav" )



function SWEP:PrimaryAttack( worldsnd )

    if not self:CanPrimaryAttack() then return end
   self.Weapon:SetNextPrimaryFire( CurTime() +self.Primary.Delay )




   if not worldsnd then
       self.Weapon:EmitSound( self.Primary.Sound, self.Primary.SoundLevel )
   elseif SERVER then
       sound.Play(self.Primary.Sound, self:GetPos(), self.Primary.SoundLevel)
   end

   self:ShootBullet( self.Primary.Damage, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )


   if not worldsnd then
      self.Weapon:EmitSound( self.Primary.Sound, self.Primary.SoundLevel )
   elseif SERVER then
      sound.Play(self.Primary.Sound, self:GetPos(), self.Primary.SoundLevel)
   end

   self:TakePrimaryAmmo( 1 )
    timer.Simple(0.01,function()
        self:ShootFrag(0)
    end)

   local owner = self.Owner
   if not IsValid(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end
   self.Weapon:SendWeaponAnim(ACT_VM_PRIMARYATTACK)
 --  owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )
end

function SWEP:CanSecondaryAttack()
   if not IsValid(self.Owner) then return end

   if self:Clip1() < 1  then

      return false
   end
   return true
end




function SWEP:SecondaryAttack()
    if self.Owner:KeyDown(IN_USE) then
        self:rFly()
        return
    end
    local bucket, att, phys, tr
    if not self:CanSecondaryAttack() then return end


    self.Weapon:SetNextSecondaryFire(CurTime() +self.Primary.Delay)
    self.Weapon:EmitSound( self.Primary.Sound, self.Primary.SoundLevel )

    if CLIENT then
        return
    end
    if not worldsnd then
        self.Weapon:EmitSound( self.Primary.Sound, self.Primary.SoundLevel )
    elseif SERVER then
        sound.Play(self.Primary.Sound, self:GetPos(), self.Primary.SoundLevel)
    end

    self:ShootBullet( self.Primary.Damage, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )

    self:TakePrimaryAmmo( 1 )
   -- self:TakePrimaryAmmo( 1 )
   -- self:ShootTracer("AirboatGunTracer")
    timer.Simple(0.01,function()
        self:ShootFrag(0)
    end)


   -- self.Owner:ViewPunch( Angle( math.Rand(-8,8), math.Rand(-8,8), 0 ) )
    self.Weapon:SendWeaponAnim(ACT_VM_PRIMARYATTACK)
end

-- COLOR
function SWEP:Deploy()
    self.Weapon:SendWeaponAnim(ACT_VM_DRAW)
    self:SetColorAndMaterial(Color(255,255,255,255), "phoenix_storms/metalset_1-2");
    return true
end


function SWEP:PreDrop()
    self:ColorReset()
end


function SWEP:Holster()
    self:ColorReset()
    return true
end

function SWEP:OnRemove()
    self:ColorReset()
end
function SWEP:OnDrop()
    self:ColorReset()

end

function SWEP:PreDrawViewModel()

    if SERVER or not IsValid(self.Owner) or not IsValid(self.Owner:GetViewModel()) then return end
    self.Owner:GetViewModel():SetColor(Color(255,255,255,255))
    self.Owner:GetViewModel():SetMaterial("phoenix_storms/metalset_1-2");

end