SWEP.Base = "gb_camo_base"
SWEP.ViewModelFOV	= 64
SWEP.HoldType = "duel"
SWEP.PrintName = "Honk's Hands"
SWEP.ViewModel		= "models/weapons/v_pist_deags.mdl"
SWEP.WorldModel		= "models/weapons/w_pist_deagle.mdl"
SWEP.Kind = WEAPON_ROLE
SWEP.Slot = 7
SWEP.AutoSpawnable = false
SWEP.Primary.Recoil     = 1.5
SWEP.Primary.Damage = 50
SWEP.Primary.Delay = 0.55
SWEP.Primary.Cone = 0.02
SWEP.Primary.ClipSize = 100
SWEP.Primary.Automatic = true
SWEP.Primary.DefaultClip = 100
SWEP.Camo = 999
SWEP.Secondary.Delay = 0.55
SWEP.Secondary.ClipSize     = 100
SWEP.Secondary.DefaultClip  = 100
SWEP.Secondary.Automatic    = true
SWEP.Secondary.Ammo         = ".357"
SWEP.Secondary.ClipMax      = 100
SWEP.Camo = 46
SWEP.Primary.Ammo			= "AlyxGun"
SWEP.AmmoEnt = "item_ammo_357_ttt"
SWEP.HeadshotMultiplier = 2.5

SWEP.Camo = 17
SWEP.CustomCamo = true
SWEP.CamoIndex = {[3] ="camos/custom_camo30",[2] ="camos/custom_camo30"}

function SWEP:OnDrop()
    self:Remove()
end

SWEP.Primary.Sound = Sound("CSTM_Deagle")
SWEP.Secondary.Sound = Sound("CSTM_Deagle")

SWEP.WElements = {
    ["weapon"] = { type = "Model", model = "models/weapons/w_pist_deagle.mdl", bone = "ValveBiped.Bip01_L_Hand", rel = "", pos = Vector(3.6, 0.631, -2.5), angle = Angle(0, 0, 0), size = Vector(1, 1, 1), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
}


function SWEP:PrimaryAttack( worldsnd )
    local mode = self:GetNWString("shootmode","Fire")

    if self.Owner:KeyDown(IN_USE) then
        if CLIENT then return end
        self.Weapon:SetNextPrimaryFire( CurTime() + 0.5 )
        if mode == "Fire" then self:SetNWString("shootmode","Shock") end
        if mode == "Shock" then self:SetNWString("shootmode","Life") end
        if mode == "Life" then self:SetNWString("shootmode","Fire") end

        return
    end

    --self.Weapon:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )

    if mode == "Fire" then
        self:Flamethrower()
        self.Weapon:SetNextPrimaryFire( CurTime() + 0.05 )
        return
    end

    if mode == "Shock" then
        self:LichtningPoison()
        return
    end

    if mode == "Life" then

        local tr = self.Owner:GetEyeTrace();


        if IsValid( tr.Entity ) and tr.Entity:IsPlayer() and tr.Entity:Health() < 150  then
            tr.Entity:SetHealth(tr.Entity:Health() + 25)
            if  tr.Entity:Health() > 150 then  tr.Entity:SetHealth(150) end
        end

        self.LaserColor = Color(50,255,50,255)
        self:ShootTracer("LaserTracer_thick")

        return
    end


    if not self:CanPrimaryAttack() then return end

    if not worldsnd then
        self.Weapon:EmitSound( self.Primary.Sound, self.Primary.SoundLevel )
    elseif SERVER then
        sound.Play(self.Primary.Sound, self:GetPos(), self.Primary.SoundLevel)
    end

    self:ShootBullet( self.Primary.Damage, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )

    self:TakePrimaryAmmo( 1 )

    local owner = self.Owner
    if not IsValid(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

    --  owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )
end

function SWEP:Flamethrower()
    if ( !self:CanPrimaryAttack() ) then return end

    local bullet = {}
    bullet.Num = 1
    bullet.Src = self.Owner:GetShootPos()
    bullet.Dir = self.Owner:GetAimVector()
    bullet.Owner = self.Owner
    bullet.Spread = Vector(0.001, 0.0001, 0 )
    bullet.Tracer = 1
    bullet.Force = 5
    bullet.Damage = 9
    --bullet.AmmoType = "Ar2AltFire" -- For some extremely stupid reason this breaks the tracer effect
    bullet.TracerName = "gb_flames"

    if ( self.Owner:IsValid()  ) then

        self.Owner:FireBullets( bullet )
        local tr = self.Owner:GetEyeTrace()
        local effectdata = EffectData()
        effectdata:SetOrigin(tr.HitPos)
        effectdata:SetNormal(tr.HitNormal)
        effectdata:SetScale(0.5)
        -- util.Effect("effect_mad_ignition", effectdata)
        util.Decal("FadingScorch", tr.HitPos + tr.HitNormal, tr.HitPos - tr.HitNormal)

        local tracedata = {}
        tracedata.start = tr.HitPos
        tracedata.endpos = Vector(tr.HitPos.x, tr.HitPos.y, tr.HitPos.z - 10)
        tracedata.filter = tr.HitPos
        local tracedata2 = util.TraceLine(tracedata)



        if tracedata2.HitWorld && self.Owner:GetPos():Distance( tracedata.endpos ) < 500 then
        if SERVER then
            local flame = ents.Create("env_fire");
            flame:SetPos(tr.HitPos + Vector( 0, 0, 1 ));
            flame:SetKeyValue("firesize", "10");
            flame:SetKeyValue("fireattack", "10");
            flame:SetKeyValue("StartDisabled", "0");
            flame:SetKeyValue("health", "1");
            flame:SetKeyValue("firetype", "0");
            flame:SetKeyValue("damagescale", "1");
            flame:SetKeyValue("spawnflags", 128 + 2);
            flame:SetPhysicsAttacker(self.Owner)
            flame:SetOwner( self.Owner )
            flame:Spawn();
            flame.DieTime = 0.1
            flame:Fire("StartFire",0);

        end


        elseif self.Owner:GetPos():Distance( tracedata.endpos ) > 450 then
           -- bullet.Damage = 0
        end




        local owner = self.Owner
        --if not ValidEntity(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

        owner:ViewPunch( Angle( math.Rand(-0.01,-0.05) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )
    end


    self:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
    self:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
end



SWEP.hideblind = true
function SWEP:Icethrower()
    if ( !self:CanPrimaryAttack() ) then return end

    self:Freeze(25,2)

    self.LaserColor = Color(255,255,255,255)
    local bullet = {}
    bullet.Num = 1
    bullet.Src = self.Owner:GetShootPos()
    bullet.Dir = self.Owner:GetAimVector()
    bullet.Owner = self.Owner
    bullet.Spread = Vector( 0.01, 0.01, 0 )
    bullet.Tracer = 1
    bullet.Force = 5
    bullet.Damage = 9
            //bullet.AmmoType = "Ar2AltFire" -- For some extremely stupid reason this breaks the tracer effect
    bullet.TracerName = "LaserTracer_thick"



    if ( self.Owner:IsValid()  ) then



        self.Owner:FireBullets( bullet )

        if SERVER then

            local owner = self.Owner
            --if not ValidEntity(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

            owner:ViewPunch( Angle( math.Rand(-0.01,-0.05) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )
        end


        self:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
        self:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
    end
end

SWEP.hideblind = true
function SWEP:Zap()
    if ( !self:CanPrimaryAttack() ) then return end


    self.LaserColor = Color(math.Rand(0,255),math.Rand(0,255),math.Rand(0,255),255)
    local bullet = {}
    bullet.Num = 1
    bullet.Src = self.Owner:GetShootPos()
    bullet.Dir = self.Owner:GetAimVector()
    bullet.Owner = self.Owner
    bullet.Spread = Vector( 0.01, 0.01, 0 )
    bullet.Tracer = 1
    bullet.Force = 5
    bullet.Damage = 13
            //bullet.AmmoType = "Ar2AltFire" -- For some extremely stupid reason this breaks the tracer effect
    bullet.TracerName = "LaserTracer_thick"


    if ( self.Owner:IsValid()  ) then


        self:EmitSound("CSTM_M14")
        self.Owner:FireBullets( bullet )

        if SERVER then

            local owner = self.Owner
            --if not ValidEntity(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

            owner:ViewPunch( Angle( math.Rand(-0.01,-0.05) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )
        end


        self:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
        self:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
    end
end


SWEP.hideblind = true
function SWEP:LichtningPoison()
    if ( !self:CanPrimaryAttack() ) then return end

    local bullet = {}
    bullet.Num = 1
    bullet.Src = self.Owner:GetShootPos()
    bullet.Dir = self.Owner:GetAimVector()
    bullet.Owner = self.Owner
    bullet.Spread = Vector( 0.01, 0.01, 0 )
    bullet.Tracer = 1
    bullet.Force = 5
    bullet.Damage = 30

    bullet.TracerName = "AirboatGunHeavyTracer"

    if ( self.Owner:IsValid()  ) then

        self.Owner:FireBullets( bullet )
        self.Weapon:EmitSound("ambient/energy/zap1.wav")
        if SERVER then

            local owner = self.Owner
            --if not ValidEntity(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

            owner:ViewPunch( Angle( math.Rand(-0.01,-0.05) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )
        end

        local tr = self.Owner:GetEyeTrace();

        if IsValid( tr.Entity ) and tr.Entity:IsPlayer()  then

            local edata = EffectData()
            edata:SetEntity(tr.Entity)
            edata:SetMagnitude(6)
            edata:SetScale(5)
            util.Effect("TeslaHitBoxes", edata)
            timer.Create("honkHands_zap",1,5,function()
                if SERVER and IsValid(self) and IsValid(tr) and IsValid(self.Owner) and IsValid(tr.Entity) then
                    local edata = EffectData()
                    edata:SetEntity(tr.Entity)
                    edata:SetMagnitude(6)
                    edata:SetScale(5)
                    util.Effect("TeslaHitBoxes", edata)
                    tr.Entity:TakeDamage(5,self.Owner,self)
                end

            end)
        end


        self:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
        self:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
    end
end

function SWEP:CanSecondaryAttack()
    if not IsValid(self.Owner) then return end

    if self.Weapon:Clip1() <= 0 then
        self:DryFire(self.SetNextSecondaryFire)
        return false
    end
    return true
end

function SWEP:SecondaryAttack(worldsnd)
    local mode = self:GetNWString("shootmode2","Ice")

    if self.Owner:KeyDown(IN_USE) then
        if CLIENT then return end
        self.Weapon:SetNextSecondaryFire( CurTime() + 0.4 )
        if mode == "Ice" then self:SetNWString("shootmode2","Zap") end
        if mode == "Zap" then self:SetNWString("shootmode2","Death") end
        if mode == "Death" then self:SetNWString("shootmode2","Ice") end

        return
    end

    if mode == "Ice" then
        self:Icethrower()
        self.Weapon:SetNextSecondaryFire( CurTime() + 0.1 )
        return
    end

    if mode == "Zap" then
        self:Zap()
        self.Weapon:SetNextSecondaryFire( CurTime() + 0.12)
        return
    end

    if mode == "Death" then
        self:Death()
        return
    end







    self.Weapon:SetNextSecondaryFire( CurTime() + self.Secondary.Delay )
    --self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )

    if not self:CanSecondaryAttack() then return end

    if not worldsnd then
        self.Weapon:EmitSound( self.Primary.Sound, self.Primary.SoundLevel )
    elseif SERVER then
        sound.Play(self.Primary.Sound, self:GetPos(), self.Primary.SoundLevel)
    end

    self:ShootBullet( self.Primary.Damage, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )

    self:TakePrimaryAmmo( 1 )

    local owner = self.Owner
    if not IsValid(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

    -- owner:ViewPunch( Angle( math.Rand(-0.6,-0.6) * self.Primary.Recoil, math.Rand(-0.6,0.6) *self.Primary.Recoil, 0 ) )

end

function SWEP:Reload()
end

function SWEP:Death()
    local tracedata = {}
    tracedata.start = self.Owner:GetShootPos()
    tracedata.endpos = self.Owner:GetShootPos() + (self.Owner:GetAimVector() * 100)
    tracedata.filter = self.Owner
    tracedata.mins = Vector(1,1,1) * -10
    tracedata.maxs = Vector(1,1,1) * 10
    tracedata.mask = MASK_SHOT_HULL
    local tr = util.TraceLine({start = self.Owner:EyePos(), endpos = self.Owner:EyePos() + self.Owner:EyeAngles():Forward() * 999, filter = self.Owner})



    local ply = self.Owner
    self.NextReload = CurTime() + 0.8
    if IsValid(tr.Entity) then

        if tr.Entity.player_ragdoll and not tr.Entity.honkthing then
            if not self.Owner.usehotdoghonk2 or self.Owner.usehotdoghonk2 < CurTime() then
                local headturtle = self:SpawnNPC2(self.Owner, tr.Entity:GetPos()+Vector(0,0,55), "npc_headcrab_black")

                headturtle:SetNPCState(2)
                headturtle:SetMaterial("engine/occlusionproxy")

                local turtle = ents.Create("prop_dynamic")
                turtle:SetModel("models/food/hotdog.mdl")
                turtle:SetPos( tr.Entity:GetPos()+Vector(0,0,55) )
                turtle:SetAngles(Angle(0,-90,0))
                turtle:SetParent(headturtle)

                local headturtle = self:SpawnNPC2(self.Owner, tr.Entity:GetPos()+Vector(0,0,75), "npc_headcrab_black")

                headturtle:SetNPCState(2)
                headturtle:SetMaterial("engine/occlusionproxy")

                local turtle = ents.Create("prop_dynamic")
                turtle:SetModel("models/food/hotdog.mdl")
                turtle:SetPos( tr.Entity:GetPos()+Vector(0,0,75) )
                turtle:SetAngles(Angle(0,-90,0))
                turtle:SetParent(headturtle)

                local headturtle = self:SpawnNPC2(self.Owner, tr.Entity:GetPos()+Vector(0,0,65), "npc_headcrab_black")

                headturtle:SetNPCState(2)
                headturtle:SetMaterial("engine/occlusionproxy")

                local turtle = ents.Create("prop_dynamic")
                turtle:SetModel("models/food/hotdog.mdl")
                turtle:SetPos( tr.Entity:GetPos()+Vector(0,0,65) )
                turtle:SetAngles(Angle(0,-90,0))
                turtle:SetParent(headturtle)
                self.Owner.usehotdoghonk = CurTime() + 120
            end



            tr.Entity:SetMaterial("engine/occlusionproxy")

            tr.Entity.honkthing = true
            local turtle = ents.Create("prop_physics")
            turtle:SetModel("models/food/hotdog.mdl")

            turtle:SetPos(tr.Entity:GetPos()+Vector(0,0,-55))
            turtle:SetAngles(Angle(0,-45,0))
            turtle:SetParent(tr.Entity)
            turtle:SetNWString("IsHonk","y")
            turtle:Spawn()
            tr.Entity:SetModelScale( tr.Entity:GetModelScale() * 3, 1)
            timer.Simple(0.3,function()
                for k,v in pairs(player.GetAll()) do
                     v:SendLua("for k,v in pairs(ents.GetAll()) do if v:GetNWString('IsHonk','1')=='y' then v:SetModelScale(4.1,0)end end")
                end
            end)

        end
    end


end


function SWEP:SpawnNPC2( Player, Position, Class )

    local NPCList = list.Get( "NPC" )
    local NPCData = NPCList[ Class ]

    -- Don't let them spawn this entity if it isn't in our NPC Spawn list.
    -- We don't want them spawning any entity they like!
    if ( not NPCData ) then
        if ( IsValid( Player ) ) then
            Player:SendLua( "Derma_Message( \"Sorry! You can't spawn that NPC!\" )" );
        end
        return end

    local bDropToFloor = false

    --
    -- This NPC has to be spawned on a ceiling ( Barnacle )
    --
    if ( NPCData.OnCeiling and Vector( 0, 0, -1 ):Dot( Normal ) < 0.95 ) then
        return nil
    end

    if ( NPCData.NoDrop ) then bDropToFloor = false end

    --
    -- Offset the position
    --


    -- Create NPC
    local NPC = ents.Create( NPCData.Class )
    if ( not IsValid( NPC ) ) then return end

    NPC:SetPos( Position )
    --
    -- This NPC has a special model we want to define
    --
    if ( NPCData.Model ) then
        NPC:SetModel( NPCData.Model )
    end

    --
    -- Spawn Flags
    --
    local SpawnFlags = bit.bor( SF_NPC_FADE_CORPSE, SF_NPC_ALWAYSTHINK)
    if ( NPCData.SpawnFlags ) then SpawnFlags = bit.bor( SpawnFlags, NPCData.SpawnFlags ) end
    if ( NPCData.TotalSpawnFlags ) then SpawnFlags = NPCData.TotalSpawnFlags end
    NPC:SetKeyValue( "spawnflags", SpawnFlags )

    --
    -- Optional Key Values
    --
    if ( NPCData.KeyValues ) then
        for k, v in pairs( NPCData.KeyValues ) do
            NPC:SetKeyValue( k, v )
        end
    end

    --
    -- This NPC has a special skin we want to define
    --
    if ( NPCData.Skin ) then
        NPC:SetSkin( NPCData.Skin )
    end

    --
    -- What weapon should this mother be carrying
    --

    NPC:Spawn()
    NPC:Activate()

    if ( bDropToFloor and not NPCData.OnCeiling ) then
        NPC:DropToFloor()
    end

    return NPC
end
-- Spiderman

local sndPowerUp		= Sound("rope_hit.mp3")
local sndPowerDown		= Sound ("shoot_rope.mp3")
local sndTooFar			= Sound ("to_far.mp3")



SWEP.NextAmmoGive = 0
SWEP.NextAmmoTake = 0
SWEP.NextRegenAmmo = 0
function SWEP:Think()

    if self.Owner:KeyPressed(IN_ATTACK)  then
        -- When the left click is pressed, then
        self.ViewModelFlip = false
    end

    if self.Owner:KeyPressed(IN_ATTACK2)  then
        -- When the right click is pressed, then
        self.ViewModelFlip = true
    end

    if self.NextRegenAmmo < CurTime() then
        if self:Clip1() < 24 then
            self.NextRegenAmmo = CurTime() + 3
            self:SetClip1(self:Clip1()+2)
        end
    end

    if self.dt.reloading and IsFirstTimePredicted() then
        if self.Owner:KeyDown(IN_ATTACK) then
            self:FinishReload()
            return
        end

        if self.reloadtimer <= CurTime() then

            if self.Owner:GetAmmoCount(self.Primary.Ammo) <= 0 then
                self:FinishReload()
            elseif self.Weapon:Clip1() < self.Primary.ClipSize then
                self:PerformReload()
            else
                self:FinishReload()
            end
            return
        end
    end

    if self.NextAmmoGive < CurTime() && self:Clip1() < 10 then
    self.NextAmmoGive = CurTime() + 3
    self:SetClip1( self:Clip1() + 1 )
    end

    if (!self.Owner || self.Owner == NULL) or self:Clip1() < 1 then

    if ( !self.Beam ) then return end
    if( IsValid( self.Beam) ) then
        self.Beam:Remove()
    end
    self.Beam = nil
    return
    end



    if ( self.Owner:KeyPressed( IN_RELOAD )   ) then

        self:StartAttack()

    elseif ( self.Owner:KeyDown( IN_RELOAD )  ) then

        self:UpdateAttack()
        self.Weapon:SetNextSecondaryFire( CurTime() + 0.02 )

    elseif ( self.Owner:KeyReleased( IN_RELOAD )  ) then

        self:EndAttack( true )

    end



end

function SWEP:DoTrace( endpos )
    local trace = {}
    trace.start = self.Owner:GetShootPos()
    trace.endpos = trace.start + (self.Owner:GetAimVector() * 14096) --14096 is length modifier.
    if(endpos) then trace.endpos = (endpos - self.Tr.HitNormal * 7) end
    trace.filter = { self.Owner, self.Weapon }

    self.Tr = nil
    self.Tr = util.TraceLine( trace )
end



function SWEP:StartAttack()


    if not self:CanPrimaryAttack() then return end

    --Get begining and end poins of trace.
    local gunPos = self.Owner:GetShootPos() --Start of distance trace.
    local disTrace = self.Owner:GetEyeTrace() --Store all results of a trace in disTrace.
    local hitPos = disTrace.HitPos --Stores Hit Position of disTrace.

    --Calculate Distance
    --Thanks to rgovostes for this code.
    local x = (gunPos.x - hitPos.x)^2;
    local y = (gunPos.y - hitPos.y)^2;
    local z = (gunPos.z - hitPos.z)^2;
    local distance = math.sqrt(x + y + z);

    --Only latches if distance is less than distance CVAR
    local distanceCvar = GetConVarNumber("grapple_distance")
    inRange = false
    if distance <= distanceCvar then
        inRange = true
    end

    if inRange then
        if (SERVER) then

            if (!self.Beam) then --If the beam does not exist, draw the beam.
            if not self:CanPrimaryAttack() then return end
            --grapple_beam
            self.Beam = ents.Create( "trace1" )
            self.Beam:SetPos( self.Owner:GetShootPos() )
            self.Beam:Spawn()
            self:TakePrimaryAmmo(1)

            end

            self.Beam:SetParent( self.Owner )
            self.Beam:SetOwner( self.Owner )

        end


        self:DoTrace()
        self.speed = 10000 --Rope latch speed. Was 3000.
        self.startTime = CurTime()
        self.endTime = CurTime() + self.speed
        self.dtfix = -1

        if (SERVER && self.Beam) then
        self.Beam:GetTable():SetEndPos( self.Tr.HitPos )
        end

        self:UpdateAttack()

        self.Weapon:EmitSound( sndPowerDown )
    else
        --Play A Sound
        self.Weapon:EmitSound( sndTooFar )
    end
end

function SWEP:UpdateAttack()

    -- if not self:CanPrimaryAttack() then return end
    if !self.Tr or self:Clip1() < 1 then return end
    self.Owner:LagCompensation( true )


    if (!endpos) then endpos = self.Tr.HitPos end

    if (SERVER && self.Beam) then
    self.Beam:GetTable():SetEndPos( endpos )
    end

    lastpos = endpos



    if ( self.Tr.Entity:IsValid() ) then

        endpos = self.Tr.Entity:GetPos()
        if ( SERVER and IsValid(self.Beam) ) then
            self.Beam:GetTable():SetEndPos( endpos )
        end

    end

    local vVel = (endpos - self.Owner:GetPos())
    local Distance = endpos:Distance(self.Owner:GetPos())

    local et = (self.startTime + (Distance/self.speed))
    if(self.dtfix != 0) then
    self.dtfix = (et - CurTime()) / (et - self.startTime)
    end
    if(self.dtfix < 0) then
        self.Weapon:EmitSound( sndPowerUp )
        self.dtfix = 0
    end

    if(self.dtfix == 0) then
        zVel = self.Owner:GetVelocity().z
        vVel = vVel:GetNormalized()*(math.Clamp(Distance,0,25))
        if( SERVER ) then
            if  self.NextAmmoTake < CurTime() then
                self:TakePrimaryAmmo(1)
                self.NextAmmoTake = CurTime() + 1
                self.NextAmmoGive = CurTime() + 1.2
            end

            local gravity = GetConVarNumber("sv_gravity")

            vVel:Add(Vector(0,0,(gravity/50)*1.5))

            if(zVel < 0) then
                vVel:Sub(Vector(0,0,zVel/100))
            end
            self.Owner:SetVelocity(vVel)
        end
    end

    endpos = nil

    self.Owner:LagCompensation( false )

end

function SWEP:EndAttack( shutdownsound )

    if ( shutdownsound ) then
        self.Weapon:EmitSound( sndPowerDown )
    end

    if ( CLIENT ) then return end
    if ( !self.Beam ) then return end
    if( IsValid( self.Beam) ) then
        self.Beam:Remove()
    end
    self.Beam = nil

end

function SWEP:Deploy()
    self:SetNWString("shootmode","Fire")
    self:SetNWString("shootmode2","Ice")
    self.BaseClass.Deploy(self)
end

if CLIENT then
    function SWEP:DrawHUD()
        local shotttext = "LMB: "..self:GetNWString("shootmode","Fire").."\nLMB+E to change"
        surface.SetFont( "ChatFont" );
        local size_x, size_y = surface.GetTextSize( shotttext );

        draw.RoundedBox( 6, ScrW( ) / 2 - size_x / 2 - 105, ScrH( ) - 100, size_x + 15, size_y + 10, Color( 0, 0, 0, 150 ) );
        draw.DrawText( shotttext , "ChatFont", ScrW( ) / 2 - 100, ScrH( ) - 100 + 5, Color( 255, 30, 30, 255 ), TEXT_ALIGN_CENTER );
        self.BaseClass.DrawHUD(self)

        local shotttext = "RMB: "..self:GetNWString("shootmode2","Ice").."\nRMB+E to change"
        surface.SetFont( "ChatFont" );
        local size_x, size_y = surface.GetTextSize( shotttext );

        draw.RoundedBox( 6, ScrW( ) / 2 - size_x / 2 + 105, ScrH( ) - 100, size_x + 15, size_y + 10, Color( 0, 0, 0, 150 ) );
        draw.DrawText( shotttext , "ChatFont", ScrW( ) / 2 + 110, ScrH( ) - 100 + 5, Color( 255, 30, 30, 255 ), TEXT_ALIGN_CENTER );
        self.BaseClass.DrawHUD(self)
    end
end
