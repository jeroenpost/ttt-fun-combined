if SERVER then
    AddCSLuaFile(  )
end
if CLIENT then

    SWEP.Author 		= "GreenBlack"

    SWEP.SlotPos 	= 1
    SWEP.Icon = "vgui/ttt_fun_killicons/golden_deagle.png"
end
SWEP.PrintName	= "A Gun"
SWEP.Slot		= 1
SWEP.HoldType 		= "pistol"
SWEP.Base			= "gb_base"
SWEP.Spawnable 		= true
SWEP.AdminSpawnable = true
SWEP.Kind 			= WEAPON_PISTOL
SWEP.Primary.Ammo   = "AlyxGun"
SWEP.Primary.Recoil	= 1
SWEP.Primary.Damage = 45
SWEP.Primary.Delay 	= 0.40
SWEP.Primary.Cone 	= 0.001
SWEP.Primary.ClipSize 		= 40
SWEP.Primary.ClipMax 		= 120
SWEP.Primary.DefaultClip 	= 40
SWEP.Primary.NumShots		= 1
SWEP.Primary.Automatic 	= true
SWEP.HeadshotMultiplier = 2
SWEP.AutoSpawnable      = false
SWEP.AmmoEnt 		= "item_ammo_revolver_ttt"
--SWEP.Primary.Sound	= "gdeagle/gdeagle-1.mp3"
SWEP.Primary.Sound	= Sound("gdeagle/gdeagle-1.mp3")
SWEP.Primary.SoundLevel = 400
SWEP.ViewModel = "models/weapons/v_357.mdl"
SWEP.WorldModel = "models/weapons/w_357.mdl"
SWEP.ShowWorldModel = true
SWEP.ShowViewModel = true
SWEP.ViewModelFlip = false

SWEP.LimitedStock = true
SWEP.InLoadoutFor = nil
SWEP.AllowDrop 	  = true
SWEP.IronSightsPos = Vector(1.14, -3.951, 0.736)
SWEP.IronSightsAng = Vector(0, 0, 0)
SWEP.IronSightsPos 	= Vector( 3.8, -1, 1.6 )
--SWEP.ViewModelFOV = 49


function SWEP:Deploy()
    self.Weapon:EmitSound("gdeagle/gdeagledeploy.mp3")

end

function SWEP:Reload()

    if self.ReloadingTime and CurTime() <= self.ReloadingTime then return end
    self:SetIronsights( false )
    if ( self:Clip1() < self.Primary.ClipSize and self.Owner:GetAmmoCount( self.Primary.Ammo ) > 0 ) then

        self:DefaultReload( ACT_VM_RELOAD )
        local AnimationTime = self.Owner:GetViewModel():SequenceDuration()
        self.ReloadingTime = CurTime() + AnimationTime
        self:SetNextPrimaryFire(CurTime() + AnimationTime)
        self:SetNextSecondaryFire(CurTime() + AnimationTime)
        self.Weapon:EmitSound("gdeagle/gdeaglereload.wav")

    end

end
function SWEP:PrimaryAttack(worldsnd)
    self.Weapon:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
    if not self:CanPrimaryAttack() then return end
    if not worldsnd then
        self.Weapon:EmitSound( self.Primary.Sound, self.Primary.SoundLevel )
    elseif SERVER then
        WorldSound(self.Primary.Sound, self:GetPos(), self.Primary.SoundLevel)
    end
    self:ShootBullet( self.Primary.Damage, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )

    if ( self.Owner:IsValid() ) then
        local tr = self.Owner:GetEyeTrace()
        local effectdata = EffectData()
        effectdata:SetOrigin(tr.HitPos)
        effectdata:SetNormal(tr.HitNormal)
        effectdata:SetScale(1)
        -- util.Effect("effect_mad_ignition", effectdata)
        util.Decal("FadingScorch", tr.HitPos + tr.HitNormal, tr.HitPos - tr.HitNormal)

        local tracedata = {}
        tracedata.start = tr.HitPos
        tracedata.endpos = Vector(tr.HitPos.x, tr.HitPos.y, tr.HitPos.z - 10)
        tracedata.filter = tr.HitPos
        local tracedata = util.TraceLine(tracedata)
        if SERVER then
            if tracedata.HitWorld then
                local flame = ents.Create("env_fire");
                flame:SetPos(tr.HitPos + Vector( 0, 0, 1 ));
                flame:SetKeyValue("firesize", "10");
                flame:SetKeyValue("fireattack", "10");
                flame:SetKeyValue("StartDisabled", "0");
                flame:SetKeyValue("health", "10");
                flame:SetKeyValue("firetype", "0");
                flame:SetKeyValue("damagescale", "5");
                flame:SetKeyValue("spawnflags", "128");
                flame:SetPhysicsAttacker(self.Owner)
                flame:SetOwner( self.Owner )
                flame:Spawn();
                flame:Fire("StartFire",0);
            end
        end
        self:TakePrimaryAmmo( 1 )
        local owner = self.Owner
        --if not ValidEntity(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

        owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )
    end
end





local function RunIgniteTimer(ent, timer_name)
    if IsValid(ent) and ent:IsOnFire() then
        if ent:WaterLevel() > 0 then
            ent:Extinguish()
        elseif CurTime() > ent.burn_destroy then
            ent:SetNotSolid(true)
            ent:Remove()
        else
            -- keep on burning
            return
        end
    end

    timer.Destroy(timer_name) -- stop running timer
end

local SendScorches

if CLIENT then
    local function ReceiveScorches(um)
        local ent = um:ReadEntity()
        local num = um:ReadChar()
        for i=1, num do
            util.PaintDown(um:ReadVector(), "FadingScorch", ent)
        end

        if IsValid(ent) then
            util.PaintDown(ent:LocalToWorld(ent:OBBCenter()), "Scorch", ent)
        end
    end
    usermessage.Hook("flare_scorch", ReceiveScorches)
else
    -- it's sad that decals are so unreliable when drawn serverside, failing to
    -- draw more often than they work, that I have to do this
    SendScorches = function(ent, tbl)
        umsg.Start("flare_scorch")
        umsg.Entity(ent)
        umsg.Char(#tbl)
        for _, p in pairs(tbl) do
            umsg.Vector(p)
        end
        umsg.End()
    end
    usermessage.Hook("flare_scorch") -- pools it
end


local function ScorchUnderRagdoll(ent)
    if SERVER then
        local postbl = {}
        -- small scorches under limbs
        for i=0, ent:GetPhysicsObjectCount()-1 do
            local subphys = ent:GetPhysicsObjectNum(i)
            if IsValid(subphys) then
                local pos = subphys:GetPos()
                util.PaintDown(pos, "FadingScorch", ent)

                table.insert(postbl, pos)
            end
        end

        SendScorches(ent, postbl)
    end

    -- big scorch at center
    local mid = ent:LocalToWorld(ent:OBBCenter())
    mid.z = mid.z + 25
    util.PaintDown(mid, "Scorch", ent)
end


function IgniteTarget(att, path, dmginfo)
    local ent = path.Entity
    if not IsValid(ent) then return end

    if CLIENT and IsFirstTimePredicted() then
        if ent:GetClass() == "prop_ragdoll" then
            ScorchUnderRagdoll(ent)
        end
        return
    end

    if SERVER then

        local dur = ent:IsPlayer() and 5 or 10

        -- disallow if prep or post round
        if ent:IsPlayer() and (not GAMEMODE:AllowPVP()) then return end

        ent:Ignite(dur, 100)

        ent.ignite_info = {att=dmginfo:GetAttacker(), infl=dmginfo:GetInflictor()}

        if ent:IsPlayer() then
            timer.Simple(dur + 0.1, function()
                if IsValid(ent) then
                    ent.ignite_info = nil
                end
            end)

        elseif ent:GetClass() == "prop_ragdoll" then
            ScorchUnderRagdoll(ent)

            local burn_time = 3
            local tname = Format("ragburn_%d_%d", ent:EntIndex(), math.ceil(CurTime()))

            ent.burn_destroy = CurTime() + burn_time

            timer.Create(tname,
                0.1,
                math.ceil(1 + burn_time / 0.1), -- upper limit, failsafe
                function()
                    RunIgniteTimer(ent, tname)
                end)
        end
    end
end

function SWEP:ShootFlare()
    local cone = self.Primary.Cone
    local bullet = {}
    bullet.Num       = 1
    bullet.Src       = self.Owner:GetShootPos()
    bullet.Dir       = self.Owner:GetAimVector()
    bullet.Spread    = Vector( cone, cone, 0 )
    bullet.Tracer    = 1
    bullet.Force     = 2
    bullet.Damage    = self.Primary.Damage
    bullet.TracerName = self.Tracer
    bullet.Callback = IgniteTarget

    self.Owner:FireBullets( bullet )
end

SWEP.flares = 5
SWEP.NextSec = 0

function SWEP:SecondaryAttack()


    if self.flares < 1 or self.NextSec > CurTime() then return end
    self.NextSec = CurTime() + 5

    self.Weapon:EmitSound( "Weapon_USP.SilencedShot"  )

    self.Weapon:SendWeaponAnim( ACT_VM_PRIMARYATTACK )

    self:ShootFlare()

    self.flares = self.flares - 1

    if IsValid(self.Owner) then
        self.Owner:SetAnimation( PLAYER_ATTACK1 )

        self.Owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )
    end

    if ( (game.SinglePlayer() && SERVER) || CLIENT ) then
    self.Weapon:SetNWFloat( "LastShootTime", CurTime() )
    end
end




