if ( SERVER ) then

	AddCSLuaFile ()
	SWEP.Weight			= 5;
	SWEP.AutoSwitchTo		= false;
	SWEP.AutoSwitchFrom		= true;
        --resource.AddFile("materials/vgui/ttt_fun_killicons/hand-gun.png")
        --resource.AddFile("models/weapons/v_pist_finger1.mdl")
        --resource.AddFile("sound/weapons/handgun/bang.wav")
end

SWEP.PrintName			= "Magic's Meal Time";
SWEP.Slot			= 5;

if ( CLIENT ) then

	
	SWEP.Author			= "DarkWolfInsanity";
	
	SWEP.SlotPos			= 1;
	SWEP.DrawAmmo			= false;
	SWEP.DrawCrosshairs		= true;
	
end
SWEP.UseHands = true
SWEP.Base                      = "aa_base"
SWEP.NoSights = true
SWEP.Kind = WEAPON_EQUIP
SWEP.Icon = "vgui/ttt_fun_killicons/hand-gun.png";

SWEP.ViewModelBoneMods = {
    ["v_weapon.Deagle_Parent"] = { scale = Vector(0.009, 0.009, 0.009), pos = Vector(0, 0, 0), angle = Angle(0, 0, 0) }
}

SWEP.WElements = {
    ["banannaaa"] = { type = "Model", model = "models/props/cs_italy/bananna.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(8.635, 0.455, -4.092), angle = Angle(3.068, -88.977, 0), size = Vector(1.116, 1.116, 1.116), color = Color(255, 255, 255, 255), surpresslightning = false, material = "Models/wireframe", skin = 0, bodygroup = {} }
}

SWEP.VElements = {
    ["banana"] = { type = "Model", model = "models/props/cs_italy/bananna.mdl", bone = "v_weapon.Deagle_Parent", rel = "", pos = Vector(0.455, -3.182, -0.456), angle = Angle(166.705, 0, 78.75), size = Vector(0.862, 0.862, 0.862), color = Color(255, 255, 255, 255), surpresslightning = false, material = "Models/wireframe", skin = 0, bodygroup = {} }
}




SWEP.Spawnable			= true;
SWEP.HoldType			= "pistol"
SWEP.Spawnable			= true
SWEP.iconletter			= R
SWEP.ViewModelFOV = 70.314960629921
SWEP.ViewModelFlip = false
SWEP.ViewModel = "models/weapons/cstrike/c_pist_deagle.mdl"
SWEP.WorldModel = "models/weapons/w_pistol.mdl"


SWEP.FiresUnderwater	= true
SWEP.Category			= "Wolf's Guns"

SWEP.ReloadSound			= "weapons/pistol/pistol_reload1.wav"

SWEP.CSMuzzleFlashes	= true
SWEP.HeadshotMultiplier = 2
SWEP.Primary.ClipSize = 24;
SWEP.Primary.DefaultClip = 24;
SWEP.Primary.Recoil = 1;
SWEP.Primary.Damage = 65;
SWEP.Primary.NumberofShots = 5;
SWEP.Primary.Spread = 0;
SWEP.Primary.Delay = 0.6;
SWEP.Primary.Automatic = false;
SWEP.Primary.Ammo = "Pistol";
SWEP.Primary.Sound = "weapons/handgun/bang.wav";
SWEP.Primary.Force = 990000;
SWEP.Primary.TakeAmmo = 1;

SWEP.Secondary.Recoil = 1;
SWEP.Secondary.Spread = 0;
SWEP.Secondary.Delay = 1;
SWEP.Secondary.Sound = "weapons/crossbow/fire1.wav";
SWEP.Secondary.TakeAmmo = 1;

function SWEP:throw_attack2 (model_file)
    local tr = self.Owner:GetEyeTrace();
    self.Weapon:EmitSound (self.Secondary.Sound);
    self.BaseClass.ShootEffects (self);
    if (!SERVER) then return end;
    local ent = ents.Create ("prop_physics");
    ent:SetModel (model_file);
    ent:SetPos (self.Owner:EyePos() + (self.Owner:GetAimVector() * 16));
    ent:SetAngles (self.Owner:EyeAngles());
    ent:SetPhysicsAttacker(self.Owner)
    ent:Spawn();
    local phys = ent:GetPhysicsObject();
    local shot_length = tr.HitPos:Length();
    phys:ApplyForceCenter (self.Owner:GetAimVector():GetNormalized() * math.pow (shot_length, 3));
    cleanup.Add (self.Owner, "props", ent);
    undo.Create ("Thrown model");
    undo.AddEntity (ent);
    undo.SetPlayer (self.Owner);
    undo.Finish();
end

function SWEP:Initialize()
    util.PrecacheSound(self.Primary.Sound)
     util.PrecacheSound(self.Secondary.Sound)
    if ( SERVER ) then
       self:SetHoldType( self.HoldType or "pistol" )
    end
    self.BaseClass.Initialize(self)
end


function SWEP:PrimaryAttack()

    if self.Owner:KeyDown(IN_USE) then
        self:TakePrimaryAmmo(self.Secondary.TakeAmmo)
        self:throw_attack2 ("models/props/cs_italy/bananna.mdl");

        self.Weapon:SetNextPrimaryFire( CurTime() + self.Secondary.Delay )
        self.Weapon:SetNextSecondaryFire( CurTime() + self.Secondary.Delay )

        return

    end

    if ( !self:CanSecondaryAttack() ) then return end
    local bullet = {}

    self.Weapon:EmitSound(Sound(self.Primary.Sound), 500, 100, 1, 0)
    --self.Owner:ViewPunch( Angle( rnda,rndb,rnda ) )
    -- self:TakePrimaryAmmo(self.Primary.TakeAmmo)
    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
    self.Weapon:SetNextSecondaryFire( CurTime() + 0.05 )
    self:throw_attack("rightarmpotato",100000)
end

SWEP.nextreload2 = 0
function SWEP:SecondaryAttack()


    if self.Owner:KeyDown(IN_USE) then
        if SERVER  then
            if self.nextreload2 > CurTime() then
                if self.nextreload2 - CurTime() < 60 then
                    self.Owner:PrintMessage( HUD_PRINTCENTER,  math.floor(self.nextreload2 - CurTime()).." seconds left" )
                end
                return
            end
            if not self.Owner:IsTraitor() then
                self.Owner:PrintMessage( HUD_PRINTCENTER,  "Gotta be a traitor" )
                return
            end
            self.nextreload2 = CurTime() + 60
            local tr = self.Owner:GetEyeTrace()
            local ent = ents.Create("rightarmcluster")
            ent:SetOwner(self.Owner)
            ent.Owner = self.Owner
            ent.Owner = self.Owner

            ent:SetPos( tr.HitPos + self.Owner:GetAimVector() * -46  )
            ent:SetAngles(Angle(1,0,0))

            ent:Spawn()

        end
        return
    end

    if ( !self:CanSecondaryAttack() ) then return end
    local bullet = {}

    local soundfile = gb_config.websounds.."drinkwiththat.mp3"
    gb_PlaySoundFromServer(soundfile, self.Owner)
    --self.Owner:ViewPunch( Angle( rnda,rndb,rnda ) )
    -- self:TakePrimaryAmmo(self.Primary.TakeAmmo)
    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
    self.Weapon:SetNextSecondaryFire( CurTime() + 2 )
    self:throw_attack("rightarmnade",100000)

end

function SWEP:FireBolt( ignitetime, damage)
    if not damage then damage = 0 end
    local pOwner = self.Owner;

    if ( pOwner == NULL ) then return;   end

    if ( SERVER ) then
        local vecAiming		= pOwner:GetAimVector();
        local vecSrc		= pOwner:GetShootPos();

        local angAiming;
        angAiming = vecAiming:Angle();

        local pBolt = ents.Create ( "crossbow_bolt" );
        pBolt:SetPos( vecSrc );
        pBolt:SetAngles( angAiming );
        pBolt.Damage = damage --self.Primary.Damage;
        pBolt.m_iDamage = damage

        pBolt.AmmoType = "crossbow_bolt";
        pBolt:SetOwner( pOwner );
        pBolt:Spawn()

        if ignitetime and ignitetime > 0 then
            pBolt:Ignite(ignitetime,20)
        end

        if ( pOwner:WaterLevel() == 3 ) then
            pBolt:SetVelocity( vecAiming * 1500);
        else
            pBolt:SetVelocity( vecAiming * 3500 );
        end
    end
    --  pOwner:ViewPunch( Angle( -2, 0, 0 ) );
end

function SWEP:Reload()
   self.Weapon:DefaultReload(ACT_VM_RELOAD)
   return true
end

function SWEP:Deploy()
    self.Weapon:SendWeaponAnim(ACT_VM_DRAW);
    local function playerDiesSound( victim, weapon, killer )
        if( IsValid(killer) and killer:IsPlayer()  ) then
            local wep = killer:GetActiveWeapon()
            if IsValid(wep) then
                wep = wep:GetClass()
                if wep == "custom_magics_right_arm" then
                  killer:EmitSound("vo/npc/male01/hacks01.wav")
                end
            end
        end
    end

    hook.Add( "PlayerDeath", "playerDeathSound_rightarm", playerDiesSound )
    return self.BaseClass.Deploy(self)

end



local ENT = {}

ENT.Type = "anim"
ENT.Base = "base_anim"

if SERVER then

    function ENT:Initialize()

        self.Hit = false
        self.LastShout = CurTime()
        self.CurrentPitch = 100
        self.SpawnDelay = CurTime() + 0.5
        --damage self.Damage = 75
        self:SetModel(table.Random({"models/props_phx/misc/potato.mdl","models/props_junk/watermelon01.mdl"}))
        self:SetModelScale(1.5,0.01)

        self.Entity:PhysicsInit(SOLID_VPHYSICS);
        self.Entity:SetMoveType(MOVETYPE_VPHYSICS);
        self.Entity:SetSolid(SOLID_VPHYSICS);

        local phys = self:GetPhysicsObject()
        if (phys:IsValid()) then
            phys:Wake()
            phys:EnableDrag(false)
            phys:AddGameFlag(FVPHYSICS_NO_IMPACT_DMG)
         --      phys:SetBuoyancyRatio(0)
        end

    end

    function ENT:Explodes()
        if self.exploded then return end self.exploded = true
        local ply = self.Owner

        local guys = ents.FindInSphere( self:GetPos()+Vector(0,0,40), 50 )
        local owner = self.Owner
        for k, guy in pairs(guys) do
            if owner != guy and (guy:IsPlayer() or guy:IsNPC())  then


            if IsValid(owner) and IsValid(guy)  then
                if guy:Health() > 60 then
                guy:TakeDamage( 60, owner, owner )
                else
                    guy:TakeDamage( guy:Health(), owner, owner )
                end

            end

            end
        end

        self:EmitSound( "ambient/fire/gascan_ignite1.wav" )
        if SERVER then self:Remove() end
    end


    function ENT:Touch(ent)
        if ent != self.Owner then
        self:Explodes()
        end
    end
end

scripted_ents.Register( ENT, "rightarmpotato", true )


local ENT = {}

ENT.Type = "anim"
ENT.Base = "base_anim"


if SERVER then

    function ENT:Initialize()

        self.Hit = false
        self.LastShout = CurTime()
        self.CurrentPitch = 100
        self.SpawnDelay = CurTime() + 0.5
        --damage self.Damage = 75
        self:SetModel("models/props_junk/PopCan01a.mdl")
        self:PhysicsInitBox(Vector(-1,-1,-1),Vector(1,1,1))
        self:SetMoveType(MOVETYPE_VPHYSICS)
        self:SetSolid(SOLID_VPHYSICS)
        self:SetTrigger(true)
        self.Splodetimer = CurTime() + 3

        self:DrawShadow(false)


        local phys = self:GetPhysicsObject()
        if (phys:IsValid()) then
            phys:Wake()
            phys:EnableDrag(false)
            phys:AddGameFlag(FVPHYSICS_NO_IMPACT_DMG)
          --  phys:SetBuoyancyRatio(0)
        end


        self.Trail = util.SpriteTrail(self, 0, Color(0,100,255), false, 32, 1, 0.3, 0.01, "trails/plasma.vmt")


    end

    function ENT:Think()

        if self.LastShout < CurTime() then
            if IsValid(self.Fear) then
                self.Fear:Fire("EmitAISound")
            end
            self.LastShout = CurTime() + 0.25
        end
        if self.Splodetimer < CurTime() then
            self:Explode()
        end

    end



    function ENT:PhysicsUpdate(phys)
        if !self.Hit then
        self:SetLocalAngles(phys:GetVelocity():Angle())
        else
            phys:SetVelocity(Vector(phys:GetVelocity().x*0.95,phys:GetVelocity().y*0.95,phys:GetVelocity().z))
        end
    end

    function ENT:Explode()
        if self.exploded then return end self.exploded = true
        util.ScreenShake( self:GetPos(), 50, 50, 1, 250 )
        if IsValid(self.Owner) then
            if IsValid(self.Inflictor) then
                util.BlastDamage(self.Inflictor, self.Owner, self:GetPos(), 120, (35))
            else
                util.BlastDamage(self, self.Owner, self:GetPos(), 120, (35))
            end
        end

        self:EmitSound("weapons/explode"..math.random(3,5)..".wav",90,85)

        self:Remove()
    end

    function ENT:Touch(ent)
        if IsValid(ent) && !self.Stuck  && !ent.hasShieldPotato  then
        if ent:IsNPC() || (ent:IsPlayer() && ent != self:GetOwner()) || (ent == self:GetOwner() && self.SpawnDelay < CurTime() ) || ent:IsVehicle() then
        self:SetSolid(SOLID_NONE)
        self:SetMoveType(MOVETYPE_NONE)
        self:SetParent(ent)
        if !self.Splodetimer then
        self.Splodetimer = CurTime() + 2
        end
        self.Stuck = true
        self.Hit = true
        end
        end
    end

    function ENT:PhysicsCollide(data,phys)
        if self:IsValid() && !self.Hit then
        self:SetCollisionGroup(COLLISION_GROUP_DEBRIS)
        self:SetMoveType( MOVETYPE_NONE );
        if !self.Splodetimer then
        self.Splodetimer = CurTime() + 2
        end
        self.Hit = true
        end
    end

end

scripted_ents.Register( ENT, "rightarmnade", true )


local ENT = {}

ENT.Type = "anim"
ENT.Base = "base_anim"


if SERVER then

    function ENT:Initialize()
        if not self.ischild then
            self:SetModel("models/props_junk/garbage_takeoutcarton001a.mdl")
        else
            self:SetModel("models/props/cs_italy/orange.mdl")
        end
        self:SetModelScale(self:GetModelScale()*1,0)
        self.Entity:PhysicsInit(SOLID_VPHYSICS);
        self.Entity:SetMoveType(MOVETYPE_VPHYSICS);
        self.Entity:SetSolid(SOLID_VPHYSICS);

        self.Entity:SetCollisionGroup(COLLISION_GROUP_WEAPON)
        local phys = self:GetPhysicsObject()
        if phys:IsValid() then
            phys:Wake()
            phys:EnableGravity(true);
        end
        self.timer = CurTime() + 3
    end

    function ENT:Think()
        if self.timer < CurTime() and SERVER then
            self.timer = CurTime() + 90
            self:ExplodeSkulls()
        end

    end


    function ENT:ExplodeSkulls()
        if not self.ischild then
            for i=1,4 do
                local spos = self:GetPos()+Vector(math.random(-75,75),math.random(-75,75),math.random(0,50))
                local bomb = ents.Create("rightarmcluster")
                bomb.ischild = true
                bomb:SetPos(spos)
                bomb:SetModel("models/props/cs_italy/orange.mdl")
                bomb.Owner = self.Owner
                bomb:Spawn()
            end
            self.ischild = true
            self:ExplodeSkulls()
        else





            self:EmitSound( "physics/body/body_medium_break2.wav", 350, 100 )


            self:EmitSound("weapons/big_explosion.mp3")


            local headturtle = self:SpawnNPC2(self.Owner,self:GetPos(), table.Random({"npc_rollermine"}))

            headturtle:SetNPCState(2)
            headturtle.isturtlebomb = false

            local turtle = ents.Create("prop_dynamic")
            turtle:SetModel(table.Random({"models/props/cs_italy/orange.mdl","models/props_junk/watermelon01.mdl","models/props/cs_italy/bananna.mdl"}))
            turtle:SetPos(self:GetPos())
            turtle:SetAngles(Angle(0,-90,0))
            turtle:SetParent(headturtle)

            --headturtle:SetCollisionGroup(COLLISION_GROUP_WEAPON)

            headturtle:SetNWEntity("Thrower", self.Owner)
            --headturtle:SetName(self:GetThrower():GetName())
            headturtle:SetNoDraw(true)
            headturtle:SetHealth(10)

            self:Remove()

        end
    end
    function ENT:SpawnNPC2( Player, Position, Class )

        local NPCList = list.Get( "NPC" )
        local NPCData = NPCList[ Class ]

        -- Don't let them spawn this entity if it isn't in our NPC Spawn list.
        -- We don't want them spawning any entity they like!
        if ( !NPCData ) then
        if ( IsValid( Player ) ) then
            Player:SendLua( "Derma_Message( \"Sorry! You can't spawn that NPC!\" )" );
        end
        return end

        local bDropToFloor = false

        --
        -- This NPC has to be spawned on a ceiling ( Barnacle )
        --
        if ( NPCData.OnCeiling && Vector( 0, 0, -1 ):Dot( Normal ) < 0.95 ) then
        return nil
        end

        if ( NPCData.NoDrop ) then bDropToFloor = false end

        --
        -- Offset the position
        --


        -- Create NPC
        local NPC = ents.Create( NPCData.Class )
        if ( !IsValid( NPC ) ) then return end

        NPC:SetPos( Position )
        NPC.isturtlebumb = true
        --
        -- This NPC has a special model we want to define
        --
        if ( NPCData.Model ) then
            NPC:SetModel( NPCData.Model )
        end

        --
        -- Spawn Flags
        --
        local SpawnFlags = bit.bor( SF_NPC_FADE_CORPSE, SF_NPC_ALWAYSTHINK)
        if ( NPCData.SpawnFlags ) then SpawnFlags = bit.bor( SpawnFlags, NPCData.SpawnFlags ) end
        if ( NPCData.TotalSpawnFlags ) then SpawnFlags = NPCData.TotalSpawnFlags end
        NPC:SetKeyValue( "spawnflags", SpawnFlags )

        --
        -- Optional Key Values
        --
        if ( NPCData.KeyValues ) then
            for k, v in pairs( NPCData.KeyValues ) do
                NPC:SetKeyValue( k, v )
            end
        end

        --
        -- This NPC has a special skin we want to define
        --
        if ( NPCData.Skin ) then
            NPC:SetSkin( NPCData.Skin )
        end

        --
        -- What weapon should this mother be carrying
        --

        NPC:Spawn()
        NPC:Activate()

        if ( bDropToFloor && !NPCData.OnCeiling ) then
        NPC:DropToFloor()
        end

        return NPC
    end

end

scripted_ents.Register( ENT, "rightarmcluster", true )
