SWEP.Base = "gb_base"
SWEP.HoldType = "pistol"
SWEP.ViewModelFOV = 74
SWEP.ViewModelFlip = true
SWEP.ViewModel  = "models/weapons/v_pist_p228.mdl"
SWEP.WorldModel = "models/weapons/w_pist_p228.mdl"
SWEP.ShowViewModel = true
SWEP.ShowWorldModel = false

SWEP.UseHands = false
SWEP.Kind = WEAPON_PISTOL


SWEP.Sound = { Sound("CSTM_Deagle") }
SWEP.Sounds = {Sound("CSTM_Deagle")}

SWEP.PrintName            = "Jeremy's Wrath"
SWEP.Slot                = 1
SWEP.SlotPos            = 1
SWEP.DrawAmmo            = true
SWEP.DrawCrosshair        = true
SWEP.Weight                = 5
SWEP.AutoSwitchTo        = false
SWEP.AutoSwitchFrom        = false
SWEP.Author            = ""
SWEP.Contact        = ""
SWEP.Purpose        = ""


SWEP.AutoSpawnable      = false
SWEP.Primary.Damage        = 65
SWEP.Primary.ClipSize        = 500
SWEP.Primary.DefaultClip    = 500
SWEP.Primary.Automatic        = false
SWEP.Primary.Ammo            = "XBowBolt"
SWEP.Primary.Delay            = 0.30
SWEP.Secondary.Delay            = 3
SWEP.Secondary.ClipSize        = 2
SWEP.Secondary.DefaultClip    = 2
SWEP.Secondary.Automatic    = false
SWEP.Secondary.Ammo            = "XBowBolt"
SWEP.AmmoEnt = ""
SWEP.HeadshotMultiplier = 1.2

SWEP.SecondaryProps = {

    "models/props_c17/oildrum001_explosive.mdl",
    "models/props_c17/oildrum001_explosive.mdl",
}
SWEP.SecondaryProps2 = {
    "models/props_c17/oildrum001_explosive.mdl",
    "models/Combine_Helicopter/helicopter_bomb01.mdl",
    "models/props_junk/TrafficCone001a.mdl",
    "models/props_junk/metal_paintcan001a.mdl",
    "models/Gibs/HGIBS.mdl",
    "models/Gibs/Fast_Zombie_Legs.mdl",
    "models/props_c17/doll01.mdl",
    "models/props_lab/huladoll.mdl",
    "models/props_lab/huladoll.mdl",
    "models/props_lab/desklamp01.mdl",
    "models/props_junk/garbage_glassbottle003a.mdl",
    "models/weapons/w_shot_gb1014.mdl",

}
SWEP.Icon = "vgui/ttt_fun_killicons/nanners_gun.png"
SWEP.NextSwapNom = 0
function SWEP:PrimaryAttack()

    if self.Owner:KeyDown(IN_USE) then
        local tr = util.TraceLine({start = self.Owner:EyePos(), endpos = self.Owner:EyePos() + self.Owner:EyeAngles():Forward() * 80000, filter = self.Owner})

        local ply = self.Owner
        self.NextReload = CurTime() + 0.8
        if IsValid(tr.Entity) then
            if tr.Entity.player_ragdoll then
                self.NextSwapNom = CurTime() + 30
                local ent = tr.Entity

                local pos = ent:GetPos() + Vector(0,0,10)
                local pos2 = ply:GetPos() + Vector(0,0,10)

                for i=0,ent:GetPhysicsObjectCount()-1 do
                    ent:GetPhysicsObjectNum(i):SetPos(pos2)
                end
                ply:SetPos(pos)
                tr.Entity:SetModel("models/props_lab/cactus.mdl")

                local ef = EffectData()
                ef:SetEntity(self.Owner)
                util.Effect("swap_effect", ef,true,true)
                self.Owner:EmitSound("ambient/energy/zap5.wav")
                ent:SetPos(pos2)

            end
        end

        return
    end


    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
    -- self:TakePrimaryAmmo( 1 )
    self:ShootBullet( 55, 1, 1, 0.002 )
    timer.Simple(0.05,function()
        self:throw_attack ( "models/props_lab/cactus.mdl", Sound(table.Random( self.Sound) ), 3);
        end)

end

SWEP.Headcrabs = 0
SWEP.Headcrabs2 = 0
SWEP.NextSecond = 0

function SWEP:Reload()
    if  self.NextSecond > CurTime() then return end
    if self.Owner:KeyDown(IN_USE) then
        self.Weapon:SetNextSecondaryFire( CurTime() + 5 )

        self.NextSecond = CurTime() + 5
        self.Weapon:SetNextSecondaryFire( CurTime() + self.Secondary.Delay )
        --self:TakePrimaryAmmo( 10 )
        self:throw_attack ( table.Random(self.SecondaryProps) , Sound(table.Random( self.Sounds) ), 10);
        return
    end

    self.Weapon:SetNextSecondaryFire( CurTime() + 5 )

    self.NextSecond = CurTime() + 5
    self.Weapon:SetNextSecondaryFire( CurTime() + self.Secondary.Delay )
    --self:TakePrimaryAmmo( 10 )
    self:throw_attack ( table.Random(self.SecondaryProps2) , Sound(table.Random( self.Sounds) ), 10);
end

function SWEP:OnDrop()
    self:Remove()
end

function SWEP:Infect()
    if SERVER  then

        local tr = self.Owner:GetEyeTrace()
        if ( IsValid(tr.Entity) and (tr.Entity:IsPlayer() or tr.Entity:IsNPC() ))  then


            local ent = tr.Entity
            local weapon = self.Owner:GetActiveWeapon()
            local edata = EffectData()

            edata:SetEntity(ent)
            edata:SetMagnitude(3)
            edata:SetScale(2)

            util.Effect("TeslaHitBoxes", edata)

            if SERVER  then
                ent:PrintMessage( HUD_PRINTCENTER, "You have been infected" )
                local modelsss = { "npc_fastzombie",}
                local randmodel = table.Random( modelsss )

                local oldPos = ent:GetPos()

                timer.Simple(4.9, function()
                    if IsValid( ent ) then
                        spot = ent:GetPos()
                    else
                        spot = oldPos
                    end
                    zed = ents.Create(randmodel)
                end)

                timer.Simple(5,function()
                    if SERVER then
                        if IsValid( ent ) then
                            ent:TakeDamage( 2000, self.Owner, weapon )
                        end
                        --local ply = self.Owner -- The first entity is always the first player
                        zed:SetPos(spot) -- This positions the zombie at the place our trace hit.
                       -- zed:SetModel("models/props_lab/cactus.mdl" )
                        zed:SetHealth( 50 )
                        zed:Spawn() -- This method spawns the zombie into the world, run for your lives! ( or just crowbar it dead(er) )
                       -- zed:SetModel("models/props_lab/cactus.mdl" )
                        zed:SetHealth( 50 )

                        local turtle = ents.Create("prop_dynamic")
                        turtle:SetModel("models/props_lab/cactus.mdl")
                        turtle:SetPos(zed:GetPos()+Vector(10,10,10))
                        turtle:SetAngles(Angle(0,-90,0))
                        turtle:SetModelScale( turtle:GetModelScale() * 2, 0.05)
                        turtle:SetParent(zed)
                        turtle:SetHealth(50)
                        zed:SetNoDraw(true)
                    end
                end) --20
                timer.Create("infectedDamage",0.25,20,function()
                    if IsValid( ent ) then
                        ent:TakeDamage( 3, owner, weapon )
                    end
                end)


            end


        else
            self.Owner:PrintMessage( HUD_PRINTCENTER, "Miss" )
        end
    end

end

SWEP.HadInfect = false

function SWEP:SecondaryAttack()

    if self.Owner:KeyDown(IN_USE) then
        if self.HadInfect then
            self.Owner:PrintMessage( HUD_PRINTCENTER, "Already used" )
            return
        end
        self.HadInfect = true;
        self:Infect()
        return
    end

    if( self.Headcrabs > 1 ) then
        self:Reload()
    return end
    if  self.NextSecond > CurTime() then return end
    if not self:CanPrimaryAttack() then return end
    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
    self.Weapon:SetNextSecondaryFire( CurTime() + 5 )

    self.NextSecond = CurTime() + 5



    if  not SERVER then return end


    self.Owner:EmitSound( "weapons/nanners/10.mp3" )

    timer.Simple(3, function()
        if not IsValid(self) or not IsValid(self.Owner) then return end

        local tr = self.Owner:GetEyeTrace()


        if self.Headcrabs2 == 0 then
            local ent = ents.Create( "npc_clawscanner" )
            ent:SetPos( tr.HitPos + self.Owner:GetAimVector() * 1 )
            ent:SetAngles( tr.HitNormal:Angle() )
            ent:SetHealth( 300 )
            ent:SetPhysicsAttacker(self.Owner)
            ent:Spawn()
            ent:Ignite(100,100)
            ent:EmitSound( "weapons/nanners/10.mp3" )

        else

            local ent = ents.Create( "npc_manhack" )
            ent:SetPos( tr.HitPos + self.Owner:GetAimVector() * 1 )
            ent:SetAngles( tr.HitNormal:Angle() )
            ent:SetHealth( 50 )
            ent:SetPhysicsAttacker(self.Owner)
            ent:Spawn()
            ent:EmitSound( "weapons/nanners/13.mp3" )
            if  self.Owner:GetRoleString() ~= "traitor" then
                ent:SetKeyValue("sk_manhack_melee_dmg", 0)
            end

            local turtle = ents.Create("prop_dynamic")
            turtle:SetModel( "models/props_lab/cactus.mdl")
            turtle:SetPos(ent:GetPos())
            turtle:SetAngles(Angle(0,-90,0))
            turtle:SetModelScale( turtle:GetModelScale() * 10, 0.05)
            turtle:SetParent(ent)
            turtle:SetHealth(1)

            if math.Rand( 1,5) == 4 then
                turtle:Ignite(100,100)
                turtle:SetHealth(75)
            end

            --headturtle:SetCollisionGroup(COLLISION_GROUP_WEAPON)

            ent:SetNWEntity("Thrower", self.Owner)
            --headturtle:SetName(self:GetThrower():GetName())
            ent:SetNoDraw(true)
            ent:SetHealth(15)
            timer.Simple(16,function()
                if IsValid( ent ) then
                    self:ReplaySound( ent )
                end

            end)

        end
        self.Headcrabs2 = self.Headcrabs2 +1
    end)
    self.Headcrabs = self.Headcrabs +1


end

function SWEP:ReplaySound( ent )
    if not IsValid( ent ) then return end
    ent:EmitSound(  "weapons/nanners/13.mp3"  )
    timer.Simple(16,function()
        if IsValid( ent ) then
            self:ReplaySound( ent )
        end

    end)
end



function SWEP:throw_attack (model_file, sound, away)
    local tr = self.Owner:GetEyeTrace();

    if IsValid( tr.Entity )  then
            tr.Entity.hasProtectionSuit= true
    end


    self.BaseClass.ShootEffects (self);
    if (!SERVER) then return end;
    local ent = ents.Create ("prop_physics");
    ent:SetModel (model_file);
    ent:SetPos (self.Owner:EyePos() + (self.Owner:GetAimVector() * 16));
    ent:SetAngles (self.Owner:EyeAngles());
    ent:SetPhysicsAttacker(self.Owner)
    ent:Spawn();
    ent:EmitSound( sound )
    ent:SetCollisionGroup( COLLISION_GROUP_DEBRIS );
    if math.random(1,10) < 2 then
        ent:Ignite(100,100)
    end
    ent:SetHealth(75)
    local phys = ent:GetPhysicsObject();
    local shot_length = tr.HitPos:Length()+10;
    phys:SetMass( 1 )
    phys:ApplyForceCenter (self.Owner:GetAimVector():GetNormalized() * math.pow (shot_length, 3));

    timer.Simple(away,function()
        if IsValid(ent) then
            ent:Remove()
        end
    end)

    cleanup.Add (self.Owner, "props", ent);
    undo.Create ("Thrown model");
    undo.AddEntity (ent);
    undo.SetPlayer (self.Owner);
    undo.Finish();
end