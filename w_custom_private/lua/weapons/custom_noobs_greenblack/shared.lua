if SERVER then
   AddCSLuaFile(  )

--resource.AddFile("materials/models/weapons/v_models/green_black/greenblack1.vmt")
--resource.AddFile("materials/models/weapons/v_models/green_black/greenblack2.vmt")
--resource.AddFile("materials/models/weapons/v_models/green_black/greenblack.vmt")

--resource.AddFile("models/weapons/v_green_black.mdl")
--resource.AddFile("models/weapons/w_green_black.mdl")
--resource.AddFile("sound/weapons/green_black1.mp3")
--resource.AddFile("sound/weapons/green_black2.mp3")
--resource.AddFile("sound/weapons/green_black3.mp3")
--resource.AddFile("sound/weapons/green_black4.mp3")
--resource.AddFile("sound/weapons/green_black5.mp3")
--resource.AddFile("materials/vgui/ttt_fun_killicons/green_black.png")

end
   
SWEP.HoldType			= "pistol"
SWEP.PrintName			= "NooB's GreenBlack"
SWEP.Category			= "TTT-FUN"
SWEP.Slot				= 2

if CLIENT then
   			
   SWEP.Author				= "GreenBlack"
   SWEP.SlotPos			= 2
   SWEP.Icon = "vgui/ttt_fun_killicons/green_black.png"
	killicon.Add( "green_black", "vgui/spawnicons", color_white )
	SWEP.WepSelectIcon = Material( "greenblack.png" )
	SWEP.BounceWeaponIcon = false
	SWEP.DrawWeaponInfoBox = false
end

SWEP.Base				= "aa_base"

SWEP.Spawnable = true
SWEP.AdminSpawnable = true

SWEP.Kind = WEAPON_PISTOL
SWEP.WeaponID = AMMO_DEAGLE

SWEP.Primary.Ammo       = "AlyxGun" 
SWEP.Primary.Recoil			= 0.05
SWEP.Primary.Damage = 11
SWEP.Primary.Delay = 0.1
SWEP.Primary.Cone = 0.008
SWEP.Primary.ClipSize = 100
SWEP.Primary.ClipMax = 180
SWEP.Primary.DefaultClip = 100
SWEP.Primary.Automatic = true

SWEP.HeadshotMultiplier = 4

SWEP.Secondary.Ammo       = "AlyxGun" 
SWEP.Secondary.Recoil			= 0.05
SWEP.Secondary.Damage = 1
SWEP.Secondary.Delay = 1
SWEP.Secondary.Cone = 0.08
SWEP.Secondary.ClipSize = 3
SWEP.Secondary.ClipMax = 3
SWEP.Secondary.DefaultClip = 3
SWEP.Secondary.Automatic = false

SWEP.AutoSpawnable      = false
SWEP.AmmoEnt = "item_ammo_revolver_ttt"
SWEP.Primary.Sound = Sound( "weapons/green_black1.mp3" )

SWEP.UseHands			= true
SWEP.ViewModelFlip		= true
SWEP.ViewModelFOV		= 75
SWEP.ViewModel			= "models/weapons/v_green_black.mdl"
SWEP.WorldModel			= "models/weapons/w_green_black.mdl"

SWEP.IronSightsPos = Vector(5.14, -2, 2.5)
SWEP.IronSightsAng = Vector(0, 0, 0)
--SWEP.IronSightsPos 	= Vector( 3.8, -1, 3.6 )
--SWEP.ViewModelFOV = 49

SWEP.SecondaryProps = {
    "models/props_interiors/furniture_chair01a.mdl",
    "models/xqm/modernchair.mdl",
    "models/props_c17/furniturechair001a.mdl",
    "models/props/cs_office/sofa_chair.mdl",
    "models/props/de_tides/patio_chair.mdl",
    "models/props_c17/chair02a.mdl",
    "models/nova/chair_plastic01.mdl",
    "models/chairs/armchair.mdl",
    "models/nova/chair_office02.mdl",
    "models/props_c17/chair_office01a.mdl",
    "models/props/cs_office/chair_office.mdl",
    "models/props/de_tides/patio_chair2.mdl",

}


function SWEP:PrimaryAttack(worldsnd) 

   self.Weapon:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
   self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )

   if not self:CanPrimaryAttack() then return end

   if not worldsnd then
      self.Weapon:EmitSound( self.Primary.Sound, self.Primary.SoundLevel )
   elseif SERVER then
      sound.Play(self.Primary.Sound, self:GetPos(), self.Primary.SoundLevel)
   end

   self:ShootBullet( self.Primary.Damage, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )
   -- local tr = self.Owner:GetEyeTrace()
   -- local effectdata = EffectData()
   -- effectdata:SetOrigin( tr.HitPos )
   -- effectdata:SetNormal( tr.HitNormal )
  --  effectdata:SetMagnitude( 1 )
  --  effectdata:SetScale( 1 )
  --  effectdata:SetRadius( 1 )
  --  util.Effect( "Sparks", effectdata )
    
   self:TakePrimaryAmmo( 1 )

   local owner = self.Owner 
   if not IsValid(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

   owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )
end

SWEP.flyammo = 5
SWEP.nextFly = 0
function SWEP:SecondaryAttack()
    self:Fly()
end

SWEP.Headcrabs = 0
SWEP.Headcrabs2 = 0
SWEP.NextSecond = 0

function SWEP:Reload()
    if  self.NextSecond > CurTime() then return end


    self.Weapon:SetNextSecondaryFire( CurTime() + 2 )

    self.NextSecond = CurTime() + 5
    self.Weapon:SetNextSecondaryFire( CurTime() + self.Secondary.Delay )
    --self:TakePrimaryAmmo( 10 )
    self:throw_attack ( table.Random(self.SecondaryProps) , Sound( "weapons/green_black1.mp3" ) , 10);
end


function SWEP:throw_attack (model_file, sound, away)
    local tr = self.Owner:GetEyeTrace();

    if IsValid( tr.Entity )  then
        if not tr.Entity.hasProtectionSuit then
            tr.Entity.hasProtectionSuit= true
            timer.Simple(5, function()
                if not IsValid( tr.Entity )  then return end
                tr.Entity.hasProtectionSuit = false
            end)
        end
    end

    self.BaseClass.ShootEffects (self);
    if (!SERVER) then return end;
    local ent = ents.Create ("prop_physics");
    ent:SetModel (model_file);
    ent:SetPos (self.Owner:EyePos() + (self.Owner:GetAimVector() * 16));
    ent:SetAngles (self.Owner:EyeAngles());
    ent:SetPhysicsAttacker(self.Owner)
    ent:Spawn();
    ent:EmitSound( sound )
    if math.Rand(1,5) == "3" then
        ent:Ignite(100,100)
    end
    local phys = ent:GetPhysicsObject();
    local shot_length = tr.HitPos:Length();
    phys:ApplyForceCenter (self.Owner:GetAimVector():GetNormalized() * math.pow (shot_length, 2));

    timer.Simple(away,function()
        if IsValid(ent) then
            ent:Remove()
        end
    end)

    cleanup.Add (self.Owner, "props", ent);
    undo.Create ("Thrown model");
    undo.AddEntity (ent);
    undo.SetPlayer (self.Owner);
    undo.Finish();
end

