SWEP.Base = "gb_camo_base"
SWEP.ViewModelFOV	= 54
SWEP.HoldType = "pistol"
SWEP.PrintName = "Franklin's Redeemer"
SWEP.ViewModel		= "models/weapons/cstrike/c_pist_deagle.mdl"
SWEP.WorldModel		= "models/weapons/w_pist_deagle.mdl"
SWEP.Kind = WEAPON_PISTOL
SWEP.Slot = 1
SWEP.AutoSpawnable = false
SWEP.Primary.Damage		    = 88
SWEP.Primary.Delay		    = 0.95
SWEP.Primary.ClipSize		= 100
SWEP.Primary.DefaultClip	= 100
SWEP.Primary.Cone	        = 0.008
SWEP.Primary.Automatic		= true
SWEP.Primary.Ammo = "AlyxGun"
SWEP.AmmoEnt = "item_ammo_revolver_ttt"
SWEP.Primary.Sound          = Sound("Weapon_Deagle.Single")
SWEP.HeadshotMultiplier = 2
SWEP.HasFireBullet = true
SWEP.CanDecapitate= true
SWEP.Camo = 5

SWEP.IronSightsPos = Vector(-6.361, -3.701, 2.15)
SWEP.IronSightsAng = Vector(0, 0, 0)

function SWEP:SecondaryAttack()
    self:Fly()
end

function SWEP:Reload()
    if self.Owner:KeyDown(IN_USE) then
        self:ShootFlare()
        return
    end
    self.BaseClass.Reload(self)
end

function SWEP:Think()
    if self.Owner:KeyDown(IN_WALK) and self.Owner:KeyDown(IN_USE) then
        self:MakeExplosion()
    end
end