if SERVER then
    AddCSLuaFile(  )


end

SWEP.HoldType			= "pistol"
SWEP.PrintName			= "DWLuckyCharm"
SWEP.Slot				= 2

if CLIENT then

    SWEP.Author				= "GreenBlack"
    SWEP.SlotPos			= 2
    SWEP.Icon = "vgui/ttt_fun_killicons/green_black.png"
end


SWEP.Base				= "weapon_tttbase"

SWEP.Spawnable = true
SWEP.AdminSpawnable = true

SWEP.Kind = WEAPON_PISTOL
SWEP.WeaponID = AMMO_DEAGLE

SWEP.Primary.Ammo       = "Battery"
SWEP.Primary.Recoil			= 0.05
SWEP.Primary.Damage = 4
SWEP.Primary.Delay = 0.5
SWEP.Primary.Cone = 0.008
SWEP.Primary.ClipSize = 50
SWEP.Primary.ClipMax = 50
SWEP.Primary.DefaultClip = 50
SWEP.Primary.Automatic = false

SWEP.HeadshotMultiplier = 1

SWEP.AutoSpawnable      = true
SWEP.AmmoEnt = "item_ammo_revolver_ttt"
SWEP.Primary.Sound = Sound( "weapons/green_black1.mp3" )

SWEP.UseHands			= true
SWEP.ViewModelFlip		= true
SWEP.ViewModelFOV		= 75
SWEP.ViewModel			= "models/weapons/v_rreen_black.mdl"
SWEP.WorldModel			= "models/weapons/w_rreen_black.mdl"

SWEP.IronSightsPos = Vector(5.14, -2, 2.5)
SWEP.IronSightsAng = Vector(0, 0, 0)
--SWEP.IronSightsPos 	= Vector( 3.8, -1, 3.6 )
--SWEP.ViewModelFOV = 49


function SWEP:Holster()
    self:SetIronsights(false)
    self:ColorReset()
    return true
end

function SWEP:OnRemove()
    self:ColorReset()
end
function SWEP:OnDrop()
    self:ColorReset()

end

-- COLOR
function SWEP:Deploy()
    --  self:SetColorAndMaterial(self:randomColor(Color(255,255,255,255)),"trails/rainbow");
    return true
end
local inputColor = {255, 0, 0}
function SWEP:randomColor(inputCol)
    speed = 0.25

    -- Red to Green
    if inputCol[1] == 255 and inputCol[3] == 0 and inputCol[2] != 255 then
    inputCol[2] = inputCol[2] + speed
    elseif inputCol[2] == 255 and inputCol[1] != 0 then
    inputCol[1] = inputCol[1] - speed

        -- Green to Blue
    elseif inputCol[2] == 255 and inputCol[3] != 255 then
    inputCol[3] = inputCol[3] + speed
    elseif inputCol[3] == 255 and inputCol[2] != 0 then
    inputCol[2] = inputCol[2] - speed

        -- Blue to Red
    elseif inputCol[3] == 255 and inputCol[1] != 255 then
    inputCol[1] = inputCol[1] + speed
    elseif inputCol[1] == 255 and inputCol[3] != 0 then
    inputCol[3] = inputCol[3] - speed
    end

    return Color( inputCol[1], inputCol[2], inputCol[3], 0)
end

function SWEP:PreDrawViewModel()
    if SERVER or not IsValid(self.Owner) or not IsValid(self.Owner:GetViewModel()) then return end
    local colors = self:randomColor(inputColor)
    self.Owner:GetViewModel():SetColor( colors )
    --self.Owner:GetViewModel():SetMaterial("models/shiny")
end
-- END COLOR

-- Color material set
function SWEP:SetColorAndMaterial( color, material)
    if SERVER then
        self:SetColor(color)
        -- self:SetMaterial("models/shiny") --models/shiny"))
        local vm = self.Owner:GetViewModel()
        if not IsValid(vm) then return end
        vm:ResetSequence(vm:LookupSequence("idle01"))
    end
end

-- Color/materialreset
function SWEP:ColorReset()
    if SERVER then
        self:SetColor(Color(255,255,255,255))
        self:SetMaterial("")
        timer.Stop(self:GetClass() .. "_idle" .. self:EntIndex())
    elseif CLIENT and IsValid(self.Owner) and IsValid(self.Owner:GetViewModel()) then
        self.Owner:GetViewModel():SetColor(Color(255,255,255,255))
        self.Owner:GetViewModel():SetMaterial("")
    end
end


SWEP.hurtdamage = 15
SWEP.Secondary.NextFire = 0
function SWEP:Reload()
    if self.Secondary.NextFire > CurTime() then
        return end

    self.Secondary.NextFire = CurTime() + 0.2

    if self.hurtdamage == 15 then
        self.hurtdamage = 25
    elseif self.hurtdamage == 25 then
        self.hurtdamage = 50
    else
        self.hurtdamage = 15
    end

    self.Owner:PrintMessage(HUD_PRINTCENTER,"Damage: "..self.hurtdamage.." HP")

end

SWEP.thunders = {

"ambient/atmosphere/thunder1.wav",
"ambient/atmosphere/thunder2.wav",
"ambient/atmosphere/thunder3.wav",
"ambient/atmosphere/thunder4.wav",
"ambient/creatures/pigeon_idle1.wav",
"ambient/creatures/rats1.wav",
    "ambient/energy/zap1.wav",
"ambient/energy/zap2.wav",
"ambient/energy/zap3.wav",
"ambient/energy/zap5.wav",
"ambient/energy/zap6.wav",
"ambient/energy/zap7.wav",
"ambient/energy/zap8.wav",
"ambient/energy/zap9.wav",
"ambient/energy/spark1.wav",
"ambient/energy/spark2.wav",
"ambient/energy/spark3.wav",
"ambient/energy/spark4.wav",
"ambient/energy/spark5.wav",
"ambient/energy/spark6.wav",
"ambient/energy/weld1.wav",
"ambient/energy/weld2.wav",
"ambient/energy/whiteflash.wav"

}


function SWEP:PrimaryAttack()

    self.Weapon:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )

    if not self:CanPrimaryAttack() then return end

    local tr = self.Owner:GetEyeTrace()

        if self.Owner:IsGhost() then
            self.Owner:PrintMessage(HUD_PRINTCENTER,"Nice trick, but this is not a ghost weapon :-)")
            return
        end

           local ent = tr.Entity
            if !ent:IsPlayer() and !ent:IsNPC() then

                self.Owner:PrintMessage(HUD_PRINTCENTER,"Make sure someone is in front of you")
                self.Weapon:SetNextPrimaryFire( CurTime() + 0.2 )

            else



                self:EmitSound(  "weapons/usp/usp1.wav", 100 )
                self:EmitSound( table.Random( self.thunders),510,100);
                self:TakePrimaryAmmo( 1 )
                if math.Rand(1,10) < 5 then
                    if SERVER then
                        ent:TakeDamage( self.hurtdamage, self.Owner, self )
                        ent:PrintMessage(HUD_PRINTCENTER,"You loose, autch")
                        self.Owner:PrintMessage(HUD_PRINTCENTER,"You win!")
                        local spos = self.Owner:GetShootPos()
                        local edata = EffectData()
                        edata:SetStart(spos)
                        edata:SetOrigin(tr.HitPos)
                        edata:SetNormal(tr.Normal)
                        edata:SetEntity(ent)
                        util.Effect("BloodImpact", edata)
                    end

                else

                    if SERVER then
                        self.Owner:TakeDamage( self.hurtdamage, self.Owner, self )
                        self.Owner:PrintMessage(HUD_PRINTCENTER,"You loose, autch")
                        ent:PrintMessage(HUD_PRINTCENTER,"You win!")
                    end

                end
            end





    local owner = self.Owner
    if not IsValid(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

    owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )

end

function SWEP:HurtOther()
    self:EmitSound(  "weapons/usp/usp1.wav", 100 )
    self:ShootBullet( self.hurtdamage , 0.0001, 1, 0.001 )
    self.Owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )
end


SWEP.Soundss = {
"vo/coast/vgossip_04.wav",
"vo/coast/barn/lighthouse_morale.wav",
"vo/coast/barn/vmech_accept.wav",
"vo/coast/barn/female01/chatter.wav",
"vo/coast/barn/female01/crapships.wav",
"vo/coast/barn/female01/ditchcar.wav",
"vo/coast/barn/female01/drop_lite.wav",
"vo/coast/barn/female01/drop_road.wav",
"vo/coast/barn/female01/exit_cliffpath.wav",
"vo/coast/barn/female01/exit_comewith.wav",
"vo/coast/barn/female01/exit_watchstep.wav",
"vo/coast/barn/female01/getcarinbarn.wav",
"vo/coast/barn/female01/getcaringarage.wav",
"vo/coast/barn/female01/getoffroad01.wav",
"vo/coast/barn/female01/gettauoff.wav",
"vo/coast/barn/female01/incomingdropship.wav",
"vo/coast/barn/female01/lite_gunship01.wav",
"vo/coast/barn/female01/lite_gunship02.wav",
"vo/coast/barn/female01/lite_rockets01.wav",
"vo/coast/barn/female01/lite_rockets03.wav",
"vo/coast/barn/female01/lite_rockets04.wav",
"vo/coast/barn/female01/parkit.wav",
"vo/coast/barn/female01/youmadeit.wav",
"vo/coast/barn/male01/chatter.wav",
"vo/coast/barn/male01/crapships.wav",
"vo/coast/barn/male01/ditchcar.wav",
"vo/coast/barn/male01/drop_lite.wav",
"vo/coast/barn/male01/drop_road.wav",
"vo/coast/barn/male01/exit_cliffpath.wav",
"vo/coast/barn/male01/exit_comewith.wav",
"vo/coast/barn/male01/exit_watchstep.wav",
"vo/canals/matt_go_nag01.wav",
"vo/canals/matt_go_nag02.wav",
"vo/canals/matt_go_nag03.wav",
"vo/canals/matt_go_nag04.wav",
"vo/canals/matt_go_nag05.wav",
"vo/canals/matt_supplies.wav",
"vo/canals/matt_tearinguprr.wav",
"vo/canals/matt_tearinguprr_a.wav",
"vo/canals/matt_tearinguprr_b.wav",
"vo/canals/matt_thanksbut.wav",
"vo/canals/matt_toolate.wav",
"vo/canals/premassacre.wav",
"vo/canals/radio_comein12.wav",
"vo/canals/radio_doyoucopy8.wav",
"vo/canals/radio_thisis8.wav",
"vo/canals/shanty_badtime.wav",
"vo/canals/shanty_gotsomeammo.wav",
"vo/canals/shanty_gotword.wav",
"vo/canals/shanty_go_nag01.wav"
}

SWEP.NextSecond = 0
function SWEP:SecondaryAttack()
    if self.NextSecond < CurTime() then
        self.NextSecond = CurTime() + 3
        self:EmitSound( table.Random( self.Soundss),510,100);
    end


end