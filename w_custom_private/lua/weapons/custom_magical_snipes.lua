
if SERVER then

   
end

SWEP.HoldType           = "ar2"

   SWEP.PrintName          = "Magical Snipes"

   SWEP.Slot               = 8


if CLIENT then

   SWEP.Icon = "VGUI/ttt/icon_scout" 
end


SWEP.Base               = "gb_camo_base"
SWEP.Spawnable = true
SWEP.AdminSpawnable = true
SWEP.Camo = 13
SWEP.CustomCamo = true

SWEP.Kind = 9
SWEP.WeaponID = AMMO_RIFLE

SWEP.InLoadoutFor = nil

SWEP.LimitedStock = true

SWEP.Primary.Delay          = 0.9
SWEP.Primary.Recoil         = 4
SWEP.Primary.Automatic = true
SWEP.Primary.Ammo = "357"
SWEP.Primary.Damage = 70
SWEP.Primary.Cone = 0
SWEP.Primary.ClipSize = 500
SWEP.Primary.ClipMax = 500 -- keep mirrored to ammo
SWEP.Primary.DefaultClip = 500

SWEP.HeadshotMultiplier = 2

SWEP.AutoSpawnable      = false
SWEP.AmmoEnt = "item_ammo_357_ttt"

SWEP.UseHands			= true
SWEP.ViewModelFlip		= true
SWEP.ViewModelFOV		= 74
SWEP.ViewModel          = Model("models/weapons/v_snip_god.mdl")
SWEP.WorldModel         = Model("models/weapons/w_snip_god.mdl")

SWEP.Primary.Sound = Sound("Weapon_USP.SilencedShot")

SWEP.Secondary.Sound = Sound("Default.Zoom")

SWEP.IronSightsPos 			= Vector(10.2, -10.8, 2)
SWEP.IronSightsAng 			= Vector(2.8, 0, 0)
SWEP.Camo = 17
SWEP.CustomCamo = true

SWEP.nextreload2 = 0
SWEP.nextreload = 0
function SWEP:PrimaryAttack(worldsnd)
    self.Weapon:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )

    if self.Owner:KeyDown(IN_USE) then
        if self.nextreload > CurTime() then
            if self.nextreload - CurTime() < 30 then
                self.Owner:PrintMessage( HUD_PRINTCENTER,  math.floor(self.nextreload - CurTime()).." seconds left" )
            end
            return
        end


        self.Weapon:EmitSound( self.Primary.Sound, self.Primary.SoundLevel )
        self:ShootBullet2( self.Primary.Damage, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )

        return
    end

    if self.Owner:KeyDown(IN_RELOAD) then

        if SERVER  then
            if self.nextreload2 > CurTime() then
                if self.nextreload2 - CurTime() < 30 then
                    self.Owner:PrintMessage( HUD_PRINTCENTER,  math.floor(self.nextreload2 - CurTime()).." seconds left" )
                end
                return
            end
            self.nextreload2 = CurTime() + 30
            local tr = self.Owner:GetEyeTrace()
            local ent = ents.Create("magic_cherry_bomb")
            ent:SetOwner(self.Owner)
            ent.Owner = self.Owner
            ent.Owner = self.Owner

            ent:SetPos( tr.HitPos + self.Owner:GetAimVector() * -46  )
            ent:SetAngles(Angle(1,0,0))

            ent:Spawn()

        end

        return
    end

    if not self:CanPrimaryAttack() then return end
    if not worldsnd then
        self.Weapon:EmitSound( self.Primary.Sound, self.Primary.SoundLevel )
    elseif SERVER then
        WorldSound(self.Primary.Sound, self:GetPos(), self.Primary.SoundLevel)
    end
    self:ShootBullet( self.Primary.Damage, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )

    if ( self.Owner:IsValid() ) then
        local tr = self.Owner:GetEyeTrace()
        local effectdata = EffectData()
        effectdata:SetOrigin(tr.HitPos)
        effectdata:SetNormal(tr.HitNormal)
        effectdata:SetScale(1)
        -- util.Effect("effect_mad_ignition", effectdata)
        util.Decal("FadingScorch", tr.HitPos + tr.HitNormal, tr.HitPos - tr.HitNormal)

        local tracedata = {}
        tracedata.start = tr.HitPos
        tracedata.endpos = Vector(tr.HitPos.x, tr.HitPos.y, tr.HitPos.z - 10)
        tracedata.filter = tr.HitPos
        local tracedata = util.TraceLine(tracedata)
        if SERVER then
            --if tracedata.HitWorld and math.Rand(0,5) == 3 then
                local flame = ents.Create("env_fire");
                flame:SetPos(tr.HitPos + Vector( 0, 0, 1 ));
                flame:SetKeyValue("firesize", "10");
                flame:SetKeyValue("fireattack", "10");
                flame:SetKeyValue("StartDisabled", "0");
                flame:SetKeyValue("health", "10");
                flame:SetKeyValue("firetype", "0");
                flame:SetKeyValue("damagescale", "5");
                flame:SetKeyValue("spawnflags", "128");
                flame:SetPhysicsAttacker(self.Owner)
                flame:SetOwner( self.Owner )
                flame:Spawn();
                flame:Fire("StartFire",0);
           -- end
        end
        self:TakePrimaryAmmo( 1 )
        local owner = self.Owner
        --if not ValidEntity(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

        owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )
    end
end

function SWEP:SetZoom(state)
    if CLIENT then 
       return
    elseif IsValid(self.Owner) and self.Owner:IsPlayer() then
       if state then
          self.Owner:SetFOV(15, 0.3)
       else
          self.Owner:SetFOV(0, 0.2)
       end
    end
end

function SWEP:Think()
    if self.Owner:KeyDown(IN_DUCK) and self.Owner:KeyDown(IN_USE) then
        self:DropHealth()
    end
end



function SWEP:DropHealth()
    if self.NextHealthDrop > CurTime() then return end
    self.NextHealthDrop = CurTime() + 5
    local healttt = 200
    if self.healththing and SERVER then
        healttt = Entity(self.healththing):GetStoredHealth()
        Entity(self.healththing):Remove()
    end
    self:HealthDrop(healttt)
end


SWEP.NextHealthDrop = 0

local throwsound = Sound( "Weapon_SLAM.SatchelThrow" )

function SWEP:HealthDrop( healttt)
    if SERVER then
        local ply = self.Owner
        if not IsValid(ply) then return end

        local vsrc = ply:GetShootPos()
        local vang = ply:GetAimVector()
        local vvel = ply:GetVelocity()

        local vthrow = vvel + vang * 200

        local health = ents.Create("magical_snipes_hs")
        if IsValid(health) then
            health:SetPos(vsrc + vang * 10)
            health:Spawn()
            --health:SetModel( "models/props_lab/cactus.mdl")
            health:SetModelScale(health:GetModelScale()*1,0)
            health:SetPlacer(ply)
            --health.healsound = "ginger_cartman.mp3"

            health:PhysWake()
            local phys = health:GetPhysicsObject()
            if IsValid(phys) then
                phys:SetVelocity(vthrow)
            end
            --  health:SetModel( "models/props_lab/cactus.mdl")
            health:SetStoredHealth(healttt)
            health.setOwner = self.Owner

            self.healththing = health:EntIndex()

            self.Planted = true
        end
    end

    self.Weapon:EmitSound(throwsound)
end

-- Add some zoom to ironsights for this gun
function SWEP:SecondaryAttack()
    if self.Owner:KeyDown(IN_USE) then
        self:Fly()
        return
    end
    if self.Owner:KeyDown(IN_RELOAD) then
        self:MakeExplosion()
        return
    end
    if not self.IronSightsPos then return end
    if self.Weapon:GetNextSecondaryFire() > CurTime() then return end
    
    bIronsights = not self:GetIronsights()
    
    self:SetIronsights( bIronsights )
    
    if SERVER then
        self:SetZoom(bIronsights)
     else
        self:EmitSound(self.Secondary.Sound)
    end
    
    self.Weapon:SetNextSecondaryFire( CurTime() + 0.3)
end

function SWEP:PreDrop()
    self:SetZoom(false)
    self:SetIronsights(false)
    return self.BaseClass.PreDrop(self)
end

SWEP.NextFire2 = 0
function SWEP:Reload()
    if self.Owner:KeyDown(IN_ATTACK2) then
        if self.NextFire2 > CurTime() then return end
        self.NextFire2 = CurTime() + 30
        if SERVER then
            local ball = ents.Create("magic_snipe_burger");
            self:EmitSound("Friends/friend_join.wav")
            if (ball) then
                ball:SetPos(self.Owner:GetPos() + Vector(0,0,48));
                ball.owner = self.Owner
                ball:Spawn();
                local physicsObject = ball:GetPhysicsObject();
                physicsObject:ApplyForceCenter(self.Owner:GetAimVector() * 230);
                return ball;
            end;
        end

        return
    end

    self.Weapon:SetNextSecondaryFire( CurTime() + 3)

    self.Weapon:DefaultReload( ACT_VM_RELOAD );
    self:SetIronsights( false )
    self:SetZoom(false)
end


function SWEP:Holster()
    self:SetIronsights(false)
    self:SetZoom(false)
    return true
end



function SWEP:ShootBullet2( dmg, recoil, numbul, cone )


    self.Weapon:SendWeaponAnim(self.PrimaryAnim)

    self.Owner:MuzzleFlash()
    self.Owner:SetAnimation( PLAYER_ATTACK1 )

    if not IsFirstTimePredicted() then return end

    local sights = self:GetIronsights()

    numbul = numbul or 1
    cone   = cone   or 0.01

    local bullet = {}
    bullet.Num    = numbul
    bullet.Src    = self.Owner:GetShootPos()
    bullet.Dir    = self.Owner:GetAimVector()
    bullet.Spread = Vector( cone, cone, 0 )
    bullet.Tracer = 0
    bullet.TracerName =  "LaserTracer_thick"
    bullet.Force  = 10
    bullet.Damage = 0


    self.Owner:FireBullets( bullet )

    -- Owner can die after firebullets
    if (not IsValid(self.Owner)) or (not self.Owner:Alive()) or self.Owner:IsNPC() then return end

    if ((game.SinglePlayer() and SERVER) or
            ((not game.SinglePlayer()) and CLIENT and IsFirstTimePredicted())) then

        -- reduce recoil if ironsighting
        recoil = sights and (recoil * 0.6) or recoil

        local eyeang = self.Owner:EyeAngles()
        eyeang.pitch = eyeang.pitch - recoil
        self.Owner:SetEyeAngles( eyeang )
    end

    attacker = self.Owner
    local tracedata = {}

    tracedata.start = self.Owner:GetShootPos()
    tracedata.endpos = self.Owner:GetShootPos() + ( self.Owner:GetAimVector() * 999999 )
    tracedata.filter = self.Owner
    tracedata.mins = Vector( -3,-3,-3 )
    tracedata.maxs = Vector( 3,3,3 )
    tracedata.mask = MASK_SHOT_HULL
    local trace = util.TraceHull( tracedata )

    victim = trace.Entity

    timer.Simple(15, function()
        if IsValid(self) and IsValid( self.Owner) and self:Clip1() < 10 then
            self:SetClip1( self:Clip1() + 1)
        end
    end)

    if ( not IsValid(attacker) or !SERVER  or not victim:IsPlayer() or victim:IsWorld() ) then return end
    if  !victim:Alive() or victim.IsDog or specialRound.isSpecialRound or _globals['specialperson'][victim:SteamID()] or (victim.NoTaze && self.Owner:GetRole() != ROLE_DETECTIVE)    then
    self.Owner:PrintMessage( HUD_PRINTCENTER, "Person cannot be tazed" )

    return end


    --if victim:GetPos():Distance( attacker:GetPos()) > 400 then return end

    self.nextreload = CurTime() + 30

    StunTime = self.StunTime

    local edata = EffectData()
    edata:SetEntity(victim)
    edata:SetMagnitude(3)
    edata:SetScale(3)

    util.Effect("TeslaHitBoxes", edata)

    self:Taze( victim)

end
SWEP.Range = 99999
SWEP.StunTime = 6
SWEP.HadTazes = 0

function SWEP:Taze( victim )

    if not victim:Alive() or victim:IsGhost() then return end

    if victim:InVehicle() then
        local vehicle = victim:GetParent()
        victim:ExitVehicle()
    end

    ULib.getSpawnInfo( victim )

    if SERVER then DamageLog("Tazer: " .. self.Owner:Nick() .. " [" .. self.Owner:GetRoleString() .. "] tazed "..victim:Nick().." [" .. victim:GetRoleString() .. "] ")
    end
    local rag = ents.Create("prop_ragdoll")
    if not (rag:IsValid()) then return end
    --if self.HadTazes > 2 then return end



    rag.BelongsTo = victim
    rag.Weapons = {}
    for _, v in pairs (victim:GetWeapons()) do
        table.insert(rag.Weapons, v:GetClass())
    end

    rag.Credits = victim:GetCredits()
    rag.OldHealth = victim:Health()
    rag:SetModel(victim:GetModel())
    rag:SetPos( victim:GetPos() )
    local velocity = victim:GetVelocity()
    rag:SetAngles( victim:GetAngles() )
    rag:SetModel( victim:GetModel() )
    rag:Spawn()
    rag:Activate()
    victim:SetParent( rag )
    victim:SetParent( rag )
    victim.Ragdolled = rag
    rag.orgPos = victim:GetPos()
    rag.orgPos.z = rag.orgPos.z + 10

    victim.orgPos = victim:GetPos()
    victim:StripWeapons()
    victim:Spectate(OBS_MODE_CHASE)
    victim:SpectateEntity(rag)




    victim:PrintMessage( HUD_PRINTCENTER, "You have been tasered. "..self.StunTime.." seconds till revival" )
    timer.Create("revivedelay"..victim:UniqueID(), self.StunTime, 1, function() TazerRevive( victim ) end )

end

function TazerRevive( victim)

    if !IsValid( victim ) then return end
    rag = victim:GetParent()

    if SERVER and victim.IsSlayed then
        victim:Kill();
        return
    end
    victim:SetParent()


    if (rag and rag:IsValid()) then
        weps = table.Copy(rag.Weapons)
        credits = rag.Credits
        oldHealth = rag.OldHealth
        rag:DropToFloor()
        pos = rag:GetPos()  --move up a little
        pos.z = pos.z + 15


        victim.Ragdolled = nil

        victim:UnSpectate()
        --  victim:DrawViewModel(true)
        --  victim:DrawWorldModel(true)
        victim:Spawn()
        --ULib.spawn( victim, true )
        rag:Remove()
        victim:SetPos( pos )
        victim:StripWeapons()

        UnStuck_stuck(victim)

        for _, v in pairs (weps) do
            victim:Give(v)
        end

        if credits > 0 then
            victim:AddCredits( credits  )
        end
        if oldHealth > 0 then
            victim:SetHealth( oldHealth  )
        end
        victim:DropToFloor()



        if  !victim:IsInWorld() or !victim:OnGround()    then
        victim:SetPos( victim.orgPos )
        end

        timer.Create("RespawnIfStuck",1,1,function()
            if IsValid(victim) and (!victim:IsInWorld() or !victim:OnGround() ) then
            victim:SetPos( victim.orgPos   )
            end
        end)

    else
        print("Debug: ragdoll or player is invalid")
        if IsValid( victim) then
            ULib.spawn( victim, true )
        end
    end

end


if CLIENT then
   local scope = surface.GetTextureID("sprites/scope")
   function SWEP:DrawHUD()
      if self:GetIronsights() then
         surface.SetDrawColor( 0, 0, 0, 255 )
         
         local x = ScrW() / 2.0
         local y = ScrH() / 2.0
         local scope_size = ScrH()

         -- crosshair
         local gap = 80
         local length = scope_size
         surface.DrawLine( x - length, y, x - gap, y )
         surface.DrawLine( x + length, y, x + gap, y )
         surface.DrawLine( x, y - length, x, y - gap )
         surface.DrawLine( x, y + length, x, y + gap )

         gap = 0
         length = 50
         surface.DrawLine( x - length, y, x - gap, y )
         surface.DrawLine( x + length, y, x + gap, y )
         surface.DrawLine( x, y - length, x, y - gap )
         surface.DrawLine( x, y + length, x, y + gap )


         -- cover edges
         local sh = scope_size / 2
         local w = (x - sh) + 2
         surface.DrawRect(0, 0, w, scope_size)
         surface.DrawRect(x + sh - 2, 0, w, scope_size)

         surface.SetDrawColor(255, 0, 0, 255)
         surface.DrawLine(x, y, x + 1, y + 1)

         -- scope
         surface.SetTexture(scope)
         surface.SetDrawColor(255, 255, 255, 255)

         surface.DrawTexturedRectRotated(x, y, scope_size, scope_size, 0)

      else
         return self.BaseClass.DrawHUD(self)
      end
   end

   function SWEP:AdjustMouseSensitivity()
      return (self:GetIronsights() and 0.2) or nil
   end
end

local ENT = {}

ENT.Type = "anim"
ENT.Base = "base_anim"


if SERVER then

    function ENT:Initialize()
        self:SetModel("models/food/burger.mdl")
        self:SetModelScale(self:GetModelScale()*1,0)
        self.Entity:PhysicsInit(SOLID_VPHYSICS);
        self.Entity:SetMoveType(MOVETYPE_VPHYSICS);
        self.Entity:SetSolid(SOLID_VPHYSICS);

        self.Entity:SetCollisionGroup(COLLISION_GROUP_WEAPON)
        local phys = self:GetPhysicsObject()
        if phys:IsValid() then
            phys:Wake()
            phys:EnableGravity(true);
        end
    end

    function ENT:Use( activator, caller )
        self.Entity:Remove()
        if ( activator:IsPlayer() ) then
            if activator:Health() < 150 then

                activator:SetHealth(activator:Health()+math.random(20,80))
                gb_check_max_health(activator)
            end
            activator:EmitSound("crisps/eat.mp3")
        end
    end


end

scripted_ents.Register( ENT, "magic_snipe_burger", true )



local ENT = {}

ENT.Type = "anim"
ENT.Base = "base_anim"


if SERVER then

    function ENT:Initialize()
        self:SetModel("models/Gibs/HGIBS.mdl")
        self:SetModelScale(self:GetModelScale()*1,0)
        self.Entity:PhysicsInit(SOLID_VPHYSICS);
        self.Entity:SetMoveType(MOVETYPE_VPHYSICS);
        self.Entity:SetSolid(SOLID_VPHYSICS);

        self.Entity:SetCollisionGroup(COLLISION_GROUP_WEAPON)
        local phys = self:GetPhysicsObject()
        if phys:IsValid() then
            phys:Wake()
            phys:EnableGravity(true);
        end
        self.timer = CurTime() + 3
    end

    function ENT:Think()
        if self.timer < CurTime() and SERVER then
            self.timer = CurTime() + 90
            self:ExplodeSkulls()
        end

    end


    function ENT:ExplodeSkulls()
        if self.exploded then return end self.exploded = true
        if not self.ischild then
            for i=1,4 do
                local spos = self:GetPos()+Vector(math.random(-75,75),math.random(-75,75),math.random(0,50))
                local bomb = ents.Create("magic_cherry_bomb")
                bomb.ischild = true
                bomb:SetPos(spos)
                bomb:Spawn()
            end
            self.ischild = true
            self:ExplodeSkulls()
        else





              local ent = ents.Create( "env_explosion" )

                ent:SetPos( self:GetPos() )
               -- ent:SetOwner( self.Owner )
                ent:SetKeyValue( "iMagnitude", "3" )
                ent:Spawn()

                ent:Fire( "Explode",1,1 )
                ent:EmitSound( "physics/body/body_medium_break2.wav", 350, 100 )


                self:EmitSound("weapons/big_explosion.mp3")


            local proximity_ents = ents.FindInSphere(self:GetPos(), 150)
            for k, v in pairs(proximity_ents) do
                v:TakeDamage( math.random(30,40), self.Owner, self )
            end

            self:Remove()

        end
    end


end

scripted_ents.Register( ENT, "magic_cherry_bomb", true )


