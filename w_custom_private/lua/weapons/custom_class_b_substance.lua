SWEP.Base = "aa_base"
SWEP.ViewModelFOV	= 54
SWEP.HoldType = "duel"
SWEP.PrintName = "Class B Substance"
SWEP.ViewModel		= "models/weapons/cstrike/c_pist_elite.mdl"
SWEP.WorldModel		= "models/weapons/w_pist_elite.mdl"
SWEP.Kind = WEAPON_PISTOL
SWEP.ViewModelFlip = false
SWEP.Slot = 1
SWEP.AutoSpawnable = false
SWEP.Primary.Recoil     = 1.5
SWEP.Primary.Damage = 4
SWEP.Primary.Delay = 0.30
SWEP.Primary.Cone = 0.02
SWEP.Primary.ClipSize = 20
SWEP.Primary.Automatic = true
SWEP.Primary.DefaultClip = 60
SWEP.NumShots = 15
SWEP.Primary.NumShots		= 12
SWEP.Primary.Cone			= 0.075
SWEP.Primary.ClipSize		= 60
SWEP.Primary.ClipMax 		= 120
SWEP.Primary.Delay			= 0.23
SWEP.UseHands = true

SWEP.Secondary.Automatic    = false
SWEP.Secondary.Ammo         = "Pistol"
SWEP.Secondary.ClipMax      = 60

SWEP.Primary.Ammo			= "AlyxGun"
SWEP.AmmoEnt = "item_ammo_pistol_ttt"
SWEP.HeadshotMultiplier = 1



SWEP.Primary.Sound = Sound( "Weapon_Elite.Single" )
SWEP.Secondary.Sound = Sound( "Weapon_Elite.Single" )


function SWEP:Reload()
    if self.Owner:KeyDown(IN_USE) then
        self.BaseClass.Reload(self)
        return
    end
    self:ShootSecondary()
end

function SWEP:OnDrop()
    self:Remove()
end

function SWEP:PrimaryAttack( worldsnd )

    --self.Weapon:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )

    if not self:CanPrimaryAttack() then return end

    if not worldsnd then
        self.Weapon:EmitSound( self.Primary.Sound, self.Primary.SoundLevel )
    elseif SERVER then
        sound.Play(self.Primary.Sound, self:GetPos(), self.Primary.SoundLevel)
    end

    self:ShootBullet( self.Primary.Damage, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )

    self:TakePrimaryAmmo( 1 )

    local owner = self.Owner
    if not IsValid(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

    --  owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )
end



function SWEP:SecondaryAttack(worldsnd)
self:Fly()
end
