
SWEP.PrintName = "The Body Builder"
SWEP.Base = "aa_base"
SWEP.Kind = WEAPON_EQUIP2

SWEP.CanBuy = nil -- no longer a buyable thing
--SWEP.LimitedStock = true

--SWEP.InLoadoutFor = {ROLE_DETECTIVE, ROLE_TRAITOR, ROLE_INNOCENT}

SWEP.AllowDrop = false
SWEP.Icon = "vgui/ttt_fun_killicons/fists.png"

SWEP.Author			= "robotboy655"
SWEP.Purpose		= "Well we sure as heck didn't use guns! We would wrestle Hunters to the ground with our bare hands! I would get ten, twenty a day, just using my fists."

SWEP.Spawnable			= true
SWEP.UseHands			= true


SWEP.ViewModel			= "models/weapons/c_arms_citizen.mdl"
SWEP.WorldModel			= ""

SWEP.ViewModelFOV		= 52
SWEP.ViewModelFlip		= false


SWEP.Primary.ClipSize		= 100
SWEP.Primary.DefaultClip	= 100
SWEP.Primary.Delay	= 0.7
SWEP.Primary.Damage			= 8
SWEP.Primary.NumShots   = 15
SWEP.Primary.Cone = 0.085
SWEP.Primary.Automatic		= true
SWEP.Primary.Ammo			= "none"
SWEP.Primary.Sound			= "weapons/shotgun/shotgun_cock.wav"

SWEP.Secondary.ClipSize		= -1
SWEP.Secondary.DefaultClip	= -1
SWEP.Secondary.Damage			= 8
SWEP.Secondary.Automatic	= true
SWEP.Secondary.Ammo			= "none"

SWEP.Weight				= 5
SWEP.AutoSwitchTo		= false
SWEP.AutoSwitchFrom		= false

SWEP.Slot				= 7
SWEP.SlotPos			= 7
SWEP.DrawAmmo			= false
SWEP.DrawCrosshair		= true

local SwingSound = Sound( "weapons/slam/throw.wav" )
local HitSound = Sound( "Flesh.ImpactHard" )

SWEP.nextreload = 0
function SWEP:PrimaryAttack()
    self.Weapon:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
    if self.nextreload > CurTime() then return end
    self.nextreload = CurTime() + 1
   -- self:ShootBullet( self.Primary.Damage, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )

    if ( self.Owner:IsValid() ) then
        if self.Owner.NextRunSpeecBodyBoulder and self.Owner.NextRunSpeecBodyBoulder > CurTime() then
            if SERVER then
                self.Owner:EmitSound("items/suitchargeno1.wav")
            end
            return
        end
        local tr = self.Owner:GetEyeTrace()

        local vm = self.Owner:GetViewModel()
        vm:ResetSequence( vm:LookupSequence( "fists_right" ) )

        self:Idle()

        local tracedata = {}
        tracedata.start = tr.HitPos
        tracedata.endpos = Vector(tr.HitPos.x, tr.HitPos.y, tr.HitPos.z - 10)
        tracedata.filter = tr.HitPos
        local tracedata = util.TraceLine(tracedata)
        if SERVER then

            local ent = tr.Entity
            if ent:IsPlayer() or ent:IsNPC() then
               local ply = ent
                if not ply.OldRunSpeed and not ply.OldWalkSpeed then
                    ply.OldRunSpeed = ply:GetRunSpeed()
                    ply.OldWalkSpeed =  ply:GetWalkSpeed()
                end
                self.Owner.NextRunSpeecBodyBoulder = CurTime() + 60

                ply:SetNWInt("runspeed", 25)
                ply:SetNWInt("walkspeed", 25)
               ply:SetWalkSpeed(2)
               ply:SetRunSpeed( 2)

                timer.Create("NormalizeRunSpeedPokemonFreeze2"..math.random(0,9999999999),6,1,function()
                    if IsValid(ply) and  ply.OldRunSpeed and ply.OldWalkSpeed then
                        ply:SetNWInt("runspeed",  ply.OldRunSpeed)
                        ply:SetNWInt("walkspeed",ply.OldWalkSpeed)
                        ply:SetWalkSpeed( ply.OldWalkSpeed)
                        ply:SetRunSpeed( ply.OldRunSpeed)
                        ply.OldRunSpeed = false
                        ply.OldWalkSpeed = false
                    end
                end)

            end
        end
        self:TakePrimaryAmmo( 1 )
        local owner = self.Owner
        --if not ValidEntity(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

        owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )
    end
end

function SWEP:SecondaryAttack()
    if self.Owner:KeyDown(IN_USE) then
        self:Fly()
        return
    end
    self.Weapon:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
    self:NormalPrimaryAttack()
    local vm = self.Owner:GetViewModel()
    vm:ResetSequence( vm:LookupSequence( "fists_right" ) )

    self:Idle()
    if SERVER then
    self.Weapon:EmitSound("weapons/shotgun/shotgun_fire7.wav")
        end
end

SWEP.NextReload = 0
function SWEP:Reload()
    if self.NextReload > CurTime() then return end
    self.NextReload = CurTime() + 1.5
    local vm = self.Owner:GetViewModel()
    vm:ResetSequence( vm:LookupSequence( "fists_right" ) )

    self:Idle()
    self:PowerHit(true)
end
function SWEP:Initialize()

	self:SetHoldType( "fist" )

end



function SWEP:PreDrawViewModel(viewModel, weapon, client)
    if SERVER or not IsValid(self.Owner) or not IsValid(viewModel) then return end
    viewModel:SetMaterial( "engine/occlusionproxy" )
end


function SWEP:Idle()

	local vm = self.Owner:GetViewModel()
	timer.Create( "fists_idle" .. self:EntIndex(), vm:SequenceDuration(), 1, function()
		vm:ResetSequence( vm:LookupSequence( "fists_idle_0" .. math.random( 1, 2 ) ) )
	end )

end

function SWEP:OnRemove()

    if ( IsValid( self.Owner ) ) then
        local vm = self.Owner:GetViewModel()
        if IsValid(vm) then
            vm:SetMaterial( "" )
        end
    end

    timer.Stop( "fists_idle" .. self:EntIndex() )

end

function SWEP:Holster( wep )
    if ( IsValid( self.Owner ) ) then
        local vm = self.Owner:GetViewModel()
        if IsValid(vm) then
            vm:SetMaterial( "" )
        end
    end

    return true
end

SWEP.HandModel = false

function SWEP:Deploy()
    if !IsValid(self.Owner) then return end
	local vm = self.Owner:GetViewModel()

       

	vm:ResetSequence( vm:LookupSequence( "fists_draw" ) )

	self:Idle()

    local ply = self.Owner

    hook.Add("PlayerDeath", "sandbootsdie"..ply:SteamID(),function( victim, weapon, killer )
        if  not ply:IsGhost() and SERVER and killer == ply and not  ply.isdisguised  then
            gb_PlaySoundFromServer(gb_config.websounds.."turndownforwhat.mp3", killer)
        end
    end)

	return true
end
