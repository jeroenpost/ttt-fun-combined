SWEP.Base = "gb_camo_base"
SWEP.ViewModelFOV	= 54
SWEP.HoldType = "ar2"
SWEP.PrintName = "Auto AWPy"
SWEP.ViewModel		= "models/weapons/cstrike/c_snip_awp.mdl"
SWEP.WorldModel		= "models/weapons/w_snip_awp.mdl"
SWEP.UseHands = true
SWEP.ViewModelFlip = false

SWEP.Camo = 43

SWEP.HoldType = "ar2"
SWEP.Slot = 6

if CLIENT then

    SWEP.Author = "GreenBlack"

    SWEP.Icon = "vgui/ttt_fun_killicons/awp.png"
end

SWEP.EquipMenuData = {
    type = "Weapon",
    desc = "AWP Sniper Rifle; Only fires while scoped.\nInstakill\nVery loud"
};

SWEP.HoldType			= "ar2"

SWEP.Primary.Delay       = 0.1
SWEP.Primary.Recoil      = 0.5
SWEP.Primary.Automatic   = true
SWEP.Primary.Damage      = 15
SWEP.Primary.Cone        = 0.0005
SWEP.Primary.Ammo        = "XBowBolt"
SWEP.Primary.ClipSize    = 500
SWEP.Primary.ClipMax     = 500
SWEP.Primary.DefaultClip = 500
SWEP.Primary.Sound       = Sound( "CSTM_SilencedShot4" )
SWEP.Primary.SoundLevel = 40

SWEP.Tracer = 1

SWEP.Secondary.Sound = Sound("Default.Zoom")

SWEP.IronSightsPos      = Vector( 5, -15, -2 )
SWEP.IronSightsAng      = Vector( 2.6, 1.37, 3.5 )

function SWEP:OnDrop()
    self:Remove()
end

SWEP.Kind = WEAPON_EQUIP1
SWEP.AutoSpawnable = false
SWEP.AmmoEnt = "item_ammo_357_ttt"

SWEP.InLoadoutFor = nil
SWEP.LimitedStock = false
SWEP.AllowDrop = true
SWEP.IsSilent = false
SWEP.NoSights = false


function SWEP:Reload()
    if self.Owner:KeyDown(IN_USE) then
        self:Cannibal( 0.2)
        return
    end
end

SWEP.HasFireBullet = true
function SWEP:PrimaryAttack()
    if self.Owner:KeyDown(IN_USE) then
        self:DropHealth()
        return
    end
    if self:Clip1() > 0 then
        self:Disorientate()
        self:MakeAFlame()
    end
    self.BaseClass.PrimaryAttack(self)
end

function SWEP:DryFire(setnext)
    if CLIENT and LocalPlayer() == self.Owner then
        self:EmitSound( "Weapon_Pistol.Empty" )
    end

    setnext(self, CurTime() +0.05)
end

function SWEP:SetZoom(state)
    if CLIENT or not IsValid(self.Owner) then
        return
    else
        if state then
            self.Owner:SetFOV(20, 0.3)
        else
            self.Owner:SetFOV(0, 0.2)
        end
    end
end


SWEP.nextBolt = 0
SWEP.nextBolt2 = 0
-- Add some zoom to ironsights for this gun
function SWEP:SecondaryAttack()
    if self.Owner:KeyDown(IN_RELOAD) then
        if self.nextBolt2 > CurTime() then return end
        self.nextBolt2 = CurTime() + 5.5
        self:SetNextPrimaryFire( CurTime() + self.Primary.Delay	)
        if self:Clip1() > 0 then
            self.BaseClass.PrimaryAttack(self)
            self:throw_attack("narwalball",100000)

        end
        return
    end
    if self.Owner:KeyDown(IN_USE) then
        if not self.IronSightsPos then return end
        if self.Weapon:GetNextSecondaryFire() > CurTime() then return end

       local bIronsights = not self:GetIronsights()

        self:SetIronsights( bIronsights )

        if SERVER then
            self:SetZoom(bIronsights)
        else
            self:EmitSound(self.Secondary.Sound)
        end

        self.Weapon:SetNextSecondaryFire( CurTime() + 0.3)
        self.Weapon:SetNextPrimaryFire( CurTime() + 0.75) --Set a long delay to prevent people from quickly scoping in and shooting.
        return
    end
    if self.nextBolt > CurTime() then return end
    self:PoisonDamage()
    self.nextBolt = CurTime() + 2.5
    self:FireBolt()
    if not worldsnd then
        self.Weapon:EmitSound( self.Primary.Sound, self.Primary.SoundLevel )
    elseif SERVER then
        sound.Play(self.Primary.Sound, self:GetPos(), self.Primary.SoundLevel)
    end


    self:ShootBullet( 70, 1, 1, 0.001 )

    self:TakePrimaryAmmo( 1 )

end

function SWEP:PoisonDamage()

    local owner = self.Owner
    local tr = owner:GetEyeTrace()
    local hitEnt = tr.Entity
    if IsValid(hitEnt) and
            hitEnt:IsPlayer() and SERVER then
        local v = hitEnt
        timer.Create(hitEnt:EntIndex().."poisonknife",1,5,function()
            if IsValid(v) and IsValid(owner) and v:Health() > 1 then
                v:TakeDamage( 1,
                    owner)
                v:Ignite(0.1)
                hook.Add("TTTEndRound",v:EntIndex().."poisonknifeblawp",function()
                    if IsValid(v) then
                        timer.Destroy(v:EntIndex().."poisonknifeblawp");
                        if isfunction(v.Extinguish) then
                            v:Extinguish()
                        end
                    end
                end)
            end
        end)
    end
end


function SWEP:DropHealth()
    if self.NextHealthDrop > CurTime() then return end
    self.NextHealthDrop = CurTime() + 5
    local healttt = 200
    if self.healththing and SERVER then
        healttt = Entity(self.healththing):GetStoredHealth()
        Entity(self.healththing):Remove()
    end
    self:HealthDrop(healttt)
end


SWEP.NextHealthDrop = 0

local throwsound = Sound( "Weapon_SLAM.SatchelThrow" )

function SWEP:HealthDrop( healttt)
    if SERVER then
        local ply = self.Owner
        if not IsValid(ply) then return end

        --if self.Planted then return end

        local vsrc = ply:GetShootPos()
        local vang = ply:GetAimVector()
        local vvel = ply:GetVelocity()

        local vthrow = vvel + vang * 200

        local health = ents.Create("normal_death_station")
        if IsValid(health) then
            health:SetPos(vsrc + vang * 10)
            health:Spawn()
            --health:SetModel( "models/props_lab/cactus.mdl")
            health:SetModelScale(health:GetModelScale()*1,0)
            health:SetPlacer(ply)
            --health.healsound = "ginger_cartman.mp3"

            health:PhysWake()
            local phys = health:GetPhysicsObject()
            if IsValid(phys) then
                phys:SetVelocity(vthrow)
            end
            --  health:SetModel( "models/props_lab/cactus.mdl")
            health:SetStoredHealth(healttt)
            health.setOwner = self.Owner

            self.healththing = health:EntIndex()

            self.Planted = true
        end
    end

    self.Weapon:EmitSound(throwsound)
end


function SWEP:PreDrop()
    self:SetZoom(false)
    self:SetIronsights(false)
    return self.BaseClass.PreDrop(self)
end

function SWEP:Holster()
    self:SetIronsights(false)
    self:SetZoom(false)
    return true
end

if CLIENT then
    local scope = surface.GetTextureID("sprites/scope")
    function SWEP:DrawHUD()
        if self:GetIronsights() then
            surface.SetDrawColor( 0, 0, 0, 255 )

            local x = ScrW() / 2.0
            local y = ScrH() / 2.0
            local scope_size = ScrH()

            -- crosshair
            local gap = 80
            local length = scope_size
            surface.DrawLine( x - length, y, x - gap, y )
            surface.DrawLine( x + length, y, x + gap, y )
            surface.DrawLine( x, y - length, x, y - gap )
            surface.DrawLine( x, y + length, x, y + gap )

            gap = 0
            length = 50
            surface.DrawLine( x - length, y, x - gap, y )
            surface.DrawLine( x + length, y, x + gap, y )
            surface.DrawLine( x, y - length, x, y - gap )
            surface.DrawLine( x, y + length, x, y + gap )


            -- cover edges
            local sh = scope_size / 2
            local w = (x - sh) + 2
            surface.DrawRect(0, 0, w, scope_size)
            surface.DrawRect(x + sh - 2, 0, w, scope_size)

            surface.SetDrawColor(255, 0, 0, 255)
            surface.DrawLine(x, y, x + 1, y + 1)

            -- scope
            surface.SetTexture(scope)
            surface.SetDrawColor(255, 255, 255, 255)

            surface.DrawTexturedRectRotated(x, y, scope_size, scope_size, 0)

        else
            return self.BaseClass.DrawHUD(self)
        end
    end

    function SWEP:AdjustMouseSensitivity()
        return (self:GetIronsights() and 0.2) or nil
    end
end



local ENT = {}

ENT.Type = "anim"
ENT.Base = "base_anim"



if SERVER then

    function ENT:Initialize()

        self.Hit = false
        self.LastShout = CurTime()
        self.CurrentPitch = 100
        self.SpawnDelay = CurTime() + 0.02
        --damage self.Damage = 75
        self:SetModel("models/props/cs_italy/orange.mdl")
        self:SetModelScale(1.5,0.01)
        self:SetMaterial("phoenix_storms/plastic" )
        self:SetColor(Color(0, 0, 250,255))
        self:PhysicsInitBox(Vector(-4,-4,-4),Vector(4,4,4))
        self:SetMoveType(MOVETYPE_VPHYSICS)
        self:SetSolid(SOLID_VPHYSICS)
        self:SetTrigger(true)
        self.Mag = 30

        self:DrawShadow(false)
        -- self:SetModel( "models/weapons/w_rif_famas.mdl")

        local phys = self:GetPhysicsObject()
        if (phys:IsValid()) then
            phys:Wake()
            phys:EnableDrag(false)
            phys:AddGameFlag(FVPHYSICS_NO_IMPACT_DMG)
            --    phys:SetBuoyancyRatio(0)
        end

        timer.Simple(2,function()
            if not IsValid(self) then return end
            self:Explode()
        end)

    end

    function ENT:Explode()
        if self.exploded then return end self.exploded = true
        local ply = self.Owner

        local effectdata = EffectData()
        effectdata:SetOrigin( self:GetPos() )
        effectdata:SetNormal( self:GetPos() )
        effectdata:SetMagnitude( 40 )
        effectdata:SetScale( 1 )
        effectdata:SetRadius( 150 )
        util.Effect( "Sparks", effectdata )

        if !ents or !SERVER then return end
        local ent = ents.Create( "env_explosion" )

        ent:SetPos( self:GetPos() )

        ent:SetOwner( ply )
        ent:SetKeyValue( "iMagnitude", "80" )
        ent:Spawn()
        ent:Fire( "Explode", 0, 0 )
        self:Remove()
        ent:EmitSound( "weapons/big_explosion.mp3", 511, 100 )

        -- util.BlastDamage( self, self.Owner, self:GetPos(), 50, 75 )

        self:EmitSound( "ambient/fire/gascan_ignite1.wav" )
        if SERVER then self:Remove() end
    end


    function ENT:Touch(ent)
        if ent != self.Owner then
        --self:Explode()
        end
    end

    function ENT:PhysicsCollide(data,phys)
        if ent != self.Owner and SERVER then
        --self:Explode()
        end
    end

end

scripted_ents.Register( ENT, "narwalball", true )
