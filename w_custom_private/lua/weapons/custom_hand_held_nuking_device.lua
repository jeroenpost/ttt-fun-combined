SWEP.Base = "gb_camo_base"
SWEP.ViewModelFOV	= 54
SWEP.HoldType = "pistol"
SWEP.PrintName = "Hand-held Supernova"
SWEP.ViewModel		= "models/weapons/cstrike/c_pist_deagle.mdl"
SWEP.WorldModel		= "models/weapons/w_pist_deagle.mdl"
SWEP.Kind = WEAPON_EQUIP
SWEP.Slot = 5
SWEP.AutoSpawnable = false
SWEP.Primary.Damage		    = 65
SWEP.Primary.Delay		    = 0.6
SWEP.Primary.ClipSize		= 150
SWEP.Primary.DefaultClip	= 250
SWEP.Primary.Cone	        = 0.008
SWEP.Primary.Automatic		= true
SWEP.Primary.Ammo = "AlyxGun"
SWEP.AmmoEnt = "item_ammo_revolver_ttt"
SWEP.Primary.Sound          = Sound("Weapon_Deagle.Single")
SWEP.HeadshotMultiplier = 1.5
SWEP.Camo = 5

SWEP.IronSightsPos = Vector(-6.361, -3.701, 2.15)
SWEP.IronSightsAng = Vector(0, 0, 0)

SWEP.nextsecond = 0
function SWEP:Think()
    if self.Owner:KeyDown(IN_USE) and self.nextsecond < CurTime() then
        self.nextsecond = CurTime() + 0.72
        local bullet = {}
        bullet.Num 		= 16
        bullet.Src 		= self.Owner:GetShootPos()
        bullet.Dir 		= self.Owner:GetAimVector()
        bullet.Spread 	= Vector( 0.020, 0.020, 0.020 )
        bullet.Tracer	= 1
        bullet.Force	= 5000
        bullet.Damage	= 5
        bullet.AmmoType = "AR2AltFire"
        self.Owner:FireBullets( bullet )
        self:EmitSound( "weapons/spas/spas-1.mp3")
        self:ShootEffects()
    end
end