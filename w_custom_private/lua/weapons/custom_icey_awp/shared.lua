
if SERVER then
   AddCSLuaFile(  )
   --resource.AddFile("materials/vgui/ttt_fun_killicons/awp.png")
end
SWEP.PrintName = "The Hit And Run"
SWEP.Slot      = 6 
   SWEP.SlotPos   = 6
if CLIENT then
   
   
   SWEP.Author = "GreenBlack"

   SWEP.ViewModelFOV  = 72
   SWEP.ViewModelFlip = true

    SWEP.Icon = "vgui/ttt_fun_killicons/awp.png"

   SWEP.EquipMenuData = {
      type = "Weapon",
      desc = "AWP Sniper Rifle; Only fires while scoped.\nInstakill\nVery loud"
   };
end


SWEP.Base				= "weapon_tttbase"
SWEP.HoldType			= "ar2"

SWEP.Primary.Delay       = 1
SWEP.Primary.Recoil      = 1.0
SWEP.Primary.Automatic   = false
SWEP.Primary.Damage      = 90
SWEP.Primary.Cone        = 0.0005
SWEP.Primary.Ammo        = "357"
SWEP.Primary.ClipSize    = 12
SWEP.Primary.ClipMax     = 12
SWEP.Primary.DefaultClip = 12
SWEP.Primary.Sound       = Sound( "weapons/awp/awp1.wav" )

SWEP.Tracer = 1

SWEP.Secondary.Sound = Sound("Default.Zoom")

SWEP.ViewModel  = "models/weapons/v_snip_awp.mdl"
SWEP.WorldModel = "models/weapons/w_snip_awp.mdl"

SWEP.IronSightsPos      = Vector( 5, -15, -2 )
SWEP.IronSightsAng      = Vector( 2.6, 1.37, 3.5 )


SWEP.Kind = WEAPON_GRENADE
SWEP.AutoSpawnable = false
SWEP.AmmoEnt = "item_ammo_357_ttt"
SWEP.InLoadoutFor = nil
SWEP.LimitedStock = false
SWEP.AllowDrop = true
SWEP.IsSilent = false
SWEP.NoSights = false


function SWEP:DryFire(setnext)
   if CLIENT and LocalPlayer() == self.Owner then
      self:EmitSound( "Weapon_Pistol.Empty" )
   end

   setnext(self, CurTime() +0.05)
end

function SWEP:SetZoom(state)
    if CLIENT then 
       return
    else
       if state then
          self.Owner:SetFOV(20, 0.3)
       else
          self.Owner:SetFOV(0, 0.2)
       end
      end
end

function SWEP:CanPrimaryAttack()
   if not IsValid(self.Owner) then return end

   --If the weapon is out of ammunition, or not scoped in we can't fire.
   if self.Weapon:Clip1() <= 0  then
      self:DryFire(self.SetNextPrimaryFire)
	  self:SetNextSecondaryFire(CurTime() +0.05)
      return false
   end
   return true
end

-- Add some zoom to ironsights for this gun
function SWEP:SecondaryAttack()
    if not self.IronSightsPos then return end
    if self.Weapon:GetNextSecondaryFire() > CurTime() then return end
    
    bIronsights = not self:GetIronsights()
    
    self:SetIronsights( bIronsights )
    
    if SERVER then
        self:SetZoom(bIronsights)
     else
        self:EmitSound(self.Secondary.Sound)
    end
    
    self.Weapon:SetNextSecondaryFire( CurTime() + 0.3)
	self.Weapon:SetNextPrimaryFire( CurTime() + 0.75) --Set a long delay to prevent people from quickly scoping in and shooting.
end

function SWEP:PreDrop()
    self:SetZoom(false)
    self:SetIronsights(false)
    return self.BaseClass.PreDrop(self)
end

function SWEP:Holster()
    self:SetIronsights(false)
    self:SetZoom(false)
    return true
end

if CLIENT then
   local scope = surface.GetTextureID("sprites/scope")
   function SWEP:DrawHUD()
      if self:GetIronsights() then
         surface.SetDrawColor( 0, 0, 0, 255 )
         
         local x = ScrW() / 2.0
         local y = ScrH() / 2.0
         local scope_size = ScrH()

         -- crosshair
         local gap = 80
         local length = scope_size
         surface.DrawLine( x - length, y, x - gap, y )
         surface.DrawLine( x + length, y, x + gap, y )
         surface.DrawLine( x, y - length, x, y - gap )
         surface.DrawLine( x, y + length, x, y + gap )

         gap = 0
         length = 50
         surface.DrawLine( x - length, y, x - gap, y )
         surface.DrawLine( x + length, y, x + gap, y )
         surface.DrawLine( x, y - length, x, y - gap )
         surface.DrawLine( x, y + length, x, y + gap )


         -- cover edges
         local sh = scope_size / 2
         local w = (x - sh) + 2
         surface.DrawRect(0, 0, w, scope_size)
         surface.DrawRect(x + sh - 2, 0, w, scope_size)

         surface.SetDrawColor(255, 0, 0, 255)
         surface.DrawLine(x, y, x + 1, y + 1)

         -- scope
         surface.SetTexture(scope)
         surface.SetDrawColor(255, 255, 255, 255)

         surface.DrawTexturedRectRotated(x, y, scope_size, scope_size, 0)

      else
         return self.BaseClass.DrawHUD(self)
      end
   end

   function SWEP:AdjustMouseSensitivity()
      return (self:GetIronsights() and 0.2) or nil
   end
end