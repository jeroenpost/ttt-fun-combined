if SERVER then

   AddCSLuaFile(  )
   --resource.AddFile("vgui/ttt_fun_killicons/m4a1.png")
end
 

SWEP.HoldType			= "ar2"
  SWEP.PrintName			=  "The American Eagle"
   SWEP.Slot				= 3

if CLIENT then

 

   SWEP.Icon = "vgui/ttt_fun_killicons/m4a1.png"
end


SWEP.Base				= "aa_base"
SWEP.Spawnable = true
SWEP.AdminSpawnable = true

SWEP.Kind = WEAPON_EQUIP3


SWEP.Primary.Delay			= 0.14
SWEP.Primary.Recoil			= 1.5
SWEP.Primary.Automatic = true
SWEP.Primary.Ammo = "smg1"
SWEP.Primary.Damage = 22
SWEP.Primary.Cone = 0.018
SWEP.Primary.ClipSize = 30
SWEP.Primary.ClipMax = 60
SWEP.Primary.DefaultClip = 30
SWEP.AutoSpawnable      = false
SWEP.AmmoEnt = "item_ammo_smg1_ttt"
SWEP.ViewModel			= "models/weapons/v_rif_m4a1.mdl"
SWEP.WorldModel			= "models/weapons/w_rif_m4a1.mdl"
SWEP.HeadshotMultiplier = 1
SWEP.Primary.Sound = Sound( "Weapon_M4A1.Silenced" )

SWEP.IronSightsPos 		= Vector( 6, 0, 0.95 )
SWEP.IronSightsAng 		= Vector( 2.6, 1.37, 3.5 )
SWEP.CanDecapitate= true


-- If AutoSpawnable is true and SWEP.Kind is not WEAPON_EQUIP1/2, then this gun can
-- be spawned as a random weapon. Of course this AK is special equipment so it won't,
-- but for the sake of example this is explicitly set to false anyway.
SWEP.AutoSpawnable = false

-- The AmmoEnt is the ammo entity that can be picked up when carrying this gun.
SWEP.AmmoEnt = "item_ammo_smg1_ttt"

-- InLoadoutFor is a table of ROLE_* entries that specifies which roles should
-- receive this weapon as soon as the round starts. In this case, none.
SWEP.InLoadoutFor = nil

-- If LimitedStock is true, you can only buy one per round.
SWEP.LimitedStock = true

-- If AllowDrop is false, players can't manually drop the gun with Q
SWEP.AllowDrop = true

-- If IsSilent is true, victims will not scream upon death.
SWEP.IsSilent = false

-- If NoSights is true, the weapon won't have ironsights
SWEP.NoSights = false



-- Add some zoom to ironsights for this gun
function SWEP:SecondaryAttack()
    self:MakeAFlame()
    timer.Create("fire"..self:EntIndex(),0.05,15, function() if IsValid(self) and IsValid(self.Owner) then self:MakeAFlame() end end)

end

