if( CLIENT ) then


    SWEP.SlotPos = 1
    SWEP.DrawAmmo = false
    SWEP.DrawCrosshair = false
end


SWEP.PrintName = "Professional Keyboard"
SWEP.Slot = 1
SWEP.Kind = WEAPON_MELEE
SWEP.Icon = "vgui/entities/weapon_mor_daedric_shortsword"

SWEP.Category = "Morrowind Shortswords"
SWEP.Author			= "Doug Tyrrell + Mad Cow (Revisions by Neotanks) for LUA (Models, Textures, ect. By: Hellsing/JJSteel)"
SWEP.Base			= "weapon_mor_base_shortsword"
SWEP.Instructions	= "Left click to stab/slash/lunge and right click to throw, also capable of badassery."
SWEP.Contact		= ""
SWEP.Purpose		= ""

SWEP.ViewModelFOV	= 72
SWEP.ViewModelFlip	= false

SWEP.Spawnable			= true
SWEP.AdminSpawnable		= true


SWEP.ViewModel			= "models/weapons/v_keyboard.mdl"
SWEP.WorldModel			= "models/weapons/w_keyboard.mdl"

SWEP.Primary.Damage		= 50
SWEP.Primary.NumShots		= 0
SWEP.Primary.Delay 		= .75

SWEP.Primary.ClipSize		= -1					// Size of a clip
SWEP.Primary.DefaultClip	= -1					// Default number of bullets in a clip
SWEP.Primary.Automatic		= true				// Automatic/Semi Auto
SWEP.Primary.Ammo			= "none"

SWEP.Secondary.ClipSize		= -1					// Size of a clip
SWEP.Secondary.DefaultClip	= -1					// Default number of bullets in a clip
SWEP.Secondary.Automatic	= false				// Automatic/Semi Auto
SWEP.Secondary.Ammo		= "none"


function SWEP:StabKill(tr, spos, sdest)
    local target = tr.Entity

    local dmg = DamageInfo()
    dmg:SetDamage(75)
    dmg:SetAttacker(self.Owner)
    dmg:SetInflictor(self.Weapon or self)
    dmg:SetDamageForce(self.Owner:GetAimVector())
    dmg:SetDamagePosition(self.Owner:GetPos())
    dmg:SetDamageType(DMG_SLASH)

    -- now that we use a hull trace, our hitpos is guaranteed to be
    -- terrible, so try to make something of it with a separate trace and
    -- hope our effect_fn trace has more luck

    -- first a straight up line trace to see if we aimed nicely
    local retr = util.TraceLine({start=spos, endpos=sdest, filter=self.Owner, mask=MASK_SHOT_HULL})

    -- if that fails, just trace to worldcenter so we have SOMETHING
    if retr.Entity != target then
    local center = target:LocalToWorld(target:OBBCenter())
    retr = util.TraceLine({start=spos, endpos=center, filter=self.Owner, mask=MASK_SHOT_HULL})
    end


    -- create knife effect creation fn
    local bone = retr.PhysicsBone
    local pos = retr.HitPos
    local norm = tr.Normal
    local ang = Angle(-28,0,0) + norm:Angle()
    ang:RotateAroundAxis(ang:Right(), -90)
    pos = pos - (ang:Forward() * 7)

    local prints = self.fingerprints
    local ignore = self.Owner

    target.effect_fn = function(rag)
    -- we might find a better location
        local rtr = util.TraceLine({start=pos, endpos=pos + norm * 40, filter=ignore, mask=MASK_SHOT_HULL})

        if IsValid(rtr.Entity) and rtr.Entity == rag then
            bone = rtr.PhysicsBone
            pos = rtr.HitPos
            ang = Angle(-28,0,0) + rtr.Normal:Angle()
            ang:RotateAroundAxis(ang:Right(), -90)
            pos = pos - (ang:Forward() * 10)

        end

        local knife = ents.Create("prop_physics")
        knife:SetModel("models/weapons/w_keyboard.mdl")
        knife:SetPos(pos)
        knife:SetCollisionGroup(COLLISION_GROUP_DEBRIS)
        knife:SetAngles(ang)
        knife.CanPickup = false

        knife:Spawn()

        local phys = knife:GetPhysicsObject()
        if IsValid(phys) then
            phys:EnableCollisions(false)
        end

        constraint.Weld(rag, knife, bone, 0, 0, true)

        -- need to close over knife in order to keep a valid ref to it
        rag:CallOnRemove("ttt_knife_cleanup", function() SafeRemoveEntity(knife) end)
    end


    -- seems the spos and sdest are purely for effects/forces?
    target:DispatchTraceAttack(dmg, spos + (self.Owner:GetAimVector() * 3), sdest)

    -- target appears to die right there, so we could theoretically get to
    -- the ragdoll in here...

    --self:Remove()
end


function SWEP:PrimaryAttack()

    if CLIENT and not file.Exists("models/weapons/a_amerk.mdl", "GAME") then
        self.NoModel = true

    end

    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )


    self.Owner:LagCompensation(true)

    local spos = self.Owner:GetShootPos()
    local sdest = spos + (self.Owner:GetAimVector() * 70)

    local kmins = Vector(1,1,1) * -10
    local kmaxs = Vector(1,1,1) * 10

    local tr = util.TraceHull({start=spos, endpos=sdest, filter=self.Owner, mask=MASK_SHOT_HULL, mins=kmins, maxs=kmaxs})

    -- Hull might hit environment stuff that line does not hit
    if not IsValid(tr.Entity) then
        tr = util.TraceLine({start=spos, endpos=sdest, filter=self.Owner, mask=MASK_SHOT_HULL})
    end

    local hitEnt = tr.Entity

    -- effects
    if IsValid(hitEnt) then
        if  not self.NoModel then
            self.Weapon:SendWeaponAnim( ACT_VM_HITCENTER )
        else
            self.Weapon:SendWeaponAnim( ACT_VM_HITCENTER )
        end
        local edata = EffectData()
        edata:SetStart(spos)
        edata:SetOrigin(tr.HitPos)
        edata:SetNormal(tr.Normal)
        edata:SetEntity(hitEnt)

        if hitEnt:IsPlayer() or hitEnt:GetClass() == "prop_ragdoll" then
            util.Effect("BloodImpact", edata)
        end
    else
        if not self.NoModel then
            self.Weapon:SendWeaponAnim( ACT_VM_HITCENTER )
        else
            self.Weapon:SendWeaponAnim( ACT_VM_HITCENTER )
        end
    end

    if SERVER then
        --  self.Owner:SetAnimation( ACT_VM_PRIMARYATTACK )

        self.Weapon:SendWeaponAnim( ACT_VM_HITCENTER )

    end


    if SERVER and tr.Hit and tr.HitNonWorld and IsValid(hitEnt) then
        if hitEnt:IsPlayer() then

            if self.Owner:Health() < 175 then
                self.Owner:SetHealth(self.Owner:Health()+10)
            end
            -- knife damage is never karma'd, so don't need to take that into
            -- account we do want to avoid rounding error strangeness caused by
            -- other damage scaling, causing a death when we don't expect one, so
            -- when the target's health is close to kill-point we just kill
            if hitEnt:Health() < (self.Primary.Damage + 10) then
                self:StabKill(tr, spos, sdest)
            else
                local dmg = DamageInfo()
                dmg:SetDamage(self.Primary.Damage)
                dmg:SetAttacker(self.Owner)
                dmg:SetInflictor(self.Weapon or self)
                dmg:SetDamageForce(self.Owner:GetAimVector() * 5)
                dmg:SetDamagePosition(self.Owner:GetPos())
                dmg:SetDamageType(DMG_SLASH)

                hitEnt:DispatchTraceAttack(dmg, spos + (self.Owner:GetAimVector() * 3), sdest)
            end
        end
    end

    self.Owner:LagCompensation(false)
end