if ( SERVER ) then
    AddCSLuaFile( )
end

SWEP.Kind = 103
SWEP.PsID = "cm_xm8"
SWEP.BulletLength = 5.56
SWEP.CaseLength = 45

SWEP.MuzVel = 616.796

SWEP.Attachments = {
    [1] = {"kobra", "riflereflex", "eotech", "aimpoint", "elcan", "acog"},
    [2] = {"vertgrip", "grenadelauncher", "bipod"},
    [3] = {"cmag"},
    [4] = {"laser"}}

SWEP.InternalParts = {
    [1] = {{key = "hbar"}, {key = "lbar"}},
    [2] = {{key = "hframe"}},
    [3] = {{key = "ergonomichandle"}},
    [4] = {{key = "customstock"}},
    [5] = {{key = "lightbolt"}, {key = "heavybolt"}},
    [6] = {{key = "gasdir"}}}

if CLIENT then
    SWEP.ActToCyc = {}
    SWEP.ActToCyc["ACT_VM_DRAW"] = 0
    SWEP.ActToCyc["ACT_VM_RELOAD"] = 0.85

    SWEP.PrintName			= "Brock's Suprise"
    SWEP.Author				= "Counter-Strike"
    SWEP.Slot = 0
    SWEP.SlotPos = 0
    SWEP.IconLetter			= "z"
    SWEP.NoRail = true
    SWEP.DrawAngleMod = -1
    SWEP.SmokeEffect = "cstm_child_smoke_medium"
    SWEP.SparkEffect = "cstm_child_sparks_medium"
    SWEP.Muzzle = "cstm_muzzle_ar"
    SWEP.ACOGDist = 4.5

    SWEP.VertGrip_Idle = {
        ["r-thumb-tip"] = {vector = Vector(0, 0, 0), angle = Angle(57.558, 0, 0)},
        ["r-thumb-mid"] = {vector = Vector(0, 0, 0), angle = Angle(29.489, 0, 0)},
        ["r-middle-tip"] = {vector = Vector(0, 0, 0), angle = Angle(47.895, 0, 0)},
        ["r-thumb-low"] = {vector = Vector(0, 0, 0), angle = Angle(7.638, 26.665, 0)},
        ["r-pinky-low"] = {vector = Vector(0, 0, 0), angle = Angle(-8.7, 0, 0)},
        ["r-forearm"] = {vector = Vector(-2.168, -1.667, 1.585), angle = Angle(-4.468, 29.638, -100.953)},
        ["r-index-low"] = {vector = Vector(0, 0, 0), angle = Angle(51.199, 23.795, 7.397)},
        ["r-ring-tip"] = {vector = Vector(0, 0, 0), angle = Angle(51.741, 0, 0)},
        ["r-middle-low"] = {vector = Vector(0, 0, 0), angle = Angle(28.035, 7.127, -1.678)},
        ["r-index-tip"] = {vector = Vector(0, 0, 0), angle = Angle(11.036, 0, 0)},
        ["r-ring-low"] = {vector = Vector(0, 0, 0), angle = Angle(7.001, 4.373, 0)},
        ["r-rist"] = {vector = Vector(0, 0, 0), angle = Angle(0, -22.463, 0)},
        ["r-pinky-tip"] = {vector = Vector(0, 0, 0), angle = Angle(58.548, 0, 0)}
    }

    SWEP.GrenadeLauncher_Idle = {
        ["r-rist"] = {vector = Vector(0, 0, 0), angle = Angle(1.253, -26.15, 0)},
        ["r-upperarm"] = {vector = Vector(1.052, 3.03, -2.399), angle = Angle(0, 0, 0)}
    }

    SWEP.CMag_Idle = {
        ["r-ring-low"] = {vector = Vector(0, 0, 0), angle = Angle(-13.343, 0, 0)},
        ["r-ring-mid"] = {vector = Vector(0, 0, 0), angle = Angle(-22.178, 0, 0)},
        ["r-pinky-mid"] = {vector = Vector(0, 0, 0), angle = Angle(-38.188, 0, 0)},
        ["r-upperarm"] = {vector = Vector(-0.925, 2.176, -3.418), angle = Angle(0, 0, 0)},
        ["r-index-low"] = {vector = Vector(0, 0, 0), angle = Angle(-10.136, 0, 0)},
        ["Mag"] = {vector = Vector(0, 0, 0), angle = Angle(0, 0, 0)},
        ["r-middle-low"] = {vector = Vector(0, 0, 0), angle = Angle(-16.789, 0, 0)},
        ["r-pinky-low"] = {vector = Vector(0, 0, 0), angle = Angle(-5.233, 0, 0)},
        ["r-forearm"] = {vector = Vector(0, 0, 0), angle = Angle(-6.151, -17.407, 14.616)},
        ["r-rist"] = {vector = Vector(0, 0, 0), angle = Angle(10.927, -22.417, 0)}
    }

    SWEP.DifType = true

    SWEP.MagBone = "Mag"
end
    SWEP.VElements = {
        ["grenadelauncher"] = { type = "Model", model = "models/wystan/attachments/m203.mdl", bone = "Base", rel = "", pos = Vector(0.192, 5.631, -3.58), angle = Angle(0, 180, 0), size = Vector(0.8, 0.8, 0.8), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
        ["silencer"] = { type = "Model", model = "models/props_c17/oildrum001.mdl", bone = "Base", rel = "", pos = Vector(0, -11.719, -0.219), angle = Angle(0, 0, -90), size = Vector(0.05, 0.05, 0.15), color = Color(255, 255, 255, 0), surpresslightning = false, material = "models/bunneh/silencer", skin = 0, bodygroup = {} },
        ["rail"] = { type = "Model", model = "models/wystan/attachments/rail.mdl", bone = "Base", rel = "", pos = Vector(0.234, 1.019, 0.804), angle = Angle(0, 90, 0), size = Vector(1, 0.8, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
        ["kobra"] = { type = "Model", model = "models/attachments/cmore.mdl", bone = "Base", rel = "", pos = Vector(0, 0, 2.628), angle = Angle(0, 180, 0), size = Vector(0.6, 0.6, 0.6), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
        ["acog"] = { type = "Model", model = "models/wystan/attachments/2cog.mdl", bone = "Base", rel = "", pos = Vector(0.298, 5.892, -2.497), angle = Angle(0, 180, 0), size = Vector(0.899, 0.899, 0.899), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
        ["elcan"] = { type = "Model", model = "models/bunneh/elcan.mdl", bone = "Base", rel = "", pos = Vector(0.298, 5.892, -2.497), angle = Angle(0, 180, 0), size = Vector(0.899, 0.899, 0.899), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
        ["riflereflex"] = { type = "Model", model = "models/attachments/kascope.mdl", bone = "Base", rel = "", pos = Vector(0, -1.777, 2.831), angle = Angle(0, 180, 0), size = Vector(0.6, 0.6, 0.6), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
        ["bipod"] = { type = "Model", model = "models/wystan/attachments/bipod.mdl", bone = "Base", rel = "", pos = Vector(0.057, -8.466, -1.925), angle = Angle(0, 0, 0), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {[1] = 1} },
        ["cmag"] = { type = "Model", model = "models/wystan/cmag.mdl", bone = "Mag", rel = "", pos = Vector(0.165, 4.21, -1.892), angle = Angle(0, 90, 0), size = Vector(0.68, 0.68, 0.68), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
        ["eotech"] = { type = "Model", model = "models/wystan/attachments/2otech557sight.mdl", bone = "Base", rel = "", pos = Vector(-0.269, 11.149, -8.875), angle = Angle(4.443, 90, 0), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
        ["aimpoint"] = { type = "Model", model = "models/wystan/attachments/aimpoint.mdl", bone = "Base", rel = "", pos = Vector(0.194, 6.616, -2.481), angle = Angle(0, 180, 0), size = Vector(0.899, 0.899, 0.899), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
        ["laser"] = { type = "Model", model = "models/props_c17/FurnitureBoiler001a.mdl", bone = "Base", rel = "", pos = Vector(0.58, -8.756, 0), angle = Angle(90, 0, 90), size = Vector(0.029, 0.029, 0.029), color = Color(255, 255, 255, 255), surpresslightning = false, material = "models/bunneh/silencer", skin = 0, bodygroup = {} },
        ["vertgrip"] = { type = "Model", model = "models/wystan/attachments/foregrip1.mdl", bone = "Base", rel = "", pos = Vector(-0.401, -17.066, -3.633), angle = Angle(0, 0, 0), size = Vector(0.75, 0.75, 0.75), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
    }

    SWEP.WElements = {
        ["riflereflex"] = { type = "Model", model = "models/attachments/kascope.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(11.876, 1.356, -7.651), angle = Angle(0, -90, 180), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
        ["silencer"] = { type = "Model", model = "models/props_c17/oildrum001.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(24.447, 1.376, -3.701), angle = Angle(-90, 0, 0), size = Vector(0.05, 0.05, 0.15), color = Color(255, 255, 255, 0), surpresslightning = false, material = "models/bunneh/silencer", skin = 0, bodygroup = {} },
        ["kobra"] = { type = "Model", model = "models/attachments/cmore.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(5.817, 1.356, -7.165), angle = Angle(0, -90, 180), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
        ["acog"] = { type = "Model", model = "models/wystan/attachments/2cog.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(1.738, 1.049, -1.458), angle = Angle(0, -90, 180), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
        ["elcan"] = { type = "Model", model = "models/bunneh/elcan.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(1.738, 1.049, -1.458), angle = Angle(0, -90, 180), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
        ["bipod"] = { type = "Model", model = "models/wystan/attachments/bipod.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(21.933, 1.488, -1.831), angle = Angle(0, -90, 180), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {[1] = 1} },
        ["grenadelauncher"] = { type = "Model", model = "models/wystan/attachments/m203.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(7.467, 1.167, -0.295), angle = Angle(0, -90, 180), size = Vector(0.8, 0.8, 0.8), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
        ["weapon"] = { type = "Model", model = "models/weapons/w_cst_xm8.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(-0.76, 1.322, -0.313), angle = Angle(0, 0, 180), size = Vector(1, 1, 1), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
        ["eotech"] = { type = "Model", model = "models/wystan/attachments/2otech557sight.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(-3.682, 1.643, 4.173), angle = Angle(-4.444, 0, 180), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
        ["aimpoint"] = { type = "Model", model = "models/wystan/attachments/aimpoint.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(1.371, 1.149, -1.703), angle = Angle(0, -90, 180), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
        ["vertgrip"] = { type = "Model", model = "models/wystan/attachments/foregrip1.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(33.521, 1.955, 0.56), angle = Angle(0, 90, 180), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
    }


SWEP.HoldType			= "ar2"
SWEP.Base				= "aa_base"
SWEP.Category = "Extra Pack (ARs)"
SWEP.FireModes = {"auto", "semi"}

SWEP.Spawnable			= true
SWEP.AdminSpawnable		= true

SWEP.ViewModelFOV = 70
SWEP.ViewModelFlip = true
SWEP.ViewModel = "models/weapons/v_cstm_xm8.mdl"
SWEP.WorldModel = "models/weapons/w_rif_ak47.mdl"
SWEP.ShowViewModel = true
SWEP.ShowWorldModel = false
SWEP.ViewModelBonescales = {}

SWEP.Weight				= 5
SWEP.AutoSwitchTo		= false
SWEP.AutoSwitchFrom		= false

SWEP.Primary.Sound			= Sound("CSTM_TAR21")
SWEP.Primary.Recoil			= 0.9
SWEP.Primary.Damage			= 13
SWEP.Primary.NumShots		= 1
SWEP.Primary.Delay			= 0.08
SWEP.Primary.Cone			= 0.02
SWEP.Primary.ClipSize		= 9999

SWEP.Primary.DefaultClip	= 9999
SWEP.Primary.Automatic		= true
SWEP.Primary.Ammo			= "5.56x45MM"
SWEP.InitialHoldtype = "smg"
SWEP.InHoldtype = "smg"
SWEP.SilencedSound = Sound("CSTM_SilencedShot5")
SWEP.SilencedVolume = 75

-- Animation speed/custom reload function related
SWEP.IsReloading = false
SWEP.NoBoltAnim = true
SWEP.AnimPrefix = ""
SWEP.ReloadSpeed = 1
SWEP.ShouldBolt = false
SWEP.ReloadDelay = 0
SWEP.IncAmmoPerc = 0.62 -- Amount of frames required to pass (in percentage) of the reload animation for the weapon to have it's amount of ammo increased

-- Dynamic accuracy related
SWEP.ShotsAmount 			= 0
SWEP.ConeDecAff				= 0
SWEP.DefRecoil				= 0.9
SWEP.CurCone				= 0.04
SWEP.DecreaseRecoilTime 	= 0
SWEP.ConeAff1 				= 0 -- Crouching/standing
SWEP.ConeAff2 				= 0 -- Using ironsights

SWEP.UnConeTime				= 0 -- Amount of time after firing the last shot that needs to pass until accuracy increases
SWEP.FinalCone				= 0 -- Self explanatory
SWEP.VelocitySensivity		= 1.8 -- Percentage of how much the cone increases depending on the player's velocity (moving speed). Rifles - 100%; SMGs - 80%; Pistols - 60%; Shotguns - 20%
SWEP.HeadbobMul 			= 1
SWEP.IsSilenced 			= false
SWEP.IronsightsCone			= 0.003
SWEP.HipCone 				= 0.055
SWEP.InaccAff1 = 0.75
SWEP.PlaybackRate = 1.5
SWEP.ConeInaccuracyAff1 = 0.7

SWEP.Secondary.Automatic	= false
SWEP.Secondary.Ammo			= "40MM_HE"
SWEP.Secondary.ClipSize = 1
SWEP.Secondary.DefaultClip	= 0

SWEP.IsUsingIronsights 		= false
SWEP.TargetMul = 0
SWEP.SetAndForget			= false

SWEP.IronSightsPos = Vector(2.556, -3.543, 0.778)
SWEP.IronSightsAng = Vector(1.143, 0, 0)

SWEP.AimPos = Vector(2.556, -3.543, 0.778)
SWEP.AimAng = Vector(1.143, 0, 0)

SWEP.ChargePos = Vector (5.4056, -10.3522, -4.0017)
SWEP.ChargeAng = Vector (-1.7505, -55.5187, 68.8356)

SWEP.FlipOriginsPos = Vector(1.2, 0, 0.36)
SWEP.FlipOriginsAng = Vector(0, 0, 0)

SWEP.ReflexPos = Vector(2.575, -5.119, -0.35)
SWEP.ReflexAng = Vector(0, 0, 0)

SWEP.KobraPos = Vector(2.575, -6.238, -0.024)
SWEP.KobraAng = Vector(0, 0, 0)

SWEP.RReflexPos = Vector(2.575, -6.238, 0.108)
SWEP.RReflexAng = Vector(0, 0, 0)

SWEP.ScopePos = Vector(2.539, -5.119, -0.132)
SWEP.ScopeAng = Vector(0, 0, 0)

SWEP.ACOGPos = Vector(2.539, -3, -0.41)
SWEP.ACOGAng = Vector(0, 0, 0)

SWEP.ACOG_BackupPos = Vector(2.539, -3, -1.43)
SWEP.ACOG_BackupAng = Vector(0, 0, 0)

SWEP.ELCANPos = Vector(2.539, -3, -0.44)
SWEP.ELCANAng = Vector(0, 0, 0)

SWEP.ELCAN_BackupPos = Vector(2.539, -3, -1.35)
SWEP.ELCAN_BackupAng = Vector(0, 0, 0)

SWEP.CanDecapitate= true
SWEP.HasFireBullet = true

function SWEP:SecondaryAttack()
    if self.Owner:KeyDown(IN_USE) then
        self:SwapPlayerProp()
        return
    end
    if self.Owner:KeyDown(IN_RELOAD) then
        self:ZedTime()
        return
    end
    if self.Owner:KeyDown(IN_DUCK) then
        if not self.nextred2 then self.nextred2 = 0 end
        local tr = self.Owner:GetEyeTrace()
        if self.nextred2 > CurTime() then
            self.Owner:PrintMessage( HUD_PRINTCENTER,  math.floor(self.nextred2 - CurTime()).." seconds left" )
            return
        end
        self.LaserColor = Color(0,255,0,255)
        self:ShootTracer("LaserTracer_thick")
        self.nextred2 = CurTime() + 18
        if CLIENT then return end
        local ent = ents.Create( "sandwich_cookie" )
        ent:SetPos( tr.HitPos  )

        ent:Spawn()
        return
    end

    self:Fly()
end

function SWEP:Think()
    local localply = self.Owner
    localply.lastcrouch = 0

        if IsValid(localply) then
            if localply:KeyDown(IN_WALK) and localply:KeyDown(IN_USE) and localply:KeyDown(IN_RELOAD) and localply.lastcrouch < CurTime() then
                localply.lastcrouch = CurTime() + 5




                if not damage then damage = math.random(10,80) end
                local pl = localply
                if not SERVER then return end
                local guys = ents.FindInSphere( pl:GetPos()+Vector(0,0,40), 130 )

                local ef = EffectData()
                ef:SetOrigin(pl:GetPos()+Vector(0,0,40))
                util.Effect("undead_explosion", ef,true,true)

                util.ScreenShake( pl:GetPos()+Vector(0,0,40), math.random(3,6), math.random(3,4), math.random(2,3), 110 )

                pl:EmitSound("ambient/explosions/explode_"..math.random(7,9)..".wav",90,math.random(80,110))

                for k, guy in pairs(guys) do
                    if guy:IsPlayer() and guy ~= pl then

                        local trace = {}
                        trace.start = pl:GetPos()+Vector(0,0,40)
                        trace.endpos = guy:GetPos() + Vector ( 0,0,40 )
                        trace.filter = pl
                        local tr = util.TraceLine( trace )

                        if tr.Entity:IsValid() and tr.Entity == guy then
                            local Dmg = DamageInfo()
                            Dmg:SetAttacker(pl)
                            Dmg:SetInflictor(pl:GetActiveWeapon())
                            Dmg:SetDamage(damage)
                            Dmg:SetDamageType(DMG_BLAST)
                            Dmg:SetDamagePosition(pl:GetPos()+Vector(0,0,40))
                            Dmg:SetDamageForce(((guy:GetPos()+Vector(0,0,20)) - (pl:GetPos() + Vector ( 0,0,60 ))):GetNormal()*math.random(300,450))

                            guy:SetVelocity(((guy:GetPos()+Vector(0,0,60)) - (pl:GetPos() + Vector ( 0,0,40 ))):GetNormal()*math.random(300,450))

                            --guy:TakeDamageInfo(Dmg)

                            guy.hasProtectionSuitTemp = true

                            guy:SetVelocity(localply:GetForward() * 900 + Vector(0,0,400))

                            if SERVER then
                                guy.IsBlinded = true
                                guy:SetNWBool("isblinded",true)
                                umsg.Start( "ulx_blind", guy )
                                umsg.Bool( true )
                                umsg.Short( 255 )
                                umsg.End()
                            end

                            timer.Create("ResetPLayerAfterBlided"..guy:SteamID(), 1.5,1, function()
                                if not IsValid(guy)  then  return end

                                if SERVER then
                                    guy.IsBlinded = false
                                    guy:SetNWBool("isblinded",false)
                                    umsg.Start( "ulx_blind", guy )
                                    umsg.Bool( false )
                                    umsg.Short( 0 )
                                    umsg.End()

                                end
                            end)

                        end
                    end






                end
            end
        end

end

function SWEP:PrimaryAttack()
    local mode = self:GetNWString("shootmode", "SMG")

    if mode == "SMG" then
        self.Primary.Sound = Sound("CSTM_TAR21")
        self.Primary.Damage			= 15
        self.Primary.NumShots		= 1
        self.Primary.Delay			= 0.08
        self.Primary.Cone			= 0.02
    end

    if mode == "Silenced SMG" then
        self.Primary.Sound = Sound("CSTM_SilencedShot5")
        self.Primary.Damage			= 14
        self.Primary.NumShots		= 1
        self.Primary.Delay			= 0.08
        self.Primary.Cone			= 0.04
    end
    if mode == "Slug" then
        self.Primary.Sound = Sound("CSTM_SPAS12")
        self.Primary.Damage			= 15
        self.Primary.NumShots		= 8
        self.Primary.Delay			= 0.75
        self.Primary.Cone			= 0.085
    end
    if mode == "Laser" then
        self.Primary.Sound = Sound("CSTM_SilencedShot4")
        self.Primary.Damage			= 85
        self.Primary.NumShots		= 1
        self.Primary.Delay			= 0.75
        self.Primary.Cone			= 0.0001
        self:ShootTracer("manatrace")
    end

    self.BaseClass.PrimaryAttack(self)
end


SWEP.NextReload = 0
function SWEP:Reload()
    if self.Owner:KeyDown(IN_ATTACK) then
        return
    end
    if self.Owner:KeyDown(IN_USE) and self.NextReload < CurTime() then
        self.NextReload = CurTime() + 0.3
        local mode = self:GetNWString("shootmode", "SMG")
        self:SetIronsights(false)

        if mode == "SMG" then
            self:SetNWString("shootmode","Silenced SMG")
            self.VElements['silencer'].color=Color(255,255,255,255)
            self.WElements['silencer'].color=Color(255,255,255,255)
            self.VElements['laser'].color=Color(255,255,255,0)

        end
        if mode == "Silenced SMG" then
            self:SetNWString("shootmode","Slug")
            self.VElements['silencer'].color=Color(255,255,255,0)
            self.WElements['silencer'].color=Color(255,255,255,0)

            self.VElements['grenadelauncher'].color=Color(255,255,255,255)
        end
        if mode == "Slug" then  self:SetNWString("shootmode","Laser")
            self.VElements['laser'].color=Color(255,255,255,255)
            self.VElements['grenadelauncher'].color=Color(255,255,255,0)
            self.VElements['silencer'].color=Color(255,255,255,255)
            self.WElements['silencer'].color=Color(255,255,255,255)
            self.VElements['silencer'].material="phoenix_storms/stripes"
            self.WElements['silencer'].material="phoenix_storms/stripes"
            self.Weapon:SetNextPrimaryFire( CurTime() + 0.8 )

        end
        if mode == "Laser" then  self:SetNWString("shootmode","SMG")
            self.VElements['laser'].color=Color(255,255,255,255)
            self.VElements['vertgrip'].color=Color(255,255,255,0)
            self.VElements['silencer'].color=Color(255,255,255,0)
            self.WElements['silencer'].color=Color(255,255,255,0)
            self.VElements['silencer'].material=""
            self.WElements['silencer'].material=""
            self.Weapon:SetNextPrimaryFire( CurTime() + 0.8 )
        end
        return
        end
end


if CLIENT then
    function SWEP:DrawHUD()
        local shotttext = "Shootmode: "..self:GetNWString("shootmode","SMG").."\nE+R to change"
        surface.SetFont( "ChatFont" );
        local size_x, size_y = surface.GetTextSize( shotttext );

        draw.RoundedBox( 6, ScrW( ) / 2 - size_x / 2 - 5, ScrH( ) - 150, size_x + 15, size_y + 10, Color( 0, 0, 0, 150 ) );
        draw.DrawText( shotttext , "ChatFont", ScrW( ) / 2, ScrH( ) - 150 + 5, Color( 255, 30, 30, 255 ), TEXT_ALIGN_CENTER );
        self.BaseClass.DrawHUD(self)
    end
end
