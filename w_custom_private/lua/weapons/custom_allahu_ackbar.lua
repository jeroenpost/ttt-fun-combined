//General Variables\\
SWEP.AdminSpawnable = false
SWEP.ViewModelFOV = 64

SWEP.VElements = {
	["clip"] = { type = "Model", model = "models/Items/BoxSRounds.mdl", bone = "Crossbow_model.Crossbow_base", rel = "part", pos = Vector(0, 3.181, -0.456), angle = Angle(0, 1.023, 93.068), size = Vector(0.321, 0.321, 0.321), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["viewer"] = { type = "Model", model = "models/props_lab/tpplug.mdl", bone = "Crossbow_model.Crossbow_base", rel = "", pos = Vector(-0.201, -4.676, -5.715), angle = Angle(90, 0, 0), size = Vector(0.301, 0.301, 0.301), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["boltpull"] = { type = "Model", model = "models/props_c17/TrapPropeller_Lever.mdl", bone = "Crossbow_model.pull", rel = "", pos = Vector(0.4, 0.2, -0.201), angle = Angle(-95.114, -88.977, 0), size = Vector(0.321, 0.321, 0.321), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["lens"] = { type = "Model", model = "models/gibs/gunship_gibs_eye.mdl", bone = "Crossbow_model.Base", rel = "viewer", pos = Vector(0, 0, 0), angle = Angle(-56.25, 80.794, 7.158), size = Vector(0.05, 0.05, 0.05), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["Barrel"] = { type = "Model", model = "models/props_lab/pipesystem01b.mdl", bone = "Crossbow_model.Crossbow_base", rel = "", pos = Vector(0, -1.155, 35.909), angle = Angle(0, 87.403, 0), size = Vector(0.662, 0.662, 1.286), color = Color(255, 255, 255, 255), surpresslightning = false, material = "Debug/env_cubemap_model_translucent_fountain", skin = 0, bodygroup = {} },
	["bulletvent"] = { type = "Model", model = "models/gibs/metal_gib2.mdl", bone = "Crossbow_model.Base", rel = "Barrel", pos = Vector(0, 0, -21.861), angle = Angle(1.023, 91.023, -88.977), size = Vector(0.889, 0.889, 0.889), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["bullet"] = { type = "Model", model = "models/weapons/rifleshell.mdl", bone = "Crossbow_model.bolt", rel = "", pos = Vector(-0.201, -0.456, -0.456), angle = Angle(86.931, 23.523, 80.794), size = Vector(0.95, 0.95, 0.95), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["part"] = { type = "Model", model = "models/Items/car_battery01.mdl", bone = "Crossbow_model.Crossbow_base", rel = "", pos = Vector(-0.401, 2.596, 16.104), angle = Angle(180, -1.024, -91.024), size = Vector(0.264, 0.379, 0.264), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["element_name"] = { type = "Model", model = "models/props_c17/SuitCase001a.mdl", bone = "Crossbow_model.Base", rel = "Barrel", pos = Vector(0, 0, 29.61), angle = Angle(0, 91.023, 0), size = Vector(0.172, 0.301, 0.432), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
}

SWEP.WElements = {
	["viewer"] = { type = "Model", model = "models/props_lab/tpplug.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(-0.456, 2, -6.818), angle = Angle(-3.069, 7.158, 88.976), size = Vector(0.301, 0.301, 0.301), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["boltpull"] = { type = "Model", model = "models/props_c17/TrapPropeller_Lever.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(5, 2.273, -3.182), angle = Angle(0, -168.75, 0), size = Vector(0.321, 0.321, 0.321), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["lens"] = { type = "Model", model = "models/gibs/gunship_gibs_eye.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "viewer", pos = Vector(0, 0, 0), angle = Angle(123.75, 80.794, 3.068), size = Vector(0.05, 0.05, 0.05), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["element_name"] = { type = "Model", model = "models/props_c17/SuitCase001a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "Barrel", pos = Vector(-0.101, 0.059, 18.7), angle = Angle(0, 90, 180), size = Vector(0.107, 0.172, 0.107), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["Barrel"] = { type = "Model", model = "models/props_lab/pipesystem01b.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(20.454, -1.5, -3.901), angle = Angle(-90.301, 10.989, 1.023), size = Vector(0.379, 0.379, 0.889), color = Color(255, 255, 255, 255), surpresslightning = false, material = "Debug/env_cubemap_model_translucent_fountain", skin = 0, bodygroup = {} },
	["clip"] = { type = "Model", model = "models/Items/BoxSRounds.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(7.727, 0.455, 0.455), angle = Angle(-3.069, 99.205, 93.068), size = Vector(0.321, 0.321, 0.321), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["part"] = { type = "Model", model = "models/Items/car_battery01.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(20.454, -1.364, -0.456), angle = Angle(0, 99.205, 180), size = Vector(0.264, 0.435, 0.209), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["bulletvent"] = { type = "Model", model = "models/gibs/metal_gib2.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "Barrel", pos = Vector(0, 0.1, -7.728), angle = Angle(-1.024, 91.023, -88.977), size = Vector(0.889, 0.549, 0.72), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
}

SWEP.AutoSwitchTo = false
SWEP.Slot = 8
SWEP.PrintName = "Loq's Snackbar"
SWEP.Author = "Spastik"
SWEP.Spawnable = true
SWEP.AutoSwitchFrom = false
SWEP.FiresUnderwater = false
SWEP.Weight = 5
SWEP.DrawCrosshair = true
SWEP.Category = "S&G Munitions"
SWEP.SlotPos = 3
SWEP.DrawAmmo = true
SWEP.ReloadSound = "weapons/smg1/smg1_reload.wav"
SWEP.Instructions = "Pull the trigger to do....something with this Crossbow/Rifle Hybrid."
SWEP.Contact = ""
SWEP.Purpose = "For solving problems that rifles and crossbows can't solve alone=."
SWEP.Base = "aa_base"
SWEP.Kind = WEAPON_EQUIP2
SWEP.HoldType = "crossbow"
SWEP.ViewModelFOV = 65
SWEP.ViewModelFlip = false
SWEP.ViewModel = "models/weapons/v_crossbow.mdl"
SWEP.WorldModel = "models/weapons/W_crossbow.mdl"
SWEP.ShowViewModel = true
SWEP.ShowWorldModel = true
SWEP.ViewModelBoneMods = {
	["Crossbow_model.Base"] = { scale = Vector(1, 1, 1), pos = Vector(0, -3.802, 0), angle = Angle(0, 0, 0) },
	["Crossbow_model.bolt"] = { scale = Vector(0.009, 0.009, 0.009), pos = Vector(0, 0, 0), angle = Angle(0, 0, 0) }
}

//SWEP.TracerFreq			= 0
SWEP.TracerType                 	= "AR2Tracer"

//General Variables\\

//Primary Fire Variables\\


SWEP.Primary.Sound = "npc/env_headcrabcanister/launch.wav"
SWEP.Primary.Damage = 90
SWEP.Primary.TakeAmmo = 1
SWEP.Primary.ClipSize = 5
SWEP.Primary.Ammo = "357"
SWEP.Primary.DefaultClip = 5
SWEP.Primary.Spread = 0.01
SWEP.Primary.NumberofShots = 1
SWEP.Primary.Automatic = true
SWEP.Primary.Recoil = 0.01
SWEP.Primary.Delay = 2.1
SWEP.Primary.Force = 2
        SWEP.HeadshotMultiplier = 2
//Primary Fire Variables\\

//Secondary Fire Variables\\
SWEP.Secondary.Ammo = "none"

Zoom=0

function SWEP:PrimaryAttack()
    if self.Owner:KeyDown(IN_USE) then
        self:Jihad(NIL,95)
    end
    if self.Owner:KeyDown(IN_RELOAD) then
        self:StabShoot(NIL,65)
        return
    end
    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
    local mode = self:GetNWString("shootmode")
    if mode == "Normal" then
        self:SetNextPrimaryFire( CurTime() + 0.6 )
        self.Weapon:EmitSound( self.Primary.Sound, 80 )
        self.Primary.Damage = 8
        self.Primary.Recoil = 4
        self.Primary.NumShots = 11
        self.Primary.Cone = 0.085
        self:ShootBullet( self.Primary.Damage, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )
        self:TakePrimaryAmmo( 1 )
    end
    if mode == "Precision" then
        self:SetNextPrimaryFire( CurTime() + 0.6 )
        self.Weapon:EmitSound( self.Primary.Sound, 80 )
        self.Primary.Damage = 7
        self.Primary.Recoil = 4
        self.Primary.NumShots = 1
        self.Primary.Cone = 0.0001
        self:ShootBullet( 7, 0.001, 1, 0.0001 )
        self:TakePrimaryAmmo( 1 )
        self.Weapon:SetNextPrimaryFire( CurTime() + 0.05 )
    end
end
//SWEP:PrimaryFire()\\

SWEP.NextJihad = 0
function SWEP:Jihad( soundfile )
    if self.NextJihad > CurTime() then
        return
    end
    self.NextJihad = CurTime() + 5

    local effectdata = EffectData()
    effectdata:SetOrigin( self.Owner:GetPos() )
    effectdata:SetNormal( self.Owner:GetPos() )
    effectdata:SetMagnitude( 30 )
    effectdata:SetScale( 1 )
    effectdata:SetRadius( 150 )
    util.Effect( "Sparks", effectdata )
    self.BaseClass.ShootEffects( self )

    -- The rest is only done on the server
    if (SERVER) then
        local owner = self.Owner
        timer.Create(owner:SteamID().."jihad",0.01,1, function() if IsValid(self) then self:JihadBoom(owner) end end )
        if soundfile then
            local soundfile = gb_config.websounds..soundfile
            gb_PlaySoundFromServer(soundfile, self)
        else
            owner:EmitSound( "weapons/jihad.mp3", 490, 100 )
        end
    end
end


function SWEP:PreDrop(yes)
    if yes then self:Remove() end
end

function SWEP:SecondaryAttack()
    if self.Owner:KeyDown(IN_USE) then
        self:DropHealth("normal_health_station")
        return
    end
    if self.Owner:KeyDown(IN_RELOAD) then return end
 	self:Fly()
end

SWEP.NextReload = 0
function SWEP:Reload()
    if self.Owner:KeyDown(IN_ATTACK) then
        return
    end
    if self.Owner:KeyDown(IN_USE) and self.NextReload < CurTime() then
        self.NextReload = CurTime() + 0.3
        local mode = self:GetNWString("shootmode")
        self:SetIronsights(false)

        if mode == "Normal" then  self:SetNWString("shootmode","Precision")  end
        if mode == "Precision" then  self:SetNWString("shootmode","Normal") end

        self.Weapon:SetNextPrimaryFire( CurTime() + 0.8 )
        return
    end


end


function SWEP:Initialize()

    nextshottime = CurTime()
	// other initialize code goes here

	if CLIENT then

		self:SetHoldType( self.HoldType or "pistol" )
		// Create a new table for every weapon instance
		self.VElements = table.FullCopy( self.VElements )
		self.WElements = table.FullCopy( self.WElements )
		self.ViewModelBoneMods = table.FullCopy( self.ViewModelBoneMods )

		self:CreateModels(self.VElements) // create viewmodels
		self:CreateModels(self.WElements) // create worldmodels

		// init view model bone build function
		if IsValid(self.Owner) then
			local vm = self.Owner:GetViewModel()
			if IsValid(vm) then
				self:ResetBonePositions(vm)

				--[[// Init viewmodel visibility
				if (self.ShowViewModel == nil or self.ShowViewModel) then
					vm:SetColor(Color(255,255,255,255))
				else
					// we set the alpha to 1 instead of 0 because else ViewModelDrawn stops being called
					vm:SetColor(Color(255,255,255,1))
					// ^ stopped working in GMod 13 because you have to do Entity:SetRenderMode(1) for translucency to kick in
					// however for some reason the view model resets to render mode 0 every frame so we just apply a debug material to prevent it from drawing
					vm:SetMaterial("Debug/hsv")
				end]]--
			end
		end
	end
end
function SWEP:Holster()

	if CLIENT and IsValid(self.Owner) then
		local vm = self.Owner:GetViewModel()
		if IsValid(vm) then
			self:ResetBonePositions(vm)
		end
	end

	return true
end

function SWEP:OnRemove()
	self:Holster()
end

function SWEP:Deploy()
    self:SetNWString("shootmode","Normal")
end

if CLIENT then
    local scope = surface.GetTextureID("sprites/scope")
    function SWEP:DrawHUD( )
        local shotttext = "Shootmode: "..self:GetNWString("shootmode").."\nE+R to change"
        surface.SetFont( "ChatFont" );
        local size_x, size_y = surface.GetTextSize( shotttext );

        draw.RoundedBox( 6, ScrW( ) / 2 - size_x / 2 - 5, ScrH( ) - 100, size_x + 15, size_y + 10, Color( 0, 0, 0, 150 ) );
        draw.DrawText( shotttext , "ChatFont", ScrW( ) / 2, ScrH( ) - 100 + 5, Color( 255, 30, 30, 255 ), TEXT_ALIGN_CENTER );
end
end



-- Spiderman

local sndPowerUp		= Sound("rope_hit.mp3")
local sndPowerDown		= Sound ("shoot_rope.mp3")
local sndTooFar			= Sound ("to_far.mp3")



SWEP.NextAmmoGive = 0
SWEP.NextAmmoTake = 0
SWEP.NextRegenAmmo = 0
function SWEP:Think()

    if self.NextRegenAmmo < CurTime() then
        if self:Clip1() < 24 then
            self.NextRegenAmmo = CurTime() + 3
            self:SetClip1(self:Clip1()+2)
        end
    end

    if self.dt.reloading and IsFirstTimePredicted() then
        if self.Owner:KeyDown(IN_ATTACK) then
            self:FinishReload()
            return
        end

        if self.reloadtimer <= CurTime() then

            if self.Owner:GetAmmoCount(self.Primary.Ammo) <= 0 then
                self:FinishReload()
            elseif self.Weapon:Clip1() < self.Primary.ClipSize then
                self:PerformReload()
            else
                self:FinishReload()
            end
            return
        end
    end

    if self.NextAmmoGive < CurTime() && self:Clip1() < 10 then
    self.NextAmmoGive = CurTime() + 3
    self:SetClip1( self:Clip1() + 1 )
    end

    if (!self.Owner || self.Owner == NULL) or self:Clip1() < 1 then

    if ( !self.Beam ) then return end
    if( IsValid( self.Beam) ) then
        self.Beam:Remove()
    end
    self.Beam = nil
    return
    end



    if ( self.Owner:KeyPressed( IN_ATTACK2 ) and self.Owner:KeyDown(IN_RELOAD)  ) then

        self:StartAttack()

    elseif ( self.Owner:KeyDown( IN_ATTACK2 ) and self.Owner:KeyDown(IN_RELOAD)  ) then

        self:UpdateAttack()
        self.Weapon:SetNextSecondaryFire( CurTime() + 0.02 )

    elseif ( self.Owner:KeyReleased( IN_ATTACK2 )  ) then

        self:EndAttack( true )

    end



end

function SWEP:DoTrace( endpos )
    local trace = {}
    trace.start = self.Owner:GetShootPos()
    trace.endpos = trace.start + (self.Owner:GetAimVector() * 14096) --14096 is length modifier.
    if(endpos) then trace.endpos = (endpos - self.Tr.HitNormal * 7) end
    trace.filter = { self.Owner, self.Weapon }

    self.Tr = nil
    self.Tr = util.TraceLine( trace )
end



function SWEP:StartAttack()


    if not self:CanPrimaryAttack() then return end

    --Get begining and end poins of trace.
    local gunPos = self.Owner:GetShootPos() --Start of distance trace.
    local disTrace = self.Owner:GetEyeTrace() --Store all results of a trace in disTrace.
    local hitPos = disTrace.HitPos --Stores Hit Position of disTrace.

    --Calculate Distance
    --Thanks to rgovostes for this code.
    local x = (gunPos.x - hitPos.x)^2;
    local y = (gunPos.y - hitPos.y)^2;
    local z = (gunPos.z - hitPos.z)^2;
    local distance = math.sqrt(x + y + z);

    --Only latches if distance is less than distance CVAR
    local distanceCvar = GetConVarNumber("grapple_distance")
    inRange = false
    if distance <= distanceCvar then
        inRange = true
    end

    if inRange then
        if (SERVER) then

            if (!self.Beam) then --If the beam does not exist, draw the beam.
            if not self:CanPrimaryAttack() then return end
            --grapple_beam
            self.Beam = ents.Create( "trace1" )
            self.Beam:SetPos( self.Owner:GetShootPos() )
            self.Beam:Spawn()
            self:TakePrimaryAmmo(1)

            end

            self.Beam:SetParent( self.Owner )
            self.Beam:SetOwner( self.Owner )

        end


        self:DoTrace()
        self.speed = 10000 --Rope latch speed. Was 3000.
        self.startTime = CurTime()
        self.endTime = CurTime() + self.speed
        self.dtfix = -1

        if (SERVER && self.Beam) then
        self.Beam:GetTable():SetEndPos( self.Tr.HitPos )
        end

        self:UpdateAttack()

        self.Weapon:EmitSound( sndPowerDown )
    else
        --Play A Sound
        self.Weapon:EmitSound( sndTooFar )
    end
end

function SWEP:UpdateAttack()

    -- if not self:CanPrimaryAttack() then return end
    if !self.Tr or self:Clip1() < 1 then return end
    self.Owner:LagCompensation( true )


    if (!endpos) then endpos = self.Tr.HitPos end

    if (SERVER && self.Beam) then
    self.Beam:GetTable():SetEndPos( endpos )
    end

    lastpos = endpos



    if ( self.Tr.Entity:IsValid() ) then

        endpos = self.Tr.Entity:GetPos()
        if ( SERVER and IsValid(self.Beam) ) then
            self.Beam:GetTable():SetEndPos( endpos )
        end

    end

    local vVel = (endpos - self.Owner:GetPos())
    local Distance = endpos:Distance(self.Owner:GetPos())

    local et = (self.startTime + (Distance/self.speed))
    if(self.dtfix != 0) then
    self.dtfix = (et - CurTime()) / (et - self.startTime)
    end
    if(self.dtfix < 0) then
        self.Weapon:EmitSound( sndPowerUp )
        self.dtfix = 0
    end

    if(self.dtfix == 0) then
        zVel = self.Owner:GetVelocity().z
        vVel = vVel:GetNormalized()*(math.Clamp(Distance,0,25))
        if( SERVER ) then
            if  self.NextAmmoTake < CurTime() then
                self:TakePrimaryAmmo(1)
                self.NextAmmoTake = CurTime() + 1
                self.NextAmmoGive = CurTime() + 1.2
            end

            local gravity = GetConVarNumber("sv_gravity")

            vVel:Add(Vector(0,0,(gravity/50)*1.5))

            if(zVel < 0) then
                vVel:Sub(Vector(0,0,zVel/100))
            end
            self.Owner:SetVelocity(vVel)
        end
    end

    endpos = nil

    self.Owner:LagCompensation( false )

end

function SWEP:EndAttack( shutdownsound )

    if ( shutdownsound ) then
        self.Weapon:EmitSound( sndPowerDown )
    end

    if ( CLIENT ) then return end
    if ( !self.Beam ) then return end
    if( IsValid( self.Beam) ) then
        self.Beam:Remove()
    end
    self.Beam = nil

end