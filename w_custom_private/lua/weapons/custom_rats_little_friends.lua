

if (SERVER) then

    AddCSLuaFile(  )
    SWEP.Weight                = 10
    SWEP.AutoSwitchTo        = true
    SWEP.AutoSwitchFrom        = true

end

if ( CLIENT ) then

    SWEP.DrawAmmo            = true
    SWEP.DrawCrosshair        = false
    SWEP.ViewModelFOV        = 65
    SWEP.ViewModelFlip        = false
    SWEP.CSMuzzleFlashes    = false
    SWEP.WepSelectIcon   = surface.GetTextureID("VGUI/entities/select")
    SWEP.BounceWeaponIcon = false

end


SWEP.Base = "weapon_tttbase"
SWEP.Kind = 17

SWEP.Slot = 16
SWEP.PrintName = "Rats Little Friends"
SWEP.Author = "Heavy-D"
SWEP.Spawnable = true
SWEP.Category = "Heavy-D's SWEPs"
SWEP.SlotPos = 1
SWEP.Instructions = "Shoot it"
SWEP.Contact = "Don't"
SWEP.Purpose = "Hurt people"
SWEP.HoldType = "physgun"

SWEP.ShowViewModel = true
SWEP.ShowWorldModel = true

SWEP.ViewModelFOV			= 80
SWEP.ViewModelFlip			= false
SWEP.ViewModel				= "models/weapons/v_deat_m249para.mdl"
SWEP.WorldModel				= "models/weapons/w_blopsminigun.mdl"

SWEP.MuzzleAttachment			= "1" 	-- Should be "1" for CSS models or "muzzle" for hl2 models
SWEP.ShellEjectAttachment		= "2" 	-- Should be "2" for CSS models or "1" for hl2 models

        //Primary Fire Variables\\
SWEP.Primary.Sound = "weapons/ar2/fire1.wav"
SWEP.Primary.Damage = 14
SWEP.Primary.TakeAmmo = 1
SWEP.Primary.ClipSize = 400
SWEP.Primary.Ammo = "SMG1"
SWEP.Primary.DefaultClip = 3000
SWEP.Primary.Spread = 0.2
SWEP.Primary.NumberofShots = 1
SWEP.Primary.Automatic = true
SWEP.Primary.Recoil = 0.08
SWEP.Primary.Delay = 0.175
SWEP.Primary.Force = 300
SWEP.BarrelAngle = 0
SWEP.Crosshair = 0.07
        //Primary Fire Variables\\

//Secondary Fire Variables\\
SWEP.Secondary.NumberofShots = 1
SWEP.Secondary.Force = 10
SWEP.Secondary.Spread = 0.1
SWEP.Secondary.DefaultClip = 32
SWEP.Secondary.Recoil = 1
SWEP.Secondary.Automatic = true
SWEP.Secondary.Delay = 0.5
SWEP.Secondary.ClipSize = 0
SWEP.Secondary.Damage = 25
        //Secondary Fire Variables\\

SWEP.Primary.Sound			= Sound("weapons/deathmachine/death-1.wav")
SWEP.Primary.Sound2			= Sound("weapons/deathmachine/death_coverup.wav")

SWEP.IronSightsPos = Vector(0, 0, 0)
SWEP.IronSightsAng = Vector(0, 0, 0)
SWEP.SightsPos = Vector(0, 0, 0)
SWEP.SightsAng = Vector(0, 0, 0)
SWEP.RunSightsPos = Vector(0, 1.496, 4.645)
SWEP.RunSightsAng = Vector(-23.425, 0, 0)
SWEP.Offset = {
    Pos = {
        Up = 2,
        Right = 32,
        Forward = 0.0,
    },
    Ang = {
        Up = -10,
        Right = 0,
        Forward = 270,
    }
}

function SWEP:Initialize()

    util.PrecacheSound(self.Primary.Sound)
    self.Reloadaftershoot = 0 				-- Can't reload when firing
    self:SetHoldType("shotgun")
end

function SWEP:DrawWorldModel( )
    local hand, offset, rotate

    if not IsValid( self.Owner ) then
        self:DrawModel( )
        return
    end

    if not self.Hand then
        self.Hand = self.Owner:LookupAttachment( "anim_attachment_rh" )
    end

    hand = self.Owner:GetAttachment( self.Hand )

    if not hand then
        self:DrawModel( )
        return
    end

    offset = hand.Ang:Right( ) * self.Offset.Pos.Right + hand.Ang:Forward( ) * self.Offset.Pos.Forward + hand.Ang:Up( ) * self.Offset.Pos.Up

    hand.Ang:RotateAroundAxis( hand.Ang:Right( ), self.Offset.Ang.Right )
    hand.Ang:RotateAroundAxis( hand.Ang:Forward( ), self.Offset.Ang.Forward )
    hand.Ang:RotateAroundAxis( hand.Ang:Up( ), self.Offset.Ang.Up )

    self:SetRenderOrigin( hand.Pos + offset )
    self:SetRenderAngles( hand.Ang )

    self:DrawModel( )
end

function SWEP:Deploy()

    if game.SinglePlayer() then self.Single=true
    else
        self.Single=false
    end
    self:SetHoldType("shotgun")                          	// Hold type styles; ar2 pistol shotgun rpg normal melee grenade smg slam fist melee2 passive knife
    self:SetIronsights(false, self.Owner)					// Set the ironsight false
    self.Weapon:SendWeaponAnim( ACT_VM_DRAW )
    if !self.Owner:IsNPC() then self.ResetSights = CurTime() + self.Owner:GetViewModel():SequenceDuration() end
    return true
end

function SWEP:Think()

    if self.Owner:KeyReleased(IN_ATTACK) then
        self.Weapon:EmitSound( self.Primary.Sound2, self.Primary.SoundLevel )
    end


    if (self.Owner:WaterLevel() > 2) then
        self.Weapon:SetNextPrimaryFire(CurTime() + 0.5)
        return false
    end

end

function SWEP:Reload()

    self.Weapon:DefaultReload(ACT_VM_RELOAD)
    if !self.Owner:IsNPC() then
    self.ResetSights = CurTime() + self.Owner:GetViewModel():SequenceDuration() end

end
function SWEP:OnDrop()
    self:Remove()
end

SWEP.CanDecapitate= true
SWEP.HasFireBullet = true
-- NormalPrimaryAttack
function SWEP:PrimaryAttack(worldsnd)

    self.Weapon:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )

    if not self:CanPrimaryAttack() then return end

    if not worldsnd then
        self.Weapon:EmitSound( self.Primary.Sound, self.Primary.SoundLevel )

    elseif SERVER then
        sound.Play(self.Primary.Sound, self:GetPos(), self.Primary.SoundLevel)
    end

    self:ShootBullet( self.Primary.Damage, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )
    self:throw_attack ("models/props_junk/harpoon002a.mdl","", 5);

    self:TakePrimaryAmmo( 1 )


end

SWEP.NextBomb = 0

-- NormalPrimaryAttack
function SWEP:SecondaryAttack(worldsnd)

    self.Weapon:SetNextSecondaryFire( CurTime() + 0.35 )
    self.Weapon:SetNextPrimaryFire( CurTime() + 0.8 )

    if not self:CanPrimaryAttack() then return end

    if not worldsnd then
        self.Weapon:EmitSound( self.Primary.Sound, self.Primary.SoundLevel )

    elseif SERVER then
        sound.Play(self.Primary.Sound, self:GetPos(), self.Primary.SoundLevel)
    end

    self:ShootBullet( 3, self.Primary.Recoil, 13, 0.085 )
    if CLIENT then return end
    if self:Clip1() > 0 and  self.NextBomb < CurTime() then

        self.NextBomb = 0
        local ent = ents.Create("thrown_penguin")
        ent:SetPos(self.Owner:GetShootPos())
        ent:SetAngles(Angle(math.random(1, 360), math.random(1, 360), math.random(1, 360)))
        ent:SetOwner(self.Weapon:GetOwner())
        ent:Spawn()
        ent:Activate()

        ent:GetPhysicsObject():ApplyForceCenter(self.Owner:GetAimVector() * 12800)

        self.Weapon:SendWeaponAnim(ACT_VM_THROW)
        self.Owner:SetAnimation( PLAYER_ATTACK1 )
        timer.Simple(2.8, function()
               if not IsValid(ent) then return end
            local effectdata = EffectData()
            effectdata:SetOrigin(ent:GetPos())

            local effect = EffectData()
            effect:SetStart(ent:GetPos())
            effect:SetOrigin(ent:GetPos())
            effect:SetScale(50)
            effect:SetRadius(50)
            effect:SetMagnitude(70)

            sound.Play( "ambient/explosions/exp1.wav", Vector( ent:GetPos() ) )

            util.Effect("Explosion", effect, true, true)
               util.BlastDamage(self, self.Owner, pos, 100, 60)
               ent:Remove()
        end)


    end
    return

    self:TakePrimaryAmmo( 1 )


end

SWEP.NextAnim = 0
function SWEP:ShootBullet( dmg, recoil, numbul, cone )


    self.Owner:SetAnimation( PLAYER_ATTACK1 )
    self.Owner:MuzzleFlash()
    if self.NextAnim < CurTime() then

        self.Weapon:SendWeaponAnim(self.PrimaryAnim)
        self.NextAnim = CurTime() + 0.1
    end

    if not IsFirstTimePredicted() then return end

    local sights = self:GetIronsights()

    numbul = numbul or 1
    cone   = cone   or 0.01

    local bullet = {}
    bullet.Num    = numbul
    bullet.Src    = self.Owner:GetShootPos()
    bullet.Dir    = self.Owner:GetAimVector()
    bullet.Spread = Vector( cone, cone, 0 )
    bullet.Tracer = 100
    bullet.TracerName = self.Tracer or "Tracer"
    bullet.Force  = 10
    bullet.Damage = dmg


    self.Owner:FireBullets( bullet )

    -- Owner can die after firebullets
    if (not IsValid(self.Owner)) or (not self.Owner:Alive()) or self.Owner:IsNPC() then return end

    if ((game.SinglePlayer() and SERVER) or
            ((not game.SinglePlayer()) and CLIENT and IsFirstTimePredicted())) then

        -- reduce recoil if ironsighting
        recoil = sights and (recoil * 0.6) or recoil

        local eyeang = self.Owner:EyeAngles()
        eyeang.pitch = eyeang.pitch - recoil
        self.Owner:SetEyeAngles( eyeang )
    end

end

function SWEP:throw_attack (model_file, sound, away)
    local tr = self.Owner:GetEyeTrace();

    if IsValid( tr.Entity )  then
        if not tr.Entity.hasProtectionSuit then
            tr.Entity.hasProtectionSuit= true
            timer.Simple(5, function()
                if not IsValid( tr.Entity )  then return end
                tr.Entity.hasProtectionSuit = false
            end)
        end
    end

    self.BaseClass.ShootEffects (self);
    if (!SERVER) then return end;
    local ent = ents.Create ("prop_physics");
    ent:SetModel (model_file);
    ent:SetPos (self.Owner:EyePos() + (self.Owner:GetAimVector() * 16));
    ent:SetAngles (self.Owner:EyeAngles());
    ent:SetPhysicsAttacker(self.Owner)
    ent:Spawn();
    ent:EmitSound( sound )
    if math.Rand(1,12) < 2 then
        ent:Ignite(100,100)
    end
    local phys = ent:GetPhysicsObject();
    local shot_length = tr.HitPos:Length();
    phys:ApplyForceCenter (self.Owner:GetAimVector():GetNormalized() * math.pow (shot_length, 3) * 2);

    timer.Simple(3,function()
        if IsValid(ent) then
            ent:Remove()
        end
    end)

    cleanup.Add (self.Owner, "props", ent);
    undo.Create ("Thrown model");
    undo.AddEntity (ent);
    undo.SetPlayer (self.Owner);
    undo.Finish();
end