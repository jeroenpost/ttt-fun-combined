if SERVER then
    AddCSLuaFile(  )
end
if CLIENT then

    SWEP.Author 		= "GreenBlack"

    SWEP.SlotPos 	= 1
    SWEP.Icon = "vgui/ttt_fun_killicons/golden_deagle.png"
end
SWEP.CanDecapitate= true
SWEP.PrintName	= "The Slaty"
SWEP.Slot		= 7
SWEP.HoldType 		= "pistol"
SWEP.Base			= "aa_base"
SWEP.Spawnable 		= true
SWEP.AdminSpawnable = true
SWEP.Kind 			= WEAPON_EQUIP
SWEP.Primary.Ammo   = "AlyxGun"
SWEP.Primary.Recoil	= 1
SWEP.Primary.Damage = 25
SWEP.Primary.Delay 	= 0.20
SWEP.Primary.Cone 	= 0.009
SWEP.Primary.ClipSize 		= 100
SWEP.Primary.ClipMax 		= 100
SWEP.Primary.DefaultClip 	= 500
SWEP.Primary.NumShots		= 1
SWEP.Primary.Automatic 	= true
SWEP.HeadshotMultiplier = 1.6
SWEP.AutoSpawnable      = false
SWEP.AmmoEnt 		= "item_ammo_revolver_ttt"
--SWEP.Primary.Sound	= "gdeagle/gdeagle-1.mp3"
SWEP.Primary.Sound	= Sound(  "weapons/usp/usp1.wav" )
SWEP.Primary.SoundLevel = 400
SWEP.ViewModel= "models/weapons/v_pist_gbagle.mdl"
SWEP.WorldModel= "models/weapons/w_pist_gbagle.mdl"
SWEP.ShowWorldModel = true
SWEP.ShowViewModel = true

SWEP.LimitedStock = true
SWEP.InLoadoutFor = nil
SWEP.AllowDrop 	  = true
SWEP.IronSightsPos = Vector(-5, -20, -20)
SWEP.IronSightsAng = Vector(0, 0, 0)

--SWEP.ViewModelFOV = 49


function SWEP:Deploy()
    self:SetNWString("shootmode","Automatic")

    ply = self.Owner

    local function playerDiesSound( victim, weapon, killer )
        if( IsValid(killer) and killer:IsPlayer()  ) then
            local wep = killer:GetActiveWeapon()
            if IsValid(wep) then
                wep = wep:GetClass()
                if wep == "custom_slaty" then
                    if killer:Health() < 150 then
                        killer:SetHealth(killer:Health()+25)
                        if killer:Health() > 150 then
                            killer:SetHealth(150)
                        end
                    end
                    local material = killer:GetMaterial()
                    killer:SetMaterial("Models/effects/vol_light001")
                    killer.isdisguised =true

                    timer.Simple(3,function()
                        if IsValid(killer) then
                            killer.isdisguised = false
                            killer:SetMaterial(material)
                        end
                    end)
                end
            end

        end
    end
    hook.Add( "PlayerDeath", "playerDeathSound_slaty", playerDiesSound )

end



function SWEP:OnDrop()
    self:Remove()
end
SWEP.tracerColor = Color(255,0,0,255)
SWEP.LaserColor = Color(110,88,20,255)

function SWEP:PrimaryAttack()

    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
    local mode = self:GetNWString("shootmode")
    if mode == "Automatic" then
        self:SetNextPrimaryFire( CurTime() + 0.2 )
        self.Weapon:EmitSound( self.Primary.Sound, 80 )
        self.Primary.Damage = 25
        self.Primary.Recoil = 0.1
        self.Primary.NumShots = 1
        self.Primary.Cone = 0.001
        self:ShootBullet( self.Primary.Damage, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )
        self:TakePrimaryAmmo( 1 )
        self:ShootTracer("LaserTracer_thick")
    end
    if mode == "Semi-Auto" then
        self:SetNextPrimaryFire( CurTime() + 0.15 )
        self.Weapon:EmitSound( self.Primary.Sound, 80 )
        self.Primary.Damage = 25
        self.Primary.Recoil = 0.1
        self.Primary.NumShots = 1
        self.Primary.Cone = 0.005
        self:ShootBullet( self.Primary.Damage, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )
        self:TakePrimaryAmmo( 1 )
        self:ShootTracer("LaserTracer_thick")
    end
end


SWEP.NextReload = 0
function SWEP:Reload()
    if self.Owner:KeyDown(IN_ATTACK) then
        return
    end
    if self.Owner:KeyDown(IN_USE) and self.NextReload < CurTime() then
        self.NextReload = CurTime() + 0.3
        local mode = self:GetNWString("shootmode")
        self:SetIronsights(false)

        if mode == "Automatic" then  self:SetNWString("shootmode","Semi-Auto") self.Primary.Automatic 	= false end
        if mode == "Semi-Auto" then  self:SetNWString("shootmode","Automatic") self.Primary.Automatic 	= true end

        self.Weapon:SetNextPrimaryFire( CurTime() + 0.8 )
        return
    end


end



function SWEP:SecondaryAttack()
    self:Fly()
end

if CLIENT then
    local scope = surface.GetTextureID("sprites/scope")
    function SWEP:DrawHUD( )
        local shotttext = "Shootmode: "..self:GetNWString("shootmode").."\nE+R to change"
        surface.SetFont( "ChatFont" );
        local size_x, size_y = surface.GetTextSize( shotttext );

        draw.RoundedBox( 6, ScrW( ) / 2 - size_x / 2 - 5, ScrH( ) - 100, size_x + 15, size_y + 10, Color( 0, 0, 0, 150 ) );
        draw.DrawText( shotttext , "ChatFont", ScrW( ) / 2, ScrH( ) - 100 + 5, Color( 255, 30, 30, 255 ), TEXT_ALIGN_CENTER );
        self.BaseClass.DrawHUD(self)
    end
end
