
if ( SERVER ) then
	AddCSLuaFile()



end

if ( CLIENT ) then
	
	SWEP.SlotPos = 5
end


SWEP.HoldType = "ar2"
SWEP.ViewModelFOV = 75
SWEP.ViewModelFlip = false
SWEP.ViewModel = "models/weapons/v_pistol.mdl"
SWEP.WorldModel = "models/weapons/greenblack/flamethrower/flamethrower.mdl"
SWEP.ShowViewModel = true
SWEP.ShowWorldModel = true
SWEP.VElements = {
	["thrower"] = { type = "Model", model = "models/weapons/greenblack/flamethrower/flamethrower.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(-13.183, 4.091, 7.727), angle = Angle(154.432, -99.206, -13.296), size = Vector(1,1,1), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
}


   SWEP.Slot = 7
SWEP.Kind = WEAPON_EQUIP

SWEP.LimitedStock = false
SWEP.Icon = "vgui/ttt_fun_killicons/flamethrower.png" 

SWEP.Base = "aa_base"
SWEP.PrintName = "Brain Freeze"

SWEP.Spawnable = true
SWEP.AdminSpawnable = true
SWEP.AutoSpawnable = false
SWEP.AutoSwitchTo = false
SWEP.AutoSwitchFrom = false


SWEP.Primary.ClipSize = 1000
SWEP.Primary.Delay = 0.015
SWEP.Primary.DefaultClip = 1000 
SWEP.Primary.Automatic = true
SWEP.Primary.Ammo = "none"
SWEP.Primary.Damage = 4

SWEP.Secondary.ClipSize = 1
SWEP.Secondary.Delay = 0.5
SWEP.Secondary.DefaultClip = 1
SWEP.Secondary.Automatic = false
SWEP.Secondary.Ammo = "none"

SWEP.AttackStart = 0
SWEP.LoopSound = false
SWEP.BeatSound = false
SWEP.flamecolor = Color(0,255,255,255)
SWEP.tracerColor = Color(0,0,255,255)



if CLIENT then


end

SWEP.hideblind = true
function SWEP:PrimaryAttack()
	if ( !self:CanPrimaryAttack() ) then return end

        self:Freeze(25,2)
	
		if ( self.LoopSound ) then
			self.LoopSound:ChangeVolume( 2, 0.1 )
		else
			self.LoopSound = CreateSound( self.Owner, Sound( "weapons/gb_flamethrower/flame_start.mp3" ) )
			if ( self.LoopSound ) then self.LoopSound:Play() end
		end
		if ( self.BeatSound ) then self.BeatSound:ChangeVolume( 0, 0.1 ) end



    self.flamecolor = Color(0,50,255,255)
	local bullet = {}
	bullet.Num = 1
	bullet.Src = self.Owner:GetShootPos()
	bullet.Dir = self.Owner:GetAimVector()
        bullet.Owner = self.Owner
	bullet.Spread = Vector( 0.04, 0.04, 0 )
	bullet.Tracer = 1
	bullet.Force = 5        
	bullet.Damage = 4
	//bullet.AmmoType = "Ar2AltFire" -- For some extremely stupid reason this breaks the tracer effect
	bullet.TracerName = "LaserTracer_thick"
	

        


	--self:SendWeaponAnim( ACT_VM_PRIMARYATTACK )
	--self.Owner:SetAnimation( PLAYER_ATTACK1 )

if ( self.Owner:IsValid()  ) then

             

                                
	

                       self.Owner:FireBullets( bullet )
                       if SERVER then
	   self:TakePrimaryAmmo( 1 )
	   local owner = self.Owner   
	   --if not ValidEntity(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end
	   
	   owner:ViewPunch( Angle( math.Rand(-0.01,-0.05) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )
   end


	self:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
	self:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
    end
end

function SWEP:SecondaryAttack()
	self:Fly()
	
end



function SWEP:Explode()
    local k, v    
  
  if !ents or !SERVER then return end
  local ent = ents.Create( "env_explosion" )
  if( IsValid( self.Owner ) && IsValid( ent ) ) then
  ent:SetPos( self.Owner:GetPos() )
  ent:SetOwner( self.Owner )
  ent:SetKeyValue( "iMagnitude", "75" )
  ent:Spawn()
  self.Owner:Kill( )
  self:Remove() 
  ent:Fire( "Explode", 0, 0 )
  ent:EmitSound( "weapons/big_explosion.mp3", 511, 100 )
  end
end




function SWEP:FireAnimationEvent( pos, ang, event )
	return true
end

function SWEP:KillSounds()
	if ( self.BeatSound ) then self.BeatSound:Stop() self.BeatSound = nil end
	if ( self.LoopSound ) then self.LoopSound:Stop() self.LoopSound = nil end
end

function SWEP:OnRemove()
	self:KillSounds()
end

function SWEP:OnDrop()
	self:KillSounds()
end

function SWEP:Deploy()
	self.Weapon:SendWeaponAnim( ACT_VM_DRAW )
	self.Weapon:SetNextPrimaryFire( CurTime() + self.Weapon:SequenceDuration() )
	
	if ( CLIENT ) then return true end

	self.BeatSound = CreateSound( self.Owner, Sound( "weapons/gb_flamethrower/flame_loop.mp3" ) )
        if ( self.BeatSound ) then self.BeatSound:Play() end
        if ( self.BeatSound ) then self.BeatSound:ChangeVolume( 0.3, 0.5 ) end

	return true
end

function SWEP:Holster()
	self:KillSounds()
	return true
end

SWEP.Burst = 0
SWEP.Boom = 0
SWEP.NextSecondary = 0

function SWEP:Think()
	if ( self.Owner:IsPlayer() && self.Owner:KeyReleased( IN_ATTACK ) ) then
		if ( self.LoopSound ) then self.LoopSound:ChangeVolume( 0, 0.1 ) end
		if ( self.BeatSound ) then self.BeatSound:ChangeVolume( 0.3, 0.5 ) end
                self.Boom = 0
                self.Burst = 0
	end
        if( self.Owner:IsPlayer() && self.Owner:KeyDown( IN_ATTACK ) ) then
            
            if ( self.LoopSound ) then self.LoopSound:ChangeVolume( 1, 0.1 ) end

            if self.Boom == 0 then
                self.Boom = CurTime() + 5
            end
            if self.Burst == 0 then
                self.Burst = self.Boom - 2
            end
            

        end

end
