SWEP.Base = "gb_camo_base"
SWEP.ViewModelFOV	= 54
SWEP.HoldType = "pistol"
SWEP.Icon = "VGUI/ttt/icon_glock"
SWEP.PrintName = "Hlocker"
SWEP.ViewModel		= "models/weapons/cstrike/c_pist_glock18.mdl"
SWEP.WorldModel		= "models/weapons/w_pist_glock18.mdl"
SWEP.Kind = 56
SWEP.Slot = 1
SWEP.AutoSpawnable = false
SWEP.Primary.Recoil	= 0.9
SWEP.Primary.Damage = 25
SWEP.Primary.Delay = 0.1
SWEP.Primary.Cone = 0.001
SWEP.Primary.ClipSize = 555
SWEP.Primary.Automatic = false
SWEP.Primary.DefaultClip = 555
SWEP.Primary.ClipMax = 555
SWEP.Primary.Ammo = "Pistol"
SWEP.AutoSpawnable = true
SWEP.AmmoEnt = "item_ammo_pistol_ttt"
SWEP.Camo = 7
SWEP.Primary.Sound = Sound( "physics/glass/glass_pottery_break1.wav" )
SWEP.IronSightsPos = Vector( -5.79, -3.9982, 2.8289 )
SWEP.UseHands = true
SWEP.HeadshotMultiplier = 1.75

function SWEP:SecondaryAttack()
    self:Fly()
end

function SWEP:Reload()
    self:DropHealth("normal_health_station","models/weapons/w_pist_glock18.mdl")
end

function SWEP:PrimaryAttack()
    if self:Clip1() > 0 then
        self:ShootTracer("AR2Tracer")
    end
    self:NormalPrimaryAttack()
end