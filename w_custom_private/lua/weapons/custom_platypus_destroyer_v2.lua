SWEP.Base = "gb_camo_base"
SWEP.ViewModelFOV	= 54
SWEP.HoldType = "ar2"
SWEP.PrintName = "Platypus Destroyer V2"
SWEP.ViewModel		= "models/weapons/cstrike/c_snip_sg550.mdl"
SWEP.WorldModel		= "models/weapons/w_snip_sg550.mdl"
SWEP.AutoSpawnable = false

SWEP.HoldType = "ar2"
SWEP.Slot = 3

if CLIENT then

    SWEP.Author = "GreenBlack"

    SWEP.Icon = "vgui/ttt_fun_killicons/sg550.png"
end

SWEP.Kind = WEAPON_NADE

SWEP.Spawnable	= false
SWEP.AdminSpawnable = true

SWEP.Primary.Sound = "CSTM_SilencedShot5"
SWEP.Secondary.Sound = Sound("Default.Zoom")
SWEP.Primary.Automatic = true
SWEP.Primary.ClipSize = 60
SWEP.Primary.ClipMax = 90
SWEP.Primary.DefaultClip = 60
SWEP.Primary.Recoil = 0.5
SWEP.Primary.Damage = 19
SWEP.Primary.Delay = 0.13
SWEP.AmmoEnt = "item_ammo_357_ttt"
SWEP.Primary.Ammo = "357"
SWEP.AutoSpawnable      = false

SWEP.HeadshotMultiplier = 1.2

SWEP.IronSightsPos = Vector( 5, -15, -2 )
SWEP.IronSightsAng = Vector( -4, 1.37, 1.5 )

SWEP.CanDecapitate= true



function SWEP:SetZoom(state)
    if CLIENT then
        return
    else
        if state then
            self.Owner:SetFOV(20, 0.3)
        else
            self.Owner:SetFOV(0, 0.2)
        end
    end
end
SWEP.Camo = 9
function SWEP:Deploy()
    timer.Simple(2,function()
        if IsValid(self.Owner) then
            local ply = self.Owner
            ply:GiveEquipmentItem( EQUIP_PROTECTIONSUIT )
            ply.hasProtectionSuit = true
        end
    end)
    self.BaseClass.Deploy(self)
end

function SWEP:OnDrop()
    self:Remove()
end

function SWEP:SecondaryAttack()
    if not self.IronSightsPos then return end
    if self.Weapon:GetNextSecondaryFire() > CurTime() then return end

    bIronsights = not self:GetIronsights()

    self:SetIronsights( bIronsights )

    if SERVER then
        self:SetZoom(bIronsights)
    else
        self:EmitSound(self.Secondary.Sound)
    end

    self.Weapon:SetNextSecondaryFire( CurTime() + 0.3)
end

function SWEP:PreDrop()
    self:SetZoom(false)
    self:SetIronsights(false)
    return self.BaseClass.PreDrop(self)
end

function SWEP:Reload()
    if self.Owner:KeyDown(IN_USE) then
        self:Fly()
        return
    end
    self.Weapon:DefaultReload( ACT_VM_RELOAD );
    self:SetIronsights( false )
    self:SetZoom(false)
end


function SWEP:Holster()
    self:SetIronsights(false)
    self:SetZoom(false)
    return true
end

if CLIENT then
    local scope = surface.GetTextureID("sprites/scope")
    function SWEP:DrawHUD()
        if self:GetIronsights() then
            surface.SetDrawColor( 0, 0, 0, 255 )

            local x = ScrW() / 2.0
            local y = ScrH() / 2.0
            local scope_size = ScrH()

            local gap = 80
            local length = scope_size
            surface.DrawLine( x - length, y, x - gap, y )
            surface.DrawLine( x + length, y, x + gap, y )
            surface.DrawLine( x, y - length, x, y - gap )
            surface.DrawLine( x, y + length, x, y + gap )

            gap = 0
            length = 50
            surface.DrawLine( x - length, y, x - gap, y )
            surface.DrawLine( x + length, y, x + gap, y )
            surface.DrawLine( x, y - length, x, y - gap )
            surface.DrawLine( x, y + length, x, y + gap )

            local sh = scope_size / 2
            local w = (x - sh) + 2
            surface.DrawRect(0, 0, w, scope_size)
            surface.DrawRect(x + sh - 2, 0, w, scope_size)

            surface.SetDrawColor(255, 0, 0, 255)
            surface.DrawLine(x, y, x + 1, y + 1)

            surface.SetTexture(scope)
            surface.SetDrawColor(255, 255, 255, 255)

            surface.DrawTexturedRectRotated(x, y, scope_size, scope_size, 0)

        else
            return self.BaseClass.DrawHUD(self)
        end
    end

    function SWEP:AdjustMouseSensitivity()
        return (self:GetIronsights() and 0.2) or nil
    end
end