SWEP.Base = "gb_camo_base"
SWEP.ViewModelFOV	= 54
SWEP.HoldType = "pistol"
SWEP.Icon = "vgui/ttt_fun_killicons/fiveseven.png"
SWEP.PrintName = "Honk's rigged fire arm"

-- SINGLE ELITES
SWEP.ViewModel		= "models/weapons/cstrike/c_pist_usp.mdl"
SWEP.WorldModel		= "models/weapons/w_pist_usp.mdl"

SWEP.ViewModel				= "models/weapons/v_rif_fury.mdl"
SWEP.WorldModel				= "models/weapons/w_frozen_acr.mdl"
SWEP.ViewModelFlip = true

SWEP.Kind = 101
SWEP.Slot = 8
SWEP.AutoSpawnable = false
SWEP.Primary.Recoil	= 0.01
SWEP.Primary.Damage = 5
SWEP.Primary.Delay = 0.13
SWEP.Primary.Cone = 0.02
SWEP.Primary.ClipSize = 100
SWEP.Primary.Automatic = true
SWEP.Primary.DefaultClip = 300
SWEP.Primary.ClipMax = 300
SWEP.Primary.Ammo = "Pistol"
SWEP.AutoSpawnable = false
SWEP.AmmoEnt = "item_ammo_pistol_ttt"
SWEP.Camo = -1
SWEP.HasFireBullet = true

SWEP.Primary.Sound = Sound( "CSTM_SilencedShot7" )
SWEP.IronSightsPos = Vector(-5.95, -4, 2.799)
SWEP.IronSightsAng = Vector(0, 0, 0)

function SWEP:SecondaryAttack()
    self:Fly()
end

function SWEP:OnDrop()
    self:Remove()
end

SWEP.tracerColor = Color(50,50,255,255)
function SWEP:PrimaryAttack()
    if self:Clip1() > 0 then
        self:MakeAFlame()
        self:ShootTracer("manatrace")
        self:Blind()
        self:ShootBullet2(2,10, 0.85)
    end
    self.BaseClass.PrimaryAttack(self)
end

function SWEP:ShootBullet2( damage, num_bullets, aimcone )
    local bullet = {}
    bullet.Num 		= 8
    bullet.Src 		= self.Owner:GetShootPos()
    bullet.Dir 		= self.Owner:GetAimVector()
    bullet.Spread 	= Vector( 0.09, 0.09, 0.09 )
    bullet.Tracer	= 5000
    bullet.Force	= 5000
    bullet.Damage	= damage
    bullet.AmmoType = "AR2AltFire"
    self.Owner:FireBullets( bullet )
    self:ShootEffects()

end