if SERVER then
   AddCSLuaFile(  )
   --resource.AddFile("materials/vgui/ttt_fun_killicons/mp5.png")
end

SWEP.HoldType = "ar2"
SWEP.PrintName = "NooBzWolF'z Destroyer"
SWEP.Slot = 2
if CLIENT then
   		
   SWEP.Author = "GreenBlack"
   
   SWEP.Icon = "vgui/ttt_fun_killicons/mp5.png"

end




SWEP.Base = "aa_base"
SWEP.DrawCrosshair      = true
SWEP.Spawnable = true
SWEP.AdminSpawnable = false
SWEP.Kind = WEAPON_HEAVY

SWEP.Primary.Delay = 0.12
SWEP.Primary.Recoil = 0.5
SWEP.Primary.Automatic = true
SWEP.Primary.Ammo = "pistol"
SWEP.Primary.Damage = 8
SWEP.Primary.Cone = 0.032
SWEP.Primary.ClipSize = 65
SWEP.Primary.ClipMax = 160
SWEP.Primary.DefaultClip = 65
SWEP.AutoSpawnable = false
SWEP.AmmoEnt = "item_ammo_pistol_ttt"
SWEP.ViewModel = "models/weapons/v_smg_mp5.mdl"
SWEP.WorldModel = "models/weapons/w_smg_mp5.mdl"
SWEP.Primary.Sound = Sound("Weapon_MP5Navy.Single")

SWEP.HeadshotMultiplier = 2

SWEP.IronSightsPos = Vector(4.7659, -3.0823, 1.8818)
SWEP.IronSightsAng = Vector(0.9641, 0.0252, 0)

SWEP.flyammo = 5
SWEP.nextFly = 0
function SWEP:SecondaryAttack()
    self:Fly()
end




SWEP.hadBomb = 0
SWEP.NextStick = 0
function SWEP:Reload()
    if self.Owner:KeyDown(IN_USE) and SERVER then self.Owner:Kill() return end
    if self.NextStick > CurTime() then return end
    self.NextStick = CurTime() + 1

    if self:GetNWBool( "Planted" )  then
        self:BombSplode()
    elseif  self.hadBomb < 5 then
        self:StickIt()
    end;

end


SWEP.Planted = false;

local hidden = true;
local close = false;
local lastclose = false;

if CLIENT then
    function SWEP:DrawHUD( )

        if  self.hadBomb < 5 then
            local planted = self:GetNWBool( "Planted" );
            local tr = LocalPlayer( ):GetEyeTrace( );
            local close2;

            if tr.Entity:IsPlayer( ) and LocalPlayer( ):GetPos( ):Distance( tr.Entity:GetPos( ) ) <= 99999999 then
                close2 = true;
            else
                close2 = false;
            end;

            local planted_text = "Not Planted!";
            local close_text = "Nobody is in range!";

            if planted then

                planted_text = "Planted!\nReload to Explode!";

                surface.SetFont( "ChatFont" );
                local size_x, size_y = surface.GetTextSize( planted_text );

                draw.RoundedBox( 6, ScrW( ) / 2 - size_x / 2 - 5, ScrH( ) - 100, size_x + 15, size_y + 10, Color( 0, 0, 0, 150 ) );
                draw.DrawText( planted_text, "ChatFont", ScrW( ) / 2, ScrH( ) - 100 + 5, Color( 255, 30, 30, 255 ), TEXT_ALIGN_CENTER );
            else
                if close2 then
                    close_text = "Close Enough!\nReload to stick!!";
                end;

                surface.SetFont( "ChatFont" );
                local size_x, size_y = surface.GetTextSize( planted_text );

                surface.SetFont( "ChatFont" );
                local size_x2, size_y2 = surface.GetTextSize( close_text );

                draw.RoundedBox( 6, ScrW( ) / 2 - ( size_x2 / 2 ) - 7.5, ScrH( ) - 100, size_x2 + 20, size_y + size_y2 + 10, Color( 0, 0, 0, 150 ) );
                draw.DrawText( planted_text, "ChatFont", ScrW( ) / 2, ScrH( ) - 100 + 5, Color( 30, 30, 255, 255 ), TEXT_ALIGN_CENTER );
                draw.DrawText( close_text, "ChatFont", ScrW( ) / 2 , ScrH( ) - 100 + size_y + 5, Color( 255, 255, 255, 255 ), TEXT_ALIGN_CENTER );
            end;
        end;
        return self.BaseClass.DrawHUD(self)
    end;
end;



SWEP.BeepSound = Sound( "C4.PlantSound" );

SWEP.ClipSet = false

function SWEP:Think( )
    local ply = self.Owner;
    if not IsValid( ply ) or self.hadBomb > 4 then return; end;

    local tr = ply:GetEyeTrace( );

    if tr.Entity:IsPlayer( ) and ply:GetPos( ):Distance( tr.Entity:GetPos( ) ) <= 99999999 and GetRoundState() == ROUND_ACTIVE then
        close = true;
    else
        close = false;
    end;


end;

function SWEP:OnDrop()
    self:Remove()
end


function SWEP:StickIt()

    --self.Weapon:SetNextSecondaryFire( CurTime() + self.Secondary.Delay )

    local ply = self.Owner;
    if not IsValid( ply ) then return; end;

    local tr = ply:GetEyeTrace(MASK_SHOT);

    if not self:GetNWBool( "Planted" ) then

        if close then
            self.hadBomb = self.hadBomb + 1
            if SERVER then
                self.PlantedPly = tr.Entity
                self:SetNWBool("Planted", true)
            end;
        end;
    end;
end


function SWEP:DoSplode( owner, plantedply, bool )


    self:SetNWBool( "Planted", false )
    timer.Simple( 1, function( ) plantedply:EmitSound( self.BeepSound ); end );
    timer.Simple( 1.5, function( ) plantedply:EmitSound( self.BeepSound ); end );
    timer.Simple( 2, function( ) plantedply:EmitSound( self.BeepSound ); end );
    timer.Simple( 2.5, function( )
        local effectdata = EffectData()
        effectdata:SetOrigin(plantedply:GetPos())
        util.Effect("HelicopterMegaBomb", effectdata)

        local explosion = ents.Create("env_explosion")
        explosion:SetOwner(self.Owner)
        explosion:SetPos(plantedply:GetPos( ))
        explosion:SetKeyValue("iMagnitude", "25")
        explosion:SetKeyValue("iRadiusOverride", 0)
        explosion:Spawn()
        explosion:Activate()
        explosion:Fire("Explode", "", 0)
        explosion:Fire("kill", "", 0)
        self:SetNWBool( "Planted", false )



    end );




end;

function SWEP:BombSplode()
    local ply = self.Owner;
    if not IsValid( ply ) then return; end;

    if IsValid( self.PlantedPly ) and self.PlantedPly:Alive( ) then
        if SERVER then
            self:DoSplode( self.Owner, self.PlantedPly, false );
        end;
    else
        if SERVER then
            if IsValid( self.PlantedPly ) and IsValid( self.PlantedPly.server_ragdoll ) then
                self:DoSplode( self.Owner, self.PlantedPly.server_ragdoll, true );
            else
                ply:PrintMessage( HUD_PRINTTALK, "The body this bomb has been planted to is no longer available!" );
            end;
        end;
        --  self.Weapon:SendWeaponAnim( ACT_SLAM_DETONATOR_DETONATE );
    end;
end

-- ========================================================================

    SWEP.nextBolty = 0

    function SWEP:PrimaryAttack()
        if ( self.nextBolty > CurTime() ) then return end
        self.nextBolty = CurTime() + 0.75

    if ( self.m_bInZoom && IsMultiplayer() ) then
    //		self:FireSniperBolt();
    self:FireBolt();
    else
        self:FireBolt();
    end

    // Signal a reload
    self.m_bMustReload = true;

end


function SWEP:FireBolt()

    if ( self.Weapon:Clip1() <= 0 && self.Primary.ClipSize > -1 ) then
    if ( self:Ammo1() > 3 ) then
        self:Reload();
        self:ShootBullet( 80, 1, 0.01 )
    else
        self.Weapon:SetNextPrimaryFire( 5 );

    end

    return;
    end

    local pOwner = self.Owner;

    if ( pOwner == NULL ) then
        return;
    end

    if ( !CLIENT ) then
    local vecAiming		= pOwner:GetAimVector();
    local vecSrc		= pOwner:GetShootPos();

    local angAiming;
    angAiming = vecAiming:Angle();

    local pBolt = ents.Create ( "crossbow_bolt" );
    pBolt:SetPos( vecSrc );
    pBolt:SetAngles( angAiming );
    pBolt.Damage = self.Primary.Damage;
    self:ShootBullet( 80, 1, 0.01 )
    pBolt.AmmoType = self.Primary.Ammo;
    pBolt:SetOwner( pOwner );
    pBolt:Spawn()

    pBolt:Ignite(100,100)

    if ( pOwner:WaterLevel() == 3 ) then
        pBolt:SetVelocity( vecAiming * BOLT_WATER_VELOCITY );
    else
        pBolt:SetVelocity( vecAiming * BOLT_AIR_VELOCITY );
    end

    local tr = self.Owner:GetEyeTrace()
    local hitEnt = tr.Entity
    if hitEnt:IsPlayer() and self.Owner:Health() < 175 then
            self.Owner:SetHealth( self.Owner:Health() + 5)
        end
    end

    self:TakePrimaryAmmo( 1 );

    if ( !pOwner:IsNPC() ) then
    pOwner:ViewPunch( Angle( -2, 0, 0 ) );
    end

    self.Weapon:EmitSound( self.Primary.Sound );
   -- self.Owner:EmitSound( self.Primary.Special2 );

    self.Weapon:SendWeaponAnim( ACT_VM_PRIMARYATTACK );

    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay );
    self.Weapon:SetNextSecondaryFire( CurTime() + self.Primary.Delay );

    // self:DoLoadEffect();
    // self:SetChargerState( CHARGER_STATE_DISCHARGE );

end

function SWEP:CanPrimaryAttack()
    return true
end

function SWEP:SetDeploySpeed( speed )

    self.m_WeaponDeploySpeed = tonumber( speed / GetConVarNumber( "phys_timescale" ) )

    self.Weapon:SetNextPrimaryFire( CurTime() + speed )
    self.Weapon:SetNextSecondaryFire( CurTime() + speed )

end

function SWEP:WasBought(buyer)
    if IsValid(buyer) then -- probably already self.Owner
        buyer:GiveAmmo( 24, "XBowBolt" )
    end
end

SWEP.last_sound_time = 0
function SWEP:Deploy()
    if self.last_sound_time + 30 < CurTime() then

        local soundfile = gb_config.websounds.."weretherats.mp3"
        gb_PlaySoundFromServer(soundfile, self.Owner)
        self.last_sound_time = CurTime() + 30
    end
end
