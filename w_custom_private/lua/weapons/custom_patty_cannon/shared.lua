SWEP.Base = "aa_base_fw"
SWEP.HoldType = "pistol"
SWEP.ViewModelFOV = 54
SWEP.ViewModelFlip = false
SWEP.ViewModel = "models/weapons/v_RPG.mdl" -- This is the model used for clients to see in first person.
SWEP.WorldModel = "models/weapons/w_rocket_launcher.mdl" -- This is the model shown to all other clients and in third-person.

SWEP.ShowViewModel = true
SWEP.ShowWorldModel = true
SWEP.HoldType                 = "rpg"
SWEP.UseHands = false
SWEP.Kind = WEAPON_ROLE


SWEP.Sound = { Sound("CSTM_M98B") }
SWEP.Sounds = {Sound("CSTM_M98B")}

SWEP.PrintName            = "The Patty Cannon"
SWEP.Slot                = 7
SWEP.SlotPos            = 1
SWEP.DrawAmmo            = true
SWEP.DrawCrosshair        = true
SWEP.Weight                = 5
SWEP.AutoSwitchTo        = false
SWEP.AutoSwitchFrom        = false
SWEP.Author            = ""
SWEP.Contact        = ""
SWEP.Purpose        = ""


SWEP.AutoSpawnable      = false
SWEP.Primary.Damage        = 20
SWEP.Primary.ClipSize        = 500
SWEP.Primary.DefaultClip    = 500
SWEP.Primary.Automatic        = true
SWEP.Primary.Ammo            = "XBowBolt"
SWEP.Primary.Delay            = 0.19
SWEP.Secondary.Delay            = 3
SWEP.Secondary.ClipSize        = 2
SWEP.Secondary.DefaultClip    = 2
SWEP.Secondary.Automatic    = true
SWEP.Secondary.Ammo            = "XBowBolt"
SWEP.AmmoEnt = ""
SWEP.HeadshotMultiplier = 1

SWEP.SecondaryProps = {
    "models/props_lab/cactus.mdl",
}
SWEP.Icon = "vgui/ttt_fun_killicons/nanners_gun.png"

function SWEP:PrimaryAttack()
    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
    -- self:TakePrimaryAmmo( 1 )
    self:ShootBullet( 25, 1, 1, 0.002 )
    self:Disorientate()
    timer.Simple(0.05,function()
        if IsValid(self) and self.Sound then
        self:throw_attack ( "models/food/burger.mdl", Sound(table.Random( self.Sound) ), 3);
            end
        end)

end

SWEP.Headcrabs = 0
SWEP.Headcrabs2 = 0
SWEP.NextSecond = 0

function SWEP:Reload()

    self:FireWorkie()
end

function SWEP:OnDrop()
    self:Remove()
end


local throwsound = Sound( "Weapon_SLAM.SatchelThrow" )

SWEP.healththing = false
-- ye olde droppe code
function SWEP:HealthDrop( healttt)
    if SERVER then
        local ply = self.Owner
        if not IsValid(ply) then return end

        --if self.Planted then return end

        local vsrc = ply:GetShootPos()
        local vang = ply:GetAimVector()
        local vvel = ply:GetVelocity()

        local vthrow = vvel + vang * 200

        local healths = ents.Create("normal_health_station")
            if IsValid(healths) then
                healths.Model ="models/food/burger.mdl"
                healths:SetPos(vsrc + vang * 10)
                healths:Spawn()
                healths:SetModel("models/food/burger.mdl")
                healths:SetPlacer(ply)

                healths:PhysWake()
                local phys = healths:GetPhysicsObject()
                if IsValid(phys) then
                    phys:SetVelocity(vthrow)
                end
                healths.Model ="models/food/burger.mdl"
                healths:SetModel("models/food/burger.mdl")
                healths:SetStoredHealth(healttt)

                self.healththing = healths:EntIndex()

                self.Planted = true
            end
        timer.Simple(2,function()
            if IsValid(healths) then
                healths.Model ="models/food/burger.mdl"
                healths:SetModel("models/food/burger.mdl")
            end
        end)
    end

    self.Weapon:EmitSound(throwsound)
end


function SWEP:SecondaryAttack()
    if( self.Headcrabs > 1 ) then
        self:Reload()
    return end
    if  self.NextSecond > CurTime() then return end
    if not self:CanPrimaryAttack() then return end
    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
    self.Weapon:SetNextSecondaryFire( CurTime() + 5 )

    self.NextSecond = CurTime() + 5

    local healttt = 200

    if self.healththing and SERVER then
        healttt = Entity(self.healththing):GetStoredHealth()
        Entity(self.healththing):Remove()
    end
    self:HealthDrop( healttt )


end

function SWEP:ReplaySound( ent )
    if not IsValid( ent ) then return end
    ent:EmitSound(  "weapons/nanners/13.mp3"  )
    timer.Simple(16,function()
        if IsValid( ent ) then
            self:ReplaySound( ent )
        end

    end)
end



function SWEP:throw_attack (model_file, sound, away)
    local tr = self.Owner:GetEyeTrace();

    if IsValid( tr.Entity )  then
            tr.Entity.hasProtectionSuit= true
    end


    self.BaseClass.ShootEffects (self);
    if (!SERVER) then return end;
    local ent = ents.Create ("prop_physics");
    ent:SetModel (model_file);
    ent:SetPos (self.Owner:EyePos() + (self.Owner:GetAimVector() * 16));
    ent:SetAngles (self.Owner:EyeAngles());
    ent:SetPhysicsAttacker(self.Owner)
    ent:Spawn();
    ent:EmitSound( sound )
    if math.random(1,10) < 2 then
        ent:Ignite(100,100)
    end
    local phys = ent:GetPhysicsObject();
    local shot_length = tr.HitPos:Length()+10;
    phys:SetMass( 1 )
    phys:ApplyForceCenter (self.Owner:GetAimVector():GetNormalized() * math.pow (shot_length, 3));

    timer.Simple(away,function()
        if IsValid(ent) then
            ent:Remove()
        end
    end)

    cleanup.Add (self.Owner, "props", ent);
    undo.Create ("Thrown model");
    undo.AddEntity (ent);
    undo.SetPlayer (self.Owner);
    undo.Finish();
end