SWEP.HoldType = "melee"
SWEP.ViewModelFOV = 45
SWEP.ViewModelFlip = false
SWEP.ViewModel = "models/weapons/v_stunstick.mdl"
SWEP.WorldModel = "models/weapons/W_stunbaton.mdl"
SWEP.ShowViewModel = true
SWEP.ShowWorldModel = true
SWEP.Kind = 1233
SWEP.ViewModelBoneMods = {
    ["ValveBiped.Bip01"] = { scale = Vector(1, 1, 1), pos = Vector(0, 1.899, 2.986), angle = Angle(0, -4.888, -4.888) }
}
SWEP.VElements = {
    ["prong+"] = { type = "Model", model = "models/props_c17/utilityconnecter006d.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "base3", pos = Vector(-0.385, 0, -5.77), angle = Angle(-82.212, 37.212, -37.213), size = Vector(0.129, 0.129, 0.129), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
    ["bottom"] = { type = "Model", model = "models/props_junk/wood_spool01.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "Handle", pos = Vector(0, 0, 5.679), angle = Angle(0, 0, 0), size = Vector(0.039, 0.039, 0.039), color = Color(255, 255, 255, 255), surpresslightning = false, material = "Debug/env_cubemap_model_translucent_fountain", skin = 0, bodygroup = {} },
    ["Handle"] = { type = "Model", model = "models/props_c17/oildrum001.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(3.099, 1.5, -1.894), angle = Angle(-2.131, 95.858, -4.261), size = Vector(0.059, 0.059, 0.119), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
    ["prong++++"] = { type = "Model", model = "models/props_c17/utilityconnecter006d.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "base3", pos = Vector(-0.385, 0.384, -3.462), angle = Angle(-82.212, 0, 0), size = Vector(0.129, 0.129, 0.129), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
    ["part"] = { type = "Model", model = "models/healthvial.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "Handle", pos = Vector(0, 0, -5), angle = Angle(0, 0, 0), size = Vector(0.5, 0.5, 0.5), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
    ["bright"] = { type = "Model", model = "models/props_lab/lab_flourescentlight002b.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "battery+", pos = Vector(0, 2.273, 0.2), angle = Angle(0, 0, 0), size = Vector(0.094, 0.094, 0.094), color = Color(0, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
    ["battery"] = { type = "Model", model = "models/Items/car_battery01.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "part", pos = Vector(0.384, -1.155, 3.461), angle = Angle(0.865, -9.52, -90.865), size = Vector(0.082, 0.082, 0.082), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
    ["prong+++++"] = { type = "Model", model = "models/props_c17/utilityconnecter006d.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "base3", pos = Vector(-0.385, 0, -3.462), angle = Angle(-82.212, 37.212, -37.213), size = Vector(0.129, 0.129, 0.129), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
    ["base2"] = { type = "Model", model = "models/props_lab/labpart.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "part", pos = Vector(0.2, 0.3, -1.155), angle = Angle(-0.866, -132.405, 90.864), size = Vector(0.225, 0.225, 0.225), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
    ["base3"] = { type = "Model", model = "models/props_junk/PopCan01a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "base2", pos = Vector(0, 8.076, 0.15), angle = Angle(16.441, 180, 90), size = Vector(0.801, 0.819, 2.098), color = Color(255, 255, 255, 255), surpresslightning = false, material = "Debug/env_cubemap_model", skin = 0, bodygroup = {} },
    ["prong+++"] = { type = "Model", model = "models/props_c17/utilityconnecter006d.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "base3", pos = Vector(-0.385, 0, -1.155), angle = Angle(-82.212, 37.212, -37.213), size = Vector(0.129, 0.129, 0.129), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
    ["prong"] = { type = "Model", model = "models/props_c17/utilityconnecter006d.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "base3", pos = Vector(-0.385, 0.384, -5.77), angle = Angle(-82.212, 0, 0), size = Vector(0.129, 0.129, 0.129), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
    ["battery+"] = { type = "Model", model = "models/Items/car_battery01.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "part", pos = Vector(0.384, -1.155, -1.923), angle = Angle(0.865, -9.52, -90.865), size = Vector(0.082, 0.082, 0.082), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
    ["prong++"] = { type = "Model", model = "models/props_c17/utilityconnecter006d.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "base3", pos = Vector(-0.385, 0.384, -1.155), angle = Angle(-82.212, 0, 0), size = Vector(0.129, 0.129, 0.129), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
    ,
    ["cutter"] = { type = "Model", model = "models/rubyscythe.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(2.596, 0, 0), angle = Angle(180, -180, 0), size = Vector(1.149, 1.149, 1.149), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }


    ,["twilysceptre_hl2size"] = { type = "Model", model = "models/freeman/twilysceptre_hl2size.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "Handle", pos = Vector(-0, 0.384, -1.155), angle = Angle(180, 0, 0), size = Vector(1,1,1), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }

}



SWEP.WElements = {
    ["prong+"] = { type = "Model", model = "models/props_c17/utilityconnecter006d.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "base3", pos = Vector(0.455, 0, -5), angle = Angle(-91.024, 0, 0), size = Vector(0.129, 0.129, 0.129), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
    ["bottom"] = { type = "Model", model = "models/props_junk/wood_spool01.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "Handle", pos = Vector(0, 0, 5.908), angle = Angle(0, 0, 0), size = Vector(0.039, 0.039, 0.039), color = Color(255, 255, 255, 255), surpresslightning = false, material = "Debug/env_cubemap_model_translucent_fountain", skin = 0, bodygroup = {} },
    ["Handle"] = { type = "Model", model = "models/props_c17/oildrum001.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(2.7, 1.6, -1.364), angle = Angle(13.295, 11.25, -9.205), size = Vector(0.059, 0.059, 0.119), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
    ["prong++++"] = { type = "Model", model = "models/props_c17/utilityconnecter006d.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "base3", pos = Vector(0.455, 0, -2.274), angle = Angle(-91.024, 0, 0), size = Vector(0.129, 0.129, 0.129), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
    ["bright"] = { type = "Model", model = "models/props_lab/lab_flourescentlight002b.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "battery+", pos = Vector(0, 2.273, 0.2), angle = Angle(95.113, 0, 0), size = Vector(0.094, 0.094, 0.094), color = Color(0, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
    ["battery+"] = { type = "Model", model = "models/Items/car_battery01.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "part", pos = Vector(0.384, -1.155, -1.923), angle = Angle(0.865, -9.52, -90.865), size = Vector(0.082, 0.082, 0.082), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
    ["battery"] = { type = "Model", model = "models/Items/car_battery01.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "part", pos = Vector(0.384, -1.155, 3.461), angle = Angle(0.865, -9.52, -90.865), size = Vector(0.082, 0.082, 0.082), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
    ["prong+++++"] = { type = "Model", model = "models/props_c17/utilityconnecter006d.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "base3", pos = Vector(0.455, 0, 0.455), angle = Angle(-91.024, 0, 0), size = Vector(0.129, 0.129, 0.129), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
    ["base2"] = { type = "Model", model = "models/props_lab/labpart.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "part", pos = Vector(0.2, 0.3, -1.155), angle = Angle(-0.866, -132.405, 90.864), size = Vector(0.225, 0.225, 0.225), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
    ["base3"] = { type = "Model", model = "models/props_junk/PopCan01a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "base2", pos = Vector(0, 8.076, 0.15), angle = Angle(16.441, 180, 90), size = Vector(0.801, 0.819, 2.098), color = Color(255, 255, 255, 255), surpresslightning = false, material = "Debug/env_cubemap_model", skin = 0, bodygroup = {} },
    ["prong+++"] = { type = "Model", model = "models/props_c17/utilityconnecter006d.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "base3", pos = Vector(0, 0.2, 0.455), angle = Angle(-91.024, -93.069, 0), size = Vector(0.129, 0.129, 0.129), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
    ["prong"] = { type = "Model", model = "models/props_c17/utilityconnecter006d.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "base3", pos = Vector(0, 0.2, -5), angle = Angle(-91.024, -93.069, 0), size = Vector(0.129, 0.129, 0.129), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
    ["part"] = { type = "Model", model = "models/healthvial.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "Handle", pos = Vector(0, 0, -5), angle = Angle(0, 0, 0), size = Vector(0.5, 0.5, 0.5), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
    ["prong++"] = { type = "Model", model = "models/props_c17/utilityconnecter006d.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "base3", pos = Vector(0, 0.2, -2.274), angle = Angle(-91.024, -93.069, 0), size = Vector(0.129, 0.129, 0.129), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
    ,
    ["cutter"] = { type = "Model", model = "models/rubyscythe.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "Handle", pos = Vector(-0, 1,0), angle = Angle(180, 0, 0), size = Vector(2,2,2), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
    ,
    ["twilysceptre_hl2size"] = { type = "Model", model = "models/freeman/twilysceptre_tf2size.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "Handle", pos = Vector(-1.6, -0.9, -1.155), angle = Angle(180, 0, 0), size = Vector(1,1,1), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }

}

function SWEP:PreDrawViewModel(vm, weapon, ply)

        for k,v in pairs(self.VElements) do
            self.VElements[k].color = Color(255, 255, 255, 0)
        end

        self.VElements["twilysceptre_hl2size"].color = Color(255, 255, 255, 255)

        self.VElements["cutter"].color = Color(255, 255, 255, 0)

    vm:SetMaterial("engine/occlusionproxy")
    return false
end
function SWEP:DrawWorldModel(vm, weapon, ply)

        for k,v in pairs(self.VElements) do
            self.WElements[k].color = Color(255, 255, 255, 0)
        end

        self.WElements["twilysceptre_hl2size"].color = Color(255, 255, 255, 255)

        self.WElements["cutter"].color = Color(255, 255, 255, 0)


    return self.BaseClass.DrawWorldModel(self)
end

SWEP.Purpose = "For feeling like you did when you hit that home run back in the summer of '06"
SWEP.AutoSwitchTo = true
SWEP.Contact = ""
SWEP.Author = "Spastik"
SWEP.FiresUnderwater = true
SWEP.Spawnable = true
SWEP.AdminSpawnable = true
SWEP.SlotPos = 0
SWEP.Instructions = "Legs spread, same width as the shoulders; body tight, then hit the ball like you're defeating the enemy. Here, the pinky finger is the key and then you just hit, hit, hit. Kakin! Bingo!"
SWEP.AutoSwitchFrom = false
SWEP.Base = "aa_base"
SWEP.Category = "S&G Munitions"
SWEP.DrawAmmo = false
SWEP.PrintName = "Lollipop"
SWEP.Primary.Recoil = 0
SWEP.Primary.Damage = 0
SWEP.Primary.NumShots = 0
SWEP.Primary.Cone = 0
SWEP.Primary.ClipSize = -1
SWEP.Primary.DefaultClip = -1
SWEP.Primary.Automatic = false
SWEP.Primary.Ammo = "none"
SWEP.Secondary.Ammo = "none"
SWEP.Slot = 1233
SWEP.Secondary.Automatic = true


SWEP.Primary.Sound = "weapons/usp/usp1.wav"
SWEP.Primary.Damage = 5
SWEP.Primary.TakeAmmo = 1
SWEP.Primary.ClipSize = 125
SWEP.Primary.ClipMax = 125
SWEP.Primary.Ammo = "SMG1"
SWEP.Primary.DefaultClip = 10000
SWEP.Primary.Spread = 0.2
SWEP.Primary.Cone = 0.05
SWEP.Primary.NumShots = 10
SWEP.Primary.Automatic = true
SWEP.Primary.Recoil = 0.01
SWEP.Primary.Delay = 0.5
SWEP.Primary.Force = 300
SWEP.BarrelAngle = 0
SWEP.Crosshair = 0.07

SWEP.Primary.Damage = 3
SWEP.Primary.Delay = 0.15
SWEP.Primary.Cone = 0.09
SWEP.Primary.ClipSize = 18000
SWEP.Primary.ClipMax = 18000
SWEP.Primary.DefaultClip	= 80000


function SWEP:OnDrop()
    self:Remove()
end

function SWEP:Think()
end
function SWEP:Deploy()
    self.Weapon:SendWeaponAnim( ACT_VM_DRAW )
    self.Weapon:SetNextPrimaryFire(CurTime() + 0.5)	--Delay using this weapon for one second so we can play the animation.


    return true
end

SWEP.nextmiget = 0
function SWEP:SecondaryAttack()
    if self.nextmiget > CurTime() then return end

    local trace = self.Owner:GetEyeTrace()

    if trace.HitPos:Distance(self.Owner:GetShootPos()) <= 250 then


    local ply = trace.Entity
    if not IsValid(ply) or not ply:IsPlayer() then return end
    self.nextmiget = CurTime() + 60
    local sound = "fu_brock.mp3"
    local soundfile = gb_config.websounds..sound
    gb_PlaySoundFromServer(soundfile, self.Owner)

    local offset =  ply:GetViewOffset()
    if not ply.oldspeedmul then
        ply.oldspeedmul =  ply:GetNWInt("speedmul")
        ply.oldjumppower = ply:GetJumpPower()
    end

    if self.Owner:Health() < 150 then
    self.Owner:SetHealth(self.Owner:Health()+25)
        if self.Owner:Health() > 150 then self.Owner:SetHealth(150) end
    end


    ply.runslowed =true
    if ply:Health() < 150 then
        ply:SetHealth(150)
    end
    ply.oldmodel = ply:GetModel()
    ply:SetModel("models/player/pbear/pbear.mdl")

    ply:SetModelScale(0.5, 0.1)
    ply:SetViewOffset(Vector(0,0,30))

    timer.Create("NormalizeRunSpeedFreeze2"..ply:UniqueID() ,8,1,function()
        if IsValid(ply)  then
            if tonumber(ply.oldspeedmul) < 220 then ply.OldRunSpeed = 260 end
            ply.runslowed =false
            if ply.oldmodel then
                ply:SetModel(ply.oldmodel)
            end


            ply:SetViewOffset(Vector(0,0,65))
            ply:SetModelScale(1, 2)

        end
    end)
        end
end


function SWEP:PrimaryAttack()
    self.Weapon:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
    self.Weapon:SetNextPrimaryFire( CurTime() + 0.3 )

    if not self:CanPrimaryAttack() then return end
    self:MakeAFlame()
    self:Disorientate()

    if not worldsnd then
        self.Weapon:EmitSound( self.Primary.Sound, 50)

    elseif SERVER then
        sound.Play(self.Primary.Sound, self:GetPos(), self.Primary.SoundLevel)
    end

    self:ShootBullet( self.Primary.Damage, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )

    self:TakePrimaryAmmo( 1 )
end


SWEP.nextreload = 0
function SWEP:Reload()
    if self.nextreload > CurTime() then return end
    self.nextreload = CurTime() + 0.4
    self.Weapon:SendWeaponAnim(ACT_VM_MISSCENTER)
    self.Weapon:SetNextPrimaryFire(CurTime() + .3)


    local trace = self.Owner:GetEyeTrace()

    if trace.HitPos:Distance(self.Owner:GetShootPos()) <= 999 then
        local bullet = {}
        bullet.Num = 1
        bullet.Src = self.Owner:GetShootPos()
        bullet.Dir = self.Owner:GetAimVector()
        bullet.Spread = Vector(0, 0, 0)
        bullet.Tracer = 0
        bullet.Force = 5
        bullet.Damage = 60
        self.Owner:FireBullets(bullet)
        self.Owner:SetAnimation(PLAYER_ATTACK1);
        self.Weapon:EmitSound("weapons/stunstick/stunstick_impact" .. math.random(1, 2) .. ".wav", 500, math.random(90, 110), 1, -1)
        self.Weapon:EmitSound("weapons/stunstick/stunstick_fleshhit" .. math.random(1, 2) .. ".wav", 500, math.random(90, 110), 1, 1)
        self.Weapon:EmitSound("physics/metal/metal_canister_impact_hard" .. math.random(1, 2) .. ".wav", 500, math.random(90, 100), 1, 0)
        if SERVER then
            local hitposent = ents.Create("info_target")
            local trace = self.Owner:GetEyeTrace()
            local hitpos = trace.HitPos

            local hiteffect = ents.Create("point_tesla")
            hiteffect:SetOwner(self:GetOwner())
            hiteffect:SetPos(hitpos)
            hiteffect:Spawn()
            hiteffect:SetKeyValue("beamcount_max", 15)
            hiteffect:SetKeyValue("beamcount_min", 10)
            hiteffect:SetKeyValue("interval_max", 0.5)
            hiteffect:SetKeyValue("interval_min", 0.1)
            hiteffect:SetKeyValue("lifetime_max", 0.3)
            hiteffect:SetKeyValue("lifetime_min", 0.3)
            hiteffect:SetKeyValue("m_Color", 255, 255, 255)
            hiteffect:SetKeyValue("m_flRadius", 50)
            hiteffect:SetKeyValue("m_SoundName", "DoSpark")
            hiteffect:SetKeyValue("texture", "sprites/physcannon_bluelight1b.vmt")
            hiteffect:SetKeyValue("thick_max", 5)
            hiteffect:SetKeyValue("thick_max", 4)
            hiteffect:Fire("DoSpark", "", 0.01)
            hiteffect:Fire("DoSpark", "", 0.1)
            hiteffect:Fire("DoSpark", "", 0.2)
            hiteffect:Fire("DoSpark", "", 0.3)
            hiteffect:Fire("DoSpark", "", 0.4)
            hiteffect:Fire("kill", "", 1.0)
        end
    else
        self.Weapon:EmitSound("weapons/stunstick/stunstick_swing" .. math.random(1, 2) .. ".wav", 500, math.random(90, 110), 1, 0)
        self.Weapon:SendWeaponAnim(ACT_VM_MISSCENTER)
        self.Owner:SetAnimation(PLAYER_ATTACK1)
    end
end




