

SWEP.PrintName			= "Silent's Sour Lemons"				// 'Nice' Weapon name (Shown on HUD)
SWEP.Slot				= 6						// Slot in the weapon selection menu
SWEP.SlotPos			= 1							// Position in the slot
SWEP.Icon				= "vgui/ttt_fun_killicons/lemongun.png"

SWEP.Base 				= "gb_camo_base"
SWEP.Camo = 43

SWEP.ViewModelFlip		= false
SWEP.ViewModel 			= "models/weapons/c_irifle.mdl"
SWEP.WorldModel 			= "models/weapons/w_irifle.mdl"
SWEP.HoldType				= "ar2"
SWEP.Spawnable			= true
SWEP.AdminSpawnable		= false
SWEP.UseHands = true


SWEP.Primary.Sound 		= Sound("weapons/lemonar2/fire1.mp3")
SWEP.Primary.Reload 		= Sound("weapons/lemonar2/empty.mp3")
SWEP.Primary.Recoil		= 0.3
SWEP.Primary.Damage		= 14
SWEP.Primary.NumShots		= 1
SWEP.Primary.Cone			= 0.016
SWEP.Primary.Delay 		= 0.12
SWEP.ViewModelFOV		= 75


SWEP.Primary.ClipSize		= 75					// Size of a clip
SWEP.Primary.DefaultClip	= 75					// Default number of bullets in a clip
SWEP.Primary.ClipMax = 90 
SWEP.Primary.Automatic		= true				// Automatic/Semi Auto
SWEP.Primary.Ammo			= "AR2"
SWEP.AmmoEnt = "item_ammo_revolver_ttt"

SWEP.Secondary.ClipSize		= -1					// Size of a clip
SWEP.Secondary.DefaultClip	= -1					// Default number of bullets in a clip
SWEP.Secondary.Automatic	= false				// Automatic/Semi Auto
SWEP.Secondary.Ammo		= "none"


SWEP.LimitedStock = false
SWEP.AllowDrop = true
SWEP.AutoSpawnable = false
SWEP.Spawnable = true
SWEP.AdminSpawnable = true
SWEP.Kind = WEAPON_EQUIP

SWEP.IronSightsPos 		= Vector (-5.8777, -11.2377, 1.1921)
SWEP.IronSightsAng 		= Vector (6.1388, -0.2003, 0)


function SWEP:Precache()

    	util.PrecacheSound("weapons/lemonar2/fire1.mp3")
end



SWEP.flyammo = 5
SWEP.nextFly = 0
function SWEP:SecondaryAttack()
   self:Fly()
end
