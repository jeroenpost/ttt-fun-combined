if CLIENT then

    SWEP.Author 		= "GreenBlack"

end
SWEP.PrintName	= "The Krieger"
SWEP.Base			= "gb_master_deagle"
SWEP.Icon = "vgui/ttt_fun_killicons/golden_deagle.png"
SWEP.Kind 			= WEAPON_PISTOL
SWEP.Slot = 1
SWEP.SlotPos 	= 1
SWEP.Primary.Recoil	= 1.25
SWEP.Primary.Damage = 45
SWEP.Primary.Delay 	= 0.5
SWEP.Primary.Cone 	= 0.001
SWEP.Primary.ClipSize 		= 200
SWEP.Primary.ClipMax 		= 9999
SWEP.Primary.DefaultClip 	= 9999
SWEP.Primary.NumShots		= 1
SWEP.Primary.Automatic 	= true
SWEP.HeadshotMultiplier = 2.5
SWEP.Primary.Ammo = "AlyxGun"
SWEP.AmmoEnt = "item_ammo_revolver_ttt"
SWEP.Primary.Sound = "meow.mp3"


function SWEP:PrimaryAttack(worldsnd)
    self.Weapon:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
    if not self:CanPrimaryAttack() then return end

    local soundfile = gb_config.websounds.."meow.mp3"
    gb_PlaySoundFromServer(soundfile, self.Owner)

    self:ShootBullet( self.Primary.Damage, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )

    if ( self.Owner:IsValid() ) then
        local tr = self.Owner:GetEyeTrace()
        local effectdata = EffectData()
        effectdata:SetOrigin(tr.HitPos)
        effectdata:SetNormal(tr.HitNormal)
        effectdata:SetScale(1)
        -- util.Effect("effect_mad_ignition", effectdata)
        util.Decal("FadingScorch", tr.HitPos + tr.HitNormal, tr.HitPos - tr.HitNormal)

        local tracedata = {}
        tracedata.start = tr.HitPos
        tracedata.endpos = Vector(tr.HitPos.x, tr.HitPos.y, tr.HitPos.z - 10)
        tracedata.filter = tr.HitPos
        local tracedata = util.TraceLine(tracedata)
        if SERVER then
            if tracedata.HitWorld then
                local flame = ents.Create("env_fire");
                flame:SetPos(tr.HitPos + Vector( 0, 0, 1 ));
                flame:SetKeyValue("firesize", "10");
                flame:SetKeyValue("fireattack", "10");
                flame:SetKeyValue("StartDisabled", "0");
                flame:SetKeyValue("health", "10");
                flame:SetKeyValue("firetype", "0");
                flame:SetKeyValue("damagescale", "5");
                flame:SetKeyValue("spawnflags", "128");
                flame:SetPhysicsAttacker(self.Owner)
                flame:SetOwner( self.Owner )
                flame:Spawn();
                flame:Fire("StartFire",0);
            end
        end
        self:TakePrimaryAmmo( 1 )
        local owner = self.Owner
        --if not ValidEntity(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

        owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )
    end
end

function SWEP:SecondaryAttack()
    if self.Owner:KeyDown(IN_RELOAD) then
        self:DropHealth("normal_health_station")
        return
    end
    self:Fly("meow.mp3",true)
end

SWEP.NextFreeze = 0
function SWEP:Think()
    if self.Owner:KeyDown(IN_USE) and self.NextFreeze < CurTime() then
        self.NextFreeze = CurTime() + 3
        self:Freeze()
        local soundfile = gb_config.websounds.."meow.mp3"
        gb_PlaySoundFromServer(soundfile, self.Owner)
    end
end

function SWEP:Reload()
    self:Cannibal(3)
end
