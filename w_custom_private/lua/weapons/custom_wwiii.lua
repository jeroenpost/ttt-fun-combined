SWEP.Base = "gb_camo_base"
SWEP.ViewModelFOV	= 54
SWEP.HoldType = "smg"
SWEP.PrintName = "Golden WW3"
SWEP.ViewModel		= "models/weapons/cstrike/c_rif_ak47.mdl"
SWEP.WorldModel		= "models/weapons/w_rif_ak47.mdl"
SWEP.Icon = "vgui/ttt_fun_killicons/ak47.png"
SWEP.Kind = WEAPON_NADE
SWEP.Slot = 4
SWEP.Primary.Delay = 0.25
SWEP.Primary.Recoil = 1
SWEP.Primary.Automatic = true
SWEP.Primary.Ammo = "Pistol"
SWEP.Primary.Damage = 36
SWEP.Primary.Cone = 0.018
SWEP.Primary.ClipSize = 250
SWEP.Primary.ClipMax = 250
SWEP.Primary.DefaultClip = 250
SWEP.AmmoEnt = "item_ammo_pistol_ttt"
SWEP.Primary.Sound = "weapons/silenced1.mp3"
SWEP.Primary.SoundLevel = 40
SWEP.Camo = 7
SWEP.AutoSpawnable = false

SWEP.HeadshotMultiplier = 1.2

SWEP.IronSightsPos = Vector( -6.6, -8.50, 3.40 )
SWEP.IronSightsAng = Vector( 0, 0, 0 )

SWEP.CanDecapitate = true
SWEP.HasFireBullet = true

SWEP.LaserColor = Color(120,120,120,255)

function SWEP:Deploy()
    self:DeployFunction()
    self.Owner.HasProtectionSuit = true
end

function SWEP:PrimaryAttack()
    self:NormalPrimaryAttack()
    if self:Clip1() > 0 then
        self:MakeAFlame()
        self:ShootTracer("LaserTracer_thick")
    end
end

function SWEP:SecondaryAttack()
    if self.Owner:KeyDown(IN_USE) then
        self:MakeExplosion()
        return
    end
    self:Fly()
end

function SWEP:PreDrop(yes)
   -- self:SetZoom(false)
   -- self:SetIronsights(false)
    if yes then self:Remove() end
   -- return self.BaseClass.PreDrop(self)
end