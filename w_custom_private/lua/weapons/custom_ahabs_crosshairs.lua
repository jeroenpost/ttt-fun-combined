SWEP.Base = "gb_camo_base"
SWEP.ViewModelFOV	= 54
SWEP.HoldType = "ar2"
SWEP.PrintName = "Ahab's Crosshairs"
SWEP.ViewModel		= "models/weapons/cstrike/c_snip_awp.mdl"
SWEP.WorldModel		= "models/weapons/w_snip_awp.mdl"


SWEP.HoldType = "ar2"
SWEP.Slot = 1
SWEP.CustomCamo = true
SWEP.Camo = 33

if CLIENT then

    SWEP.Author = "GreenBlack"

    SWEP.Icon = "vgui/ttt_fun_killicons/awp.png"
end


SWEP.HoldType			= "ar2"

SWEP.Primary.Delay       = 0.25
SWEP.Primary.Recoil      = 2.0
SWEP.Primary.Automatic   = true
SWEP.Primary.Damage      = 32
SWEP.Primary.Cone        = 0.0005
SWEP.Primary.Ammo        = "XBowBolt"
SWEP.Primary.ClipSize    = 200
SWEP.Primary.ClipMax     = 200
SWEP.Primary.DefaultClip = 200
SWEP.Primary.Sound       = Sound( "weapons/awp/awp1.wav" )
SWEP.Primary.SoundLevel = 10
SWEP.Tracer = 1

SWEP.Secondary.Sound = Sound("Default.Zoom")

SWEP.IronSightsPos      = Vector( 5, -15, -2 )
SWEP.IronSightsAng      = Vector( 2.6, 1.37, 3.5 )


SWEP.Kind = WEAPON_PISTOL
SWEP.AutoSpawnable = false
SWEP.AmmoEnt = "item_ammo_357_ttt"
SWEP.InLoadoutFor = nil
SWEP.LimitedStock = false
SWEP.AllowDrop = true
SWEP.IsSilent = false
SWEP.NoSights = false
SWEP.HasFireBullet = true

SWEP.tracerColor = Color(50,50,255,255)
function SWEP:PrimaryAttack()
    if self.Owner:KeyDown(IN_RELOAD) then
        self:TripMineStick()
    end
    if self.Owner:KeyDown(IN_RELOAD) then return end
    if self.Owner:KeyDown(IN_USE) then
        if not self.nextgreen then self.nextgreen = 0 end
        if self.nextgreen > CurTime() then
            self.Owner:PrintMessage( HUD_PRINTCENTER,  math.floor(self.nextgreen - CurTime()).." seconds left" )
            return
        end
        local soundfile = gb_config.websounds.."nyeh_0.mp3"
        gb_PlaySoundFromServer(soundfile, self.Owner)
        local tr = self.Owner:GetEyeTrace()
        self.LaserColor = Color(50,255,50,255)
        self:ShootTracer("LaserTracer_thick")
        self.nextgreen = CurTime() + 60
        if CLIENT then return end
        local ent = ents.Create( "cse_ent_shorthealthgrenade" )
        ent:SetPos( tr.HitPos  )
        ent:SetOwner( self.Owner  )
        ent:SetPhysicsAttacker(  self.Owner )
        ent.Owner = self.Owner
        ent:Spawn()
        ent.timer = CurTime() + 0.01
        ent.EndColor = "0 0 0"
        ent.weapon = self
        ent.StartColor = "0 255 0"
        timer.Simple(8,function()
            if IsValid(ent) then
                ent.Bastardgas:Remove()
                ent.Entity:Remove()
            end
        end)
        return
    end


    local mode = self:GetNWString("shootmode")

    if mode == "Shotgun" then
        local soundfile = gb_config.websounds.."nyeh_0.mp3"
        gb_PlaySoundFromServer(soundfile, self.Owner)
        self.Weapon:SetNextSecondaryFire( CurTime() + 0.5 )
        self.Weapon:SetNextPrimaryFire( CurTime() + 0.5 )

        self:ShootBullet( 11, 2, 6, 0.085 )
        self:ShootTracer("manatrace")
        self:TakePrimaryAmmo( 1 )

        local owner = self.Owner
        if not IsValid(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

        owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )
        return
    end
    if mode == "Auto" then

        if self:Clip1() > 0 then
            self:ShootTracer("manatrace")
            local soundfile = gb_config.websounds.."nyeh_0.mp3"
            gb_PlaySoundFromServer(soundfile, self.Owner)
        end
        self.BaseClass.PrimaryAttack(self)        return
    end


end

function SWEP:SetZoom(state)
    if CLIENT or not IsValid(self.Owner) then
        return
    else
        if state then
            self.Owner:SetFOV(20, 0.3)
        else
            self.Owner:SetFOV(0, 0.2)
        end
    end
end


-- Add some zoom to ironsights for this gun
function SWEP:SecondaryAttack()
    if self.Owner:KeyDown(IN_USE) then
        local soundfile = gb_config.websounds.."nyeh_0.mp3"
        gb_PlaySoundFromServer(soundfile, self.Owner)
        self:DropHealth("normal_health_station" )
        return
    end

    if self.Owner:KeyDown(IN_RELOAD)  then
        if not self.IronSightsPos then return end
        if self.Weapon:GetNextSecondaryFire() > CurTime() then return end

        bIronsights = not self:GetIronsights()

        self:SetIronsights( bIronsights )

        if SERVER then
            self:SetZoom(bIronsights)
        else
            self:EmitSound(self.Secondary.Sound)
        end

        self.Weapon:SetNextSecondaryFire( CurTime() + 0.3)
        self.Weapon:SetNextPrimaryFire( CurTime() + 0.75) --Set a long delay to prevent people from quickly scoping in and shooting.
        end
    end

function SWEP:PreDrop()
    self:SetZoom(false)
    self:SetIronsights(false)
    return self.BaseClass.PreDrop(self)
end

function SWEP:Holster()
    self:SetIronsights(false)
    self:SetZoom(false)
    return true
end

if CLIENT then
    local scope = surface.GetTextureID("sprites/scope")

    function SWEP:AdjustMouseSensitivity()
        return (self:GetIronsights() and 0.2) or nil
    end
end

local close = false
local hidden = true;
local lastclose = false;

-- CHARGING
SWEP.IsHolding = false
SWEP.LastHold = 0
SWEP.Sound = false
function SWEP:Think()


    local ply = self.Owner;
    if  IsValid( ply ) and self.hadBomb < 4 then

            local tr = ply:GetEyeTrace( );

            if tr.Entity:IsPlayer( ) and ply:GetPos( ):Distance( tr.Entity:GetPos( ) ) <= 99999999 and GetRoundState() == ROUND_ACTIVE then
                close = true;
            else
                close = false;
            end;
    end;

    self:Think2()



    local mode = self:GetNWString("shootmode")
    if  mode == "Charge" and self.Owner:KeyDown(IN_ATTACK) and not self.Owner:KeyDown(IN_USE) and not self.Owner:KeyDown(IN_RELOAD)    then

        if not self.IsHolding then
            self.LastHold = CurTime()
            self.IsHolding = true
            self.Sound = CreateSound( self,"weapons/gauss/chargeloop.wav");
            self.Sound:Play();
            self.Sound:ChangePitch(250, 2.5)
        end

        if self.IsHolding and self.LastHold + 3 < CurTime() then
            self:SuperShot()
            self.IsHolding = false
        end
    elseif mode == "Charge" and self.IsHolding then
        self.IsHolding = false
        self.Sound:Stop();
        self.Sound = false
        self:NormalSecondShot(self.LastHold+3-CurTime())
    end
    self.BaseClass.Think(self)



end



SWEP.hadtripmines = 0
function SWEP:TripMineStick()
    if SERVER then
        local ply = self.Owner
        if not IsValid(ply) then return end


        local ignore = {ply, self.Weapon}
        local spos = ply:GetShootPos()
        local epos = spos + ply:GetAimVector() * 80
        local tr = util.TraceLine({start=spos, endpos=epos, filter=ignore, mask=MASK_SOLID})

        if tr.HitWorld and self.hadtripmines < 3 then

            local mine = ents.Create("npc_tripmine")
            if IsValid(mine) then


                local tr_ent = util.TraceEntity({start=spos, endpos=epos, filter=ignore, mask=MASK_SOLID}, mine)

                if tr_ent.HitWorld then

                    local ang = tr_ent.HitNormal:Angle()
                    ang.p = ang.p + 90

                    mine:SetPos(tr_ent.HitPos + (tr_ent.HitNormal * 3))
                    mine:SetAngles(ang)
                   -- mine:SetModel("models/dynamite/dynamite.mdl")

                    mine:SetOwner(ply)
                    mine:Spawn()
                  --  mine:SetModel("models/dynamite/dynamite.mdl")
                    -- mine:SetHealth( 10 )

                    function mine:OnTakeDamage(dmg)
                        self:Remove()
                    end

                    -- mine:Ignite(100)
                    --  print( mine )
                    if SERVER then DamageLog("Tripmine: " .. self.Owner:Nick() .. " [" .. self.Owner:GetRoleString() .. "] placed a tripmine ")
                    end

                    mine.fingerprints = self.fingerprints

                    self:SendWeaponAnim( ACT_SLAM_TRIPMINE_ATTACH )

                    local holdup = self.Owner:GetViewModel():SequenceDuration()

                    timer.Simple(holdup,
                        function()
                            if SERVER then
                                self:SendWeaponAnim( ACT_SLAM_TRIPMINE_ATTACH2 )
                            end
                        end)

                    timer.Simple(holdup + .1,
                        function()
                            if SERVER then
                                if self.Owner == nil then return end
                                if self.Weapon:Clip1() == 0 && self.Owner:GetAmmoCount( self.Weapon:GetPrimaryAmmoType() ) == 0 then
                                --self.Owner:StripWeapon(self.Gun)
                                --RunConsoleCommand("lastinv")
                                self:Remove()
                                else
                                    self:Deploy()
                                end
                            end
                        end)


                    --self:Remove()
                    self.Planted = true

                    self.hadtripmines = self.hadtripmines + 1

                    if not self.Owner:IsTraitor() then
                        self.Owner:PrintMessage( HUD_PRINTCENTER, "You lost 75 health for placing a tripmine as inno" )
                        self.Owner:SetHealth( self.Owner:Health() - 75)
                    end

                end
            end
        end
    end
end


SWEP.TracerColor = Color(50,255,50,255)
function SWEP:SuperShot()

    if SERVER then
        local tr = self.Owner:GetEyeTrace()
        local ent = ents.Create("env_explosion")

        ent:SetPos(tr.HitPos)
        ent:SetOwner(self.Owner)
        ent:SetPhysicsAttacker(self.Owner)
        ent:Spawn()
        ent:SetKeyValue("iMagnitude", "125")
        ent:Fire("Explode", 0, 0)

        util.BlastDamage(self, self:GetOwner(), self:GetPos(), 60, 75)
        local soundfile = gb_config.websounds.."nyeh_0.mp3"
        gb_PlaySoundFromServer(soundfile, self.Owner)

        ent:EmitSound("weapons/big_explosion.mp3")
    end
    self:FireBolt()
    self:ShootTracer( "LaserTracer_thick")
end

function SWEP:NormalSecondShot(multi)
    multi = 3 - multi
    if multi > 0 and multi < 0.3 then return end
    if multi < 1 then multi = 0 end

    damage = 20+ (multi*25)
    if SERVER then
        local tr = self.Owner:GetEyeTrace()
        local ent = ents.Create("env_explosion")

        ent:SetPos(tr.HitPos)
        ent:SetOwner(self.Owner)
        ent:SetPhysicsAttacker(self.Owner)
        ent:Spawn()
        ent:SetKeyValue("iMagnitude", damage)
        ent:Fire("Explode", 0, 0)

        util.BlastDamage(self, self:GetOwner(), self:GetPos(), 60, 75)

        ent:EmitSound("weapons/big_explosion.mp3")
    end
    self:FireBolt()
    self:ShootTracer( "LaserTracer_thick")
end


SWEP.NextReload = 0
function SWEP:Reload()
    if self.Owner:KeyDown(IN_ATTACK) then
        return
    end
    if self.Owner:KeyDown(IN_USE) and self.NextReload < CurTime() then
        self.NextReload = CurTime() + 0.3
        local mode = self:GetNWString("shootmode")
        self:SetIronsights(false)

        if mode == "Auto" then  self:SetNWString("shootmode","Charge")  end
        if mode == "Charge" then  self:SetNWString("shootmode","Shotgun") end
        if mode == "Shotgun" then  self:SetNWString("shootmode","Auto") end

        self.Weapon:SetNextPrimaryFire( CurTime() + 0.8 )
        return
    end




    if self.NextStick > CurTime() then return end
    self.NextStick = CurTime() + 1

    if self:GetNWBool( "Planted" )  then
        self:BombSplode()
    elseif  self.hadBomb < 5 then

        self:StickIt()
    end;

end


function SWEP:Deploy()
    self:SetNWString("shootmode","Auto")

    self.BaseClass.Deploy(self)

end


SWEP.BeepSound = Sound( "C4.PlantSound" );

SWEP.ClipSet = false

if CLIENT then
    local scope = surface.GetTextureID("sprites/scope")
    function SWEP:DrawHUD( )
        if  self.hadBomb < 5 then
            local planted = self:GetNWBool( "Planted" );
            local tr = LocalPlayer( ):GetEyeTrace( );
            local close2;

            if tr.Entity:IsPlayer( ) and LocalPlayer( ):GetPos( ):Distance( tr.Entity:GetPos( ) ) <= 99999999 then
                close2 = true;
            else
                close2 = false;
            end;

            local planted_text = "Not Planted!";
            local close_text = "Nobody is in range!";

            if planted then

                planted_text = "Planted!\nReload to Explode!";

                surface.SetFont( "ChatFont" );
                local size_x, size_y = surface.GetTextSize( planted_text );

                draw.RoundedBox( 6, ScrW( ) / 2 - size_x / 2 - 5, ScrH( ) - 100, size_x + 15, size_y + 10, Color( 0, 0, 0, 150 ) );
                draw.DrawText( planted_text, "ChatFont", ScrW( ) / 2, ScrH( ) - 100 + 5, Color( 255, 30, 30, 255 ), TEXT_ALIGN_CENTER );
            else
                if close2 then
                    close_text = "Close Enough!\nReload to stick!!";
                end;

                surface.SetFont( "ChatFont" );
                local size_x, size_y = surface.GetTextSize( planted_text );

                surface.SetFont( "ChatFont" );
                local size_x2, size_y2 = surface.GetTextSize( close_text );

                draw.RoundedBox( 6, ScrW( ) / 2 - ( size_x2 / 2 ) - 7.5, ScrH( ) - 100, size_x2 + 20, size_y + size_y2 + 10, Color( 0, 0, 0, 150 ) );
                draw.DrawText( planted_text, "ChatFont", ScrW( ) / 2, ScrH( ) - 100 + 5, Color( 30, 30, 255, 255 ), TEXT_ALIGN_CENTER );
                draw.DrawText( close_text, "ChatFont", ScrW( ) / 2 , ScrH( ) - 100 + size_y + 5, Color( 255, 255, 255, 255 ), TEXT_ALIGN_CENTER );
            end;
        end;

        local shotttext = "Shootmode: "..self:GetNWString("shootmode").."\nE+R to change"
        surface.SetFont( "ChatFont" );
        local size_x, size_y = surface.GetTextSize( shotttext );

        draw.RoundedBox( 6, ScrW( ) / 2 - size_x / 2 - 5, ScrH( ) - 150, size_x + 15, size_y + 10, Color( 0, 0, 0, 150 ) );
        draw.DrawText( shotttext , "ChatFont", ScrW( ) / 2, ScrH( ) - 150 + 5, Color( 255, 30, 30, 255 ), TEXT_ALIGN_CENTER );


        if self:GetIronsights() then
            surface.SetDrawColor( 0, 0, 0, 255 )

            local x = ScrW() / 2.0
            local y = ScrH() / 2.0
            local scope_size = ScrH()

            -- crosshair
            local gap = 80
            local length = scope_size
            surface.DrawLine( x - length, y, x - gap, y )
            surface.DrawLine( x + length, y, x + gap, y )
            surface.DrawLine( x, y - length, x, y - gap )
            surface.DrawLine( x, y + length, x, y + gap )

            gap = 0
            length = 50
            surface.DrawLine( x - length, y, x - gap, y )
            surface.DrawLine( x + length, y, x + gap, y )
            surface.DrawLine( x, y - length, x, y - gap )
            surface.DrawLine( x, y + length, x, y + gap )


            -- cover edges
            local sh = scope_size / 2
            local w = (x - sh) + 2
            surface.DrawRect(0, 0, w, scope_size)
            surface.DrawRect(x + sh - 2, 0, w, scope_size)

            surface.SetDrawColor(255, 0, 0, 255)
            surface.DrawLine(x, y, x + 1, y + 1)

            -- scope
            surface.SetTexture(scope)
            surface.SetDrawColor(255, 255, 255, 255)

            surface.DrawTexturedRectRotated(x, y, scope_size, scope_size, 0)

        else
            return self.BaseClass.DrawHUD(self)
        end

    end
end


-- Spiderman

local sndPowerUp		= Sound("rope_hit.mp3")
local sndPowerDown		= Sound ("shoot_rope.mp3")
local sndTooFar			= Sound ("to_far.mp3")

function SWEP:Initialize()

    nextshottime = CurTime()
    self.BaseClass.Initialize(self)
end


SWEP.NextAmmoGive = 0
SWEP.NextAmmoTake = 0
SWEP.NextRegenAmmo = 0
function SWEP:Think2()



    if self.NextRegenAmmo < CurTime() then
        if self:Clip1() < 24 then
            self.NextRegenAmmo = CurTime() + 3
            self:SetClip1(self:Clip1()+2)
        end
    end

    if self.dt.reloading and IsFirstTimePredicted() then
        if self.Owner:KeyDown(IN_ATTACK) then
            self:FinishReload()
            return
        end

        if self.reloadtimer <= CurTime() then

            if self.Owner:GetAmmoCount(self.Primary.Ammo) <= 0 then
                self:FinishReload()
            elseif self.Weapon:Clip1() < self.Primary.ClipSize then
                self:PerformReload()
            else
                self:FinishReload()
            end
            return
        end
    end

    if self.NextAmmoGive < CurTime() && self:Clip1() < 10 then
    self.NextAmmoGive = CurTime() + 3
    self:SetClip1( self:Clip1() + 1 )
    end

    if (!self.Owner || self.Owner == NULL) or self:Clip1() < 1 then

    if ( !self.Beam ) then return end
    if( IsValid( self.Beam) ) then
        self.Beam:Remove()
    end
    self.Beam = nil
    return
    end



    if ( self.Owner:KeyPressed( IN_ATTACK2 ) ) then

        self:StartAttack()

    elseif ( self.Owner:KeyDown( IN_ATTACK2 )  ) then

        self:UpdateAttack()
        self.Weapon:SetNextSecondaryFire( CurTime() + 1 )

    elseif ( self.Owner:KeyReleased( IN_ATTACK2 )  ) then

        self:EndAttack( true )

    end



end

function SWEP:DoTrace( endpos )
    local trace = {}
    trace.start = self.Owner:GetShootPos()
    trace.endpos = trace.start + (self.Owner:GetAimVector() * 14096) --14096 is length modifier.
    if(endpos) then trace.endpos = (endpos - self.Tr.HitNormal * 7) end
    trace.filter = { self.Owner, self.Weapon }

    self.Tr = nil
    self.Tr = util.TraceLine( trace )
end




SWEP.DrawCrosshair      = true
SWEP.hadBomb = 0
SWEP.NextStick = 0
SWEP.Planted = false;

local hidden = true;

local lastclose = false;

function SWEP:StickIt()

    --self.Weapon:SetNextSecondaryFire( CurTime() + self.Secondary.Delay )

    local ply = self.Owner;
    if not IsValid( ply ) then return; end;

    local tr = ply:GetEyeTrace(MASK_SHOT);

    if not self:GetNWBool( "Planted" ) then

        if close then
            self.hadBomb = self.hadBomb + 1
            if SERVER then
                self.PlantedPly = tr.Entity
                self:SetNWBool("Planted", true)
            end;
        end;
    end;
end


function SWEP:DoSplode( owner, plantedply, bool )

    self:SetNWBool( "Planted", false )

    timer.Simple( 1, function( ) plantedply:EmitSound( self.BeepSound ); end );
    timer.Simple( 1.5, function( ) plantedply:EmitSound( self.BeepSound ); end );
    timer.Simple( 2, function( ) plantedply:EmitSound( self.BeepSound ); end );
    timer.Simple( 2.5, function( )
        local effectdata = EffectData()
        effectdata:SetOrigin(plantedply:GetPos())
        util.Effect("HelicopterMegaBomb", effectdata)

        local explosion = ents.Create("env_explosion")
        explosion:SetOwner(self.Owner)
        explosion:SetPos(plantedply:GetPos( ))
        explosion:SetKeyValue("iMagnitude", "45")
        explosion:SetKeyValue("iRadiusOverride", 0)
        explosion:Spawn()
        explosion:Activate()
        explosion:Fire("Explode", "", 0)
        explosion:Fire("kill", "", 0)
        self:SetNWBool( "Planted", false )



    end );




end;

function SWEP:BombSplode()
    local ply = self.Owner;
    if not IsValid( ply ) then return; end;

    if IsValid( self.PlantedPly ) and self.PlantedPly:Alive( ) then
        if SERVER then
            self:DoSplode( self.Owner, self.PlantedPly, false );
        end;
    else
        if SERVER then
            if IsValid( self.PlantedPly ) and IsValid( self.PlantedPly.server_ragdoll ) then
                self:DoSplode( self.Owner, self.PlantedPly.server_ragdoll, true );
            else
                ply:PrintMessage( HUD_PRINTTALK, "The body this bomb has been planted to is no longer available!" );
            end;
        end;
        --  self.Weapon:SendWeaponAnim( ACT_SLAM_DETONATOR_DETONATE );
    end;
end


function SWEP:StartAttack()


    if not self:CanPrimaryAttack() then return end

    --Get begining and end poins of trace.
    local gunPos = self.Owner:GetShootPos() --Start of distance trace.
    local disTrace = self.Owner:GetEyeTrace() --Store all results of a trace in disTrace.
    local hitPos = disTrace.HitPos --Stores Hit Position of disTrace.

    --Calculate Distance
    --Thanks to rgovostes for this code.
    local x = (gunPos.x - hitPos.x)^2;
    local y = (gunPos.y - hitPos.y)^2;
    local z = (gunPos.z - hitPos.z)^2;
    local distance = math.sqrt(x + y + z);

    --Only latches if distance is less than distance CVAR
    local distanceCvar = GetConVarNumber("grapple_distance")
    inRange = false
    if distance <= distanceCvar then
        inRange = true
    end

    if inRange then
        if (SERVER) then

            if (!self.Beam) then --If the beam does not exist, draw the beam.
            if not self:CanPrimaryAttack() then return end
            --grapple_beam
            self.Beam = ents.Create( "trace1" )
            self.Beam:SetPos( self.Owner:GetShootPos() )
            self.Beam:Spawn()
            self:TakePrimaryAmmo(1)

            end

            self.Beam:SetParent( self.Owner )
            self.Beam:SetOwner( self.Owner )

        end


        self:DoTrace()
        self.speed = 10000 --Rope latch speed. Was 3000.
        self.startTime = CurTime()
        self.endTime = CurTime() + self.speed
        self.dtfix = -1

        if (SERVER && self.Beam) then
        self.Beam:GetTable():SetEndPos( self.Tr.HitPos )
        end

        self:UpdateAttack()

        self.Weapon:EmitSound( sndPowerDown )
    else
        --Play A Sound
        self.Weapon:EmitSound( sndTooFar )
    end
end

function SWEP:UpdateAttack()

    -- if not self:CanPrimaryAttack() then return end
    if !self.Tr or self:Clip1() < 1 then return end
    self.Owner:LagCompensation( true )


    if (!endpos) then endpos = self.Tr.HitPos end

    if (SERVER && self.Beam) then
    self.Beam:GetTable():SetEndPos( endpos )
    end

    lastpos = endpos



    if ( self.Tr.Entity:IsValid() ) then

        endpos = self.Tr.Entity:GetPos()
        if ( SERVER and IsValid(self.Beam) ) then
            self.Beam:GetTable():SetEndPos( endpos )
        end

    end

    local vVel = (endpos - self.Owner:GetPos())
    local Distance = endpos:Distance(self.Owner:GetPos())

    local et = (self.startTime + (Distance/self.speed))
    if(self.dtfix != 0) then
    self.dtfix = (et - CurTime()) / (et - self.startTime)
    end
    if(self.dtfix < 0) then
        self.Weapon:EmitSound( sndPowerUp )
        self.dtfix = 0
    end

    if(self.dtfix == 0) then
        zVel = self.Owner:GetVelocity().z
        vVel = vVel:GetNormalized()*(math.Clamp(Distance,0,25))
        if( SERVER ) then
            if  self.NextAmmoTake < CurTime() then
                self:TakePrimaryAmmo(1)
                self.NextAmmoTake = CurTime() + 1
                self.NextAmmoGive = CurTime() + 1.2
            end

            local gravity = GetConVarNumber("sv_gravity")

            vVel:Add(Vector(0,0,(gravity/50)*1.5))

            if(zVel < 0) then
                vVel:Sub(Vector(0,0,zVel/100))
            end
            self.Owner:SetVelocity(vVel)
        end
    end

    endpos = nil

    self.Owner:LagCompensation( false )

end

function SWEP:EndAttack( shutdownsound )

    if ( shutdownsound ) then
        self.Weapon:EmitSound( sndPowerDown )
    end

    if ( CLIENT ) then return end
    if ( !self.Beam ) then return end
    if( IsValid( self.Beam) ) then
        self.Beam:Remove()
    end
    self.Beam = nil

end
