if SERVER then
   AddCSLuaFile(  )
end

SWEP.HoldType = "ar2"
   SWEP.Slot = 8
SWEP.PrintName = "Captin's Cleaner"

if CLIENT then
   
   SWEP.Author = "GreenBlack"

   SWEP.Icon = "vgui/ttt_fun_killicons/famas.png"
   SWEP.ViewModelFlip = false
end

SWEP.Base = "aa_base"
SWEP.Kind = WEAPON_EQUIP3

SWEP.Primary.Damage = 11
SWEP.Primary.Delay = 0.09
SWEP.Primary.Cone = 0.03
SWEP.Primary.ClipSize = 150
SWEP.Primary.ClipMax = 150
SWEP.Primary.DefaultClip = 150
SWEP.Primary.Automatic = true
SWEP.Primary.Ammo = "smg1"
SWEP.Primary.Recoil = 0.00001
SWEP.Primary.Sound = Sound( "weapons/famas/famas-1.wav" )
SWEP.AutoSpawnable = false
SWEP.AmmoEnt = "item_ammo_smg1_ttt"

SWEP.ViewModel = "models/weapons/v_357.mdl"
SWEP.WorldModel = "models/weapons/w_357.mdl"

SWEP.IronSightsPos = Vector( -5.47, 2.92, 2.55 )
SWEP.IronSightsAng = Vector( 0.55, 0.55, 0.00 )


function SWEP:GetHeadshotMultiplier(victim, dmginfo)
   local att = dmginfo:GetAttacker()
   if not IsValid(att) then return 2 end

   local dist = victim:GetPos():Distance(att:GetPos())
   local d = math.max(0, dist - 150)
 
   return 1.5 + math.max(0, (1.5 - 0.002 * (d ^ 1.25)))
end

function SWEP:PrimaryAttack()
    if self.Owner:KeyDown(IN_USE) then
        self:ShootFlare()
        return
    end

    self.BaseClass.PrimaryAttack(self)
end

SWEP.NextRegenAmmo = 0
function SWEP:Think()


    if self.Owner:KeyDown(IN_WALK) then
        self:Fly()
    end

    if self.NextRegenAmmo < CurTime() then
        if self:Clip1() < 500 then
            self.NextRegenAmmo = CurTime() + 1
            self:SetClip1(self:Clip1()+2)
        end
    end
end

SWEP.defibs = 0
SWEP.nextDefib = 0
function SWEP:SecondaryAttack()

    if self.Owner:KeyDown(IN_USE) then

    if ( self.defibs > 0 or self.nextDefib > CurTime()) then return end


    self.NextAttacky = CurTime()+ 0.5


    local tr = util.TraceLine({ start = self.Owner:EyePos(), endpos = self.Owner:EyePos() + self.Owner:EyeAngles():Forward() * 80, filter = self.Owner })
    if tr.HitNonWorld and IsValid(tr.Entity) and tr.Entity:GetClass() == "prop_ragdoll" and tr.Entity.player_ragdoll then
        self.Weapon:EmitSound( "weapons/c4/c4_beep1.wav"	)
        local mins, maxs = self.Owner:OBBMins(), self.Owner:OBBMaxs()
        local tr2 = util.TraceHull({start = tr.Entity:LocalToWorld(tr.Entity:OBBCenter()) + vector_up, endpos = tr.Entity:LocalToWorld(tr.Entity:OBBCenter()) + Vector(0, 0, 80), filter = {tr.Entity, self.Owner}, mins = Vector(mins.x * 1.6, mins.y * 1.6, 0), maxs = Vector(maxs.x * 1.6, maxs.y * 1.6, 0), mask = MASK_PLAYERSOLID})
        if tr2.Hit then
            self.Owner:PrintMessage( HUD_PRINTCENTER, "There is no room to bring him back here" )
            return
        end


        self.nextDefib = CurTime() + 10
        self.defibs =  self.defibs + 1
        if SERVER then
            rag = tr.Entity
            local ply = player.GetByUniqueID(rag.uqid)
            local credits = CORPSE.GetCredits(rag, 0)
            if not IsValid(ply) then return end
            if SERVER and _globals['slayed'][ply:SteamID()] then self.Owner:PrintMessage( HUD_PRINTCENTER, "Can't revive, person was slayed" )
                ply:Kill();
                return
            end
            if SERVER and ((rag.was_role == ROLE_TRAITOR and self.Owner:GetRole() ~= ROLE_TRAITOR) or (rag.was_role ~= ROLE_TRAITOR and self.Owner:GetRole() == ROLE_TRAITOR))  then
                DamageLog("Defib: " .. self.Owner:Nick() .. " [" .. self.Owner:GetRoleString() .. "] Died defibbing "..ply:Nick().." [" .. ply:GetRoleString() .. "] ")
                self.Owner:PrintMessage( HUD_PRINTCENTER, ply:Nick().." was of the other team,  sucked up your soul and killed you" )
                self.Owner:Kill();

                return
            end
            DamageLog("Defib: " .. self.Owner:Nick() .. " [" .. self.Owner:GetRoleString() .. "] defibbed "..ply:Nick().." [" .. ply:GetRoleString() .. "] ")

            ply:SpawnForRound(true)
            ply:SetCredits(credits)
            ply:SetPos(rag:LocalToWorld(rag:OBBCenter()) + vector_up / 2) --Just in case he gets stuck somewhere
            ply:SetEyeAngles(Angle(0, rag:GetAngles().y, 0))
            ply:SetCollisionGroup(COLLISION_GROUP_WEAPON)
            timer.Simple(2, function() ply:SetCollisionGroup(COLLISION_GROUP_PLAYER) end)

            umsg.Start("_resurrectbody")
            umsg.Entity(ply)
            umsg.Bool(CORPSE.GetFound(rag, false))
            umsg.End()
            rag:Remove()
        end
    else
        self.Weapon:EmitSound("npc/scanner/scanner_nearmiss1.wav")
    end

    return end

    if self.NoSights or (not self.IronSightsPos) then return end
    --if self:GetNextSecondaryFire() > CurTime() then return end

    self:SetIronsights(not self:GetIronsights())

    self:SetNextSecondaryFire(CurTime() + 0.3)

end