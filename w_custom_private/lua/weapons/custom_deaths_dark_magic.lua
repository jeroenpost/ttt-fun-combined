SWEP.Base = "gb_camo_base"
SWEP.ViewModelFOV	= 54
SWEP.HoldType = "physgun"

SWEP.PrintName = "Death's Dark Magic"
SWEP.UseHands			= true
SWEP.ViewModel  = "models/weapons/c_superphyscannon.mdl"
SWEP.WorldModel = "models/weapons/w_physics.mdl"
SWEP.Kind = WEAPON_PISTOL
SWEP.Slot = 1
SWEP.AutoSpawnable = false
SWEP.Primary.Damage		    = 35
SWEP.Primary.Delay		    = 0.5
SWEP.Primary.ClipSize		= 8
SWEP.Primary.DefaultClip	= 8
SWEP.Primary.Cone	        = 0.008
SWEP.Primary.Automatic		= false
SWEP.Primary.Ammo			= "AlyxGun"
SWEP.AmmoEnt = "item_ammo_revolver_ttt"
SWEP.Primary.Sound = Sound( "weapons/ar2/fire1.wav" )
SWEP.HeadshotMultiplier = 2.5
SWEP.Camo = 11
SWEP.IronSightsPos = Vector( -4.6, -3, 0.65 )
SWEP.IronSightsAng = Vector( 0, 0, 0.00 )

function SWEP:OnDrop()
    self:Remove()
end

SWEP.NextFire2 = 0
function SWEP:SecondaryAttack()
    if self.NextFire2 > CurTime() then return end
    self.NextFire2 = CurTime() + 3
    self:EmitSound(self.Primary.Sound)
    if SERVER then
        local ball = ents.Create("deaths_magic_skull");

        if (ball) then
            ball:SetPos(self.Owner:GetPos() + Vector(0,0,48));
            ball.Owner = self.Owner
            ball:Spawn();
            local physicsObject = ball:GetPhysicsObject();
            physicsObject:ApplyForceCenter(self.Owner:GetAimVector() * 3000);
            return ball;
        end;
    end
end
SWEP.NextSwap = 0
function SWEP:Reload()
    if self.NextSwap < CurTime() then
        self.NextSwap = CurTime() + 0.2
        local tr = self.Owner:GetEyeTrace()
        local hitEnt = tr.Entity

        if IsValid( tr.Entity) and not  tr.Entity:IsWorld()  then

            local phys = tr.Entity:GetPhysicsObject()
            if not phys:IsValid() then return end
            if phys:GetVolume() and phys:GetVolume() > 100000 then
                self.Owner:PrintMessage( HUD_PRINTCENTER, "Prop is too big" )
                return
            end

            local pos =  self.Owner:GetPos()
            self.Owner:SetPos(hitEnt:GetPos())
            hitEnt:SetPos(pos)
            local ef = EffectData()
            ef:SetEntity(self.Owner)
            util.Effect("swap_effect", ef,true,true)
            self:EmitSound("ambient/energy/zap5.wav")
            self.NextSwap = CurTime() + 11
        end
    end
end

SWEP.NextFire2 = 0
function SWEP:PrimaryAttack()
    if self.NextFire2 > CurTime() then return end
    self.NextFire2 = CurTime() + 3
    self:EmitSound(self.Primary.Sound)
    if SERVER then
        local ball = ents.Create("deaths_magic_skull");

        if (ball) then
            ball:SetPos(self.Owner:GetPos() + Vector(0,0,48));
            ball.Owner = self.Owner
            ball.explodeontouch = true
            ball:Spawn();
            local physicsObject = ball:GetPhysicsObject();
            physicsObject:ApplyForceCenter(self.Owner:GetAimVector() * 25000);
            return ball;
        end;
    end
end


local ENT = {}

ENT.Type = "anim"
ENT.Base = "base_anim"


if SERVER then

    function ENT:Initialize()
        self:SetModel("models/Gibs/HGIBS.mdl")
        self:SetModelScale(self:GetModelScale()*1,0)
        self.Entity:PhysicsInit(SOLID_VPHYSICS);
        self.Entity:SetMoveType(MOVETYPE_VPHYSICS);
        self.Entity:SetSolid(SOLID_VPHYSICS);

        --self.Entity:SetCollisionGroup(COLLISION_GROUP_WEAPON)
        local phys = self:GetPhysicsObject()
        if phys:IsValid() then
            phys:Wake()
            phys:EnableGravity(true);
        end

        self.activate = CurTime() + 0.03



        timer.Create(self:EntIndex().."sandboom2boom",10,1,function()
            self:Explode()
        end)
    end

    function ENT:Explode()
        if self.exploded then return end self.exploded = true
        local effect = EffectData()
        effect:SetStart(self:GetPos())
        effect:SetOrigin(self:GetPos())
        effect:SetScale(1)
        effect:SetRadius(1)
        effect:SetMagnitude(1)

        util.Effect("Explosion", effect, true, true)
        if not self.explodeontouch then
        util.BlastDamage(self, self.Owner, self:GetPos(), 250, 100)
        else
            util.BlastDamage(self, self.Owner, self:GetPos(), 50, 45)
        end
        self:Remove()
    end

    ENT.touches = 0
    function ENT:PhysicsCollide(data, obj)
        if self.activate < CurTime() and self.explodeontouch and obj:IsPlayer() and not obj.nextsandtouch or (obj.nextsandtouch and obj.nextsandtouch < CurTime()) then
            obj.nextsandtouch = CurTime() + 3
            obj:Ignite(0.5,0)
            local dmginfo = DamageInfo()
            dmginfo:SetDamage( 2 ) --50 damage
            dmginfo:SetDamageType( DMG_BULLET ) --Bullet damage
            dmginfo:SetAttacker( self.Owner or self ) --First player found gets credit
            dmginfo:SetDamageForce( Vector( 0, 0, 50 ) ) --Launch upwards
            dmginfo:SetInflictor(self.Owner or self)
            ent:TakeDamageInfo( dmginfo )
            self:Explode()
        end
    end

    function ENT:Touch( ent )
        if self.activate < CurTime() and self.explodeontouch and ent:IsPlayer() and not ent.nextsandtouch or (ent.nextsandtouch and ent.nextsandtouch < CurTime()) then
            ent.nextsandtouch = CurTime() + 3
            ent:Ignite(0.5,0)
            local dmginfo = DamageInfo()
            dmginfo:SetDamage( 2 ) --50 damage
            dmginfo:SetDamageType( DMG_BULLET ) --Bullet damage
            dmginfo:SetAttacker( self.Owner or self ) --First player found gets credit
            dmginfo:SetDamageForce( Vector( 0, 0, 50 ) ) --Launch upwards
            dmginfo:SetInflictor(self.Owner or self)
            ent:TakeDamageInfo( dmginfo )
            self:Explode()
        end
    end


    function ENT:Use( activator, caller )
        self:EmitSound("common/bugreporter_failed.wav")
    end


end

scripted_ents.Register( ENT, "deaths_magic_skull", true )