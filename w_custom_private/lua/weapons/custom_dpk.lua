
SWEP.HoldType = "shotgun"
SWEP.PrintName    = "Valve's Deadly Popper"
SWEP.Slot         = 7

if CLIENT then




    SWEP.ViewModelFlip = false


    SWEP.Icon = "vgui/ttt_fun_killicons/knife.png"
end

SWEP.Base               = "aa_base"

SWEP.UseHands			= true
SWEP.ViewModelFlip		= false
SWEP.ViewModelFOV		= 54
SWEP.ViewModel = "models/Teh_Maestro/popcorn.mdl"
SWEP.WorldModel = "models/Teh_Maestro/popcorn.mdl"
SWEP.HeadshotMultiplier = 1.1

function SWEP:GetViewModelPosition( pos , ang)
    pos,ang = LocalToWorld(Vector(30,-10,-15),Angle(0,0,0),pos,ang)
    return pos, ang
end

SWEP.DrawCrosshair      = false
SWEP.Primary.Damage         = 65
SWEP.Primary.ClipSize       = 40
SWEP.Primary.DefaultClip    = 90
SWEP.Primary.Automatic      = false
SWEP.Primary.Sound = "ambient/energy/zap1.wav"
SWEP.Primary.Delay = 0.45
SWEP.Primary.Ammo       = "none"
SWEP.Secondary.ClipSize     = 50
SWEP.Secondary.DefaultClip  = 90
SWEP.Secondary.Automatic    = false
SWEP.Secondary.Ammo     = "none"
SWEP.Secondary.Delay = 1.2
SWEP.Primary.Recoil = 0.01

SWEP.Kind = WEAPON_ROLE

SWEP.LimitedStock = true -- only buyable once

SWEP.IsSilent = true

-- Pull out faster than standard guns
SWEP.DeploySpeed = 1

function SWEP:Deploy()
    self.Weapon:SendWeaponAnim(ACT_VM_DRAW_SILENCED)
    self:SetNWString("shootmode","knife")
    return true
end



function SWEP:PrimaryAttack()
    self.Weapon:SetNextPrimaryFire( CurTime() + 1.5 )
    self.Weapon:SetNextSecondaryFire( CurTime() + 1.5 )
    self.NextSecondary = CurTime() + 1.2

   -- if not self:CanPrimaryAttack() then return end

    if  SERVER then
        sound.Play(self.Primary.Sound, self:GetPos(), self.Primary.SoundLevel)
    end

    local mode = self:GetNWString("shootmode")
    if mode == "knife" then

    self.Owner:LagCompensation(true)

    local spos = self.Owner:GetShootPos()
    local sdest = spos + (self.Owner:GetAimVector() * 70)

    local kmins = Vector(1,1,1) * -10
    local kmaxs = Vector(1,1,1) * 10

    local tr = util.TraceHull({start=spos, endpos=sdest, filter=self.Owner, mask=MASK_SHOT_HULL, mins=kmins, maxs=kmaxs})

    -- Hull might hit environment stuff that line does not hit
    if not IsValid(tr.Entity) then
        tr = util.TraceLine({start=spos, endpos=sdest, filter=self.Owner, mask=MASK_SHOT_HULL})
    end

    local hitEnt = tr.Entity

    -- effects
    if IsValid(hitEnt) then
        self.Weapon:SendWeaponAnim( ACT_VM_HITCENTER )

        local edata = EffectData()
        edata:SetStart(spos)
        edata:SetOrigin(tr.HitPos)
        edata:SetNormal(tr.Normal)
        edata:SetEntity(hitEnt)

        if hitEnt:IsPlayer() or hitEnt:GetClass() == "prop_ragdoll" then
            util.Effect("BloodImpact", edata)
        end
    else
        self.Weapon:SendWeaponAnim( ACT_VM_MISSCENTER )
    end

    if SERVER then
        self.Owner:SetAnimation( PLAYER_ATTACK1 )
    end


    if SERVER and tr.Hit and tr.HitNonWorld and IsValid(hitEnt) then
        if hitEnt:IsPlayer() then
            -- knife damage is never karma'd, so don't need to take that into
            -- account we do want to avoid rounding error strangeness caused by
            -- other damage scaling, causing a death when we don't expect one, so
            -- when the target's health is close to kill-point we just kill

            local dmg = DamageInfo()
            dmg:SetDamage(60)
            dmg:SetAttacker(self.Owner)
            dmg:SetInflictor(self.Weapon or self)
            dmg:SetDamageForce(self.Owner:GetAimVector() * 5)
            dmg:SetDamagePosition(self.Owner:GetPos())
            dmg:SetDamageType(DMG_SLASH)

            hitEnt:DispatchTraceAttack(dmg, spos + (self.Owner:GetAimVector() * 3), sdest)

            if math.random(1,2) == 2 then
                local v = hitEnt
                local owner = self.Owner
                timer.Create(v:EntIndex().."poisonknife",5,6,function()
                    if IsValid(v) and IsValid(owner) and v:Health() > 1 then
                        v:TakeDamage( 5,
                            owner)
                        v:Ignite(0.2)
                        hook.Add("TTTEndRound",v:EntIndex().."poisonknife",function()
                            timer.Destroy(v:EntIndex().."poisonknife");
                            v:Extinguish()
                        end)
                    end
                end)

            end

        else
            bullet = {}
            bullet.Num    = 1
            bullet.Src    = self.Owner:GetShootPos()
            bullet.Dir    = self.Owner:GetAimVector()
            bullet.Spread = Vector(0, 0, 0)
            bullet.Tracer = 0
            bullet.Force  = 1
            bullet.Damage = 60
            self.Owner:FireBullets(bullet)
            self.Owner:ViewPunch(Angle(7, 0, 0))
        end
    end

    self.Owner:LagCompensation(false)

    end
    if mode == "shotgun" then  self:ShootBullet( 6, 4, 15, 0.105 ) self.Weapon:SetNextPrimaryFire( CurTime() + 0.8 )
    if SERVER then
            local bucket = ents.Create( "sent_popcorn_thrown" )
            bucket:SetOwner( self.Owner )

            bucket:SetPos( self.Owner:GetShootPos( ) )
            bucket:Spawn()
            bucket:SetMaterial( "engine/occlusionproxy" )
            bucket:Activate()


            local phys = bucket:GetPhysicsObject( )

            if IsValid( phys ) then
                phys:SetVelocity( self.Owner:GetPhysicsObject():GetVelocity() )
                phys:AddVelocity( self.Owner:GetAimVector( ) * 128 * phys:GetMass( ) )
                phys:AddAngleVelocity( VectorRand() * 128 * phys:GetMass( ) )
            end
            net.Start("Popcorn_Explosion")
            net.WriteVector(self:GetPos())
            net.WriteVector(VectorRand())
            net.WriteVector(phys:GetVelocity())
            net.WriteFloat(bucket:EntIndex())
            net.Broadcast()
        end

    end
    if mode == "sniper" then self:ShootBullet( math.random(60,90), 3, 1, 0.0001 ) self.Weapon:SetNextPrimaryFire( CurTime() + 1.8 ) end


  -- self:TakePrimaryAmmo( 1 )

    local owner = self.Owner
    if not IsValid(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

    owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )
end

SWEP.IronSightsPos      = Vector( -100, -100, -100 )
SWEP.IronSightsAng      = Vector( 2.6, 1.37, 3.5 )
SWEP.Secondary.Sound = Sound("Default.Zoom")
function SWEP:SecondaryAttack()



    if self.Owner:KeyDown(IN_USE) then
        self:Jihad()
        return
    end
    if self.Owner:KeyDown(IN_RELOAD) then
        self:DropHealth()
        return
    end

    local mode = self:GetNWString("shootmode")
    if mode == "sniper" then
            if not self.IronSightsPos then return end
            if self.Weapon:GetNextSecondaryFire() > CurTime() then return end

            local bIronsights = not self:GetIronsights()

            self:SetIronsights( bIronsights )

            if SERVER then
                self:SetZoom(bIronsights)
            else
                self:EmitSound(self.Secondary.Sound)
            end

            self.Weapon:SetNextSecondaryFire( CurTime() + 0.3)
        return
    end


    self:Fly()
end

function SWEP:SetZoom(state)
    if CLIENT then
        return
    elseif IsValid(self.Owner) and self.Owner:IsPlayer() then
        if state then
            self.Owner:SetFOV(30, 0.3)
        else
            self.Owner:SetFOV(0, 0.2)
        end
    end
end

SWEP.NextAmmo = 0
function SWEP:Think()
    if self.NextAmmo < CurTime() and self:Clip1() < 20 then
        self.NextAmmo = CurTime() + 2
        self:SetClip1(self:Clip1() + 1)
    end
    self:HealSecond()
end

function SWEP:DropHealth()
    if self.NextHealthDrop > CurTime() then return end
    self.NextHealthDrop = CurTime() + 5
    local healttt = 200
    if self.healththing and SERVER then
        healttt = Entity(self.healththing):GetStoredHealth()
        Entity(self.healththing):Remove()
    end
    self:HealthDrop(healttt)
end


SWEP.NextHealthDrop = 0

local throwsound = Sound( "Weapon_SLAM.SatchelThrow" )

function SWEP:HealthDrop( healttt)
    if SERVER then
        local ply = self.Owner
        if not IsValid(ply) then return end

        --if self.Planted then return end

        local vsrc = ply:GetShootPos()
        local vang = ply:GetAimVector()
        local vvel = ply:GetVelocity()

        local vthrow = vvel + vang * 200

        local health = ents.Create("normal_health_station")
        if IsValid(health) then
            health:SetPos(vsrc + vang * 10)
            health:Spawn()
            --health:SetModel( "models/props_lab/cactus.mdl")
            health:SetModelScale(health:GetModelScale()*1,0)
            health:SetPlacer(ply)
            --health.healsound = "ginger_cartman.mp3"

            health:PhysWake()
            local phys = health:GetPhysicsObject()
            if IsValid(phys) then
                phys:SetVelocity(vthrow)
            end
          --  health:SetModel( "models/props_lab/cactus.mdl")
            health:SetStoredHealth(healttt)
            health.setOwner = self.Owner

            self.healththing = health:EntIndex()

            self.Planted = true
        end
    end

    self.Weapon:EmitSound(throwsound)
end


function SWEP:Equip()
    self.Weapon:SetNextPrimaryFire( CurTime() + (self.Primary.Delay * 1.5) )
    self.Weapon:SetNextSecondaryFire( CurTime() + (self.Secondary.Delay * 1.5) )
end

function SWEP:PreDrop()
    -- for consistency, dropped knife should not have DNA/prints
    self.fingerprints = {}
end

function SWEP:OnRemove()
    if CLIENT and IsValid(self.Owner) and self.Owner == LocalPlayer() and self.Owner:Alive() then
        RunConsoleCommand("lastinv")
    end
end

if CLIENT then
    function SWEP:DrawHUD()
        local tr = self.Owner:GetEyeTrace(MASK_SHOT)

        if tr.HitNonWorld and IsValid(tr.Entity) and tr.Entity:IsPlayer()
                and tr.Entity:Health() < (self.Primary.Damage + 10) then

            local x = ScrW() / 2.0
            local y = ScrH() / 2.0

            surface.SetDrawColor(255, 0, 0, 255)

            local outer = 20
            local inner = 10
            surface.DrawLine(x - outer, y - outer, x - inner, y - inner)
            surface.DrawLine(x + outer, y + outer, x + inner, y + inner)

            surface.DrawLine(x - outer, y + outer, x - inner, y + inner)
            surface.DrawLine(x + outer, y - outer, x + inner, y - inner)

            draw.SimpleText("INSTANT KILL", "TabLarge", x, y - 30, COLOR_RED, TEXT_ALIGN_CENTER, TEXT_ALIGN_BOTTOM)
        end

        return self.BaseClass.DrawHUD(self)
    end
end

SWEP.NextSecondary = 0
SWEP.NextReload = 0
function SWEP:Reload()
    if self.Owner:KeyDown(IN_ATTACK2) then return end

    if self.Owner:KeyDown(IN_USE) and self.NextReload < CurTime() then
        self.NextReload = CurTime() + 0.3
        local mode = self:GetNWString("shootmode")
        self:SetIronsights(false)
        self:SetZoom(false)
        if mode == "knife" then  self:SetNWString("shootmode","shotgun") self.Primary.Sound = "weapons/shotgun/shotgun_dbl_fire.wav" end
        if mode == "shotgun" then  self:SetNWString("shootmode","sniper") self.Primary.Sound = "weapons/crossbow/fire1.wav" end
        if mode == "sniper" then  self:SetNWString("shootmode","knife") self.Primary.Sound = "weapons/stunstick/spark1.wav" end
        self.Weapon:SetNextPrimaryFire( CurTime() + 0.8 )
        return
    end




end



if CLIENT then
    function SWEP:DrawHUD( )
        local shotttext = "Shootmode: "..self:GetNWString("shootmode").."\nE+R to change"
        surface.SetFont( "ChatFont" );
        local size_x, size_y = surface.GetTextSize( shotttext );

        draw.RoundedBox( 6, ScrW( ) / 2 - size_x / 2 - 5, ScrH( ) - 100, size_x + 15, size_y + 10, Color( 0, 0, 0, 150 ) );
        draw.DrawText( shotttext , "ChatFont", ScrW( ) / 2, ScrH( ) - 100 + 5, Color( 255, 30, 30, 255 ), TEXT_ALIGN_CENTER );

        local scope = surface.GetTextureID("sprites/scope")
        if self:GetIronsights() then
            surface.SetDrawColor( 0, 0, 0, 255 )

            local x = ScrW() / 2.0
            local y = ScrH() / 2.0
            local scope_size = ScrH()

            -- crosshair
            local gap = 80
            local length = scope_size
            surface.DrawLine( x - length, y, x - gap, y )
            surface.DrawLine( x + length, y, x + gap, y )
            surface.DrawLine( x, y - length, x, y - gap )
            surface.DrawLine( x, y + length, x, y + gap )

            gap = 0
            length = 50
            surface.DrawLine( x - length, y, x - gap, y )
            surface.DrawLine( x + length, y, x + gap, y )
            surface.DrawLine( x, y - length, x, y - gap )
            surface.DrawLine( x, y + length, x, y + gap )


            -- cover edges
            local sh = scope_size / 2
            local w = (x - sh) + 2
            surface.DrawRect(0, 0, w, scope_size)
            surface.DrawRect(x + sh - 2, 0, w, scope_size)

            surface.SetDrawColor(255, 0, 0, 255)
            surface.DrawLine(x, y, x + 1, y + 1)

            -- scope
            surface.SetTexture(scope)
            surface.SetDrawColor(255, 255, 255, 255)

            surface.DrawTexturedRectRotated(x, y, scope_size, scope_size, 0)

        else
            return self.BaseClass.DrawHUD(self)
        end
    end

    function SWEP:AdjustMouseSensitivity()
        return (self:GetIronsights() and 0.2) or nil
    end
end;
