SWEP.Base = "aa_base"
SWEP.ViewModelFOV	= 75
SWEP.HoldType = "normal"
SWEP.PrintName = "Professional Gunslinger"

SWEP.WorldModel = ""
SWEP.ViewModel = "models/weapons/v_pist_magnum.mdl"
SWEP.WorldModel = "models/weapons/w_357.mdl"

SWEP.Kind = WEAPON_PISTOL
SWEP.Slot = 1
SWEP.AutoSpawnable = false
SWEP.Primary.Damage		    = 9
SWEP.Primary.Delay		    = 10
SWEP.Primary.ClipSize		= 3
SWEP.Primary.DefaultClip	= 3
SWEP.Primary.NumShots = 20
SWEP.Primary.Cone	        = 0.008
SWEP.Primary.Recoil	        = 0.008
SWEP.Primary.Automatic		= true
SWEP.Secondary.ClipSize		= -1
SWEP.Secondary.DefaultClip	= -1
SWEP.Secondary.ClipSize     = -1
SWEP.Secondary.DefaultClip  = -1
SWEP.Secondary.Automatic    = true
SWEP.Secondary.Ammo     = "none"
SWEP.Secondary.Delay = 0.4
SWEP.Primary.Ammo = "yup"
SWEP.AmmoEnt = "fake"
SWEP.Primary.Sound          = Sound("Weapon_Deagle.Single")
SWEP.HeadshotMultiplier = 2.5
SWEP.ShowViewModel = false
SWEP.ViewModelFlip = true

function SWEP:PrimaryAttack()
    if self:GetNWInt("Holdtype") == 1 then
        return
    end
    self.BaseClass.PrimaryAttack(self)

    self:SendWeaponAnim(ACT_VM_DRAW)
    self:SetHoldType( "normal" )
    if SERVER then
        self:SetNWInt("Holdtype", 1)
    end

    self:SetColorAndMaterial(Color(255,255,255,255),"engine/occlusionproxy")
   -- self.WorldModel = ""
   -- self.VElements['wunderwaffe'].color.a = 0
end

function SWEP:DrawWorldModel( )
    local hand, offset, rotate
    --self:SetColorAndMaterial("engine/occlusionproxy",Color(255,255,255,0))
    if  self:GetNWInt("Holdtype") == 2 or not IsValid(self.Owner) then
        self:DrawModel( )
        return
    end
end

function SWEP:SecondaryAttack()

    self:SetNextSecondaryFire( CurTime() + 0.5)

    if  self:GetNWInt("Holdtype") != 2 then

        self:SetHoldType( "pistol" )
        if SERVER then
        self:SetNWInt("Holdtype", 2)
        end
        self:SetColorAndMaterial(Color(255,255,255,255),"models/weapons/w_357.mdl")
         self:SendWeaponAnim(ACT_VM_DRAW)

    else


        self:SetHoldType( "normal" )
       -- if SERVER then
        self:SetNWInt("Holdtype", 1)
       -- end
        self:SetColorAndMaterial(Color(255,255,255,255),"engine/occlusionproxy")
    end

end


function SWEP:Initialize()

    self:SetColorAndMaterial(Color(255,255,255,255),"engine/occlusionproxy")

    self:SetHoldType( "normal" )
    if SERVER then
    self:SetNWInt("Holdtype", 1)
end
    -- other initialize code goes here

end

SWEP.IronSightsPos = Vector(-6.361, -3.701, 2.15)
SWEP.IronSightsAng = Vector(0, 0, 0)

function SWEP:Deploy()
    timer.Simple(120,function()
        if IsValid(self) then
            if self:Clip1() < 1 then
                self:SetClip1(1)
            end
        end
    end)

    self.BaseClass.Deploy(self)
    self:SetColorAndMaterial(Color(255,255,255,255),"engine/occlusionproxy")
    --cam.IgnoreZ(true)
end


function SWEP:PreDrawViewModel()
    --cam.IgnoreZ(true)
    if self:GetNWInt("Holdtype") == 1 then
        --self.VElements['wunderwaffe'].color.a = 0

         self.Owner:GetViewModel():SetMaterial(  "engine/occlusionproxy"  )
    else
        --self.VElements['wunderwaffe'].color.a = 255
        self.Owner:GetViewModel():SetMaterial(   )
    end
end


function SWEP:OnDrop()
    self:SetColorAndMaterial(Color(255,255,255,255),"models/weapons/w_357.mdl")
end