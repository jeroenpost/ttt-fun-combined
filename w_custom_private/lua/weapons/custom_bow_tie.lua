
AddCSLuaFile()

SWEP.HoldType = "knife"

if CLIENT then

    SWEP.PrintName    = "Spood's Bow Tie"
    SWEP.Slot         = 6

    SWEP.ViewModelFlip = false

    SWEP.EquipMenuData = {
        type = "item_weapon",
        desc = "knife_desc"
    };

    SWEP.Icon = "vgui/ttt/icon_knife"
end

SWEP.Base               = "gb_camo_base"

SWEP.UseHands			= true
SWEP.ViewModelFlip		= false
SWEP.ViewModelFOV		= 54
SWEP.ViewModel          = "models/weapons/cstrike/c_knife_t.mdl"
SWEP.WorldModel         = "models/weapons/w_knife_t.mdl"
SWEP.Camo = 20
SWEP.CustomCamo = true
SWEP.DrawCrosshair      = false
SWEP.Primary.Damage         = 75
SWEP.Primary.ClipSize       = -1
SWEP.Primary.DefaultClip    = -1
SWEP.Primary.Automatic      = true
SWEP.Primary.Delay = 0.6
SWEP.Primary.Ammo       = "none"
SWEP.Secondary.ClipSize     = -1
SWEP.Secondary.DefaultClip  = -1
SWEP.Secondary.Automatic    = true
SWEP.Secondary.Ammo     = "none"
SWEP.Secondary.Delay = 1.4

SWEP.Kind = WEAPON_EQUIP1

SWEP.LimitedStock = true -- only buyable once
SWEP.WeaponID = AMMO_KNIFE

SWEP.IsSilent = true

-- Pull out faster than standard guns
SWEP.DeploySpeed = 2
include "weaponfunctions/teleport.lua"

function SWEP:FirePulse(force_fwd, force_up)
    if not IsValid(self.Owner) then return end

    self.Owner:SetAnimation( PLAYER_ATTACK1 )

    sound.Play(self.Primary.Sound, self:GetPos(), self.Primary.SoundLevel)

    self:SendWeaponAnim(ACT_VM_IDLE)

    local cone = self.Primary.Cone or 0.1
    local num = 6

    local bullet = {}
    bullet.Num    = num
    bullet.Src    = self.Owner:GetShootPos()
    bullet.Dir    = self.Owner:GetAimVector()
    bullet.Spread = Vector( cone, cone, 0 )
    bullet.Tracer = 1
    bullet.Force  = force_fwd / 10
    bullet.Damage = 1
    bullet.TracerName = "AirboatGunHeavyTracer"

    local owner = self.Owner
    local fwd = force_fwd / num
    local up = force_up / num
    bullet.Callback = function(att, tr, dmginfo)
        local ply = tr.Entity
        if SERVER and IsValid(ply) and ply:IsPlayer() and (not ply:IsFrozen()) then
            local pushvel = tr.Normal * fwd

            pushvel.z = math.max(pushvel.z, up)

            ply:SetGroundEntity(nil)
            ply:SetLocalVelocity(ply:GetVelocity() + pushvel)

            ply.was_pushed = {att=owner, t=CurTime()}

        end
    end

    self.Owner:FireBullets( bullet )

end

function SWEP:PrimaryAttack()
    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
    self.Weapon:SetNextSecondaryFire( CurTime() + self.Secondary.Delay )

    if  self.Owner:KeyDown(IN_USE) then
        self:FirePulse(200,500)
        return
    end

    if  self.Owner:KeyDown(IN_RELOAD) then
        self:DoTeleport()
        return
    end


    if not IsValid(self.Owner) then return end

    self.Owner:LagCompensation(true)

    local spos = self.Owner:GetShootPos()
    local sdest = spos + (self.Owner:GetAimVector() * 70)

    local kmins = Vector(1,1,1) * -10
    local kmaxs = Vector(1,1,1) * 10

    local tr = util.TraceHull({start=spos, endpos=sdest, filter=self.Owner, mask=MASK_SHOT_HULL, mins=kmins, maxs=kmaxs})

    -- Hull might hit environment stuff that line does not hit
    if not IsValid(tr.Entity) then
        tr = util.TraceLine({start=spos, endpos=sdest, filter=self.Owner, mask=MASK_SHOT_HULL})
    end

    local hitEnt = tr.Entity

    self:Disorientate()

    -- effects
    if IsValid(hitEnt) then
        self.Weapon:SendWeaponAnim( ACT_VM_HITCENTER )

        local edata = EffectData()
        edata:SetStart(spos)
        edata:SetOrigin(tr.HitPos)
        edata:SetNormal(tr.Normal)
        edata:SetEntity(hitEnt)

        if hitEnt:IsPlayer() or hitEnt:GetClass() == "prop_ragdoll" then
            util.Effect("BloodImpact", edata)
        end
    else
        self.Weapon:SendWeaponAnim( ACT_VM_MISSCENTER )
    end

    if SERVER then
        self.Owner:SetAnimation( PLAYER_ATTACK1 )
    end


    if SERVER and tr.Hit and tr.HitNonWorld and IsValid(hitEnt) then
        if hitEnt:IsPlayer() then
            -- knife damage is never karma'd, so don't need to take that into
            -- account we do want to avoid rounding error strangeness caused by
            -- other damage scaling, causing a death when we don't expect one, so
            -- when the target's health is close to kill-point we just kill

                local dmg = DamageInfo()
                dmg:SetDamage(self.Primary.Damage)
                dmg:SetAttacker(self.Owner)
                dmg:SetInflictor(self.Weapon or self)
                dmg:SetDamageForce(self.Owner:GetAimVector() * 5)
                dmg:SetDamagePosition(self.Owner:GetPos())
                dmg:SetDamageType(DMG_SLASH)

                hitEnt:DispatchTraceAttack(dmg, spos + (self.Owner:GetAimVector() * 3), sdest)

        end
    end

    self.Owner:LagCompensation(false)
end

SWEP.nextprimary = 0
SWEP.nextprimary2= 0
function SWEP:Reload()
    if self.Owner:KeyDown(IN_ATTACK) or self.Owner:KeyDown(IN_ATTACK2) then return end
    if self.nextprimary2 > CurTime() then return end
    if self.Weapon.nextprimary and  self.Weapon.nextprimary > CurTime() then
        self.Owner:PrintMessage( HUD_PRINTCENTER,  math.floor(self.Weapon.nextprimary - CurTime()).." seconds left" )

        return
    end
    self.nextprimary = CurTime() + 5
    self.nextprimary2 = CurTime() + 0.50
    if CLIENT then return end
    local tr = self.Owner:GetEyeTrace(MASK_SHOT)
    CORPSE.ShowSearch(self.Owner, tr.Entity, false, true)

end

function SWEP:SecondaryAttack()
    if not self.Owner:KeyDown(IN_USE) and not self.Owner:KeyDown(IN_RELOAD) then
        self:Fly()
        return
    end
    if  self.Owner:KeyDown(IN_RELOAD) then
        self:SetTeleportMarker()
        return
    end
    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
    self.Weapon:SetNextSecondaryFire( CurTime() + 0.66 )


    self.Weapon:SendWeaponAnim( ACT_VM_MISSCENTER )

    if SERVER then
        local ply = self.Owner
        if not IsValid(ply) then return end

        ply:SetAnimation( PLAYER_ATTACK1 )

        local ang = ply:EyeAngles()

        if ang.p < 90 then
            ang.p = -10 + ang.p * ((90 + 10) / 90)
        else
            ang.p = 360 - ang.p
            ang.p = -10 + ang.p * -((90 + 10) / 90)
        end

        local vel = math.Clamp((90 - ang.p) * 5.5, 550, 800)

        local vfw = ang:Forward()
        local vrt = ang:Right()

        local src = ply:GetPos() + (ply:Crouching() and ply:GetViewOffsetDucked() or ply:GetViewOffset())

        src = src + (vfw * 1) + (vrt * 3)

        local thr = vfw * vel + ply:GetVelocity()

        local knife_ang = Angle(-28,0,0) + ang
        knife_ang:RotateAroundAxis(knife_ang:Right(), -90)

        local knife = ents.Create("normal_throw_knife")
        if not IsValid(knife) then return end
        knife:SetPos(src)
        knife:SetAngles(knife_ang)

        knife:Spawn()

        knife.Damage = self.Primary.Damage - 25

        knife:SetOwner(ply)
        knife.Weaponised = true

        local phys = knife:GetPhysicsObject()
        if IsValid(phys) then
            phys:SetVelocity(thr)
            phys:AddAngleVelocity(Vector(0, 1500, 0))
            phys:Wake()
        end
        timer.Simple(5,function()
            if IsValid(knife) then knife:Remove() end
        end)
    end
end

function SWEP:Equip()
    self.Weapon:SetNextPrimaryFire( CurTime() + (self.Primary.Delay * 1.5) )
    self.Weapon:SetNextSecondaryFire( CurTime() + (self.Secondary.Delay * 1.5) )
end

function SWEP:PreDrop()
    -- for consistency, dropped knife should not have DNA/prints
    self.fingerprints = {}
end

function SWEP:OnRemove()
    if CLIENT and IsValid(self.Owner) and self.Owner == LocalPlayer() and self.Owner:Alive() then
        RunConsoleCommand("lastinv")
    end
end

if CLIENT then
    function SWEP:DrawHUD()
        local tr = self.Owner:GetEyeTrace(MASK_SHOT)

        if tr.HitNonWorld and IsValid(tr.Entity) and tr.Entity:IsPlayer()
                and tr.Entity:Health() < (self.Primary.Damage + 10) then

            local x = ScrW() / 2.0
            local y = ScrH() / 2.0

            surface.SetDrawColor(255, 0, 0, 255)

            local outer = 20
            local inner = 10
            surface.DrawLine(x - outer, y - outer, x - inner, y - inner)
            surface.DrawLine(x + outer, y + outer, x + inner, y + inner)

            surface.DrawLine(x - outer, y + outer, x - inner, y + inner)
            surface.DrawLine(x + outer, y - outer, x + inner, y - inner)

            draw.SimpleText("INSTANT KILL", "TabLarge", x, y - 30, COLOR_RED, TEXT_ALIGN_CENTER, TEXT_ALIGN_BOTTOM)
        end

        return self.BaseClass.DrawHUD(self)
    end
end


