
SWEP.Kind = WEAPON_HEAVY
SWEP.PsID = "cm_awp"
SWEP.BulletLength = 8.58
SWEP.CaseLength = 69.20

SWEP.MuzVel = 668.633

SWEP.InternalParts = {
	[1] = {{key = "hbar"}, {key = "lbar"}},
	[2] = {{key = "hframe"}},
	[3] = {{key = "ergonomichandle"}},
	[4] = {{key = "customstock"}},
	[5] = {{key = "lightbolt"}, {key = "heavybolt"}},
	[6] = {{key = "gasdir"}}}

SWEP.PrintName			= "Professional Tr1ck5h0t"
if ( CLIENT ) then


	SWEP.Author				= "Counter-Strike"
	SWEP.Slot				= 5
	SWEP.SlotPos = 0 //			= 1
	SWEP.IconLetter			= "r"
	SWEP.Muzzle = "cstm_muzzle_br"
	SWEP.SparkEffect = "cstm_child_sparks_large"
	SWEP.SmokeEffect = "cstm_child_smoke_large"
	SWEP.BoneApproachSpeed = 0.8
	SWEP.SemiText = "Hax-action"

	SWEP.PitchMod = 0.5
	SWEP.RollMod = 0.5
	
	killicon.AddFont( "cstm_sniper_awp", "CSKillIcons", SWEP.IconLetter, Color( 255, 80, 0, 255 ) )
	
	SWEP.VElements = {
		["silencer"] = { type = "Model", model = "models/props_c17/oildrum001.mdl", bone = "v_weapon.awm_parent", pos = Vector(-0.232, 3.381, 20), angle = Angle(1.031, 3.094, 0), size = Vector(0.059, 0.059, 0.25), color = Color(255, 255, 255, 1), surpresslightning = false, material = "models/bunneh/silencer", skin = 0, bodygroup = {} }
	}
	
	SWEP.WElements = {
		["silencer"] = { type = "Model", model = "models/props_c17/oildrum001.mdl", pos = Vector(37.721, 1.019, -10.728), angle = Angle(-100, 0, 0), size = Vector(0.059, 0.059, 0.25), color = Color(255, 255, 255, 1), surpresslightning = false, material = "models/bunneh/silencer", skin = 0, bodygroup = {} }
	}
end

SWEP.Category = "Customizable Weaponry"
SWEP.HoldType			= "ar2"
SWEP.Base				= "cstm_base_sniper"
SWEP.FireModes = {"bolt"}

SWEP.Spawnable			= true
SWEP.AdminSpawnable		= true

SWEP.ViewModelFOV = 70
SWEP.ViewModelFlip = true
SWEP.ViewModel = "models/weapons/v_snip_awp.mdl"
SWEP.WorldModel = "models/weapons/w_snip_awp.mdl"
SWEP.ShowViewModel = true
SWEP.ShowWorldModel = true
SWEP.ViewModelBonescales = {}

SWEP.Weight				= 5
SWEP.AutoSwitchTo		= false
SWEP.AutoSwitchFrom		= false

SWEP.Primary.Sound			= Sound("Weapon_AWP.Single")
SWEP.Primary.Recoil			= 6
SWEP.Primary.Damage			= 100
SWEP.Primary.NumShots		= 1
SWEP.Primary.Cone			= 0.0001
SWEP.Primary.ClipSize		= 25
SWEP.Primary.Delay			= 0.5
SWEP.Primary.DefaultClip	= 25
SWEP.Primary.Automatic		= false
SWEP.Primary.Ammo			= ".338"
SWEP.InitialHoldtype = "smg"
SWEP.InHoldtype = "smg"
SWEP.SilencedSound = Sound("weapons/scout/scout_fire-1.wav")
SWEP.SilencedVolume = 80
SWEP.HeadshotMultiplier = 2.5

-- Animation speed/custom reload function related
SWEP.IsReloading = false
SWEP.NoBoltAnim = true
SWEP.AnimPrefix = "awm_"
SWEP.ReloadSpeed = 1
SWEP.ShouldBolt = false
SWEP.ReloadDelay = 0
SWEP.IncAmmoPerc = 0.65 -- Amount of frames required to pass (in percentage) of the reload animation for the weapon to have it's amount of ammo increased

-- Dynamic accuracy related
SWEP.ShotsAmount 			= 0
SWEP.ConeDecAff				= 0
SWEP.DefRecoil				= 0.0001
SWEP.DefCone				= 0.0001
SWEP.DecreaseRecoilTime 	= 0
SWEP.ConeAff1 				= 0 -- Crouching/standing
SWEP.ConeAff2 				= 0 -- Using ironsights

SWEP.UnConeTime				= 0 -- Amount of time after firing the last shot that needs to pass until accuracy increases
SWEP.FinalCone				= 0 -- Self explanatory
SWEP.VelocitySensivity		= 0.001 -- Percentage of how much the cone increases depending on the player's velocity (moving speed). Rifles - 100%; SMGs - 80%; Pistols - 60%; Shotguns - 20%
SWEP.HeadbobMul 			= 1
SWEP.IsSilenced 			= false
SWEP.AltSilencedSound 		= true
SWEP.NoAuto = false
SWEP.IronsightsCone			= 0.0001
SWEP.HipCone = 0.0001

SWEP.Secondary.Automatic	= false
SWEP.Secondary.Ammo			= "none"

SWEP.IsUsingIronsights 		= false
SWEP.TargetMul = 0
SWEP.SetAndForget			= false
SWEP.DrawCrosshair = true;
SWEP.IronSightsPos = Vector(6.0749, -5.5216, 2.3984)
SWEP.IronSightsAng = Vector(2.5174, -0.0099, 0)

SWEP.AimPos = Vector(5.4, -3.425, 2.086)
SWEP.AimAng = Vector(0, 0, 0)

SWEP.ChargePos = Vector (5.4056, -10.3522, -4.0017)
SWEP.ChargeAng = Vector (-1.7505, -55.5187, 68.8356)

function SWEP:PrimaryAttack()


end

function SWEP:Think()

    if self.Owner:KeyDown(IN_ATTACK) then
        if not self.Owner.nextTrickshot2 then self.Owner.nextTrickshot2 = 0 end
        if self.Owner.StopTrickshot or self.Owner.nextTrickshot2 > CurTime() then return end
        if not self.Owner.nextTrickshot then self.Owner.nextTrickshot = 0 end
        self.Owner.nextTrickshot = CurTime() + 0.2
        if not self.Owner.trickshotturn or self.Owner.nextTrickshot < CurTime() then self.Owner.trickshotturn = 0 end

        local eyeang = self.Owner:EyeAngles()
        if not self.Owner.oldeyeang then self.Owner.oldeyeang = self.Owner:EyeAngles() end

        self.Owner.trickshotturn = self.Owner.trickshotturn + 36
        if self.Owner.trickshotturn > 359 then

            self.Owner:SetEyeAngles(self.Owner.oldeyeang)
            self.Owner.nextTrickshot = 0
            self.Owner.trickshotturn = 0
            self:NormalPrimaryAttack()
            self.Owner.StopTrickshot = true
            self.Owner.nextTrickshot2 = CurTime() + 0.5
            self.Owner.oldeyeang = false
            self:SphereExplosion()

        else
            eyeang.yaw = eyeang.yaw + 36
            self.Owner:SetEyeAngles(eyeang)
        end



    end

    if self.Owner:KeyReleased(IN_ATTACK) then
        self.Owner.StopTrickshot = false
        self.Owner.nextTrickshot2 = CurTime() + 0.2
        self.Owner.oldeyeang = false
    end

        return self.BaseClass.Think(self)
end