if SERVER then
   --resource.AddFile("materials/vgui/ttt_fun_killicons/mac10.png")
   AddCSLuaFile(  )
end

SWEP.HoldType = "pistol"

SWEP.PrintName = "Spray and Pray"
SWEP.Slot = 2
if CLIENT then

   
   

   SWEP.Icon = "vgui/ttt_fun_killicons/mac10.png"
end


SWEP.Base = "aa_base"

SWEP.Kind = WEAPON_HEAVY
SWEP.WeaponID = AMMO_MAC10

SWEP.Primary.Damage      = 8
SWEP.Primary.Delay       = 0.030
SWEP.Primary.Cone        = 0.045
SWEP.Primary.ClipSize    = 120
SWEP.Primary.ClipMax     = 240
SWEP.Primary.DefaultClip = 120
SWEP.Primary.Automatic   = true
SWEP.Primary.Ammo        = "smg1"
SWEP.Primary.Recoil      = 0.6
SWEP.Primary.Sound       = Sound( "Weapon_mac10.Single" )

SWEP.AutoSpawnable = false


SWEP.AmmoEnt = "item_ammo_smg1_ttt"

SWEP.UseHands			= true
SWEP.ViewModelFlip		= false
SWEP.ViewModelFOV		= 54
SWEP.ViewModel  = "models/weapons/cstrike/c_smg_mac10.mdl"
SWEP.WorldModel = "models/weapons/w_smg_mac10.mdl"

SWEP.IronSightsPos = Vector(-8.921, -9.528, 2.9)
SWEP.IronSightsAng = Vector(0.699, -5.301, -7)

SWEP.DeploySpeed = 3

function SWEP:GetHeadshotMultiplier(victim, dmginfo)
   local att = dmginfo:GetAttacker()
   if not IsValid(att) then return 2 end

   local dist = victim:GetPos():Distance(att:GetPos())
   local d = math.max(0, dist - 150)

   -- decay from 3.2 to 1.7
   return 1.7 + math.max(0, (1.5 - 0.002 * (d ^ 1.25)))
end

SWEP.flyammo = 5

function SWEP:SecondaryAttack()
self:Fly()
end