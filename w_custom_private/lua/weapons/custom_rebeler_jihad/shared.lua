if SERVER then
    AddCSLuaFile(  );
    --resource.AddFile("sound/weapons/jihad.mp3");
    --resource.AddFile("sound/weapons/big_explosion.mp3");

end

SWEP.HoldType                   = "slam"
SWEP.PrintName                       = "Rebeler Jihad"
SWEP.Slot                            = 5

if CLIENT then


    SWEP.EquipMenuData = {
        type  = "item_weapon",
        name  = "Jihad bomb",
        desc  = "Explode yourself and everything around you."
    }
    SWEP.Icon = "VGUI/ttt/icon_c4"
end



SWEP.Base = "weapon_tttbase"

SWEP.Kind = WEAPON_EQUIP


SWEP.ViewModel  = Model("models/weapons/v_c4.mdl")
SWEP.WorldModel = Model("models/weapons/w_c4.mdl")




SWEP.DrawCrosshair          = false
SWEP.ViewModelFlip          = false
SWEP.Primary.ClipSize       = -1
SWEP.Primary.DefaultClip    = -1
SWEP.Primary.Automatic      = false
SWEP.Primary.Ammo           = "none"
SWEP.Primary.Delay          = 5.0

SWEP.Secondary.ClipSize     = -1
SWEP.Secondary.DefaultClip  = -1
SWEP.Secondary.Automatic    = false
SWEP.Secondary.Ammo         = "none"

SWEP.NoSights               = true



function SWEP:Reload()
end

function SWEP:Initialize()
    util.PrecacheSound("weapons/big_explosion.mp3")
    util.PrecacheSound("weapons/jihad.mp3")
end


function SWEP:Think()
end



function SWEP:PrimaryAttack()
    self.Weapon:SetNextPrimaryFire(CurTime() + 2)

    local effectdata = EffectData()
    effectdata:SetOrigin( self.Owner:GetPos() )
    effectdata:SetNormal( self.Owner:GetPos() )
    effectdata:SetMagnitude( 16 )
    effectdata:SetScale( 1 )
    effectdata:SetRadius( 76 )
    util.Effect( "Sparks", effectdata )
    self.BaseClass.ShootEffects( self )
    self.Weapon:EmitSound( Sound("basscannon.mp3"), 450 )
    timer.Simple(3.2,function() if IsValid(self) then self.Weapon:StopSound( Sound("basscannon.mp3") ) end end)
    -- The rest is only done on the server
    if (SERVER) then
        local owner = self.Owner
        timer.Create(owner:SteamID().."jihad",3.3,1, function() if IsValid(self) then self:Asplode(owner) end end )



    end
end


function SWEP:Asplode(owner)
    local k, v
    if !ents or !SERVER then return end
    local ent = ents.Create( "env_explosion" )
    if( IsValid( owner ) ) then
        ent:SetPos( owner:GetPos() )
        ent:SetOwner( owner )
        ent:SetKeyValue( "iMagnitude", "375" )
        ent:Spawn()
        owner:Kill( )
        self:Remove()
        ent:Fire( "Explode", 0, 0 )
        ent:EmitSound( "weapons/big_explosion.mp3", 511, 100 )
    end
end



function SWEP:SecondaryAttack()
    self.Weapon:SetNextSecondaryFire( CurTime() + 1 )
    local TauntSound = Sound( "vo/npc/male01/overhere01.mp3" )
    self.Weapon:EmitSound( TauntSound )
end
