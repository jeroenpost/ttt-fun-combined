SWEP.Base = "weapon_tttbase"

SWEP.HoldType = "slam"

SWEP.ViewModel = Model("models/weapons/v_c4.mdl")
SWEP.WorldModel = Model("models/weapons/w_c4.mdl")

SWEP.Kind = WEAPON_EQUIP2
SWEP.AutoSpawnable = false

SWEP.LimitedStock = true
SWEP.PrintName = "Revenge"
SWEP.Slot = 7


SWEP.Primary.Sound			= Sound("weapons/c4/c4_beep1.wav")
SWEP.Primary.Recoil			= 0
SWEP.Primary.Damage			= 0
SWEP.Primary.NumShots		= 1
SWEP.Primary.Cone			= 0
SWEP.Primary.ClipSize		= 100
SWEP.Primary.Delay			= 15
SWEP.Primary.DefaultClip	= 100
SWEP.Primary.Automatic		= false
SWEP.Primary.Ammo			= "buckshot"

SWEP.NextAttacky = 0

function SWEP:PrimaryAttack()

    if ( not self:CanPrimaryAttack() or self.NextAttacky > CurTime()) then return end

    self.Weapon:EmitSound( self.Primary.Sound	)
    self.NextAttacky = CurTime()+ 0.5


    local tr = util.TraceLine({ start = self.Owner:EyePos(), endpos = self.Owner:EyePos() + self.Owner:EyeAngles():Forward() * 80, filter = self.Owner })
    if tr.HitNonWorld and IsValid(tr.Entity) and tr.Entity:GetClass() == "prop_ragdoll" and tr.Entity.player_ragdoll then

        self.NextAttacky = CurTime()+ 30
        local mins, maxs = self.Owner:OBBMins(), self.Owner:OBBMaxs()
        local tr2 = util.TraceHull({start = tr.Entity:LocalToWorld(tr.Entity:OBBCenter()) + vector_up, endpos = tr.Entity:LocalToWorld(tr.Entity:OBBCenter()) + Vector(0, 0, 80), filter = {tr.Entity, self.Owner}, mins = Vector(mins.x * 1.6, mins.y * 1.6, 0), maxs = Vector(maxs.x * 1.6, maxs.y * 1.6, 0), mask = MASK_PLAYERSOLID})
        if tr2.Hit then
            self.Owner:PrintMessage( HUD_PRINTCENTER, "There is no room to bring him back here" )
            return
        end


        self.NextAttacky = CurTime() + 15
        self:TakePrimaryAmmo( 1 )
        if SERVER then
            rag = tr.Entity
            local ply = player.GetByUniqueID(rag.uqid)
            local credits = CORPSE.GetCredits(rag, 0)
            if not IsValid(ply) then return end
            if SERVER and _globals['slayed'][ply:SteamID()] then self.Owner:PrintMessage( HUD_PRINTCENTER, "Can't revive, person was slayed" )
                ply:Kill();
                return
            end
            if SERVER and ((rag.was_role == ROLE_TRAITOR and self.Owner:GetRole() ~= ROLE_TRAITOR) or (rag.was_role ~= ROLE_TRAITOR and self.Owner:GetRole() == ROLE_TRAITOR))  then
                DamageLog("Defib: " .. self.Owner:Nick() .. " [" .. self.Owner:GetRoleString() .. "] Died defibbing "..ply:Nick().." [" .. ply:GetRoleString() .. "] ")
                self.Owner:PrintMessage( HUD_PRINTCENTER, ply:Nick().." was of the other team,  sucked up your soul and killed you" )
                self.Owner:Kill();

                return
            end
            DamageLog("Defib: " .. self.Owner:Nick() .. " [" .. self.Owner:GetRoleString() .. "] defibbed "..ply:Nick().." [" .. ply:GetRoleString() .. "] ")

            ply:SpawnForRound(true)
            ply:SetCredits(credits)
            ply:SetPos(rag:LocalToWorld(rag:OBBCenter()) + vector_up / 2) --Just in case he gets stuck somewhere
            ply:SetEyeAngles(Angle(0, rag:GetAngles().y, 0))
            ply:SetCollisionGroup(COLLISION_GROUP_WEAPON)
            timer.Simple(2, function() ply:SetCollisionGroup(COLLISION_GROUP_PLAYER) end)

            umsg.Start("_resurrectbody")
            umsg.Entity(ply)
            umsg.Bool(CORPSE.GetFound(rag, false))
            umsg.End()
            rag:Remove()
        end
    end
end

if CLIENT then


    SWEP.ViewModelFOV = 40
    --SWEP.ViewModelFlip = true

    SWEP.Icon = "vgui/ttt_fun_killicons/defibrillator.png"



    usermessage.Hook("_resurrectbody", function(um)
        local ply = um:ReadEntity()
        ply.was_resurrected = true
        ply.was_found = um:ReadBool()
    end)

    usermessage.Hook("_resetresurrectedbody", function(um)
        for _, p in pairs(player.GetAll()) do
            p.was_resurrected = nil
            p.was_found = nil
        end
    end)
end

if SERVER then
    --resource.AddFile("materials/vgui/ttt_fun_killicons/defibrillator.png")
    AddCSLuaFile(  )

    hook.Add("TTTEndRound", "_resetresurrectedbody", function()
        umsg.Start("_resetresurrectedbody")
        umsg.End()
    end)
end