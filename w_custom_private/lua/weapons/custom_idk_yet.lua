

SWEP.Icon = "vgui/ttt_fun_killicons/awp.png"

SWEP.Base = "aa_base"
SWEP.Category = "Other"

SWEP.AdminSpawnable = true
SWEP.Spawnable = true

SWEP.ViewModel = "models/weapons/v_snip_m24gb.mdl"
SWEP.WorldModel = "models/weapons/w_snip_m24gb.mdl"

SWEP.ViewModelFOV	= 80
SWEP.ViewModelFlip	= true

SWEP.IronSightsPos      = Vector( 5, -15, -2 )
SWEP.IronSightsAng      = Vector( 2.6, 1.37, 3.5 )
SWEP.Secondary.Sound = Sound("Default.Zoom")

SWEP.Primary.Damage = 45
SWEP.Primary.Force = 125
SWEP.Primary.BulletType = "Pistol"
SWEP.Primary.Aimcone = 0.00001

SWEP.PrintName			= "IDK YET"		-- 'Nice' Weapon name (Shown on HUD)
SWEP.Slot				= 10						-- Slot in the weapon selection menu
SWEP.SlotPos			= 0					-- Position in the slot
SWEP.Kind = 10
SWEP.Primary.Sound = Sound( "Weapon_M4A1.Silenced" )


SWEP.InfiniteMag = false
SWEP.MagazineCapacity	= 5
SWEP.Primary.ClipSize		= 300	-- Initial capacity of a magazine
SWEP.Primary.DefaultClip	= 300				-- Default number of bullets in a clip
SWEP.Primary.Delay = 0.4
SWEP.Primary.Automatic		= false				-- Automatic/Semi Auto
SWEP.Primary.Ammo			= "357"

SWEP.Secondary.ClipSize		= -1					-- Size of a clip
SWEP.Secondary.DefaultClip	= -1			-- Default number of bullets in a clip
SWEP.Secondary.Automatic	= false				-- Automatic/Semi Auto
SWEP.Secondary.Ammo			= "none"

---------------------------------------------------
if SERVER then
    AddCSLuaFile()
end

function SWEP:PrimaryAttack(worldsnd)
    if self.Owner:KeyDown(IN_USE) then
        self:ZedTime()
        return
    end


    self.Weapon:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )

    if not self:CanPrimaryAttack() then return end

    if not worldsnd then
        self.Weapon:EmitSound( self.Primary.Sound, self.Primary.SoundLevel )
    elseif SERVER then
        sound.Play(self.Primary.Sound, self:GetPos(), self.Primary.SoundLevel)
    end

    self:ShootBullet( self.Primary.Damage, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )
    self:ShootBullet( self.Primary.Damage, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )
    self:TakePrimaryAmmo( 1 )
    self:TakePrimaryAmmo( 1 )

    self:Blind()

    local owner = self.Owner
    if not IsValid(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

    owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )
end

function SWEP:PreDrop(yes)
    self:SetZoom(false)
    self:SetIronsights(false)
    if yes then  self:Remove() end
end
SWEP.nextshot = 0
function SWEP:Reload()
    if self.Owner:KeyDown(IN_USE) then

        if self.nextshot > CurTime() then return end
        self.nextshot = CurTime() + 1

        if not worldsnd then
            self.Weapon:EmitSound( self.Primary.Sound, self.Primary.SoundLevel )
        elseif SERVER then
            sound.Play(self.Primary.Sound, self:GetPos(), self.Primary.SoundLevel)
        end

        self:ShootBullet( 8, 4, 10, 0.085 )

        self:TakePrimaryAmmo( 1 )
        return
    end
    self.Weapon:DefaultReload( 1 );
    self:SetIronsights( false )
    self:SetZoom(false)
end

function SWEP:Holster()
    self:SetIronsights(false)
    self:SetZoom(false)
    return true
end

function SWEP:SecondaryAttack()
    if self.Owner:KeyDown(IN_USE) then
        self:ShootFlare()
        return
    end
    self:Zoom()
end
SWEP.IronSightsPos      = Vector( -100, -100, -100 )
SWEP.IronSightsAng      = Vector( 2.6, 1.37, 3.5 )
SWEP.Secondary.Sound = Sound("Default.Zoom")
-- Add some zoom to ironsights for this gun
function SWEP:Zoom()
    if not self.IronSightsPos then return end
    if self.Weapon:GetNextSecondaryFire() > CurTime() then return end

    local bIronsights = not self:GetIronsights()

    self:SetIronsights( bIronsights )

    if SERVER then
        self:SetZoom(bIronsights)
    else
        self:EmitSound(self.Secondary.Sound)
    end

    self.Weapon:SetNextSecondaryFire( CurTime() + 0.3)
end




function SWEP:SetZoom(state)
    if CLIENT then
        return
    elseif IsValid(self.Owner) and self.Owner:IsPlayer() then
        if state then
            self.Owner:SetFOV(30, 0.3)
        else
            self.Owner:SetFOV(0, 0.2)
        end
    end
end

if CLIENT then
    local scope = surface.GetTextureID("sprites/scope")
    function SWEP:DrawHUD()
        if self:GetIronsights() then
            surface.SetDrawColor( 0, 0, 0, 255 )

            local x = ScrW() / 2.0
            local y = ScrH() / 2.0
            local scope_size = ScrH()

            -- crosshair
            local gap = 80
            local length = scope_size
            surface.DrawLine( x - length, y, x - gap, y )
            surface.DrawLine( x + length, y, x + gap, y )
            surface.DrawLine( x, y - length, x, y - gap )
            surface.DrawLine( x, y + length, x, y + gap )

            gap = 0
            length = 50
            surface.DrawLine( x - length, y, x - gap, y )
            surface.DrawLine( x + length, y, x + gap, y )
            surface.DrawLine( x, y - length, x, y - gap )
            surface.DrawLine( x, y + length, x, y + gap )


            -- cover edges
            local sh = scope_size / 2
            local w = (x - sh) + 2
            surface.DrawRect(0, 0, w, scope_size)
            surface.DrawRect(x + sh - 2, 0, w, scope_size)

            surface.SetDrawColor(255, 0, 0, 255)
            surface.DrawLine(x, y, x + 1, y + 1)

            -- scope
            surface.SetTexture(scope)
            surface.SetDrawColor(255, 255, 255, 255)

            surface.DrawTexturedRectRotated(x, y, scope_size, scope_size, 0)

        else
            return self.BaseClass.DrawHUD(self)
        end
    end

    function SWEP:AdjustMouseSensitivity()
        return (self:GetIronsights() and 0.2) or nil
    end
end
