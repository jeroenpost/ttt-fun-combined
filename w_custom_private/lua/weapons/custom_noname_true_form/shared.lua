
if SERVER then
   AddCSLuaFile(  )
end

SWEP.HoldType = "normal"
SWEP.PrintName = "No Name's True Form"
SWEP.Slot = 3

if CLIENT then


   SWEP.Icon = "VGUI/ttt/icon_health"
end

SWEP.Base = "aa_base_fw"

SWEP.ViewModel = "models/weapons/v_pistol.mdl"
SWEP.ViewModelFOV = 54
SWEP.WorldModel = "models/weapons/w_pistol.mdl"
SWEP.ViewModelFlip = false

SWEP.DrawCrosshair      = false
SWEP.Primary.ClipSize       = -1
SWEP.Primary.DefaultClip    = -1
SWEP.Primary.Automatic      = true
SWEP.Primary.Ammo       = "none"
SWEP.Primary.Delay = 3.0

SWEP.Secondary.ClipSize     = -1
SWEP.Secondary.DefaultClip  = -1
SWEP.Secondary.Automatic    = true
SWEP.Secondary.Ammo     = "none"
SWEP.Secondary.Delay = 3.0

-- This is special equipment


SWEP.Kind = WEAPON_NADE

SWEP.LimitedStock = true -- only buyable once

SWEP.AllowDrop = true

SWEP.NoSights = true

function SWEP:OnDrop()
   self:Remove()
end

function SWEP:PrimaryAttack()
   self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
   self:HealthDrop()
end
function SWEP:Reload()
    if self.Owner:KeyDown(IN_USE) then self:FireWorkie() return end
   self.Weapon:SetNextSecondaryFire( CurTime() + self.Secondary.Delay )
   if( IsValid(self.spawnedent)) then
       self.spawnedent:ExplodeHard()
       self:Remove()
   end
end

local throwsound = Sound( "Weapon_SLAM.SatchelThrow" )

SWEP.spawnedent = false

-- ye olde droppe code
function SWEP:HealthDrop()
   if SERVER then
      local ply = self.Owner
      if not IsValid(ply) then return end

      if self.spawnedent then
        if( IsValid(self.spawnedent)) then
            self.spawnedent:Remove()
        end
      end

      local vsrc = ply:GetShootPos()
      local vang = ply:GetAimVector()
      local vvel = ply:GetVelocity()
      
      local vthrow = vvel + vang * 200

      local health = ents.Create("noname_health_station")
      if IsValid(health) then
         health:SetPos(vsrc + vang * 10)
         health:Spawn()

         health:SetPlacer(ply)
         health:SetPlacer(ply)
         health.setOwner = ply

         health:PhysWake()
         local phys = health:GetPhysicsObject()
         if IsValid(phys) then
            phys:SetVelocity(vthrow)
         end

         self.spawnedent = health
         self.Planted = true
      end
   end

   self.Weapon:EmitSound(throwsound)
end



function SWEP:OnRemove()
   if CLIENT and IsValid(self.Owner) and self.Owner == LocalPlayer() and self.Owner:Alive() then
      RunConsoleCommand("lastinv")
   end
end

if CLIENT then
   function SWEP:Initialize()
      self:AddHUDHelp("hstation_help", nil, true)

      return self.BaseClass.Initialize(self)
   end
end

function SWEP:Deploy()
   if SERVER and IsValid(self.Owner) then
      self.Owner:DrawViewModel(false)
   end
   return true
end

function SWEP:DrawWorldModel()
end

function SWEP:DrawWorldModelTranslucent()
end


SWEP.flyammo = 5
SWEP.nextFly = 0
function SWEP:SecondaryAttack()
    if self.Owner:KeyDown(IN_USE) then
        self:SwapPlayer()
        return
    end
    self:Fly()
end
