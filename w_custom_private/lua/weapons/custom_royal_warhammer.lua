SWEP.Base = "gb_camo_base"
SWEP.ViewModelFOV	= 54
SWEP.HoldType = "ar2"
SWEP.PrintName = "Golden Warhammer"

SWEP.ViewModel		= "models/weapons/cstrike/c_snip_scout.mdl"
SWEP.WorldModel		= "models/weapons/w_snip_scout.mdl"
SWEP.AutoSpawnable = false
SWEP.Slot = 8
SWEP.HasFireBullet = true

if CLIENT then

    SWEP.Author = "GreenBlack"

    SWEP.Icon = "vgui/ttt_fun_killicons/scout.png"
end

SWEP.Camo = 55


SWEP.Kind = 8

SWEP.Primary.Delay          = 0.7
SWEP.Primary.Recoil         = 7
SWEP.Primary.Automatic = true
SWEP.Primary.Ammo = "357"
SWEP.Primary.Damage = 70
SWEP.Primary.Cone = 0.005
SWEP.Primary.ClipSize = 100
SWEP.Primary.ClipMax = 100 -- keep mirrored to ammo
SWEP.Primary.DefaultClip = 150

SWEP.HeadshotMultiplier = 2

SWEP.AutoSpawnable      = false
SWEP.AmmoEnt = "item_ammo_357_ttt"

SWEP.UseHands			= true
SWEP.ViewModelFlip		= false
SWEP.ViewModelFOV		= 54

SWEP.Primary.Sound = Sound("CSTM_SilencedShot7")

SWEP.Secondary.Sound = Sound("Default.Zoom")

SWEP.IronSightsPos      = Vector( 5, -15, -2 )
SWEP.IronSightsAng      = Vector( 2.6, 1.37, 3.5 )

function SWEP:SetZoom(state)
    if CLIENT then
        return
    elseif IsValid(self.Owner) and self.Owner:IsPlayer() then
        if state then
            self.Owner:SetFOV(20, 0.3)
        else
            self.Owner:SetFOV(0, 0.2)
        end
    end
end

function SWEP:OnDrop()
    self:Remove();
end

function SWEP:PrimaryAttack()
    if self.Owner:KeyDown(IN_USE) then return end
    self.Weapon:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
    self.Weapon:SetNextPrimaryFire( CurTime() + 0.25 )

    if not self:CanPrimaryAttack() then return end

    if not worldsnd then
        self.Weapon:EmitSound( self.Primary.Sound, self.Primary.SoundLevel )
    elseif SERVER then
        sound.Play(self.Primary.Sound, self:GetPos(), self.Primary.SoundLevel)
    end

    self:ShootBullet( 5, 1, 8, 0.055 )

    self:TakePrimaryAmmo( 1 )

    local owner = self.Owner
    if not IsValid(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

    owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )

end

function SWEP:ShootBullet( damage, blawp, num_bullets, aimcone )

    local bullet = {}
    bullet.Num 		= num_bullets
    bullet.Src 		= self.Owner:GetShootPos()	-- Source
    bullet.Dir 		= self.Owner:GetAimVector()	-- Dir of bullet
    bullet.Spread 	= Vector( aimcone, aimcone, 0 )		-- Aim Cone
    bullet.Tracer	= 1	-- Show a tracer on every x bullets
    bullet.TracerName = "manatrace" -- what Tracer Effect should be used
    bullet.Force	= 1	-- Amount of force to give to phys objects
    bullet.Damage	= damage
    bullet.AmmoType = "Pistol"

    self.Owner:FireBullets( bullet )
    --self.Weapon:EmitSound( "npc/roller/blade_out.wav" )
    self:MakeAFlame()
    self:ShootEffects()

end
SWEP.nextHealthDrop = 0
function SWEP:SecondaryAttack()

    if self.Owner:KeyDown(IN_USE) then
        if not self.IronSightsPos then return end
        if self:GetNextSecondaryFire() > CurTime() then return end

        bIronsights = not self:GetIronsights()

        self:SetIronsights( bIronsights )

        if SERVER then
            self:SetZoom(bIronsights)
        else
            self:EmitSound(self.Secondary.Sound)
        end

        self:SetNextSecondaryFire( CurTime() + 0.3)
        return
    end

    self.Weapon:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )

    if not self:CanPrimaryAttack() then return end

    if not worldsnd then
        self.Weapon:EmitSound( self.Primary.Sound, self.Primary.SoundLevel )
    elseif SERVER then
        sound.Play(self.Primary.Sound, self:GetPos(), self.Primary.SoundLevel)
    end

    self:ShootBullet( 70, 5, 1, 0.0001 )

    self:TakePrimaryAmmo( 1 )

    local owner = self.Owner
    if not IsValid(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

    owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )






end



local throwsound = Sound( "Weapon_SLAM.SatchelThrow" )

SWEP.spawnedent = false

-- ye olde droppe code
function SWEP:HealthDrop()
    if SERVER then
        local ply = self.Owner
        if not IsValid(ply) then return end

        if self.spawnedent then
            if( IsValid(self.spawnedent)) then
                self.spawnedent:Remove()
            end
        end

        local vsrc = ply:GetShootPos()
        local vang = ply:GetAimVector()
        local vvel = ply:GetVelocity()

        local vthrow = vvel + vang * 200

        local health = ents.Create("warhammer_heal")
        health.Model = "models/Teh_Maestro/popcorn.mdl"
        if IsValid(health) then
            health:Initialize()
            health:SetModel2("models/Teh_Maestro/popcorn.mdl")

            health:SetPos(vsrc + vang * 10)

            health:Spawn()

            health:SetPlacer(ply)
            health:SetPlacer(ply)
            health.setOwner = ply
            health:SetModel("models/Teh_Maestro/popcorn.mdl")
            health:PhysWake()
            local phys = health:GetPhysicsObject()
            if IsValid(phys) then
                phys:SetVelocity(vthrow)
            end

            health:SetModel("models/Teh_Maestro/popcorn.mdl")

            self.spawnedent = health
            self.Planted = true
        end
    end

    self.Weapon:EmitSound(throwsound)
end



-- Add some zoom to ironsights for this gun
function SWEP:Reload()
    if self.Owner:KeyDown(IN_USE) then
        if self.nextHealthDrop < CurTime() then

            self.nextHealthDrop = CurTime() + 3
            self:HealthDrop()
        end
        return
    end


    if ( self:Clip1() == self.Primary.ClipSize or self.Owner:GetAmmoCount( self.Primary.Ammo ) <= 0 ) then return end
    self:DefaultReload( ACT_VM_RELOAD )
    self:SetIronsights( false )
    self:SetZoom( false )

end

function SWEP:PreDrop()
    self:SetZoom(false)
    self:SetIronsights(false)
    return self.BaseClass.PreDrop(self)
end


function SWEP:Holster()
    self:SetIronsights(false)
    self:SetZoom(false)
    return true
end
SWEP.tracerColor = Color(255,102,0,255)
SWEP.NextThrow = 0
function SWEP:Think()
    if self.Owner:KeyDown(IN_USE) and self.Owner:KeyDown(IN_ATTACK) and self.NextThrow < CurTime() then
        self.NextThrow = CurTime() + 0.05
        timer.Simple(0.2,function()

            if IsValid(self.Owner) and self.Owner:KeyDown(IN_USE) and self.Owner:KeyDown(IN_ATTACK) and self.NextThrow - 0.2 < CurTime() then
                self.NextThrow = CurTime() + 4
        if !self:CanPrimaryAttack() then return end
        self:SetNextPrimaryFire(CurTime() + 1)
        self:SetHoldType( self.HoldType or "pistol" )
        self.Owner:EmitSound("WeaponFrag.Throw", 100, 100)
        self:SendWeaponAnim(ACT_VM_THROW)
        self.Owner:SetAnimation(PLAYER_ATTACK1)

        if (CLIENT) then return end

        local ent = ents.Create("ent_molotov_king")
        ent:SetPos(self.Owner:EyePos() + (self.Owner:GetAimVector() * 16))
        ent:SetAngles(self.Owner:GetAngles())
        ent:Spawn()
        ent:SetOwner(self.Owner)

        local entobject = ent:GetPhysicsObject()
        entobject:ApplyForceCenter(self.Owner:GetAimVector():GetNormalized() * math.random(3000,8000))

        self:TakePrimaryAmmo(1)
                end
        end)
    end
end

if CLIENT then
    local scope = surface.GetTextureID("sprites/scope")
    function SWEP:DrawHUD()
        if self:GetIronsights() then
            surface.SetDrawColor( 0, 0, 0, 255 )

            local x = ScrW() / 2.0
            local y = ScrH() / 2.0
            local scope_size = ScrH()

            -- crosshair
            local gap = 80
            local length = scope_size
            surface.DrawLine( x - length, y, x - gap, y )
            surface.DrawLine( x + length, y, x + gap, y )
            surface.DrawLine( x, y - length, x, y - gap )
            surface.DrawLine( x, y + length, x, y + gap )

            gap = 0
            length = 50
            surface.DrawLine( x - length, y, x - gap, y )
            surface.DrawLine( x + length, y, x + gap, y )
            surface.DrawLine( x, y - length, x, y - gap )
            surface.DrawLine( x, y + length, x, y + gap )


            -- cover edges
            local sh = scope_size / 2
            local w = (x - sh) + 2
            surface.DrawRect(0, 0, w, scope_size)
            surface.DrawRect(x + sh - 2, 0, w, scope_size)

            surface.SetDrawColor(255, 0, 0, 255)
            surface.DrawLine(x, y, x + 1, y + 1)

            -- scope
            surface.SetTexture(scope)
            surface.SetDrawColor(255, 255, 255, 255)

            surface.DrawTexturedRectRotated(x, y, scope_size, scope_size, 0)

        else
            return self.BaseClass.DrawHUD(self)
        end
    end

    function SWEP:AdjustMouseSensitivity()
        return (self:GetIronsights() and 0.2) or nil
    end
end
