

if (SERVER) then

    AddCSLuaFile(  )
    SWEP.Weight                = 10
    SWEP.AutoSwitchTo        = true
    SWEP.AutoSwitchFrom        = true

end

if ( CLIENT ) then

    SWEP.DrawAmmo            = true
    SWEP.DrawCrosshair        = false
    SWEP.ViewModelFOV        = 65
    SWEP.ViewModelFlip        = false
    SWEP.CSMuzzleFlashes    = false
	SWEP.WepSelectIcon   = surface.GetTextureID("VGUI/entities/select")
	SWEP.BounceWeaponIcon = false

end

SWEP.Base = "gb_camo_base"
SWEP.Camo = 28
SWEP.CustomCamo = true
SWEP.Icon = "vgui/ttt_fun_killicons/minigun.png"

SWEP.Kind = 1024

SWEP.Slot = 10
SWEP.PrintName = "The Penguinator"
SWEP.Author = "Heavy-D"
SWEP.Spawnable = true
SWEP.Category = "Heavy-D's SWEPs"
SWEP.SlotPos = 1
SWEP.Instructions = "Shoot it"
SWEP.Contact = "Don't"
SWEP.Purpose = "Hurt people"
SWEP.HoldType = "physgun"

SWEP.ShowViewModel = true
SWEP.ShowWorldModel = true

SWEP.ViewModelFOV			= 80
SWEP.ViewModelFlip			= false
SWEP.ViewModel				= "models/weapons/v_deat_m249para.mdl"
SWEP.WorldModel				= "models/weapons/w_blopsminigun.mdl"

SWEP.MuzzleAttachment			= "1" 	-- Should be "1" for CSS models or "muzzle" for hl2 models
SWEP.ShellEjectAttachment		= "2" 	-- Should be "2" for CSS models or "1" for hl2 models

//Primary Fire Variables\\
SWEP.Primary.Sound = "weapons/ar2/fire1.wav"
SWEP.Primary.Damage = 13
SWEP.Primary.TakeAmmo = 1
SWEP.Primary.ClipSize = 400
SWEP.Primary.Ammo = "SMG1"
SWEP.Primary.DefaultClip = 3000
SWEP.Primary.Spread = 0.2
SWEP.Primary.NumberofShots = 1
SWEP.Primary.Automatic = true
SWEP.Primary.Recoil = 0.08
SWEP.Primary.Delay = 0.1
SWEP.Primary.Force = 300
SWEP.BarrelAngle = 0
SWEP.Crosshair = 0.07
//Primary Fire Variables\\

//Secondary Fire Variables\\
SWEP.Secondary.NumberofShots = 1
SWEP.Secondary.Force = 10
SWEP.Secondary.Spread = 0.1
SWEP.Secondary.DefaultClip = 32
SWEP.Secondary.Recoil = 1
SWEP.Secondary.Automatic = true
SWEP.Secondary.Delay = 0.03
SWEP.Secondary.ClipSize = 0
SWEP.Secondary.Damage = 10
//Secondary Fire Variables\\

SWEP.Primary.Sound			= Sound("weapons/deathmachine/death-1.wav")
SWEP.Primary.Sound2			= Sound("weapons/deathmachine/death_coverup.wav")

SWEP.IronSightsPos = Vector(0, 0, 0)
SWEP.IronSightsAng = Vector(0, 0, 0)
SWEP.SightsPos = Vector(0, 0, 0)
SWEP.SightsAng = Vector(0, 0, 0)
SWEP.RunSightsPos = Vector(0, 1.496, 4.645)
SWEP.RunSightsAng = Vector(-23.425, 0, 0)
SWEP.Offset = {
    Pos = {
        Up = 2,
        Right = 32,
        Forward = 0.0,
    },
    Ang = {
        Up = -10,
        Right = 0,
        Forward = 270,
    }
}
function SWEP:OnDrop()

    local wep = ents.Create(self:GetClass())
    wep:SetPos(self:GetPos()+Vector(0,100,0))
    wep:SetAngles(self:GetAngles())
    wep.IsDropped = true
    self:Remove(self)
    wep:Spawn()

end

function SWEP:SecondaryAttack()
    self:NormalPrimaryAttack()
end


function SWEP:DrawWorldModel( )
    local hand, offset, rotate

    if not IsValid( self.Owner ) then
        self:DrawModel( )
        return
    end

    if not self.Hand then
        self.Hand = self.Owner:LookupAttachment( "anim_attachment_rh" )
    end

    hand = self.Owner:GetAttachment( self.Hand )

    if not hand then
        self:DrawModel( )
        return
    end

    offset = hand.Ang:Right( ) * self.Offset.Pos.Right + hand.Ang:Forward( ) * self.Offset.Pos.Forward + hand.Ang:Up( ) * self.Offset.Pos.Up

    hand.Ang:RotateAroundAxis( hand.Ang:Right( ), self.Offset.Ang.Right )
    hand.Ang:RotateAroundAxis( hand.Ang:Forward( ), self.Offset.Ang.Forward )
    hand.Ang:RotateAroundAxis( hand.Ang:Up( ), self.Offset.Ang.Up )

    self:SetRenderOrigin( hand.Pos + offset )
    self:SetRenderAngles( hand.Ang )

    self:DrawModel( )
end

function SWEP:Initialize()

    util.PrecacheSound(self.Primary.Sound)
    self.Reloadaftershoot = 0 				-- Can't reload when firing
    self:SetHoldType("shotgun")
    self.BaseClass.Initialize(self)
end

function SWEP:Deploy()

    if game.SinglePlayer() then self.Single=true
    else
        self.Single=false
    end
    self:SetHoldType("shotgun")                          	// Hold type styles; ar2 pistol shotgun rpg normal melee grenade smg slam fist melee2 passive knife
    self:SetIronsights(false, self.Owner)					// Set the ironsight false
    self.Weapon:SendWeaponAnim( ACT_VM_DRAW )
    if !self.Owner:IsNPC() then self.ResetSights = CurTime() + self.Owner:GetViewModel():SequenceDuration() end
    return self.BaseClass.Deploy(self)
end

function SWEP:Think()

    if self.Owner:KeyReleased(IN_ATTACK) then
        self.Weapon:EmitSound( self.Primary.Sound2, self.Primary.SoundLevel )
    end


    if (self.Owner:WaterLevel() > 2) then
        self.Weapon:SetNextPrimaryFire(CurTime() + 0.5)
        return false
    end

    self.BaseClass.Think(self)

end

function SWEP:Reload()

    if self.Owner:KeyDown(IN_USE) then
        self:ShootFlare()
        return
    end

    self.Weapon:DefaultReload(ACT_VM_RELOAD)
    if !self.Owner:IsNPC() then
    self.ResetSights = CurTime() + self.Owner:GetViewModel():SequenceDuration() end



end

-- NormalPrimaryAttack
function SWEP:PrimaryAttack(worldsnd)

    self.Weapon:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )

    if not self:CanPrimaryAttack() then return end

    if not worldsnd then
        self.Weapon:EmitSound( self.Primary.Sound, self.Primary.SoundLevel )

    elseif SERVER then
        sound.Play(self.Primary.Sound, self:GetPos(), self.Primary.SoundLevel)
    end

    self:ShootBullet( self.Primary.Damage, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )

    self:TakePrimaryAmmo( 1 )




end

SWEP.NextAnim = 0
function SWEP:ShootBullet( dmg, recoil, numbul, cone )


    self.Owner:SetAnimation( PLAYER_ATTACK1 )
    self.Owner:MuzzleFlash()
    if self.NextAnim < CurTime() then

    self.Weapon:SendWeaponAnim(self.PrimaryAnim)
    self.NextAnim = CurTime() + 0.1
    end

    if not IsFirstTimePredicted() then return end

    local sights = self:GetIronsights()

    numbul = numbul or 1
    cone   = cone   or 0.01

    local bullet = {}
    bullet.Num    = numbul
    bullet.Src    = self.Owner:GetShootPos()
    bullet.Dir    = self.Owner:GetAimVector()
    bullet.Spread = Vector( cone, cone, 0 )
    bullet.Tracer = 1
    bullet.TracerName = self.Tracer or "Tracer"
    bullet.Force  = 10
    bullet.Damage = dmg
    if self.Owner:KeyDown(IN_ATTACK2) then
        bullet.Damage = 5
    end


    if SERVER and not self.Owner:KeyDown(IN_ATTACK2) then

        local ball = ents.Create("stoners_cactus");
        local tr = self.Owner:GetEyeTrace()
        if (ball) then
            ball:SetPos(tr.HitPos + self.Owner:GetAimVector() * -0 );
            ball:SetPhysicsAttacker(self.Owner)
            ball.owner = self.Owner


            ball:Spawn();

                local entz = tr.Entity
                if (entz:IsPlayer() or entz:IsNPC())  then
                    local phys = ball:GetPhysicsObject()
                    if phys and phys:IsValid() then
                        phys:SetVelocity( phys:GetVelocity() * 0 )



                        phys:EnableMotion(false)
                    ball:SetSolid(SOLID_NONE)
                    ball:SetMoveType(MOVETYPE_NONE)
                    ball:SetParent(entz)
                        timer.Create(ball:EntIndex().."explode",1,1,function()
                            ball:Explode()
                        end)
                    end

                else
                    if math.random(0,10) > 1 then
                        timer.Create(ball:EntIndex().."remove",1,1,function()
                            ball:Remove()
                        end)
                    else
                        timer.Create(ball:EntIndex().."remove",0.25,1,function()
                             ball:Explode()
                        end)
                    end
                end

            ball:Spawn();

            self.RemoteBoom= ball;
        end;
    end


    self.Owner:FireBullets( bullet )

    -- Owner can die after firebullets
    if (not IsValid(self.Owner)) or (not self.Owner:Alive()) or self.Owner:IsNPC() then return end

    if ((game.SinglePlayer() and SERVER) or
            ((not game.SinglePlayer()) and CLIENT and IsFirstTimePredicted())) then

        -- reduce recoil if ironsighting
        recoil = sights and (recoil * 0.6) or recoil

        local eyeang = self.Owner:EyeAngles()
        eyeang.pitch = eyeang.pitch - recoil
        self.Owner:SetEyeAngles( eyeang )
    end

end




local ENT = {}

ENT.Type = "anim"
ENT.Base = "base_anim"


if SERVER then

    function ENT:Initialize()
        self:SetModel("models/penguin/penguin.mdl")
        self:SetModelScale(self:GetModelScale()*1,0)
        self.Entity:PhysicsInit(SOLID_VPHYSICS);
        self.Entity:SetMoveType(MOVETYPE_VPHYSICS);
        self.Entity:SetSolid(SOLID_VPHYSICS);
        self.CanBoom = CurTime() + 3
        self.Angles = -360

        self.Entity:SetCollisionGroup(COLLISION_GROUP_WEAPON)
        local phys = self:GetPhysicsObject()
        if phys:IsValid() then
            phys:Wake()
            phys:EnableGravity(true);
        end


    end



    function ENT:Use( activator, caller )
        if self.CanBoom > CurTime() then return end
        self:EmitSound("common/bugreporter_failed.wav")
        self:Explode()
    end


    function ENT:Explode()
        if self.exploded then return end self.exploded = true
        local effect = EffectData()
        effect:SetStart(self:GetPos())
        effect:SetOrigin(self:GetPos())
        effect:SetScale(75)
        effect:SetRadius(75)
        effect:SetMagnitude(50)

        util.Effect("Explosion", effect, true, true)

        util.BlastDamage(self, self.owner, self:GetPos(), 175, 15)
        self:Remove()
    end


end

scripted_ents.Register( ENT, "stoners_cactus", true )