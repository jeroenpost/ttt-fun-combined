SWEP.Base = "gb_camo_base"
SWEP.ViewModelFOV	= 54
SWEP.HoldType = "smg"
SWEP.PrintName = "The Disorienter"
SWEP.ViewModel		= "models/weapons/cstrike/c_mach_m249para.mdl"
SWEP.WorldModel		= "models/weapons/w_mach_m249para.mdl"
SWEP.AutoSpawnable = false
SWEP.Camo = 0

SWEP.Primary.Damage = 0
SWEP.Primary.Delay = 0.01
SWEP.Primary.Cone = 0.09
SWEP.Primary.Automatic		= true
SWEP.Primary.Ammo        = "smg1"
SWEP.AutoSpawnable      = false
SWEP.Primary.Sound			= Sound("Weapon_m249.Single")
SWEP.AmmoEnt = "item_ammo_smg1_ttt"

SWEP.Primary.ClipSize = 99999
SWEP.Primary.ClipMax = 250
SWEP.Primary.DefaultClip	= 9999999
SWEP.Primary.Recoil			= 1

SWEP.HoldType = "crossbow"
SWEP.Slot = 6
SWEP.Kind = WEAPON_ROLE

SWEP.HeadshotMultiplier = 1

SWEP.IronSightsPos = Vector(-5.96, -5.119, 2.349)
SWEP.IronSightsAng = Vector(0, 0, 0)


function SWEP:PrimaryAttack()
    if math.random(0,3) == 1 then
        self:ShootBullet(5,0,0,0)
    end
    self:Disorientate()
    self.BaseClass.PrimaryAttack(self)
end

function SWEP:SecondaryAttack()
    self:Fly()
end

function SWEP:OnDrop()
    self:Remove()
end