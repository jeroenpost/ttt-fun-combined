if SERVER then
    AddCSLuaFile ("shared.lua")
    SWEP.AutoSwitchTo        = true
    SWEP.AutoSwitchFrom        = false
end
SWEP.CanDecapitate= true
SWEP.HasFireBullet = true
    SWEP.DrawCrosshair        = false
    SWEP.PrintName            = "Silent's Saxophone"

SWEP.HoldType        = "pistol"
SWEP.Author            = "Devenger, Edited by E.M.C. & ported by uacnix"
SWEP.Contact        = ""
SWEP.Purpose        = "Left Click to play sax music that will cause NPC's to explode."
SWEP.Instructions    = ""

SWEP.Spawnable                = true
SWEP.AdminSpawnable            = true

SWEP.Category       		= "uacport Sweps"

SWEP.ViewModel            = "models/weapons/c_models/c_bugle/c_bugle.mdl"
SWEP.WorldModel            = "models/weapons/c_models/c_bugle/c_bugle.mdl"
SWEP.BobScale            = 2
SWEP.SwayScale            = 2
SWEP.Base = "aa_base_fw"

SWEP.Primary.ClipSize        = -1 
SWEP.Primary.DefaultClip    = -1 
SWEP.Primary.Automatic        = true
SWEP.Primary.Ammo            = "none" 

SWEP.Secondary.ClipSize        = -1 
SWEP.Secondary.DefaultClip    = -1 
SWEP.Secondary.Automatic    = false 
SWEP.Secondary.Ammo            = "none"

--actual SWEP details
SWEP.ViewModelDefPos = Vector (-40.9885, -90.7463, -22.2584)
SWEP.ViewModelDefAng = Vector (-178.6234, 40.6472, 100.4833)

SWEP.MoveToPos = Vector (-105, -53, 20)
SWEP.MoveToAng = Vector (-230, 45, 185)

SWEP.Sound = "epicsaxguy.mp3"

SWEP.Volume = 500
SWEP.Influence = 0
SWEP.ShakesNormal = 0
SWEP.Shakes = SWEP.ShakesNormal
SWEP.TotalShakes = 0
SWEP.LastSoundRelease = 0
SWEP.TotalShakes = 0
SWEP.lastActivate = 0
SWEP.RestartDelay = 1
SWEP.RandomEffectsDelay = 0.2
SWEP.LaserColor = Color(120,120,0,255)
function SWEP:PrimaryAttack()

    self:CreateSound ()

    if self.lastActivate < (CurTime() -3) then
        self.lastActivate = CurTime()
        self.trace = self.Owner:GetEyeTrace()
        self.target = self.Owner
        self.tracepos = self.Owner:GetPos()
        self:makeThemDance()
    end



    self.Shakes = self.Shakes + 1
    self.TotalShakes = self.TotalShakes + 1



    local tr, vm, muzzle, effect
    vm = self.Owner:GetViewModel( )
    tr = { }
    tr.start = self.Owner:GetShootPos( )
    tr.filter = self.Owner
    tr.endpos = tr.start + self.Owner:GetAimVector( ) * 4096
    tr.mins = Vector( ) * -2
    tr.maxs = Vector( ) * 2
    tr = util.TraceHull( tr )


       -- if self.Shakes != self.ShakesNormal then
        effect = EffectData( )
        effect:SetStart( tr.StartPos )
        effect:SetOrigin( tr.HitPos )
        effect:SetEntity( self )
        effect:SetAttachment( vm:LookupAttachment( "muzzle" ) )
        util.Effect( "ToolTracer", effect )
       -- if self.Shakes == self.ShakesNormal - 1 then
            tr.start = self.Owner:GetShootPos( )
            effect = EffectData( )
            effect:SetStart( tr.StartPos )
            effect:SetOrigin( tr.HitPos )
            effect:SetEntity( self )
            effect:SetAttachment( vm:LookupAttachment( "muzzle" ) )
            util.Effect( "LaserTracer_thick", effect )
     --   end
            --if (false && self.Shakes <= 10 ) then
            --	local props = ents.GetAll()
            --	for _, prop in ipairs( props ) do
            --		if(prop:GetPhysicsObject():IsValid()) then
            --		prop:GetPhysicsObject():ApplyForceCenter( Vector( 0, 0, ((self.ShakesNormal*0.73) * prop:GetPhysicsObject():GetMass() ) ) )
            --	end
            --	end
            --	local Players = player.GetAll()
            --		for i = 1, table.Count(Players) do
            --		local ply = Players[i]
            --			ply:SetVelocity(Vector(0,0,50))
            --		end
            --end
       -- end


        if( tr.Entity:IsValid() and ( tr.Entity:IsPlayer() or tr.Entity:IsNPC())) then
            if SERVER then tr.Entity:Ignite(0.005)		end
            local bullet = {}
            bullet.Num 		= 2
            bullet.Src 		= self.Owner:GetShootPos()
            bullet.Dir 		= self.Owner:GetAimVector()
            bullet.Spread 	= Vector( 0, 0, 0 )
            bullet.Tracer	= 1
            bullet.Force	= 1000
            bullet.Damage	= 4
            bullet.AmmoType = "Buckshot"
            self.Owner:FireBullets( bullet )

        end
    self.Weapon:SetNextPrimaryFire(CurTime()+0.1)


end

function SWEP:OnDrop()
    self:EndSound ()
end

function SWEP:SecondaryAttack()
    if self.Owner:KeyDown(IN_RELOAD) then
        self:PowerHit();
        return
    end
    if self.Owner:KeyDown(IN_USE) then
       self:MakeExplosion()
    end
    self:FireWorkie()

end

function SWEP:GetViewModelPosition (pos, ang, inv, mul)
    local mul = 0
    if self.Weapon:GetNWBool ("on") then
        self.Volume = math.Clamp (self.Volume + FrameTime() * 3, 0, 1)
    else
        self.Volume = math.Clamp (self.Volume - FrameTime() * 3, 0, 1)
    end
    mul = self.Volume
    
    --this is always applied
    local DefPos = self.ViewModelDefPos
    local DefAng = self.ViewModelDefAng
    
    if DefAng then
        ang = ang * 1
        ang:RotateAroundAxis (ang:Right(),         DefAng.x)
        ang:RotateAroundAxis (ang:Up(),         DefAng.y)
        ang:RotateAroundAxis (ang:Forward(),     DefAng.z)
    end

    if DefPos then
        local Right     = ang:Right()
        local Up         = ang:Up()
        local Forward     = ang:Forward()
    
        pos = pos + DefPos.x * Right
        pos = pos + DefPos.y * Forward
        pos = pos + DefPos.z * Up
    end
    
    --and some more
    local AddPos = self.MoveToPos - self.ViewModelDefPos
    local AddAng = self.MoveToAng - self.ViewModelDefAng
    
    if AddAng then
        ang = ang * 1
        ang:RotateAroundAxis (ang:Right(),         AddAng.x * mul)
        ang:RotateAroundAxis (ang:Up(),         AddAng.y * mul)
        ang:RotateAroundAxis (ang:Forward(),     AddAng.z * mul)
    end

    if AddPos then
        local Right     = ang:Right()
        local Up         = ang:Up()
        local Forward     = ang:Forward()
    
        pos = pos + AddPos.x * Right * mul
        pos = pos + AddPos.y * Forward * mul
        pos = pos + AddPos.z * Up * mul
    end
    
    return pos, ang
end

SWEP.RandomEffects = {

}
SWEP.SoundObject = false
SWEP.LastFrame = false
SWEP.RestartDelay = 0
SWEP.SoundPlaying = false
SWEP.LastSoundRelease = 0
function SWEP:Think ()
    if !self.Owner:KeyDown( IN_ATTACK ) and self.Weapon:GetNWBool("on") then
      self:EndSound ()
    end
end

SWEP.NextSoundCheck = 0
function SWEP:CreateSound ()
    if not self.Weapon:GetNWBool("on") and ( not self.NextSoundCheck2 or self.NextSoundCheck2 < CurTime()) then
        self.NextSoundCheck2 = CurTime() + 1

        gb_PlaySoundFromServer(gb_config.websounds.."epicsaxguy.mp3", self.Owner,true,"sil_sax");
        self.Weapon:SetNWBool ("on", true)
        timer.Create("silSaxPlay",7.1,10,function()
            if not IsValid(self) then return end
            self.Weapon:SetNWBool ("on", true)
            gb_PlaySoundFromServer(gb_config.websounds.."epicsaxguy.mp3", self.Owner,true,"sil_sax");
        end)


    end

end

function SWEP:Holster() self:EndSound() return true end
function SWEP:OwnerChanged() self:EndSound() end

function SWEP:EndSound()
    self.Weapon:SetNWBool ("on", false)
    timer.Destroy("silSaxPlay")
    gb_StopSoundFromServer("sil_sax")
end


SWEP.NextDamageTake = 0
SWEP.danceMoves = {ACT_GMOD_TAUNT_ROBOT,ACT_GMOD_TAUNT_DANCE,ACT_GMOD_TAUNT_MUSCLE}
SWEP.lastActivate = 0
function SWEP:makeThemDance()

    if(!IsValid(self)) then return end
    local trace = self.trace
    local target = self.target
    local tracepos = self.tracepos
    danceMove = table.Random(self.danceMoves)



    if(IsValid(self) && IsValid(target)) then
    local entstoattack = ents.FindInSphere(target:GetPos(),450)
    numberOfItems = 0
    if entstoattack != nil then
    for _,v in pairs(entstoattack) do
        if(IsValid(v)) then

            v:SetColor(Color(math.random(100,255), math.random(100,255), math.random(100,255)))
            if ( v:IsPlayer() && v != self.Owner ) then
            if SERVER && (!v.LastDance or v.LastDance < CurTime() - 5) then
            v.LastDance = CurTime()
            v:AnimPerformGesture(danceMove);
            end

            if( false && SERVER && self.NextDamageTake < CurTime() ) then
            self.NextDamageTake = CurTime() + 0.1
            local dmg = DamageInfo()
            dmg:SetDamage(10)
            dmg:SetAttacker(self.Owner)
            dmg:SetInflictor(self.Weapon)
            dmg:SetDamageType(DMG_SLASH )
            v:TakeDamageInfo(dmg)
            end
            end

            if IsValid(v:GetPhysicsObject())  && numberOfItems < 20 then
            numberOfItems = numberOfItems + 1
            v:GetPhysicsObject():ApplyForceCenter( Vector( 0, 0, ((self.ShakesNormal*0.73) * v:GetPhysicsObject():GetMass() ) ) )
            v:GetPhysicsObject():AddVelocity( Vector(math.random(-100,100), math.random(-100,100), math.random(-100,100)) )
            end

        end
    end
    end
    end
end

-- COLOR
function SWEP:Deploy()

    self:SetColorAndMaterial(Color(255,255,255,255),   "camos/camo43" );
    return true
end
function SWEP:PreDrawViewModel()

    if SERVER or not IsValid(self.Owner) or not IsValid(self.Owner:GetViewModel()) then return end
    self.Owner:GetViewModel():SetColor(Color(255,255,255,255))
    self.Owner:GetViewModel():SetMaterial(  "camos/camo43"  )

end