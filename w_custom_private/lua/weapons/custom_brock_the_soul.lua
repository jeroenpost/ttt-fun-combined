SWEP.Base = "aa_base"
SWEP.ViewModelFOV	= 75
SWEP.HoldType = "physgun"
SWEP.PrintName = "Brock the Rock Soul"
SWEP.ViewModel		= "models/weapons/c_357.mdl"
SWEP.ViewModelFlip = false
SWEP.WorldModel		= "models/props_phx/misc/egg.mdl"
SWEP.Kind = WEAPON_PISTOL
SWEP.Slot = 1
SWEP.AutoSpawnable = false
SWEP.Primary.Damage		    = 35
SWEP.Primary.Delay		    = 0.15
SWEP.Primary.ClipSize		= 8
SWEP.Primary.DefaultClip	= 8
SWEP.Primary.Cone	        = 0.008
SWEP.Primary.Automatic		= true
SWEP.Primary.Ammo			= "AlyxGun"
SWEP.AmmoEnt = "item_ammo_revolver_ttt"
SWEP.Primary.Sound          = Sound("weapons/357/357_fire2.wav")
SWEP.HeadshotMultiplier = 1
SWEP.UseHands = true

SWEP.IronSightsPos = Vector( -4.6, -3, 0.65 )
SWEP.IronSightsAng = Vector( 0, 0, 0.00 )

SWEP.VElements = {
    ["element_name"] = { type = "Model", model = "models/props_phx/misc/fender.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(-2.597, 0.518, -1.558), angle = Angle(24.545, 101.688, 92.337), size = Vector(0.497, 0.497, 0.497), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
}

SWEP.WElements = {
    ["element_name"] = { type = "Model", model = "models/props_phx/misc/fender.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(11.947, 11.947, -7.792), angle = Angle(148.442, -38.571, -85.325), size = Vector(0.69, 0.69, 0.69), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
}

function SWEP:Reload()
    if self.Owner:KeyDown(IN_USE) then
    self:DropHealth("brock_rock_health")
     end
end

function SWEP:CamoPreDrawViewModel()
    if  not IsValid(self.Owner) or not IsValid(self.Owner:GetViewModel()) then return end
    self.Owner:GetViewModel():SetColor( Color(255,255,255,1) )
    self.Owner:GetViewModel():SetMaterial( "engine/occlusionproxy"  )
end

function SWEP:PreDrawViewModel()
    self:CamoPreDrawViewModel()
end

local ENT = {}

ENT.Type = "anim"
ENT.Base = "normal_health_station"

function ENT:Initialize()

    self:SetModel("models/props_phx/misc/soccerball.mdl")
    self.healsound = "items/medshot4.wav"
    self:PhysicsInit(SOLID_VPHYSICS)
    self:SetMoveType(MOVETYPE_VPHYSICS)
    self:SetSolid(SOLID_BBOX)
    self:SetCollisionGroup(COLLISION_GROUP_WEAPON)
    if SERVER then
        self:SetMaxHealth(200)

        local phys = self:GetPhysicsObject()
        if IsValid(phys) then
            phys:SetMass(200)
        end

        self:SetUseType(CONTINUOUS_USE)
    end
    self:SetHealth(200)

    self:SetColor(Color(180, 180, 250, 255))

    self:SetStoredHealth(200)

    self:SetPlacer(nil)

    self.NextHeal = 0

    self.fingerprints = {}
end
scripted_ents.Register( ENT, "brock_rock_health", true )

function SWEP:PrimaryAttack()
    if ( !self:CanSecondaryAttack() ) then return end
    local bullet = {}
    bullet.Num = self.Primary.NumberofShots
    bullet.Src = self.Owner:GetShootPos()
    bullet.Dir = self.Owner:GetAimVector()
    bullet.Spread = Vector( 0.08,0.08, 0)
    bullet.Tracer = 5
    bullet.Force = 1
    bullet.Damage = 7
    bullet.AmmoType = self.Primary.Ammo
    local rnda = self.Primary.Recoil * .8
    local rndb = self.Primary.Recoil * math.random(-1, 1)
    self:ShootEffects()
    self.Owner:FireBullets( bullet )
    self.Weapon:EmitSound(Sound(self.Primary.Sound), 500, 100, 1, 0)
    --self.Owner:ViewPunch( Angle( rnda,rndb,rnda ) )
    -- self:TakePrimaryAmmo(self.Primary.TakeAmmo)
    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
    self.Weapon:SetNextSecondaryFire( CurTime() + 0.05 )
    self:throw_attack("brock_sould_potato",10000)
end

local ENT = {}

ENT.Type = "anim"
ENT.Base = "base_anim"



if SERVER then

    function ENT:Initialize()

        self.Hit = false
        self.LastShout = CurTime()
        self.CurrentPitch = 100
        self.SpawnDelay = CurTime() + 0.5
        --damage self.Damage = 75
        self:SetModel(table.Random({"models/props_phx/misc/egg.mdl","models/props_phx/misc/potato.mdl"}))
        self:SetModelScale(1.5,0.01)
        --self:SetMaterial("phoenix_storms/plastic" )
        --self:SetColor(Color(0, 255, 0,255))
        self:PhysicsInitBox(Vector(-4,-4,-4),Vector(4,4,4))
        self:SetMoveType(MOVETYPE_VPHYSICS)
        self:SetSolid(SOLID_VPHYSICS)
        self:SetTrigger(true)
        self.Mag = 30

        self:DrawShadow(false)
        -- self:SetModel( "models/weapons/w_rif_famas.mdl")

        local phys = self:GetPhysicsObject()
        if (phys:IsValid()) then
            phys:Wake()
            phys:EnableDrag(false)
            phys:AddGameFlag(FVPHYSICS_NO_IMPACT_DMG)
            --    phys:SetBuoyancyRatio(0)
        end

    end

    function ENT:Explode()
        if self.exploded then return end self.exploded = true
        local ply = self.Owner


        local guys = ents.FindInSphere( self:GetPos()+Vector(0,0,40), 50 )
        local owner = self.Owner
        for k, guy in pairs(guys) do
            if owner != guy and (guy:IsPlayer() )  then


                if IsValid(owner) and IsValid(guy)  then
                    guy:TakeDamage( 4, owner, owner )
                    local tr = self.Owner:GetEyeTrace()
                    if SERVER then
                        local entz = guy
                        if (entz:IsPlayer() or entz:IsNPC()) and  isfunction(entz.SetEyeAngles) then
                            if (self.Owner.NextDisOrentate and self.Owner.NextDisOrentate > CurTime()) then return end
                            self.Owner.NextDisOrentate = CurTime() + 4
                            local eyeang = entz:EyeAngles()
                            local j = 20
                            eyeang.pitch = math.Clamp(eyeang.pitch + math.Rand(-j, j), -50, 50)
                            eyeang.yaw = math.Clamp(eyeang.yaw + math.Rand(-j, j), -50, 50)
                            entz:SetEyeAngles(eyeang)
                        end
                    end
                end

            end
        end

        self:EmitSound( "ambient/fire/gascan_ignite1.wav" )
        if SERVER then self:Remove() end
    end


    function ENT:Touch(ent)

        if ent != self.Owner then
        self:Explode()
        end
    end


end

scripted_ents.Register( ENT, "brock_sould_potato", true )

SWEP.nextprimary = 0
function SWEP:SecondaryAttack()
    if self.Weapon.nextprimary and  self.Weapon.nextprimary > CurTime() then
        self.Owner:PrintMessage( HUD_PRINTCENTER,  math.floor(self.Weapon.nextprimary - CurTime()).." seconds left" )

        return
    end

   -- self:DropHealth("healthdonut_brock")

        self.nextprimary = CurTime() + 90
        if SERVER then
            local ball = ents.Create("healthdonut_brock");
            self:EmitSound("Friends/friend_join.wav")
            if (ball) then
                ball:SetPos(self.Owner:GetPos() + Vector(0,0,100));
                ball.owner = self.Owner
                ball:Spawn();
                local physicsObject = ball:GetPhysicsObject();
                physicsObject:ApplyForceCenter(self.Owner:GetAimVector() * 10000);
                return ball;
            end;
        end

end




local ENT = {}

ENT.Type = "anim"
ENT.Base = "base_anim"


if SERVER then
    function ENT:SetPlacer()
    end
    function ENT:SetStoredHealth()
    end
    function ENT:Initialize()

            self.heal = 50
            self.model = "models/noesis/donut.mdl"


        self:SetModel(self.model)
        self:SetModelScale(self:GetModelScale()*1,0)
        self:SetColor(Color(128,128,128,255))
        self.Entity:PhysicsInit(SOLID_VPHYSICS);
        self.Entity:SetMoveType(MOVETYPE_VPHYSICS);
        self.Entity:SetSolid(SOLID_VPHYSICS);
        self.active = CurTime() + 2

        self.Entity:SetCollisionGroup(COLLISION_GROUP_WEAPON)
        local phys = self:GetPhysicsObject()
        if phys:IsValid() then
            phys:Wake()
            phys:EnableGravity(true);
        end
    end

    function ENT:Use( activator, caller )
        if self.active > CurTime() then return end
        self.Entity:Remove()
        if ( activator:IsPlayer() ) then
            if activator:Health() < 150 then
                activator:SetHealth(activator:Health()+self.heal)
                if activator:Health() > 150 then
                    activator:SetHealth(150)
                end
            end
            activator:EmitSound("crisps/eat.mp3")
        end
    end


end

scripted_ents.Register( ENT, "healthdonut_brock", true )
