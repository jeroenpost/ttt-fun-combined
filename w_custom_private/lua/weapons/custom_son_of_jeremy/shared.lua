if SERVER then
    AddCSLuaFile(  )
    --resource.AddFile("materials/vgui/ttt_fun_killicons/plant-disguiser.png")
end

SWEP.Base				= "aa_base"
SWEP.Kind = 6
SWEP.HoldType = "camera"
SWEP.ViewModelFlip = false
SWEP.Spawnable			= true
SWEP.AdminSpawnable		= true
SWEP.AutoSpawnable      = false 
SWEP.Icon = "vgui/ttt_fun_killicons/plant-disguiser.png"

SWEP.PrintName            = "Son of Jeremy"
SWEP.Slot                = 5
SWEP.SlotPos            = 1
SWEP.DrawAmmo            = true
SWEP.DrawCrosshair        = true
SWEP.Weight                = 5 


SWEP.ViewModel            = "models/props/de_inferno/cactus2.mdl"
SWEP.WorldModel			= "models/weapons/w_pistol.mdl"
SWEP.Primary.ClipSize        = 75
SWEP.Primary.DefaultClip    = 75
SWEP.Primary.Damage = 65
SWEP.Primary.Automatic        = false
SWEP.Primary.Ammo            = "XBowBolt"
SWEP.Primary.Delay            = 1
SWEP.Used = false 
SWEP.LastOwner = nil
SWEP.Used2 = false 
 
SWEP.VElements = {
	["element_name"] = { type = "Model", model = "models/props/de_inferno/cactus2.mdl", bone = "static_prop", rel = "", pos = Vector(16.818, 0.455, -14.091), angle = Angle(-5.114, 0, 0), size = Vector(0.3, 0.3, 0.3), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
}
SWEP.WElements = {
	["element_name"] = { type = "Model", model = "models/props/de_inferno/cactus2.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(5, 5.908, -0.456), angle = Angle(180, -7.159, 0), size = Vector(1, 1, 1), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
}


function SWEP:Initialize() 
    if SERVER then 
        self:SetHoldType( self.HoldType or "pistol" )
    end 
    self:SetNWBool("Used", false) 
    self.BaseClass.Initialize( self )
end

SWEP.activated = false
SWEP.OldModel = "models/player/phoenix.mdl"
SWEP.OldScale = 1
function SWEP:PrimaryAttack()
    
    if self:GetNWBool("Used", false) then 
        self:DryFire(self.SetNextPrimaryFire)
    return false end


    self.LastOwner = self.Owner


    if SERVER then

        if self.activated then
            self.LastOwner:SetModel( self.OldModel )
            self.activated = false
            if SERVER and not self.Used2 then
            self.LastOwner:ChatPrint("You are not disguised anymore")
            end
            timer.Destroy( "takePlantAmmo"..self.LastOwner:SteamID() );
            self.LastOwner:SetModelScale(self.OldScale, 1)
            return
        end
        self.activated = true
        self.OldModel = self.Owner:GetModel()
        self.OldScale = self.Owner:GetModelScale()
        self.LastOwner:SetModel("models/props/de_inferno/cactus2.mdl")
        self.LastOwner:SetModelScale(1, 1)

    self.Owner:ChatPrint("You are disguised as a cactus now for 60 seconds.")
    self.Owner:ChatPrint("Keep in mind that your weapon is visible!")


    end
    timer.Create("takePlantAmmo"..self.LastOwner:SteamID(),1,self:Clip1(),function() if IsValid(self) and self.activated then
        self:TakePrimaryAmmo(1)
            if self:Clip1() < 5 and IsValid(self.LastOwner) and not self.Used2  then

                 if SERVER then
                    self.LastOwner:ChatPrint("Your disguise will be gone in "..self:Clip1().." seconds")
                end
            end
            if not self:CanPrimaryAttack() and IsValid(self.LastOwner) then
                self.LastOwner:SetModel( self.OldModel )
                 self.LastOwner:SetModelScale(self.OldScale, 1)
                if SERVER and not self.Used2 then
                self.Used2 = true
                self:SetNWBool("Used", true)
                self.LastOwner:ChatPrint("You are not disguised anymore")
                end
            end
            
    end end)
    

end

function SWEP:OnDrop()
    if IsValid(self) then
        self.Used2 = true
        self:SetNWBool("Used", false)
    end
end

SWEP.nextreload = 0

function SWEP:Reload(worldsnd)
    if not self.Owner:KeyDown(IN_USE) then
          self:StabShoot()
        return
    end

    self.Weapon:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
    if self.nextreload > CurTime() or self:Clip1() < 15 then return end
    self.nextreload = CurTime() + 1
    self:ShootBullet( self.Primary.Damage, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )

    if ( self.Owner:IsValid() and self:Clip1() > 0 ) then
        local tr = self.Owner:GetEyeTrace()
        local effectdata = EffectData()
        effectdata:SetOrigin(tr.HitPos)
        effectdata:SetNormal(tr.HitNormal)
        effectdata:SetScale(1)
        -- util.Effect("effect_mad_ignition", effectdata)
        util.Decal("FadingScorch", tr.HitPos + tr.HitNormal, tr.HitPos - tr.HitNormal)

        local tracedata = {}
        tracedata.start = tr.HitPos
        tracedata.endpos = Vector(tr.HitPos.x, tr.HitPos.y, tr.HitPos.z - 10)
        tracedata.filter = tr.HitPos
        local tracedata = util.TraceLine(tracedata)
        if SERVER then

            ent = tr.Entity
            if ent:IsPlayer() or ent:IsNPC() then
                ply = ent
                --if not ply.OldRunSpeed and not ply.OldWalkSpeed then
                    ply.OldRunSpeed = 300
                    ply.OldWalkSpeed =  300
               -- end

                ply:SetNWInt("runspeed", 25)
                ply:SetNWInt("walkspeed", 25)

                timer.Create("NormalizeRunSpeedPokemonFreeze2"..math.random(0,9999999999),5,1,function()
                    if IsValid(ply) and  ply.OldRunSpeed and ply.OldWalkSpeed then
                        ply:SetNWInt("runspeed",  300)
                        ply:SetNWInt("walkspeed",300)
                        ply.OldRunSpeed = false
                        ply.OldWalkSpeed = false
                    end
                end)

            end
        end
        self:TakePrimaryAmmo( 15 )
        local owner = self.Owner
        --if not ValidEntity(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

        owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )
    end
end

SWEP.NextSecondary = 0
function SWEP:SecondaryAttack()

    if self.NextSecondary > CurTime() then
        return
    end
    self.NextSecondary = CurTime() + 1.2
    self:EmitSound(  "weapons/ump45/ump45-1.wav", 100 )

    self:ShootBullet( 35, 0.02, 1, 0.003 )

    self.Owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )

end
