SWEP.VElements = {
		["leftclaw+"] = { type = "Model", model = "models/mechanics/robotics/claw.mdl", bone = "ValveBiped.base", rel = "", pos = Vector(-1.395, -1.005, 5.638), angle = Angle(-180, 0, 179.968), size = Vector(0.086, 0.086, 0.086), color = Color(0, 0, 0, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
		["pipe1+++++"] = { type = "Model", model = "models/props_canal/mattpipe.mdl", bone = "ValveBiped.Bip01", rel = "barrel", pos = Vector(1.129, -0.058, 5.25), angle = Angle(-0.508, 180, 0.261), size = Vector(0.305, 0.305, 0.305), color = Color(0, 0, 0, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
        ["pipe1"] = { type = "Model", model = "models/props_canal/mattpipe.mdl", bone = "ValveBiped.Bip01", rel = "barrel", pos = Vector(0, -1.264, 5.144), angle = Angle(0.1, -83.37, 0), size = Vector(0.305, 0.305, 0.305), color = Color(0, 0, 0, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
        ["leftclaw"] = { type = "Model", model = "models/mechanics/robotics/claw.mdl", bone = "ValveBiped.base", rel = "", pos = Vector(1.404, -1.005, 5.638), angle = Angle(0, 0, 179.968), size = Vector(0.086, 0.086, 0.086), color = Color(0, 0, 0, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
        ["train"] = { type = "Model", model = "models/props_combine/CombineTrain01a.mdl", bone = "ValveBiped.base", rel = "", pos = Vector(0, 2.194, -4.843), angle = Angle(-90, 91.099, 0), size = Vector(0.01, 0.016, 0.01), color = Color(255, 255, 255, 254), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
        ["barrel"] = { type = "Model", model = "models/props_c17/oildrum001.mdl", bone = "ValveBiped.base", rel = "", pos = Vector(0.068, -1.114, 4.362), angle = Angle(0, 0, 0), size = Vector(0.065, 0.065, 0.065), color = Color(255, 255, 255, 255), surpresslightning = false, material = "models/Combine_Mine/combine_mine03", skin = 0, bodygroup = {} },
        ["pipe1+"] = { type = "Model", model = "models/props_canal/mattpipe.mdl", bone = "ValveBiped.Bip01", rel = "barrel", pos = Vector(-1.264, -0.132, 5.156), angle = Angle(-0.475, 1.394, 0.056), size = Vector(0.305, 0.305, 0.305), color = Color(0, 0, 0, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
        ["pipe1++++"] = { type = "Model", model = "models/props_canal/mattpipe.mdl", bone = "ValveBiped.Bip01", rel = "barrel", pos = Vector(-0.889, 0.686, 5.25), angle = Angle(-0.508, 45.93, 0.261), size = Vector(0.305, 0.305, 0.305), color = Color(0, 0, 0, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
        ["pipe1+++++++"] = { type = "Model", model = "models/props_canal/mattpipe.mdl", bone = "ValveBiped.Bip01", rel = "barrel", pos = Vector(0.885, 0.742, 5.25), angle = Angle(-0.508, 139.669, 0.261), size = Vector(0.305, 0.305, 0.305), color = Color(0, 0, 0, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
        ["pipe1++++++"] = { type = "Model", model = "models/props_canal/mattpipe.mdl", bone = "ValveBiped.Bip01", rel = "barrel", pos = Vector(-0.884, -0.988, 5.25), angle = Angle(-0.508, -45.445, 0.261), size = Vector(0.305, 0.305, 0.305), color = Color(0, 0, 0, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
        ["pipe1+++"] = { type = "Model", model = "models/props_canal/mattpipe.mdl", bone = "ValveBiped.Bip01", rel = "barrel", pos = Vector(0.836, -0.939, 5.25), angle = Angle(-0.508, -138.976, 0.261), size = Vector(0.305, 0.305, 0.305), color = Color(0, 0, 0, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
        ["pipe1++"] = { type = "Model", model = "models/props_canal/mattpipe.mdl", bone = "ValveBiped.Bip01", rel = "barrel", pos = Vector(-0.12, 1.131, 5.193), angle = Angle(-0.288, 90.207, -0.958), size = Vector(0.305, 0.305, 0.305), color = Color(0, 0, 0, 254), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
}
 
       
SWEP.WElements = {
        ["pipe1+++++++"] = { type = "Model", model = "models/props_canal/mattpipe.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "barrel", pos = Vector(0.885, 0.742, 5.25), angle = Angle(-0.508, 139.669, 0.261), size = Vector(0.305, 0.305, 0.305), color = Color(0, 0, 0, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
        ["pipe1+++++"] = { type = "Model", model = "models/props_canal/mattpipe.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "barrel", pos = Vector(1.129, -0.058, 5.25), angle = Angle(-0.508, 180, 0.261), size = Vector(0.305, 0.305, 0.305), color = Color(0, 0, 0, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
        ["leftclaw"] = { type = "Model", model = "models/mechanics/robotics/claw.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(9.55, 2.868, -6.949), angle = Angle(2.03, -88.962, 77.336), size = Vector(0.098, 0.098, 0.098), color = Color(0, 0, 0, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
        ["pipe1+"] = { type = "Model", model = "models/props_canal/mattpipe.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "barrel", pos = Vector(-1.264, -0.132, 5.156), angle = Angle(-0.475, 1.394, 0.056), size = Vector(0.305, 0.305, 0.305), color = Color(0, 0, 0, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
        ["pipe1++++++"] = { type = "Model", model = "models/props_canal/mattpipe.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "barrel", pos = Vector(-0.884, -0.988, 5.25), angle = Angle(-0.508, -45.445, 0.261), size = Vector(0.305, 0.305, 0.305), color = Color(0, 0, 0, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
        ["pipe1++"] = { type = "Model", model = "models/props_canal/mattpipe.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "barrel", pos = Vector(-0.12, 1.131, 5.193), angle = Angle(-0.288, 90.207, -0.958), size = Vector(0.305, 0.305, 0.305), color = Color(0, 0, 0, 254), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
        ["leftclaw+"] = { type = "Model", model = "models/mechanics/robotics/claw.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(9.557, -0.269, -6.987), angle = Angle(-1.081, 91.973, 104.18), size = Vector(0.098, 0.098, 0.098), color = Color(0, 0, 0, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
        ["pipe1"] = { type = "Model", model = "models/props_canal/mattpipe.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "barrel", pos = Vector(0, -1.264, 5.144), angle = Angle(0.1, -83.37, 0), size = Vector(0.305, 0.305, 0.305), color = Color(0, 0, 0, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
        ["can"] = { type = "Model", model = "models/props_junk/PopCan01a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(5.543, 1.83, -5.277), angle = Angle(-10.325, 1.25, -180), size = Vector(0.5, 0.5, 0.5), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
        ["barrel"] = { type = "Model", model = "models/props_c17/oildrum001.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "can", pos = Vector(2.663, 0.193, 1.156), angle = Angle(0, 91.168, 90), size = Vector(0.065, 0.065, 0.065), color = Color(255, 255, 255, 255), surpresslightning = false, material = "models/Combine_Mine/combine_mine03", skin = 0, bodygroup = {} },
        ["pipe1+++"] = { type = "Model", model = "models/props_canal/mattpipe.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "barrel", pos = Vector(0.836, -0.939, 5.25), angle = Angle(-0.508, -138.976, 0.261), size = Vector(0.305, 0.305, 0.305), color = Color(0, 0, 0, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
        ["pipe1++++"] = { type = "Model", model = "models/props_canal/mattpipe.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "barrel", pos = Vector(-0.889, 0.686, 5.25), angle = Angle(-0.508, 45.93, 0.261), size = Vector(0.305, 0.305, 0.305), color = Color(0, 0, 0, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
        ["train"] = { type = "Model", model = "models/props_combine/CombineTrain01a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(3.25, 1.425, -2.375), angle = Angle(167.511, 0.368, 0.919), size = Vector(0.016, 0.021, 0.016), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
}

if (SERVER) then

    AddCSLuaFile(  )
    SWEP.Weight                = 10
    SWEP.AutoSwitchTo        = true
    SWEP.AutoSwitchFrom        = true

end

if ( CLIENT ) then

    SWEP.DrawAmmo            = true
    SWEP.DrawCrosshair        = false
    SWEP.ViewModelFOV        = 65
    SWEP.ViewModelFlip        = false
    SWEP.CSMuzzleFlashes    = false
	SWEP.WepSelectIcon   = surface.GetTextureID("VGUI/entities/select")
	SWEP.BounceWeaponIcon = false            

end

SWEP.HeadshotMultiplier = 1
SWEP.Icon = "vgui/ttt_fun_killicons/minigun.png"

SWEP.Base = "aa_base"
SWEP.Kind = WEAPON_EQUIP2

SWEP.Slot = 7
SWEP.PrintName = "Clown's Fingeror"
SWEP.Author = "Heavy-D"
SWEP.Spawnable = true
SWEP.Category = "Heavy-D's SWEPs"
SWEP.SlotPos = 1
SWEP.Instructions = "Shoot it"
SWEP.Contact = "Don't"
SWEP.Purpose = "Hurt people"
SWEP.HoldType = "physgun"
SWEP.ViewModel			= "models/weapons/v_pist_finger1.mdl"
SWEP.WorldModel			= ""
SWEP.DrawWorldModel = false
SWEP.ViewModelFlip		= true
SWEP.ViewModelFOV		= 60
SWEP.ShowViewModel = true
SWEP.ShowWorldModel = false
SWEP.ViewModelBoneMods = {
	["ValveBiped.base"] = { scale = Vector(0.777, 0.777, 0.777), pos = Vector(0, 0, 0), angle = Angle(0, 0, 0) },
	["ValveBiped.clip"] = { scale = Vector(0.009, 0.009, 0.009), pos = Vector(0, 0, 0), angle = Angle(0, 0, 0) }
}
//Primary Fire Variables\\
SWEP.Primary.Sound = "weapons/ar2/fire1.wav"
SWEP.Primary.Damage = 1.4
SWEP.Primary.TakeAmmo = 1
SWEP.Primary.ClipSize = 3000
SWEP.Primary.Ammo = "SMG1"
SWEP.Primary.DefaultClip = 6000
SWEP.Primary.Spread = 0.2
SWEP.Primary.NumberofShots = 1
SWEP.Primary.Automatic = true
SWEP.Primary.Recoil = 0.8
SWEP.Primary.Delay = 0.3
SWEP.Primary.Force = 5
SWEP.BarrelAngle = 0
SWEP.Crosshair = 0.07
SWEP.Crosshair = 0.07
//Primary Fire Variables\\

//Secondary Fire Variables\\
SWEP.Secondary.NumberofShots = 1
SWEP.Secondary.Force = 10
SWEP.Secondary.Spread = 0.1
SWEP.Secondary.DefaultClip = 32
SWEP.Secondary.Recoil = 1
SWEP.Secondary.Automatic = false
SWEP.Secondary.Delay = 0.5
SWEP.Secondary.ClipSize = 0
SWEP.Secondary.Damage = 10
//Secondary Fire Variables\\

Zoom = 0

function SWEP:Initialize()

	util.PrecacheSound(self.Primary.Sound)
	
	if ( SERVER ) then
        self:SetNPCMinBurst( 20 )
        self:SetNPCMaxBurst( 50 )
        self:SetNPCFireRate( 0.07 )
    end

	
	if CLIENT then
	
		// Create a new table for every weapon instance
		self.VElements = table.FullCopy( self.VElements )
		self.WElements = table.FullCopy( self.WElements )
		self.ViewModelBoneMods = table.FullCopy( self.ViewModelBoneMods )

		self:CreateModels(self.VElements) // create viewmodels
		self:CreateModels(self.WElements) // create worldmodels
		
		// init view model bone build function
		if IsValid(self.Owner) then
		if self.Owner:IsNPC() then return end
			local vm = self.Owner:GetViewModel()
			if IsValid(vm) then
				self:ResetBonePositions(vm)
			end
			
			// Init viewmodel visibility
			if (self.ShowViewModel == nil or self.ShowViewModel) then
				--vm:SetColor(Color(255,255,255,255))
			else
				// we set the alpha to 1 instead of 0 because else ViewModelDrawn stops being called
				vm:SetColor(Color(255,255,255,1))
				// ^ stopped working in GMod 13 because you have to do Entity:SetRenderMode(1) for translucency to kick in
				// however for some reason the view model resets to render mode 0 every frame so we just apply a debug material to prevent it from drawing
				vm:SetMaterial("Debug/hsv")			
			end
		end
		
	end
	
	self.BarrelAngle = 0

end

function SWEP:DrawHUD()

	if self:GetNWBool("Reloading") == true then return end
 
        local x, y
 
        if ( self.Owner == LocalPlayer() && self.Owner:ShouldDrawLocalPlayer() ) then
 
                local tr = util.GetPlayerTrace( self.Owner )
                tr.mask = ( CONTENTS_SOLID || CONTENTS_MOVEABLE || CONTENTS_MONSTER || CONTENTS_WINDOW || CONTENTS_DEBRIS || CONTENTS_GRATE || CONTENTS_AUX )
                local trace = util.TraceLine( tr )
               
                local coords = trace.HitPos:ToScreen()
                x, y = coords.x, coords.y
 
        else
                x, y = ScrW() / 2.0, ScrH() / 2.0
        end
		

        local scale = 10*self.Crosshair

       
        
        surface.SetDrawColor( 0, 255, 0, 255 )
       
        local gap = 40 * scale
        local length = gap + 20 * scale
        surface.DrawLine( x - length, y, x - gap, y )
        surface.DrawLine( x + length, y, x + gap, y )
        surface.DrawLine( x, y - length, x, y - gap )
        surface.DrawLine( x, y + length, x, y + gap )
 
end

function SWEP:Reload()
    
    if self.Owner:KeyDown(IN_ATTACK) or self.Owner:KeyDown(IN_ATTACK2) or self.Weapon:Ammo1()==0 or self.Weapon:Clip1() >399 or self:GetNWBool("Reloading") == true then return false end
    self.Weapon:DefaultReload(ACT_VM_RELOAD)
    self:SetNWBool("Reloading", true)
	self.Weapon:EmitSound("weapons/smg1/smg1_reload.wav")
    
    timer.Simple(1.3, function()
            self:SetNWBool("Reloading", false)
    end)
end


SWEP.LastAtt = CurTime()

function SWEP:ApplyDelay()

if self.Owner:KeyDown(IN_ATTACK) then
self.Weapon:SetNWFloat( "LastShootTime", CurTime() )
local CT = CurTime()
local RecTime = CT - self.LastAtt
local RecoverScale = (1 - RecTime/0.5)
self.LastAtt = CT

	if self.Primary.Delay>0.05 then

	self.Primary.Delay = self.Primary.Delay - 0.025
	end

end

if self.Owner:KeyReleased(IN_ATTACK) then
self.Primary.Delay = 0.3
end

end
	
function SWEP:GetViewModelPosition( origin, angles )

    if self:GetNWBool("Reloading") == true or self.Weapon:Clip1() == 0 then return end
    
    
    local targetOffset = 0;
    local wiggle = VectorRand();
    wiggle:Mul( 0.0001 )

    if self.Owner:KeyDown(IN_ATTACK) then
        
        wiggle:Mul( 1000 );
        targetOffset = 1;
        
    end
    
    if self.Owner:KeyReleased(IN_ATTACK) then
        
        wiggle = vector_origin;
        
    end
    
   -- self.VMOffset = Lerp( FrameTime() * 20, self.VMOffset, targetOffset );
    
    local offset = angles:Forward();
    offset:Mul( self.VMOffset );
    offset:Add( wiggle );
    
    origin:Sub( offset );
    
    return origin, angles;
    
end

function SWEP:PrimaryAttack()
	if self:GetNWBool("Reloading") == true then return end
	if ( !self:CanPrimaryAttack() ) then return end
	self.Weapon:SendWeaponAnim(ACT_VM_PRIMARYATTACK)
	local rnda = self.Primary.Recoil * -1
	local rndb = self.Primary.Recoil * math.random(-1, 1)
	self:ShootBullet()
	self:ApplyDelay()
	self.Weapon:EmitSound(Sound(self.Primary.Sound))
	self:TakePrimaryAmmo(self.Primary.TakeAmmo)
	self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
	
	
end

function SWEP:DrawWeaponSelection(x, y, wide, tall, alpha)

    surface.SetDrawColor(255, 255, 255, alpha)
    surface.SetTexture(self.WepSelectIcon)

    local fsin = 0

    if (self.BounceWeaponIcon == true) then
        fsin = math.sin(CurTime() * 10) * 5
    end

    y = y + 10
    x = x + 10
    wide = wide - 20

    surface.DrawTexturedRect(x + (fsin), y - (fsin), wide - fsin * 2, (wide / 2) + (fsin))

    self:PrintWeaponInfo(x + wide + 20, y + tall * 0.95, alpha)
end

function SWEP:ShootBullet(damage, recoil, num_bullets, aimcone)

	num_bullets 		= 1
	
	local bullet = {}
		bullet.Num 		= num_bullets
		bullet.Src 		= self.Owner:GetShootPos()			// Source
		bullet.Dir 		= self.Owner:GetAimVector()			// Dir of bullet
		bullet.Spread 	= Vector(self.Primary.Spread * 0.1 , self.Primary.Spread * 0.1, 0)			// Aim Cone
		bullet.Tracer	= 1						// Show a tracer on every x bullets
		bullet.TracerName = "AR2Tracer"
		bullet.Force	= 10					// Amount of force to give to phys objects
		bullet.Damage	= 1


	self.Owner:FireBullets(bullet)
	
		if self.Weapon:Clip1() >0 and self.Owner:KeyDown(IN_ATTACK) and self:GetNWBool("Reloading") == false then
		self.BarrelAngle = self.BarrelAngle + 10
		end
	
	self.VElements["barrel"].angle.y = math.Approach(self.VElements["barrel"].angle.y, self.BarrelAngle, FrameTime()*200)
	

end

/*---------------------------------------------------------
   Name: SWEP:BulletPenetrate()
---------------------------------------------------------*/
function SWEP:BulletPenetrate(bouncenum, attacker, tr, dmginfo, isplayer)

	if (CLIENT) then return end
    if ( tr.HitWorld ) then return end
	
	Penetration = tr.HitPos + attacker:GetAimVector() * 30
	
	local hitMat = tr.MatType
	if hitMat == MAT_FLESH or hitMat == MAT_ALIENFLESH or hitMat == MAT_BLOODYFLESH or hitMat == MAT_ANTILON or hitMat == MAT_ANTILON then
	Penetration = tr.HitPos + attacker:GetAimVector() *15
    end

		
	// Fire bullet from the exit point using the original trajectory
	local penbullet = 
	{	
		Num 		= 1,
		Src 		= Penetration,
		Dir 		= tr.Normal,	
		Spread 	= Vector(0, 0, 0),
		Tracer	= 0,
		TracerName 	= "",
		Force		= 10,
		Damage	= 0.5,
		HullSize	= 20
		
	}
	
	 penbullet.Callback = function(attacker, tr, dmginfo) 
						
		self:BulletPenetrate(boucenum, attacker, tr, dmginfo, isplayer) 			
					  end
	
	timer.Simple(0.01, function()
		
		attacker.FireBullets(attacker, penbullet, true)
	end)

	return true
end


function SWEP:SecondaryAttack()
    self:StabShoot()
    local spos = self.Owner:GetShootPos()
    local sdest = spos + (self.Owner:GetAimVector() * 80)
    local kmins = Vector(1,1,1) * -10
    local kmaxs = Vector(1,1,1) * 10
    local tr = util.TraceHull({start=spos, endpos=sdest, filter=self.Owner, mask=MASK_SHOT_HULL, mins=kmins, maxs=kmaxs})
    if IsValid(tr.Entity) then
        self.Weapon:EmitSound(Sound("outside.mp3"))
    end


end

function SWEP:AdjustMouseSensitivity()

    if ( self.Owner:KeyDown( IN_ATTACK2 )  ) then

    return 0.1
    
end
end

function SWEP:Think()

		if self:GetNWBool("Zoomed") == true then
			self.Primary.Spread = 0.1
			self.Crosshair = 0.05
		end

		if self:GetNWBool("Zoomed") == false then 
		self.Primary.Spread = 0.2
		self.Crosshair = 0.07
		end
		
		if self.Weapon:Clip1() >0 and self.Owner:KeyDown(IN_ATTACK) then
		self.BarrelAngle = self.BarrelAngle + 10
		end
		
        if CLIENT then
                self.VElements["barrel"].angle.y = math.Approach(self.VElements["barrel"].angle.y, self.BarrelAngle, FrameTime()*150)
               
                self.WElements["barrel"].angle.p = math.Approach(self.WElements["barrel"].angle.p, self.BarrelAngle, FrameTime()*150)
        end

		
		if not self.Owner:KeyDown(IN_ATTACK) and self.Primary.Delay<0.3 then
		self.Primary.Delay = self.Primary.Delay + 0.001
		end
		
       
end


function SWEP:Holster()
	
	if CLIENT and IsValid(self.Owner) then
		local vm = self.Owner:GetViewModel()
		if IsValid(vm) then
			self:ResetBonePositions(vm)
		end
	end
	
	return true
end

if CLIENT then

	SWEP.vRenderOrder = nil
	function SWEP:ViewModelDrawn()
        if !IsValid(self.Owner) then return end
		local vm = self.Owner:GetViewModel()
		if !IsValid(vm) then return end
		
		if (!self.VElements) then return end
		
		self:UpdateBonePositions(vm)

		if (!self.vRenderOrder) then
			
			// we build a render order because sprites need to be drawn after models
			self.vRenderOrder = {}

			for k, v in pairs( self.VElements ) do
				if (v.type == "Model") then
					table.insert(self.vRenderOrder, 1, k)
				elseif (v.type == "Sprite" or v.type == "Quad") then
					table.insert(self.vRenderOrder, k)
				end
			end
			
		end

		for k, name in ipairs( self.vRenderOrder ) do
		
			local v = self.VElements[name]
			if (!v) then self.vRenderOrder = nil break end
			if (v.hide) then continue end
			
			local model = v.modelEnt
			local sprite = v.spriteMaterial
			
			if (!v.bone) then continue end
			
			local pos, ang = self:GetBoneOrientation( self.VElements, v, vm )
			
			if (!pos) then continue end
			
			if (v.type == "Model" and IsValid(model)) then

				model:SetPos(pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z )
				ang:RotateAroundAxis(ang:Up(), v.angle.y)
				ang:RotateAroundAxis(ang:Right(), v.angle.p)
				ang:RotateAroundAxis(ang:Forward(), v.angle.r)

				model:SetAngles(ang)
				//model:SetModelScale(v.size)
				local matrix = Matrix()
				matrix:Scale(v.size)
				model:EnableMatrix( "RenderMultiply", matrix )
				
				if (v.material == "") then
					model:SetMaterial("")
				elseif (model:GetMaterial() != v.material) then
					model:SetMaterial( v.material )
				end
				
				if (v.skin and v.skin != model:GetSkin()) then
					model:SetSkin(v.skin)
				end
				
				if (v.bodygroup) then
					for k, v in pairs( v.bodygroup ) do
						if (model:GetBodygroup(k) != v) then
							model:SetBodygroup(k, v)
						end
					end
				end
				
				if (v.surpresslightning) then
					render.SuppressEngineLighting(true)
				end
				
				render.SetColorModulation(v.color.r/255, v.color.g/255, v.color.b/255)
				render.SetBlend(v.color.a/255)
				model:DrawModel()
				render.SetBlend(1)
				render.SetColorModulation(1, 1, 1)
				
				if (v.surpresslightning) then
					render.SuppressEngineLighting(false)
				end
				
			elseif (v.type == "Sprite" and sprite) then
				
				local drawpos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
				render.SetMaterial(sprite)
				render.DrawSprite(drawpos, v.size.x, v.size.y, v.color)
				
			elseif (v.type == "Quad" and v.draw_func) then
				
				local drawpos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
				ang:RotateAroundAxis(ang:Up(), v.angle.y)
				ang:RotateAroundAxis(ang:Right(), v.angle.p)
				ang:RotateAroundAxis(ang:Forward(), v.angle.r)
				
				cam.Start3D2D(drawpos, ang, v.size)
					v.draw_func( self )
				cam.End3D2D()

			end
			
		end
		
	end

	SWEP.wRenderOrder = nil
	function SWEP:DrawWorldModel()
		
		if (self.ShowWorldModel == nil or self.ShowWorldModel) then
			self:DrawModel()
		end
		
		if (!self.WElements) then return end
		
		if (!self.wRenderOrder) then

			self.wRenderOrder = {}

			for k, v in pairs( self.WElements ) do
				if (v.type == "Model") then
					table.insert(self.wRenderOrder, 1, k)
				elseif (v.type == "Sprite" or v.type == "Quad") then
					table.insert(self.wRenderOrder, k)
				end
			end

		end
		
		if (IsValid(self.Owner)) then
			bone_ent = self.Owner
		else
			// when the weapon is dropped
			bone_ent = self
		end
		
		for k, name in pairs( self.wRenderOrder ) do
		
			local v = self.WElements[name]
			if (!v) then self.wRenderOrder = nil break end
			if (v.hide) then continue end
			
			local pos, ang
			
			if (v.bone) then
				pos, ang = self:GetBoneOrientation( self.WElements, v, bone_ent )
			else
				pos, ang = self:GetBoneOrientation( self.WElements, v, bone_ent, "ValveBiped.Bip01_R_Hand" )
			end
			
			if (!pos) then continue end
			
			local model = v.modelEnt
			local sprite = v.spriteMaterial
			
			if (v.type == "Model" and IsValid(model)) then

				model:SetPos(pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z )
				ang:RotateAroundAxis(ang:Up(), v.angle.y)
				ang:RotateAroundAxis(ang:Right(), v.angle.p)
				ang:RotateAroundAxis(ang:Forward(), v.angle.r)

				model:SetAngles(ang)
				//model:SetModelScale(v.size)
				local matrix = Matrix()
				matrix:Scale(v.size)
				model:EnableMatrix( "RenderMultiply", matrix )
				
				if (v.material == "") then
					model:SetMaterial("")
				elseif (model:GetMaterial() != v.material) then
					model:SetMaterial( v.material )
				end
				
				if (v.skin and v.skin != model:GetSkin()) then
					model:SetSkin(v.skin)
				end
				
				if (v.bodygroup) then
					for k, v in pairs( v.bodygroup ) do
						if (model:GetBodygroup(k) != v) then
							model:SetBodygroup(k, v)
						end
					end
				end
				
				if (v.surpresslightning) then
					render.SuppressEngineLighting(true)
				end
				
				render.SetColorModulation(v.color.r/255, v.color.g/255, v.color.b/255)
				render.SetBlend(v.color.a/255)
				model:DrawModel()
				render.SetBlend(1)
				render.SetColorModulation(1, 1, 1)
				
				if (v.surpresslightning) then
					render.SuppressEngineLighting(false)
				end
				
			elseif (v.type == "Sprite" and sprite) then
				
				local drawpos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
				render.SetMaterial(sprite)
				render.DrawSprite(drawpos, v.size.x, v.size.y, v.color)
				
			elseif (v.type == "Quad" and v.draw_func) then
				
				local drawpos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
				ang:RotateAroundAxis(ang:Up(), v.angle.y)
				ang:RotateAroundAxis(ang:Right(), v.angle.p)
				ang:RotateAroundAxis(ang:Forward(), v.angle.r)
				
				cam.Start3D2D(drawpos, ang, v.size)
					v.draw_func( self )
				cam.End3D2D()

			end
			
		end
		
	end

	function SWEP:GetBoneOrientation( basetab, tab, ent, bone_override )
		
		local bone, pos, ang
		if (tab.rel and tab.rel != "") then
			
			local v = basetab[tab.rel]
			
			if (!v) then return end
			
			// Technically, if there exists an element with the same name as a bone
			// you can get in an infinite loop. Let's just hope nobody's that stupid.
			pos, ang = self:GetBoneOrientation( basetab, v, ent )
			
			if (!pos) then return end
			
			pos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
			ang:RotateAroundAxis(ang:Up(), v.angle.y)
			ang:RotateAroundAxis(ang:Right(), v.angle.p)
			ang:RotateAroundAxis(ang:Forward(), v.angle.r)
				
		else
		
			bone = ent:LookupBone(bone_override or tab.bone)

			if (!bone) then return end
			
			pos, ang = Vector(0,0,0), Angle(0,0,0)
			local m = ent:GetBoneMatrix(bone)
			if (m) then
				pos, ang = m:GetTranslation(), m:GetAngles()
			end
			
			if (IsValid(self.Owner) and self.Owner:IsPlayer() and 
				ent == self.Owner:GetViewModel() and self.ViewModelFlip) then
				ang.r = -ang.r // Fixes mirrored models
			end
		
		end
		
		return pos, ang
	end

	function SWEP:CreateModels( tab )

		if (!tab) then return end

		// Create the clientside models here because Garry says we can't do it in the render hook
		for k, v in pairs( tab ) do
			if (v.type == "Model" and v.model and v.model != "" and (!IsValid(v.modelEnt) or v.createdModel != v.model) and 
					string.find(v.model, ".mdl") and file.Exists (v.model, "GAME") ) then
				
				v.modelEnt = ClientsideModel(v.model, RENDER_GROUP_VIEW_MODEL_OPAQUE)
				if (IsValid(v.modelEnt)) then
					v.modelEnt:SetPos(self:GetPos())
					v.modelEnt:SetAngles(self:GetAngles())
					v.modelEnt:SetParent(self)
					v.modelEnt:SetNoDraw(true)
					v.createdModel = v.model
				else
					v.modelEnt = nil
				end
				
			elseif (v.type == "Sprite" and v.sprite and v.sprite != "" and (!v.spriteMaterial or v.createdSprite != v.sprite) 
				and file.Exists ("materials/"..v.sprite..".vmt", "GAME")) then
				
				local name = v.sprite.."-"
				local params = { ["$basetexture"] = v.sprite }
				// make sure we create a unique name based on the selected options
				local tocheck = { "nocull", "additive", "vertexalpha", "vertexcolor", "ignorez" }
				for i, j in pairs( tocheck ) do
					if (v[j]) then
						params["$"..j] = 1
						name = name.."1"
					else
						name = name.."0"
					end
				end

				v.createdSprite = v.sprite
				v.spriteMaterial = CreateMaterial(name,"UnlitGeneric",params)
				
			end
		end
		
	end
	
	local allbones
	local hasGarryFixedBoneScalingYet = false

	function SWEP:UpdateBonePositions(vm)
		
		if self.ViewModelBoneMods then
			
			if (!vm:GetBoneCount()) then return end
			
			// !! WORKAROUND !! //
			// We need to check all model names :/
			local loopthrough = self.ViewModelBoneMods
			if (!hasGarryFixedBoneScalingYet) then
				allbones = {}
				for i=0, vm:GetBoneCount() do
					local bonename = vm:GetBoneName(i)
					if (self.ViewModelBoneMods[bonename]) then 
						allbones[bonename] = self.ViewModelBoneMods[bonename]
					else
						allbones[bonename] = { 
							scale = Vector(1,1,1),
							pos = Vector(0,0,0),
							angle = Angle(0,0,0)
						}
					end
				end
				
				loopthrough = allbones
			end
			// !! ----------- !! //
			
			for k, v in pairs( loopthrough ) do
				local bone = vm:LookupBone(k)
				if (!bone) then continue end
				
				// !! WORKAROUND !! //
				local s = Vector(v.scale.x,v.scale.y,v.scale.z)
				local p = Vector(v.pos.x,v.pos.y,v.pos.z)
				local ms = Vector(1,1,1)
				if (!hasGarryFixedBoneScalingYet) then
					local cur = vm:GetBoneParent(bone)
					while(cur >= 0) do
						local pscale = loopthrough[vm:GetBoneName(cur)].scale
						ms = ms * pscale
						cur = vm:GetBoneParent(cur)
					end
				end
				
				s = s * ms
				// !! ----------- !! //
				
				if vm:GetManipulateBoneScale(bone) != s then
					vm:ManipulateBoneScale( bone, s )
				end
				if vm:GetManipulateBoneAngles(bone) != v.angle then
					vm:ManipulateBoneAngles( bone, v.angle )
				end
				if vm:GetManipulateBonePosition(bone) != p then
					vm:ManipulateBonePosition( bone, p )
				end
			end
		else
			self:ResetBonePositions(vm)
		end
		   
	end
	 
	function SWEP:ResetBonePositions(vm)
		
		if (!vm:GetBoneCount()) then return end
		for i=0, vm:GetBoneCount() do
			vm:ManipulateBoneScale( i, Vector(1, 1, 1) )
			vm:ManipulateBoneAngles( i, Angle(0, 0, 0) )
			vm:ManipulateBonePosition( i, Vector(0, 0, 0) )
		end
		
	end

	/**************************
		Global utility code
	**************************/

	// Fully copies the table, meaning all tables inside this table are copied too and so on (normal table.Copy copies only their reference).
	// Does not copy entities of course, only copies their reference.
	// WARNING: do not use on tables that contain themselves somewhere down the line or you'll get an infinite loop
	function table.FullCopy( tab )

		if (!tab) then return nil end
		
		local res = {}
		for k, v in pairs( tab ) do
			if (type(v) == "table") then
				res[k] = table.FullCopy(v) // recursion ho!
			elseif (type(v) == "Vector") then
				res[k] = Vector(v.x, v.y, v.z)
			elseif (type(v) == "Angle") then
				res[k] = Angle(v.p, v.y, v.r)
			else
				res[k] = v
			end
		end
		
		return res
		
	end
	
end