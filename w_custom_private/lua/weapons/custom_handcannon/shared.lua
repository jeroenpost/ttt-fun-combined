--- - Example TTT custom weapon

-- First some standard GMod stuff
if SERVER then
    AddCSLuaFile(  )
end

if CLIENT then
    SWEP.PrintName = "The Handcannon"
    SWEP.Slot = 1 -- add 1 to get the slot number key

    SWEP.ViewModelFOV = 72
    SWEP.ViewModelFlip = false
end

-- Always derive from weapon_tttbase.
SWEP.Base = "aa_base"

--- Standard GMod values

SWEP.HoldType = "pistol"

SWEP.Primary.Delay = 1.3
SWEP.Primary.Recoil = 7
SWEP.Primary.Automatic = false
SWEP.Primary.Damage = 75
SWEP.Primary.Cone = 0.0009
SWEP.Primary.Ammo = "357"
SWEP.Primary.ClipSize = 3
SWEP.Primary.ClipMax = 500
SWEP.Primary.DefaultClip = 30
SWEP.Primary.Sound = Sound("weapons/g3sg1/g3sg1-1.wav")
SWEP.Secondary.Sound = Sound("Default.Zoom")
SWEP.HeadshotMultiplier = 3

SWEP.IronSightsPos = Vector(6.05, -5, 2.4)
SWEP.IronSightsAng = Vector(2.2, -0.1, 0)

SWEP.ViewModel = "models/weapons/v_snip_g2_hum.mdl"
SWEP.WorldModel = "models/weapons/w_snip_g2_hum.mdl"


--- TTT config values

-- Kind specifies the category this weapon is in. Players can only carry one of
-- each. Can be: WEAPON_... MELEE, PISTOL, HEAVY, NADE, CARRY, EQUIP1, EQUIP2 or ROLE.
-- Matching SWEP.Slot values: 0      1       2     3      4      6       7        8
SWEP.Kind = WEAPON_PISTOL

-- If AutoSpawnable is true and SWEP.Kind is not WEAPON_EQUIP1/2, then this gun can
-- be spawned as a random weapon. Of course this AK is special equipment so it won't,
-- but for the sake of example this is explicitly set to false anyway.
SWEP.AutoSpawnable = false

-- The AmmoEnt is the ammo entity that can be picked up when carrying this gun.
SWEP.AmmoEnt = "item_ammo_357_ttt"

-- CanBuy is a table of ROLE_* entries like ROLE_TRAITOR and ROLE_DETECTIVE. If
-- a role is in this table, those players can buy this.
-- SWEP.CanBuy = { ROLE_TRAITOR }

-- InLoadoutFor is a table of ROLE_* entries that specifies which roles should
-- receive this weapon as soon as the round starts. In this case, none.
SWEP.InLoadoutFor = nil

-- If LimitedStock is true, you can only buy one per round.
-- SWEP.LimitedStock = true

-- If AllowDrop is false, players can't manually drop the gun with Q
SWEP.AllowDrop = true

-- If IsSilent is true, victims will not scream upon death.
SWEP.IsSilent = false

-- If NoSights is true, the weapon won't have ironsights
SWEP.NoSights = false

-- Equipment menu information is only needed on the client
if CLIENT then
    -- Path to the icon material
    SWEP.Icon = "vgui/ttt_fun_killicons/thomson_g2.png"

    -- Text shown in the equip menu
    SWEP.EquipMenuData = {
        type = "Thompson G2 Contender Handheld Rifle",
        desc = "Description"
    };
end

SWEP.nextfiree = 0
SWEP.Primary.Round 			= ("ent_atr_shell")
function SWEP:PrimaryAttack(worldsnd)

    if self.nextfiree > CurTime() then return end
    local mode = self:GetNWString("shootmode")
    if mode == "Anti-Tank" then
        self.nextfiree = CurTime() + 1.5
        if SERVER then
            local tr = self.Owner:GetEyeTrace()
            local ent = ents.Create("env_explosion")

            ent:SetPos(tr.HitPos)
            ent:SetOwner(self.Owner)
            ent:SetPhysicsAttacker(self.Owner)
            ent:Spawn()
            ent:SetKeyValue("iMagnitude", "15")
            ent:Fire("Explode", 0, 0)

            util.BlastDamage(self, self:GetOwner(), self:GetPos(), 20, 85)

            ent:EmitSound("weapons/big_explosion.mp3")
        end

        if self:Clip1() > 0 then
            if not worldsnd then
                self.Weapon:EmitSound(self.Primary.Sound, self.Primary.SoundLevel)
            elseif SERVER then
                sound.Play(self.Primary.Sound, self:GetPos(), self.Primary.SoundLevel)
            end
            self:TakePrimaryAmmo(1)
            self:ShootBullet(self.Primary.Damage, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone())
        else
            self.nextfiree = CurTime() + 0.8
        end
        self.Owner:SetAnimation(PLAYER_ATTACK1)
        local owner = self.Owner
        if not IsValid(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

        owner:ViewPunch(Angle(math.Rand(-0.2, -0.1) * self.Primary.Recoil, math.Rand(-0.1, 0.1) * self.Primary.Recoil, 0))
    end
    if mode == "Anti-Material" then
        self.nextfiree = CurTime() + 1.5
        if SERVER then
            local tr = self.Owner:GetEyeTrace()
            local ent = ents.Create("env_explosion")

            ent:SetPos(tr.HitPos)
            ent:SetOwner(self.Owner)
            ent:SetPhysicsAttacker(self.Owner)
            ent:Spawn()
            ent:SetKeyValue("iMagnitude", "15")
            ent:Fire("Explode", 0, 0)

            util.BlastDamage(self, self:GetOwner(), self:GetPos(), 20, 85)

            ent:EmitSound("weapons/big_explosion.mp3")
        end

        if self:Clip1() > 0 then
            if not worldsnd then
                self.Weapon:EmitSound(self.Primary.Sound, self.Primary.SoundLevel)
            elseif SERVER then
                sound.Play(self.Primary.Sound, self:GetPos(), self.Primary.SoundLevel)
            end
            self:TakePrimaryAmmo(2)
            self:ShootBullet(self.Primary.Damage/3, self.Primary.Recoil+5, self.Primary.NumShots+2, 0.04)
        else
            self.nextfiree = CurTime() + 0.8
        end
        self.Owner:SetAnimation(PLAYER_ATTACK1)
        local owner = self.Owner
        if not IsValid(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

        owner:ViewPunch(Angle(math.Rand(-0.2, -0.1) * self.Primary.Recoil, math.Rand(-0.1, 0.1) * self.Primary.Recoil, 0))
    end
    if mode == "Mortar" then
        self.nextfiree = CurTime() + 5
        self:FireRocket()
        self:TakePrimaryAmmo(1)
        self:SendWeaponAnim(ACT_VM_PRIMARYATTACK)
        self.Owner:SetAnimation( PLAYER_ATTACK1 )
        self:EmitSound( "weapons/mortar/mortar_explode2.wav", 500, 80, 1, 0 )
        self:EmitSound( "weapons/mortar/mortar", 500, 80, 1, -1 )
        self.Weapon:SetNWFloat( "LastShootTime", CurTime() )
    end
end

function SWEP:FireRocket()
    local aim = self.Owner:GetAimVector()
    local side = aim:Cross(Vector(0,0,1))
    local up = side:Cross(aim)
    local pos = self.Owner:GetShootPos() + side * 10 + up * -3

    if SERVER then
        local rocket = ents.Create(self.Primary.Round)
        if !rocket:IsValid() then return false end
        rocket:SetAngles(self.Owner:GetAimVector():Angle(90,90,0))
        rocket:SetPos(pos)
        rocket:SetOwner(self.Owner)
        rocket:Spawn()
        rocket.Owner = self.Owner
        rocket:Activate()
        eyes = self.Owner:EyeAngles()
        local phys = rocket:GetPhysicsObject()
        phys:SetVelocity(self.Owner:GetAimVector() * 7000)
    end
    if SERVER and !self.Owner:IsNPC() then
    local anglo = Angle(-3, 0, 0)
    self.Owner:ViewPunch(anglo)
    end

end


function SWEP:SetZoom(state)
    if CLIENT then
        return
    elseif IsValid(self.Owner) and self.Owner:IsPlayer() then
        if state then
            self.Owner:SetFOV(20, 0.3)
        else
            self.Owner:SetFOV(0, 0.2)
        end
    end
end

function SWEP:SecondaryAttack()
    if not self.IronSightsPos or self.Owner:KeyDown(IN_USE) then return end
    if self.Weapon:GetNextSecondaryFire() > CurTime() then return end

    bIronsights = not self:GetIronsights()

    self:SetIronsights(bIronsights)

    if SERVER then
        self:SetZoom(bIronsights)
    else
        self:EmitSound(self.Secondary.Sound)
    end

    self.Weapon:SetNextSecondaryFire(CurTime() + 0.3)
end

function SWEP:PreDrop()
    self:SetZoom(false)
    self:SetIronsights(false)
    return self.BaseClass.PreDrop(self)
end

SWEP.NextReload = 1
function SWEP:Reload()
    if self.Owner:KeyDown(IN_DUCK) then
        if self.NextReload < CurTime() then
            self.NextReload = CurTime() +0.5
            self:TripMineStick()
        end
        return
    end
    if self.Owner:KeyDown(IN_USE) then return end
    self.Weapon:DefaultReload(ACT_VM_RELOAD);
    self:SetIronsights(false)
    self:SetZoom(false)
end


function SWEP:Holster()
    self:SetIronsights(false)
    self:SetZoom(false)
    return true
end

SWEP.TracerColor = Color(255,255,255,255)
function SWEP:SuperShot()

    if SERVER then
        local tr = self.Owner:GetEyeTrace()
        local ent = ents.Create("env_explosion")

        ent:SetPos(tr.HitPos)
        ent:SetOwner(self.Owner)
        ent:SetPhysicsAttacker(self.Owner)
        ent:Spawn()
        ent:SetKeyValue("iMagnitude", "180")
        ent:Fire("Explode", 0, 0)

        util.BlastDamage(self, self:GetOwner(), self:GetPos(), 60, 75)

        ent:EmitSound("weapons/big_explosion.mp3")
    end
    self:FireBolt()
    self:ShootTracer( "LaserTracer_thick")
end

-- CHARGING
SWEP.IsHolding = false
SWEP.LastHold = 0
SWEP.Sound = false
SWEP.NextReload = 0
function SWEP:Think()
    if self.Owner:KeyDown(IN_WALK) then
        self:Fly()
        return
    end
    if self.Owner:KeyDown(IN_USE) and self.Owner:KeyDown(IN_DUCK) and self.NextReload < CurTime() then
        self.NextReload = CurTime() + 0.3
        local mode = self:GetNWString("shootmode")
        self:SetIronsights(false)

        if mode == "Anti-Tank" then  self:SetNWString("shootmode","Anti-Material")  end
        if mode == "Anti-Material" then  self:SetNWString("shootmode","Mortar") end
        if mode == "Mortar" then  self:SetNWString("shootmode","Anti-Tank") end

        self.Weapon:SetNextPrimaryFire( CurTime() + 0.8 )
        return
    end
    if self.Owner:KeyDown(IN_USE) and self.Owner:KeyDown(IN_RELOAD) then

        if not self.IsHolding then
            self.LastHold = CurTime()
            self.IsHolding = true
            self.Sound = CreateSound( self,"weapons/gauss/chargeloop.wav");
            self.Sound:Play();
            self.Sound:ChangePitch(250, 4.5)
        end

        if self.IsHolding and self.LastHold + 5 < CurTime() then
            self:SuperShot()
            self.IsHolding = false
        end
    elseif self.IsHolding then
        self.IsHolding = false
        self.Sound:Stop();
        self.Sound = false
    end
end


SWEP.hadtripmines = 0
function SWEP:TripMineStick()
    if SERVER then
        local ply = self.Owner
        if not IsValid(ply) then return end


        local ignore = {ply, self.Weapon}
        local spos = ply:GetShootPos()
        local epos = spos + ply:GetAimVector() * 80
        local tr = util.TraceLine({start=spos, endpos=epos, filter=ignore, mask=MASK_SOLID})

        if tr.HitWorld and self.hadtripmines < 3 then

            local mine = ents.Create("npc_tripmine")
            if IsValid(mine) then


                local tr_ent = util.TraceEntity({start=spos, endpos=epos, filter=ignore, mask=MASK_SOLID}, mine)

                if tr_ent.HitWorld then

                    local ang = tr_ent.HitNormal:Angle()
                    ang.p = ang.p + 90

                    mine:SetPos(tr_ent.HitPos + (tr_ent.HitNormal * 3))
                    mine:SetAngles(ang)
                    mine:SetPhysicsAttacker(ply)
                    mine:SetOwner(ply)
                    mine:Spawn()

                    -- mine:SetHealth( 10 )

                    function mine:OnTakeDamage(dmg)
                        self:Remove()
                    end

                    -- mine:Ignite(100)
                    --  print( mine )
                    if SERVER then DamageLog("Tripmine: " .. self.Owner:Nick() .. " [" .. self.Owner:GetRoleString() .. "] placed a tripmine ")
                    end

                    mine.fingerprints = self.fingerprints

                    self:SendWeaponAnim( ACT_SLAM_TRIPMINE_ATTACH )

                    local holdup = self.Owner:GetViewModel():SequenceDuration()

                    timer.Simple(holdup,
                        function()
                            if SERVER then
                                self:SendWeaponAnim( ACT_SLAM_TRIPMINE_ATTACH2 )
                            end
                        end)

                    timer.Simple(holdup + .1,
                        function()
                            if SERVER then
                                if self.Owner == nil then return end
                                if self.Weapon:Clip1() == 0 && self.Owner:GetAmmoCount( self.Weapon:GetPrimaryAmmoType() ) == 0 then
                                --self.Owner:StripWeapon(self.Gun)
                                --RunConsoleCommand("lastinv")
                                self:Remove()
                                else
                                    self:Deploy()
                                end
                            end
                        end)


                    --self:Remove()
                    self.Planted = true

                    self.hadtripmines = self.hadtripmines + 1

                    if not self.Owner:IsTraitor() then
                        self.Owner:PrintMessage( HUD_PRINTCENTER, "You lost 75 health for placing a tripmine as inno" )
                        self.Owner:SetHealth( self.Owner:Health() - 75)
                    end

                end
            end
        end
    end
end

function SWEP:Deploy()
    self:SetNWString("shootmode","Anti-Tank")
    return self.BaseClass.Deploy(self)
end

if CLIENT then
    local scope = surface.GetTextureID("sprites/scope")
    function SWEP:DrawHUD( )
        local shotttext = "Shootmode: "..self:GetNWString("shootmode").."\nE+CTRL to change"
        surface.SetFont( "ChatFont" );
        local size_x, size_y = surface.GetTextSize( shotttext );

        draw.RoundedBox( 6, ScrW( ) / 2 - size_x / 2 - 5, ScrH( ) - 100, size_x + 15, size_y + 10, Color( 0, 0, 0, 150 ) );
        draw.DrawText( shotttext , "ChatFont", ScrW( ) / 2, ScrH( ) - 100 + 5, Color( 255, 30, 30, 255 ), TEXT_ALIGN_CENTER );


        if self:GetIronsights() then
            surface.SetDrawColor(0, 0, 0, 255)

            local x = ScrW() / 2.0
            local y = ScrH() / 2.0
            local scope_size = ScrH()

            -- crosshair
            local gap = 80
            local length = scope_size
            surface.DrawLine(x - length, y, x - gap, y)
            surface.DrawLine(x + length, y, x + gap, y)
            surface.DrawLine(x, y - length, x, y - gap)
            surface.DrawLine(x, y + length, x, y + gap)

            gap = 0
            length = 50
            surface.DrawLine(x - length, y, x - gap, y)
            surface.DrawLine(x + length, y, x + gap, y)
            surface.DrawLine(x, y - length, x, y - gap)
            surface.DrawLine(x, y + length, x, y + gap)


            -- cover edges
            local sh = scope_size / 2
            local w = (x - sh) + 2
            surface.DrawRect(0, 0, w, scope_size)
            surface.DrawRect(x + sh - 2, 0, w, scope_size)

            surface.SetDrawColor(255, 0, 0, 255)
            surface.DrawLine(x, y, x + 1, y + 1)

            -- scope
            surface.SetTexture(scope)
            surface.SetDrawColor(255, 255, 255, 255)

            surface.DrawTexturedRectRotated(x, y, scope_size, scope_size, 0)

        else
            return self.BaseClass.DrawHUD(self)
        end
    end

    function SWEP:AdjustMouseSensitivity()
        return (self:GetIronsights() and 0.2) or nil
    end
end

