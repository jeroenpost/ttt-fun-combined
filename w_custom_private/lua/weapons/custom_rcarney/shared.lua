if SERVER then
   AddCSLuaFile(  )
   --resource.AddFile("materials/vgui/ttt_fun_killicons/mp5.png")
end

SWEP.HoldType = "ar2"
SWEP.PrintName = "Quad's Rigged MP5"
SWEP.Slot = 3

if CLIENT then
   			
   SWEP.Author = "GreenBlack"
   
   SWEP.Icon = "vgui/ttt_fun_killicons/mp5.png"
end

SWEP.Base = "weapon_tttbase"
SWEP.Spawnable = true
SWEP.AdminSpawnable = true
SWEP.Kind = WEAPON_NADE

SWEP.Primary.Delay = 0.06
SWEP.Primary.Recoil = 0
SWEP.Primary.Automatic = true
SWEP.Primary.Ammo = "pistol"
SWEP.Primary.Damage = 9
SWEP.Primary.Cone = 0.028
SWEP.Primary.ClipSize = 120
SWEP.Primary.ClipMax = 180
SWEP.Primary.DefaultClip = 120
SWEP.AutoSpawnable = false
SWEP.AmmoEnt = "item_ammo_pistol_ttt"
SWEP.ViewModel = "models/weapons/v_smg_mp5.mdl"
SWEP.WorldModel = "models/weapons/w_smg_mp5.mdl"
SWEP.Primary.Sound = Sound("Weapon_MP5Navy.Single")

SWEP.HeadshotMultiplier = 1.2

SWEP.IronSightsPos = Vector(4.7659, -3.0823, 1.8818)
SWEP.IronSightsAng = Vector(0.9641, 0.0252, 0)