if ( SERVER ) then
   -- AddCSLuaFile(  )
end
SWEP.Kind = WEAPON_HEAVY
SWEP.PsID = "cm_scout"
SWEP.BulletLength = 8.58
SWEP.CaseLength = 69.20

SWEP.MuzVel = 668.633

SWEP.Attachments = {
    [1] = {"kobra",},
    [2] = {"bipod"},
    [4] = {"laser"} }

SWEP.InternalParts = {
    [1] = {{key = "hbar"}, {key = "lbar"}},
    [2] = {{key = "hframe"}},
    [3] = {{key = "ergonomichandle"}},
    [4] = {{key = "customstock"}},
    [5] = {{key = "lightbolt"}, {key = "heavybolt"}},
    [6] = {{key = "gasdir"}}}

if ( CLIENT ) then

    SWEP.PrintName			= "Professional Hacker"
    SWEP.Author				= "Counter-Strike"
    SWEP.Slot				= 5
    SWEP.SlotPos = 0 //			= 1
    SWEP.IconLetter			= "r"
    SWEP.Muzzle = "cstm_muzzle_br"
    SWEP.SparkEffect = "cstm_child_sparks_large"
    SWEP.SmokeEffect = "cstm_child_smoke_large"
    SWEP.BoneApproachSpeed = 0.8
    SWEP.SemiText = "Bolt-action"
    SWEP.ViewMoveWhenFiring = true
    SWEP.PitchMod = 0.5
    SWEP.RollMod = 0.5

    killicon.AddFont( "cstm_sniper_awp", "CSKillIcons", SWEP.IconLetter, Color( 255, 80, 0, 255 ) )

    SWEP.VElements = {
        ["laser"] = { type = "Model", model = "models/props_c17/FurnitureBoiler001a.mdl", bone = "v_weapon.scout_Parent", rel = "", pos = Vector(-0.165, 1.307, 14.847), angle = Angle(0, 0, 180), size = Vector(0.028, 0.028, 0.028), color = Color(0, 0, 0, 0), surpresslightning = false, material = "models/bunneh/silencer", skin = 0, bodygroup = {} },
        --["kobra"] = { type = "Model", model = "models/attachments/cmore.mdl", bone = "v_weapon.scout_Parent", rel = "", pos = Vector(-0.013, 3.148, 6.651), angle = Angle(180, 0, 90), size = Vector(0.75, 0.75, 0.75), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
        ["kobra"] = { type = "Model", model = "models/bunneh/scope01.mdl", bone = "v_weapon.scout_Parent", rel = "", pos = Vector(1.258, 4.701, -0.291), angle = Angle(90, 90, 0), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "camos/camo52", skin = 0, bodygroup = {} },
        ["ballistic2"] = { type = "Model", model = "models/bunneh/scope01.mdl", bone = "v_weapon.scout_Parent", rel = "", pos = Vector(1.258, 6.301, -0.291), angle = Angle(90, 90, 0), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "camos/camo53", skin = 0, bodygroup = {} },
        ["eotech2"] = { type = "Model", model = "models/wystan/attachments/2otech557sight.mdl", bone = "v_weapon.scout_Parent", rel = "", pos = Vector(-0.341, -3.2, -5.237), angle = Angle(91.023, 0.972, 90.507), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "camos/camo50", skin = 0, bodygroup = {} },
        ["ballistic3"] = { type = "Model", model = "models/bunneh/scope01.mdl", bone = "v_weapon.scout_Parent", rel = "", pos = Vector(2.258, 5.301, -0.291), angle = Angle(90, 120, 0), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "camos/camo42", skin = 0, bodygroup = {} },
        ["ballistic4"] = { type = "Model", model = "models/bunneh/scope01.mdl", bone = "v_weapon.scout_Parent", rel = "", pos = Vector(2.258, 3.801, -0.291), angle = Angle(90, 120, 0), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "camos/camo43", skin = 0, bodygroup = {} },
        ["ballistic5"] = { type = "Model", model = "models/bunneh/scope01.mdl", bone = "v_weapon.scout_Parent", rel = "", pos = Vector(2.258, 2.501, -0.291), angle = Angle(90, 120, 0), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "camos/camo25", skin = 0, bodygroup = {} },

        ["bipod"] = { type = "Model", model = "models/wystan/attachments/bipod.mdl", bone = "v_weapon.scout_Parent", rel = "", pos = Vector(0.165, 0.524, 12.399), angle = Angle(0, 0, 93.293), size = Vector(1, 1, 1), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {[1] = 1} },

        ["silencer"] = { type = "Model", model = "models/props_c17/oildrum001.mdl", bone = "v_weapon.scout_Parent", rel = "", pos = Vector(-0.068, 2.601, 19.705), angle = Angle(0, 0, 0), size = Vector(0.03, 0.03, 0.081), color = Color(0, 0, 0, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
    }

    SWEP.WElements = {
        ["ballistic"] = { type = "Model", model = "models/bunneh/scope01.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(-0.983, 0.134, -5.087), angle = Angle(-6.032, 2.755, 180), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "camos/camo52", skin = 0, bodygroup = {} },

        ["silencer"] = { type = "Model", model = "models/props_c17/oildrum001.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(31.768, 0.767, -8.768), angle = Angle(-100.389, 0, 0), size = Vector(0.059, 0.059, 0.25), color = Color(255, 255, 255, 1), surpresslightning = false, material = "models/bunneh/silencer", skin = 0, bodygroup = {} }
    }
end

SWEP.Category = "Customizable Weaponry"
SWEP.HoldType			= "ar2"
SWEP.Base				= "cstm_base_sniper"
SWEP.FireModes = {"bolt"}

SWEP.Spawnable			= true
SWEP.AdminSpawnable		= true

SWEP.ViewModelFOV = 65
SWEP.ViewModelFlip = true
SWEP.ViewModel = "models/weapons/v_snip_scout.mdl"
SWEP.WorldModel = "models/weapons/w_snip_scout.mdl"
SWEP.ShowViewModel = true
SWEP.ShowWorldModel = true
SWEP.ViewModelBonescales = {}


SWEP.KobraPos = Vector(5, -4.789, 2.079)
SWEP.KobraAng = Vector(-0.042, 0, 0)

SWEP.Weight				= 5
SWEP.AutoSwitchTo		= false
SWEP.AutoSwitchFrom		= false

SWEP.Primary.Sound			= Sound("Weapon_Scout.Single")
SWEP.Primary.Recoil			= 0.3
SWEP.Primary.Damage			= 85
SWEP.Primary.NumShots		= 1
SWEP.Primary.Cone			= 0.02
SWEP.Primary.ClipSize		=  25
SWEP.Primary.Delay			= 1.5
SWEP.Primary.DefaultClip	= 10
SWEP.Primary.Automatic		= true
SWEP.Primary.Ammo			= "7.62x51MM"
SWEP.InitialHoldtype = "smg"
SWEP.InHoldtype = "smg"
SWEP.CantSilence = false
SWEP.HeadshotMultiplier = 2.5
SWEP.DrawCrosshair = true
-- Animation speed/custom reload function related
SWEP.IsReloading = false
SWEP.NoBoltAnim = true
SWEP.AnimPrefix = ""
SWEP.ReloadSpeed = 1
SWEP.ShouldBolt = false
SWEP.ReloadDelay = 0
SWEP.IncAmmoPerc = 0.55 -- Amount of frames required to pass (in percentage) of the reload animation for the weapon to have it's amount of ammo increased

-- Dynamic accuracy related
SWEP.ShotsAmount 			= 0
SWEP.ConeDecAff				= 0
SWEP.DefRecoil				= 1.75
SWEP.DefCone				= 0.0001
SWEP.DecreaseRecoilTime 	= 0
SWEP.ConeAff1 				= 0 -- Crouching/standing
SWEP.ConeAff2 				= 0 -- Using ironsights

SWEP.UnConeTime				= 0 -- Amount of time after firing the last shot that needs to pass until accuracy increases
SWEP.FinalCone				= 0 -- Self explanatory
SWEP.VelocitySensivity		= 3 -- Percentage of how much the cone increases depending on the player's velocity (moving speed). Rifles - 100%; SMGs - 80%; Pistols - 60%; Shotguns - 20%
SWEP.HeadbobMul 			= 1
SWEP.IsSilenced 			= false
SWEP.AltSilencedSound 		= true
SWEP.NoAuto = true
SWEP.IronsightsCone			= 0.0001
SWEP.HipCone = 0.06

SWEP.Secondary.Automatic	= false
SWEP.Secondary.Ammo			= "none"

SWEP.IsUsingIronsights 		= false
SWEP.TargetMul = 0
SWEP.SetAndForget			= false

SWEP.BallisticPos = Vector(5, -2.98, 2.121)
SWEP.BallisticAng = Vector(0, 0, 0)
SWEP.IronSightsPos = Vector(6.0749, -5.5216, 2.3984)
SWEP.IronSightsAng = Vector(2.5174, -0.0099, 0)

SWEP.AimPos = Vector(5, -9.056, 2.523)
SWEP.AimAng = Vector(0, 0, 0)

SWEP.ChargePos = Vector (5.4056, -10.3522, -4.0017)
SWEP.ChargeAng = Vector (-1.7505, -55.5187, 68.8356)

function SWEP:Deploy()
    local function playerDiesSound( victim, weapon, killer )
        if( IsValid(killer) and killer:IsPlayer()  ) then
            local wep = killer:GetActiveWeapon()
            if IsValid(wep) then
                wep = wep:GetClass()
                if wep == "cstm_professional_sniper" then
                    gb_PlaySoundFromServer(gb_config.websounds.."pro_snip1.mp3", killer)
                end
            end
        end
    end

    hook.Add( "PlayerDeath", "playerDeathSound_prosniper", playerDiesSound )
    return self.BaseClass.Deploy(self)

end

SWEP.NextReload = 0
function SWEP:Reload()
    if self.NextReload < CurTime() then
        self.NextReload = CurTime() + 10
        gb_PlaySoundFromServer(gb_config.websounds.."pro_snip2.mp3", self.Owner)
    end

    return self.BaseClass.Reload(self)

end

function SWEP:PreDrawViewModel()
    if self.VElements["kobra"].color.a ==  255 then
    self.VElements["ballistic2"].color = Color(255, 255, 255, 255)
    self.VElements["ballistic3"].color = Color(255, 255, 255, 255)
    self.VElements["ballistic4"].color = Color(255, 255, 255, 255)
    self.VElements["ballistic5"].color = Color(255, 255, 255, 255)
    self.VElements["eotech2"].color = Color(255, 255, 255, 255)
    else
        self.VElements["ballistic2"].color = Color(255, 255, 255, 0)
        self.VElements["ballistic3"].color = Color(255, 255, 255, 0)
        self.VElements["ballistic4"].color = Color(255, 255, 255, 0)
        self.VElements["ballistic5"].color = Color(255, 255, 255, 0)
        self.VElements["eotech2"].color = Color(255, 255, 255, 0)
    end
end

