SWEP.Base = "gb_camo_base"
SWEP.ViewModelFOV	= 54
SWEP.HoldType = "ar2"
SWEP.PrintName = "Scout's Scout"

SWEP.ViewModel		= "models/weapons/cstrike/c_snip_scout.mdl"
SWEP.WorldModel		= "models/weapons/w_snip_scout.mdl"
SWEP.AutoSpawnable = false
SWEP.Slot = 99

if CLIENT then

    SWEP.Author = "GreenBlack"

    SWEP.Icon = "vgui/ttt_fun_killicons/scout.png"
end


SWEP.Kind = 99

SWEP.Primary.Delay          = 1.3
SWEP.Primary.Recoil         = 0.001
SWEP.Primary.Automatic = true
SWEP.Primary.Ammo = "357"
SWEP.Primary.Damage = 90
SWEP.Primary.Cone = 0.001
SWEP.Primary.ClipSize = 200
SWEP.Primary.ClipMax = 200 -- keep mirrored to ammo
SWEP.Primary.DefaultClip = 100

SWEP.HeadshotMultiplier = 4

SWEP.AutoSpawnable      = true
SWEP.AmmoEnt = "item_ammo_357_ttt"

SWEP.UseHands			= true
SWEP.ViewModelFlip		= false
SWEP.ViewModelFOV		= 54

SWEP.Primary.Sound = Sound( "weapons/usp/usp1.wav")

SWEP.Secondary.Sound = Sound("Default.Zoom")

SWEP.IronSightsPos      = Vector( 5, -15, -2 )
SWEP.IronSightsAng      = Vector( 2.6, 1.37, 3.5 )
SWEP.HasFireBullet = true

function SWEP:SetZoom(state)
    if CLIENT or not IsValid(self.Owner) then
        return
    elseif IsValid(self.Owner) and self.Owner:IsPlayer() then
        if state then
            self.Owner:SetFOV(20, 0.3)
        else
            self.Owner:SetFOV(0, 0.2)
        end
    end
end

-- Add some zoom to ironsights for this gun
function SWEP:SecondaryAttack()
    if not self.IronSightsPos then return end
    if self:GetNextSecondaryFire() > CurTime() then return end

    bIronsights = not self:GetIronsights()

    self:SetIronsights( bIronsights )

    if SERVER then
        self:SetZoom(bIronsights)
    else
        self:EmitSound(self.Secondary.Sound)
    end

    self:SetNextSecondaryFire( CurTime() + 0.3)
end

function SWEP:PreDrop()
    self:SetZoom(false)
    self:SetIronsights(false)
    return self.BaseClass.PreDrop(self)
end

function SWEP:Reload()
    if ( self:Clip1() == self.Primary.ClipSize or self.Owner:GetAmmoCount( self.Primary.Ammo ) <= 0 ) then return end
    self:DefaultReload( ACT_VM_RELOAD )
    self:SetIronsights( false )
    self:SetZoom( false )
end


function SWEP:Holster()
    self:SetIronsights(false)
    self:SetZoom(false)
    return true
end

if CLIENT then
    local scope = surface.GetTextureID("sprites/scope")
    function SWEP:DrawHUD()
        if self:GetIronsights() then
            surface.SetDrawColor( 0, 0, 0, 255 )

            local x = ScrW() / 2.0
            local y = ScrH() / 2.0
            local scope_size = ScrH()

            -- crosshair
            local gap = 80
            local length = scope_size
            surface.DrawLine( x - length, y, x - gap, y )
            surface.DrawLine( x + length, y, x + gap, y )
            surface.DrawLine( x, y - length, x, y - gap )
            surface.DrawLine( x, y + length, x, y + gap )

            gap = 0
            length = 50
            surface.DrawLine( x - length, y, x - gap, y )
            surface.DrawLine( x + length, y, x + gap, y )
            surface.DrawLine( x, y - length, x, y - gap )
            surface.DrawLine( x, y + length, x, y + gap )


            -- cover edges
            local sh = scope_size / 2
            local w = (x - sh) + 2
            surface.DrawRect(0, 0, w, scope_size)
            surface.DrawRect(x + sh - 2, 0, w, scope_size)

            surface.SetDrawColor(255, 0, 0, 255)
            surface.DrawLine(x, y, x + 1, y + 1)

            -- scope
            surface.SetTexture(scope)
            surface.SetDrawColor(255, 255, 255, 255)

            surface.DrawTexturedRectRotated(x, y, scope_size, scope_size, 0)

        else
            return self.BaseClass.DrawHUD(self)
        end
    end

    function SWEP:AdjustMouseSensitivity()
        return (self:GetIronsights() and 0.2) or nil
    end
end

function SWEP:CamoPreDrawViewModel()
   if not IsValid(self.Owner) or not IsValid(self.Owner:GetViewModel()) then return end
    self.Owner:GetViewModel():SetColor( self.WeaponColor )
    self.Owner:GetViewModel():SetMaterial(  "models/rendertarget"  )
end

function SWEP:DeployFunction()

        self:SetColorAndMaterial( self.WeaponColor,  "models/rendertarget" );

    return true
end
