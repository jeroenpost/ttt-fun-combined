SWEP.Base = "gb_camo_base"
SWEP.ViewModelFOV	= 64
SWEP.HoldType = "duel"
SWEP.PrintName = "Magic's Punishment"
SWEP.ViewModel		= "models/weapons/v_pist_deags.mdl"
SWEP.WorldModel		= "models/weapons/w_pist_deagle.mdl"
SWEP.Kind = WEAPON_CARRY
SWEP.Slot = 4
SWEP.AutoSpawnable = false
SWEP.Primary.Recoil     = 1.5
SWEP.Primary.Damage = 50
SWEP.Primary.Delay = 0.55
SWEP.Primary.Cone = 0.02
SWEP.Primary.ClipSize = 100
SWEP.Primary.Automatic = true
SWEP.Primary.DefaultClip = 100
SWEP.Camo = 999
SWEP.Secondary.Delay = 0.55
SWEP.Secondary.ClipSize     = 100
SWEP.Secondary.DefaultClip  = 100
SWEP.Secondary.Automatic    = true
SWEP.Secondary.Ammo         = ".357"
SWEP.Secondary.ClipMax      = 100
SWEP.Camo = 46
SWEP.Primary.Ammo			= "AlyxGun"
SWEP.AmmoEnt = "item_ammo_357_ttt"
SWEP.HeadshotMultiplier = 2.5

SWEP.Camo = 17
SWEP.CustomCamo = true
SWEP.CamoIndex = {[1] = "camos/custom_camo17",[2] = "camos/custom_camo17"}

function SWEP:OnDrop()
    self:Remove()
end

SWEP.Primary.Sound = Sound("CSTM_Deagle")
SWEP.Secondary.Sound = Sound("CSTM_Deagle")

SWEP.WElements = {
    ["weapon"] = { type = "Model", model = "models/weapons/w_pist_deagle.mdl", bone = "ValveBiped.Bip01_L_Hand", rel = "", pos = Vector(3.6, 0.631, -2.5), angle = Angle(0, 0, 0), size = Vector(1, 1, 1), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
}


function SWEP:PrimaryAttack( worldsnd )

    --self.Weapon:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )

    if not self:CanPrimaryAttack() then return end

    if not worldsnd then
        self.Weapon:EmitSound( self.Primary.Sound, self.Primary.SoundLevel )
    elseif SERVER then
        sound.Play(self.Primary.Sound, self:GetPos(), self.Primary.SoundLevel)
    end

    self:ShootBullet( self.Primary.Damage, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )

    self:TakePrimaryAmmo( 1 )

    local owner = self.Owner
    if not IsValid(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

    --  owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )
end

function SWEP:Think()

    if self.Owner:KeyPressed(IN_ATTACK)  then
        -- When the left click is pressed, then
        self.ViewModelFlip = false
    end

    if self.Owner:KeyPressed(IN_ATTACK2)  then
        -- When the right click is pressed, then
        self.ViewModelFlip = true
    end
end

function SWEP:CanSecondaryAttack()
    if not IsValid(self.Owner) then return end

    if self.Weapon:Clip1() <= 0 then
        self:DryFire(self.SetNextSecondaryFire)
        return false
    end
    return true
end

function SWEP:SecondaryAttack(worldsnd)
    if  self.Owner:KeyDown(IN_USE) then
        self:Fly()
        return
    end

    self.Weapon:SetNextSecondaryFire( CurTime() + self.Secondary.Delay )
    --self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )

    if not self:CanSecondaryAttack() then return end

    if not worldsnd then
        self.Weapon:EmitSound( self.Primary.Sound, self.Primary.SoundLevel )
    elseif SERVER then
        sound.Play(self.Primary.Sound, self:GetPos(), self.Primary.SoundLevel)
    end

    self:ShootBullet( self.Primary.Damage, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )

    self:TakePrimaryAmmo( 1 )

    local owner = self.Owner
    if not IsValid(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

    -- owner:ViewPunch( Angle( math.Rand(-0.6,-0.6) * self.Primary.Recoil, math.Rand(-0.6,0.6) *self.Primary.Recoil, 0 ) )

end
