SWEP.Base = "gb_camo_base"
SWEP.ViewModelFOV	= 54
SWEP.HoldType = "pistol"
SWEP.Icon = "VGUI/ttt/icon_glock"
SWEP.PrintName = "King's Glock"
SWEP.ViewModel		= "models/weapons/cstrike/c_pist_glock18.mdl"
SWEP.WorldModel		= "models/weapons/w_pist_glock18.mdl"
SWEP.Kind = 123
SWEP.Slot = 12
SWEP.AutoSpawnable = false
SWEP.Primary.Recoil	= 0.9
SWEP.Primary.Damage = 9
SWEP.Primary.Delay = 0.08
SWEP.Primary.Cone = 0.028
SWEP.Primary.ClipSize = 20
SWEP.Primary.Automatic = true
SWEP.Primary.DefaultClip = 20
SWEP.Primary.ClipMax = 60
SWEP.Primary.Ammo = "Pistol"
SWEP.AutoSpawnable = false
SWEP.AmmoEnt = "item_ammo_pistol_ttt"

SWEP.Primary.Sound = Sound( "Weapon_Glock.Single" )
SWEP.IronSightsPos = Vector( -5.79, -3.9982, 2.8289 )

SWEP.HeadshotMultiplier = 1.2

SWEP.HasFireBullet = true
function SWEP:SecondaryAttack()
    if self.Owner:KeyDown(IN_USE) and self.NextReload < CurTime() then
        self.NextReload = CurTime() + 0.2
        local mode = self:GetNWString("shootmode")
        if mode == "full_auto" then  self:SetNWString("shootmode","semi_auto") end
        if mode == "semi_auto" then  self:SetNWString("shootmode","burst") end
        if mode == "burst" then  self:SetNWString("shootmode","full_auto") end
        self.Weapon:SetNextPrimaryFire( CurTime() + 0.8 )
        return
    end

    self:Fly()
end

SWEP.NextAmmo = 0
function SWEP:Think()
    if self.NextAmmo < CurTime() and self:Clip1() < 150 then
        self.NextAmmo = CurTime() + 2
        self:SetClip1(self:Clip1() + 5)
    end
end

function SWEP:Initialize()
    self:SetNWString("shootmode","full_auto")
    self.LastShot2 = 0
    self.LastShot = 0
end

SWEP.NextReload = 0
function SWEP:Reload()


    self.Weapon:DefaultReload( ACT_VM_RELOAD );
    self:SetIronsights( false )

end

function SWEP:PrimaryAttack()

    if not self:CanPrimaryAttack() then return end

    local mode = self:GetNWString("shootmode")
    if mode == "full_auto" then
        self.Primary.Delay = 0.06
        self.Primary.Damage = 9
        self.Primary.Automatic = true
    end
    if mode == "semi_auto" then
        self.Primary.Delay = 0.4
        self.Primary.Damage = 49
        self.Primary.Automatic = false
    end
    if mode == "burst" then
        self.Primary.Delay = 0.04
        self.Primary.Damage = 25
        self.Primary.Automatic = true
    end

    self.Weapon:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )

    if mode == "burst" then
        if self.LastShot > CurTime() and self.LastShot2 > CurTime() then
            if self.LastShot2 > CurTime() then
                self.Weapon:SetNextPrimaryFire( CurTime() + 0.9)
            end
        end
        if self.LastShot > CurTime() and self.LastShot2 < CurTime() then
            self.LastShot2 = CurTime() + 0.4
        end

    end
    self.LastShot = CurTime() + 0.4

    if not worldsnd then
        self.Weapon:EmitSound( self.Primary.Sound, self.Primary.SoundLevel )
    elseif SERVER then
        sound.Play(self.Primary.Sound, self:GetPos(), self.Primary.SoundLevel)
    end

    self:MakeAFlame()
    self:ShootBullet( self.Primary.Damage, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )

    self:TakePrimaryAmmo( 1 )

    local owner = self.Owner
    if not IsValid(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

    owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )

end





if CLIENT then
    function SWEP:DrawHUD( )
        local shotttext = "Shootmode: "..self:GetNWString("shootmode").."\nE+RMB to change"
        surface.SetFont( "ChatFont" );
        local size_x, size_y = surface.GetTextSize( shotttext );

        draw.RoundedBox( 6, ScrW( ) / 2 - size_x / 2 - 5, ScrH( ) - 100, size_x + 15, size_y + 10, Color( 0, 0, 0, 150 ) );
        draw.DrawText( shotttext , "ChatFont", ScrW( ) / 2, ScrH( ) - 100 + 5, Color( 255, 30, 30, 255 ), TEXT_ALIGN_CENTER );

        local scope = surface.GetTextureID("sprites/scope")
        if self:GetIronsights() then
            surface.SetDrawColor( 0, 0, 0, 255 )

            local x = ScrW() / 2.0
            local y = ScrH() / 2.0
            local scope_size = ScrH()

            -- crosshair
            local gap = 80
            local length = scope_size
            surface.DrawLine( x - length, y, x - gap, y )
            surface.DrawLine( x + length, y, x + gap, y )
            surface.DrawLine( x, y - length, x, y - gap )
            surface.DrawLine( x, y + length, x, y + gap )

            gap = 0
            length = 50
            surface.DrawLine( x - length, y, x - gap, y )
            surface.DrawLine( x + length, y, x + gap, y )
            surface.DrawLine( x, y - length, x, y - gap )
            surface.DrawLine( x, y + length, x, y + gap )


            -- cover edges
            local sh = scope_size / 2
            local w = (x - sh) + 2
            surface.DrawRect(0, 0, w, scope_size)
            surface.DrawRect(x + sh - 2, 0, w, scope_size)

            surface.SetDrawColor(255, 0, 0, 255)
            surface.DrawLine(x, y, x + 1, y + 1)

            -- scope
            surface.SetTexture(scope)
            surface.SetDrawColor(255, 255, 255, 255)

            surface.DrawTexturedRectRotated(x, y, scope_size, scope_size, 0)

        else
            return self.BaseClass.DrawHUD(self)
        end
    end

    function SWEP:AdjustMouseSensitivity()
        return (self:GetIronsights() and 0.2) or nil
    end
end;
