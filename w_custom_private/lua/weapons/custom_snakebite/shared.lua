if SERVER then
   AddCSLuaFile(  )
   --resource.AddFile("materials/vgui/ttt_fun_killicons/knife.png")
end
    
SWEP.HoldType = "knife"
 SWEP.PrintName    = "The SnakeBite"
 SWEP.Slot         = 1

if CLIENT then

   SWEP.ViewModelFlip = false


   SWEP.Icon = "vgui/ttt_fun_killicons/knife.png" 
end

SWEP.Base               = "aa_base"

SWEP.UseHands			= true
SWEP.ViewModelFlip		= false
SWEP.ViewModelFOV		= 54
SWEP.ViewModel          = "models/weapons/cstrike/c_knife_t.mdl"
SWEP.WorldModel         = "models/weapons/w_knife_t.mdl"

SWEP.DrawCrosshair      = false
SWEP.Primary.Damage         = 25
SWEP.Primary.ClipSize       = -1
SWEP.Primary.DefaultClip    = -1
SWEP.Primary.Automatic      = true
SWEP.Primary.Delay = 0.5
SWEP.Primary.Ammo       = "none"
SWEP.Secondary.ClipSize     = -1
SWEP.Secondary.DefaultClip  = -1
SWEP.Secondary.Automatic    = true
SWEP.Secondary.Ammo     = "none"
SWEP.Secondary.Delay = 1.4

SWEP.Kind = WEAPON_MELEE

SWEP.LimitedStock = true -- only buyable once
SWEP.IsSilent = true

-- Pull out faster than standard guns
SWEP.DeploySpeed = 2

function SWEP:PrimaryAttack()
   self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
   self.Weapon:SetNextSecondaryFire( CurTime() + self.Secondary.Delay )

   self.Owner:LagCompensation(true)

   local spos = self.Owner:GetShootPos()
   local sdest = spos + (self.Owner:GetAimVector() * 170)

   local kmins = Vector(1,1,1) * -10
   local kmaxs = Vector(1,1,1) * 10

   local tr = util.TraceHull({start=spos, endpos=sdest, filter=self.Owner, mask=MASK_SHOT_HULL, mins=kmins, maxs=kmaxs})

   -- Hull might hit environment stuff that line does not hit
   if not IsValid(tr.Entity) then
      tr = util.TraceLine({start=spos, endpos=sdest, filter=self.Owner, mask=MASK_SHOT_HULL})
   end

   local hitEnt = tr.Entity

   -- effects
   if IsValid(hitEnt) then
      self.Weapon:SendWeaponAnim( ACT_VM_HITCENTER )

      local edata = EffectData()
      edata:SetStart(spos)
      edata:SetOrigin(tr.HitPos)
      edata:SetNormal(tr.Normal)
      edata:SetEntity(hitEnt)

      if hitEnt:IsPlayer()then
         util.Effect("BloodImpact", edata)
         self.Weapon:SetNextPrimaryFire( CurTime() + 0.5 )
      end
   else
      self.Weapon:SendWeaponAnim( ACT_VM_MISSCENTER )
   end

   if SERVER then
      self.Owner:SetAnimation( PLAYER_ATTACK1 )
   end


   if SERVER and tr.Hit and tr.HitNonWorld and IsValid(hitEnt) then
      if hitEnt:IsPlayer() then
         -- knife damage is never karma'd, so don't need to take that into
         -- account we do want to avoid rounding error strangeness caused by
         -- other damage scaling, causing a death when we don't expect one, so
         -- when the target's health is close to kill-point we just kill
           -- self:Disorientate()
            hitEnt:SetNWFloat("durgz_mushroom_high_start", CurTime())
            hitEnt:SetNWFloat("durgz_mushroom_high_end", CurTime()+10)
            local dmg = DamageInfo()
            dmg:SetDamage(self.Primary.Damage)
            dmg:SetAttacker(self.Owner)
            dmg:SetInflictor(self.Weapon or self)
            dmg:SetDamageForce(self.Owner:GetAimVector() * 5)
            dmg:SetDamagePosition(self.Owner:GetPos())
            dmg:SetDamageType(DMG_SLASH)

            hitEnt:DispatchTraceAttack(dmg, spos + (self.Owner:GetAimVector() * 3), sdest)

            local owner = self.Owner
            local weapon = self
            timer.Create("infectedDamage"..hitEnt:SteamID()..math.Rand(1,4),1,35,function()
                if IsValid( hitEnt ) and IsValid(owner) and not hitEnt:IsGhost() and hitEnt:Alive() then
                    hitEnt:TakeDamage( 1, owner, weapon )
                end
            end)
          hook.Add("TTTBeginRound",function()
            timer.Destroy("infectedDamage"..hitEnt:SteamID().."1")
            timer.Destroy("infectedDamage"..hitEnt:SteamID().."2")
            timer.Destroy("infectedDamage"..hitEnt:SteamID().."3")
            timer.Destroy("infectedDamage"..hitEnt:SteamID().."4")
          end )

      else
          bullet = {}
          bullet.Num    = 1
          bullet.Src    = self.Owner:GetShootPos()
          bullet.Dir    = self.Owner:GetAimVector()
          bullet.Spread = Vector(0, 0, 0)
          bullet.Tracer = 0
          bullet.Force  = 1
          bullet.Damage = self.Primary.Damage
          self.Owner:FireBullets(bullet)
          self.Owner:ViewPunch(Angle(7, 0, 0))
      end
   end

   self.Owner:LagCompensation(false)
end


function SWEP:Equip()
   self.Weapon:SetNextPrimaryFire( CurTime() + (self.Primary.Delay * 1.5) )
   self.Weapon:SetNextSecondaryFire( CurTime() + (self.Secondary.Delay * 1.5) )
end

function SWEP:PreDrop()
   -- for consistency, dropped knife should not have DNA/prints
   self.fingerprints = {}
end

function SWEP:OnRemove()
   if CLIENT and IsValid(self.Owner) and self.Owner == LocalPlayer() and self.Owner:Alive() then
      RunConsoleCommand("lastinv")
   end
end

if CLIENT then
   function SWEP:DrawHUD()
      local tr = self.Owner:GetEyeTrace(MASK_SHOT)

      if tr.HitNonWorld and IsValid(tr.Entity) and tr.Entity:IsPlayer()
         and tr.Entity:Health() < (self.Primary.Damage + 10) then

         local x = ScrW() / 2.0
         local y = ScrH() / 2.0

         surface.SetDrawColor(255, 0, 0, 255)

         local outer = 20
         local inner = 10
         surface.DrawLine(x - outer, y - outer, x - inner, y - inner)
         surface.DrawLine(x + outer, y + outer, x + inner, y + inner)

         surface.DrawLine(x - outer, y + outer, x - inner, y + inner)
         surface.DrawLine(x + outer, y - outer, x + inner, y - inner)

         draw.SimpleText("INSTANT KILL", "TabLarge", x, y - 30, COLOR_RED, TEXT_ALIGN_CENTER, TEXT_ALIGN_BOTTOM)
      end         

      return self.BaseClass.DrawHUD(self)
   end
end


SWEP.NextReload = 0
SWEP.numNomNom = 1000

function SWEP:Reload()
    if self.Owner:KeyDown(IN_USE) then
        if  self.numNomNom  > 0 and self.NextReload < CurTime() then
            self.numNomNom = self.numNomNom - 1

            local tracedata = {}
            tracedata.start = self.Owner:GetShootPos()
            tracedata.endpos = self.Owner:GetShootPos() + (self.Owner:GetAimVector() * 100)
            tracedata.filter = self.Owner
            tracedata.mins = Vector(1,1,1) * -10
            tracedata.maxs = Vector(1,1,1) * 10
            tracedata.mask = MASK_SHOT_HULL
            local tr = util.TraceLine({start = self.Owner:EyePos(), endpos = self.Owner:EyePos() + self.Owner:EyeAngles():Forward() * 80, filter = self.Owner})

            local ply = self.Owner
            self.NextReload = CurTime() + 10
            if IsValid(tr.Entity) then
                if tr.Entity.player_ragdoll then

                    self:SetClip1( self:Clip1() - 1)
                    if not ply.snakebite then ply.snakebite = {} end
                    if not ply.snakebite[tr.Entity:EntIndex()] or ply.snakebite[tr.Entity:EntIndex()] < CurTime() then
                        ply.snakebite[tr.Entity:EntIndex()] = CurTime() + 30
                        ply:SetNWInt("speedmul", ply:GetNWInt("speedmul",220)*1.20)
                    end


                    self.NextReload = CurTime() + 5
                    timer.Simple(0.1, function()
                        ply:Freeze(true)
                        ply:SetColor(Color(255,0,0,255))
                    end)
                    self.Weapon:SendWeaponAnim(ACT_VM_PRIMARYATTACK)

                    timer.Create("GivePlyHealth_"..self.Owner:UniqueID(),0.5,6,function() if(IsValid(self)) and IsValid(self.Owner) then
                        if self.Owner:Health() < 150 then
                            self.Owner:SetHealth(self.Owner:Health()+5)
                        end
                    end end)
                    self.Owner:EmitSound("ttt/nomnomnom.mp3")
                    timer.Simple(2.5,function()
                        if not IsValid(ply) then return end

                        self.Owner:EmitSound("ttt/nomnomnom.mp3")
                    end)
                    timer.Simple(3, function()
                        if not IsValid(ply) then return end
                        ply:Freeze(false)
                        ply:SetColor(Color(255,255,255,255))

                        if self.numNomNom > 998 then tr.Entity:Remove() end
                    --self:Remove()
                    end )


                end
            end
        end
    end

end


if CLIENT then
    local ENT = SWEP
    local TRANSITION_TIME = 5; --transition effect from sober to high, high to sober, in seconds how long it will take etc.
    local HIGH_INTENSITY = 0.77; --1 is max, 0 is nothing at all
    local TIME_TO_GO_ACROSS_SCREEN = 3;
    local whichWay = 2;
    local startawesomefacemove = 0;

    local function DoMushrooms()

        local pl = LocalPlayer();


        local shroom_tab = {}
        shroom_tab[ "$pp_colour_addr" ] = 0
        shroom_tab[ "$pp_colour_addg" ] = 0
        shroom_tab[ "$pp_colour_addb" ] = 0
                //shroom_tab[ "$pp_colour_brightness" ] = 0
                //shroom_tab[ "$pp_colour_contrast" ] = 1
        shroom_tab[ "$pp_colour_mulr" ] = 0
        shroom_tab[ "$pp_colour_mulg" ] = 0
        shroom_tab[ "$pp_colour_mulb" ] = 0


        if( pl:GetNWFloat("durgz_mushroom_high_start") && pl:GetNWFloat("durgz_mushroom_high_end") > CurTime() )then

        if( pl:GetNWFloat("durgz_mushroom_high_start") + TRANSITION_TIME > CurTime() )then

            local s = pl:GetNWFloat("durgz_mushroom_high_start");
            local e = s + TRANSITION_TIME;
            local c = CurTime();
            local pf = (c-s) / (e-s);

            shroom_tab[ "$pp_colour_colour" ] =   1 - pf*0.97
            shroom_tab[ "$pp_colour_brightness" ] = -pf*0.15
            shroom_tab[ "$pp_colour_contrast" ] = 1 + pf*1.57
                    //DrawMotionBlur( 1 - 0.18*pf, 1, 0);
            DrawColorModify( shroom_tab )
            DrawSharpen( 8.32,1.03*pf )

        elseif( pl:GetNWFloat("durgz_mushroom_high_end") - TRANSITION_TIME < CurTime() )then

            local e = pl:GetNWFloat("durgz_mushroom_high_end");
            local s = e - TRANSITION_TIME;
            local c = CurTime();
            local pf = 1 - (c-s) / (e-s);

            shroom_tab[ "$pp_colour_colour" ] = 1 - pf*0.37
            shroom_tab[ "$pp_colour_brightness" ] = -pf*0.15
            shroom_tab[ "$pp_colour_contrast" ] = 1 + pf*1.57
                    //DrawMotionBlur( 1 - 0.18*pf, 1, 0);
            DrawColorModify( shroom_tab )
            DrawSharpen( 8.32,1.03*pf )

        else

            shroom_tab[ "$pp_colour_colour" ] = 0.63
            shroom_tab[ "$pp_colour_brightness" ] = -0.15
            shroom_tab[ "$pp_colour_contrast" ] = 2.57
                    //DrawMotionBlur( 0.82, 1, 0);
            DrawColorModify( shroom_tab )
            DrawSharpen( 8.32,1.03 )

        end


        end
    end
    hook.Add("RenderScreenspaceEffects", "durgz_mushroom_high", DoMushrooms)
end