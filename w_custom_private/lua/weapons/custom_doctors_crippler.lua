	SWEP.PrintName = "Doctor's Cripplers"
	SWEP.DrawAmmo = false
	SWEP.DrawCrosshair = false


    function SWEP:SwitchMode()
        if  self.NextSwitch and self.NextSwitch > CurTime() then return end
        self.NextSwitch = CurTime() + 1

        if self.IsShotty then
            self.Primary.Damage = 65
            self.HeadshotMultiplier = 2
            self.Primary.Cone = 0.006
            self.Primary.NumShots = 1
            self.Primary.Sound = Sound ("Weapon_M4A1.Silenced")
            self.IsShotty = false
            self.Owner:PrintMessage( HUD_PRINTCENTER, "Knife mode activated" )

        else
            self.Primary.Damage = 5
            self.HeadshotMultiplier = 1
            self.Primary.Cone = 0.12
            self.Primary.Sound = Sound ("Weapon_M3.Single")
            self.Primary.NumShots = 18
            self.IsShotty = true
            self.Owner:PrintMessage( HUD_PRINTCENTER, "Shotty mode activated" )

        end
    end

    SWEP.Mines = false
SWEP.Base = "gb_base"
SWEP.Kind = WEAPON_NADE

SWEP.CanBuy = nil -- no longer a buyable thing
--SWEP.LimitedStock = true

SWEP.AllowDrop = false
SWEP.Icon = "vgui/ttt_fun_killicons/fists.png"

SWEP.Author			= "robotboy655"
SWEP.Purpose		= "Well we sure as heck didn't use guns! We would wrestle Hunters to the ground with our bare hands! I would get ten, twenty a day, just using my fists."


SWEP.Spawnable			= true
SWEP.UseHands			= true
SWEP.DrawAmmo			= false

    SWEP.ViewModel          = "models/weapons/cstrike/c_knife_t.mdl"
    SWEP.WorldModel         = "models/weapons/w_knife_t.mdl"

SWEP.ViewModelFOV		= 52
SWEP.ViewModelFlip		= false

SWEP.Primary.ClipSize		= 25
SWEP.Primary.DefaultClip	= 25
SWEP.Primary.Damage			= 5
SWEP.Primary.Automatic		= true
SWEP.Primary.Ammo			= "none"
SWEP.Primary.Cone = 0.001
SWEP.Secondary.ClipSize		= 1
SWEP.Secondary.DefaultClip	= 1
SWEP.Secondary.Damage			= 5
SWEP.Secondary.Automatic	= true
SWEP.Secondary.Ammo			= "Battery"

SWEP.Weight				= 5
SWEP.AutoSwitchTo		= false
SWEP.AutoSwitchFrom		= false

SWEP.Slot				= 3
SWEP.SlotPos			= 5
SWEP.DrawAmmo			= false
SWEP.DrawCrosshair		= true
SWEP.secondUsed                 = false
    SWEP.Primary.Delay = 0.8
    SWEP.Secondary.Delay = 0.8
local SwingSound = Sound( "weapons/slam/throw.wav" )
local HitSound = Sound( "Flesh.ImpactHard" )

function SWEP:Initialize()

	self:SetHoldType( "fist" )

end

function SWEP:Think()
   
end

    SWEP.nextswitch = 0

    function SWEP:Reload()
    if self.Owner:KeyDown(IN_USE) and self.nextswitch < CurTime() then
        self.nextswitch = CurTime() + 0.5
        self:SwitchMode()
        return
    end
    self.BaseClass.Reload(self)
    end


SWEP.Distance = 67
SWEP.AttackAnimsRight = { "fists_left", "fists_right", "fists_uppercut" }
SWEP.AttackAnims = { "fists_right", "fists_uppercut" }
    SWEP.LimitedStock = true -- only buyable once

    SWEP.IsSilent = true

    -- Pull out faster than standard guns
    SWEP.DeploySpeed = 1
    SWEP.TrewMine = 0

    function SWEP:PrimaryAttack()
        if self.Owner:KeyDown(IN_DUCK) and self.Mines and isfunction(self.Mines.Explode) and CurTime() > self.TrewMine then
            self.WillExplode = true
            self.Mines.Owner = self.Owner
            self.Mines:Explode()
            return
        end
        self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
        self.Weapon:SetNextSecondaryFire( CurTime() + self.Secondary.Delay )

        if self.IsShotty then
            self.BaseClass.PrimaryAttack(self)
            return
        end

        self.Owner:LagCompensation(true)

        if self:GetNWString("mode") == "shotgun" then

        end

        local spos = self.Owner:GetShootPos()
        local sdest = spos + (self.Owner:GetAimVector() * 70)

        local kmins = Vector(1,1,1) * -10
        local kmaxs = Vector(1,1,1) * 10

        local tr = util.TraceHull({start=spos, endpos=sdest, filter=self.Owner, mask=MASK_SHOT_HULL, mins=kmins, maxs=kmaxs})

        -- Hull might hit environment stuff that line does not hit
        if not IsValid(tr.Entity) then
            tr = util.TraceLine({start=spos, endpos=sdest, filter=self.Owner, mask=MASK_SHOT_HULL})
        end

        local hitEnt = tr.Entity

        -- effects
        if IsValid(hitEnt) then
            self.Weapon:SendWeaponAnim( ACT_VM_HITCENTER )

            local edata = EffectData()
            edata:SetStart(spos)
            edata:SetOrigin(tr.HitPos)
            edata:SetNormal(tr.Normal)
            edata:SetEntity(hitEnt)

            if hitEnt:IsPlayer() or hitEnt:GetClass() == "prop_ragdoll" then
                util.Effect("BloodImpact", edata)
            end
        else
            self.Weapon:SendWeaponAnim( ACT_VM_MISSCENTER )
        end

        if SERVER then
            self.Owner:SetAnimation( PLAYER_ATTACK1 )
        end


        if SERVER and tr.Hit and tr.HitNonWorld and IsValid(hitEnt) then
            if hitEnt:IsPlayer() then
                -- knife damage is never karma'd, so don't need to take that into
                -- account we do want to avoid rounding error strangeness caused by
                -- other damage scaling, causing a death when we don't expect one, so
                -- when the target's health is close to kill-point we just kill

                local dmg = DamageInfo()
                dmg:SetDamage(65)
                dmg:SetAttacker(self.Owner)
                dmg:SetInflictor(self.Weapon or self)
                dmg:SetDamageForce(self.Owner:GetAimVector() * 5)
                dmg:SetDamagePosition(self.Owner:GetPos())
                dmg:SetDamageType(DMG_SLASH)

                hitEnt:DispatchTraceAttack(dmg, spos + (self.Owner:GetAimVector() * 3), sdest)
            else
                bullet = {}
                bullet.Num    = 1
                bullet.Src    = self.Owner:GetShootPos()
                bullet.Dir    = self.Owner:GetAimVector()
                bullet.Spread = Vector(0, 0, 0)
                bullet.Tracer = 0
                bullet.Force  = 1
                bullet.Damage = 35
                self.Owner:FireBullets(bullet)
                self.Owner:ViewPunch(Angle(7, 0, 0))
            end
        end

        self.Owner:LagCompensation(false)
    end


function SWEP:SecondaryAttack()
    if not self.secondUsed then
         self:AttackThing( "fists_left"  )
    else
        self:AttackThing( "fists_right")
    end
	
end


function SWEP:DealDamage( anim )
	local tr = util.TraceLine( {
		start = self.Owner:GetShootPos(),
		endpos = self.Owner:GetShootPos() + self.Owner:GetAimVector() * self.Distance,
		filter = self.Owner
	} )

	if ( not IsValid( tr.Entity ) ) then 
		tr = util.TraceHull( {
			start = self.Owner:GetShootPos(),
			endpos = self.Owner:GetShootPos() + self.Owner:GetAimVector() * self.Distance,
			filter = self.Owner,
			mins = self.Owner:OBBMins() / 3,
			maxs = self.Owner:OBBMaxs() / 3
		} )
	end

	if ( tr.Hit ) then self.Owner:EmitSound( HitSound ) end

	if ( IsValid( tr.Entity ) && ( tr.Entity:IsNPC() || tr.Entity:IsPlayer() ) ) then
		local dmginfo = DamageInfo()
		dmginfo:SetDamage( self.Primary.Damage )
		if ( anim == "fists_left" ) then
			dmginfo:SetDamageForce( self.Owner:GetRight() * 49125 + self.Owner:GetForward() * 99984 ) -- Yes we need those specific numbers
                        dmginfo:SetDamage( self.Secondary.Damage )
                        

                        self.secondUsed = false
		elseif ( anim == "fists_right" ) then
			dmginfo:SetDamageForce( self.Owner:GetRight() * -49124 + self.Owner:GetForward() * 99899 )
                        dmginfo:SetDamage( self.Primary.Damage )
		elseif ( anim == "fists_uppercut" ) then
			dmginfo:SetDamageForce( self.Owner:GetUp() * 51589 + self.Owner:GetForward() * 100128 )
                        dmginfo:SetDamage( self.Primary.Damage )
		end
                tr.Entity:SetVelocity(self.Owner:GetForward() * 600 + Vector(0,0,400))
		dmginfo:SetInflictor( self )
		local attacker = self.Owner
		if ( !IsValid( attacker ) ) then attacker = self end
		dmginfo:SetAttacker( attacker )

		tr.Entity:TakeDamageInfo( dmginfo )
	end
end



function SWEP:AttackThing( anim )
   self.Owner:SetAnimation( PLAYER_ATTACK1 )
   
   
    
	if ( !SERVER ) then return end
   if !IsValid(self.Owner) then return end
	-- We need this because attack sequences won't work otherwise in multiplayer
	local vm = self.Owner:GetViewModel()
	vm:ResetSequence( vm:LookupSequence( "fists_idle_01" ) )

	--local anim = "fists_left" --self.AttackAnims[ math.random( 1, #self.AttackAnims ) ]

	timer.Simple( 0, function()
		if ( !IsValid( self ) || !IsValid( self.Owner ) || !self.Owner:GetActiveWeapon() || self.Owner:GetActiveWeapon() != self ) then return end
	
		local vm = self.Owner:GetViewModel()
		vm:ResetSequence( vm:LookupSequence( anim ) )

		self:Idle()
	end )

	timer.Simple( 0.05, function()
		if ( !IsValid( self ) || !IsValid( self.Owner ) || !self.Owner:GetActiveWeapon() || self.Owner:GetActiveWeapon() != self ) then return end
		if ( anim == "fists_left" ) then
			self.Owner:ViewPunch( Angle( 0, 16, 0 ) )
		elseif ( anim == "fists_right" ) then
			self.Owner:ViewPunch( Angle( 0, -16, 0 ) )
		elseif ( anim == "fists_uppercut" ) then
			self.Owner:ViewPunch( Angle( 16, -8, 0 ) )
		end
	end )

	timer.Simple( 0.2, function()
		if ( !IsValid( self ) || !IsValid( self.Owner ) || !self.Owner:GetActiveWeapon() || self.Owner:GetActiveWeapon() != self ) then return end
		if ( anim == "fists_left" ) then
			self.Owner:ViewPunch( Angle( 4, -16, 0 ) )
		elseif ( anim == "fists_right" ) then
			self.Owner:ViewPunch( Angle( 4, 16, 0 ) )
		elseif ( anim == "fists_uppercut" ) then
			self.Owner:ViewPunch( Angle( -32, 0, 0 ) )
		end
		self.Owner:EmitSound( SwingSound )
		
	end )

	timer.Simple( 0.2, function()
		if ( !IsValid( self ) || !IsValid( self.Owner ) || !self.Owner:GetActiveWeapon() || self.Owner:GetActiveWeapon() != self ) then return end
		self:DealDamage( anim )
	end )

	self.Weapon:SetNextSecondaryFire(CurTime() + 0.5)
    self.Weapon:SetNextPrimaryFire(CurTime() + 0.5)



end

function SWEP:Idle()

	local vm = self.Owner:GetViewModel()
	timer.Create( "fists_idle" .. self:EntIndex(), vm:SequenceDuration(), 1, function()
		vm:ResetSequence( vm:LookupSequence( "fists_idle_0" .. math.random( 1, 2 ) ) )
	end )

end



function SWEP:OnRemove()

	if ( IsValid( self.Owner ) ) then
		local vm = self.Owner:GetViewModel()
        if IsValid(vm) then
		vm:SetMaterial( "" )
        end
	end

	timer.Stop( "fists_idle" .. self:EntIndex() )

end

function SWEP:Holster( wep )
    if ( IsValid( self.Owner ) ) then
        local vm = self.Owner:GetViewModel()
        if IsValid(vm) then
            vm:SetMaterial( "" )
        end
    end

	return true
end
SWEP.modelView = false

function SWEP:Deploy()

    local vm = self.Owner:GetViewModel()
    vm:ResetSequence( vm:LookupSequence( "fists_draw" ) )


    self:Idle()

    return true

end

    function SWEP:SecondaryAttack()
        self.Weapon:SetNextSecondaryFire( CurTime() + 30 )

        if CLIENT or (self.Owner.NextEatMoney and self.Owner.NextEatMoney > CurTime()) then return end
        self.Owner.NextEatMoney =  CurTime() + 5

        self:throw_attack ("boommine",  "physics/glass/glass_impact_hard3.wav", 100)


    end


    function SWEP:throw_attack (model_file, sound, away)
        local tr = self.Owner:GetEyeTrace();

        if IsValid( tr.Entity )  then
            if not tr.Entity.hasProtectionSuit then
                tr.Entity.hasProtectionSuit= true
                timer.Simple(5, function()
                    if not IsValid( tr.Entity )  then return end
                    tr.Entity.hasProtectionSuit = false
                end)
            end
        end

        self.BaseClass.ShootEffects (self);
        if (!SERVER) then return end;
        local ent = ents.Create ("boommine");
        --  ent:SetModel (model_file);
        ent:SetPos( tr.HitPos + self.Owner:GetAimVector() * -46 )
        ent:SetAngles (self.Owner:EyeAngles());
        ent:SetPhysicsAttacker(self.Owner)
        ent:Spawn();
        ent.WillExplode = self.Owner:IsTraitor()
        ent:EmitSound( sound )
        self.Mines = ent
        self.TrewMine = CurTime() + 3
        if math.Rand(1,25) < 2 then
            ent:Ignite(100,100)
        end
        local phys = ent:GetPhysicsObject();
        local shot_length = tr.HitPos:Length();
        -- phys:ApplyForceCenter (self.Owner:GetAimVector():GetNormalized() * math.pow (shot_length, 3) * 2);

        timer.Simple(away,function()
            if IsValid(ent) then
                ent:Remove()
            end
        end)

        cleanup.Add (self.Owner, "props", ent);
        undo.Create ("Thrown model");
        undo.AddEntity (ent);
        undo.SetPlayer (self.Owner);
        undo.Finish();
    end

    local ENT = {}

    ENT.Type = "anim"
    ENT.Base = "base_anim"


    if SERVER then

        function ENT:Initialize()

            self.WillExplode = false
            self:SetModel("models/props_combine/combine_mine01.mdl")
            self:SetModelScale(self:GetModelScale()*1,0)
            self.Deployed = CurTime()
            self.Entity:PhysicsInit(SOLID_VPHYSICS);
            self.Entity:SetMoveType(MOVETYPE_VPHYSICS);
            self.Entity:SetSolid(SOLID_VPHYSICS);
            self.Entity:SetCollisionGroup(COLLISION_GROUP_PROJECTILE)
            self.Damage = 1
            self.health =50
            local phys = self:GetPhysicsObject()
            if phys:IsValid() then
                phys:Wake()
                phys:EnableGravity(true);
                phys:AddGameFlag(FVPHYSICS_NO_PLAYER_PICKUP)
            end


            self:EmitSound("npc/roller/mine/rmine_blades_out3.wav")


        end

        function ENT:Touch( hitent )
            if  self.WillExplode then
                if not ents or not SERVER then return end

                if self.Deployed + 3 > CurTime() then return end

                local ent = ents.Create( "env_explosion" )
                if( IsValid( hitent ) ) then
                    ent:SetPos( hitent:GetPos() )
                    ent:SetOwner( self.Owner )
                    ent:SetKeyValue( "iMagnitude", "195" )
                    ent:Spawn()

                    self:Remove()
                    ent:Fire( "Explode", 0, 0 )
                    ent:EmitSound( "weapons/big_explosion.mp3", 511, 100 )
                end
                timer.Simple(0.2,function()
                    if not IsValid(hitent) then return end
                hitent:SetVelocity(hitent:GetUp() * 6000 )
                end)
           end

        end

        function ENT:TakeDamage(damageAmount)
            self.health = self.health - damageAmount
            if self.health > 20 then
                local ent = ents.Create( "env_explosion" )
                if( IsValid( self ) ) then
                    ent:SetPos( self:GetPos() )
                    ent:SetOwner( self.Owner )
                    ent:SetKeyValue( "iMagnitude", "195" )
                    ent:Spawn()

                    self:Remove()
                    ent:Fire( "Explode", 0, 0 )
                    ent:EmitSound( "weapons/big_explosion.mp3", 511, 100 )
                end
            end


        end

        function ENT:Explode()
            if self.exploded then return end self.exploded = true
            local ent = ents.Create( "env_explosion" )
            ent:SetPos( self:GetPos() )
            ent:SetOwner( self.Owner )
            ent:SetKeyValue( "iMagnitude", "125" )
            ent:Spawn()

            self:Remove()
            ent:Fire( "Explode", 0, 0 )
            ent:EmitSound( "weapons/big_explosion.mp3", 511, 100 )

        end

        ENT.nextBounce = 0
        function ENT:Think()
            if self.nextBounce < CurTime() then
                self.nextBounce = CurTime() + 5
               local phys = self:GetPhysicsObject()
                phys:SetVelocity(self:GetUp() * math.Rand(75,250) )

            end
        end


        function ENT:Use( activator, caller )

            if  self.WillExplode then
                if not ents or not SERVER then return end


                if self.Deployed + 3 > CurTime() then return end

                local ent = ents.Create( "env_explosion" )
                if( IsValid( activator ) ) then
                    ent:SetPos( self:GetPos() )
                    ent:SetOwner( self.Owner )
                    ent:SetKeyValue( "iMagnitude", "195" )
                    ent:Spawn()

                    self:Remove()
                    ent:Fire( "Explode", 0, 0 )
                    ent:EmitSound( "weapons/big_explosion.mp3", 511, 100 )
                end
                timer.Simple(0.2,function()
                    if not IsValid(activator) then return end
                    activator:SetVelocity(activator:GetUp() * 6000 )
                end)

            else
                self.Entity:Remove()
                if ( activator:IsPlayer() ) then
                    if activator:Health() < 150 then
                        if self.lower then
                            activator:SetHealth(activator:Health()+10)
                        else
                            activator:SetHealth(activator:Health()+25)
                        end
                    end
                    activator:EmitSound("crisps/eat.mp3")
                end
            end
        end


    end

    scripted_ents.Register( ENT, "boommine", true )