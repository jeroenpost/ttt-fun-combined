include "weapons/weapon_zm_shotgun.lua"
SWEP.Base = "aa_base"
SWEP.ViewModelFOV	= 54
SWEP.HoldType = "smg"
SWEP.PrintName = "Batpig's tickle stick"
SWEP.ViewModel		= "models/weapons/cstrike/c_shot_m3super90.mdl"
SWEP.WorldModel		= "models/weapons/w_shot_m3super90.mdl"
SWEP.AutoSpawnable = false
SWEP.UseHands = true
SWEP.ViewModelFlip = false

SWEP.HoldType = "shotgun"
SWEP.Slot = 1
SWEP.Kind = WEAPON_EQUIP3

if CLIENT then

    SWEP.Author = "GreenBlack"

    SWEP.Icon = "vgui/ttt_fun_killicons/xm1014.png"
end

SWEP.Primary.Sound			= Sound( "Weapon_M3.Single" )
SWEP.Primary.Recoil			= 7

SWEP.Primary.Ammo = "Buckshot"
SWEP.Primary.Damage = 18
SWEP.Primary.Cone = 0.084
SWEP.Primary.Delay = 1
SWEP.Primary.ClipSize = 12
SWEP.Primary.ClipMax = 24
SWEP.Primary.DefaultClip = 12
SWEP.Primary.Automatic = true
SWEP.Primary.NumShots = 8
SWEP.AutoSpawnable      = false
SWEP.AmmoEnt = "item_box_buckshot_ttt"

function SWEP:Deploy()
    self.dt.reloading = false
    self.reloadtimer = 0
    return self:DeployFunction()
end

SWEP.IronSightsPos = Vector(-7.65, -12.214, 3.2)
SWEP.IronSightsAng = Vector(0, 0, 0)

function SWEP:PrimaryAttack()
    local mode = self:GetNWString("shootmode")
    if mode == "Shotgun" then
        self:SetNextPrimaryFire( CurTime() + 0.6 )
        self.Weapon:EmitSound( self.Primary.Sound, 80 )
        self.Primary.Damage = 8
        self.Primary.Recoil = 4
        self.Primary.NumShots = 11
        self.Primary.Cone = 0.083
        self:ShootBullet( self.Primary.Damage, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )
        self:TakePrimaryAmmo( 1 )
    end
    if mode == "Kittez Mode" then
        self:SetNextPrimaryFire( CurTime() + 1.25 )
        self.Weapon:EmitSound( "weapons/scout/scout_fire-1.wav", self.Primary.SoundLevel )
        self.Primary.Damage = 95
        self.Primary.Recoil = 1
        self.Primary.NumShots = 1
        self.Primary.Cone = 0.0001
        self:ShootBullet( self.Primary.Damage, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )
        self:TakePrimaryAmmo( 1 )
    end
    if mode == "SMG" then
        self:SetNextPrimaryFire( CurTime() + 0.1 )
        self.Weapon:EmitSound( "weapons/scout/scout_fire-1.wav", self.Primary.SoundLevel )
        self.Primary.Damage = 13
        self.Primary.Recoil = 0.05
        self.Primary.NumShots = 1
        self.Primary.Cone = 0.005
        self:ShootBullet( self.Primary.Damage, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )
        self:TakePrimaryAmmo( 1 )
    end
end

function SWEP:SecondaryAttack()
    local mode = self:GetNWString("shootmode")
    if mode == "Shotgun" or mode == "SMG" then
        self:Fly()
    end
    if mode == "Kittez Mode" then
       self:Zoom()
    end
end

function SWEP:Deploy()
    self.dt.reloading = false
    self.reloadtimer = 0
    self:SetNWString("shootmode","Shotgun")
    return self.BaseClass.Deploy(self)
end

function SWEP:Reload()

    if self.Owner:KeyDown(IN_USE)  then
        if  self.NextReload < CurTime() then
        self.NextReload = CurTime() + 0.3
        local mode = self:GetNWString("shootmode")
        self:SetIronsights(false)

        if mode == "Shotgun" then  self:SetNWString("shootmode","Kittez Mode")  end
        if mode == "Kittez Mode" then  self:SetNWString("shootmode","SMG") end
        if mode == "SMG" then  self:SetNWString("shootmode","Shotgun") end

        self.Weapon:SetNextPrimaryFire( CurTime() + 0.8 )
        end
        return
    end

    local mode = self:GetNWString("shootmode")
    if mode == "Shotgun" then
        self:Zoom()
        return
    end

    self.BaseClass.Reload(self)
end


if CLIENT then
    local scope = surface.GetTextureID("sprites/scope")
    function SWEP:DrawHUD( )
        local shotttext = "Shootmode: "..self:GetNWString("shootmode").."\nE+R to change"
        surface.SetFont( "ChatFont" );
        local size_x, size_y = surface.GetTextSize( shotttext );

        draw.RoundedBox( 6, ScrW( ) / 2 - size_x / 2 - 5, ScrH( ) - 100, size_x + 15, size_y + 10, Color( 0, 0, 0, 150 ) );
        draw.DrawText( shotttext , "ChatFont", ScrW( ) / 2, ScrH( ) - 100 + 5, Color( 255, 30, 30, 255 ), TEXT_ALIGN_CENTER );

        if self:GetIronsights() then
            surface.SetDrawColor( 0, 0, 0, 255 )

            local x = ScrW() / 2.0
            local y = ScrH() / 2.0
            local scope_size = ScrH()

            -- crosshair
            local gap = 80
            local length = scope_size
            surface.DrawLine( x - length, y, x - gap, y )
            surface.DrawLine( x + length, y, x + gap, y )
            surface.DrawLine( x, y - length, x, y - gap )
            surface.DrawLine( x, y + length, x, y + gap )

            gap = 0
            length = 50
            surface.DrawLine( x - length, y, x - gap, y )
            surface.DrawLine( x + length, y, x + gap, y )
            surface.DrawLine( x, y - length, x, y - gap )
            surface.DrawLine( x, y + length, x, y + gap )


            -- cover edges
            local sh = scope_size / 2
            local w = (x - sh) + 2
            surface.DrawRect(0, 0, w, scope_size)
            surface.DrawRect(x + sh - 2, 0, w, scope_size)

            surface.SetDrawColor(255, 0, 0, 255)
            surface.DrawLine(x, y, x + 1, y + 1)

            -- scope
            surface.SetTexture(scope)
            surface.SetDrawColor(255, 255, 255, 255)

            surface.DrawTexturedRectRotated(x, y, scope_size, scope_size, 0)

        end
        self.BaseClass.DrawHUD(self)
    end

    function SWEP:AdjustMouseSensitivity()
        return (self:GetIronsights() and 0.2) or nil
    end
end


SWEP.IronSightsPos      = Vector( -100, -100, -100 )
SWEP.IronSightsAng      = Vector( 2.6, 1.37, 3.5 )
SWEP.Secondary.Sound = Sound("Default.Zoom")
-- Add some zoom to ironsights for this gun
function SWEP:Zoom()
    if not self.IronSightsPos then return end
    if self.Weapon:GetNextSecondaryFire() > CurTime() then return end

    local bIronsights = not self:GetIronsights()

    self:SetIronsights( bIronsights )

    if SERVER then
        self:SetZoom(bIronsights)
    else
        self:EmitSound(self.Secondary.Sound)
    end

    self.Weapon:SetNextSecondaryFire( CurTime() + 0.3)
end




function SWEP:SetZoom(state)
    if CLIENT then
        return
    elseif IsValid(self.Owner) and self.Owner:IsPlayer() then
        if state then
            self.Owner:SetFOV(30, 0.3)
        else
            self.Owner:SetFOV(0, 0.2)
        end
    end
end