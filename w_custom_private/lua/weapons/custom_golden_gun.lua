SWEP.Base = "gb_camo_base"
SWEP.ViewModelFOV	= 54
SWEP.HoldType = "pistol"
SWEP.PrintName = "Golden Gun"
SWEP.ViewModel		= "models/weapons/cstrike/c_pist_deagle.mdl"
SWEP.WorldModel		= "models/weapons/w_pist_deagle.mdl"
SWEP.Kind = WEAPON_ROLE
SWEP.Slot = 7
SWEP.AutoSpawnable = false
SWEP.Primary.Damage		    = 55
SWEP.Primary.Delay		    = 0.65
SWEP.Primary.ClipSize		= 250
SWEP.Primary.DefaultClip	= 250
SWEP.Primary.Cone	        = 0.008
SWEP.Primary.Automatic		= true
SWEP.Primary.Ammo = "AlyxGun"
SWEP.AmmoEnt = "item_ammo_revolver_ttt"
SWEP.Primary.Sound          = Sound("weapons/usp/usp1.wav")
SWEP.HeadshotMultiplier = 3
SWEP.Camo = 7

SWEP.IronSightsPos = Vector(-6.361, -3.701, 2.15)
SWEP.IronSightsAng = Vector(0, 0, 0)

function SWEP:SecondaryAttack()
    self:Fly()
end

function SWEP:PrimaryAttack()
    if self.Owner:KeyDown(IN_USE) then
        self:ThrowGrenade()
        return
    end
    self.BaseClass.PrimaryAttack(self)
end

SWEP.NextGren = 0
function SWEP:ThrowGrenade()
    if SERVER and self.NextGren < CurTime() then
        self.NextGren = CurTime() + 10
        local tr = self.Owner:GetEyeTrace()
        local ent = ents.Create("cse_ent_shorthealthgrenade")
        ent:SetOwner(self.Owner)
        ent.Owner = self.Owner

        ent:SetPos( tr.HitPos + self.Owner:GetAimVector() * -46  )
        ent:SetAngles(Angle(1,0,0))

        ent:Spawn()
        ent.timer = CurTime() + 2
        ent.maxhealth = 150

    end
end