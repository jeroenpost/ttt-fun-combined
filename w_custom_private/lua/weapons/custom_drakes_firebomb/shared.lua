
if SERVER then
   AddCSLuaFile(  )
   --resource.AddFile("materials/vgui/ttt_fun_killicons/discomb.png")
end

SWEP.HoldType = "grenade"
SWEP.PrintName = "Drake's Firebomb fireball"
SWEP.Slot = 3
   SWEP.SlotPos	= 0


   SWEP.Icon = "vgui/ttt_fun_killicons/discomb.png"
SWEP.Primary.ClipSize		= 15
SWEP.Primary.DefaultClip	= 15
SWEP.Primary.Automatic		= false
SWEP.Primary.Ammo			= "grenade"

SWEP.Base				= "gb_base"

SWEP.WeaponID = AMMO_DISCOMB
SWEP.Kind = WEAPON_NADE

SWEP.Spawnable = true
SWEP.AdminSpawnable = true
SWEP.ViewModelFlip = false

SWEP.AutoSpawnable      = false

SWEP.UseHands			= true
SWEP.ViewModelFlip		= false
SWEP.ViewModelFOV		= 54
SWEP.ViewModel			= "models/weapons/cstrike/c_eq_fraggrenade.mdl"
SWEP.WorldModel			= "models/weapons/w_eq_fraggrenade.mdl"
SWEP.Weight			= 5

-- really the only difference between grenade weapons: the model and the thrown
-- ent.

function SWEP:PrimaryAttack()
    if !self:CanPrimaryAttack() then return end
    self:SetNextPrimaryFire(CurTime() + 1)
    self.Owner:EmitSound("WeaponFrag.Throw", 100, 100)
    self:SendWeaponAnim(ACT_VM_THROW)
    self.Owner:SetAnimation(PLAYER_ATTACK1)

    if (CLIENT) then return end

    local ent = ents.Create("custom_drakes_firebom")
    ent:SetPos(self.Owner:EyePos() + (self.Owner:GetAimVector() * 16))
    ent:SetAngles(self.Owner:GetAngles())
    ent:SetPhysicsAttacker(self.Owner)
    ent:SetOwner( self.Owner )
    ent:Spawn()

    local entobject = ent:GetPhysicsObject()
    entobject:ApplyForceCenter(self.Owner:GetAimVector():GetNormalized() * math.random(1500,2000))

    self:TakePrimaryAmmo(1)

end
