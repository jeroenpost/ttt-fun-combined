if SERVER then
    AddCSLuaFile(  )
    --resource.AddFile("materials/vgui/ttt_fun_killicons/ump45.png")
end

SWEP.HoldType = "ar2"
SWEP.PrintName = "Slow Death"
SWEP.Slot      = 8
if CLIENT then

    SWEP.Author = "GreenBlack"

    SWEP.Icon = "vgui/ttt_fun_killicons/ump45.png"
end

SWEP.Base = "aa_base"
SWEP.Spawnable = true
SWEP.AdminSpawnable = true
SWEP.Kind = WEAPON_EQUIP3

SWEP.Primary.Damage = 13
SWEP.Primary.Delay = 0.11
SWEP.Primary.Cone = 0.02
SWEP.Primary.ClipSize = 45
SWEP.Primary.ClipMax = 75
SWEP.Primary.DefaultClip = 45
SWEP.Primary.Automatic = true
SWEP.Primary.Ammo = "smg1"
SWEP.AutoSpawnable = false
SWEP.AmmoEnt = "item_ammo_smg1_ttt"
SWEP.Primary.Recoil = 1.2
SWEP.Primary.Sound = Sound( "weapons/ump45/ump45-1.wav" )
SWEP.ViewModel = "models/weapons/v_smg_ump45.mdl"
SWEP.WorldModel = "models/weapons/w_smg_ump45.mdl"

SWEP.HeadshotMultiplier = 2

SWEP.IronSightsPos = Vector( 7.3, -5.5, 3.2 )
SWEP.IronSightsAng = Vector( -1.5, 0.35, 2 )

function SWEP:SecondaryAttack()
    self:Fly()
end
SWEP.NextReload = 0
function SWEP:Reload()
    if self.NextReload > CurTime() then return end
    self.NextReload = CurTime() + 2
    if self.Owner:KeyDown(IN_USE) then
        self:ZedTime()
        return
    end
    self.BaseClass.Reload(self)
end

function SWEP:OnDrop()
    self:Remove()
end
