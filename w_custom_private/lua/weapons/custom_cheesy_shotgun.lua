SWEP.Base = "gb_camo_base"
SWEP.ViewModelFOV	= 54
SWEP.HoldType = "smg"
SWEP.PrintName = "Cheesy Shotgun"
SWEP.ViewModel		= "models/weapons/cstrike/c_shot_xm1014.mdl"
SWEP.WorldModel		= "models/weapons/w_shot_xm1014.mdl"
SWEP.AutoSpawnable = false
SWEP.Primary.Ammo = "Buckshot"
SWEP.Primary.Damage = 6
SWEP.Primary.Cone = 0.085
SWEP.Primary.Delay = 0.42
SWEP.Primary.ClipSize = 9999
SWEP.Primary.ClipMax = 9999
SWEP.Primary.DefaultClip = 9999
SWEP.Primary.Automatic = true
SWEP.Primary.NumShots = 9
SWEP.AutoSpawnable      = false
SWEP.AmmoEnt = "item_box_buckshot_ttt"
SWEP.Primary.Sound			= Sound( "Weapon_XM1014.Single" )
SWEP.Primary.Recoil			= 7
SWEP.Camo = 31
SWEP.CustomCamo = true
SWEP.HoldType = "shotgun"
SWEP.Slot = 2
SWEP.Kind = WEAPON_HEAVY
SWEP.DrawCrosshair      = true

if CLIENT then

    SWEP.Author = "GreenBlack"

    SWEP.Icon = "vgui/ttt_fun_killicons/xm1014.png"
end


SWEP.IronSightsPos = Vector(-6.881, -9.214, 2.66)
SWEP.IronSightsAng = Vector(-0.101, -0.7, -0.201)

SWEP.reloadtimer = 0

function SWEP:SetupDataTables()
    self:DTVar("Bool", 0, "reloading")

    return self.BaseClass.SetupDataTables(self)
end

function SWEP:Reload()

    if self.Owner:KeyDown(IN_USE) and self.NextReload < CurTime() then
        self.NextReload = CurTime() + 0.3
        local mode = self:GetNWString("shootmode")
        self:SetIronsights(false)

        if mode == "Shotgun" then  self:SetNWString("shootmode","SMG")  end
        if mode == "SMG" then  self:SetNWString("shootmode","Sniper") end
        if mode == "Sniper" then  self:SetNWString("shootmode","ZombieInfect") end
        if mode == "ZombieInfect" then  self:SetNWString("shootmode","Shotgun")  end

        self.Weapon:SetNextPrimaryFire( CurTime() + 0.8 )
        return
    end

    --if self:GetNWBool( "reloading", false ) then return end
    if self.dt.reloading then return end

    if not IsFirstTimePredicted() then return end

    if self:Clip1() < self.Primary.ClipSize and self.Owner:GetAmmoCount( self.Primary.Ammo ) > 0 then

        if self:StartReload() then
            return
        end
    end

end

function SWEP:StartReload()
    --if self:GetNWBool( "reloading", false ) then
    if self.dt.reloading then
        return false
    end

    self:SetIronsights( false )

    if not IsFirstTimePredicted() then return false end

    self:SetNextPrimaryFire( CurTime() + self.Primary.Delay )

    local ply = self.Owner

    if not ply or ply:GetAmmoCount(self.Primary.Ammo) <= 0 then
        return false
    end

    local wep = self

    if wep:Clip1() >= self.Primary.ClipSize then
        return false
    end

    wep:SendWeaponAnim(ACT_SHOTGUN_RELOAD_START)

    self.reloadtimer =  CurTime() + wep:SequenceDuration()

    --wep:SetNWBool("reloading", true)
    self.dt.reloading = true

    return true
end

function SWEP:PerformReload()
    local ply = self.Owner

    -- prevent normal shooting in between reloads
    self:SetNextPrimaryFire( CurTime() + self.Primary.Delay )

    if not ply or ply:GetAmmoCount(self.Primary.Ammo) <= 0 then return end

    if self:Clip1() >= self.Primary.ClipSize then return end

    self.Owner:RemoveAmmo( 1, self.Primary.Ammo, false )
    self:SetClip1( self:Clip1() + 1 )

    self:SendWeaponAnim(ACT_VM_RELOAD)

    self.reloadtimer = CurTime() + self:SequenceDuration()
end

function SWEP:FinishReload()
    self.dt.reloading = false
    self:SendWeaponAnim(ACT_SHOTGUN_RELOAD_FINISH)

    self.reloadtimer = CurTime() + self:SequenceDuration()
end

function SWEP:PrimaryAttack()
    local mode = self:GetNWString("shootmode")
    if mode == "Shotgun" then
        self:SetNextPrimaryFire( CurTime() + 0.4 )
        self.Weapon:EmitSound( self.Primary.Sound, 40 )
        local soundfile = gb_config.websounds.."cheese_everyone.mp3"
        gb_StopSoundFromServer("cheesefor")
         gb_PlaySoundFromServer(soundfile, self.Owner, nil, "cheesefor" )
        self.Primary.Damage = 6
        self.Primary.Recoil = 4
        self.Primary.NumShots = 8
        self.Primary.Cone = 0.085
        self:ShootBullet( self.Primary.Damage, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )
        self:TakePrimaryAmmo( 1 )
    end
    if mode == "SMG" then
        self:SetNextPrimaryFire( CurTime() + 0.1 )
        self.Weapon:EmitSound( self.Primary.Sound, self.Primary.SoundLevel )
        self.Primary.Damage = 11
        self.Primary.Recoil = 0.3
        self.Primary.NumShots = 1
        self.Primary.Cone = 0.009
        self:ShootBullet( self.Primary.Damage, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )
        self:TakePrimaryAmmo( 1 )
    end
    if mode == "Sniper" then
        self:SetNextPrimaryFire( CurTime() + 0.9 )
        self.Weapon:EmitSound( "vo/npc/male01/question06.wav", self.Primary.SoundLevel )
        self.Primary.Damage = 60
        self.Primary.Recoil = 5
        self.Primary.NumShots = 1
        self.Primary.Cone = 0.0001
        self:ShootBullet( self.Primary.Damage, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )
        self:TakePrimaryAmmo( 1 )
    end
    if mode == "ZombieInfect" then
        self:SetNextPrimaryFire( CurTime() + 0.5 )
        self.Weapon:EmitSound( self.Primary.Sound, self.Primary.SoundLevel )
        if self.hadzombie then    self.Owner:PrintMessage( HUD_PRINTCENTER, "Already used" ) return end
        self:ShootBullet2( self.Primary.Damage, self.Primary.Recoil, self.Primary.NumShots, 0.001 )
    end
end


function SWEP:CreateZombie( ply, owner )

    if not IsValid( ply ) or not IsValid( owner ) then print("Not valid zombie") return end

    local ent = ply
    local weapon = owner:GetActiveWeapon()
    local edata = EffectData()

    edata:SetEntity(ent)
    edata:SetMagnitude(3)
    edata:SetScale(2)

    util.Effect("TeslaHitBoxes", edata)

    if SERVER and ent:IsPlayer() then

        local modelsss = { "npc_zombie", "npc_antlionguard", "npc_fastzombie", "npc_poisonzombie" }
        local randmodel = table.Random( modelsss )
        self.hadzombie =1
        local oldPos = ent:GetPos()

        timer.Simple(4.9, function()
            if IsValid( ent ) then
                spot = ent:GetPos()
            else
                spot = oldPos
            end
            zed = ents.Create(randmodel)
        end)

        timer.Simple(5,function()
            if SERVER then
                if IsValid( ent ) then
                    ent:TakeDamage( 2000, owner, weapon )
                end
                --local ply = self.Owner -- The first entity is always the first player
                zed:SetPos(spot) -- This positions the zombie at the place our trace hit.
                zed:SetHealth( 100 )
                zed:SetMaterial("camos/custom_camo31")
                zed:Spawn() -- This method spawns the zombie into the world, run for your lives! ( or just crowbar it dead(er) )
                zed:SetHealth( 100 )
            end
        end) --20
        timer.Create("infectedDamage",0.25,20,function()
            if IsValid( ent ) then
                ent:TakeDamage( 3, owner, weapon )
            end
        end)

    end


end

SWEP.IronSightsPos      = Vector( -100, -100, -100 )
SWEP.IronSightsAng      = Vector( 2.6, 1.37, 3.5 )
SWEP.Secondary.Sound = Sound("Default.Zoom")
-- Add some zoom to ironsights for this gun
function SWEP:Zoom()
    if not self.IronSightsPos then return end
    if self.Weapon:GetNextSecondaryFire() > CurTime() then return end

    local bIronsights = not self:GetIronsights()

    self:SetIronsights( bIronsights )

    if SERVER then
        self:SetZoom(bIronsights)
    else
        self:EmitSound(self.Secondary.Sound)
    end

    self.Weapon:SetNextSecondaryFire( CurTime() + 0.3)
end




function SWEP:SetZoom(state)
    if CLIENT then
        return
    elseif IsValid(self.Owner) and self.Owner:IsPlayer() then
        if state then
            self.Owner:SetFOV(30, 0.3)
        else
            self.Owner:SetFOV(0, 0.2)
        end
    end
end

function SWEP:ShootBullet2( dmg, recoil, numbul, cone )

    local sights = self:GetIronsights()

    numbul = numbul or 1
    cone   = cone   or 0.01

    -- 10% accuracy bonus when sighting
    cone = sights and (cone * 0.9) or cone

    local bullet = {}
    bullet.Num    = numbul
    bullet.Src    = self.Owner:GetShootPos()
    bullet.Dir    = self.Owner:GetAimVector()
    bullet.Spread = Vector( cone, cone, 0 )
    bullet.Tracer = 4
    bullet.Force  = 5
    bullet.Damage = dmg

    local soundfile = gb_config.websounds.."weapons/fart.mp3"
    gb_PlaySoundFromServer(soundfile, self.Owner)

    bullet.Callback = function(att, tr, dmginfo)
        if SERVER or (CLIENT and IsFirstTimePredicted()) then

            if (not tr.HitWorld)  then
                local pl2 = 0

                    pl2 = tr.Entity
                self.hadzombie = true


                self:CreateZombie( pl2, self.Owner )
            end
        end
    end
    self.Owner:FireBullets( bullet )
    self.Weapon:SendWeaponAnim(self.PrimaryAnim)

    -- Owner can die after firebullets, giving an error at muzzleflash
    if not IsValid(self.Owner) or not self.Owner:Alive() then return end

    self.Owner:MuzzleFlash()
    self.Owner:SetAnimation( PLAYER_ATTACK1 )

    if self.Owner:IsNPC() then return end

    if ((game.SinglePlayer() and SERVER) or
            ((not game.SinglePlayer()) and CLIENT and IsFirstTimePredicted() )) then

        -- reduce recoil if ironsighting
        recoil = sights and (recoil * 0.5) or recoil

    end

end

function SWEP:CanPrimaryAttack()
    if self:Clip1() <= 0 then
        self:EmitSound( "Weapon_Shotgun.Empty" )
        self:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
        return false
    end
    return true
end

function SWEP:Think()
    if self.dt.reloading and IsFirstTimePredicted() then
        if self.Owner:KeyDown(IN_ATTACK) then
            self:FinishReload()
            return
        end

        if self.reloadtimer <= CurTime() then

            if self.Owner:GetAmmoCount(self.Primary.Ammo) <= 0 then
                self:FinishReload()
            elseif self:Clip1() < self.Primary.ClipSize then
                self:PerformReload()
            else
                self:FinishReload()
            end
            return
        end
    end
end


function SWEP:Deploy()
    self.dt.reloading = false
    self.reloadtimer = 0
    self:SetNWString("shootmode","Shotgun")
    return self.BaseClass.Deploy(self)
end

-- The shotgun's headshot damage multiplier is based on distance. The closer it
-- is, the more damage it does. This reinforces the shotgun's role as short
-- range weapon by reducing effectiveness at mid-range, where one could score
-- lucky headshots relatively easily due to the spread.
function SWEP:GetHeadshotMultiplier(victim, dmginfo)
    local att = dmginfo:GetAttacker()
    if not IsValid(att) then return 3 end

    local dist = victim:GetPos():Distance(att:GetPos())
    local d = math.max(0, dist - 140)

    -- decay from 3.1 to 1 slowly as distance increases
    return 1 + math.max(0, (2.1 - 0.002 * (d ^ 1.25)))
end

function SWEP:SecondaryAttack()
    local mode = self:GetNWString("shootmode")

    if mode == "Sniper" then
        self:Zoom()
        return
    end
    self:Fly()
end

if CLIENT then
    local scope = surface.GetTextureID("sprites/scope")
    function SWEP:DrawHUD( )
        local shotttext = "Shootmode: "..self:GetNWString("shootmode").."\nE+R to change"
        surface.SetFont( "ChatFont" );
        local size_x, size_y = surface.GetTextSize( shotttext );

        draw.RoundedBox( 6, ScrW( ) / 2 - size_x / 2 - 5, ScrH( ) - 100, size_x + 15, size_y + 10, Color( 0, 0, 0, 150 ) );
        draw.DrawText( shotttext , "ChatFont", ScrW( ) / 2, ScrH( ) - 100 + 5, Color( 255, 30, 30, 255 ), TEXT_ALIGN_CENTER );

        if self:GetIronsights() then
            surface.SetDrawColor( 0, 0, 0, 255 )

            local x = ScrW() / 2.0
            local y = ScrH() / 2.0
            local scope_size = ScrH()

            -- crosshair
            local gap = 80
            local length = scope_size
            surface.DrawLine( x - length, y, x - gap, y )
            surface.DrawLine( x + length, y, x + gap, y )
            surface.DrawLine( x, y - length, x, y - gap )
            surface.DrawLine( x, y + length, x, y + gap )

            gap = 0
            length = 50
            surface.DrawLine( x - length, y, x - gap, y )
            surface.DrawLine( x + length, y, x + gap, y )
            surface.DrawLine( x, y - length, x, y - gap )
            surface.DrawLine( x, y + length, x, y + gap )


            -- cover edges
            local sh = scope_size / 2
            local w = (x - sh) + 2
            surface.DrawRect(0, 0, w, scope_size)
            surface.DrawRect(x + sh - 2, 0, w, scope_size)

            surface.SetDrawColor(255, 0, 0, 255)
            surface.DrawLine(x, y, x + 1, y + 1)

            -- scope
            surface.SetTexture(scope)
            surface.SetDrawColor(255, 255, 255, 255)

            surface.DrawTexturedRectRotated(x, y, scope_size, scope_size, 0)

        end
    end

    function SWEP:AdjustMouseSensitivity()
        return (self:GetIronsights() and 0.2) or nil
    end
end
