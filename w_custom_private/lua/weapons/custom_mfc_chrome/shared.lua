 if SERVER then
   AddCSLuaFile(  )
end
if CLIENT then
   
   SWEP.Author 		= "GreenBlack"   

   SWEP.SlotPos 	= 2
   SWEP.Icon = "vgui/ttt_fun_killicons/golden_deagle.png"
end
SWEP.PrintName	= "MFC's Chrome"
SWEP.Slot		= 2
SWEP.HoldType 		= "pistol"
SWEP.Base			= "aa_base"
SWEP.Spawnable 		= true
SWEP.AdminSpawnable = true
SWEP.Kind 			= WEAPON_HEAVY
SWEP.Primary.Ammo   = "AlyxGun" 
SWEP.Primary.Recoil	= 1
SWEP.Primary.Damage = 45
SWEP.Primary.Delay 	= 0.6
SWEP.Primary.Cone 	= 0.01
SWEP.Primary.ClipSize 		= 50
SWEP.Primary.ClipMax 		= 120
SWEP.Primary.DefaultClip 	= 50
SWEP.Primary.NumShots		= 1
SWEP.Primary.Automatic 	= true
SWEP.HeadshotMultiplier = 3
SWEP.AutoSpawnable      = false
SWEP.AmmoEnt 		= "item_ammo_revolver_ttt"
--SWEP.Primary.Sound	= "gdeagle/gdeagle-1.mp3"
SWEP.Primary.Sound	= Sound( "Weapon_USP.SilencedShot" )
SWEP.Primary.SoundLevel = 400
SWEP.ViewModel= "models/weapons/v_pist_gbagle.mdl"
SWEP.WorldModel= "models/weapons/w_pist_gbagle.mdl"
SWEP.ShowWorldModel = true
SWEP.ShowViewModel = true

SWEP.LimitedStock = true
SWEP.InLoadoutFor = nil
SWEP.AllowDrop 	  = true
SWEP.IronSightsPos = Vector(1.14, -3.951, 0.736)
SWEP.IronSightsAng = Vector(0, 0, 0)
SWEP.IronSightsPos 	= Vector( 3.8, -1, 1.6 )
--SWEP.ViewModelFOV = 49


function SWEP:Deploy()
   self.Weapon:EmitSound("gdeagle/gdeagledeploy.mp3")

end

function SWEP:Reload()
 
	if self.ReloadingTime and CurTime() <= self.ReloadingTime then return end
	self:SetIronsights( false )
	if ( self:Clip1() < self.Primary.ClipSize and self.Owner:GetAmmoCount( self.Primary.Ammo ) > 0 ) then
 
	self:DefaultReload( ACT_VM_RELOAD )
	local AnimationTime = self.Owner:GetViewModel():SequenceDuration()
	self.ReloadingTime = CurTime() + AnimationTime
	self:SetNextPrimaryFire(CurTime() + AnimationTime)
	self:SetNextSecondaryFire(CurTime() + AnimationTime)
	self.Weapon:EmitSound("gdeagle/gdeaglereload.mp3")
 
	end

end

SWEP.usedBlind = 0

function SWEP:PrimaryAttack(worldsnd)
   self.Weapon:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
   self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
   if not self:CanPrimaryAttack() then return end
   if not worldsnd then
      self.Weapon:EmitSound( self.Primary.Sound, self.Primary.SoundLevel )
   elseif SERVER then
      WorldSound(self.Primary.Sound, self:GetPos(), self.Primary.SoundLevel)
   end
   self:ShootBullet( self.Primary.Damage, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )
   
   if ( self.Owner:IsValid() ) then
		local tr = self.Owner:GetEyeTrace()
		local effectdata = EffectData()
		effectdata:SetOrigin(tr.HitPos)
		effectdata:SetNormal(tr.HitNormal)
		effectdata:SetScale(1)
		-- util.Effect("effect_mad_ignition", effectdata)
		util.Decal("FadingScorch", tr.HitPos + tr.HitNormal, tr.HitPos - tr.HitNormal)
        if tr.Entity:IsPlayer() and self.usedBlind < 2 then
		    self:Freeze();
             self:Blind();
             self.usedBlind =  self.usedBlind+1
            self.Weapon:SetNextPrimaryFire( CurTime() + 1.5 )
        end
				local tracedata = {}
				tracedata.start = tr.HitPos
				tracedata.endpos = Vector(tr.HitPos.x, tr.HitPos.y, tr.HitPos.z - 10)
				tracedata.filter = tr.HitPos
				local tracedata = util.TraceLine(tracedata)
				if SERVER then
				if tracedata.HitWorld then
					local flame = ents.Create("env_fire");
					flame:SetPos(tr.HitPos + Vector( 0, 0, 1 ));
					flame:SetKeyValue("firesize", "10");
					flame:SetKeyValue("fireattack", "10");
					flame:SetKeyValue("StartDisabled", "0");
					flame:SetKeyValue("health", "10");
					flame:SetKeyValue("firetype", "0");
					flame:SetKeyValue("damagescale", "5");
					flame:SetKeyValue("spawnflags", "128");
					flame:SetPhysicsAttacker(self.Owner)
					flame:SetOwner( self.Owner )
					flame:Spawn();
					flame:Fire("StartFire",0);
				end
				end
	   self:TakePrimaryAmmo( 1 )
	   local owner = self.Owner   
	   --if not ValidEntity(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end
	   
	   owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )
   end
end


function SWEP:SecondaryAttack()
    local tr = self.Owner:GetEyeTrace()
    self.Weapon:SetNextSecondaryFire( CurTime() + 0.2 )
    if tr.Entity:IsPlayer() and self.usedBlind < 2 then
        self.Weapon:SetNextSecondaryFire( CurTime() + 1.5 )
        self:Freeze();
        self:Blind();
        self.usedBlind =  self.usedBlind+1
        self.Weapon:SetNextPrimaryFire( CurTime() + 1.5 )
    end
end
