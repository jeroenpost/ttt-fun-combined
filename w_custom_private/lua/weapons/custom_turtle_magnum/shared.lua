if SERVER then
   AddCSLuaFile(  )
end
   
SWEP.HoldType = "pistol"
SWEP.PrintName = "Blackhearted magnum"
SWEP.Slot = 1

if CLIENT then
   			
   SWEP.Author = "JBoytheGreat"
   SWEP.ViewModelFlip = false

   SWEP.Icon = "vgui/ttt_fun_killicons/magnum.png"

end

function SWEP:OnDrop()
    self:Remove()
end


SWEP.Base = "weapon_tttbase"
SWEP.Spawnable = true
SWEP.AdminSpawnable = true
SWEP.Kind = WEAPON_EQUIP3
SWEP.WeaponID = AMMO_DEAGLE

SWEP.Primary.Ammo = "AlyxGun"
SWEP.Primary.Recoil = 0.5
SWEP.Primary.Damage = 55
SWEP.Primary.Delay = 0.35
SWEP.Primary.Cone = 0.002
SWEP.Primary.ClipSize = 25
SWEP.Primary.ClipMax = 50
SWEP.Primary.DefaultClip = 25
SWEP.Primary.Automatic = true

SWEP.HeadshotMultiplier = 3

SWEP.AutoSpawnable = false
SWEP.AmmoEnt = "item_ammo_revolver_ttt"
SWEP.Primary.Sound = Sound("Weapon_357.Single")
SWEP.ViewModel = "models/weapons/v_357.mdl"
SWEP.WorldModel = "models/weapons/w_357.mdl"

SWEP.IronSightsPos = Vector( -5.47, 2.92, 2.55 )
SWEP.IronSightsAng = Vector( 0.55, 0.55, 0.00 )