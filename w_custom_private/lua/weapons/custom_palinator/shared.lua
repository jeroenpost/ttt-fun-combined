if SERVER then
    AddCSLuaFile(  )
end
SWEP.PrintName = "The Palinator"
if CLIENT then

    SWEP.Slot = 0
    SWEP.SlotPos = 5
    SWEP.DrawAmmo = false
    SWEP.DrawCrosshair = false
end

SWEP.Base = "aa_base"
SWEP.Kind = WEAPON_MELEE
SWEP.Author = "DarkRP Developers"
SWEP.Instructions = "Left click to discipline\nRight click to kill"
SWEP.Contact = ""
SWEP.Purpose = ""
SWEP.IconLetter = ""

SWEP.ViewModelFOV = 62
SWEP.ViewModelFlip = false
SWEP.AnimPrefix = "stunstick"

SWEP.Spawnable = true
SWEP.AdminOnly = true
SWEP.Category = "DarkRP (Utility)"

SWEP.NextStrike = 0

SWEP.ViewModel = Model("models/weapons/v_stunbaton.mdl")
SWEP.WorldModel = Model("models/weapons/w_stunbaton.mdl")

SWEP.Sound = Sound("weapons/stunstick/stunstick_swing1.wav")

SWEP.Primary.ClipSize = -1
SWEP.Primary.DefaultClip = 0
SWEP.Primary.Automatic = false
SWEP.Primary.Ammo = ""

SWEP.Secondary.ClipSize = -1
SWEP.Secondary.DefaultClip = 0
SWEP.Secondary.Automatic = false
SWEP.Secondary.Ammo = ""

function SWEP:Initialize()
    self:SetHoldType("normal")

    self.Hit = {
        Sound("weapons/stunstick/stunstick_impact1.wav"),
        Sound("weapons/stunstick/stunstick_impact2.wav")
    }

    self.FleshHit = {
        Sound("weapons/stunstick/stunstick_fleshhit1.wav"),
        Sound("weapons/stunstick/stunstick_fleshhit2.wav")
    }
end

function SWEP:OnRemove()
    if SERVER then
        self:SetColor(Color(255,255,255,255))
        self:SetMaterial("")
        timer.Stop(self:GetClass() .. "_idle" .. self:EntIndex())
    elseif CLIENT and IsValid(self.Owner) and IsValid(self.Owner:GetViewModel()) then
        self.Owner:GetViewModel():SetColor(Color(255,255,255,255))
        self.Owner:GetViewModel():SetMaterial("")
    end
end

function SWEP:DoFlash(ply)
    if not IsValid(ply) or not ply:IsPlayer() then return end
    umsg.Start("StunStickFlash", ply)
    umsg.End()
end

function SWEP:SecondaryAttack()
    self:Fly()
end

function SWEP:PrimaryAttack()
    if CurTime() < self.NextStrike then return end

    --self:NewSetHoldType("melee")


    self.NextStrike = CurTime() + 0.30 -- Actual delay is set later.

    if CLIENT then return end

    timer.Stop(self:GetClass() .. "_idle" .. self:EntIndex())
    local vm = self.Owner:GetViewModel()
    if IsValid(vm) then
        vm:ResetSequence(vm:LookupSequence("idle01"))
        timer.Simple(0, function()
            if not IsValid(self) or not IsValid(self.Owner) or not IsValid(self.Owner:GetActiveWeapon()) or self.Owner:GetActiveWeapon() ~= self then return end
            self.Owner:SetAnimation(PLAYER_ATTACK1)

            if IsValid(self.Weapon) then
                self.Weapon:EmitSound(self.Sound)
            end

            local vm = self.Owner:GetViewModel()
            if not IsValid(vm) then return end
            vm:ResetSequence(vm:LookupSequence("attackch"))
            vm:SetPlaybackRate(1 + 1/3)
            local duration = vm:SequenceDuration() / vm:GetPlaybackRate()
            timer.Create(self:GetClass() .. "_idle" .. self:EntIndex(), duration, 1, function()
                if not IsValid(self) or not IsValid(self.Owner) then return end
                local vm = self.Owner:GetViewModel()
                if not IsValid(vm) then return end
                vm:ResetSequence(vm:LookupSequence("idle01"))
            end)
            self.NextStrike = CurTime() + duration
        end)
    end

    local trace = self.Owner:GetEyeTrace()

    if not IsValid(trace.Entity) or (self.Owner:EyePos():Distance(trace.Entity:GetPos()) > 180) then return end

       if trace.Entity:IsPlayer() or trace.Entity:IsNPC() or trace.Entity:IsVehicle() then
           trace.Entity:TakeDamage(35, self.Owner, self)
        self.DoFlash(self, trace.Entity)

        self.Owner:EmitSound(self.FleshHit[math.random(1,#self.FleshHit)])
    else
        self.Owner:EmitSound(self.Hit[math.random(1,#self.Hit)])

    end


end

SWEP.NextBlind = 0
function SWEP:Reload()

    if self.Owner:KeyDown(IN_USE) then
        self:Cannibal()
        return
    end

    --self:NewSetHoldType("melee")
    timer.Destroy("rp_stunstick_threaten")
    timer.Create("rp_stunstick_threaten", 1, 1, function()
        if not IsValid(self) then return end
        --self:NewSetHoldType("normal")
    end)

    if not SERVER then return end

    local trace = self.Owner:GetEyeTrace()

    if self.LastReload and self.LastReload > CurTime() - 0.1 then self.LastReload = CurTime() return end
    self.LastReload = CurTime()
    self.Owner:EmitSound("weapons/stunstick/spark"..math.random(1,3)..".wav")

    if self.NextBlind > CurTime() or not IsValid(trace.Entity) or not trace.Entity:IsPlayer() or (self.Owner:EyePos():Distance(trace.Entity:GetPos()) > 300) then return end

    self.NextBlind = CurTime() + 10
    local person = trace.Entity;
    self.DoFlash(self, person)
    timer.Create("stunBlind"..self.Owner:SteamID(),0.05,50,function()
        self.DoFlash(self, person)
    end)


end

if CLIENT then
    local function StunStickFlash()
        local alpha = 255
        hook.Add("HUDPaint", "RP_StunstickFdsflash", function()
            alpha = Lerp(0.05, alpha, 0)
            surface.SetDrawColor(255,255,255,alpha)
            surface.DrawRect(0,0,ScrW(), ScrH())

            if math.Round(alpha) == 0 then
                hook.Remove("HUDPaint", "RP_StunstickFdsflash")
            end
        end)
    end
    usermessage.Hook("StunStickFlash", StunStickFlash)
end