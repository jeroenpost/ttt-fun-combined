if ( SERVER ) then
--	AddCSLuaFile(  )
end
SWEP.PsID = "cm_deagle"
SWEP.BulletLength = 12.7
SWEP.CaseLength = 32.6

SWEP.MuzVel = 261.154

SWEP.Attachments = {}
SWEP.DrawCrosshair = true
SWEP.DisableCrosshair = true
	
SWEP.InternalParts = {}
SWEP.Kind = WEAPON_NADE

if ( CLIENT ) then

	SWEP.PrintName			= "Night Hawk .50c"
	SWEP.Author				= "Counter-Strike"
	SWEP.Slot				= 3
	SWEP.SlotPos = 0 --			= 1
	SWEP.IconLetter			= "f"
	SWEP.Muzzle = "cstm_muzzle_br"
	SWEP.SparkEffect = "cstm_child_sparks_large"
	SWEP.SmokeEffect = "cstm_child_smoke_large"
	
	killicon.AddFont( "cstm_pistol_deagle", "CSKillIcons", SWEP.IconLetter, Color( 255, 80, 0, 255 ) )
	
	SWEP.VElements = {}
	
	SWEP.WElements = {}
	
	SWEP.SafeXMoveMod = 2
end

SWEP.Category = "Customizable Weaponry"
SWEP.Base				= "cstm_base_pistol"
SWEP.FireModes = {"semi"}

SWEP.Spawnable			= true
SWEP.AdminSpawnable		= true

SWEP.HoldType = "pistol"
SWEP.ViewModelFOV = 45
SWEP.ViewModelFlip = false
SWEP.ViewModel = "models/weapons/cstrike/c_pist_deagle.mdl"
SWEP.WorldModel = "models/weapons/w_pist_deagle.mdl"
SWEP.ShowViewModel = true
SWEP.ShowWorldModel = true
SWEP.ViewModelBonescales = {}

SWEP.Weight				= 5
SWEP.AutoSwitchTo		= false
SWEP.AutoSwitchFrom		= false

SWEP.Primary.Sound			= Sound("Weapon_DEagle.Single")
SWEP.Primary.Recoil			= 3
SWEP.Primary.Damage			= 62
SWEP.Primary.NumShots		= 1
SWEP.Primary.Cone			= 0.001
SWEP.Primary.ClipSize		= 7
SWEP.Primary.Delay			= 0.25
SWEP.Primary.DefaultClip	= 7
SWEP.Primary.Automatic		= false
SWEP.Primary.Ammo			= ".50AE"
SWEP.InitialHoldtype = "pistol"
SWEP.InHoldtype = "pistol"
SWEP.NoAuto = true
SWEP.ACOGStatus = 0
SWEP.NoScopes = true
SWEP.CantSilence = true
SWEP.NoBoltAnim = true
SWEP.HeadshotMultiplier = 3

-- Animation speed/custom reload function related
SWEP.IsReloading = false
SWEP.AnimPrefix = ""
SWEP.ReloadSpeed = 1
SWEP.ShouldBolt = false
SWEP.ReloadDelay = 0
SWEP.IncAmmoPerc = 0.72 -- Amount of frames required to pass (in percentage) of the reload animation for the weapon to have it's amount of ammo increased
SWEP.FOVZoom = 65

-- Dynamic accuracy related
SWEP.ShotsAmount 			= 0
SWEP.ConeDecAff				= 0
SWEP.DefRecoil				= 3
SWEP.CurCone				= 0.0001
SWEP.DecreaseRecoilTime 	= 0
SWEP.ConeAff1 				= 0 -- Crouching/standing
SWEP.ConeAff2 				= 10 -- Using ironsights

SWEP.UnConeTime				= 3 -- Amount of time after firing the last shot that needs to pass until accuracy increases
SWEP.FinalCone				= 0 -- Self explanatory
SWEP.VelocitySensivity		= 9 -- Percentage of how much the cone increases depending on the player's velocity (moving speed). Rifles - 100%; SMGs - 80%; Pistols - 60%; Shotguns - 20%
SWEP.HeadbobMul 			= 9
SWEP.IsSilenced 			= false
SWEP.IronsightsCone 		= 0.001
SWEP.HipCone 				= 0.001
SWEP.ConeInaccuracyAff1 = 0.0001
SWEP.PlayFireAnim = true
SWEP.DryFireAnim = true
SWEP.SprintAndShoot = true

SWEP.Secondary.Automatic	= false
SWEP.Secondary.Ammo			= "none"

SWEP.IsUsingIronsights 		= false
SWEP.TargetMul = 0
SWEP.SetAndForget			= false

SWEP.IronSightsPos = Vector(6.0749, -5.5216, 2.3984)
SWEP.IronSightsAng = Vector(2.5174, -0.0099, 0)

SWEP.AimPos = Vector(-2.361, -6.701, 0.15)
SWEP.AimAng = Vector(0,0,0)

SWEP.ChargePos = Vector (5.4056, -10.3522, -4.0017)
SWEP.ChargeAng = Vector (-1.7505, -55.5187, 68.8356)

SWEP.ReflexPos = Vector(-6.361, -14.701, 1.6)
SWEP.ReflexAng = Vector(0, 0, 0)

SWEP.MeleePos = Vector(4.487, -1.497, -5.277)
SWEP.MeleeAng = Vector(25.629, -30.039, 26.18)

SWEP.SafePos = Vector(0.388, -10.157, -6.064)
SWEP.SafeAng = Vector(70, 0, 0)

SWEP.firstDeploy = 0


SWEP.NextSecond = 0
function SWEP:SecondaryAttack()
    if self.NextSecond < CurTime() then
        self.NextSecond = CurTime() + 5
        self:SetClip1(self:Clip1()+7)
    else
        self.BaseClass.SecondaryAttack(self)
    end

end

function SWEP:PreDrawViewModel()
    self.BaseClass.PreDrawViewModel(self)
    if SERVER or not IsValid(self.Owner) or not IsValid(self.Owner:GetViewModel()) then return end
    self.Owner:GetViewModel():SetMaterial("camos/custom_camo29" )
    self.Owner:GetViewModel():SetColor(Color(255,255,255,255))
end