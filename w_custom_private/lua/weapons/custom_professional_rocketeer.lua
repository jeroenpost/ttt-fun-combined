if SERVER then
   AddCSLuaFile( )
   --resource.AddFile("materials/vgui/ttt_fun_killicons/m3.png")
end

include "weaponfunctions/teleport.lua"

SWEP.HoldType = "pistol"
SWEP.PrintName = "Professional Rocketeer"
SWEP.Slot = 6
if CLIENT then
   
   SWEP.Author = "GreenBlack"
   
   SWEP.Icon = "vgui/ttt_fun_killicons/rpg.png"
end

SWEP.Base = "aa_base"
SWEP.Spawnable = true
SWEP.AdminSpawnable = true
SWEP.Kind = WEAPON_ROLE

SWEP.Primary.Ammo = "Battery"
SWEP.Primary.Damage = 5
SWEP.Primary.Cone = 0
SWEP.Primary.Delay = 1
SWEP.Primary.ClipSize = 10
SWEP.Primary.ClipMax = 12
SWEP.Primary.DefaultClip = 20
SWEP.Primary.Automatic = false
SWEP.Primary.NumShots = 20
SWEP.HeadshotMultiplier = 1.2
SWEP.AutoSpawnable = false
SWEP.AmmoEnt = "item_box_buckshot_ttt"
SWEP.UseHands			= true
SWEP.ViewModelFlip		= false
SWEP.ViewModelFOV		= 70
SWEP.ViewModel  = "models/weapons/c_rpg.mdl"
SWEP.WorldModel = "models/weapons/w_rocket_launcher.mdl"

SWEP.Primary.Sound = "ambient/explosions/explode_4.wav"
SWEP.Primary.Recoil = 0.1
SWEP.reloadtimer = 0


SWEP.IronSightsPos = Vector(5.74, -2.19, 3.28)
SWEP.IronSightsAng = Vector(0.547, 0, 0)


function SWEP:Reload()
    if self.nextReload and self.nextReload > CurTime() then return end
    self.nextReload = CurTime() + 0.5

    local soundfile = gb_config.websounds.."pirate2.mp3"
    gb_PlaySoundFromServer(soundfile, self.Owner)

    if !self.Owner:KeyDown(IN_USE) then
    self.Weapon:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
    self:DoTeleport()
    return
    end
    if self.Owner:KeyDown(IN_USE) then
        self.Weapon:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
        self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
        self:SetTeleportMarker()
        return
    end
end


function SWEP:PrimaryAttack(worldsnd)
    self.Weapon:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
    if self:Clip1() < 1 then
        self:EmitSound("weapons/slam/mine_mode.wav")
        return
    end
    self:rFly()
    self:SphereExplosion( 0 )
   self:TakePrimaryAmmo(1)
end



-- COLOR
function SWEP:Deploy()
    self:SetColorAndMaterial(Color(255,255,255,255),"gb/gold/gold_player");
    local ply = self.Owner
    if ply:GetNWInt("walkspeed") < 350 then
        ply:SetNWInt("walkspeed",350)
    end
    if ply:GetNWInt("runspeed") < 450 then
        ply:SetNWInt("runspeed", 450)
    end
    self.Owner:SetNWFloat("w_stamina", 1)
end


function SWEP:PreDrop()
    self:ColorReset()
end


function SWEP:Holster()
    self:ColorReset()
    return true
end

function SWEP:OnRemove()
    self:ColorReset()
end
function SWEP:OnDrop()
    self:ColorReset()

end

function SWEP:PreDrawViewModel()

    if SERVER or not IsValid(self.Owner) or not IsValid(self.Owner:GetViewModel()) then return end
    self.Owner:GetViewModel():SetColor(Color(255,255,255,255))
    self.Owner:GetViewModel():SetMaterial("gb/gold/gold_player")

end
-- END COLORD:


