
if SERVER then
   AddCSLuaFile(  )
   --resource.AddFile("materials/vgui/ttt_fun_killicons/fiveseven.png")
end
   
SWEP.HoldType = "pistol"
   

if CLIENT then
   
   
   SWEP.Author = "GreenBlack"
   SWEP.Icon = "vgui/ttt_fun_killicons/fiveseven.png"
end
SWEP.Slot = 7
SWEP.PrintName = "FIVE-SEVENS"
SWEP.Kind = WEAPON_EQUIP2

SWEP.Base = "weapon_tttbase"
SWEP.Primary.Recoil	= 2.2
SWEP.Primary.Damage = 40
SWEP.Primary.Delay = 0.3
SWEP.Primary.Cone = 0.02
SWEP.Primary.ClipSize = 12
SWEP.Primary.Automatic = true
SWEP.Primary.DefaultClip = 50
SWEP.Primary.ClipMax = 50
SWEP.Primary.Ammo = "XBowBolt"
SWEP.AutoSpawnable = false
SWEP.AmmoEnt = ""

SWEP.HeadshotMultiplier = 0.8

SWEP.UseHands			= true
SWEP.ViewModelFlip		= true
SWEP.ViewModelFOV		= 74
SWEP.ViewModel  = "models/weapons/v_pist_fiveseven.mdl"
SWEP.WorldModel = "models/weapons/w_pist_fiveseven.mdl"

SWEP.Primary.Sound = Sound( "Weapon_FiveSeven.Single" )
SWEP.IronSightsPos = Vector(4.52, -4, 3.199)
SWEP.IronSightsAng = Vector(0, 0, 0)

function SWEP:OnDrop()
    self:Remove()
end

function SWEP:GivePrimaryAmmo( num )

    if self:Clip1() < self.Primary.ClipMax then
        self:SetClip1( self:Clip1() + num )
    end
    
end


function SWEP:Deploy()
    if self.Owner then
        timer.Destroy("GiveAmmoForCrim"..self.Owner:SteamID())
        timer.Create("GiveAmmoForCrim"..self.Owner:SteamID(),2,0,function() if IsValid(self) then self:GivePrimaryAmmo(1) end end)
    end
    return self.BaseClass.Deploy(self)

end