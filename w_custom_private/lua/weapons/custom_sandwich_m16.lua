SWEP.Base = "gb_camo_base"
SWEP.ViewModelFOV	= 54
SWEP.HoldType = "smg"
SWEP.PrintName = "Sandwich m16"
SWEP.ViewModelFOV = 70
SWEP.ViewModelFlip = false
SWEP.ViewModel = "models/weapons/v_rif_m4cu.mdl"
SWEP.WorldModel		= "models/weapons/w_rif_m4a1.mdl"
SWEP.AutoSpawnable = false

SWEP.HoldType = "ar2"
SWEP.Slot = 5
SWEP.Kind = WEAPON_UNARMED

if CLIENT then

    SWEP.Author = "GreenBlack"

    SWEP.Icon = "vgui/ttt_fun_killicons/m4a1.png"
end

SWEP.Primary.Delay			= 0.10
SWEP.Primary.Recoil			= 0.001
SWEP.Primary.Automatic = true
SWEP.Primary.Ammo = "Pistol"
SWEP.Primary.Damage = 14
SWEP.Primary.Cone = 0.008
SWEP.Primary.ClipSize = 200
SWEP.Primary.ClipMax = 600
SWEP.Primary.DefaultClip = 600
SWEP.AutoSpawnable      = false
SWEP.AmmoEnt = "item_ammo_pistol_ttt"
SWEP.Camo = -2
SWEP.Primary.Sound = Sound( "CSTM_SilencedShot5")


SWEP.IronSightsPos = Vector(-4, -6.2, 0.55)
SWEP.IronSightsAng = Vector(2.599, -1.3, -3.6)

function SWEP:PrimaryAttack()
    if self:Clip1() > 0 then
        self:Disorientate()
    end
    self.BaseClass.PrimaryAttack(self)
end

function SWEP:SetZoom(state)
    if CLIENT then return end
    if not (IsValid(self.Owner) and self.Owner:IsPlayer()) then return end
    if state then
        self.Owner:SetFOV(25, 0.2)
    else
        self.Owner:SetFOV(0, 0.2)
    end
end

-- Add some zoom to ironsights for this gun
function SWEP:SecondaryAttack()
    if not self.Owner:KeyDown(IN_USE) then
        self:Fly()
        return
    end
    if not self.IronSightsPos then return end
    if self.Weapon:GetNextSecondaryFire() > CurTime() then return end

    bIronsights = not self:GetIronsights()

    self:SetIronsights( bIronsights )

    if SERVER then
        self:SetZoom(bIronsights)
    end

    self.Weapon:SetNextSecondaryFire(CurTime() + 0.3)
end

function SWEP:PreDrop()
    self:SetZoom(false)
    self:SetIronsights(false)
    return self.BaseClass.PreDrop(self)
end

function SWEP:Reload()
    self.Weapon:DefaultReload( ACT_VM_RELOAD );
    self:SetIronsights( false )
    self:SetZoom(false)
end


function SWEP:Holster()
    self:SetIronsights(false)
    self:SetZoom(false)
    return true
end



if CLIENT then
    local scope = surface.GetTextureID("sprites/scope")
    function SWEP:DrawHUD()
        if self:GetIronsights() then
            surface.SetDrawColor( 0, 0, 0, 255 )

            local x = ScrW() / 2.0
            local y = ScrH() / 2.0
            local scope_size = ScrH()

            -- crosshair
            local gap = 80
            local length = scope_size
            surface.DrawLine( x - length, y, x - gap, y )
            surface.DrawLine( x + length, y, x + gap, y )
            surface.DrawLine( x, y - length, x, y - gap )
            surface.DrawLine( x, y + length, x, y + gap )

            gap = 0
            length = 50
            surface.DrawLine( x - length, y, x - gap, y )
            surface.DrawLine( x + length, y, x + gap, y )
            surface.DrawLine( x, y - length, x, y - gap )
            surface.DrawLine( x, y + length, x, y + gap )


            -- cover edges
            local sh = scope_size / 2
            local w = (x - sh) + 2
            surface.DrawRect(0, 0, w, scope_size)
            surface.DrawRect(x + sh - 2, 0, w, scope_size)

            surface.SetDrawColor(255, 0, 0, 255)
            surface.DrawLine(x, y, x + 1, y + 1)

            -- scope
            surface.SetTexture(scope)
            surface.SetDrawColor(255, 255, 255, 255)

            surface.DrawTexturedRectRotated(x, y, scope_size, scope_size, 0)

        else
            return self.BaseClass.DrawHUD(self)
        end
    end

    function SWEP:AdjustMouseSensitivity()
        return (self:GetIronsights() and 0.2) or nil
    end
end

SWEP.NextRegenAmmo = 0
function SWEP:Think()

    if self.NextRegenAmmo < CurTime() then
        if self:Clip1() < 200 then
            self.NextRegenAmmo = CurTime() + 3
            self:SetClip1(self:Clip1()+5)
        end
    end

    self.BaseClass.Think(self)
end
