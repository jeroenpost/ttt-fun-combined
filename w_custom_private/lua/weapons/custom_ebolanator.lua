if SERVER then
    AddCSLuaFile()
    --resource.AddFile("materials/vgui/ttt_fun_killicons/knife.png")
end

SWEP.Kind = WEAPON_GRENADE
SWEP.HoldType = "knife"
SWEP.PrintName    = "Ebolanator"
SWEP.Slot         = 3

if CLIENT then



    SWEP.ViewModelFlip = false



    SWEP.Icon = "vgui/ttt_fun_killicons/knife.png"
end

SWEP.Base               = "aa_base"
SWEP.ViewModelFOV = 65
SWEP.ViewModel          = "models/weapons/cstrike/c_knife_t.mdl"
SWEP.WorldModel         = "models/weapons/w_knife_t.mdl"

SWEP.UseHands = true
SWEP.DrawCrosshair      = false
SWEP.Primary.Damage         = 75
SWEP.Primary.ClipSize       = 1000
SWEP.Primary.DefaultClip    = 1000
SWEP.Primary.Automatic      = true
SWEP.Primary.Delay = 0.60
SWEP.Primary.Ammo       = "none"
SWEP.Secondary.ClipSize     = -1
SWEP.Secondary.DefaultClip  = -1
SWEP.Secondary.Automatic    = true
SWEP.Secondary.Ammo     = "none"
SWEP.Secondary.Delay = 1.4
SWEP.AllowDrop = false




SWEP.IsSilent = true

-- Pull out faster than standard guns
SWEP.DeploySpeed = 2

function SWEP:PrimaryAttack()
    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
    --self.Weapon:SetNextSecondaryFire( CurTime() + self.Secondary.Delay )
    if self.Owner:KeyDown(IN_USE) then
        if self.Owner.hadzombie and self.Owner.hadzombie > CurTime() then    self.Owner:PrintMessage( HUD_PRINTCENTER, "Already used" ) return end
        self.Owner.hadzombie = CurTime() + 300
        self:ShootBullet2(1,0.01,0.01,0.01)
        return
    end

    self.Owner:LagCompensation(true)

    local spos = self.Owner:GetShootPos()
    local sdest = spos + (self.Owner:GetAimVector() * 70)

    local kmins = Vector(1,1,1) * -10
    local kmaxs = Vector(1,1,1) * 10

    local tr = util.TraceHull({start=spos, endpos=sdest, filter=self.Owner, mask=MASK_SHOT_HULL, mins=kmins, maxs=kmaxs})

    -- Hull might hit environment stuff that line does not hit
    if not IsValid(tr.Entity) then
        tr = util.TraceLine({start=spos, endpos=sdest, filter=self.Owner, mask=MASK_SHOT_HULL})
    end

    local hitEnt = tr.Entity

    -- effects
    if IsValid(hitEnt) then
        self.Weapon:SendWeaponAnim( ACT_VM_HITCENTER )

        local edata = EffectData()
        edata:SetStart(spos)
        edata:SetOrigin(tr.HitPos)
        edata:SetNormal(tr.Normal)
        edata:SetEntity(hitEnt)

        if hitEnt:IsPlayer() or hitEnt:GetClass() == "prop_ragdoll" then
            util.Effect("BloodImpact", edata)
        end
    else
        self.Weapon:SendWeaponAnim( ACT_VM_MISSCENTER )
    end

    if SERVER then
        self.Owner:SetAnimation( PLAYER_ATTACK1 )
    end


    if SERVER and tr.Hit and tr.HitNonWorld and IsValid(hitEnt) then
        if hitEnt:IsPlayer() then

            if self.Owner:Health() < 175 then
                self.Owner:SetHealth(self.Owner:Health()+10)
            end
            -- knife damage is never karma'd, so don't need to take that into
            -- account we do want to avoid rounding error strangeness caused by
            -- other damage scaling, causing a death when we don't expect one, so
            -- when the target's health is close to kill-point we just kill
            if hitEnt:Health() < (self.Primary.Damage + 10) then
                self:StabKill(tr, spos, sdest)
            else
                local dmg = DamageInfo()
                dmg:SetDamage(self.Primary.Damage)
                dmg:SetAttacker(self.Owner)
                dmg:SetInflictor(self.Weapon or self)
                dmg:SetDamageForce(self.Owner:GetAimVector() * 5)
                dmg:SetDamagePosition(self.Owner:GetPos())
                dmg:SetDamageType(DMG_SLASH)

                hitEnt:DispatchTraceAttack(dmg, spos + (self.Owner:GetAimVector() * 3), sdest)
            end
        end
    end

    self.Owner:LagCompensation(false)
end

function SWEP:Deploy()
    local function playerDiesSound( victim, weapon, killer )
        if( IsValid(killer) and killer:IsPlayer()  ) then
            local wep = killer:GetActiveWeapon()
            if IsValid(wep) then
                wep = wep:GetClass()
                if wep == "custom_ebolanator" then
                    if IsValid(killer) and killer:Health() < 150 then
                        killer:SetHealth(killer:Health()+25)
                        if killer:Health() > 150 then killer:SetHealth(150) end
                    end
                    timer.Create("ebolanatorgas"..victim:EntIndex(),0.5,100,function()
                        if IsValid(victim) and not victim:Alive() and victim.server_ragdoll and IsValid(victim.server_ragdoll) and IsValid(killer) then
                            local effect = EffectData()
                            local origin = victim.server_ragdoll:GetPos()
                            effect:SetOrigin( origin )
                            effect:SetScale( 3 )
                            util.Effect( "AntlionGib", effect )
                                for k, v in pairs (ents.FindInSphere(victim.server_ragdoll:GetPos(), 220)) do
                                    if not v:IsPlayer()  then return end
                                    v:Ignite(0.05,1)
                                    v:TakeDamage( 3, victim.server_ragdoll )
                                end
                            end
                    end)

                end
            end
        end
    end

    hook.Add( "PlayerDeath", "playerDeathSound_prosniper", playerDiesSound )
    return self.BaseClass.Deploy(self)

end

SWEP.NextParty = 0
function SWEP:PartyShot()
    if (self:Clip1() <= 0) or self.NextParty > CurTime() then return end
    self.NextParty = CurTime() + 2
    self:TakePrimaryAmmo(1)

    self:SendWeaponAnim(ACT_VM_PRIMARYATTACK)
    self.Owner:SetAnimation(PLAYER_ATTACK1)



    if (IsFirstTimePredicted()) then
       -- local snd, i = table.Random(self.Sounds)
        self:EmitSound("weapons/gb_weapons/partygun1.mp3");
    end

    danceMoves = {ACT_GMOD_TAUNT_ROBOT,ACT_GMOD_TAUNT_DANCE,ACT_GMOD_TAUNT_MUSCLE}

    local trace = self.Owner:GetEyeTrace()
    if (trace.HitNonWorld and trace.HitPos:Distance(trace.StartPos) < 1000) then
        local victim = trace.Entity
        if (!victim:IsPlayer()) then return end

        danceMove = table.Random(danceMoves)
        victim:AnimPerformGesture_gb(danceMove);
        if (SERVER) then

            local soundfile = gb_config.websounds.."birdistheword.mp3"
            gb_PlaySoundFromServer(soundfile,victim, false)

            timer.Simple(15,function()
            --Start the dance again
                if IsValid(victim) and IsValid(self) and IsValid(self.Owner) and danceMove  then
                    victim:AnimPerformGesture_gb(danceMove);
                end
            end)

            timer.Create("TakeHealthPartyGun"..victim:UniqueID(),0.5,30, function()
                if IsValid(victim) and IsValid(self) and IsValid(self.Owner) then
                    victim:TakeDamage( 1, self.Owner, self.Owner:GetActiveWeapon() )
                    local shock1 = math.random(-4200, 4200)
                    local shock2 = math.random(-4200, 4200)
                    local shock3 = math.random(-4200, 4200)
                    victim:GetPhysicsObject():ApplyForceCenter(Vector(shock1, shock2, shock3))
                end
            end)
        end

    end

end

function SWEP:StabKill(tr, spos, sdest)
    local target = tr.Entity

    local dmg = DamageInfo()
    dmg:SetDamage(75)
    dmg:SetAttacker(self.Owner)
    dmg:SetInflictor(self.Weapon or self)
    dmg:SetDamageForce(self.Owner:GetAimVector())
    dmg:SetDamagePosition(self.Owner:GetPos())
    dmg:SetDamageType(DMG_SLASH)

    -- now that we use a hull trace, our hitpos is guaranteed to be
    -- terrible, so try to make something of it with a separate trace and
    -- hope our effect_fn trace has more luck

    -- first a straight up line trace to see if we aimed nicely
    local retr = util.TraceLine({start=spos, endpos=sdest, filter=self.Owner, mask=MASK_SHOT_HULL})

    -- if that fails, just trace to worldcenter so we have SOMETHING
    if retr.Entity != target then
    local center = target:LocalToWorld(target:OBBCenter())
    retr = util.TraceLine({start=spos, endpos=center, filter=self.Owner, mask=MASK_SHOT_HULL})
    end


    -- create knife effect creation fn
    local bone = retr.PhysicsBone
    local pos = retr.HitPos
    local norm = tr.Normal
    local ang = Angle(-28,0,0) + norm:Angle()
    ang:RotateAroundAxis(ang:Right(), -90)
    pos = pos - (ang:Forward() * 7)

    local prints = self.fingerprints
    local ignore = self.Owner

    target.effect_fn = function(rag)
    -- we might find a better location
        local rtr = util.TraceLine({start=pos, endpos=pos + norm * 40, filter=ignore, mask=MASK_SHOT_HULL})

        if IsValid(rtr.Entity) and rtr.Entity == rag then
            bone = rtr.PhysicsBone
            pos = rtr.HitPos
            ang = Angle(-28,0,0) + rtr.Normal:Angle()
            ang:RotateAroundAxis(ang:Right(), -90)
            pos = pos - (ang:Forward() * 10)

        end

        local knife = ents.Create("prop_physics")
        knife:SetModel("models/weapons/w_knife_t.mdl")
        knife:SetPos(pos)
        knife:SetCollisionGroup(COLLISION_GROUP_DEBRIS)
        knife:SetAngles(ang)
        knife.CanPickup = false

        knife:Spawn()

        local phys = knife:GetPhysicsObject()
        if IsValid(phys) then
            phys:EnableCollisions(false)
        end

        constraint.Weld(rag, knife, bone, 0, 0, true)

        -- need to close over knife in order to keep a valid ref to it
        rag:CallOnRemove("ttt_knife_cleanup", function() SafeRemoveEntity(knife) end)
    end


    -- seems the spos and sdest are purely for effects/forces?
    target:DispatchTraceAttack(dmg, spos + (self.Owner:GetAimVector() * 3), sdest)

    -- target appears to die right there, so we could theoretically get to
    -- the ragdoll in here...

    --self:Remove()
end

function SWEP:Reload()
    if self.Owner:KeyDown(IN_USE) then
        self:PartyShot()
        return
    end
    self:Fly()

end

function SWEP:OnDrop()
    self:Remove()
end

function SWEP:SecondaryAttack()
    self.Weapon:SetNextSecondaryFire( CurTime() + (self.Secondary.Delay * 1.5) )



    if not worldsnd then
        self.Weapon:EmitSound( "weapons/tar21/tar21-1.mp3", self.Primary.SoundLevel )
    elseif SERVER then
        sound.Play("weapons/tar21/tar21-1.mp3", self:GetPos(), self.Primary.SoundLevel)
    end

    self:ShootBullet(95, 0.5, 1, 0.0001 )



    local owner = self.Owner
    if not IsValid(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

    owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )
end

function SWEP:Equip()
    self.Weapon:SetNextPrimaryFire( CurTime() + (self.Primary.Delay * 1.5) )
    self.Weapon:SetNextSecondaryFire( CurTime() + (self.Secondary.Delay * 1.5) )
end

function SWEP:PreDrop()
    -- for consistency, dropped knife should not have DNA/prints
    self.fingerprints = {}
end

function SWEP:OnRemove()
    if CLIENT and IsValid(self.Owner) and self.Owner == LocalPlayer() and self.Owner:Alive() then
        RunConsoleCommand("lastinv")
    end
end

if CLIENT then
    function SWEP:DrawHUD()
        local tr = self.Owner:GetEyeTrace(MASK_SHOT)

        if tr.HitNonWorld and IsValid(tr.Entity) and tr.Entity:IsPlayer()
                and tr.Entity:Health() < (self.Primary.Damage + 10) then

            local x = ScrW() / 2.0
            local y = ScrH() / 2.0

            surface.SetDrawColor(255, 0, 0, 255)

            local outer = 20
            local inner = 10
            surface.DrawLine(x - outer, y - outer, x - inner, y - inner)
            surface.DrawLine(x + outer, y + outer, x + inner, y + inner)

            surface.DrawLine(x - outer, y + outer, x - inner, y + inner)
            surface.DrawLine(x + outer, y - outer, x + inner, y - inner)

            draw.SimpleText("INSTANT KILL", "TabLarge", x, y - 30, COLOR_RED, TEXT_ALIGN_CENTER, TEXT_ALIGN_BOTTOM)
        end

        return self.BaseClass.DrawHUD(self)
    end
end



function SWEP:CreateZombie( ply, owner )

    if not IsValid( ply ) or not IsValid( owner ) then print("Not valid zombie") return end

    local ent = ply
    local weapon = owner:GetActiveWeapon()
    local edata = EffectData()

    edata:SetEntity(ent)
    edata:SetMagnitude(3)
    edata:SetScale(2)

    util.Effect("TeslaHitBoxes", edata)

    if SERVER and ent:IsPlayer() then

        local modelsss = { "npc_zombie", "npc_antlionguard", "npc_fastzombie", "npc_poisonzombie" }
        local randmodel = table.Random( modelsss )

        local oldPos = ent:GetPos()

        timer.Simple(4.9, function()
            if IsValid( ent ) then
                spot = ent:GetPos()
            else
                spot = oldPos
            end
            zed = ents.Create(randmodel)
        end)

        timer.Simple(5,function()
            if SERVER then
                if IsValid( ent ) then
                    ent:TakeDamage( 2000, owner, weapon )
                end
                --local ply = self.Owner -- The first entity is always the first player
                zed:SetPos(spot) -- This positions the zombie at the place our trace hit.
                zed:SetHealth( 100 )
                zed:Spawn() -- This method spawns the zombie into the world, run for your lives! ( or just crowbar it dead(er) )
                zed:SetHealth( 100 )
            end
        end) --20
        timer.Create("infectedDamage",0.25,20,function()
            if IsValid( ent ) then
                ent:TakeDamage( 3, owner, weapon )
            end
        end)

    end


end


function SWEP:ShootBullet2( dmg, recoil, numbul, cone )

    local sights = self:GetIronsights()

    numbul = numbul or 1
    cone   = cone   or 0.01

    -- 10% accuracy bonus when sighting
    cone = sights and (cone * 0.9) or cone

    local bullet = {}
    bullet.Num    = numbul
    bullet.Src    = self.Owner:GetShootPos()
    bullet.Dir    = self.Owner:GetAimVector()
    bullet.Spread = Vector( cone, cone, 0 )
    bullet.Tracer = 4
    bullet.Force  = 5
    bullet.Damage = dmg

    bullet.Callback = function(att, tr, dmginfo)
        if SERVER or (CLIENT and IsFirstTimePredicted()) then

            if (not tr.HitWorld)  then
                local pl2 = 0
                --if math.random(1,4) == 3 or (specialRound and specialRound.isSpecialRound) or _globals['specialperson'][tr.Entity:SteamID()]  then
                 --   pl2 = self.Owner
                --else
                    pl2 = tr.Entity
               -- end

                self:CreateZombie( pl2, self.Owner )
            end
        end
    end
    self.Owner:FireBullets( bullet )
    self.Weapon:SendWeaponAnim(self.PrimaryAnim)

    -- Owner can die after firebullets, giving an error at muzzleflash
    if not IsValid(self.Owner) or not self.Owner:Alive() then return end

    self.Owner:MuzzleFlash()
    self.Owner:SetAnimation( PLAYER_ATTACK1 )

    if self.Owner:IsNPC() then return end

    if ((game.SinglePlayer() and SERVER) or
            ((not game.SinglePlayer()) and CLIENT and IsFirstTimePredicted() )) then

        -- reduce recoil if ironsighting
        recoil = sights and (recoil * 0.5) or recoil

    end

end
