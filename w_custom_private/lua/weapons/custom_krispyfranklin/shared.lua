if SERVER then

   AddCSLuaFile(  )

end

SWEP.HoldType			= "smg"
SWEP.PrintName			= "KrispyFranklin"
SWEP.Slot				= 6
if CLIENT then

   
   SWEP.Author				= "Assassin"
   
   SWEP.SlotPos			= 0

   SWEP.Icon = "vgui/ttt_fun_killicons/mp7.png"

	 
	 
   
   SWEP.ViewModelFlip		= true
end


SWEP.Base				= "aa_base"

SWEP.Spawnable = true
SWEP.AdminSpawnable = true

SWEP.Kind = WEAPON_EQUIP2


SWEP.Primary.Damage = 26
SWEP.Primary.Delay = 0.20
SWEP.Primary.Cone = 0.009
SWEP.Primary.ClipSize = 50
SWEP.Primary.ClipMax = 120
SWEP.Primary.DefaultClip	= 50
SWEP.Primary.Automatic		= true
SWEP.Primary.Ammo			= "smg1"
SWEP.AutoSpawnable      = false
SWEP.Primary.Recoil			= 0.8
SWEP.Primary.Sound			= Sound("weapons/mp7/mp7_fire.mp3")
SWEP.ViewModel			= "models/weapons/v_mp7_silenced.mdl"
SWEP.WorldModel			= "models/weapons/w_mp7_silenced.mdl"

SWEP.AmmoEnt = "item_ammo_smg1_ttt"

SWEP.HeadshotMultiplier = 2.2

SWEP.Secondary.Sound = Sound("Default.Zoom")

SWEP.IronSightsPos      = Vector( 5, -15, -2 )
SWEP.IronSightsAng      = Vector( 2.6, 1.37, 3.5 )

function SWEP:PrimaryAttack()
    self.Weapon:SetNextPrimaryFire(CurTime() + self.Primary.Delay)
    if not self:CanPrimaryAttack() then return end

    self.Weapon:EmitSound(self.Primary.Sound, self.Primary.SoundLevel)
    self:ShootBullet(self.Primary.Damage, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone())
    if SERVER then
        self:MakeAFlame();
    end
    self:TakePrimaryAmmo(1)
    self.Owner:ViewPunch(Angle(math.Rand(-0.2, -0.1) * self.Primary.Recoil, math.Rand(-0.1, 0.1) * self.Primary.Recoil, 0))
end

function SWEP:SetZoom(state)
    if CLIENT or not self.Owner:IsValid() then
       return
    else
       if state then
          self.Owner:SetFOV(20, 0.3)
       else
          self.Owner:SetFOV(0, 0.2)
       end
    end
end

-- Add some zoom to ironsights for this gun
function SWEP:SecondaryAttack()

        if not self.Owner:KeyDown(IN_USE) then
            self:Fly();
            return
        end

    if not self.IronSightsPos then return end
    if self.Weapon:GetNextSecondaryFire() > CurTime() then return end
    
    bIronsights = not self:GetIronsights()
    
    self:SetIronsights( bIronsights )
    
    if SERVER then
        self:SetZoom(bIronsights)
     else
        self:EmitSound(self.Secondary.Sound)
    end
    
    self.Weapon:SetNextSecondaryFire( CurTime() + 0.3)
end

function SWEP:PreDrop()
    self:SetZoom(false)
    self:SetIronsights(false)
    return self.BaseClass.PreDrop(self)
end


function SWEP:Reload()

    if self.ReloadingTime and CurTime() <= self.ReloadingTime then return
    end

    if not self.Owner:KeyDown(IN_USE) then
        self.Weapon:DefaultReload( ACT_VM_RELOAD );
        self:SetIronsights( false )
        self:SetZoom(false)
        self:DefaultReload(ACT_VM_RELOAD)
        local AnimationTime = self.Owner:GetViewModel():SequenceDuration()
        self.ReloadingTime = CurTime() + AnimationTime
        self:SetNextPrimaryFire(CurTime() + AnimationTime)
        self:SetNextSecondaryFire(CurTime() + AnimationTime)
        return
    end

    if SERVER then
        self:ShootFlare();
    end
end


function SWEP:Holster()
    self:SetIronsights(false)
    self:SetZoom(false)
    return true
end

if CLIENT then
   local scope = surface.GetTextureID("sprites/scope")
   function SWEP:DrawHUD()
      if self:GetIronsights() then
         surface.SetDrawColor( 0, 0, 0, 255 )
         
         local x = ScrW() / 2.0
         local y = ScrH() / 2.0
         local scope_size = ScrH()

         -- crosshair
         local gap = 80
         local length = scope_size
         surface.DrawLine( x - length, y, x - gap, y )
         surface.DrawLine( x + length, y, x + gap, y )
         surface.DrawLine( x, y - length, x, y - gap )
         surface.DrawLine( x, y + length, x, y + gap )

         gap = 0
         length = 50
         surface.DrawLine( x - length, y, x - gap, y )
         surface.DrawLine( x + length, y, x + gap, y )
         surface.DrawLine( x, y - length, x, y - gap )
         surface.DrawLine( x, y + length, x, y + gap )


         -- cover edges
         local sh = scope_size / 2
         local w = (x - sh) + 2
         surface.DrawRect(0, 0, w, scope_size)
         surface.DrawRect(x + sh - 2, 0, w, scope_size)

         surface.SetDrawColor(255, 0, 0, 255)
         surface.DrawLine(x, y, x + 1, y + 1)

         -- scope
         surface.SetTexture(scope)
         surface.SetDrawColor(255, 255, 255, 255)

         surface.DrawTexturedRectRotated(x, y, scope_size, scope_size, 0)

      else
         return self.BaseClass.DrawHUD(self)
      end
   end

   function SWEP:AdjustMouseSensitivity()
      return (self:GetIronsights() and 0.2) or nil
   end
end
local maxrange = 10000

local math = math

-- Returns if an entity is a valid physhammer punching target. Does not take
-- distance into account.
local function ValidTarget(ent)
    return IsValid(ent) and ent:IsPlayer()
    -- NOTE: cannot check for motion disabled on client
end

if CLIENT then
    local surface = surface

    local linex = 0
    local liney = 0
    local laser = Material("trails/laser")
    function SWEP:ViewModelDrawn()
        local client = LocalPlayer()
        local vm = client:GetViewModel()
        if not IsValid(vm) then return end

        local plytr = client:GetEyeTrace(MASK_SHOT)

        local muzzle_angpos = vm:GetAttachment(1)
        local spos = muzzle_angpos.Pos + muzzle_angpos.Ang:Forward() * 10
        local epos = client:GetShootPos() + client:GetAimVector() * maxrange

        -- Painting beam
        local tr = util.TraceLine({start=spos, endpos=epos, filter=client, mask=MASK_ALL})

        local c = COLOR_RED
        local a = 150
        local d = (plytr.StartPos - plytr.HitPos):Length()
        if plytr.HitNonWorld then
            if ValidTarget(plytr.Entity) then
                if d < maxrange then
                    c = COLOR_GREEN
                    a = 255
                else
                    c = COLOR_RED
                end
            end
        end


        render.SetMaterial(laser)
        render.DrawBeam(spos, tr.HitPos, 5, 0, 0, c)
    end
end
