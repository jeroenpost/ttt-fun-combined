
SWEP.Base			= "gb_master_deagle"
SWEP.PrintName	= "Superiors Superior"
SWEP.Kind 			= WEAPON_PISTOL
SWEP.Slot = 1
SWEP.SlotPos 	= 1
SWEP.Primary.Recoil	= 1
SWEP.Primary.Damage = 55
SWEP.Primary.Delay 	= 0.45
SWEP.Primary.Cone 	= 0.02
SWEP.Primary.ClipSize 		= 40
SWEP.Primary.ClipMax 		= 120
SWEP.Primary.DefaultClip 	= 40
SWEP.Primary.NumShots		= 1
SWEP.Primary.Automatic 	= true
SWEP.HeadshotMultiplier = 3
SWEP.AmmoEnt 		= "item_ammo_revolver_ttt"



SWEP.NumBlinds = 0
function SWEP:SecondaryAttack()
    self.Weapon:SetNextSecondaryFire( CurTime() + 0.14 )
    self.Weapon:SetNextPrimaryFire( CurTime() + 0.14 )

    if  self.NumBlinds > 4 then return end

    self:ShootBullet( 25, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )


    local owner = self.Owner
    if not IsValid(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

    owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )

    local trace = self.Owner:GetEyeTrace()
    if (trace.HitNonWorld) then
        local victim = trace.Entity
        if ( not victim:IsPlayer()) then return end

        self.NumBlinds = self.NumBlinds + 1
        if SERVER then

            victim.IsBlinded = true
            victim:SetNWBool("isblinded",true)
            umsg.Start( "ulx_blind", victim )
            umsg.Bool( true )
            umsg.Short( 255 )
            umsg.End()
        end
        victim:AnimPerformGesture(ACT_GMOD_TAUNT_PERSISTENCE);
        if (SERVER) then

            timer.Create("ResetPLayerAfterBlided"..victim:UniqueID(), 3,1, function()
                if not IsValid(victim)  then  return end

                if SERVER then
                    victim.IsBlinded = false
                    victim:SetNWBool("isblinded",false)
                    umsg.Start( "ulx_blind", victim )
                    umsg.Bool( false )
                    umsg.Short( 0 )
                    umsg.End()

                end
            end)
        end
    end


end