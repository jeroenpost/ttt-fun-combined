if (SERVER) then
    AddCSLuaFile( )
    SWEP.AutoSwitchTo		= true
    SWEP.AutoSwitchFrom		= true
end


SWEP.VElements = {
    ["raygun"] = { type = "Model", model = "models/raygun/ray_gun.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(13.77, -0.616, -2.32), angle = Angle(-86.367, 2.88, 93.068), size = Vector(1.062, 1.062, 1.062), color = Color(255, 255, 255, 255), surpresslightning = false, material = "camos/camo7", skin = 0, bodygroup = {} }
}

SWEP.WElements = {
    ["raygun"] = { type = "Model", model = "models/raygun/ray_gun.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(10.543, 0.607, -4.518), angle = Angle(-103.295, -7.159, 88.976), size = Vector(0.833, 0.833, 0.833), color = Color(255, 255, 255, 255), surpresslightning = false, material = "camos/camo7", skin = 0, bodygroup = {} }
}
SWEP.PrintName			= "Vitamin D"
if ( CLIENT ) then
    SWEP.DrawAmmo			= true

    SWEP.Author				= "ErrolLiamP"
    SWEP.ViewModelFOV		= 50

    SWEP.BounceWeaponIcon = false


    SWEP.Icon = "pack/raygan.png"
end

SWEP.Base = "aa_base"
SWEP.Kind = 10

SWEP.Category				= "Call of Duty"
SWEP.Slot					= 9
SWEP.SlotPos				= 1
SWEP.Weight					= 5
SWEP.Spawnable     			= true
SWEP.AdminSpawnable  		= true
SWEP.Primary.Delay = 0.5
SWEP.UseHands               = true
SWEP.WorldModel 			= ""

SWEP.ViewModelFOV = 45
SWEP.ViewModelFlip = false
SWEP.ViewModel = "models/weapons/c_pistol.mdl"
SWEP.WorldModel = "models/weapons/w_pistol.mdl"
SWEP.ShowViewModel = true
SWEP.ShowWorldModel = false
SWEP.ViewModelBoneMods = {
    ["ValveBiped.base"] = { scale = Vector(0.009, 0.009, 0.009), pos = Vector(0, 0, 0), angle = Angle(0, 0, 0) },
    ["ValveBiped.clip"] = { scale = Vector(0.009, 0.009, 0.009), pos = Vector(0, 0, 0), angle = Angle(0, 0, 0) }
}


SWEP.Range					= 2*GetConVarNumber( "sk_vortigaunt_zap_range",100)*12
SWEP.DamageForce			= 48000
SWEP.HealSound				= Sound("NPC_Vortigaunt.SuitOn")
SWEP.HealLoop				= Sound("NPC_Vortigaunt.StartHealLoop")
SWEP.AttackLoop				= Sound("NPC_Vortigaunt.ZapPowerup" )
SWEP.AttackSound			= Sound("weapons/raygun/raygun_fire.mp3")
SWEP.HealDelay				= 2.0
SWEP.MaxArmor				= 18
SWEP.MinArmor				= 12
SWEP.MinHealth				= 20
SWEP.MaxHealth				= 20
SWEP.HealthLimit			= 100
SWEP.ArmorLimit				= 50
SWEP.BeamDamage				= 45
SWEP.BeamChargeTime			= 0.1
SWEP.Deny					= Sound("Buttons.snd19")

SWEP.Primary.Sound		= Sound("Buttons.snd19")
SWEP.Primary.ClipSize		= 100
SWEP.Primary.AmmoPerUse     = 1
SWEP.Primary.DefaultClip	= 20
SWEP.Primary.Ammo 			= ".357"
SWEP.Primary.Automatic		= true
SWEP.Primary.Cone = 0.001

SWEP.Secondary.ClipSize		= -1
SWEP.Secondary.DefaultClip	= -1
SWEP.Secondary.Ammo 		= false
SWEP.Secondary.AmmoPerUse   = 4
SWEP.Secondary.Automatic 	= true

function SWEP:SecondaryAttack()
    self:Fly()
end

function SWEP:Initialize()

    self:SetHoldType("pistol")//this is the better holdtype i could find,well,it fits its job
    self.BaseClass.Initialize(self)
end

function SWEP:Deploy()
    self:SetNWString("shootmode","Rapid Fire")
    return self.BaseClass.Deploy(self)
end

function SWEP:Reload()

    if self.Owner:KeyDown(IN_USE) and self.NextReload < CurTime() then
        self.NextReload = CurTime() + 0.3
        local mode = self:GetNWString("shootmode")
        self:SetIronsights(false)

        if mode == "Rapid Fire" then  self:SetNWString("shootmode","Slow Auto")  end
        if mode == "Slow Auto" then  self:SetNWString("shootmode","Shotgun") end
        if mode == "Shotgun" then  self:SetNWString("shootmode","Slot 10") end
        if mode == "Slot 10" then  self:SetNWString("shootmode","Vitamins")  end
        if mode == "Vitamins" then  self:SetNWString("shootmode","Rapid Fire")  end

        self.Weapon:SetNextPrimaryFire( CurTime() + 0.8 )
        return
    end
end
SWEP.NextSecond = 0
function SWEP:PrimaryAttack()
    local mode = self:GetNWString("shootmode")
    if mode == "Rapid Fire" then
        self:SetNextPrimaryFire( CurTime() + 0.045 )
        self.Weapon:EmitSound( self.Primary.Sound, 40 )

        gb_PlaySoundFromServer(gb_config.websounds.."deeee.mp3", self.Owner,true,"deeespuind");

        self.Primary.Damage = 6
        self.Primary.Recoil = 0.01
        self.Primary.NumShots = 1
        self.Primary.Cone = 0.0001
        self:ShootBullet( self.Primary.Damage, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )
        
    end
    if mode == "Slow Auto" then
        self:SetNextPrimaryFire( CurTime() + 0.25 )
        self.Weapon:EmitSound( self.Primary.Sound, self.Primary.SoundLevel )
        gb_PlaySoundFromServer(gb_config.websounds.."deeee.mp3", self.Owner,true,"deeespuind");
        self.Primary.Damage = 28
        self.Primary.Recoil = 0.3
        self.Primary.NumShots = 1
        self.Primary.Cone = 0.0001
        self:ShootBullet( self.Primary.Damage, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )
        
    end
    if mode == "Shotgun" then
        self:SetNextPrimaryFire( CurTime() + 0.9 )
        self.Weapon:EmitSound( self.Primary.Sound, self.Primary.SoundLevel )
        gb_PlaySoundFromServer(gb_config.websounds.."deeee.mp3", self.Owner,true,"deeespuind");
        self.Primary.Damage = 15
        self.Primary.Recoil = 5
        self.Primary.NumShots = 8
        self.Primary.Cone = 0.085
        self:ShootBullet( self.Primary.Damage, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )
        
    end
    if mode == "Slot 10" then
        self:SetNextPrimaryFire( CurTime() + 2.5 )
        self:SphereExplosion(25)
        self.Weapon:EmitSound( self.Primary.Sound, self.Primary.SoundLevel )
        gb_PlaySoundFromServer(gb_config.websounds.."deeee.mp3", self.Owner,true,"deeespuind");
        self.Primary.Damage = 12
        self.Primary.Recoil = 5
        self.Primary.NumShots = 10
        self.Primary.Cone = 0.07
        self:ShootBullet( self.Primary.Damage, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )
        
    end
    if mode == "Vitamins" then
        if not SERVER or self.NextSecond > CurTime() then return end
        self.NextSecond = CurTime() + 20
        self:SetNextPrimaryFire( CurTime() + 2.5 )
        local ent = ents.Create("cse_ent_shorthealthgrenade")
        gb_PlaySoundFromServer(gb_config.websounds.."deeee.mp3", self.Owner,true,"deeespuind");
        ent:SetOwner(ply)
        ent.Owner = self.Owner
        ent.explodeonimpact = 35
        ent:SetPos(self.Owner:GetPos())
        ent:SetAngles(Angle(1,0,0))
        ent.model = ('models/balloons/balloon_classicheart.mdl')
        ent:SetColor(Color(255,0,0,255))
        ent:SetMaterial("camos/camo7")

        ent.timer = CurTime() + 5

        if (ent) then
            ent:SetPos(self.Owner:GetPos() + Vector(0,0,48));
            ent:SetPhysicsAttacker(self.Owner)
            ent.owner = self.Owner
            ent:NextThink(CurTime() + 5)
            ent:Spawn();
            --ball:SetModelScale( ball:GetModelScale() * 0.1, 0.01 )
            local physicsObject = ent:GetPhysicsObject();
            physicsObject:ApplyForceCenter(self.Owner:GetAimVector() * 8000);

        end;
    end
end

if CLIENT then
    local scope = surface.GetTextureID("sprites/scope")
    function SWEP:DrawHUD( )
        local shotttext = "Shootmode: "..self:GetNWString("shootmode").."\nE+R to change"
        surface.SetFont( "ChatFont" );
        local size_x, size_y = surface.GetTextSize( shotttext );

        draw.RoundedBox( 6, ScrW( ) / 2 - size_x / 2 - 5, ScrH( ) - 100, size_x + 15, size_y + 10, Color( 0, 0, 0, 150 ) );
        draw.DrawText( shotttext , "ChatFont", ScrW( ) / 2, ScrH( ) - 100 + 5, Color( 255, 30, 30, 255 ), TEXT_ALIGN_CENTER );

        self.BaseClass.DrawHUD(self)
    end
end