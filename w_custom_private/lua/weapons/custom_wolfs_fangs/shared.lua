
if SERVER then
   AddCSLuaFile(  )
end

SWEP.HoldType			= "pistol"
SWEP.PrintName = "Wolf's Fangs"
SWEP.Slot = 1
if CLIENT then


   SWEP.Icon = "VGUI/ttt/icon_silenced"
end


SWEP.Base = "aa_base_fw"
SWEP.Spawnable = true
SWEP.AdminSpawnable = true
SWEP.AutoSpawnable = false
SWEP.Kind = WEAPON_PISTOL
SWEP.Primary.Delay = 0.12
SWEP.Primary.Recoil = 0.05
SWEP.Primary.Automatic = true
SWEP.Primary.Ammo = "Pistol"
SWEP.Primary.Damage = 16
SWEP.Primary.Cone = 0.012
SWEP.Primary.ClipSize = 120
SWEP.Primary.ClipMax = 190
SWEP.Primary.DefaultClip = 190


SWEP.AmmoEnt = "item_ammo_pistol_ttt"

SWEP.IsSilent = true

SWEP.UseHands			= true
SWEP.ViewModelFlip		= false
SWEP.ViewModelFOV		= 54
SWEP.ViewModel			= "models/weapons/cstrike/c_pist_usp.mdl"
SWEP.WorldModel			= "models/weapons/w_pist_usp.mdl"
SWEP.HasFireBullet = true
SWEP.Primary.Sound = Sound( "weapons/usp/usp1.wav" )
SWEP.Primary.SoundLevel = 50

SWEP.IronSightsPos = Vector( -5.91, -4, 2.84 )
SWEP.IronSightsAng = Vector(-0.5, 0, 0)

SWEP.PrimaryAnim = ACT_VM_PRIMARYATTACK_SILENCED
SWEP.ReloadAnim = ACT_VM_RELOAD_SILENCED

function SWEP:Deploy()
   self.Weapon:SendWeaponAnim(ACT_VM_DRAW_SILENCED)
   self:SetNWString("shootmode","normal")
   return true
end

SWEP.NextAmmo = 0
SWEP.NextHealth = 0
SWEP.NextThinks = 0
function SWEP:Think()
    if self.Owner:KeyDown(IN_USE) and self.NextThinks < CurTime() then
        self.NextThinks = CurTime() + 1
        local mode = self:GetNWString("shootmode");
        if mode != "sniper" then
             self:SetNWString("shootmode","sniper")
        self.Primary.Sound = Sound( "weapons/357/357_fire2.wav" )
        else
            self:SetNWString("shootmode","pistol")
            self.Primary.Sound = Sound( "weapons/usp/usp1.wav" )
        end
        return
    end
    if self.Owner:KeyDown(IN_USE) and self.Owner:KeyDown(IN_RELOAD) then
        self:FireWorkie()
    end
    if self.NextAmmo < CurTime() then
        self.NextAmmo = CurTime() + 1
        self:SetClip1( self:Clip1() + 1)
    end
end

-- We were bought as special equipment, and we have an extra to give
function SWEP:WasBought(buyer)
   if IsValid(buyer) then -- probably already self.Owner
      buyer:GiveAmmo( 20, "Pistol" )
   end
end


function SWEP:SecondaryAttack()
    if self.Owner:KeyDown(IN_USE) then
        self:SetNWString("shootmode","sniper")
        self:Zoom()
        return
    end
    self:Fly()
end

function SWEP:OnDrop()
    self:Remove()
end



function SWEP:PrimaryAttack()
    self.Weapon:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
    self.Weapon:SetNextPrimaryFire( CurTime() + 0.4 )

    if not self:CanPrimaryAttack() then return end

    if math.random(1,10) != 3 then
        sound.Play(self.Primary.Sound, self:GetPos(), self.Primary.SoundLevel)
    else
        gb_PlaySoundFromServer(gb_config.websounds.."patrick.mp3",self.Weapon)
    end

    local mode = self:GetNWString("shootmode")
    if mode != "sniper" then  self:ShootBullet( 40, 0.5, 1, 0.004 ) self.Weapon:SetNextPrimaryFire( CurTime() + 0.4 )  end
    if mode == "sniper" then self:ShootBullet( math.random(60,125), 3, 1, 0.0001 ) self.Weapon:SetNextPrimaryFire( CurTime() + 1.8 ) gb_PlaySoundFromServer(gb_config.websounds.."EvilSylvanasYesAttack3.mp3", self.Owner,true,"kon_tsdffdp"); end


    self:TakePrimaryAmmo( 1 )
    self:MakeAFlame();

    local owner = self.Owner
    if not IsValid(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

    owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )
end


SWEP.IronSightsPos      = Vector( -100, -100, -100 )
SWEP.IronSightsAng      = Vector( 2.6, 1.37, 3.5 )
SWEP.Secondary.Sound = Sound("Default.Zoom")
-- Add some zoom to ironsights for this gun
function SWEP:Zoom()
    if not self.IronSightsPos then return end
    if self.Weapon:GetNextSecondaryFire() > CurTime() then return end

    local bIronsights = not self:GetIronsights()

    self:SetIronsights( bIronsights )

    if SERVER then
        self:SetZoom(bIronsights)
    else
        self:EmitSound(self.Secondary.Sound)
    end

    self.Weapon:SetNextSecondaryFire( CurTime() + 0.3)
end

function SWEP:PreDrop()
    self:SetZoom(false)
    self:SetIronsights(false)
    return self.BaseClass.PreDrop(self)
end


function SWEP:Holster()
    self:SetIronsights(false)
    self:SetZoom(false)
    return true
end

function SWEP:SetZoom(state)
    if CLIENT then
        return
    elseif IsValid(self.Owner) and self.Owner:IsPlayer() then
        if state then
            self.Owner:SetFOV(20, 0.3)
        else
            self.Owner:SetFOV(0, 0.2)
        end
    end

end

if CLIENT then
    function SWEP:DrawHUD( )
        local shotttext = "Shootmode: "..self:GetNWString("shootmode").."\nE to change"
        surface.SetFont( "ChatFont" );
        local size_x, size_y = surface.GetTextSize( shotttext );

        draw.RoundedBox( 6, ScrW( ) / 2 - size_x / 2 - 5, ScrH( ) - 100, size_x + 15, size_y + 10, Color( 0, 0, 0, 150 ) );
        draw.DrawText( shotttext , "ChatFont", ScrW( ) / 2, ScrH( ) - 100 + 5, Color( 255, 30, 30, 255 ), TEXT_ALIGN_CENTER );

        local scope = surface.GetTextureID("sprites/scope")
        if self:GetIronsights() then
            surface.SetDrawColor( 0, 0, 0, 255 )

            local x = ScrW() / 2.0
            local y = ScrH() / 2.0
            local scope_size = ScrH()

            -- crosshair
            local gap = 80
            local length = scope_size
            surface.DrawLine( x - length, y, x - gap, y )
            surface.DrawLine( x + length, y, x + gap, y )
            surface.DrawLine( x, y - length, x, y - gap )
            surface.DrawLine( x, y + length, x, y + gap )

            gap = 0
            length = 50
            surface.DrawLine( x - length, y, x - gap, y )
            surface.DrawLine( x + length, y, x + gap, y )
            surface.DrawLine( x, y - length, x, y - gap )
            surface.DrawLine( x, y + length, x, y + gap )


            -- cover edges
            local sh = scope_size / 2
            local w = (x - sh) + 2
            surface.DrawRect(0, 0, w, scope_size)
            surface.DrawRect(x + sh - 2, 0, w, scope_size)

            surface.SetDrawColor(255, 0, 0, 255)
            surface.DrawLine(x, y, x + 1, y + 1)

            -- scope
            surface.SetTexture(scope)
            surface.SetDrawColor(255, 255, 255, 255)

            surface.DrawTexturedRectRotated(x, y, scope_size, scope_size, 0)

        else
            return self.BaseClass.DrawHUD(self)
        end
    end

    function SWEP:AdjustMouseSensitivity()
        return (self:GetIronsights() and 0.2) or nil
    end
end;