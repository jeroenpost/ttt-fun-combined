

SWEP.HoldType			= "ar2"
SWEP.PrintName = "Siltergeist"
if CLIENT then

    SWEP.Slot = 28

    SWEP.ViewModelFlip = false
    SWEP.ViewModelFOV = 54

    SWEP.EquipMenuData = {
        type = "item_weapon",
        desc = "polter_desc"
    };

    SWEP.Icon = "VGUI/ttt/icon_polter"
end

SWEP.Base = "gb_camo_base"
SWEP.Primary.Recoil	= 0.1
SWEP.Primary.Delay = 3
SWEP.Primary.Cone = 0.02
SWEP.Primary.ClipSize = 100
SWEP.Primary.DefaultClip = 100
SWEP.Primary.ClipMax = 100
SWEP.Primary.Ammo = "Gravity"
SWEP.Primary.Automatic = true

SWEP.Secondary.Automatic = false

SWEP.Camo = 38
SWEP.CustomCamo = true
SWEP.Kind = 29

SWEP.WeaponID = AMMO_POLTER

SWEP.UseHands			= true
SWEP.ViewModel	= "models/weapons/c_irifle.mdl"
SWEP.WorldModel	= "models/weapons/w_IRifle.mdl"

SWEP.Primary.Sound = Sound( "weapons/airboat/airboat_gun_energy1.wav" )

SWEP.NoSights = true

SWEP.IsCharging = false
SWEP.NextCharge = 0

AccessorFuncDT(SWEP, "charge", "Charge")

local maxrange = 5000

local math = math

-- Returns if an entity is a valid physhammer punching target. Does not take
-- distance into account.
local function ValidTarget(ent)
    return IsValid(ent) and ent:GetPhysicsObject() and (not ent:IsWeapon()) and (not ent:GetNWBool("punched", false))
    -- NOTE: cannot check for motion disabled on client
end

function SWEP:SetupDataTables()
    self:DTVar("Float", 0, "charge")
end



function SWEP:throw_attack (model_file, sound, away)
    local tr = self.Owner:GetEyeTrace();

    if IsValid( tr.Entity )  then
        if not tr.Entity.hasProtectionSuit then
            tr.Entity.hasProtectionSuit= true
            timer.Simple(5, function()
                if not IsValid( tr.Entity )  then return end
                tr.Entity.hasProtectionSuit = false
            end)
        end
    end

    if self.Owner:KeyDown(IN_DUCK) then
        model_file = "models/maxofs2d/companion_doll.mdl"
        end

    self.BaseClass.ShootEffects (self);
    if (!SERVER) then return end;
    local ent = ents.Create ("prop_physics");
    ent:SetModel (model_file);
    ent:SetPos (tr.HitPos + Vector(0,0,40));
    ent:SetAngles (self.Owner:EyeAngles());
    ent:SetPhysicsAttacker(self.Owner)
    ent:Spawn();
    --ent:EmitSound( sound )
    if math.Rand(1,5) == 3 then
        ent:Ignite(100,100)
    end


    timer.Simple(away,function()
        if IsValid(ent) then
            ent:Remove()
        end
    end)

    cleanup.Add (self.Owner, "props", ent);
    undo.Create ("Thrown model");
    undo.AddEntity (ent);
    undo.SetPlayer (self.Owner);
    undo.Finish();
end

local ghostmdl = Model("models/Items/combine_rifle_ammo01.mdl")
function SWEP:Initialize()
    if CLIENT then
        -- create ghosted indicator
        local ghost = ents.CreateClientProp(ghostmdl)
        if IsValid(ghost) then
            ghost:SetPos(self:GetPos())
            ghost:Spawn()

            -- PhysPropClientside whines here about not being able to parse the
            -- physmodel. This is not important as we won't use that anyway, and it
            -- happens in sandbox as well for the ghosted ents used there.

            ghost:SetSolid(SOLID_NONE)
            ghost:SetMoveType(MOVETYPE_NONE)
            ghost:SetNotSolid(true)
            ghost:SetRenderMode(RENDERMODE_TRANSCOLOR)
            ghost:AddEffects(EF_NOSHADOW)
            ghost:SetNoDraw(true)

            self.Ghost = ghost
        end
    end

    self.IsCharging = false
    self:SetCharge(0)

    return self.BaseClass.Initialize(self)
end

function SWEP:PreDrop()
    self.IsCharging = false
    self:SetCharge(0)

    -- OnDrop does not happen on client
    self.Weapon:CallOnClient("HideGhost", "")
end

function SWEP:HideGhost()
    if IsValid(self.Ghost) then
        self.Ghost:SetNoDraw(true)
    end
end

function SWEP:PrimaryAttack()
    self.Weapon:SetNextPrimaryFire(CurTime() + 0.1)
    if self.Owner:KeyDown(IN_USE) then
        self:ShootTracer( "AR2Tracer")
        self.Weapon:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
        self.Weapon:SetNextPrimaryFire( CurTime() + 0.15 )

        if not self:CanPrimaryAttack() then return end

        if not worldsnd then
            self.Weapon:EmitSound( self.Primary.Sound, self.Primary.SoundLevel )
        elseif SERVER then
            sound.Play(self.Primary.Sound, self:GetPos(), self.Primary.SoundLevel)
        end

        self:ShootBullet( 12, 0.005, 1, 0.005 )


        local owner = self.Owner
        if not IsValid(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

        owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )
        return

    end

    if not self:CanPrimaryAttack() then return end

    if SERVER then
        if self.IsCharging then return end

        local ply = self.Owner
        if not IsValid(ply) then return end

        local tr = util.TraceLine({start=ply:GetShootPos(), endpos=ply:GetShootPos() + ply:GetAimVector() * maxrange, filter={ply, self.Entity}, mask=MASK_SOLID})

        if tr.HitNonWorld and ValidTarget(tr.Entity)  then

            self:CreateHammer(tr.Entity, tr.HitPos)

            self.Weapon:EmitSound(self.Primary.Sound)

            self:TakePrimaryAmmo(1)

            self.Weapon:SetNextPrimaryFire(CurTime() + self.Primary.Delay)
        end
    end
end

SWEP.SecondaryProps = {
    "models/props_c17/oildrum001_explosive.mdl",
    "models/Combine_Helicopter/helicopter_bomb01.mdl",
    "models/props_c17/oildrum001.mdl",
    "models/props_c17/furnitureboiler001a.mdl",
    "models/maxofs2d/companion_doll.mdl"
   --"models/props_junk/TrafficCone001a.mdl",
    --"models/props_junk/metal_paintcan001a.mdl",
  --  "models/Gibs/HGIBS.mdl",
   -- "models/Gibs/Fast_Zombie_Legs.mdl",
   -- "models/props_c17/doll01.mdl",
   -- "models/props_lab/huladoll.mdl",
   -- "models/props_lab/huladoll.mdl",
    --"models/props_lab/desklamp01.mdl",
   -- "models/props_junk/garbage_glassbottle003a.mdl",
   -- "models/weapons/w_shot_gb1014.mdl",

}

SWEP.nextreload = 0
function SWEP:Reload()
    if self.nextreload > CurTime() then return end
    self.nextreload = CurTime() + 0.3
    if self.Owner:KeyDown(IN_USE) then
        self.nextreload = CurTime() + 3
        self:throw_attack ( table.Random(self.SecondaryProps) , nil, 65);
    end

end

function SWEP:SecondaryAttack()
--    if self.IsCharging then return end

    self.Weapon:SetNextSecondaryFire( CurTime() + 0.1 )

    if SERVER then
       if IsValid(self.hammer) then
           self.hammer:NextThink(CurTime() + 0.001)
       end

    end
end

SWEP.lastplayer = 0
function SWEP:CreateHammer(tgt, pos)

    local hammer = false

        if tgt:IsPlayer( ) then
            if self.lastplayer > CurTime() then
                self.Owner:PrintMessage( HUD_PRINTCENTER, "Wait ".. math.Round(self.lastplayer - CurTime()) .." seconds"  )
                return

            end
            hammer = ents.Create("silent_pushhammer")

            self.lastplayer = CurTime() + 25
        else
            hammer = ents.Create("ttt_physhammer")
        end

    if IsValid(hammer) then
        local ang = self.Owner:GetAimVector():Angle()
        ang:RotateAroundAxis(ang:Right(), 90)

        hammer:SetPos(pos)
        hammer:SetAngles(ang)

        hammer:Spawn()

        hammer:SetOwner(self.Owner)

        local stuck = hammer:StickTo(tgt)
        self.hammer = hammer
        if not stuck then hammer:Remove() end

    end
end

function SWEP:OnRemove()
    if CLIENT and IsValid(self.Ghost) then
        self.Ghost:Remove()
    end

    self.IsCharging = false
    self:SetCharge(0)
end

function SWEP:Holster()
    if CLIENT and IsValid(self.Ghost) then
        self.Ghost:SetNoDraw(true)
    end

    self.IsCharging = false
    self:SetCharge(0)

    return self.BaseClass.Holster(self)
end


if SERVER then

    local CHARGE_AMOUNT = 0.015
    local CHARGE_DELAY = 0.025

    function SWEP:Think()
        if not IsValid(self.Owner) then return end

        if self.IsCharging and self.Owner:KeyDown(IN_ATTACK2) then
            local tr = self.Owner:GetEyeTrace(MASK_SOLID)
            if tr.HitNonWorld and ValidTarget(tr.Entity) then

                if self:GetCharge() >= 1 then
                    self:CreateHammer(tr.Entity, tr.HitPos)

                    self.Weapon:EmitSound(self.Primary.Sound)

                    self:TakePrimaryAmmo(1)

                    self.Weapon:SetNextPrimaryFire(CurTime() + self.Primary.Delay)

                    self.IsCharging = false
                    self:SetCharge(0)
                    return true
                elseif self.NextCharge < CurTime() then
                    local d = tr.Entity:GetPos():Distance(self.Owner:GetPos())
                    local f = math.max(1, math.floor(d / maxrange))

                    self:SetCharge(math.min(1, self:GetCharge() + (CHARGE_AMOUNT / f)))

                    self.NextCharge = CurTime() + CHARGE_DELAY
                end
            else
                self.IsCharging = false
                self:SetCharge(0)
            end

        elseif self:GetCharge() > 0 then
            -- owner let go of rmouse
            self:SetCharge(0)
            self.IsCharging = false
        end
    end
end

local function around( val )
    return math.Round( val * (10 ^ 3) ) / (10 ^ 3);
end

if CLIENT then
    local surface = surface

    function SWEP:UpdateGhost(pos, c, a)
        if IsValid(self.Ghost) then
            if self.Ghost:GetPos() != pos then
            self.Ghost:SetPos(pos)
            local ang = LocalPlayer():GetAimVector():Angle()
            ang:RotateAroundAxis(ang:Right(), 90)

            self.Ghost:SetAngles(ang)

            self.Ghost:SetColor(Color(c.r, c.g, c.b, a))

            self.Ghost:SetNoDraw(false)
            end
        end
    end

    local linex = 0
    local liney = 0
    local laser = Material("trails/laser")
    function SWEP:ViewModelDrawn()
        local client = LocalPlayer()
        local vm = client:GetViewModel()
        if not IsValid(vm) then return end

        local plytr = client:GetEyeTrace(MASK_SHOT)

        local muzzle_angpos = vm:GetAttachment(1)
        local spos = muzzle_angpos.Pos + muzzle_angpos.Ang:Forward() * 10
        local epos = client:GetShootPos() + client:GetAimVector() * maxrange

        -- Painting beam
        local tr = util.TraceLine({start=spos, endpos=epos, filter=client, mask=MASK_ALL})

        local c = COLOR_RED
        local a = 150
        local d = (plytr.StartPos - plytr.HitPos):Length()
        if plytr.HitNonWorld then
            if ValidTarget(plytr.Entity) then
                if d < maxrange then
                    c = COLOR_GREEN
                    a = 255
                else
                    c = COLOR_YELLOW
                end
            end
        end

        self:UpdateGhost(plytr.HitPos, c, a)

        render.SetMaterial(laser)
        render.DrawBeam(spos, tr.HitPos, 5, 0, 0, c)

        -- Charge indicator
        local vm_ang = muzzle_angpos.Ang
        local cpos = muzzle_angpos.Pos + (vm_ang:Up() * -8) + (vm_ang:Forward() * -5.5) + (vm_ang:Right() * 0)
        local cang = vm:GetAngles()
        cang:RotateAroundAxis(cang:Forward(), 90)
        cang:RotateAroundAxis(cang:Right(), 90)
        cang:RotateAroundAxis(cang:Up(), 90)

        cam.Start3D2D(cpos, cang, 0.05)

        surface.SetDrawColor(255, 55, 55, 50)
        surface.DrawOutlinedRect(0, 0, 50, 15)

        local sz = 48
        local next = self.Weapon:GetNextPrimaryFire()
        local ready = (next - CurTime()) <= 0
        local frac = 1.0
        if not ready then
            frac = 1 - ((next - CurTime()) / self.Primary.Delay)
            sz = sz * math.max(0, frac)
        end

        surface.SetDrawColor(255, 10, 10, 170)
        surface.DrawRect(1, 1, sz, 13)

        surface.SetTextColor(255,255,255,15)
        surface.SetFont("Default")
        surface.SetTextPos(2,0)
        surface.DrawText(string.format("%.3f", around(frac)))

        surface.SetDrawColor(0,0,0, 80)
        surface.DrawRect(linex, 1, 3, 13)

        surface.DrawLine(1, liney, 48, liney)

        linex = linex + 3 > 48 and 0 or linex + 1
        liney = liney > 13 and 0 or liney + 1

        cam.End3D2D()
        self.BaseClass.ViewModelDrawn(self)
    end

    local draw = draw
    function SWEP:DrawHUD()
        local x = ScrW() / 2.0
        local y = ScrH() / 2.0


        local charge = self.dt.charge

        if charge > 0 then
            y = y + (y / 3)

            local w, h = 100, 20

            surface.DrawOutlinedRect(x - w/2, y - h, w, h)

            if LocalPlayer():IsTraitor() then
                surface.SetDrawColor(255, 0, 0, 155)
            else
                surface.SetDrawColor(0, 255, 0, 155)
            end

            surface.DrawRect(x - w/2, y - h, w * charge, h)

            surface.SetFont("TabLarge")
            surface.SetTextColor(255, 255, 255, 180)
            surface.SetTextPos( (x - w / 2) + 3, y - h - 15)
            surface.DrawText("CHARGE")
        end
    end
end

local ENT = {}
ENT.Type = "anim"
ENT.Model = Model("models/Items/combine_rifle_ammo01.mdl")

ENT.Stuck = false
ENT.Weaponised = false

ENT.PunchMax = 6
ENT.PunchRemaining = 6


function ENT:Initialize()
    self.Entity:SetModel(self.Model)

    self.Entity:SetSolid(SOLID_NONE)

    if SERVER then
        self:SetGravity(0.4)
        self:SetFriction(1.0)
        self:SetElasticity(0.45)

        self.Entity:NextThink(CurTime() + 1)
    end

    self:SetColor(Color(55, 50, 250, 255))

    self.Stuck = false
    self.PunchMax = 6
    self.PunchRemaining = self.PunchMax
end


function ENT:StickTo(ent)
    if (not IsValid(ent))  then return false end

    local phys = ent:GetPhysicsObject()
    if (not IsValid(phys)) or (not phys:IsMoveable()) then return false end

    --   local norm = self:GetAngles():Up()

    self:SetParent(ent)

    ent:SetPhysicsAttacker(self:GetOwner())
    ent:SetNWBool("punched", true)
    self.PunchEntity = ent

    self:StartEffects()

    self.Stuck = true

    return true
end

function ENT:OnRemove()
    if IsValid(self.BallSprite) then
        self.BallSprite:Remove()
    end

    if IsValid(self.PunchEntity) then
        self.PunchEntity:SetPhysicsAttacker(self.PunchEntity)
        self.PunchEntity:SetNWBool("punched", false)
    end
end

function ENT:StartEffects()
    -- MAKE IT PRETTY

    local sprite = ents.Create("env_sprite")
    if IsValid(sprite) then
        --      local angpos = self:GetAttachment(ball)
        -- sometimes attachments don't work (Lua-side) on dedicated servers,
        -- so have to fudge it
        local ang = self:GetAngles()
        local pos = self:GetPos() + self:GetAngles():Up() * 9
        sprite:SetPos(pos)
        sprite:SetAngles(ang)
        sprite:SetParent(self.Entity)

        sprite:SetKeyValue("model", "sprites/combineball_glow_blue_1.vmt")
        sprite:SetKeyValue("spawnflags", "1")
        sprite:SetKeyValue("scale", "0.25")
        sprite:SetKeyValue("rendermode", "5")
        sprite:SetKeyValue("renderfx", "7")

        sprite:Spawn()
        sprite:Activate()

        self.BallSprite = sprite

    end

    local effect = EffectData()
    effect:SetStart(self:GetPos())
    effect:SetOrigin(self:GetPos())
    effect:SetNormal(self:GetAngles():Up())
    util.Effect("ManhackSparks", effect, true, true)

    if SERVER then
        local ball = self:LookupAttachment("attach_ball")
        util.SpriteTrail(self.Entity, ball, Color(250, 250, 250), false, 30, 0, 1, 0.07, "trails/physbeam.vmt")
    end
end

if SERVER then

    local diesound = Sound("weapons/physcannon/energy_disintegrate4.wav")
    local punchsound = Sound("weapons/ar2/ar2_altfire.wav")


    function ENT:Boombox()
        local pos = self:GetPos()

        util.BlastDamage(self, self:GetOwner(), pos, 75, 75)

        sound.Play(diesound, pos, 100, 100)
        self:Remove()


        local effect = EffectData()
        effect:SetStart(pos)
        effect:SetOrigin(pos)
        util.Effect("Explosion", effect, true, true)
    end

    function ENT:Think()
        if not self.Stuck then return end

        if self.PunchRemaining <= 0 then
            --         self.Entity:StopParticles()

            local pos = self:GetPos()

            util.BlastDamage(self, self:GetOwner(), pos, 65, 65)

            sound.Play(diesound, pos, 100, 100)
            self:Remove()


            local effect = EffectData()
            effect:SetStart(pos)
            effect:SetOrigin(pos)
            util.Effect("Explosion", effect, true, true)
        else
            self.PunchRemaining = self.PunchRemaining - 1

            if IsValid(self.PunchEntity) and IsValid(self.PunchEntity:GetPhysicsObject()) then
                local punchphys = self.PunchEntity:GetPhysicsObject()


                -- Make physexplosion
                local phexp = ents.Create("env_physexplosion")
                if IsValid(phexp) then
                    phexp:SetPos(self:GetPos())
                    phexp:SetKeyValue("magnitude", 20)
                    phexp:SetKeyValue("radius", 20)
                    phexp:SetKeyValue("spawnflags", 1 + 2)
                    phexp:Spawn()
                    phexp:Fire("Explode", "", 0)
                end

                local norm = self:GetAngles():Up() * -1

                -- Add speed to ourselves
                local base = 120
                local bonus = punchphys:GetMass() * 3

                local vel = math.max(base * 2, base + bonus)

                self.PunchEntity:SetVelocity(self:GetOwner():GetForward() * 500 + Vector(0, 0, 500))

                util.BlastDamage(self, self:GetOwner(), self:GetPos(), 5, 5)

                local effect = EffectData()
                effect:SetStart(self:GetPos())
                effect:SetOrigin(self:GetPos())
                effect:SetNormal(norm * -1)
                effect:SetRadius(16)
                effect:SetScale(1)
                util.Effect("ManhackSparks", effect, true, true)

                sound.Play(punchsound, self:GetPos(), 80, 100)
            end
        end

        local delay = math.max(0.1, self.PunchRemaining / self.PunchMax) * 3
        self.Entity:NextThink(CurTime() + delay)
        return true
    end
end

scripted_ents.Register( ENT, "silent_pushhammer", true )