if ( SERVER ) then

	AddCSLuaFile( )
	SWEP.HasRail = true
	SWEP.NoEOTech = true
	SWEP.NoDocter = true
	SWEP.NoAimpoint = true
	SWEP.RequiresRail = false
	
	SWEP.NoSightBG = 2
	SWEP.SightBG = 0
	SWEP.MainBG = 2
end

SWEP.BulletLength = 7.62
SWEP.CaseLength = 51

SWEP.MuzVel = 557.741

SWEP.Attachments = {
	[1] = {"kobra", "riflereflex", "eotech", "aimpoint", "elcan", "acog", "ballistic"},
	[2] = {"vertgrip", "grenadelauncher", "bipod"},
	[3] = {"laser"}}
	
SWEP.InternalParts = {
	[1] = {{key = "hbar"}, {key = "lbar"}},
	[2] = {{key = "hframe"}},
	[3] = {{key = "ergonomichandle"}},
	[4] = {{key = "customstock"}},
	[5] = {{key = "lightbolt"}, {key = "heavybolt"}},
	[6] = {{key = "gasdir"}}}

if ( CLIENT ) then
	SWEP.PrintName			= "Rail gun"
	SWEP.Author				= "Counter-Strike"
	SWEP.Slot = 2 //				= 3
	SWEP.SlotPos = 0 //			= 3
	SWEP.IconLetter			= "z"
	SWEP.DifType = true
	SWEP.SmokeEffect = "cstm_child_smoke_large"
	SWEP.SparkEffect = "cstm_child_sparks_large"
	SWEP.Muzzle = "cstm_muzzle_br"
	SWEP.ACOGDist = 6
	SWEP.BallisticDist = 4.5
	SWEP.NoRail = true
	
	SWEP.ActToCyc = {}
	SWEP.ActToCyc["ACT_VM_DRAW"] = 0.45
	SWEP.ActToCyc["ACT_VM_RELOAD"] = 0.8
	
	SWEP.LaserTune = {
		PosRight = 0,
		PosForward = 0,
		PosUp = 0,
		AngUp = -90,
		AngRight = 0,
		AngForward = 0 }
		
	SWEP.VertGrip_Idle = {
		['Left24'] = {vector = Vector(0, 0, 0), angle = Angle(12, 22.194000244141, 0)},
		['Left5'] = {vector = Vector(0, 0, 0), angle = Angle(44.180999755859, 0, 0)},
		['Left_L_Arm'] = {vector = Vector(0.75, 1, 1), angle = Angle(0, 0, 94.130996704102)},
		['Left3'] = {vector = Vector(0, 0, 0), angle = Angle(0, 56.924999237061, 0)},
		['Left17'] = {vector = Vector(0, 0, 0), angle = Angle(22.368999481201, 0, 0)},
		['Left19'] = {vector = Vector(0, 0, 0), angle = Angle(20.893999099731, 0, 0)},
		['Left15'] = {vector = Vector(0, 0, 0), angle = Angle(47.96900177002, 0, 0)},
		['Left14'] = {vector = Vector(0, 0, 0), angle = Angle(35.25, 0, 0)},
		['Left12'] = {vector = Vector(0, 0, 0), angle = Angle(7.4310002326965, 0, 0)},
		['Left8'] = {vector = Vector(0, 0, 0), angle = Angle(29.837999343872, 0, 0)},
		['Left2'] = {vector = Vector(0, 0, 0), angle = Angle(0, 55.01900100708, 0)},
		['Left6'] = {vector = Vector(0, 0, 0), angle = Angle(12.769000053406, 0, 0)},
		['Left18'] = {vector = Vector(0, 0, 0), angle = Angle(27.299999237061, 0, 0)},
		['Left_U_Arm'] = {vector = Vector(-0.59399998188019, 0.83700001239777, 0.75), angle = Angle(0, 0, 0)},
		['Left11'] = {vector = Vector(0, 0, 0), angle = Angle(29.662000656128, 0, 0)},
		['Left16'] = {vector = Vector(0, 0, 0), angle = Angle(-29.756000518799, 0, 0)}}
		
	SWEP.GrenadeLauncher_Idle = {
		['Left_U_Arm'] = {vector = Vector(2.4809999465942, -3, -1), angle = Angle(0, 0, 0)}}
	
	SWEP.WepSelectIcon = surface.GetTextureID("VGUI/kills/kill_scarh")
	killicon.Add("cstm_rif_scarh", "VGUI/kills/kill_scarh", Color( 255, 80, 0, 255 ))

    SWEP.VElements = {
        ["pipe1+"] = { type = "Model", model = "models/props_c17/GasPipes006a.mdl", bone = "base", rel = "", pos = Vector(2.273, -7.618, -1.255), angle = Angle(-90, 180, 0.382), size = Vector(0.076, 0.076, 0.076), color = Color(255, 255, 255, 255), surpresslightning = false, material = "phoenix_storms/MetalSet_1-2", skin = 0, bodygroup = {} },
        ["pipe1"] = { type = "Model", model = "models/props_c17/GasPipes006a.mdl", bone = "base", rel = "", pos = Vector(2.267, -7.224, -1.43), angle = Angle(-90, 180, 0.382), size = Vector(0.076, 0.076, 0.076), color = Color(255, 255, 255, 255), surpresslightning = false, material = "phoenix_storms/MetalSet_1-2", skin = 0, bodygroup = {} },
        ["indicator_holder"] = { type = "Model", model = "models/Items/battery.mdl", bone = "base", rel = "", pos = Vector(0.177, -6.804, -1.203), angle = Angle(180, 180, 90), size = Vector(0.109, 0.109, 0.109), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
        ["indicator"] = { type = "Sprite", sprite = "sprites/glow04", bone = "base", rel = "", pos = Vector(-0.332, -7.698, -1.203), size = { x = 1, y = 1 }, color = Color(255, 255, 255, 255), nocull = true, additive = true, vertexalpha = true, vertexcolor = true, ignorez = false}
    }

	
	SWEP.WElements = {
		}
end
SWEP.Kind = WEAPON_HEAVY
SWEP.HoldType			= "ar2"
SWEP.Base				= "aa_base"
SWEP.Category = "Extra Pack (BRs)"

SWEP.FireModes = {"auto", "semi"}

SWEP.Spawnable			= true
SWEP.AdminSpawnable		= true

SWEP.ViewModelFOV = 70
SWEP.ViewModelFlip = false
SWEP.HoldType 			= "ar2" -- the animation in thirdperson
SWEP.ViewModel			= "models/weapons/v_combinesniper_e2.mdl" -- model you will see in first person
SWEP.WorldModel			= "models/weapons/w_combinesniper_e2.mdl" -- model you will see in thirdperson
SWEP.ViewModelFlip		= false -- is the models is left handed ()


SWEP.ViewModelBonescales = {}

SWEP.Weight				= 5
SWEP.AutoSwitchTo		= false
SWEP.AutoSwitchFrom		= false

SWEP.Primary.Sound			= Sound("CSTM_SCARH")
SWEP.Primary.Recoil			= 0.3
SWEP.Primary.Damage			= 95
SWEP.Primary.NumShots		= 1
SWEP.Primary.Cone			= 0.02
SWEP.Primary.ClipSize		= 20
SWEP.Primary.Delay			= 1.4
SWEP.Primary.DefaultClip	= 120
SWEP.Primary.Automatic		= true
SWEP.Primary.Ammo			= "7.62x51MM"
SWEP.InitialHoldtype = "smg"
SWEP.InHoldtype = "smg"
SWEP.SilencedSound = Sound("CSTM_SilencedShot2")
SWEP.SilencedVolume = 78

-- Animation speed/custom reload function related
SWEP.IsReloading = false
SWEP.NoBoltAnim = true
SWEP.AnimPrefix = ""
SWEP.ReloadSpeed = 1
SWEP.ShouldBolt = false
SWEP.ReloadDelay = 0
SWEP.IncAmmoPerc = 0.63 -- Amount of frames required to pass (in percentage) of the reload animation for the weapon to have it's amount of ammo increased

-- Dynamic accuracy related
SWEP.ShotsAmount 			= 0
SWEP.ConeDecAff				= 0
SWEP.DefRecoil				= 1.55
SWEP.CurCone				= 0.04
SWEP.DecreaseRecoilTime 	= 0
SWEP.ConeAff1 				= 0 -- Crouching/standing
SWEP.ConeAff2 				= 0 -- Using ironsights

SWEP.UnConeTime				= 0 -- Amount of time after firing the last shot that needs to pass until accuracy increases
SWEP.FinalCone				= 0 -- Self explanatory
SWEP.VelocitySensivity		= 2.2 -- Percentage of how much the cone increases depending on the player's velocity (moving speed). Rifles - 100%; SMGs - 80%; Pistols - 60%; Shotguns - 20%
SWEP.HeadbobMul 			= 1
SWEP.IsSilenced 			= false
SWEP.IronsightsCone			= 0.001
SWEP.HipCone 				= 0.06
SWEP.InaccAff1 = 0.75
SWEP.ConeInaccuracyAff1 = 0.7

SWEP.Secondary.Automatic	= false
SWEP.Secondary.Ammo			= "40MM_HE"
SWEP.Secondary.ClipSize = 1
SWEP.Secondary.DefaultClip	= 0

SWEP.IsUsingIronsights 		= false
SWEP.TargetMul = 0
SWEP.SetAndForget			= false

SWEP.IronSightsPos = Vector(6.0749, -5.5216, 2.3984)
SWEP.IronSightsAng = Vector(2.5174, -0.0099, 0)

SWEP.AimPos = Vector(-2.018, -3.135, 0.12)
SWEP.AimAng = Vector(0, 0, 0)

SWEP.ChargePos = Vector (5.4056, -10.3522, -4.0017)
SWEP.ChargeAng = Vector (-1.7505, -55.5187, 68.8356)

SWEP.NoFlipOriginsPos = Vector(-0.361, 0, 0.159)
SWEP.NoFlipOriginsAng = Vector(0, 0, 0)

SWEP.KobraPos = Vector(-2.014, -2.681, 0.453)
SWEP.KobraAng = Vector(0, 0, 0)

SWEP.RReflexPos = Vector(-2.014, -2.681, 0.653)
SWEP.RReflexAng = Vector(0, 0, 0)

SWEP.BallisticPos = Vector(-2.02, -4, 0.363)
SWEP.BallisticAng = Vector(0, 0, 0)

SWEP.ReflexPos = Vector(-2.03, -2.681, 0.17)
SWEP.ReflexAng = Vector(0, 0, 0)

SWEP.ScopePos = Vector(-2.008, -2.681, 0.367)
SWEP.ScopeAng = Vector(0, 0, 0)

SWEP.ACOGPos = Vector(-2.02, -2.681, 0.041)
SWEP.ACOGAng = Vector(0, 0, 0)

SWEP.ACOG_BackupPos = Vector(-2.02, -2.681, -1.08)
SWEP.ACOG_BackupAng = Vector(0, 0, 0)

SWEP.ELCANPos = Vector(-2.02, -2.681, 0.02)
SWEP.ELCANAng = Vector(0, 0, 0)

SWEP.ELCAN_BackupPos = Vector(-2.02, -2.681, -0.98)
SWEP.ELCAN_BackupAng = Vector(0, 0, 0)

function SWEP:SecondaryAttack()
    if self.Owner:KeyDown(IN_USE) then self:Zoom() end
end
function SWEP:PreDrop(yes)
    self:SetZoom(false)
    self:SetIronsights(false)
    if yes then self:Remove() end
    return self.BaseClass.PreDrop(self)
end

function SWEP:Reload()
    self.Weapon:DefaultReload( ACT_VM_RELOAD );
    self:SetIronsights( false )
    self:SetZoom(false)
end

function SWEP:Holster()
    self:SetIronsights(false)
    self:SetZoom(false)
    return true
end

SWEP.IronSightsPos      = Vector( -100, -100, -100 )
SWEP.IronSightsAng      = Vector( 2.6, 1.37, 3.5 )
SWEP.Secondary.Sound = Sound("Default.Zoom")
-- Add some zoom to ironsights for this gun
function SWEP:Zoom()
    if not self.IronSightsPos then return end
    if self.Weapon:GetNextSecondaryFire() > CurTime() then return end

    local bIronsights = not self:GetIronsights()

    self:SetIronsights( bIronsights )

    if SERVER then
        self:SetZoom(bIronsights)
    else
        self:EmitSound(self.Secondary.Sound)
    end

    self.Weapon:SetNextSecondaryFire( CurTime() + 0.3)
end

-- CHARGING
SWEP.IsHolding = false
SWEP.LastHold = 0
SWEP.Sound = false
SWEP.NextHold = 0
function SWEP:Think()
    if self.Owner:KeyDown(IN_ATTACK2) then
        if self.NextHold > CurTime() then return end
        if not self.IsHolding then
            self.LastHold = CurTime()
            self.IsHolding = true
            self.Sound = CreateSound( self,"weapons/gauss/chargeloop.wav");
            self.Sound:Play();
            self.Sound:ChangePitch(250, 1)
        end

        if self.IsHolding and self.LastHold + 1.5 < CurTime() and self.NextHold < CurTime() then
            self.NextHold = CurTime() + 1.5
            self:SuperShot()
            self.IsHolding = false
            self.IsHolding = false
            self.Sound:Stop();
            self.Sound = false
        end
    elseif self.IsHolding then
        self.IsHolding = false
        self.Sound:Stop();
        self.Sound = false
    end
end

SWEP.TracerColor = Color(255,255,255,255)
function SWEP:SuperShot()

    if SERVER then
        local tr = self.Owner:GetEyeTrace()

        local proximity_ents = ents.FindInSphere(tr.HitPos, 80)
        for k, v in pairs(proximity_ents) do
            dist = v:GetPos():Distance( self.Owner:GetPos() )
            if( dist < 450) then
                v:TakeDamage( math.random(100, 100), self.Owner, self )
            else
                v:TakeDamage( math.random(50,90), self.Owner, self )
            end
        end

        self:EmitSound("weapons/big_explosion.mp3")
    end
    self:FireBolt()
    self:ShootTracer( "LaserTracer_thick")
end



function SWEP:SetZoom(state)
    if CLIENT then
        return
    elseif IsValid(self.Owner) and self.Owner:IsPlayer() then
        if state then
            self.Owner:SetFOV(30, 0.3)
        else
            self.Owner:SetFOV(0, 0.2)
        end
    end
end

if CLIENT then
    local scope = surface.GetTextureID("sprites/scope")
    function SWEP:DrawHUD()
        if self:GetIronsights() then
            surface.SetDrawColor( 0, 0, 0, 255 )

            local x = ScrW() / 2.0
            local y = ScrH() / 2.0
            local scope_size = ScrH()

            -- crosshair
            local gap = 80
            local length = scope_size
            surface.DrawLine( x - length, y, x - gap, y )
            surface.DrawLine( x + length, y, x + gap, y )
            surface.DrawLine( x, y - length, x, y - gap )
            surface.DrawLine( x, y + length, x, y + gap )

            gap = 0
            length = 50
            surface.DrawLine( x - length, y, x - gap, y )
            surface.DrawLine( x + length, y, x + gap, y )
            surface.DrawLine( x, y - length, x, y - gap )
            surface.DrawLine( x, y + length, x, y + gap )


            -- cover edges
            local sh = scope_size / 2
            local w = (x - sh) + 2
            surface.DrawRect(0, 0, w, scope_size)
            surface.DrawRect(x + sh - 2, 0, w, scope_size)

            surface.SetDrawColor(255, 0, 0, 255)
            surface.DrawLine(x, y, x + 1, y + 1)

            -- scope
            surface.SetTexture(scope)
            surface.SetDrawColor(255, 255, 255, 255)

            surface.DrawTexturedRectRotated(x, y, scope_size, scope_size, 0)

        else
            return self.BaseClass.DrawHUD(self)
        end
    end

    function SWEP:AdjustMouseSensitivity()
        return (self:GetIronsights() and 0.2) or nil
    end
end

