AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")
include('shared.lua')

function ENT:Initialize()
	self.Entity:SetModel("models/weapons/w_snowball_thrown.mdl");
	self.Entity:PhysicsInit(SOLID_VPHYSICS);
	self.Entity:SetMoveType(MOVETYPE_VPHYSICS);
	self.Entity:SetSolid(SOLID_VPHYSICS);
	self.Entity:SetCollisionGroup(COLLISION_GROUP_PROJECTILE)
    self.Damage = 1
	local phys = self:GetPhysicsObject()
	if phys:IsValid() then
		phys:Wake()
		phys:EnableGravity(true);
	end
	--self.Trail = util.SpriteTrail(self.Entity, 0, Color(255,255,255,255), false, 15, 1, 0.2, 1/(15+1)*0.5, "trails/laser.vmt")
end

function ENT:Think()
end

function ENT:SetDamage( dmg)
    self.Damage = dmg
end

function ENT:SpawnFunction(ply, tr)
	if (!tr.Hit) then return end
	local SpawnPos = tr.HitPos + tr.HitNormal * 16;
	local ent = ents.Create("ent_hankball");
	ent:SetPos(SpawnPos);
	ent:Spawn();
	ent:Activate();
	ent:SetOwner(ply)
	ent:SetPhysicsAttacker(self.Owner)
	return ent;
end

function ENT:PhysicsCollide(data)
	local pos = self.Entity:GetPos() --Get the position of the snowball
	local effectdata = EffectData()
	data.HitObject:ApplyForceCenter(self:GetPhysicsObject():GetVelocity() * 40)
	data.HitObject:GetEntity():TakeDamage( self.Damage , self.Owner, self);
	
	--effectdata:SetStart( pos )
	--effectdata:SetOrigin( pos )
	--effectdata:SetScale( 1.5 )
	--util.Effect( "watersplash", effectdata ) -- effect
	--util.Effect( "inflator_magic", effectdata ) -- effect
	--util.Effect( "WheelDust", effectdata ) -- effect
	--util.Effect( "GlassImpact", effectdata ) -- effect
	--local damage = ents.Create("point_hurt")
	--damage:SetPos(self.Entity:GetPos())
	--damage:SetKeyValue("DamageRadius", 2)
	--damage:SetKeyValue("Damage" , 200)
	--damage:SetKeyValue("DamageDelay", 0)
	--damage:SetKeyValue("DamageType" ,"256")
	--damage:SetKeyValue("DamageType" ,"128")
	self.Entity:Remove(); --Remove the snowball
end 