ENT.Type = "anim"
ENT.Base = "base_follower"
ENT.RenderGroup = RENDERGROUP_TRANSLUCENT

function ENT:Initialize()

	self.ModelString = "models/props_lab/cactus.mdl"
	self.ModelScale = 1
	self.Shadows = true
	self.Particles = false
	self.PetColor = Color( 255, 255, 255, 255 )
	
	self.BaseClass.Initialize( self )
	
end