//Precache the sounds in use.

AddCSLuaFile( "cl_init.lua" )
AddCSLuaFile( "shared.lua" )
include( 'shared.lua' )

ENT.Seats = {}
ENT.Seat = NULL
ENT.SeatTwo = NULL



function ENT:SpawnFunction( ply, tr )

	if ( !tr.Hit ) then return end
	
	local SpawnPos = tr.HitPos + tr.HitNormal * 16
	
	local ent = ents.Create( "sent_adventurecouch" )
		ent:SetPos( SpawnPos )
	ent:Spawn()
	ent:Activate()
	
	return ent
	
end


function ENT:Initialize()

	self.Entity:SetModel( "models/nova/chair_office02.mdl" )
	
	self.Entity:PhysicsInit( SOLID_VPHYSICS )
	self.Entity:SetMoveType( MOVETYPE_VPHYSICS )
	self.Entity:SetSolid( SOLID_VPHYSICS )
	self.Adventure = true
    self.punchPower = -1000
	
	
	self.Seats[1] = ents.Create("prop_vehicle_prisoner_pod")  
    self.Seats[1]:SetKeyValue("vehiclescript","scripts/vehicles/ACSeat.txt")  
    self.Seats[1]:SetModel( "models/nova/airboat_seat.mdl" ) 
	self.Seats[1]:SetNoDraw(true)
	--self.Seats[1]:SetCollisionGroup(COLLISION_GROUP_NONE)
    self.Seats[1]:SetPos( self.Entity:GetPos() + (self.Entity:GetForward()*-0) + ( self.Entity:GetRight() * -4.5 ) + ( self.Entity:GetUp()*17))
	self.Seats[1]:SetAngles(self.Entity:GetAngles() + Angle(0,-0,0))
	self.Seats[1]:SetKeyValue("limitview", "0")  
	self.Seats[1]:SetColor(255,255,255,0)
	self.Seats[1]:Spawn()  
	self.Seats[1]:GetPhysicsObject():EnableGravity(true);
	self.Seats[1]:GetPhysicsObject():SetMass(1)	
	--self.Seats[1]:GetPhysicsObject():Sleep()
	self.Entity.SeatOne = self.Seats[1]
	self.Seats[1].EntOwner = self.Entity
	self.Seats[1].SeatNum = 1
	self.Seats[1].Occupied = false
	self.Entity.UseSeatOne = 0

	constraint.Weld( self.Entity, self.Seats[1], 0, 0, 0, 1 )	


end



function ENT:PhysicsCollide( data, physobj )
	if (!self.Seats[1].Occupied) then
		if (physobj:IsValid()) then
			physobj:Sleep()
		end
	else
		physobj:ApplyForceCenter(-data.HitNormal*data.Speed*self.punchPower )
	end
end


ENT.healthdf = false
function ENT:OnTakeDamage( dmginfo )
    if not self.healthdf then
    self.healthdf = true
    elseif not self.blewup then
        self.blewup = true
        local ent2 = ents.Create( "env_explosion" )

        ent2:SetPos( self:GetPos()  )

        ent2:Spawn()
        ent2:SetKeyValue( "iMagnitude", "10" )
        ent2:Fire( "Explode", 0, 0 )

        ent2:EmitSound( "weapons/big_explosion.mp3" )
        timer.Simple(0.01,function()
        self:Remove() end)
    end

end

function ENT:ExplodeHard()
    if self.exploded then return end self.exploded = true
if not IsValid( self.setOwner ) then return end

local ent = ents.Create( "env_explosion" )

ent:SetPos( self:GetPos()  )
if IsValid(self.setOwner  ) then
ent:SetOwner( self.setOwner  )
ent:SetPhysicsAttacker(  self.setOwner )
end
ent:Spawn()
ent:SetKeyValue( "iMagnitude", "50" )
ent:Fire( "Explode", 0, 0 )


ent:EmitSound( "weapons/big_explosion.mp3" )


self:Remove()
end


function ENT:Use( activator, caller, Player )
	local phys = self.Entity:GetPhysicsObject()

	if (activator.IsPlayer()) then


		if (!self.Seats[1].Occupied) then
			self.Entity.UserOne = activator
			self.Seats[1].Occupied = true

        if (activator.SeatAdventureused  and activator.SeatAdventureused > CurTime()) or (IsValid(self.setOwner) and activator:SteamID() != self.setOwner:SteamID()) then
                if self.setOwner:SteamID() == activator:SteamID() then
                    activator:PrintMessage( HUD_PRINTCENTER, "You can only use me once every minute" )
                    self.punchPower = 5
                    return

                else
                    activator:EnterVehicle(self.Seats[1])
                    activator:PrintMessage( HUD_PRINTCENTER, "Selfdestruct in 5 seconds." )
                    self.punchPower = 1000
                    phys:ApplyForceCenter(Vector(0,0,self.punchPower))
                    timer.Simple(5,function() self:ExplodeHard() end)

                        self:EmitSound("vo/npc/male01/gethellout.wav",130,160)
                    timer.Create("gethealthAdventureSound"..activator:SteamID(),2,1,function()
                            self:EmitSound("vo/npc/male01/gethellout.wav",130,50)
                    end)

                    timer.Simple(5,function()
                        self:EmitSound("ambient/voices/citizen_punches1.wav")
                        if (self.Seats[1].Occupied) then
                            if IsValid(activator) then
                                activator:ExitVehicle()
                            end
                            self.Entity.UserOne = nil
                            self.Seats[1].Occupied = false
                        end
                    end)
                end
                    else
                    self.punchPower = 5
                    activator:EnterVehicle(self.Seats[1])
                    activator.SeatAdventureused = CurTime() + 60
                    timer.Create("gethealthAdventure"..activator:SteamID(),0.24,20,function()
                        if IsValid(activator) and activator:Health() < 150 then
                            activator:SetHealth(activator:Health()+3)
                        end
                    end)


        end







		end
	end
		
	if (phys:IsValid()) then
		phys:Wake()
		self.Seats[1]:GetPhysicsObject():Wake()

		phys:ApplyForceCenter(Vector(0,0,self.punchPower))
	end
	
end


function ENT:OnRemove( )
	if self.Entity and self.Entity.SeatOne and self.Entity.SeatOne:IsValid() then
		self.Entity.SeatOne:Remove()
	end

end


function ENT:OnThink()
	if (!self.Seats[1].Occupied && !self.Seats[2].Occupied) then
		if (phys:IsValid()) then
			phys:Sleep()
		end
	end

	if (self.Seats[1].Occupied) then
		if (self.Entity.UserOne:KeyDown(IN_USE)) then
			self.Entity.UserOne = nil
			self.Seats[1].Occupied = false
		end
	end


end
