ENT.Type = "anim"
ENT.Base = "base_anim"
ENT.PrintName		= "Adventure Couch"
ENT.Author			= "jmoak3"
ENT.Contact			= "mrjmoak3@gmail.com"
ENT.Purpose			= "Hop inside for ADVENTURE"
ENT.Instructions	= "IF THERE IS AN ANIMATION WHEN GETTING IN COUCH ADVENTURE WILL NOT BEGIN. TAKE A STEP BACK AND TRY AGAIN."

ENT.Spawnable			= true
ENT.AdminSpawnable		= true

--ENT.AutomaticFrameAdvance = true
