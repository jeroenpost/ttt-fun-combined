if SERVER then
    AddCSLuaFile()
end

ENT.Type = "anim"
ENT.Base = "base_anim"


if SERVER then

    function ENT:Initialize()
        self:SetModel("models/weapons/w_medkit.mdl")
        self:SetModelScale(self:GetModelScale()*1,0)
        self.Entity:PhysicsInit(SOLID_VPHYSICS);
        self.Entity:SetMoveType(MOVETYPE_VPHYSICS);
        self.Entity:SetSolid(SOLID_VPHYSICS);
        self.Activate2 = CurTime() + 2
        self.Entity:SetCollisionGroup(COLLISION_GROUP_WEAPON)
        local phys = self:GetPhysicsObject()
        if phys:IsValid() then
            phys:Wake()
            phys:EnableGravity(true);
        end
    end

    function ENT:Use( activator, caller )
        if self.Activate2 > CurTime() then return end
        self.Entity:Remove()
        if ( activator:IsPlayer() ) then
            if activator:Health() < 150 then

                activator:SetHealth(activator:Health()+25)

            end
            activator:EmitSound("crisps/eat.mp3")
        end
    end


end

