---- Health dispenser

if SERVER then AddCSLuaFile("shared.lua") end

if CLIENT then
   -- this entity can be DNA-sampled so we need some display info
   ENT.Icon = "VGUI/ttt/icon_health"
   ENT.PrintName = "hstation_name"

   local GetPTranslation = LANG.GetParamTranslation

   ENT.TargetIDHint = {
      name = "Health Station",
      hint = "hstation_hint",
      fmt  = function(ent, txt)
                return GetPTranslation(txt,
                                       { usekey = Key("+use", "USE"),
                                         num    = ent:GetStoredHealth() or 0 } )
             end
   };

end

ENT.Type = "anim"
ENT.Model = Model("models/weapons/w_eq_fraggrenade.mdl")

--ENT.CanUseKey = true
ENT.CanHavePrints = true
ENT.MaxHeal = 120
ENT.MaxStored = 200
ENT.RechargeRate = 1
ENT.RechargeFreq = 2 -- in seconds

ENT.NextHeal = 0
ENT.HealRate = 1
ENT.HealFreq = 0.2

AccessorFuncDT(ENT, "StoredHealth", "StoredHealth")

AccessorFunc(ENT, "Placer", "Placer")

function ENT:SetupDataTables()
   self:DTVar("Int", 0, "StoredHealth")
end

function ENT:SetExplodeTime( time )
    self.NextExplode = CurTime() + time
    timer.Simple(time,function() if IsValid(self) then self:ExplodeHard() end end)
end

function ENT:Initialize()
    self:SetModelScale( self:GetModelScale() * 2,0.1)
   self:SetModel(self.Model)
   self.healsound = "HL1/fvox/health_dropping2.wav"
   self:PhysicsInit(SOLID_VPHYSICS)
   self:SetMoveType(MOVETYPE_VPHYSICS)
   self:SetSolid(SOLID_BBOX)



   timer.Simple(30,function()
   if IsValid(self) then
    self:ExplodeHard()
   end
   end)

   local b = 90
   self:SetCollisionBounds(Vector(-b, -b, -b), Vector(b,b,b))

   self:SetCollisionGroup(COLLISION_GROUP_WEAPON)
   if SERVER then
      self:SetMaxHealth(200)

      local phys = self:GetPhysicsObject()
      if IsValid(phys) then
         phys:SetMass(200)
      end

      self:SetUseType(CONTINUOUS_USE)
   end
   self:SetHealth(200)

   --self:SetColor(Color(180, 180, 250, 255))

   self:SetStoredHealth(200)

   self:SetPlacer(nil)

   self.NextHeal = 0

   self.fingerprints = {}
    self:SetModelScale( self:GetModelScale() * 2,0.1)
end


function ENT:AddToStorage(amount)
   self:SetStoredHealth(math.min(self.MaxStored, self:GetStoredHealth() + amount))
end

function ENT:TakeFromStorage(amount)
   -- if we only have 5 healthpts in store, that is the amount we heal
   amount = math.min(amount, self:GetStoredHealth())
   self:SetStoredHealth(math.max(0, self:GetStoredHealth() - amount))
   return amount
end

local healsound = Sound("items/medshot4.wav")
local failsound = Sound("items/medshotno1.wav")

local last_sound_time = 0
function ENT:GiveHealth(ply, max_heal)

   if self:GetStoredHealth() > 0 then
      max_heal = max_heal or self.MaxHeal
      local dmg = 150 - ply:Health()
      if dmg > 0 then
         -- constant clamping, no risks
         local healed = self:TakeFromStorage(math.min(max_heal, dmg))
         local new = math.min(125, ply:Health() + healed)

         ply:SetHealth(new)

         if IsValid(self.setOwner  ) then
             if self.setOwner:Health() < 174 then
                 self.setOwner:SetHealth( self.setOwner:Health() + ((healed + 1) /2) )
             end

         end



         if last_sound_time + 8 < CurTime() then

            local soundfile = gb_config.websounds..self.healsound
            gb_PlaySoundFromServer(soundfile, self.Owner)
            last_sound_time = CurTime()
         end

         if not table.HasValue(self.fingerprints, ply) then
            table.insert(self.fingerprints, ply)
         end

         return true
      else
         self:EmitSound(failsound)
      end
   else
      self:EmitSound(failsound)
   end

   return false
end

function ENT:Use(ply)
   if IsValid(ply) and ply:IsPlayer() and ply:IsActive() then
      local t = CurTime()
      if t > self.NextHeal then
         local healed = self:GiveHealth(ply, self.HealRate)

         self.NextHeal = t + (self.HealFreq * (healed and 1 or 2))
      end
   end
end

-- traditional equipment destruction effects
function ENT:OnTakeDamage(dmginfo)
   if dmginfo:GetAttacker() == self:GetPlacer() then return end

   self:TakePhysicsDamage(dmginfo)

   self:SetHealth(self:Health() - dmginfo:GetDamage())

   local att = dmginfo:GetAttacker()
   if IsPlayer(att) then
      DamageLog(Format("%s damaged health station for %d dmg",
                       att:Nick(), dmginfo:GetDamage()))
   end

   if self:Health() < 0 then
       self:ExplodeHard()
      self:Remove()

      util.EquipmentDestroyed(self:GetPos())

      if IsValid(self:GetPlacer()) then
         LANG.Msg(self:GetPlacer(), "hstation_broken")
      end
   end
end


function ENT:ExplodeHard()

    if not IsValid( self.setOwner ) then return end

   self:Explode()
end

if SERVER then
   -- recharge
   local nextcharge = 0
   function ENT:Think()
      if nextcharge < CurTime() then
         self:AddToStorage(self.RechargeRate)

         nextcharge = CurTime() + self.RechargeFreq
      end
      if self.NextExplode and self.NextExplode < CurTime() then
          self:ExplodeHard()
      end
   end
end



if SERVER then
    function ENT:Explode()
        if self.exploded then return end self.exploded = true
        self.Entity:EmitSound( Sound("npc/assassin/ball_zap1.wav") )
        local radius = 500
        local phys_force = 1500
        local push_force = 512
        local pos = self.Entity:GetPos()
        -- pull physics objects and push players
        for k, target in pairs(ents.FindInSphere(pos, radius)) do
            if IsValid(target) then
                local tpos = target:LocalToWorld(target:OBBCenter())
                local dir = (tpos - pos):GetNormal()
                local phys = target:GetPhysicsObject()

                if target:IsPlayer() and (not target:IsFrozen()) and ((not target.was_pushed) or target.was_pushed.t != CurTime()) then

                -- always need an upwards push to prevent the ground's friction from
                -- stopping nearly all movement
                dir.z = math.abs(dir.z) + 1

                local push = dir * push_force
                local vel = target:GetVelocity() + push
                vel.z = math.min(vel.z, push_force)
                local pusher = self:GetOwner() or self.Owner or nil

                target:SetVelocity(vel)
                target:SetGroundEntity( nil )
                target.was_pushed = {att=pusher, t=CurTime()}

                elseif IsValid(phys) then
                    phys:ApplyForceCenter(dir * -1 * phys_force)
                end
            end
        end

        local phexp = ents.Create("env_physexplosion")
        if IsValid(phexp) then
            phexp:SetPos(pos)
            phexp:SetKeyValue("magnitude", 100) --max
            phexp:SetKeyValue("radius", radius)
            -- 1 = no dmg, 2 = push ply, 4 = push radial, 8 = los, 16 = viewpunch
            phexp:SetKeyValue("spawnflags", 1 + 2 + 16)
            phexp:Spawn()
            phexp:Fire("Explode", "", 0.2)
        end
    end



end


