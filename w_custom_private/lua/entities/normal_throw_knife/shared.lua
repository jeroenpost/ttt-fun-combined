if SERVER then
    AddCSLuaFile("shared.lua")
else
    ENT.PrintName = "knife_thrown"
    ENT.Icon = "VGUI/ttt/icon_knife"
end

ENT.Type = "anim"
ENT.Base = "base_anim"


if SERVER then

    function ENT:Initialize()
        self:SetModel("models/weapons/w_knife_t.mdl")
        self:SetModelScale(self:GetModelScale()*1,0)
        self:SetColor(Color(128,128,128,255))
        self.Entity:PhysicsInit(SOLID_VPHYSICS);
        self.Entity:SetMoveType(MOVETYPE_VPHYSICS);
        self.Entity:SetSolid(SOLID_VPHYSICS);
        self.active = CurTime() + 0.2

        --  self.Entity:SetCollisionGroup(COLLISION_GROUP_WEAPON)
        local phys = self:GetPhysicsObject()
        if phys:IsValid() then
            phys:Wake()
            phys:EnableGravity(true);
        end

        timer.Simple(3,function()
            if IsValid(self) then self:Remove() end
        end)
    end

    function ENT:Touch( ent )
        if self.exploded then return end self.exploded = true
        if ent:IsPlayer() and not ent.nextsandtouch or (ent.nextsandtouch and ent.nextsandtouch < CurTime()) then
            ent.nextsandtouch = CurTime() + 1
            -- ent:Ignite(0.5,0)
            local dmginfo = DamageInfo()
            if not self.Damage then self.Damage = 50 end
            dmginfo:SetDamage( self.Damage ) --50 damage
            dmginfo:SetDamageType( DMG_BULLET ) --Bullet damage
            dmginfo:SetAttacker( self.owner or ent ) --First player found gets credit
            dmginfo:SetDamageForce( Vector( 0, 0, 50 ) ) --Launch upwards
            dmginfo:SetInflictor(self)
            ent:TakeDamageInfo( dmginfo )
            self:Remove()
        end
    end

    function ENT:PhysicsCollide(data, phys)
        if self.Stuck then return false end

        local other = data.HitEntity
        if IsValid(other) and other:IsPlayer() then
            self:Touch(other)
            self:Remove()
        end
    end


end