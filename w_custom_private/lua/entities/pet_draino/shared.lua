ENT.Type = "anim"
ENT.Base = "base_follower"
ENT.RenderGroup = RENDERGROUP_TRANSLUCENT

function ENT:Initialize()
	
	self.ModelString = 'models/props_junk/garbage_plasticbottle002a.mdl'
	self.ModelScale = 1
	self.Particles = "boat_waves"

	self.BaseClass.Initialize( self )




    local ENT2 = {}

    ENT2.Type = "anim"
    ENT2.Base = "base_anim"

    if CLIENT then
        -- this entity can be DNA-sampled so we need some display info
        ENT2.Icon = "VGUI/ttt/icon_health"
        ENT2.PrintName = "Draino"

        local GetPTranslation = LANG.GetParamTranslation

        ENT2.TargetIDHint = {
            name = "Draino",
            hint = "Drink me to die a horrible death",

        };

    end

    if SERVER then

        function ENT2:Initialize()


            self:SetModel('models/props_junk/garbage_plasticbottle002a.mdl')
            self:SetModelScale(self:GetModelScale()*1,0)

            self.Entity:PhysicsInit(SOLID_VPHYSICS);
            self.Entity:SetMoveType(MOVETYPE_VPHYSICS);
            self.Entity:SetSolid(SOLID_VPHYSICS);
            self.Entity:SetCollisionGroup(COLLISION_GROUP_PROJECTILE)
            self.Damage = 1
            local phys = self:GetPhysicsObject()
            if phys:IsValid() then
                phys:Wake()
                phys:EnableGravity(true);
            end

        end

        function ENT2:Use( activator, caller )
            --self.Entity:Remove()
            if ( activator:IsPlayer() ) then
                self:DealDamage(activator)
               timer.Create("draino"..activator:SteamID(),1.5,10,function()
                    self:DealDamage(activator)
               end)
                timer.Create("draino2"..activator:SteamID(),15,1,function()
                    if IsValid(activator) then
                        local pos = activator:GetPos()
                        local ent = ents.Create ("draino_normal");
                        --  ent:SetModel (model_file);
                        ent:SetPos( pos)
                        ent:Spawn();
                        local paininfo = DamageInfo()
                        paininfo:SetDamage( 2000 )
                        paininfo:SetDamageType(DMG_POISON)
                        paininfo:SetAttacker(self)
                        paininfo:SetInflictor(self)
                        activator:EmitSound("npc/zombie/zo_attack2.wav")
                        if SERVER then activator:TakeDamageInfo(paininfo) end
                    end
                end)
            end
        end


    end

    function ENT2:DealDamage( activator)
        if IsValid(activator) then
            local paininfo = DamageInfo()
            paininfo:SetDamage( 5 )
            paininfo:SetDamageType(DMG_POISON)
            paininfo:SetAttacker(self)
            paininfo:SetInflictor(self)

            if SERVER then
                activator:EmitSound("npc/zombie/zombie_voice_idle"..math.random(1,10)..".wav")
                activator:TakeDamageInfo(paininfo)
            end
            activator:ViewPunch( Angle( 100,100,100 ) );
        end
    end

    scripted_ents.Register( ENT2, "draino_normal", true )
	
end

function ENT:Fo_UpdatePet( speed, weight )
	self:SetAngles( self:GetAngles() + Angle( math.sin( CurTime() * fo.WobbleSpeed ) * -8, 0, 0 ) )
end

function ENT:Fo_OnDeath()
    if SERVER then
        local pos = self:GetPos()
        local ent = ents.Create ("draino_normal");
        --  ent:SetModel (model_file);
        ent:SetPos( pos)
        ent:Spawn();
    end
end
