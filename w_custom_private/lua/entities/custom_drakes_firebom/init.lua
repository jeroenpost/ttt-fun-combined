if (SERVER) then
	AddCSLuaFile("cl_init.lua")
	AddCSLuaFile("shared.lua")
end

include("shared.lua")

function ENT:Initialize()
    self.Entity:SetModel( "models/dav0r/hoverball.mdl" )
    self.Entity:SetMaterial( "models/props_lab/Tank_Glass001" )

    self.Entity:PhysicsInit(SOLID_VPHYSICS)
    self.Entity:SetMoveType(MOVETYPE_VPHYSICS)
    self.Entity:SetSolid(SOLID_BBOX)
    self.Entity:SetCollisionGroup(COLLISION_GROUP_PROJECTILE)
    self.Entity:SetCollisionBounds( Vector( -16, -16, -16), Vector( 16, 16, 16 ) )
    self.Entity:SetColor(100, 100, 100, 255)
    if SERVER then self.Entity:Ignite(100, 100) end

    timer.Simple(10, function()
		if self:IsValid() then
			self:Remove()
		end
	end )
end

function ENT:Think()
	if self.Entity:WaterLevel() > 0 then
		self.Entity:Extinguish()
	end
end

function ENT:OnRemove()
	if self.Entity:WaterLevel() > 0 then return end
	
	local expl = ents.Create("env_explosion")
		expl:SetPos(self.Entity:GetPos())
		expl:SetPhysicsAttacker(self.Owner)
		expl:SetOwner( self.Owner )
		expl:Spawn()
		
		expl:SetKeyValue("iMagnitude", "20")
		expl:Fire("Explode", 0, 0)
		expl:EmitSound("BaseGrenade.Explode", 300, 300)
		
	for i=1,10 do
		local explfire = ents.Create("env_fire")
			explfire:SetPos(self.Entity:GetPos() + Vector(math.random(-100, 100), math.random(-100, 100), 0))
			explfire:SetKeyValue("health", "30")
			explfire:SetKeyValue("firesize", "50")
			explfire:SetKeyValue("damagescale", "5")
			explfire:SetPhysicsAttacker(self.Owner)
			explfire:SetOwner( self.Owner )
			explfire:Spawn()
			explfire:Fire("StartFire", "", 0)
			
		timer.Simple(9, function()
			if explfire:IsValid() then
				explfire:Remove()
			end
		end )
		
	end	
	
	for k, v in pairs (ents.FindInSphere(self.Entity:GetPos(), 100)) do
		if v:IsPlayer() then return end
			v:Ignite(10,100)
	end
	
end

function ENT:PhysicsCollide(tbl, obj)
	self.Entity:EmitSound("physics/glass/glass_largesheet_break" ..math.random(1,3).. ".wav", 500, math.random(80,120))
	self.Entity:Remove()
end