AddCSLuaFile()

local BounceSound = Sound( "garrysmod/balloon_pop_cute.wav" )


DEFINE_BASECLASS( "base_anim" )


ENT.PrintName		= "Black_hole"
ENT.Author			= "Mecknavorz"
ENT.Information		= "IT'S A DUCKING BLACK HOLE WHAT MORE COULD YOU POSSIBLY WANT TO KNOW!"
ENT.Category		= "Mecknavorz"

ENT.Editable			= false
ENT.Spawnable			= false
ENT.AdminOnly			= false
ENT.RenderGroup 		= RENDERGROUP_TRANSLUCENT

ENT.blackHoleMass	= 100000
        // scale multiplier  THE HIGHER THIS GOES, THE MORE POWERFUL THE BLACK HOLE
ENT.Scale			= 200
        // range of black hole (FLATGRASS IS ABOUT 20,000 ACROSS)
ENT.Range			= 200
        // entities black hole can' suck in
ENT.Disallow		= {info_player_start = false, physgun_beam = false, predicted_viewmodel = false, sent_blackhole = false, sent_spawnblackhole = false, env_sprite = false}
        // the warp effect around the black hole
ENT.Refraction    = Material( "black_hole/refract_ring" )

local EXPLODE_DAMAGE = 90
local EXPLODE_RADIUS = 250

function ENT:SetupDataTables()

    self:NetworkVar( "Float", 0, "BallSize", { KeyName = "ballsize", Edit = { type = "Float", min=4, max=128, order = 1 } }  );
    self:NetworkVar( "Vector", 0, "BallColor", { KeyName = "ballcolor", Edit = { type = "VectorColor", order = 2 } }  );

end

function ENT:SpawnFunction( ply, tr, ClassName )

    if ( !tr.Hit ) then return end

    local size = 16
    local SpawnPos = tr.HitPos + tr.HitNormal * 16

    local ent = ents.Create( ClassName )
    ent:SetPos( SpawnPos )
    ent:SetBallSize( 16 )
    ent:Spawn()
    ent:Activate()

    return ent

end

function ENT:Initialize()

    if ( SERVER ) then

        local size = self:GetBallSize() / 2;

        self:SetModel( "models/Combine_Helicopter/helicopter_bomb01.mdl" )

        self:PhysicsInitSphere( 16, "metal_bouncy" )

        local phys = self:GetPhysicsObject()
        if (phys:IsValid()) then
            phys:Wake()
            phys:EnableGravity(false)
            phys:EnableDrag(false)
        end

        self:SetCollisionBounds( Vector( -16, -16, -16 ), Vector( 16, 16, 16 ) )

        self.Color = Color( 0, 0, 0, 255 )

        self:NetworkVarNotify( "BallSize", self.OnBallSizeChanged );

    else

        self.LightColor = Vector( 0, 0, 0 )

    end

    self.timer = CurTime() + 30
    timer.Create("blackhole"..self:EntIndex(),1,300,function()
        if not IsValid(self) then return end

        if self.timer < CurTime() then self:Explode() end
    end)

end

function ENT:OnBallSizeChanged( varname, oldvalue, newvalue )

    local delta = oldvalue - newvalue

    local size = self:GetBallSize() / 2;
    self:PhysicsInitSphere( 16, "metal_bouncy" )
    self:SetCollisionBounds( Vector( -16, -16, -16 ), Vector( 16, 16, 16 ) )

    self:PhysWake()

end

if ( CLIENT ) then

    local matBall = Material( "sprites/sent_ball" )

    function ENT:Draw()

        local pos = self:GetPos()
        local vel = self:GetVelocity()

        render.SetMaterial( matBall )

        render.DrawSprite( pos, 32, 32, Color( 0, 0, 0, 255 ) )

    end

end

function ENT:star( pos, norm )

    if not self:IsValid() then return end

    if self.Spawning then return end
    self.Spawning = true
    pos = self:GetPos()

    norm = norm or Vector( 0, 0, 1 )

    local fx = EffectData()
    fx:SetOrigin( pos )
    fx:SetNormal( norm )
    util.Effect( "Sparks", fx )

    self:EmitSound( "NPC_CombineBall.Launch" )


end

function ENT:colapse( pos, norm )

    norm = norm or Vector( 0, 0, 1 )

    local fx = EffectData()
    fx:SetOrigin( pos )
    fx:SetNormal( norm )
    util.Effect( "TeslaZap", fx )

    self:EmitSound( "NPC_CombineBall.Launch" )

end

function ENT:PhysicsCollide( data )

    self:star( pos, norm )

end

function ENT:OnTakeDamage( dmginfo )

    self:TakePhysicsDamage( dmginfo )

end


function ENT:Think()

    local phys = self.Entity:GetPhysicsObject()
    if phys:IsValid() then
        phys:ApplyForceCenter( Vector(0,0,0) )
    end

    local myPosition = self.Entity:GetPos()

    local halfVector = ( self.Range / 2 ) * Vector(1, 1, 1)
    local lowRange = myPosition - halfVector
    local highRange = myPosition + halfVector


    local inRange = ents.FindInBox( lowRange, highRange )

            // LOOP OVER ALL ENTITIES FOUND IN THE RANGE BOX
    for entKey,entVal in pairs(inRange) do

        if(entVal:IsPlayer()) then

            local entLocation = entVal:GetPos()

            local difference = myPosition - entLocation
            local objRange = difference:Length()

            if(objRange < self.Range) then
                --I MAKE E DE PLRS GET SUCKEDED IN
                --entVal:Freeze( false )
               -- entVal:SetMoveType(6)
                difference:Normalize()

                local startVec = (myPosition - Vector(0,0,2));
                local endVec = entLocation;
                local normVec = startVec - endVec;
                local dist = 1;

                normVec:Normalize();

                local point = (LerpVector(.25, endVec, startVec));

                entVal:SetPos( point )

                if(objRange < 80) then
                    if (CLIENT) then return end
                    local ent = entVal
                    if ent:IsPlayer() and not ent.nextsandtouch or (ent.nextsandtouch and ent.nextsandtouch  < CurTime()) then
                        ent.nextsandtouch = CurTime() + 3
                        ent:Ignite(0.5,0)
                        local dmginfo = DamageInfo()
                        dmginfo:SetDamage( self.damage or 45 ) --50 damage
                        dmginfo:SetDamageType( DMG_BULLET ) --Bullet damage
                        dmginfo:SetAttacker( self.Owner or ent ) --First player found gets credit
                        dmginfo:SetDamageForce( Vector( 0, 0, 50 ) ) --Launch upwards
                        dmginfo:SetInflictor(self)
                        ent:TakeDamageInfo( dmginfo )
                    end


                end

            else

            end
        end
    end
end


function ENT:Explode(tr)
    if self.exploded then return end self.exploded = true
    if SERVER then
        self.Entity:SetNoDraw(true)
        self.Entity:SetSolid(SOLID_NONE)

        local pos = self.Entity:GetPos()



        local effect = EffectData()
        effect:SetStart(pos)
        effect:SetOrigin(pos)
        effect:SetScale(10)
        effect:SetRadius(250)
        effect:SetMagnitude(120)


        util.Effect("Explosion", effect, true, true)

        util.BlastDamage(self, self.Owner, pos, 250, 120)
        self:Remove()
    end




end
