if SERVER then
   AddCSLuaFile()
end

ENT.Type = "anim"
ENT.Base = "ttt_basegrenade_proj"
ENT.Model = Model("models/Gibs/HGIBS.mdl")


AccessorFunc( ENT, "radius", "Radius", FORCE_NUMBER )
AccessorFunc( ENT, "dmg", "Dmg", FORCE_NUMBER )

function ENT:Initialize()
   if not self:GetRadius() then self:SetRadius(256) end
   if not self:GetDmg() then self:SetDmg(0) end
   
   self.BaseClass.Initialize(self)

   self:SetHealth(1)
   self:SetModel("models/Gibs/HGIBS.mdl")
	local phys = self:GetPhysicsObject()
	if phys:IsValid() then phys:SetMass(350) end

    if CLIENT then return end
   self:Ignite(5,10)
end

ENT.touches = 0
function ENT:PhysicsCollide(data, obj)
   if self.shouldmakefire then
       for i=1,5 do
           local explfire = ents.Create("env_fire")
           explfire:SetPos(self.Entity:GetPos() + Vector(math.random(-100, 100), math.random(-100, 100), 0))
           explfire:SetKeyValue("health", "20")
           explfire:SetKeyValue("firesize", "50")
           explfire:SetKeyValue("damagescale", "1")
           explfire:SetPhysicsAttacker(self.Owner)
           explfire:SetOwner( self.Owner )
           explfire:Spawn()
           explfire:Fire("StartFire", "", 0)

           timer.Simple(5, function()
               if explfire:IsValid() then
                   explfire:Remove()
               end
           end )

       end
        self:Remove()
   end
end

function ENT:Touch( ent )
    if not self.exploded and ent:IsPlayer() and not ent.nextsandtouch or (ent.nextsandtouch and ent.nextsandtouch  < CurTime()) then
        ent.nextsandtouch = CurTime() + 3
        ent:Ignite(0.5,0)
        self.exploded = true
        local dmginfo = DamageInfo()
        dmginfo:SetDamage( self.damage or 50 ) --50 damage
        dmginfo:SetDamageType( DMG_BULLET ) --Bullet damage
        dmginfo:SetAttacker( self.Owner or ent ) --First player found gets credit
        dmginfo:SetDamageForce( Vector( 0, 0, 50 ) ) --Launch upwards
        dmginfo:SetInflictor(self)
        ent:TakeDamageInfo( dmginfo )
    end
end

function ENT:Explode(tr)
    if self.exploded then return end self.exploded = true
   if SERVER then
      self.Entity:SetNoDraw(true)
      self.Entity:SetSolid(SOLID_NONE)

	  local pos = self.Entity:GetPos()
	  
      -- pull out of the surface
      if tr.Fraction != 1.0 then
         self.Entity:SetPos(tr.HitPos + tr.HitNormal * 0.6)
      end

      --[[if util.PointContents(pos) == CONTENTS_WATER then
         self:Remove()
         return
      end]]--

      local effect = EffectData()
      effect:SetStart(pos)
      effect:SetOrigin(pos)
      effect:SetScale(self:GetRadius() * 0.5)
      effect:SetRadius(self:GetRadius())
      effect:SetMagnitude(self.dmg)

      if tr.Fraction != 1.0 then
         effect:SetNormal(tr.HitNormal)
      end

      util.Effect("Explosion", effect, true, true)

      util.BlastDamage(self, self.Owner, pos, self:GetRadius(), self:GetDmg())

      self:SetDetonateExact(0)	

	     for i=1,2 do
			local spos = pos+Vector(math.random(-75,75),math.random(-75,75),math.random(0,50))
			local contents = util.PointContents( spos )
			local _i = 0
			while i < 10 and (contents == CONTENTS_SOLID or contents == CONTENTS_PLAYERCLIP) do 
				_i = 1 + i
				spos = pos+Vector(math.random(-125,125),math.random(-125,125),math.random(-50,50)) 
				contents = util.PointContents( spos )
			end
			
			local headturtle = SpawnNPC2(self:GetThrower(),spos, "npc_headcrab")
		
			headturtle:SetNPCState(0)
            headturtle.isskullsandwich = true

			local turtle = ents.Create("prop_dynamic")
			turtle:SetModel("models/Gibs/HGIBS.mdl")
			turtle:SetPos(spos)
			turtle:SetAngles(Angle(0,-45,0))
			turtle:SetParent(headturtle)
            turtle:Ignite(100,10)



			--headturtle:SetCollisionGroup(COLLISION_GROUP_WEAPON)
			
			headturtle:SetNWEntity("Thrower", self.Owner)
			--headturtle:SetName(self:GetThrower():GetName())
			headturtle:SetNoDraw(true)
			headturtle:SetHealth(1050)
             timer.Simple(5,function()
                if IsValid(headturtle) then
                    headturtle:SetHealth(1)
                end
             end)

		end
	  

   else
   
      local spos = self.Entity:GetPos()
      local trs = util.TraceLine({start=spos + Vector(0,0,64), endpos=spos + Vector(0,0,-128), filter=self})
      util.Decal("Scorch", trs.HitPos + trs.HitNormal, trs.HitPos - trs.HitNormal)      

      self:SetDetonateExact(0)
   end

end

--From: gamemodes\sandbox\gamemode\commands.lua
--TODO: Adjust for TTT.

function SpawnNPC2( Player, Position, Class )

	local NPCList = list.Get( "NPC" )
	local NPCData = NPCList[ Class ]
	
	-- Don't let them spawn this entity if it isn't in our NPC Spawn list.
	-- We don't want them spawning any entity they like!
	if ( !NPCData ) then 
		if ( IsValid( Player ) ) then
			Player:SendLua( "Derma_Message( \"Sorry! You can't spawn that NPC!\" )" );
		end
	return end
	
	local bDropToFloor = false
		
	--
	-- This NPC has to be spawned on a ceiling ( Barnacle )
	--
	if ( NPCData.OnCeiling && Vector( 0, 0, -1 ):Dot( Normal ) < 0.95 ) then
		return nil
	end
	
	if ( NPCData.NoDrop ) then bDropToFloor = false end
	
	--
	-- Offset the position
	--
	
	
	-- Create NPC
	local NPC = ents.Create( NPCData.Class )
	if ( !IsValid( NPC ) ) then return end

	NPC:SetPos( Position )
	--
	-- This NPC has a special model we want to define
	--
	if ( NPCData.Model ) then
		NPC:SetModel( NPCData.Model )
	end
	
	--
	-- Spawn Flags
	--
	local SpawnFlags = bit.bor( SF_NPC_FADE_CORPSE, SF_NPC_ALWAYSTHINK)
	if ( NPCData.SpawnFlags ) then SpawnFlags = bit.bor( SpawnFlags, NPCData.SpawnFlags ) end
	if ( NPCData.TotalSpawnFlags ) then SpawnFlags = NPCData.TotalSpawnFlags end
	NPC:SetKeyValue( "spawnflags", SpawnFlags )
	NPC.isskullsandwich = true
	--
	-- Optional Key Values
	--
	if ( NPCData.KeyValues ) then
		for k, v in pairs( NPCData.KeyValues ) do
			NPC:SetKeyValue( k, v )
		end		
	end
	
	--
	-- This NPC has a special skin we want to define
	--
	if ( NPCData.Skin ) then
		NPC:SetSkin( NPCData.Skin )
	end
	
	--
	-- What weapon should this mother be carrying
	--
	
	NPC:Spawn()
	NPC:Activate()
	
	if ( bDropToFloor && !NPCData.OnCeiling ) then
		NPC:DropToFloor()	
	end
	
	return NPC
end

if SERVER and GetConVarString("gamemode") == "terrortown" then
local function TurtleNadeDamage(victim, dmg)
	local attacker = dmg:GetAttacker()
	
	
	if attacker:IsValid() and attacker:IsNPC() and attacker:GetClass() == "npc_headcrab" and attacker.isskullsandwich  then
            dmg:SetDamage(1)
        end
	if victim:GetClass() == "npc_headcrab" and victim.isskullsandwich and attacker:IsValid()   then
		dmg:SetDamageType(DMG_REMOVENORAGDOLL)

		if (victim:Health() - dmg:GetDamage()) < 980 then

			local turtle = ents.Create("eatable_skull_sand")
			turtle:SetModel("models/Gibs/HGIBS.mdl")
			turtle:SetPos(victim:GetPos()+Vector(0,0,5))
			turtle:SetAngles(victim:GetAngles() + Angle(0,-90,0))
			turtle:SetColor(Color(128,128,128,255))
			turtle:SetCollisionGroup(COLLISION_GROUP_WEAPON)
			turtle:Spawn()

			local phys = turtle:GetPhysicsObject()
			if !(phys && IsValid(phys)) then turtle:Remove() end
			
			victim:Remove()
		end
	end
end

hook.Add("EntityTakeDamage","TurtlenadeDmgHandle2",TurtleNadeDamage)
end
