AddCSLuaFile( "cl_init.lua" )
AddCSLuaFile( "shared.lua" )
include( 'shared.lua' )
function ENT:Initialize()    
    self.Entity:SetModel( "models/dav0r/hoverball.mdl" )
    self.Entity:SetModelScale( self.Entity:GetModelScale() * 0.3, 0.01 )
    self.Entity:PhysicsInitSphere( 8, "metal_bouncy" )

        
    local phys = self.Entity:GetPhysicsObject()
    if (phys:IsValid()) then
        phys:Wake()
    end

    self.Entity:SetCollisionBounds( Vector( -16, -16, -16 ), Vector(16, 16, 16 ) )
    self.Entity:SetPos(self:GetPos()+Vector(0,0,16))
end

function ENT:OnTakeDamage( dmginfo )

    // React physically when shot/getting blown
    self.Entity:TakePhysicsDamage( dmginfo )
    
end

function ENT:Touch( ent )

	if ent:IsPlayer()  then
        if self.exploded then return end self.exploded = true
        ent:EmitSound("physics/rubber/rubber_tire_impact_hard1.wav")
        local dmginfo = DamageInfo()
        dmginfo:SetDamage( self.damage or 25 ) --50 damage
        dmginfo:SetDamageType( DMG_BULLET ) --Bullet damage
        dmginfo:SetAttacker( self.Owner or ent ) --First player found gets credit
        dmginfo:SetDamageForce( Vector( 0, 0, 500 ) ) --Launch upwards
        dmginfo:SetInflictor(self)
        ent:TakeDamageInfo( dmginfo )

        local grav = ent:GetGravity()
        ent:SetGravity(0.1)
        ent:SetPos(ent:GetPos() + Vector(0,0,50))
        ent:SetVelocity(ent:GetUp() * 1 + Vector(0, 0, 200))
        local ent = ent
        timer.Create(ent:EntIndex().."normalGrav",3,1,function()
            if IsValid(ent) then
                ent.hasProtectionSuit = true
                 ent:SetGravity(grav or 1)
            end
        end)
        if self.shouldexplode  then
            self.exploded = true
            self:Explode()
        end
    end

end
ENT.touches = 0
function ENT:PhysicsCollide(data, obj)
    self.touches = self.touches + 1
    if self.touches > 3 then
        self:Remove()
    end
end

function ENT:Explode()
    if self.exploded then return end self.exploded = true
    local ent = ents.Create( "env_explosion" )
    ent:SetPos( self.Entity:GetPos()  )
    ent:SetOwner( self:GetOwner()  )
    ent:SetPhysicsAttacker(  self:GetOwner() )
    ent:Spawn()
    ent:SetKeyValue( "iMagnitude", "25" )
    ent:Fire( "Explode", 0, 0 )

    util.BlastDamage( self, self.Owner or self, self:GetPos(), 25, 50 )

    ent:EmitSound( "weapons/big_explosion.mp3" )

    self:Remove()
end
