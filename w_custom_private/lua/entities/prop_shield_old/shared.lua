ENT.Type = "anim"
ENT.Base = "base_anim"

ENT.PrintName = "Shield Ent"
ENT.Author = ""
ENT.Contact = ""
ENT.Purpose = ""
ENT.Instructions = "" 

ENT.Spawnable			= false
ENT.AdminSpawnable		= false

ENT.ShieldTime = 20

function ENT:SetupModel()

	--self.Entity:SetModel( "models/hunter/tubes/tube4x4x1to2x2.mdl" )
	self.Entity:SetModel( "models/props_phx/construct/glass/glass_dome360.mdl" )
	--print("11")
end

function ENT:OnRemove()

--models/hunter/tubes/circle2x2.mdl

end


if CLIENT then

	function ENT:Think() -- client think, for lightss
		
		-- 	local dlight = DynamicLight( self:EntIndex()  )
		-- 	if ( dlight ) then
		-- 		dlight.Pos = self:GetPos() + Vector(0,0,20)
		-- 		dlight.r = 60
		-- 		dlight.g = 140
		-- 		dlight.b = 40
		-- 		dlight.Brightness = 2000 + 2
		-- 		dlight.Decay = 900 * 5
		-- 		dlight.Size = 90000
		-- 		dlight.DieTime = CurTime() + 1
		-- 	end
		-- 	--print("11")
		-- --self.Entity:NextThink( CurTime() + 0.1 ) 
		-- return true
	end

	function ENT:Initialize()
		self:SetRenderOrigin( self:GetPos() + (self:GetAngles():Up()*-8) ) --muck with the render pos a tiny bit so it looks pretty
		self.SpawnTime = CurTime()
		--self:AddEffects( EF_BRIGHTLIGHT )
		self:SetMaterial("shield_new2") --models/props_combine/tprings_globe
		local effectdata=EffectData()
		effectdata:SetEntity(self)
		util.Effect("propspawn",effectdata)
		timer.Simple(self.ShieldTime-0.25,function()
			local effectdata=EffectData()
			effectdata:SetEntity(self)
			util.Effect("shield_despawn",effectdata)
		end)
	end
	local sprmat = Material("sprites/light_glow02_add")
	function ENT:Draw()
		local timeleftperc =  1-( (CurTime()-self.SpawnTime)/(self.ShieldTime+1) )
		local perc = math.Clamp(timeleftperc*1.25,0,1)
		--print(perc)
		self:DrawModel()
		render.SetMaterial(sprmat)
		render.DrawSprite( self:GetPos() + Vector(0,0,45), 128*perc, 128*perc, Color(250,200,50,255) )
		render.DrawSprite( self:GetPos() + Vector(0,0,25), 512*perc, 512*perc, Color(250,200,50,255) )
		render.DrawSprite( self:GetPos() + Vector(0,0,5), 128*perc, 128*perc, Color(250,200,50,255) )

		local dlight = DynamicLight( self:EntIndex() )
		if ( dlight ) then
			--local r, g, b, a = self:GetColor()
			dlight.Pos = self:GetPos() - Vector(0,0,5) --self:GetPos()
			dlight.r = 250
			dlight.g = 190
			dlight.b = 50
			dlight.Brightness = 1.5
			dlight.Size = math.Clamp(192*perc,70,999)
			dlight.MinLight = 0.02
			dlight.Decay = 200
			dlight.DieTime = CurTime() + 0.25
	        dlight.Style = 12
		end

	end

end