AddCSLuaFile( "shared.lua" )
include( 'shared.lua' )

AccessorFunc( ENT, "radius", "Radius", FORCE_NUMBER )
AccessorFunc( ENT, "dmg", "Dmg", FORCE_NUMBER )

function ENT:Initialize()
    if not self:GetRadius() then self:SetRadius(256) end
    if not self:GetDmg() then self:SetDmg(0) end
 	self.SpawnTime = CurTime()
    -- Set up the entity
    --self.Entity:SetModel( "models/hunter/tubes/tube4x4x1to2x2.mdl" )
    self.Entity:SetModel( "models/props_phx/construct/glass/glass_dome360.mdl" )
 
	self.Entity:PhysicsInit( SOLID_VPHYSICS )
    self.Entity:SetMoveType( MOVETYPE_VPHYSICS )
    self.Entity:SetSolid( SOLID_VPHYSICS )

    self.Entity:SetColor( Color( 255, 255, 255, 255 ) )
    
    self.Index = self.Entity:EntIndex()
    self.Entity:SetCollisionGroup( COLLISION_GROUP_DEBRIS )
    local phys = self.Entity:GetPhysicsObject()
    if phys:IsValid() then
        phys:Wake()
        phys:EnableMotion(false)
    end
	
    --self:SetTrigger(true)
    --self:AddEffects( EF_BRIGHTLIGHT )
    self:AddEffects(EF_NOSHADOW)
    self:SetHealth(2000)


    --make some zaps mon
    local zap = ents.Create("point_tesla")
        zap:SetPos(self:GetPos() - Vector(0,0,5))
        --zap:SetKeyValue("targetname", "teslab")
        zap:SetKeyValue("m_SoundName" ,"DoSpark")
        zap:SetKeyValue("texture" ,"sprites/plasmabeam.spr")
        zap:SetKeyValue("m_Color" ,"255 80 0")
        zap:SetKeyValue("m_flRadius" ,tostring(80))
        zap:SetKeyValue("beamcount_min" ,tostring(12))
        zap:SetKeyValue("beamcount_max", tostring(16))
        zap:SetKeyValue("thick_min", tostring(3))
        zap:SetKeyValue("thick_max", tostring(5))
        zap:SetKeyValue("lifetime_min" ,"0.02")
        zap:SetKeyValue("lifetime_max", "0.05")
        zap:SetKeyValue("interval_min", "0.05")
        zap:SetKeyValue("interval_max" ,"0.08")
        zap:Spawn()
        zap:Fire("DoSpark","",0.1)
        zap:Fire("DoSpark","",self.ShieldTime*0.01)
        zap:Fire("DoSpark","",self.ShieldTime*0.02)
        zap:Fire("DoSpark","",self.ShieldTime*0.04)
        zap:Fire("DoSpark","",self.ShieldTime*0.07)
        zap:Fire("DoSpark","",self.ShieldTime*0.1)
        zap:Fire("DoSpark","",self.ShieldTime*0.15)
        zap:Fire("DoSpark","",self.ShieldTime*0.24)
        zap:Fire("DoSpark","",self.ShieldTime*0.32)
        zap:Fire("DoSpark","",self.ShieldTime*0.39)
        zap:Fire("DoSpark","",self.ShieldTime*0.5)
        zap:Fire("DoSpark","",self.ShieldTime*0.68)
        zap:Fire("DoSpark","",self.ShieldTime*0.8)
        zap:Fire("DoSpark","",self.ShieldTime*0.95)
        --zap:Fire("DoSpark","",self.ShieldTime*1)

        zap:Fire("kill","", self.ShieldTime+1)

end
 

ENT.Nextthin = 0
 function ENT:Think()
 	if CurTime() > (self.SpawnTime + self.ShieldTime) then
 		self:Remove()
 	end
    if self.Nextthin < CurTime() then
        for k, v in pairs(ents.FindInSphere(self:GetPos(), 50)) do -- make sure to say "take this medkit" or something like that if there are others around, not talk to yourself
            if v:IsPlayer()  and v:Health() < 150 then
                v:EmitSound("items/smallmedkit1.wav", 85, math.random(99, 101))
                v:SetHealth(v:Health()+2)
                gb_check_max_health(v)
            end
        end
        self.Nextthin = CurTime() + 0.5
    end

 end



function ENT:OnTakeDamage(dmginfo)

        if not self.dead then
            self.dead = true
            local effectdata=EffectData()
            effectdata:SetEntity(self)
            util.Effect("shield_despawn",effectdata)
            self:Fire("kill",0.1,0.5)
        end

end

function ENT:Explode(tr)
    if self.exploded then return end self.exploded = true
    if SERVER then
        self.Entity:SetNoDraw(true)
        self.Entity:SetSolid(SOLID_NONE)

        local pos = self.Entity:GetPos()

        local effect = EffectData()
        effect:SetStart(pos)
        effect:SetOrigin(pos)
        effect:SetScale(125)
        effect:SetRadius(250)
        effect:SetMagnitude(self.dmg)


        util.Effect("Explosion", effect, true, true)

        util.BlastDamage(self, self.Owner, pos, 250, 125)

        self:Remove()
     end
end
