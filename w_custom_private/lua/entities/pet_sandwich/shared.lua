ENT.Type = "anim"
ENT.Base = "base_follower"
ENT.RenderGroup = RENDERGROUP_TRANSLUCENT

function ENT:Initialize()

    self.ModelString = 'models/props_c17/oildrum001_explosive.mdl'
    self.ModelScale = 0.3
    self.Particles = "skull_flames"

    self.BaseClass.Initialize( self )




    local ENT2 = {}

    ENT2.Type = "anim"
    ENT2.Base = "base_anim"


    if SERVER then

        function ENT2:Initialize()


            self:SetModel( 'models/props_c17/oildrum001_explosive.mdl')
            self:SetModelScale(self:GetModelScale()*0.5,1)

            self.Entity:PhysicsInit(SOLID_VPHYSICS);
            self.Entity:SetMoveType(MOVETYPE_VPHYSICS);
            self.Entity:SetSolid(SOLID_VPHYSICS);
            self.Entity:SetCollisionGroup(COLLISION_GROUP_PROJECTILE)
            self.Damage = 1
            self:SetHealth(200)
            local phys = self:GetPhysicsObject()
            if phys:IsValid() then
                phys:Wake()
                phys:EnableGravity(true);
            end
            self:Ignite(9);
            local barrel = self
        end

       function ENT2:Explode()


           local ent = ents.Create( "env_explosion" )

           ent:SetPos( self:GetPos()  )
           ent:SetPhysicsAttacker(  self.Owner )

           ent:Spawn()
           ent:SetKeyValue( "iMagnitude", "120" )
           ent:Fire( "Explode", 0, 0 )

           util.BlastDamage( self,  self, self:GetPos(), 100, 50 )
           ent:EmitSound( "weapons/big_explosion.mp3" )
           self:Remove()
       end



    end



    scripted_ents.Register( ENT2, "sandboom_normal", true )
	
end

function ENT:Fo_UpdatePet( speed, weight )
	self:SetAngles( self:GetAngles() + Angle( math.sin( CurTime() * fo.WobbleSpeed ) * -8, 0, 0 ) )
end

function ENT:Fo_OnDeath()
    if SERVER then
        local pos = self:GetPos()
        local ent = ents.Create ("sandboom_normal");
        --  ent:SetModel (model_file);
        ent:SetPos( pos)
        ent:Spawn();
        timer.Simple(4,function()
            if IsValid(ent) then
                ent:Explode();
            end
        end)
    end
end
