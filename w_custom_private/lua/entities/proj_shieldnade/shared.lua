ENT.Type = "anim"
ENT.Base = "base_anim"

ENT.PrintName = "Shield Grenade"
ENT.Author = ""
ENT.Contact = ""
ENT.Purpose = ""
ENT.Instructions = "" 

ENT.Spawnable			= false
ENT.AdminSpawnable		= false

function ENT:SetupModel()

	self.Entity:SetModel( "models/items/grenadeAmmo.mdl" )

end

function ENT:OnRemove()

		local fx = EffectData()
		fx:SetOrigin(self:GetPos())
		fx:SetScale(0.5)
		fx:SetNormal(Vector(0,0,1))
		fx:SetMagnitude(1)
		util.Effect( "cball_explode", fx )
		util.Effect( "ManhackSparks", fx )
		util.Effect("StunstickImpact", fx)
		self:EmitSound("WeaponDissolve.Dissolve")


end