AddCSLuaFile( "shared.lua" )
include( 'shared.lua' )
 
function ENT:Initialize()
 
    -- Set up the entity
    self.Entity:SetModel( "models/items/grenadeAmmo.mdl" )
 
	self.Entity:PhysicsInit( SOLID_BSP )
    self.Entity:SetMoveType( MOVETYPE_VPHYSICS )
    self.Entity:SetSolid( SOLID_BSP )
	self.Entity:SetCollisionGroup( COLLISION_GROUP_WEAPON )
    self.Entity:SetColor( Color( 255, 255, 255, 255 ) )
    
    self.Index = self.Entity:EntIndex()
        
    local phys = self.Entity:GetPhysicsObject()
    if phys:IsValid() then
    	phys:SetMass(1)
    	phys:SetDamping(0.1225,10)
        phys:Wake()
    end
end
 
function ENT:PhysicsCollide( data, physobj )
	-- Msg(data.HitNormal)
	-- Msg( "--" )
	--MsgN(self:IsOnGround() ) 
	local tr = util.TraceLine( {
		start = self:GetPos(), 
		endpos = self:GetPos() - Vector(0,0,5), 
		filter = self
	} )


	-- Msg(tr.HitWorld)
	-- Msg(" (")
	-- Msg(self:IsOnGround())
	-- Msg(" hit: ")
	-- Msg(tr.Entity)
	-- MsgN(")")
	if data.HitNormal.z < -0.9 and (tr.HitWorld or (tr.Entity:IsValid() and (tr.Entity:GetClass() == "func_brush")) )then -- we hit something valid at a valid angle, record relevant data and remove 
		physobj:EnableMotion(false)
		--print("froze")
		self.HitNormal = data.HitNormal
		self.HitPos = tr.HitPos
		local zap = ents.Create("point_tesla")
					--zap:SetKeyValue("targetname", "teslab")
					zap:SetKeyValue("m_SoundName" ,"DoSpark")
					zap:SetKeyValue("texture" ,"sprites/plasmabeam.spr")
					zap:SetKeyValue("m_Color" ,"255 80 0")
					zap:SetKeyValue("m_flRadius" ,tostring(100))
					zap:SetKeyValue("beamcount_min" ,tostring(12))
					zap:SetKeyValue("beamcount_max", tostring(16))
					zap:SetKeyValue("thick_min", tostring(3))
					zap:SetKeyValue("thick_max", tostring(24))
					zap:SetKeyValue("lifetime_min" ,"0.01")
					zap:SetKeyValue("lifetime_max", "0.075")
					zap:SetKeyValue("interval_min", "0.05")
					zap:SetKeyValue("interval_max" ,"0.08")
					zap:SetPos(tr.HitPos + (tr.HitNormal*25))
					zap:Spawn()
					zap:Fire("DoSpark","",0)
					zap:Fire("DoSpark","",0.01)
					zap:Fire("DoSpark","",0.03)
					zap:Fire("DoSpark","",0.05)
					zap:Fire("DoSpark","",0.08)
					--
					zap:Fire("DoSpark","",0.1)
					zap:Fire("DoSpark","",0.13)
					--zap:Fire("DoSpark","",0.15)
					zap:Fire("DoSpark","",0.18)
					--zap:Fire("DoSpark","",0.21)
					zap:Fire("DoSpark","",0.23)
					--zap:Fire("DoSpark","",0.25)
					--zap:Fire("DoSpark","",0.28)
					zap:Fire("DoSpark","",0.31)

					zap:Fire("kill","", 1)
		local ent = ents.Create ( "prop_shield_old" );
		-- ent.HitNormal = self.HitNormal
		-- ent.HitPos = self.HitPos

		ent:SetPos ( self.HitPos+ self.HitNormal*-11 ); --model is tiny; shove it upward a bit 
		--silly model, 48
		ent:SetAngles( self.HitNormal:Angle():Up():Angle() )
		ent:ManipulateBoneScale( 0, Vector(1,1.1,1) ) --make it look a little less tiny

		--local ent = ents.Create("prop_physics")
		 --ent:SetAngles(Angle(0,math.random(1,360),0))
 		 -- ent:SetPos(self.HitPos + Vector(math.random(-10,10),math.random(-10,10),math.random(15,40)))
		 ent:Spawn()
        ent.Owner = self.Owner
        self.Owner.HealShield = ent

        -- ent:Spawn();
		-- ent:SetPos ( self.HitPos+ Vector(0,0,48) ); --silly model
		-- ent:SetAngles( self.HitNormal:Angle():Up():Angle() )
		-- ent:SetOwner( self.Owner )

		self:Remove()
	else
		self:EmitSound("ItemSoda.Bounce")
	end
end
