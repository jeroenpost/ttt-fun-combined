AddCSLuaFile( 'cl_init.lua' )
AddCSLuaFile( 'shared.lua' )
include( 'shared.lua' )


function ENT:SpawnFunction( ply, tr )

	if ( !tr.Hit ) then return end
	
	local SpawnPos = tr.HitPos + tr.HitNormal * 24
	local ent = ents.Create( 'sent_firework' )
	
	ent:SetPos( SpawnPos )
	ent:SetAngles( Angle(0, 0, 0) )

	ent:Spawn()
	ent:Activate()

	return ent
	
end

function ENT:Initialize()

	self:SetModel( 'models/turtle/firework.mdl' )
    self.IsFirework = true
	self:PhysicsInit( SOLID_VPHYSICS )
	self:SetMoveType( MOVETYPE_VPHYSICS )
	self:SetSolid( SOLID_VPHYSICS )
    self.big = false
	self:SetUseType( SIMPLE_USE )
	
	self.Launched = false
	self.FuseStarted = false

	local phys = self:GetPhysicsObject()
	
	if phys:IsValid() then
		
		phys:Wake()

	end
    self.FWScale = 1

	    self:StartMotionController()
    if math.Rand(1,20) > 19 then
        self:SetModelScale( self:GetModelScale() * 50, 1 );
        self.FWScale =10
        self:ManipulateBoneScale(0, Vector(3, 3, 3))

    end
	
end

/*---------------------------------------------------------
   Name: PhysicsCollide
---------------------------------------------------------*/
function ENT:PhysicsCollide( Data, Phys )

	if Data.Speed > 500 && self.Launched && self.CollisionExplode then
	
		self:Explode()
		
	end

end


function ENT:SetupDataTables()
 
    self:DTVar( 'Int', 0, 'FuseLevel' );
	self:DTVar( 'Vector', 0, 'FlameColor' ) -- Using a Vector because there's no way of sending Color via datatable. (Please inform me if there is)
	
end

function ENT:SetupData( Col, FuseTime, Scale, AutoUnfreeze, ExplodeTime, FlameColor, DamageExplode, CollisionExplode, TrajectoryRand, BlastDamage, Key )


	self.Col = Col || Color( 255, 255, 255 )
	self.FuseTime = FuseTime || 2
       --     if self.big then
        --        self.FWScale = 10
       --     else
       --         self.FWScale = 1
       --     end
	self.AutoUnfreeze = AutoUnfreeze != false
	self.ExplodeTime = ExplodeTime || 0
	self.FlameColor = FlameColor || Vector( 255, 165, 0 )
	self.DamageExplode = false
	self.CollisionExplode = false
	self.TrajectoryRand = TrajectoryRand || 10
	self.BlastDamage = false
	self.Key = Key
	
	// Make FlameColor brighter, because it looks really bad when its one pure color. (255, 0, 0) for example
	
	local Hue, Saturation, Value = ColorToHSV( Color( FlameColor.x, FlameColor.y, FlameColor.z ) )
	local Col = HSVToColor( Hue, Saturation/3, Value )
	local NewFlame = Vector( Col.r, Col.g, Col.b ) || Vector( 255, 165, 0 )
	
	self:SetDTVector( 0, NewFlame )
	self:SetDTVector( 1, Vector( self.Col.r/255, self.Col.g/255, self.Col.b/255 ) )
	
end

function ENT:OnTakeDamage( dmginfo )

	self:TakePhysicsDamage( dmginfo )

	 self:Launch();
	
end

function ENT:PhysicsSimulate( phys, deltatime )

	if !self.Launched then return SIM_NOTHING end

	local ForceAngle = Vector( 0, 0, 0 )
	local ForceLinear = Vector( 0, 0, 4750 )

	local Traj = self.TrajectoryRand
	
	if Traj != 0 then
	
		ForceAngle = VectorRand()*25*Traj
		local RandForceLinear = VectorRand()*225*Traj
		ForceLinear = Vector( RandForceLinear.x, RandForceLinear.y, 4750 )
	
	end
	
	if self.ExplodeTime != 0 then
	
		local Num = self.ExplodeTime
		local Frac = ( -( CurTime() - (self.LaunchTime) ) + Num ) / Num
		ForceLinear.z = ForceLinear.z*Frac

	end
	
	return ForceAngle, ForceLinear, SIM_LOCAL_ACCELERATION
	
end


function ENT:Explode()

	if self.Exploded then return end
	self.Exploded = true
	if WireAddon then Wire_TriggerOutput(self, 'Exploded', 1); end
	
	--for i=0, 5 do


    if IsValid(self.pl) and self.pl:Alive() and self.pl:GetActiveWeapon() and self.pl:GetActiveWeapon():GetClass() == "custom_melonpistol" then
        local melons = 0
        for i=0, 5 do
            local ent = ents.Create("prop_physics")
            ent:SetModel("models/props_junk/watermelon01.mdl")
            ent:SetPos(self:GetPos())
            ent:SetAngles(Angle(20, 90, 0))
            ent:Spawn()

        end
    end
	
	local ED = EffectData()
	
	ED:SetOrigin( self:GetPos() )
	ED:SetStart( Vector( self.Col.r, self.Col.g, self.Col.b ) ) // Vector is color.
	ED:SetScale( self.FWScale )
	
	local effect = util.Effect( 'firework_explosion', ED, true, true )


	--end
	
	local DoBlastDamage = self.BlastDamage -- I have to save this as a variable because I'm removing the entity before blast damage (more information in comment below)
	self:Remove()
	
	-- Note! Make sure this is called AFTER it's removed!
	-- Otherwise, itll cause an infinite loop of creating the effect & exploding.
	
	-- (It was pretty cool, though)
	
	if DoBlastDamage then util.BlastDamage( self, self.pl || self, self:GetPos(), 512*self.FWScale, 350 ); end

end

local function SafeExplode( Ent )

	if !IsValid( Ent ) then return end
	
	Ent:Explode()

end

function ENT:Launch()

	if self.Launched then return end

    local soundfile = gb_config.websounds..'fireworks/firework_launch_'..math.random( 1, 2 )..'.mp3'
    gb_PlaySoundFromServer(soundfile, self)
	
	self:SetDTInt( 0, 2 ) -- This controls the flame effect
	
	local Phys = self:GetPhysicsObject()
	
	if Phys:IsValid() then

		if self.AutoUnfreeze then Phys:EnableMotion( true ); end
		Phys:Wake()
		
	end
	
	if self.ExplodeTime != 0 then timer.Simple( self.ExplodeTime, function() SafeExplode(self) end ); end
	
	self.LaunchTime = CurTime()
	self.Launched = true
	
	if WireAddon then Wire_TriggerOutput(self, 'Fuse Started', 1); end
	if WireAddon then Wire_TriggerOutput(self, 'Launched', 1); end
	
	local ED = EffectData()
	ED:SetOrigin( self:GetPos() + self:GetUp()*19 )
	util.Effect( 'cball_explode', ED )
	
end

local function SafeLaunch( Ent )

	if !IsValid( Ent ) then return end
	
	Ent:Launch()

end

function ENT:StartFuse()

	if self.FuseStarted then return end
	
	self:SetDTInt( 0, 1 )  -- This controls the flame effect
	
	timer.Simple( self.FuseTime, function() SafeLaunch(self) end )

	self.FuseStarted = true
	
	if WireAddon then Wire_TriggerOutput(self, 'Fuse Started', 1); end
	
end

function ENT:Use( activator, caller )

	self:StartFuse()
	
end

function ENT:TriggerInput( iname, value )

	if ( iname == 'Launch' && util.tobool( value )) then
	
		self:Launch()
	
	elseif ( iname == 'Start Fuse' && util.tobool( value )) then
	
		self:StartFuse()
	
	elseif ( iname == 'Explode' && util.tobool( value )) then

		SafeExplode( self )
	
	elseif ( iname == 'Fuse Time' && value) then

		self.FuseTime = math.max( tonumber( value ), 0 ) || 2
	
	elseif ( iname == 'Explode Time') then

		self.ExplodeTime = math.max( tonumber( value ), 0 ) || 2
	
	elseif ( iname == 'Explode Color RGB' && value) then

		if type( value ) != 'Vector' then return end
		
		local R,G,B = value[1], value[2], value[3]
		self.Col = Color( R, G, B )
		self:SetDTVector( 1, Vector( self.Col.r/255, self.Col.g/255, self.Col.b/255 ) )
		
	elseif ( iname == 'Instability' && value) then
	
		self.TrajectoryRand = math.Clamp( tonumber( value ), 0, 50 ) || 10
	
	elseif ( iname == 'Scale' && value) then

		self.FWScale = math.Clamp( tonumber( value ), 0.1, 3 ) || 1
	
	elseif ( iname == 'Trail Color RGB' && value) then
	
		if type( value ) != 'Vector' then return end
		local R,G,B = value[1], value[2], value[3]
		
		local Hue, Saturation, Value = ColorToHSV( Color( R, G, B ) )
		local Col = HSVToColor( Hue, Saturation/3, Value )
		local NewFlame = Vector( Col.r, Col.g, Col.b ) || Vector( 255, 165, 0 )
		
		self.FlameColor = NewFlame
		self:SetDTVector( 0, NewFlame )
		
	end
	return true
end