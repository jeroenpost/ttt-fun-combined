ENT.Type 			= 'anim'
ENT.Base 			= 'base_anim'
ENT.PrintName		= 'Firework'
ENT.Author			= 'TurtleHax/LinkTwilight/eng101'
ENT.Information		= 'A firework'

ENT.Spawnable			= false
ENT.AdminSpawnable		= false


-- Wiremod Compatibility:

-- Most of this is taken from the base_wire_entity code
-- If the firework entity ever breaks with wiremod, it's probably because they updated the base_wire_entity code and I don't have it in here

-- I could do some hack to derive the entire entity from base_wire_entity if you have wiremod installed, but that's just asking for trouble.

ENT.IsWire = true

if SERVER && WireAddon then
	ENT.WireDebugName = 'Firework'
	
	function ENT:OnRemove()
		Wire_Remove(self)
	end

	function ENT:OnRestore()
		Wire_Restored(self)
	end

	function ENT:BuildDupeInfo()
		return WireLib.BuildDupeInfo(self)
	end

	function ENT:ApplyDupeInfo(ply, ent, info, GetEntByID)
		WireLib.ApplyDupeInfo( ply, ent, info, GetEntByID )
	end

	function ENT:PreEntityCopy()
		//build the DupeInfo table and save it as an entity mod
		local DupeInfo = self:BuildDupeInfo()
		if(DupeInfo) then
			duplicator.StoreEntityModifier(self,'WireDupeInfo',DupeInfo)
		end
	end

	function ENT:PostEntityPaste(Player,Ent,CreatedEntities)
		//apply the DupeInfo
		if(Ent.EntityMods and Ent.EntityMods.WireDupeInfo) then
			Ent:ApplyDupeInfo(Player, Ent, Ent.EntityMods.WireDupeInfo, function(id) return CreatedEntities[id] end)
		end
	end
end

if CLIENT then
	WireAddonCL = ( Wire_Render != nil )
	if !WireAddonCL then return end
	
	function ENT:Think()
		if (CurTime() >= (self.NextRBUpdate or 0)) then
			self.NextRBUpdate = CurTime() + math.random(30,100)/10 --update renderbounds every 3 to 10 seconds
			Wire_UpdateRenderBounds(self)
		end
	end
end