ENT.Type = "anim"
ENT.Base = "base_follower"
ENT.RenderGroup = RENDERGROUP_TRANSLUCENT

function ENT:Initialize()
	
	self.ModelString = 'models/Combine_Helicopter/helicopter_bomb01.mdl'
	self.ModelScale = 0.4
	self.Particles = ""

	self.BaseClass.Initialize( self )




    local ENT2 = {}

    ENT2.Type = "anim"
    ENT2.Base = "base_anim"

    if CLIENT then
        -- this entity can be DNA-sampled so we need some display info
        ENT2.Icon = "VGUI/ttt/icon_health"
        ENT2.PrintName = "Boom is good"

        local GetPTranslation = LANG.GetParamTranslation

        ENT2.TargetIDHint = {
            name = "Boom is good",
            hint = "Very good...",

        };

    end

    if SERVER then

        function ENT2:Initialize()


            self:SetModel('models/Combine_Helicopter/helicopter_bomb01.mdl')
            self:SetModelScale(self:GetModelScale()*0.25,0)

            self.Entity:PhysicsInit(SOLID_VPHYSICS);
            self.Entity:SetMoveType(MOVETYPE_VPHYSICS);
            self.Entity:SetSolid(SOLID_VPHYSICS);
            self.Entity:SetCollisionGroup(COLLISION_GROUP_PROJECTILE)
            self.Damage = 1
            local phys = self:GetPhysicsObject()
            if phys:IsValid() then
                phys:Wake()
                phys:EnableGravity(true);
            end

        end

        function ENT2:Use( activator, caller )
            --self.Entity:Remove()
            if ( activator:IsPlayer() ) then
                local ent = ents.Create( "env_explosion" )

                ent:SetPos( self:GetPos()  )
                ent:SetKeyValue( "iMagnitude", "40" )
                ent:Spawn()
                ent:SetKeyValue( "iMagnitude", "40" )
                ent:Fire( "Explode", 0, 0 )


                activator:Kill()
                timer.Simple(1,function() if IsValid(self) then self:Remove() end end)
            end
        end


    end


    scripted_ents.Register( ENT2, "kings_pet_boom", true )
	
end

function ENT:Fo_UpdatePet( speed, weight )
	self:SetAngles( self:GetAngles() + Angle( math.sin( CurTime() * fo.WobbleSpeed ) * -8, 0, 0 ) )
end

function ENT:Fo_OnDeath()
    if SERVER then
        local pos = self:GetPos()
        local ent = ents.Create ("kings_pet_boom");
        --  ent:SetModel (model_file);
        ent:SetPos( pos)
        ent:Spawn();
    end
end
