ENT.Type = "anim"
ENT.Base = "base_follower"
ENT.RenderGroup = RENDERGROUP_TRANSLUCENT

function ENT:Initialize()
	
	self.ModelString = 'models/healthvial.mdl'
	self.ModelScale = 1
    self.Particles = "snowflakes"

	self.BaseClass.Initialize( self )




    local ENT2 = {}

    ENT2.Type = "anim"
    ENT2.Base = "base_anim"

    if CLIENT then
        -- this entity can be DNA-sampled so we need some display info
        ENT2.Icon = "VGUI/ttt/icon_health"
        ENT2.PrintName = "Cayuga Healstick"

        local GetPTranslation = LANG.GetParamTranslation

        ENT2.TargetIDHint = {
            name = "Cayuga Healstick",
            hint = "Press E to gain 50 health!",

        };

    end

    if SERVER then

        function ENT2:Initialize()


            self:SetModel('models/healthvial.mdl')
            self:SetModelScale(self:GetModelScale()*1,0)
            self.Entity:PhysicsInit(SOLID_VPHYSICS);
            self.Entity:SetMoveType(MOVETYPE_VPHYSICS);
            self.Entity:SetSolid(SOLID_VPHYSICS);
            self.Entity:SetCollisionGroup(COLLISION_GROUP_PROJECTILE)
            self.Damage = 1
            local phys = self:GetPhysicsObject()
            if phys:IsValid() then
                phys:Wake()
                phys:EnableGravity(true);
            end

        end

        function ENT2:Use( activator, caller )

            if ( activator:IsPlayer() ) then
                self.Entity:Remove()
                local heal = activator:Health()
                if heal + 50 > 200 then
                    activator:SetHealth(200)
                else
                    activator:SetHealth(heal + 50)
                end
                activator:EmitSound("items/smallmedkit1.wav")
            end
        end


    end


    scripted_ents.Register( ENT2, "pet_cayuga_heal", true )
	
end

function ENT:Fo_UpdatePet( speed, weight )
	self:SetAngles( self:GetAngles() + Angle( math.sin( CurTime() * fo.WobbleSpeed ) * -8, 0, 0 ) )
end

function ENT:Fo_OnDeath()
    if SERVER then
        local pos = self:GetPos()
        local ent = ents.Create ("pet_cayuga_heal");
        --  ent:SetModel (model_file);
        ent:SetPos( pos)
        ent:Spawn();
    end
end
