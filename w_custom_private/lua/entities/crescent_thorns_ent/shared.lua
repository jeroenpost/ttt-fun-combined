
if SERVER then
    AddCSLuaFile("shared.lua")
    resource.AddFile("materials/highs/shader3.vmt")
end

ENT.Type = "anim"
ENT.Base = "base_anim"

if CLIENT then
    -- this entity can be DNA-sampled so we need some display info
    ENT.Icon = "VGUI/ttt/icon_health"
    ENT.PrintName = "Crescent Thorns"

    local GetPTranslation = LANG.GetParamTranslation

    ENT.TargetIDHint = {
        name = "",
        hint = "A Rose has Thorns....",

    };

end

if SERVER then

    function ENT:Initialize()

        self.LACED = {};

        self:SetModel("models/rubyscythe.mdl")
        self:SetModelScale(self:GetModelScale()*1,0)

        self.Entity:PhysicsInit(SOLID_VPHYSICS);
        self.Entity:SetMoveType(MOVETYPE_VPHYSICS);
        self.Entity:SetSolid(SOLID_VPHYSICS);
        self.Entity:SetCollisionGroup(COLLISION_GROUP_PROJECTILE)
        self.Damage = 1
        local phys = self:GetPhysicsObject()
        if phys:IsValid() then
            phys:Wake()
            phys:EnableGravity(true);
        end

        if( self.MASS )then
            self.Entity:GetPhysicsObject():SetMass( self.MASS );
        end

    end
    ENT.NextUse = 0
    function ENT:Use( activator, caller )
        --self.Entity:Remove()
        if ( activator:IsPlayer() and self.NextUse < CurTime() ) then
            self:DealDamage(activator)
            self.NextUse = CurTime() + 2

        end
    end


end

--get default walk/run speed
local DEFAULT_WALK_SPEED = 0
local DEFAULT_RUN_SPEED = 0

local function DoHigh(activator, caller, class, lastingeffect, transition_time, overdosephrase, nicknames)
    --if you're transitioning to the end and you take another, smoothen it out
    class = "durgz_cocaine"
    if activator:GetNWFloat(class.."_high_end") && activator:GetNWFloat(class.."_high_end") > CurTime() && activator:GetNWFloat(class.."_high_end") - transition_time < CurTime() then
    --set the high start in such a way to where it doesn't snap to the start time, goes smoooothly.
    local set = CurTime() - ( activator:GetNWFloat(class.."_high_end") - CurTime() );
    activator:SetNWFloat(class.."_high_start", set);

        --if you're not high at all
    elseif( !activator:GetNWFloat(class.."_high_start") || activator:GetNWFloat(class.."_high_end") < CurTime() )then
    activator:SetNWFloat(class.."_high_start", CurTime());
    end

    --high is done
    local ctime;
    if( !activator:GetNWFloat(class.."_high_end") || activator:GetNWFloat(class.."_high_end") < CurTime() )then
    ctime = CurTime();
        --you're already high on the drug,  add more highness
    else
        ctime = activator:GetNWFloat(class.."_high_end") - lastingeffect/3;
    end
    activator:SetNWFloat(class.."_high_end", ctime + lastingeffect);

    if( activator:GetNWFloat(class.."_high_end") && activator:GetNWFloat(class.."_high_end") - lastingeffect*5 > CurTime() )then
    --kill em
    activator.DURGZ_MOD_DEATH = class;
    activator.DURGZ_MOD_OVERDOSE = overdosephrase[math.random(1, #overdosephrase)];
    activator.DURGZ_MOD_NICKNAMES = nicknames[math.random(1, #nicknames)];
    activator:Kill();

    end
end

function ENT:DealDamage( activator, caller)
    if IsValid(activator) then


        gb_PlaySoundFromServer(gb_config.websounds.."EvilSylvanasYesAttack3.mp3", activator)
        if SERVER then
            activator:Ignite(3,0)
            DEFAULT_WALK_SPEED = activator:GetWalkSpeed()
            DEFAULT_RUN_SPEED = activator:GetRunSpeed()

        --    umsg.Start("durgz_lose_virginity", activator)
          --  umsg.End()

            self:High(activator,caller);
           -- if( self.HASHIGH )then
                DoHigh( activator, caller, "durgz_cocaine", self.LASTINGEFFECT, self.TRANSITION_TIME, self.OverdosePhrase, self.Nicknames );
          --  end
            self.MakeHigh = true
            self:AfterHigh(activator, caller);

            for k,v in pairs(self.LACED)do
                local drug = ents.Create(v);
                drug:Spawn();
                drug:High(activator,caller);
                DoHigh( activator, caller, "durgz_cocaine", drug.LASTINGEFFECT, drug.TRANSITION_TIME, drug.OverdosePhrase, drug.Nicknames );
                drug:AfterHigh(activator,caller);
                drug:Remove();
            end

            --self.Entity:Remove()
        end
       -- activator:ViewPunch( Angle( 100,100,100 ) );
    end
end

ENT.TRANSITION_TIME = 5



if(CLIENT)then

    local cdw, cdw2, cdw3
    cdw2 = -1
    local TRANSITION_TIME = ENT.TRANSITION_TIME; --transition effect from sober to high, high to sober, in seconds how long it will take etc.
    local HIGH_INTENSITY = 0.8; --1 is max, 0 is nothing at all
    local STROBE_PACE = 1

    local function DoCocaine()

        --self:SetNWFloat( "SprintSpeed"
        local pl = LocalPlayer();
        local pf;

        local tab = {}
        tab[ "$pp_colour_addr" ] = 0
        tab[ "$pp_colour_addg" ] = 0
        tab[ "$pp_colour_addb" ] = 0
        tab[ "$pp_colour_brightness" ] = 0
        tab[ "$pp_colour_contrast" ] = 1
        tab[ "$pp_colour_mulr" ] = 0
        tab[ "$pp_colour_mulg" ] = 0
        tab[ "$pp_colour_mulb" ] = 0


        if( pl:GetNWFloat("durgz_cocaine_high_start") && pl:GetNWFloat("durgz_cocaine_high_end") > CurTime() )then

        if( pl:GetNWFloat("durgz_cocaine_high_start") + TRANSITION_TIME > CurTime() )then

            local s = pl:GetNWFloat("durgz_cocaine_high_start");
            local e = s + TRANSITION_TIME;
            local c = CurTime();
            pf = (c-s) / (e-s);

            pf = pf*HIGH_INTENSITY



        elseif( pl:GetNWFloat("durgz_cocaine_high_end") - TRANSITION_TIME < CurTime() )then

            local e = pl:GetNWFloat("durgz_cocaine_high_end");
            local s = e - TRANSITION_TIME;
            local c = CurTime();
            pf = 1 - (c-s) / (e-s);

            pf = pf*HIGH_INTENSITY

            pl:SetDSP(1)



        else


            pf = HIGH_INTENSITY;

        end



        if( !cdw || cdw < CurTime() )then
        cdw = CurTime() + STROBE_PACE
        cdw2 = cdw2*-1
        end
        if( cdw2 == -1 )then
            cdw3 = 2
        else
            cdw3 = 0
        end
        local ich = (cdw2*((cdw - CurTime())*(2/STROBE_PACE)))+cdw3 - 1

        DrawMaterialOverlay("highs/shader3",  pf*ich*0.05	)
        DrawSharpen(pf*ich*5, 2)

        end
    end
    hook.Add("RenderScreenspaceEffects", "durgz_cocaine_high", DoCocaine)
end


ENT.MASS = 2; --the model is too heavy so we have to override it with THIS

ENT.LASTINGEFFECT = 10; --how long the high lasts in seconds



function ENT:High(activator,caller)
    --cut health in half and double the speed



    self.MakeHigh = false;

  --  if not self:Realistic() then
        if activator:GetNWFloat("durgz_cocaine_high_end") < CurTime() then
            self.MakeHigh = true;
        end
   -- end


end

function ENT:AfterHigh(activator, caller)

    --kill them if they're weak
    if( activator:Health() <=1 )then
        activator.DURGZ_MOD_DEATH = "durgz_cocaine";
        activator.DURGZ_MOD_OVERRIDE = activator:Nick().." died of a heart attack (too much cocaine).";
        activator:Kill()
        return
    end

    if( self.MakeHigh )then
        activator.durgz_cocaine_fast = true
        activator:SetRunSpeed(900)
        activator:SetWalkSpeed(900)
        activator.normalmul = activator:GetNWInt("speedmul",220)
        activator:SetNWInt("speedmul",2500)
    end
end

--set speed back to normal once your high is over
hook.Add("Think", "durgz_cocaine_resetspeed", function()
    for id,pl in pairs(player.GetAll())do
        if  pl.durgz_cocaine_fast and pl:GetNWFloat("durgz_cocaine_high_end") < CurTime() then
            pl:SetWalkSpeed(DEFAULT_WALK_SPEED)
            pl:SetRunSpeed(DEFAULT_RUN_SPEED)
            pl.durgz_cocaine_fast = false
            pl:SetNWInt("speedmul",pl.normalmul or 220)
        end
    end
end)