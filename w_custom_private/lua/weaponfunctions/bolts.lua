
SWEP.nextBolty = 0

function SWEP:PrimaryAttack()
    if ( self.nextBolty > CurTime() ) then return end
    self.nextBolty = CurTime() + 0.75

    if ( self.m_bInZoom && IsMultiplayer() ) then
    //		self:FireSniperBolt();
    self:FireBolt();
    else
        self:FireBolt();
    end

    // Signal a reload
    self.m_bMustReload = true;

end


function SWEP:FireBolt()

    if ( self.Weapon:Clip1() <= 0 && self.Primary.ClipSize > -1 ) then
    if ( self:Ammo1() > 3 ) then
        self:Reload();
        self:ShootBullet( 100, 1, 0.01 )
    else
        self.Weapon:SetNextPrimaryFire( 5 );

    end

    return;
    end

    local pOwner = self.Owner;

    if ( pOwner == NULL ) then
        return;
    end

    if ( !CLIENT ) then
    local vecAiming		= pOwner:GetAimVector();
    local vecSrc		= pOwner:GetShootPos();

    local angAiming;
    angAiming = vecAiming:Angle();

    local pBolt = ents.Create ( "crossbow_bolt" );
    pBolt:SetPos( vecSrc );
    pBolt:SetAngles( angAiming );
    pBolt.Damage = self.Primary.Damage;
    self:ShootBullet( 100, 1, 0.01 )
    pBolt.AmmoType = self.Primary.Ammo;
    pBolt:SetOwner( pOwner );
    pBolt:Spawn()

    if ( pOwner:WaterLevel() == 3 ) then
        pBolt:SetVelocity( vecAiming * BOLT_WATER_VELOCITY );
    else
        pBolt:SetVelocity( vecAiming * BOLT_AIR_VELOCITY );
    end
    if self.Owner:Health() < 150 then
        self.Owner:SetHealth( self.Owner:Health() + 10)
    end
    end

    self:TakePrimaryAmmo(1 );

    if ( !pOwner:IsNPC() ) then
    pOwner:ViewPunch( Angle( -2, 0, 0 ) );
    end

    self.Weapon:EmitSound( self.Primary.Sound );
   -- self.Owner:EmitSound( self.Primary.Special2 );

    self.Weapon:SendWeaponAnim( ACT_VM_PRIMARYATTACK );

    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay );
    self.Weapon:SetNextSecondaryFire( CurTime() + self.Primary.Delay );

    // self:DoLoadEffect();
    // self:SetChargerState( CHARGER_STATE_DISCHARGE );

end

function SWEP:CanPrimaryAttack()
    return true
end

function SWEP:SetDeploySpeed( speed )

    self.m_WeaponDeploySpeed = tonumber( speed / GetConVarNumber( "phys_timescale" ) )

    self.Weapon:SetNextPrimaryFire( CurTime() + speed )
    self.Weapon:SetNextSecondaryFire( CurTime() + speed )

end

function SWEP:WasBought(buyer)
    if IsValid(buyer) then -- probably already self.Owner
        buyer:GiveAmmo( 6, "XBowBolt" )
    end
end 