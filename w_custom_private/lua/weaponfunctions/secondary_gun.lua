
SWEP.NextSecondary = 0
function SWEP:ShootSecondary()

    if self.NextSecondary > CurTime() then
        return
    end
    self.NextSecondary = CurTime() + 1.2
    self:EmitSound(  "weapons/usp/usp1.wav", 100 )

    self:ShootBullet( 25, 0.02, 1, 0.003 )

    self.Owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )

end
