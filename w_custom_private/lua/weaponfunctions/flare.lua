SWEP.flares = 20
SWEP.NextFlare = 0

local function flareRunIgniteTimer(ent, timer_name)
    if IsValid(ent) and ent:IsOnFire() then

        if ent:WaterLevel() > 0 then
            ent:Extinguish()
        elseif CurTime() > ent.burn_destroy then

            ent:Extinguish()
            if ent.willRemove and ent.ignite_info then
                local owner = ignite_info[1]
                if not IsValid(owner) then return end
                if owner.burnedBody then return end
                owner.burnedBody = true
                ent:SetNotSolid(true)
                ent:Remove()
            end

        else
            -- keep on burning
            return
        end
    end

    timer.Destroy(timer_name) -- stop running timer
end

local function ScorchUnderRagdoll(ent)
    if SERVER then
        local postbl = {}
        -- small scorches under limbs
        for i = 0, ent:GetPhysicsObjectCount() - 1 do
            local subphys = ent:GetPhysicsObjectNum(i)
            if IsValid(subphys) then
                local pos = subphys:GetPos()
                util.PaintDown(pos, "FadingScorch", ent)

                table.insert(postbl, pos)
            end
        end
    end
    -- big scorch at center
    local mid = ent:LocalToWorld(ent:OBBCenter())
    mid.z = mid.z + 25
    util.PaintDown(mid, "Scorch", ent)
end

local function flareIgniteTarget(att, path, dmginfo)
    local ent = path.Entity
    if not IsValid(ent) then return end

    if SERVER then
        local dur = ent:IsPlayer() and 5 or 10
        -- disallow if prep or post round
        if ent:IsPlayer() and (not GAMEMODE:AllowPVP()) then return end
        ent:Ignite(dur, 100)
        ent.ignite_info = { att = dmginfo:GetAttacker(), infl = dmginfo:GetInflictor() }

        if ent:IsPlayer() then
            timer.Simple(dur + 0.1, function()
                if IsValid(ent) then
                    ent.ignite_info = nil
                end
            end)

        elseif ent:GetClass() == "prop_ragdoll" then

            ScorchUnderRagdoll(ent)
            if not att.burnedBody then
                ent.willRemove = true
                att.burnedBody = true
            end

            local burn_time = 3
            local tname = Format("ragburn_%d_%d", ent:EntIndex(), math.ceil(CurTime()))

            ent.burn_destroy = CurTime() + burn_time

            timer.Create(tname,
                0.1,
                math.ceil(1 + burn_time / 0.1), -- upper limit, failsafe
                function()
                    flareRunIgniteTimer(ent, tname)
                end)
        end
    end
end

function SWEP:ShootFlare()
    if self.flares < 1 or self.NextFlare > CurTime() then return end
    self.NextFlare = CurTime() + 5
    self.Weapon:EmitSound("Weapon_USP.SilencedShot")
    self.Weapon:SendWeaponAnim(ACT_VM_PRIMARYATTACK)
    self.flares = self.flares - 1
    if IsValid(self.Owner) then
        self.Owner:SetAnimation(PLAYER_ATTACK1)

        self.Owner:ViewPunch(Angle(math.Rand(-0.2, -0.1) * self.Primary.Recoil, math.Rand(-0.1, 0.1) * self.Primary.Recoil, 0))
    end
    local cone = self.Primary.Cone
    local bullet = {}
    bullet.Num = 1
    bullet.Src = self.Owner:GetShootPos()
    bullet.Dir = self.Owner:GetAimVector()
    bullet.Spread = Vector(cone, cone, 0)
    bullet.Tracer = 1
    bullet.Force = 2
    bullet.Damage = 15
    bullet.TracerName = self.Tracer
    bullet.Callback = flareIgniteTarget

    self.Owner:FireBullets(bullet)
end

