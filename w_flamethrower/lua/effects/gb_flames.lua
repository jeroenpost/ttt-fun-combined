
EFFECT.Flames = Material( "gb_flamethrower/flame" )

function EFFECT:Init( data )
	self.Position = data:GetStart()
        self.WeaponEnt = data:GetEntity()
	self.Attachment = data:GetAttachment()

    if not IsValid(self.WeaponEnt.Owner) then return end
	self.EndPos =  self.WeaponEnt.Owner:EyePos() + self.WeaponEnt.Owner:EyeAngles( ):Forward( )*400 
        self.EndPos = self.EndPos + Vector( math.Rand(25,-25),math.Rand(25,-25), math.Rand(25,-25) )
	
	self.StartPos = self:GetTracerShootPos( self.Position, self.WeaponEnt, self.Attachment )
	self:SetRenderBoundsWS( self.StartPos, self.EndPos )

	self.Dir = ( self.EndPos - self.StartPos ):GetNormalized()
	self.Dist = self.StartPos:Distance( self.EndPos )
	
	self.LifeTime = 1
	self.DieTime = CurTime() + self.LifeTime
end

function EFFECT:GetTracerShootPos( Position, Ent, Attachment )

	self.ViewModelTracer = false

	if (!Ent:IsValid()) then return Position end
	if (!Ent:IsWeapon()) then return Position end

	-- Shoot from the viewmodel
	if ( Ent:IsCarriedByLocalPlayer() && !LocalPlayer():ShouldDrawLocalPlayer() ) then

		local ViewModel = LocalPlayer():GetViewModel()

		if ( ViewModel:IsValid() ) then

			local att = ViewModel:GetAttachment( Attachment )
			if ( att ) then
				Position = att.Pos
				self.ViewModelTracer = true
			end

		end

	-- Shoot from the world model
	else
            
		local att = Ent:GetAttachment( Attachment )
		if ( att ) then
			Position = att.Pos 
		end
                return self.Position * 100
	end 

	return Position

end

function EFFECT:Think()
    if self.DieTime then
	if ( CurTime() > self.DieTime ) then return false end
	return true
    end
end

function EFFECT:Render()
    if not self.DieTime then return end
	local v1 = ( CurTime() - self.DieTime ) / self.LifeTime
	local v2 = ( self.DieTime - CurTime() ) / self.LifeTime
	local a = self.EndPos - self.Dir * math.min( 1 - ( v1 * self.Dist ), self.Dist )

	render.SetMaterial(  self.Flames )
	render.DrawBeam( a, self.EndPos, v2 * 9, 10, self.Dist / 5, self.WeaponEnt.flamecolor or Color( 255, 255, 255,  255 ) )
end
