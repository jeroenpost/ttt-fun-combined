
AddCSLuaFile()

DEFINE_BASECLASS( "base_anim" )

ENT.PrintName		= "Drone"
ENT.Author			= "Gonzalolog"
ENT.Information		= "Partner"
ENT.Category		= "Gonzalolog"

ENT.Editable			= false
ENT.Spawnable			= false
ENT.AdminOnly			= true
ENT.RenderGroup 		= RENDERGROUP_TRANSLUCENT
ENT.AutomaticFrameAdvance = true 

ENT.Health 			= 150 		//Max health of the drone
ENT.WepDmg 			= 22  		//Bullet damage
ENT.EnableShare 	= false		//Let's to the player use other people drones
ENT.UpgradeShare	= true		//Let's take any player upgrades
ENT.OnTTT 			= false
------------------
//	Properties 	//
------------------

ENT.flash = 5
ENT.fuel = 1000
ENT.bullets = 3000

ENT.FuelUpg = true
ENT.SpeedUpg = 6
ENT.AnthUpg = true
ENT.gunUpg = true
ENT.flashUpg = true

ENT.target = nil

ENT.l = 0
ENT.f = 0
ENT.u = 0
ENT.r = false

ENT.nFlash = 0
ENT.nextFire = 0
ENT.nReload = 0
ENT.nFuel = 0
ENT.NextUse = 0

ENT.Driving = false

ENT.mlp = 1
ENT.fs = nil 


ENT.Equipment = {}

ENT.FlySound = Sound("npc/scanner/cbot_fly_loop.wav")

------------------------------
// Sandbox Initialization 	//
------------------------------

function ENT:SpawnFunction( ply, tr, ClassName )

	if ( !tr.Hit ) then return end
	
	local size = math.random( 16, 48 )
	local SpawnPos = tr.HitPos + tr.HitNormal * size
	
	local ent = ents.Create( ClassName )
		ent:SetPos( SpawnPos )
	
	ent:Spawn()
	ent:Activate()

	return ent

	
end

function ENT:Initialize()

	if ( SERVER ) then


            self:SetNWString("shootmode","SMG")

		-- Use the helibomb model just for the shadow (because it's about the same size)
		self:SetModel( "models/Combine_Scanner.mdl" )
        self:SetMaterial("")
        self:SetMaterial("Models/effects/vol_light001")
		
		self:PhysicsInit(SOLID_VPHYSICS )
    	self:SetMoveType(MOVETYPE_VPHYSICS)
    	self:SetSolid(SOLID_VPHYSICS)

		local phys = self:GetPhysicsObject()
		if (phys:IsValid()) then

			phys:Wake()			
			phys:EnableMotion( true )

		end

		self:SetPoseParameter("alert_control",1)

		self:SetBodygroup(1,math.random(0,3))


	end
	
end

//////////////////////////
//	Animating at Spawn 	//
//////////////////////////

function ENT:AnimateEye()

	local st = -1
	local dt = 0

	hook.Add("Think","AnimatingEye"..self:EntIndex(),function()
		if(self == nil || !self:IsValid()) then
			hook.Remove("Think","AnimatingEye"..self:EntIndex())
		end
		if self != nil && self:IsValid() then
			self:SetPoseParameter("alert_control",st)
			if(dt == 0) then
				if(st < 1) then
					st = Lerp(FrameTime()*5,st,1.2)
				else
					dt = 1
				end
			elseif(dt == 1) then
				if(st > 0.5) then
					st = Lerp(FrameTime()*5,st,0.4)
				else
					hook.Remove("Think","AnimatingEye"..self:EntIndex())
				end
			end
		end
	end)

end



/////////////////////////////////////
//	We need to create basic lights //
/////////////////////////////////////

function ENT:CreateLights()

		local bone = self:LookupBone("Scanner.Neck")
		local pos,ang = self:GetBonePosition(bone)

		self.RedDotLaser = ents.Create("env_sprite");
		self.RedDotLaser:SetPos( pos + self:GetUp()*7.5 - self:GetForward()*4 );
		self.RedDotLaser:SetKeyValue( "renderfx", "14" )
		self.RedDotLaser:SetKeyValue( "model", "sprites/light_glow02.vmt")
		self.RedDotLaser:SetKeyValue( "scale","0.5")
		self.RedDotLaser:SetKeyValue( "spawnflags","1")
		self.RedDotLaser:SetKeyValue( "angles","0 0 0")
		self.RedDotLaser:SetKeyValue( "rendermode","9")
		self.RedDotLaser:SetKeyValue( "renderamt","255")
		self.RedDotLaser:SetKeyValue( "rendercolor", "255 255 255" )				
		self.RedDotLaser:Spawn()
		self.RedDotLaser:SetParent(self)

		self.light = ents.Create("env_sprite");
		self.light:SetPos( pos );
		self.light:SetKeyValue( "renderfx", "14" )
		self.light:SetKeyValue( "model", "sprites/orangeglow1.vmt")
		self.light:SetKeyValue( "scale","0.25")
		self.light:SetKeyValue( "spawnflags","0")
		self.light:SetKeyValue( "angles","0 0 0")
		self.light:SetKeyValue( "rendermode","9")
		self.light:SetKeyValue( "renderamt","255")
		self.light:SetKeyValue( "rendercolor", "255 0 0" )				
		self.light:Spawn()
		self.light:SetParent(self)

end

//////////////////////////////
//	Just remove the entity 	//
//////////////////////////////

function ENT:OnRemove()

	if SERVER && (self.fs != nil) then
		self.fs:Stop()
	end

	if SERVER then

		net.Start("RemoveCModels")
		net.WriteEntity(self)
		net.Broadcast()

	end

	if(self:GetOwner() != nil && self:GetOwner():IsValid() && self:GetOwner():Alive()) then

	if SERVER then
		self:Explode()
	end

	self:GetOwner().OnDrone = false

	for k,v in pairs(self.Equipment) do

		if(v != nil) then
		
			local wep = ents.Create(v)
			wep:SetPos(self:GetOwner():GetPos())
			wep:Spawn()

		end
	end

	self:GetOwner():SetMoveType(MOVETYPE_WALK)

	end

end

//////////////////////////////////////////////////////////////////////
//	We use this function when we need to remove the entity by code 	//
//////////////////////////////////////////////////////////////////////

function ENT:SafeRemove()

	if SERVER && (self.fs != nil) then
		self.fs:Stop()
	end

	if SERVER then

		net.Start("RemoveCModels")
		net.WriteEntity(self)
		net.Broadcast()

	end

	if(self:GetOwner() != nil && self:GetOwner():IsValid() && self:GetOwner():Alive()) then

	if SERVER then
		self:Explode()
	end



	for k,v in pairs(self.Equipment) do

		if(v != nil) then
		
			local wep = ents.Create(v)
			wep:SetPos(self:GetOwner():GetPos())
			wep:Spawn()

		end
	end

	self:GetOwner():SetMoveType(MOVETYPE_WALK)

	end

end

//////////////////////////////
//	Handle item upgrades 	//
//////////////////////////////

function ENT:AddFuel(b)

	self.FuelUpg = true

	--net.Start("FuelUpgrade")
	--net.WriteEntity(self)
	--net.Broadcast()

end

function ENT:AddFlash()

	self.flashUpg = true

	--net.Start("FlashUpgrade")
	--net.WriteEntity(self)
	--net.Broadcast()


    local fuel = ents.Create("prop_dynamic")
    fuel:SetModel("models/food/burger.mdl")
    fuel:SetPos(self:GetPos() + self:GetUp()*-28 + self:GetForward()*2 + self:GetRight()*-0)
    fuel:SetParent(self)
    fuel:SetLocalAngles(Angle(0,90,00))
    fuel:SetModelScale(2,0)
    --fuel.IsGhost = true
    fuel:Spawn()

   -- ent.flashModel = fuel

end

function ENT:AddGun()

	self.gunUpg = true

	net.Start("GunUpgrade")
	net.WriteEntity(self)
	net.Broadcast()

end

function ENT:AddAnthena()

	self.AnthUpg = true

	--net.Start("AnthenaUpgrade")
	--net.WriteEntity(self)
	--net.Broadcast()

end

function ENT:AddSpeed()

	self.SpeedUpg = 2

	--net.Start("SpeedUpgrade")
	--net.WriteEntity(self)
	--net.Broadcast()

end

//////////////////////////////////
//	We start using the drone 	//
//////////////////////////////////

function ENT:Use(ply)

	if((self:GetOwner() == nil || !self:GetOwner():IsValid()) && self.NextUse < CurTime() && (self.Owner == ply || self.EnableShare)) then

    self.NextUse = CurTime()+1

    ply.OnDrone = true
    self.Driving = true

	self.Equipment = {}

	net.Start("ActivateDrone")
	net.WriteFloat(1)
	net.Send(ply)

	if(self.OnTTT) then

		net.Start("DisableTag")
		net.WriteFloat(1)
		net.WriteEntity(self)
		net.Broadcast()

	end

	for k,v in pairs(ply:GetWeapons()) do

		table.insert(self.Equipment,v:GetClass())

	end

	--self:CreateLights()

	self.fs = CreateSound(self, self.FlySound )
	self.fs:Play()
	self.fs:ChangeVolume(0.5,0)

	self:AnimateEye()

	ply:StripWeapons()
    ply:Give("weapon_zm_improvised")
    ply:SelectWeapon("weapon_zm_improvised")

	self:SetOwner(ply)

	local ent = self

	timer.Simple(1.25,function() if(ent != nil && ent:IsValid()) then ent.r = true ent.u = ent:GetPos().z end end)

	ply:SetMoveType(MOVETYPE_NONE)

	local phys = self:GetPhysicsObject()
	if (phys:IsValid()) then
		phys:Wake()
			
		phys:EnableMotion( true )
	end

	self:StartMotionController()

	self.ShadowParams = {}
	self:SetNWBool( "active", false )
	self:SetNWInt( "numPlyTouch", 0 )
	self.Shoot = math.random(0.5,1.5)

	end

end

local dinamoWheel = -1

//////////////////////////////////////////
//	We handle EVERYTHING about input 	//
//////////////////////////////////////////

local nextCheck = 0

hook.Add("Tick","Fuel",function()

	for k,v in pairs(ents.FindByClass("sent_drone")) do


		if(v:GetVelocity():Length() < 10 && v.fuel <= 100 && v.nFuel < CurTime()) then

			v.fuel = v.fuel + 0.25
			v.nFuel = CurTime() + 0.25

		end

		if v:GetOwner() != nil && v:GetOwner():IsValid() then

        if(v:GetOwner():KeyDown(IN_WALK) )then
            if v.nextturn and v.nextturn > CurTime() then return end
            v:SetAngles(v:GetAngles()+Angle(-180,0,-180))
            v:SetVelocity( Vector( 0, 0, 0 ) )
            v.nextturn = CurTime() + 0.5

        end


        if(v:GetOwner():KeyDown(IN_USE) &&
        v:GetOwner():KeyDown(IN_RELOAD)) then
            if not v.NextReload then v.NextReload = 0 end
            if v.NextReload > CurTime() then return end
            v.NextReload = CurTime() + 0.3
            local mode = v:GetNWString("shootmode")

            if mode == "SMG" then  v:SetNWString("shootmode","Fire")  end
            if mode == "Fire" then  v:SetNWString("shootmode","Shotgun") end
            if mode == "Shotgun" then  v:SetNWString("shootmode","Minigun") end
        if mode == "Minigun" then  v:SetNWString("shootmode","Self Destruct") end
            if mode == "Self Destruct" then  v:SetNWString("shootmode","SMG") end

            return


        end

		if(v:GetOwner():KeyDown(IN_DUCK) && !v:GetOwner():KeyDown(IN_RELOAD) && v.NextUse < CurTime()) then

			v.NextUse = CurTime() + 1
			v:GetOwner():SetMoveType(MOVETYPE_WALK)
			v.Driving = false

			for a,s in pairs(v.Equipment) do

				if(s != nil) then
			
					local wep = ents.Create(s)
					wep:SetPos(v:GetOwner():GetPos())
					wep:Spawn()

				end

			end

		--	net.Start("ActivateDrone")
		--	net.WriteFloat(0)
		--	net.Send(v:GetOwner())

			v:GetOwner().OnDrone = false
			v:SetOwner(nil)

		end

		end
		
		if v:GetOwner() != nil && v:GetOwner():IsValid() && v.fuel > 0 then

			if(v:GetOwner():KeyDown(IN_SPEED)) then

				v.mlp = 2

				if SERVER && fs != nil then
					v.fs:ChangeVolume(2,0)
				end

			else

				v.mlp = 1
				if SERVER && fs != nil then
					v.fs:ChangeVolume(0.5,0)
				end

			end

			if(v.nFlash < CurTime() && v.flash < 100) then

				v.flash = Lerp(0.5*FrameTime(),v.flash,100)

			end

			if(v:GetOwner():KeyDown(IN_ATTACK) && v.nFlash < CurTime() && v.flash >= 25) then

				v:DoFlash()
				v.nFlash = CurTime() + 10

			end


			if(v:GetOwner():KeyDown(IN_ATTACK2)) then

				v:ShootBB()
				v.nReload = CurTime()+3

			end

			if(v.bullets < 30 && v.nReload < CurTime()) then

				v.nReload = CurTime()+0.15
				v.bullets = v.bullets + 1

			end

			if(v:GetOwner():KeyDown(IN_MOVELEFT)) then

				v.l = Lerp(FrameTime()*5,v.l,-90*v.mlp*v.SpeedUpg)

			elseif(v:GetOwner():KeyDown(IN_MOVERIGHT)) then

				v.l = Lerp(FrameTime()*5,v.l,90*v.mlp*v.SpeedUpg)

			else

				v.l = Lerp(FrameTime()*5,v.l,0)

			end

			if(v:GetOwner():KeyDown(IN_FORWARD)) then

				v.f = Lerp(FrameTime()*5,v.f,90*v.mlp*v.SpeedUpg)


			elseif(v:GetOwner():KeyDown(IN_BACK)) then

				v.f = Lerp(FrameTime()*5,v.f,-90*v.mlp*v.SpeedUpg)

			else

				v.f = Lerp(FrameTime()*5,v.f,0)

			end



		end

	end

end)


//////////////////////////////////////////////////////
//	Kill the drone if we don't have enough life 	//
//////////////////////////////////////////////////////

function ENT:OnTakeDamage(dmg)

	if(self.health == nil) then

		self.health = 1000

	end

	self.health = self.health - dmg:GetDamage()

	if(self.health <= 0) and SERVER then

		self:Explode()
		self:SafeRemove()
		self:Remove()

		if(self:GetOwner() != nil && self:GetOwner():IsValid()) then

			self:GetOwner().OnDrone = false

		end

	end

end

//////////////////////////////////////////////////////////////////////////////
//	Shoot the bullet if we have enough bullets, the upgrade and the decay 	//
//////////////////////////////////////////////////////////////////////////////

function ENT:ShootBB()

	if(self.gunUpg && self.bullets > 0 && self.nextFire < CurTime() and SERVER) then


        local mode = self:GetNWString("shootmode")

        if mode == "SMG" then
		self:EmitSound("npc/strider/strider_minigun.wav",40)

		self.bullets = self.bullets - 1
		self.nextFire = CurTime()+math.random(0.25,0.15)

		local bone = self:LookupBone("Scanner.Neck")
		local pos,ang = self:GetBonePosition(bone)

		bullet = {}
		bullet.Src=pos
		bullet.Dir=self:GetAngles():Forward()
		bullet.Spread=Vector(0.025,0.025,0)
		bullet.Tracer=1	
		bullet.Force= 12
		bullet.Attacker = self:GetOwner()
		bullet.Damage= self.WepDmg
 
		self:FireBullets(bullet)
        end


    if mode == "Shotgun" then
        self:EmitSound("weapons/m3/s3-1.mp3" ,40)

        self.bullets = self.bullets - 1
        self.nextFire = CurTime()+1.25

        local bone = self:LookupBone("Scanner.Neck")
        local pos,ang = self:GetBonePosition(bone)

        bullet = {}
        bullet.Src=pos
        bullet.Dir=self:GetAngles():Forward()
        bullet.Spread=Vector(0.025,0.025,0)
        bullet.Tracer=1
        bullet.Num=12
        bullet.Force= 12
        bullet.Attacker = self:GetOwner()
        bullet.Damage= 9

        self:FireBullets(bullet)

    end

        if mode == "Fire" then
            local bone = self:LookupBone("Scanner.Neck")
            local pos,ang = self:GetBonePosition(bone)
            local bullet = {}
            bullet.Num = 1
            bullet.Src=pos
            bullet.Dir=self:GetAngles():Forward()
            bullet.Owner = self.Owner
            bullet.Spread = Vector( 0.04, 0.04, 0 )
            bullet.Tracer = 1
            bullet.Force = 5
            bullet.Damage = 6
                    //bullet.AmmoType = "Ar2AltFire" -- For some extremely stupid reason this breaks the tracer effect
            bullet.TracerName = "gb_flames"
            self.nextFire = CurTime()+math.random(0.05,0.03)
            self:FireBullets(bullet)
        end

    if mode == "Minigun" then
        self:EmitSound("weapons/ar2/fire1.wav",50)
        local bone = self:LookupBone("Scanner.Neck")
        local pos,ang = self:GetBonePosition(bone)
        local bullet = {}
        bullet.Num = 1
        bullet.Src=pos
        bullet.Dir=self:GetAngles():Forward()
        bullet.Owner = self.Owner
        bullet.Spread = Vector( 0.07, 0.07, 0 )
        bullet.Tracer = 1
        bullet.Force = 10
        bullet.Damage = math.random(5,8)
                //bullet.AmmoType = "Ar2AltFire" -- For some extremely stupid reason this breaks the tracer effect
        self.nextFire = CurTime()+math.random(0.05,0.015)
        self:FireBullets(bullet)
    end
    if mode == "Self Destruct" then
        if !ents or !SERVER then return end
        local ent = ents.Create( "env_explosion" )
        if( IsValid(self:GetOwner()) ) then
            ent:SetPos( self:GetPos() )
            ent:SetOwner( self:GetOwner() )
            ent:SetKeyValue( "iMagnitude", "250" )
            ent:Spawn()

            self:Destroy()
            self:Remove()
            ent:Fire( "Explode", 0, 0 )
            ent:EmitSound( "weapons/big_explosion.mp3", 511, 100 )
        end

    end
 
	end

end

//////////////////////////////////
//	We must create the flash 	//
//////////////////////////////////

function ENT:DoFlash()

	self.flash = self.flash - 25
	self:EmitSound("npc/scanner/scanner_photo1.wav")

	if SERVER then
		net.Start("SetBlind") 
		net.WriteFloat(5)
		net.Send(self:GetOwner())
	end

	if SERVER && !self.flashUpg then

		for k,v in pairs(ents.FindInSphere(self:GetPos(),256)) do

			if(v:IsPlayer() && v != self:GetOwner()) then

				local trA = math.Round(v:GetAngles().y - self:GetAngles().y)
				local trB = math.Round(self:GetAngles().y)

				if((trB > 100 || trB < -115)) then

					local tracedata = {}

					tracedata.start = self:GetPos()
					tracedata.endpos = self:GetPos() + ( self:GetForward() * 200 )
					tracedata.filter = {self}
					tracedata.mins = Vector( -15,-15,-15 )
					tracedata.maxs = Vector( 15,15,15 )
					tracedata.mask = MASK_SHOT_HULL
					local tr = util.TraceHull( tracedata )

					if(tr.Entity:IsPlayer()) then
						
						net.Start("SetBlind") 
						net.WriteFloat(1)
						net.Send(tr.Entity)

					end

				end

			end

		end

	elseif(SERVER) then

		for k,v in pairs(ents.FindInSphere(self:GetPos(),256)) do

			if(v:IsPlayer()) then

				net.Start("SetBlind") 
				net.WriteFloat(1)
				net.Send(v)
				
			end

		end

	end

end

//////////////////////////////////////////////
//	Once we're killed/destroyed we explode 	//
//////////////////////////////////////////////

function ENT:Destroy()

    if not SERVER then return end
	local ent = ents.Create("env_explosion")

	ent:SetPos(self:GetPos())
	ent:Spawn()
	ent:Fire("explode")
	self:Remove()

	if(self:GetOwner() != nil && self:GetOwner():IsValid()) then

		self:GetOwner().OnDrone = false

	end


end



//////////////////////////
//	Handling physics 	//
//////////////////////////

function ENT:PhysicsSimulate( phys, deltatime )
 	
 	if((self:GetOwner() != nil && self:GetOwner():IsValid()) || self.target != nil) then

	ply = self:GetOwner()

	phys:Wake()

	self.ShadowParams.secondstoarrive = 2

	// We only want to move it if the drone isn't on the ground

	if(!self.r && self.target == nil) then
		self.ShadowParams.pos = ply:GetPos() + ply:GetRight()*75 + ply:GetForward() * -15 + ply:GetUp() * -(-80 + math.cos(RealTime())*25)
	elseif(self.target == nil) then
		self.ShadowParams.pos = self:GetPos() + self:GetRight()*self.l*self.mlp + self:GetForward()*self.f*self.mlp
	else
		self.ShadowParams.pos = self.target
	end

	self.ShadowParams.maxangular = 5000
	self.ShadowParams.maxangulardamp = 10000
	self.ShadowParams.maxspeed = 1000000
	self.ShadowParams.maxspeeddamp = 10000
	self.ShadowParams.dampfactor = 1
	self.ShadowParams.teleportdistance = 900000000
	self.ShadowParams.deltatime = deltatime
 	
	if(self.r && self:GetOwner() != nil && self:GetOwner():IsValid()) then
 		self.ShadowParams.angle = self:GetOwner():EyeAngles()
 	end

 	if(self.target != nil) then

 		self.ShadowParams.angle = self.target:Angle()

 	end
 	// Drop if we don't have enough fuel

 	if(self.fuel > 1 && self.Driving == true) then
			phys:ComputeShadowControl(self.ShadowParams)
		else
			net.Start("SyncFuel")
			net.WriteFloat(self.fuel)
			net.Send(self:GetOwner())
	end
 
 	end

end

/////////////////////////////////
//	We handle basic animations //
/////////////////////////////////

function ENT:Think()

	local ply = self
	local sequence = ply:LookupSequence("idle");


	if ( ply:GetSequence() != sequence ) then
     	ply:SetPlaybackRate( 1.0 );
     	ply:ResetSequence(sequence);
     	ply:SetCycle( 0 );

	end

end


if CLIENT then

surface.CreateFont( "CV20", {
	font = "coolvetica",
	size = ScrW()/64,
	weight = 500,
} )

//////////////////////////////////////////////////////
//	Forcing to draw the tracers and the player tag 	//
//////////////////////////////////////////////////////

net.Receive("ActivateDrone",function()

	local b = net.ReadFloat()

	if(b == nil) then
		if(LocalPlayer().OnDrone) then
			LocalPlayer().OnDrone = false
		else
			LocalPlayer().OnDrone = true
		end
	else
		if(b==1) then
			LocalPlayer().OnDrone = true
		else
			LocalPlayer().OnDrone = false
		end
	end
end)

net.Receive("DisableTag",function()

	local bool = net.ReadFloat()
	local ent = net.ReadEntity()

	if(bool == 1) then

		ent.OnTTT = true

	end

end)

function ENT:Draw()
	
	self:DrawModel()
	self:SetModelScale(0.75,0)

	dinamoWheel = dinamoWheel + 1

	self:SetPoseParameter("dynamo_wheel",dinamoWheel)

	if(dinamoWheel > 180) then dinamoWheel = -180 end

	local bone = self:LookupBone("Scanner.Neck")
	local pos,ang = self:GetBonePosition(bone)

	ply = self

	local offset = Vector( 0, 0, 20 )
	local ang = LocalPlayer():EyeAngles()
	local pos = ply:GetPos() + offset + ang:Up()
 
	ang:RotateAroundAxis( ang:Forward(), 90 )
	ang:RotateAroundAxis( ang:Right(), 90 )


end

//////////////////////////////////////
//	Just drawing the Playermodel 	//
//////////////////////////////////////

hook.Add("ShouldDrawLocalPlayer", "DrawnMyPlayerDrone",function()

    if LocalPlayer().OnDrone then
        for k,v in pairs(ents.FindByClass("sent_drone")) do

            if(v:GetOwner() == LocalPlayer() && LocalPlayer().OnDrone) then

                return true

            end

        end
    end

end)

surface.CreateFont( "RobotText", {
    font = "Fixedsys",
    size = ScrW()/32,
    weight = 1,
} )

surface.CreateFont( "RobotText2", {
    font = "Fixedsys",
    size = ScrW()/48,
    weight = 1,
} )
//////////////////////////////////////////////////////////////
//	Pain everything since the yellow screen to the letters 	//
//////////////////////////////////////////////////////////////

local function MESPCheck(v)
    if v:Alive()  && v ~= LocalPlayer() && LocalPlayer():Alive() then
    return true
    else
        return false
    end
end

local function coordinates( ent )
    local min, max = ent:OBBMins(), ent:OBBMaxs()
    local corners = {
        Vector( min.x, min.y, min.z ),
        Vector( min.x, min.y, max.z ),
        Vector( min.x, max.y, min.z ),
        Vector( min.x, max.y, max.z ),
        Vector( max.x, min.y, min.z ),
        Vector( max.x, min.y, max.z ),
        Vector( max.x, max.y, min.z ),
        Vector( max.x, max.y, max.z )
    }

    local minX, minY, maxX, maxY = ScrW() * 2, ScrH() * 2, 0, 0
    for _, corner in pairs( corners ) do
        local onScreen = ent:LocalToWorld( corner ):ToScreen()
        minX, minY = math.min( minX, onScreen.x ), math.min( minY, onScreen.y )
        maxX, maxY = math.max( maxX, onScreen.x ), math.max( maxY, onScreen.y )
    end

    return minX, minY, maxX, maxY
end

hook.Add("HUDPaint", "DrawDronevision",function()
    if LocalPlayer().OnDrone then
	for k,v in pairs(ents.FindByClass("sent_drone")) do

		if(v:GetOwner() == LocalPlayer() && LocalPlayer().OnDrone) then


        for k,v in pairs(player.GetAll()) do
            if(v ~= LocalPlayer() and MESPCheck(v)) then
                local Box1x,Box1y,Box2x,Box2y = coordinates(v)
                surface.SetDrawColor(team.GetColor(v:Team()))
                local Box1x,Box1y,Box2x,Box2y = coordinates(v)
                surface.SetDrawColor(Color(255,255,255,255))
                surface.DrawLine( Box1x, Box1y, math.min( Box1x + 20, Box2x ), Box1y )


                surface.DrawLine( Box1x, Box1y, Box1x, math.min( Box1y + 20, Box2y ) )


                surface.DrawLine( Box2x, Box1y, math.max( Box2x - 20, Box1x ), Box1y )


                surface.DrawLine( Box2x, Box1y, Box2x, math.min( Box1y + 20, Box2y ) )


                surface.DrawLine( Box1x, Box2y, math.min( Box1x + 20, Box2x ), Box2y )


                surface.DrawLine( Box1x, Box2y, Box1x, math.max( Box2y - 20, Box1y ) )


                surface.DrawLine( Box2x, Box2y, math.max( Box2x - 20, Box1x ), Box2y )


                surface.DrawLine( Box2x, Box2y, Box2x, math.max( Box2y - 20, Box1y ) )

                surface.SetDrawColor(Color(255,0,0,255))
                surface.DrawLine( Box1x+1, Box1y+1, math.min( Box1x + 20, Box2x )+1, Box1y+1 )
                surface.DrawLine( Box1x+1, Box1y+1, Box1x+1, math.min( Box1y + 20, Box2y )+1 )
                surface.DrawLine( Box2x-1, Box1y+1, math.max( Box2x - 20, Box1x )-1, Box1y+1 )
                surface.DrawLine( Box2x-1, Box1y+1, Box2x-1, math.min( Box1y + 20, Box2y )-1 )
                surface.DrawLine( Box1x-1, Box2y+1, math.min( Box1x + 20, Box2x )-1, Box2y+1 )
                surface.DrawLine( Box1x-1, Box2y+1, Box1x-1, math.max( Box2y - 20, Box1y )-1 )
                surface.DrawLine( Box2x+1, Box2y+1, math.max( Box2x - 20, Box1x )+1, Box2y+1 )
                surface.DrawLine( Box2x+1, Box2y+1, Box2x+1, math.max( Box2y - 20, Box1y )+1 )
            end
        end

			surface.SetDrawColor(Color(245,184,0,80))

			local maxValue = ScrW()/5 - ScrW()/128*1.75
			if(v.fuel == nil) then
				v.fuel = 100
			end
			if(v.flash == nil) then
				v.flash = 100
			end

			local dist = v:GetPos():Distance(LocalPlayer():GetPos())

			local signal = math.Round(math.Clamp(100-(dist/4500)*100,0,100))

			if(v.Anthena) then
				signal = math.Round(math.Clamp(100-(dist/9000)*100,0,100))
			end

			--surface.DrawRect(ScrW()/2-ScrW()/10 + ScrW()/128/1.05,ScrH()/24 + ScrH()/86,(v.fuel/100)*(maxValue),ScrH()/24 - ScrH()/64)

			--draw.SimpleText("FUEL: "..math.Round(v.fuel).."%","RobotText",ScrW()/2,ScrH()/8.5,Color(245,184,0,80),TEXT_ALIGN_CENTER,TEXT_ALIGN_CENTER)

			draw.SimpleText("FLASH: "..math.Round(v.flash).."%","RobotText",ScrW()/12,ScrH()/3,Color(245,184,0,80),TEXT_ALIGN_LEFT,TEXT_ALIGN_CENTER)
			draw.SimpleText("SIGNAL: "..signal.."%","RobotText",ScrW()/12,ScrH()/2.5,Color(245,184,0,80),TEXT_ALIGN_LEFT,TEXT_ALIGN_CENTER)
			draw.SimpleText("SPEED: "..math.Round(v:GetVelocity():Length()*(15/352)).."MP/H","RobotText",ScrW()/12,ScrH()/2.2,Color(245,184,0,80),TEXT_ALIGN_LEFT,TEXT_ALIGN_CENTER)

				draw.SimpleText("GUN: "..math.Round((v.bullets/30)*100).."%","RobotText",ScrW()/12,ScrH()/1.975,Color(245,184,0,80),TEXT_ALIGN_LEFT,TEXT_ALIGN_CENTER)
        draw.SimpleText("HP: "..(v.health or 100).."%","RobotText",ScrW()/12,ScrH()/1.7,Color(245,184,0,80),TEXT_ALIGN_LEFT,TEXT_ALIGN_CENTER)


        --local l = v:GetPos()
			--local ll = v:GetAngles()
			--draw.SimpleText("X"..tostring(math.Round(l.x)).." Y"..tostring(math.Round(l.y)).." Z"..tostring(math.Round(l.z)),"RobotText",ScrW()/2,ScrH()/1.2,Color(245,184,0,80),TEXT_ALIGN_CENTER,TEXT_ALIGN_CENTER)
			--draw.SimpleText("P"..tostring(math.Round(ll.p)).." Y"..tostring(math.Round(ll.y)).." R"..tostring(math.Round(ll.r)),"RobotText",ScrW()/2,ScrH()/1.125,Color(245,184,0,80),TEXT_ALIGN_CENTER,TEXT_ALIGN_CENTER)

			draw.SimpleText("Mode:"..v:GetNWString("shootmode"),"RobotText",ScrW()/1.075,ScrH()/3,Color(100,255,0,80),TEXT_ALIGN_RIGHT,TEXT_ALIGN_CENTER)



			--	draw.SimpleText("ANTHENA: ONLINE","RobotText",ScrW()/1.075,ScrH()/2.5,Color(100,255,0,80),TEXT_ALIGN_RIGHT,TEXT_ALIGN_CENTER)
			draw.SimpleText("E+R to change mode","RobotText2",ScrW()/1.075,ScrH()/2.5,Color(245,50,0,80),TEXT_ALIGN_RIGHT,TEXT_ALIGN_CENTER)


			--if(v.SpeedUpg == 2) then
			--	draw.SimpleText("BOOSTER: ONLINE","RobotText",ScrW()/1.075,ScrH()/2.2,Color(100,255,0,80),TEXT_ALIGN_RIGHT,TEXT_ALIGN_CENTER)

				draw.SimpleText("CTRL to exit","RobotText2",ScrW()/1.075,ScrH()/2.2,Color(245,50,0,80),TEXT_ALIGN_RIGHT,TEXT_ALIGN_CENTER)
                draw.SimpleText("ALT = 360 turn","RobotText2",ScrW()/1.075,ScrH()/2,Color(245,50,0,80),TEXT_ALIGN_RIGHT,TEXT_ALIGN_CENTER)


			surface.SetDrawColor(Color(245,184,0, 3 + 2*math.sin(RealTime()*5) + 25-(signal/100)*25))
			surface.DrawRect(0,0,ScrW(),ScrH())

			for k = 1, ScrH()/16 do

				surface.SetDrawColor(Color(245,184,0, 3 + 2*math.cos(RealTime()*5) ))
				surface.DrawRect(ScrW()/2-ScrW()/1.1/2,k*32,ScrW()/1.1,ScrH()/128)

			end

			for k = 1, math.random(100-signal,48+(100-signal)) do

				local tone = math.random(1,256)
				surface.SetDrawColor(Color(tone,tone,tone,150))
				surface.DrawRect(math.random(0,ScrW()),math.random(0,ScrH()),ScrH()/signal/2,ScrH()/signal/2)

			end

			if signal <= 1 then

				local tone = math.random(1,80)
				surface.SetDrawColor(Color(tone,tone,tone,250))
				surface.DrawRect(0,0,ScrW(),ScrH())

			end

			local tracedata = {}

			tracedata.start = v:GetPos()
			tracedata.endpos = v:GetPos() + ( v:GetForward() * 500 )
			tracedata.filter = {v}
			tracedata.mins = Vector( -15,-15,-15 )
			tracedata.maxs = Vector( 15,15,15 )
			tracedata.mask = MASK_SHOT_HULL
			local tr = util.TraceHull( tracedata )

			if(tr.Entity != nill && tr.Entity:IsValid() && tr.Entity:IsPlayer()) then

				draw.SimpleText("TARGET:","RobotText",ScrW()/2,ScrH()/1.6,Color(100,255,0,80),TEXT_ALIGN_CENTER,TEXT_ALIGN_CENTER)
				draw.SimpleText("<"..tr.Entity:Nick().."> "..tr.Entity:Health().." hp","RobotText",ScrW()/2,ScrH()/1.5,Color(100,255,0,80),TEXT_ALIGN_CENTER,TEXT_ALIGN_CENTER)

			end


		end

    end
    end

end)

//////////////////////////////////////////////////////////////////
//	We need to change the fov because we are an eye on a bot 	//
//////////////////////////////////////////////////////////////////

hook.Add("CalcView", "Drone",function(ply,pos,angle,fov) 
    if LocalPlayer().OnDrone then
	local view = {}

	for k,v in pairs(ents.FindByClass("sent_drone")) do

		if(ply.OnDrone == nil) then
			ply.OnDrone = false
		end

		if(v:GetOwner() == ply && ply.OnDrone) then

			local bone = v:LookupBone("Scanner.Neck")
			local pos,ang = v:GetBonePosition(bone)

			view.origin = pos + v:GetForward()*15
			view.angles = ang
			view.fov = 60

			return view

		end

    end
    end

end)

hook.Add("onPocketItemRemoved", "FixReDrone",function(ply,item)

	if(item:GetClass() == "sent_drone") then

		item.Owner = ply

	end

end)

//////////////////////////////////////////
//	Here we handle the blind overlay 	//
//////////////////////////////////////////

local alp = 0
local dba = false

net.Receive("SetBlind",function()

	local short = net.ReadFloat()
	alp = 0
	dba = false

	hook.Add("HUDPaint", "DrawBlindness",function()

		surface.SetDrawColor(255,255,255,alp)
		surface.DrawRect(0,0,ScrW(),ScrH())

		if(dba == false) then
			alp = Lerp(FrameTime()*10*short,alp,1500)
			if(alp > 1100) then
				dba = true
			end
		else
			alp = Lerp(FrameTime()*2*short,alp,-10)
			if(alp < 0) then
				hook.Remove("HUDPaint","DrawBlindness")
			end
		end
	end)
end)

//////////////////////////////////////////////////////////////////////////////
//	We must create the client model and sync the server with the client 	//
//////////////////////////////////////////////////////////////////////////////

net.Receive("AnthenaUpgrade",function()

	local ent = net.ReadEntity()

	if(ent != nil && ent:IsValid()) then
		ent.AnthUpg = true
		local fuel = ClientsideModel("models/props_c17/TrapPropeller_Lever.mdl")
		fuel:SetPos(ent:GetPos() + ent:GetUp()*12 + ent:GetRight()*-3)
		fuel:SetParent(ent)
		fuel:SetLocalAngles(Angle(180,0,90))
		fuel:SetModelScale(0.001,0)
		fuel.IsGhost = true
		fuel:Spawn()

		ent.Anthena = fuel
	end

end)

net.Receive("FuelUpgrade",function()

	local ent = net.ReadEntity()

	if(ent != nil && ent:IsValid()) then

		local fuel = ClientsideModel("models/props_junk/PropaneCanister001a.mdl")
		fuel:SetPos(ent:GetPos() + ent:GetRight()*7 + ent:GetUp()*2)
		fuel:SetParent(ent)
		fuel:SetLocalAngles(Angle(0,90,0))
		fuel:SetModelScale(0.001,0)
		fuel.IsGhost = true
		fuel:Spawn()

		ent.fuelTank = fuel
		ent.FuelUpg = true

	end

end)

net.Receive("SpeedUpgrade",function()

	local ent = net.ReadEntity()

	if(ent != nil && ent:IsValid()) then
	
		ent.SpeedUpg = 2
		local fuel = ClientsideModel("models/maxofs2d/thruster_propeller.mdl")
		fuel:SetPos(ent:GetPos() + ent:GetUp()*-0 + ent:GetForward()*1)
		fuel:SetParent(ent)
		fuel:SetLocalAngles(Angle(-90,0,0))
		fuel:SetModelScale(0.001,0)
		fuel.IsGhost = true
		fuel:Spawn()

		ent.speedThank = fuel

	end


end)

net.Receive("GunUpgrade",function()

	local ent = net.ReadEntity()

	if(ent != nil && ent:IsValid()) then

		ent.gunUpg = true
		local fuel = ClientsideModel("models/gibs/gunship_gibs_nosegun.mdl")
		fuel:SetPos(ent:GetPos() + ent:GetUp()*-5 + ent:GetForward()*8 + ent:GetRight()*-2)
		fuel:SetParent(ent)
		fuel:SetLocalAngles(Angle(0,0,0))
		fuel:SetModelScale(0.25,0)
		fuel.IsGhost = true
		fuel:Spawn()

		ent.gunModel = fuel

	end

end)

net.Receive("FlashUpgrade",function()

	local ent = net.ReadEntity()

	if(ent != nil && ent:IsValid()) and SERVER then

		ent.flashUpg = true
		--local fuel = ClientsideModel("models/food/burger.mdl")
    local fuel = ents.Create("prop_dynamic")
         fuel:SetModel("models/food/burger.mdl")
		fuel:SetPos(ent:GetPos() + ent:GetUp()*-2 + ent:GetForward()*2 + ent:GetRight()*-0)
		fuel:SetParent(ent)
		fuel:SetLocalAngles(Angle(0,90,00))
		fuel:SetModelScale(2,0)
		--fuel.IsGhost = true
		fuel:Spawn()

		ent.flashModel = fuel

	end

end)

net.Receive("RefuelDrone",function()

	local ent = net.ReadEntity()

	if(ent != nil && ent:IsValid()) then
		
		ent.fuel = 100
		
	end

end)

//////////////////////////////////////////////
//	Remove CModels only if we have them 	//
//////////////////////////////////////////////

net.Receive("RemoveCModels",function()

	local ent = net.ReadEntity()

	if(ent != nil && ent:IsValid()) then

	if(ent.fuelTank != nil) then
		ent.fuelTank:Remove()
	end
	if(ent.Anthena != nil) then
		ent.Anthena:Remove()
	end
	if(ent.speedThank != nil) then
		ent.speedThank:Remove()
	end
	if(ent.gunModel != nil) then
		ent.gunModel:Remove()
	end
	if(ent.flashModel != nil) then
		ent.flashModel:Remove()
	end

	for k,v in pairs(ents.GetAll()) do

		if(v.IsGhost) then

			v:Remove()

		end

	end

	end

end)

end

if SERVER then

util.AddNetworkString("SetBlind")
util.AddNetworkString("FuelUpgrade")
util.AddNetworkString("RemoveCModels")
util.AddNetworkString("AnthenaUpgrade")
util.AddNetworkString("SpeedUpgrade")
util.AddNetworkString("GunUpgrade")
util.AddNetworkString("FlashUpgrade")
util.AddNetworkString("DisableTag")
util.AddNetworkString("ActivateDrone")

//////////////////////////////////////
//	Let's destroy it when we die 	//
//////////////////////////////////////

hook.Add("PlayerDeath","RemoveDrone",function(ply,ent)

	for k,v in pairs(ents.FindByClass("sent_drone")) do

		if(v:GetOwner() == ply) then

			v:Explode()
			v:Destroy()
			v:Remove()
			ply.OnDrone = false

		end

	end

end)

//////////////////////////////////////////////////////////////////
//	And let's create a beauty explosion when it's destroyed 	//
//////////////////////////////////////////////////////////////////

function ENT:Explode()

	local ent = ents.Create("env_explosion")

	ent:SetPos(self:GetPos())
	ent:Spawn()
	ent:Fire("explode")

end


end

function gravDrop(ply,ent)
	if(ent:GetClass() == "sent_drone") then
		return true
	end
end
 
hook.Add( "GravGunOnDropped", "firyLaunch", gravDrop)

hook.Add("GravGunPickupAllowed", "NoDronge", function(ply,ent)

	if(ent:GetClass() == "sent_drone") then

		return true

	end

end)

//////////////////////////////////////////////
//	Check if we're looking at something 	//
//////////////////////////////////////////////

function IsLookingAt( ply, targetVec )

	local cv = targetVec - ply:GetPos()
	local dc = cv:Normalize()

	return ply:GetAimVector():DotProduct( cv ) < 10
end

//////////////////////////////////////////////////
//	Check if something is inside of the screen 	//
//////////////////////////////////////////////////

function checkInside(ply)

	local ents = { };

		for _, v in pairs( player.GetAll() ) do
	
			if( ( v:GetPos() - ply:GetPos() ):GetNormal():DotProduct( ply:GetAimVector() ) < 1 ) then
		
				local trace = { };
				trace.start = ply:EyePos();
				trace.endpos = v:EyePos();
				trace.filter = ply;
				local tr = util.TraceLine( trace );

					if( tr.Entity and tr.Entity:IsValid() and tr.Entity == v ) then
			
						table.insert( ents, v );

			
					end
			

			end

		end

	return ents;

end

