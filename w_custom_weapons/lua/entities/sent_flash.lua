
AddCSLuaFile()

DEFINE_BASECLASS( "base_anim" )

ENT.PrintName		= "Flash Upgrade"
ENT.Author			= "Gonzalolog"
ENT.Information		= "Partner"
ENT.Category		= "Gonzalolog"

ENT.Editable			= false
ENT.Spawnable			= false
ENT.AdminOnly			= false
ENT.RenderGroup 		= RENDERGROUP_TRANSLUCENT
ENT.AutomaticFrameAdvance = true 

function ENT:SpawnFunction( ply, tr, ClassName )

	if ( !tr.Hit ) then return end
	
	local size = math.random( 16, 48 )
	local SpawnPos = tr.HitPos + tr.HitNormal * size
	
	local ent = ents.Create( ClassName )
		ent:SetPos( SpawnPos )
	
	ent:Spawn()
	ent:Activate()

	return ent
	
end

function ENT:Initialize()

	if ( SERVER ) then

	
		-- Use the helibomb model just for the shadow (because it's about the same size)
		self:SetModel( "models/props_wasteland/light_spotlight01_lamp.mdl" )
		
		self:PhysicsInit(SOLID_VPHYSICS )
    	self:SetMoveType(MOVETYPE_VPHYSICS)
    	self:SetSolid(SOLID_VPHYSICS)

		local phys = self:GetPhysicsObject()
		if (phys:IsValid()) then

			phys:Wake()			
			phys:SetMass(0)

		end


	end
	
end

function ENT:Draw()

	self:DrawModel()
	self:SetModelScale(0.5,0)

	ply = self

	local offset = Vector( 0, 0, 20 )
	local ang = LocalPlayer():EyeAngles()
	local pos = ply:GetPos() + offset + ang:Up()
 
	ang:RotateAroundAxis( ang:Forward(), 90 )
	ang:RotateAroundAxis( ang:Right(), 90 )

	cam.Start3D2D( pos, Angle( 0, ang.y, 90 ), 0.25 )

		surface.SetFont("CV20")
		
		local clr = Color(255,204,51)
		local dwr = "Flash Upgrade"

		local w,h = surface.GetTextSize(dwr)
		
		surface.SetDrawColor(clr.r,clr.g,clr.b,20)
		surface.DrawRect(0-w/2,0,w+2,h)

		draw.DrawText( dwr, "CV20", 2, 2, clr, TEXT_ALIGN_CENTER )
		
	cam.End3D2D()


end

function ENT:PhysicsCollide( colData, collider )

	if(colData.HitEntity:GetClass() == "sent_drone") then

		if(colData.HitEntity.flashUpg == false) then

			local ent = colData.HitEntity

			if(ent.Owner == self.Owner  || ent.UpgradeShare) then
				self:EmitSound("buttons/combine_button3.wav")
				colData.HitEntity:AddFlash()
				self:Remove()
			end
		end

	end

end