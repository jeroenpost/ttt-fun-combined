
AddCSLuaFile()

DEFINE_BASECLASS( "base_anim" )

ENT.PrintName		= "Nikola"
ENT.Author			= "Gonzalolog"
ENT.Information		= "Partner"
ENT.Category		= "Other"

ENT.Editable			= false
ENT.Spawnable			= true
ENT.AdminOnly			= true
ENT.RenderGroup 		= RENDERGROUP_TRANSLUCENT
ENT.AutomaticFrameAdvance = true 

//////////////
//	Config 	/////////////////////////
//////////////

ENT.SellerName = "Nikola"
ENT.BuyerGroups = {2,3,8}

local prices = {
500, 				//Drone price
50,					//Flash price
30,					//Signal price
75,					//Fuel price
50,					//Gun price
30, 				//Speed price
20 					//Refill price
}

///////////////////////////////////

if SERVER then

	util.AddNetworkString("CreateDroneStore")
	util.AddNetworkString("ReturnDroneStore")
	util.AddNetworkString("ReReturnBuy")
	util.AddNetworkString("RefuelDrone")

end

function ENT:SpawnFunction( ply, tr, ClassName )

	if ( !tr.Hit ) then return end
	
	local SpawnPos = tr.HitPos + tr.HitNormal
	
	local ent = ents.Create( ClassName )
		ent:SetPos( SpawnPos )
	
	ent:Spawn()
	ent:SetAngles(Angle(0,ply:GetAngles().y-180,0))
	ent:Activate()

	ent:SetPersistent(true)

	return ent
	
end

function ENT:Initialize()

	if ( SERVER ) then

		self:SetModel( "models/kleiner.mdl" )
		
		self:PhysicsInit(SOLID_VPHYSICS )
    	self:SetMoveType(MOVETYPE_NONE)
    	self:SetSolid(SOLID_VPHYSICS)

    	self:Activate()

    	local l = #ents.FindByClass("sent_kleiner_store")

    	if(l == 1) then

    		for k,v in pairs(player.GetAll()) do

    			ply.Buying = false
    			ply.OnDrone = false

    		end

    	end

	end

end

function ENT:OnRemove()

end

CreateConVar("gzg_drone_debug",0)

function ENT:Draw()

	if(GetConVarNumber("gzg_drone_debug") == 1) then
		MsgN("Your team ID is: ",self:GetOwner():Team())
	end

	self:DrawModel()

	local sec = self:LookupSequence("LineIdle01")
	self:SetSequence(sec)
	
	local flex = self:GetFlexNum("eyes_rightleft")
	self:SetFlexWeight(50,1)

	ply = self

	local offset = Vector( 0, 0, 85 )
	local ang = LocalPlayer():EyeAngles()
	local pos = ply:GetPos() + offset + ang:Up()
 
	ang:RotateAroundAxis( ang:Forward(), 90 )
	ang:RotateAroundAxis( ang:Right(), 90 )

	cam.Start3D2D( pos, Angle( 0, ang.y, 90 ), 0.5 )

		render.PushFilterMin( 2 );
		render.PushFilterMag( 2 );

		surface.SetFont("CV20")
		
		local clr = Color(102,50,120)
		local dwr = self.SellerName

		local w,h = surface.GetTextSize(dwr)
		
		surface.SetDrawColor(clr.r,clr.g,clr.b,20)
		surface.DrawRect(0-w/2,0,w+2,h)

		draw.DrawText( dwr, "CV20", 1, 1, Color(clr.r+25,clr.g+25,clr.b+25), TEXT_ALIGN_CENTER )
		draw.DrawText( dwr, "CV20", 2, 2, clr, TEXT_ALIGN_CENTER )

		render.PopFilterMin();
		render.PopFilterMag();
		
	cam.End3D2D()

end

function ENT:Use(ply)


	if(ply.Buying != true && (table.HasValue(self.BuyerGroups,ply:Team()) || gmod.GetGamemode().Name == "Sandbox") && ply.OnDrone == false) then

		ply.Buying = true

		net.Start("CreateDroneStore")
		net.WriteEntity(self)
		net.Send(ply)

	end

end

local items = {
"sent_drone",
"sent_flash",
"sent_anthena",
"sent_fuel",
"sent_gun",
"sent_truster",
"sent_refuel",
}


net.Receive("ReturnDroneStore",function(len,ply)

	ply.Buying = false
	
	local cart = net.ReadTable()

	local money = 0

	for k,v in pairs(cart) do

		money = money + prices[v]

	end

	if(gmod.GetGamemode().Name == "Sandbox" || money < ply:getDarkRPVar("money")) then

		if(gmod.GetGamemode().Name != "Sandbox") then
			ply:addMoney(-money)
		end
		
		ply.tempDrone = nil

		local iterate = false

		if(table.HasValue(cart,1)) then

			local dick = ents.Create("sent_drone")
			dick:SetPos(ply:GetPos() + ply:GetForward()*32 + Vector(0,0,math.Clamp(48*1,48,150)))
			dick:Spawn()
			dick.Owner = ply
			ply.tempDrone = dick
			iterate = true

		else

			iterate = false

		end

		for k,v in pairs(cart) do

			if(items[v] == "sent_refuel" || iterate == false) then
			
				local dick = ents.Create(items[v])
				dick:SetPos(ply:GetPos() + ply:GetForward()*32 + Vector(0,0,math.Clamp(48*k,48,150)))
				dick:Spawn()
				dick.Owner = ply

			elseif(ply.tempDrone != nil && ply.tempDrone:IsValid()) then

				timer.Simple(0.5,function() 

				if(items[v] == "sent_gun") then
					ply.tempDrone:AddGun(true)
				end
				if(items[v] == "sent_flash") then
					ply.tempDrone:AddFlash(true)
				end
				if(items[v] == "sent_fuel") then
					ply.tempDrone:AddFuel(true)
				end
				if(items[v] == "sent_truster") then
					ply.tempDrone:AddSpeed(true)
				end
				if(items[v] == "sent_anthena") then
					ply.tempDrone:AddAnthena(true)
				end

				end)

			end
		end

	end

	ply.Buying = false
	

end)

if CLIENT then

local pS = {
"models/Combine_Scanner.mdl",
"models/props_wasteland/light_spotlight01_lamp.mdl",
"models/props_c17/TrapPropeller_Lever.mdl",
"models/props_junk/PropaneCanister001a.mdl",
"models/gibs/gunship_gibs_nosegun.mdl",
"models/maxofs2d/thruster_propeller.mdl",
"models/props_junk/gascan001a.mdl"
}

local dS = {
"Drone",
"Flash Upgrade",
"Signal Upgrade",
"Fuel Upgrade",
"Gun Upgrade",
"Boost upgrade",
"Refill tank"
}

local desc = {
"Just the \ndrone base",
"Upgrade the \nflash, so you\n can flash in radius",
"It let's you\nto fly far away",
"Your fuel\nconsumption will be\n lesser",
"Shoot bullets",
"You will travel\nmore faster",
"Use it for refill\nyour tank"
}



local Cart = {}
local seller = nil
local pnl = nil

net.Receive("CreateDroneStore",function()

	Cart = {}

	if(pnl != nil) then pnl:Remove() end

	pnl = vgui.Create("DFrame")
	pnl:SetSize(250,360)
	pnl:SetPos(ScrW()/2,ScrH()/2)
	pnl:Center()
	pnl:ShowCloseButton(false)
	pnl:MakePopup()
	pnl:SetTitle("Drone market")

	local bk = vgui.Create("DListView",pnl)
	bk:SetPos( 85, 35 )
	bk:SetSize( 150, 270 )
	bk:AddColumn("In-Cart")
	bk:AddColumn("Price")
	bk.OnClickLine = function(parent, line, isselected)
		local d = line:GetValue(1)
		local c = concernValue(d)
		table.RemoveByValue(Cart,c)
		bk:RemoveLine(line:GetID())
	end
 

	for k,v in pairs(pS) do

		local but = vgui.Create("SpawnIcon",pnl)
		but:SetSize(44,44)
		but:SetPos(24,44*k+5-10)
		but:SetModel(v)
		but:SetToolTip(dS[k].." - $"..tostring(prices[k]).."\n"..desc[k])
		but.DoClick = function( button )
			if(!table.HasValue(Cart,k)) then
				table.insert(Cart,k)
				bk:AddLine(dS[k],"$"..tostring(prices[k]))
			end
		end

	end

	local btn = vgui.Create( "DButton", pnl )
	btn:SetSize( 150, 32 )
	btn:SetPos( 85, 360-42 )
	btn:SetText( "Buy/Cancel" )
	btn.DoClick = function( button )
		closeEverything(Cart)
		pnl:Remove()
	end

end)

function closeEverything(Cart)

	net.Start("ReturnDroneStore")
	net.WriteTable(Cart)
	net.SendToServer()

end

function concernValue(d)
	for k,v in pairs(dS) do
		if(d == v) then
			return k
		end
	end
end

end