-- The Glaive
-- By Magenta & Marl

-- Please get out of here :c

AddCSLuaFile("cl_init.lua") AddCSLuaFile("shared.lua") include("shared.lua") ENT.SizeSide = 2 ENT.SizeUp = 1 ENT.Trail_Size1 = 10 ENT.Trail_Size2 = 1 function ENT:Initialize()
    self.Trail = util.SpriteTrail(self, 0, Color(255, 10, 0, 200), false, 0, self.Trail_Size1, self.Trail_Size2, 1 / (self.Trail_Size1 + self.Trail_Size2) * 0.5, "trails/laser.vmt")
    self:SetModel("models/glaive/world/glaive.mdl")
    self:SetRenderMode(RENDERMODE_TRANSALPHA)
    self:SetMoveType(MOVETYPE_VPHYSICS)
    self:SetSolid(SOLID_VPHYSICS)
    self:PhysicsInit(SOLID_VPHYSICS)
    self:DrawShadow(true)
    self.ReturnToOwner = CurTime() + 4

    local Collision1 = Vector(self.SizeSide * -1, self.SizeSide * -1, self.SizeUp * -1)
    local Collision2 = Vector(self.SizeSide, self.SizeSide, self.SizeUp)

    self:SetCollisionGroup(COLLISION_GROUP_INTERACTIVE)

    self.SpinningSound = CreateSound(self, "Glaive.Spinning")
    self.SpinningSound:Play()

    self.OldVelocity = self:GetForward() * 1500
    self:Think()
    self:NextThink(CurTime())
    self.GetVel = CurTime() + 0.001
    self.StuckCheck = CurTime() + 0.05

    local PHYS = self:GetPhysicsObject()

    if IsValid(PHYS) then
        PHYS:Wake()
        PHYS:EnableGravity(false)
    end
end

function ENT:CheckForKilling()
    local OWNER = self:GetOwner()
    local POS = OWNER:EyePos() + OWNER:EyeAngles():Forward() * 20 - OWNER:EyeAngles():Up() * 2
    local POS2 = self:GetPos()
    local DIR = (POS - POS2):GetNormal()

    local TR = {}
    TR.start = self:GetPos()
    TR.endpos = self:GetPos() + DIR * 75
    TR.mask = MASK_PLAYERSOLID
    TR.filter = { self, self:GetOwner() }
    TR.mins = Vector(-0.5, -0.5, -0.5)
    TR.maxs = Vector(0.5, 0.5, 0.5)
    local Trace = util.TraceHull(TR)

    if Trace.Hit and (Trace.Entity:IsPlayer() or Trace.Entity:IsNPC()) then
        local ent = Trace.Entity
        self:BloodEffect(ent)
        ent:EmitSound("Glaive.ThrownHit", 75, 100, 1)
        ent:EmitSound("Glaive.ThrownHit", 75, 100, 1)
        local DMG = DamageInfo()
        DMG:SetAttacker(self:GetOwner())
        DMG:SetInflictor(self)
        DMG:SetDamage(85)
        DMG:SetDamageType(DMG_SLASH)
        DMG:SetDamagePosition(self:GetPos())
        DMG:SetDamageForce(DIR * 100000)
        if ent.hasShieldPotato then DMG:SetDamage(25) end ent:TakeDamageInfo(DMG)
    end
end

function ENT:Think()

    if not IsValid(self:GetOwner()) then self:Remove() return end

    local CT = CurTime()

    if IsValid(self:GetOwner()) then

        local OWNER = self:GetOwner()

        if not OWNER:Alive() or not IsValid(OWNER:GetActiveWeapon()) or not OWNER:GetActiveWeapon():GetClass() == "custom_polo_glaive" then self:Remove() end

        local RETURN = self.ReturnToOwner

        if RETURN and CT >= RETURN then
            local POS = OWNER:EyePos() + OWNER:EyeAngles():Forward() * 20 - OWNER:EyeAngles():Up() * 2
            local POS2 = self:GetPos()
            local DIR = (POS - POS2):GetNormal()
            local DISTANCE = (POS:Distance(POS2))
            local SPEED = GlaiveSpeed() * 0.0022
            self:SetPos(POS2 + DIR * SPEED)
            self:CheckForKilling()
            if DISTANCE <= 240 and not self.Caught then
                self.Caught = true
                OWNER:GetActiveWeapon():GlaiveCatch()
                self:Remove()
            end
        end
    end

    if self.LastPos and self:GetPos() == self.LastPos and self.StuckCheck and CT >= self.StuckCheck then
        self.StuckCheck = CurTime() + 0.05
        self:SetLocalVelocity(VectorRand() * 500)
    end

    if not self.SparkMe then self.SparkMe = CT end

    if self.SparkMe and CT >= self.SparkMe then
        self.SparkMe = CT + 0.1
        local effectdata = EffectData()
        effectdata:SetOrigin(self:GetPos())
        effectdata:SetEntity(self)
        effectdata:SetStart(self:GetPos())
        effectdata:SetNormal(VectorRand() * 1)
        effectdata:SetAngles(Angle(math.Rand(-180, 180), math.Rand(-180, 180), math.Rand(-180, 180)))
        effectdata:SetScale(1)
        effectdata:SetRadius(1)
        effectdata:SetMagnitude(0.2)
        util.Effect("Sparks", effectdata)
    end

    self.LastPos = self:GetPos()

    self:NextThink(CT)

    return true
end

function ENT:OnRemove()
    self.SpinningSound:Stop()
end

local BLOODCOL = {} BLOODCOL[0] = "blood_impact_red_01" BLOODCOL[1] = "blood_impact_yellow_01" BLOODCOL[2] = "blood_impact_green_01" BLOODCOL[3] = "blood_impact_synth_01" BLOODCOL[4] = "blood_impact_yellow_01" BLOODCOL[5] = "blood_impact_green_01" BLOODCOL[6] = "blood_impact_green_01" function ENT:GetBloodColorParticle(ent)
    if IsValid(ent) and ent.GetBloodColor then

        local BLOOD = ent:GetBloodColor()

        if ent:IsPlayer() and BLOOD == -1 then
            return "blood_impact_red_01"
        end

        if BLOODCOL[BLOOD] then
            return BLOODCOL[BLOOD]
        else
            return "blood_impact_synth_01"
        end
    end
end

function ENT:BloodEffect(enemy)
    if enemy:GetBloodColor() == -1 then return end
    for i = 1, math.random(1, 9) do
        local Splatter = ents.Create("info_particle_system")
        Splatter:SetPos(self:GetPos() + VectorRand() * 15)
        Splatter:SetAngles(Angle(0, 0, 0))
        local BloodParticle = self:GetBloodColorParticle(enemy)
        Splatter:SetKeyValue("effect_name", BloodParticle)
        Splatter:SetKeyValue("start_active", "1")
        Splatter:Spawn()
        Splatter:Activate()
        Splatter:Fire("Kill", nil, 1)
    end
end

function ENT:PhysicsCollide(data, physobj)
    local CT = CurTime()

    local LastSpeed = math.max(data.OurOldVelocity:Length(), data.Speed)
    local NewVelocity = physobj:GetVelocity()
    NewVelocity:Normalize()

    LastSpeed = math.max(NewVelocity:Length(), LastSpeed * 1.05)

    local TargetVelocity = NewVelocity * LastSpeed
    local OldVelocity = data.OurOldVelocity

    local ent = data.HitEntity

    if not ent:IsPlayer() and not ent:IsNPC() and data.DeltaTime > 0.2 then
        sound.Play("Glaive.Ricochet", self:GetPos(), 100, 100)
    end

    local PHYS = ent:GetPhysicsObject()

    if IsValid(PHYS) then
        PHYS:ApplyForceCenter(self.OldVelocity)
    end

    local effectdata = EffectData()
    effectdata:SetOrigin(data.HitPos)
    effectdata:SetEntity(self)
    effectdata:SetStart(data.HitPos)
    effectdata:SetNormal(data.HitNormal)
    effectdata:SetAngles(data.HitNormal:Angle())
    effectdata:SetScale(1)
    effectdata:SetRadius(1)
    effectdata:SetMagnitude(1)
    util.Effect("Sparks", effectdata)

    if ent:IsPlayer() or ent:IsNPC() then
        self:BloodEffect(ent)
        ent:EmitSound("Glaive.ThrownHit", 75, 100, 1)
        ent:EmitSound("Glaive.ThrownHit", 75, 100, 1)
        local DMG = DamageInfo()
        DMG:SetAttacker(self:GetOwner())
        DMG:SetInflictor(self)
        DMG:SetDamage(180)
        DMG:SetDamageType(DMG_SLASH)
        DMG:SetDamagePosition(self:GetPos())
        DMG:SetDamageForce(OldVelocity * 50000)
        if ent.hasShieldPotato then DMG:SetDamage(25) end ent:TakeDamageInfo(DMG)
        physobj:SetVelocity(OldVelocity)
    else
        local DMG = DamageInfo()
        DMG:SetDamage(25)
        DMG:SetDamageType(DMG_SLASH)
        ent:TakeDamageInfo(DMG)
        physobj:SetVelocity(TargetVelocity)
    end
end