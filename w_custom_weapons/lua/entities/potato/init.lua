AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")

include('shared.lua')

function ENT:Initialize()
	self.DieTime = CurTime() + 8

	local pl = self.Entity:GetOwner()
	local aimvec = pl:GetAimVector()
	self.Entity.Team = pl:Team()
	--self.Entity:SetPos(pl:GetShootPos() + pl:GetAimVector() * 30)
	--self.Entity:SetAngles(pl:GetAimVector():Angle()* math.random(1,45))
	self.Entity:SetModel("models/nemo/potato.mdl")
	self.Entity:PhysicsInit(SOLID_VPHYSICS)
	--self.Entity:SetCollisionGroup(COLLISION_GROUP_WORLD)
        if !self.Welded then
            local phys = self.Entity:GetPhysicsObject()
            if phys:IsValid() then
                    phys:SetMass(200) 
            end
        end 
	--self.Touched = {}
	self.OriginalAngles = self.Entity:GetAngles()
end

function ENT:Think()
	--if CurTime() > self.DieTime then
	--	self.Entity:Remove()
	--end
end

function ENT:PhysicsCollide(data, obj)
   -- self:EmitSound( "Rubber.BulletImpact" )
    if  data.HitEntity:IsPlayer( ) then
        obj:SetVelocity( obj:GetVelocity() * 0 )



        obj:EnableMotion(false)
        self:SetSolid(SOLID_NONE)
        self:SetMoveType(MOVETYPE_NONE)
        self:SetParent(data.HitEntity)
    end

end
function ENT:Touch(ent)
    if IsValid(ent) and  ent:IsPlayer( ) then
        if  ent != self:GetOwner()  then

        self:SetSolid(SOLID_NONE)
        self:SetMoveType(MOVETYPE_NONE)
        self:SetParent(ent)
        local move = ent:GetMoveType();

        local phys = ent:GetPhysicsObject()
        if phys and phys:IsValid() then
            phys:EnableMotion(false) -- Freezes the object in place.
            timer.Simple(0.01,function()
                phys:EnableMotion(true)
            end)
        end



        if !self.Splodetimer then
        self.Splodetimer = CurTime() + 2
        end
        self.Stuck = true
        self.Hit = true
        end
    end
end




function ENT:Explode()
	local ent = ents.Create( "env_explosion" )    
	ent:SetPos( self.Entity:GetPos()  )
	ent:SetOwner( self:GetOwner()  )
	ent:SetPhysicsAttacker(  self:GetOwner() )
	ent:Spawn()
	ent:SetKeyValue( "iMagnitude", "50" )
	ent:Fire( "Explode", 0, 0 )
	
	util.BlastDamage( self, self:GetOwner(), self:GetPos(), 50, 50 )
	
	ent:EmitSound( "weapons/big_explosion.mp3" )

	self:Remove()	
end
