AddCSLuaFile()

SWEP.PrintName = "Tranquilizer M9"

SWEP.Spawnable = true
SWEP.AdminOnly = false

SWEP.ViewModel = "models/weapons/c_m9.mdl"
SWEP.WorldModel = "models/weapons/w_m9.mdl"
SWEP.UseHands = true
SWEP.ViewModelFOV = 50
SWEP.ViewModelFlip = false

SWEP.Slot = 1
SWEP.SlotPos = 3

SWEP.DrawWeaponInfoBox = false
SWEP.Base = "aa_base"
SWEP.Primary.ClipSize = 500
SWEP.Primary.DefaultClip= 500
SWEP.Primary.Automatic = false
SWEP.Primary.Ammo = "357"

SWEP.Secondary.ClipSize	= -1
SWEP.Secondary.DefaultClip = -1
SWEP.Secondary.Automatic = false
SWEP.Secondary.Ammo	= "none"

function SWEP:ShootBullet( damage, num, aimcone )
	local bullet = {}

	bullet.Num 	= num
	bullet.Src 	= self.Owner:GetShootPos()
	bullet.Dir 	= self.Owner:GetAimVector()
	bullet.Spread 	= Vector( aimcone, aimcone, 0 )
	bullet.Tracer	= 5
	bullet.Force	= 1
	bullet.Damage	= damage
	bullet.AmmoType = "Pistol"
	

	self.Owner:FireBullets( bullet )
	
	self:ShootEffects()
end

function SWEP:PrimaryAttack()
	if ( self:CanPrimaryAttack() ) then
		self:SetNextPrimaryFire( CurTime() + 0.5 )
		
		self:ShootBullet( 75, 1, 0 )
		self:EmitSound( "weapons/usp/usp1.wav" )
		
		self:TakePrimaryAmmo( 1 )
	end
end

function SWEP:SecondaryAttack()
    self:Fly()
end

function SWEP:Reload()
	if ( self:DefaultReload( ACT_VM_RELOAD ) ) then
		self:EmitSound( "weapons/pistol/pistol_reload1.wav" )
	end
end
