
SWEP.HoldType = "ar2"
SWEP.Slot = 12
SWEP.PrintName = "Megapulsator"

if CLIENT then

    SWEP.Author = "GreenBlack"

    SWEP.Icon = "vgui/ttt_fun_killicons/famas.png"
    SWEP.ViewModelFlip = false
end

SWEP.Base = "gb_camo_base"
SWEP.Kind = 13
SWEP.Camo = 7

SWEP.Primary.Damage = 8
SWEP.Primary.Delay = 0.06
SWEP.Primary.Cone = 0.03
SWEP.Primary.ClipSize = 65
SWEP.Primary.ClipMax = 500
SWEP.Primary.DefaultClip = 65
SWEP.Primary.Automatic = true
SWEP.Primary.Ammo = "smg1"
SWEP.Primary.Recoil = 0.8
SWEP.Primary.Sound = Sound( "weapons/famas/famas-1.wav" )
SWEP.AutoSpawnable = false
SWEP.AmmoEnt = "item_ammo_smg1_ttt"
SWEP.UseHands = true
SWEP.ViewModel = "models/weapons/cstrike/c_rif_famas.mdl"
SWEP.WorldModel = "models/weapons/w_rif_famas.mdl"

SWEP.IronSightsPos = Vector( -4.65, -3.28, 0.01 )
SWEP.IronSightsAng = Vector( 1.09, 0, -2.19 )

function SWEP:GetHeadshotMultiplier(victim, dmginfo)
    local att = dmginfo:GetAttacker()
    if not IsValid(att) then return 2 end

    local dist = victim:GetPos():Distance(att:GetPos())
    local d = math.max(0, dist - 150)

    return 1.5 + math.max(0, (1.5 - 0.002 * (d ^ 1.25)))
end

SWEP.flyammo = 5
SWEP.nextFly = 0
function SWEP:SecondaryAttack()
    self:Fly()
end

function SWEP:PrimaryAttack(worldsnd)
    self.Weapon:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
    if not self:CanPrimaryAttack() then return end
    if not worldsnd then
        self.Weapon:EmitSound( self.Primary.Sound, self.Primary.SoundLevel )
    elseif SERVER then
        WorldSound(self.Primary.Sound, self:GetPos(), self.Primary.SoundLevel)
    end
    self:ShootBullet( self.Primary.Damage, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )

    if ( self.Owner:IsValid() ) then
        local tr = self.Owner:GetEyeTrace()
        local effectdata = EffectData()
        effectdata:SetOrigin(tr.HitPos)
        effectdata:SetNormal(tr.HitNormal)
        effectdata:SetScale(1)
        -- util.Effect("effect_mad_ignition", effectdata)
        util.Decal("FadingScorch", tr.HitPos + tr.HitNormal, tr.HitPos - tr.HitNormal)

        if SERVER then
            ply = tr.Entity
            if IsValid(ply) and ply:IsPlayer() then
                ply:Ignite( 2, 10)
            end
        end

        if math.Rand(0,3) == "2" then
            local tracedata = {}
            tracedata.start = tr.HitPos
            tracedata.endpos = Vector(tr.HitPos.x, tr.HitPos.y, tr.HitPos.z - 10)
            tracedata.filter = tr.HitPos
            local tracedata = util.TraceLine(tracedata)
            if SERVER then
                if tracedata.HitWorld and math.Rand(0,5) == 3 then
                    local flame = ents.Create("env_fire");
                    flame:SetPos(tr.HitPos + Vector( 0, 0, 1 ));
                    flame:SetKeyValue("firesize", "10");
                    flame:SetKeyValue("fireattack", "10");
                    flame:SetKeyValue("StartDisabled", "0");
                    flame:SetKeyValue("health", "10");
                    flame:SetKeyValue("firetype", "0");
                    flame:SetKeyValue("damagescale", "5");
                    flame:SetKeyValue("spawnflags", "128");
                    flame:SetPhysicsAttacker(self.Owner)
                    flame:SetOwner( self.Owner )
                    flame:Spawn();
                    flame:Fire("StartFire",0);
                end
            end
        end
        self:TakePrimaryAmmo( 1 )
        local owner = self.Owner
        --if not ValidEntity(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

        owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )
    end
end
