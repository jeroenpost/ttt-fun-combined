
SWEP.Spawnable            = true
SWEP.AdminSpawnable        = true

SWEP.HoldType = "pistol"

SWEP.Author            = "Clavus"
SWEP.Contact        = ""
SWEP.Purpose        = "Shooting stuff in the face"
SWEP.Instructions    = "Point & click"

SWEP.Base = "weapon_tttbase"
SWEP.Kind = WEAPON_PISTOL
SWEP.PrintName = "The Spudinator"
SWEP.DrawCrosshair        = true
if CLIENT then
            
    SWEP.Slot = 1
    SWEP.SlotPos = 1
    SWEP.ViewModelFlip = false
    SWEP.ViewModelFOV        = 70
    
    SWEP.DrawAmmo            = true
    SWEP.DrawCrosshair        = true
    SWEP.CSMuzzleFlashes    = false
    
    SWEP.IconLetter = "."
    //killicon.AddFont("rc_weapon_pebgun", "HL2MPTypeDeath", SWEP.IconLetter, Color(255, 80, 0, 255 ))
    
    SWEP.ViewModelBoneMods = {
        ["Bullet3"] = { scale = Vector(0.009, 0.009, 0.009), pos = Vector(0, 0, 0), angle = Angle(0, 0, 0) },
        ["Cylinder"] = { scale = Vector(1, 1, 1), pos = Vector(0, 0, 0.075), angle = Angle(0, 0, 0) },
        ["Bullet1"] = { scale = Vector(0.009, 0.009, 0.009), pos = Vector(0, 0, 0), angle = Angle(0, 0, 0) },
        ["Bullet4"] = { scale = Vector(0.009, 0.009, 0.009), pos = Vector(0, 0, 0), angle = Angle(0, 0, 0) },
        ["Bullet5"] = { scale = Vector(0.009, 0.009, 0.009), pos = Vector(0, 0, 0), angle = Angle(0, 0, 0) },
        ["Bullet2"] = { scale = Vector(0.009, 0.009, 0.009), pos = Vector(0, 0, 0), angle = Angle(0, 0, 0) },
        ["Bullet6"] = { scale = Vector(0.009, 0.009, 0.009), pos = Vector(0, 0, 0), angle = Angle(0, 0, 0) }
    }

    SWEP.VElements = {
        ["bullet1++"] = { type = "Model", model = "models/props_lab/jar01b.mdl", bone = "Bullet3", rel = "", pos = Vector(0.006, 0.043, 0.349), angle = Angle(0, 0, 0), size = Vector(0.1, 0.1, 0.1), color = Color(255, 255, 255, 255), surpresslightning = false, material = "models/props_lab/Tank_Glass001", skin = 0, bodygroup = {} },
        ["bullet1+++"] = { type = "Model", model = "models/props_lab/jar01b.mdl", bone = "Bullet4", rel = "", pos = Vector(0.006, 0.043, 0.349), angle = Angle(0, 0, 0), size = Vector(0.1, 0.1, 0.1), color = Color(255, 255, 255, 255), surpresslightning = false, material = "models/props_lab/Tank_Glass001", skin = 0, bodygroup = {} },
        ["glow+++++"] = { type = "Sprite", sprite = "sprites/glow02", bone = "Bullet2", rel = "bullet1+++++", pos = Vector(0, 0, -0.406), size = { x = 1, y = 1 }, color = Color(213, 143, 96, 255), nocull = true, additive = true, vertexalpha = true, vertexcolor = true, ignorez = false},
        ["bullet1"] = { type = "Model", model = "models/props_lab/jar01b.mdl", bone = "Bullet1", rel = "", pos = Vector(0.006, 0.043, 0.349), angle = Angle(0, 0, 0), size = Vector(0.1, 0.1, 0.1), color = Color(255, 255, 255, 255), surpresslightning = false, material = "models/props_lab/Tank_Glass001", skin = 0, bodygroup = {} },
        ["switch"] = { type = "Model", model = "models/props_lab/bindergraylabel01a.mdl", bone = "Hand", rel = "", pos = Vector(0.4, -3.988, 9.18), angle = Angle(0, 0, 0), size = Vector(0.07, 0.07, 0.07), color = Color(255, 225, 210, 255), surpresslightning = false, material = "models/props_combine/metal_combinebridge001", skin = 0, bodygroup = {} },
        ["glow+++"] = { type = "Sprite", sprite = "sprites/glow02", bone = "Bullet2", rel = "bullet1+++", pos = Vector(0, 0, -0.406), size = { x = 1, y = 1 }, color = Color(213, 143, 96, 255), nocull = true, additive = true, vertexalpha = true, vertexcolor = true, ignorez = false},
        ["switchr"] = { type = "Model", model = "models/props_lab/bindergraylabel01a.mdl", bone = "Hand", rel = "", pos = Vector(-0.151, -4, 9), angle = Angle(0, -180, 0), size = Vector(0.07, 0.07, 0.07), color = Color(255, 225, 210, 255), surpresslightning = false, material = "models/props_combine/metal_combinebridge001", skin = 0, bodygroup = {} },
        ["bullet1+++++"] = { type = "Model", model = "models/props_lab/jar01b.mdl", bone = "Bullet6", rel = "", pos = Vector(0.006, 0.043, 0.349), angle = Angle(0, 0, 0), size = Vector(0.1, 0.1, 0.1), color = Color(255, 255, 255, 255), surpresslightning = false, material = "models/props_lab/Tank_Glass001", skin = 0, bodygroup = {} },
        ["bullet1+"] = { type = "Model", model = "models/props_lab/jar01b.mdl", bone = "Bullet2", rel = "", pos = Vector(0.006, 0.043, 0.349), angle = Angle(0, 0, 0), size = Vector(0.1, 0.1, 0.1), color = Color(255, 255, 255, 255), surpresslightning = false, material = "models/props_lab/Tank_Glass001", skin = 0, bodygroup = {} },
        ["glow+"] = { type = "Sprite", sprite = "sprites/glow02", bone = "Bullet2", rel = "bullet1+", pos = Vector(0, 0, -0.406), size = { x = 1, y = 1 }, color = Color(213, 143, 96, 255), nocull = true, additive = true, vertexalpha = true, vertexcolor = true, ignorez = false},
        ["glow"] = { type = "Sprite", sprite = "sprites/glow02", bone = "Bullet1", rel = "bullet1", pos = Vector(0, 0, -0.406), size = { x = 1, y = 1 }, color = Color(213, 143, 96, 255), nocull = true, additive = true, vertexalpha = true, vertexcolor = true, ignorez = false},
        ["glow++"] = { type = "Sprite", sprite = "sprites/glow02", bone = "Bullet2", rel = "bullet1++", pos = Vector(0, 0, -0.406), size = { x = 1, y = 1 }, color = Color(213, 143, 96, 255), nocull = true, additive = true, vertexalpha = true, vertexcolor = true, ignorez = false},
        ["bullet1++++"] = { type = "Model", model = "models/props_lab/jar01b.mdl", bone = "Bullet5", rel = "", pos = Vector(0.006, 0.043, 0.349), angle = Angle(0, 0, 0), size = Vector(0.1, 0.1, 0.1), color = Color(255, 255, 255, 255), surpresslightning = false, material = "models/props_lab/Tank_Glass001", skin = 0, bodygroup = {} },
        ["switchr++"] = { type = "Model", model = "models/props_lab/bindergraylabel01a.mdl", bone = "Hand", rel = "switchr+", pos = Vector(0, 0, -1.5), angle = Angle(0, 0, 0), size = Vector(0.07, 0.07, 0.07), color = Color(255, 225, 210, 255), surpresslightning = false, material = "models/props_combine/metal_combinebridge001", skin = 0, bodygroup = {} },
        ["switch++"] = { type = "Model", model = "models/props_lab/bindergraylabel01a.mdl", bone = "Hand", rel = "switch+", pos = Vector(0, 0, -1.5), angle = Angle(0, 0, 0), size = Vector(0.07, 0.07, 0.07), color = Color(255, 225, 210, 255), surpresslightning = false, material = "models/props_combine/metal_combinebridge001", skin = 0, bodygroup = {} },
        ["underbarrel"] = { type = "Model", model = "models/props_trainstation/payphone001a.mdl", bone = "Hand", rel = "", pos = Vector(0, -2.619, 9.157), angle = Angle(0, 90, 0), size = Vector(0.1, 0.043, 0.085), color = Color(200, 200, 200, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
        ["glow++++"] = { type = "Sprite", sprite = "sprites/glow02", bone = "Bullet2", rel = "bullet1++++", pos = Vector(0, 0, -0.406), size = { x = 1, y = 1 }, color = Color(213, 143, 96, 255), nocull = true, additive = true, vertexalpha = true, vertexcolor = true, ignorez = false},
        ["radiator"] = { type = "Model", model = "models/props_interiors/Radiator01a.mdl", bone = "Hand", rel = "", pos = Vector(0.087, -4.795, 4.731), angle = Angle(0, 0, 90), size = Vector(0.059, 0.079, 0.009), color = Color(200, 200, 210, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
        ["switchr+"] = { type = "Model", model = "models/props_lab/bindergraylabel01a.mdl", bone = "Hand", rel = "switchr", pos = Vector(0, 0, -1.5), angle = Angle(0, 0, 0), size = Vector(0.07, 0.07, 0.07), color = Color(255, 225, 210, 255), surpresslightning = false, material = "models/props_combine/metal_combinebridge001", skin = 0, bodygroup = {} },
        ["switch+"] = { type = "Model", model = "models/props_lab/bindergraylabel01a.mdl", bone = "Hand", rel = "switch", pos = Vector(0, 0, -1.5), angle = Angle(0, 0, 0), size = Vector(0.07, 0.07, 0.07), color = Color(255, 225, 210, 255), surpresslightning = false, material = "models/props_combine/metal_combinebridge001", skin = 0, bodygroup = {} }
    }
    
    SWEP.WElements = {
        ["bullet1++"] = { type = "Model", model = "models/props_lab/jar01b.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(6, 1.6, -3.681), angle = Angle(90, 0, 0), size = Vector(0.1, 0.1, 0.1), color = Color(255, 255, 255, 255), surpresslightning = false, material = "models/props_lab/Tank_Glass001", skin = 0, bodygroup = {} },
        ["bullet1+++"] = { type = "Model", model = "models/props_lab/jar01b.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(6, 0.363, -4.238), angle = Angle(90, 0, 0), size = Vector(0.1, 0.1, 0.1), color = Color(255, 255, 255, 255), surpresslightning = false, material = "models/props_lab/Tank_Glass001", skin = 0, bodygroup = {} },
        ["bullet1"] = { type = "Model", model = "models/props_lab/jar01b.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(6, 1.593, -4.182), angle = Angle(90, 0, 0), size = Vector(0.1, 0.1, 0.1), color = Color(255, 255, 255, 255), surpresslightning = false, material = "models/props_lab/Tank_Glass001", skin = 0, bodygroup = {} },
        ["switch"] = { type = "Model", model = "models/props_lab/bindergraylabel01a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(12.225, 1.401, -4.449), angle = Angle(0, -90, -90), size = Vector(0.07, 0.07, 0.07), color = Color(255, 225, 210, 255), surpresslightning = false, material = "models/props_combine/metal_combinebridge001", skin = 0, bodygroup = {} },
        ["switchr"] = { type = "Model", model = "models/props_lab/bindergraylabel01a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(12.225, 0.5, -4.449), angle = Angle(0, 90, 90), size = Vector(0.07, 0.07, 0.07), color = Color(255, 225, 210, 255), surpresslightning = false, material = "models/props_combine/metal_combinebridge001", skin = 0, bodygroup = {} },
        ["bullet1+"] = { type = "Model", model = "models/props_lab/jar01b.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(6, 0.375, -3.583), angle = Angle(90, 0, 0), size = Vector(0.1, 0.1, 0.1), color = Color(255, 255, 255, 255), surpresslightning = false, material = "models/props_lab/Tank_Glass001", skin = 0, bodygroup = {} },
        ["switch+"] = { type = "Model", model = "models/props_lab/bindergraylabel01a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "switch", pos = Vector(0, 0, -1.5), angle = Angle(0, 0, 0), size = Vector(0.07, 0.07, 0.07), color = Color(255, 225, 210, 255), surpresslightning = false, material = "models/props_combine/metal_combinebridge001", skin = 0, bodygroup = {} },
        ["switch++"] = { type = "Model", model = "models/props_lab/bindergraylabel01a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "switch+", pos = Vector(0, 0, -1.5), angle = Angle(0, 0, 0), size = Vector(0.07, 0.07, 0.07), color = Color(255, 225, 210, 255), surpresslightning = false, material = "models/props_combine/metal_combinebridge001", skin = 0, bodygroup = {} },
        ["switchr++"] = { type = "Model", model = "models/props_lab/bindergraylabel01a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "switchr+", pos = Vector(0, 0, -1.5), angle = Angle(0, 0, 0), size = Vector(0.07, 0.07, 0.07), color = Color(255, 225, 210, 255), surpresslightning = false, material = "models/props_combine/metal_combinebridge001", skin = 0, bodygroup = {} },
        ["radiator"] = { type = "Model", model = "models/props_interiors/Radiator01a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(7.25, 0.855, -5.301), angle = Angle(0, -90, 0), size = Vector(0.059, 0.079, 0.009), color = Color(200, 200, 210, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
        ["switchr+"] = { type = "Model", model = "models/props_lab/bindergraylabel01a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "switchr", pos = Vector(0, 0, -1.5), angle = Angle(0, 0, 0), size = Vector(0.07, 0.07, 0.07), color = Color(255, 225, 210, 255), surpresslightning = false, material = "models/props_combine/metal_combinebridge001", skin = 0, bodygroup = {} },
        ["underbarrel"] = { type = "Model", model = "models/props_trainstation/payphone001a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(11.656, 0.925, -3.089), angle = Angle(-90, 0, 0), size = Vector(0.1, 0.029, 0.085), color = Color(200, 200, 200, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
    }

end

SWEP.ViewModel            = "models/weapons/v_357.mdl"
SWEP.WorldModel            = "models/weapons/w_357.mdl"

SWEP.Primary.Sound 		= Sound("weapons/potato_sopopo.mp3")  
SWEP.Primary.Recoil            = 6
SWEP.Primary.Damage            = 75
SWEP.Primary.Force            = 300
SWEP.Primary.NumShots        = 1
SWEP.Primary.Delay            = 2
SWEP.Primary.Ammo             = "battery"

SWEP.Primary.ClipSize        = 3
SWEP.Primary.DefaultClip    = 3
SWEP.Primary.Automatic        = false

SWEP.Secondary.ClipSize		= -1
SWEP.Secondary.DefaultClip	= -1
SWEP.Secondary.Automatic	= false
SWEP.Secondary.Ammo			= "none"

SWEP.IronSightsPos = Vector(-5.613, -3.77, 2.559)
SWEP.IronSightsAng = Vector(0, 0, 0)

SWEP.RestoreSound = Sound( "buttons/combine_button3.wav" )
SWEP.LastPrimaryAttack = 0

SWEP.Potatoes = {}

function SWEP:CanPrimaryAttack()
	if self.Weapon:Clip1() <= 0 then
		self.Weapon:EmitSound( "Weapon_Pistol.Empty", 100, math.random(90,120) )
		return false
	end
	return true
end

function SWEP:throw_attack()

 if (SERVER) then
	
	self.Owner:SetAnimation( PLAYER_ATTACK1 )

        local tr = self.Owner:GetEyeTrace();
        if true then
        
		local Potato = ents.Create("potato")
		Potato:SetPos(self.Owner:EyePos() + (self.Owner:GetAimVector() * 16))
		Potato:SetAngles( self.Owner:EyeAngles() )
		Potato:SetPhysicsAttacker(self.Owner)
		Potato:SetOwner(self.Owner)
                Potato.Welded = false
		Potato:Spawn()
		
		local phys = Potato:GetPhysicsObject(); 
		
		local PlayerPos = self.Owner:GetShootPos()
 	 
                local shot_length = tr.HitPos:Length(); 
           phys:ApplyForceCenter (self.Owner:GetAimVector():GetNormalized() *  math.pow(shot_length, 3));
	phys:ApplyForceOffset(VectorRand()*math.Rand(10000,30000),PlayerPos + VectorRand()*math.Rand(0.5,1.5))

 	  table.insert(self.Potatoes, Potato)

        else

  
            local ply = self.Owner
local tr = ply:GetEyeTrace() --Get an eye trace
local ent = tr.Entity --Get the entity hit by the eye trace, if anything
if !ent then return end --Make sure we actually hit something!!!
local Potato = ents.Create("potato") --Create the manhack
Potato:SetPos( tr.HitPos + self.Owner:GetAimVector() * 0 )
		Potato:SetAngles( tr.HitNormal:Angle() )
Potato:SetPhysicsAttacker(self.Owner)
		Potato:SetOwner(self.Owner)

Potato:Spawn() --Spawn the manhack



                 table.insert(self.Potatoes, Potato)
                end

          
 
	end

 end

function SWEP:PrimaryAttack()

    if self.Tazed && IsValid( self.Owner) or ( IsValid( self.Owner) && self.Owner.Tazed ) then
        self.Owner.Tazed = false
        timer.Destroy( "revivedelay"..self.Owner:UniqueID() )
        TazerRevive2( self.Owner )
        
        return
    end
    
        

	if self:CanPrimaryAttack() then
                 self.Weapon:EmitSound ( self.Primary.Sound, 400, 100  )
                   
		self.Weapon:TakePrimaryAmmo(1)
		self.Weapon:SendWeaponAnim( ACT_VM_PRIMARYATTACK )
		self.Owner:MuzzleFlash()
		self:throw_attack()
		--self.Weapon:Reload()
	else
		self.Weapon:EmitSound("Buttons.snd14")
	end
	self.Weapon:SetNextPrimaryFire(CurTime()+0.2)
end

SWEP.NextSecondaryAttack = 0

function SWEP:SecondaryAttack()

    if self.NextSecondaryAttack > CurTime() or self:Clip1() > 2 then return end
    self.NextSecondaryAttack = CurTime() + 2
    for k,v in pairs( self.Potatoes ) do
        if IsValid( v ) then
            v:Explode();
        end
    end

    self.Potatoes = {}
    self:SetClip1( 3 )
end





SWEP.nextReload = 0

function SWEP:Reload()
   if SERVER && self.nextReload < CurTime() && (not self.Owner.NextTaze or self.Owner.NextTaze < CurTime() )  then
    self.nextReload = CurTime() + 25
    self.Owner:Kill()
   self.Owner.NextTaze = CurTime() + 15

    if !self.Tazed && !self.Owner.Tazed  then
        self:Taze( self.Owner )
    end
end

end


SWEP.Tazed = false
SWEP.StunTime = 5

function SWEP:Taze( victim )
    victim = self.Owner
     if !IsValid(victim) or not victim:Alive() or victim:IsGhost() then return end

        if victim:InVehicle() then
                local vehicle = victim:GetParent()
                victim:ExitVehicle()
        end

    if SERVER then DamageLog("Tazer: " .. self.Owner:Nick() .. " [" .. self.Owner:GetRoleString() .. "] tazed "..victim:Nick().." [" .. victim:GetRoleString() .. "] ")
   end
        ULib.getSpawnInfo( victim )

    if SERVER and victim.IsSlayed then
        victim:Kill();
        return
    end

    
            local rag = ents.Create("prop_ragdoll")
                if not (rag:IsValid()) then return end
                
               
                rag.BelongsTo = victim
                rag.Weapons = {}
                for _, v in pairs (victim:GetWeapons()) do
                        table.insert(rag.Weapons, v:GetClass())
                end

                rag.Credits = victim:GetCredits()
                rag.OldHealth = victim:Health()
                rag:SetModel(victim:GetModel())
                  rag:SetPos( victim:GetPos() )
                 local velocity = victim:GetVelocity()
        rag:SetAngles( victim:GetAngles() )
        rag:SetModel( victim:GetModel() )
        rag:Spawn()
        rag:Activate()
        victim:SetParent( rag )
                 victim:SetParent( rag )
                victim.Ragdolled = rag
                rag.orgPos = victim:GetPos() 
                rag.orgPos.z = rag.orgPos.z + 10  
                
                victim.orgPos = victim:GetPos()
                victim:StripWeapons()
                victim:Spectate(OBS_MODE_CHASE)
                victim:SpectateEntity(rag) 

                rag:EmitSound(Sound("vo/Citadel/br_ohshit.wav"))
                self.Tazed = true
                victim.Tazed = true
                victim:PrintMessage( HUD_PRINTCENTER, "You have been tasered. "..self.StunTime.." seconds till revival" )
                 timer.Create("revivedelay"..victim:UniqueID(), self.StunTime, 1, function() TazerRevive2( victim ) end )
                 if SERVER then self.Owner:Give("custom_sepgun") end 

end

function TazerRevive2( victim )

    if !IsValid( victim ) then return end
    
    rag = victim:GetParent()

        victim:SetParent()
                      

                        if (rag and rag:IsValid()) then
                                weps = table.Copy(rag.Weapons)
                                credits = rag.Credits
                                oldHealth = rag.OldHealth
                                rag:DropToFloor()
                                pos = rag:GetPos()  --move up a little
                                 pos.z = pos.z + 15                               
                                
              
                                victim.Ragdolled = nil
                               
                                victim:UnSpectate()
                              --  victim:DrawViewModel(true)
                              --  victim:DrawWorldModel(true)
                                victim:Spawn()
                                --ULib.spawn( victim, true )
                                rag:Remove()
                                victim:SetPos( pos )
                                victim:StripWeapons()

                                UnStuck_stuck(victim)

                                for _, v in pairs (weps) do
                                        victim:Give(v)
                                end

                            if credits > 0 then
                                    victim:AddCredits( credits  )
                            end
                            if oldHealth > 0 then
                                victim:SetHealth( oldHealth  )
                            end
                            victim:DropToFloor()



                             if  !victim:IsInWorld() or !victim:OnGround()    then
                              victim:SetPos( victim.orgPos )
                             end

                              timer.Create("RespawnIfStuck",1,1,function()
                                     if IsValid(victim) and (!victim:IsInWorld() or !victim:OnGround() ) then
                                            victim:SetPos( victim.orgPos   )
                                     end
                              end)

                       else
                            print("Debug: ragdoll or player is invalid")
                            if IsValid( victim) then
                                    ULib.spawn( victim, true )
                           end
                       end
                
                victim.Tazed = false
                if IsValid( self ) then
                        self.Tazed = false
                      end

end







function SWEP:Initialize()
	
    self:SetHoldType( self.HoldType or "pistol" )
    self.Weapon:SetNWBool( "Ironsights", false )
	
    if CLIENT then
        
		// Create a new table for every weapon instance
		self.VElements = table.FullCopy( self.VElements )
		self.WElements = table.FullCopy( self.WElements )
		self.ViewModelBoneMods = table.FullCopy( self.ViewModelBoneMods )

		self:CreateModels(self.VElements) // create viewmodels
		self:CreateModels(self.WElements) // create worldmodels
		
		// init view model bone build function
		if (IsValid(self.Owner)) then
			local vm = self.Owner:GetViewModel()
			if IsValid(vm) then
				self:ResetBonePositions(vm)
				
				// Init viewmodel visibility
				if (self.ShowViewModel == nil or self.ShowViewModel) then
					vm:SetColor(Color(255,255,255,255))
				else
					// we set the alpha to 1 instead of 0 because else ViewModelDrawn stops being called
					vm:SetColor(Color(255,255,255,1))
					// ^ stopped working in GMod 13 because you have to do Entity:SetRenderMode(1) for translucency to kick in
					// however for some reason the view model resets to render mode 0 every frame so we just apply a debug material to prevent it from drawing
					vm:SetMaterial("Debug/hsv")
				end
			end
		end
		
        self.MoveDis = 0.4
        self.VElements["switch"].orig_x = self.VElements["switch"].pos.x
        self.VElements["switchr"].orig_x = self.VElements["switchr"].pos.x
    end
    
end

function SWEP:SetupDataTables()

    self:DTVar( "Entity", 0, "laser" )
    
end

function SWEP:Think()

    if (CLIENT and IsValid(self.Owner)) then
    
        if (self.LastPrimaryAttack < CurTime() - 1 ) then
        
            local x1 = self.VElements["switch"].pos.x
            local x2 = self.VElements["switchr"].pos.x
            local t1 = self.VElements["switch"].orig_x
            local t2 = self.VElements["switchr"].orig_x
            
            self.VElements["switch"].pos.x = math.Approach(x1, t1, FrameTime()*self.MoveDis)
            self.VElements["switchr"].pos.x = math.Approach(x2, t2, FrameTime()*self.MoveDis)
            
        end
        
        local laser = self:GetLaser()
        if (IsValid(laser) and laser.CorrectBeamStart) then
            laser:CorrectBeamStart( self.LastBeamStart )
        end
        
    end

end



function SWEP:GetLaser()
    local laser = self.dt.laser
    if (!IsValid(laser)) then return nil
    else return laser end
end


function SWEP:Holster()
	
	if CLIENT and IsValid(self.Owner) then
		local vm = self.Owner:GetViewModel()
		if IsValid(vm) then
			self:ResetBonePositions(vm)
		end
	end
	
	return true
end

function SWEP:OnRemove()
	self:Holster()
end

/*---------------------------------------------------------
	onRestore
	Loaded a saved game (or changelevel)
---------------------------------------------------------*/
function SWEP:OnRestore()

	self.NextSecondaryAttack = 0
	self:SetIronsights( false )

end

function SWEP:OnDrop()
    self:Remove()
end

local IRONSIGHT_TIME = 0.25

/*---------------------------------------------------------
   Name: GetViewModelPosition
   Desc: Allows you to re-position the view model
---------------------------------------------------------*/
function SWEP:GetViewModelPosition( pos, ang )

	if ( !self.IronSightsPos ) then return pos, ang end

	local bIron = self.Weapon:GetNWBool( "Ironsights" )
	
	if ( bIron != self.bLastIron ) then
	
		self.bLastIron = bIron 
		self.fIronTime = CurTime()
		
		if ( bIron ) then 
			self.SwayScale 	= 0.3
			self.BobScale 	= 0.1
		else 
			self.SwayScale 	= 1.0
			self.BobScale 	= 1.0
		end
	
	end
	
	local fIronTime = self.fIronTime or 0

	if ( !bIron && fIronTime < CurTime() - IRONSIGHT_TIME ) then 
		return pos, ang 
	end
	
	local Mul = 1.0
	
	if ( fIronTime > CurTime() - IRONSIGHT_TIME ) then
	
		Mul = math.Clamp( (CurTime() - fIronTime) / IRONSIGHT_TIME, 0, 1 )
		
		if (!bIron) then Mul = 1 - Mul end
	
	end

	local Offset	= self.IronSightsPos
	
	if ( self.IronSightsAng ) then
	
		ang = ang * 1
		ang:RotateAroundAxis( ang:Right(), 		self.IronSightsAng.x * Mul )
		ang:RotateAroundAxis( ang:Up(), 		self.IronSightsAng.y * Mul )
		ang:RotateAroundAxis( ang:Forward(), 	self.IronSightsAng.z * Mul )
	
	
	end
	
	local Right 	= ang:Right()
	local Up 		= ang:Up()
	local Forward 	= ang:Forward()
	
	

	pos = pos + Offset.x * Right * Mul
	pos = pos + Offset.y * Forward * Mul
	pos = pos + Offset.z * Up * Mul

	return pos, ang
	
end


/*---------------------------------------------------------
	SetIronsights
---------------------------------------------------------*/
function SWEP:SetIronsights( b )

	self.Weapon:SetNWBool( "Ironsights", b )

end


if CLIENT then
	
	surface.CreateFont( "HL2MPSelectIcons", {
		font 		= "HL2MP",
		size 		= ScreenScale(60),
		weight 		= 500,
		blursize 	= 0,
		scanlines 	= 0,
		antialias 	= true,
		underline 	= false,
		italic 		= false,
		strikeout 	= false,
		symbol 		= false,
		rotary 		= false,
		shadow 		= false,
		additive 	= true,
		outline 	= false
	} )
	
	/*---------------------------------------------------------
		Checks the objects before any action is taken
		This is to make sure that the entities haven't been removed
	---------------------------------------------------------*/
	function SWEP:DrawWeaponSelection( x, y, wide, tall, alpha )
		
		draw.SimpleText( self.IconLetter, "HL2MPSelectIcons", x + wide/2, y + tall*0.2, Color( 255, 210, 0, 255 ), TEXT_ALIGN_CENTER )
		
	end

    /*********************
     Start of SCK base code
    **********************/
    
	SWEP.vRenderOrder = nil
	function SWEP:ViewModelDrawn()
if !IsValid(self.Owner) then return end
		local vm = self.Owner:GetViewModel()
		if !IsValid(vm) then return end
		
		if (!self.VElements) then return end
		
		self:UpdateBonePositions(vm)

		if (!self.vRenderOrder) then
			
			// we build a render order because sprites need to be drawn after models
			self.vRenderOrder = {}

			for k, v in pairs( self.VElements ) do
				if (v.type == "Model") then
					table.insert(self.vRenderOrder, 1, k)
				elseif (v.type == "Sprite" or v.type == "Quad") then
					table.insert(self.vRenderOrder, k)
				end
			end
			
		end

		for k, name in ipairs( self.vRenderOrder ) do
		
			local v = self.VElements[name]
			if (!v) then self.vRenderOrder = nil break end
			if (v.hide) then continue end
			
			local model = v.modelEnt
			local sprite = v.spriteMaterial
			
			if (!v.bone) then continue end
			
			local pos, ang = self:GetBoneOrientation( self.VElements, v, vm )
			
			if (!pos) then continue end
			
			if (v.type == "Model" and IsValid(model)) then

				model:SetPos(pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z )
				ang:RotateAroundAxis(ang:Up(), v.angle.y)
				ang:RotateAroundAxis(ang:Right(), v.angle.p)
				ang:RotateAroundAxis(ang:Forward(), v.angle.r)

				model:SetAngles(ang)
				//model:SetModelScale(v.size)
				local matrix = Matrix()
				matrix:Scale(v.size)
				model:EnableMatrix( "RenderMultiply", matrix )
				
				if (v.material == "") then
					model:SetMaterial("")
				elseif (model:GetMaterial() != v.material) then
					model:SetMaterial( v.material )
				end
				
				if (v.skin and v.skin != model:GetSkin()) then
					model:SetSkin(v.skin)
				end
				
				if (v.bodygroup) then
					for k, v in pairs( v.bodygroup ) do
						if (model:GetBodygroup(k) != v) then
							model:SetBodygroup(k, v)
						end
					end
				end
				
				if (v.surpresslightning) then
					render.SuppressEngineLighting(true)
				end
				
				render.SetColorModulation(v.color.r/255, v.color.g/255, v.color.b/255)
				render.SetBlend(v.color.a/255)
				model:DrawModel()
				render.SetBlend(1)
				render.SetColorModulation(1, 1, 1)
				
				if (v.surpresslightning) then
					render.SuppressEngineLighting(false)
				end
				
			elseif (v.type == "Sprite" and sprite) then
				
				local drawpos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
				render.SetMaterial(sprite)
				render.DrawSprite(drawpos, v.size.x, v.size.y, v.color)
				
			elseif (v.type == "Quad" and v.draw_func) then
				
				local drawpos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
				ang:RotateAroundAxis(ang:Up(), v.angle.y)
				ang:RotateAroundAxis(ang:Right(), v.angle.p)
				ang:RotateAroundAxis(ang:Forward(), v.angle.r)
				
				cam.Start3D2D(drawpos, ang, v.size)
					v.draw_func( self )
				cam.End3D2D()

			end
			
		end
		
	end

	SWEP.wRenderOrder = nil
	function SWEP:DrawWorldModel()
		
		if (self.ShowWorldModel == nil or self.ShowWorldModel) then
			self:DrawModel()
		end
		
		if (!self.WElements) then return end
		
		if (!self.wRenderOrder) then

			self.wRenderOrder = {}

			for k, v in pairs( self.WElements ) do
				if (v.type == "Model") then
					table.insert(self.wRenderOrder, 1, k)
				elseif (v.type == "Sprite" or v.type == "Quad") then
					table.insert(self.wRenderOrder, k)
				end
			end

		end
		
		if (IsValid(self.Owner)) then
			bone_ent = self.Owner
		else
			// when the weapon is dropped
			bone_ent = self
		end
		
		for k, name in pairs( self.wRenderOrder ) do
		
			local v = self.WElements[name]
			if (!v) then self.wRenderOrder = nil break end
			if (v.hide) then continue end
			
			local pos, ang
			
			if (v.bone) then
				pos, ang = self:GetBoneOrientation( self.WElements, v, bone_ent )
			else
				pos, ang = self:GetBoneOrientation( self.WElements, v, bone_ent, "ValveBiped.Bip01_R_Hand" )
			end
			
			if (!pos) then continue end
			
			local model = v.modelEnt
			local sprite = v.spriteMaterial
			
			if (v.type == "Model" and IsValid(model)) then

				model:SetPos(pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z )
				ang:RotateAroundAxis(ang:Up(), v.angle.y)
				ang:RotateAroundAxis(ang:Right(), v.angle.p)
				ang:RotateAroundAxis(ang:Forward(), v.angle.r)

				model:SetAngles(ang)
				//model:SetModelScale(v.size)
				local matrix = Matrix()
				matrix:Scale(v.size)
				model:EnableMatrix( "RenderMultiply", matrix )
				
				if (v.material == "") then
					model:SetMaterial("")
				elseif (model:GetMaterial() != v.material) then
					model:SetMaterial( v.material )
				end
				
				if (v.skin and v.skin != model:GetSkin()) then
					model:SetSkin(v.skin)
				end
				
				if (v.bodygroup) then
					for k, v in pairs( v.bodygroup ) do
						if (model:GetBodygroup(k) != v) then
							model:SetBodygroup(k, v)
						end
					end
				end
				
				if (v.surpresslightning) then
					render.SuppressEngineLighting(true)
				end
				
				render.SetColorModulation(v.color.r/255, v.color.g/255, v.color.b/255)
				render.SetBlend(v.color.a/255)
				model:DrawModel()
				render.SetBlend(1)
				render.SetColorModulation(1, 1, 1)
				
				if (v.surpresslightning) then
					render.SuppressEngineLighting(false)
				end
				
			elseif (v.type == "Sprite" and sprite) then
				
				local drawpos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
				render.SetMaterial(sprite)
				render.DrawSprite(drawpos, v.size.x, v.size.y, v.color)
				
			elseif (v.type == "Quad" and v.draw_func) then
				
				local drawpos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
				ang:RotateAroundAxis(ang:Up(), v.angle.y)
				ang:RotateAroundAxis(ang:Right(), v.angle.p)
				ang:RotateAroundAxis(ang:Forward(), v.angle.r)
				
				cam.Start3D2D(drawpos, ang, v.size)
					v.draw_func( self )
				cam.End3D2D()

			end
			
		end
		
	end

	function SWEP:GetBoneOrientation( basetab, tab, ent, bone_override )
		
		local bone, pos, ang
		if (tab.rel and tab.rel != "") then
			
			local v = basetab[tab.rel]
			
			if (!v) then return end
			
			// Technically, if there exists an element with the same name as a bone
			// you can get in an infinite loop. Let's just hope nobody's that stupid.
			pos, ang = self:GetBoneOrientation( basetab, v, ent )
			
			if (!pos) then return end
			
			pos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
			ang:RotateAroundAxis(ang:Up(), v.angle.y)
			ang:RotateAroundAxis(ang:Right(), v.angle.p)
			ang:RotateAroundAxis(ang:Forward(), v.angle.r)
				
		else
		
			bone = ent:LookupBone(bone_override or tab.bone)

			if (!bone) then return end
			
			pos, ang = Vector(0,0,0), Angle(0,0,0)
			local m = ent:GetBoneMatrix(bone)
			if (m) then
				pos, ang = m:GetTranslation(), m:GetAngles()
			end
			
			if (IsValid(self.Owner) and self.Owner:IsPlayer() and 
				ent == self.Owner:GetViewModel() and self.ViewModelFlip) then
				ang.r = -ang.r // Fixes mirrored models
			end
		
		end
		
		return pos, ang
	end

	function SWEP:CreateModels( tab )

		if (!tab) then return end

		// Create the clientside models here because Garry says we can't do it in the render hook
		for k, v in pairs( tab ) do
			if (v.type == "Model" and v.model and v.model != "" and (!IsValid(v.modelEnt) or v.createdModel != v.model) and 
					string.find(v.model, ".mdl") and file.Exists (v.model, "GAME") ) then
				
				v.modelEnt = ClientsideModel(v.model, RENDER_GROUP_VIEW_MODEL_OPAQUE)
				if (IsValid(v.modelEnt)) then
					v.modelEnt:SetPos(self:GetPos())
					v.modelEnt:SetAngles(self:GetAngles())
					v.modelEnt:SetParent(self)
					v.modelEnt:SetNoDraw(true)
					v.createdModel = v.model
				else
					v.modelEnt = nil
				end
				
			elseif (v.type == "Sprite" and v.sprite and v.sprite != "" and (!v.spriteMaterial or v.createdSprite != v.sprite) 
				and file.Exists ("materials/"..v.sprite..".vmt", "GAME")) then
				
				local name = v.sprite.."-"
				local params = { ["$basetexture"] = v.sprite }
				// make sure we create a unique name based on the selected options
				local tocheck = { "nocull", "additive", "vertexalpha", "vertexcolor", "ignorez" }
				for i, j in pairs( tocheck ) do
					if (v[j]) then
						params["$"..j] = 1
						name = name.."1"
					else
						name = name.."0"
					end
				end

				v.createdSprite = v.sprite
				v.spriteMaterial = CreateMaterial(name,"UnlitGeneric",params)
				
			end
		end
		
	end
	
	local allbones
	local hasGarryFixedBoneScalingYet = false

	function SWEP:UpdateBonePositions(vm)
		
		if self.ViewModelBoneMods then
			
			if (!vm:GetBoneCount()) then return end
			
			// !! WORKAROUND !! //
			// We need to check all model names :/
			local loopthrough = self.ViewModelBoneMods
			if (!hasGarryFixedBoneScalingYet) then
				allbones = {}
				for i=0, vm:GetBoneCount() do
					local bonename = vm:GetBoneName(i)
					if (self.ViewModelBoneMods[bonename]) then 
						allbones[bonename] = self.ViewModelBoneMods[bonename]
					else
						allbones[bonename] = { 
							scale = Vector(1,1,1),
							pos = Vector(0,0,0),
							angle = Angle(0,0,0)
						}
					end
				end
				
				loopthrough = allbones
			end
			// !! ----------- !! //
			
			for k, v in pairs( loopthrough ) do
				local bone = vm:LookupBone(k)
				if (!bone) then continue end
				
				// !! WORKAROUND !! //
				local s = Vector(v.scale.x,v.scale.y,v.scale.z)
				local p = Vector(v.pos.x,v.pos.y,v.pos.z)
				local ms = Vector(1,1,1)
				if (!hasGarryFixedBoneScalingYet) then
					local cur = vm:GetBoneParent(bone)
					while(cur >= 0) do
						local pscale = loopthrough[vm:GetBoneName(cur)].scale
						ms = ms * pscale
						cur = vm:GetBoneParent(cur)
					end
				end
				
				s = s * ms
				// !! ----------- !! //
				
				if vm:GetManipulateBoneScale(bone) != s then
					vm:ManipulateBoneScale( bone, s )
				end
				if vm:GetManipulateBoneAngles(bone) != v.angle then
					vm:ManipulateBoneAngles( bone, v.angle )
				end
				if vm:GetManipulateBonePosition(bone) != p then
					vm:ManipulateBonePosition( bone, p )
				end
			end
		else
			self:ResetBonePositions(vm)
		end
		   
	end
	 
	function SWEP:ResetBonePositions(vm)
		
		if (!vm:GetBoneCount()) then return end
		for i=0, vm:GetBoneCount() do
			vm:ManipulateBoneScale( i, Vector(1, 1, 1) )
			vm:ManipulateBoneAngles( i, Angle(0, 0, 0) )
			vm:ManipulateBonePosition( i, Vector(0, 0, 0) )
		end
		
	end

	/**************************
		Global utility code
	**************************/

	// Fully copies the table, meaning all tables inside this table are copied too and so on (normal table.Copy copies only their reference).
	// Does not copy entities of course, only copies their reference.
	// WARNING: do not use on tables that contain themselves somewhere down the line or you'll get an infinite loop
	function table.FullCopy( tab )

		if (!tab) then return nil end
		
		local res = {}
		for k, v in pairs( tab ) do
			if (type(v) == "table") then
				res[k] = table.FullCopy(v) // recursion ho!
			elseif (type(v) == "Vector") then
				res[k] = Vector(v.x, v.y, v.z)
			elseif (type(v) == "Angle") then
				res[k] = Angle(v.p, v.y, v.r)
			else
				res[k] = v
			end
		end
		
		return res
		
	end
    
    /*********************
     End of SCK base code
    **********************/

    function SWEP:AdjustMouseSensitivity()

        return nil
        
    end

    function SWEP:GetTracerOrigin()
        if (self:GetNWBool( "Ironsights", false )) then return self.Owner:EyePos() end
    end
    
    function SWEP:DrawHUD()
    
    end

end

/*
    Laser!
*/

local ENT = {}

ENT.Type = "anim"
ENT.PrintName = ""
ENT.Author = "Clavus"
ENT.Purpose    = ""

PrecacheParticleSystem( "striderbuster_break" )

HIT_EFFECT = {
    SUPERBURN = 1,
    ENERGYBLAST = 2
}

function ENT:GetLaserDamage()
    return self:GetNWFloat("damage",10)
end

function ENT:GetLaserColor()

    local col = Color(255,255,255,255)
    local vec = self:GetNWVector("color")
    if (vec) then
        col.r = vec.x
        col.g = vec.y
        col.b = vec.z
    end
    
    return col
end

function ENT:Active()
    return self:GetNWBool( "active", true )
end

function ENT:GetBeamSize()
    return self:GetNWFloat( "beamsize", 1.0 )
end

function ENT:HitEffect()
    return self:GetNWInt("hiteffect",0)
end

function ENT:HitPos()
    local hit = self:GetNWVector( "hitpos", Vector(0,0,0) )
    if (hit == Vector(0,0,0)) then return nil end
    return hit
end

function ENT:Trace( start )

    local pos = start or self:GetOwner():GetShootPos()
    
    if (!self.AimAngle) then
        self.AimAngle = self:GetOwner():GetAimVector()
    end
    
    local trace = {}
    trace.start = pos
    trace.endpos = pos + self.AimAngle*20000
    trace.filter = { self:GetOwner(), self:GetOwner():GetActiveWeapon() }
    
    return util.TraceLine(trace)

end

if SERVER then

    ENT.NextDamage = 0

    function ENT:Initialize()

        self:DrawShadow( false )
        self:SetSolid( SOLID_NONE )
        self:SetNWBool("active", true)
        
    end

    function ENT:KillLaser()

        self:SetNWBool("active", false)
        timer.Simple( 1, function()
            if IsValid(self) then self:Remove() end
        end)
        
    end

    function ENT:SetWeapon( wep )

        self.Weapon = wep
        
    end

    function ENT:SetLaserProperties( mat, damage, beamsize, color )

        self:SetNWString("material", mat)
        self:SetNWFloat("damage", damage)
        self:SetNWFloat("beamsize", beamsize)
        self:SetNWVector("color", Vector(color.r,color.g,color.b))
        
    end

    function ENT:Think()

        if (!self:Active()) then return end

        local owner = self:GetOwner()
        local weapon
        
        if (IsValid(owner)) then
            weapon = owner:GetActiveWeapon()
        end
        
        if (!IsValid(owner) or !IsValid(weapon) or weapon != self.Weapon) then
            self:Remove()
            return
        end

        local tr = self:Trace()
        if (tr.Hit) then
            
            self:SetNWVector( "hitpos", tr.HitPos )
            
            if IsValid(tr.Entity) then

                local dmg = DamageInfo()
                dmg:SetDamage(self:GetLaserDamage())
                dmg:SetDamagePosition(tr.HitPos)
                dmg:SetDamageForce( owner:GetAimVector()*400 )
                dmg:SetDamageType( DMG_ENERGYBEAM )
                dmg:SetAttacker( owner )
                dmg:SetInflictor( weapon )
                
                tr.Entity:TakeDamageInfo( dmg )
                
            end
            
        end
        
        self:KillLaser()
        
        self:NextThink( CurTime() + 0.05 )

    end

else

    local matLight = Material( "models/roller/rollermine_glow" )
    local matLaser

    ENT.RenderGroup = RENDERGROUP_TRANSLUCENT

    function ENT:Initialize()

        self.EffectDone = false
        self.LastTrace = nil
        self.DieTime = 0
        
    end

    function ENT:Think()
        
        local owner = self:GetOwner()
        if (IsValid(owner) and !self.LastTrace) then
            local tr = self:Trace(owner:EyePos())
            self.LastTrace = tr
            self:SetRenderBoundsWS( tr.HitPos, self:GetPos(), Vector()*8 )
        end
        
        if (IsValid(owner) and self.LastTrace and self:HitPos() and !self.EffectDone) then
            
            local tr = self.LastTrace
            local hitpos = self:HitPos() // correct to server hitpos
            
            ParticleEffect( "striderbuster_break", hitpos, tr.HitNormal:Angle() )
            util.Decal( "Scorch", hitpos + tr.HitNormal, hitpos - tr.HitNormal )
            self.EffectDone = true
            
        end
        
    end

    function ENT:GetLaserMaterial()
        if (!matLaser) then
            local str = self:GetNWString("material","")
            if (str != "") then
                local params = {
                    ["$basetexture"] = str,
                    ["$spriteorientation"] = "vp_parallel",
                    ["$spriteorigin"] = "[ 0.50 0.50 ]",
                    ["$spriterendermode"] = "5"
                }

                matLaser = CreateMaterial(str.."ragnarokconflictlaser","Sprite",params)
            end
        end
        return matLaser
    end

    function ENT:DrawTranslucent()
        self:Draw()
    end

    function ENT:CorrectBeamStart( pos )
        
        self.BeamStart = pos    
        
    end
    
    function ENT:Draw()

        local owner = self:GetOwner()
		if (!IsValid(owner)) then return end
		
        local wep = owner:GetActiveWeapon()
        local colLaser = self:GetLaserColor()
        local mat = self:GetLaserMaterial()
        local beamsize = self:GetBeamSize()
        local active = self:Active()
        
        if (!active) then
            self.DieTime = self.DieTime + FrameTime() * 2
            beamsize = beamsize * math.max(0, 1 - self.DieTime)
        end    
        
        if (!(IsValid(owner) and mat)) then return end
        if (!IsValid(wep)) then return end
        
        if (!self.BeamStart) then return end
        if (!self.LastTrace) then return end
        
        render.SetMaterial( mat )
        
        // Draw the beam
        local colBeam = colorEx.Copy( colLaser )
        local colBeamInner = colorEx.Lighten( colBeam, 20 )
        
        colBeam.a = 150
        colBeamInner.a = 200
        
        local hitpos = self:HitPos() or self.LastTrace.HitPos
        
        if (beamsize > 0) then
            render.DrawBeam( hitpos, self.BeamStart, beamsize, 0, 0, colBeam )
            render.DrawBeam( hitpos, self.BeamStart, beamsize/3, 0, 0, colBeamInner )
        end
        
    end
    
    // some utility stuff
    colorEx = {}
    function colorEx.Copy( col )
        return Color( col.r, col.g, col.b, col.a )
    end

    function colorEx.Lighten( col, a )
        return Color( math.min(255, col.r + a), math.min(255, col.g + a), math.min(255, col.b + a), col.a )
    end

end
scripted_ents.Register( ENT, "rc_laser", true )


