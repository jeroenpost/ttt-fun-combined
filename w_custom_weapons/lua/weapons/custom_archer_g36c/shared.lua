
SWEP.Base 				= "weapon_tttbase"
SWEP.HoldType				= "smg"
SWEP.ViewModelFlip		= false
SWEP.ViewModel			= Model("models/weapons/i_rif_g37ed.mdl")
SWEP.WorldModel			= Model("models/weapons/j_rif_g37ed.mdl")
SWEP.ViewModelFOV      = 65
SWEP.Spawnable			= false
SWEP.AdminSpawnable		= false

SWEP.PrintName			= "G36C"					// 'Nice' Weapon name (Shown on HUD)	
SWEP.Slot				= 3							// Slot in the weapon selection menu
SWEP.SlotPos			= 1							// Position in the slot
SWEP.Kind = WEAPON_HEAVY

SWEP.Primary.Sound = Sound("Weapon_MP5Navy.Single")
SWEP.Primary.Recoil		= 1.2
SWEP.Primary.Damage		= 9
SWEP.Primary.NumShots		= 1
SWEP.Primary.Cone			= 0.008
SWEP.Primary.Delay 		= 0.08

SWEP.Primary.ClipSize		= 30					// Size of a clip
SWEP.Primary.DefaultClip	= 30				// Default number of bullets in a clip
SWEP.Primary.Automatic		= true				// Automatic/Semi Auto
SWEP.Primary.Ammo = "pistol"

SWEP.Secondary.ClipSize		= -1					// Size of a clip
SWEP.Secondary.DefaultClip	= -1					// Default number of bullets in a clip
SWEP.Secondary.Automatic	= false				// Automatic/Semi Auto
SWEP.Secondary.Ammo		= "none"
SWEP.AmmoEnt = "item_ammo_pistol_ttt"
SWEP.AutoSpawnable = true
SWEP.IronSightsPos = Vector (-2.0235, -3.0003, 0.953)
SWEP.IronSightsAng = Vector (0.032, 0.0153, 0)


SWEP.Mode				= true

SWEP.Speed = 0.6
SWEP.Mass = 0.8
SWEP.WeaponName = "weapon_ins_sim_g36c"
SWEP.WeaponEntName = "weapon_ins_sim_ent_g36c"

