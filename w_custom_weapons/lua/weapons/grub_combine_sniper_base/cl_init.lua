
include('shared.lua')


SWEP.PrintName			= "Shot base"		-- 'Nice' Weapon name (Shown on HUD)	
SWEP.Slot				= 0						-- Slot in the weapon selection menu
SWEP.SlotPos			= 10					-- Position in the slot
SWEP.DrawAmmo			= true					-- Should draw the default HL2 ammo counter
SWEP.DrawCrosshair		= false 					-- Should draw the default crosshair
SWEP.DrawWeaponInfoBox	= true					-- Should draw the weapon info box
SWEP.BounceWeaponIcon   = true					-- Should the weapon icon bounce?
SWEP.SwayScale			= 1.0					-- The scale of the viewmodel sway
SWEP.BobScale			= 1.0					-- The scale of the viewmodel bob

SWEP.RenderGroup 		= RENDERGROUP_OPAQUE

-- Override this in your SWEP to set the icon in the weapon selection
SWEP.WepSelectIcon		= surface.GetTextureID( "weapons/swep" )

-- This is the corner of the speech bubble
SWEP.SpeechBubbleLid	= surface.GetTextureID( "gui/speech_lid" )

--[[---------------------------------------------------------
	You can draw to the HUD here - it will only draw when
	the client has the weapon deployed..
-----------------------------------------------------------]]
function SWEP:DrawHUD()
 
if self.Weapon:GetNWBool( "Ironsights", true ) then
         
      surface.SetDrawColor(0, 0, 0, 220)
      surface.SetTexture(surface.GetTextureID("jaanus/ep2snip_parascope"))
      surface.DrawTexturedRect(self.ParaScopeTable.x,self.ParaScopeTable.y,self.ParaScopeTable.w,self.ParaScopeTable.h)
         
         
       -- Draw the lens
       surface.SetDrawColor(20,20,20,40)
       surface.SetTexture(surface.GetTextureID("overlays/scope_lens"))
       surface.DrawTexturedRect(self.LensTable.x,self.LensTable.y,self.LensTable.w,self.LensTable.h)

       -- Draw the scope
       surface.SetDrawColor(0, 0, 0, 255)
       surface.SetTexture(surface.GetTextureID("jaanus/sniper_corner"))
       surface.DrawTexturedRectRotated(self.ScopeTable.x1,self.ScopeTable.y1,self.ScopeTable.l,self.ScopeTable.l,270)
       surface.DrawTexturedRectRotated(self.ScopeTable.x2,self.ScopeTable.y2,self.ScopeTable.l,self.ScopeTable.l,180)
       surface.DrawTexturedRectRotated(self.ScopeTable.x3,self.ScopeTable.y3,self.ScopeTable.l,self.ScopeTable.l,90)
       surface.DrawTexturedRectRotated(self.ScopeTable.x4,self.ScopeTable.y4,self.ScopeTable.l,self.ScopeTable.l,0)

       -- Fill in everything else
       surface.SetDrawColor(0,0,0,255)
       surface.DrawRect(self.QuadTable.x1,self.QuadTable.y1,self.QuadTable.w1,self.QuadTable.h1)
       surface.DrawRect(self.QuadTable.x2,self.QuadTable.y2,self.QuadTable.w2,self.QuadTable.h2)
       surface.DrawRect(self.QuadTable.x3,self.QuadTable.y3,self.QuadTable.w3,self.QuadTable.h3)
       surface.DrawRect(self.QuadTable.x4,self.QuadTable.y4,self.QuadTable.w4,self.QuadTable.h4) 


end
end


--[[---------------------------------------------------------
	Checks the objects before any action is taken
	This is to make sure that the entities haven't been removed
-----------------------------------------------------------]]
function SWEP:DrawWeaponSelection( x, y, wide, tall, alpha )
	
end


--[[---------------------------------------------------------
	This draws the weapon info box
-----------------------------------------------------------]]
function SWEP:PrintWeaponInfo( x, y, alpha )

end


--[[---------------------------------------------------------
   Name: SWEP:FreezeMovement()
   Desc: Return true to freeze moving the view
-----------------------------------------------------------]]
function SWEP:FreezeMovement()
	return false
end


--[[---------------------------------------------------------
   Name: SWEP:ViewModelDrawn( ViewModel )
   Desc: Called straight after the viewmodel has been drawn
-----------------------------------------------------------]]
function SWEP:ViewModelDrawn( ViewModel )
end


--[[---------------------------------------------------------
   Name: OnRestore
   Desc: Called immediately after a "load"
-----------------------------------------------------------]]
function SWEP:OnRestore()
end

--[[---------------------------------------------------------
   Name: OnRemove
   Desc: Called just before entity is deleted
-----------------------------------------------------------]]
function SWEP:OnRemove()
end

--[[---------------------------------------------------------
   Name: CustomAmmoDisplay
   Desc: Return a table
-----------------------------------------------------------]]
function SWEP:CustomAmmoDisplay()
end

--[[---------------------------------------------------------
   Name: GetViewModelPosition
   Desc: Allows you to re-position the view model
-----------------------------------------------------------]]

VmPos, VmAng = Vector(0,0,0), Angle(0,0,0)
WantedPos, WantedAng = Vector(0,0,0), Angle(0,0,0)
SetPos, SetAng = Vector(0,0,0), Angle(0,0,0)
VmSmooth = 0

function SWEP:GetViewModelPosition( pos, ang )
                       
	bIron = self:GetIronsight()
	Running = self:IsRunning()
	
	if bIron then
		self.SwayScale = 0.25
		self.BobScale = 0.25
	else
		self.SwayScale = 1
		self.BobScale = 1
	end
	
	if bIron and !Running then
		if self.IronSightsPos and self.IronSightsAng then
			SetPos = self.IronSightsPos
			SetAng = self.IronSightsAng
		end
		VmSmooth = self.IronsightTime * 30
	elseif Running and !bIron then
		if self.RunSightsPos and self.RunSightsAng then
			SetPos = self.RunSightsPos
			SetAng = self.RunSightsAng
		end
		VmSmooth = self.RunSightTime * 30
	elseif !bIron and !Running then
		SetPos = Vector(0,0,0)
		SetAng = Angle(0,0,0)
		VmSmooth = 5
	end

	WantedPos = LerpVector(FrameTime() * VmSmooth, WantedPos, SetPos)
	WantedAng = LerpAngle(FrameTime() * VmSmooth, WantedAng, SetAng)

	VmPos.x = WantedPos.x
	VmPos.y = WantedPos.y
	VmPos.z = WantedPos.z
   
	VmAng.p = WantedAng.p
	VmAng.y = WantedAng.y
	VmAng.r = WantedAng.r
   
	Right = ang:Right()
	Up = ang:Up()
	Forward = ang:Forward()
	
	ang = ang * 1
	ang:RotateAroundAxis( Right, VmAng.p )
	ang:RotateAroundAxis( Up, VmAng.y )
	ang:RotateAroundAxis( Forward, VmAng.r )      
   
	pos = pos + VmPos.x * Right
	pos = pos + VmPos.y * Forward
	pos = pos + VmPos.z * Up
   
	return pos, ang
       
end

--[[---------------------------------------------------------
   Name: TranslateFOV
   Desc: Allows the weapon to translate the player's FOV (clientside)
-----------------------------------------------------------]]
function SWEP:TranslateFOV( current_fov )
	
	return current_fov

end


--[[---------------------------------------------------------
   Name: DrawWorldModel
   Desc: Draws the world model (not the viewmodel)
-----------------------------------------------------------]]
function SWEP:DrawWorldModel()
	
	self.Weapon:DrawModel()

end


--[[---------------------------------------------------------
   Name: DrawWorldModelTranslucent
   Desc: Draws the world model (not the viewmodel)
-----------------------------------------------------------]]
function SWEP:DrawWorldModelTranslucent()
	
	self.Weapon:DrawModel()

end


--[[---------------------------------------------------------
   Name: AdjustMouseSensitivity()
   Desc: Allows you to adjust the mouse sensitivity.
-----------------------------------------------------------]]
function SWEP:AdjustMouseSensitivity()
     
    
		if self.Weapon:GetNWBool( "Ironsights", true ) then
        return (1/(self.Secondary.ScopeZoom/2))
        else
        return 1
        end
end

--[[---------------------------------------------------------
   Name: GetTracerOrigin()
   Desc: Allows you to override where the tracer comes from (in first person view)
		 returning anything but a vector indicates that you want the default action
-----------------------------------------------------------]]
function SWEP:GetTracerOrigin()

--[[
	local ply = self:GetOwner()
	local pos = ply:EyePos() + ply:EyeAngles():Right() * -5
	return pos
--]]

end

