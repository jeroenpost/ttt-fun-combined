


-- Variables that are used on both client and server
SWEP.Base = "weapon_tttbase"
SWEP.Category  		= "Twilight Sparkle`s SWEPs" -- the categorry where all your weapon from this vbase will be

SWEP.Author			= "Twilight Sparkle aka Residualgrub"
SWEP.Contact		= ""
SWEP.Purpose		= ""
SWEP.Instructions	= ""
SWEP.CSMuzzleFlashes    = true
SWEP.ViewModelFOV	= 62
SWEP.ViewModelFlip	= false
SWEP.ViewModel		= "models/weapons/v_pistol.mdl"
SWEP.WorldModel		= "models/weapons/w_357.mdl"
SWEP.AnimPrefix		= "python"
SWEP.HoldType 		= "pistol"

SWEP.Spawnable			= false
SWEP.AdminSpawnable		= false

SWEP.Primary.Sound			= Sound( "" )
SWEP.Primary.Recoil			= 0
SWEP.Primary.CurrentSpread 	= 0
SWEP.Primary.Damage			= 0
SWEP.Primary.NumShots		= 0
SWEP.Primary.Cone			= 0
SWEP.Primary.ClipSize		= -1
SWEP.Primary.RPM			= 0
SWEP.Primary.DefaultClip	= 0
SWEP.Primary.Automatic		= false
SWEP.Primary.Ammo			= "none"

SWEP.Secondary.ClipSize		= -1
SWEP.Secondary.DefaultClip	= -1
SWEP.Secondary.Automatic	= false
SWEP.Secondary.Ammo			= "none"

SWEP.ReloadSound 			= Sound("")

SWEP.ShellDelay = 0
SWEP.ShellEffect = "none"
SWEP.MuzzleEffect            = "none"
SWEP.MuzzleAttachment        = "1"
SWEP.ShellEjectAttachment    = "1"

SWEP.Secondary.ScopeZoom        = 5
SWEP.Secondary.UseACOG          = false
SWEP.Secondary.UseMilDot        = false    
SWEP.Secondary.UseSVD           = false
SWEP.Secondary.UseParabolic     = false
SWEP.Secondary.UseElcan         = false
SWEP.Secondary.UseGreenDuplex   = false
 
 
SWEP.ScopeScale             = 0.5
SWEP.ReticleScale           =0.5

SWEP.IronsightTime 			= 0.25
SWEP.RunSightTime 			= 0.15

SWEP.IronSightsPos 			= Vector(0,0,0)
SWEP.IronSightsAng 			= Angle(0,0,0)

SWEP.RunSightsPos 			= Vector(0,0,0)
SWEP.RunSightsAng 			= Angle(0,0,0)

--[[---------------------------------------------------------
   Name: SWEP:Initialize( )
   Desc: Called when the weapon is first loaded
-----------------------------------------------------------]]
function SWEP:Initialize()

if CLIENT then --Ripped the scope table from the original combine sniper.
	
		
		local iScreenWidth = surface.ScreenWidth()
		local iScreenHeight = surface.ScreenHeight()
		
		
		self.ScopeTable = {}
		self.ScopeTable.l = iScreenHeight*self.ScopeScale
		self.ScopeTable.x1 = 0.5*(iScreenWidth + self.ScopeTable.l)
		self.ScopeTable.y1 = 0.5*(iScreenHeight - self.ScopeTable.l)
		self.ScopeTable.x2 = self.ScopeTable.x1
		self.ScopeTable.y2 = 0.5*(iScreenHeight + self.ScopeTable.l)
		self.ScopeTable.x3 = 0.5*(iScreenWidth - self.ScopeTable.l)
		self.ScopeTable.y3 = self.ScopeTable.y2
		self.ScopeTable.x4 = self.ScopeTable.x3
		self.ScopeTable.y4 = self.ScopeTable.y1
				
		self.ParaScopeTable = {}
		self.ParaScopeTable.x = 0.5*iScreenWidth - self.ScopeTable.l
		self.ParaScopeTable.y = 0.5*iScreenHeight - self.ScopeTable.l
		self.ParaScopeTable.w = 2*self.ScopeTable.l
		self.ParaScopeTable.h = 2*self.ScopeTable.l
		
		self.ScopeTable.l = (iScreenHeight + 1)*self.ScopeScale

		self.QuadTable = {}
		self.QuadTable.x1 = 0
		self.QuadTable.y1 = 0
		self.QuadTable.w1 = iScreenWidth
		self.QuadTable.h1 = 0.5*iScreenHeight - self.ScopeTable.l
		self.QuadTable.x2 = 0
		self.QuadTable.y2 = 0.5*iScreenHeight + self.ScopeTable.l
		self.QuadTable.w2 = self.QuadTable.w1
		self.QuadTable.h2 = self.QuadTable.h1
		self.QuadTable.x3 = 0
		self.QuadTable.y3 = 0
		self.QuadTable.w3 = 0.5*iScreenWidth - self.ScopeTable.l
		self.QuadTable.h3 = iScreenHeight
		self.QuadTable.x4 = 0.5*iScreenWidth + self.ScopeTable.l
		self.QuadTable.y4 = 0
		self.QuadTable.w4 = self.QuadTable.w3
		self.QuadTable.h4 = self.QuadTable.h3

		self.LensTable = {}
		self.LensTable.x = self.QuadTable.w3
		self.LensTable.y = self.QuadTable.h1
		self.LensTable.w = 2*self.ScopeTable.l
		self.LensTable.h = 2*self.ScopeTable.l

		self.CrossHairTable = {}
		self.CrossHairTable.x11 = 0
		self.CrossHairTable.y11 = 0.5*iScreenHeight
		self.CrossHairTable.x12 = iScreenWidth
		self.CrossHairTable.y12 = self.CrossHairTable.y11
		self.CrossHairTable.x21 = 0.5*iScreenWidth
		self.CrossHairTable.y21 = 0
		self.CrossHairTable.x22 = 0.5*iScreenWidth
		self.CrossHairTable.y22 = iScreenHeight
		
		// Create a new table for every weapon instance
		self.VElements = table.FullCopy( self.VElements )
		self.WElements = table.FullCopy( self.WElements )
		self.ViewModelBoneMods = table.FullCopy( self.ViewModelBoneMods )

		self:CreateModels(self.VElements) // create viewmodels
		self:CreateModels(self.WElements) // create worldmodels
		
		// init view model bone build function
		if IsValid(self.Owner) then
			local vm = self.Owner:GetViewModel()
			if IsValid(vm) then
				self:ResetBonePositions(vm)
				
				// Init viewmodel visibility
				if (self.ShowViewModel == nil or self.ShowViewModel) then
					vm:SetColor(Color(255,255,255,255))
				else
					// we set the alpha to 1 instead of 0 because else ViewModelDrawn stops being called
					vm:SetColor(Color(255,255,255,1))
					// ^ stopped working in GMod 13 because you have to do Entity:SetRenderMode(1) for translucency to kick in
					// however for some reason the view model resets to render mode 0 every frame so we just apply a debug material to prevent it from drawing
					vm:SetMaterial("Debug/hsv")			
				end
			end
		end

	end
	self:SetHoldType( self.HoldType or "pistol" )
	self:ResetVar()
	self.Weapon:SetNWBool( "Ironsights", false )
	self.Weapon:SetNWBool( "boot", true )
	self:SetupDataTables() 
end

function SWEP:ResetVar() -- use this function when you need to make the waepon go back to the normal (example: deploy, holster) prevent a lot of problems
	self.dt.Ironsight = false
	self.dt.Running = false
	self.Primary.CurrentSpread = 0
end

function SWEP:SetupDataTables() 
	self.Weapon:DTVar("Bool", 0, "Ironsight")
	self.Weapon:DTVar("Bool", 1, "Running")
end

--[[---------------------------------------------------------
   Name: SWEP:Precache( )
   Desc: Use this function to precache stuff
-----------------------------------------------------------]]
function SWEP:Precache()
end


--[[---------------------------------------------------------
   Name: SWEP:PrimaryAttack( )
   Desc: +attack1 has been pressed
-----------------------------------------------------------]]
function SWEP:PrimaryAttack()

	-- Make sure we can shoot first
	if self:IsRunning() then
		return false
	end
if ( self.Weapon:Clip1() > 0 ) then
	self.Weapon:SetNextPrimaryFire(CurTime()+1/(self.Primary.RPM/60))
	self.Weapon:EmitSound(self.Primary.Sound)
	self:ShootBullet( self.Primary.Damage, self.Primary.Numshots, self.Primary.Recoil, self.Primary.Cone )
	self:TakePrimaryAmmo( 1 )
else
	self.Weapon:EmitSound("jaanus/ep2sniper_empty.wav")
end
end


--[[---------------------------------------------------------
   Name: SWEP:SecondaryAttack( )
   Desc: +attack2 has been pressed
-----------------------------------------------------------]]
function SWEP:SecondaryAttack()
	LastIron = !self:GetIronsight()
	self:SetIronsight(LastIron)
	self.Weapon:SetNextSecondaryFire(CurTime() + self.IronsightTime)
end

--[[---------------------------------------------------------
   Name: SWEP:CheckReload( )
   Desc: CheckReload
-----------------------------------------------------------]]
function SWEP:CheckReload()
	
end

--[[---------------------------------------------------------
   Name: SWEP:Reload( )
   Desc: Reload is being pressed
-----------------------------------------------------------]]
function SWEP:Reload()
	if self.Weapon:Clip1() >= self.Primary.ClipSize or self.Owner:GetAmmoCount(self.Primary.Ammo) <= 0 then return end
	self.Weapon:DefaultReload( ACT_VM_RELOAD );
	self.Weapon:EmitSound("jaanus/ep2sniper_reload.wav")
	self.Weapon:SetNWBool( "Ironsights", false )
	self:SetIronsight(false)
	if self.ReloadSound then
		self:EmitSound(self.ReloadSound)
	end
end


--[[---------------------------------------------------------
   Name: SWEP:Think( )
   Desc: Called every frame
-----------------------------------------------------------]]
function SWEP:Think()

	if !self:IsRunning() and self.Owner:KeyDown(IN_SPEED) and self.Owner:GetVelocity():Length() > self.Owner:GetWalkSpeed() then
		self:SetRunning(true)
		self:SetNextPrimaryFire(CurTime() + self.RunSightTime)
	elseif self:IsRunning() and !self.Owner:KeyDown(IN_SPEED) or self:IsRunning() and self.Owner:GetVelocity():Length() <= self.Owner:GetWalkSpeed() then
		self:SetRunning(false)
		self:SetNextPrimaryFire(CurTime() + self.RunSightTime)
	end

end


--[[---------------------------------------------------------
   Name: SWEP:Holster( weapon_to_swap_to )
   Desc: Weapon wants to holster
   RetV: Return true to allow the weapon to holster
-----------------------------------------------------------]]
function SWEP:Holster( wep )
	return true
end

--[[---------------------------------------------------------
   Name: SWEP:Deploy( )
   Desc: Whip it out
-----------------------------------------------------------]]
function SWEP:Deploy()
	if self.Weapon:GetNWBool( "boot", true ) then
		self.Weapon:EmitSound("HL1/fvox/bell.wav")
		timer.Simple( 1, function()  if not IsValid(self.Weapon) then return end self.Weapon:EmitSound("HL1/fvox/activated.WAV") end )
		timer.Simple( 2.5, function()  if not IsValid(self.Weapon) then return end self.Weapon:EmitSound("HL1/fvox/buzz.wav") end )
		timer.Simple( 3, function() if not IsValid(self.Weapon) then return end self.Weapon:EmitSound("HL1/fvox/power_restored.wav") end )
		self.Weapon:SetNWBool( "boot", false )
	end

	self.Weapon:SetNWBool( "Ironsights", false )
	self:ResetVar()
	return true
end

function SWEP:ShootEffects(recoil)

	self.Weapon:SendWeaponAnim( ACT_VM_PRIMARYATTACK ) 		-- View model animation
	self.Owner:MuzzleFlash()

    local fx         = EffectData()
    fx:SetEntity(self.Weapon)
    fx:SetOrigin(self.Owner:GetShootPos())
    fx:SetNormal(self.Owner:GetAimVector())
    fx:SetAttachment(self.MuzzleAttachment)
    util.Effect(self.MuzzleEffect,fx)        
    
    local effectdata = EffectData()
        effectdata:SetOrigin(self.Owner:GetShootPos())
        effectdata:SetEntity(self.Weapon)
        effectdata:SetStart(self.Owner:GetShootPos())
        effectdata:SetNormal(self.Owner:GetAimVector())
        effectdata:SetAttachment(1)

        
        timer.Simple(self.ShellDelay, function()
        if not IsValid(self) or !IsValid(self.Owner) then return end
        if not IsFirstTimePredicted() then return end
        if not self.Owner:IsNPC() and not self.Owner:Alive() then return end

        local fxs     = EffectData()
            fxs:SetEntity(self.Weapon)
            fxs:SetNormal(self.Owner:GetAimVector())
            fxs:SetAttachment(self.ShellEjectAttachment)

            util.Effect(self.ShellEffect,fxs)                
    end)
    
        local trace = self.Owner:GetEyeTrace()							-- Crappy muzzle light
	


	self.Owner:SetAnimation( PLAYER_ATTACK1 )	-- 3rd Person Animation
	
	SideRecoil = recoil / 1.5
	anglo = Angle(-recoil, math.Rand(-SideRecoil,SideRecoil), 0)
	self.Owner:ViewPunch(anglo)

end

function SWEP:GetIronsight()
	if self.dt.Ironsight then
		return self.dt.Ironsight
	end
	return false
end

function SWEP:SetIronsight(Bool)
	self.Weapon:SetNWBool( "Ironsights", b )
	self.dt.Ironsight = Bool
	if Bool then -- things will happen if Bool is true
	self.Weapon:SetNWBool( "Ironsights", true )
	self.Owner:SetFOV( 75/self.Secondary.ScopeZoom, 0.15 )
    self.Owner:DrawViewModel(false)
	--self.Owner:SetWalkSpeed( 110 )
	else -- things will happen if bool is false
	self.Weapon:SetNWBool( "Ironsights", false )
	self.Owner:SetFOV( 0, 0.2 )
    self.Owner:DrawViewModel(true)
   -- self.Owner:SetWalkSpeed( 250 )
	end
end

function SWEP:SetRunning(Bool)
	self.dt.Running = Bool
	if Bool then -- things will happen if Bool is true
	self.Weapon:SetNWBool( "Ironsights", false )
	self:SetIronsight(false)
	else -- things will happen if bool is false
	
	end
end

function SWEP:IsRunning()
	if self.dt.Running then
		self:SetHoldType( "passive" )
		return self.dt.Running
	end
	self:SetHoldType( self.HoldType or "pistol" )
	return false
end







--[[---------------------------------------------------------
   Name: SWEP:ShootBullet( )
   Desc: A convenience function to shoot bullets
-----------------------------------------------------------]]
function SWEP:ShootBullet( damage, num_bullets, recoil, aimcone )
	num_bullets = self.Primary.NumShots
	local bullet = {}
	bullet.Num 		= num_bullets
	bullet.Src 		= self.Owner:GetShootPos()			-- Source
	bullet.Dir 		= (self.Owner:GetAimVector():Angle() + self.Owner:GetPunchAngle()):Forward()			-- Dir of bullet
	bullet.Spread 	= Vector( aimcone, aimcone, 0 )		-- Aim Cone
	bullet.Tracer	= 5									-- Show a tracer on every x bullets 
	bullet.Force	= 1									-- Amount of force to give to phys objects
	bullet.Damage	= damage
	bullet.AmmoType = "Pistol"
	
	self.Owner:FireBullets( bullet )
	
	self:ShootEffects(recoil)
	
end


--[[---------------------------------------------------------
   Name: SWEP:TakePrimaryAmmo(   )
   Desc: A convenience function to remove ammo
-----------------------------------------------------------]]
function SWEP:TakePrimaryAmmo( num )
	
	-- Doesn't use clips
	if ( self.Weapon:Clip1() <= 0 ) then 
	
		if ( self:Ammo1() <= 0 ) then return end
		
		self.Owner:RemoveAmmo( num, self.Weapon:GetPrimaryAmmoType() )
	
	return end
	
	self.Weapon:SetClip1( self.Weapon:Clip1() - num )	
	
end


--[[---------------------------------------------------------
   Name: SWEP:TakeSecondaryAmmo(   )
   Desc: A convenience function to remove ammo
-----------------------------------------------------------]]
function SWEP:TakeSecondaryAmmo( num )
	
	-- Doesn't use clips
	if ( self.Weapon:Clip2() <= 0 ) then 
	
		if ( self:Ammo2() <= 0 ) then return end
		
		self.Owner:RemoveAmmo( num, self.Weapon:GetSecondaryAmmoType() )
	
	return end
	
	self.Weapon:SetClip2( self.Weapon:Clip2() - num )	
	
end


--[[---------------------------------------------------------
   Name: SWEP:CanPrimaryAttack( )
   Desc: Helper function for checking for no ammo
-----------------------------------------------------------]]
function SWEP:CanPrimaryAttack()

	if ( self.Weapon:Clip1() <= 0 ) then
	
		self:EmitSound( "Weapon_Pistol.Empty" )
		self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
		self:Reload()
		return false
		
	end
	
	if self:IsRunning() then
		return false
	end
	
	return true

end


--[[---------------------------------------------------------
   Name: SWEP:CanSecondaryAttack( )
   Desc: Helper function for checking for no ammo
-----------------------------------------------------------]]
function SWEP:CanSecondaryAttack()

	if ( self.Weapon:Clip2() <= 0 ) then
	
		self.Weapon:EmitSound( "Weapon_Pistol.Empty" )
		self.Weapon:SetNextSecondaryFire( CurTime() + 0.2 )
		return false
		
	end

	return true

end


--[[---------------------------------------------------------
   Name: OnRemove
   Desc: Called just before entity is deleted
-----------------------------------------------------------]]
function SWEP:OnRemove()
end


--[[---------------------------------------------------------
   Name: OwnerChanged
   Desc: When weapon is dropped or picked up by a new player
-----------------------------------------------------------]]
function SWEP:OwnerChanged()
end


--[[---------------------------------------------------------
   Name: Ammo1
   Desc: Returns how much of ammo1 the player has
-----------------------------------------------------------]]
function SWEP:Ammo1()
	return self.Owner:GetAmmoCount( self.Weapon:GetPrimaryAmmoType() )
end


--[[---------------------------------------------------------
   Name: Ammo2
   Desc: Returns how much of ammo2 the player has
-----------------------------------------------------------]]
function SWEP:Ammo2()
	return self.Owner:GetAmmoCount( self.Weapon:GetSecondaryAmmoType() )
end

--[[---------------------------------------------------------
   Name: SetDeploySpeed
   Desc: Sets the weapon deploy speed. 
		 This value needs to match on client and server.
-----------------------------------------------------------]]
function SWEP:SetDeploySpeed( speed )
	self.m_WeaponDeploySpeed = tonumber( speed )
end

--[[---------------------------------------------------------
   Name: DoImpactEffect
   Desc: Callback so the weapon can override the impact effects it makes
		 return true to not do the default thing - which is to call UTIL_ImpactTrace in c++
-----------------------------------------------------------]]
function SWEP:DoImpactEffect( tr, nDamageType )
		
	return false;
	
end


