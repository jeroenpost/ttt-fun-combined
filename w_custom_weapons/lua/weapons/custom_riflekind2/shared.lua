
if SERVER then
   AddCSLuaFile( "shared.lua" )

--resource.AddFile("models/w_rif_l2552.mdl")
--resource.AddFile("models/v_rif_le552.mdl")

--resource.AddFile("materials/models/weapons/v_models/rif_le552/rif_sg552.vmt")
--resource.AddFile("materials/models/weapons/v_models/rif_le552/rif_le552_ref.vtf")
--resource.AddFile("materials/models/weapons/v_models/rif_le552/rif_le552.vtf")
--resource.AddFile("materials/models/weapons/w_models/w_rif_le552/w_rif_le552.vtf")
--resource.AddFile("materials/models/weapons/w_models/w_rif_le552/w_rif_sg552.vmt")
--resource.AddFile("sound/weapons/le552.mp3")


end

SWEP.HoldType = "ar2"
SWEP.PrintName = "RifleKind"
SWEP.Slot = 2

if CLIENT then
   
   SWEP.Author = "GreenBlack"
   
   SWEP.Icon = "materials/vgui/ttt_fun_killicons/sg552.png"
end

SWEP.Base = "gb_base"
SWEP.Spawnable = true
SWEP.AdminSpawnable = true
SWEP.Kind = WEAPON_HEAVY

SWEP.Weight             = 5
SWEP.Primary.Delay = 0.11
SWEP.Primary.Recoil = 0.01
SWEP.Primary.Automatic = true
SWEP.Primary.Ammo = "Pistol"
SWEP.Primary.Damage = 13
SWEP.Primary.Cone = 0.018
SWEP.Primary.ClipSize = 60
SWEP.Primary.ClipMax = 160
SWEP.Primary.DefaultClip = 60
SWEP.AutoSpawnable      = false
SWEP.AmmoEnt = "item_ammo_pistol_ttt"
SWEP.ViewModel = "models/v_rif_le552.mdl"
SWEP.WorldModel = "models/w_rif_l2552.mdl"
SWEP.Primary.Sound = Sound("weapons/sg552/sg552-1.wav")
SWEP.Secondary.Sound = Sound("Default.Zoom")
SWEP.ShowWorldModel = true
SWEP.ViewModelBoneMods = {}


 


SWEP.HeadshotMultiplier = 2

SWEP.IronSightsPos = Vector( 4.377, -6.930, 0 )
SWEP.IronSightsAng = Vector( -8.754, 0, 0 )




function SWEP:Deploy()
   self.Weapon:EmitSound("weapons/le552.mp3")

end

function SWEP:SetZoom(state)
    if CLIENT or not IsValid(self.Owner)then
       return
    else
       if state then
          self.Owner:SetFOV(30, 0.3)
       else
          self.Owner:SetFOV(0, 0.2)
       end
    end
end

function SWEP:SecondaryAttack()
    if not self.IronSightsPos then return end
    if self.Weapon:GetNextSecondaryFire() > CurTime() then return end
    
    bIronsights = not self:GetIronsights()
    
    self:SetIronsights( bIronsights )
    
    if SERVER then
        self:SetZoom(bIronsights)
     else
        self:EmitSound(self.Secondary.Sound)
    end
    
    self.Weapon:SetNextSecondaryFire( CurTime() + 0.3)
end

function SWEP:PreDrop()
    self:SetZoom(false)
    self:SetIronsights(false)
    return self.BaseClass.PreDrop(self)
end

function SWEP:Reload()
    self.Weapon:DefaultReload( ACT_VM_RELOAD );
    self:SetIronsights( false )
    self:SetZoom(false)
end


function SWEP:Holster()
    self:SetIronsights(false)
    self:SetZoom(false)
    return true
end

if CLIENT then
   local scope = surface.GetTextureID("sprites/scope")
   function SWEP:DrawHUD()
      if self:GetIronsights() then
         surface.SetDrawColor( 0, 0, 0, 255 )
         
         local x = ScrW() / 2.0
         local y = ScrH() / 2.0
         local scope_size = ScrH()

         local gap = 80
         local length = scope_size
         surface.DrawLine( x - length, y, x - gap, y )
         surface.DrawLine( x + length, y, x + gap, y )
         surface.DrawLine( x, y - length, x, y - gap )
         surface.DrawLine( x, y + length, x, y + gap )

         gap = 0
         length = 50
         surface.DrawLine( x - length, y, x - gap, y )
         surface.DrawLine( x + length, y, x + gap, y )
         surface.DrawLine( x, y - length, x, y - gap )
         surface.DrawLine( x, y + length, x, y + gap )

         local sh = scope_size / 2
         local w = (x - sh) + 2
         surface.DrawRect(0, 0, w, scope_size)
         surface.DrawRect(x + sh - 2, 0, w, scope_size)

         surface.SetDrawColor(255, 0, 0, 255)
         surface.DrawLine(x, y, x + 1, y + 1)

         surface.SetTexture(scope)
         surface.SetDrawColor(255, 255, 255, 255)

         surface.DrawTexturedRectRotated(x, y, scope_size, scope_size, 0)

      else
         return self.BaseClass.DrawHUD(self)
      end
   end

   function SWEP:AdjustMouseSensitivity()
      return (self:GetIronsights() and 0.2) or nil
   end
end
