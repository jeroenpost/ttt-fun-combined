if SERVER then
   AddCSLuaFile( "shared.lua" )
end

SWEP.HoldType = "melee"
SWEP.PrintName = "Drone"
if CLIENT then


   SWEP.Slot = 6

   SWEP.ViewModelFOV  = 54
   SWEP.ViewModelFlip = false


   SWEP.Icon = "VGUI/ttt/icon_drone"
end

SWEP.Base = "weapon_tttbase"
SWEP.Primary.Recoil  = 0
SWEP.Primary.Damage = 0
SWEP.Primary.Delay = 0
SWEP.Primary.Cone = 0.01
SWEP.Primary.ClipSize = 1
SWEP.Primary.Automatic = false
SWEP.Primary.DefaultClip = 1
SWEP.Primary.ClipMax = 0

SWEP.Kind = WEAPON_EQUIP

SWEP.LimitedStock = true -- only buyable once
SWEP.WeaponID = -1

SWEP.NoNodes = false
SWEP.DroneName = "Drone"

SWEP.UseHands        = true
SWEP.ViewModel = Model("models/weapons/c_slam.mdl")
SWEP.WorldModel   = Model("models/weapons/w_slam.mdl")

SWEP.Primary.Ammo = "AR2AltFire"

if SERVER then
   --resource.AddFile("materials/VGUI/ttt/icon_drone.vmt")
end

function SWEP:Deploy()

   if(self.Owner.Launched) then

      self:TakePrimaryAmmo(1)
      self.NoNodes = true
      self.Drone = self.Owner.DDrone

   end

   if(!self.NoNodes) then
      local seq = self.Owner:GetViewModel():LookupSequence("tripmine_draw")
      self.Owner:GetViewModel():SetSequence(seq)
   else
      local seq = self.Owner:GetViewModel():LookupSequence("detonator_draw")
      self.Owner:GetViewModel():SetSequence(seq)
   end

   local vm = self.Owner:GetViewModel()

   timer.Simple(self.Owner:GetViewModel():SequenceDuration(),function() 

      if(vm != nil && vm:IsValid()) then

         if(!self.NoNodes) then
            local secB = vm:LookupSequence("tripmine_idle")
            vm:SetSequence(secB)
         else
            local secB = vm:LookupSequence("detonator_idle")
            vm:SetSequence(secB)
         end
      end

   end)

end


function SWEP:PrimaryAttack()

    if  IsValid(self.Owner.DDrone)   then
        if not self.Owner.DDrone.Use then
            self.Owner.DDrone = nil
        else
            self.Owner.DDrone:Use(self.Owner,self.Owner,SIMPLE_USE,1)
            return
        end
        return
    end

   if SERVER  then

      self:TakePrimaryAmmo(1)

      local seq = self.Owner:GetViewModel():LookupSequence("tripmine_attach1")
      self.Owner:GetViewModel():SetSequence(seq)

      local ply = self.Owner

      ply.Launched = true

      local droneent = ents.Create("sent_drone")
      droneent:SetPos(ply:GetPos() + ply:GetForward()*32 + Vector(0,0,math.Clamp(48,48,150)))
      droneent:Spawn()
      droneent.Owner = ply
      droneent.OnTTT = true
      
      self.Drone = droneent
      self.Owner.DDrone = droneent

      timer.Simple(0.1,function()

      droneent:AddGun(true)

      for k=1,4 do

         local it = math.random(1,6)


            droneent:AddFlash(true)

            droneent:AddFuel(true)

            droneent:AddSpeed(true)

            droneent:AddAnthena(true)


      end

      end)

      local zm = self

      timer.Simple(zm.Owner:GetViewModel():SequenceDuration(),function()
          if not IsValid(zm.Owner) or not zm.Owner:GetViewModel() then return end
         local seq = zm.Owner:GetViewModel():LookupSequence("detonator_draw")
         zm.Owner:GetViewModel():SetSequence(seq)

         //zm.Owner:ConCommand("ttt_dropweapon")
      end)

   end
end


function SWEP:SecondaryAttack()

   local seq = self.Owner:GetViewModel():LookupSequence("detonator_detonate")
   self.Owner:GetViewModel():SetSequence(seq)

   if(self.Drone != nil && self.Drone:IsValid()) and SERVER then

      net.Start("RemoveCModels")
      net.WriteEntity(self.Drone)
      net.Broadcast()

      self.Drone:Explode()
      self.Drone:Remove()

      self.NoNodes = true

   end

end

hook.Add("PlayerSpawn","FixRe-Slam",function(ply)

   ply.Launched = false

end)