
if SERVER then
    AddCSLuaFile("shared.lua")
    util.AddNetworkString("crimson_startsmoke")
    --resource.AddFile("materials/vgui/ttt_fun_killicons/tmp.png")
end

SWEP.Gun = ("custmo_hsmp")

SWEP.HoldType = "ar2"
SWEP.PrintName = "Silent PDW"
SWEP.Slot = 2
if CLIENT then


    SWEP.Icon = "materials/vgui/ttt_fun_killicons/tmp.png"
end

SWEP.Base				= "psychobot_gun_base"

SWEP.Primary.Delay = 0.12
SWEP.Primary.Recoil = 0.01
SWEP.Primary.Automatic = true
SWEP.Primary.Ammo = "Pistol"
SWEP.Primary.Damage = 10
SWEP.Primary.Cone = 0.018
SWEP.Primary.ClipSize = 60
SWEP.Primary.ClipMax = 160
SWEP.Primary.DefaultClip = 99999
SWEP.AutoSpawnable      = false
SWEP.Primary.Automatic = true
SWEP.Primary.NumShots = 1
SWEP.Primary.RPM			= 500

SWEP.AutoSpawnable = false
SWEP.Kind = WEAPON_NADE
SWEP.HeadshotMultiplier = 1.3

SWEP.AmmoEnt = "item_ammo_smg1_ttt"

SWEP.IsSilent = true

SWEP.ViewModel = "models/weapons/v_smg_kac.mdl"
SWEP.WorldModel = "models/weapons/w_smg_kac.mdl"
SWEP.ViewModelFlip = false
SWEP.ViewModelFOV = 65

SWEP.Primary.Sound = "weapons/g36c/sil.mp3"
SWEP.Primary.SoundLevel = 1


SWEP.IronSightsPos = Vector(-2.685, -5.119, 0.2)
SWEP.IronSightsAng = Vector(0.275, 0, 0)

SWEP.NextSmoke = 0
SWEP.Bastardgas = false
SWEP.NextThink = 0
SWEP.hadSmokes = 0

SWEP.Offset = {
    Pos = {
        Up = 0,
        Right = 1,
        Forward = -4.5,
    },
    Ang = {
        Up = 0,
        Right = 04,
        Forward = 0,
    }
}

function SWEP:PrimaryAttack(worldsnd)

    self.Weapon:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )

    if not self:CanPrimaryAttack() then return end



    self:ShootBullet( self.Primary.Damage, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )

    self:TakePrimaryAmmo( 1 )

    local owner = self.Owner
    if not IsValid(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

    owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )
end

function SWEP:DrawWorldModel( )
    local hand, offset, rotate

    if not IsValid( self.Owner ) then
        self:DrawModel( )
        return
    end

    if not self.Hand then
        self.Hand = self.Owner:LookupAttachment( "anim_attachment_rh" )
    end

    hand = self.Owner:GetAttachment( self.Hand )

    if not hand then
        self:DrawModel( )
        return
    end

    offset = hand.Ang:Right( ) * self.Offset.Pos.Right + hand.Ang:Forward( ) * self.Offset.Pos.Forward + hand.Ang:Up( ) * self.Offset.Pos.Up

    hand.Ang:RotateAroundAxis( hand.Ang:Right( ), self.Offset.Ang.Right )
    hand.Ang:RotateAroundAxis( hand.Ang:Forward( ), self.Offset.Ang.Forward )
    hand.Ang:RotateAroundAxis( hand.Ang:Up( ), self.Offset.Ang.Up )

    self:SetRenderOrigin( hand.Pos + offset )
    self:SetRenderAngles( hand.Ang )

    self:DrawModel( )
end

function SWEP:Think()

   -- if SERVER then
   --     if  self.Weapon:GetNWBool( "doit" ) then
   --         self:revive()
   --         self.Weapon:SetNWBool( "doit", false )
   --         end
   -- end

   -- if CLIENT then
    --    if IsValid(self.Owner) and CLIENT and (input.IsKeyDown(KEY_X)) then
    --        self.Weapon:SetNWBool( "doit", true )
            -- self:MakeSmokeCrim()
    --    end
    --end
end


function SWEP:revive()
    if self.NextSmoke < CurTime() and self.hadSmokes < 1 then



        local tr = util.TraceLine({ start = self.Owner:EyePos(), endpos = self.Owner:EyePos() + self.Owner:EyeAngles():Forward() * 80, filter = self.Owner })
        if tr.HitNonWorld and IsValid(tr.Entity) and tr.Entity:GetClass() == "prop_ragdoll" and tr.Entity.player_ragdoll then
            local mins, maxs = self.Owner:OBBMins(), self.Owner:OBBMaxs()
            local tr2 = util.TraceHull({start = tr.Entity:LocalToWorld(tr.Entity:OBBCenter()) + vector_up, endpos = tr.Entity:LocalToWorld(tr.Entity:OBBCenter()) + Vector(0, 0, 80), filter = {tr.Entity, self.Owner}, mins = Vector(mins.x * 1.6, mins.y * 1.6, 0), maxs = Vector(maxs.x * 1.6, maxs.y * 1.6, 0), mask = MASK_PLAYERSOLID})
            if tr2.Hit then
                self.Owner:PrintMessage( HUD_PRINTCENTER, "There is no room to bring him back here" )
                return
            end



            self.NextSmoke = CurTime() + 5
            self.hadSmokes = self.hadSmokes + 1
            if SERVER then
                rag = tr.Entity
                local ply = player.GetByUniqueID(rag.uqid)
                local credits = CORPSE.GetCredits(rag, 0)
                if not IsValid(ply) then return end
                if SERVER and _globals['slayed'][ply:SteamID()] then self.Owner:PrintMessage( HUD_PRINTCENTER, "Can't revive, person was slayed" )
                    ply:Kill();
                    return
                end
                if SERVER and ((rag.was_role == ROLE_TRAITOR and self.Owner:GetRole() ~= ROLE_TRAITOR) or (rag.was_role ~= ROLE_TRAITOR and self.Owner:GetRole() == ROLE_TRAITOR))  then
                    DamageLog("Defib: " .. self.Owner:Nick() .. " [" .. self.Owner:GetRoleString() .. "] Died defibbing "..ply:Nick().." [" .. ply:GetRoleString() .. "] ")
                    self.Owner:PrintMessage( HUD_PRINTCENTER, ply:Nick().." was of the other team,  sucked up your soul and killed you" )
                    self.Owner:Kill();

                    return
                end
                DamageLog("Defib: " .. self.Owner:Nick() .. " [" .. self.Owner:GetRoleString() .. "] defibbed "..ply:Nick().." [" .. ply:GetRoleString() .. "] ")

                ply:SpawnForRound(true)
                ply:SetCredits(credits)
                ply:SetPos(rag:LocalToWorld(rag:OBBCenter()) + vector_up / 2) --Just in case he gets stuck somewhere
                ply:SetEyeAngles(Angle(0, rag:GetAngles().y, 0))
                ply:SetCollisionGroup(COLLISION_GROUP_WEAPON)
                timer.Simple(2, function() ply:SetCollisionGroup(COLLISION_GROUP_PLAYER) end)
                umsg.Start("_resurrectbody")
                umsg.Entity(ply)
                umsg.Bool(CORPSE.GetFound(rag, false))
                umsg.End()
                rag:Remove()
            end
        end
    end
end



function SWEP:MakeSmokeCrim()

    if self.NextSmoke > CurTime() then return end
    self.Weapon:EmitSound(Sound("BaseSmokeEffect.Sound"))
    self.NextSmoke = CurTime() + 5
    self.hadSmokes = self.hadSmokes + 1

    if CLIENT then return end
    local ent = ents.Create( "ttt_smokegrenade_proj" )

    ent:SetPos( self.Owner:GetPos() )
    ent:SetOwner( self.Owner  )
    ent:SetPhysicsAttacker(  self.Owner )
    ent.Owner = self.Owner
    ent:SetDetonateExact(0)
    ent:Spawn()
    ent:SetDetonateExact(3)
   -- ent:CreateSmoke( self.Owner:GetPos())
    ent.healdamage = 0
    ent.smokebombexplode = true
    ent.timer = CurTime() + 0.01
    ent.EndColor = "50 50 50"
    ent.StartColor = "50 50 50"


end




function SWEP:SetZoom(state)
    if CLIENT or not IsValid(self.Owner) then return end

    if state then
        self.Owner:SetFOV(35, 0.5)
        self.Primary.Cone = 0.024
        self.Primary.Recoil = 0.9
    else
        self.Owner:SetFOV(0, 0.2)
        self.Primary.Cone = 0.04
        self.Primary.Recoil = 1.2
    end
end


function SWEP:OnDrop()
    self:Remove()
end

-- Add some zoom to ironsights for this gun
function SWEP:SecondaryAttack()
    if not self.IronSightsPos then return end
    if self.Weapon:GetNextSecondaryFire() > CurTime() then return end

    bIronsights = not self:GetIronsights()

    self:SetIronsights(bIronsights)

    if SERVER then
        self:SetZoom(bIronsights)
    end

    self.Weapon:SetNextSecondaryFire(CurTime() + 0.3)
end

function SWEP:PreDrop()
    self:SetZoom(false)
    self:SetIronsights(false)
    return self.BaseClass.PreDrop(self)
end

function SWEP:Reload()
    if self.Owner:KeyDown(IN_USE) then
        self:revive()
        return
    end
    if self.Owner:KeyDown(IN_ATTACK2) then
        self:MakeSmokeCrim()
        return

    end
    self.Weapon:DefaultReload(ACT_VM_RELOAD);
    self:SetIronsights(false)
    self:SetZoom(false)
end


function SWEP:Holster()
    self:SetIronsights(false)
    self:SetZoom(false)
    return true
end


if CLIENT then
    local scope = surface.GetTextureID("sprites/scope")
    function SWEP:DrawHUD()
        if self:GetIronsights() then
            surface.SetDrawColor( 0, 0, 0, 255 )

            local x = ScrW() / 2.0
            local y = ScrH() / 2.0
            local scope_size = ScrH()

            -- crosshair
            local gap = 80
            local length = scope_size
            surface.DrawLine( x - length, y, x - gap, y )
            surface.DrawLine( x + length, y, x + gap, y )
            surface.DrawLine( x, y - length, x, y - gap )
            surface.DrawLine( x, y + length, x, y + gap )

            gap = 0
            length = 50
            surface.DrawLine( x - length, y, x - gap, y )
            surface.DrawLine( x + length, y, x + gap, y )
            surface.DrawLine( x, y - length, x, y - gap )
            surface.DrawLine( x, y + length, x, y + gap )


            -- cover edges
            local sh = scope_size / 2
            local w = (x - sh) + 2
            surface.DrawRect(0, 0, w, scope_size)
            surface.DrawRect(x + sh - 2, 0, w, scope_size)

            surface.SetDrawColor(255, 0, 0, 255)
            surface.DrawLine(x, y, x + 1, y + 1)

            -- scope
            surface.SetTexture(scope)
            surface.SetDrawColor(255, 255, 255, 255)

            surface.DrawTexturedRectRotated(x, y, scope_size, scope_size, 0)

        else
            return self.BaseClass.DrawHUD(self)
        end
    end

    function SWEP:AdjustMouseSensitivity()
        return (self:GetIronsights() and 0.2) or nil
    end
end


