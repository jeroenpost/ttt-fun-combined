if SERVER then
	AddCSLuaFile("shared.lua")
end
-- BELOGNS TO THE DRONE
if CLIENT then
	SWEP.PrintName = "Laser wep"
	SWEP.Slot = 0
	SWEP.SlotPos = 5
	SWEP.DrawAmmo = false
	SWEP.DrawCrosshair = true
end

SWEP.Base = "weapon_tttbase"

SWEP.Author = "Gonzalolog"
SWEP.Instructions = "Left click to discipline, right click to kill"
SWEP.Contact = ""
SWEP.Purpose = ""
SWEP.IconLetter = ""

SWEP.ViewModelFOV = 62
SWEP.ViewModelFlip = false
SWEP.AnimPrefix = "pistol"

SWEP.Spawnable = true
SWEP.AdminSpawnable = true
SWEP.Category = "DarkRP (Utility)"

SWEP.ViewModel = Model("models/weapons/c_pistol.mdl")
SWEP.WorldModel = Model("models/weapons/w_pistol.mdl")

SWEP.Primary.ClipSize = -1
SWEP.Primary.DefaultClip = 0
SWEP.Primary.Automatic = false
SWEP.Primary.Ammo = ""

SWEP.Secondary.ClipSize = -1
SWEP.Secondary.DefaultClip = 0
SWEP.Secondary.Automatic = false
SWEP.Secondary.Ammo = ""

function SWEP:Initialize()
	self:SetHoldType("normal")

end

function SWEP:Deploy()
	
end

SWEP.bot = nil

function SWEP:SecondaryAttack()
	
	if(self.bot != nil && self.bot:IsValid()) then

		self.bot.target = nil

	end

end

function SWEP:PrimaryAttack()
	if(self.bot == nil || !self.bot:IsValid()) then

		local tr = self.Owner:GetEyeTrace()

		if(tr.Entity != nil && tr.Entity:IsValid()) then

			if(tr.Entity.Owner == self.Owner) then

				self.bot = tr.Entity

				local phys = tr.Entity:GetPhysicsObject()
				if (phys:IsValid()) then
					phys:Wake()
					phys:EnableMotion( true )
				end

				tr.Entity:StartMotionController()

			end

		end

	elseif(self.bot != nil && self.bot:IsValid() && self.bot.Owner == self.Owner) then

		local tr = self.Owner:GetEyeTrace()

		if(tr.HitPos != nil) then

			self.bot.target = tr.HitPos

		end

	end
end
