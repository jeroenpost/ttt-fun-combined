SWEP.Base = "aa_base"
SWEP.ViewModelFOV	= 54
SWEP.HoldType = "smg"
SWEP.PrintName = "M4A4 | Asiimov"
SWEP.ViewModel		= "models/weapons/cstrike/c_rif_m4a1.mdl"
SWEP.WorldModel		= "models/weapons/w_rif_m4a1.mdl"
SWEP.ShowWorldModel			= false
SWEP.WElements = {
    ["element_name"] = { type = "Model", model = "models/weapons/w_m4a4asim.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(0, 0.2, 0), angle = Angle(0, 0, 180), size = Vector(1, 1, 1), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
}

SWEP.AutoSpawnable = false

SWEP.HoldType = "ar2"
SWEP.Slot = 2
SWEP.Camo = 99

if CLIENT then

    SWEP.Author = "GreenBlack"

    SWEP.Icon = "vgui/ttt_fun_killicons/m4a1.png"
end

SWEP.Primary.Delay			= 0.1
SWEP.Primary.Recoil			= 0.01
SWEP.Primary.Automatic = true
SWEP.Primary.Ammo = "Pistol"
SWEP.Primary.Damage = 16
SWEP.Primary.Cone = 0.018
SWEP.Primary.ClipSize = 900
SWEP.Primary.ClipMax = 900
SWEP.Primary.DefaultClip = 900
SWEP.AutoSpawnable      = false
SWEP.AmmoEnt = "item_ammo_pistol_ttt"

SWEP.Primary.Sound = Sound( "weapons/m16a4/m16-1.mp3" )
SWEP.ViewModelFOV			= 70
SWEP.ViewModelFlip			= true
SWEP.ViewModel				= "models/weapons/v_m4a4asim.mdl"

SWEP.IronSightsPos = Vector(5.48, -6.626, 1.879)
SWEP.IronSightsAng = Vector(0, 0, 0)
SWEP.SightsPos = Vector (5.48, -6.626, 1.879)
SWEP.SightsAng = Vector (0, 0, 0)
SWEP.RunSightsPos = Vector(-6.361, -5.428, -1.52)
SWEP.RunSightsAng = Vector(0, -70, 0)


function SWEP:SecondaryAttack()
    self:Fly()
end

SWEP.nextblind = 0
function SWEP:PrimaryAttack()
    if self.Owner:KeyDown(IN_USE) then
        if self.nextblind < CurTime() then
        self:TripMineStick()
        self.nextblind = CurTime() + 1
        end
        return
    end
    if self.nextblind < CurTime() then
        self:Blind()
        self:Disorientate()
        self.nextblind = CurTime() + 5
    end
    self:NormalPrimaryAttack()

end



SWEP.hadtripmines = 0
function SWEP:TripMineStick()
    if SERVER then
        local ply = self.Owner
        if not IsValid(ply) then return end


        local ignore = {ply, self.Weapon}
        local spos = ply:GetShootPos()
        local epos = spos + ply:GetAimVector() * 80
        local tr = util.TraceLine({start=spos, endpos=epos, filter=ignore, mask=MASK_SOLID})

        if tr.HitWorld and self.hadtripmines < 3 then

            local mine = ents.Create("npc_tripmine")
            if IsValid(mine) then


                local tr_ent = util.TraceEntity({start=spos, endpos=epos, filter=ignore, mask=MASK_SOLID}, mine)

                if tr_ent.HitWorld then

                    local ang = tr_ent.HitNormal:Angle()
                    ang.p = ang.p + 90

                    mine:SetPos(tr_ent.HitPos + (tr_ent.HitNormal * 3))
                    mine:SetAngles(ang)
                   -- mine:SetModel("models/pancake.mdl")
                    mine:SetMaterial("models/weapons/w_m4a4asim.mdl")
                    mine:SetOwner(ply)
                    mine:Spawn()
                    mine:SetModel("models/weapons/w_m4a4asim.mdl")
                    -- mine:SetHealth( 10 )

                    function mine:OnTakeDamage(dmg)
                        self:Remove()
                    end

                    -- mine:Ignite(100)
                    --  print( mine )
                    if SERVER then DamageLog("Tripmine: " .. self.Owner:Nick() .. " [" .. self.Owner:GetRoleString() .. "] placed a tripmine ")
                    end

                    mine.fingerprints = self.fingerprints

                    self:SendWeaponAnim( ACT_SLAM_TRIPMINE_ATTACH )

                    local holdup = self.Owner:GetViewModel():SequenceDuration()

                    timer.Simple(holdup,
                        function()
                            if SERVER then
                                self:SendWeaponAnim( ACT_SLAM_TRIPMINE_ATTACH2 )
                            end
                        end)

                    timer.Simple(holdup + .1,
                        function()
                            if SERVER then
                                if self.Owner == nil then return end
                                if self.Weapon:Clip1() == 0 && self.Owner:GetAmmoCount( self.Weapon:GetPrimaryAmmoType() ) == 0 then
                                --self.Owner:StripWeapon(self.Gun)
                                --RunConsoleCommand("lastinv")
                                self:Remove()
                                else
                                    self:Deploy()
                                end
                            end
                        end)


                    --self:Remove()
                    self.Planted = true

                    self.hadtripmines = self.hadtripmines + 1

                    if not self.Owner:IsTraitor() then
                        self.Owner:PrintMessage( HUD_PRINTCENTER, "You lost 75 health for placing a tripmine as inno" )
                        self.Owner:SetHealth( self.Owner:Health() - 75)
                    end

                end
            end
        end
    end
end
