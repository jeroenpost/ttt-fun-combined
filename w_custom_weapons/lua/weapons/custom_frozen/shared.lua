

SWEP.PrintName				= "Black's Frosted Fury"
SWEP.Slot				= 3
SWEP.SlotPos				= 3			
SWEP.DrawAmmo				= true		
SWEP.DrawWeaponInfoBox			= false		
SWEP.BounceWeaponIcon   		= 	false	
SWEP.DrawCrosshair			= true		
SWEP.Weight				= 30			
SWEP.AutoSwitchTo			= true		
SWEP.AutoSwitchFrom			= true		
SWEP.HoldType 				= "ar2"		

SWEP.ViewModelFOV			= 70
SWEP.ViewModelFlip			= true
SWEP.ViewModel				= "models/weapons/v_rif_fury.mdl"	
SWEP.WorldModel				= "models/weapons/w_frozen_acr.mdl"
SWEP.Base				= "weapon_tttbase"
SWEP.Spawnable				= true
SWEP.AdminSpawnable			= true

SWEP.Kind = WEAPON_NADE
SWEP.Primary.SoundLevel  = 33
SWEP.Primary.Sound			= Sound( "Weapon_M4A1.Silenced")
SWEP.Primary.Delay			= 0.15
SWEP.Primary.ClipSize			= 250
SWEP.Primary.DefaultClip		= 250
SWEP.Primary.Automatic			= true		
SWEP.Primary.Ammo			= "ar2"		
SWEP.AmmoEnt = "item_ammo_smg1_ttt"
SWEP.Primary.ClipMax = 250
SWEP.Secondary.IronFOV			= 55			


SWEP.Primary.NumShots	= 1		
SWEP.Primary.Damage		= 20
SWEP.Primary.Cone		= .025	
SWEP.Primary.IronAccuracy = .015 


SWEP.IronSightsPos = Vector(2.668, 0, 0.675)
SWEP.IronSightsAng = Vector(0, 0, 0)


function SWEP:ShootBullet( dmg, recoil, numbul, cone )

   self.Weapon:SendWeaponAnim(self.PrimaryAnim)

   self.Owner:MuzzleFlash()
   self.Owner:SetAnimation( PLAYER_ATTACK1 )

   if not IsFirstTimePredicted() then return end

   local sights = self:GetIronsights()

   numbul = numbul or 1
   cone   = cone   or 0.01

   local bullet = {}
   bullet.Num    = numbul
   bullet.Src    = self.Owner:GetShootPos()
   bullet.Dir    = self.Owner:GetAimVector()
   bullet.Spread = Vector( cone, cone, 0 )
      bullet.Tracer = 1
   bullet.TracerName = "AirboatGunHeavyTracer"
   bullet.Force  = 10
   bullet.Damage = dmg


   self.Owner:FireBullets( bullet )

   -- Owner can die after firebullets
   if (not IsValid(self.Owner)) or (not self.Owner:Alive()) or self.Owner:IsNPC() then return end

   if ((game.SinglePlayer() and SERVER) or
       ((not game.SinglePlayer()) and CLIENT and IsFirstTimePredicted())) then

      -- reduce recoil if ironsighting
      recoil = sights and (recoil * 0.6) or recoil

      local eyeang = self.Owner:EyeAngles()
      eyeang.pitch = eyeang.pitch - 0
      self.Owner:SetEyeAngles( eyeang )
   end

end

