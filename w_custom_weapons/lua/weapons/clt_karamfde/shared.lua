-- Variables that are used on both client and server
SWEP.Gun = ("clt_karamfde") -- must be the name of your swep but NO CAPITALS!
SWEP.Category				= "Clout's Karambit Knives"
SWEP.Author				= ""
SWEP.Contact				= ""
SWEP.Purpose				= ""
SWEP.Instructions				= ""
SWEP.PrintName				= "Karambit - Fade"	
SWEP.Slot				= 9
SWEP.HasFlare = true
SWEP.SlotPos				= 27			
SWEP.DrawAmmo				= true		
SWEP.DrawWeaponInfoBox			= false		
SWEP.BounceWeaponIcon   		= 	false	
SWEP.DrawCrosshair			= false		
SWEP.Weight				= 30			
SWEP.AutoSwitchTo			= true		
SWEP.AutoSwitchFrom			= true		
SWEP.HoldType 				= "knife"	

SWEP.ViewModelFOV			= 70
SWEP.ViewModelFlip			= false
SWEP.ViewModel				= "models/weapons/karamfade.mdl"	-- Weapon view model
SWEP.WorldModel				= "models/weapons/w_knife_t.mdl"	-- Weapon world model
SWEP.ShowWorldModel			= true
SWEP.Base				= "clout_gun_base"
SWEP.Spawnable				= true
SWEP.UseHands = true
SWEP.AdminSpawnable			= true
SWEP.FiresUnderwater = false
SWEP.Kind = 200
SWEP.Primary.RPM			= 100			-- This is in Rounds Per Minute
SWEP.Primary.ClipSize			= 30		-- Size of a clip
SWEP.Primary.DefaultClip		= 60		-- Bullets you start with
SWEP.Primary.KickUp				= 0.4		-- Maximum up recoil (rise)
SWEP.Primary.KickDown			= 0.3		-- Maximum down recoil (skeet)
SWEP.Primary.KickHorizontal		= 0.3		-- Maximum up recoil (stock)
SWEP.Primary.Automatic			= false		-- Automatic = true; Semi Auto = false
SWEP.Primary.Ammo			= ""			-- pistol, 357, smg1, ar2, buckshot, slam, SniperPenetratedRound, AirboatGun
-- Pistol, buckshot, and slam always ricochet. Use AirboatGun for a light metal peircing shotgun pellets

SWEP.Secondary.IronFOV			= 55		-- How much you 'zoom' in. Less is more! 	

SWEP.data 				= {}				--The starting firemode
SWEP.data.ironsights			= 1

SWEP.Primary.Damage		= 30	-- Base damage per bullet
SWEP.Primary.Spread		= .02	-- Define from-the-hip accuracy 1 is terrible, .0001 is exact)
SWEP.Primary.IronAccuracy = .01 -- Ironsight accuracy, should be the same for shotguns
SWEP.RunSightsPos = Vector(0, 0, 0)
SWEP.RunSightsAng = Vector(-25.577, 0, 0)

SWEP.Slash = 1

SWEP.Primary.Sound	= Sound("Weapon_Knife.Slash") //woosh
SWEP.KnifeShink = ("Weapon_Knife.HitWall")
SWEP.KnifeSlash = ("Weapon_Knife.Hit")
SWEP.KnifeStab = ("Weapon_Knife.Stab")


function SWEP:Deploy()
	self:SetHoldType(self.HoldType)
	self.Weapon:SendWeaponAnim( ACT_VM_DRAW )
	self.Weapon:SetNextPrimaryFire(CurTime() + 1)

	self.Weapon:EmitSound("weapons/knife/knife_deploy1.wav", 50, 100)
	return true
end

SWEP.nextshoot = 0
function SWEP:PrimaryAttack()
    if self.Owner:KeyDown(IN_USE) then
        self:Fly()
        return
    end
    if self:CanPrimaryAttack() and self.nextshoot < CurTime() then
        self.nextshoot = CurTime() + 0.7
    end
	vm = self.Owner:GetViewModel()
	if SERVER and self:CanPrimaryAttack() and self.Owner:IsPlayer() then

        bullet = {}
        bullet.Src = self.Owner:GetShootPos()
        bullet.Dir = self.Owner:GetAimVector()
        bullet.Spread=Vector(0.08,0.08,0)
        bullet.Tracer=0
        bullet.Num=10
        bullet.Force= 12
        bullet.Attacker = self:GetOwner()
        bullet.Damage= 4

        self:FireBullets(bullet)

	self.Weapon:SendWeaponAnim( ACT_VM_IDLE )
		if !self.Owner:KeyDown(IN_RELOAD) then
			if self.Slash == 1 then
				vm:SetSequence(vm:LookupSequence("midslash1"))
				self.Slash = 2
			else
				vm:SetSequence(vm:LookupSequence("midslash2"))
				self.Slash = 1
			end --if it looks stupid but works, it aint stupid!
			self.Weapon:EmitSound(self.Primary.Sound)//slash in the wind sound here


					self:PrimarySlash() 


			self.Owner:SetAnimation( PLAYER_ATTACK1 )
			self.Weapon:SetNextPrimaryFire(CurTime()+1/(self.Primary.RPM/35))
		end
	end
end

function SWEP:PrimarySlash()

	pos = self.Owner:GetShootPos()
	ang = self.Owner:GetAimVector()
	damagedice = math.Rand(.85,1.25)
--	pain = self.Primary.Damage * damagedice
    pain = 5
	self.Owner:LagCompensation(true)
	if IsValid(self.Owner) and IsValid(self.Weapon) then
		if self.Owner:Alive() then if true then




			local slash = {}
			slash.start = pos
			slash.endpos = pos + (ang * 64)
			slash.filter = self.Owner
			slash.mins = Vector(-160, -150, 0)
			slash.maxs = Vector(150, 150, 50)
			local slashtrace = util.TraceHull(slash)
			if slashtrace.Hit then
				if slashtrace.Entity == nil then return end
				targ = slashtrace.Entity
				if targ:IsPlayer() or targ:IsNPC() then
					//find a way to splash a little blood
					self.Weapon:EmitSound(self.KnifeSlash)//stab noise
					paininfo = DamageInfo()
					paininfo:SetDamage(pain)
					paininfo:SetDamageType(DMG_SLASH)
					paininfo:SetAttacker(self.Owner)
					paininfo:SetInflictor(self.Weapon)
					paininfo:SetDamageForce(slashtrace.Normal *35000)
					if SERVER then targ:TakeDamageInfo(paininfo) end
					targ:Ignite(0.3,1)
				else
					self.Weapon:EmitSound(self.KnifeShink)//SHINK!
					look = self.Owner:GetEyeTrace()
					util.Decal("ManhackCut", look.HitPos + look.HitNormal, look.HitPos - look.HitNormal )
				end
			end
		end end
	end
	self.Owner:LagCompensation(false)
end

function SWEP:PreDrop(yes)
    if yes then self:Remove() end
end

function SWEP:Initialize()
    self:SetColorAndMaterial(Color(255,255,255,255),"models/weapons/v_models/kafde/karam" );
end
function SWEP:SecondaryAttack()
    vm = self.Owner:GetViewModel()

    if SERVER and self:CanPrimaryAttack() and self.Owner:IsPlayer() and self.nextshoot < CurTime() then

            self.nextshoot = CurTime() + 1



        bullet = {}
        bullet.Src = self.Owner:GetShootPos()
        bullet.Dir = self.Owner:GetAimVector()
        bullet.Spread=Vector(0.08,0.08,0)
        bullet.Tracer=0
        bullet.Num=10
        bullet.Force= 12
        bullet.Attacker = self:GetOwner()
        bullet.Damage= 4

        self:FireBullets(bullet)

        self.Weapon:SendWeaponAnim( ACT_VM_IDLE )
        if !self.Owner:KeyDown(IN_RELOAD) then
        if self.Slash == 1 then
            vm:SetSequence(vm:LookupSequence("midslash1"))
            self.Slash = 2
        else
            vm:SetSequence(vm:LookupSequence("midslash2"))
            self.Slash = 1
        end --if it looks stupid but works, it aint stupid!
        self.Weapon:EmitSound(self.Primary.Sound)//slash in the wind sound here


        self:PrimarySlash()


        self.Owner:SetAnimation( PLAYER_ATTACK1 )
        self.Weapon:SetNextSecondaryFire(CurTime()+1/(self.Primary.RPM/35))
        self.Weapon:SetNextPrimaryFire(CurTime()+1/(self.Primary.RPM/35))
        end
    end
end

function SWEP:Stab()

	pos2 = self.Owner:GetShootPos()
	ang2 = self.Owner:GetAimVector()
	damagedice = math.Rand(.85,0.99)
	pain = 35 * damagedice
	self.Owner:LagCompensation(true)
	local stab2 = {}
	stab2.start = pos2
	stab2.endpos = pos2 + (ang2 * 150)
	stab2.filter = self.Owner
	stab2.mins = Vector(-150,-150, 0)
	stab2.maxs = Vector(150, 150, 5)
	local stabtrace2 =  util.TraceHull(stab2)
    if SERVER then
        bullet = {}
        bullet.Src = self.Owner:GetShootPos()
        bullet.Dir = self.Owner:GetAimVector()
        bullet.Spread=Vector(0.08,0.08,0)
        bullet.Tracer=0
        bullet.Num=14
        bullet.Force= 12
        bullet.Attacker = self:GetOwner()
        bullet.Damage= 5

        self:FireBullets(bullet)
    end

	if IsValid(self.Owner) and IsValid(self.Weapon) then
		if self.Owner:Alive() then if self.Owner:GetActiveWeapon():GetClass() == self.Gun then
			if stabtrace2.Hit then
			if stabtrace2.Entity == nil then return end
			targ = stabtrace2.Entity
				if targ:IsPlayer() or targ:IsNPC() then
				
					paininfo = DamageInfo()
					paininfo:SetDamage(pain)
					paininfo:SetDamageType(DMG_SLASH)
					paininfo:SetAttacker(self.Owner)
					paininfo:SetInflictor(self.Weapon)
					paininfo:SetDamageForce(stabtrace2.Normal *75000)
					if SERVER then targ:TakeDamageInfo(paininfo) end
					self.Weapon:EmitSound(self.KnifeStab)//stab noise
				else
					self.Weapon:EmitSound(self.KnifeShink)//SHINK!
					look = self.Owner:GetEyeTrace()
					util.Decal("ManhackCut", look.HitPos + look.HitNormal, look.HitPos - look.HitNormal )
				end
			else
				self.Weapon:EmitSound(self.Primary.Sound)
			end
		end end
	end
	self.Owner:LagCompensation(false)
end

function SWEP:ThrowKnife()
	self:ThrowLikeKnife()
end

function SWEP:IronSight()
	
	if !self.Owner:IsNPC() then
	if self.ResetSights and CurTime() >= self.ResetSights then
	self.ResetSights = nil
	
	if self.Silenced then
		self:SendWeaponAnim(ACT_VM_IDLE_SILENCED)
	else
		self:SendWeaponAnim(ACT_VM_IDLE)
	end
	end end
	


end
SWEP.NextChangeColor = 0
function SWEP:Reload()
    if self.Owner:KeyDown(IN_USE) then
       if self.NextChangeColor < CurTime() and SERVER then
           local matt = self:GetNetworkedVar("materialSub","models/weapons/v_models/kafde/karam")
           if matt == "models/weapons/v_models/kafde/karam" then
                self:SetNetworkedVar("materialSub","models/weapons/v_models/kastk/karam")
           elseif matt == "models/weapons/v_models/kastk/karam" then
               self:SetNetworkedVar("materialSub","models/weapons/v_models/katig/karam")
           else
               self:SetNetworkedVar("materialSub","models/weapons/v_models/kafde/karam")
           end
           self.NextChangeColor = CurTime() + 0.2
       end
    end

    if self.Owner:KeyDown(IN_ATTACK) then
        self:ThrowKnife()
        return false
    end

    return false
end



function SWEP:PreDrawViewModel()
    self.Owner:GetViewModel():SetSubMaterial(0,self:GetNetworkedVar("materialSub","models/weapons/v_models/kafde/karam"))
end
