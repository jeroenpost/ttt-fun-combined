if SERVER then
    AddCSLuaFile( "shared.lua" )
    --resource.AddFile("materials/vgui/ttt_fun_killicons/ump45.png")
end

SWEP.HoldType = "ar2"
SWEP.PrintName = "Orb's UMP"
SWEP.Slot      = 3
if CLIENT then

    SWEP.Author = "GreenBlack"

    SWEP.Icon = "vgui/ttt_fun_killicons/ump45.png"
end

SWEP.Base = "aa_base"
SWEP.Spawnable = true
SWEP.AdminSpawnable = true
SWEP.Kind = WEAPON_NADE

SWEP.Primary.Damage = 24
SWEP.Primary.Delay = 0.095
SWEP.Primary.Cone = 0.01
SWEP.Primary.ClipSize = 30
SWEP.Primary.ClipMax = 3000
SWEP.Primary.DefaultClip = 3000
SWEP.Primary.Automatic = true
SWEP.Primary.Ammo = "smg1"
SWEP.AutoSpawnable = false
SWEP.AmmoEnt = "item_ammo_smg1_ttt"
SWEP.Primary.Recoil = 2.5
SWEP.Primary.Sound = Sound( "weapons/ump45/ump45-1.wav" )
SWEP.Primary.SoundLevel = 90
SWEP.ViewModel = "models/weapons/v_smg_ump45.mdl"
SWEP.WorldModel = "models/weapons/w_smg_ump45.mdl"

SWEP.HeadshotMultiplier = 1.5

SWEP.IronSightsPos = Vector( 7.3, -5.5, 3.2 )
SWEP.IronSightsAng = Vector( -1.5, 0.35, 2 )

function SWEP:SecondaryAttack()
    self:Fly()
end
function SWEP:OnDrop()
    self:Remove()
end