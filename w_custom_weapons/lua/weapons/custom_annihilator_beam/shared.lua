
SWEP.Base			= "gb_base"
SWEP.PrintName	= "Annihilator Beam"
SWEP.Kind 			= WEAPON_MELEE
SWEP.Slot = 0
SWEP.SlotPos 	= 1
SWEP.Primary.Recoil	= 0.1
SWEP.Primary.Damage = 49
SWEP.Primary.Delay 	= 0.35
SWEP.Primary.Cone 	= 0.01
SWEP.Primary.ClipSize 		= 40
SWEP.Primary.ClipMax 		= 120
SWEP.Primary.DefaultClip 	= 40
SWEP.Primary.NumShots		= 1
SWEP.Primary.Automatic 	= true
SWEP.HeadshotMultiplier = 3
SWEP.AmmoEnt 		= "item_ammo_revolver_ttt"

SWEP.Primary.Sound = Sound( "weapons/m3/s3-1.mp3" )
SWEP.Primary.SoundLevel = 70

SWEP.HoldType = "pistol"
SWEP.ViewModelFOV = 61
SWEP.ViewModelFlip = false
SWEP.ViewModel = "models/weapons/v_pistol.mdl"
SWEP.WorldModel = "models/weapons/w_pistol.mdl"
SWEP.ShowViewModel = true
SWEP.ShowWorldModel = true
SWEP.ViewModelBoneMods = {}



SWEP.WElements = {
    ["element_name"] = { type = "Model", model = "models/armcannon/annihilatorbeam.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(0, 0, 0), angle = Angle(3.068, 86.931, -166.706), size = Vector(1.286, 1.286, 1.286), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
}

SWEP.VElements = {
    ["element_name"] = { type = "Model", model = "models/armcannon/annihilatorbeam.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(0, 1.363, -2.274), angle = Angle(-166.706, -95.114, 7.158), size = Vector(0.9,0.9,0.9), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
}



function SWEP:PrimaryAttack(worldsnd)

    self.Weapon:SetNextSecondaryFire( CurTime() + 0.40 )
    self.Weapon:SetNextPrimaryFire( CurTime() + 0.40 )

    --if self:Clip1() < 1 then return end

    if not worldsnd then
        self.Weapon:EmitSound( self.Primary.Sound, self.Primary.SoundLevel )
    elseif SERVER then
        sound.Play(self.Primary.Sound, self:GetPos(), self.Primary.SoundLevel)
    end

    self:ShootBullet( self.Primary.Damage, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone(), 8, 10 )

    --self:TakePrimaryAmmo( 1 )

    local owner = self.Owner
    if not IsValid(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

    owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )
end


function SWEP:SecondaryAttack( soundfile )
    self:Fly()
end
-- FLY
SWEP.flyammo = 5
SWEP.nextFly = 0
function SWEP:Fly( soundfile )
    if self.nextFly > CurTime() or CLIENT then return end
    self.nextFly = CurTime() + 0.30
    if self.flyammo < 1 then return end

    if not soundfile then
        if SERVER then self.Owner:EmitSound(Sound("weapons/ls/lightsaber_swing.mp3")) end
        if SERVER then self.Owner:EmitSound(Sound("player/suit_sprint.wav")) end
    else
        if SERVER then self.Owner:EmitSound(Sound(soundfile)) end
    end
    self.flyammo = self.flyammo - 1
    self.Owner:SetAnimation(PLAYER_ATTACK1)
    local power = 400
    if math.random(1,5) < 2 then power = 100
    elseif math.random(1,15) < 2 then power = -500
    elseif math.random(1,15) < 2 then power = 2500
    else power = math.random( 350,450 ) end

    if SERVER then self.Owner:SetVelocity(self.Owner:GetForward() * power + Vector(0, 0, power)) end
    timer.Simple(20, function() if IsValid(self) then self.flyammo = self.flyammo + 1 end end)
    --timer.Simple(0.2,self.SecondaryAttackDelay,self)
end



function SWEP:Think()

    if self.Owner:KeyDown(IN_RELOAD) then
        self.ThrowVel = 100000
        self.Damage = 150
        self.Tossed = true
    end

    if self.StartThrow && !self.Owner:KeyDown(IN_ATTACK) && !self.Owner:KeyDown(IN_ATTACK2) && self.NextThrow < CurTime() then

    self.StartThrow = false
    self.Throwing = true
    if self.Tossed then
        self.Weapon:SendWeaponAnim(ACT_VM_SECONDARYATTACK)
    else
        self.Weapon:SendWeaponAnim(ACT_VM_THROW)
    end
    self.Owner:SetAnimation(PLAYER_ATTACK1)
    self:CreateGrenade(self.Damage)

    self.NextAnimation = CurTime() + self.Primary.Delay
    self.ResetThrow = true

    end

    if self.Throwing && self.ResetThrow && self.NextAnimation < CurTime() then

    if self.Owner:GetAmmoCount(self.Primary.Ammo) > 0 && self:Clip1() <= 0 then

    self:SetClip1(self:Clip1()+1)
    self.Weapon:SendWeaponAnim(ACT_VM_DRAW)
    if IsValid(self.Owner:GetViewModel()) then
        self.NextThrow = CurTime() + self.Owner:GetViewModel():SequenceDuration()
        self.Weapon:SetNextPrimaryFire(CurTime() + self.Owner:GetViewModel():SequenceDuration())
        self.Weapon:SetNextSecondaryFire(CurTime() + self.Owner:GetViewModel():SequenceDuration())
    end

    else
      --  self.Owner:ConCommand("lastinv")
    end

    self.ResetThrow = false
    self.Throwing = false

    end

end

function SWEP:OnDrop()
    self:Remove()
end


SWEP.HadSecondary = 0

SWEP.nextReloady = 0

function SWEP:Reload()
    if ( self.nextReloady > CurTime() ) then return end
        self.nextReloady = CurTime() + 1
    if self.HadSecondary > 3 then
        self.Owner:SetNWFloat("w_stamina", 1)
        self.Owner:SetNWInt("runspeed", 450)
        self.Owner:SetNWInt("walkspeed",250)
        self.Owner:SetJumpPower(550)

        timer.Simple(7, function()
            if !IsValid(self) or !IsValid( self.Owner) then return end
            self.Owner:SetNWInt("runspeed", 320)
            self.Owner:SetNWInt("walkspeed",220)
            self.Owner:SetJumpPower(160)
        end)
        return
    end
    if !self.Throwing && !self.StartThrow then
    self.StartThrow = true
    self.HadSecondary = self.HadSecondary + 1

    self.Weapon:SendWeaponAnim(ACT_VM_PULLBACK_LOW)
    if IsValid(self.Owner:GetViewModel()) then
        self.Weapon:SetNextPrimaryFire(CurTime() + self.Owner:GetViewModel():SequenceDuration())
        self.Weapon:SetNextSecondaryFire(CurTime() + 3)
        self.NextThrow = CurTime() + self.Owner:GetViewModel():SequenceDuration()
    end
    end
end



function SWEP:CreateGrenade( damage )

    if IsValid(self.Owner) && IsValid(self.Weapon) then

    if (SERVER) then

        local ent = ents.Create("jiac_plasmanade2")
        if !ent then return end
        ent.Owner = self.Owner
        ent.Inflictor = self.Weapon
        ent.Damage = 150
        ent:SetOwner(self.Owner)
        local eyeang = self.Owner:GetAimVector():Angle()
        local right = eyeang:Right()
        local up = eyeang:Up()
        if self.Tossed then
            ent:SetPos(self.Owner:GetShootPos()+right*4-up*4)
        else
            ent:SetPos(self.Owner:GetShootPos()+right*4+up*4)
        end
        ent:SetAngles(self.Owner:GetAngles())
        ent:SetPhysicsAttacker(self.Owner)
        ent:Spawn()

        local phys = ent:GetPhysicsObject()
        if phys:IsValid() then
            phys:SetVelocity(self.Owner:GetAimVector() * 100000 + (self.Owner:GetVelocity() * 0.75))
        end

    end

    end

end


function SWEP:ShootBullet( dmg, recoil, numbul, cone, scale, magnitude )
    local sights = self:GetIronsights()

    numbul = numbul or 1
    cone   = cone   or 0.01

    -- 10% accuracy bonus when sighting
    cone = sights and (cone * 0.9) or cone

    local bullet = {}
    bullet.Num    = numbul
    bullet.Src    = self.Owner:GetShootPos()
    bullet.Dir    = self.Owner:GetAimVector()
    bullet.Spread = Vector( cone, cone, 0 )
    bullet.Tracer = 1
    bullet.TracerName = "AirboatGunHeavyTracer"
    bullet.Force  = 5
    bullet.Damage = dmg


    bullet.Callback = function(att, tr, dmginfo)
        if SERVER or (CLIENT and IsFirstTimePredicted()) then
            local ent = tr.Entity
            if (not tr.HitWorld) and IsValid(ent) then
                local edata = EffectData()

                edata:SetEntity(ent)
                edata:SetMagnitude(1)
                edata:SetScale(1)

                util.Effect("TeslaHitBoxes", edata)

                if SERVER and ent:IsPlayer() then
                    local eyeang = ent:EyeAngles()

                    local j = math.Rand(1,5) --10
                    eyeang.pitch = math.Clamp(eyeang.pitch + math.Rand(-j, j), -90, 90)
                    eyeang.yaw = math.Clamp(eyeang.yaw + math.Rand(-j, j), -90, 90)
                    ent:SetEyeAngles(eyeang)
                end
            end
        end
    end

    self.Owner:FireBullets( bullet )
    self.Weapon:SendWeaponAnim(self.PrimaryAnim)

    -- Owner can die after firebullets, giving an error at muzzleflash
    if not IsValid(self.Owner) or not self.Owner:Alive() then return end

    self.Owner:MuzzleFlash()
    self.Owner:SetAnimation( PLAYER_ATTACK1 )

    if self.Owner:IsNPC() then return end

    if ((game.SinglePlayer() and SERVER) or
            ((not game.SinglePlayer()) and CLIENT and IsFirstTimePredicted() )) then

        -- reduce recoil if ironsighting
        recoil = sights and (recoil * 0.75) or recoil

        local eyeang = self.Owner:EyeAngles()
        eyeang.pitch = eyeang.pitch - recoil
        self.Owner:SetEyeAngles( eyeang )

    end
end









local ENT = {}

ENT.Type = "anim"
ENT.Base = "base_anim"

if CLIENT then
    ENT.Mat = Material("sprites/light_glow02_add")
    ENT.Scaled = false

    function ENT:Draw()
        self:DrawModel()
        render.SetMaterial(self.Mat)
        render.DrawSprite(self:GetPos(), 72+(16*math.sin(CurTime()*5)), 72+(16*math.sin(CurTime()*5)), Color(255,255,255,255))
        render.DrawSprite(self:GetPos(), 64+(16*math.sin(CurTime()*5)), 64+(16*math.sin(CurTime()*5)), Color(255,255,255,255))
    end
end

if SERVER then

    function ENT:Initialize()

        self.Hit = false
        self.LastShout = CurTime()
        self.CurrentPitch = 100
        self.SpawnDelay = CurTime() + 0.001
        --damage self.Damage = 75
        self:SetModel("models/dav0r/hoverball.mdl")
        self:PhysicsInitBox(Vector(-1,-1,-1),Vector(1,1,1))
        self:SetMoveType(MOVETYPE_VPHYSICS)
        self:SetSolid(SOLID_VPHYSICS)
        self:SetTrigger(true)
        self:SetMaterial("models/weapons/v_grenade/grenade body")
        self:SetModelScale(self:GetModelScale()*0.5,0)
        self:SetColor(255,255,255,255)
        self:DrawShadow(false)


        local phys = self:GetPhysicsObject()
        if (phys:IsValid()) then
            phys:Wake()
            phys:EnableDrag(false)
            phys:AddGameFlag(FVPHYSICS_NO_IMPACT_DMG)
            phys:SetBuoyancyRatio(0)
        end

        self.Fear = ents.Create("ai_sound")
        self.Fear:SetPos(self:GetPos())
        self.Fear:SetParent(self)
        self.Fear:SetKeyValue("SoundType", "8|1")
        self.Fear:SetKeyValue("Volume", "500")
        self.Fear:SetKeyValue("Duration", "1")
        self.Fear:Spawn()

        self.Trail = util.SpriteTrail(self, 0, Color(255,255,255), false, 64, 1, 0.3, 0.01, "trails/plasma.vmt")
        self.WhirrSound = CreateSound(self, "ambient/energy/force_field_loop1.wav")
        self.WhirrSound:Play()

        self:Fire("kill", 1, 60)

    end

    function ENT:Think()

        if self.LastShout < CurTime() then
            if IsValid(self.Fear) then
                self.Fear:Fire("EmitAISound")
            end
            self.LastShout = CurTime() + 0.10
        end

        if self.Hit then
            self.CurrentPitch = self.CurrentPitch + 5
            if self.WhirrSound then self.WhirrSound:ChangePitch(math.Clamp(self.CurrentPitch,100,255),0) end
        end

        if self.Hit && self.Splodetimer && self.Splodetimer < CurTime() then

        util.ScreenShake( self:GetPos(), 50, 50, 1, 250 )
        if IsValid(self.Owner) then
            if IsValid(self.Inflictor) then
                util.BlastDamage(self.Inflictor, self.Owner, self:GetPos(), 250, (self.Damage))
            else
                util.BlastDamage(self, self.Owner, self:GetPos(), 250, (self.Damage))
            end
        end

        local fx = EffectData()
        fx:SetOrigin(self:GetPos())
        util.Effect("plasmasplode2", fx)

        self:EmitSound("ambient/energy/ion_cannon_shot"..math.random(1,3)..".wav",90,100)
        self:EmitSound("ambient/explosions/explode_"..math.random(7,9)..".wav",90,100)
        self:EmitSound("weapons/explode"..math.random(3,5)..".wav",90,85)

        self:Remove()

        end

    end

    function ENT:OnRemove()
        if self.WhirrSound then self.WhirrSound:Stop() end
        if IsValid(self.Fear) then self.Fear:Fire("kill") end
    end

    function ENT:PhysicsUpdate(phys)
        if !self.Hit then
        self:SetLocalAngles(phys:GetVelocity():Angle())
        else
            phys:SetVelocity(Vector(phys:GetVelocity().x*0.95,phys:GetVelocity().y*0.95,phys:GetVelocity().z))
        end
    end

    function ENT:Touch(ent)
        if IsValid(ent) && !self.Stuck  && !ent.hasShieldPotato  then
        if ent:IsNPC() || (ent:IsPlayer() && ent != self:GetOwner()) || (ent == self:GetOwner() && self.SpawnDelay < CurTime() ) || ent:IsVehicle() then
       -- self:SetSolid(SOLID_NONE)
        --self:SetMoveType(MOVETYPE_NONE)
        --self:SetParent(ent)
        if !self.Splodetimer then
        self.Splodetimer = CurTime() + 1
        end
        self.Stuck = true
        self.Hit = true
        end
        end
    end

    function ENT:PhysicsCollide(data,phys)
        if self:IsValid() && !self.Hit then
        self:SetCollisionGroup(COLLISION_GROUP_DEBRIS)
        if !self.Splodetimer then
        self.Splodetimer = CurTime() + 1
        end
        self.Hit = true
        end
    end

end

scripted_ents.Register( ENT, "jiac_plasmanade2", true )

if CLIENT then

    local EFFECT = {}

    function EFFECT:Init(ed)

        local vOrig = ed:GetOrigin()
        self.Emitter = ParticleEmitter(vOrig)

        for i=1,24 do

            local smoke = self.Emitter:Add("particle/particle_smokegrenade", vOrig)

            if (smoke) then

                smoke:SetColor(255,255,255)
                smoke:SetVelocity(VectorRand():GetNormal()*math.random(100, 300))
                smoke:SetRoll(math.Rand(0, 360))
                smoke:SetRollDelta(math.Rand(-2, 2))
                smoke:SetDieTime(1)
                smoke:SetLifeTime(0)
                smoke:SetStartSize(50)
                smoke:SetStartAlpha(255)
                smoke:SetEndSize(100)
                smoke:SetEndAlpha(0)
                smoke:SetGravity(Vector(0,0,0))

            end

            local smoke2 = self.Emitter:Add("particle/particle_smokegrenade", vOrig)

            if (smoke2) then

                smoke:SetColor(255,255,255)
                smoke2:SetVelocity(VectorRand():GetNormal()*math.random(50, 100))
                smoke2:SetRoll(math.Rand(0, 360))
                smoke2:SetRollDelta(math.Rand(-2, 2))
                smoke2:SetDieTime(5)
                smoke2:SetLifeTime(0)
                smoke2:SetStartSize(50)
                smoke2:SetStartAlpha(255)
                smoke2:SetEndSize(100)
                smoke2:SetEndAlpha(0)
                smoke2:SetGravity(Vector(0,0,0))

            end

            local smoke3 = self.Emitter:Add("particle/particle_smokegrenade", vOrig+Vector(math.random(-150,150),math.random(-150,150),0))

            if (smoke3) then

                smoke3:SetColor(255,255,255)
                smoke3:SetVelocity(VectorRand():GetNormal()*math.random(50, 100))
                smoke3:SetRoll(math.Rand(0, 360))
                smoke3:SetRollDelta(math.Rand(-2, 2))
                smoke3:SetDieTime(5)
                smoke3:SetLifeTime(0)
                smoke3:SetStartSize(50)
                smoke3:SetStartAlpha(255)
                smoke3:SetEndSize(100)
                smoke3:SetEndAlpha(0)
                smoke3:SetGravity(Vector(0,0,0))

            end

            local heat = self.Emitter:Add("sprites/heatwave", vOrig+Vector(math.random(-150,150),math.random(-150,150),0))

            if (heat) then

                heat:SetColor(255,255,255)
                heat:SetVelocity(VectorRand():GetNormal()*math.random(50, 100))
                heat:SetRoll(math.Rand(0, 360))
                heat:SetRollDelta(math.Rand(-2, 2))
                heat:SetDieTime(3)
                heat:SetLifeTime(0)
                heat:SetStartSize(100)
                heat:SetStartAlpha(255)
                heat:SetEndSize(0)
                heat:SetEndAlpha(0)
                heat:SetGravity(Vector(0,0,0))

            end

        end

        for i=1,72 do

            local sparks = self.Emitter:Add("effects/spark", vOrig)

            if (sparks) then

                sparks:SetColor(255,255,255)
                sparks:SetVelocity(VectorRand():GetNormal()*math.random(300, 500))
                sparks:SetRoll(math.Rand(0, 360))
                sparks:SetRollDelta(math.Rand(-2, 2))
                sparks:SetDieTime(2)
                sparks:SetLifeTime(0)
                sparks:SetStartSize(3)
                sparks:SetStartAlpha(255)
                sparks:SetStartLength(15)
                sparks:SetEndLength(3)
                sparks:SetEndSize(3)
                sparks:SetEndAlpha(255)
                sparks:SetGravity(Vector(0,0,-800))

            end

            local sparks2 = self.Emitter:Add("effects/spark", vOrig)

            if (sparks2) then

                sparks2:SetColor(255,255,255)
                sparks2:SetVelocity(VectorRand():GetNormal()*math.random(400, 800))
                sparks2:SetRoll(math.Rand(0, 360))
                sparks2:SetRollDelta(math.Rand(-2, 2))
                sparks2:SetDieTime(0.4)
                sparks2:SetLifeTime(0)
                sparks2:SetStartSize(5)
                sparks2:SetStartAlpha(255)
                sparks2:SetStartLength(80)
                sparks2:SetEndLength(0)
                sparks2:SetEndSize(5)
                sparks2:SetEndAlpha(0)
                sparks2:SetGravity(Vector(0,0,0))

            end

        end

        for i=1,8 do

            local flash = self.Emitter:Add("effects/combinemuzzle2_dark", vOrig)

            if (flash) then

                flash:SetColor(255, 255, 255)
                flash:SetVelocity(VectorRand():GetNormal()*math.random(10, 30))
                flash:SetRoll(math.Rand(0, 360))
                flash:SetDieTime(0.10)
                flash:SetLifeTime(0)
                flash:SetStartSize(150)
                flash:SetStartAlpha(255)
                flash:SetEndSize(240)
                flash:SetEndAlpha(0)
                flash:SetGravity(Vector(0,0,0))

            end

            local quake = self.Emitter:Add("effects/splashwake3", vOrig)

            if (quake) then

                quake:SetColor(0,0,0)
                quake:SetVelocity(VectorRand():GetNormal()*math.random(10, 30))
                quake:SetRoll(math.Rand(0, 360))
                quake:SetDieTime(0.10)
                quake:SetLifeTime(0)
                quake:SetStartSize(160)
                quake:SetStartAlpha(200)
                quake:SetEndSize(270)
                quake:SetEndAlpha(0)
                quake:SetGravity(Vector(0,0,0))

            end

            local wave = self.Emitter:Add("sprites/heatwave", vOrig)

            if (wave) then

                wave:SetColor(0,0,0)
                wave:SetVelocity(VectorRand():GetNormal()*math.random(10, 30))
                wave:SetRoll(math.Rand(0, 360))
                wave:SetDieTime(0.25)
                wave:SetLifeTime(0)
                wave:SetStartSize(160)
                wave:SetStartAlpha(255)
                wave:SetEndSize(270)
                wave:SetEndAlpha(0)
                wave:SetGravity(Vector(0,0,0))

            end

        end

        self.Emitter:Finish()

    end

    function EFFECT:Think()
        return false
    end

    function EFFECT:Render()
    end

    effects.Register( EFFECT, "plasmasplode2", true )

end