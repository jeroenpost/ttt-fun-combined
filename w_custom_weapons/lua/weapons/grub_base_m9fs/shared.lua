

-- Variables that are used on both client and server
SWEP.Category  		= "Twilight Sparkle`s SWEPs" -- the categorry where all your weapon from this vbase will be
SWEP.Base			= "weapon_tttbase"
SWEP.Author			= "Residualgrub"
SWEP.Contact		= ""
SWEP.Purpose		= ""
SWEP.Instructions	= ""
SWEP.CSMuzzleFlashes    = true
SWEP.ViewModelFOV	= 62
SWEP.ViewModelFlip	= false
SWEP.ViewModel		= "models/weapons/v_pistol.mdl"
SWEP.WorldModel		= "models/weapons/w_357.mdl"
SWEP.AnimPrefix		= "python"
SWEP.HoldType 		= "none"

SWEP.Spawnable			= false
SWEP.AdminSpawnable		= false

SWEP.Primary.Sound			= Sound( "" )
SWEP.Primary.Recoil			= 0
SWEP.Primary.CurrentSpread 	= 0
SWEP.Primary.Damage			= 0
SWEP.Primary.NumShots		= 0
SWEP.Primary.Cone			= 0
SWEP.Primary.ClipSize		= -1
SWEP.Primary.RPM			= 0
SWEP.Primary.DefaultClip	= 0
SWEP.Primary.Automatic		= false
SWEP.Primary.Ammo			= "none"

SWEP.Secondary.ClipSize		= -1
SWEP.Secondary.DefaultClip	= -1
SWEP.Secondary.Automatic	= false
SWEP.Secondary.Ammo			= "none"

SWEP.ReloadSound 			= Sound("")

SWEP.Burst				= false
SWEP.BurstShots			= 3
SWEP.BurstDelay			= 0.05
SWEP.BurstCounter			= 0
SWEP.BurstTimer			= 0
SWEP.Sli = 0
SWEP.Iron = 0
SWEP.ShellDelay = 0
SWEP.ShellEffect = "none"
SWEP.MuzzleEffect            = "smoke_trail_new"
SWEP.MuzzleAttachment        = "1"
SWEP.ShellEjectAttachment    = "1"
SWEP.IronsightTime 			= 0.25
SWEP.RunSightTime 			= 0.15

SWEP.IronSightsPos 			= Vector(0,0,0)
SWEP.IronSightsAng 			= Angle(0,0,0)

SWEP.RunSightsPos 			= Vector(0,0,0)
SWEP.RunSightsAng 			= Angle(0,0,0)

--[[---------------------------------------------------------
   Name: SWEP:Initialize( )
   Desc: Called when the weapon is first loaded
-----------------------------------------------------------]]
function SWEP:Initialize()
	self:SetHoldType( self.HoldType or "pistol" )
	self:ResetVar()
	self:SetupDataTables() 
	self.Sli = 0

	// other initialize code goes here

	if CLIENT then
	
		// Create a new table for every weapon instance
		self.VElements = table.FullCopy( self.VElements )
		self.WElements = table.FullCopy( self.WElements )
		self.ViewModelBoneMods = table.FullCopy( self.ViewModelBoneMods )

		self:CreateModels(self.VElements) // create viewmodels
		self:CreateModels(self.WElements) // create worldmodels
		
		// init view model bone build function
		if IsValid(self.Owner) then
			local vm = self.Owner:GetViewModel()
			if IsValid(vm) then
				self:ResetBonePositions(vm)
				
				// Init viewmodel visibility
				if (self.ShowViewModel == nil or self.ShowViewModel) then
					vm:SetColor(Color(255,255,255,255))
				else
					// we set the alpha to 1 instead of 0 because else ViewModelDrawn stops being called
					vm:SetColor(Color(255,255,255,1))
					// ^ stopped working in GMod 13 because you have to do Entity:SetRenderMode(1) for translucency to kick in
					// however for some reason the view model resets to render mode 0 every frame so we just apply a debug material to prevent it from drawing
					vm:SetMaterial("Debug/hsv")			
				end
			end
		end
		
	end
	self.Weapon:SetNWBool( "SLI", false )
end

function SWEP:ResetVar() -- use this function when you need to make the waepon go back to the normal (example: deploy, holster) prevent a lot of problems
	self.dt.Ironsight = false
	self.dt.Running = false
	self.Primary.CurrentSpread = 0
end

function SWEP:SetupDataTables() 
	self.Weapon:DTVar("Bool", 0, "Ironsight")
	self.Weapon:DTVar("Bool", 1, "Running")
end

--[[---------------------------------------------------------
   Name: SWEP:Precache( )
   Desc: Use this function to precache stuff
-----------------------------------------------------------]]
function SWEP:Precache()
end


--[[---------------------------------------------------------
   Name: SWEP:PrimaryAttack( )
   Desc: +attack1 has been pressed
-----------------------------------------------------------]]
function SWEP:PrimaryAttack()

	-- Make sure we can shoot first
if self:IsRunning() then
		return false
	end
if ( self.Weapon:Clip1() > 0 ) then
	self.Weapon:SetNextPrimaryFire(CurTime()+1/(self.Primary.RPM/60))
	self.Weapon:EmitSound(self.Primary.Sound)
	self:ShootBullet( self.Primary.Damage, self.Primary.Numshots, self.Primary.Recoil, self.Primary.Cone )

	self:TakePrimaryAmmo( 1 )

else
	self.Weapon:EmitSound("weapons/Custom 96FS/96FS Click.wav")
	self.Owner:ViewPunch(Angle( -.5, 0, 0 ))
end
end


--[[---------------------------------------------------------
   Name: SWEP:SecondaryAttack( )
   Desc: +attack2 has been pressed
-----------------------------------------------------------]]
function SWEP:SecondaryAttack()
if self:IsRunning() then return end
	if self.Owner:KeyDown(IN_USE) and self.Sli == 1 then
		self.Sli = 0
		self.Weapon:SendWeaponAnim(ACT_VM_DETACH_SILENCER)
		Vm = self.Owner:GetViewModel()
		if IsValid(Vm) then
			self.Weapon:SendWeaponAnim(ACT_VM_DEPLOY)
			self.AnimDuration = Vm:SequenceDuration()
			self.Weapon:SetNextPrimaryFire(CurTime() + self.AnimDuration)
			self.Weapon:SetNextSecondaryFire(CurTime() + self.AnimDuration)
			Vm:SetPlaybackRate(1)
		end
		self.Primary.Damage = 30
		self.Primary.Cone = .03
		self.Primary.Recoil = 2
		self.Primary.Sound = Sound( "weapons/Custom 96FS/96FS Fire Unsilenced.wav" )
		self.Weapon:SetNWBool( "SLI", false )
	elseif self.Owner:KeyDown(IN_USE) and self.Sli == 0 then
		self.Weapon:SendWeaponAnim(ACT_VM_ATTACH_SILENCER)
		
		Vm = self.Owner:GetViewModel()
	if IsValid(Vm) then
		self.Weapon:SendWeaponAnim(ACT_VM_DEPLOY)
		self.AnimDuration = Vm:SequenceDuration()
		self.Weapon:SetNextPrimaryFire(CurTime() + self.AnimDuration)
		self.Weapon:SetNextSecondaryFire(CurTime() + self.AnimDuration)
		Vm:SetPlaybackRate(1)
	end
		self.Primary.Recoil = 1
		self.Primary.Cone = .04
		self.Primary.Damage = 25
		self.Primary.Sound = Sound( "weapons/Custom 96FS/96FS Fire Silenced.wav" )
		self.Sli = 1
		self.Weapon:SetNWBool( "SLI", true )
	end
	LastIron = !self:GetIronsight()
	self:SetIronsight(LastIron)
	self.Weapon:SetNextSecondaryFire(CurTime() + self.IronsightTime)
	self:SetHoldType( "normal" )
end

--[[---------------------------------------------------------
   Name: SWEP:CheckReload( )
   Desc: CheckReload
-----------------------------------------------------------]]


--[[---------------------------------------------------------
   Name: SWEP:Reload( )
   Desc: Reload is being pressed
-----------------------------------------------------------]]
function SWEP:Reload()
	if self.Weapon:Clip1() >= self.Primary.ClipSize or self.Owner:GetAmmoCount(self.Primary.Ammo) <= 0 then return end
		if self.Sli == 1 then
			self.Weapon:DefaultReload( ACT_VM_RELOAD );
			local Animation = self.Owner:GetViewModel()
			Animation:SetSequence(Animation:LookupSequence("sequence_6"))
		else
			self.Weapon:DefaultReload( ACT_VM_RELOAD );
		end
	self:SetIronsight(false)
end



--[[---------------------------------------------------------
   Name: SWEP:Think( )
   Desc: Called every frame
-----------------------------------------------------------]]
function SWEP:Think()
		
	if !self:IsRunning() and self.Owner:KeyDown(IN_SPEED) and self.Owner:GetVelocity():Length() > self.Owner:GetWalkSpeed() then
		self:SetRunning(true)
		self:SetNextPrimaryFire(CurTime() + self.RunSightTime)
	elseif self:IsRunning() and !self.Owner:KeyDown(IN_SPEED) or self:IsRunning() and self.Owner:GetVelocity():Length() <= self.Owner:GetWalkSpeed() then
		self:SetRunning(false)
		self:SetNextPrimaryFire(CurTime() + self.RunSightTime)
	end
end


--[[---------------------------------------------------------
   Name: SWEP:Holster( weapon_to_swap_to )
   Desc: Weapon wants to holster
   RetV: Return true to allow the weapon to holster
-----------------------------------------------------------]]
function SWEP:Holster( wep )
	return true
end

--[[---------------------------------------------------------
   Name: SWEP:Deploy( )
   Desc: Whip it out
-----------------------------------------------------------]]
function SWEP:Deploy()
	self:SetIronsight(false)
	Vm = self.Owner:GetViewModel()
	if IsValid(Vm) then
		if (self.Sli == 0) then
			self.Weapon:SendWeaponAnim(ACT_VM_DEPLOY)
			self.AnimDuration = Vm:SequenceDuration()
			self.Weapon:SetNextPrimaryFire(CurTime() + self.AnimDuration)
			self.Weapon:SetNextSecondaryFire(CurTime() + self.AnimDuration)
			Vm:SetPlaybackRate(1)
			self:ResetVar()
		end
		if (self.Sli == 1) then
			self.Weapon:SendWeaponAnim(ACT_VM_DEPLOY)
			local Animation = self.Owner:GetViewModel()
			Animation:SetSequence(Animation:LookupSequence("sequence_7"))
			self.AnimDuration = Vm:SequenceDuration()
			self.Weapon:SetNextPrimaryFire(CurTime() + self.AnimDuration)
			self.Weapon:SetNextSecondaryFire(CurTime() + self.AnimDuration)
			Vm:SetPlaybackRate(1)
			self:ResetVar()
		end
	end
	return true
end

function SWEP:ShootEffects(recoil)

if (self.Sli == 0) then
	if self.Weapon:Clip1() == 1 then
		self.Weapon:SendWeaponAnim(ACT_VM_PRIMARYATTACK)
		local Animation = self.Owner:GetViewModel()
		Animation:SetSequence(Animation:LookupSequence("sequence_13"))
	else	
		self.Weapon:SendWeaponAnim(ACT_VM_PRIMARYATTACK)
	end
end

if (self.Sli == 1) then
	if self.Weapon:Clip1() == 1 then
		self.Weapon:SendWeaponAnim(ACT_VM_PRIMARYATTACK_SILENCED)
		local Animation = self.Owner:GetViewModel()
		Animation:SetSequence(Animation:LookupSequence("sequence_5"))
	else
		self.Weapon:SendWeaponAnim(ACT_VM_PRIMARYATTACK_SILENCED)
	end
end
   
self.Owner:MuzzleFlash()

    local fx         = EffectData()
    fx:SetEntity(self.Weapon)
    fx:SetOrigin(self.Owner:GetShootPos())
    fx:SetNormal(self.Owner:GetAimVector())
    fx:SetAttachment(self.MuzzleAttachment)
    util.Effect(self.MuzzleEffect,fx)        
    
    local effectdata = EffectData()
        effectdata:SetOrigin(self.Owner:GetShootPos())
        effectdata:SetEntity(self.Weapon)
        effectdata:SetStart(self.Owner:GetShootPos())
        effectdata:SetNormal(self.Owner:GetAimVector())
        effectdata:SetAttachment(1)

        
        timer.Simple(self.ShellDelay, function()
        if not IsValid(self.Owner) then return end
        if not IsFirstTimePredicted() then return end
        if not self.Owner:IsNPC() and not self.Owner:Alive() then return end

        local fxs     = EffectData()
            fxs:SetEntity(self.Weapon)
            fxs:SetNormal(self.Owner:GetAimVector())
            fxs:SetAttachment(self.ShellEjectAttachment)

            util.Effect(self.ShellEffect,fxs)                
    end)
    
        local trace = self.Owner:GetEyeTrace()							-- Crappy muzzle light
	


	self.Owner:SetAnimation( PLAYER_ATTACK1 )	-- 3rd Person Animation
	SideRecoil = recoil / 1.5
	anglo = Angle(-recoil, math.Rand(-SideRecoil,SideRecoil), 0)
	self.Owner:ViewPunch(anglo)

end

function SWEP:GetIronsight()
	if self.dt.Ironsight then
		self.HoldType = "revolver"
		return self.dt.Ironsight
	end
	self.HoldType = "pistol"
	self:SetHoldType( self.HoldType or "pistol" )
	return false
end

function SWEP:SetIronsight(Bool)
	if self.Owner:KeyDown(IN_USE) then return end
	self.dt.Ironsight = Bool
	if Bool then -- things will happen if Bool is true
	else -- things will happen if bool is false
	end
end

function SWEP:SetRunning(Bool)
	self.dt.Running = Bool
	if Bool then -- things will happen if Bool is true
	else -- things will happen if bool is false
	end
end

function SWEP:IsRunning()
	if self.dt.Running then
		self:SetHoldType( "normal" )
		return self.dt.Running
	end
	self:SetHoldType( self.HoldType or "pistol" )
	return false
end

--[[---------------------------------------------------------
   Name: SWEP:ShootBullet( )
   Desc: A convenience function to shoot bullets
-----------------------------------------------------------]]
function SWEP:ShootBullet( damage, num_bullets, recoil, aimcone )
	num_bullets = self.Primary.NumShots
	local bullet = {}
	bullet.Num 		= num_bullets
	bullet.Src 		= self.Owner:GetShootPos()			-- Source
	bullet.Dir 		= (self.Owner:GetAimVector():Angle() + self.Owner:GetPunchAngle()):Forward()			-- Dir of bullet
	bullet.Spread 	= Vector( aimcone, aimcone, 0 )		-- Aim Cone
	bullet.Tracer	= 5									-- Show a tracer on every x bullets 
	bullet.Force	= 1									-- Amount of force to give to phys objects
	bullet.Damage	= damage
	bullet.AmmoType = "Pistol"
	
	self.Owner:FireBullets( bullet )
	
	self:ShootEffects(recoil)
	
end


--[[---------------------------------------------------------
   Name: SWEP:TakePrimaryAmmo(   )
   Desc: A convenience function to remove ammo
-----------------------------------------------------------]]
function SWEP:TakePrimaryAmmo( num )
	
	-- Doesn't use clips
	if ( self.Weapon:Clip1() <= 0 ) then 
	
		if ( self:Ammo1() <= 0 ) then return end
		
		self.Owner:RemoveAmmo( num, self.Weapon:GetPrimaryAmmoType() )
	
	return end
	
	self.Weapon:SetClip1( self.Weapon:Clip1() - num )	
	
end


--[[---------------------------------------------------------
   Name: SWEP:TakeSecondaryAmmo(   )
   Desc: A convenience function to remove ammo
-----------------------------------------------------------]]
function SWEP:TakeSecondaryAmmo( num )
	
	-- Doesn't use clips
	if ( self.Weapon:Clip2() <= 0 ) then 
	
		if ( self:Ammo2() <= 0 ) then return end
		
		self.Owner:RemoveAmmo( num, self.Weapon:GetSecondaryAmmoType() )
	
	return end
	
	self.Weapon:SetClip2( self.Weapon:Clip2() - num )	
	
end


--[[---------------------------------------------------------
   Name: SWEP:CanPrimaryAttack( )
   Desc: Helper function for checking for no ammo
-----------------------------------------------------------]]
function SWEP:CanPrimaryAttack()

	if ( self.Weapon:Clip1() <= 0 ) then
	
		self:EmitSound( "Weapon_Pistol.Empty" )
		self.Weapon:SetNextPrimaryFire(CurTime()+1/(self.Primary.RPM/60))
		return false
		
	end
	
	if self:IsRunning() then
		return false
	end
	
	return true

end


--[[---------------------------------------------------------
   Name: SWEP:CanSecondaryAttack( )
   Desc: Helper function for checking for no ammo
-----------------------------------------------------------]]
function SWEP:CanSecondaryAttack()

	if ( self.Weapon:Clip2() <= 0 ) then
	
		self.Weapon:EmitSound( "Weapon_Pistol.Empty" )
		self.Weapon:SetNextSecondaryFire( CurTime() + 0.2 )
		return false
		
	end

	return true

end


--[[---------------------------------------------------------
   Name: OnRemove
   Desc: Called just before entity is deleted
-----------------------------------------------------------]]
function SWEP:OnRemove()
end


--[[---------------------------------------------------------
   Name: OwnerChanged
   Desc: When weapon is dropped or picked up by a new player
-----------------------------------------------------------]]
function SWEP:OwnerChanged()
end


--[[---------------------------------------------------------
   Name: Ammo1
   Desc: Returns how much of ammo1 the player has
-----------------------------------------------------------]]
function SWEP:Ammo1()
        if !IsValid(self.Owner) then return end
	return self.Owner:GetAmmoCount( self.Weapon:GetPrimaryAmmoType() )
end


--[[---------------------------------------------------------
   Name: Ammo2
   Desc: Returns how much of ammo2 the player has
-----------------------------------------------------------]]
function SWEP:Ammo2()
	return self.Owner:GetAmmoCount( self.Weapon:GetSecondaryAmmoType() )
end

--[[---------------------------------------------------------
   Name: SetDeploySpeed
   Desc: Sets the weapon deploy speed. 
		 This value needs to match on client and server.
-----------------------------------------------------------]]
function SWEP:SetDeploySpeed( speed )
	self.m_WeaponDeploySpeed = tonumber( speed )
end

--[[---------------------------------------------------------
   Name: DoImpactEffect
   Desc: Callback so the weapon can override the impact effects it makes
		 return true to not do the default thing - which is to call UTIL_ImpactTrace in c++
-----------------------------------------------------------]]
function SWEP:DoImpactEffect( tr, nDamageType )
		
	return false;
	
end


