
include('shared.lua')


SWEP.PrintName			= "Scripted Weapon"		-- 'Nice' Weapon name (Shown on HUD)	
SWEP.Slot				= 0						-- Slot in the weapon selection menu
SWEP.SlotPos			= 10					-- Position in the slot
SWEP.DrawAmmo			= true					-- Should draw the default HL2 ammo counter
SWEP.DrawCrosshair		= true 					-- Should draw the default crosshair
SWEP.DrawWeaponInfoBox	= true					-- Should draw the weapon info box
SWEP.BounceWeaponIcon   = true					-- Should the weapon icon bounce?
SWEP.SwayScale			= 9.0					-- The scale of the viewmodel sway
SWEP.BobScale			= 1.0					-- The scale of the viewmodel bob

SWEP.RenderGroup 		= RENDERGROUP_OPAQUE

-- Override this in your SWEP to set the icon in the weapon selection
SWEP.WepSelectIcon		= surface.GetTextureID( "weapons/swep" )

-- This is the corner of the speech bubble
SWEP.SpeechBubbleLid	= surface.GetTextureID( "gui/speech_lid" )

--[[---------------------------------------------------------
	You can draw to the HUD here - it will only draw when
	the client has the weapon deployed..
-----------------------------------------------------------]]
function SWEP:DrawHUD()
end


--Credit to Warshiper
function SWEP:DrawWeaponSelection(x, y, wide, tall, alpha)

	
	// Set us up the texture
	surface.SetDrawColor(255, 255, 255, alpha)
	surface.SetTexture(self.WepSelectIcon)
	
	// Lets get a sin wave to make it bounce
	local fsin = 0
	
	if (self.BounceWeaponIcon == true) then
		fsin = math.sin(CurTime() * 10) * 5
	end
	
	// Borders
	y = y + 10
	x = x + 10
	wide = wide - 20
	
	// Draw that mother
	surface.DrawTexturedRect(x + (fsin), y - (fsin), wide - fsin * 2, (wide / 2) + (fsin))
	
	// Draw weapon info box
	self:PrintWeaponInfo(x + wide + 20, y + tall * 0.95, alpha)
end

/*---------------------------------------------------------
   Name: SWEP:PrintWeaponInfo()
   Desc: Draws the weapon info box.
---------------------------------------------------------*/
function SWEP:PrintWeaponInfo(x, y, alpha)

	if (self.DrawWeaponInfoBox == false) then return end

	if (self.InfoMarkup == nil) then
		local str
		local title_color = "<color = 130, 0, 0, 255>"
		local text_color = "<color = 255, 255, 255, 200>"
		
		str = "<font=HudSelectionText>"
		if (self.Author != "") then str = str .. title_color .. "Author:</color>\t" .. text_color .. self.Author .. "</color>\n" end
		if (self.Instructions!= "") then str = str .. title_color .. "Instructions:</color>\n" .. text_color .. self.Instructions .. "</color>\n" end
		str = str .. "</font>"
		
		self.InfoMarkup = markup.Parse(str, 250)
	end

	alpha = 180
	
	surface.SetDrawColor(0, 0, 0, alpha)
	surface.SetTexture(self.SpeechBubbleLid)
	
	surface.DrawTexturedRect(x, y - 69.5, 128, 64) 
	draw.RoundedBox(8, x - 5, y - 6, 260, self.InfoMarkup:GetHeight() + 18, Color(0, 0, 0, alpha))
	
	self.InfoMarkup:Draw(x + 5, y + 5, nil, nil, alpha)
end
-- End credit to Warshiper


--[[---------------------------------------------------------
   Name: SWEP:FreezeMovement()
   Desc: Return true to freeze moving the view
-----------------------------------------------------------]]
function SWEP:FreezeMovement()
	return false
end


--[[---------------------------------------------------------
   Name: SWEP:ViewModelDrawn( ViewModel )
   Desc: Called straight after the viewmodel has been drawn
-----------------------------------------------------------]]
function SWEP:ViewModelDrawn( ViewModel )
end


--[[---------------------------------------------------------
   Name: OnRestore
   Desc: Called immediately after a "load"
-----------------------------------------------------------]]
function SWEP:OnRestore()
end

--[[---------------------------------------------------------
   Name: OnRemove
   Desc: Called just before entity is deleted
-----------------------------------------------------------]]
function SWEP:OnRemove()
end

--[[---------------------------------------------------------
   Name: CustomAmmoDisplay
   Desc: Return a table
-----------------------------------------------------------]]
function SWEP:CustomAmmoDisplay()
end

--[[---------------------------------------------------------
   Name: GetViewModelPosition
   Desc: Allows you to re-position the view model
-----------------------------------------------------------]]

VmPos, VmAng = Vector(0,0,0), Angle(0,0,0)
WantedPos, WantedAng = Vector(0,0,0), Angle(0,0,0)
SetPos, SetAng = Vector(0,0,0), Angle(0,0,0)
VmSmooth = 0

function SWEP:GetViewModelPosition( pos, ang )
                       
	bIron = self:GetIronsight()
	Running = self:IsRunning()
	
	if bIron then
		self.SwayScale = 0.25
		self.BobScale = 0.25
	else
		self.SwayScale = 1
		self.BobScale = 1
	end
	
	if bIron and !Running then
		if self.IronSightsPos and self.IronSightsAng then
			SetPos = self.IronSightsPos
			SetAng = self.IronSightsAng
		end
		VmSmooth = self.IronsightTime * 30
	elseif Running and !bIron then
		if self.RunSightsPos and self.RunSightsAng then
			SetPos = self.RunSightsPos
			SetAng = self.RunSightsAng
		end
		VmSmooth = self.RunSightTime * 30
	elseif !bIron and !Running then
		SetPos = Vector(0,0,0)
		SetAng = Angle(0,0,0)
		VmSmooth = 5
	end

	WantedPos = LerpVector(FrameTime() * VmSmooth, WantedPos, SetPos)
	WantedAng = LerpAngle(FrameTime() * VmSmooth, WantedAng, SetAng)

	VmPos.x = WantedPos.x
	VmPos.y = WantedPos.y
	VmPos.z = WantedPos.z
   
	VmAng.p = WantedAng.p
	VmAng.y = WantedAng.y
	VmAng.r = WantedAng.r
   
	Right = ang:Right()
	Up = ang:Up()
	Forward = ang:Forward()
	
	ang = ang * 1
	ang:RotateAroundAxis( Right, VmAng.p )
	ang:RotateAroundAxis( Up, VmAng.y )
	ang:RotateAroundAxis( Forward, VmAng.r )      
   
	pos = pos + VmPos.x * Right
	pos = pos + VmPos.y * Forward
	pos = pos + VmPos.z * Up
   
	return pos, ang
       
end

--[[---------------------------------------------------------
   Name: TranslateFOV
   Desc: Allows the weapon to translate the player's FOV (clientside)
-----------------------------------------------------------]]
function SWEP:TranslateFOV( current_fov )
	
	return current_fov

end


--[[---------------------------------------------------------
   Name: DrawWorldModel
   Desc: Draws the world model (not the viewmodel)
-----------------------------------------------------------]]
function SWEP:DrawWorldModel()
	
	self.Weapon:DrawModel()

end


--[[---------------------------------------------------------
   Name: DrawWorldModelTranslucent
   Desc: Draws the world model (not the viewmodel)
-----------------------------------------------------------]]
function SWEP:DrawWorldModelTranslucent()
	
	self.Weapon:DrawModel()

end


--[[---------------------------------------------------------
   Name: AdjustMouseSensitivity()
   Desc: Allows you to adjust the mouse sensitivity.
-----------------------------------------------------------]]
function SWEP:AdjustMouseSensitivity()

	return nil
	
end

--[[---------------------------------------------------------
   Name: GetTracerOrigin()
   Desc: Allows you to override where the tracer comes from (in first person view)
		 returning anything but a vector indicates that you want the default action
-----------------------------------------------------------]]
function SWEP:GetTracerOrigin()

--[[
	local ply = self:GetOwner()
	local pos = ply:EyePos() + ply:EyeAngles():Right() * -5
	return pos
--]]

end

