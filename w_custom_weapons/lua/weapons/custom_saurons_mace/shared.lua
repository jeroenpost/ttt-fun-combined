
if( SERVER ) then
	AddCSLuaFile( "shared.lua" )
        --resource.AddFile("models/morrowind/molagbal/mace/v_molagbal_mace.mdl")
        --resource.AddFile("models/morrowind/molagbal/mace/w_molagbal_mace.mdl")
        --resource.AddFile("materials/vgui/entities/weapon_mor_molagbal_mace.vmt")
        --resource.AddFile("materials/morrowind/molagbal/mace/molagbal1.vmt")
        --resource.AddFile("materials/morrowind/molagbal/mace/molagbal2.vmt")
        --resource.AddFile("materials/morrowind/molagbal/mace/molagbal3.vmt")
        --resource.AddFile("sound/weapons/mace/morrowind_mace_deploy1.mp3")
        --resource.AddFile("sound/weapons/mace/morrowind_mace_hit.mp3")
        --resource.AddFile("sound/weapons/mace/morrowind_mace_hitwall1.mp3")
        --resource.AddFile("sound/weapons/mace/morrowind_mace_slash.mp3")
        --resource.AddFile("materials/models/v_models/hands/v_hands.vmt") 
end

SWEP.Icon = "materials/vgui/entities/weapon_mor_molagbal_mace"
SWEP.Kind = WEAPON_MELEE
 
	SWEP.PrintName = "Sauron's Mace"
	SWEP.Slot = 0
	SWEP.SlotPos = 1
	SWEP.DrawAmmo = false
	SWEP.DrawCrosshair = false

SWEP.WElements = {
	["element_name"] = { type = "Model", model = "models/morrowind/molagbal/mace/w_molagbal_mace.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(5, 1.363, -0.456), angle = Angle(-180, 0, 0), size = Vector(6.054, 6.054, 6.054), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
}

SWEP.Category = "Morrowind Maces"
SWEP.Author			= "Neotanks+Doug Tyrrell + Mad Cow For LUA (Models, Textures, ect. By: Hellsing/JJmolagbal)"
SWEP.Base			= "gb_base"
SWEP.Instructions	= "Left click to stab/slash/lunge and right click to throw, Throwing will be cabable on all future weapons for added coolness/badassery."
SWEP.Contact		= ""
SWEP.Purpose		= ""

SWEP.ViewModelFOV	= 72
SWEP.ViewModelFlip	= false

SWEP.Spawnable			= true
SWEP.AdminSpawnable		= true
  
SWEP.ViewModel      = "models/morrowind/molagbal/mace/v_molagbal_mace.mdl"
SWEP.WorldModel   = "models/morrowind/molagbal/mace/w_molagbal_mace.mdl"

SWEP.Primary.Damage		= 75
SWEP.Primary.NumShots		= 0
SWEP.Primary.Delay 		= 0.80

SWEP.Primary.ClipSize		= 5					// Size of a clip
SWEP.Primary.DefaultClip	= 5					// Default number of bullets in a clip
SWEP.Primary.Automatic		= true				// Automatic/Semi Auto
SWEP.Primary.Ammo			= "none"

SWEP.Secondary.ClipSize		= -1					// Size of a clip
SWEP.Secondary.DefaultClip	= -1					// Default number of bullets in a clip
SWEP.Secondary.Automatic	= false				// Automatic/Semi Auto
SWEP.Secondary.Ammo		= "none"




function SWEP:SecondaryAttack( soundfile )
    self:Fly()
end
    -- FLY
    SWEP.flyammo = 5
    SWEP.nextFly = 0
    function SWEP:Fly( soundfile )
        if self.nextFly > CurTime() or CLIENT then return end
        self.nextFly = CurTime() + 0.30
        if self.flyammo < 1 then return end

        if not soundfile then
            if SERVER then self.Owner:EmitSound(Sound("weapons/ls/lightsaber_swing.mp3")) end
            if SERVER then self.Owner:EmitSound(Sound("player/suit_sprint.wav")) end
        else
            if SERVER then self.Owner:EmitSound(Sound(soundfile)) end
        end
        self.flyammo = self.flyammo - 1
        self.Owner:SetAnimation(PLAYER_ATTACK1)
        local power = 400
        if math.random(1,5) < 2 then power = 100
        elseif math.random(1,15) < 2 then power = -500
        elseif math.random(1,15) < 2 then power = 2500
        else power = math.random( 350,450 ) end

        if SERVER then self.Owner:SetVelocity(self.Owner:GetForward() * power + Vector(0, 0, power)) end
        timer.Simple(20, function() if IsValid(self) then self.flyammo = self.flyammo + 1 end end)
        --timer.Simple(0.2,self.SecondaryAttackDelay,self)
    end

function SWEP:GivePrimaryAmmo( num )

    self:SetClip1( self:Clip1() + num )
    
end


function SWEP:Precache()
	util.PrecacheSound("weapons/mace/morrowind_mace_deploy1.mp3")
	util.PrecacheSound("weapons/mace/morrowind_mace_hitwall1.mp3")
	util.PrecacheSound("weapons/mace/morrowind_mace_hit.mp3")
	util.PrecacheSound("weapons/mace/morrowind_mace_slash.mp3")
end

function SWEP:Initialize()
    self:SetHoldType("melee")
	self.Hit = { 
	Sound( "weapons/mace/morrowind_mace_hitwall1.mp3" )}
	self.FleshHit = {
  	Sound("weapons/mace/morrowind_mace_hit.mp3") }
end

function SWEP:Deploy()
         self:SetHoldType("melee")
	self.Owner:EmitSound("weapons/mace/morrowind_mace_deploy1.mp3")
	return true
end

function SWEP:PrimaryAttack()
	self:SetNextPrimaryFire(CurTime() + self.Primary.Delay)
	self:SetNextSecondaryFire(CurTime() + self.Primary.Delay)
	self.Owner:SetAnimation( PLAYER_ATTACK1 )
	self.Weapon:SendWeaponAnim( ACT_VM_HITCENTER )
	timer.Simple(.05, function()
		if !IsValid(self) or !IsValid(self.Owner) then return end
		local trace = self.Owner:GetEyeTrace()
		if trace.HitPos:Distance(self.Owner:GetShootPos()) <= 200 then
			if( trace.Entity:IsPlayer() or trace.Entity:IsNPC() or trace.Entity:GetClass()=="prop_ragdoll" ) then
				self.Owner:EmitSound( self.FleshHit[math.random(1,#self.FleshHit)] )
			else 
				self.Owner:EmitSound( self.Hit[math.random(1,#self.Hit)] )
			end
				bullet = {}
				bullet.Num    = 1
				bullet.Src    = self.Owner:GetShootPos()
				bullet.Dir    = self.Owner:GetAimVector()
				bullet.Spread = Vector(0, 0, 0)
				bullet.Tracer = 0
				bullet.Force  = 1
				bullet.Damage = 75
			self.Owner:FireBullets(bullet) 
			self.Owner:ViewPunch(Angle(7, 0, 0))
		else
			self.Weapon:EmitSound("weapons/mace/morrowind_mace_slash.mp3")
		end
	end)
end

function SWEP:Holster()
	if self:GetNextPrimaryFire() > CurTime() then return end
	return true
end

/*---------------------------------------------------------
   Name: SWEP:SecondaryAttack()
   Desc: +attack2 has been pressed.
---------------------------------------------------------*/
function SWEP:Reload()
	self.Weapon:EmitSound("weapons/mace/morrowind_mace_slash.mp3")
	self.Weapon:SetNextPrimaryFire(CurTime() + 1)
	self.Weapon:SetNextSecondaryFire(CurTime() + 1)
	self.Weapon:SendWeaponAnim(ACT_VM_HITCENTER)

	if (SERVER) then
		timer.Simple(.15, function()
			if  !IsValid(self) or  !IsValid(self.Owner) then return end
			local knife = ents.Create("ent_mor_mace")
			knife:SetAngles(self.Owner:EyeAngles())
			local pos = self.Owner:GetShootPos()
				pos = pos + self.Owner:GetForward() * 5
				pos = pos + self.Owner:GetRight() * 9
				pos = pos + self.Owner:GetUp() * -5
			knife:SetPos(pos)
			knife:SetOwner(self.Owner)
			knife.Weapon = self		// Used to se the mace's model and the weapon it gives when used.
			knife:Spawn()
			knife:Activate()

			self.Owner:SetAnimation(PLAYER_ATTACK1)

			local phys = knife:GetPhysicsObject()
			phys:SetVelocity(self.Owner:GetAimVector() * 1500)
			phys:AddAngleVelocity(Vector(0, 500, 0))
			self:Remove()
		end)
	end
end