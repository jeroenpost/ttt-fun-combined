include "weapons/weapon_zm_shotgun.lua"
SWEP.Base = "gb_camo_base"
SWEP.ViewModelFOV	= 54
SWEP.HoldType = "smg"
SWEP.PrintName = "Shotty Bugatti"
SWEP.ViewModel		= "models/weapons/cstrike/c_shot_m3super90.mdl"
SWEP.WorldModel		= "models/weapons/w_shot_m3super90.mdl"
SWEP.AutoSpawnable = false
SWEP.Camo = -1
SWEP.HoldType = "shotgun"
SWEP.Slot = 13
SWEP.Kind = 13

if CLIENT then

    SWEP.Author = "GreenBlack"

    SWEP.Icon = "vgui/ttt_fun_killicons/xm1014.png"
end

SWEP.Primary.Sound			= Sound( "Weapon_M3.Single" )
SWEP.Primary.Recoil			= 1

SWEP.Primary.Ammo = "Buckshot"
SWEP.Primary.Damage = 7
SWEP.Primary.Cone = 0.085
SWEP.Primary.Delay = 0.45
SWEP.Primary.ClipSize = 250
SWEP.Primary.ClipMax = 250
SWEP.Primary.DefaultClip = 250
SWEP.Primary.Automatic = true
SWEP.Primary.NumShots = 10
SWEP.AutoSpawnable      = false
SWEP.AmmoEnt = "item_box_buckshot_ttt"

function SWEP:Deploy()
    self.dt.reloading = false
    self.reloadtimer = 0
    local function playerDiesSound( victim, weapon, killer )
        if( IsValid(killer) and killer:IsPlayer()  ) then
            local wep = killer:GetActiveWeapon()
            if IsValid(wep) then
                wep = wep:GetClass()
                if wep == "custom_shotty_bugatti" then
                    gb_PlaySoundFromServer(gb_config.websounds.."shottybugatti.mp3", killer)
                end
            end
        end
    end

    hook.Add( "PlayerDeath", "playerDeathSound_bugatti", playerDiesSound )
    return self:DeployFunction()
end

function SWEP:PrimaryAttack()
    if self.Owner:KeyDown(IN_USE) then
        self:ShootFlare()
        return
    end
    self.BaseClass.PrimaryAttack(self)
end

SWEP.IronSightsPos = Vector(-7.65, -12.214, 3.2)
SWEP.IronSightsAng = Vector(0, 0, 0)
SWEP.NextBolt = 0
SWEP.nextprimary = 0
function SWEP:SecondaryAttack()
    if self.Owner:KeyDown(IN_RELOAD) then
        if self.NextBolt < CurTime() then
        self:MakeExplosion()
        self:FireBolt(0,10)
            self.NextBolt = CurTime() + 0.5
        end
        return
    end
    if self.Owner:KeyDown(IN_USE) then
        if CLIENT then return end
        if self.nextprimary and  self.nextprimary > CurTime() then
            self.Owner:PrintMessage( HUD_PRINTCENTER,  math.floor(self.nextprimary - CurTime()).." seconds left" )
            return

        end
        self.nextprimary = CurTime() + 30
        local ent = ents.Create("cse_ent_shorthealthgrenade")
        ent:SetOwner(self.Owner)
        ent.Owner = self.Owner
        ent.healdamage = 5
        ent:SetPos(self.Owner:GetEyeTrace().HitPos + self.Owner:GetAimVector() * -36  )
        ent:SetAngles(Angle(1,0,0))

        ent:Spawn()


        ent.timer = CurTime() + 2
        return
    end
    self:Fly()
end

function SWEP:Reload()
    if self.Owner:KeyDown(IN_ATTACK2) then
        return
    end
    if self.Owner:KeyDown(IN_USE) then
        self:DropHealth("normal_health_station")
        return
    end
    self.BaseClass.Reload(self)
end