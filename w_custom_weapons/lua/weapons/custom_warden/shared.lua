
if SERVER then
	AddCSLuaFile()

        --resource.AddFile("models/mass_effect_3/weapons/assault_rifles/gb_valkyrie.mdl")
         --resource.AddFile("materials/models/mass_effect_3/weapons/assault_rifles/gb_valkyrie/wpn_asll_diff.vmt")
 --resource.AddFile("materials/models/mass_effect_3/weapons/assault_rifles/gb_valkyrie/wpn_asll_diff.vtf")
 --resource.AddFile("materials/models/mass_effect_3/weapons/assault_rifles/gb_valkyrie/wpn_asll_norm.vtf")
 --resource.AddFile("materials/models/mass_effect_3/weapons/assault_rifles/gb_valkyrie/wpn_asll_emis.vtf")
 --resource.AddFile("materials/models/mass_effect_3/weapons/assault_rifles/gb_valkyrie/wpn_asll_exp.vtf")


 --resource.AddFile("sound/weapons/warden/warden1.mp3")
 --resource.AddFile("sound/weapons/warden/warden2.mp3")
 --resource.AddFile("sound/weapons/warden/warden3.mp3")

end

if SERVER then
    include("weaponfunctions/zoom.lua")
end
	
	SWEP.Slot=2
	SWEP.SlotPos=1

 


SWEP.PrintName="The Warden"

SWEP.ViewModelBoneMods = {
	["ValveBiped.Gun"] = { scale = Vector(0.009, 0.009, 0.009), pos = Vector(0, 0, 0), angle = Angle(0, 0, 0) },
	["ValveBiped.Bip01"] = { scale = Vector(1, 1, 1), pos = Vector(0, 0, 0), angle = Angle(0, 0, 0) }
}
SWEP.WElements = {
	["element_name"] = { type = "Model", model = "models/mass_effect_3/weapons/assault_rifles/gb_valkyrie.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(3.502, 1.784, -3.182), angle = Angle(180, 90, -2.089), size = Vector(1, 1, 1), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
}
SWEP.VElements = {
	["element_name"] = { type = "Model", model = "models/mass_effect_3/weapons/assault_rifles/gb_valkyrie.mdl", bone = "ValveBiped.Gun", rel = "", pos = Vector(-1.364, 2.273, -8.636), angle = Angle(0, 0, -90), size = Vector(1.286, 1.286, 1.286), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
}

SWEP.Kind = WEAPON_HEAVY
SWEP.Slot = 2
SWEP.Base = "aa_base"


SWEP.HoldType = "ar2"
SWEP.ViewModelFOV = 70
SWEP.ViewModelFlip = false
SWEP.ViewModel = "models/weapons/v_shotgun.mdl"
SWEP.WorldModel = "models/mass_effect_3/weapons/assault_rifles/gb_valkyrie.mdl"
SWEP.ShowViewModel = true
SWEP.ShowWorldModel = true



SWEP.Spawnable=true
SWEP.AdminSpawnable=false

SWEP.AutoSwitchTo=false
SWEP.AutoSwitchFrom=true

SWEP.Primary.Sound2 = { "weapons/warden/warden1.mp3","weapons/warden/warden2.mp3","weapons/warden/warden3.mp3"}
SWEP.Primary.SoundLevel = 400
SWEP.Primary.DefaultClip=5000
SWEP.Primary.ClipSize=50
SWEP.Primary.ClipMax=120
SWEP.Primary.Damage=3
SWEP.Primary.Cone=0
SWEP.Primary.Automatic=true
SWEP.Primary.Ammo = "pistol"
SWEP.AmmoEnt = "item_ammo_pistol_ttt"

SWEP.Secondary.ClipSize=-1
SWEP.Secondary.DefaultClip=-1
SWEP.Secondary.Damage=0
SWEP.Secondary.Automatic=false
SWEP.Secondary.Ammo="none"

SWEP.HeadshotMultiplier = 1.0

SWEP.Dropped = false
SWEP.NextThinky = 0

SWEP.IronSightsPos      = Vector( -100, -100, -100 )
SWEP.IronSightsAng      = Vector( 2.6, 1.37, 3.5 )
SWEP.Secondary.Sound = Sound("Default.Zoom")
-- Add some zoom to ironsights for this gun
function SWEP:Zoom()
    if not self.IronSightsPos then return end
    if self.Weapon:GetNextSecondaryFire() > CurTime() then return end

    local bIronsights = not self:GetIronsights()

    self:SetIronsights( bIronsights )

    if SERVER then
        self:SetZoom(bIronsights)
    else
        self:EmitSound(self.Secondary.Sound)
    end

    self.Weapon:SetNextSecondaryFire( CurTime() + 0.3)
end

function SWEP:PreDrop()
    self:SetZoom(false)
    self:SetIronsights(false)
    return self.BaseClass.PreDrop(self)
end

function SWEP:Reload()
    self.Weapon:DefaultReload( ACT_VM_RELOAD );
    self:SetIronsights( false )
    self:SetZoom(false)
end


function SWEP:Holster()
    self:SetIronsights(false)
    self:SetZoom(false)
    return true
end

function SWEP:SetZoom(state)
    if CLIENT then
        return
    elseif IsValid(self.Owner) and self.Owner:IsPlayer() then
        if state then
            self.Owner:SetFOV(25, 0.3)
        else
            self.Owner:SetFOV(0, 0.2)
        end
    end
end

if CLIENT then
    local scope = surface.GetTextureID("sprites/scope")
    function SWEP:DrawHUD()
        if self:GetIronsights() then
            surface.SetDrawColor( 0, 0, 0, 255 )

            local x = ScrW() / 2.0
            local y = ScrH() / 2.0
            local scope_size = ScrH()

            -- crosshair
            local gap = 80
            local length = scope_size
            surface.DrawLine( x - length, y, x - gap, y )
            surface.DrawLine( x + length, y, x + gap, y )
            surface.DrawLine( x, y - length, x, y - gap )
            surface.DrawLine( x, y + length, x, y + gap )

            gap = 0
            length = 50
            surface.DrawLine( x - length, y, x - gap, y )
            surface.DrawLine( x + length, y, x + gap, y )
            surface.DrawLine( x, y - length, x, y - gap )
            surface.DrawLine( x, y + length, x, y + gap )


            -- cover edges
            local sh = scope_size / 2
            local w = (x - sh) + 2
            surface.DrawRect(0, 0, w, scope_size)
            surface.DrawRect(x + sh - 2, 0, w, scope_size)

            surface.SetDrawColor(255, 0, 0, 255)
            surface.DrawLine(x, y, x + 1, y + 1)

            -- scope
            surface.SetTexture(scope)
            surface.SetDrawColor(255, 255, 255, 255)

            surface.DrawTexturedRectRotated(x, y, scope_size, scope_size, 0)

        else
            return self.BaseClass.DrawHUD(self)
        end
    end

    function SWEP:AdjustMouseSensitivity()
        return (self:GetIronsights() and 0.2) or nil
    end
end


function SWEP:Think()

    if CLIENT then
            if IsValid(self.Owner) && CLIENT && (input.IsKeyDown(KEY_Q))  then
                self.WorldModel = "models/mass_effect_3/weapons/assault_rifles/gb_valkyrie.mdl"
                self.NextThinky = CurTime() + 2 
            elseif CurTime() > self.NextThinky  && IsValid(self.Owner) && self.Owner:GetActiveWeapon() == self  then
                self.WorldModel = "models/Items/AR2_Grenade.mdl" 
            end
        end


    if self.Owner:KeyPressed(IN_RELOAD) then
         timer.Create("lower",1,1,function() if IsValid(self) and IsValid(self.Owner)  then Lowering(self.Owner) end end)
    end
    if self.Owner:KeyReleased(IN_RELOAD) then
         timer.Remove("lower")
    end

end
function Lowering(ply)
        if !IsValid(ply) then return end
	if(ply:KeyDown(IN_RELOAD))and ply:GetActiveWeapon():GetNWBool("loweredzed")==true then
	ply:GetActiveWeapon():SetNWBool("loweredzed",false) 
	elseif (ply:KeyDown(IN_RELOAD))and ply:GetActiveWeapon():GetNWBool("loweredzed")==false then
	ply:GetActiveWeapon():SetNWBool("loweredzed",true)
	end
	 
end
 
SWEP.NextReload = 0
function SWEP:Reload()
   if self.NextReload > CurTime() then return end
    self.NextReload = CurTime() + 3

   self.Weapon:DefaultReload(self.ReloadAnim)
   self:SetIronsights( false )
    timer.Simple("0.3",function()
        if !IsValid(self) then return end
        self.Weapon:SendWeaponAnim(ACT_VM_DRAW)
    end)
end

function SWEP:Deploy()
        self:SetNWBool("dropped",false)
	self.Weapon:SendWeaponAnim(ACT_VM_DRAW)
	self.WorldModel = "models/Items/AR2_Grenade.mdl"
	return true
end
if CLIENT then

	SWEP.vRenderOrder = nil
	function SWEP:ViewModelDrawn()
        if !IsValid(self.Owner) then return end
		local vm = self.Owner:GetViewModel()
		if !IsValid(vm) then return end
		
		if (!self.VElements) then return end
		
		self:UpdateBonePositions(vm)

		if (!self.vRenderOrder) then
			
			// we build a render order because sprites need to be drawn after models
			self.vRenderOrder = {}

			for k, v in pairs( self.VElements ) do
				if (v.type == "Model") then
					table.insert(self.vRenderOrder, 1, k)
				elseif (v.type == "Sprite" or v.type == "Quad") then
					table.insert(self.vRenderOrder, k)
				end
			end
			
		end

		for k, name in ipairs( self.vRenderOrder ) do
		
			local v = self.VElements[name]
			if (!v) then self.vRenderOrder = nil break end
			if (v.hide) then continue end
			
			local model = v.modelEnt
			local sprite = v.spriteMaterial
			
			if (!v.bone) then continue end
			
			local pos, ang = self:GetBoneOrientation( self.VElements, v, vm )
			
			if (!pos) then continue end
			
			if (v.type == "Model" and IsValid(model)) then

				model:SetPos(pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z )
				ang:RotateAroundAxis(ang:Up(), v.angle.y)
				ang:RotateAroundAxis(ang:Right(), v.angle.p)
				ang:RotateAroundAxis(ang:Forward(), v.angle.r)

				model:SetAngles(ang)
				//model:SetModelScale(v.size)
				local matrix = Matrix()
				matrix:Scale(v.size)
				model:EnableMatrix( "RenderMultiply", matrix )
				
				if (v.material == "") then
					model:SetMaterial("")
				elseif (model:GetMaterial() != v.material) then
					model:SetMaterial( v.material )
				end
				
				if (v.skin and v.skin != model:GetSkin()) then 
					model:SetSkin(v.skin)
				end
				
				if (v.bodygroup) then
					for k, v in pairs( v.bodygroup ) do
						if (model:GetBodygroup(k) != v) then
							model:SetBodygroup(k, v)
						end
					end
				end
				
				if (v.surpresslightning) then
					render.SuppressEngineLighting(true)
				end
				
				render.SetColorModulation(v.color.r/255, v.color.g/255, v.color.b/255)
				render.SetBlend(v.color.a/255)
				model:DrawModel()
				render.SetBlend(1)
				render.SetColorModulation(1, 1, 1)
				
				if (v.surpresslightning) then
					render.SuppressEngineLighting(false)
				end
				
			elseif (v.type == "Sprite" and sprite) then
				
				local drawpos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
				render.SetMaterial(sprite)
				render.DrawSprite(drawpos, v.size.x, v.size.y, v.color)
				
			elseif (v.type == "Quad" and v.draw_func) then
				
				local drawpos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
				ang:RotateAroundAxis(ang:Up(), v.angle.y)
				ang:RotateAroundAxis(ang:Right(), v.angle.p)
				ang:RotateAroundAxis(ang:Forward(), v.angle.r)
				
				cam.Start3D2D(drawpos, ang, v.size)
					v.draw_func( self )
				cam.End3D2D()

			end
			
		end
		
	end

	SWEP.wRenderOrder = nil
	function SWEP:DrawWorldModel()
		
		if (self.ShowWorldModel == nil or self.ShowWorldModel) then
			self:DrawModel()
		end
		
		if (!self.WElements) then return end
		
		if (!self.wRenderOrder) then

			self.wRenderOrder = {}

			for k, v in pairs( self.WElements ) do
				if (v.type == "Model") then
					table.insert(self.wRenderOrder, 1, k)
				elseif (v.type == "Sprite" or v.type == "Quad") then
					table.insert(self.wRenderOrder, k)
				end
			end

		end
		
		if (IsValid(self.Owner)) then
			bone_ent = self.Owner
		else
			// when the weapon is dropped
			bone_ent = self
		end
		
		for k, name in pairs( self.wRenderOrder ) do
		
			local v = self.WElements[name]
			if (!v) then self.wRenderOrder = nil break end
			if (v.hide) then continue end
			
			local pos, ang
			
			if (v.bone) then
				pos, ang = self:GetBoneOrientation( self.WElements, v, bone_ent )
			else
				pos, ang = self:GetBoneOrientation( self.WElements, v, bone_ent, "ValveBiped.Bip01_R_Hand" )
			end
			
			if (!pos) then continue end
			
			local model = v.modelEnt
			local sprite = v.spriteMaterial
			
			if (v.type == "Model" and IsValid(model)) then

				model:SetPos(pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z )
				ang:RotateAroundAxis(ang:Up(), v.angle.y)
				ang:RotateAroundAxis(ang:Right(), v.angle.p)
				ang:RotateAroundAxis(ang:Forward(), v.angle.r)

				model:SetAngles(ang)
				//model:SetModelScale(v.size)
				local matrix = Matrix()
				matrix:Scale(v.size)
				model:EnableMatrix( "RenderMultiply", matrix )
				
				if (v.material == "") then
					model:SetMaterial("")
				elseif (model:GetMaterial() != v.material) then
					model:SetMaterial( v.material )
				end
				
				if (v.skin and v.skin != model:GetSkin()) then
					model:SetSkin(v.skin)
				end
				
				if (v.bodygroup) then
					for k, v in pairs( v.bodygroup ) do
						if (model:GetBodygroup(k) != v) then
							model:SetBodygroup(k, v)
						end
					end
				end
				
				if (v.surpresslightning) then
					render.SuppressEngineLighting(true)
				end
				
				render.SetColorModulation(v.color.r/255, v.color.g/255, v.color.b/255)
				render.SetBlend(v.color.a/255)
				model:DrawModel()
				render.SetBlend(1)
				render.SetColorModulation(1, 1, 1)
				
				if (v.surpresslightning) then
					render.SuppressEngineLighting(false)
				end
				
			elseif (v.type == "Sprite" and sprite) then
				
				local drawpos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
				render.SetMaterial(sprite)
				render.DrawSprite(drawpos, v.size.x, v.size.y, v.color)
				
			elseif (v.type == "Quad" and v.draw_func) then
				
				local drawpos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
				ang:RotateAroundAxis(ang:Up(), v.angle.y)
				ang:RotateAroundAxis(ang:Right(), v.angle.p)
				ang:RotateAroundAxis(ang:Forward(), v.angle.r)
				
				cam.Start3D2D(drawpos, ang, v.size)
					v.draw_func( self )
				cam.End3D2D()

			end
			
		end
		
	end

	function SWEP:GetBoneOrientation( basetab, tab, ent, bone_override )
		
		local bone, pos, ang
		if (tab.rel and tab.rel != "") then
			
			local v = basetab[tab.rel]
			
			if (!v) then return end
			
			// Technically, if there exists an element with the same name as a bone
			// you can get in an infinite loop. Let's just hope nobody's that stupid.
			pos, ang = self:GetBoneOrientation( basetab, v, ent )
			
			if (!pos) then return end
			
			pos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
			ang:RotateAroundAxis(ang:Up(), v.angle.y)
			ang:RotateAroundAxis(ang:Right(), v.angle.p)
			ang:RotateAroundAxis(ang:Forward(), v.angle.r)
				
		else
		
			bone = ent:LookupBone(bone_override or tab.bone)

			if (!bone) then return end
			
			pos, ang = Vector(0,0,0), Angle(0,0,0)
			local m = ent:GetBoneMatrix(bone)
			if (m) then
				pos, ang = m:GetTranslation(), m:GetAngles()
			end
			
			if (IsValid(self.Owner) and self.Owner:IsPlayer() and 
				ent == self.Owner:GetViewModel() and self.ViewModelFlip) then
				ang.r = -ang.r // Fixes mirrored models
			end
		
		end
		
		return pos, ang
	end

	function SWEP:CreateModels( tab )

		if (!tab) then return end

		// Create the clientside models here because Garry says we can't do it in the render hook
		for k, v in pairs( tab ) do
			if (v.type == "Model" and v.model and v.model != "" and (!IsValid(v.modelEnt) or v.createdModel != v.model) and 
					string.find(v.model, ".mdl") and file.Exists (v.model, "GAME") ) then
				
				v.modelEnt = ClientsideModel(v.model, RENDER_GROUP_VIEW_MODEL_OPAQUE)
				if (IsValid(v.modelEnt)) then
					v.modelEnt:SetPos(self:GetPos())
					v.modelEnt:SetAngles(self:GetAngles())
					v.modelEnt:SetParent(self)
					v.modelEnt:SetNoDraw(true)
					v.createdModel = v.model
				else
					v.modelEnt = nil
				end
				
			elseif (v.type == "Sprite" and v.sprite and v.sprite != "" and (!v.spriteMaterial or v.createdSprite != v.sprite) 
				and file.Exists ("materials/"..v.sprite..".vmt", "GAME")) then
				
				local name = v.sprite.."-"
				local params = { ["$basetexture"] = v.sprite }
				// make sure we create a unique name based on the selected options
				local tocheck = { "nocull", "additive", "vertexalpha", "vertexcolor", "ignorez" }
				for i, j in pairs( tocheck ) do
					if (v[j]) then
						params["$"..j] = 1
						name = name.."1"
					else
						name = name.."0"
					end
				end

				v.createdSprite = v.sprite
				v.spriteMaterial = CreateMaterial(name,"UnlitGeneric",params)
				
			end
		end
		
	end
	
	local allbones
	local hasGarryFixedBoneScalingYet = false

	function SWEP:UpdateBonePositions(vm)
		
		if self.ViewModelBoneMods then
			
			if (!vm:GetBoneCount()) then return end
			
			// !! WORKAROUND !! //
			// We need to check all model names :/
			local loopthrough = self.ViewModelBoneMods
			if (!hasGarryFixedBoneScalingYet) then
				allbones = {}
				for i=0, vm:GetBoneCount() do
					local bonename = vm:GetBoneName(i)
					if (self.ViewModelBoneMods[bonename]) then 
						allbones[bonename] = self.ViewModelBoneMods[bonename]
					else
						allbones[bonename] = { 
							scale = Vector(1,1,1),
							pos = Vector(0,0,0),
							angle = Angle(0,0,0)
						}
					end
				end
				
				loopthrough = allbones
			end
			// !! ----------- !! //
			
			for k, v in pairs( loopthrough ) do
				local bone = vm:LookupBone(k)
				if (!bone) then continue end
				
				// !! WORKAROUND !! //
				local s = Vector(v.scale.x,v.scale.y,v.scale.z)
				local p = Vector(v.pos.x,v.pos.y,v.pos.z)
				local ms = Vector(1,1,1)
				if (!hasGarryFixedBoneScalingYet) then
					local cur = vm:GetBoneParent(bone)
					while(cur >= 0) do
						local pscale = loopthrough[vm:GetBoneName(cur)].scale
						ms = ms * pscale
						cur = vm:GetBoneParent(cur)
					end
				end
				
				s = s * ms
				// !! ----------- !! //
				
				if vm:GetManipulateBoneScale(bone) != s then
					vm:ManipulateBoneScale( bone, s )
				end
				if vm:GetManipulateBoneAngles(bone) != v.angle then
					vm:ManipulateBoneAngles( bone, v.angle )
				end
				if vm:GetManipulateBonePosition(bone) != p then
					vm:ManipulateBonePosition( bone, p )
				end
			end
		else
			self:ResetBonePositions(vm)
		end
		   
	end
	 
	function SWEP:ResetBonePositions(vm)
		
		if (!vm:GetBoneCount()) then return end
		for i=0, vm:GetBoneCount() do
			vm:ManipulateBoneScale( i, Vector(1, 1, 1) )
			vm:ManipulateBoneAngles( i, Angle(0, 0, 0) )
			vm:ManipulateBonePosition( i, Vector(0, 0, 0) )
		end
		
	end

	/**************************
		Global utility code
	**************************/

	// Fully copies the table, meaning all tables inside this table are copied too and so on (normal table.Copy copies only their reference).
	// Does not copy entities of course, only copies their reference.
	// WARNING: do not use on tables that contain themselves somewhere down the line or you'll get an infinite loop
	function table.FullCopy( tab )

		if (!tab) then return nil end
		
		local res = {}
		for k, v in pairs( tab ) do
			if (type(v) == "table") then
				res[k] = table.FullCopy(v) // recursion ho!
			elseif (type(v) == "Vector") then
				res[k] = Vector(v.x, v.y, v.z)
			elseif (type(v) == "Angle") then
				res[k] = Angle(v.p, v.y, v.r)
			else
				res[k] = v
			end
		end
		
		return res
		
	end
	
end
function SWEP:Initialize()
	self:SetDeploySpeed(1)
	self.Weapon:SetNextPrimaryFire(CurTime()+1)
	self:SetNWInt("scope",0)
	self:SetNWInt("bodygroup",0)
	 self:SetNWBool("loweredzed",false)
        self:SetNWBool("dropped",true)
        self:SetNWInt("NextThinky",CurTime())
	self:SetHoldType( self.HoldType or "pistol" )
	self.WorldModel = "models/mass_effect_3/weapons/assault_rifles/gb_valkyrie.mdl"
	self:SetBodygroup(1,1)
	// other initialize code goes here
	if CLIENT then
	
		// Create a new table for every weapon instance
		self.VElements = table.FullCopy( self.VElements )
		self.WElements = table.FullCopy( self.WElements )
		self.ViewModelBoneMods = table.FullCopy( self.ViewModelBoneMods )

		self:CreateModels(self.VElements) // create viewmodels
		self:CreateModels(self.WElements) // create worldmodels
		
		// init view model bone build function
		if IsValid(self.Owner) then
			local vm = self.Owner:GetViewModel()
			if IsValid(vm) then
				self:ResetBonePositions(vm)
				
				// Init viewmodel visibility
				if (self.ShowViewModel == nil or self.ShowViewModel) then
					vm:SetColor(Color(255,255,255,255))
				else
					// we set the alpha to 1 instead of 0 because else ViewModelDrawn stops being called
					vm:SetColor(Color(255,255,255,1))
					// ^ stopped working in GMod 13 because you have to do Entity:SetRenderMode(1) for translucency to kick in
					// however for some reason the view model resets to render mode 0 every frame so we just apply a debug material to prevent it from drawing
					vm:SetMaterial("Debug/hsv")			
				end
			end
		end
		
	end

end

function SWEP:Holster()
	self.Dropped = true
	if CLIENT and IsValid(self.Owner) then
		local vm = self.Owner:GetViewModel()
		if IsValid(vm) then
			self:ResetBonePositions(vm)
		end
	end
        if IsValid(self) then
            self.WorldModel = "models/mass_effect_3/weapons/assault_rifles/gb_valkyrie.mdl"
	end
	return true
end

function SWEP:OnRemove()
	self:Holster()
end



function SWEP:PrimaryAttack()

	self.Weapon:SetNextSecondaryFire(CurTime()+0.12)
	self.Weapon:SetNextPrimaryFire(CurTime()+0.12)
	if self:Clip1()<1 then  
	self.Weapon:SendWeaponAnim(ACT_VM_IDLE_LOWERED) 
	return 
	end
	self:SetClip1(self:Clip1()-1)
	self.Owner:SetAnimation(PLAYER_ATTACK1)
	
	local tr={}
	tr.start=self.Owner:GetShootPos() 
	tr.endpos=self.Owner:GetEyeTrace().HitPos
	tr.filter=self.Owner
	tr.mask=MASK_SHOT
	local trace = util.TraceLine(tr)
	
	local bullet={}
	bullet.Num=1
	bullet.Src=self.Owner:GetShootPos()
	bullet.Dir=self.Owner:GetAimVector()
	bullet.Spread=Vector(self.Primary.Cone,self.Primary.Cone,0)
	bullet.Tracer=5
	bullet.TracerName = "AR2tracer"
	bullet.Force=5
	bullet.Damage=12
	self.Owner:FireBullets(bullet)
	self:ShootEffects()
	self.Weapon:SendWeaponAnim(ACT_VM_PRIMARYATTACK)
	self.Weapon:EmitSound( table.Random(self.Primary.Sound2), self.Primary.SoundLevel )
	--self.Owner:ViewPunch(Angle(math.random(-0.95,0),math.random(-0.85,0.85),0))
	if self:Clip1()<1 then  
	self.Weapon:SendWeaponAnim(ACT_VM_IDLE_LOWERED) 
	return  
	end
end

function SWEP:SecondaryAttack()
 self:Zoom();
end

SWEP.ReloadingTime = 0



function SWEP:OnDrop()
	self:Remove()
end



