
if SERVER then
   --resource.AddFile("materials/vgui/ttt_fun_killicons/m4a1.png")
   AddCSLuaFile( "shared.lua" )
end

SWEP.HoldType			= "ar2"

SWEP.PrintName			= "Irish's M16"
 SWEP.Slot				= 2
if CLIENT then

   
  
   SWEP.Author = "GreenBlack"
   SWEP.Icon = "vgui/ttt_fun_killicons/m4a1.png"
end


SWEP.Base				= "weapon_tttbase"
SWEP.Spawnable = true
SWEP.AdminSpawnable = true


SWEP.Primary.Delay			= 0.14
SWEP.Primary.Recoil			= 0.06
SWEP.Primary.Automatic = true
SWEP.Primary.Ammo = "Pistol" 
SWEP.Primary.Damage = 20
SWEP.Primary.Cone = 0.018
SWEP.Primary.ClipSize = 35
SWEP.Primary.ClipMax = 90
SWEP.Primary.DefaultClip = 35
SWEP.AutoSpawnable      = false
SWEP.AmmoEnt = "item_ammo_pistol_ttt"


SWEP.ViewModelFOV		= 74
SWEP.ViewModelFlip		= false
SWEP.ViewModel			= Model("models/weapons/i_rif_m16a2.mdl")
SWEP.WorldModel			= "models/weapons/w_rif_m4a1.mdl"
SWEP.ViewModelFOV      = 65
 
SWEP.Primary.Sound = Sound( "Weapon_M4A1.Single" )

SWEP.IronSightsPos = Vector (-2.0314, -4.000, 0.8416)
SWEP.IronSightsAng = Vector (0.0194, 0.0163, 0)


function SWEP:SetZoom(state)
   if CLIENT then return end
   if not (IsValid(self.Owner) and self.Owner:IsPlayer()) then return end
   if state then
      self.Owner:SetFOV(35, 0.5)
   else
      self.Owner:SetFOV(0, 0.2)
   end
end

-- Add some zoom to ironsights for this gun
function SWEP:SecondaryAttack()
   if not self.IronSightsPos then return end
   if self.Weapon:GetNextSecondaryFire() > CurTime() then return end

   bIronsights = not self:GetIronsights()

   self:SetIronsights( bIronsights )

   if SERVER then
      self:SetZoom(bIronsights)
   end

   self.Weapon:SetNextSecondaryFire(CurTime() + 0.3)
end

function SWEP:PreDrop()
   self:SetZoom(false)
   self:SetIronsights(false)
   return self.BaseClass.PreDrop(self)
end

function SWEP:Reload()
   self.Weapon:DefaultReload( ACT_VM_RELOAD );
   self:SetIronsights( false )
   self:SetZoom(false)
end


function SWEP:Holster()
   self:SetIronsights(false)
   self:SetZoom(false)
   return true
end


