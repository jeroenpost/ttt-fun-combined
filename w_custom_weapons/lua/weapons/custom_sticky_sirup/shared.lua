
SWEP.Spawnable            = true
SWEP.AdminSpawnable        = true

SWEP.HoldType = "pistol"

SWEP.Author            = "Clavus"
SWEP.Contact        = ""
SWEP.Purpose        = "Shooting stuff in the face"
SWEP.Instructions    = "Point & click"

SWEP.Base = "aa_base"
SWEP.Kind = WEAPON_CARRY
SWEP.PrintName = "Faded Puncakes"
SWEP.DrawCrosshair        = true
if CLIENT then
            
    SWEP.Slot = 4
    SWEP.SlotPos = 1
    SWEP.ViewModelFlip = false
    SWEP.ViewModelFOV        = 70
    
    SWEP.DrawAmmo            = true
    SWEP.DrawCrosshair        = true
    SWEP.CSMuzzleFlashes    = false
    
end

SWEP.ViewModel = "models/weapons/v_grenade.mdl"
SWEP.WorldModel = "models/weapons/w_grenade.mdl"

SWEP.Primary.Sound 		= Sound( "weapons/puncake_green1.mp3" )
SWEP.Primary.Recoil            = 6
SWEP.Primary.Damage            = 75
SWEP.Primary.Force            = 300
SWEP.Primary.NumShots        = 1
SWEP.Primary.Delay            = 2
SWEP.Primary.Ammo             = "battery"
SWEP.ShowWorldModel = false
SWEP.Primary.ClipSize        = 5
SWEP.Primary.DefaultClip    = 5
SWEP.Primary.Automatic        = false

SWEP.Secondary.ClipSize		= -1
SWEP.Secondary.DefaultClip	= -1
SWEP.Secondary.Automatic	= false
SWEP.Secondary.Ammo			= "none"

SWEP.IronSightsPos = Vector(-5.613, -3.77, 2.559)
SWEP.IronSightsAng = Vector(0, 0, 0)

SWEP.RestoreSound = Sound( "buttons/combine_button3.wav" )
SWEP.LastPrimaryAttack = 0

SWEP.Potatoes = {}

SWEP.ViewModelBoneMods = {
    ["ValveBiped.Grenade_body"] = { scale = Vector(0.009, 0.009, 0.009), pos = Vector(0, 0, 0), angle = Angle(0, 0, 0) }
}
SWEP.VElements = {
    ["pancake"] = { type = "Model", model = "models/pancake.mdl", bone = "ValveBiped.Grenade_body", rel = "", pos = Vector(0.455, 2.273, -1.364), angle = Angle(0, 97.158, 111.476), size = Vector(1.003, 1.003, 1.003), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
}
SWEP.WElements = {
    ["pancake"] = { type = "Model", model = "models/pancake.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(5, 5, -0.456), angle = Angle(1.023, -54.206, 91.023), size = Vector(1.003, 1.003, 1.003), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
}


function SWEP:CanPrimaryAttack()
	if self.Weapon:Clip1() <= 0 then
		self.Weapon:EmitSound( "Weapon_Pistol.Empty", 100, math.random(90,120) )
		return false
	end
	return true
end

function SWEP:throw_attack()

 if (SERVER) then
	
	self.Owner:SetAnimation( PLAYER_ATTACK1 )

        local tr = self.Owner:GetEyeTrace();
       -- if tr.Entity:IsWorld() then
        
		local Potato = ents.Create("sticky_syrup")
		Potato:SetPos(self.Owner:EyePos() + (self.Owner:GetAimVector() * 16))
		Potato:SetAngles( self.Owner:EyeAngles() )
		Potato:SetPhysicsAttacker(self.Owner)
		Potato:SetOwner(self.Owner)
                Potato.Welded = false
		Potato:Spawn()
		
		local phys = Potato:GetPhysicsObject(); 
		
		local PlayerPos = self.Owner:GetShootPos()

                local shot_length = tr.HitPos:Length();
        if self.Owner:KeyDown(IN_USE) then
            phys:ApplyForceCenter (self.Owner:GetAimVector():GetNormalized() * 500);

        else
            phys:ApplyForceCenter (self.Owner:GetAimVector():GetNormalized() *  math.pow(shot_length, 3));
        end
	--phys:ApplyForceOffset(VectorRand()*math.Rand(10000,30000),PlayerPos)

 	  table.insert(self.Potatoes, Potato)

	end

 end

function SWEP:PrimaryAttack()

        

	if self:CanPrimaryAttack() then
                 self.Weapon:EmitSound ( self.Primary.Sound, 400, 100  )
                   
		self.Weapon:TakePrimaryAmmo(1)
		self.Weapon:SendWeaponAnim( ACT_VM_PRIMARYATTACK )
		self.Owner:MuzzleFlash()
		self:throw_attack()
		--self.Weapon:Reload()
	else
		self.Weapon:EmitSound("Buttons.snd14")
	end
	self.Weapon:SetNextPrimaryFire(CurTime()+0.2)
end

SWEP.NextSecondaryAttack = 0

function SWEP:SecondaryAttack()

    if self.Owner:KeyDown(IN_USE) then
        self:Fly();
        return
    end

    if self.NextSecondaryAttack > CurTime() or self:Clip1() > 2 then return end
    self.NextSecondaryAttack = CurTime() + 2
    for k,v in pairs( self.Potatoes ) do
        if IsValid( v ) then
            v:Explode();
        end
    end

    self.Potatoes = {}
    self:SetClip1( 5 )
end

function SWEP:Reload()
    if self.Owner:KeyDown(IN_USE) then
        self:Jihad()
    else
        self:SwapPlayer()
    end
end



