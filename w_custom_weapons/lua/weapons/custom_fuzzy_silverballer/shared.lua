
// Variables that are used on both client and server
SWEP.Category				= "Doktor's SWEPs"
SWEP.Author					= "Generic Default, Doktor haus"
SWEP.Contact				= ""
SWEP.Purpose				= ""
SWEP.Instructions			= "Action: semi-automatic / Caliber: .45 ACP Custom / Magazine capacity: 7 rounds / Attachments: SureFire X300 LED WeaponLight"

SWEP.Weight				= 30		// Decides whether we should switch from/to this
SWEP.AutoSwitchTo			= true		// Auto switch to if we pick it up
SWEP.AutoSwitchFrom			= true		// Auto switch from if you pick up a better weapon

function SWEP:Initialize()
	self:SetHoldType("pistol")
end
 SWEP.Icon = "vgui/ttt_fun_killicons/golden_deagle.png"
function SWEP:OnRemove()
end
SWEP.PrintName				= "Fuzzy's SilverBaller"			// 'Nice' Weapon name (Shown on HUD)	
SWEP.Slot				= 3				// Slot in the weapon selection menu
SWEP.SlotPos				= 3				// Position in the slot
SWEP.DrawAmmo				= true				// Should draw the default HL2 ammo counter				// Should draw the default crosshair
SWEP.DrawWeaponInfoBox			= true				// Should draw the weapon info box
SWEP.BounceWeaponIcon   			= true				// Should the weapon icon bounce?

SWEP.Kind = WEAPON_GRENADE
 


SWEP.MuzzleAttachment		= "1" 	-- Should be "1" for CSS models or "muzzle" for hl2 models
SWEP.ShellEjectAttachment	= "2" 	-- Should be "2" for CSS models or "1" for hl2 models
--SWEP.DrawCrosshair			= true	

SWEP.ViewModelFOV			= 65
SWEP.ViewModelFlip			= false
SWEP.ViewModel				= "models/weapons/v_silverballer_lsx.mdl"
SWEP.WorldModel				= "models/weapons/w_silverballer_lsx.mdl"

SWEP.Base 					= "weapon_tttbase"
SWEP.Spawnable				= true
SWEP.AdminSpawnable			= true
 
SWEP.Primary.Sound			= Sound("gdc/silverballer/silverballer-1.mp3")

SWEP.Primary.RPM			= 100						// This is in Rounds Per Minute
SWEP.Primary.ClipSize			= 20						// Size of a clip
SWEP.Primary.DefaultClip		= 20
SWEP.Primary.ConeSpray			= 1.5					// Hip fire accuracy
SWEP.Primary.ConeIncrement		= 2.0					// Rate of innacuracy
SWEP.Primary.ConeMax			= 4.0					// Maximum Innacuracy
SWEP.Primary.ConeDecrement		= 0.1					// Rate of accuracy
SWEP.Primary.KickUp			= 1.4					// Maximum up recoil (rise)
SWEP.Primary.KickDown			= 0.3					// Maximum down recoil (skeet)
SWEP.Primary.KickHorizontal		= 0.3					// Maximum up recoil (stock)
SWEP.Primary.Automatic			= false					// Automatic/Semi Auto
SWEP.Primary.Ammo			= "pistol"
SWEP.Primary.Damage			= 40
SWEP.HeadshotMultiplier = 1.2

SWEP.Secondary.ClipSize			= 1						// Size of a clip
SWEP.Secondary.DefaultClip			= 1						// Default number of bullets in a clip
SWEP.Secondary.Automatic			= false						// Automatic/Semi Auto
SWEP.Secondary.Ammo			= ""
SWEP.Secondary.IronFOV			= 75						// How much you 'zoom' in. Less is more! 	

SWEP.data 				= {}						// The starting firemode
SWEP.data.ironsights			= 1
SWEP.AmmoEnt = "item_ammo_357_ttt"

SWEP.IronSightsPos = Vector(-2.84, 0, 1.799)
SWEP.IronSightsAng = Vector(0.275, 0, 0)
SWEP.SightsPos = Vector(-2.84, 0, 1.799)
SWEP.SightsAng = Vector(0.275, 0, 0)
SWEP.RunSightsPos = Vector (0.7565, 0, 3.0687)
SWEP.RunSightsAng = Vector (-20.6358, 2.9693, 0)
SWEP.WallSightsPos = Vector (0.2442, -11.6177, -3.9856)
SWEP.WallSightsAng = Vector (59.2164, 1.6346, -1.8014)
SWEP.GSightsPos = Vector (1.3838, -2.1202, -2.5469)
SWEP.GSightsAng = Vector (0, 0, 65.0667)

SWEP.ISPos = Vector (2.9453, 0, 1.3595)
SWEP.ISAng = Vector (0.5317, 0.2754, 0)
SWEP.RSPos = Vector (2.967, 0, 0.8302)
SWEP.RSAng = Vector (1.8624, 0.3413, 0)
SWEP.HSPos = Vector (2.8715, 0, 0.5942)
SWEP.HSAng = Vector (0, 0, 0)
SWEP.ASPos = Vector (3.0729, -3.3862, 0.9386)
SWEP.ASAng = Vector (0, 0, 0)


function SWEP:Initialize()

	util.PrecacheSound(self.Primary.Sound)
	self.Reloadaftershoot = 0 				-- Can't reload when firering
	if !self.Owner:IsNPC() then
		self:SetHoldType("pistol")                          	-- Hold type style ("pistol" "pistol" "shotgun" "rpg" "normal" "melee" "grenade" "smg")
	end
	if SERVER and self.Owner:IsNPC() then
		self:SetHoldType("pistol")                          	-- Hold type style ("pistol" "pistol" "shotgun" "rpg" "normal" "melee" "grenade" "smg")
		self:SetNPCMinBurst(3)			
		self:SetNPCMaxBurst(10)			// None of this really matters but you need it here anyway
		self:SetNPCFireRate(1)	
		self:SetCurrentWeaponProficiency( WEAPON_PROFICIENCY_VERY_GOOD )
	end
end

function SWEP:Deploy()

if game.SinglePlayer() then self.Single=true
else
self.Single=false
    self.Owner.LaserOn = true
end
	self:SetHoldType("pistol")                          	// Hold type styles; pistol pistol shotgun rpg normal melee grenade smg slam fist melee2 passive knife
	self:SetIronsights(false, self.Owner)					// Set the ironsight false
	self.Weapon:SendWeaponAnim( ACT_VM_DRAW )
	return true
	end


function SWEP:Precache()
	util.PrecacheSound(self.Primary.Sound)
	util.PrecacheSound("Buttons.snd14")
end


function SWEP:SecondaryAttack()
        self.LaserOn = !self.LaserOn 
	return false
end

function SWEP:Reload()
	
	self.Weapon:DefaultReload(ACT_VM_RELOAD) 
	if !self.Owner:IsNPC() then
	self.Idle = CurTime() + self.Owner:GetViewModel():SequenceDuration() end

	if ( self.Weapon:Clip1() < self.Primary.ClipSize ) and !self.Owner:IsNPC() then
	-- When the current clip < full clip and the rest of your ammo > 0, then

		self.Owner:SetFOV( 0, 0.3 )
		-- Zoom = 0

		self:SetIronsights(false)
		-- Set the ironsight to false

end


end

SWEP.Secondary.Sound = Sound("Default.Zoom")
-- Add some zoom to ironsights for this gun
function SWEP:SecondaryAttack()
    if not self.IronSightsPos then return end
    if self.Weapon:GetNextSecondaryFire() > CurTime() then return end

    bIronsights = not self:GetIronsights()

    self:SetIronsights( bIronsights )

    if SERVER then
        self:SetZoom(bIronsights)
    else
        self:EmitSound(self.Secondary.Sound)
    end

    self.Weapon:SetNextSecondaryFire( CurTime() + 0.3)
end
function SWEP:SetZoom(state)
    if CLIENT then
        return
    elseif IsValid(self.Owner) and self.Owner:IsPlayer() then
        if state then
            self.Owner:SetFOV(45, 0.3)
        else
            self.Owner:SetFOV(0, 0.2)
        end
    end
end
if CLIENT then


    function SWEP:AdjustMouseSensitivity()
        return (self:GetIronsights() and 0.2) or nil
    end
end

function SWEP:PrimaryAttack(worldsnd)
   self.Weapon:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
   self.Weapon:SetNextPrimaryFire( CurTime() + 0.18 )
   if not self:CanPrimaryAttack() then return end
   if not worldsnd then
      self.Weapon:EmitSound( self.Primary.Sound, self.Primary.SoundLevel )
   elseif SERVER then
      WorldSound(self.Primary.Sound, self:GetPos(), self.Primary.SoundLevel)
   end
   self:ShootBullet( self.Primary.Damage, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )
   
   if ( self.Owner:IsValid() ) then
		local tr = self.Owner:GetEyeTrace()
		local effectdata = EffectData()
		effectdata:SetOrigin(tr.HitPos)
		effectdata:SetNormal(tr.HitNormal)
		effectdata:SetScale(1)
		-- util.Effect("effect_mad_ignition", effectdata)
		util.Decal("FadingScorch", tr.HitPos + tr.HitNormal, tr.HitPos - tr.HitNormal)
		
				local tracedata = {}
				tracedata.start = tr.HitPos
				tracedata.endpos = Vector(tr.HitPos.x, tr.HitPos.y, tr.HitPos.z - 10)
				tracedata.filter = tr.HitPos
				local tracedata = util.TraceLine(tracedata)
				if SERVER then
				if tracedata.HitWorld then
					local flame = ents.Create("env_fire");
					flame:SetPos(tr.HitPos + Vector( 0, 0, 1 ));
					flame:SetKeyValue("firesize", "10");
					flame:SetKeyValue("fireattack", "10");
					flame:SetKeyValue("StartDisabled", "0");
					flame:SetKeyValue("health", "10");
					flame:SetKeyValue("firetype", "0");
					flame:SetKeyValue("damagescale", "5");
					flame:SetKeyValue("spawnflags", "128");
					flame:SetPhysicsAttacker(self.Owner)
					flame:SetOwner( self.Owner )
					flame:Spawn();
					flame:Fire("StartFire",0);
				end
				end
	   self:TakePrimaryAmmo( 1 )
	   local owner = self.Owner   
	   --if not ValidEntity(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end
	   
	   owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )
   end
end



hook.Add( "PostDrawViewModel", "PostDrawViewModel-LaserThingy", function( VMdl, oPlr, oWpn )
	local ply = oPlr or nil
	
            if IsValid( ply ) and IsValid(ply:GetActiveWeapon()) and ply:GetActiveWeapon():GetClass() == "custom_fuzzy_silverballer" then
		local vmo = VMdl
		local attachmentIndex = vmo:LookupAttachment("1")
		if attachmentIndex == 0 then attachmentIndex = vmo:LookupAttachment("muzzle") end
if not vmo:IsValid() or not vmo:GetAttachment(attachmentIndex) then return end

		local t = util.GetPlayerTrace(ply)
		local tr = util.TraceLine(t)
			cam.Start3D(EyePos(), EyeAngles())
		local LaserSprite = "cable/redlaser"
		--local LasersEndot = "sprites/light_glow02_add"
	--	--local LaserBeamLn = 64
		render.SetMaterial(Material(LaserSprite))
		render.DrawBeam(vmo:GetAttachment(attachmentIndex).Pos - Vector(0,0,1), tr.HitPos, 1, 0, 1, Color(0, 255, 255, 255)); 
		--render.SetMaterial(Material(LasersEndot))
		--local Size = math.random() * 1.35
		--render.DrawQuadEasy(tr.HitPos, (EyePos() - tr.HitPos):GetNormal(), Size, Size, Color(255, 0, 0, 255), 0); 
			cam.End3D()
	end
end )