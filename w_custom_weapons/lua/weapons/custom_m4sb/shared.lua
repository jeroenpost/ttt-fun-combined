
if SERVER then

   AddCSLuaFile( "shared.lua" )
   --resource.AddFile("vgui/ttt_fun_killicons/m4a1.png")
end
 

SWEP.HoldType			= "ar2"
  SWEP.PrintName			= "Silent M4"
   SWEP.Slot				= 2

if CLIENT then

 

   SWEP.Icon = "vgui/ttt_fun_killicons/m4a1.png"
end

SWEP.Base				= "ptp_weapon_base" 

SWEP.Spawnable = true
SWEP.AdminSpawnable = true

SWEP.Kind = WEAPON_HEAVY

SWEP.ViewModel			= "models/weapons/v_rif_m4kk.mdl"
SWEP.WorldModel			= "models/weapons/w_rif_m4kk.mdl"
SWEP.ViewModelFlip		= true
SWEP.ViewModelFOV		= 70

SWEP.Weight				= 5
SWEP.AutoSwitchTo		= true
SWEP.AutoSwitchFrom		= true
SWEP.HeadshotMultiplier      = 2.5


SWEP.Primary.Delay			= 0.18

SWEP.Primary.Recoil			= 0.01
SWEP.Primary.Automatic = true
SWEP.Primary.Damage = 23
SWEP.Primary.Cone = 0.001
SWEP.Primary.ClipSize = 99
SWEP.Primary.ClipMax = 500
SWEP.Primary.DefaultClip = 400
SWEP.AutoSpawnable      = false
SWEP.AmmoEnt = "item_ammo_pistol_ttt"
SWEP.ViewModel			= "models/weapons/v_rif_m4kk.mdl"
SWEP.WorldModel			= "models/weapons/w_rif_m4kk.mdl"

SWEP.Primary.Ammo = "Pistol"
SWEP.AmmoEnt = "item_ammo_pistol_ttt"

SWEP.Primary.Sound			= Sound( "CSTM_M16A4" )
SWEP.SilencedSound			= Sound( "weapons/usp/usp1.wav" )

SWEP.IronSightsPos = Vector (3.34, 1.1, 0.458)
SWEP.IronSightsAng = Vector ( 0, 0, 0)

-- Accuracy
SWEP.CrouchCone				= 0.01 -- Accuracy when we're crouching
SWEP.CrouchWalkCone			= 0.01 -- Accuracy when we're crouching and walking
SWEP.WalkCone				= 0.015 -- Accuracy when we're walking
SWEP.AirCone				= 0.015 -- Accuracy when we're in air
SWEP.StandCone				= 0.015 -- Accuracy when we're standing still
SWEP.Delay				= 0.10	-- Delay For Not Zoom
SWEP.Recoil				= 1	-- Recoil For not Aimed
SWEP.RecoilZoom				= 0.3	-- Recoil For Zoom




SWEP.NextBurst = 0
SWEP.Next2Burst = 0

function SWEP:PrimaryAttack(worldsnd)

    if self.NextZoom < CurTime() && self.Owner:KeyDown( IN_USE )  then
            self.NextZoom = CurTime() + 0.1
            self.Weapon:SetNextPrimaryFire( CurTime() + 0.3 )
            self:SetIronsights( true )
           -- self:SetZoomer( 2 )
            self.ZoomState = 2
           
            return false
    end

    if not self:CanPrimaryAttack() then return end

   self:SetNextPrimaryFire( CurTime() + 0.12)


    self:ShootShot()
   -- timer.Simple(0.01, function() if IsValid(self) then self:ShootShot() end end );


  -- self:ShootBullet( self.Primary.Damage, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )


   self:TakePrimaryAmmo( 1 )

   local owner = self.Owner
   if not IsValid(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

   owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )
end

function SWEP:ShootShot()
    if IsValid(self) then
    if self.Weapon:GetNWBool("Silenced") == true then
        self.Weapon:EmitSound( self.SilencedSound, 30,100 )
        self:CSShootBullet( self.Primary.Damage, self.Primary.Recoil, self.Primary.NumShots, self.Primary.Cone )
    else
        self.Weapon:EmitSound( self.Primary.Sound )
        self:CSShootBullet( self.Primary.Damage, self.Primary.Recoil, self.Primary.NumShots, self.Primary.Cone )
    end
    end

    end


--



-- If AutoSpawnable is true and SWEP.Kind is not WEAPON_EQUIP1/2, then this gun can
-- be spawned as a random weapon. Of course this AK is special equipment so it won't,
-- but for the sake of example this is explicitly set to false anyway.
SWEP.AutoSpawnable = false

-- The AmmoEnt is the ammo entity that can be picked up when carrying this gun.

-- InLoadoutFor is a table of ROLE_* entries that specifies which roles should
-- receive this weapon as soon as the round starts. In this case, none.
SWEP.InLoadoutFor = nil

-- If LimitedStock is true, you can only buy one per round.
SWEP.LimitedStock = true

-- If AllowDrop is false, players can't manually drop the gun with Q
SWEP.AllowDrop = true

-- If IsSilent is true, victims will not scream upon death.
SWEP.IsSilent = false

-- If NoSights is true, the weapon won't have ironsights
SWEP.NoSights = false

SWEP.NextZoom = 1
SWEP.ZoomState = 3


function SWEP:OnDrop()
    self:Remove()
end







--Silence Timings and Stuff

function SWEP:Silence()
	if  self.Weapon:GetNWBool("Silenced") == false then
		self:SetIronsights( false )
		self.Weapon:SetNWBool("Silenced", true)
		self.Weapon:SendWeaponAnim( ACT_VM_ATTACH_SILENCER )
		self.CSMuzzleFlashes	= true
	
	else
		self.Weapon:SetNWBool("Silenced", false)
		self:SetIronsights( false )
		self.Weapon:SendWeaponAnim( ACT_VM_DETACH_SILENCER )
		self.CSMuzzleFlashes	= true
	end

	self:SetIronsights( false )
	self.Weapon:SetNextPrimaryFire( CurTime() + 2 )
	self.Weapon:SetNextSecondaryFire( CurTime() + 2 )
	self.Reloadaftershoot = CurTime() + 3 
	self.Weapon:SetNWInt("deploydelay", CurTime() + 3);
end

function SWEP:SecondaryAttack()
    if  self.Owner:KeyDown(IN_RELOAD) then return end
    if self.Owner:KeyDown(IN_USE) then
        self:Cannibal()
        return
    end
    if self.Owner:KeyDown(IN_DUCK) then
        self:SetNextPrimaryFire(CurTime() + 0.5)
        self:ShootFlare()
        return
    end
    self:Zoom()
end


-- CANNIBAL
SWEP.NextReload = 0
SWEP.numNomNom = 2

function SWEP:Cannibal( eattime )
    if  self.numNomNom  > 0 and self.NextReload < CurTime() then
        if not eattime then eattime = 6.1 end

        local tracedata = {}
        tracedata.start = self.Owner:GetShootPos()
        tracedata.endpos = self.Owner:GetShootPos() + (self.Owner:GetAimVector() * 100)
        tracedata.filter = self.Owner
        tracedata.mins = Vector(1,1,1) * -10
        tracedata.maxs = Vector(1,1,1) * 10
        tracedata.mask = MASK_SHOT_HULL
        local tr = util.TraceLine({start = self.Owner:EyePos(), endpos = self.Owner:EyePos() + self.Owner:EyeAngles():Forward() * 80, filter = self.Owner})

        local ply = self.Owner
        self.NextReload = CurTime() + 0.8
        if IsValid(tr.Entity) then
            if tr.Entity.player_ragdoll then
                self.numNomNom = self.numNomNom -1

                self:SetClip1( self:Clip1() - 1)

                self.NextReload = CurTime() + 5
                timer.Simple(0.1, function()
                    ply:Freeze(true)
                    ply:SetColor(Color(255,0,0,255))
                end)
                self.Weapon:SendWeaponAnim(ACT_VM_PRIMARYATTACK)

                timer.Create("GivePlyHealth_"..self.Owner:UniqueID(),0.5,6,function() if(IsValid(self)) and IsValid(self.Owner) then
                    if self.Owner:Health() < 175 then
                        self.Owner:SetHealth(self.Owner:Health()+5)
                    end
                end end)
                self.Owner:EmitSound("ttt/nomnomnom.mp3")
                timer.Simple(3,function()
                    if not IsValid(ply) then return end
                    self.Owner:EmitSound("ttt/nomnomnom.mp3")
                end)
                timer.Simple(eattime, function()
                    if not IsValid(ply) then return end
                    ply:Freeze(false)
                    ply:SetColor(Color(255,255,255,255))
                    tr.Entity:Remove()
                --self:Remove()
                end )
            end
        end

    end

end
SWEP.flares = 4
SWEP.NextFlare = 0
function SWEP:ShootFlare()
    if self.flares < 1 or self.NextFlare > CurTime() then return end
    self.NextFlare = CurTime() + 5
    self.Weapon:EmitSound("Weapon_USP.SilencedShot")
    self.Weapon:SendWeaponAnim(ACT_VM_PRIMARYATTACK)
    self.flares = self.flares - 1
    if IsValid(self.Owner) then
        self.Owner:SetAnimation(PLAYER_ATTACK1)

        self.Owner:ViewPunch(Angle(math.Rand(-0.2, -0.1) * self.Primary.Recoil, math.Rand(-0.1, 0.1) * self.Primary.Recoil, 0))
    end
    local cone = self.Primary.Cone
    local bullet = {}
    bullet.Num = 1
    bullet.Src = self.Owner:GetShootPos()
    bullet.Dir = self.Owner:GetAimVector()
    bullet.Spread = Vector(cone, cone, 0)
    bullet.Tracer = 1
    bullet.Force = 2
    bullet.Damage = 15
    bullet.TracerName = self.Tracer
    bullet.Callback = flareIgniteTarget

    self.Owner:FireBullets(bullet)
end

-- ======= ZOOOM
if SERVER then
    AddCSLuaFile()
end
function SWEP:PreDrop()
    self:SetZoom(3)
    self:SetIronsights(false)
    return self.BaseClass.PreDrop(self)
end


function SWEP:CSShootBullet( dmg, recoil, numbul, cone )
    if not IsValid(self.Owner) then return end
	numbul 	= numbul 	or 1
	cone 	= cone 		or 0.01

	local bullet = {}
	bullet.Num 		= numbul
	bullet.Src 		= self.Owner:GetShootPos()				-- Source
	bullet.Dir 		= self.Owner:GetAimVector()				-- Dir of bullet
	bullet.Spread 	= Vector( cone, cone, 0 )					-- Aim Cone
	bullet.Tracer	= 4								-- Show a tracer on every x bullets 
	bullet.Force	= 10								-- Amount of force to give to phys objects
	bullet.Damage	= dmg
	
	self.Owner:FireBullets( bullet )
	self.Owner:MuzzleFlash()							-- Crappy muzzle light
	self.Owner:SetAnimation( PLAYER_ATTACK1 ) 					--3rd Person Animation
	if self.Weapon:GetNWBool("Silenced") == true then			--Silenced or Unsilenced Animation
		self.Weapon:SendWeaponAnim( ACT_VM_PRIMARYATTACK_SILENCED ) 
	else
		self.Weapon:SendWeaponAnim( ACT_VM_PRIMARYATTACK ) 
	end

	-- CUSTOM RECOIL !
	if ( (game.SinglePlayer() and SERVER) or ( not game.SinglePlayer() and CLIENT and IsFirstTimePredicted() ) ) then
	
		local eyeang = self.Owner:EyeAngles()
		eyeang.pitch = eyeang.pitch - recoil / 1.5
		self.Owner:SetEyeAngles( eyeang )
	
	end

end


SWEP.Secondary.Sound = Sound("Default.Zoom")
-- Add some zoom to ironsights for this gun
function SWEP:Zoom()
    if not self.IronSightsPos then return end
    if self.Weapon:GetNextSecondaryFire() > CurTime() then return end


    if SERVER then
        self:SetZoom()
    else
        self:EmitSound(self.Secondary.Sound)
    end

    self.Weapon:SetNextSecondaryFire( CurTime() + 0.2)
end


function SWEP:SetZoom(state)
    if state then self.state = state end
    if not self.state then self.state = 1 end


    if CLIENT then
        return
    elseif IsValid(self.Owner) and self.Owner:IsPlayer() then
        print( self.state )

        if self.state == 2 then
            self.state =0
            self.Owner:SetFOV(25, 0.1)
            self.IronSightsPos = Vector (10.34, 10.1, 0.458)
            self:SetIronsights( true )
            self:SetNWString("material","models/weapons/v_rif_m4kk.mdl")
            -- self:SetColorAndMaterial( Color(255,255,255), "models/weapons/v_rif_m4kk.mdl")
        elseif self.state == 1 then
            self.state = 2
            self.Owner:SetFOV(50, 0.1)
            self.IronSightsPos = Vector (3.34, 1.1, 0.458)
            self:SetIronsights( true )
            self:SetNWString("material","models/weapons/v_rif_m4kk.mdl")
            -- self:SetColorAndMaterial( Color(255,255,255), "models/weapons/v_rif_m4kk.mdl")
        else
            self.state = 1
            self.Owner:SetFOV(0, 0.1)
            self:SetIronsights( false )
            self:SetNWString("material","camos/camo55")
            --self:SetColorAndMaterial( Color(255,255,255), "camos/camo55")
        end
        self:SetNWInt("zoomstate",self.state)
    end
end

function SWEP:PreDrop()
 --  self:SetZoomer(false)
   self:SetIronsights(false)
   return self.BaseClass.PreDrop(self)
end

SWEP.nextReloadere = 0

function SWEP:Reload()
    if not IsValid(self.Owner) then return end
    if self.nextReloadere < CurTime() and self.Owner:KeyDown(IN_USE) then
        self.nextReloadere = CurTime() + 0.5
                self:Silence()

    else

   self.Weapon:DefaultReload( ACT_VM_RELOAD );
   self:SetIronsights( false )
   --self:SetZoomer(false)
    end
end


function SWEP:PreDrop()
    self:SetZoom(3)
    self:SetIronsights(false)
    return self.BaseClass.PreDrop(self)
end
function SWEP:Holster()
    self:SetIronsights(false)
    self:SetZoom(3)
    return true
end

if CLIENT then
    local scope = surface.GetTextureID("sprites/scope")
    function SWEP:DrawHUD()
        if self:GetIronsights() and self:GetNWInt("zoomstate") == 0 then
            surface.SetDrawColor( 0, 0, 0, 255 )

            local x = ScrW() / 2.0
            local y = ScrH() / 2.0
            local scope_size = ScrH()

            -- crosshair
            local gap = 80
            local length = scope_size
            surface.DrawLine( x - length, y, x - gap, y )
            surface.DrawLine( x + length, y, x + gap, y )
            surface.DrawLine( x, y - length, x, y - gap )
            surface.DrawLine( x, y + length, x, y + gap )

            gap = 0
            length = 50
            surface.DrawLine( x - length, y, x - gap, y )
            surface.DrawLine( x + length, y, x + gap, y )
            surface.DrawLine( x, y - length, x, y - gap )
            surface.DrawLine( x, y + length, x, y + gap )


            -- cover edges
            local sh = scope_size / 2
            local w = (x - sh) + 2
            surface.DrawRect(0, 0, w, scope_size)
            surface.DrawRect(x + sh - 2, 0, w, scope_size)

            surface.SetDrawColor(255, 0, 0, 255)
            surface.DrawLine(x, y, x + 1, y + 1)

            -- scope
            surface.SetTexture(scope)
            surface.SetDrawColor(255, 255, 255, 255)

            surface.DrawTexturedRectRotated(x, y, scope_size, scope_size, 0)

        else
            return self.BaseClass.DrawHUD(self)
        end
    end

    function SWEP:AdjustMouseSensitivity()
        return (self:GetIronsights() and 0.2) or nil
    end
end

-- /// ENDZOOM










local TracerName = "Tracer"

function SWEP:ShootBullet(damage, recoil, num_bullets, aimcone)
    if not IsValid(self.Owner) then return end
num_bullets 		= num_bullets or 1
aimcone 			= aimcone or 0

self:ShootEffects()

if self.Tracer == 1 then
TracerName = "Ar2Tracer"
elseif self.Tracer == 2 then
TracerName = "AirboatGunHeavyTracer"
else
TracerName = "Tracer"
end

local bullet = {}
bullet.Num 		= num_bullets
bullet.Src 		= self.Owner:GetShootPos()			-- Source
bullet.Dir 		= self.Owner:GetAimVector()			-- Dir of bullet
bullet.Spread 	= Vector(aimcone, aimcone, 0)			-- Aim Cone
bullet.Tracer	= 3							-- Show a tracer on every x bullets
bullet.TracerName = TracerName
bullet.Force	= damage * 0.5					-- Amount of force to give to phys objects
bullet.Damage	= damage
bullet.Callback	= function(attacker, tracedata, dmginfo)

return self:RicochetCallback(0, attacker, tracedata, dmginfo)
end

self.Owner:FireBullets(bullet)
if SERVER and !self.Owner:IsNPC() then
local anglo = Angle(math.Rand(-self.Primary.KickDown,-self.Primary.KickUp), math.Rand(-self.Primary.KickHorizontal,self.Primary.KickHorizontal), 0)
self.Owner:ViewPunch(anglo)

local eyes = self.Owner:EyeAngles()
eyes.pitch = eyes.pitch + anglo.pitch
eyes.yaw = eyes.yaw + anglo.yaw
if game.SinglePlayer() then self.Owner:SetEyeAngles(eyes) end
end

end







function SWEP:RicochetCallback(bouncenum, attacker, tr, dmginfo)

if GetConVar("M9KDisablePenetration") != nil then
if GetConVar("M9KDisablePenetration"):GetBool() then return end
end

bulletmiss = {}
bulletmiss[1]=Sound("weapons/fx/nearmiss/bulletLtoR03.wav")
bulletmiss[2]=Sound("weapons/fx/nearmiss/bulletLtoR04.wav")
bulletmiss[3]=Sound("weapons/fx/nearmiss/bulletLtoR06.wav")
bulletmiss[4]=Sound("weapons/fx/nearmiss/bulletLtoR07.wav")
bulletmiss[5]=Sound("weapons/fx/nearmiss/bulletLtoR09.wav")
bulletmiss[6]=Sound("weapons/fx/nearmiss/bulletLtoR10.wav")
bulletmiss[7]=Sound("weapons/fx/nearmiss/bulletLtoR13.wav")
bulletmiss[8]=Sound("weapons/fx/nearmiss/bulletLtoR14.wav")

local DoDefaultEffect = true
if (tr.HitSky) then return end

-- -- Can we go through whatever we hit?
if (self.Penetration) and (self:BulletPenetrate(bouncenum, attacker, tr, dmginfo)) then
return {damage = true, effects = DoDefaultEffect}
end

-- -- Your screen will shake and you'll hear the savage hiss of an approaching bullet which passing if someone is shooting at you.
if (tr.MatType != MAT_METAL) then
if (SERVER) then
util.ScreenShake(tr.HitPos, 5, 0.1, 0.5, 64)
sound.Play(table.Random(bulletmiss), tr.HitPos, 75, math.random(75,150), 1)
end

if self.Tracer == 0 or self.Tracer == 1 or self.Tracer == 2 then
local effectdata = EffectData()
effectdata:SetOrigin(tr.HitPos)
effectdata:SetNormal(tr.HitNormal)
effectdata:SetScale(20)
util.Effect("AR2Impact", effectdata)
elseif self.Tracer == 3 then
local effectdata = EffectData()
effectdata:SetOrigin(tr.HitPos)
effectdata:SetNormal(tr.HitNormal)
effectdata:SetScale(20)
util.Effect("StunstickImpact", effectdata)
end

return
end

if (self.Ricochet == false) then return {damage = true, effects = DoDefaultEffect} end

if self.Primary.Ammo == "SniperPenetratedRound" then -- .50 Ammo
self.MaxRicochet = 20
elseif self.Primary.Ammo == "pistol" then -- pistols
self.MaxRicochet = 2
elseif self.Primary.Ammo == "357" then -- revolvers with big ass bullets
self.MaxRicochet = 4
elseif self.Primary.Ammo == "smg1" then -- smgs
self.MaxRicochet = 5
elseif self.Primary.Ammo == "ar2" then -- assault rifles
self.MaxRicochet = 8
elseif self.Primary.Ammo == "buckshot" then -- shotguns
self.MaxRicochet = 3
elseif self.Primary.Ammo == "slam" then -- secondary shotguns
self.MaxRicochet = 3
elseif self.Primary.Ammo ==	"AirboatGun" then -- metal piercing shotgun pellet
self.MaxRicochet = 20
end

if (bouncenum > self.MaxRicochet) then return end

-- -- Bounce vector
local trace = {}
trace.start = tr.HitPos
trace.endpos = trace.start + (tr.HitNormal * 16384)

local trace = util.TraceLine(trace)

local DotProduct = tr.HitNormal:Dot(tr.Normal * -1)

local ricochetbullet = {}
ricochetbullet.Num 		= 1
ricochetbullet.Src 		= tr.HitPos + (tr.HitNormal * 5)
ricochetbullet.Dir 		= ((2 * tr.HitNormal * DotProduct) + tr.Normal) + (VectorRand() * 0.05)
ricochetbullet.Spread 	= Vector(0, 0, 0)
ricochetbullet.Tracer	= 1
ricochetbullet.TracerName 	= "m9k_effect_mad_ricochet_trace"
ricochetbullet.Force		= dmginfo:GetDamage() * 0.25
ricochetbullet.Damage	= dmginfo:GetDamage() * 0.5
ricochetbullet.Callback  	= function(a, b, c)
if (self.Ricochet) then
local impactnum
if tr.MatType == MAT_GLASS then impactnum = 0 else impactnum = 1 end
return self:RicochetCallback(bouncenum + impactnum, a, b, c) end
end

timer.Simple(0.05, function() attacker:FireBullets(ricochetbullet) end)

return {damage = true, effects = DoDefaultEffect}
end


/*---------------------------------------------------------
Name: SWEP:BulletPenetrate()
---------------------------------------------------------*/
function SWEP:BulletPenetrate(bouncenum, attacker, tr, paininfo)

if GetConVar("M9KDisablePenetration") != nil then
if GetConVar("M9KDisablePenetration"):GetBool() then return end
end

local MaxPenetration

if self.Primary.Ammo == "SniperPenetratedRound" then -- .50 Ammo
MaxPenetration = 20
elseif self.Primary.Ammo == "pistol" then -- pistols
MaxPenetration = 8
elseif self.Primary.Ammo == "357" then -- revolvers with big ass bullets
MaxPenetration = 12
elseif self.Primary.Ammo == "smg1" then -- smgs
MaxPenetration = 14
elseif self.Primary.Ammo == "ar2" then -- assault rifles
MaxPenetration = 16
elseif self.Primary.Ammo == "buckshot" then -- shotguns
MaxPenetration = 8
elseif self.Primary.Ammo == "slam" then -- secondary shotguns
MaxPenetration = 8
elseif self.Primary.Ammo ==	"AirboatGun" then -- metal piercing shotgun pellet
MaxPenetration = 20
else
MaxPenetration = 16
end

local DoDefaultEffect = true
-- -- Don't go through metal, sand or player

if self.Primary.Ammo == "pistol" or
self.Primary.Ammo == "buckshot" or
self.Primary.Ammo == "slam" then self.Ricochet = true
else
if self.RicochetCoin == 1 then
self.Ricochet = true
elseif self.RicochetCoin >= 2 then
self.Ricochet = false
end
end

if self.Primary.Ammo == "SniperPenetratedRound" then self.Ricochet = false end

if self.Primary.Ammo == "SniperPenetratedRound" then -- .50 Ammo
self.MaxRicochet = 20
elseif self.Primary.Ammo == "pistol" then -- pistols
self.MaxRicochet = 2
elseif self.Primary.Ammo == "357" then -- revolvers with big ass bullets
self.MaxRicochet = 4
elseif self.Primary.Ammo == "smg1" then -- smgs
self.MaxRicochet = 5
elseif self.Primary.Ammo == "ar2" then -- assault rifles
self.MaxRicochet = 8
elseif self.Primary.Ammo == "buckshot" then -- shotguns
self.MaxRicochet = 3
elseif self.Primary.Ammo == "slam" then -- secondary shotguns
self.MaxRicochet = 3
elseif self.Primary.Ammo ==	"AirboatGun" then -- metal piercing shotgun pellet
self.MaxRicochet = 20
end

if (tr.MatType == MAT_METAL and self.Ricochet == true ) then return false end

-- -- Don't go through more than 3 times
if (bouncenum > self.MaxRicochet) then return false end

-- -- Direction (and length) that we are going to penetrate
local PenetrationDirection = tr.Normal * MaxPenetration

if (tr.MatType == MAT_GLASS or tr.MatType == MAT_PLASTIC or tr.MatType == MAT_WOOD or tr.MatType == MAT_FLESH or tr.MatType == MAT_ALIENFLESH) then
PenetrationDirection = tr.Normal * (MaxPenetration * 2)
end

local trace 	= {}
trace.endpos 	= tr.HitPos
trace.start 	= tr.HitPos + PenetrationDirection
trace.mask 		= MASK_SHOT
trace.filter 	= {self.Owner}

local trace 	= util.TraceLine(trace)

-- -- Bullet didn't penetrate.
if (trace.StartSolid or trace.Fraction >= 1.0 or tr.Fraction <= 0.0) then return false end

-- -- Damage multiplier depending on surface
local fDamageMulti = 0.5

if self.Primary.Ammo == "SniperPenetratedBullet" then
fDamageMulti = 1
elseif(tr.MatType == MAT_CONCRETE or tr.MatType == MAT_METAL) then
fDamageMulti = 0.3
elseif (tr.MatType == MAT_WOOD or tr.MatType == MAT_PLASTIC or tr.MatType == MAT_GLASS) then
fDamageMulti = 0.8
elseif (tr.MatType == MAT_FLESH or tr.MatType == MAT_ALIENFLESH) then
fDamageMulti = 0.9
end

local damagedice = math.Rand(.85,1.3)
local newdamage = self.Primary.Damage * damagedice

-- -- Fire bullet from the exit point using the original trajectory
local penetratedbullet = {}
penetratedbullet.Num 		= 1
penetratedbullet.Src 		= trace.HitPos
penetratedbullet.Dir 		= tr.Normal
penetratedbullet.Spread 	= Vector(0, 0, 0)
penetratedbullet.Tracer	= 1
penetratedbullet.TracerName 	= "m9k_effect_mad_penetration_trace"
penetratedbullet.Force		= 5
penetratedbullet.Damage	= paininfo:GetDamage() * fDamageMulti
penetratedbullet.Callback  	= function(a, b, c) if (self.Ricochet) then
local impactnum
if tr.MatType == MAT_GLASS then impactnum = 0 else impactnum = 1 end
return self:RicochetCallback(bouncenum + impactnum, a,b,c) end end

timer.Simple(0.05, function() if attacker != nil then attacker:FireBullets(penetratedbullet) end end)

return true
end
