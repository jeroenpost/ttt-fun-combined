
// Variables that are used on both client and server

SWEP.PrintName = "Faded Shocker";
   SWEP.Slot = 2;
   SWEP.SlotPos = 1;
if (CLIENT) then
 
   
   SWEP.DrawAmmo = true;
   SWEP.DrawCrosshair = false;
   SWEP.Category = "Babel Industries"
   SWEP.WepSelectIcon        = surface.GetTextureID("vgui/entities/weapon_rivet")
   SWEP.BounceWeaponIcon = false
end
SWEP.VElements = {
	["hud2"] = { type = "Quad", bone = "v_weapon.p90_Parent", rel = "", pos = Vector(-0.050, 5.717, 1.075), angle = Angle(0, 0, 129.781), size = 0.019, draw_func = nil},
	["hud2+"] = { type = "Quad", bone = "v_weapon.p90_Parent", rel = "", pos = Vector(-0.051, 5.673, 0.412), angle = Angle(0, 0, 129.781), size = 0.019, draw_func = nil}
}

SWEP.WElements = {
	["element_name"] = { type = "Model", model = "models/weapons/w_smg_r90.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(-4.613, 1.085, 3.723), angle = Angle(-12.622, -0.647, -176.933), size = Vector(1,1,1), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
}
 
 
SWEP.RunArmOffset 		= Vector (0, -4.919, -2.623)
SWEP.RunArmAngle 			= Vector (33.852, 0, 0)

SWEP.IronSightsPos = Vector(-0.201, -5.082, -1.981)
SWEP.IronSightsAng = Vector(16.065, -36.722, 0)
 
SWEP.Author = "Chris (Model by Cbast)";
SWEP.Contact = "Babel Industries/Scarlet-I Labs";
SWEP.Purpose = "";
SWEP.Instructions = "Standard-Issue rifle Of the future. It's light enough to be held with one hand, and comes with a holographic sight and ammo counter.";
SWEP.IconLetter = "m"

SWEP.Kind = 23
SWEP.Base 				= "aa_base"

SWEP.HoldType = "ar2"
SWEP.ViewModelFOV = 65
SWEP.ViewModelFlip = true
SWEP.ViewModel = "models/weapons/v_smg_r90.mdl"
SWEP.WorldModel = "models/weapons/w_pistol.mdl"
SWEP.ShowViewModel = true
SWEP.ShowWorldModel = false
SWEP.ViewModelBoneMods = {}


SWEP.Tracer				= 3

function SWEP:Reload()
	if not( ( self.Weapon:Clip1() ) < ( self.Primary.ClipSize ) ) then return end
	self.Weapon:DefaultReload( ACT_VM_RELOAD )
	self:EmitSound( "weapons/lyl/deploy.wav" )
	
end
function SWEP:Deploy()
    self.Weapon:SendWeaponAnim( ACT_VM_DRAW )

	self.Owner:EmitSound( "weapons/Rivet/rivet_boltpull.wav" ) ;
	
	return true;
end


SWEP.IronSightsPos = Vector(4.519, -5, 0.588)
SWEP.IronSightsAng = Vector(0, 0, 0)


SWEP.Spawnable			= true
SWEP.AdminSpawnable		= false

SWEP.Primary.Sound 		= Sound("weapons/warden/warden1.mp3")
SWEP.Primary.Recoil		= 1
SWEP.Primary.Damage		= 45
SWEP.Primary.NumShots   = 1
SWEP.Primary.Cone	    = 0.030
SWEP.Primary.Delay 		= .5
SWEP.Primary.Force      = 9500;

SWEP.Primary.ClipSize		= 10					// Size of a clip
SWEP.Primary.DefaultClip	= 10					// Default number of bullets in a clip
SWEP.Primary.Automatic		= true				// Automatic/Semi Auto
SWEP.Primary.Ammo			= "smg1"

SWEP.Secondary.ClipSize		= -1					// Size of a clip
SWEP.Secondary.DefaultClip	= -1					// Default number of bullets in a clip
SWEP.Secondary.Automatic	= false				// Automatic/Semi Auto
SWEP.Secondary.Ammo		= "none"

function SWEP:OnDrop()
    self:Remove()
end

SWEP.AmmoEnt = "item_ammo_pistol_ttt"

SWEP.Burst				= true
SWEP.BurstShots			= 5
SWEP.BurstDelay			= 0.001
SWEP.BurstCounter			= 1
SWEP.BurstTimer			= 0.7
SWEP.SwitchBurstMsg		= "Riot Control Mode activated. 12 shots."
SWEP.SwitchSingleMsg	= "Fully Automatic Mode Activated. 60 shots."




SWEP.StunTime = 5


SWEP.NextReload = 0

function SWEP:Reload()
    if self.NextReload < CurTime() && IsValid(self.Owner)then
        self.NextReload = CurTime() + 40
        if SERVER then
            self.Owner:PS_DropWeaponsSameSlot("ttt_turret")
        self.Owner:Give("ttt_turret")
        end
    end
end

SWEP.NextSecondary = 0

function SWEP:SecondaryAttack()
if (!SERVER) or self.NextSecondary > CurTime() then return end

local bullet = {}
bullet.Num    = 1
bullet.Src    = self.Owner:GetShootPos()
bullet.Dir    = self.Owner:GetAimVector()
bullet.Spread = Vector( 0.01, 0.01, 0 )
bullet.Tracer = 1
bullet.TracerName =  "Tracer"
bullet.Force  = 10
bullet.Damage = 50


self.Owner:FireBullets( bullet )

    self.NextSecondary = CurTime() + 2.5

self.Weapon:SendWeaponAnim( ACT_VM_MISSCENTER )
self.Weapon:EmitSound("weapons/iceaxe/iceaxe_swing1.wav")

	local ent = ents.Create("sopopochisp")
	local pos = self.Owner:GetShootPos()
	ent:SetPos(pos)
	ent:SetAngles(self.Owner:EyeAngles())
	--ent:SetPhysicsAttacker( self.Owner )
	--ent.owner = self
	ent:Spawn()
	ent:Activate()
	--ent:SetOwner(self.Owner)
	--ent:SetPhysicsAttacker(self.Owner)

self.Weapon:SetNextSecondaryFire(CurTime() + 2)
self.Weapon:SetNextPrimaryFire(CurTime() + 1)

	local tr = self.Owner:GetEyeTrace()
	
	local phys = ent:GetPhysicsObject()
	if (phys:IsValid()) then
		phys:Wake()
		phys:SetMass(1)
		phys:EnableGravity( true )
		phys:ApplyForceCenter( self:GetForward() * 4000 )
		phys:SetBuoyancyRatio( 0 )
	end
 
	local shot_length = tr.HitPos:Length()
	ent:SetVelocity(self.Owner:GetAimVector():GetNormalized() *  math.pow(shot_length, 3))

	
end

SWEP.LaserColor = Color(0,0,200,255)

function SWEP:ShootBullet( dmg, recoil, numbul, cone )


     self.Weapon:SendWeaponAnim(self.PrimaryAnim)

   self.Owner:MuzzleFlash()
   self.Owner:SetAnimation( PLAYER_ATTACK1 )

   if not IsFirstTimePredicted() then return end

   local sights = self:GetIronsights()

   numbul = numbul or 1
   cone   = cone   or 0.01

   local bullet = {}
   bullet.Num    = numbul
   bullet.Src    = self.Owner:GetShootPos()
   bullet.Dir    = self.Owner:GetAimVector()
   bullet.Spread = Vector( cone, cone, 0 )
   bullet.Tracer = 1
   bullet.TracerName =  "LaserTracer_thick"
   bullet.Force  = 10
   bullet.Damage = dmg


   self.Owner:FireBullets( bullet )

   -- Owner can die after firebullets
   if (not IsValid(self.Owner)) or (not self.Owner:Alive()) or self.Owner:IsNPC() then return end

   if ((game.SinglePlayer() and SERVER) or
       ((not game.SinglePlayer()) and CLIENT and IsFirstTimePredicted())) then

      -- reduce recoil if ironsighting
      recoil = sights and (recoil * 0.6) or recoil

      local eyeang = self.Owner:EyeAngles()
      eyeang.pitch = eyeang.pitch - recoil
      self.Owner:SetEyeAngles( eyeang )
   end
 
    attacker = self.Owner
    local tracedata = {}

    tracedata.start = self.Owner:GetShootPos()
    tracedata.endpos = self.Owner:GetShootPos() + ( self.Owner:GetAimVector() * 450 )
    tracedata.filter = self.Owner
    tracedata.mins = Vector( -3,-3,-3 )
    tracedata.maxs = Vector( 3,3,3 )
    tracedata.mask = MASK_SHOT_HULL
    local trace = util.TraceHull( tracedata )

    victim = trace.Entity

     timer.Simple(15, function()
                    if IsValid(self) and IsValid( self.Owner) and self:Clip1() < 10 then
                        self:SetClip1( self:Clip1() + 1)
                    end
                end)
     
    if ( not IsValid(attacker) or !SERVER  or not victim:IsPlayer() or victim:IsWorld() ) then return end
    if  !victim:Alive() or victim.IsDog or specialRound.isSpecialRound or _globals['specialperson'][victim:SteamID()] or (victim.NoTaze && self.Owner:GetRole() != ROLE_DETECTIVE)    then
        self.Owner:PrintMessage( HUD_PRINTCENTER, "Person cannot be tazed" )
            
            return end			
        

    if victim:GetPos():Distance( attacker:GetPos()) > 400 then return end
                        
               StunTime = self.StunTime
 
               local edata = EffectData()
                edata:SetEntity(victim)
                edata:SetMagnitude(3)
                edata:SetScale(3)

                util.Effect("TeslaHitBoxes", edata)

                    self:Taze( victim)
                
                  
               
               
                       


end




function SWEP:Taze( victim )

     if not victim:Alive() or victim:IsGhost() then return end

        if victim:InVehicle() then
                local vehicle = victim:GetParent()
                victim:ExitVehicle()
        end

        ULib.getSpawnInfo( victim )

     if SERVER then DamageLog("Tazer: " .. self.Owner:Nick() .. " [" .. self.Owner:GetRoleString() .. "] tazed "..victim:Nick().." [" .. victim:GetRoleString() .. "] ")
    end
            local rag = ents.Create("prop_ragdoll")
                if not (rag:IsValid()) then return end
                
               
                rag.BelongsTo = victim
                rag.Weapons = {}
                for _, v in pairs (victim:GetWeapons()) do
                        table.insert(rag.Weapons, v:GetClass())
                end

                rag.Credits = victim:GetCredits()
                rag.OldHealth = victim:Health()
                rag:SetModel(victim:GetModel())
                  rag:SetPos( victim:GetPos() )
                 local velocity = victim:GetVelocity()
        rag:SetAngles( victim:GetAngles() )
        rag:SetModel( victim:GetModel() )
        rag:Spawn()
        rag:Activate()
        victim:SetParent( rag )
                 victim:SetParent( rag )
                victim.Ragdolled = rag
                rag.orgPos = victim:GetPos() 
                rag.orgPos.z = rag.orgPos.z + 10  
                
                victim.orgPos = victim:GetPos()
                victim:StripWeapons()
                victim:Spectate(OBS_MODE_CHASE)
                victim:SpectateEntity(rag) 

                rag:EmitSound(Sound("vo/Citadel/br_ohshit.wav"))
             
                victim:PrintMessage( HUD_PRINTCENTER, "You have been tasered. "..self.StunTime.." seconds till revival" )
                 timer.Create("revivedelay"..victim:UniqueID(), self.StunTime, 1, function() TazerRevive( victim ) end )

end

function TazerRevive( victim)

    if !IsValid( victim ) then return end
    rag = victim:GetParent()

    if SERVER and victim.IsSlayed then
        victim:Kill();
        return
    end
        victim:SetParent()
                       

                        if (rag and rag:IsValid()) then
                                weps = table.Copy(rag.Weapons)
                                credits = rag.Credits
                                oldHealth = rag.OldHealth
                                rag:DropToFloor()
                                pos = rag:GetPos()  --move up a little
                                 pos.z = pos.z + 15                               
                                
              
                                victim.Ragdolled = nil
                               
                                victim:UnSpectate()
                              --  victim:DrawViewModel(true)
                              --  victim:DrawWorldModel(true)
                                victim:Spawn()
                                --ULib.spawn( victim, true )
                                rag:Remove()
                                victim:SetPos( pos )
                                victim:StripWeapons()

                                UnStuck_stuck(victim)

                                for _, v in pairs (weps) do
                                        victim:Give(v)
                                end

                            if credits > 0 then
                                    victim:AddCredits( credits  )
                            end
                            if oldHealth > 0 then
                                victim:SetHealth( oldHealth  )
                            end
                            victim:DropToFloor()



                             if  !victim:IsInWorld() or !victim:OnGround()    then
                              victim:SetPos( victim.orgPos )
                             end

                              timer.Create("RespawnIfStuck",1,1,function()
                                     if IsValid(victim) and (!victim:IsInWorld() or !victim:OnGround() ) then
                                            victim:SetPos( victim.orgPos   )
                                     end
                              end)

                       else
                            print("Debug: ragdoll or player is invalid")
                            if IsValid( victim) then
                                    ULib.spawn( victim, true )
                           end
                       end
                


end



local ENT = {}

ENT.Type = "anim"
ENT.Base = "base_anim"


if SERVER then

    function ENT:Initialize()


        self:SetModel("models/crisps/gaprika.mdl")
        self:SetModelScale(self:GetModelScale()*1,0)

        self.Entity:PhysicsInit(SOLID_VPHYSICS);
        self.Entity:SetMoveType(MOVETYPE_VPHYSICS);
        self.Entity:SetSolid(SOLID_VPHYSICS);
        self.Entity:SetCollisionGroup(COLLISION_GROUP_PROJECTILE)
        self.Damage = 1
        self:SetUseType(SIMPLE_USE)
        local phys = self:GetPhysicsObject()
        if phys:IsValid() then
            phys:Wake()
            phys:EnableGravity(true);
        end
        timer.Simple(15,function() if IsValid(self) then self:Remove() end end)

    end

    function ENT:Use( activator, caller )

        if ( activator:IsPlayer() ) then
            if activator:Health() < 150 then

                    activator:SetHealth(activator:Health()+10)

            end
            activator:EmitSound("crisps/eat.mp3")
        end
        self.Entity:Remove()
    end


end

scripted_ents.Register( ENT, "sopopochisp", true )






/********************************************************
	SWEP Construction Kit base code
		Created by Clavus
	Available for public use, thread at:
	   facepunch.com/threads/1032378
	   
	   
	DESCRIPTION:
		This script is meant for experienced scripters 
		that KNOW WHAT THEY ARE DOING. Don't come to me 
		with basic Lua questions.
		
		Just copy into your SWEP or SWEP base of choice
		and merge with your own code.
		
		The SWEP.VElements, SWEP.WElements and
		SWEP.ViewModelBoneMods tables are all optional
		and only have to be visible to the client.
********************************************************/

function SWEP:Initialize()
 

	// other initialize code goes here

	if CLIENT then
	
	self.VElements["hud2"].draw_func = function( weapon )
	
	if ( weapon:Clip1() < 21 ) then
            draw.SimpleText(""..weapon:Clip1(), "default", 0, 0, Color(255,0,0,255), TEXT_ALIGN_CENTER, TEXT_ALIGN_TOP)
    	else 
	    draw.SimpleText(""..weapon:Clip1(), "default", 0, 0, Color(255,255,255,255), TEXT_ALIGN_CENTER, TEXT_ALIGN_TOP)
	end
     
end

self.VElements["hud2+"].draw_func = function( weapon )
	
	if ( weapon:Ammo1() < 60 ) then
            draw.SimpleText(""..weapon:Ammo1(), "default", 0, 0, Color(255,0,0,255), TEXT_ALIGN_CENTER, TEXT_ALIGN_TOP)
    	else 
	    draw.SimpleText(""..weapon:Ammo1(), "default", 0, 0, Color(255,255,255,255), TEXT_ALIGN_CENTER, TEXT_ALIGN_TOP)
	end
     
end




		// Create a new table for every weapon instance
		self.VElements = table.FullCopy( self.VElements )
		self.WElements = table.FullCopy( self.WElements )
		self.ViewModelBoneMods = table.FullCopy( self.ViewModelBoneMods )

		self:CreateModels(self.VElements) // create viewmodels
		self:CreateModels(self.WElements) // create worldmodels
		
		// init view model bone build function
                if IsValid(self.Owner) then
                    local vm = self.Owner:GetViewModel()
                    if IsValid(vm) then
                            self:ResetBonePositions(vm)
                    end
                end
		
		// Init viewmodel visibility
		if (self.ShowViewModel == nil or self.ShowViewModel) then
		--	vm:SetColor(Color(255,255,255,255))
		else
			// we set the alpha to 1 instead of 0 because else ViewModelDrawn stops being called
			vm:SetColor(Color(255,255,255,1))
			// ^ stopped working in GMod 13 because you have to do Entity:SetRenderMode(1) for translucency to kick in
			// however for some reason the view model resets to render mode 0 every frame so we just apply a debug material to prevent it from drawing
			vm:SetMaterial("Debug/hsv")			
		end
		
	end

end

function SWEP:Holster()
	
	if CLIENT && IsValid(self.Owner) then
		local vm = self.Owner:GetViewModel()
		if IsValid(vm) then
			self:ResetBonePositions(vm)
		end
	end
	
	return true
end

if CLIENT then

	SWEP.vRenderOrder = nil
	function SWEP:ViewModelDrawn()
        if !IsValid(self.Owner) then return end
		local vm = self.Owner:GetViewModel()
		if !IsValid(vm) then return end
		
		if (!self.VElements) then return end
		
		self:UpdateBonePositions(vm)

		if (!self.vRenderOrder) then
			
			// we build a render order because sprites need to be drawn after models
			self.vRenderOrder = {}

			for k, v in pairs( self.VElements ) do
				if (v.type == "Model") then
					table.insert(self.vRenderOrder, 1, k)
				elseif (v.type == "Sprite" or v.type == "Quad") then
					table.insert(self.vRenderOrder, k)
				end
			end
			
		end

		for k, name in ipairs( self.vRenderOrder ) do
		
			local v = self.VElements[name]
			if (!v) then self.vRenderOrder = nil break end
			if (v.hide) then continue end
			
			local model = v.modelEnt
			local sprite = v.spriteMaterial
			
			if (!v.bone) then continue end
			
			local pos, ang = self:GetBoneOrientation( self.VElements, v, vm )
			
			if (!pos) then continue end
			
			if (v.type == "Model" and IsValid(model)) then

				model:SetPos(pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z )
				ang:RotateAroundAxis(ang:Up(), v.angle.y)
				ang:RotateAroundAxis(ang:Right(), v.angle.p)
				ang:RotateAroundAxis(ang:Forward(), v.angle.r)

				model:SetAngles(ang)
				//model:SetModelScale(v.size)
				local matrix = Matrix()
				matrix:Scale(v.size)
				model:EnableMatrix( "RenderMultiply", matrix )
				
				if (v.material == "") then
					model:SetMaterial("")
				elseif (model:GetMaterial() != v.material) then
					model:SetMaterial( v.material )
				end
				
				if (v.skin and v.skin != model:GetSkin()) then
					model:SetSkin(v.skin)
				end
				
				if (v.bodygroup) then
					for k, v in pairs( v.bodygroup ) do
						if (model:GetBodygroup(k) != v) then
							model:SetBodygroup(k, v)
						end
					end
				end
				
				if (v.surpresslightning) then
					render.SuppressEngineLighting(true)
				end
				
				render.SetColorModulation(v.color.r/255, v.color.g/255, v.color.b/255)
				render.SetBlend(v.color.a/255)
				model:DrawModel()
				render.SetBlend(1)
				render.SetColorModulation(1, 1, 1)
				
				if (v.surpresslightning) then
					render.SuppressEngineLighting(false)
				end
				
			elseif (v.type == "Sprite" and sprite) then
				
				local drawpos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
				render.SetMaterial(sprite)
				render.DrawSprite(drawpos, v.size.x, v.size.y, v.color)
				
			elseif (v.type == "Quad" and v.draw_func) then
				
				local drawpos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
				ang:RotateAroundAxis(ang:Up(), v.angle.y)
				ang:RotateAroundAxis(ang:Right(), v.angle.p)
				ang:RotateAroundAxis(ang:Forward(), v.angle.r)
				
				cam.Start3D2D(drawpos, ang, v.size)
					v.draw_func( self )
				cam.End3D2D()

			end
			
		end
		
	end

	SWEP.wRenderOrder = nil
	function SWEP:DrawWorldModel()
		
		if (self.ShowWorldModel == nil or self.ShowWorldModel) then
			self:DrawModel()
		end
		
		if (!self.WElements) then return end
		
		if (!self.wRenderOrder) then

			self.wRenderOrder = {}

			for k, v in pairs( self.WElements ) do
				if (v.type == "Model") then
					table.insert(self.wRenderOrder, 1, k)
				elseif (v.type == "Sprite" or v.type == "Quad") then
					table.insert(self.wRenderOrder, k)
				end
			end

		end
		
		if (IsValid(self.Owner)) then
			bone_ent = self.Owner
		else
			// when the weapon is dropped
			bone_ent = self
		end
		
		for k, name in pairs( self.wRenderOrder ) do
		
			local v = self.WElements[name]
			if (!v) then self.wRenderOrder = nil break end
			if (v.hide) then continue end
			
			local pos, ang
			
			if (v.bone) then
				pos, ang = self:GetBoneOrientation( self.WElements, v, bone_ent )
			else
				pos, ang = self:GetBoneOrientation( self.WElements, v, bone_ent, "ValveBiped.Bip01_R_Hand" )
			end
			
			if (!pos) then continue end
			
			local model = v.modelEnt
			local sprite = v.spriteMaterial
			
			if (v.type == "Model" and IsValid(model)) then

				model:SetPos(pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z )
				ang:RotateAroundAxis(ang:Up(), v.angle.y)
				ang:RotateAroundAxis(ang:Right(), v.angle.p)
				ang:RotateAroundAxis(ang:Forward(), v.angle.r)

				model:SetAngles(ang)
				//model:SetModelScale(v.size)
				local matrix = Matrix()
				matrix:Scale(v.size)
				model:EnableMatrix( "RenderMultiply", matrix )
				
				if (v.material == "") then
					model:SetMaterial("")
				elseif (model:GetMaterial() != v.material) then
					model:SetMaterial( v.material )
				end
				
				if (v.skin and v.skin != model:GetSkin()) then
					model:SetSkin(v.skin)
				end
				
				if (v.bodygroup) then
					for k, v in pairs( v.bodygroup ) do
						if (model:GetBodygroup(k) != v) then
							model:SetBodygroup(k, v)
						end
					end
				end
				
				if (v.surpresslightning) then
					render.SuppressEngineLighting(true)
				end
				
				render.SetColorModulation(v.color.r/255, v.color.g/255, v.color.b/255)
				render.SetBlend(v.color.a/255)
				model:DrawModel()
				render.SetBlend(1)
				render.SetColorModulation(1, 1, 1)
				
				if (v.surpresslightning) then
					render.SuppressEngineLighting(false)
				end
				
			elseif (v.type == "Sprite" and sprite) then
				
				local drawpos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
				render.SetMaterial(sprite)
				render.DrawSprite(drawpos, v.size.x, v.size.y, v.color)
				
			elseif (v.type == "Quad" and v.draw_func) then
				
				local drawpos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
				ang:RotateAroundAxis(ang:Up(), v.angle.y)
				ang:RotateAroundAxis(ang:Right(), v.angle.p)
				ang:RotateAroundAxis(ang:Forward(), v.angle.r)
				
				cam.Start3D2D(drawpos, ang, v.size)
					v.draw_func( self )
				cam.End3D2D()

			end
			
		end
		
	end

	function SWEP:GetBoneOrientation( basetab, tab, ent, bone_override )
		
		local bone, pos, ang
		if (tab.rel and tab.rel != "") then
			
			local v = basetab[tab.rel]
			
			if (!v) then return end
			
			// Technically, if there exists an element with the same name as a bone
			// you can get in an infinite loop. Let's just hope nobody's that stupid.
			pos, ang = self:GetBoneOrientation( basetab, v, ent )
			
			if (!pos) then return end
			
			pos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
			ang:RotateAroundAxis(ang:Up(), v.angle.y)
			ang:RotateAroundAxis(ang:Right(), v.angle.p)
			ang:RotateAroundAxis(ang:Forward(), v.angle.r)
				
		else
		
			bone = ent:LookupBone(bone_override or tab.bone)

			if (!bone) then return end
			
			pos, ang = Vector(0,0,0), Angle(0,0,0)
			local m = ent:GetBoneMatrix(bone)
			if (m) then
				pos, ang = m:GetTranslation(), m:GetAngles()
			end
			
			if (IsValid(self.Owner) and self.Owner:IsPlayer() and 
				ent == self.Owner:GetViewModel() and self.ViewModelFlip) then
				ang.r = -ang.r // Fixes mirrored models
			end
		
		end
		
		return pos, ang
	end

	function SWEP:CreateModels( tab )

		if (!tab) then return end

		// Create the clientside models here because Garry says we can't do it in the render hook
		for k, v in pairs( tab ) do
			if (v.type == "Model" and v.model and v.model != "" and (!IsValid(v.modelEnt) or v.createdModel != v.model) and 
					string.find(v.model, ".mdl") and file.Exists (v.model, "GAME") ) then
				
				v.modelEnt = ClientsideModel(v.model, RENDER_GROUP_VIEW_MODEL_OPAQUE)
				if (IsValid(v.modelEnt)) then
					v.modelEnt:SetPos(self:GetPos())
					v.modelEnt:SetAngles(self:GetAngles())
					v.modelEnt:SetParent(self)
					v.modelEnt:SetNoDraw(true)
					v.createdModel = v.model
				else
					v.modelEnt = nil
				end
				
			elseif (v.type == "Sprite" and v.sprite and v.sprite != "" and (!v.spriteMaterial or v.createdSprite != v.sprite) 
				and file.Exists ("materials/"..v.sprite..".vmt", "GAME")) then
				
				local name = v.sprite.."-"
				local params = { ["$basetexture"] = v.sprite }
				// make sure we create a unique name based on the selected options
				local tocheck = { "nocull", "additive", "vertexalpha", "vertexcolor", "ignorez" }
				for i, j in pairs( tocheck ) do
					if (v[j]) then
						params["$"..j] = 1
						name = name.."1"
					else
						name = name.."0"
					end
				end

				v.createdSprite = v.sprite
				v.spriteMaterial = CreateMaterial(name,"UnlitGeneric",params)
				
			end
		end
		
	end
	
	local allbones
	local hasGarryFixedBoneScalingYet = false

	function SWEP:UpdateBonePositions(vm)
		
		if self.ViewModelBoneMods then
			
			if (!vm:GetBoneCount()) then return end
			
			// !! WORKAROUND !! //
			// We need to check all model names :/
			local loopthrough = self.ViewModelBoneMods
			if (!hasGarryFixedBoneScalingYet) then
				allbones = {}
				for i=0, vm:GetBoneCount() do
					local bonename = vm:GetBoneName(i)
					if (self.ViewModelBoneMods[bonename]) then 
						allbones[bonename] = self.ViewModelBoneMods[bonename]
					else
						allbones[bonename] = { 
							scale = Vector(1,1,1),
							pos = Vector(0,0,0),
							angle = Angle(0,0,0)
						}
					end
				end
				
				loopthrough = allbones
			end
			// !! ----------- !! //
			
			for k, v in pairs( loopthrough ) do
				local bone = vm:LookupBone(k)
				if (!bone) then continue end
				
				// !! WORKAROUND !! //
				local s = Vector(v.scale.x,v.scale.y,v.scale.z)
				local p = Vector(v.pos.x,v.pos.y,v.pos.z)
				local ms = Vector(1,1,1)
				if (!hasGarryFixedBoneScalingYet) then
					local cur = vm:GetBoneParent(bone)
					while(cur >= 0) do
						local pscale = loopthrough[vm:GetBoneName(cur)].scale
						ms = ms * pscale
						cur = vm:GetBoneParent(cur)
					end
				end
				
				s = s * ms
				// !! ----------- !! //
				
				if vm:GetManipulateBoneScale(bone) != s then
					vm:ManipulateBoneScale( bone, s )
				end
				if vm:GetManipulateBoneAngles(bone) != v.angle then
					vm:ManipulateBoneAngles( bone, v.angle )
				end
				if vm:GetManipulateBonePosition(bone) != p then
					vm:ManipulateBonePosition( bone, p )
				end
			end
		else
			self:ResetBonePositions(vm)
		end
		   
	end
	 
	function SWEP:ResetBonePositions(vm)
		
		if (!vm:GetBoneCount()) then return end
		for i=0, vm:GetBoneCount() do
			vm:ManipulateBoneScale( i, Vector(1, 1, 1) )
			vm:ManipulateBoneAngles( i, Angle(0, 0, 0) )
			vm:ManipulateBonePosition( i, Vector(0, 0, 0) )
		end
		
	end

	/**************************
		Global utility code
	**************************/

	// Fully copies the table, meaning all tables inside this table are copied too and so on (normal table.Copy copies only their reference).
	// Does not copy entities of course, only copies their reference.
	// WARNING: do not use on tables that contain themselves somewhere down the line or you'll get an infinite loop
	function table.FullCopy( tab )

		if (!tab) then return nil end
		
		local res = {}
		for k, v in pairs( tab ) do
			if (type(v) == "table") then
				res[k] = table.FullCopy(v) // recursion ho!
			elseif (type(v) == "Vector") then
				res[k] = Vector(v.x, v.y, v.z)
			elseif (type(v) == "Angle") then
				res[k] = Angle(v.p, v.y, v.r)
			else
				res[k] = v
			end
		end
		
		return res
		
	end
	
end

