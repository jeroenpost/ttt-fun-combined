﻿-- This script and all files associated with it and the name 'StraySwordsman' are protected by copyright infringement laws.
--   Paranoia Instinctive ®2013 StraySwordsman 2012®
--   You must ask the permission of StraySwordsman for use of this script.
--   By using this script with my permission, you acknowledge that I may take your permission back at any time rendering you to be unable to use it.
-- Let the script begin.

-- Some edits by Render.

if SERVER then
	AddCSLuaFile ( )
end

if CLIENT then
	SWEP.PrintName = "Silent's S.N.A.S. Prototype"
	SWEP.Slot	   = 7 -- + 1 for slot hotkey

	SWEP.ViewModelFOV = 72
	SWEP.ViewModelFlip = false -- true = right hand holds gun
   
   SWEP.EquipMenuData = {
      type = "Shoots explosive rockets.",
      desc = "Turn those pesky innocents into pink mist! \nLonger flight time = more damage and radius!"
    };
end

SWEP.Base = "aa_base"

---Standard GMod Values, feel free to add more
SWEP.HoldType			= "rpg"
SWEP.Primary.Delay		= 0.2
SWEP.Primary.Recoil		= 10
SWEP.Primary.Automatic	= false
SWEP.Primary.Damage		= 260
SWEP.Primary.Cone		= 0.01
SWEP.Primary.Ammo		= "RPG_Round"
SWEP.HeadshotMultiplier = 1
SWEP.DrawCrosshair 		= true
SWEP.XHair				= false

-- Ammo: How to:
SWEP.Primary.Empty			= Sound( "Weapon_Shotgun.Empty" )
SWEP.Primary.ClipSize 		= 1
SWEP.Primary.DefaultClip 	= 3
SWEP.Primary.ClipMax 		= 3
SWEP.Primary.Round 			= ("ammo_ren_rpg_missle") -- This is the entity that's created upon fire.
SWEP.Primary.RPM 			= 14.2 -- Rounds per minute
SWEP.IronSightsPos			= Vector(-7.778, -7.24, -3)
SWEP.IronSightsAng			= Vector(2.71, -1.8, -3.8)
SWEP.UseHands			= true
SWEP.ViewModelFlip		= false
SWEP.ViewModelFOV		= 70
SWEP.ViewModel  = "models/weapons/c_rpg.mdl"
SWEP.WorldModel = "models/weapons/w_rocket_launcher.mdl"

--- TTT Values, proceed with caution ---

SWEP.Kind 			= WEAPON_EQUIP2 -- EQUIP1 is the primary bought weapon slot
SWEP.AutoSpawnable 	= false -- true = spawns around the map
SWEP.AmmEnt 		= "none" -- Entity treated as ammo
--SWEP.CanBuy 		= { ROLE_TRAITOR } -- ROLE_TRAITOR and ROLE_DETECTIVE are usable, ROLE_INNOCENT is untested
SWEP.InLoadoutFor 	= nil -- Role that automatically spawns with this, like the DNA Scanner or Body Armor
SWEP.LimitedStock 	= true -- true = can only buy once
SWEP.AllowDrop 		= true -- true = allowed to drop weapon with drop hotkey
SWEP.IsSilent 		= false -- true = victim makes no sound on death, knife for example
SWEP.NoSights 		= false -- Confused...
-- Custom input functions --

function SWEP:OnRemove() -- Removes timer on weapon removal (IE. Round restart.)
	timer.Destroy("rpgIdle")
end

if CLIENT then
	function SWEP:AdjustMouseSensitivity()
		return (self:GetIronsights() and 0.5) or nil
	end
end

function SWEP:Idle() -- Timer function for idle.
local rpg_vm = self.Owner:GetViewModel()
	if IsValid(self.Owner) then
	timer.Destroy("rpgIdle")
		timer.Create("rpgIdle", 4.5, 0, function()
			self:idleChoose();
		end)
	else
		timer.Destroy("rpgIdle")
	end
end

function SWEP:idleChoose() -- Check ammo, chooses Idle animation.
--	print (self.Owner:IsValid())
	if IsValid(self.Owner) then
	local rpg_vm = self.Owner:GetViewModel()
		if self.Owner:GetActiveWeapon():Clip1() == 0 then 
			rpg_vm:SetSequence(rpg_vm:LookupSequence("idle_empty")) 
		end
		if self.Owner:GetActiveWeapon():Clip1() > 0 then 
			rpg_vm:SetSequence(rpg_vm:LookupSequence("idle")) 
		end
	else
		timer.Destroy("rpgIdle")
	end
end

function SWEP:DrawHUD() -- No Crosshair.
end
SWEP.Potatoes = {}
SWEP.NextSecondaryAttack = 0
function SWEP:PrimaryAttack() -- Attack hook.
    if self.Owner:KeyDown(IN_RELOAD) then
        if self.NextSecondaryAttack > CurTime() or self:Clip1() > 2 then return end
        self.NextSecondaryAttack = CurTime() + 2
        for k,v in pairs( self.Potatoes ) do
            if IsValid( v ) then
                v:Explode();
            end
        end

        self.Potatoes = {}
        self:SetClip1( 3 )
        return
    end
	if self:CanPrimaryAttack() then
		timer.Destroy("rpgIdle"); -- Kills timer to stop it from calling idle animation in the middle of fire anim.
		local rpg_vm = self.Owner:GetViewModel()
		local pos2 = self.Owner:GetShootPos()
		self:FireRocket()
		self.Weapon:EmitSound("weapons/rpg/rocketfire1.wav")
		util.ScreenShake(pos2, 10, 2, 0.1, 200)
		self.Weapon:TakePrimaryAmmo(1)
		rpg_vm:SetSequence(rpg_vm:LookupSequence("shoot1"))
		self.Owner:SetAnimation(PLAYER_ATTACK1)
		self.Owner:MuzzleFlash()
		self.Weapon:SetNextPrimaryFire(CurTime()+1/(self.Primary.RPM/70))
		self:Idle() -- Runs timer for idle anims.
	end
end

function SWEP:FireRocket() -- Rocket spawn function, refers to outside lua ent (self.primary.round).
	local aim = self.Owner:GetAimVector()
	local side = aim:Cross(Vector(0,0,1))
	local up = side:Cross(aim)
	local pos = self.Owner:GetShootPos() + side * 6 + up * -5
	
if SERVER then
	local rocket = ents.Create(self.Primary.Round)
		if !rocket:IsValid() then return false end
		rocket:SetAngles(aim:Angle()+Angle(0,0,0))
		rocket:SetPos(pos)
		rocket:SetOwner(self.Owner)
		rocket:Spawn()
		rocket:Activate()
	end
end
SWEP.nextsh = 0
function SWEP:SecondaryAttack() -- Ironsight function.
    if self.Owner:KeyDown(IN_RELOAD) then
        if self.nextsh < CurTime() && #self.Potatoes < 3 then
            self.nextsh = CurTime() + 0.5
            self.Weapon:EmitSound ( self.Primary.Sound, 400, 100  )
            self.Weapon:SendWeaponAnim( ACT_VM_PRIMARYATTACK )
            self.Owner:MuzzleFlash()
            self:throw_attack()
            --self.Weapon:Reload()
        else
            self.Weapon:EmitSound("Buttons.snd14")
        end
        self.Weapon:SetNextPrimaryFire(CurTime()+0.2)
        return
    end
    self:rFly()
end

function SWEP:PreDrop() -- Remove ironsights before drop.
	self:SetIronsights(false)	
	return self.BaseClass.PreDrop(self)
end

function SWEP:Reload() -- Checks ammo and clip before reloading.
    if self.Owner:KeyDown(IN_ATTACK2) then return end
    if self.Owner:KeyDown(IN_ATTACK) then return end
	if self.Owner:GetAmmoCount(self.Primary.Ammo) != 0 && self.Owner:GetActiveWeapon():Clip1() != 1 then
		timer.Destroy("rpgIdle");
		local rpg_vm = self.Owner:GetViewModel()
		self.Weapon:DefaultReload( ACT_VM_RELOAD )
		self.Weapon:EmitSound("npc/sniper/reload1.wav")
		rpg_vm:SetSequence(rpg_vm:LookupSequence("reload"))
		self:SetIronsights(false)
		self:Idle()
	end	
end

function SWEP:Deploy() -- Checks ammo and clip to choose draw sequence.
	local rpg_vm = self.Owner:GetViewModel()
	self.Weapon:SetNextPrimaryFire(CurTime() + 1.7)
	self:SetIronsights(false)
	self.Weapon:EmitSound("weapons/ar2/ar2_reload.wav")
	self:Idle()
		if self.Owner:GetActiveWeapon():Clip1() == 0 then
			rpg_vm:SetPlaybackRate( 0.84 )
			rpg_vm:SetSequence(rpg_vm:LookupSequence("draw_empty"))
		end
		if self.Owner:GetActiveWeapon():Clip1() > 0 then
			rpg_vm:SetPlaybackRate( 0.84 )
			rpg_vm:SetSequence(rpg_vm:LookupSequence("draw"))
		end
	return true
end

function SWEP:Holster() -- Kills idle function timer to stop it from malfunctioning when swapping weapon quickly.
	self:SetIronsights(false)
	timer.Destroy("rpgIdle");
	return true
end

function SWEP:WasBought(rpg_buyer) -- Adds primary ammo, leaves clip empty. Names the player who bought the weapon 'rpg_buyer'.
   if IsValid(rpg_buyer) then
      rpg_buyer:GiveAmmo( 3, "RPG_Round", false )
	  self.Weapon:SetNextPrimaryFire(0)
   end
end


function SWEP:throw_attack()

    if (SERVER) then

        self.Owner:SetAnimation( PLAYER_ATTACK1 )

        local tr = self.Owner:GetEyeTrace();
        if true then

            local Potato = ents.Create("silent_rpg_remote")
            Potato:SetPos(self.Owner:EyePos() + (self.Owner:GetAimVector() * 16))
            Potato:SetAngles( self.Owner:EyeAngles() )
            Potato:SetPhysicsAttacker(self.Owner)
            Potato:SetOwner(self.Owner)
            Potato.Welded = false
            Potato:Spawn()

            local phys = Potato:GetPhysicsObject();

            local PlayerPos = self.Owner:GetShootPos()

            local shot_length = tr.HitPos:Length();
            phys:ApplyForceCenter (self.Owner:GetAimVector():GetNormalized() * 200);

            table.insert(self.Potatoes, Potato)

        else


            local ply = self.Owner
            local tr = ply:GetEyeTrace() --Get an eye trace
            local ent = tr.Entity --Get the entity hit by the eye trace, if anything
            if !ent then return end --Make sure we actually hit something!!!
            local Potato = ents.Create("silent_rpg_remote") --Create the manhack
            Potato:SetPos( tr.HitPos + self.Owner:GetAimVector() * 0 )
            Potato:SetAngles( tr.HitNormal:Angle() )
            Potato:SetPhysicsAttacker(self.Owner)
            Potato:SetOwner(self.Owner)
            Potato.Owner = self.Owner

            Potato:Spawn() --Spawn the manhack



            table.insert(self.Potatoes, Potato)
        end



    end

end




local ENT = {}

ENT.Type = "anim"
ENT.Base = "base_anim"


if SERVER then

    function ENT:Initialize()
        self:SetModel("models/maxofs2d/companion_doll.mdl")
        self:SetModelScale(self:GetModelScale()*math.Rand(0.5,3),0)
        self.Entity:PhysicsInit(SOLID_VPHYSICS);
        self.Entity:SetMoveType(MOVETYPE_VPHYSICS);
        self.Entity:SetSolid(SOLID_VPHYSICS);
        self.CanBoom = CurTime() + 3


        self.Entity:SetCollisionGroup(COLLISION_GROUP_WEAPON)
        local phys = self:GetPhysicsObject()
        if phys:IsValid() then
            phys:Wake()
            phys:EnableGravity(true);
        end


    end


    function ENT:Use( activator, caller )
        if self.CanBoom > CurTime() then return end
        self:EmitSound("common/bugreporter_failed.wav")
        self:Explode()
    end

    function ENT:Touch( ent )
        if ent:IsPlayer() and not ent.nextsandtouch or (ent.nextsandtouch and ent.nextsandtouch < CurTime()) then
            ent.nextsandtouch = CurTime() + 3
            ent:Ignite(0.5,0)
            local dmginfo = DamageInfo()
            dmginfo:SetDamage( 20 ) --50 damage
            dmginfo:SetDamageType( DMG_BULLET ) --Bullet damage
            dmginfo:SetAttacker( self.owner or ent ) --First player found gets credit
            dmginfo:SetDamageForce( Vector( 0, 0, 50 ) ) --Launch upwards
            dmginfo:SetInflictor(self)
            ent:TakeDamageInfo( dmginfo )
            self:Explode()
        end
    end

    function ENT:Explode()
        local effect = EffectData()
        effect:SetStart(self:GetPos())
        effect:SetOrigin(self:GetPos())
        effect:SetScale(120)
        effect:SetRadius(120)
        effect:SetMagnitude(250)

        util.Effect("Explosion", effect, true, true)

        util.BlastDamage(self, self.Owner, self:GetPos(), 250, 100)
        self:Remove()
    end


end

scripted_ents.Register( ENT, "silent_rpg_remote", true )