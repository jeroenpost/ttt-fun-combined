
SWEP.Base = "weapon_tttbase"
if (SERVER) then AddCSLuaFile("shared.lua") end SWEP.HoldType = "knife" function SWEP:Initialize() self:SetDeploySpeed(10) self:SetHoldType( self.HoldType or "pistol" ) end

if CLIENT then SWEP.Icon = surface.GetTextureID("vgui/entities/glv_glaive") function SWEP:DrawWeaponSelection(x, y, wide, tall, alpha) surface.SetDrawColor(255, 255, 255, alpha) surface.SetTexture(self.Icon) y = y + ScrH() * 0.01 x = x + ScrW() * 0.01 wide = wide * 0.8 tall = tall * 0.8 surface.DrawTexturedRect(x, y, wide, tall) end

killicon.Add("glv_glaive", "vgui/entities/glv_glaive", Color(255, 255, 255, 255)) killicon.Add("glv_ent_glaive", "vgui/entities/glv_glaive", Color(255, 255, 255, 255)) SWEP.WorldR = 0.6 SWEP.WorldF = 0 SWEP.WorldU = 0 SWEP.WorldANGR = 0 SWEP.WorldANGF = 90 SWEP.WorldANGU = -40 SWEP.WorldScaleX = 0.85 SWEP.WorldScaleY = 1 SWEP.WorldScaleZ = 1 function SWEP:DrawWorldModel() local hand, offset, rotate if not IsValid(self.Owner) then self:DrawModel() return end local OWNER = self.Owner if OWNER:GetModel() == "models/error.mdl" then return end hand = OWNER:GetAttachment(OWNER:LookupAttachment("anim_attachment_rh")) if not hand then hand = OWNER:GetAttachment(OWNER:LookupAttachment("blood_right")) end if hand then offset = hand.Ang:Right() * self.WorldR + hand.Ang:Forward() * self.WorldF + hand.Ang:Up() * self.WorldU hand.Ang:RotateAroundAxis(hand.Ang:Right(), self.WorldANGR) hand.Ang:RotateAroundAxis(hand.Ang:Forward(), self.WorldANGF) hand.Ang:RotateAroundAxis(hand.Ang:Up(), self.WorldANGU) self:SetRenderOrigin(hand.Pos + offset) self:SetRenderAngles(hand.Ang) self:DrawModel() self:SetModelScale(self.WorldScaleX, 0) end end
end if (CLIENT) then SWEP.Category = "The Glaive" SWEP.PrintName = "Glaive" SWEP.Slot = 8 SWEP.SlotPos = 0 SWEP.ViewModelFOV = 60 end SWEP.Instructions = "" SWEP.Author = "" SWEP.Contact = "" SWEP.Weight = 2 SWEP.ViewModelFlip = false SWEP.Spawnable = true SWEP.AdminOnly = true SWEP.ViewModel = "models/glaive/view/glaive.mdl" SWEP.WorldModel = "models/glaive/world/glaive.mdl" SWEP.Combo_Count = 1 SWEP.Primary.Recoil = 0 SWEP.Primary.Damage = 0 SWEP.Primary.NumShots = 0 SWEP.Primary.Cone = 0 SWEP.Primary.ClipSize = -1 SWEP.Primary.Delay = 1 SWEP.Primary.DefaultClip = 0 SWEP.Primary.Automatic = false SWEP.Primary.Ammo = "none" SWEP.Secondary.Automatic = false SWEP.Secondary.Ammo = "none" SWEP.Secondary.Delay = 1

function SWEP:ThrowingGlaive() return self.Glaive_Thrown end

function SWEP:ResetStuff() self.Glaive_Hit = nil self:SetThrown(false) self:SetComboCount(1) self.Reset_Combo = nil end

function SWEP:Deploy() self:SendWeaponAnim(ACT_VM_DRAW) self:SetTimers(self:AnimTime()) self:DoIdle(self:AnimTime()) self:ResetStuff() return true end

function SWEP:Holster() if self:ThrowingGlaive() then return false end return true end

function SWEP:AnimTime() if !IsValid(self.Owner) then return end local VM = self.Owner:GetViewModel() if IsValid(VM) then return CurTime() + VM:SequenceDuration() else return CurTime() + 0.5 end end

function SWEP:SetTimers(time) self.Glaive_Fidget = CurTime() + 10 self:SetNextPrimaryFire(time) self:SetNextSecondaryFire(time) end

function SWEP:DoIdle(time) self.Glaive_Idle = time end

function SWEP:GlaiveAttack() self.Glaive_Hit = nil if SERVER then local OWNER = self.Owner local EYE = OWNER:EyePos() local ANG = OWNER:EyeAngles() local TR = {} TR.start = EYE TR.endpos = EYE + ANG:Forward() * 70 TR.filter = OWNER TR.mask = MASK_PLAYERSOLID TR.mins = Vector(-10, -10, -5) TR.maxs = Vector(10, 10, 5) local Trace = util.TraceHull(TR) if Trace.Hit then local ENT = Trace.Entity local SND = "Glaive.Hit1" local COMBO = self:GetComboCount() if COMBO == 2 then SND = "Glaive.Hit2" elseif COMBO == 3 then SND = "Glaive.Hit3" end if ENT:IsPlayer() or ENT:IsNPC() then local FORCE = ANG:Forward() * 60000 + ANG:Right() * -80000 + ANG:Up() * 50000 local COMBO = self:GetComboCount() if COMBO == 2 then FORCE = ANG:Forward() * 60000 + ANG:Right() * 80000 + ANG:Up() * 50000 elseif COMBO == 3 then FORCE = ANG:Forward() * 30000 + ANG:Up() * 200000 end OWNER:EmitSound(SND, 75, 100, 0.8) self:BloodEffect(Trace.HitPos, ENT) self:MeleeDamage(Trace.HitPos, FORCE, ENT) else OWNER:EmitSound("Glaive.HitWall", 75, 100, 0.8) local PHYS = ENT:GetPhysicsObject() if IsValid(PHYS) then PHYS:ApplyForceCenter(ANG:Forward() * (150 * 40)) end end end end end

function SWEP:GlaiveThrow() self.Glaive_Throw = nil self:SetThrown(true) if SERVER then local OWNER = self.Owner local EYE = OWNER:EyePos() local ANG = OWNER:EyeAngles() self.MyGlaive = ents.Create("glv_ent_glaive") self.MyGlaive:SetPos(EYE + ANG:Forward() * 10 - ANG:Up() * 4) self.MyGlaive:SetAngles(ANG) self.MyGlaive:SetOwner(OWNER) self.MyGlaive:Spawn() self.MyGlaive:Activate() local PHYS = self.MyGlaive:GetPhysicsObject() if IsValid(PHYS) then PHYS:ApplyForceCenter(ANG:Forward() * GlaiveSpeed()) end end end

function SWEP:GlaiveCatch() self:SetThrown(false) self:SendWeaponAnim(ACT_VM_SECONDARYATTACK) self:SetTimers(self:AnimTime()) self:DoIdle(self:AnimTime()) self.RemoveThrown = CurTime() + 0.01 end

function SWEP:RemoveGlaive() self.RemoveThrown = nil if IsValid(self.MyGlaive) and self.MyGlaive.Remove then self.MyGlaive:Remove() end self.MyGlaive = nil end

function SWEP:Think() local CT = CurTime() if self.RemoveThrown and CT >= self.RemoveThrown then self:RemoveGlaive() end if self.Glaive_Hit and CT >= self.Glaive_Hit then self:GlaiveAttack() end if self.Glaive_Throw and CT >= self.Glaive_Throw then self:GlaiveThrow() end if self.Reset_Combo and CT >= self.Reset_Combo then self.Reset_Combo = nil self:SetComboCount(1) end if self.Glaive_Idle and CT >= self.Glaive_Idle then self.Glaive_Idle = nil local THR = self:ThrowingGlaive() local ANIM = ACT_VM_IDLE if THR then ANIM = ACT_VM_IDLE_DEPLOYED end self:SendWeaponAnim(ANIM) self.Glaive_Fidget = CT + 10 end if self.Glaive_Fidget and CT >= self.Glaive_Fidget then self.Glaive_Fidget = nil if not self:ThrowingGlaive() then self:SendWeaponAnim(ACT_VM_FIDGET) self:DoIdle(self:AnimTime()) end end end

function SWEP:Reload() if IsValid(self.MyGlaive) then self.MyGlaive.ReturnToOwner = CurTime() end end

function SWEP:PrimaryAttack() if self:ThrowingGlaive() then return end local COMBO = self:GetComboCount() local ANIM = ACT_VM_DEPLOY_1 local DELAY = 0.15 if COMBO == 1 then ANIM = ACT_VM_DEPLOY_1 DELAY = 0.15 elseif COMBO == 2 then ANIM = ACT_VM_DEPLOY_2 DELAY = 0.15 elseif COMBO == 3 then ANIM = ACT_VM_DEPLOY_3 DELAY = 0.07 end self:SendWeaponAnim(ANIM) self.Glaive_Hit = CurTime() + DELAY self:SetTimers(self:AnimTime() - 0.3) self:DoIdle(self:AnimTime()) end

function SWEP:SecondaryAttack() if self:ThrowingGlaive() then return end self:SendWeaponAnim(ACT_VM_PRIMARYATTACK) self.Glaive_Throw = CurTime() + 0.25 self:SetTimers(self:AnimTime()) self:DoIdle(self:AnimTime()) end

local BLOODCOL = {} BLOODCOL[0] = "blood_impact_red_01" BLOODCOL[1] = "blood_impact_yellow_01" BLOODCOL[2] = "blood_impact_green_01" BLOODCOL[3] = "blood_impact_synth_01" BLOODCOL[4] = "blood_impact_yellow_01" BLOODCOL[5] = "blood_impact_green_01" BLOODCOL[6] = "blood_impact_green_01" function SWEP:GetBloodColorParticle(ent) if IsValid(ent) and ent.GetBloodColor then local BLOOD = ent:GetBloodColor() if ent:IsPlayer() and BLOOD == -1 then return "blood_impact_red_01" end if BLOODCOL[BLOOD] then return BLOODCOL[BLOOD] else return "blood_impact_synth_01" end end end

function SWEP:BloodEffect(hitpos, enemy) if enemy:GetBloodColor() == -1 then return end for i = 1, math.random(1, 3) do local Splatter = ents.Create("info_particle_system") Splatter:SetPos(hitpos + self.Owner:EyeAngles():Forward() * 7 + VectorRand() * 7.5) Splatter:SetAngles(self.Owner:EyeAngles() + Angle(0, 180, 0)) local BloodParticle = self:GetBloodColorParticle(enemy) Splatter:SetKeyValue("effect_name", BloodParticle) Splatter:SetKeyValue("start_active", "1") Splatter:Spawn() Splatter:Activate() Splatter:Fire("Kill", nil, 1) end end

function SWEP:MeleeDamage(pos, force, ent) local MUL = 1 if ent:IsNPC() then MUL = 0.7 end if self:GetComboCount() == 3 then MUL = MUL * 2 end local DMG = DamageInfo() DMG:SetDamage(35) DMG:SetDamageType(DMG_SLASH) DMG:SetAttacker(self.Owner) DMG:SetInflictor(self) DMG:SetDamagePosition(pos) DMG:SetDamageForce(force) ent:TakeDamageInfo(DMG) if self:GetComboCount() < 3 then self:SetComboCount(self.Combo_Count + 1) self.Reset_Combo = CurTime() + 2 else self:SetComboCount(1) end end

function SWEP:GetComboCount() return (self.Combo_Count or 1) end

function SWEP:SetComboCount(num) self.Combo_Count = num if SERVER then net.Start("Glaive_Combo") net.WriteEntity(self) net.WriteFloat(self.Combo_Count) net.Send(self.Owner) end end

function SWEP:SetThrown(bool) self.Glaive_Thrown = bool if SERVER then net.Start("Glaive_Thrown") net.WriteEntity(self) net.WriteBit(self.Glaive_Thrown) net.Send(self.Owner) end end


function SWEP:OnDrop()
    self:RemoveGlaive()
    self:Remove()
    return falsed_tool
end

function SWEP:OnRemove()
    self:RemoveGlaive()
end