
sound.Add(
{
    name = "96FS.magout",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 80,
    sound = "weapons/Custom 96FS/96FS Mag Out.wav"
})
sound.Add(
{
    name = "96FS.magin",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 80,
    sound = "weapons/Custom 96FS/96FS Mag In.wav"
})
sound.Add(
{
    name = "96FS.release",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 80,
    sound = "weapons/Custom 96FS/96FS Release.wav"
})
sound.Add(
{
    name = "96FS.pull",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 80,
    sound = "weapons/Custom 96FS/96FS Pull.wav"
})
sound.Add(
{
    name = "96FS.rotate",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 80,
    sound = "weapons/Custom 96FS/96FS Rotate.wav"
})
sound.Add(
{
    name = "96FS.deploy",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 80,
    sound = "weapons/Custom 96FS/96FS Deploy.wav"
})
sound.Add(
{
    name = "96FS.click",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 80,
    sound = "weapons/Custom 96FS/96FS Click.wav"
})

sound.Add(
{
    name = "96FS.clutch",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 80,
    sound = "weapons/Custom 96FS/96FS Clutch.wav"
})