
if SERVER then

	if GetConVar("M9KWeaponStrip") == nil then
		CreateConVar("M9KWeaponStrip", "1", { FCVAR_REPLICATED, FCVAR_NOTIFY, FCVAR_ARCHIVE }, "Allow empty weapon stripping? 1 for true, 0 for false")
	end
	
	if GetConVar("M9KGasEffect") == nil then
		CreateConVar("M9KGasEffect", "1", { FCVAR_ARCHIVE }, "Use gas effect when shooting? 1 for true, 0 for false")
	end
	
	if GetConVar("M9KDisablePenetration") == nil then
		CreateConVar("M9KDisablePenetration", "0", { FCVAR_ARCHIVE }, "Disable Penetration and Ricochets? 1 for true, 0 for false")
	end
	
end


//jackal
sound.Add({
	name = 			"jackal_gun_shot",
	channel = 		CHAN_USER_BASE+10, --see how this is a different channel? Gunshots go here
	volume = 		1.0,
	sound = 			"weapons/jackal/deagle-1.mp3"
})

sound.Add({
	name = 			"murder_jackal.Slideback",
	channel = 		CHAN_ITEM,
	volume = 		1.0,
	sound = 		"weapons/jackal/1911slideback.mp3"
})

sound.Add({
	name = 			"murder_jackal.Clipout",
	channel = 		CHAN_ITEM,
	volume = 		1.0,
	sound = 		"weapons/jackal/clipout.mp3"
})

sound.Add({
	name = 			"murder_jackal.Clipin",
	channel = 		CHAN_ITEM,
	volume = 		1.0,
	sound = 		"weapons/jackal/clipin.mp3"
})

sound.Add({
	name = 			"murder_jackal.Deploy",
	channel = 		CHAN_ITEM,
	volume = 		1.0,
	sound = 		"weapons/jackal/deagle_draw.mp3"
})
