 function EFFECT:Init(data)
	
	self.WeaponEnt = data:GetEntity()
	self.Attachment = data:GetAttachment()
	local attachname = self.WeaponEnt.MuzzleAttachment
	self.Forward = data:GetNormal()	
	
	if ( IsValid( self.WeaponEnt ) == false ) then return end
	if ( IsValid( self.WeaponEnt.Owner ) == false ) then return end
	
	if ( GetViewEntity() == self.WeaponEnt.Owner ) then --First person
	
		local vm = self.WeaponEnt.Owner:GetViewModel()
		if ( IsValid( vm ) == false ) then return end

		local vmindex = vm:LookupAttachment( attachname )
		if ( vmindex == 0 ) then return end
		
		self.Position = vm:GetAttachment( vmindex ).Pos
		self.Position = self.Position + ( 30 * self.Forward )
		
		--In singleplayer, the attachment position will include my viewmodel-raising adjustments.
		--In multiplayer I have to add them manually to the reported attachment position.
		if ( game.SinglePlayer() ) then
			--self:FDebug( self.WeaponEnt.Owner, "SP First-person, no adjust. Shooting position: " .. tostring( self.Position ) )
			
		else
			--self:FDebug( self.WeaponEnt.Owner, "MP First-person, adjusting. Shooting position: " .. tostring( self.Position ) )
			if ( self.WeaponEnt.ZoomDraw == true ) then
				return
			end
			self.Position = self.Position + ( self.WeaponEnt.Owner:GetRight() * -1 * math.abs( self.WeaponEnt.LowerOffset.x ) ) + ( self.WeaponEnt.Owner:GetAimVector() * self.WeaponEnt.LowerOffset.y ) + ( self.WeaponEnt.Owner:GetUp() * self.WeaponEnt.LowerOffset.z )
		end
			
	else --Third person
	
		local attachindex = self.WeaponEnt:LookupAttachment( attachname )

		--self:FDebug( self.WeaponEnt.Owner, "Attachment index: " .. attachindex )
		
		--If singleplayer and attachment available, do that
		--If multiplayer or no attachment, can't use attachment anyway - use hand method
		
		if ( game.SinglePlayer() ) and ( attachindex != 0 ) then
			self.Position = self.WeaponEnt:GetAttachment( attachindex ).Pos
			--self:FDebug( self.WeaponEnt.Owner, "Third-person by attachment. Shooting position: " .. tostring( self.Position ) )
		else
			local bonenum = self.WeaponEnt.Owner:LookupBone( "ValveBiped.Bip01_R_Hand" )
			if ( bonenum ) then
				local hpos, hang = self.WeaponEnt.Owner:GetBonePosition( bonenum )
				--self.Position = hpos + ( 30 * self.Forward )
				self.Position = LocalToWorld( self.WeaponEnt.RHandMuzzleOffset, self.Forward:Angle(), hpos, hang )
			else
				self.Position = self.WeaponEnt.Owner:GetShootPos()
			end
			
			--self:FDebug( self.WeaponEnt.Owner, "Third-person by hand. Shooting position: " .. tostring( self.Position ) )
		end
			
	end	
	
	
	self.Angle = self.Forward:Angle()
	self.Right = self.Angle:Right()

	self.AddVel = self.WeaponEnt:GetOwner():GetVelocity()	|| Vector(0,0,0)

	--local AddVel = self.WeaponEnt:GetOwner():GetVelocity()

	local emitter = ParticleEmitter( self.Position )
		
		local particle = emitter:Add("sprites/heatwave", self.Position)
		particle:SetVelocity(80*self.Forward + 20*VectorRand())
		particle:SetDieTime(math.Rand(0.15,0.2))
		particle:SetStartSize(math.random(15,19))
		particle:SetEndSize(0)
		particle:SetRoll(math.Rand(180,480))
		particle:SetRollDelta(math.Rand(-1,1))
		particle:SetAirResistance(160)

		for i=0,2 do
		local particle = emitter:Add("particle/smokesprites_000"..math.random(1,9), self.Position)
		particle:SetVelocity(100*i*self.Forward)
		particle:SetDieTime(math.Rand(0.05,0.3))
		particle:SetStartAlpha(math.Rand(20,25))
		particle:SetEndAlpha(0)
		particle:SetStartSize(math.random(6,8))
		particle:SetEndSize(math.Rand(25,30))
		particle:SetRoll(math.Rand(180,480))
		particle:SetRollDelta(math.Rand(-3,3))
		particle:SetColor(170,170,170)
		particle:SetAirResistance(350)
		end
		
		for i=-1,1 do 
		local particle = emitter:Add("particle/smokesprites_000"..math.random(1,9), self.Position)
		particle:SetVelocity(80*i*self.Right)
		particle:SetDieTime(math.Rand(0.2,0.3))
		particle:SetStartAlpha(math.Rand(20,30))
		particle:SetEndAlpha(0)
		particle:SetStartSize(math.random(2,3))
		particle:SetEndSize(math.Rand(14,16))
		particle:SetRoll(math.Rand(180,480))
		particle:SetRollDelta(math.Rand(-1,1))
		particle:SetColor(200,200,200)
		particle:SetAirResistance(160)
		end
		
		if ( self.WeaponEnt.Suppressed == false ) then
			for i=0,5 do 
			local particle = emitter:Add("effects/muzzleflash"..math.random(1,4), self.Position+(self.Forward*i*2))
			particle:SetVelocity((self.Forward*i*5) + self.AddVel)
			particle:SetDieTime(0.05)
			particle:SetStartAlpha(255)
			particle:SetEndAlpha(0)
			particle:SetStartSize(8-i)
			particle:SetEndSize(15-i)
			particle:SetRoll(math.Rand(180,480))
			particle:SetRollDelta(math.Rand(-1,1))
			particle:SetColor(255,255,255)	
			end
		end
		
	emitter:Finish()

end


function EFFECT:Think()

	return false
	
end


function EFFECT:Render()

	
end



