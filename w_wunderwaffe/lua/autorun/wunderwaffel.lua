
//these are some variables we need to keep for stuff to work
//that means don't delete them
if CLIENT then

	if GetConVar("M9KWeaponStrip") == nil then
		CreateClientConVar("M9KWeaponStrip", "1", true, true)
		print("client-side Weapon Strip Con Var created")
	end
	
	if GetConVar("M9KGasEffect") == nil then
		CreateClientConVar("M9KGasEffect", "1", true, true)
		print("client-side Gas Effect Con Var created")
	end
	
	if GetConVar("M9KDisablePenetration") == nil then
		CreateClientConVar("M9KDisablePenetration", "0", true, true )
		print("client-side Penetration/Ricochet Con Var created")
	end

end

if GetConVar("M9KDynamicRecoil") == nil then
	CreateConVar("M9KDynamicRecoil", "1", { FCVAR_REPLICATED, FCVAR_NOTIFY, FCVAR_ARCHIVE }, "Use Aim-modifying recoil? 1 for true, 0 for false")
	print("Recoil con var created")
end



if SERVER then

	if GetConVar("M9KWeaponStrip") == nil then
		CreateConVar("M9KWeaponStrip", "1", { FCVAR_REPLICATED, FCVAR_NOTIFY, FCVAR_ARCHIVE }, "Allow empty weapon stripping? 1 for true, 0 for false")
		print("Weapon Strip con var created")
	end
	
	if GetConVar("M9KGasEffect") == nil then
		CreateConVar("M9KGasEffect", "1", { FCVAR_REPLICATED, FCVAR_NOTIFY, FCVAR_ARCHIVE }, "Use gas effect when shooting? 1 for true, 0 for false")
		print("Gas effect con var created")
	end
	
	if GetConVar("M9KDisablePenetration") == nil then
		CreateConVar("M9KDisablePenetration", "0", { FCVAR_REPLICATED, FCVAR_NOTIFY, FCVAR_ARCHIVE }, "Disable Penetration and Ricochets? 1 for true, 0 for false")
		print("Penetration/ricochet con var created")
	end
	
	util.AddNetworkString("PenetrationSynch")
	function PenetrationChangeCallback(cvar, previous, new)
		net.Start("PenetrationSynch")
		net.WriteBit(new)
		net.Broadcast()
	end
	cvars.AddChangeCallback("M9KDisablePenetration", PenetrationChangeCallback)

end

