
AddCSLuaFile()
DEFINE_BASECLASS( "base_entity" )

ENT.PrintName = "Fire Hazard projectile"
ENT.Author = "SAGA_"

ENT.Category = "SAGA_"
ENT.AdminOnly = false
ENT.Spawnable = false
ENT.AdminSpawnable = false

function ENT:Initialize()
         if ( SERVER ) then
		      self:SetModel( "models/Items/AR2_Grenade.mdl" )
			  self:SetSolid( SOLID_VPHYSICS )
			  self:SetMoveType( MOVETYPE_VPHYSICS )
			  self:PhysicsInit( SOLID_VPHYSICS )
			  timer.Simple( 12.00,function()
			        if ( IsValid( self ) ) then
					     self:Remove()
					end
			  end )
			  self.Bounced = false
			  if ( !self.BurnSound ) then
			       self.BurnSound = CreateSound( self,"weapons/flaregun/burn.wav" )
				   self.BurnSound:Play()
				  else
				   print( "the burning sound already exists, bug?" )
			  end
			  
			  local phys = self:GetPhysicsObject()
			  if ( IsValid( phys ) ) then
			       phys:Wake()
			  end
			 else
			  self.Emitter = ParticleEmitter( vector_origin )
		 end
end

function ENT:Draw()
         self:DrawModel()
end

function ENT:PhysicsCollide( data,ent )
         ent2 = data.HitEntity
		 if ( !IsValid( ent2 ) ) then return end
		 if ( self.Bounced == false ) then
			  if ( SERVER ) then
			       ent2:Ignite( 7 )
                  self:Explode()
		      end
			  self.Bounced = true
		 end
end


 function ENT:Explode()
        self.Entity:EmitSound( Sound("npc/assassin/ball_zap1.wav") )
        local radius = 250
        local phys_force = 512
        local push_force = 256
        local pos = self.Entity:GetPos()
        -- pull physics objects and push players
        for k, target in pairs(ents.FindInSphere(pos, radius)) do
            if IsValid(target) then
                local tpos = target:LocalToWorld(target:OBBCenter())
                local dir = (tpos - pos):GetNormal()
                local phys = target:GetPhysicsObject()

                if target:IsPlayer() and (not target:IsFrozen()) and ((not target.was_pushed) or target.was_pushed.t != CurTime()) then

                -- always need an upwards push to prevent the ground's friction from
                -- stopping nearly all movement
                dir.z = math.abs(dir.z) + 1

                local push = dir * push_force
                local vel = target:GetVelocity() + push
                vel.z = math.min(vel.z, push_force)
                local pusher = self:GetOwner() or self.Owner or nil

                target:SetVelocity(vel)
                target:SetGroundEntity( nil )
                target.was_pushed = {att=pusher, t=CurTime()}

                elseif IsValid(phys) then
                    phys:ApplyForceCenter(dir * -1 * phys_force)
                end
            end
        end

        local phexp = ents.Create("env_physexplosion")
        if IsValid(phexp) then
            phexp:SetPos(pos)
            phexp:SetKeyValue("magnitude", 100) --max
            phexp:SetKeyValue("radius", radius)
            -- 1 = no dmg, 2 = push ply, 4 = push radial, 8 = los, 16 = viewpunch
            phexp:SetKeyValue("spawnflags", 1 + 2 + 16)
            phexp:Spawn()
            phexp:Fire("Explode", "", 0.2)
        end
    end
function ENT:OnRemove()
         if ( !IsFirstTimePredicted() ) then return end // OnRemove gets called twice wtf.
         if ( self.BurnSound ) then
		      self.BurnSound:Stop()
			  self.BurnSound = nil
			 else
			  print( "burning sound was removed early, bug?" )
		 end
		 if ( CLIENT ) then
		      self.Emitter:Finish()
		 end
             end



function ENT:Think()
         if ( CLIENT ) then
		      local rand = math.Rand( -2,2 )
		      local flarelight1 = self.Emitter:Add( "effects/yellowflare",self:GetPos() )
			        flarelight1:SetRoll( rand )
			        flarelight1:SetStartSize( 11 )
					flarelight1:SetEndSize( 0 )
					flarelight1:SetColor( 255,0,0,255 )
					flarelight1:SetStartAlpha( 255 )
					flarelight1:SetEndAlpha( 255 )
					flarelight1:SetDieTime( 0.1 )
					
			  local flarelight2 = self.Emitter:Add( "effects/yellowflare",self:GetPos() )
			        flarelight2:SetRoll( rand )
			        flarelight2:SetStartSize( 11 )
					flarelight2:SetEndSize( 0 )
					flarelight2:SetColor( 255,255,255,255 )
					flarelight2:SetStartAlpha( 255 )
					flarelight2:SetEndAlpha( 255 )
					flarelight2:SetDieTime( 0.1 )
					
			  local smoke = self.Emitter:Add( "particles/smokey",self:GetPos() + VectorRand() )
			        smoke:SetStartSize( 2 )
					smoke:SetEndSize( 12 )
					smoke:SetColor( 210,0,0,255 )
					smoke:SetStartAlpha( 255 )
					smoke:SetEndAlpha( 0 )
					smoke:SetDieTime( 3 )
					smoke:SetVelocity( Vector( math.Rand( -5,5 ),math.Rand( -5,5 ),32 ) )
		 end
end

function ENT:Use()
end