ENT.Type = "anim"
ENT.PrintName			= "Nade"
ENT.Author			= ""
ENT.Contact			= ""
ENT.Purpose			= ""
ENT.Instructions			= ""

if SERVER then

AddCSLuaFile( "shared.lua" )

function ENT:Initialize()

	self.Owner = self.Entity.Owner

	self.Entity:SetModel("models/weapons/w_shotgun.mdl")
	self.Entity:PhysicsInit( SOLID_VPHYSICS )
	self.Entity:SetMoveType( MOVETYPE_VPHYSICS )
	self.Entity:SetSolid( SOLID_VPHYSICS )
	self.Entity:DrawShadow( false )
	self.Entity:SetCollisionGroup( COLLISION_GROUP_WEAPON )
	
	local phys = self.Entity:GetPhysicsObject()
	if (phys:IsValid()) then
	phys:Wake()
	phys:SetMass(5)
	end

	self.timeleft = CurTime() + 3
	--self:Think() -- we don't need to call think on spawn, it is called automatically.


end

 function ENT:Think()
	
	if self.timeleft < CurTime() then
		self:Explosion()	
	end

	self.Entity:NextThink( CurTime() )
	return true
end

function ENT:Explosion()


	local effectdata = EffectData()
		effectdata:SetOrigin(self.Entity:GetPos())
		effectdata:SetEntity(self.Entity)
		effectdata:SetStart(self.Entity:GetPos())
		effectdata:SetNormal(Vector(0,0,1))
		--util.Effect("ManhackSparks", effectdata)
		util.Effect("HelicopterMegaBomb", effectdata)
		util.Effect("Explosion", effectdata)
	
	local thumper = effectdata
		thumper:SetOrigin(self.Entity:GetPos())
		thumper:SetScale(500)
		thumper:SetMagnitude(500)
		util.Effect("ThumperDust", effectdata)
		
	local sparkeffect = effectdata
		sparkeffect:SetMagnitude(3)
		sparkeffect:SetRadius(8)
		sparkeffect:SetScale(5)
		util.Effect("Sparks", sparkeffect)
		
	local scorchstart = self.Entity:GetPos() + ((Vector(0,0,1)) * 5)
	local scorchend = self.Entity:GetPos() + ((Vector(0,0,-1)) * 5)
	
	util.BlastDamage(self.Entity, self.Owner, self.Entity:GetPos(), 350, 250)
	util.ScreenShake(self.Entity:GetPos(), 500, 500, 1.25, 500)
	self.Entity:Remove()
	-- loos like if I don't remove the entity before drawing this scorch, the scorch will draw on the entity 
	-- and disappear immediately after
	util.Decal("Scorch", scorchstart, scorchend)
end

/*---------------------------------------------------------
PhysicsCollide
---------------------------------------------------------*/
function ENT:PhysicsCollide(data,phys)
	if data.Speed > 50 then
		self.Owner:EmitSound("physics/metal/weapon_impact_hard" .. math.random(1, 3) .. ".wav", 90, math.random(95, 105))
	end
	
	local impulse = -data.Speed * data.HitNormal * .2 + (data.OurOldVelocity * -.6)
	phys:ApplyForceCenter(impulse)
	
end

end

function ENT:Draw()
	self.Entity:DrawModel()
end

if CLIENT then
ENT.WElements = {
	["backbot"] = { type = "Model", model = "models/props_lab/powerbox02d.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "back", pos = Vector(0, 0, 2.68), angle = Angle(90, 180, 0), size = Vector(0.379, 0.379, 0.379), color = Color(255, 255, 255, 255), surpresslightning = false, material = "models/props_trainstation/trackprops01a", skin = 0, bodygroup = {} },
	["back"] = { type = "Model", model = "models/props_c17/consolebox05a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "connect", pos = Vector(-5, 0, 1.7), angle = Angle(180, 0, 0), size = Vector(0.151, 0.151, 0.151), color = Color(255, 255, 255, 255), surpresslightning = false, material = "models/props_junk/popcan03a", skin = 0, bodygroup = {} },
	["rear"] = { type = "Model", model = "models/props_lab/powerbox01a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "back", pos = Vector(4.091, 0, 1.363), angle = Angle(-90, 0, 180), size = Vector(0.094, 0.094, 0.094), color = Color(159, 153, 162, 255), surpresslightning = false, material = "models/items/boxmrounds", skin = 0, bodygroup = {} },
	["holder++"] = { type = "Model", model = "models/props_lab/miniteleportarc.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "frontbarrels", pos = Vector(1.363, -2.274, -1.364), angle = Angle(180, 90, -90), size = Vector(0.094, 0.094, 0.094), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["sight"] = { type = "Model", model = "models/props_junk/metalgascan.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "back", pos = Vector(-0.456, 0, 0.455), angle = Angle(-180, 90, 0), size = Vector(0.3, 0.151, 0.094), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["stock"] = { type = "Model", model = "models/props_combine/CombineTrain01a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "rear", pos = Vector(-8.636, -0.801, -4.092), angle = Angle(-101.25, 0, 5.113), size = Vector(0.014, 0.014, 0.014), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["shell+++"] = { type = "Model", model = "models/weapons/shotgun_shell.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "holder+++", pos = Vector(-0.89, 0, 1.363), angle = Angle(-90, 0, 0), size = Vector(0.5, 0.5, 0.5), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["frontbarrels"] = { type = "Model", model = "models/props_c17/furnitureStove001a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(23.181, 0.899, -6.818), angle = Angle(97.158, -174.887, -3.069), size = Vector(0.094, 0.094, 0.549), color = Color(255, 255, 255, 255), surpresslightning = false, material = "models/props_trainstation/trainstation_post001", skin = 0, bodygroup = {} },
	["connect"] = { type = "Model", model = "models/props_junk/TrashDumpster01a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "frontbarrels", pos = Vector(0, 0, -12.273), angle = Angle(90, 0, 0), size = Vector(0.094, 0.05, 0.079), color = Color(255, 255, 255, 255), surpresslightning = false, material = "models/props_junk/trafficcone001a", skin = 0, bodygroup = {} },
	["holder+"] = { type = "Model", model = "models/props_lab/miniteleportarc.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "frontbarrels", pos = Vector(1.363, -2.274, -4.092), angle = Angle(180, 90, -90), size = Vector(0.094, 0.094, 0.094), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["shell+"] = { type = "Model", model = "models/weapons/shotgun_shell.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "holder+", pos = Vector(-0.89, 0, 1.363), angle = Angle(-90, 0, 0), size = Vector(0.5, 0.5, 0.5), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["ammobox"] = { type = "Model", model = "models/Items/BoxBuckshot.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "backbot", pos = Vector(-0.456, -2.5, -1.364), angle = Angle(0, 90, -90), size = Vector(0.321, 0.321, 0.321), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["shell++"] = { type = "Model", model = "models/weapons/shotgun_shell.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "holder++", pos = Vector(-0.89, 0, 1.363), angle = Angle(-90, 0, 0), size = Vector(0.5, 0.5, 0.5), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["holder+++"] = { type = "Model", model = "models/props_lab/miniteleportarc.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "frontbarrels", pos = Vector(1.363, -2.274, 1.363), angle = Angle(180, 90, -90), size = Vector(0.094, 0.094, 0.094), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["holder"] = { type = "Model", model = "models/props_lab/miniteleportarc.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "frontbarrels", pos = Vector(1.363, -2.274, -6.818), angle = Angle(180, 90, -90), size = Vector(0.094, 0.094, 0.094), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["shell"] = { type = "Model", model = "models/weapons/shotgun_shell.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "holder", pos = Vector(-0.89, 0, 1.363), angle = Angle(-90, 0, 0), size = Vector(0.5, 0.5, 0.5), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["under"] = { type = "Model", model = "models/props_c17/Lockers001a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "frontbarrels", pos = Vector(2.273, 0, 0.455), angle = Angle(0, 0, 0), size = Vector(0.094, 0.079, 0.209), color = Color(255, 255, 255, 255), surpresslightning = false, material = "models/props_c17/door01a_skin5", skin = 0, bodygroup = {} }
}
function ENT:Initialize()
    self.SpawnedCModels = {}
	for k,v in pairs( self.WElements ) do
	    local cmodel = ClientsideModel( v.model )
		      cmodel:SetPos( self:GetPos() + self:GetAngles():Right() * v.pos.x + self:GetAngles():Forward() * v.pos.y + self:GetAngles():Up() * v.pos.z )
			  local ang = self:GetAngles()
			        ang:RotateAroundAxis(ang:Right(), v.angle.pitch)
			        ang:RotateAroundAxis(ang:Up(), v.angle.yaw)
				    ang:RotateAroundAxis(ang:Forward(), v.angle.roll)
			  cmodel:SetAngles( ang )
			  local matrix = Matrix()
			        matrix:Scale( v.size )
			  cmodel:EnableMatrix( "RenderMultiply",matrix )
			  cmodel:SetParent( self )
			  if (v.material == "") then
					cmodel:SetMaterial("")
				elseif (cmodel:GetMaterial() != v.material) then
					cmodel:SetMaterial( v.material )
				end
				
				if (v.skin and v.skin != cmodel:GetSkin()) then
					cmodel:SetSkin(v.skin)
				end
				
				if (v.bodygroup) then
					for k, v in pairs( v.bodygroup ) do
						if (cmodel:GetBodygroup(k) != v) then
							cmodel:SetBodygroup(k, v)
						end
					end
				end
			  local tbl = v
			  self.SpawnedCModels[k] = { data = tbl[k],modelent = cmodel }
	end
end
function ENT:OnRemove()
         -- start destroying CModels
		 for k,v in pairs( self.SpawnedCModels ) do
		     if ( IsValid( v.modelent ) ) then
			      v.modelent:Remove()
			 end
		 end
end
end