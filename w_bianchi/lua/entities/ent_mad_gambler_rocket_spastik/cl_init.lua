ENT.Spawnable			= false
ENT.AdminSpawnable		= false

include("shared.lua")

language.Add("ent_mad_gambler_rocket_spastik", "Gambler Rocket")

/*---------------------------------------------------------
   Name: ENT:Initialize()
---------------------------------------------------------*/
function ENT:Initialize()
 
	self.TimeLeft = CurTime() + 3

	local vOffset 	= self.Entity:LocalToWorld(Vector(0, 0, self.Entity:OBBMins().z))
	local vNormal 	= (vOffset - self.Entity:GetPos()):GetNormalized()

	self.Emitter 	= ParticleEmitter(vOffset)

	// 150 particles per second during 30 seconds
	for i = 1, 10 do
		timer.Simple(i / 150, function()
			if not IsValid(self.Entity) then return end
			if ( self:GetNWBool( "dud" ) == true ) then return end -- we're dud, so no smoke/fire effects
			
			local vOffset 	= self.Entity:LocalToWorld(Vector(0, 0, self.Entity:OBBMins().z))
			local vNormal 	= (vOffset - self.Entity:GetPos()):GetNormalized()

			local particle = self.Emitter:Add("particle/particle_smokegrenade", vOffset)
			particle:SetVelocity(vNormal * 5)
			particle:SetDieTime(10)
			particle:SetStartAlpha(255)
			particle:SetStartSize(5)
			particle:SetEndSize(25)
			particle:SetRoll(math.Rand(-5, 5))
			particle:SetColor(160, 160, 160)
		end)
	end

end

function ENT:OnRemove()
         if ( self.Emitter ) then
		      self.Emitter:Finish()
		 end
end 

/*---------------------------------------------------------
   Name: ENT:Think()
---------------------------------------------------------*/
function ENT:Think()

    if ( self:GetNWBool( "dud" ) == true ) then return end -- we're dud, so no smoke/fire effects

end
