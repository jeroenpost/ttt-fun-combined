AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")

include("shared.lua")

local RocketSound = Sound("Missile.Accelerate")

/*---------------------------------------------------------
   Name: ENT:Initialize()
---------------------------------------------------------*/
function ENT:Initialize()

	self.Owner = self.Entity:GetOwner()

	if !IsValid(self.Owner) then
		self:Remove()
		return
	end
 
	self.Entity:SetModel("models/weapons/w_missile_closed.mdl")
	
	self.Entity:PhysicsInit(SOLID_VPHYSICS)
	self.Entity:SetMoveType(MOVETYPE_VPHYSICS)   
	self.Entity:SetSolid(SOLID_VPHYSICS)
	self.SpawnTime = CurTime()
 
	self.PhysObj = self.Entity:GetPhysicsObject()

	if (self.PhysObj:IsValid()) then
		self.PhysObj:EnableGravity(false)
		self.PhysObj:EnableDrag(false) 
		self.PhysObj:SetMass(30)
        	self.PhysObj:Wake()
	end
		
	self.Entity:EmitSound(RocketSound)
	util.PrecacheSound("explode_4")
		//Randomizer to create "dud" rockets now and then//

			self.TimeLeft = CurTime() + 3
			self:SetNWBool( "dud",false )


end

/*---------------------------------------------------------
   Name: ENT:Think()
---------------------------------------------------------*/
function ENT:Think()

	local phys 		= self.Entity:GetPhysicsObject()
	local ang 		= self.Entity:GetForward() * 7500
	local upang
	local rightang

	upang 	= self.Entity:GetUp() * math.Rand(500, 2000) * (math.sin(CurTime() * math.Rand(500, 1000)))
	rightang 	= self.Entity:GetRight() * math.Rand(500, 2000) * (math.cos(CurTime() * math.Rand(500, 1000)))

	local force

	if self.TimeLeft < CurTime() then
		phys:EnableGravity(true)
		phys:EnableDrag(true)
		self.Entity:StopSound(RocketSound)
	else
		if self.SpawnTime + 0.75 < CurTime() then
			force = ang + upang + rightang
		else
			force = ang
		end

		phys:ApplyForceCenter(force)
	end
end

/*---------------------------------------------------------
   Name: ENT:Explosion()
---------------------------------------------------------*/
function ENT:Explosion()


	local explo = ents.Create("env_explosion")
		explo:SetOwner(self.Owner)
		explo:SetPos(self.Entity:GetPos())
		explo:SetKeyValue("iMagnitude", "55")
		explo:Spawn()
		explo:Activate()
		explo:Fire("Explode", "", 0)
	



end

/*---------------------------------------------------------
   Name: ENT:PhysicsCollide()
---------------------------------------------------------*/
function ENT:PhysicsCollide(data, physobj) 


	self:Explosion()

	self.Entity:Remove()
end

/*---------------------------------------------------------
   Name: ENT:OnRemove()
---------------------------------------------------------*/
function ENT:OnRemove()

	self.Entity:StopSound(RocketSound)
end

