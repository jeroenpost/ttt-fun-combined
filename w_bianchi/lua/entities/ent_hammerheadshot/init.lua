AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")
include('shared.lua')

function ENT:Initialize()
	self:SetModel("models/crossbow_bolt.mdl")
	self:PhysicsInit(SOLID_VPHYSICS)
	self:SetMoveType(MOVETYPE_VPHYSICS)
	self:SetSolid(SOLID_VPHYSICS)
	local phys = self:GetPhysicsObject()

	if (phys:IsValid()) then
		phys:Wake()
	end
end

function ENT:Think()
		 local phys = self:GetPhysicsObject()
	     if (phys:IsValid() and !self.Sticked) then
		     for i = 1,125 do
		         phys:AddVelocity( self:GetForward() * 1200000 )
		     end
         end
		 self:NextThink( CurTime() + 0.01 )
		return true
end

function ENT:Touch(ent)
end

function ENT:PhysicsCollide()
        -- stick to surface.
	     self.Sticked = true
		 self:GetPhysicsObject():EnableMotion( false )
		-- local trace = util.TraceLine( { start = self:GetPos(),endpos = self:GetForward()*9001,filter = self } )
		 if self.Sticked then return else self:Remove() end
   	     local effectdata = EffectData() 
		 effectdata:SetOrigin( self:GetPos() ) 
		 --effectdata:SetNormal( trace.HitNormal ) 
		 effectdata:SetEntity( self ) 
		 effectdata:SetAttachment( trace.PhysicsBone ) 
		 util.Effect( "StunstickImpact", effectdata )
		 util.Effect( "GlassImpact", effectdata )
		 util.Effect( "GlassImpact", effectdata )
		 util.Effect( "ManhackSparks", effectdata )
end