//General Variables\\
SWEP.AdminSpawnable = true
SWEP.ViewModelFOV = 64

SWEP.VElements = {
	["saw"] = { type = "Model", model = "models/props_c17/signpole001.mdl", bone = "Crossbow_model.bolt", rel = "", pos = Vector(-0.101, 0, 26.818), angle = Angle(0, 0, 180), size = Vector(0.435, 0.435, 0.321), color = Color(255, 169, 0, 255), surpresslightning = false, material = "models/props_c17/metalwall085a", skin = 0, bodygroup = {} },
	["weight"] = { type = "Model", model = "models/props_junk/cinderblock01a.mdl", bone = "Crossbow_model.Crossbow_base", rel = "body", pos = Vector(-2, 1.363, 0), angle = Angle(0, 0, 90), size = Vector(0.2, 0.2, 0.2), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["wheel"] = { type = "Model", model = "models/props_c17/pulleywheels_small01.mdl", bone = "Crossbow_model.Crossbow_base", rel = "hframe++++", pos = Vector(0, -5, -0.456), angle = Angle(0, 0, 0), size = Vector(0.17, 0.17, 0.17), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["hframe++"] = { type = "Model", model = "models/gibs/metal_gib2.mdl", bone = "Crossbow_model.Crossbow_base", rel = "", pos = Vector(0, -0.32, 20.454), angle = Angle(0, 90, 180), size = Vector(1.003, 1.83, 1.003), color = Color(255, 255, 255, 255), surpresslightning = false, material = "models/props_c17/oil_drum_chunks001a", skin = 0, bodygroup = {} },
	["hframe+++"] = { type = "Model", model = "models/gibs/metal_gib2.mdl", bone = "Crossbow_model.Crossbow_base", rel = "hframe++", pos = Vector(0.001, 14.09, 4.091), angle = Angle(0, 180, 90), size = Vector(1.003, 0.606, 1.003), color = Color(255, 255, 255, 255), surpresslightning = false, material = "models/props_c17/oil_drum_chunks001a", skin = 0, bodygroup = {} },
	["hframe++++"] = { type = "Model", model = "models/gibs/metal_gib2.mdl", bone = "Crossbow_model.Crossbow_base", rel = "hframe++", pos = Vector(0.001, -14.091, 4.091), angle = Angle(0, 0, 90), size = Vector(1.003, 0.606, 1.003), color = Color(255, 255, 255, 255), surpresslightning = false, material = "models/props_c17/oil_drum_chunks001a", skin = 0, bodygroup = {} },
	["pull"] = { type = "Model", model = "models/props_c17/trappropeller_lever.mdl", bone = "Crossbow_model.pull", rel = "", pos = Vector(1.363, 0, 0), angle = Angle(-180, -90, 0), size = Vector(0.264, 0.264, 0.264), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["wheel+"] = { type = "Model", model = "models/props_c17/pulleywheels_small01.mdl", bone = "Crossbow_model.Crossbow_base", rel = "hframe+++", pos = Vector(0, -5, -0.456), angle = Angle(0, 0, 0), size = Vector(0.17, 0.17, 0.17), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["adj+"] = { type = "Model", model = "models/props_citizen_tech/firetrap_button01a.mdl", bone = "Crossbow_model.Crossbow_base", rel = "scope", pos = Vector(0, 1.1, 5), angle = Angle(0, -90, 0), size = Vector(0.209, 0.209, 0.209), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["scope"] = { type = "Model", model = "models/props_c17/oildrum001.mdl", bone = "Crossbow_model.Crossbow_base", rel = "body", pos = Vector(4.699, -13.94, 0), angle = Angle(0, 0, 90), size = Vector(0.079, 0.079, 0.239), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["body"] = { type = "Model", model = "models/props_junk/wood_crate002a.mdl", bone = "Crossbow_model.Crossbow_base", rel = "hframe++", pos = Vector(-0.5, 0.1, 11.8), angle = Angle(0, 0, 90), size = Vector(0.052, 0.321, 0.052), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["adj"] = { type = "Model", model = "models/props_citizen_tech/firetrap_button01a.mdl", bone = "Crossbow_model.Crossbow_base", rel = "scope", pos = Vector(1.2, 0, 5), angle = Angle(0, 0, 0), size = Vector(0.209, 0.209, 0.209), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
}
SWEP.WElements = {
	["adj+"] = { type = "Model", model = "models/props_citizen_tech/firetrap_button01a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "scope", pos = Vector(0, 1.1, 5), angle = Angle(0, -90, 0), size = Vector(0.209, 0.209, 0.209), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["weight"] = { type = "Model", model = "models/props_junk/cinderblock01a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "body", pos = Vector(2, 1.363, 0.1), angle = Angle(0, 0, 90), size = Vector(0.2, 0.2, 0.321), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["wheel"] = { type = "Model", model = "models/props_c17/pulleywheels_small01.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "hframe++++", pos = Vector(0, -5, 0.455), angle = Angle(0, 0, 0), size = Vector(0.17, 0.17, 0.17), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["hframe++"] = { type = "Model", model = "models/gibs/metal_gib2.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(25, -2.274, -3.182), angle = Angle(89, -3.069, 13.295), size = Vector(1.003, 1.83, 1.003), color = Color(255, 255, 255, 255), surpresslightning = false, material = "models/props_c17/oil_drum_chunks001a", skin = 0, bodygroup = {} },
	["hframe+++"] = { type = "Model", model = "models/gibs/metal_gib2.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "hframe++", pos = Vector(0.001, 14.09, 4.091), angle = Angle(0, 180, 90), size = Vector(1.003, 0.606, 1.003), color = Color(255, 255, 255, 255), surpresslightning = false, material = "models/props_c17/oil_drum_chunks001a", skin = 0, bodygroup = {} },
	["hframe++++"] = { type = "Model", model = "models/gibs/metal_gib2.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "hframe++", pos = Vector(0.001, -14.091, 4.091), angle = Angle(0, 0, 90), size = Vector(1.003, 0.606, 1.003), color = Color(255, 255, 255, 255), surpresslightning = false, material = "models/props_c17/oil_drum_chunks001a", skin = 0, bodygroup = {} },
	["scope"] = { type = "Model", model = "models/props_c17/oildrum001.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(0, 2.059, -7), angle = Angle(-90, 10, 0), size = Vector(0.079, 0.079, 0.239), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["wheel+"] = { type = "Model", model = "models/props_c17/pulleywheels_small01.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "hframe+++", pos = Vector(0, -5, 0.455), angle = Angle(0, 0, 0), size = Vector(0.17, 0.17, 0.17), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["element_name"] = { type = "Model", model = "", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(0, 0, 0), angle = Angle(0, 0, 0), size = Vector(0.5, 0.5, 0.5), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["saw"] = { type = "Model", model = "models/props_c17/signpole001.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(7.727, 0.699, -3.8), angle = Angle(-90.5, 9.699, 0), size = Vector(0.435, 0.435, 0.209), color = Color(237, 182, 0, 255), surpresslightning = false, material = "models/props_c17/metalwall085a", skin = 0, bodygroup = {} },
	["body"] = { type = "Model", model = "models/props_junk/wood_crate002a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "hframe++", pos = Vector(0.455, -0.071, 11.364), angle = Angle(0, 0, 90), size = Vector(0.052, 0.321, 0.052), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["adj"] = { type = "Model", model = "models/props_citizen_tech/firetrap_button01a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "scope", pos = Vector(1.2, 0, 5), angle = Angle(0, 0, 0), size = Vector(0.209, 0.209, 0.209), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["pull"] = { type = "Model", model = "models/props_c17/trappropeller_lever.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(5.908, 2.273, -3.5), angle = Angle(0, 15.34, 180), size = Vector(0.264, 0.264, 0.264), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
}

SWEP.AutoSwitchTo = false
SWEP.Slot = 0
SWEP.PrintName = "The Hammerhead"
SWEP.Author = "Spastik/BFG"
SWEP.Spawnable = true
SWEP.AutoSwitchFrom = false
SWEP.FiresUnderwater = false
SWEP.Weight = 5
SWEP.DrawCrosshair = true
SWEP.Category = "S&G Munitions"
SWEP.SlotPos = 3
SWEP.DrawAmmo = true
SWEP.ReloadSound = "weapons/smg1/smg1_reload.wav"
SWEP.Instructions = "Pull the trigger to....well, this isn't a shark, so you figure out what to do."
SWEP.Contact = ""
SWEP.Purpose = "For confusing your enemies into thinking that you're carrying a shark up until a long rusted rod is plunged through their abdomen."
SWEP.Base = "aa_base"
SWEP.HoldType = "crossbow"
SWEP.ViewModelFOV = 56.614173228346
SWEP.ViewModelFlip = false
SWEP.ViewModel = "models/weapons/v_crossbow.mdl"
SWEP.WorldModel = "models/weapons/w_crossbow.mdl"
SWEP.ShowViewModel = true
SWEP.ShowWorldModel = true
SWEP.ViewModelBoneMods = {
	["Crossbow_model.pull"] = { scale = Vector(0.381, 0.381, 0.381), pos = Vector(0, 0, 0), angle = Angle(0, 0, 0) },
	["Crossbow_model.Base"] = { scale = Vector(1, 1, 1), pos = Vector(0, -2.54, 0), angle = Angle(0, 0, 0) },
	["Crossbow_model.bolt"] = { scale = Vector(0.009, 0.009, 0.009), pos = Vector(0, 0, 0), angle = Angle(0, 0, 0) },
	["Crossbow_model.bowl_wheel"] = { scale = Vector(0.009, 0.009, 0.009), pos = Vector(0, 0, 0), angle = Angle(0, 0, 0) }
}

//SWEP.TracerFreq			= 1
//SWEP.TracerType                 	= "AirboatGunHeavyTracer"

//General Variables\\

//Primary Fire Variables\\

SWEP.Primary.Damage = 200
SWEP.Primary.TakeAmmo = 1
SWEP.Primary.ClipSize = 1
SWEP.Primary.Ammo = "Xbowbolt"
SWEP.Primary.DefaultClip = 20
SWEP.Primary.Spread = 0
SWEP.Primary.NumberofShots = 1
SWEP.Primary.Automatic = false
SWEP.Primary.Recoil = 5
SWEP.Primary.Delay = 1
SWEP.Primary.Force = 7
//Primary Fire Variables\\

//Secondary Fire Variables\\
SWEP.Secondary.Sound=Sound("weapons/sniper/sniper_zoomin.wav")
SWEP.Secondary.Ammo = "none"
Zoom=0
//Secondary Fire Variables\\


//SWEP:PrimaryFire()\\
function SWEP:PrimaryAttack()
	if ( !self:CanPrimaryAttack() ) then return end
	local bullet = {}
		bullet.Num = self.Primary.NumberofShots
		bullet.Src = self.Owner:GetShootPos()
		bullet.Dir = self.Owner:GetAimVector()
		bullet.Spread = Vector( self.Primary.Spread * 0.1 , self.Primary.Spread * 0.1, 0)
		bullet.Tracer = 1
		bullet.TracerName = "AirboatGunHeavyTracer"
		bullet.Force = self.Primary.Force
		bullet.Damage = self.Primary.Damage
		bullet.AmmoType = self.Primary.Ammo
	local rnda = self.Primary.Recoil * -1
	local rndb = self.Primary.Recoil * math.random(-1, 1)
	self:ShootEffects()
	self.Owner:FireBullets( bullet )
	self.Weapon:EmitSound("weapons/crossbow/fire1.wav", 500,95, 1, 0)
	self.Owner:ViewPunch( Angle( rnda,rndb,rnda ) )
	self:TakePrimaryAmmo(self.Primary.TakeAmmo)
	self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
	self.Weapon:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
	
	local trace = self.Owner:GetEyeTrace()
		if trace.HitSky then return end
			//EmitSound("Weapon_Crossbow.BoltHitWorld", trace.Hitpos)
			sound.Play("Weapon_Crossbow.BoltHitWorld", trace.HitPos, 200, 100)

		if trace.HitWorld then --this is neccessary, because the bolt will float in mid air if this is not done
   			local effectdata = EffectData() 
			effectdata:SetOrigin( trace.HitPos ) 
			effectdata:SetNormal( trace.HitNormal ) 
			effectdata:SetEntity( trace.Entity ) 
			effectdata:SetAttachment( trace.PhysicsBone ) 
			util.Effect( "Boltimpact", effectdata )
			util.Effect( "StunstickImpact", effectdata )
			util.Effect( "GlassImpact", effectdata )
			util.Effect( "GlassImpact", effectdata )
			util.Effect( "ManhackSparks", effectdata )
		else
			local effectdata = EffectData() 
			effectdata:SetOrigin( trace.HitPos ) 
			effectdata:SetNormal( trace.HitNormal ) 
			effectdata:SetEntity( trace.Entity ) 
			effectdata:SetAttachment( trace.PhysicsBone ) 
			util.Effect( "StunstickImpact", effectdata )
			util.Effect( "ManhackSparks", effectdata )
			util.Effect( "ManhackSparks", effectdata )
			util.Effect( "ManhackSparks", effectdata ) --Fuck you I like calling things multiple times
		end
end

//SWEP:PrimaryFire()\\

//SWEP:SecondaryFire()\\
//SWEP:SecondaryFire()\\
function SWEP:SecondaryAttack()
 	if !self.Zoomed then -- The player is not zoomed in.
		self.Zoomed = true -- Now he is
		if SERVER then
			self.Owner:SetFOV( 35, 0.3 ) -- SetFOV is serverside only
		end
	else
		self.Zoomed = false
		if SERVER then
			self.Owner:SetFOV( 0, 0.3 ) -- Setting to 0 resets the FOV
		end
	end
end

function SWEP:Reload()
	self.Weapon:DefaultReload( ACT_VM_RELOAD )
	// SAGA's edit
	if ( self:Clip1() == self.Primary.ClipSize or self:Ammo1() == 0 ) then return end -- this stops sound spam if we press reload with no ammo or full clip
	if self.Zoomed then
		self.Zoomed = false
		if SERVER then
			self.Owner:SetFOV( 0, 0.3 )
		end
	end
	self.Weapon:EmitSound( "weapons/crossbow/reload1.wav", 500, 100, 1, -1 )
	timer.Simple(0.9, function()
		if ( not IsValid( self ) ) then return end
		self.Weapon:EmitSound( "physics/wood/wood_crate_impact_soft"..math.random( 1,3 )..".wav", 500, 100, 1, 1 )
	end)
end

/*
/*-------------------
YO YO YO THIS IS FUCKING IMPORTANT
---------------------*/

function SWEP:GetTracerOrigin()

	local pos = self:GetOwner():EyePos()
	return pos

end

 --eh, didn't work

/********************************************************
	SWEP Construction Kit base code
		Created by Clavus
	Available for public use, thread at:
	   facepunch.com/threads/1032378
	   
	   
	DESCRIPTION:
		This script is meant for experienced scripters 
		that KNOW WHAT THEY ARE DOING. Don't come to me 
		with basic Lua questions.
		
		Just copy into your SWEP or SWEP base of choice
		and merge with your own code.
		
		The SWEP.VElements, SWEP.WElements and
		SWEP.ViewModelBoneMods tables are all optional
		and only have to be visible to the client.
********************************************************/

function SWEP:Initialize()

	// other initialize code goes here

	if CLIENT then

		self:SetHoldType( self.HoldType or "pistol" )
		// Create a new table for every weapon instance
		self.VElements = table.FullCopy( self.VElements )
		self.WElements = table.FullCopy( self.WElements )
		self.ViewModelBoneMods = table.FullCopy( self.ViewModelBoneMods )

		self:CreateModels(self.VElements) // create viewmodels
		self:CreateModels(self.WElements) // create worldmodels
		
		// init view model bone build function
		if IsValid(self.Owner) then
			local vm = self.Owner:GetViewModel()
			if IsValid(vm) then
				self:ResetBonePositions(vm)
				
				--[[// Init viewmodel visibility
				if (self.ShowViewModel == nil or self.ShowViewModel) then
					vm:SetColor(Color(255,255,255,255))
				else
					// we set the alpha to 1 instead of 0 because else ViewModelDrawn stops being called
					vm:SetColor(Color(255,255,255,1))
					// ^ stopped working in GMod 13 because you have to do Entity:SetRenderMode(1) for translucency to kick in
					// however for some reason the view model resets to render mode 0 every frame so we just apply a debug material to prevent it from drawing
					vm:SetMaterial("Debug/hsv")			
				end]]--
			end
		end
	end
end
function SWEP:Holster()
	
	if CLIENT and IsValid(self.Owner) then
		local vm = self.Owner:GetViewModel()
		if IsValid(vm) then
			self:ResetBonePositions(vm)
		end
	end
	
	return true
end

function SWEP:OnRemove()
	self:Holster()
end

if CLIENT then

	SWEP.vRenderOrder = nil
	function SWEP:ViewModelDrawn()
		
		local vm = self.Owner:GetViewModel()
		if !IsValid(vm) then return end
		
		if (!self.VElements) then return end
		
		self:UpdateBonePositions(vm)

		if (!self.vRenderOrder) then
			
			// we build a render order because sprites need to be drawn after models
			self.vRenderOrder = {}

			for k, v in pairs( self.VElements ) do
				if (v.type == "Model") then
					table.insert(self.vRenderOrder, 1, k)
				elseif (v.type == "Sprite" or v.type == "Quad") then
					table.insert(self.vRenderOrder, k)
				end
			end
			
		end

		for k, name in ipairs( self.vRenderOrder ) do
		
			local v = self.VElements[name]
			if (!v) then self.vRenderOrder = nil break end
			if (v.hide) then continue end
			
			local model = v.modelEnt
			local sprite = v.spriteMaterial
			
			if (!v.bone) then continue end
			
			local pos, ang = self:GetBoneOrientation( self.VElements, v, vm )
			
			if (!pos) then continue end
			
			if (v.type == "Model" and IsValid(model)) then

				model:SetPos(pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z )
				ang:RotateAroundAxis(ang:Up(), v.angle.y)
				ang:RotateAroundAxis(ang:Right(), v.angle.p)
				ang:RotateAroundAxis(ang:Forward(), v.angle.r)

				model:SetAngles(ang)
				//model:SetModelScale(v.size)
				local matrix = Matrix()
				matrix:Scale(v.size)
				model:EnableMatrix( "RenderMultiply", matrix )
				
				if (v.material == "") then
					model:SetMaterial("")
				elseif (model:GetMaterial() != v.material) then
					model:SetMaterial( v.material )
				end
				
				if (v.skin and v.skin != model:GetSkin()) then
					model:SetSkin(v.skin)
				end
				
				if (v.bodygroup) then
					for k, v in pairs( v.bodygroup ) do
						if (model:GetBodygroup(k) != v) then
							model:SetBodygroup(k, v)
						end
					end
				end
				
				if (v.surpresslightning) then
					render.SuppressEngineLighting(true)
				end
				
				render.SetColorModulation(v.color.r/255, v.color.g/255, v.color.b/255)
				render.SetBlend(v.color.a/255)
				model:DrawModel()
				render.SetBlend(1)
				render.SetColorModulation(1, 1, 1)
				
				if (v.surpresslightning) then
					render.SuppressEngineLighting(false)
				end
				
			elseif (v.type == "Sprite" and sprite) then
				
				local drawpos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
				render.SetMaterial(sprite)
				render.DrawSprite(drawpos, v.size.x, v.size.y, v.color)
				
			elseif (v.type == "Quad" and v.draw_func) then
				
				local drawpos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
				ang:RotateAroundAxis(ang:Up(), v.angle.y)
				ang:RotateAroundAxis(ang:Right(), v.angle.p)
				ang:RotateAroundAxis(ang:Forward(), v.angle.r)
				
				cam.Start3D2D(drawpos, ang, v.size)
					v.draw_func( self )
				cam.End3D2D()

			end
			
		end
		
	end

	SWEP.wRenderOrder = nil
	function SWEP:DrawWorldModel()
		
		if (self.ShowWorldModel == nil or self.ShowWorldModel) then
			self:DrawModel()
		end
		
		if (!self.WElements) then return end
		
		if (!self.wRenderOrder) then

			self.wRenderOrder = {}

			for k, v in pairs( self.WElements ) do
				if (v.type == "Model") then
					table.insert(self.wRenderOrder, 1, k)
				elseif (v.type == "Sprite" or v.type == "Quad") then
					table.insert(self.wRenderOrder, k)
				end
			end

		end
		
		if (IsValid(self.Owner)) then
			bone_ent = self.Owner
		else
			// when the weapon is dropped
			bone_ent = self
		end
		
		for k, name in pairs( self.wRenderOrder ) do
		
			local v = self.WElements[name]
			if (!v) then self.wRenderOrder = nil break end
			if (v.hide) then continue end
			
			local pos, ang
			
			if (v.bone) then
				pos, ang = self:GetBoneOrientation( self.WElements, v, bone_ent )
			else
				pos, ang = self:GetBoneOrientation( self.WElements, v, bone_ent, "ValveBiped.Bip01_R_Hand" )
			end
			
			if (!pos) then continue end
			
			local model = v.modelEnt
			local sprite = v.spriteMaterial
			
			if (v.type == "Model" and IsValid(model)) then

				model:SetPos(pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z )
				ang:RotateAroundAxis(ang:Up(), v.angle.y)
				ang:RotateAroundAxis(ang:Right(), v.angle.p)
				ang:RotateAroundAxis(ang:Forward(), v.angle.r)

				model:SetAngles(ang)
				//model:SetModelScale(v.size)
				local matrix = Matrix()
				matrix:Scale(v.size)
				model:EnableMatrix( "RenderMultiply", matrix )
				
				if (v.material == "") then
					model:SetMaterial("")
				elseif (model:GetMaterial() != v.material) then
					model:SetMaterial( v.material )
				end
				
				if (v.skin and v.skin != model:GetSkin()) then
					model:SetSkin(v.skin)
				end
				
				if (v.bodygroup) then
					for k, v in pairs( v.bodygroup ) do
						if (model:GetBodygroup(k) != v) then
							model:SetBodygroup(k, v)
						end
					end
				end
				
				if (v.surpresslightning) then
					render.SuppressEngineLighting(true)
				end
				
				render.SetColorModulation(v.color.r/255, v.color.g/255, v.color.b/255)
				render.SetBlend(v.color.a/255)
				model:DrawModel()
				render.SetBlend(1)
				render.SetColorModulation(1, 1, 1)
				
				if (v.surpresslightning) then
					render.SuppressEngineLighting(false)
				end
				
			elseif (v.type == "Sprite" and sprite) then
				
				local drawpos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
				render.SetMaterial(sprite)
				render.DrawSprite(drawpos, v.size.x, v.size.y, v.color)
				
			elseif (v.type == "Quad" and v.draw_func) then
				
				local drawpos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
				ang:RotateAroundAxis(ang:Up(), v.angle.y)
				ang:RotateAroundAxis(ang:Right(), v.angle.p)
				ang:RotateAroundAxis(ang:Forward(), v.angle.r)
				
				cam.Start3D2D(drawpos, ang, v.size)
					v.draw_func( self )
				cam.End3D2D()

			end
			
		end
		
	end

	function SWEP:GetBoneOrientation( basetab, tab, ent, bone_override )
		
		local bone, pos, ang
		if (tab.rel and tab.rel != "") then
			
			local v = basetab[tab.rel]
			
			if (!v) then return end
			
			// Technically, if there exists an element with the same name as a bone
			// you can get in an infinite loop. Let's just hope nobody's that stupid.
			pos, ang = self:GetBoneOrientation( basetab, v, ent )
			
			if (!pos) then return end
			
			pos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
			ang:RotateAroundAxis(ang:Up(), v.angle.y)
			ang:RotateAroundAxis(ang:Right(), v.angle.p)
			ang:RotateAroundAxis(ang:Forward(), v.angle.r)
				
		else
		
			bone = ent:LookupBone(bone_override or tab.bone)

			if (!bone) then return end
			
			pos, ang = Vector(0,0,0), Angle(0,0,0)
			local m = ent:GetBoneMatrix(bone)
			if (m) then
				pos, ang = m:GetTranslation(), m:GetAngles()
			end
			
			if (IsValid(self.Owner) and self.Owner:IsPlayer() and 
				ent == self.Owner:GetViewModel() and self.ViewModelFlip) then
				ang.r = -ang.r // Fixes mirrored models
			end
		
		end
		
		return pos, ang
	end

	function SWEP:CreateModels( tab )

		if (!tab) then return end

		// Create the clientside models here because Garry says we can't do it in the render hook
		for k, v in pairs( tab ) do
			if (v.type == "Model" and v.model and v.model != "" and (!IsValid(v.modelEnt) or v.createdModel != v.model) and 
					string.find(v.model, ".mdl") and file.Exists (v.model, "GAME") ) then
				
				v.modelEnt = ClientsideModel(v.model, RENDER_GROUP_VIEW_MODEL_OPAQUE)
				if (IsValid(v.modelEnt)) then
					v.modelEnt:SetPos(self:GetPos())
					v.modelEnt:SetAngles(self:GetAngles())
					v.modelEnt:SetParent(self)
					v.modelEnt:SetNoDraw(true)
					v.createdModel = v.model
				else
					v.modelEnt = nil
				end
				
			elseif (v.type == "Sprite" and v.sprite and v.sprite != "" and (!v.spriteMaterial or v.createdSprite != v.sprite) 
				and file.Exists ("materials/"..v.sprite..".vmt", "GAME")) then
				
				local name = v.sprite.."-"
				local params = { ["$basetexture"] = v.sprite }
				// make sure we create a unique name based on the selected options
				local tocheck = { "nocull", "additive", "vertexalpha", "vertexcolor", "ignorez" }
				for i, j in pairs( tocheck ) do
					if (v[j]) then
						params["$"..j] = 1
						name = name.."1"
					else
						name = name.."0"
					end
				end

				v.createdSprite = v.sprite
				v.spriteMaterial = CreateMaterial(name,"UnlitGeneric",params)
				
			end
		end
		
	end
	
	local allbones
	local hasGarryFixedBoneScalingYet = false

	function SWEP:UpdateBonePositions(vm)
		
		if self.ViewModelBoneMods then
			
			if (!vm:GetBoneCount()) then return end
			
			// !! WORKAROUND !! //
			// We need to check all model names :/
			local loopthrough = self.ViewModelBoneMods
			if (!hasGarryFixedBoneScalingYet) then
				allbones = {}
				for i=0, vm:GetBoneCount() do
					local bonename = vm:GetBoneName(i)
					if (self.ViewModelBoneMods[bonename]) then 
						allbones[bonename] = self.ViewModelBoneMods[bonename]
					else
						allbones[bonename] = { 
							scale = Vector(1,1,1),
							pos = Vector(0,0,0),
							angle = Angle(0,0,0)
						}
					end
				end
				
				loopthrough = allbones
			end
			// !! ----------- !! //
			
			for k, v in pairs( loopthrough ) do
				local bone = vm:LookupBone(k)
				if (!bone) then continue end
				
				// !! WORKAROUND !! //
				local s = Vector(v.scale.x,v.scale.y,v.scale.z)
				local p = Vector(v.pos.x,v.pos.y,v.pos.z)
				local ms = Vector(1,1,1)
				if (!hasGarryFixedBoneScalingYet) then
					local cur = vm:GetBoneParent(bone)
					while(cur >= 0) do
						local pscale = loopthrough[vm:GetBoneName(cur)].scale
						ms = ms * pscale
						cur = vm:GetBoneParent(cur)
					end
				end
				
				s = s * ms
				// !! ----------- !! //
				
				if vm:GetManipulateBoneScale(bone) != s then
					vm:ManipulateBoneScale( bone, s )
				end
				if vm:GetManipulateBoneAngles(bone) != v.angle then
					vm:ManipulateBoneAngles( bone, v.angle )
				end
				if vm:GetManipulateBonePosition(bone) != p then
					vm:ManipulateBonePosition( bone, p )
				end
			end
		else
			self:ResetBonePositions(vm)
		end
		   
	end
	 
	function SWEP:ResetBonePositions(vm)
		
		if (!vm:GetBoneCount()) then return end
		for i=0, vm:GetBoneCount() do
			vm:ManipulateBoneScale( i, Vector(1, 1, 1) )
			vm:ManipulateBoneAngles( i, Angle(0, 0, 0) )
			vm:ManipulateBonePosition( i, Vector(0, 0, 0) )
		end
		
	end

	/**************************
		Global utility code
	**************************/

	// Fully copies the table, meaning all tables inside this table are copied too and so on (normal table.Copy copies only their reference).
	// Does not copy entities of course, only copies their reference.
	// WARNING: do not use on tables that contain themselves somewhere down the line or you'll get an infinite loop
	function table.FullCopy( tab )

		if (!tab) then return nil end
		
		local res = {}
		for k, v in pairs( tab ) do
			if (type(v) == "table") then
				res[k] = table.FullCopy(v) // recursion ho!
			elseif (type(v) == "Vector") then
				res[k] = Vector(v.x, v.y, v.z)
			elseif (type(v) == "Angle") then
				res[k] = Angle(v.p, v.y, v.r)
			else
				res[k] = v
			end
		end
		
		return res
		
	end
	
end

