SWEP.HoldType = "melee"
SWEP.ViewModelFOV = 45
SWEP.ViewModelFlip = false
SWEP.ViewModel = "models/weapons/v_stunstick.mdl"
SWEP.WorldModel = "models/weapons/W_stunbaton.mdl"
SWEP.ShowViewModel = true
SWEP.ShowWorldModel = true
SWEP.ViewModelBoneMods = {
	["ValveBiped.Bip01"] = { scale = Vector(1, 1, 1), pos = Vector(0, 1.899, 2.986), angle = Angle(0, -4.888, -4.888) }
}
SWEP.VElements = {
	["prong+"] = { type = "Model", model = "models/props_c17/utilityconnecter006d.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "base3", pos = Vector(-0.385, 0, -5.77), angle = Angle(-82.212, 37.212, -37.213), size = Vector(0.129, 0.129, 0.129), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["bottom"] = { type = "Model", model = "models/props_junk/wood_spool01.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "Handle", pos = Vector(0, 0, 5.679), angle = Angle(0, 0, 0), size = Vector(0.039, 0.039, 0.039), color = Color(255, 255, 255, 255), surpresslightning = false, material = "Debug/env_cubemap_model_translucent_fountain", skin = 0, bodygroup = {} },
	["Handle"] = { type = "Model", model = "models/props_c17/oildrum001.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(3.099, 1.5, -1.894), angle = Angle(-2.131, 95.858, -4.261), size = Vector(0.059, 0.059, 0.119), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["prong++++"] = { type = "Model", model = "models/props_c17/utilityconnecter006d.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "base3", pos = Vector(-0.385, 0.384, -3.462), angle = Angle(-82.212, 0, 0), size = Vector(0.129, 0.129, 0.129), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["part"] = { type = "Model", model = "models/healthvial.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "Handle", pos = Vector(0, 0, -5), angle = Angle(0, 0, 0), size = Vector(0.5, 0.5, 0.5), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["bright"] = { type = "Model", model = "models/props_lab/lab_flourescentlight002b.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "battery+", pos = Vector(0, 2.273, 0.2), angle = Angle(0, 0, 0), size = Vector(0.094, 0.094, 0.094), color = Color(0, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["battery"] = { type = "Model", model = "models/Items/car_battery01.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "part", pos = Vector(0.384, -1.155, 3.461), angle = Angle(0.865, -9.52, -90.865), size = Vector(0.082, 0.082, 0.082), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["prong+++++"] = { type = "Model", model = "models/props_c17/utilityconnecter006d.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "base3", pos = Vector(-0.385, 0, -3.462), angle = Angle(-82.212, 37.212, -37.213), size = Vector(0.129, 0.129, 0.129), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["base2"] = { type = "Model", model = "models/props_lab/labpart.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "part", pos = Vector(0.2, 0.3, -1.155), angle = Angle(-0.866, -132.405, 90.864), size = Vector(0.225, 0.225, 0.225), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["base3"] = { type = "Model", model = "models/props_junk/PopCan01a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "base2", pos = Vector(0, 8.076, 0.15), angle = Angle(16.441, 180, 90), size = Vector(0.801, 0.819, 2.098), color = Color(255, 255, 255, 255), surpresslightning = false, material = "Debug/env_cubemap_model", skin = 0, bodygroup = {} },
	["prong+++"] = { type = "Model", model = "models/props_c17/utilityconnecter006d.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "base3", pos = Vector(-0.385, 0, -1.155), angle = Angle(-82.212, 37.212, -37.213), size = Vector(0.129, 0.129, 0.129), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["prong"] = { type = "Model", model = "models/props_c17/utilityconnecter006d.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "base3", pos = Vector(-0.385, 0.384, -5.77), angle = Angle(-82.212, 0, 0), size = Vector(0.129, 0.129, 0.129), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["battery+"] = { type = "Model", model = "models/Items/car_battery01.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "part", pos = Vector(0.384, -1.155, -1.923), angle = Angle(0.865, -9.52, -90.865), size = Vector(0.082, 0.082, 0.082), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["prong++"] = { type = "Model", model = "models/props_c17/utilityconnecter006d.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "base3", pos = Vector(-0.385, 0.384, -1.155), angle = Angle(-82.212, 0, 0), size = Vector(0.129, 0.129, 0.129), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
}
SWEP.WElements = {
	["prong+"] = { type = "Model", model = "models/props_c17/utilityconnecter006d.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "base3", pos = Vector(0.455, 0, -5), angle = Angle(-91.024, 0, 0), size = Vector(0.129, 0.129, 0.129), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["bottom"] = { type = "Model", model = "models/props_junk/wood_spool01.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "Handle", pos = Vector(0, 0, 5.908), angle = Angle(0, 0, 0), size = Vector(0.039, 0.039, 0.039), color = Color(255, 255, 255, 255), surpresslightning = false, material = "Debug/env_cubemap_model_translucent_fountain", skin = 0, bodygroup = {} },
	["Handle"] = { type = "Model", model = "models/props_c17/oildrum001.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(2.7, 1.6, -1.364), angle = Angle(13.295, 11.25, -9.205), size = Vector(0.059, 0.059, 0.119), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["prong++++"] = { type = "Model", model = "models/props_c17/utilityconnecter006d.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "base3", pos = Vector(0.455, 0, -2.274), angle = Angle(-91.024, 0, 0), size = Vector(0.129, 0.129, 0.129), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["bright"] = { type = "Model", model = "models/props_lab/lab_flourescentlight002b.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "battery+", pos = Vector(0, 2.273, 0.2), angle = Angle(95.113, 0, 0), size = Vector(0.094, 0.094, 0.094), color = Color(0, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["battery+"] = { type = "Model", model = "models/Items/car_battery01.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "part", pos = Vector(0.384, -1.155, -1.923), angle = Angle(0.865, -9.52, -90.865), size = Vector(0.082, 0.082, 0.082), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["battery"] = { type = "Model", model = "models/Items/car_battery01.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "part", pos = Vector(0.384, -1.155, 3.461), angle = Angle(0.865, -9.52, -90.865), size = Vector(0.082, 0.082, 0.082), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["prong+++++"] = { type = "Model", model = "models/props_c17/utilityconnecter006d.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "base3", pos = Vector(0.455, 0, 0.455), angle = Angle(-91.024, 0, 0), size = Vector(0.129, 0.129, 0.129), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["base2"] = { type = "Model", model = "models/props_lab/labpart.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "part", pos = Vector(0.2, 0.3, -1.155), angle = Angle(-0.866, -132.405, 90.864), size = Vector(0.225, 0.225, 0.225), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["base3"] = { type = "Model", model = "models/props_junk/PopCan01a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "base2", pos = Vector(0, 8.076, 0.15), angle = Angle(16.441, 180, 90), size = Vector(0.801, 0.819, 2.098), color = Color(255, 255, 255, 255), surpresslightning = false, material = "Debug/env_cubemap_model", skin = 0, bodygroup = {} },
	["prong+++"] = { type = "Model", model = "models/props_c17/utilityconnecter006d.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "base3", pos = Vector(0, 0.2, 0.455), angle = Angle(-91.024, -93.069, 0), size = Vector(0.129, 0.129, 0.129), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["prong"] = { type = "Model", model = "models/props_c17/utilityconnecter006d.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "base3", pos = Vector(0, 0.2, -5), angle = Angle(-91.024, -93.069, 0), size = Vector(0.129, 0.129, 0.129), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["part"] = { type = "Model", model = "models/healthvial.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "Handle", pos = Vector(0, 0, -5), angle = Angle(0, 0, 0), size = Vector(0.5, 0.5, 0.5), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["prong++"] = { type = "Model", model = "models/props_c17/utilityconnecter006d.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "base3", pos = Vector(0, 0.2, -2.274), angle = Angle(-91.024, -93.069, 0), size = Vector(0.129, 0.129, 0.129), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
}
SWEP.Purpose = "For feeling like you did when you hit that home run back in the summer of '06"
SWEP.AutoSwitchTo = true
SWEP.Contact = ""
SWEP.Author = "Spastik"
SWEP.FiresUnderwater = true
SWEP.Spawnable = true
SWEP.AdminSpawnable = true
SWEP.SlotPos = 0
SWEP.Instructions = "Legs spread, same width as the shoulders; body tight, then hit the ball like you're defeating the enemy. Here, the pinky finger is the key and then you just hit, hit, hit. Kakin! Bingo!"
SWEP.AutoSwitchFrom = false
SWEP.Base = "aa_base"
SWEP.Category = "S&G Munitions"
SWEP.DrawAmmo = false
SWEP.PrintName = "The Hometown Slugger"
SWEP.Primary.Recoil				= 0
SWEP.Primary.Damage				= 0
SWEP.Primary.NumShots			= 0
SWEP.Primary.Cone				= 0	
SWEP.Primary.ClipSize			= -1
SWEP.Primary.DefaultClip		= -1
SWEP.Primary.Automatic   		= false
SWEP.Primary.Ammo         		= "none"
SWEP.Secondary.Ammo = "none"

/*---------------------------------------------------------
---------------------------------------------------------*/

//SWEP:PrimaryFire()\\
function SWEP:PrimaryAttack()
self.Weapon:SendWeaponAnim(ACT_VM_MISSCENTER)
self.Weapon:SetNextPrimaryFire(CurTime() + .5)

local trace = self.Owner:GetEyeTrace()

if trace.HitPos:Distance(self.Owner:GetShootPos()) <= 75 then
	bullet = {}
	bullet.Num    = 1
	bullet.Src    = self.Owner:GetShootPos()
	bullet.Dir    = self.Owner:GetAimVector()
	bullet.Spread = Vector(0, 0, 0)
	bullet.Tracer = 0
	bullet.Force  = 5
	bullet.Damage = 55
        self.Owner:FireBullets(bullet)
        self.Owner:SetAnimation( PLAYER_ATTACK1 );
		self.Weapon:EmitSound("weapons/stunstick/stunstick_impact"..math.random(1,2)..".wav",500,math.random(90,110), 1, -1)
		self.Weapon:EmitSound("weapons/stunstick/stunstick_fleshhit"..math.random(1,2)..".wav",500,math.random(90,110), 1, 1)
		self.Weapon:EmitSound("physics/metal/metal_canister_impact_hard"..math.random(1,2)..".wav",500,math.random(90,100), 1, 0)
        if SERVER then
        local hitposent = ents.Create("info_target")
        local trace = self.Owner:GetEyeTrace()
        local hitpos = trace.HitPos

		local hiteffect = ents.Create("point_tesla")
        hiteffect:SetOwner(self:GetOwner())
		hiteffect:SetPos(hitpos)
	hiteffect:Spawn()
	hiteffect:SetKeyValue("beamcount_max",15)
	hiteffect:SetKeyValue("beamcount_min",10)
	hiteffect:SetKeyValue("interval_max",0.5)
	hiteffect:SetKeyValue("interval_min",0.1)
	hiteffect:SetKeyValue("lifetime_max",0.3)
	hiteffect:SetKeyValue("lifetime_min",0.3)
	hiteffect:SetKeyValue("m_Color",255,255,255)
	hiteffect:SetKeyValue("m_flRadius",50)
	hiteffect:SetKeyValue("m_SoundName","DoSpark")
	hiteffect:SetKeyValue("texture","sprites/physcannon_bluelight1b.vmt")
	hiteffect:SetKeyValue("thick_max",5)
	hiteffect:SetKeyValue("thick_max",4)
	hiteffect:Fire( "DoSpark","",0.01 )
	hiteffect:Fire( "DoSpark","",0.1 )
	hiteffect:Fire( "DoSpark","",0.2 )
	hiteffect:Fire( "DoSpark","",0.3 )
	hiteffect:Fire( "DoSpark","",0.4 )
	hiteffect:Fire("kill","",1.0)	
	end
else
	self.Weapon:EmitSound("weapons/stunstick/stunstick_swing"..math.random(1,2)..".wav", 500, math.random(90,110), 1, 0)
	self.Weapon:SendWeaponAnim(ACT_VM_MISSCENTER)
self.Owner:SetAnimation( PLAYER_ATTACK1 )
end

end
//SWEP:PrimaryFire()\\

//SWEP:SecondaryFire()\\
function SWEP:SecondaryAttack()
end
//SWEP:SecondaryFire()\\

function SWEP:Think() -- Called every frame
end

function shockEnt(ent, pos, life, radius)
	local b = ents.Create( "point_hurt" )
	b:SetKeyValue("targetname", "fier" ) 
	b:SetKeyValue("DamageRadius", radius )
	b:SetKeyValue("Damage", "100" )
	b:SetKeyValue("DamageDelay", "1" )
	b:SetKeyValue("DamageType", "256" )
	b:SetPos( pos )
	b:SetParent(ent)
	b:Spawn()
	b:Fire("turnon", "", 0)
	b:Fire("turnoff", "", life)
	b:Fire("kill", "", life)
end

function hitBoxes(ent, ent2)
if ent:IsValid() then
	local effectdata3 = EffectData()
	effectdata3:SetOrigin( ent:GetPos() )
	effectdata3:SetStart( ent2:GetPos() )
	effectdata3:SetMagnitude(10)
	effectdata3:SetEntity( ent )
	util.Effect( "TeslaHitBoxes", effectdata3)
end
end


/********************************************************
	SWEP Construction Kit base code
		Created by Clavus
	Available for public use, thread at:
	   facepunch.com/threads/1032378
	   
	   
	DESCRIPTION:
		This script is meant for experienced scripters 
		that KNOW WHAT THEY ARE DOING. Don't come to me 
		with basic Lua questions.
		
		Just copy into your SWEP or SWEP base of choice
		and merge with your own code.
		
		The SWEP.VElements, SWEP.WElements and
		SWEP.ViewModelBoneMods tables are all optional
		and only have to be visible to the client.
********************************************************/

function SWEP:Initialize()

	// other initialize code goes here

	if CLIENT then

		self:SetHoldType( self.HoldType or "pistol" )
		// Create a new table for every weapon instance
		self.VElements = table.FullCopy( self.VElements )
		self.WElements = table.FullCopy( self.WElements )
		self.ViewModelBoneMods = table.FullCopy( self.ViewModelBoneMods )

		self:CreateModels(self.VElements) // create viewmodels
		self:CreateModels(self.WElements) // create worldmodels
		
		// init view model bone build function
		if IsValid(self.Owner) then
			local vm = self.Owner:GetViewModel()
			if IsValid(vm) then
				self:ResetBonePositions(vm)
				
				--[[// Init viewmodel visibility
				if (self.ShowViewModel == nil or self.ShowViewModel) then
					vm:SetColor(Color(255,255,255,255))
				else
					// we set the alpha to 1 instead of 0 because else ViewModelDrawn stops being called
					vm:SetColor(Color(255,255,255,1))
					// ^ stopped working in GMod 13 because you have to do Entity:SetRenderMode(1) for translucency to kick in
					// however for some reason the view model resets to render mode 0 every frame so we just apply a debug material to prevent it from drawing
					vm:SetMaterial("Debug/hsv")			
				end]]--
			end
		end
	end
end
function SWEP:Holster()
	
	if CLIENT and IsValid(self.Owner) then
		local vm = self.Owner:GetViewModel()
		if IsValid(vm) then
			self:ResetBonePositions(vm)
		end
	end
	
	return true
end

function SWEP:OnRemove()
	self:Holster()
end

if CLIENT then

	SWEP.vRenderOrder = nil
	function SWEP:ViewModelDrawn()
		
		local vm = self.Owner:GetViewModel()
		if !IsValid(vm) then return end
		
		if (!self.VElements) then return end
		
		self:UpdateBonePositions(vm)

		if (!self.vRenderOrder) then
			
			// we build a render order because sprites need to be drawn after models
			self.vRenderOrder = {}

			for k, v in pairs( self.VElements ) do
				if (v.type == "Model") then
					table.insert(self.vRenderOrder, 1, k)
				elseif (v.type == "Sprite" or v.type == "Quad") then
					table.insert(self.vRenderOrder, k)
				end
			end
			
		end

		for k, name in ipairs( self.vRenderOrder ) do
		
			local v = self.VElements[name]
			if (!v) then self.vRenderOrder = nil break end
			if (v.hide) then continue end
			
			local model = v.modelEnt
			local sprite = v.spriteMaterial
			
			if (!v.bone) then continue end
			
			local pos, ang = self:GetBoneOrientation( self.VElements, v, vm )
			
			if (!pos) then continue end
			
			if (v.type == "Model" and IsValid(model)) then

				model:SetPos(pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z )
				ang:RotateAroundAxis(ang:Up(), v.angle.y)
				ang:RotateAroundAxis(ang:Right(), v.angle.p)
				ang:RotateAroundAxis(ang:Forward(), v.angle.r)

				model:SetAngles(ang)
				//model:SetModelScale(v.size)
				local matrix = Matrix()
				matrix:Scale(v.size)
				model:EnableMatrix( "RenderMultiply", matrix )
				
				if (v.material == "") then
					model:SetMaterial("")
				elseif (model:GetMaterial() != v.material) then
					model:SetMaterial( v.material )
				end
				
				if (v.skin and v.skin != model:GetSkin()) then
					model:SetSkin(v.skin)
				end
				
				if (v.bodygroup) then
					for k, v in pairs( v.bodygroup ) do
						if (model:GetBodygroup(k) != v) then
							model:SetBodygroup(k, v)
						end
					end
				end
				
				if (v.surpresslightning) then
					render.SuppressEngineLighting(true)
				end
				
				render.SetColorModulation(v.color.r/255, v.color.g/255, v.color.b/255)
				render.SetBlend(v.color.a/255)
				model:DrawModel()
				render.SetBlend(1)
				render.SetColorModulation(1, 1, 1)
				
				if (v.surpresslightning) then
					render.SuppressEngineLighting(false)
				end
				
			elseif (v.type == "Sprite" and sprite) then
				
				local drawpos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
				render.SetMaterial(sprite)
				render.DrawSprite(drawpos, v.size.x, v.size.y, v.color)
				
			elseif (v.type == "Quad" and v.draw_func) then
				
				local drawpos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
				ang:RotateAroundAxis(ang:Up(), v.angle.y)
				ang:RotateAroundAxis(ang:Right(), v.angle.p)
				ang:RotateAroundAxis(ang:Forward(), v.angle.r)
				
				cam.Start3D2D(drawpos, ang, v.size)
					v.draw_func( self )
				cam.End3D2D()

			end
			
		end
		
	end

	SWEP.wRenderOrder = nil
	function SWEP:DrawWorldModel()
		
		if (self.ShowWorldModel == nil or self.ShowWorldModel) then
			self:DrawModel()
		end
		
		if (!self.WElements) then return end
		
		if (!self.wRenderOrder) then

			self.wRenderOrder = {}

			for k, v in pairs( self.WElements ) do
				if (v.type == "Model") then
					table.insert(self.wRenderOrder, 1, k)
				elseif (v.type == "Sprite" or v.type == "Quad") then
					table.insert(self.wRenderOrder, k)
				end
			end

		end
		
		if (IsValid(self.Owner)) then
			bone_ent = self.Owner
		else
			// when the weapon is dropped
			bone_ent = self
		end
		
		for k, name in pairs( self.wRenderOrder ) do
		
			local v = self.WElements[name]
			if (!v) then self.wRenderOrder = nil break end
			if (v.hide) then continue end
			
			local pos, ang
			
			if (v.bone) then
				pos, ang = self:GetBoneOrientation( self.WElements, v, bone_ent )
			else
				pos, ang = self:GetBoneOrientation( self.WElements, v, bone_ent, "ValveBiped.Bip01_R_Hand" )
			end
			
			if (!pos) then continue end
			
			local model = v.modelEnt
			local sprite = v.spriteMaterial
			
			if (v.type == "Model" and IsValid(model)) then

				model:SetPos(pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z )
				ang:RotateAroundAxis(ang:Up(), v.angle.y)
				ang:RotateAroundAxis(ang:Right(), v.angle.p)
				ang:RotateAroundAxis(ang:Forward(), v.angle.r)

				model:SetAngles(ang)
				//model:SetModelScale(v.size)
				local matrix = Matrix()
				matrix:Scale(v.size)
				model:EnableMatrix( "RenderMultiply", matrix )
				
				if (v.material == "") then
					model:SetMaterial("")
				elseif (model:GetMaterial() != v.material) then
					model:SetMaterial( v.material )
				end
				
				if (v.skin and v.skin != model:GetSkin()) then
					model:SetSkin(v.skin)
				end
				
				if (v.bodygroup) then
					for k, v in pairs( v.bodygroup ) do
						if (model:GetBodygroup(k) != v) then
							model:SetBodygroup(k, v)
						end
					end
				end
				
				if (v.surpresslightning) then
					render.SuppressEngineLighting(true)
				end
				
				render.SetColorModulation(v.color.r/255, v.color.g/255, v.color.b/255)
				render.SetBlend(v.color.a/255)
				model:DrawModel()
				render.SetBlend(1)
				render.SetColorModulation(1, 1, 1)
				
				if (v.surpresslightning) then
					render.SuppressEngineLighting(false)
				end
				
			elseif (v.type == "Sprite" and sprite) then
				
				local drawpos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
				render.SetMaterial(sprite)
				render.DrawSprite(drawpos, v.size.x, v.size.y, v.color)
				
			elseif (v.type == "Quad" and v.draw_func) then
				
				local drawpos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
				ang:RotateAroundAxis(ang:Up(), v.angle.y)
				ang:RotateAroundAxis(ang:Right(), v.angle.p)
				ang:RotateAroundAxis(ang:Forward(), v.angle.r)
				
				cam.Start3D2D(drawpos, ang, v.size)
					v.draw_func( self )
				cam.End3D2D()

			end
			
		end
		
	end

	function SWEP:GetBoneOrientation( basetab, tab, ent, bone_override )
		
		local bone, pos, ang
		if (tab.rel and tab.rel != "") then
			
			local v = basetab[tab.rel]
			
			if (!v) then return end
			
			// Technically, if there exists an element with the same name as a bone
			// you can get in an infinite loop. Let's just hope nobody's that stupid.
			pos, ang = self:GetBoneOrientation( basetab, v, ent )
			
			if (!pos) then return end
			
			pos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
			ang:RotateAroundAxis(ang:Up(), v.angle.y)
			ang:RotateAroundAxis(ang:Right(), v.angle.p)
			ang:RotateAroundAxis(ang:Forward(), v.angle.r)
				
		else
		
			bone = ent:LookupBone(bone_override or tab.bone)

			if (!bone) then return end
			
			pos, ang = Vector(0,0,0), Angle(0,0,0)
			local m = ent:GetBoneMatrix(bone)
			if (m) then
				pos, ang = m:GetTranslation(), m:GetAngles()
			end
			
			if (IsValid(self.Owner) and self.Owner:IsPlayer() and 
				ent == self.Owner:GetViewModel() and self.ViewModelFlip) then
				ang.r = -ang.r // Fixes mirrored models
			end
		
		end
		
		return pos, ang
	end

	function SWEP:CreateModels( tab )

		if (!tab) then return end

		// Create the clientside models here because Garry says we can't do it in the render hook
		for k, v in pairs( tab ) do
			if (v.type == "Model" and v.model and v.model != "" and (!IsValid(v.modelEnt) or v.createdModel != v.model) and 
					string.find(v.model, ".mdl") and file.Exists (v.model, "GAME") ) then
				
				v.modelEnt = ClientsideModel(v.model, RENDER_GROUP_VIEW_MODEL_OPAQUE)
				if (IsValid(v.modelEnt)) then
					v.modelEnt:SetPos(self:GetPos())
					v.modelEnt:SetAngles(self:GetAngles())
					v.modelEnt:SetParent(self)
					v.modelEnt:SetNoDraw(true)
					v.createdModel = v.model
				else
					v.modelEnt = nil
				end
				
			elseif (v.type == "Sprite" and v.sprite and v.sprite != "" and (!v.spriteMaterial or v.createdSprite != v.sprite) 
				and file.Exists ("materials/"..v.sprite..".vmt", "GAME")) then
				
				local name = v.sprite.."-"
				local params = { ["$basetexture"] = v.sprite }
				// make sure we create a unique name based on the selected options
				local tocheck = { "nocull", "additive", "vertexalpha", "vertexcolor", "ignorez" }
				for i, j in pairs( tocheck ) do
					if (v[j]) then
						params["$"..j] = 1
						name = name.."1"
					else
						name = name.."0"
					end
				end

				v.createdSprite = v.sprite
				v.spriteMaterial = CreateMaterial(name,"UnlitGeneric",params)
				
			end
		end
		
	end
	
	local allbones
	local hasGarryFixedBoneScalingYet = false

	function SWEP:UpdateBonePositions(vm)
		
		if self.ViewModelBoneMods then
			
			if (!vm:GetBoneCount()) then return end
			
			// !! WORKAROUND !! //
			// We need to check all model names :/
			local loopthrough = self.ViewModelBoneMods
			if (!hasGarryFixedBoneScalingYet) then
				allbones = {}
				for i=0, vm:GetBoneCount() do
					local bonename = vm:GetBoneName(i)
					if (self.ViewModelBoneMods[bonename]) then 
						allbones[bonename] = self.ViewModelBoneMods[bonename]
					else
						allbones[bonename] = { 
							scale = Vector(1,1,1),
							pos = Vector(0,0,0),
							angle = Angle(0,0,0)
						}
					end
				end
				
				loopthrough = allbones
			end
			// !! ----------- !! //
			
			for k, v in pairs( loopthrough ) do
				local bone = vm:LookupBone(k)
				if (!bone) then continue end
				
				// !! WORKAROUND !! //
				local s = Vector(v.scale.x,v.scale.y,v.scale.z)
				local p = Vector(v.pos.x,v.pos.y,v.pos.z)
				local ms = Vector(1,1,1)
				if (!hasGarryFixedBoneScalingYet) then
					local cur = vm:GetBoneParent(bone)
					while(cur >= 0) do
						local pscale = loopthrough[vm:GetBoneName(cur)].scale
						ms = ms * pscale
						cur = vm:GetBoneParent(cur)
					end
				end
				
				s = s * ms
				// !! ----------- !! //
				
				if vm:GetManipulateBoneScale(bone) != s then
					vm:ManipulateBoneScale( bone, s )
				end
				if vm:GetManipulateBoneAngles(bone) != v.angle then
					vm:ManipulateBoneAngles( bone, v.angle )
				end
				if vm:GetManipulateBonePosition(bone) != p then
					vm:ManipulateBonePosition( bone, p )
				end
			end
		else
			self:ResetBonePositions(vm)
		end
		   
	end
	 
	function SWEP:ResetBonePositions(vm)
		
		if (!vm:GetBoneCount()) then return end
		for i=0, vm:GetBoneCount() do
			vm:ManipulateBoneScale( i, Vector(1, 1, 1) )
			vm:ManipulateBoneAngles( i, Angle(0, 0, 0) )
			vm:ManipulateBonePosition( i, Vector(0, 0, 0) )
		end
		
	end

	/**************************
		Global utility code
	**************************/

	// Fully copies the table, meaning all tables inside this table are copied too and so on (normal table.Copy copies only their reference).
	// Does not copy entities of course, only copies their reference.
	// WARNING: do not use on tables that contain themselves somewhere down the line or you'll get an infinite loop
	function table.FullCopy( tab )

		if (!tab) then return nil end
		
		local res = {}
		for k, v in pairs( tab ) do
			if (type(v) == "table") then
				res[k] = table.FullCopy(v) // recursion ho!
			elseif (type(v) == "Vector") then
				res[k] = Vector(v.x, v.y, v.z)
			elseif (type(v) == "Angle") then
				res[k] = Angle(v.p, v.y, v.r)
			else
				res[k] = v
			end
		end
		
		return res
		
	end
	
end

