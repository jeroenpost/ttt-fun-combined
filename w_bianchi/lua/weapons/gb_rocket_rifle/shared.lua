//General Variables\\
SWEP.AdminSpawnable = true
SWEP.ViewModelFOV = 64

SWEP.VElements = {
	["handle"] = { type = "Model", model = "models/props_junk/propane_tank001a.mdl", bone = "ValveBiped.base", rel = "bodyfront", pos = Vector(0, 3.7, 3.181), angle = Angle(0, 0, -90), size = Vector(0.209, 0.209, 0.209), color = Color(255, 255, 255, 255), surpresslightning = false, material = "models/props_junk/trafficcone001a", skin = 0, bodygroup = {} },
	["railcover"] = { type = "Model", model = "models/props_interiors/refrigeratorDoor01a.mdl", bone = "ValveBiped.base", rel = "body inner", pos = Vector(-2, 0, -6.818), angle = Angle(0, 0, 0), size = Vector(0.094, 0.037, 0.094), color = Color(255, 255, 255, 255), surpresslightning = false, material = "models/items/flare", skin = 0, bodygroup = {} },
	["railcoverback"] = { type = "Model", model = "models/props_c17/playgroundTick-tack-toe_block01a.mdl", bone = "ValveBiped.base", rel = "body inner", pos = Vector(-1.471, 0, 1), angle = Angle(0, 90, -90), size = Vector(0.1, 0.379, 0.1), color = Color(255, 255, 254, 255), surpresslightning = false, material = "models/items/car_battery01", skin = 0, bodygroup = {} },
	["body innerside"] = { type = "Model", model = "models/props_wasteland/barricade002a.mdl", bone = "ValveBiped.base", rel = "body inner", pos = Vector(0, 0.879, 0), angle = Angle(-90, -180, 0), size = Vector(0.086, 0.009, 0.035), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["body inner"] = { type = "Model", model = "models/props_wasteland/controlroom_filecabinet001a.mdl", bone = "ValveBiped.base", rel = "bodyhalf1", pos = Vector(0, 0.639, 1.236), angle = Angle(-90, 0, 0), size = Vector(0.138, 0.086, 0.189), color = Color(255, 154, 143, 255), surpresslightning = false, material = "models/props_lab/chess_sheet", skin = 0, bodygroup = {} },
	["barrel front"] = { type = "Model", model = "models/props_junk/garbage_coffeemug001a.mdl", bone = "ValveBiped.muzzle", rel = "bodyfront", pos = Vector(0, 0.455, 5.908), angle = Angle(0, 0, -180), size = Vector(0.662, 0.662, 1.57), color = Color(255, 255, 255, 255), surpresslightning = false, material = "models/items/flare", skin = 0, bodygroup = {} },
	["barrel bracer"] = { type = "Model", model = "models/props_interiors/Furniture_Couch02a.mdl", bone = "ValveBiped.base", rel = "barrel front", pos = Vector(-0.9, 0.5, 3.181), angle = Angle(0, 0, 0), size = Vector(0.07, 0.07, 0.379), color = Color(255, 255, 255, 255), surpresslightning = false, material = "models/props_trainstation/trainstation_post001", skin = 0, bodygroup = {} },
	["sight"] = { type = "Model", model = "models/props_c17/pulleywheels_large01.mdl", bone = "ValveBiped.base", rel = "barrel front", pos = Vector(0, 2.92, -3.182), angle = Angle(90, 48.068, 0), size = Vector(0.037, 0.037, 0.037), color = Color(120, 121, 118, 255), surpresslightning = false, material = "models/props_trainstation/clocklarge01", skin = 0, bodygroup = {} },
	["stock"] = { type = "Model", model = "models/props_junk/garbage_plasticbottle002a.mdl", bone = "ValveBiped.base", rel = "body inner", pos = Vector(0, 0, 7.727), angle = Angle(-180, -90, 0), size = Vector(0.492, 0.492, 0.776), color = Color(255, 255, 255, 255), surpresslightning = false, material = "models/props_lab/lab_objects03", skin = 0, bodygroup = {} },
	["bodyhalf1"] = { type = "Model", model = "models/props_c17/display_cooler01a.mdl", bone = "ValveBiped.base", rel = "", pos = Vector(-0.601, 1.236, -5.2), angle = Angle(-90, 0, -90), size = Vector(0.059, 0.035, 0.059), color = Color(255, 255, 255, 255), surpresslightning = false, material = "models/props_lab/recievers01_off", skin = 0, bodygroup = {} },
	["body innerside+"] = { type = "Model", model = "models/props_wasteland/barricade002a.mdl", bone = "ValveBiped.base", rel = "body inner", pos = Vector(0, -0.9, 0), angle = Angle(90, 0, 0), size = Vector(0.086, 0.009, 0.035), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["barrel"] = { type = "Model", model = "models/weapons/w_missile_closed.mdl", bone = "ValveBiped.muzzle", rel = "barrel front", pos = Vector(0, 0.455, -2.274), angle = Angle(-90, 0, 0), size = Vector(0.5, 0.5, 0.5), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["barrel bracer+"] = { type = "Model", model = "models/props_interiors/Furniture_Couch02a.mdl", bone = "ValveBiped.base", rel = "barrel front", pos = Vector(0.899, 0.5, 3.181), angle = Angle(0, 180, 0), size = Vector(0.07, 0.07, 0.379), color = Color(255, 255, 255, 255), surpresslightning = false, material = "models/props_trainstation/trainstation_post001", skin = 0, bodygroup = {} },
	["bodyhalf1+"] = { type = "Model", model = "models/props_c17/display_cooler01a.mdl", bone = "ValveBiped.base", rel = "bodyhalf1", pos = Vector(0, 1.236, 0), angle = Angle(0, 180, 0), size = Vector(0.059, 0.035, 0.059), color = Color(255, 255, 255, 255), surpresslightning = false, material = "models/props_lab/recievers01_off", skin = 0, bodygroup = {} },
	["bodymain"] = { type = "Model", model = "models/props_c17/FurnitureWashingmachine001a.mdl", bone = "ValveBiped.Bip01", rel = "body inner", pos = Vector(-0.456, 0, -5.909), angle = Angle(0, 0, 0), size = Vector(0.094, 0.061, 0.209), color = Color(255, 255, 255, 255), surpresslightning = false, material = "models/props_lab/powerbox001", skin = 0, bodygroup = {} },
	["rearsight"] = { type = "Model", model = "models/props_interiors/refrigeratorDoor02a.mdl", bone = "ValveBiped.base", rel = "body inner", pos = Vector(-3.182, 0, -0.456), angle = Angle(-180, 0, 180), size = Vector(0.037, 0.037, 0.094), color = Color(255, 255, 255, 255), surpresslightning = false, material = "models/items/flare", skin = 0, bodygroup = {} },
	["bodyfront"] = { type = "Model", model = "models/props_junk/gascan001a.mdl", bone = "ValveBiped.base", rel = "bodymain", pos = Vector(0, 0, -3.182), angle = Angle(180, 90, 0), size = Vector(0.239, 0.151, 0.379), color = Color(255, 255, 255, 255), surpresslightning = false, material = "models/props_trainstation/tracksigns01a", skin = 0, bodygroup = {} }
}
SWEP.WElements = {
	["bodyhalf1+"] = { type = "Model", model = "models/props_c17/display_cooler01a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "bodyhalf1", pos = Vector(0, 1.236, 0), angle = Angle(0, 180, 0), size = Vector(0.059, 0.035, 0.059), color = Color(255, 255, 255, 255), surpresslightning = false, material = "models/props_lab/recievers01_off", skin = 0, bodygroup = {} },
	["railcover"] = { type = "Model", model = "models/props_interiors/refrigeratorDoor01a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "bodymain", pos = Vector(-1.5, 0, 0), angle = Angle(0, 0, 0), size = Vector(0.094, 0.037, 0.151), color = Color(255, 255, 255, 255), surpresslightning = false, material = "models/items/flare", skin = 0, bodygroup = {} },
	["railcoverback"] = { type = "Model", model = "models/props_c17/playgroundTick-tack-toe_block01a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "body inner", pos = Vector(-1.471, 0, 1), angle = Angle(0, 90, -90), size = Vector(0.1, 0.379, 0.1), color = Color(255, 255, 254, 255), surpresslightning = false, material = "models/items/car_battery01", skin = 0, bodygroup = {} },
	["body innerside"] = { type = "Model", model = "models/props_wasteland/barricade002a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "body inner", pos = Vector(0, 0.879, 0), angle = Angle(-90, -180, 0), size = Vector(0.086, 0.009, 0.035), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["body inner"] = { type = "Model", model = "models/props_wasteland/controlroom_filecabinet001a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "bodyhalf1", pos = Vector(0, 0.639, 1.236), angle = Angle(-90, 0, 0), size = Vector(0.138, 0.086, 0.189), color = Color(255, 154, 143, 255), surpresslightning = false, material = "models/props_lab/chess_sheet", skin = 0, bodygroup = {} },
	["barrel front"] = { type = "Model", model = "models/props_junk/garbage_coffeemug001a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "bodyfront", pos = Vector(0, 0.455, 5.908), angle = Angle(0, 0, -180), size = Vector(0.662, 0.662, 1.57), color = Color(255, 255, 255, 255), surpresslightning = false, material = "models/items/flare", skin = 0, bodygroup = {} },
	["barrel bracer"] = { type = "Model", model = "models/props_interiors/Furniture_Couch02a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "barrel front", pos = Vector(-0.9, 0.5, 3.181), angle = Angle(0, 0, 0), size = Vector(0.07, 0.07, 0.379), color = Color(255, 255, 255, 255), surpresslightning = false, material = "models/props_trainstation/trainstation_post001", skin = 0, bodygroup = {} },
	["sight"] = { type = "Model", model = "models/props_c17/pulleywheels_large01.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "barrel front", pos = Vector(0, 2.92, -3.182), angle = Angle(90, 48.068, 0), size = Vector(0.037, 0.037, 0.037), color = Color(120, 121, 118, 255), surpresslightning = false, material = "models/props_trainstation/clocklarge01", skin = 0, bodygroup = {} },
	["stock"] = { type = "Model", model = "models/props_junk/garbage_plasticbottle002a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "body inner", pos = Vector(0, 0, 6.817), angle = Angle(-180, -90, 0), size = Vector(0.549, 0.549, 0.606), color = Color(255, 255, 255, 255), surpresslightning = false, material = "models/props_lab/lab_objects03", skin = 0, bodygroup = {} },
	["bodyhalf1"] = { type = "Model", model = "models/props_c17/display_cooler01a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(2, 0.699, -3.3), angle = Angle(170.794, 0, 3.068), size = Vector(0.059, 0.035, 0.059), color = Color(255, 255, 255, 255), surpresslightning = false, material = "models/props_lab/recievers01_off", skin = 0, bodygroup = {} },
	["body innerside+"] = { type = "Model", model = "models/props_wasteland/barricade002a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "body inner", pos = Vector(0, -0.9, 0), angle = Angle(90, 0, 0), size = Vector(0.086, 0.009, 0.035), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["barrel"] = { type = "Model", model = "models/weapons/w_missile_closed.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "barrel front", pos = Vector(0, 0.455, -2.274), angle = Angle(-90, 0, 0), size = Vector(0.5, 0.5, 0.5), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["barrel bracer+"] = { type = "Model", model = "models/props_interiors/Furniture_Couch02a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "barrel front", pos = Vector(0.899, 0.5, 3.181), angle = Angle(0, 180, 0), size = Vector(0.07, 0.07, 0.379), color = Color(255, 255, 255, 255), surpresslightning = false, material = "models/props_trainstation/trainstation_post001", skin = 0, bodygroup = {} },
	["bodyfront"] = { type = "Model", model = "models/props_junk/gascan001a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "bodymain", pos = Vector(0, 0, -3.182), angle = Angle(180, 90, 0), size = Vector(0.239, 0.151, 0.379), color = Color(255, 255, 255, 255), surpresslightning = false, material = "models/props_trainstation/tracksigns01a", skin = 0, bodygroup = {} },
	["bodymain"] = { type = "Model", model = "models/props_c17/FurnitureWashingmachine001a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "body inner", pos = Vector(-0.456, 0, -5.909), angle = Angle(0, 0, 0), size = Vector(0.094, 0.061, 0.209), color = Color(255, 255, 255, 255), surpresslightning = false, material = "models/props_lab/powerbox001", skin = 0, bodygroup = {} },
	["rearsight"] = { type = "Model", model = "models/props_interiors/refrigeratorDoor02a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "body inner", pos = Vector(-2.8, 0, 0.899), angle = Angle(-180, 0, 180), size = Vector(0.037, 0.037, 0.094), color = Color(255, 255, 255, 255), surpresslightning = false, material = "models/items/flare", skin = 0, bodygroup = {} },
	["handle"] = { type = "Model", model = "models/props_junk/propane_tank001a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "bodyfront", pos = Vector(0, 3.7, 3.299), angle = Angle(0, 0, -90), size = Vector(0.209, 0.209, 0.209), color = Color(255, 255, 255, 255), surpresslightning = false, material = "models/props_junk/trafficcone001a", skin = 0, bodygroup = {} }
}

SWEP.AutoSwitchTo = false
SWEP.Slot = 0
SWEP.PrintName = "The Reanimated Rocket Rifle"
SWEP.Author = "Spastik"
SWEP.Spawnable = true
SWEP.AutoSwitchFrom = false
SWEP.FiresUnderwater = false
SWEP.Weight = 5
SWEP.DrawCrosshair = true
SWEP.Category = "S&G Munitions"
SWEP.SlotPos = 3
SWEP.DrawAmmo = true
SWEP.ReloadSound = "weapons/smg1/smg1_reload.wav"
SWEP.Instructions = "PULL THE TRIGGER TO BRING BACK THE DEAD (JUST TO KILL THEM AGAIN SECONDS LATER)."
SWEP.Contact = ""
SWEP.Purpose = "FOR KEEPING YOUR ENEMIES MORE LIVELY BY CHASING THEM DOWN WITH A STREAM OF ROCKETS."
SWEP.Base = "aa_base"
SWEP.HoldType = "smg"
SWEP.ViewModelFOV = 70
SWEP.ViewModelFlip = false
SWEP.ViewModel = "models/weapons/v_smg1.mdl"
SWEP.WorldModel = "models/weapons/w_smg1.mdl"
SWEP.ShowViewModel = true
SWEP.ShowWorldModel = true
SWEP.ViewModelBoneMods = {
	["ValveBiped.handle"] = { scale = Vector(0.009, 0.009, 0.009), pos = Vector(0, 0, 0), angle = Angle(0, 0, 0) },
	["ValveBiped.Bip01"] = { scale = Vector(1, 1, 1), pos = Vector(-6.667, 2.029, 1.159), angle = Angle(0, 0, 0) }
}
//General Variables\\
SWEP.PrimaryReloadSound = Sound("Weapon_SMG1.Reload")
//Primary Fire Variables\\
SWEP.Primary.Round 			= ("ent_rocketshot_spastik")	--NAME OF ENTITY GOES HERE
SWEP.Primary.Damage = 25
SWEP.Primary.TakeAmmo = 1
SWEP.Primary.ClipSize = 10
SWEP.Primary.Ammo = "RPG_round"
SWEP.Primary.DefaultClip = 60
SWEP.Primary.Spread = 1.2
SWEP.Primary.NumberofShots = 1
SWEP.Primary.Automatic = true
SWEP.Primary.Recoil = 1
SWEP.Primary.Delay = 0.43
SWEP.Primary.Force = 40
//Primary Fire Variables\\

//Secondary Fire Variables\\
SWEP.Secondary.NumberofShots = 1
SWEP.Secondary.Force = 30
SWEP.Secondary.Spread = 0.1
SWEP.Secondary.DefaultClip = 32
SWEP.Secondary.Automatic = "true"
SWEP.Secondary.Ammo = "None"
SWEP.Secondary.Recoil = 1
SWEP.Secondary.Delay = 0.2
SWEP.Secondary.TakeAmmo = 1
SWEP.Secondary.ClipSize = 0
SWEP.Secondary.Damage = 10
//Secondary Fire Variables\\
//Secondary Fire Variables\\
//SWEP:Initialize()\\
	function SWEP:Initialize()
    self:SetHoldType("smg1")
end
//SWEP:Initialize()\\

function SWEP:PrimaryAttack()
	if self:Clip1() <= 0 then return end
	self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
	self:FireRocket()
	self:SetClip1( self:Clip1() -1 )
	self:SendWeaponAnim(ACT_VM_PRIMARYATTACK)
	self.Owner:SetAnimation( PLAYER_ATTACK1 )
	self.Weapon:EmitSound("weapons/grenade_launcher1.wav", 500, 80, 1, 0)
	self.Weapon:EmitSound("weapons/smg1/npc_smg1_fire1.wav", 500, 80, 1, -1)
	self.Owner:EmitSound("weapons/pistol/pistol_fire2.wav" .. math.random(1, 2) .. ".wav", 90, math.random(95, 75),1 ,1)
	self.Weapon:SetNWFloat( "LastShootTime", CurTime() )
end

function SWEP:FireRocket()
	local aim = self.Owner:GetAimVector()
	local side = aim:Cross(Vector(0,0,1))
	local up = side:Cross(aim)
	local pos = self.Owner:GetShootPos() + side * 10 + up * -10

	if SERVER then
	local rocket = ents.Create(self.Primary.Round)
	if !rocket:IsValid() then return false end
	rocket:SetAngles(self.Owner:GetAimVector():Angle(90,90,0))
	rocket:SetPos(pos)
	rocket:SetOwner(self.Owner)
	rocket:Spawn()
	rocket.Owner = self.Owner
	rocket:Activate()
	eyes = self.Owner:EyeAngles()
		local phys = rocket:GetPhysicsObject()
			phys:SetVelocity(self.Owner:GetAimVector() * 4000)
	end
		if SERVER and !self.Owner:IsNPC() then
		local anglo = Angle(-3, 0, 0)		
		self.Owner:ViewPunch(anglo)
		end

end

function SWEP:CheckWeaponsAndAmmo()
	if SERVER and self.Weapon != nil then 
		timer.Simple(.01, function() if not IsValid(self) then return end 
			if not IsValid(self.Owner) then return end
			self.Owner:StripWeapon(self.Gun)
		end)	
	end
end

//SWEP:SecondaryFire()\\
function SWEP:SecondaryAttack()
end
function SWEP:Reload()

	self.Weapon:DefaultReload(ACT_VM_RELOAD)



		if (self.Weapon:Clip1() < self.Primary.ClipSize) and (self.Owner:GetAmmoCount(self.Primary.Ammo) > 0) then

		//self:SetIronSights(false)
		self:EmitSound( "weapons/m4a1/m4a1_clipout.wav", 500, 100, 1, 2 )
		self:EmitSound( self.PrimaryReloadSound, 500, 100, 1, 1 )

		if not (CLIENT) then

			self.Owner:DrawViewModel(true)

		end

	end
end

/********************************************************
	SWEP Construction Kit base code
		Created by Clavus
	Available for public use, thread at:
	   facepunch.com/threads/1032378
	   
	   
	DESCRIPTION:
		This script is meant for experienced scripters 
		that KNOW WHAT THEY ARE DOING. Don't come to me 
		with basic Lua questions.
		
		Just copy into your SWEP or SWEP base of choice
		and merge with your own code.
		
		The SWEP.VElements, SWEP.WElements and
		SWEP.ViewModelBoneMods tables are all optional
		and only have to be visible to the client.
********************************************************/

function SWEP:Initialize()

	// other initialize code goes here

	if CLIENT then

		self:SetHoldType( self.HoldType or "pistol" )
		// Create a new table for every weapon instance
		self.VElements = table.FullCopy( self.VElements )
		self.WElements = table.FullCopy( self.WElements )
		self.ViewModelBoneMods = table.FullCopy( self.ViewModelBoneMods )

		self:CreateModels(self.VElements) // create viewmodels
		self:CreateModels(self.WElements) // create worldmodels
		
		// init view model bone build function
		if IsValid(self.Owner) then
			local vm = self.Owner:GetViewModel()
			if IsValid(vm) then
				self:ResetBonePositions(vm)
				
				--[[// Init viewmodel visibility
				if (self.ShowViewModel == nil or self.ShowViewModel) then
					vm:SetColor(Color(255,255,255,255))
				else
					// we set the alpha to 1 instead of 0 because else ViewModelDrawn stops being called
					vm:SetColor(Color(255,255,255,1))
					// ^ stopped working in GMod 13 because you have to do Entity:SetRenderMode(1) for translucency to kick in
					// however for some reason the view model resets to render mode 0 every frame so we just apply a debug material to prevent it from drawing
					vm:SetMaterial("Debug/hsv")			
				end]]--
			end
		end
	end
end

function SWEP:Holster()
	
	if CLIENT and IsValid(self.Owner) then
		local vm = self.Owner:GetViewModel()
		if IsValid(vm) then
			self:ResetBonePositions(vm)
		end
	end
	
	return true
end

if CLIENT then

	SWEP.vRenderOrder = nil
	function SWEP:ViewModelDrawn()
		
		local vm = self.Owner:GetViewModel()
		if !IsValid(vm) then return end
		
		if (!self.VElements) then return end
		
		self:UpdateBonePositions(vm)

		if (!self.vRenderOrder) then
			
			// we build a render order because sprites need to be drawn after models
			self.vRenderOrder = {}

			for k, v in pairs( self.VElements ) do
				if (v.type == "Model") then
					table.insert(self.vRenderOrder, 1, k)
				elseif (v.type == "Sprite" or v.type == "Quad") then
					table.insert(self.vRenderOrder, k)
				end
			end
			
		end

		for k, name in ipairs( self.vRenderOrder ) do
		
			local v = self.VElements[name]
			if (!v) then self.vRenderOrder = nil break end
			if (v.hide) then continue end
			
			local model = v.modelEnt
			local sprite = v.spriteMaterial
			
			if (!v.bone) then continue end
			
			local pos, ang = self:GetBoneOrientation( self.VElements, v, vm )
			
			if (!pos) then continue end
			
			if (v.type == "Model" and IsValid(model)) then

				model:SetPos(pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z )
				ang:RotateAroundAxis(ang:Up(), v.angle.y)
				ang:RotateAroundAxis(ang:Right(), v.angle.p)
				ang:RotateAroundAxis(ang:Forward(), v.angle.r)

				model:SetAngles(ang)
				//model:SetModelScale(v.size)
				local matrix = Matrix()
				matrix:Scale(v.size)
				model:EnableMatrix( "RenderMultiply", matrix )
				
				if (v.material == "") then
					model:SetMaterial("")
				elseif (model:GetMaterial() != v.material) then
					model:SetMaterial( v.material )
				end
				
				if (v.skin and v.skin != model:GetSkin()) then
					model:SetSkin(v.skin)
				end
				
				if (v.bodygroup) then
					for k, v in pairs( v.bodygroup ) do
						if (model:GetBodygroup(k) != v) then
							model:SetBodygroup(k, v)
						end
					end
				end
				
				if (v.surpresslightning) then
					render.SuppressEngineLighting(true)
				end
				
				render.SetColorModulation(v.color.r/255, v.color.g/255, v.color.b/255)
				render.SetBlend(v.color.a/255)
				model:DrawModel()
				render.SetBlend(1)
				render.SetColorModulation(1, 1, 1)
				
				if (v.surpresslightning) then
					render.SuppressEngineLighting(false)
				end
				
			elseif (v.type == "Sprite" and sprite) then
				
				local drawpos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
				render.SetMaterial(sprite)
				render.DrawSprite(drawpos, v.size.x, v.size.y, v.color)
				
			elseif (v.type == "Quad" and v.draw_func) then
				
				local drawpos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
				ang:RotateAroundAxis(ang:Up(), v.angle.y)
				ang:RotateAroundAxis(ang:Right(), v.angle.p)
				ang:RotateAroundAxis(ang:Forward(), v.angle.r)
				
				cam.Start3D2D(drawpos, ang, v.size)
					v.draw_func( self )
				cam.End3D2D()

			end
			
		end
		
	end

	SWEP.wRenderOrder = nil
	function SWEP:DrawWorldModel()
		
		if (self.ShowWorldModel == nil or self.ShowWorldModel) then
			self:DrawModel()
		end
		
		if (!self.WElements) then return end
		
		if (!self.wRenderOrder) then

			self.wRenderOrder = {}

			for k, v in pairs( self.WElements ) do
				if (v.type == "Model") then
					table.insert(self.wRenderOrder, 1, k)
				elseif (v.type == "Sprite" or v.type == "Quad") then
					table.insert(self.wRenderOrder, k)
				end
			end

		end
		
		if (IsValid(self.Owner)) then
			bone_ent = self.Owner
		else
			// when the weapon is dropped
			bone_ent = self
		end
		
		for k, name in pairs( self.wRenderOrder ) do
		
			local v = self.WElements[name]
			if (!v) then self.wRenderOrder = nil break end
			if (v.hide) then continue end
			
			local pos, ang
			
			if (v.bone) then
				pos, ang = self:GetBoneOrientation( self.WElements, v, bone_ent )
			else
				pos, ang = self:GetBoneOrientation( self.WElements, v, bone_ent, "ValveBiped.Bip01_R_Hand" )
			end
			
			if (!pos) then continue end
			
			local model = v.modelEnt
			local sprite = v.spriteMaterial
			
			if (v.type == "Model" and IsValid(model)) then

				model:SetPos(pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z )
				ang:RotateAroundAxis(ang:Up(), v.angle.y)
				ang:RotateAroundAxis(ang:Right(), v.angle.p)
				ang:RotateAroundAxis(ang:Forward(), v.angle.r)

				model:SetAngles(ang)
				//model:SetModelScale(v.size)
				local matrix = Matrix()
				matrix:Scale(v.size)
				model:EnableMatrix( "RenderMultiply", matrix )
				
				if (v.material == "") then
					model:SetMaterial("")
				elseif (model:GetMaterial() != v.material) then
					model:SetMaterial( v.material )
				end
				
				if (v.skin and v.skin != model:GetSkin()) then
					model:SetSkin(v.skin)
				end
				
				if (v.bodygroup) then
					for k, v in pairs( v.bodygroup ) do
						if (model:GetBodygroup(k) != v) then
							model:SetBodygroup(k, v)
						end
					end
				end
				
				if (v.surpresslightning) then
					render.SuppressEngineLighting(true)
				end
				
				render.SetColorModulation(v.color.r/255, v.color.g/255, v.color.b/255)
				render.SetBlend(v.color.a/255)
				model:DrawModel()
				render.SetBlend(1)
				render.SetColorModulation(1, 1, 1)
				
				if (v.surpresslightning) then
					render.SuppressEngineLighting(false)
				end
				
			elseif (v.type == "Sprite" and sprite) then
				
				local drawpos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
				render.SetMaterial(sprite)
				render.DrawSprite(drawpos, v.size.x, v.size.y, v.color)
				
			elseif (v.type == "Quad" and v.draw_func) then
				
				local drawpos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
				ang:RotateAroundAxis(ang:Up(), v.angle.y)
				ang:RotateAroundAxis(ang:Right(), v.angle.p)
				ang:RotateAroundAxis(ang:Forward(), v.angle.r)
				
				cam.Start3D2D(drawpos, ang, v.size)
					v.draw_func( self )
				cam.End3D2D()

			end
			
		end
		
	end

	function SWEP:GetBoneOrientation( basetab, tab, ent, bone_override )
		
		local bone, pos, ang
		if (tab.rel and tab.rel != "") then
			
			local v = basetab[tab.rel]
			
			if (!v) then return end
			
			// Technically, if there exists an element with the same name as a bone
			// you can get in an infinite loop. Let's just hope nobody's that stupid.
			pos, ang = self:GetBoneOrientation( basetab, v, ent )
			
			if (!pos) then return end
			
			pos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
			ang:RotateAroundAxis(ang:Up(), v.angle.y)
			ang:RotateAroundAxis(ang:Right(), v.angle.p)
			ang:RotateAroundAxis(ang:Forward(), v.angle.r)
				
		else
		
			bone = ent:LookupBone(bone_override or tab.bone)

			if (!bone) then return end
			
			pos, ang = Vector(0,0,0), Angle(0,0,0)
			local m = ent:GetBoneMatrix(bone)
			if (m) then
				pos, ang = m:GetTranslation(), m:GetAngles()
			end
			
			if (IsValid(self.Owner) and self.Owner:IsPlayer() and 
				ent == self.Owner:GetViewModel() and self.ViewModelFlip) then
				ang.r = -ang.r // Fixes mirrored models
			end
		
		end
		
		return pos, ang
	end

	function SWEP:CreateModels( tab )

		if (!tab) then return end

		// Create the clientside models here because Garry says we can't do it in the render hook
		for k, v in pairs( tab ) do
			if (v.type == "Model" and v.model and v.model != "" and (!IsValid(v.modelEnt) or v.createdModel != v.model) and 
					string.find(v.model, ".mdl") and file.Exists (v.model, "GAME") ) then
				
				v.modelEnt = ClientsideModel(v.model, RENDER_GROUP_VIEW_MODEL_OPAQUE)
				if (IsValid(v.modelEnt)) then
					v.modelEnt:SetPos(self:GetPos())
					v.modelEnt:SetAngles(self:GetAngles())
					v.modelEnt:SetParent(self)
					v.modelEnt:SetNoDraw(true)
					v.createdModel = v.model
				else
					v.modelEnt = nil
				end
				
			elseif (v.type == "Sprite" and v.sprite and v.sprite != "" and (!v.spriteMaterial or v.createdSprite != v.sprite) 
				and file.Exists ("materials/"..v.sprite..".vmt", "GAME")) then
				
				local name = v.sprite.."-"
				local params = { ["$basetexture"] = v.sprite }
				// make sure we create a unique name based on the selected options
				local tocheck = { "nocull", "additive", "vertexalpha", "vertexcolor", "ignorez" }
				for i, j in pairs( tocheck ) do
					if (v[j]) then
						params["$"..j] = 1
						name = name.."1"
					else
						name = name.."0"
					end
				end

				v.createdSprite = v.sprite
				v.spriteMaterial = CreateMaterial(name,"UnlitGeneric",params)
				
			end
		end
		
	end
	
	local allbones
	local hasGarryFixedBoneScalingYet = false

	function SWEP:UpdateBonePositions(vm)
		
		if self.ViewModelBoneMods then
			
			if (!vm:GetBoneCount()) then return end
			
			// !! WORKAROUND !! //
			// We need to check all model names :/
			local loopthrough = self.ViewModelBoneMods
			if (!hasGarryFixedBoneScalingYet) then
				allbones = {}
				for i=0, vm:GetBoneCount() do
					local bonename = vm:GetBoneName(i)
					if (self.ViewModelBoneMods[bonename]) then 
						allbones[bonename] = self.ViewModelBoneMods[bonename]
					else
						allbones[bonename] = { 
							scale = Vector(1,1,1),
							pos = Vector(0,0,0),
							angle = Angle(0,0,0)
						}
					end
				end
				
				loopthrough = allbones
			end
			// !! ----------- !! //
			
			for k, v in pairs( loopthrough ) do
				local bone = vm:LookupBone(k)
				if (!bone) then continue end
				
				// !! WORKAROUND !! //
				local s = Vector(v.scale.x,v.scale.y,v.scale.z)
				local p = Vector(v.pos.x,v.pos.y,v.pos.z)
				local ms = Vector(1,1,1)
				if (!hasGarryFixedBoneScalingYet) then
					local cur = vm:GetBoneParent(bone)
					while(cur >= 0) do
						local pscale = loopthrough[vm:GetBoneName(cur)].scale
						ms = ms * pscale
						cur = vm:GetBoneParent(cur)
					end
				end
				
				s = s * ms
				// !! ----------- !! //
				
				if vm:GetManipulateBoneScale(bone) != s then
					vm:ManipulateBoneScale( bone, s )
				end
				if vm:GetManipulateBoneAngles(bone) != v.angle then
					vm:ManipulateBoneAngles( bone, v.angle )
				end
				if vm:GetManipulateBonePosition(bone) != p then
					vm:ManipulateBonePosition( bone, p )
				end
			end
		else
			self:ResetBonePositions(vm)
		end
		   
	end
	 
	function SWEP:ResetBonePositions(vm)
		
		if (!vm:GetBoneCount()) then return end
		for i=0, vm:GetBoneCount() do
			vm:ManipulateBoneScale( i, Vector(1, 1, 1) )
			vm:ManipulateBoneAngles( i, Angle(0, 0, 0) )
			vm:ManipulateBonePosition( i, Vector(0, 0, 0) )
		end
		
	end

	/**************************
		Global utility code
	**************************/

	// Fully copies the table, meaning all tables inside this table are copied too and so on (normal table.Copy copies only their reference).
	// Does not copy entities of course, only copies their reference.
	// WARNING: do not use on tables that contain themselves somewhere down the line or you'll get an infinite loop
	function table.FullCopy( tab )

		if (!tab) then return nil end
		
		local res = {}
		for k, v in pairs( tab ) do
			if (type(v) == "table") then
				res[k] = table.FullCopy(v) // recursion ho!
			elseif (type(v) == "Vector") then
				res[k] = Vector(v.x, v.y, v.z)
			elseif (type(v) == "Angle") then
				res[k] = Angle(v.p, v.y, v.r)
			else
				res[k] = v
			end
		end
		
		return res
		
	end
	
end

