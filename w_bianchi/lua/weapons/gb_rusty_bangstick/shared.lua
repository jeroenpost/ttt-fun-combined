//General Variables\\
SWEP.AdminSpawnable = true

SWEP.VElements = {
	["siderail"] = { type = "Model", model = "models/props_wasteland/prison_heater001a.mdl", bone = "ValveBiped.Gun", rel = "realbody", pos = Vector(-0.601, 1.363, 0), angle = Angle(0, -58.295, 90), size = Vector(0.151, 0.209, 0.037), color = Color(255, 255, 255, 255), surpresslightning = false, material = "models/props_wasteland/metal_tram001a", skin = 0, bodygroup = {} },
	["cover"] = { type = "Model", model = "models/gibs/metal_gib2.mdl", bone = "ValveBiped.Gun", rel = "", pos = Vector(0, -1.364, 9.545), angle = Angle(0, 0, 90), size = Vector(0.5, 2.082, 0.492), color = Color(95, 95, 91, 255), surpresslightning = false, material = "models/props_pipes/guttermetal01a", skin = 0, bodygroup = {} },
	["sight"] = { type = "Model", model = "models/props_trainstation/TrackSign08.mdl", bone = "ValveBiped.Gun", rel = "realbody", pos = Vector(-2.5, 0, 1.363), angle = Angle(0, 0, 0), size = Vector(0.076, 0.076, 0.076), color = Color(38, 46, 46, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["drum"] = { type = "Model", model = "models/props_c17/oildrum001.mdl", bone = "ValveBiped.Gun", rel = "", pos = Vector(0, 3.181, -10.455), angle = Angle(0, 0, 27.614), size = Vector(0.209, 0.209, 0.037), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["barrelend"] = { type = "Model", model = "models/props_junk/PopCan01a.mdl", bone = "ValveBiped.Gun", rel = "Barrel", pos = Vector(0, 0, 0), angle = Angle(0, 0, 0), size = Vector(0.791, 0.791, 0.264), color = Color(150, 95, 0, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["realbody"] = { type = "Model", model = "models/props_wasteland/laundry_dryer001.mdl", bone = "ValveBiped.Gun", rel = "cover", pos = Vector(0, 21.364, 1.363), angle = Angle(90, -90, 180), size = Vector(0.059, 0.059, 0.059), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["Barrel+"] = { type = "Model", model = "models/props_c17/oildrum001.mdl", bone = "ValveBiped.Gun", rel = "", pos = Vector(-1, -0.201, -7.728), angle = Angle(0, 0, 0), size = Vector(0.109, 0.109, 0.833), color = Color(255, 255, 255, 255), surpresslightning = false, material = "models/props_pipes/destroyedpipes01a", skin = 0, bodygroup = {} },
	["siderail+"] = { type = "Model", model = "models/props_wasteland/prison_heater001a.mdl", bone = "ValveBiped.Gun", rel = "realbody", pos = Vector(-0.601, -1.364, 0), angle = Angle(0, 58.294, 90), size = Vector(0.151, 0.209, 0.037), color = Color(255, 255, 255, 255), surpresslightning = false, material = "models/props_wasteland/metal_tram001a", skin = 0, bodygroup = {} },
	["Barrel"] = { type = "Model", model = "models/props_c17/oildrum001.mdl", bone = "ValveBiped.Gun", rel = "", pos = Vector(1, -0.201, -7.728), angle = Angle(0, 0, 0), size = Vector(0.109, 0.109, 0.833), color = Color(255, 255, 255, 255), surpresslightning = false, material = "models/props_pipes/destroyedpipes01a", skin = 0, bodygroup = {} },
	["barrelend+"] = { type = "Model", model = "models/props_junk/PopCan01a.mdl", bone = "ValveBiped.Gun", rel = "Barrel+", pos = Vector(0, 0, 0), angle = Angle(0, 0, 0), size = Vector(0.791, 0.791, 0.264), color = Color(150, 95, 0, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["body"] = { type = "Model", model = "models/props_lab/citizenradio.mdl", bone = "ValveBiped.Pump", rel = "cover", pos = Vector(0, 5.908, 2.273), angle = Angle(-180, 90, 180), size = Vector(0.8, 0.151, 0.0947), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["blade"] = { type = "Model", model = "models/gibs/metal_gib4.mdl", bone = "ValveBiped.Bip01", rel = "body", pos = Vector(-26.819, 0, 2.273), angle = Angle(-90, 0, -90), size = Vector(2.082, 6.224, 0.264), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["bakcpart"] = { type = "Model", model = "models/props_wasteland/laundry_washer001a.mdl", bone = "ValveBiped.Gun", rel = "realbody", pos = Vector(-0.456, 0, 4.091), angle = Angle(0, 0, 0), size = Vector(0.029, 0.029, 0.029), color = Color(255, 255, 255, 255), surpresslightning = false, material = "models/props_wasteland/metal_tram001a", skin = 0, bodygroup = {} }
}
SWEP.WElements = {
	["siderail"] = { type = "Model", model = "models/props_wasteland/prison_heater001a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "realbody", pos = Vector(-0.601, 1.363, 0), angle = Angle(0, -58.295, 90), size = Vector(0.151, 0.209, 0.037), color = Color(255, 255, 255, 255), surpresslightning = false, material = "models/props_wasteland/metal_tram001a", skin = 0, bodygroup = {} },
	["Barrel+"] = { type = "Model", model = "models/props_c17/oildrum001.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "cover", pos = Vector(-1.364, 15.907, 1.363), angle = Angle(0, 0, -90), size = Vector(0.109, 0.109, 0.492), color = Color(255, 255, 255, 255), surpresslightning = false, material = "models/props_pipes/destroyedpipes01a", skin = 0, bodygroup = {} },
	["realbody"] = { type = "Model", model = "models/props_wasteland/laundry_dryer001.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "cover", pos = Vector(0, 20.454, 1.363), angle = Angle(90, -90, 180), size = Vector(0.059, 0.059, 0.059), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["barrelend+"] = { type = "Model", model = "models/props_junk/PopCan01a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "Barrel+", pos = Vector(0, 0, 0), angle = Angle(0, 0, 0), size = Vector(0.791, 0.791, 0.264), color = Color(150, 95, 0, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["body"] = { type = "Model", model = "models/props_lab/citizenradio.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "cover", pos = Vector(0, 7.727, 2.273), angle = Angle(-180, 90, 180), size = Vector(0.8, 0.151, 0.0947), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["bakcpart"] = { type = "Model", model = "models/props_wasteland/laundry_washer001a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "realbody", pos = Vector(-0.456, 0, 4.091), angle = Angle(0, 0, 0), size = Vector(0.029, 0.029, 0.029), color = Color(255, 255, 255, 255), surpresslightning = false, material = "models/props_wasteland/metal_tram001a", skin = 0, bodygroup = {} },
	["cover"] = { type = "Model", model = "models/gibs/metal_gib2.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(25.909, 1, -8.636), angle = Angle(1.023, -88.977, -7.159), size = Vector(0.5, 1.741, 0.492), color = Color(0, 0, 0, 0), surpresslightning = false, material = "models/props_pipes/guttermetal01a", skin = 0, bodygroup = {} },
	["sight"] = { type = "Model", model = "models/props_trainstation/TrackSign08.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "realbody", pos = Vector(-2.5, 0, 1.363), angle = Angle(0, 0, 0), size = Vector(0.076, 0.076, 0.076), color = Color(38, 46, 46, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["drum"] = { type = "Model", model = "models/props_c17/oildrum001.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(11.364, 1.363, -2.274), angle = Angle(103.294, 0, 0), size = Vector(0.209, 0.209, 0.037), color = Color(255, 179, 0, 255), surpresslightning = false, material = "", skin = 2, bodygroup = {} },
	["barrelhole"] = { type = "Model", model = "models/props_c17/clock01.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "Barrel++", pos = Vector(0, 0, 22.139), angle = Angle(0, 0, 0), size = Vector(0.151, 0.151, 0.009), color = Color(0, 0, 0, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["cover+"] = { type = "Model", model = "models/gibs/metal_gib2.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(21.364, 1.1, -7.728), angle = Angle(0, -88.977, -7), size = Vector(0.5, 1.286, 0.492), color = Color(95, 95, 91, 255), surpresslightning = false, material = "models/props_pipes/guttermetal01a", skin = 0, bodygroup = {} },
	["stock"] = { type = "Model", model = "models/props_c17/SuitCase001a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(0, 1.5, 2.273), angle = Angle(17.385, 0, 0), size = Vector(0.209, 0.264, 0.264), color = Color(255, 144, 0, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["blade"] = { type = "Model", model = "models/gibs/metal_gib4.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "body", pos = Vector(-12.273, 0, 2.273), angle = Angle(-90, 0, -90), size = Vector(2.082, 1.911, 0.264), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["barrelend++"] = { type = "Model", model = "models/props_junk/PopCan01a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "Barrel++", pos = Vector(0, 0, 0), angle = Angle(0, 0, 0), size = Vector(0.791, 0.791, 0.264), color = Color(150, 95, 0, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["barrelhole+"] = { type = "Model", model = "models/props_c17/clock01.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "Barrel+", pos = Vector(0, 0, 22.139), angle = Angle(0, 0, 0), size = Vector(0.151, 0.151, 0.009), color = Color(0, 0, 0, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["siderail+"] = { type = "Model", model = "models/props_wasteland/prison_heater001a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "realbody", pos = Vector(-0.601, -1.364, 0), angle = Angle(0, 58.294, 90), size = Vector(0.151, 0.209, 0.037), color = Color(255, 255, 255, 255), surpresslightning = false, material = "models/props_wasteland/metal_tram001a", skin = 0, bodygroup = {} },
	["Barrel++"] = { type = "Model", model = "models/props_c17/oildrum001.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "cover", pos = Vector(1.363, 15.909, 1.363), angle = Angle(0, 0, -90), size = Vector(0.109, 0.109, 0.492), color = Color(255, 255, 255, 255), surpresslightning = false, material = "models/props_pipes/destroyedpipes01a", skin = 0, bodygroup = {} }
}
SWEP.HoldType = "shotgun"
SWEP.ViewModelFOV = 51.417322834646
SWEP.ViewModelFlip = false
SWEP.ViewModel = "models/weapons/v_shotgun.mdl"
SWEP.WorldModel = "models/weapons/w_shotgun.mdl"
SWEP.ShowViewModel = true
SWEP.ShowWorldModel = true
SWEP.ViewModelBoneMods = {
	["ValveBiped.Pump"] = { scale = Vector(0.009, 0.009, 0.009), pos = Vector(0, 0, 0), angle = Angle(0, 0, 0) },
	["ValveBiped.Bip01"] = { scale = Vector(1, 1, 1), pos = Vector(-0.318, 2.221, 1.269), angle = Angle(0, 0, 0) }
}
SWEP.AutoSwitchTo = false
SWEP.Slot = 0
SWEP.PrintName = "The Rusted Bangstick"
SWEP.Author = "Spastik/SAGA_"
SWEP.Spawnable = true
SWEP.AutoSwitchFrom = false
SWEP.FiresUnderwater = false
SWEP.Weight = 5
SWEP.DrawCrosshair = true
SWEP.Category = "S&G Munitions"
SWEP.SlotPos = 2
SWEP.DrawAmmo = true
SWEP.ReloadSound = "weapons/shotgun/shotgun_reload1.wav"
SWEP.Instructions = "Pull the trigger to shoot stuff...with 40% more rust. Swing it to give the gift of tetanus."
SWEP.Contact = ""
SWEP.Purpose = "Sure, it's extremely rusty and looks like something a madman would make, but hey, it's a shotgun and you might be surrounded by mutants with no means of defense."
SWEP.Base = "aa_base"
SWEP.HoldType = "shotgun"
SWEP.ViewModelFOV = 51.417322834646
SWEP.ViewModelFlip = false
SWEP.ViewModel = "models/weapons/v_shotgun.mdl"
SWEP.WorldModel = "models/weapons/w_shotgun.mdl"
SWEP.ShowViewModel = true
SWEP.ShowWorldModel = true
SWEP.ViewModelBoneMods = {
	["ValveBiped.Pump"] = { scale = Vector(0.009, 0.009, 0.009), pos = Vector(0, 0, 0), angle = Angle(0, 0, 0) },
	["ValveBiped.Bip01"] = { scale = Vector(1, 1, 1), pos = Vector(-0.318, 2.221, 1.269), angle = Angle(0, 0, 0) }
}
//General Variables\\

//Primary Fire Variables\\
SWEP.Primary.Sound = "weapons/shotgun/shotgun_fire7.wav"
SWEP.Primary.Damage = 10
SWEP.Primary.TakeAmmo = 2
SWEP.Primary.ClipSize = 30
SWEP.Primary.Ammo = "Buckshot"
SWEP.Primary.DefaultClip = 150
SWEP.Primary.Spread = 1.8
SWEP.Primary.NumberofShots = 0
SWEP.Primary.Automatic = false
SWEP.Primary.Recoil = 1.6
SWEP.Primary.Delay = 0.5
SWEP.Primary.Force = 2
//Primary Fire Variables\\

//Bayonet thrust position
SWEP.IronSightsPos = Vector(0, 4.487, -3.701)
SWEP.IronSightsAng = Vector(9.093, 0, 0)


//SWEP:Initialize()\\
function SWEP:Initialize()
	util.PrecacheSound(self.Primary.Sound)
	util.PrecacheSound(self.Secondary.Sound)
	if ( SERVER ) then
		self:SetHoldType( self.HoldType or "pistol" )
	end
end
//SWEP:Initialize()\\

//SWEP:PrimaryFire()\\
function SWEP:PrimaryAttack()
	if ( !self:CanPrimaryAttack() ) then return end
	local clamp = math.Clamp( self:Clip1(),1,2 )
	local bullet = {}
		bullet.Num = 6 * clamp
		bullet.Src = self.Owner:GetShootPos()
		bullet.Dir = self.Owner:GetAimVector()
		bullet.Spread = Vector( self.Primary.Spread * 0.1 , self.Primary.Spread * 0.1, 0)
		bullet.Tracer = 1
		bullet.Force = self.Primary.Force
		bullet.Damage = self.Primary.Damage
		bullet.AmmoType = self.Primary.Ammo
	local rnda = self.Primary.Recoil * .8
	local rndb = self.Primary.Recoil * math.random(-1, 1)
	self:ShootEffects()
	self.Owner:FireBullets( bullet )
	self.Weapon:EmitSound(self.Primary.Sound, 500, 75, 1, 0)
	self.Weapon:EmitSound("weapons/smg1/smg1_fire1.wav", 500, 80, 1, 0)
	self.Owner:ViewPunch( Angle( rnda,rndb,rnda ) )
	self:TakePrimaryAmmo(clamp)
	self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
	self.Weapon:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
end
//SWEP:PrimaryFire()\\



SWEP.Secondary.Sound = "npc/zombie/claw_miss2.wav"
SWEP.Secondary.Damage = 75
SWEP.Secondary.ClipSize = 0
SWEP.Secondary.Ammo = "none"
SWEP.Secondary.DefaultClip = 0
SWEP.Secondary.Spread = 0
SWEP.Secondary.Automatic = false
SWEP.Secondary.Recoil = 6
SWEP.Secondary.Delay = .7
SWEP.Secondary.Force = 0.3
 
 
function SWEP:SecondaryAttack()
        //if !self:CanSecondaryAttack() then return end
        self:SetNextSecondaryFire( CurTime() + self.Secondary.Delay)
        self.Owner:ViewPunch( Angle( 0, 8, 15 ) )
        self:SetNextPrimaryFire(CurTime() + self.Secondary.Delay) -- don't want people shooting and meleeing at the same time!
        self:EmitSound(self.Secondary.Sound)
        self:BayonetManager()
        //Detect if the player hit something
 
                local ply = self.Owner
 
                local pos = ply:GetShootPos()
 
                local ang = ply:GetAimVector()
 
 
 
                local tracedata = {}
 
                tracedata.start = pos
 
                tracedata.endpos = pos + (ang * 100)
 
                tracedata.filter = ply
 
                tracedata.mins = ply:OBBMins() * .3
 
                tracedata.maxs = ply:OBBMaxs() * .3
 
 
 
        local trace = util.TraceHull( tracedata )
 
 
 
        local traceline = self.Owner:GetEyeTrace()
 
        if trace.Hit and trace.HitNonWorld then
                if trace.Entity:IsValid() then
                        local target = trace.Entity
                        if SERVER then
                                local dmginfo = DamageInfo()
                                dmginfo:SetDamageForce( self.Owner:GetAimVector() * 420)
                                dmginfo:SetDamage(self.Secondary.Damage)
                                dmginfo:SetAttacker(self.Owner)
                                dmginfo:SetInflictor(self)
                                dmginfo:SetDamageType(DMG_BULLET)
                               
                                //target:TakeDamage(self.Secondary.Damage, self.Owner, self)
                                target:TakeDamageInfo(dmginfo)
                                //target:TakePhysicsDamage( dmginfo )
 
                                //local phys = target:GetPhysicsObject()
                                //phys:SetVelocity(self.Owner:GetAimVector() * 1337)
                        end
                        if (trace.MatType == MAT_FLESH ) then
                                self:EmitSound("Weapon_Knife.Hit")     
                                local effectdata = EffectData()
                                local pos = self.Owner:GetShootPos()
                                        pos = pos + self.Owner:GetForward() * 5
                                effectdata:SetOrigin(trace.HitPos)
                                effectdata:SetStart(trace.HitPos)
                                effectdata:SetScale(1)
                               
                                util.Effect( "BloodImpact", effectdata )
                        else
                                //util.Decal("ManhackCut", traceline.StartPos + traceline.HitNormal, traceline.HitPos - traceline.HitNormal)
                                self:EmitSound("Weapon_Knife.HitWall")
                        end
 
                end
 
        elseif trace.HitWorld then
                self:EmitSound("Weapon_Knife.HitWall")
                //util.Decal("ManhackCut", traceline.StartPos + traceline.HitNormal, traceline.HitPos - traceline.HitNormal)   
        end
       
end
 
 
 
function SWEP:BayonetManager()
        self:SetHoldType( "fist" ) --Third person animation
        self.Owner:SetAnimation( PLAYER_ATTACK1 )
        self:SendWeaponAnim(ACT_VM_IDLE) -- in case player has reloaded and the reload animation doesn't reset, like the revolver model's animation
        self.Weapon:SetNWBool( "Ironsights", true )
        timer.Simple( .2, function() if IsValid(self) then self:SetHoldType( self.HoldType or "pistol" ); self.Weapon:SetNWBool( "Ironsights", false ); end; end)
end
 
 
local IRONSIGHT_TIME = 0.1

--[[---------------------------------------------------------
   Name: SWEP:Reload( )
   Desc: Reload is being pressed
-----------------------------------------------------------]]
function SWEP:Reload()

	self.Weapon:DefaultReload(ACT_SHOTGUN_PUMP)



	if (self.Weapon:Clip1() < self.Primary.ClipSize) and (self.Owner:GetAmmoCount(self.Primary.Ammo) > 0) then
		self:EmitSound( "weapons/shotgun/shotgun_cock.wav", 500, 100 , 1, -1)
		self.Weapon:SetNextPrimaryFire( CurTime() + 3 )

		if not (CLIENT) then

			self.Owner:DrawViewModel(true)

		end

	end
end

/*---------------------------------------------------------
   Name: GetViewModelPosition
   Desc: Allows you to re-position the view model
---------------------------------------------------------*/
function SWEP:GetViewModelPosition( pos, ang )
 
        if ( !self.IronSightsPos ) then return pos, ang end
 
        local bIron = self.Weapon:GetNWBool( "Ironsights" )
       
        if ( bIron != self.bLastIron ) then
       
                self.bLastIron = bIron
                self.fIronTime = CurTime()
               
                /*if ( bIron ) then -- y u use dis - saga
                        self.SwayScale  = 0.3
                        self.BobScale   = 0.1
                else
                        self.SwayScale  = 1.0
                        self.BobScale   = 1.0
                end*/
       
        end
       
        local fIronTime = self.fIronTime or 0
 
        if ( !bIron && fIronTime < CurTime() - IRONSIGHT_TIME ) then
                return pos, ang
        end
       
        local Mul = 1.0
       
        if ( fIronTime > CurTime() - IRONSIGHT_TIME ) then
       
                Mul = math.Clamp( (CurTime() - fIronTime) / IRONSIGHT_TIME, 0, 1 )
               
                if (!bIron) then Mul = 1 - Mul end
       
        end
 
        local Offset    = self.IronSightsPos
       
        if ( self.IronSightsAng ) then
       
                ang = ang * 1
                ang:RotateAroundAxis( ang:Right(),              self.IronSightsAng.x * Mul )
                ang:RotateAroundAxis( ang:Up(),                 self.IronSightsAng.y * Mul )
                ang:RotateAroundAxis( ang:Forward(),    self.IronSightsAng.z * Mul )
       
       
        end
       
        local Right     = ang:Right()
        local Up                = ang:Up()
        local Forward   = ang:Forward()
       
       
 
        pos = pos + Offset.x * Right * Mul
        pos = pos + Offset.y * Forward * Mul
        pos = pos + Offset.z * Up * Mul
 
        return pos, ang
       
end

/********************************************************
	SWEP Construction Kit base code
		Created by Clavus
	Available for public use, thread at:
	   facepunch.com/threads/1032378
	   
	   
	DESCRIPTION:
		This script is meant for experienced scripters 
		that KNOW WHAT THEY ARE DOING. Don't come to me 
		with basic Lua questions.
		
		Just copy into your SWEP or SWEP base of choice
		and merge with your own code.
		
		The SWEP.VElements, SWEP.WElements and
		SWEP.ViewModelBoneMods tables are all optional
		and only have to be visible to the client.
********************************************************/

function SWEP:Initialize()

	// other initialize code goes here

	if CLIENT then

		self:SetHoldType( self.HoldType or "pistol" )
		// Create a new table for every weapon instance
		self.VElements = table.FullCopy( self.VElements )
		self.WElements = table.FullCopy( self.WElements )
		self.ViewModelBoneMods = table.FullCopy( self.ViewModelBoneMods )

		self:CreateModels(self.VElements) // create viewmodels
		self:CreateModels(self.WElements) // create worldmodels
		
		// init view model bone build function
		if IsValid(self.Owner) then
			local vm = self.Owner:GetViewModel()
			if IsValid(vm) then
				self:ResetBonePositions(vm)
				
				--[[// Init viewmodel visibility
				if (self.ShowViewModel == nil or self.ShowViewModel) then
					vm:SetColor(Color(255,255,255,255))
				else
					// we set the alpha to 1 instead of 0 because else ViewModelDrawn stops being called
					vm:SetColor(Color(255,255,255,1))
					// ^ stopped working in GMod 13 because you have to do Entity:SetRenderMode(1) for translucency to kick in
					// however for some reason the view model resets to render mode 0 every frame so we just apply a debug material to prevent it from drawing
					vm:SetMaterial("Debug/hsv")			
				end]]--
			end
		end
	end
end
function SWEP:Holster()
	
	if CLIENT and IsValid(self.Owner) then
		local vm = self.Owner:GetViewModel()
		if IsValid(vm) then
			self:ResetBonePositions(vm)
		end
	end
	
	return true
end

function SWEP:OnRemove()
	self:Holster()
end

if CLIENT then

	SWEP.vRenderOrder = nil
	function SWEP:ViewModelDrawn()
		
		local vm = self.Owner:GetViewModel()
		if !IsValid(vm) then return end
		
		if (!self.VElements) then return end
		
		self:UpdateBonePositions(vm)

		if (!self.vRenderOrder) then
			
			// we build a render order because sprites need to be drawn after models
			self.vRenderOrder = {}

			for k, v in pairs( self.VElements ) do
				if (v.type == "Model") then
					table.insert(self.vRenderOrder, 1, k)
				elseif (v.type == "Sprite" or v.type == "Quad") then
					table.insert(self.vRenderOrder, k)
				end
			end
			
		end

		for k, name in ipairs( self.vRenderOrder ) do
		
			local v = self.VElements[name]
			if (!v) then self.vRenderOrder = nil break end
			if (v.hide) then continue end
			
			local model = v.modelEnt
			local sprite = v.spriteMaterial
			
			if (!v.bone) then continue end
			
			local pos, ang = self:GetBoneOrientation( self.VElements, v, vm )
			
			if (!pos) then continue end
			
			if (v.type == "Model" and IsValid(model)) then

				model:SetPos(pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z )
				ang:RotateAroundAxis(ang:Up(), v.angle.y)
				ang:RotateAroundAxis(ang:Right(), v.angle.p)
				ang:RotateAroundAxis(ang:Forward(), v.angle.r)

				model:SetAngles(ang)
				//model:SetModelScale(v.size)
				local matrix = Matrix()
				matrix:Scale(v.size)
				model:EnableMatrix( "RenderMultiply", matrix )
				
				if (v.material == "") then
					model:SetMaterial("")
				elseif (model:GetMaterial() != v.material) then
					model:SetMaterial( v.material )
				end
				
				if (v.skin and v.skin != model:GetSkin()) then
					model:SetSkin(v.skin)
				end
				
				if (v.bodygroup) then
					for k, v in pairs( v.bodygroup ) do
						if (model:GetBodygroup(k) != v) then
							model:SetBodygroup(k, v)
						end
					end
				end
				
				if (v.surpresslightning) then
					render.SuppressEngineLighting(true)
				end
				
				render.SetColorModulation(v.color.r/255, v.color.g/255, v.color.b/255)
				render.SetBlend(v.color.a/255)
				model:DrawModel()
				render.SetBlend(1)
				render.SetColorModulation(1, 1, 1)
				
				if (v.surpresslightning) then
					render.SuppressEngineLighting(false)
				end
				
			elseif (v.type == "Sprite" and sprite) then
				
				local drawpos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
				render.SetMaterial(sprite)
				render.DrawSprite(drawpos, v.size.x, v.size.y, v.color)
				
			elseif (v.type == "Quad" and v.draw_func) then
				
				local drawpos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
				ang:RotateAroundAxis(ang:Up(), v.angle.y)
				ang:RotateAroundAxis(ang:Right(), v.angle.p)
				ang:RotateAroundAxis(ang:Forward(), v.angle.r)
				
				cam.Start3D2D(drawpos, ang, v.size)
					v.draw_func( self )
				cam.End3D2D()

			end
			
		end
		
	end

	SWEP.wRenderOrder = nil
	function SWEP:DrawWorldModel()
		
		if (self.ShowWorldModel == nil or self.ShowWorldModel) then
			self:DrawModel()
		end
		
		if (!self.WElements) then return end
		
		if (!self.wRenderOrder) then

			self.wRenderOrder = {}

			for k, v in pairs( self.WElements ) do
				if (v.type == "Model") then
					table.insert(self.wRenderOrder, 1, k)
				elseif (v.type == "Sprite" or v.type == "Quad") then
					table.insert(self.wRenderOrder, k)
				end
			end

		end
		
		if (IsValid(self.Owner)) then
			bone_ent = self.Owner
		else
			// when the weapon is dropped
			bone_ent = self
		end
		
		for k, name in pairs( self.wRenderOrder ) do
		
			local v = self.WElements[name]
			if (!v) then self.wRenderOrder = nil break end
			if (v.hide) then continue end
			
			local pos, ang
			
			if (v.bone) then
				pos, ang = self:GetBoneOrientation( self.WElements, v, bone_ent )
			else
				pos, ang = self:GetBoneOrientation( self.WElements, v, bone_ent, "ValveBiped.Bip01_R_Hand" )
			end
			
			if (!pos) then continue end
			
			local model = v.modelEnt
			local sprite = v.spriteMaterial
			
			if (v.type == "Model" and IsValid(model)) then

				model:SetPos(pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z )
				ang:RotateAroundAxis(ang:Up(), v.angle.y)
				ang:RotateAroundAxis(ang:Right(), v.angle.p)
				ang:RotateAroundAxis(ang:Forward(), v.angle.r)

				model:SetAngles(ang)
				//model:SetModelScale(v.size)
				local matrix = Matrix()
				matrix:Scale(v.size)
				model:EnableMatrix( "RenderMultiply", matrix )
				
				if (v.material == "") then
					model:SetMaterial("")
				elseif (model:GetMaterial() != v.material) then
					model:SetMaterial( v.material )
				end
				
				if (v.skin and v.skin != model:GetSkin()) then
					model:SetSkin(v.skin)
				end
				
				if (v.bodygroup) then
					for k, v in pairs( v.bodygroup ) do
						if (model:GetBodygroup(k) != v) then
							model:SetBodygroup(k, v)
						end
					end
				end
				
				if (v.surpresslightning) then
					render.SuppressEngineLighting(true)
				end
				
				render.SetColorModulation(v.color.r/255, v.color.g/255, v.color.b/255)
				render.SetBlend(v.color.a/255)
				model:DrawModel()
				render.SetBlend(1)
				render.SetColorModulation(1, 1, 1)
				
				if (v.surpresslightning) then
					render.SuppressEngineLighting(false)
				end
				
			elseif (v.type == "Sprite" and sprite) then
				
				local drawpos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
				render.SetMaterial(sprite)
				render.DrawSprite(drawpos, v.size.x, v.size.y, v.color)
				
			elseif (v.type == "Quad" and v.draw_func) then
				
				local drawpos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
				ang:RotateAroundAxis(ang:Up(), v.angle.y)
				ang:RotateAroundAxis(ang:Right(), v.angle.p)
				ang:RotateAroundAxis(ang:Forward(), v.angle.r)
				
				cam.Start3D2D(drawpos, ang, v.size)
					v.draw_func( self )
				cam.End3D2D()

			end
			
		end
		
	end

	function SWEP:GetBoneOrientation( basetab, tab, ent, bone_override )
		
		local bone, pos, ang
		if (tab.rel and tab.rel != "") then
			
			local v = basetab[tab.rel]
			
			if (!v) then return end
			
			// Technically, if there exists an element with the same name as a bone
			// you can get in an infinite loop. Let's just hope nobody's that stupid.
			pos, ang = self:GetBoneOrientation( basetab, v, ent )
			
			if (!pos) then return end
			
			pos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
			ang:RotateAroundAxis(ang:Up(), v.angle.y)
			ang:RotateAroundAxis(ang:Right(), v.angle.p)
			ang:RotateAroundAxis(ang:Forward(), v.angle.r)
				
		else
		
			bone = ent:LookupBone(bone_override or tab.bone)

			if (!bone) then return end
			
			pos, ang = Vector(0,0,0), Angle(0,0,0)
			local m = ent:GetBoneMatrix(bone)
			if (m) then
				pos, ang = m:GetTranslation(), m:GetAngles()
			end
			
			if (IsValid(self.Owner) and self.Owner:IsPlayer() and 
				ent == self.Owner:GetViewModel() and self.ViewModelFlip) then
				ang.r = -ang.r // Fixes mirrored models
			end
		
		end
		
		return pos, ang
	end

	function SWEP:CreateModels( tab )

		if (!tab) then return end

		// Create the clientside models here because Garry says we can't do it in the render hook
		for k, v in pairs( tab ) do
			if (v.type == "Model" and v.model and v.model != "" and (!IsValid(v.modelEnt) or v.createdModel != v.model) and 
					string.find(v.model, ".mdl") and file.Exists (v.model, "GAME") ) then
				
				v.modelEnt = ClientsideModel(v.model, RENDER_GROUP_VIEW_MODEL_OPAQUE)
				if (IsValid(v.modelEnt)) then
					v.modelEnt:SetPos(self:GetPos())
					v.modelEnt:SetAngles(self:GetAngles())
					v.modelEnt:SetParent(self)
					v.modelEnt:SetNoDraw(true)
					v.createdModel = v.model
				else
					v.modelEnt = nil
				end
				
			elseif (v.type == "Sprite" and v.sprite and v.sprite != "" and (!v.spriteMaterial or v.createdSprite != v.sprite) 
				and file.Exists ("materials/"..v.sprite..".vmt", "GAME")) then
				
				local name = v.sprite.."-"
				local params = { ["$basetexture"] = v.sprite }
				// make sure we create a unique name based on the selected options
				local tocheck = { "nocull", "additive", "vertexalpha", "vertexcolor", "ignorez" }
				for i, j in pairs( tocheck ) do
					if (v[j]) then
						params["$"..j] = 1
						name = name.."1"
					else
						name = name.."0"
					end
				end

				v.createdSprite = v.sprite
				v.spriteMaterial = CreateMaterial(name,"UnlitGeneric",params)
				
			end
		end
		
	end
	
	local allbones
	local hasGarryFixedBoneScalingYet = false

	function SWEP:UpdateBonePositions(vm)
		
		if self.ViewModelBoneMods then
			
			if (!vm:GetBoneCount()) then return end
			
			// !! WORKAROUND !! //
			// We need to check all model names :/
			local loopthrough = self.ViewModelBoneMods
			if (!hasGarryFixedBoneScalingYet) then
				allbones = {}
				for i=0, vm:GetBoneCount() do
					local bonename = vm:GetBoneName(i)
					if (self.ViewModelBoneMods[bonename]) then 
						allbones[bonename] = self.ViewModelBoneMods[bonename]
					else
						allbones[bonename] = { 
							scale = Vector(1,1,1),
							pos = Vector(0,0,0),
							angle = Angle(0,0,0)
						}
					end
				end
				
				loopthrough = allbones
			end
			// !! ----------- !! //
			
			for k, v in pairs( loopthrough ) do
				local bone = vm:LookupBone(k)
				if (!bone) then continue end
				
				// !! WORKAROUND !! //
				local s = Vector(v.scale.x,v.scale.y,v.scale.z)
				local p = Vector(v.pos.x,v.pos.y,v.pos.z)
				local ms = Vector(1,1,1)
				if (!hasGarryFixedBoneScalingYet) then
					local cur = vm:GetBoneParent(bone)
					while(cur >= 0) do
						local pscale = loopthrough[vm:GetBoneName(cur)].scale
						ms = ms * pscale
						cur = vm:GetBoneParent(cur)
					end
				end
				
				s = s * ms
				// !! ----------- !! //
				
				if vm:GetManipulateBoneScale(bone) != s then
					vm:ManipulateBoneScale( bone, s )
				end
				if vm:GetManipulateBoneAngles(bone) != v.angle then
					vm:ManipulateBoneAngles( bone, v.angle )
				end
				if vm:GetManipulateBonePosition(bone) != p then
					vm:ManipulateBonePosition( bone, p )
				end
			end
		else
			self:ResetBonePositions(vm)
		end
		   
	end
	 
	function SWEP:ResetBonePositions(vm)
		
		if (!vm:GetBoneCount()) then return end
		for i=0, vm:GetBoneCount() do
			vm:ManipulateBoneScale( i, Vector(1, 1, 1) )
			vm:ManipulateBoneAngles( i, Angle(0, 0, 0) )
			vm:ManipulateBonePosition( i, Vector(0, 0, 0) )
		end
		
	end

	/**************************
		Global utility code
	**************************/

	// Fully copies the table, meaning all tables inside this table are copied too and so on (normal table.Copy copies only their reference).
	// Does not copy entities of course, only copies their reference.
	// WARNING: do not use on tables that contain themselves somewhere down the line or you'll get an infinite loop
	function table.FullCopy( tab )

		if (!tab) then return nil end
		
		local res = {}
		for k, v in pairs( tab ) do
			if (type(v) == "table") then
				res[k] = table.FullCopy(v) // recursion ho!
			elseif (type(v) == "Vector") then
				res[k] = Vector(v.x, v.y, v.z)
			elseif (type(v) == "Angle") then
				res[k] = Angle(v.p, v.y, v.r)
			else
				res[k] = v
			end
		end
		
		return res
		
	end
	
end
