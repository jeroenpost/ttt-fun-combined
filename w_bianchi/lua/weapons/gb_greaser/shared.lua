//General Variables\\
SWEP.AdminSpawnable = true
SWEP.ViewModelFOV = 64

SWEP.VElements = {
	["stock2"] = { type = "Model", model = "models/props_lab/reciever_cart.mdl", bone = "ValveBiped.base", rel = "stock1", pos = Vector(-0.801, 0, 1.1), angle = Angle(90, 0, 0), size = Vector(0.037, 0.037, 0.037), color = Color(106, 60, 0, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["handle"] = { type = "Model", model = "models/props_junk/popcan01a.mdl", bone = "ValveBiped.handle", rel = "", pos = Vector(0, 2.273, 0), angle = Angle(0, 0, -90), size = Vector(0.264, 0.264, 0.833), color = Color(82, 51, 0, 255), surpresslightning = false, material = "models/props_c17/chairchrome01", skin = 0, bodygroup = {} },
	["sightholder"] = { type = "Model", model = "models/props_c17/tv_monitor01.mdl", bone = "ValveBiped.base", rel = "cover", pos = Vector(-4.5, 0, 0.879), angle = Angle(0, 180, -90), size = Vector(0.2, 0.1, 0.11), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["handguard++"] = { type = "Model", model = "models/props_c17/furnitureshelf001b.mdl", bone = "ValveBiped.base", rel = "handguard", pos = Vector(0, 0, -0.201), angle = Angle(0, 0, 0), size = Vector(0.086, 0.109, 0.037), color = Color(78, 79, 70, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["clipholder2+"] = { type = "Model", model = "models/items/boxmrounds.mdl", bone = "ValveBiped.base", rel = "", pos = Vector(-0.456, 0, -4.092), angle = Angle(0, 31.704, -90), size = Vector(0.094, 0.209, 0.094), color = Color(179, 76, 0, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["sight length"] = { type = "Model", model = "models/props_junk/wood_pallet001a_chunka1.mdl", bone = "ValveBiped.base", rel = "sightholder", pos = Vector(-5, 0.4, 0), angle = Angle(90, -180, 90), size = Vector(0.264, 0.435, 0.151), color = Color(255, 255, 255, 255), surpresslightning = false, material = "models/props_c17/handrailmetal002a", skin = 0, bodygroup = {} },
	["stock"] = { type = "Model", model = "models/props_lab/pipesystem03a.mdl", bone = "ValveBiped.base", rel = "cover", pos = Vector(-10.455, 0, -0.456), angle = Angle(0, 90, 0), size = Vector(0.264, 0.321, 0.264), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["clip"] = { type = "Model", model = "models/items/boxsrounds.mdl", bone = "ValveBiped.Bip01", rel = "clipholder2", pos = Vector(1.363, -1.364, 0.455), angle = Angle(90, 5.113, 90), size = Vector(0.151, 0.264, 0.264), color = Color(69, 40, 0, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["top"] = { type = "Model", model = "models/props_c17/furniturewashingmachine001a.mdl", bone = "ValveBiped.base", rel = "", pos = Vector(0, 0.699, 10.454), angle = Angle(0, 90, 0), size = Vector(0.037, 0.037, 0.379), color = Color(176, 121, 0, 255), surpresslightning = false, material = "models/props_lab/ravendoor_sheet", skin = 0, bodygroup = {} },
	["clipholder2"] = { type = "Model", model = "models/items/boxmrounds.mdl", bone = "ValveBiped.base", rel = "", pos = Vector(0.455, 0, -4.092), angle = Angle(0, -31.705, -90), size = Vector(0.094, 0.209, 0.094), color = Color(179, 76, 0, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["handguard"] = { type = "Model", model = "models/props_c17/furnitureshelf001b.mdl", bone = "ValveBiped.base", rel = "", pos = Vector(0, 3.75, 3.181), angle = Angle(0, 0, 0), size = Vector(0.094, 0.119, 0.321), color = Color(153, 92, 0, 255), surpresslightning = false, material = "models/props_lab/ravendoor_sheet", skin = 0, bodygroup = {} },
	["sightcover"] = { type = "Model", model = "models/props_lab/partsbin01.mdl", bone = "ValveBiped.base", rel = "sightholder", pos = Vector(1.6, 0.3, 0), angle = Angle(180, 0, -90), size = Vector(0.037, 0.05, 0.05), color = Color(167, 109, 0, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["barrel"] = { type = "Model", model = "models/props_rooftop/chimneypipe01c.mdl", bone = "ValveBiped.muzzle", rel = "", pos = Vector(0, -0.201, 0), angle = Angle(-90, 0, 0), size = Vector(0.07, 0.07, 0.07), color = Color(255, 211, 0, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["handguard+++"] = { type = "Model", model = "models/props_c17/furnitureshelf001b.mdl", bone = "ValveBiped.base", rel = "handguard", pos = Vector(0, 2.273, -3.182), angle = Angle(0, 0, 91.023), size = Vector(0.086, 0.151, 0.264), color = Color(85, 82, 76, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["clip+"] = { type = "Model", model = "models/items/boxsrounds.mdl", bone = "ValveBiped.Bip01", rel = "clipholder2+", pos = Vector(-1.364, -1.364, 0.455), angle = Angle(90, 5.113, 90), size = Vector(0.151, 0.264, 0.264), color = Color(69, 40, 0, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["stock1"] = { type = "Model", model = "models/props_lab/powerbox01a.mdl", bone = "ValveBiped.base", rel = "stock", pos = Vector(0, -1.364, 0), angle = Angle(90, 0, -90), size = Vector(0.037, 0.037, 0.07), color = Color(147, 92, 0, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["handguard+"] = { type = "Model", model = "models/props_c17/furnitureshelf001b.mdl", bone = "ValveBiped.base", rel = "handguard", pos = Vector(0, 2.5, -3.182), angle = Angle(0, 0, 91.023), size = Vector(0.094, 0.151, 0.321), color = Color(141, 82, 0, 255), surpresslightning = false, material = "models/props_lab/ravendoor_sheet", skin = 0, bodygroup = {} },
	["cover"] = { type = "Model", model = "models/props_c17/briefcase001a.mdl", bone = "ValveBiped.base", rel = "", pos = Vector(0, -1.364, -0.456), angle = Angle(90, 0, -90), size = Vector(0.776, 0.209, 0.094), color = Color(172, 99, 0, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
}
SWEP.WElements = {
	["stock2"] = { type = "Model", model = "models/props_lab/reciever_cart.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "stock1", pos = Vector(-0.7, 0, 2.273), angle = Angle(90, 180, -180), size = Vector(0.05, 0.05, 0.037), color = Color(106, 60, 0, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["handle"] = { type = "Model", model = "models/props_junk/popcan01a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(14.399, 1.363, -3.182), angle = Angle(-7.159, -5.114, -180), size = Vector(0.435, 0.435, 0.776), color = Color(82, 51, 0, 255), surpresslightning = false, material = "models/props_c17/chairchrome01", skin = 0, bodygroup = {} },
	["sightholder"] = { type = "Model", model = "models/props_c17/tv_monitor01.mdl", bone = "ValveBiped.base", rel = "cover", pos = Vector(-4.5, 0, 0.879), angle = Angle(0, 180, -90), size = Vector(0.2, 0.1, 0.11), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["handguard++"] = { type = "Model", model = "models/props_c17/furnitureshelf001b.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "handguard", pos = Vector(0, 0, -0.101), angle = Angle(0, 0, 0), size = Vector(0.086, 0.109, 0.435), color = Color(78, 79, 70, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["clipholder2+"] = { type = "Model", model = "models/items/boxmrounds.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(3.181, 1.363, -4.092), angle = Angle(-138.068, -82.842, 11.25), size = Vector(0.094, 0.209, 0.094), color = Color(179, 76, 0, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["sight length"] = { type = "Model", model = "models/props_junk/wood_pallet001a_chunka1.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "sightholder", pos = Vector(-5, 0.4, 0), angle = Angle(90, -180, 90), size = Vector(0.264, 0.435, 0.151), color = Color(255, 255, 255, 255), surpresslightning = false, material = "models/props_c17/handrailmetal002a", skin = 0, bodygroup = {} },
	["stock"] = { type = "Model", model = "models/props_lab/pipesystem03a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "cover", pos = Vector(-10.455, 0, -0.456), angle = Angle(0, 90, 0), size = Vector(0.264, 0.321, 0.264), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["clip"] = { type = "Model", model = "models/items/boxsrounds.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "clipholder2", pos = Vector(2.273, -1.364, 0.455), angle = Angle(90, 0, 90), size = Vector(0.151, 0.264, 0.264), color = Color(69, 40, 0, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["cover"] = { type = "Model", model = "models/props_c17/briefcase001a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "barrel", pos = Vector(0.455, -0.101, -8.636), angle = Angle(90, -180, 0), size = Vector(0.776, 0.209, 0.094), color = Color(172, 99, 0, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["stock1"] = { type = "Model", model = "models/props_lab/powerbox01a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "stock", pos = Vector(0, -1.364, 0), angle = Angle(90, 0, -90), size = Vector(0.05, 0.05, 0.094), color = Color(147, 92, 0, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["handguard"] = { type = "Model", model = "models/props_c17/furnitureshelf001b.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(5.908, 1.2, 2.273), angle = Angle(0, 90, 11.25), size = Vector(0.094, 0.119, 0.321), color = Color(153, 92, 0, 255), surpresslightning = false, material = "models/props_lab/ravendoor_sheet", skin = 0, bodygroup = {} },
	["sightcover"] = { type = "Model", model = "models/props_lab/partsbin01.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "sightholder", pos = Vector(1.6, 0.3, 0), angle = Angle(180, 0, -90), size = Vector(0.037, 0.05, 0.05), color = Color(167, 109, 0, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["barrel"] = { type = "Model", model = "models/props_rooftop/chimneypipe01c.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(14.09, 1.5, -7.6), angle = Angle(-101, 0, 0), size = Vector(0.07, 0.07, 0.07), color = Color(255, 211, 0, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["handguard+"] = { type = "Model", model = "models/props_c17/furnitureshelf001b.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "handguard", pos = Vector(0, 2.5, -3.182), angle = Angle(0, 0, 91.023), size = Vector(0.094, 0.151, 0.321), color = Color(141, 82, 0, 255), surpresslightning = false, material = "models/props_lab/ravendoor_sheet", skin = 0, bodygroup = {} },
	["top"] = { type = "Model", model = "models/props_c17/furniturewashingmachine001a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "barrel", pos = Vector(-1.364, -0.101, -2.274), angle = Angle(0, -1.024, 0), size = Vector(0.037, 0.037, 0.379), color = Color(176, 121, 0, 255), surpresslightning = false, material = "models/props_lab/ravendoor_sheet", skin = 0, bodygroup = {} },
	["clipholder2"] = { type = "Model", model = "models/items/boxmrounds.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(3.181, 1.363, -4.092), angle = Angle(138.067, -99, 11.25), size = Vector(0.094, 0.209, 0.094), color = Color(179, 76, 0, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["handguard+++"] = { type = "Model", model = "models/props_c17/furnitureshelf001b.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "handguard", pos = Vector(0, 2.273, -3.182), angle = Angle(0, 0, 91.023), size = Vector(0.086, 0.151, 0.264), color = Color(85, 82, 76, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["clip+"] = { type = "Model", model = "models/items/boxsrounds.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "clipholder2+", pos = Vector(-2.274, -1.364, 0.455), angle = Angle(90, 0, 90), size = Vector(0.151, 0.264, 0.264), color = Color(69, 40, 11, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
}
SWEP.PrimaryReloadSound = Sound("Weapon_SMG1.Reload")

SWEP.AutoSwitchTo = false
SWEP.Slot = 0
SWEP.PrintName = "The Greaser"
SWEP.Author = "Spastik"
SWEP.Spawnable = true
SWEP.AutoSwitchFrom = false
SWEP.FiresUnderwater = false
SWEP.Weight = 5
SWEP.DrawCrosshair = true
SWEP.Category = "S&G Munitions"
SWEP.SlotPos = 3
SWEP.DrawAmmo = true
SWEP.ReloadSound = "weapons/smg1/smg1_reload.wav"
SWEP.Instructions = "Pull the trigger and make everything dirty."
SWEP.Contact = ""
SWEP.Purpose = "For giving things that are excessively clean a fine coat of grease and bullet holes."
SWEP.Base = "aa_base"
SWEP.HoldType = "smg"
SWEP.ViewModelFOV = 50.472440944882
SWEP.ViewModelFlip = false
SWEP.ViewModel = "models/weapons/v_smg1.mdl"
SWEP.WorldModel = "models/weapons/w_smg1.mdl"
SWEP.ShowViewModel = true
SWEP.ShowWorldModel = true
SWEP.ViewModelBoneMods = {
	["ValveBiped.handle"] = { scale = Vector(0.317, 0.317, 0.317), pos = Vector(0, 0, 0), angle = Angle(0, 0, 0) },
	["ValveBiped.Bip01"] = { scale = Vector(1, 1, 1), pos = Vector(0, 0, 0.952), angle = Angle(0, 0, 0) }
}
//General Variables\\

//Primary Fire Variables\\
SWEP.Primary.Sound = "weapons/ar1/ar1_dist1.wav"
SWEP.Primary.Damage = 35
SWEP.Primary.TakeAmmo = 1
SWEP.Primary.ClipSize = 80
SWEP.Primary.Ammo = "smg1"
SWEP.Primary.DefaultClip = 240
SWEP.Primary.Spread = 0.7
SWEP.Primary.NumberofShots = 1
SWEP.Primary.Automatic = true
SWEP.Primary.Recoil = 1
SWEP.Primary.Delay = 0.08
SWEP.Primary.Force = 0.6
//Primary Fire Variables\\

//Secondary Fire Variables\\
SWEP.Secondary.Ammo = "none"

Zoom=0
//Secondary Fire Variables\\
//SWEP:Initialize()\\
	function SWEP:Initialize()
    self:SetHoldType("smg1")
end
//SWEP:Initialize()\\

//SWEP:PrimaryFire()\\
function SWEP:PrimaryAttack()
	if ( !self:CanPrimaryAttack() ) then return end
	local bullet = {}
		bullet.Num = self.Primary.NumberofShots
		bullet.Src = self.Owner:GetShootPos()
		bullet.Dir = self.Owner:GetAimVector()
		bullet.Spread = Vector( self.Primary.Spread * 0.1 , self.Primary.Spread * 0.1, 0)
		bullet.Tracer = 1
		bullet.Force = self.Primary.Force
		bullet.Damage = self.Primary.Damage
		bullet.AmmoType = self.Primary.Ammo
	local rnda = self.Primary.Recoil * -1
	local rndb = self.Primary.Recoil * math.random(-1, 1)
	self:ShootEffects()
	self.Owner:FireBullets( bullet )
	self.Weapon:EmitSound("weapons/smg1/smg1_fire1.wav", 500, 140, 1, 0)
	self.Weapon:EmitSound("weapons/ar2/fire1.wav", 500, 140, 1, 0)
	self.Owner:ViewPunch( Angle( rnda,rndb,rnda ) )
	self:TakePrimaryAmmo(self.Primary.TakeAmmo)
	self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
	self.Weapon:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
end
//SWEP:PrimaryFire()\\

//SWEP:SecondaryFire()\\
function SWEP:SecondaryAttack()
end

function SWEP:Reload()

	self.Weapon:DefaultReload(ACT_VM_RELOAD)



		if (self.Weapon:Clip1() < self.Primary.ClipSize) and (self.Owner:GetAmmoCount(self.Primary.Ammo) > 0) then

		//self:SetIronsights(false)
		self:EmitSound( "weapons/ak47/ak47_clipout.wav", 500, 100, 1, 2 )
		self:EmitSound( self.PrimaryReloadSound, 500, 100, 1, 1 )

		if not (CLIENT) then

			self.Owner:DrawViewModel(true)

		end

	end
end
/********************************************************
	SWEP Construction Kit base code
		Created by Clavus
	Available for public use, thread at:
	   facepunch.com/threads/1032378
	   
	   
	DESCRIPTION:
		This script is meant for experienced scripters 
		that KNOW WHAT THEY ARE DOING. Don't come to me 
		with basic Lua questions.
		
		Just copy into your SWEP or SWEP base of choice
		and merge with your own code.
		
		The SWEP.VElements, SWEP.WElements and
		SWEP.ViewModelBoneMods tables are all optional
		and only have to be visible to the client.
********************************************************/

function SWEP:Initialize()

	// other initialize code goes here

	if CLIENT then

		self:SetHoldType( self.HoldType or "pistol" )
		// Create a new table for every weapon instance
		self.VElements = table.FullCopy( self.VElements )
		self.WElements = table.FullCopy( self.WElements )
		self.ViewModelBoneMods = table.FullCopy( self.ViewModelBoneMods )

		self:CreateModels(self.VElements) // create viewmodels
		self:CreateModels(self.WElements) // create worldmodels
		
		// init view model bone build function
		if IsValid(self.Owner) then
			local vm = self.Owner:GetViewModel()
			if IsValid(vm) then
				self:ResetBonePositions(vm)
				
				--[[// Init viewmodel visibility
				if (self.ShowViewModel == nil or self.ShowViewModel) then
					vm:SetColor(Color(255,255,255,255))
				else
					// we set the alpha to 1 instead of 0 because else ViewModelDrawn stops being called
					vm:SetColor(Color(255,255,255,1))
					// ^ stopped working in GMod 13 because you have to do Entity:SetRenderMode(1) for translucency to kick in
					// however for some reason the view model resets to render mode 0 every frame so we just apply a debug material to prevent it from drawing
					vm:SetMaterial("Debug/hsv")			
				end]]--
			end
		end
	end
end
function SWEP:Holster()
	
	if CLIENT and IsValid(self.Owner) then
		local vm = self.Owner:GetViewModel()
		if IsValid(vm) then
			self:ResetBonePositions(vm)
		end
	end
	
	return true
end

function SWEP:OnRemove()
	self:Holster()
end

if CLIENT then

	SWEP.vRenderOrder = nil
	function SWEP:ViewModelDrawn()
		
		local vm = self.Owner:GetViewModel()
		if !IsValid(vm) then return end
		
		if (!self.VElements) then return end
		
		self:UpdateBonePositions(vm)

		if (!self.vRenderOrder) then
			
			// we build a render order because sprites need to be drawn after models
			self.vRenderOrder = {}

			for k, v in pairs( self.VElements ) do
				if (v.type == "Model") then
					table.insert(self.vRenderOrder, 1, k)
				elseif (v.type == "Sprite" or v.type == "Quad") then
					table.insert(self.vRenderOrder, k)
				end
			end
			
		end

		for k, name in ipairs( self.vRenderOrder ) do
		
			local v = self.VElements[name]
			if (!v) then self.vRenderOrder = nil break end
			if (v.hide) then continue end
			
			local model = v.modelEnt
			local sprite = v.spriteMaterial
			
			if (!v.bone) then continue end
			
			local pos, ang = self:GetBoneOrientation( self.VElements, v, vm )
			
			if (!pos) then continue end
			
			if (v.type == "Model" and IsValid(model)) then

				model:SetPos(pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z )
				ang:RotateAroundAxis(ang:Up(), v.angle.y)
				ang:RotateAroundAxis(ang:Right(), v.angle.p)
				ang:RotateAroundAxis(ang:Forward(), v.angle.r)

				model:SetAngles(ang)
				//model:SetModelScale(v.size)
				local matrix = Matrix()
				matrix:Scale(v.size)
				model:EnableMatrix( "RenderMultiply", matrix )
				
				if (v.material == "") then
					model:SetMaterial("")
				elseif (model:GetMaterial() != v.material) then
					model:SetMaterial( v.material )
				end
				
				if (v.skin and v.skin != model:GetSkin()) then
					model:SetSkin(v.skin)
				end
				
				if (v.bodygroup) then
					for k, v in pairs( v.bodygroup ) do
						if (model:GetBodygroup(k) != v) then
							model:SetBodygroup(k, v)
						end
					end
				end
				
				if (v.surpresslightning) then
					render.SuppressEngineLighting(true)
				end
				
				render.SetColorModulation(v.color.r/255, v.color.g/255, v.color.b/255)
				render.SetBlend(v.color.a/255)
				model:DrawModel()
				render.SetBlend(1)
				render.SetColorModulation(1, 1, 1)
				
				if (v.surpresslightning) then
					render.SuppressEngineLighting(false)
				end
				
			elseif (v.type == "Sprite" and sprite) then
				
				local drawpos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
				render.SetMaterial(sprite)
				render.DrawSprite(drawpos, v.size.x, v.size.y, v.color)
				
			elseif (v.type == "Quad" and v.draw_func) then
				
				local drawpos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
				ang:RotateAroundAxis(ang:Up(), v.angle.y)
				ang:RotateAroundAxis(ang:Right(), v.angle.p)
				ang:RotateAroundAxis(ang:Forward(), v.angle.r)
				
				cam.Start3D2D(drawpos, ang, v.size)
					v.draw_func( self )
				cam.End3D2D()

			end
			
		end
		
	end

	SWEP.wRenderOrder = nil
	function SWEP:DrawWorldModel()
		
		if (self.ShowWorldModel == nil or self.ShowWorldModel) then
			self:DrawModel()
		end
		
		if (!self.WElements) then return end
		
		if (!self.wRenderOrder) then

			self.wRenderOrder = {}

			for k, v in pairs( self.WElements ) do
				if (v.type == "Model") then
					table.insert(self.wRenderOrder, 1, k)
				elseif (v.type == "Sprite" or v.type == "Quad") then
					table.insert(self.wRenderOrder, k)
				end
			end

		end
		
		if (IsValid(self.Owner)) then
			bone_ent = self.Owner
		else
			// when the weapon is dropped
			bone_ent = self
		end
		
		for k, name in pairs( self.wRenderOrder ) do
		
			local v = self.WElements[name]
			if (!v) then self.wRenderOrder = nil break end
			if (v.hide) then continue end
			
			local pos, ang
			
			if (v.bone) then
				pos, ang = self:GetBoneOrientation( self.WElements, v, bone_ent )
			else
				pos, ang = self:GetBoneOrientation( self.WElements, v, bone_ent, "ValveBiped.Bip01_R_Hand" )
			end
			
			if (!pos) then continue end
			
			local model = v.modelEnt
			local sprite = v.spriteMaterial
			
			if (v.type == "Model" and IsValid(model)) then

				model:SetPos(pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z )
				ang:RotateAroundAxis(ang:Up(), v.angle.y)
				ang:RotateAroundAxis(ang:Right(), v.angle.p)
				ang:RotateAroundAxis(ang:Forward(), v.angle.r)

				model:SetAngles(ang)
				//model:SetModelScale(v.size)
				local matrix = Matrix()
				matrix:Scale(v.size)
				model:EnableMatrix( "RenderMultiply", matrix )
				
				if (v.material == "") then
					model:SetMaterial("")
				elseif (model:GetMaterial() != v.material) then
					model:SetMaterial( v.material )
				end
				
				if (v.skin and v.skin != model:GetSkin()) then
					model:SetSkin(v.skin)
				end
				
				if (v.bodygroup) then
					for k, v in pairs( v.bodygroup ) do
						if (model:GetBodygroup(k) != v) then
							model:SetBodygroup(k, v)
						end
					end
				end
				
				if (v.surpresslightning) then
					render.SuppressEngineLighting(true)
				end
				
				render.SetColorModulation(v.color.r/255, v.color.g/255, v.color.b/255)
				render.SetBlend(v.color.a/255)
				model:DrawModel()
				render.SetBlend(1)
				render.SetColorModulation(1, 1, 1)
				
				if (v.surpresslightning) then
					render.SuppressEngineLighting(false)
				end
				
			elseif (v.type == "Sprite" and sprite) then
				
				local drawpos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
				render.SetMaterial(sprite)
				render.DrawSprite(drawpos, v.size.x, v.size.y, v.color)
				
			elseif (v.type == "Quad" and v.draw_func) then
				
				local drawpos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
				ang:RotateAroundAxis(ang:Up(), v.angle.y)
				ang:RotateAroundAxis(ang:Right(), v.angle.p)
				ang:RotateAroundAxis(ang:Forward(), v.angle.r)
				
				cam.Start3D2D(drawpos, ang, v.size)
					v.draw_func( self )
				cam.End3D2D()

			end
			
		end
		
	end

	function SWEP:GetBoneOrientation( basetab, tab, ent, bone_override )
		
		local bone, pos, ang
		if (tab.rel and tab.rel != "") then
			
			local v = basetab[tab.rel]
			
			if (!v) then return end
			
			// Technically, if there exists an element with the same name as a bone
			// you can get in an infinite loop. Let's just hope nobody's that stupid.
			pos, ang = self:GetBoneOrientation( basetab, v, ent )
			
			if (!pos) then return end
			
			pos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
			ang:RotateAroundAxis(ang:Up(), v.angle.y)
			ang:RotateAroundAxis(ang:Right(), v.angle.p)
			ang:RotateAroundAxis(ang:Forward(), v.angle.r)
				
		else
		
			bone = ent:LookupBone(bone_override or tab.bone)

			if (!bone) then return end
			
			pos, ang = Vector(0,0,0), Angle(0,0,0)
			local m = ent:GetBoneMatrix(bone)
			if (m) then
				pos, ang = m:GetTranslation(), m:GetAngles()
			end
			
			if (IsValid(self.Owner) and self.Owner:IsPlayer() and 
				ent == self.Owner:GetViewModel() and self.ViewModelFlip) then
				ang.r = -ang.r // Fixes mirrored models
			end
		
		end
		
		return pos, ang
	end

	function SWEP:CreateModels( tab )

		if (!tab) then return end

		// Create the clientside models here because Garry says we can't do it in the render hook
		for k, v in pairs( tab ) do
			if (v.type == "Model" and v.model and v.model != "" and (!IsValid(v.modelEnt) or v.createdModel != v.model) and 
					string.find(v.model, ".mdl") and file.Exists (v.model, "GAME") ) then
				
				v.modelEnt = ClientsideModel(v.model, RENDER_GROUP_VIEW_MODEL_OPAQUE)
				if (IsValid(v.modelEnt)) then
					v.modelEnt:SetPos(self:GetPos())
					v.modelEnt:SetAngles(self:GetAngles())
					v.modelEnt:SetParent(self)
					v.modelEnt:SetNoDraw(true)
					v.createdModel = v.model
				else
					v.modelEnt = nil
				end
				
			elseif (v.type == "Sprite" and v.sprite and v.sprite != "" and (!v.spriteMaterial or v.createdSprite != v.sprite) 
				and file.Exists ("materials/"..v.sprite..".vmt", "GAME")) then
				
				local name = v.sprite.."-"
				local params = { ["$basetexture"] = v.sprite }
				// make sure we create a unique name based on the selected options
				local tocheck = { "nocull", "additive", "vertexalpha", "vertexcolor", "ignorez" }
				for i, j in pairs( tocheck ) do
					if (v[j]) then
						params["$"..j] = 1
						name = name.."1"
					else
						name = name.."0"
					end
				end

				v.createdSprite = v.sprite
				v.spriteMaterial = CreateMaterial(name,"UnlitGeneric",params)
				
			end
		end
		
	end
	
	local allbones
	local hasGarryFixedBoneScalingYet = false

	function SWEP:UpdateBonePositions(vm)
		
		if self.ViewModelBoneMods then
			
			if (!vm:GetBoneCount()) then return end
			
			// !! WORKAROUND !! //
			// We need to check all model names :/
			local loopthrough = self.ViewModelBoneMods
			if (!hasGarryFixedBoneScalingYet) then
				allbones = {}
				for i=0, vm:GetBoneCount() do
					local bonename = vm:GetBoneName(i)
					if (self.ViewModelBoneMods[bonename]) then 
						allbones[bonename] = self.ViewModelBoneMods[bonename]
					else
						allbones[bonename] = { 
							scale = Vector(1,1,1),
							pos = Vector(0,0,0),
							angle = Angle(0,0,0)
						}
					end
				end
				
				loopthrough = allbones
			end
			// !! ----------- !! //
			
			for k, v in pairs( loopthrough ) do
				local bone = vm:LookupBone(k)
				if (!bone) then continue end
				
				// !! WORKAROUND !! //
				local s = Vector(v.scale.x,v.scale.y,v.scale.z)
				local p = Vector(v.pos.x,v.pos.y,v.pos.z)
				local ms = Vector(1,1,1)
				if (!hasGarryFixedBoneScalingYet) then
					local cur = vm:GetBoneParent(bone)
					while(cur >= 0) do
						local pscale = loopthrough[vm:GetBoneName(cur)].scale
						ms = ms * pscale
						cur = vm:GetBoneParent(cur)
					end
				end
				
				s = s * ms
				// !! ----------- !! //
				
				if vm:GetManipulateBoneScale(bone) != s then
					vm:ManipulateBoneScale( bone, s )
				end
				if vm:GetManipulateBoneAngles(bone) != v.angle then
					vm:ManipulateBoneAngles( bone, v.angle )
				end
				if vm:GetManipulateBonePosition(bone) != p then
					vm:ManipulateBonePosition( bone, p )
				end
			end
		else
			self:ResetBonePositions(vm)
		end
		   
	end
	 
	function SWEP:ResetBonePositions(vm)
		
		if (!vm:GetBoneCount()) then return end
		for i=0, vm:GetBoneCount() do
			vm:ManipulateBoneScale( i, Vector(1, 1, 1) )
			vm:ManipulateBoneAngles( i, Angle(0, 0, 0) )
			vm:ManipulateBonePosition( i, Vector(0, 0, 0) )
		end
		
	end

	/**************************
		Global utility code
	**************************/

	// Fully copies the table, meaning all tables inside this table are copied too and so on (normal table.Copy copies only their reference).
	// Does not copy entities of course, only copies their reference.
	// WARNING: do not use on tables that contain themselves somewhere down the line or you'll get an infinite loop
	function table.FullCopy( tab )

		if (!tab) then return nil end
		
		local res = {}
		for k, v in pairs( tab ) do
			if (type(v) == "table") then
				res[k] = table.FullCopy(v) // recursion ho!
			elseif (type(v) == "Vector") then
				res[k] = Vector(v.x, v.y, v.z)
			elseif (type(v) == "Angle") then
				res[k] = Angle(v.p, v.y, v.r)
			else
				res[k] = v
			end
		end
		
		return res
		
	end
	
end

