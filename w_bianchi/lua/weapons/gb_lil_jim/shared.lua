//General Variables\\
SWEP.AdminSpawnable = true
SWEP.ViewModelFOV = 64
SWEP.VElements = {
	["FRONTSPIN"] = { type = "Model", model = "models/props_c17/oildrum001.mdl", bone = "Base", rel = "", pos = Vector(0, 2.7, 21.364), angle = Angle(-180, 0, 0), size = Vector(0.428, 0.428, 0.037), color = Color(40, 40, 40, 255), surpresslightning = false, material = "debug/debugambientcube", skin = 0, bodygroup = {} },
	["ammocage"] = { type = "Model", model = "models/props_junk/PlasticCrate01a.mdl", bone = "Base", rel = "barrelholder", pos = Vector(0, -8.636, 5), angle = Angle(0, 0, 90), size = Vector(0.5, 0.5, 0.5), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["barrel+"] = { type = "Model", model = "models/props_junk/propane_tank001a.mdl", bone = "Base", rel = "FRONTSPIN", pos = Vector(2.299, 3.181, -18.636), angle = Angle(0, 0, 0), size = Vector(0.379, 0.379, 1.684), color = Color(255, 255, 255, 255), surpresslightning = false, material = "debug/env_cubemap_model", skin = 0, bodygroup = {} },
	["barrel+++"] = { type = "Model", model = "models/props_junk/propane_tank001a.mdl", bone = "Base", rel = "FRONTSPIN", pos = Vector(-4.092, -0.456, -18.636), angle = Angle(0, 0, 0), size = Vector(0.379, 0.379, 1.684), color = Color(255, 255, 255, 255), surpresslightning = false, material = "debug/env_cubemap_model", skin = 0, bodygroup = {} },
	["barrelholder"] = { type = "Model", model = "models/props_wasteland/laundry_basket001.mdl", bone = "Base", rel = "", pos = Vector(0, 2.9, 11.364), angle = Angle(0, 0, -180), size = Vector(0.234, 0.234, 0.435), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["barreldisc+"] = { type = "Model", model = "models/props_c17/oildrum001.mdl", bone = "Base", rel = "FRONTSPIN", pos = Vector(0, 0, -34.091), angle = Angle(0, 0, 0), size = Vector(0.428, 0.428, 0.009), color = Color(40, 40, 40, 255), surpresslightning = false, material = "debug/debugambientcube", skin = 0, bodygroup = {} },
	["back"] = { type = "Model", model = "models/props_vehicles/apc_tire001.mdl", bone = "Base", rel = "connect", pos = Vector(0, 0, 0), angle = Angle(0, -90, 0), size = Vector(0.17, 0.17, 0.17), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["barreldisc++"] = { type = "Model", model = "models/props_c17/oildrum001.mdl", bone = "Base", rel = "FRONTSPIN", pos = Vector(0, 0, -28.636), angle = Angle(0, 0, 0), size = Vector(0.428, 0.428, 0.009), color = Color(40, 40, 40, 255), surpresslightning = false, material = "debug/debugambientcube", skin = 0, bodygroup = {} },
	["barreldisc"] = { type = "Model", model = "models/props_c17/oildrum001.mdl", bone = "Base", rel = "FRONTSPIN", pos = Vector(0, 0, -40.456), angle = Angle(0, 0, 0), size = Vector(0.428, 0.428, 0.009), color = Color(40, 40, 40, 255), surpresslightning = false, material = "debug/debugambientcube", skin = 0, bodygroup = {} },
	["motor"] = { type = "Model", model = "models/props_c17/TrapPropeller_Engine.mdl", bone = "Base", rel = "", pos = Vector(1, 4.091, -10.455), angle = Angle(-180, 90, 180), size = Vector(0.379, 0.379, 0.379), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["connect"] = { type = "Model", model = "models/props_lab/tpplug.mdl", bone = "Base", rel = "hider", pos = Vector(0, -6.818, 0), angle = Angle(-180, 90, 0), size = Vector(1.498, 1.498, 1.498), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["barrel+++++"] = { type = "Model", model = "models/props_junk/propane_tank001a.mdl", bone = "Base", rel = "FRONTSPIN", pos = Vector(2.299, -3.182, -18.636), angle = Angle(0, 0, 0), size = Vector(0.379, 0.379, 1.684), color = Color(255, 255, 255, 255), surpresslightning = false, material = "debug/env_cubemap_model", skin = 0, bodygroup = {} },
	["barrel++++"] = { type = "Model", model = "models/props_junk/propane_tank001a.mdl", bone = "Base", rel = "FRONTSPIN", pos = Vector(-2.3, -3.182, -18.636), angle = Angle(0, 0, 0), size = Vector(0.379, 0.379, 1.684), color = Color(255, 255, 255, 255), surpresslightning = false, material = "debug/env_cubemap_model", skin = 0, bodygroup = {} },
	["barrel"] = { type = "Model", model = "models/props_junk/propane_tank001a.mdl", bone = "Base", rel = "FRONTSPIN", pos = Vector(-2.3, 3.181, -18.636), angle = Angle(0, 0, 0), size = Vector(0.379, 0.379, 1.684), color = Color(255, 255, 255, 255), surpresslightning = false, material = "debug/env_cubemap_model", skin = 0, bodygroup = {} },
	["handle"] = { type = "Model", model = "models/Items/combine_rifle_cartridge01.mdl", bone = "Base", rel = "back", pos = Vector(0, 13.182, -1.364), angle = Angle(-180, 90, 0), size = Vector(0.889, 0.889, 0.889), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["box"] = { type = "Model", model = "models/Items/BoxMRounds.mdl", bone = "Base", rel = "ammocage", pos = Vector(0, 0, -3.182), angle = Angle(0, 0, 0), size = Vector(0.606, 0.606, 0.492), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["hider"] = { type = "Model", model = "models/props_c17/streetsign004e.mdl", bone = "Base", rel = "barrelholder", pos = Vector(0, 0, 10.454), angle = Angle(0, 0, 90), size = Vector(0.499, 0.499, 0.499), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["barrel++"] = { type = "Model", model = "models/props_junk/propane_tank001a.mdl", bone = "Base", rel = "FRONTSPIN", pos = Vector(4.091, -0.456, -18.636), angle = Angle(0, 0, 0), size = Vector(0.379, 0.379, 1.684), color = Color(255, 255, 255, 255), surpresslightning = false, material = "debug/env_cubemap_model", skin = 0, bodygroup = {} },
	["trigger"] = { type = "Model", model = "models/props_wasteland/panel_leverHandle001a.mdl", bone = "Base", rel = "handle", pos = Vector(1.363, 0, 3.7), angle = Angle(170.794, 0, 0), size = Vector(0.151, 0.094, 0.151), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
}
SWEP.WElements = {
	["handle"] = { type = "Model", model = "models/Items/combine_rifle_cartridge01.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "back", pos = Vector(0, 5.908, -3.182), angle = Angle(-142.159, 90, 0), size = Vector(0.72, 0.72, 0.72), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["ammocage"] = { type = "Model", model = "models/props_junk/PlasticCrate01a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "barrelholder", pos = Vector(5.908, 1.363, 7.727), angle = Angle(0, -100, 90), size = Vector(0.435, 0.435, 0.435), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["barrel+"] = { type = "Model", model = "models/props_junk/propane_tank001a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "FRONTSPIN", pos = Vector(-2, -1.601, -8.636), angle = Angle(0, 0, 0), size = Vector(0.264, 0.264, 0.776), color = Color(255, 255, 255, 255), surpresslightning = false, material = "debug/env_cubemap_model", skin = 0, bodygroup = {} },
	["hole+"] = { type = "Model", model = "models/props_c17/clock01.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "barrel+", pos = Vector(0, 0, -12.273), angle = Angle(180, 0, 0), size = Vector(0.123, 0.123, 0.009), color = Color(0, 0, 0, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["hole+++++"] = { type = "Model", model = "models/props_c17/clock01.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "barrel++++", pos = Vector(0, 0, -12.273), angle = Angle(180, 0, 0), size = Vector(0.123, 0.123, 0.009), color = Color(0, 0, 0, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["hole+++"] = { type = "Model", model = "models/props_c17/clock01.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "barrel+++++", pos = Vector(0, 0, -12.273), angle = Angle(180, 0, 0), size = Vector(0.123, 0.123, 0.009), color = Color(0, 0, 0, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["trigger"] = { type = "Model", model = "models/props_wasteland/panel_leverHandle001a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "handle", pos = Vector(2, -0.051, 2.273), angle = Angle(170.794, 0, 0), size = Vector(0.209, 0.094, 0.209), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["hole"] = { type = "Model", model = "models/props_c17/clock01.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "barrel", pos = Vector(0, 0, -12.273), angle = Angle(180, 0, 0), size = Vector(0.123, 0.123, 0.009), color = Color(0, 0, 0, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["barreldisc++"] = { type = "Model", model = "models/props_c17/oildrum001.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "FRONTSPIN", pos = Vector(0, 0, -16.819), angle = Angle(0, 0, 0), size = Vector(0.33, 0.33, 0.009), color = Color(40, 40, 40, 255), surpresslightning = false, material = "debug/debugambientcube", skin = 0, bodygroup = {} },
	["hole++++"] = { type = "Model", model = "models/props_c17/clock01.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "barrel+++", pos = Vector(0, 0, -12.273), angle = Angle(180, 0, 0), size = Vector(0.123, 0.123, 0.009), color = Color(0, 0, 0, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["barrel+++"] = { type = "Model", model = "models/props_junk/propane_tank001a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "FRONTSPIN", pos = Vector(2.4, 1.6, -8.636), angle = Angle(0, 0, 0), size = Vector(0.264, 0.264, 0.776), color = Color(255, 255, 255, 255), surpresslightning = false, material = "debug/env_cubemap_model", skin = 0, bodygroup = {} },
	["barreldisc"] = { type = "Model", model = "models/props_c17/oildrum001.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "FRONTSPIN", pos = Vector(0, 0, -20.455), angle = Angle(0, 0, 0), size = Vector(0.33, 0.33, 0.009), color = Color(40, 40, 40, 255), surpresslightning = false, material = "debug/debugambientcube", skin = 0, bodygroup = {} },
	["barrel++++"] = { type = "Model", model = "models/props_junk/propane_tank001a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "FRONTSPIN", pos = Vector(0, 3.181, -8.636), angle = Angle(0, 0, 0), size = Vector(0.264, 0.264, 0.776), color = Color(255, 255, 255, 255), surpresslightning = false, material = "debug/env_cubemap_model", skin = 0, bodygroup = {} },
	["box"] = { type = "Model", model = "models/Items/BoxMRounds.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "ammocage", pos = Vector(0, 0, -2.274), angle = Angle(0, 0, 0), size = Vector(0.492, 0.549, 0.379), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["motor"] = { type = "Model", model = "models/props_c17/TrapPropeller_Engine.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "back", pos = Vector(1, 3.181, 0.455), angle = Angle(90, -90, 0), size = Vector(0.264, 0.264, 0.264), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["hider"] = { type = "Model", model = "models/props_c17/streetsign004e.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "barrelholder", pos = Vector(0, 0, 3.779), angle = Angle(0, 0, 90), size = Vector(0.365, 0.365, 0.365), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["connect"] = { type = "Model", model = "models/props_lab/tpplug.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "hider", pos = Vector(0, -5, 0), angle = Angle(-180, 90, 101.25), size = Vector(1.049, 1.049, 1.049), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["hole++"] = { type = "Model", model = "models/props_c17/clock01.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "barrel++", pos = Vector(0, 0, -12.273), angle = Angle(180, 0, 0), size = Vector(0.123, 0.123, 0.009), color = Color(0, 0, 0, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["FRONTSPIN"] = { type = "Model", model = "models/props_c17/oildrum001.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(23, -3.182, -5), angle = Angle(-86.932, -172.842, -5), size = Vector(0.321, 0.321, 0.037), color = Color(40, 40, 40, 255), surpresslightning = false, material = "debug/debugambientcube", skin = 0, bodygroup = {} },
	["barrel"] = { type = "Model", model = "models/props_junk/propane_tank001a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "FRONTSPIN", pos = Vector(-2.274, 1.1, -8.636), angle = Angle(0, 0, 0), size = Vector(0.264, 0.264, 0.776), color = Color(255, 255, 255, 255), surpresslightning = false, material = "debug/env_cubemap_model", skin = 0, bodygroup = {} },
	["barrel+++++"] = { type = "Model", model = "models/props_junk/propane_tank001a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "FRONTSPIN", pos = Vector(0.6, -3.182, -8.636), angle = Angle(0, 0, 0), size = Vector(0.264, 0.264, 0.776), color = Color(255, 255, 255, 255), surpresslightning = false, material = "debug/env_cubemap_model", skin = 0, bodygroup = {} },
	["barreldisc+"] = { type = "Model", model = "models/props_c17/oildrum001.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "FRONTSPIN", pos = Vector(0, 0, -18.636), angle = Angle(0, 0, 0), size = Vector(0.33, 0.33, 0.009), color = Color(40, 40, 40, 255), surpresslightning = false, material = "debug/debugambientcube", skin = 0, bodygroup = {} },
	["back"] = { type = "Model", model = "models/props_vehicles/apc_tire001.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "connect", pos = Vector(-0.456, 0, 1.363), angle = Angle(0, -90, 0), size = Vector(0.135, 0.135, 0.135), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["barrel++"] = { type = "Model", model = "models/props_junk/propane_tank001a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "FRONTSPIN", pos = Vector(2.799, -1.111, -8.636), angle = Angle(0, 0, 0), size = Vector(0.264, 0.264, 0.776), color = Color(255, 255, 255, 255), surpresslightning = false, material = "debug/env_cubemap_model", skin = 0, bodygroup = {} },
	["barrelholder"] = { type = "Model", model = "models/props_wasteland/laundry_basket001.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(17.726, -2.201, -5), angle = Angle(86.931, 3.068, 7.158), size = Vector(0.166, 0.166, 0.166), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["Backpack"] = { type = "Model", model = "models/Items/BoxMRounds.mdl", bone = "ValveBiped.Bip01_Spine2", rel = "", pos = Vector(2.94, 2.94, -0.588), angle = Angle(93.971, 91.323, 1.324), size = Vector(1.075, 0.855, 0.34), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
}
SWEP.AutoSwitchTo = false
SWEP.Slot = 0
SWEP.PrintName = "Lil' Jim"
SWEP.Author = "Spastik/SAGA_"
SWEP.Spawnable = true
SWEP.AutoSwitchFrom = false
SWEP.FiresUnderwater = false
SWEP.Weight = 5
SWEP.DrawCrosshair = true
SWEP.Category = "S&G Munitions"
SWEP.SlotPos = 1
SWEP.DrawAmmo = true
SWEP.ReloadSound = "Weapon_Pistol.Reload"
SWEP.Instructions = "Pull the trigger to break Jim's parole and send him back to therapy."
SWEP.Contact = ""
SWEP.Purpose = "He's an angry lil' fellow, isn't he?"
SWEP.Base = "aa_base"
SWEP.HoldType = "physgun"
SWEP.ViewModelFOV = 65
SWEP.ViewModelFlip = false
SWEP.ViewModel = "models/weapons/v_physcannon.mdl"
SWEP.WorldModel = "models/weapons/w_physics.mdl"
SWEP.ShowViewModel = true
SWEP.ShowWorldModel = true
SWEP.HoldType = "physgun"
SWEP.ViewModelFOV = 54.251968503937
SWEP.ViewModelFlip = false
SWEP.ViewModel = "models/weapons/v_physcannon.mdl"
SWEP.WorldModel = "models/weapons/w_physics.mdl"
SWEP.ShowViewModel = true
SWEP.ShowWorldModel = true
SWEP.ViewModelBoneMods = {
	["Prong_B"] = { scale = Vector(0.009, 0.009, 0.009), pos = Vector(0, 0, 0), angle = Angle(0, 0, 0) },
	["Doodad_1"] = { scale = Vector(0.009, 0.009, 0.009), pos = Vector(-9.841, 3.174, 0), angle = Angle(0, 0, 0) },
	["Prong_A"] = { scale = Vector(0.009, 0.009, 0.009), pos = Vector(0, 0, 0), angle = Angle(0, 0, 0) },
	["Doodad_2"] = { scale = Vector(0.009, 0.009, 0.009), pos = Vector(0, 0, 0), angle = Angle(0, 0, 0) },
	["Base"] = { scale = Vector(1, 1, 1), pos = Vector(0, 0.634, 0), angle = Angle(0, 0, 0) },
	["Doodad_3"] = { scale = Vector(0.009, 0.009, 0.009), pos = Vector(0, 0, 0), angle = Angle(0, 0, 0) },
	["Doodad_4"] = { scale = Vector(0.009, 0.009, 0.009), pos = Vector(0, -6.349, 0), angle = Angle(0, 0, 0) }
}
//General Variables\\

//Primary Fire Variables\\
SWEP.Primary.Sound = "Weapon_SMG1.Single"
SWEP.Primary.Damage = 15
SWEP.Primary.TakeAmmo = 1
SWEP.Primary.ClipSize = 100
SWEP.Primary.Ammo = "SMG1"
SWEP.Primary.DefaultClip = 500
SWEP.Primary.Spread = 0.36
SWEP.Primary.NumberofShots = 1
SWEP.Primary.Automatic = true
SWEP.Primary.Recoil = 0.25
SWEP.Primary.Delay = 0.05
SWEP.Primary.Force = 0.5
//Primary Fire Variables\\

//Secondary Fire Variables\\
SWEP.Secondary.Ammo = "none"
//Secondary Fire Variables\\

		
//SWEP:Initialize()\\
function SWEP:Initialize()
	util.PrecacheSound(self.Primary.Sound)
	util.PrecacheSound(self.Secondary.Sound)
	if ( SERVER ) then
		self:SetHoldType( self.HoldType or "pistol" )
	end
end
//SWEP:Initialize()\\

local SpinAng = 0
local SpinAccel = 0

/*

function SWEP:OnThink()
	if (CLIENT) then
		if(self.Owner:KeyDown(IN_ATTACK) && (self:Clip1() > 0)) then
			SpinAccel = math.Approach(SpinAccel, 1000, 100*FrameTime())
			SpinAng = SpinAng + SpinAccel
			self.VElements["FRONTSPIN"].angle.y = SpinAng
		else
			SpinAccel = math.Approach(SpinAccel, 0, 50*FrameTime())
		end
	end
end
*/

function SWEP:Think()
	self:OnThink() --remember spastik, Think has to be called :3
end

function SWEP:OnThink()
    self.Reloading = self.Reloading or false
    if(self.Owner:KeyDown(IN_ATTACK2) and !self.Reloading) then
	   SpinAccel = math.Approach(SpinAccel, 12, 10*FrameTime())
	else
	   SpinAccel = math.Approach(SpinAccel, 0, 2*FrameTime())
	end
	if (CLIENT) then
		SpinAng = SpinAng + SpinAccel
		self.VElements["FRONTSPIN"].angle.y = SpinAng
		--self.WElements["FRONTSPIN"].angle.y = SpinAng -- removed since it makes worldmodel go like if it were singing chacarron macarron
	end
	self:SetNWInt( "Spin",SpinAccel )
end

function SWEP:Reload()
         self.NextReload = self.NextReload or 0
         if ( self.NextReload > CurTime() ) then return end
         if ( self:Clip1() ~= self.Primary.ClipSize and self:Ammo1() ~= 0 ) then
		      self:SendWeaponAnim( ACT_VM_HOLSTER )
			  self:SetNWInt( "Spin",0 )
			  self:SetNextPrimaryFire( CurTime() + 1.25 )
			  self:SetNextSecondaryFire( CurTime() + 1.25 )
			  self.NextReload = CurTime() + 1.25
			  self.Reloading = true
			  self:EmitSound( "weapons/m249/m249_boxout.wav", 500, 100, 1, 0)
			  self:EmitSound( "weapons/sg552/sg552_boltpull.wav", 500, 100, 1, -1)
			  timer.Simple( 2.3,function()
			        if ( IsValid( self ) ) then
                         self:DefaultReload( ACT_VM_DRAW )
						 self.Reloading = false
						self:EmitSound( "weapons/m249/m249_boxin.wav", 100, 80, 1, -1) 
						self:EmitSound( "weapons/m249/m249_chain.wav", 100, 80, 1, 0 )
							//self:SetIronsights(false)
					end
			  end )
		 end
end


//SWEP:PrimaryFire()\\
function SWEP:PrimaryAttack()
	if ( !self:CanPrimaryAttack() ) then return end
	if ( self:GetNWInt( "Spin" ) ~= 12 ) then return end
	local bullet = {}
		bullet.Num = self.Primary.NumberofShots
		bullet.Src = self.Owner:GetShootPos()
		bullet.Dir = self.Owner:GetAimVector()
		bullet.Spread = Vector( self.Primary.Spread * 0.1 , self.Primary.Spread * 0.1, 0)
		bullet.Tracer = 1
		bullet.Force = self.Primary.Force
		bullet.Damage = self.Primary.Damage
		bullet.AmmoType = self.Primary.Ammo
	local rnda = self.Primary.Recoil * math.random(-1, 1)
	local rndb = self.Primary.Recoil * math.random(-1, 1)
	self:ShootEffects()
	self.Owner:FireBullets( bullet )
	self.Weapon:EmitSound( self.Primary.Sound,500,50 )
	self.Weapon:EmitSound( "weapons/shotgun/shotgun_fire7.wav", 500, 100, 1, -1 )
	self.Weapon:EmitSound( "weapons/smg1/smg1_fire1.wav", 500, 100, 1, 1 )
	self.Owner:ViewPunch( Angle( rnda,rndb,rnda ) )
	self:SetClip1( self:Clip1() - 1 )
	self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
	self.Weapon:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
end

//SWEP:PrimaryFire()\\

//SWEP:SecondaryFire()\\
function SWEP:SecondaryAttack()
end
/********************************************************
	SWEP Construction Kit base code
		Created by Clavus
	Available for public use, thread at:
	   facepunch.com/threads/1032378
	   
	   
	DESCRIPTION:
		This script is meant for experienced scripters 
		that KNOW WHAT THEY ARE DOING. Don't come to me 
		with basic Lua questions.
		
		Just copy into your SWEP or SWEP base of choice
		and merge with your own code.
		
		The SWEP.VElements, SWEP.WElements and
		SWEP.ViewModelBoneMods tables are all optional
		and only have to be visible to the client.
********************************************************/

function SWEP:Initialize()

	// other initialize code goes here

	if CLIENT then

		self:SetHoldType( self.HoldType or "pistol" )
		// Create a new table for every weapon instance
		self.VElements = table.FullCopy( self.VElements )
		self.WElements = table.FullCopy( self.WElements )
		self.ViewModelBoneMods = table.FullCopy( self.ViewModelBoneMods )

		self:CreateModels(self.VElements) // create viewmodels
		self:CreateModels(self.WElements) // create worldmodels
		
		// init view model bone build function
		if IsValid(self.Owner) then
			local vm = self.Owner:GetViewModel()
			if IsValid(vm) then
				self:ResetBonePositions(vm)
				
				--[[// Init viewmodel visibility
				if (self.ShowViewModel == nil or self.ShowViewModel) then
					vm:SetColor(Color(255,255,255,255))
				else
					// we set the alpha to 1 instead of 0 because else ViewModelDrawn stops being called
					vm:SetColor(Color(255,255,255,1))
					// ^ stopped working in GMod 13 because you have to do Entity:SetRenderMode(1) for translucency to kick in
					// however for some reason the view model resets to render mode 0 every frame so we just apply a debug material to prevent it from drawing
					vm:SetMaterial("Debug/hsv")			
				end]]--
			end
		end
	end
end
function SWEP:Holster()
	
	if CLIENT and IsValid(self.Owner) then
		local vm = self.Owner:GetViewModel()
		if IsValid(vm) then
			self:ResetBonePositions(vm)
		end
	end
	
	return true
end

function SWEP:OnRemove()
	self:Holster()
end

if CLIENT then

	SWEP.vRenderOrder = nil
	function SWEP:ViewModelDrawn()
		
		local vm = self.Owner:GetViewModel()
		if !IsValid(vm) then return end
		
		if (!self.VElements) then return end
		
		self:UpdateBonePositions(vm)

		if (!self.vRenderOrder) then
			
			// we build a render order because sprites need to be drawn after models
			self.vRenderOrder = {}

			for k, v in pairs( self.VElements ) do
				if (v.type == "Model") then
					table.insert(self.vRenderOrder, 1, k)
				elseif (v.type == "Sprite" or v.type == "Quad") then
					table.insert(self.vRenderOrder, k)
				end
			end
			
		end

		for k, name in ipairs( self.vRenderOrder ) do
		
			local v = self.VElements[name]
			if (!v) then self.vRenderOrder = nil break end
			if (v.hide) then continue end
			
			local model = v.modelEnt
			local sprite = v.spriteMaterial
			
			if (!v.bone) then continue end
			
			local pos, ang = self:GetBoneOrientation( self.VElements, v, vm )
			
			if (!pos) then continue end
			
			if (v.type == "Model" and IsValid(model)) then

				model:SetPos(pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z )
				ang:RotateAroundAxis(ang:Up(), v.angle.y)
				ang:RotateAroundAxis(ang:Right(), v.angle.p)
				ang:RotateAroundAxis(ang:Forward(), v.angle.r)

				model:SetAngles(ang)
				//model:SetModelScale(v.size)
				local matrix = Matrix()
				matrix:Scale(v.size)
				model:EnableMatrix( "RenderMultiply", matrix )
				
				if (v.material == "") then
					model:SetMaterial("")
				elseif (model:GetMaterial() != v.material) then
					model:SetMaterial( v.material )
				end
				
				if (v.skin and v.skin != model:GetSkin()) then
					model:SetSkin(v.skin)
				end
				
				if (v.bodygroup) then
					for k, v in pairs( v.bodygroup ) do
						if (model:GetBodygroup(k) != v) then
							model:SetBodygroup(k, v)
						end
					end
				end
				
				if (v.surpresslightning) then
					render.SuppressEngineLighting(true)
				end
				
				render.SetColorModulation(v.color.r/255, v.color.g/255, v.color.b/255)
				render.SetBlend(v.color.a/255)
				model:DrawModel()
				render.SetBlend(1)
				render.SetColorModulation(1, 1, 1)
				
				if (v.surpresslightning) then
					render.SuppressEngineLighting(false)
				end
				
			elseif (v.type == "Sprite" and sprite) then
				
				local drawpos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
				render.SetMaterial(sprite)
				render.DrawSprite(drawpos, v.size.x, v.size.y, v.color)
				
			elseif (v.type == "Quad" and v.draw_func) then
				
				local drawpos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
				ang:RotateAroundAxis(ang:Up(), v.angle.y)
				ang:RotateAroundAxis(ang:Right(), v.angle.p)
				ang:RotateAroundAxis(ang:Forward(), v.angle.r)
				
				cam.Start3D2D(drawpos, ang, v.size)
					v.draw_func( self )
				cam.End3D2D()

			end
			
		end
		
	end

	SWEP.wRenderOrder = nil
	function SWEP:DrawWorldModel()
		
		if (self.ShowWorldModel == nil or self.ShowWorldModel) then
			self:DrawModel()
		end
		
		if (!self.WElements) then return end
		
		if (!self.wRenderOrder) then

			self.wRenderOrder = {}

			for k, v in pairs( self.WElements ) do
				if (v.type == "Model") then
					table.insert(self.wRenderOrder, 1, k)
				elseif (v.type == "Sprite" or v.type == "Quad") then
					table.insert(self.wRenderOrder, k)
				end
			end

		end
		
		if (IsValid(self.Owner)) then
			bone_ent = self.Owner
		else
			// when the weapon is dropped
			bone_ent = self
		end
		
		for k, name in pairs( self.wRenderOrder ) do
		
			local v = self.WElements[name]
			if (!v) then self.wRenderOrder = nil break end
			if (v.hide) then continue end
			
			local pos, ang
			
			if (v.bone) then
				pos, ang = self:GetBoneOrientation( self.WElements, v, bone_ent )
			else
				pos, ang = self:GetBoneOrientation( self.WElements, v, bone_ent, "ValveBiped.Bip01_R_Hand" )
			end
			
			if (!pos) then continue end
			
			local model = v.modelEnt
			local sprite = v.spriteMaterial
			
			if (v.type == "Model" and IsValid(model)) then

				model:SetPos(pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z )
				ang:RotateAroundAxis(ang:Up(), v.angle.y)
				ang:RotateAroundAxis(ang:Right(), v.angle.p)
				ang:RotateAroundAxis(ang:Forward(), v.angle.r)

				model:SetAngles(ang)
				//model:SetModelScale(v.size)
				local matrix = Matrix()
				matrix:Scale(v.size)
				model:EnableMatrix( "RenderMultiply", matrix )
				
				if (v.material == "") then
					model:SetMaterial("")
				elseif (model:GetMaterial() != v.material) then
					model:SetMaterial( v.material )
				end
				
				if (v.skin and v.skin != model:GetSkin()) then
					model:SetSkin(v.skin)
				end
				
				if (v.bodygroup) then
					for k, v in pairs( v.bodygroup ) do
						if (model:GetBodygroup(k) != v) then
							model:SetBodygroup(k, v)
						end
					end
				end
				
				if (v.surpresslightning) then
					render.SuppressEngineLighting(true)
				end
				
				render.SetColorModulation(v.color.r/255, v.color.g/255, v.color.b/255)
				render.SetBlend(v.color.a/255)
				model:DrawModel()
				render.SetBlend(1)
				render.SetColorModulation(1, 1, 1)
				
				if (v.surpresslightning) then
					render.SuppressEngineLighting(false)
				end
				
			elseif (v.type == "Sprite" and sprite) then
				
				local drawpos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
				render.SetMaterial(sprite)
				render.DrawSprite(drawpos, v.size.x, v.size.y, v.color)
				
			elseif (v.type == "Quad" and v.draw_func) then
				
				local drawpos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
				ang:RotateAroundAxis(ang:Up(), v.angle.y)
				ang:RotateAroundAxis(ang:Right(), v.angle.p)
				ang:RotateAroundAxis(ang:Forward(), v.angle.r)
				
				cam.Start3D2D(drawpos, ang, v.size)
					v.draw_func( self )
				cam.End3D2D()

			end
			
		end
		
	end

	function SWEP:GetBoneOrientation( basetab, tab, ent, bone_override )
		
		local bone, pos, ang
		if (tab.rel and tab.rel != "") then
			
			local v = basetab[tab.rel]
			
			if (!v) then return end
			
			// Technically, if there exists an element with the same name as a bone
			// you can get in an infinite loop. Let's just hope nobody's that stupid.
			pos, ang = self:GetBoneOrientation( basetab, v, ent )
			
			if (!pos) then return end
			
			pos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
			ang:RotateAroundAxis(ang:Up(), v.angle.y)
			ang:RotateAroundAxis(ang:Right(), v.angle.p)
			ang:RotateAroundAxis(ang:Forward(), v.angle.r)
				
		else
		
			bone = ent:LookupBone(bone_override or tab.bone)

			if (!bone) then return end
			
			pos, ang = Vector(0,0,0), Angle(0,0,0)
			local m = ent:GetBoneMatrix(bone)
			if (m) then
				pos, ang = m:GetTranslation(), m:GetAngles()
			end
			
			if (IsValid(self.Owner) and self.Owner:IsPlayer() and 
				ent == self.Owner:GetViewModel() and self.ViewModelFlip) then
				ang.r = -ang.r // Fixes mirrored models
			end
		
		end
		
		return pos, ang
	end

	function SWEP:CreateModels( tab )

		if (!tab) then return end

		// Create the clientside models here because Garry says we can't do it in the render hook
		for k, v in pairs( tab ) do
			if (v.type == "Model" and v.model and v.model != "" and (!IsValid(v.modelEnt) or v.createdModel != v.model) and 
					string.find(v.model, ".mdl") and file.Exists (v.model, "GAME") ) then
				
				v.modelEnt = ClientsideModel(v.model, RENDER_GROUP_VIEW_MODEL_OPAQUE)
				if (IsValid(v.modelEnt)) then
					v.modelEnt:SetPos(self:GetPos())
					v.modelEnt:SetAngles(self:GetAngles())
					v.modelEnt:SetParent(self)
					v.modelEnt:SetNoDraw(true)
					v.createdModel = v.model
				else
					v.modelEnt = nil
				end
				
			elseif (v.type == "Sprite" and v.sprite and v.sprite != "" and (!v.spriteMaterial or v.createdSprite != v.sprite) 
				and file.Exists ("materials/"..v.sprite..".vmt", "GAME")) then
				
				local name = v.sprite.."-"
				local params = { ["$basetexture"] = v.sprite }
				// make sure we create a unique name based on the selected options
				local tocheck = { "nocull", "additive", "vertexalpha", "vertexcolor", "ignorez" }
				for i, j in pairs( tocheck ) do
					if (v[j]) then
						params["$"..j] = 1
						name = name.."1"
					else
						name = name.."0"
					end
				end

				v.createdSprite = v.sprite
				v.spriteMaterial = CreateMaterial(name,"UnlitGeneric",params)
				
			end
		end
		
	end
	
	local allbones
	local hasGarryFixedBoneScalingYet = false

	function SWEP:UpdateBonePositions(vm)
		
		if self.ViewModelBoneMods then
			
			if (!vm:GetBoneCount()) then return end
			
			// !! WORKAROUND !! //
			// We need to check all model names :/
			local loopthrough = self.ViewModelBoneMods
			if (!hasGarryFixedBoneScalingYet) then
				allbones = {}
				for i=0, vm:GetBoneCount() do
					local bonename = vm:GetBoneName(i)
					if (self.ViewModelBoneMods[bonename]) then 
						allbones[bonename] = self.ViewModelBoneMods[bonename]
					else
						allbones[bonename] = { 
							scale = Vector(1,1,1),
							pos = Vector(0,0,0),
							angle = Angle(0,0,0)
						}
					end
				end
				
				loopthrough = allbones
			end
			// !! ----------- !! //
			
			for k, v in pairs( loopthrough ) do
				local bone = vm:LookupBone(k)
				if (!bone) then continue end
				
				// !! WORKAROUND !! //
				local s = Vector(v.scale.x,v.scale.y,v.scale.z)
				local p = Vector(v.pos.x,v.pos.y,v.pos.z)
				local ms = Vector(1,1,1)
				if (!hasGarryFixedBoneScalingYet) then
					local cur = vm:GetBoneParent(bone)
					while(cur >= 0) do
						local pscale = loopthrough[vm:GetBoneName(cur)].scale
						ms = ms * pscale
						cur = vm:GetBoneParent(cur)
					end
				end
				
				s = s * ms
				// !! ----------- !! //
				
				if vm:GetManipulateBoneScale(bone) != s then
					vm:ManipulateBoneScale( bone, s )
				end
				if vm:GetManipulateBoneAngles(bone) != v.angle then
					vm:ManipulateBoneAngles( bone, v.angle )
				end
				if vm:GetManipulateBonePosition(bone) != p then
					vm:ManipulateBonePosition( bone, p )
				end
			end
		else
			self:ResetBonePositions(vm)
		end
		   
	end
	 
	function SWEP:ResetBonePositions(vm)
		
		if (!vm:GetBoneCount()) then return end
		for i=0, vm:GetBoneCount() do
			vm:ManipulateBoneScale( i, Vector(1, 1, 1) )
			vm:ManipulateBoneAngles( i, Angle(0, 0, 0) )
			vm:ManipulateBonePosition( i, Vector(0, 0, 0) )
		end
		
	end

	/**************************
		Global utility code
	**************************/

	// Fully copies the table, meaning all tables inside this table are copied too and so on (normal table.Copy copies only their reference).
	// Does not copy entities of course, only copies their reference.
	// WARNING: do not use on tables that contain themselves somewhere down the line or you'll get an infinite loop
	function table.FullCopy( tab )

		if (!tab) then return nil end
		
		local res = {}
		for k, v in pairs( tab ) do
			if (type(v) == "table") then
				res[k] = table.FullCopy(v) // recursion ho!
			elseif (type(v) == "Vector") then
				res[k] = Vector(v.x, v.y, v.z)
			elseif (type(v) == "Angle") then
				res[k] = Angle(v.p, v.y, v.r)
			else
				res[k] = v
			end
		end
		
		return res
		
	end
	
end

