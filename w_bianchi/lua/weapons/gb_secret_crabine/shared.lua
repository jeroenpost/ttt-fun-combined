//General Variables\\
SWEP.AdminSpawnable = true
SWEP.ViewModelFOV = 64

SWEP.VElements = {
	["cover"] = { type = "Model", model = "models/props_combine/combine_barricade_short02a.mdl", bone = "Base", rel = "", pos = Vector(-0.101, -0.456, 5.908), angle = Angle(27.614, 0, -86.932), size = Vector(0.094, 0.094, 0.037), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["clip"] = { type = "Model", model = "models/items/boxsrounds.mdl", bone = "Base", rel = "", pos = Vector(0, 2.273, 14.09), angle = Angle(0, 0, -90), size = Vector(0.264, 0.321, 0.264), color = Color(112, 109, 115, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["sightdot++"] = { type = "Sprite", sprite = "sprites/redglow1", bone = "Base", rel = "sightglass", pos = Vector(0, 0, 0), size = { x = 0.436, y = 0.436 }, color = Color(255, 0, 0, 255), nocull = true, additive = true, vertexalpha = true, vertexcolor = true, ignorez = false},
	["emitter"] = { type = "Model", model = "models/props_combine/breenlight.mdl", bone = "Base", rel = "silencer", pos = Vector(0, -3.182, 6.817), angle = Angle(0, 90, 0), size = Vector(0.606, 0.606, 0.606), color = Color(72, 72, 66, 255), surpresslightning = false, material = "models/props_c17/chair_stool01a", skin = 0, bodygroup = {} },
	["silencer"] = { type = "Model", model = "models/props_junk/popcan01a.mdl", bone = "Base", rel = "", pos = Vector(0, 0, 32.272), angle = Angle(0, 0, 180), size = Vector(0.776, 0.776, 2.819), color = Color(98, 91, 93, 255), surpresslightning = false, material = "models/props_c17/chair_stool01a", skin = 0, bodygroup = {} },
	["hinge"] = { type = "Model", model = "models/props_lab/rotato.mdl", bone = "Base", rel = "", pos = Vector(1.37, 1.299, 14.6), angle = Angle(90, 0, -90), size = Vector(0.5, 0.5, 0.5), color = Color(79, 77, 77, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["sight"] = { type = "Model", model = "models/props_combine/combine_window001.mdl", bone = "Base", rel = "", pos = Vector(0, -2.3, 6.752), angle = Angle(127.402, -90, 0), size = Vector(0.014, 0.014, 0.014), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["HSS"] = { type = "Model", model = "models/props_lab/blastdoor001c.mdl", bone = "Base", rel = "hinge", pos = Vector(-1.558, -3.3, -1.3), angle = Angle(-38.571, 0, 0), size = Vector(0.041, 0.035, 0.037), color = Color(34, 32, 32, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["screen"] = { type = "Model", model = "models/props_lab/scanner2_scrapyard.mdl", bone = "Base", rel = "HSS", pos = Vector(-0.04, 0, 2), angle = Angle(0, 180, 0), size = Vector(0.041, 0.69, 0.432), color = Color(255, 255, 255, 255), surpresslightning = false, material = "models/props_lab/eyescanner_disp", skin = 0, bodygroup = {} },
	["sightglass"] = { type = "Model", model = "models/props_c17/FurnitureShelf001b.mdl", bone = "Base", rel = "sight", pos = Vector(0, 0, 1.399), angle = Angle(0, 90, 77.699), size = Vector(0.07, 0.056, 0.07), color = Color(255, 255, 255, 255), surpresslightning = false, material = "models/props_combine/health_charger_glass", skin = 0, bodygroup = {} }
}
SWEP.WElements = {
	["cover"] = { type = "Model", model = "models/props_combine/combine_barricade_short02a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(5.908, 1, -6.6), angle = Angle(1.023, -64.432, 164.658), size = Vector(0.151, 0.151, 0.094), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["clip"] = { type = "Model", model = "models/items/boxsrounds.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(14.09, 0.455, -5.909), angle = Angle(0, -90, 166.705), size = Vector(0.264, 0.492, 0.264), color = Color(112, 109, 115, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["sightdot++"] = { type = "Sprite", sprite = "sprites/yelflare2", bone = "ValveBiped.Bip01_R_Hand", rel = "sightglass", pos = Vector(-0.101, 0, 1.363), size = { x = 2.082, y = 2.082 }, color = Color(255, 0, 0, 255), nocull = true, additive = true, vertexalpha = true, vertexcolor = true, ignorez = false},
	["dot"] = { type = "Model", model = "", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(5, 0, -10), angle = Angle(0, 0, 0), size = Vector(0.5, 0.5, 0.5), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["emitter"] = { type = "Model", model = "models/props_combine/breenlight.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(23.181, 1, -5), angle = Angle(105.341, 180, -180), size = Vector(0.606, 0.606, 0.606), color = Color(72, 72, 66, 255), surpresslightning = false, material = "models/props_c17/chair_stool01a", skin = 0, bodygroup = {} },
	["silencer"] = { type = "Model", model = "models/props_junk/popcan01a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(27.726, 0.899, -10.455), angle = Angle(74.658, 0, 0), size = Vector(0.776, 0.776, 1.116), color = Color(98, 91, 93, 255), surpresslightning = false, material = "models/props_c17/chair_stool01a", skin = 0, bodygroup = {} },
	["sightdot"] = { type = "Sprite", sprite = "sprites/redglow3", bone = "ValveBiped.Bip01_R_Hand", rel = "emitter", pos = Vector(-0.456, 0, -5), size = { x = 4.012, y = 4.012 }, color = Color(255, 255, 255, 255), nocull = true, additive = true, vertexalpha = true, vertexcolor = true, ignorez = false},
	["hinge"] = { type = "Model", model = "models/props_lab/rotato.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(17.142, 1.557, -5.715), angle = Angle(-19.871, 0, 180), size = Vector(0.5, 0.5, 0.5), color = Color(79, 77, 77, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["sight"] = { type = "Model", model = "models/props_combine/combine_window001.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(9, 0.66, -8), angle = Angle(-146.105, 180, 0), size = Vector(0.014, 0.014, 0.014), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["HSS"] = { type = "Model", model = "models/props_lab/blastdoor001c.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "hinge", pos = Vector(-1.558, -3.636, -1.558), angle = Angle(-38.571, 0, 0), size = Vector(0.041, 0.035, 0.037), color = Color(34, 32, 32, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["screen"] = { type = "Model", model = "models/props_lab/scanner2_scrapyard.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "HSS", pos = Vector(-0.141, 0, 2), angle = Angle(0, 180, 0), size = Vector(0.013, 0.69, 0.432), color = Color(255, 255, 255, 255), surpresslightning = false, material = "models/props_lab/eyescanner_disp", skin = 0, bodygroup = {} },
	["sightglass"] = { type = "Model", model = "models/props_c17/FurnitureShelf001b.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "sight", pos = Vector(0, 0, 1.399), angle = Angle(0, 90, 77.699), size = Vector(0.07, 0.056, 0.07), color = Color(255, 255, 255, 255), surpresslightning = false, material = "models/props_combine/health_charger_glass", skin = 0, bodygroup = {} }
}
SWEP.AutoSwitchTo = false
SWEP.Slot = 0
SWEP.PrintName = "The Secret Carbine"
SWEP.Author = "Spastik"
SWEP.Spawnable = true
SWEP.AutoSwitchFrom = false
SWEP.FiresUnderwater = false
SWEP.Weight = 5
SWEP.DrawCrosshair = true
SWEP.Category = "S&G Munitions"
SWEP.SlotPos = 3
SWEP.DrawAmmo = true
SWEP.ReloadSound = "weapons/smg1/smg1_reload.wav"
SWEP.Instructions = "Pull the trigger to keep your secrets safe."
SWEP.Contact = ""
SWEP.Purpose = "For making sure your dirty secrets actually stay secret. Secretly."
SWEP.Base = "aa_base"
SWEP.HoldType = "ar2"
SWEP.ViewModelFOV = 53.307086614173
SWEP.ViewModelFlip = false
SWEP.ViewModel = "models/weapons/v_irifle.mdl"
SWEP.WorldModel = "models/weapons/w_irifle.mdl"
SWEP.ShowViewModel = true
SWEP.ShowWorldModel = true
SWEP.ViewModelBoneMods = {
	["Reload"] = { scale = Vector(0.009, 0.009, 0.009), pos = Vector(0, 0, 0), angle = Angle(0, 0, 0) },
	["Shell2"] = { scale = Vector(0.009, 0.009, 0.009), pos = Vector(0, 0, 0), angle = Angle(0, 0, 0) },
	["Reload1"] = { scale = Vector(0.009, 0.009, 0.009), pos = Vector(0, 0, 0), angle = Angle(0, 0, 0) },
	["Vent"] = { scale = Vector(0.999, 0.999, 0.999), pos = Vector(0, 0, 0), angle = Angle(0, 0, 0) },
	["Claw1"] = { scale = Vector(0.009, 0.009, 0.009), pos = Vector(0, 0, 0), angle = Angle(0, 0, 0) },
	["Shell1"] = { scale = Vector(0.009, 0.009, 0.009), pos = Vector(0, 0, 0), angle = Angle(0, 0, 0) },
	["Claw2"] = { scale = Vector(0.009, 0.009, 0.009), pos = Vector(0, 0, 0), angle = Angle(0, 0, 0) }
}
//General Variables\\

//Primary Fire Variables\\
SWEP.Primary.Sound = "weapons/ar2/fire1.wav"
SWEP.Primary.Damage = 17.7
SWEP.Primary.TakeAmmo = 1
SWEP.Primary.ClipSize = 40
SWEP.Primary.Ammo = "smg1"
SWEP.Primary.DefaultClip = 200
SWEP.Primary.Spread = .23
SWEP.Primary.NumberofShots = 1
SWEP.Primary.Automatic = true
SWEP.Primary.Recoil = 0.6
SWEP.Primary.Delay = 0.07
SWEP.Primary.Force = 3
//Primary Fire Variables\\

//Secondary Fire Variables\\
SWEP.Secondary.Ammo = "none"

Zoom=0

//Secondary Fire Variables\\
//SWEP:Initialize()\\
	function SWEP:Initialize()
    self:SetHoldType("smg1")
end
//SWEP:Initialize()\\

//SWEP:PrimaryFire()\\
function SWEP:PrimaryAttack()
	if ( !self:CanPrimaryAttack() ) then return end
	local bullet = {}
		bullet.Num = self.Primary.NumberofShots
		bullet.Src = self.Owner:GetShootPos()
		bullet.Dir = self.Owner:GetAimVector()
		bullet.Spread = Vector( self.Primary.Spread * 0.1 , self.Primary.Spread * 0.1, 0)
		bullet.Tracer = 1
		bullet.Force = self.Primary.Force
		bullet.Damage = self.Primary.Damage
		bullet.AmmoType = self.Primary.Ammo
	local rnda = self.Primary.Recoil * -1
	local rndb = self.Primary.Recoil * math.random(-1, 1)
	self:ShootEffects()
	self.Owner:FireBullets( bullet )
	self.Weapon:EmitSound(self.Primary.Sound, 500, 180, 1, 0)
	self.Owner:ViewPunch( Angle( rnda,rndb,rnda ) )
	self:TakePrimaryAmmo(self.Primary.TakeAmmo)
	self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
	self.Weapon:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
end
//SWEP:PrimaryFire()\\

function SWEP:Reload()
         self.NextReload = self.NextReload or 0
         if ( self.NextReload > CurTime() ) then return end
         if ( self:Clip1() ~= self.Primary.ClipSize and self:Ammo1() ~= 0 ) then
		      self:SendWeaponAnim( ACT_VM_HOLSTER )
			  self:SetNWInt( "Spin",0 )
			  self:SetNextPrimaryFire( CurTime() + 1.8 )
			  self:SetNextSecondaryFire( CurTime() + 1.8 )
			  self.NextReload = CurTime() + 1.25
			  self.Reloading = true
			  self:EmitSound( "weapons/m249/m249_boxout.wav", 500, 100, 1, 0)
			  self:EmitSound( "weapons/m4a1/m4a1_boltpull.wav", 500, 100, 1, -1)
			  timer.Simple( 1.8,function()
			        if ( IsValid( self ) ) then
                         self:DefaultReload( ACT_VM_DRAW )
						 self.Reloading = false
						self:EmitSound( "weapons/famas/m249_boxin.wav", 100, 80, 1, -1) 
						self:EmitSound( "weapons/famas/famas_clipin.wav", 100, 80, 1, 0 )
							//self:SetIronsights(false)
					end
			  end )
		 end
end
//SWEP:SecondaryFire()\\
function SWEP:SecondaryAttack()
end
/********************************************************
	SWEP Construction Kit base code
		Created by Clavus
	Available for public use, thread at:
	   facepunch.com/threads/1032378
	   
	   
	DESCRIPTION:
		This script is meant for experienced scripters 
		that KNOW WHAT THEY ARE DOING. Don't come to me 
		with basic Lua questions.
		
		Just copy into your SWEP or SWEP base of choice
		and merge with your own code.
		
		The SWEP.VElements, SWEP.WElements and
		SWEP.ViewModelBoneMods tables are all optional
		and only have to be visible to the client.
********************************************************/

function SWEP:Initialize()

	// other initialize code goes here

	if CLIENT then

		self:SetHoldType( self.HoldType or "pistol" )
		// Create a new table for every weapon instance
		self.VElements = table.FullCopy( self.VElements )
		self.WElements = table.FullCopy( self.WElements )
		self.ViewModelBoneMods = table.FullCopy( self.ViewModelBoneMods )

		self:CreateModels(self.VElements) // create viewmodels
		self:CreateModels(self.WElements) // create worldmodels
		
		// init view model bone build function
		if IsValid(self.Owner) then
			local vm = self.Owner:GetViewModel()
			if IsValid(vm) then
				self:ResetBonePositions(vm)
				
				--[[// Init viewmodel visibility
				if (self.ShowViewModel == nil or self.ShowViewModel) then
					vm:SetColor(Color(255,255,255,255))
				else
					// we set the alpha to 1 instead of 0 because else ViewModelDrawn stops being called
					vm:SetColor(Color(255,255,255,1))
					// ^ stopped working in GMod 13 because you have to do Entity:SetRenderMode(1) for translucency to kick in
					// however for some reason the view model resets to render mode 0 every frame so we just apply a debug material to prevent it from drawing
					vm:SetMaterial("Debug/hsv")			
				end]]--
			end
		end
	end
end
function SWEP:Holster()
	
	if CLIENT and IsValid(self.Owner) then
		local vm = self.Owner:GetViewModel()
		if IsValid(vm) then
			self:ResetBonePositions(vm)
		end
	end
	
	return true
end

function SWEP:OnRemove()
	self:Holster()
end

SWEP.LazerAttachment = "muzzle"
if CLIENT then

	SWEP.vRenderOrder = nil
	function SWEP:ViewModelDrawn()

		local vm = self.Owner:GetViewModel()
		if ( IsValid( vm ) ) then
		     local attachment = vm:GetAttachment( vm:LookupAttachment( self.LazerAttachment ) )
			 if ( attachment ~= nil ) then
		          local tr = util.TraceLine( { start = attachment.Pos - attachment.Ang:Forward()*24,
                                               endpos = attachment.Pos + attachment.Ang:Forward()*2400,
										       filter = { self,self.Owner } } )
		          render.SetMaterial( Material( "effects/redflare" ) )
                  render.DrawSprite( tr.HitPos + attachment.Ang:Forward()*64 - attachment.Ang:Up()*6,12,12,Color( 255,255,255 ) )
             end
		end
		if !IsValid(vm) then return end
		
		if (!self.VElements) then return end
		
		self:UpdateBonePositions(vm)

		if (!self.vRenderOrder) then
			
			// we build a render order because sprites need to be drawn after models
			self.vRenderOrder = {}

			for k, v in pairs( self.VElements ) do
				if (v.type == "Model") then
					table.insert(self.vRenderOrder, 1, k)
				elseif (v.type == "Sprite" or v.type == "Quad") then
					table.insert(self.vRenderOrder, k)
				end
			end
			
		end

		for k, name in ipairs( self.vRenderOrder ) do
		
			local v = self.VElements[name]
			if (!v) then self.vRenderOrder = nil break end
			if (v.hide) then continue end
			
			local model = v.modelEnt
			local sprite = v.spriteMaterial
			
			if (!v.bone) then continue end
			
			local pos, ang = self:GetBoneOrientation( self.VElements, v, vm )
			
			if (!pos) then continue end
			
			if (v.type == "Model" and IsValid(model)) then

				model:SetPos(pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z )
				ang:RotateAroundAxis(ang:Up(), v.angle.y)
				ang:RotateAroundAxis(ang:Right(), v.angle.p)
				ang:RotateAroundAxis(ang:Forward(), v.angle.r)

				model:SetAngles(ang)
				//model:SetModelScale(v.size)
				local matrix = Matrix()
				matrix:Scale(v.size)
				model:EnableMatrix( "RenderMultiply", matrix )
				
				if (v.material == "") then
					model:SetMaterial("")
				elseif (model:GetMaterial() != v.material) then
					model:SetMaterial( v.material )
				end
				
				if (v.skin and v.skin != model:GetSkin()) then
					model:SetSkin(v.skin)
				end
				
				if (v.bodygroup) then
					for k, v in pairs( v.bodygroup ) do
						if (model:GetBodygroup(k) != v) then
							model:SetBodygroup(k, v)
						end
					end
				end
				
				if (v.surpresslightning) then
					render.SuppressEngineLighting(true)
				end
				
				render.SetColorModulation(v.color.r/255, v.color.g/255, v.color.b/255)
				render.SetBlend(v.color.a/255)
				model:DrawModel()
				render.SetBlend(1)
				render.SetColorModulation(1, 1, 1)
				
				if (v.surpresslightning) then
					render.SuppressEngineLighting(false)
				end
				
			elseif (v.type == "Sprite" and sprite) then
				
				local drawpos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
				render.SetMaterial(sprite)
				render.DrawSprite(drawpos, v.size.x, v.size.y, v.color)
				
			elseif (v.type == "Quad" and v.draw_func) then
				
				local drawpos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
				ang:RotateAroundAxis(ang:Up(), v.angle.y)
				ang:RotateAroundAxis(ang:Right(), v.angle.p)
				ang:RotateAroundAxis(ang:Forward(), v.angle.r)
				
				cam.Start3D2D(drawpos, ang, v.size)
					v.draw_func( self )
				cam.End3D2D()

			end
			
		end
		
	end

	SWEP.wRenderOrder = nil
	function SWEP:DrawWorldModel()
		
		if (self.ShowWorldModel == nil or self.ShowWorldModel) then
			self:DrawModel()
		end
		
		if (!self.WElements) then return end
		
		if (!self.wRenderOrder) then

			self.wRenderOrder = {}

			for k, v in pairs( self.WElements ) do
				if (v.type == "Model") then
					table.insert(self.wRenderOrder, 1, k)
				elseif (v.type == "Sprite" or v.type == "Quad") then
					table.insert(self.wRenderOrder, k)
				end
			end

		end
		
		if (IsValid(self.Owner)) then
			bone_ent = self.Owner
		else
			// when the weapon is dropped
			bone_ent = self
		end
		
		for k, name in pairs( self.wRenderOrder ) do
		
			local v = self.WElements[name]
			if (!v) then self.wRenderOrder = nil break end
			if (v.hide) then continue end
			
			local pos, ang
			
			if (v.bone) then
				pos, ang = self:GetBoneOrientation( self.WElements, v, bone_ent )
			else
				pos, ang = self:GetBoneOrientation( self.WElements, v, bone_ent, "ValveBiped.Bip01_R_Hand" )
			end
			
			if (!pos) then continue end
			
			local model = v.modelEnt
			local sprite = v.spriteMaterial
			
			if (v.type == "Model" and IsValid(model)) then

				model:SetPos(pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z )
				ang:RotateAroundAxis(ang:Up(), v.angle.y)
				ang:RotateAroundAxis(ang:Right(), v.angle.p)
				ang:RotateAroundAxis(ang:Forward(), v.angle.r)

				model:SetAngles(ang)
				//model:SetModelScale(v.size)
				local matrix = Matrix()
				matrix:Scale(v.size)
				model:EnableMatrix( "RenderMultiply", matrix )
				
				if (v.material == "") then
					model:SetMaterial("")
				elseif (model:GetMaterial() != v.material) then
					model:SetMaterial( v.material )
				end
				
				if (v.skin and v.skin != model:GetSkin()) then
					model:SetSkin(v.skin)
				end
				
				if (v.bodygroup) then
					for k, v in pairs( v.bodygroup ) do
						if (model:GetBodygroup(k) != v) then
							model:SetBodygroup(k, v)
						end
					end
				end
				
				if (v.surpresslightning) then
					render.SuppressEngineLighting(true)
				end
				
				render.SetColorModulation(v.color.r/255, v.color.g/255, v.color.b/255)
				render.SetBlend(v.color.a/255)
				model:DrawModel()
				render.SetBlend(1)
				render.SetColorModulation(1, 1, 1)
				
				if (v.surpresslightning) then
					render.SuppressEngineLighting(false)
				end
				
			elseif (v.type == "Sprite" and sprite) then
				
				local drawpos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
				render.SetMaterial(sprite)
				render.DrawSprite(drawpos, v.size.x, v.size.y, v.color)
				
			elseif (v.type == "Quad" and v.draw_func) then
				
				local drawpos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
				ang:RotateAroundAxis(ang:Up(), v.angle.y)
				ang:RotateAroundAxis(ang:Right(), v.angle.p)
				ang:RotateAroundAxis(ang:Forward(), v.angle.r)
				
				cam.Start3D2D(drawpos, ang, v.size)
					v.draw_func( self )
				cam.End3D2D()

			end
			
		end
		
	end

	function SWEP:GetBoneOrientation( basetab, tab, ent, bone_override )
		
		local bone, pos, ang
		if (tab.rel and tab.rel != "") then
			
			local v = basetab[tab.rel]
			
			if (!v) then return end
			
			// Technically, if there exists an element with the same name as a bone
			// you can get in an infinite loop. Let's just hope nobody's that stupid.
			pos, ang = self:GetBoneOrientation( basetab, v, ent )
			
			if (!pos) then return end
			
			pos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
			ang:RotateAroundAxis(ang:Up(), v.angle.y)
			ang:RotateAroundAxis(ang:Right(), v.angle.p)
			ang:RotateAroundAxis(ang:Forward(), v.angle.r)
				
		else
		
			bone = ent:LookupBone(bone_override or tab.bone)

			if (!bone) then return end
			
			pos, ang = Vector(0,0,0), Angle(0,0,0)
			local m = ent:GetBoneMatrix(bone)
			if (m) then
				pos, ang = m:GetTranslation(), m:GetAngles()
			end
			
			if (IsValid(self.Owner) and self.Owner:IsPlayer() and 
				ent == self.Owner:GetViewModel() and self.ViewModelFlip) then
				ang.r = -ang.r // Fixes mirrored models
			end
		
		end
		
		return pos, ang
	end

	function SWEP:CreateModels( tab )

		if (!tab) then return end

		// Create the clientside models here because Garry says we can't do it in the render hook
		for k, v in pairs( tab ) do
			if (v.type == "Model" and v.model and v.model != "" and (!IsValid(v.modelEnt) or v.createdModel != v.model) and 
					string.find(v.model, ".mdl") and file.Exists (v.model, "GAME") ) then
				
				v.modelEnt = ClientsideModel(v.model, RENDER_GROUP_VIEW_MODEL_OPAQUE)
				if (IsValid(v.modelEnt)) then
					v.modelEnt:SetPos(self:GetPos())
					v.modelEnt:SetAngles(self:GetAngles())
					v.modelEnt:SetParent(self)
					v.modelEnt:SetNoDraw(true)
					v.createdModel = v.model
				else
					v.modelEnt = nil
				end
				
			elseif (v.type == "Sprite" and v.sprite and v.sprite != "" and (!v.spriteMaterial or v.createdSprite != v.sprite) 
				and file.Exists ("materials/"..v.sprite..".vmt", "GAME")) then
				
				local name = v.sprite.."-"
				local params = { ["$basetexture"] = v.sprite }
				// make sure we create a unique name based on the selected options
				local tocheck = { "nocull", "additive", "vertexalpha", "vertexcolor", "ignorez" }
				for i, j in pairs( tocheck ) do
					if (v[j]) then
						params["$"..j] = 1
						name = name.."1"
					else
						name = name.."0"
					end
				end

				v.createdSprite = v.sprite
				v.spriteMaterial = CreateMaterial(name,"UnlitGeneric",params)
				
			end
		end
		
	end
	
	local allbones
	local hasGarryFixedBoneScalingYet = false

	function SWEP:UpdateBonePositions(vm)
		
		if self.ViewModelBoneMods then
			
			if (!vm:GetBoneCount()) then return end
			
			// !! WORKAROUND !! //
			// We need to check all model names :/
			local loopthrough = self.ViewModelBoneMods
			if (!hasGarryFixedBoneScalingYet) then
				allbones = {}
				for i=0, vm:GetBoneCount() do
					local bonename = vm:GetBoneName(i)
					if (self.ViewModelBoneMods[bonename]) then 
						allbones[bonename] = self.ViewModelBoneMods[bonename]
					else
						allbones[bonename] = { 
							scale = Vector(1,1,1),
							pos = Vector(0,0,0),
							angle = Angle(0,0,0)
						}
					end
				end
				
				loopthrough = allbones
			end
			// !! ----------- !! //
			
			for k, v in pairs( loopthrough ) do
				local bone = vm:LookupBone(k)
				if (!bone) then continue end
				
				// !! WORKAROUND !! //
				local s = Vector(v.scale.x,v.scale.y,v.scale.z)
				local p = Vector(v.pos.x,v.pos.y,v.pos.z)
				local ms = Vector(1,1,1)
				if (!hasGarryFixedBoneScalingYet) then
					local cur = vm:GetBoneParent(bone)
					while(cur >= 0) do
						local pscale = loopthrough[vm:GetBoneName(cur)].scale
						ms = ms * pscale
						cur = vm:GetBoneParent(cur)
					end
				end
				
				s = s * ms
				// !! ----------- !! //
				
				if vm:GetManipulateBoneScale(bone) != s then
					vm:ManipulateBoneScale( bone, s )
				end
				if vm:GetManipulateBoneAngles(bone) != v.angle then
					vm:ManipulateBoneAngles( bone, v.angle )
				end
				if vm:GetManipulateBonePosition(bone) != p then
					vm:ManipulateBonePosition( bone, p )
				end
			end
		else
			self:ResetBonePositions(vm)
		end
		   
	end
	 
	function SWEP:ResetBonePositions(vm)
		
		if (!vm:GetBoneCount()) then return end
		for i=0, vm:GetBoneCount() do
			vm:ManipulateBoneScale( i, Vector(1, 1, 1) )
			vm:ManipulateBoneAngles( i, Angle(0, 0, 0) )
			vm:ManipulateBonePosition( i, Vector(0, 0, 0) )
		end
		
	end

	/**************************
		Global utility code
	**************************/

	// Fully copies the table, meaning all tables inside this table are copied too and so on (normal table.Copy copies only their reference).
	// Does not copy entities of course, only copies their reference.
	// WARNING: do not use on tables that contain themselves somewhere down the line or you'll get an infinite loop
	function table.FullCopy( tab )

		if (!tab) then return nil end
		
		local res = {}
		for k, v in pairs( tab ) do
			if (type(v) == "table") then
				res[k] = table.FullCopy(v) // recursion ho!
			elseif (type(v) == "Vector") then
				res[k] = Vector(v.x, v.y, v.z)
			elseif (type(v) == "Angle") then
				res[k] = Angle(v.p, v.y, v.r)
			else
				res[k] = v
			end
		end
		
		return res
		
	end
	
end

