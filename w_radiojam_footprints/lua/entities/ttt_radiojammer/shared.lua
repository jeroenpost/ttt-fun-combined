local config = ttt_perky_config

ENT.Type = "anim"
ENT.Model = Model("models/props/cs_office/radio.mdl")
ENT.LifeTime = 15
ENT.destructTime = 15
ENT.SoundInterval = config.radiojammer_sound_interval
ENT.NextSound = 0
ENT.DetectiveNearRadius = 400
ENT.Health = config.radiojammer_health

--local jamSound = Sound( "npc/scanner/cbot_servochatter.wav" )
-- load sounds
local randomSounds = {}
for i = 1, 15 do
	randomSounds[i] = "ambient/levels/prison/radio_random"..i..".wav"--Sound( "ambient/levels/prison/radio_random"..i..".wav" )
end

if SERVER then 
	AddCSLuaFile("shared.lua") 
         --resource.AddFile("materials/vgui/ttt_fun_killicons/icon_radiofrequency.png")
end

if CLIENT then
   -- this entity can be DNA-sampled so we need some display info
   ENT.Icon = "vgui/ttt_fun_killicons/icon_radiofrequency.png"
   ENT.PrintName = "Radio Jammer"
end

function ENT:Initialize()
	self.Entity:SetModel( self.Model )
	self.Entity:PhysicsInit(SOLID_VPHYSICS)
	self.Entity:SetMoveType(MOVETYPE_VPHYSICS)
	self.Entity:SetSolid(SOLID_VPHYSICS)
	self.Entity:SetCollisionGroup(COLLISION_GROUP_NONE)
	
	self.Entity:SetHealth( 40 )
	if SERVER then
	    self.Entity:SetMaxHealth( 40 )
		self.Entity:SetUseType(SIMPLE_USE)
		self.destructTime = CurTime() + self.LifeTime
	elseif CLIENT then
	   if LocalPlayer() == self:GetOwner() then
	      LocalPlayer().radiojammer = self.Entity
	   end
	end
	self.NextSound = CurTime() + self.SoundInterval
	self.jamming = false
	self.fingerprints = {}
end

function ENT:IsDetectiveNear()
   local center = self:GetPos()
   local r = self.DetectiveNearRadius ^ 2
   local d = 0.0
   local diff = nil
   for _, ent in pairs(player.GetAll()) do
      if IsValid(ent) and ent:IsActiveDetective() then
         -- dot of the difference with itself is distance squared
         diff = center - ent:GetPos()
         d = diff:DotProduct(diff)

         if d < r then
               return true
         end
      end
   end

   return false
end

function ENT:Think()
	if SERVER then
		if CurTime() > self.destructTime then
			self:DoExplode()
			self:Remove()
		end
		if CurTime() > self.NextSound then
			self.NextSound = CurTime() + self.SoundInterval
			if self:IsDetectiveNear() then
		        local amp = 140
			else
			local amp = 100
			end
			local n = math.random(1,15)
			for i = 1, 15 do
			self:EmitSound( "ambient/levels/prison/radio_random"..i..".wav", self:GetPos(), amp, 100 )
			end
			self:EmitSound( "npc/scanner/cbot_servochatter.wav", self:GetPos(), amp, 100 )
			
			--for i = 1, 15 do
			--WorldSound( "ambient/levels/prison/radio_random"..i..".wav", self:GetPos(), amp, 100 )
			--end
			--WorldSound( "npc/scanner/cbot_servochatter.wav", self:GetPos(), amp, 100 )
		end
	end
end

local zapsound = Sound("npc/assassin/ball_zap1.wav")
function ENT:OnTakeDamage(dmginfo)
   self:TakePhysicsDamage(dmginfo)

   self:SetHealth(self:Health() - dmginfo:GetDamage())
   if self:Health() < 0 then
      self:Remove()

      local effect = EffectData()
      effect:SetOrigin(self:GetPos())
      util.Effect("cball_explode", effect)
      sound.Play(zapsound, self:GetPos())

      if IsValid(self:GetOwner()) then
         LANG.Msg(self:GetOwner(), "radio_broken")
      end
   end
end

function ENT:DoExplode()
     self:Remove()

      local effect = EffectData()
      effect:SetOrigin(self:GetPos())
      util.Effect("cball_explode", effect)
      sound.Play(zapsound, self:GetPos())
end

function ENT:OnRemove()
   if CLIENT then
      if LocalPlayer() == self:GetOwner() then
         LocalPlayer().radiojammer = nil
      end
   end
end

if SERVER then
	
	function ENT.PlayerSay( ply, text, team )
        if not IsValid(ply) then return "" end
        if ply:GetNWBool("IsRadioJammed") then
            return "[JAMMED]"
        end

		if #ents.FindByClass( 'ttt_radiojammer' ) > 0 then
			if ply:IsSpec() then

			elseif not ply:HasEquipmentItem( EQUIP_RADIOFREQUENCY ) or ply:GetNWBool("IsRadioJammed") then
				return "[JAMMED]"
			end
        end

	end
	hook.Add( 'PlayerSay', 'ttt_radiojammer_playersay', ENT.PlayerSay )
	
	function ENT.TTTPlayerRadioCommand (ply, cmd_name, cmd_target)
        if ply:GetNWBool("IsRadioJammed") and not ply:IsAdmin() then
            return false
        end

		if #ents.FindByClass( 'ttt_radiojammer' ) > 0 then
			if ply:IsSpec() then
				return nil
			elseif not ply:HasEquipmentItem( EQUIP_RADIOFREQUENCY )  or  ply:GetNWBool("IsRadioJammed")  then
				return "[JAMMED]"
			end
		end
	end
	hook.Add( 'TTTPlayerRadioCommand', 'ttt_radiojammer_playerradiocommand', ENT.TTTPlayerRadioCommand )
	
	function ENT.PlayerCanHearVoice( listner, talker )
        if talker:GetNWBool("IsRadioJammed") or  listner:GetNWBool("IsRadioJammed") and not talker:IsAdmin() and not listner:IsAdmin() then return false, false end

		if #ents.FindByClass( 'ttt_radiojammer' ) > 0 then
                if talker:GetNWBool("IsRadioJammed") or  listner:GetNWBool("IsRadioJammed") then return false, false end
                if talker:IsSpec() then
			    return nil
			elseif talker:HasEquipmentItem( EQUIP_RADIOFREQUENCY ) or listner:HasEquipmentItem( EQUIP_RADIOFREQUENCY ) then
				return nil
			else
				return false, false
			end
		end
	end
	hook.Add( 'PlayerCanHearPlayersVoice', 'ttt_radiojammer_PlayerCanHearPlayersVoice', ENT.PlayerCanHearVoice )

end
