-- Config settings.
-- I haven't used ConVars because well..
-- I tried that and it was a pain replicating it
-- to client.

if SERVER then
	AddCSLuaFile('ttt_perky_config.lua')
end

ttt_perky_config = {
	----------------------------------------------
	----------------------------------------------
	
	radiojammer_duration  = 20,
	radiojammer_limited_stock = true,
	radiojammer_health = 30,
	radiojammer_sound_interval = 0.5,
	
	footprints_interval = 1.5,
	footprints_max = 30,
	----------------------------------------------
	----------------------------------------------
}