local config = ttt_perky_config

if SERVER then
    --resource.AddFile("materials/ttt/footprint.vmt")
    --resource.AddFile("materials/vgui/ttt_fun_killicons/icon_radiofrequency.png")
end
 
EQUIP_RADIOFREQUENCY 	= 8
EQUIP_IRONBOOTS			= 16
EQUIP_PROTECTIONSUIT			= 32

---- -----------------------------------
--- Load the custom equipment, and language stuff.
----- ----------------------------------
local function Init()
	local mat_dir = "vgui/ttt_fun_killicons/"
	local radiofrequency = {  
		id       = EQUIP_RADIOFREQUENCY,
	    loadout  = false, -- default equipment for detectives
	    type     = "item_active",
	    material = mat_dir .. "icon_radiofrequency.png",
	    name     = "Radio Frequency 42Hz",
	    desc     = "Immune to Radio Jammer"
	}


	table.insert( EquipmentItems[ROLE_DETECTIVE], radiofrequency )
	
        local runningshoes = {  
		id       = EQUIP_IRONBOOTS,
	    loadout  = false, -- default equipment for detectives
	    type     = "item_active",
	    material = mat_dir .. "runningshoes.png",
	    name     = "Running Shoes",
	    desc     = "Run twice as fast! Use Shift to run."
	}


	table.insert( EquipmentItems[ROLE_DETECTIVE], runningshoes )


          local protectionshuit = {  
		id       = EQUIP_PROTECTIONSUIT,
	    loadout  = false, -- default equipment for detectives
	    type     = "item_active",
	    material = mat_dir .. "protectionsuit.png",
	    name     = "Protection Suit",
	    desc     = "Protects you from props, burns, drowning and fall damage.\n Also gives you 50% less explosion damage."
	}


	table.insert( EquipmentItems[ROLE_DETECTIVE], protectionshuit )
        table.insert( EquipmentItems[ROLE_TRAITOR], protectionshuit )


 
end
hook.Add( 'InitPostEntity', 'ttt_perky_customsweps_init', Init )

local function checkRunningShoes( ply, equipment, is_item )
    
    if( equipment == EQUIP_IRONBOOTS) then
        ply.hasRunningBoots = true
        ply:SetNWInt("runspeed", 420)
        ply:SetNWInt("walkspeed",320)
        ply:SetJumpPower(350)
    end
    if( equipment == EQUIP_PROTECTIONSUIT) then
        ply.hasProtectionSuit = true
    end 

end


hook.Add( 'TTTOrderedEquipment', "userBoughtItemDorT", checkRunningShoes)

	