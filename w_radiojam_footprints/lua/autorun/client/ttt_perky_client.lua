local config = ttt_perky_config

local footprints = {}
for i = 1, config.footprints_max do table.insert( footprints, {} ) end

local quad = {
	texture = surface.GetTextureID( 'ttt/footprint' ),
	color   = Color( 255, 255, 255, 255 ),
	x = -10,
	y = -10,
	w = 20,
	h = 20	
}

---- -----------------------------------
--- Render footprints
----- ----------------------------------
local function RenderFootprints(  )
	if not LocalPlayer():IsActive() then return end
	
	local wep = LocalPlayer():GetActiveWeapon()
	if wep:IsValid() and wep:GetClass() == 'weapon_ttt_footvision' then	
		local upvec 	 = Vector( 0, 0, 2 )

		-- Set the screen to black and white.
		local tab = {}
		tab[ "$pp_colour_addr" ] = 0
		tab[ "$pp_colour_addg" ] = 0
		tab[ "$pp_colour_addb" ] = 0
		tab[ "$pp_colour_brightness" ] = 0
		tab[ "$pp_colour_contrast" ] = 1
		tab[ "$pp_colour_colour" ] = 0.1
		tab[ "$pp_colour_mulr" ] = 0
		tab[ "$pp_colour_mulg" ] = 0
	    tab[ "$pp_colour_mulb" ] = 0
	    DrawColorModify( tab )
	
		for k, frame in ipairs( footprints ) do
			for __, footprint in ipairs( frame ) do
				cam.Start3D2D( footprint.pos+upvec, Angle( 0, footprint.ang-90, 0 ), 1 )
					render.SuppressEngineLighting( true )
					render.SetColorModulation( 1, 0, 0 )
					quad.color = Color( 255, 0, 0, k*(250/config.footprints_max) )
					draw.TexturedQuad( quad )
					render.SuppressEngineLighting( false )
					render.SetColorModulation( 1, 1, 1 )
				cam.End3D2D()
			end
		end
	end
end
hook.Add( 'PostDrawOpaqueRenderables', 'ttt_perky_renderfootprints', RenderFootprints )

---- -----------------------------------
--- Place all player position into a table.
----- ----------------------------------
local function RecordFootprints( )
	local currentFrame = {}
	local wep
	for k,pl in pairs( player.GetAll() ) do
		if pl:IsActive() and (pl:GetGroundEntity():IsValid() or pl:GetGroundEntity():IsWorld()) then
		 	wep = pl:GetActiveWeapon()
			if not (IsValid( wep ) and wep:GetClass() == 'weapon_ttt_sheepwoolshoes') then
				table.insert( currentFrame, { pos = pl:GetPos(), ang = pl:GetAngles().y } )
			end
		end
	end
	table.insert( footprints, currentFrame )
	if #footprints > config.footprints_max then
		table.remove( footprints, 1 )
	end
end

timer.Create( 'TTT_perky_footprint_timer', 1, 0, RecordFootprints )