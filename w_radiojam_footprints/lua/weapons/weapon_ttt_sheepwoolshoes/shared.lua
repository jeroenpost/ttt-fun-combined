-- Foot Vision.
local config = ttt_perky_config

SWEP.Base					= "weapon_tttbase"
SWEP.HoldType				= "normal"

SWEP.Primary.ClipSize       = -1
SWEP.Primary.DefaultClip    = -1
SWEP.Primary.Automatic      = false
SWEP.Primary.Ammo       	= "none"
SWEP.Primary.Delay 			= 1.0

SWEP.Secondary.ClipSize     = -1
SWEP.Secondary.DefaultClip  = -1
SWEP.Secondary.Automatic    = false
SWEP.Secondary.Ammo     	= "none"
SWEP.Secondary.Delay 		= 1.0

SWEP.IronSightsPos = Vector( 6.05, -5, 2.4 )
SWEP.IronSightsAng = Vector( 2.2, -0.1, 0 )
SWEP.ViewModel			= "models/weapons/v_fists_t.mdl"
SWEP.WorldModel			= "models/weapons/w_fists_t.mdl"

--- TTT config values
SWEP.Kind = WEAPON_EQUIP1
SWEP.AutoSpawnable = false
SWEP.CanBuy = { ROLE_TRAITOR }
SWEP.InLoadoutFor = nil
SWEP.LimitedStock = true
SWEP.AllowDrop = true
SWEP.IsSilent = false
SWEP.NoSights = true

if SERVER then
	AddCSLuaFile( "shared.lua" )
	--resource.AddFile("materials/vgui/ttt_fun_killicons/icon_sheepwoolshoes.png")
end

SWEP.PrintName = "Sheep Wool Shoes"
	SWEP.Slot      = 6 -- add 1 to get the slot number key

if CLIENT then
	

	SWEP.ViewModelFlip = false

	-- Path to the icon material
	SWEP.Icon = "vgui/ttt_fun_killicons/icon_sheepwoolshoes.png"

	-- Text shown in the equip menu
	SWEP.EquipMenuData = {
	   type = "Weapon",
	   desc = "These shoes leave no footprints."
	}
end

function SWEP:OnDrop()
	self:Remove()
end

function SWEP:Deploy()
   	self.Owner:DrawViewModel(false)
	self.drawworld = false
   	return true
end

function SWEP:DrawWorldModel()
   return false
end