-- Foot Vision.
local config = ttt_perky_config

SWEP.Base					= "weapon_tttbase"
SWEP.HoldType				= "normal"
SWEP.drawworld				= false

SWEP.Primary.ClipSize       = -1
SWEP.Primary.DefaultClip    = -1
SWEP.Primary.Automatic      = false
SWEP.Primary.Ammo       	= "none"
SWEP.Primary.Delay 			= 1.0

SWEP.Secondary.ClipSize     = -1
SWEP.Secondary.DefaultClip  = -1
SWEP.Secondary.Automatic    = false
SWEP.Secondary.Ammo     	= "none"
SWEP.Secondary.Delay 		= 1.0

SWEP.IronSightsPos = Vector( 6.05, -5, 2.4 )
SWEP.IronSightsAng = Vector( 2.2, -0.1, 0 )
SWEP.ViewModel  = "models/weapons/v_crowbar.mdl"
SWEP.WorldModel = "models/weapons/w_toolgun.mdl"

--- TTT config values
SWEP.Kind = WEAPON_EQUIP2
SWEP.AutoSpawnable = false
SWEP.CanBuy = { ROLE_DETECTIVE, ROLE_TRAITOR }
SWEP.InLoadoutFor = nil
SWEP.LimitedStock = true
SWEP.AllowDrop = true
SWEP.IsSilent = false
SWEP.NoSights = true

if SERVER then
	AddCSLuaFile( "shared.lua" )
	--resource.AddFile("materials/vgui/ttt_fun_killicons/icon_footvision.png")

end

SWEP.PrintName = "Foot Vision"
	SWEP.Slot      = 7 -- add 1 to get the slot number key

if CLIENT then
	

	SWEP.ViewModelFOV  = 10
	SWEP.ViewModelFlip = false

	-- Path to the icon material
	SWEP.Icon = "vgui/ttt_fun_killicons/icon_footvision.png"

	-- Text shown in the equip menu
	SWEP.EquipMenuData = {
	   type = "Weapon",
	   desc = "Let's you see footprints."
	}
end

function SWEP:OnDrop()
	self.drawworld = true
end

function SWEP:Deploy()
   	self.Owner:DrawViewModel(false)
	self.drawworld = false
   	return true
end