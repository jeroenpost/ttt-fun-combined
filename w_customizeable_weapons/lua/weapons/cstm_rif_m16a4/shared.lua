if ( SERVER ) then
	AddCSLuaFile( "shared.lua" )
	SWEP.NoDocter = true
	SWEP.DoesntDropRail = true
	SWEP.NoVertGrip = true
	SWEP.NoCMag = false
	SWEP.NoGL = false
	SWEP.NoBipod = false

	SWEP.NoSightBG = 1
	SWEP.SightBG = 0
	SWEP.MainBG = 2
end

SWEP.Kind = WEAPON_HEAVY
SWEP.PsID = "cm_m16a4"
SWEP.BulletLength = 5.56
SWEP.CaseLength = 45

SWEP.MuzVel = 616.796

SWEP.Attachments = {
	[1] = {"kobra", "riflereflex", "eotech", "aimpoint", "elcan", "acog"},
	[2] = {"grenadelauncher", "bipod"},
	[3] = {"cmag"},
	[4] = {"laser"}}
	
SWEP.InternalParts = {
	[1] = {{key = "hbar"}, {key = "lbar"}},
	[2] = {{key = "hframe"}},
	[3] = {{key = "ergonomichandle"}},
	[4] = {{key = "customstock"}},
	[5] = {{key = "lightbolt"}, {key = "heavybolt"}},
	[6] = {{key = "gasdir"}}}

if ( CLIENT ) then
	SWEP.ActToCyc = {}
	SWEP.ActToCyc["ACT_VM_DRAW"] = 0
	SWEP.ActToCyc["ACT_VM_RELOAD"] = 0.7
	
	SWEP.LaserTune = {
		PosRight = 0,
		PosForward = 0,
		PosUp = 0,
		AngUp = -90,
		AngRight = 0,
		AngForward = 0 }
	
	SWEP.RollMod = 0.25
	SWEP.PitchMod = 0.5
	SWEP.PrintName			= "M16A4"			
	SWEP.Author				= "Counter-Strike"
	SWEP.Slot = 3 //				= 3
	SWEP.SlotPos = 0 //			= 3
	SWEP.IconLetter			= "z"
	SWEP.NoRail = true
	SWEP.InitialDraw = true
	SWEP.Bone = "Right_L_Arm"
	SWEP.BoneOffset = Vector(0, 0, 5)
	SWEP.CurOffset = Vector(0, 0, 0)
	SWEP.CurAngleOffset = Angle(0, 0, 0)
	SWEP.ActiveOffset = Vector(-4.5, -4.5, 0)
	SWEP.ActiveAngleOffset = Vector(0, 0, -90)
	SWEP.SmokeEffect = "cstm_child_smoke_medium"
	SWEP.SparkEffect = "cstm_child_sparks_medium"
	SWEP.Muzzle = "cstm_muzzle_ar"
	SWEP.ACOGDist = 5
	SWEP.ELCANDist = 4
	
	SWEP.GrenadeLauncher_Idle = {
	["Bip01 L Clavicle"] = { vector = Vector(-2.256, 0.695, 0.301), angle = Angle(0, 0, 0) }
	}
	
	SWEP.GrenadeLauncher_Active = {
		["Bip01 L Clavicle"] = { vector = Vector(-1.362, -2.954, -2.113), angle = Angle(-54.193, 5.467, 0) }
	}
	
	SWEP.VertGrip_Idle = {
		['R_Pinky1'] = {vector = Vector(0, 0, 0), angle = Angle(-30.301000595093, -5.9060001373291, 31.805999755859)},
		['R_Ring3'] = {vector = Vector(0, 0, 0), angle = Angle(-26.25, 0, 0)},
		['R_Pinky3'] = {vector = Vector(0, 0, 0), angle = Angle(-32.381000518799, 0, 0)},
		['R_Thumb2'] = {vector = Vector(0, 0, 0), angle = Angle(0, 33.46900177002, 0)},
		['R_Thumb1'] = {vector = Vector(0, 0, 0), angle = Angle(-18.350999832153, 26.450000762939, 0)},
		['R_Ring2'] = {vector = Vector(0, 0, 0), angle = Angle(-4.2189998626709, 0, 0)},
		['R_Ring1'] = {vector = Vector(0, 0, 0), angle = Angle(-46.51900100708, 7.4499998092651, 11.255999565125)},
		['R_Mid3'] = {vector = Vector(0, 0, 0), angle = Angle(-12.875, 0, 0)},
		['R_Armdummy'] = {vector = Vector(-3.4, 11.8, -2.6), angle = Angle(49.643001556396, -85.625, 0)},
		['R_Mid2'] = {vector = Vector(0, 0, 0), angle = Angle(-20.549999237061, 0, 0)},
		['R_Mid1'] = {vector = Vector(0, 0, 0), angle = Angle(-46.331001281738, 10.293999671936, 9.5059995651245)},
		['R_Index2'] = {vector = Vector(0, 0, 0), angle = Angle(-51.26900100708, 0, 0)},
		['R_Thumb3'] = {vector = Vector(0, 0, 0), angle = Angle(0, 72.305999755859, 0)},
		['R_Index1'] = {vector = Vector(0, 0, 0), angle = Angle(-56.956001281738, 6.0250000953674, 18.386999130249)},
		['R_Index3'] = {vector = Vector(0, 0, 0), angle = Angle(-22.118999481201, 0, 0)}}
	
	SWEP.CMag_Idle = {
	["Bip01 L Finger3"] = { vector = Vector(0, 0, 0), angle = Angle(0, -19.538, 0) },
	["Bip01 L Finger4"] = { vector = Vector(0, 0, 0), angle = Angle(0, -16.188, 0) },
	["Bip01 L Finger41"] = { vector = Vector(0, 0, 0), angle = Angle(0, 19.038, 0) },
	["Bip01 L Finger2"] = { vector = Vector(0, 0, 0), angle = Angle(0, -9.778, 0) },
	["Bip01 L Finger0"] = { vector = Vector(-0.11, 0, 0), angle = Angle(1.616, 0.903, 0) },
	["Bip01 L Finger31"] = { vector = Vector(0, 0, 0), angle = Angle(0, 27.274, 0) },
	["Bip01 L Clavicle"] = { vector = Vector(-0.087, 0.751, 0.609), angle = Angle(1.718, -1.089, 0) },
	["Bip01 L Forearm"] = { vector = Vector(0, 0, 0), angle = Angle(0, 1, 0) },
	["Bip01 L Finger21"] = { vector = Vector(0, 0, 0), angle = Angle(0, 27.163, 0) },
	["Bip01 L Finger11"] = { vector = Vector(0, 0, 0), angle = Angle(0, 25.239, 0) },
	["Bip01 L Finger01"] = { vector = Vector(0, 0, 0), angle = Angle(0, -7.276, 0) },
	["Bip01 L UpperArm"] = { vector = Vector(3.637, -1.635, -0.271), angle = Angle(11.802, 0.342, 0) }}
	
	SWEP.CMag_Reload = {
	["Bip01 L Finger3"] = { vector = Vector(0, 0, 0), angle = Angle(0, 0, 0) },
	["Bip01 L Finger4"] = { vector = Vector(0, 0, 0), angle = Angle(0, 0, 0) },
	["Bip01 L Finger41"] = { vector = Vector(0, 0, 0), angle = Angle(0, 0, 0) },
	["Bip01 L Finger2"] = { vector = Vector(0, 0, 0), angle = Angle(0, 0, 0) },
	["Bip01 L Finger0"] = { vector = Vector(0, 0, 0), angle = Angle(0, 0, 0) },
	["Bip01 L Finger31"] = { vector = Vector(0, 0, 0), angle = Angle(0, 0, 0) },
	["Bip01 L Clavicle"] = { vector = Vector(0, 0, 0), angle = Angle(0, 0, 0) },
	["Bip01 L Forearm"] = { vector = Vector(0, 0, 0), angle = Angle(0, 0, 0) },
	["Bip01 L Finger21"] = { vector = Vector(0, 0, 0), angle = Angle(0, 0, 0) },
	["Bip01 L Finger11"] = { vector = Vector(0, 0, 0), angle = Angle(0, 0, 0) },
	["Bip01 L Finger01"] = { vector = Vector(0, 0, 0), angle = Angle(0, 0, 0) },
	["Bip01 L UpperArm"] = { vector = Vector(0, 0, 0), angle = Angle(0, 0, 0) }}
	
	SWEP.MagBone = "Magazine001"
	
	SWEP.DifType = true

	
	SWEP.VElements = {
		["bipod"] = { type = "Model", model = "models/wystan/attachments/bipod.mdl", bone = "smdimport001", rel = "", pos = Vector(0.075, 11.234, 0.647), angle = Angle(0, 180, 0), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {[1] = 1} },
		["acog"] = { type = "Model", model = "models/wystan/attachments/2cog.mdl", bone = "smdimport001", rel = "", pos = Vector(-0.281, -3.027, -0.969), angle = Angle(0, 0, 3), size = Vector(0.8, 0.8, 0.8), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
		["elcan"] = { type = "Model", model = "models/bunneh/elcan.mdl", bone = "smdimport001", rel = "", pos = Vector(-0.281, -3.027, -0.969), angle = Angle(0, 0, 3), size = Vector(0.8, 0.8, 0.8), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
		["aimpoint"] = { type = "Model", model = "models/wystan/attachments/aimpoint.mdl", bone = "smdimport001", rel = "", pos = Vector(-0.24, -4.388, -2.20), angle = Angle(0, 0, 1), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
		["cmag"] = { type = "Model", model = "models/wystan/cmag.mdl", bone = "Magazine001", rel = "", pos = Vector(-0.08, -3.612, -1.366), angle = Angle(0, -90, 0), size = Vector(0.699, 0.699, 0.699), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
		["grenadelauncher"] = { type = "Model", model = "models/wystan/attachments/m203.mdl", bone = "smdimport001", rel = "", pos = Vector(-0.244, -3.1, -1.392), angle = Angle(0, 0, 0), size = Vector(0.8, 0.8, 0.8), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
		["riflereflex"] = { type = "Model", model = "models/attachments/kascope.mdl", bone = "smdimport001", rel = "", pos = Vector(0, 4.408, 3.594), angle = Angle(0, 0, 2), size = Vector(0.8, 0.8, 0.8), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
		["eotech"] = { type = "Model", model = "models/wystan/attachments/2otech557sight.mdl", bone = "smdimport001", rel = "", pos = Vector(0.273, -8.513, -7.808), angle = Angle(3, -90, 0), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
		["kobra"] = { type = "Model", model = "models/attachments/cmore.mdl", bone = "smdimport001", rel = "", pos = Vector(0, 2.345, 3.38), angle = Angle(0, 0, 2), size = Vector(0.699, 0.699, 0.699), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
		["laser"] = { type = "Model", model = "models/props_c17/furnitureboiler001a.mdl", bone = "smdimport001", rel = "", pos = Vector(-0.712, 12.26, 2.855), angle = Angle(0, 0, -88), size = Vector(0.029, 0.029, 0.029), color = Color(255, 255, 255, 0), surpresslightning = false, material = "models/bunneh/silencer", skin = 0, bodygroup = {} },
		["silencer"] = { type = "Model", model = "models/props_c17/oildrum001.mdl", bone = "smdimport001", rel = "", pos = Vector(0, 17.726, 1.738), angle = Angle(93, 90, 0), size = Vector(0.05, 0.05, 0.129), color = Color(255, 255, 255, 0), surpresslightning = false, material = "models/bunneh/silencer", skin = 0, bodygroup = {} }
	}
	
	SWEP.WElements = {
		["acog"] = { type = "Model", model = "models/wystan/attachments/2cog.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(0, 0.228, -0.398), angle = Angle(0, -90, 180), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
		["elcan"] = { type = "Model", model = "models/bunneh/elcan.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(0, 0.228, -0.398), angle = Angle(0, -90, 180), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
		["aimpoint"] = { type = "Model", model = "models/wystan/attachments/aimpoint.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(0, 0.727, -0.639), angle = Angle(0, -90, 180), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
		["kobra"] = { type = "Model", model = "models/attachments/cmore.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(7.131, 0.898, -6.154), angle = Angle(0, -90, 180), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
		["riflereflex"] = { type = "Model", model = "models/attachments/kascope.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(8.64, 0.836, -6.423), angle = Angle(0, -90, 180), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
		["eotech"] = { type = "Model", model = "models/wystan/attachments/2otech557sight.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(-5.588, 1.205, 4.407), angle = Angle(0, 0, 180), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
		["weapon"] = { type = "Model", model = "models/weapons/w_cst_m16a4.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(-2.516, 0.867, -0.24), angle = Angle(0, 0, 180), size = Vector(1, 1, 1), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
		["vertgrip"] = { type = "Model", model = "models/wystan/attachments/foregrip1.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(16, 16.249, -1.139), angle = Angle(0, 0, 180), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
		["grenadelauncher"] = { type = "Model", model = "models/wystan/attachments/m203.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(0.705, 1.368, -0.871), angle = Angle(180, 90, 0), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
		["bipod"] = { type = "Model", model = "models/wystan/attachments/bipod.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(20.857, 1.18, -2.975), angle = Angle(0, 90, 180), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {[1] = 1} }}
end

SWEP.HoldType			= "ar2"
SWEP.Base				= "cstm_base_pistol"
SWEP.Category = "Extra Pack (ARs)"

SWEP.FireModes = {"auto", "3burst", "semi"}

SWEP.Spawnable			= true
SWEP.AdminSpawnable		= true

SWEP.ViewModelFOV = 70
SWEP.ViewModelFlip = false
SWEP.ViewModel = "models/weapons/v_cstm_m16a4.mdl"
SWEP.WorldModel = "models/weapons/w_rif_m4a1.mdl"
SWEP.ShowViewModel = true
SWEP.ShowWorldModel = false
SWEP.ViewModelBonescales = {}

SWEP.Weight				= 5
SWEP.AutoSwitchTo		= false
SWEP.AutoSwitchFrom		= false

SWEP.Primary.Sound			= Sound("CSTM_M16a4")
SWEP.Primary.Recoil			= 0.3
SWEP.Primary.Damage			= 13
SWEP.Primary.NumShots		= 1
SWEP.Primary.Cone			= 0.02
SWEP.Primary.ClipSize		= 200
SWEP.Primary.Delay			= 0.0631
SWEP.Primary.DefaultClip	= 200
SWEP.Primary.Automatic		= true
SWEP.Primary.Ammo			= "5.56x45MM"
SWEP.InitialHoldtype = "smg"
SWEP.InHoldtype = "smg"
SWEP.SilencedSound = Sound("CSTM_SilencedShot4")
SWEP.SilencedVolume = 75

-- Animation speed/custom reload function related
SWEP.IsReloading = false
SWEP.NoBoltAnim = true
SWEP.AnimPrefix = ""
SWEP.ReloadSpeed = 1
SWEP.ShouldBolt = false
SWEP.ReloadDelay = 0
SWEP.IncAmmoPerc = 0.5 -- Amount of frames required to pass (in percentage) of the reload animation for the weapon to have it's amount of ammo increased
SWEP.NumShots = 0

-- Dynamic accuracy related
SWEP.ShotsAmount 			= 0
SWEP.ConeDecAff				= 0
SWEP.DefRecoil				= 1
SWEP.CurCone				= 0.04
SWEP.DecreaseRecoilTime 	= 0
SWEP.ConeAff1 				= 0 -- Crouching/standing
SWEP.ConeAff2 				= 0 -- Using ironsights
SWEP.ViewPunchCycle 		= 0
SWEP.ViewPunchDelay			= 0
SWEP.WalkPunchCycle			= 0
SWEP.WalkPunchDelay 		= 0
SWEP.UnConeTime				= 0 -- Amount of time after firing the last shot that needs to pass until accuracy increases
SWEP.FinalCone				= 0 -- Self explanatory
SWEP.VelocitySensivity		= 1.8 -- Percentage of how much the cone increases depending on the player's velocity (moving speed). Rifles - 100%; SMGs - 80%; Pistols - 60%; Shotguns - 20%
SWEP.HeadbobMul 			= 1
SWEP.IronsightsCone			= 0.0015
SWEP.HipCone 				= 0.063
SWEP.InaccAff1 = 0.75
SWEP.ConeInaccuracyAff1 = 0.7

SWEP.Secondary.Automatic	= false
SWEP.Secondary.Ammo			= "40MM_HE"
SWEP.Secondary.ClipSize = 1
SWEP.Secondary.DefaultClip	= 0

SWEP.IsUsingIronsights 		= false
SWEP.TargetMul = 0
SWEP.SetAndForget			= false

SWEP.IronSightsPos = Vector(-2.171, -2.987, 0.3)
SWEP.IronSightsAng = Vector(0.07, 0.064, 0)

SWEP.AimPos = Vector(-2.171, -4.5, 0.3)
SWEP.AimAng = Vector(0.07, 0.01, 0)

SWEP.ChargePos = Vector (5.4056, -10.3522, -4.0017)
SWEP.ChargeAng = Vector (-1.7505, -55.5187, 68.8356)

SWEP.ScopePos = Vector(-2.141, -2.415, 0.458)
SWEP.ScopeAng = Vector(-0.257, 0.14, 0)

SWEP.NoFlipOriginsPos = Vector(-1.16, 0, -0.16)
SWEP.NoFlipOriginsAng = Vector(0, 0, 0)

SWEP.ReflexPos = Vector(-2.17, -2.437, 0.25)
SWEP.ReflexAng = Vector(0.067, 0, 0)

SWEP.KobraPos = Vector(-2.162, -2.27, 0.601)
SWEP.KobraAng = Vector(-0.714, 0.075, 0)

SWEP.ACOGPos = Vector(-2.168, -3.563, 0.379)
SWEP.ACOGAng = Vector(0, 0, 0)

SWEP.ACOG_BackupPos = Vector(-2.168, -3.563, -0.53)
SWEP.ACOG_BackupAng = Vector(0.5, 0, 0)

SWEP.ELCANPos = Vector(-2.168, -3, 0.35)
SWEP.ELCANAng = Vector(0, 0, 0)

SWEP.ELCAN_BackupPos = Vector(-2.168, -3, -0.53)
SWEP.ELCAN_BackupAng = Vector(1, 0, 0)

SWEP.RReflexPos = Vector(-2.161, -1.951, 0.519)
SWEP.RReflexAng = Vector(-0.494, 0.068, 0)

SWEP.CustomizePos = Vector(8.173, -4.106, -0.895)
SWEP.CustomizeAng = Vector(30.736, 40.667, 28.569)

if CLIENT then
	function SWEP:CanUseBackUp()
		if self.VElements["acog"].color.a == 255 or self.VElements["elcan"].color.a == 255 then
			return true
		end
		
		return false
	end
	
	function SWEP:UseBackUp()
		if self.VElements["acog"].color.a == 255 then
			if self.AimPos == self.ACOG_BackupPos then
				self.AimPos = self.ACOGPos
				self.AimAng = self.ACOGAng
			else
				self.AimPos = self.ACOG_BackupPos
				self.AimAng = self.ACOG_BackupAng
			end
		elseif self.VElements["elcan"].color.a == 255 then
			if self.AimPos == self.ELCAN_BackupPos then
				self.AimPos = self.ELCANPos
				self.AimAng = self.ELCANAng
			else
				self.AimPos = self.ELCAN_BackupPos
				self.AimAng = self.ELCAN_BackupAng
			end
		end
	end
end