if ( SERVER ) then

	AddCSLuaFile( "shared.lua" )
	SWEP.HasRail = true
	SWEP.NoEOTech = true
	SWEP.NoDocter = true
	SWEP.NoAimpoint = true
	SWEP.RequiresRail = false
	
	SWEP.NoSightBG = 2
	SWEP.SightBG = 0
	SWEP.MainBG = 2
end

SWEP.BulletLength = 7.62
SWEP.CaseLength = 51

SWEP.MuzVel = 557.741

SWEP.Attachments = {
	[1] = {"kobra", "riflereflex", "eotech", "aimpoint", "elcan", "acog", "ballistic"},
	[2] = {"vertgrip", "grenadelauncher", "bipod"},
	[3] = {"laser"}}
	
SWEP.InternalParts = {
	[1] = {{key = "hbar"}, {key = "lbar"}},
	[2] = {{key = "hframe"}},
	[3] = {{key = "ergonomichandle"}},
	[4] = {{key = "customstock"}},
	[5] = {{key = "lightbolt"}, {key = "heavybolt"}},
	[6] = {{key = "gasdir"}}}

if ( CLIENT ) then
	SWEP.PrintName			= "FN SCAR-H"			
	SWEP.Author				= "Counter-Strike"
	SWEP.Slot = 2 //				= 3
	SWEP.SlotPos = 0 //			= 3
	SWEP.IconLetter			= "z"
	SWEP.DifType = true
	SWEP.SmokeEffect = "cstm_child_smoke_large"
	SWEP.SparkEffect = "cstm_child_sparks_large"
	SWEP.Muzzle = "cstm_muzzle_br"
	SWEP.ACOGDist = 6
	SWEP.BallisticDist = 4.5
	SWEP.NoRail = true
	
	SWEP.ActToCyc = {}
	SWEP.ActToCyc["ACT_VM_DRAW"] = 0.45
	SWEP.ActToCyc["ACT_VM_RELOAD"] = 0.8
	
	SWEP.LaserTune = {
		PosRight = 0,
		PosForward = 0,
		PosUp = 0,
		AngUp = -90,
		AngRight = 0,
		AngForward = 0 }
		
	SWEP.VertGrip_Idle = {
		['Left24'] = {vector = Vector(0, 0, 0), angle = Angle(12, 22.194000244141, 0)},
		['Left5'] = {vector = Vector(0, 0, 0), angle = Angle(44.180999755859, 0, 0)},
		['Left_L_Arm'] = {vector = Vector(0.75, 1, 1), angle = Angle(0, 0, 94.130996704102)},
		['Left3'] = {vector = Vector(0, 0, 0), angle = Angle(0, 56.924999237061, 0)},
		['Left17'] = {vector = Vector(0, 0, 0), angle = Angle(22.368999481201, 0, 0)},
		['Left19'] = {vector = Vector(0, 0, 0), angle = Angle(20.893999099731, 0, 0)},
		['Left15'] = {vector = Vector(0, 0, 0), angle = Angle(47.96900177002, 0, 0)},
		['Left14'] = {vector = Vector(0, 0, 0), angle = Angle(35.25, 0, 0)},
		['Left12'] = {vector = Vector(0, 0, 0), angle = Angle(7.4310002326965, 0, 0)},
		['Left8'] = {vector = Vector(0, 0, 0), angle = Angle(29.837999343872, 0, 0)},
		['Left2'] = {vector = Vector(0, 0, 0), angle = Angle(0, 55.01900100708, 0)},
		['Left6'] = {vector = Vector(0, 0, 0), angle = Angle(12.769000053406, 0, 0)},
		['Left18'] = {vector = Vector(0, 0, 0), angle = Angle(27.299999237061, 0, 0)},
		['Left_U_Arm'] = {vector = Vector(-0.59399998188019, 0.83700001239777, 0.75), angle = Angle(0, 0, 0)},
		['Left11'] = {vector = Vector(0, 0, 0), angle = Angle(29.662000656128, 0, 0)},
		['Left16'] = {vector = Vector(0, 0, 0), angle = Angle(-29.756000518799, 0, 0)}}
		
	SWEP.GrenadeLauncher_Idle = {
		['Left_U_Arm'] = {vector = Vector(2.4809999465942, -3, -1), angle = Angle(0, 0, 0)}}
	
	SWEP.WepSelectIcon = surface.GetTextureID("VGUI/kills/kill_scarh")
	killicon.Add("cstm_rif_scarh", "VGUI/kills/kill_scarh", Color( 255, 80, 0, 255 ))
	
	SWEP.VElements = {
		["kobra"] = { type = "Model", model = "models/attachments/cmore.mdl", bone = "scar", rel = "", pos = Vector(0.013, 1.026, 2.875), angle = Angle(0, 0, 0), size = Vector(0.699, 0.699, 0.699), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
		["riflereflex"] = { type = "Model", model = "models/attachments/kascope.mdl", bone = "scar", rel = "", pos = Vector(0, 2.625, 3.055), angle = Angle(0, 0, 0), size = Vector(0.699, 0.699, 0.699), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
		["acog"] = { type = "Model", model = "models/wystan/attachments/2cog.mdl", bone = "scar", rel = "", pos = Vector(-0.357, -5.182, -2.839), angle = Angle(0, 0, 0), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
		["elcan"] = { type = "Model", model = "models/bunneh/elcan.mdl", bone = "scar", rel = "", pos = Vector(-0.357, -5.182, -2.839), angle = Angle(0, 0, 0), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
		["vertgrip"] = { type = "Model", model = "models/wystan/attachments/foregrip1.mdl", bone = "scar", rel = "", pos = Vector(-10.608, 7.519, -1.701), angle = Angle(0, 90, 0), size = Vector(0.859, 0.859, 0.859), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
		["bipod"] = { type = "Model", model = "models/wystan/attachments/bipod.mdl", bone = "scar", rel = "", pos = Vector(0.075, 7.717, -0.294), angle = Angle(0, 0, 0), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {[1] = 1} },
		["eotech"] = { type = "Model", model = "models/wystan/attachments/2otech557sight.mdl", bone = "scar", rel = "", pos = Vector(0.275, -9.919, -8.556), angle = Angle(4.443, -90, 0), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
		["silencer"] = { type = "Model", model = "models/props_c17/oildrum001.mdl", bone = "scar", rel = "", pos = Vector(-0.007, 14.225, 1.332), angle = Angle(0, 0, 90), size = Vector(0.05, 0.05, 0.15), color = Color(255, 255, 255, 0), surpresslightning = false, material = "models/bunneh/silencer", skin = 0, bodygroup = {} },
		["laser"] = { type = "Model", model = "models/props_c17/FurnitureBoiler001a.mdl", bone = "scar", rel = "", pos = Vector(-0.888, 8.211, 1.312), angle = Angle(90, 0, -90), size = Vector(0.029, 0.029, 0.029), color = Color(255, 255, 255, 0), surpresslightning = false, material = "models/bunneh/silencer", skin = 0, bodygroup = {} },
		["grenadelauncher"] = { type = "Model", model = "models/wystan/attachments/m203.mdl", bone = "scar", rel = "", pos = Vector(-0.245, -5.6, -2.776), angle = Angle(0, 0, 0), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
		["aimpoint"] = { type = "Model", model = "models/wystan/attachments/aimpoint.mdl", bone = "scar", rel = "", pos = Vector(-0.232, -5.944, -2.783), angle = Angle(0, 0, 0), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
		["ballistic"] = { type = "Model", model = "models/bunneh/scope01.mdl", bone = "scar", rel = "", pos = Vector(-1.331, -7.045, 3.43), angle = Angle(0, -90, 0), size = Vector(1.049, 1.049, 1.049), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
	}
	
	SWEP.WElements = {
		["kobra"] = { type = "Model", model = "models/attachments/cmore.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(6.837, 1.006, -6.963), angle = Angle(0, -90, 180), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
		["riflereflex"] = { type = "Model", model = "models/attachments/kascope.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(9.968, 0.994, -7.389), angle = Angle(0, -90, 180), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
		["acog"] = { type = "Model", model = "models/wystan/attachments/2cog.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(1.906, 0.72, -1.5), angle = Angle(0, -90, 180), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
		["elcan"] = { type = "Model", model = "models/bunneh/elcan.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(1.906, 0.72, -1.5), angle = Angle(0, -90, 180), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
		["vertgrip"] = { type = "Model", model = "models/wystan/attachments/foregrip1.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(16.212, 13.112, -1.344), angle = Angle(0, 0, 180), size = Vector(0.8, 0.8, 0.8), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
		["bipod"] = { type = "Model", model = "models/wystan/attachments/bipod.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(18.381, 1.131, -2.925), angle = Angle(0, -90, 180), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {[1] = 1} },
		["eotech"] = { type = "Model", model = "models/wystan/attachments/2otech557sight.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(-4.731, 1.299, 4.355), angle = Angle(-4.444, 0, 180), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
		["aimpoint"] = { type = "Model", model = "models/wystan/attachments/aimpoint.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(0, 0.805, -1.438), angle = Angle(0, -90, 180), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
		["grenadelauncher"] = { type = "Model", model = "models/wystan/attachments/m203.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(1.424, 0.755, -0.532), angle = Angle(180, 90, 0), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
		["weapon"] = { type = "Model", model = "models/weapons/w_rif_scarh.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(0, 0.43, -0.62), angle = Angle(0, 0, 180), size = Vector(1, 1, 1), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
	}
end
SWEP.Kind = WEAPON_HEAVY
SWEP.HoldType			= "ar2"
SWEP.Base				= "cstm_base_pistol"
SWEP.Category = "Extra Pack (BRs)"

SWEP.FireModes = {"auto", "semi"}

SWEP.Spawnable			= true
SWEP.AdminSpawnable		= true

SWEP.ViewModelFOV = 70
SWEP.ViewModelFlip = false
SWEP.ViewModel = "models/weapons/v_cstm_scarh.mdl"
SWEP.WorldModel = "models/weapons/w_rif_galil.mdl"
SWEP.ShowViewModel = true
SWEP.ShowWorldModel = false
SWEP.ViewModelBonescales = {}

SWEP.Weight				= 5
SWEP.AutoSwitchTo		= false
SWEP.AutoSwitchFrom		= false

SWEP.Primary.Sound			= Sound("CSTM_SCARH")
SWEP.Primary.Recoil			= 0.3
SWEP.Primary.Damage			= 19
SWEP.Primary.NumShots		= 1
SWEP.Primary.Cone			= 0.02
SWEP.Primary.ClipSize		= 100
SWEP.Primary.Delay			= 0.096
SWEP.Primary.DefaultClip	= 100
SWEP.Primary.Automatic		= true
SWEP.Primary.Ammo			= "7.62x51MM"
SWEP.InitialHoldtype = "smg"
SWEP.InHoldtype = "smg"
SWEP.SilencedSound = Sound("CSTM_SilencedShot2")
SWEP.SilencedVolume = 78

-- Animation speed/custom reload function related
SWEP.IsReloading = false
SWEP.NoBoltAnim = true
SWEP.AnimPrefix = ""
SWEP.ReloadSpeed = 1
SWEP.ShouldBolt = false
SWEP.ReloadDelay = 0
SWEP.IncAmmoPerc = 0.63 -- Amount of frames required to pass (in percentage) of the reload animation for the weapon to have it's amount of ammo increased

-- Dynamic accuracy related
SWEP.ShotsAmount 			= 0
SWEP.ConeDecAff				= 0
SWEP.DefRecoil				= 1.55
SWEP.CurCone				= 0.04
SWEP.DecreaseRecoilTime 	= 0
SWEP.ConeAff1 				= 0 -- Crouching/standing
SWEP.ConeAff2 				= 0 -- Using ironsights

SWEP.UnConeTime				= 0 -- Amount of time after firing the last shot that needs to pass until accuracy increases
SWEP.FinalCone				= 0 -- Self explanatory
SWEP.VelocitySensivity		= 2.2 -- Percentage of how much the cone increases depending on the player's velocity (moving speed). Rifles - 100%; SMGs - 80%; Pistols - 60%; Shotguns - 20%
SWEP.HeadbobMul 			= 1
SWEP.IsSilenced 			= false
SWEP.IronsightsCone			= 0.001
SWEP.HipCone 				= 0.06
SWEP.InaccAff1 = 0.75
SWEP.ConeInaccuracyAff1 = 0.7

SWEP.Secondary.Automatic	= false
SWEP.Secondary.Ammo			= "40MM_HE"
SWEP.Secondary.ClipSize = 1
SWEP.Secondary.DefaultClip	= 0

SWEP.IsUsingIronsights 		= false
SWEP.TargetMul = 0
SWEP.SetAndForget			= false

SWEP.IronSightsPos = Vector(6.0749, -5.5216, 2.3984)
SWEP.IronSightsAng = Vector(2.5174, -0.0099, 0)

SWEP.AimPos = Vector(-2.018, -3.135, 0.12)
SWEP.AimAng = Vector(0, 0, 0)

SWEP.ChargePos = Vector (5.4056, -10.3522, -4.0017)
SWEP.ChargeAng = Vector (-1.7505, -55.5187, 68.8356)

SWEP.NoFlipOriginsPos = Vector(-0.361, 0, 0.159)
SWEP.NoFlipOriginsAng = Vector(0, 0, 0)

SWEP.KobraPos = Vector(-2.014, -2.681, 0.453)
SWEP.KobraAng = Vector(0, 0, 0)

SWEP.RReflexPos = Vector(-2.014, -2.681, 0.653)
SWEP.RReflexAng = Vector(0, 0, 0)

SWEP.BallisticPos = Vector(-2.02, -4, 0.363)
SWEP.BallisticAng = Vector(0, 0, 0)

SWEP.ReflexPos = Vector(-2.03, -2.681, 0.17)
SWEP.ReflexAng = Vector(0, 0, 0)

SWEP.ScopePos = Vector(-2.008, -2.681, 0.367)
SWEP.ScopeAng = Vector(0, 0, 0)

SWEP.ACOGPos = Vector(-2.02, -2.681, 0.041)
SWEP.ACOGAng = Vector(0, 0, 0)

SWEP.ACOG_BackupPos = Vector(-2.02, -2.681, -1.08)
SWEP.ACOG_BackupAng = Vector(0, 0, 0)

SWEP.ELCANPos = Vector(-2.02, -2.681, 0.02)
SWEP.ELCANAng = Vector(0, 0, 0)

SWEP.ELCAN_BackupPos = Vector(-2.02, -2.681, -0.98)
SWEP.ELCAN_BackupAng = Vector(0, 0, 0)

if CLIENT then
	function SWEP:CanUseBackUp()
		if self.VElements["acog"].color.a == 255 or self.VElements["elcan"].color.a == 255 then
			return true
		end
		
		return false
	end
	
	function SWEP:UseBackUp()
		if self.VElements["acog"].color.a == 255 then
			if self.AimPos == self.ACOG_BackupPos then
				self.AimPos = self.ACOGPos
				self.AimAng = self.ACOGAng
			else
				self.AimPos = self.ACOG_BackupPos
				self.AimAng = self.ACOG_BackupAng
			end
		elseif self.VElements["elcan"].color.a == 255 then
			if self.AimPos == self.ELCAN_BackupPos then
				self.AimPos = self.ELCANPos
				self.AimAng = self.ELCANAng
			else
				self.AimPos = self.ELCAN_BackupPos
				self.AimAng = self.ELCAN_BackupAng
			end
		end
	end
end