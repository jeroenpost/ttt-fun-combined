if ( SERVER ) then

	AddCSLuaFile( "shared.lua" )
	SWEP.HasRail = false
	SWEP.NoAimpoint = false
	SWEP.NoACOG = false
	SWEP.NoDocter = false
	SWEP.DoesntDropRail = false
	SWEP.NoVertGrip = false
	
	SWEP.RequiresRail = false
end

SWEP.Kind = WEAPON_HEAVY

SWEP.BulletLength = 9
SWEP.CaseLength = 19

SWEP.MuzVel = 255.905

SWEP.Attachments = {
	[1] = {"elcan"},
	[2] = {"laser"}}
	

SWEP.InternalParts = {
	[1] = {{key = "hbar"}, {key = "lbar"}},
	[2] = {{key = "hframe"}},
	[3] = {{key = "ergonomichandle"}},
	[4] = {{key = "customstock"}},
	[5] = {{key = "lightbolt"}, {key = "heavybolt"}},
	[6] = {{key = "gasdir"}}}

if ( CLIENT ) then

	SWEP.PrintName			= "PPSH-41"			
	SWEP.Author				= "Counter-Strike"
	SWEP.Slot				= 2
	SWEP.SlotPos = 0 //			= 1
	SWEP.ReloadAngleMod = -0.75
	SWEP.NoRail = true
	SWEP.DifType = true
	SWEP.SparkEffect = "cstm_child_sparks_small"
	SWEP.Muzzle = "cstm_muzzle_smg"
	SWEP.SmokeEffect = "cstm_child_smoke_small"


	
        SWEP.VElements = {
	["elcan"] = { type = "Model", model = "models/bunneh/elcan.mdl", bone = "a_ppsh41", rel = "", pos = Vector(1.531, 0.412, 4.032), angle = Angle(0.638, 89.544, -180), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	--["acog"] = { type = "Model", model = "models/wystan/attachments/2cog.mdl", bone = "a_ppsh41", rel = "", pos = Vector(2.019, 0.324, 4.007), angle = Angle(-180, -90, 0), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },--
	["laser"] = { type = "Model", model = "models/props_c17/FurnitureBoiler001a.mdl", bone = "a_ppsh41", rel = "", pos = Vector(-9.639, -0.459, -0.528), angle = Angle(-90, 0.03, 0), size = Vector(0.028, 0.028, 0.028), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["silencer"] = { type = "Model", model = "models/props_c17/oildrum001.mdl", bone = "a_ppsh41", rel = "", pos = Vector(-17.469, -0.028, -1.195), angle = Angle(91.761, 1.182, -2.037), size = Vector(0.048, 0.048, 0.075), color = Color(0, 0, 0, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
}

       SWEP.WElements = {
	["elcan"] = { type = "Model", model = "models/bunneh/elcan.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(5.13, 0.017, 0.37), angle = Angle(-180, 89.536, -3.888), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	--["acog"] = { type = "Model", model = "models/wystan/attachments/2cog.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(5.407, 0.159, 0.606), angle = Angle(-180, 91.749, -5.371), size = Vector(1, 1, 1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },--
	["weapon"] = { type = "Model", model = "models/weapons/w_smg_ppsh.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(-6.329, 0.476, 1.883), angle = Angle(-180, -88.334, 3.953), size = Vector(1,1,1), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["silencer"] = { type = "Model", model = "models/props_c17/oildrum001.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(22.631, 0.145, -6.003), angle = Angle(-94.931, 0, 0), size = Vector(0.048, 0.048, 0.063), color = Color(0, 0, 0, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
}
end

SWEP.HoldType			= "smg"
SWEP.Base				= "cstm_base_pistol"
SWEP.Category = "Customizable Weaponry WW2"
SWEP.FireModes = {"auto", "semi"}
SWEP.Spawnable			= true
SWEP.AdminSpawnable		= true

SWEP.ViewModelFOV = 70
SWEP.ViewModelFlip = true
SWEP.ViewModel = "models/weapons/v_smg_ppsh.mdl"
SWEP.WorldModel = "models/weapons/w_smg_ppsh.mdl"
SWEP.ShowViewModel = true
SWEP.ShowWorldModel = false
SWEP.ViewModelBonescales = {}

SWEP.Weight				= 5
SWEP.AutoSwitchTo		= false
SWEP.AutoSwitchFrom		= false

SWEP.Primary.Sound			= Sound("CSTM_PPSH")
SWEP.Primary.Recoil			= 0.3
SWEP.Primary.Damage			= 13
SWEP.Primary.NumShots		= 1
SWEP.Primary.Cone			= 0.02
SWEP.Primary.ClipSize		= 330
SWEP.Primary.Delay			= 0.075
SWEP.Primary.DefaultClip	= 330
SWEP.Primary.Automatic		= true
SWEP.Primary.Ammo			= "9x19MM"
SWEP.InitialHoldtype = "smg"
SWEP.InHoldtype = "smg"
SWEP.SilencedVolume = 70
SWEP.SilencedSound = Sound("CSTM_SilencedShot7")

-- Animation speed/custom reload function related
SWEP.IsReloading = false
SWEP.AnimPrefix = ""
SWEP.ReloadSpeed = 1
SWEP.ShouldBolt = false
SWEP.SkipIdle = true
SWEP.ReloadDelay = 0
SWEP.IncAmmoPerc = 0.7 -- Amount of frames required to pass (in percentage) of the reload animation for the weapon to have it's amount of ammo increased
SWEP.NoBoltAnim = true

-- Dynamic accuracy related
SWEP.ShotsAmount 			= 0
SWEP.ConeDecAff				= 0
SWEP.DefRecoil				= 0.6
SWEP.CurCone				= 0.038
SWEP.DecreaseRecoilTime 	= 0
SWEP.ConeAff1 				= 0 -- Crouching/standing
SWEP.ConeAff2 				= 0 -- Using ironsights

SWEP.UnConeTime				= 0 -- Amount of time after firing the last shot that needs to pass until accuracy increases
SWEP.FinalCone				= 0 -- Self explanatory
SWEP.VelocitySensivity		= 1.2 -- Percentage of how much the cone increases depending on the player's velocity (moving speed). Rifles - 100%; SMGs - 80%; Pistols - 60%; Shotguns - 20%
SWEP.IsSilenced 			= ture
SWEP.IronsightsCone			= 0.011
SWEP.HipCone 				= 0.04
SWEP.InaccAff1 = 0.45
SWEP.ConeInaccuracyAff1 = 0.55

SWEP.Secondary.Automatic	= false
SWEP.Secondary.Ammo			= "none"

SWEP.IsUsingIronsights 		= true
SWEP.TargetMul = 0
SWEP.SetAndForget			= false
SWEP.IronSights = 0

SWEP.IronSightsPos = Vector(3.734, -7.068, 1.922)
SWEP.IronSightsAng = Vector(1.065, -0.064, 0)

SWEP.AimPos = Vector(3.734, -7.068, 1.922)
SWEP.AimAng = Vector(1.065, -0.064, 0)

SWEP.ACOGPos = Vector(3.701, -6.443, 1.34)
SWEP.ACOGAng = Vector(0, 0, 0)

SWEP.ACOG_BackupPos = Vector(3.701, -8.4, 0.287)
SWEP.ACOG_BackupAng = Vector(-0.422, 0, 0)

SWEP.ELCANPos = Vector(3.7, -4.817, 1.333)
SWEP.ELCANAng = Vector(0, 0, 0)


SWEP.ELCAN_BackupPos = Vector(3.74, -8.131, 0.349)
SWEP.ELCAN_BackupAng = Vector(0.112, 0.537, 0)

if CLIENT then
	function SWEP:CanUseBackUp()
		if self.VElements["acog"].color.a == 255 or self.VElements["elcan"].color.a == 255 then
			return true
		end
		
		return false
	end
	
	function SWEP:UseBackUp()
		if self.VElements["acog"].color.a == 255 then
			if self.AimPos == self.ACOG_BackupPos then
				self.AimPos = self.ACOGPos
				self.AimAng = self.ACOGAng
			else
				self.AimPos = self.ACOG_BackupPos
				self.AimAng = self.ACOG_BackupAng
			end
		elseif self.VElements["elcan"].color.a == 255 then
			if self.AimPos == self.ELCAN_BackupPos then
				self.AimPos = self.ELCANPos
				self.AimAng = self.ELCANAng
			else
				self.AimPos = self.ELCAN_BackupPos
				self.AimAng = self.ELCAN_BackupAng
			end
		end
	end
end