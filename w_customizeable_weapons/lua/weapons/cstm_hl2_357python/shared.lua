if ( SERVER ) then

    AddCSLuaFile( "shared.lua" )
    SWEP.HasRail = true
    SWEP.NoAimpoint = true
    SWEP.NoEOTech = true
    SWEP.HasMasher = true

    SWEP.RequiresRail = false
end
SWEP.PsID = "cm_357"
SWEP.BulletLength = 10.9
SWEP.CaseLength = 32.6

SWEP.MuzVel = 321.521

SWEP.Attachments = {
    [1] = {"acog"},
    [2] = {"laser"}}

if ( CLIENT ) then

    SWEP.PrintName			= ".357 python"
    SWEP.Author				= "Counter-Strike"
    SWEP.Slot				= 1
    SWEP.SlotPos = 0 //			= 1
    SWEP.IconLetter			= "f"
    SWEP.DifType = true
    SWEP.PitchMod = 0.25
    SWEP.RollMod = 0.5
    SWEP.DrawAngleMod = 0.5
    SWEP.Muzzle = "cstm_muzzle_br"
    SWEP.SparkEffect = "cstm_child_sparks_large"
    SWEP.SmokeEffect = "cstm_child_smoke_large"
    SWEP.ACOGDist = 6
    SWEP.AimpointDist = 11
    SWEP.SafeXMoveMod = 2
    SWEP.AttachmentName = "gun"


    SWEP.VElements = {
        ["acog"] = { type = "Model", model = "models/wystan/attachments/2cog.mdl", bone = "python", rel = "", pos = Vector(-0.44, 4.75, -5.084), angle = Angle(180, 180, 90.325), size = Vector(1.05,1.05,1.05), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
        ["laser"] = { type = "Model", model = "models/props_c17/FurnitureBoiler001a.mdl", bone = "python", rel = "", pos = Vector(0, 1, 5.984), angle = Angle(-0.18, 0, 180), size = Vector(0.04, 0.04, 0.04), color = Color(255, 255, 255, 0), surpresslightning = false, material = "models/bunneh/silencer", skin = 0, bodygroup = {} },
        ["silencer"] = { type = "Model", model = "models/props_c17/oildrum001.mdl", bone = "Python", rel = "", pos = Vector(0.001, -0.727, 7.975), angle = Angle(0, 0, 0), size = Vector(0.06, 0.06, 0.2), color = Color(255, 255, 255, 0), surpresslightning = false, material = "models/bunneh/silencer", skin = 0, bodygroup = {} }
    }

    SWEP.WElements = {
        ["acog"] = { type = "Model", model = "models/wystan/attachments/2cog.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(0, 0, 1.2), angle = Angle(0, -90, 180), size = Vector(1.1,1.1,1.1), color = Color(255, 255, 255, 0), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
        ["silencer"] = { type = "Model", model = "models/props_c17/oildrum001.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(13.279, 1, -4.500), angle = Angle(-94, 0, 0), size = Vector(0.05, 0.05, 0.15), color = Color(255, 255, 255, 0), surpresslightning = false, material = "models/bunneh/silencer", skin = 0, bodygroup = {} },
        --["weapon"] = { type = "Model", model = "models/weapons/w_pist_magnum.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(-0.16, 0.873, 0.609), angle = Angle(0, 0, 180), size = Vector(1, 1, 1), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
    }
end

SWEP.HoldType			= "ar2"
SWEP.Base				= "cstm_base_pistol"
SWEP.Category = "Customizable Weaponry: HL2"
SWEP.FireModes = {"semi"}

SWEP.Spawnable			= true
SWEP.AdminSpawnable		= true

SWEP.HoldType = "pistol"

SWEP.ViewModelFOV  = 64
SWEP.ViewModelFlip = false
SWEP.UseHands = true
SWEP.ViewModel	= Model("models/weapons/c_357.mdl")
SWEP.WorldModel	= Model("models/weapons/w_357.mdl")

SWEP.ShowViewModel = true
SWEP.ShowWorldModel = true
SWEP.ViewModelBonescales = {}


SWEP.Weight				= 5
SWEP.AutoSwitchTo		= false
SWEP.AutoSwitchFrom		= false

SWEP.Primary.Sound			= Sound("Weapon_357.Single")
SWEP.Primary.Recoil			= 3
SWEP.Primary.Damage			= 65
SWEP.Primary.NumShots		= 1
SWEP.Primary.Cone			= 0.02
SWEP.Primary.ClipSize		= 60
SWEP.Primary.Delay			= 0.4
SWEP.Primary.DefaultClip	= 60
SWEP.Primary.Automatic		= false
SWEP.Primary.Ammo			= ".357"
SWEP.InitialHoldtype = "pistol"
SWEP.InHoldtype = "pistol"
SWEP.NoBoltAnim = true
SWEP.SilencedSound = Sound("CSTM_SilencedShot2")
SWEP.SilencedVolume = 80
SWEP.DryFireAnim = false
SWEP.SprintAndShoot = true
SWEP.HeadshotMultiplier = 2

-- Animation speed/custom reload function related
SWEP.IsReloading = false
SWEP.AnimPrefix = ""
SWEP.ReloadSpeed = 1
SWEP.ShouldBolt = false
SWEP.ReloadDelay = 0
SWEP.IncAmmoPerc = 0.72 -- Amount of frames required to pass (in percentage) of the reload animation for the weapon to have it's amount of ammo increased
SWEP.CantChamber = true
SWEP.SkipIdle = true
SWEP.PlaybackRate = 2
SWEP.FOVZoom = 85

-- Dynamic accuracy related
SWEP.ShotsAmount 			= 0
SWEP.ConeDecAff				= 0
SWEP.DefRecoil				= 2.8
SWEP.CurCone				= 0.04
SWEP.DecreaseRecoilTime 	= 0
SWEP.ConeAff1 				= 0 -- Crouching/standing
SWEP.ConeAff2 				= 0 -- Using ironsights

SWEP.UnConeTime				= 0 -- Amount of time after firing the last shot that needs to pass until accuracy increases
SWEP.FinalCone				= 0 -- Self explanatory
SWEP.VelocitySensivity		= 1.2 -- Percentage of how much the cone increases depending on the player's velocity (moving speed). Rifles - 100%; SMGs - 80%; Pistols - 60%; Shotguns - 20%
SWEP.HeadbobMul 			= 1
SWEP.IsSilenced 			= false
SWEP.IronsightsCone 		= 0.009
SWEP.HipCone 				= 0.049
SWEP.ConeInaccuracyAff1 = 0.5
SWEP.PlayFireAnim = true

SWEP.Secondary.Automatic	= false
SWEP.Secondary.Ammo			= "none"

SWEP.IsUsingIronsights 		= false
SWEP.TargetMul = 0
SWEP.SetAndForget			= false

SWEP.IronSightsPos = Vector( -4.6, -3, 0.65 )
SWEP.IronSightsAng = Vector(0, 0, 0)

SWEP.AimPos = Vector( -4.6, -3, 0.65 )
SWEP.AimAng =  Vector(0, 0, 0)

SWEP.ReflexPos = Vector( -4.6, -12, 0.65 )
SWEP.ReflexAng = Vector(0, 0, 0)

SWEP.ACOGPos =  Vector( -4.67, -12, -.0855 )
SWEP.ACOGAng = Vector(0, 0, 0)

SWEP.ACOG_BackupPos = Vector( -4.65, -12, -1.55 )
SWEP.ACOG_BackupAng = Vector(0, 0, 0)

SWEP.FlipOriginsPos = Vector(4.651, -8.426, 0.61)
SWEP.FlipOriginsAng = Vector(0, 0, 0)

SWEP.MeleePos = Vector(4.487, -1.497, -5.277)
SWEP.MeleeAng = Vector(25.629, -30.039, 26.18)

if CLIENT then
    function SWEP:CanUseBackUp()
        if self.VElements["acog"].color.a == 255 or self.VElements["elcan"].color.a == 255 then
            return true
        end

        return false
    end

    function SWEP:UseBackUp()
        if self.VElements["acog"].color.a == 255 then
            if self.AimPos == self.ACOG_BackupPos then
                self.AimPos = self.ACOGPos
                self.AimAng = self.ACOGAng
            else
                self.AimPos = self.ACOG_BackupPos
                self.AimAng = self.ACOG_BackupAng
            end
        elseif self.VElements["elcan"].color.a == 255 then
            if self.AimPos == self.ELCAN_BackupPos then
                self.AimPos = self.ELCANPos
                self.AimAng = self.ELCANAng
            else
                self.AimPos = self.ELCAN_BackupPos
                self.AimAng = self.ELCAN_BackupAng
            end
        end
    end
end

