function SWEP:NWAlpha(alpha)
	umsg.Start("NWAlpha")
		umsg.Entity(self)
		umsg.Short(alpha)
	umsg.End()
end

local can, att, wep, CT, dtint3, t, num

function RequestPimp(ply, com, args)
    wep = args[1]

    if not wep then
        wep = ply:GetActiveWeapon()
    else
        wep = weapons.Get(wep)
    end

	if not wep.IsCSTMWeapon then
		return
	end

	if wep.ReloadTime then
		return
	end

	CT = CurTime()

	if CT < wep.ReloadDelay and not wep:GetNWBool("canEquip") then
		return
	end

	if IsValid(wep) then
		dtint3 = wep:GetDTInt(3)

		if dtint3 == 0 then
			wep:SetDTInt(3, 19)
		elseif dtint3 == 19 then
			wep:SetDTInt(3, 0)
		end

		wep.ReloadDelay = CT + 0.1
		wep:SetNextPrimaryFire(CT + 0.2)
		wep:SetNextSecondaryFire(CT + 0.2)
	end
end

concommand.Add("cstm_requestpimp", RequestPimp)

function PimpMyGun(ply, com, args)
	att = args[1]
    wep = args[2]

	if not att then
		return
    end
    if not wep then
        wep = ply:GetActiveWeapon()
    else
         wep = ply:GetWeapon(wep)
    end


	att = tonumber(att)


	if not IsValid(wep) then
        print("Not a valid wep")
		return
	end

	CT = CurTime()

	if CT < wep.ReloadDelay and not wep:GetNWBool("canEquip") then
        print( wep.ReloadDelay - CT )
		return
	end
	wep:NWATT2(att, CWAttachments[CWAttachmentsMeta[att]].scope)
end

concommand.Add("cstm_pimpmygun", PimpMyGun)

function UnPimpMyGun(ply, com, args)
	att = args[1]

	if not att then
		return
	end

	att = tonumber(att)
	wep = ply:GetActiveWeapon()

	if not IsValid(wep) then
		return
	end

	CT = CurTime()

	if CT < wep.ReloadDelay and not wep:GetNWBool("canEquip") then
		return
	end

	wep:Deselect(att, CWAttachments[CWAttachmentsMeta[att]].scope)
end

concommand.Add("cstm_unpimpmygun", UnPimpMyGun)

function ChangeInternalPart(ply, com, args)
	att = args[1]
    wep = args[2]

    if not att then
        return
    end
    if not wep then
        wep = ply:GetActiveWeapon()
    else
        wep = ply:GetWeapon(wep)
    end

	att = tonumber(att)

	if not IsValid(wep) then
		return
	end

	CT = CurTime()

	if CT < wep.ReloadDelay and not wep:GetNWBool("canEquip") then
		return
	end

	wep:ChangeInternalPart(att)
end

concommand.Add("cstm_addswag", ChangeInternalPart)

function RemoveInternalPart(ply, com, args)
	att = args[1]

	if not att then
		return
	end

	att = tonumber(att)
	wep = ply:GetActiveWeapon()

	if not IsValid(wep) then
		return
	end

	CT = CurTime()

	if CT < wep.ReloadDelay and not wep:GetNWBool("canEquip") then
		return
	end

	wep:RemoveInternalPart(att)
end

concommand.Add("cstm_removeswag", RemoveInternalPart)

function SWEP:IsLastAtt(num, b)
	for k, v in pairs(self.Attachments) do
		for k2, v2 in pairs(v) do
			if CWAttachments[v2] then
				if CWAttachments[v2].num == number then
					if b then
						return v
					end

					return true
				end
			end
		end
	end
end

function SWEP:SendWeaponStats()
	umsg.Start("CSTM_WEPSTATS", self.Owner)
		umsg.Float(self.DefRecoil)
		umsg.Float(self.Primary.Damage)
		umsg.Float((self.ClumpSpread and self.ClumpSpread or 0))
		umsg.Float(self.HipCone)
		umsg.Float(self.IronsightsCone)
		umsg.Float(self.Primary.Delay)
		umsg.Float(self.ConeInaccuracyAff1)
	umsg.End()
end

function SWEP:ChangeInternalPart(number, dontsave)
	if not IsValid(self) then
		return
	end

	if self:GetDTInt(3) != 19 and not self:GetNWBool("canEquip") then
    print("Not 19")
		return
	end

	for k, v in pairs(CWInternalParts) do
		if v.num == number then
			if not table.HasValue(self.Owner.InternalParts, k) then
				return
			end
		end
	end

	CT = CurTime()

	if CT < self.ReloadDelay and not self:GetNWBool("canEquip") then
		return
	end

	t = CWInternalPartsMeta[number]

	if t then
		for k, v in pairs(self.InternalParts) do
			for k2, v2 in pairs(v) do
				if type(v2) != "function" and t == v2.key then
					if v.lastdeattfunc then
						v.lastdeattfunc(self)
					end

					v.lastdeattfunc = CWInternalParts[t].svdeattfunc
					break
				end
			end
		end

		t = CWInternalParts[t]

		if t.svattfunc then
			t.svattfunc(self)
		end

		self.ReloadDelay = CT + 0.05

		self:CalculateWeaponStats()
		self:SendWeaponStats()

        -- ============================
        if not dontsave then
            self:SaveParts(t.key,true)
        end
        -- ============================


		umsg.Start("INTERNALPART", self.Owner)
			umsg.Short(number)
            umsg.Entity(self)
		umsg.End()
	end
end

function SWEP:RemoveInternalPart(number)
	if not IsValid(self) then
		return
	end

	if self:GetDTInt(3) != 19 and not self:GetNWBool("canEquip") then
		return
	end

	for k, v in pairs(CWInternalParts) do
		if v.num == number then
			if not table.HasValue(self.Owner.InternalParts, k) then
				return
			end
		end
	end

	CT = CurTime()

	if CT < self.ReloadDelay then
		return
	end

	t = CWInternalPartsMeta[number]



	if t then
		t = CWInternalParts[t]
			if t.svdeattfunc then
			t.svdeattfunc(self)
            end

        -- ============================
        self:SaveParts(t.key,false)
        -- ============================

		self.ReloadDelay = CT + 0.05

		self:CalculateWeaponStats()
		self:SendWeaponStats()

		umsg.Start("INTERNALPARTREM", self.Owner)
			umsg.Short(number)
		umsg.End()
	end
end

function SWEP:NWATT2(number, scope, dontsave)
	if not IsValid(self) then
		return
    end


	if self:GetDTInt(3) != 19 and not self:GetNWBool("canEquip") then
        print("Not 19")
		return
	end

	for k, v in pairs(CWAttachments) do
		if v.num == number then
			if not table.HasValue(self.Owner.Attachments, k) then
                print("Not having attachment")
				return
			end
		end
	end

	CT = CurTime()

	if CT < self.ReloadDelay and not self:GetNWBool("canEquip")  then
        print("Waitring for reload")
		return
	end

	t = CWAttachmentsMeta[number]
	can = false

	if t then
		for k, v in pairs(self.Attachments) do
			if #v > 1 then
				for k2, v2 in pairs(v) do
					if t == v2 then
						if v.lastdeattfunc then
							v.lastdeattfunc(self)
						end

						v.lastdeattfunc = CWAttachments[t].svdeattfunc
						break
					end
				end
			end
		end

		t = CWAttachments[t]
		can = true

		if scope then
			self.ScopeStatus = number
			self.FOVMod = t.scope.fovmod

			if self.NoSightBG then
				self.Owner:GetViewModel():SetBodygroup((self.MainBG and self.MainBG or 1), self.NoSightBG)
				self.BodyGroup = self.NoSightBG
			end
		end

		if t.svattfunc then
			t.svattfunc(self)
		end

		if can then
			for k, v in pairs(self.Attachments) do
				for k2, v2 in pairs(v) do
					if CWAttachments[v2] then
						if CWAttachments[v2].num == number then
							v.last = number
							break
						end
					end
				end
            end
         -- ===================================================
            if not dontsave then

                    if CWAttachments[t.key] and CWAttachments[t.key].incompability then
                        for k2, v2 in pairs(CWAttachments[t.key].incompability) do

                                self:SaveParts(v2,false)
                              --  print("disabled "..v2)

                        end
                    end

                --print("enabled "..t.key)
                self:SaveParts(t.key,true)
            end
         -- ===================================================
                self.ReloadDelay = CT + 0.05
                self.LastAttachment = number

                self:CalculateWeaponStats()
                self:SendWeaponStats()



			umsg.Start("NWATT2")
				umsg.Entity(self)
				umsg.Short(number)
			umsg.End()
		end
	end
end

function SWEP:Deselect(num, scope)
	if not IsValid(self) then
		return
	end

	if self:GetDTInt(3) != 19 and not self:GetNWBool("canEquip") then
		return
	end

	for k, v in pairs(CWAttachments) do
		if v.num == num then
			if not table.HasValue(self.Owner.Attachments, k) then
				return
			end
		end
	end

	CT = CurTime()

	if CT < self.ReloadDelay then

		return
	end

	if scope then
		if self.ScopeStatus == num then
			self.ScopeStatus = 0

			if self.SightBG then
				self.Owner:GetViewModel():SetBodygroup((self.MainBG and self.MainBG or 1), self.SightBG)
				self.BodyGroup = self.SightBG
			end

			can = true
		end
	end

	if CWAttachmentsMeta[num] then
		can = true

		if CWAttachments[CWAttachmentsMeta[num]].svdeattfunc then
			CWAttachments[CWAttachmentsMeta[num]].svdeattfunc(self)
		end
	end
		if can then
         -- ============================
            self:SaveParts(CWAttachments[CWAttachmentsMeta[num]].key,false)
         -- ============================

		self.ReloadDelay = CT + 0.05

		self:CalculateWeaponStats()
		self:SendWeaponStats()

		umsg.Start("NWRATT")
			umsg.Entity(self)
			umsg.Short(num)
		umsg.End()
	end
end

function CSTM_SelectAmmoType(ply, com, args)
	if not IsValid(ply) or not ply:Alive() then
		return
	end

    wep = args[2]
    dontsafe = args[3]

    if not wep then
        wep = ply:GetActiveWeapon()
    else
        wep = ply:GetWeapon(wep)
    end

	if not wep.IsCSTMWeapon then
		return
	end

	CT = CurTime()

	if CT < wep.ReloadDelay and not wep:GetNWBool("canEquip") then
		return
	end

	num = tonumber(args[1])

	if not num then
		return
	end

	t = nil
	s = nil

	for k, v in pairs(AmmoTypes) do
		if v.num == num then
			if wep.LastAmmo != v.name then
				if table.HasValue(ply.AmmoTypes, k) then
					t = v
					s = k
					break
				end
			end
		end
	end

	if t then
		wep.ReloadDelay = CT + 0.05
		wep:RestoreStats()
		wep:SetDTInt(2, t.num)
		wep.LastAmmo = t.name
		wep.PenMod = t.penmod

		clip = wep:Clip1()
		ammo = ply:GetAmmoCount(wep.Primary.Ammo)

        if  wep:GetNWBool("canEquip") then -- When starting the round
            wep:SetClip1( ammo + clip, wep:GetPrimaryAmmoType() )
        else
		    wep:SetClip1(0)
            ply:SetAmmo(ammo + clip, wep:GetPrimaryAmmoType())
        end


		if wep.AmmoDeselectFunc then
			wep.AmmoDeselectFunc(ply, wep)
		end

		wep.AmmoDeselectFunc = t.deselfunc

		if t.selfunc then
			t.selfunc(ply, wep)
		end

		wep:CalculateWeaponStats()
		wep:SendWeaponStats()

        -- ============================
        if not dontsafe then
          wep:SaveParts("ammo",s)
        end

        -- ============================

		umsg.Start("NWAMMO", ply)
			umsg.String(s)
            umsg.Entity(wep)
		umsg.End()
	end
end

concommand.Add("cstm_selectammo", CSTM_SelectAmmoType)

function CSTM_DeselectAmmo(ply)
	if not ply:Alive() then
		return
	end

	wep = ply:GetActiveWeapon()

	if not IsValid(wep) then
		return
	end

	if not wep.IsCSTMWeapon then
		return
	end

	CT = CurTime()

	if CT < wep.ReloadDelay and not wep:GetNWBool("canEquip") then
		return
	end

	if wep.LastAmmo != "Normal" then
		wep.ReloadDelay = CT + 0.05
		wep:SetDTInt(2, 0)
		wep:RestoreStats()
		wep.LastAmmo = "Normal"

		SendUserMessage("DESELECTAMMO", ply)

		clip = wep:Clip1()
		ammo = ply:GetAmmoCount(wep.Primary.Ammo)

		wep:SetClip1(0)
		--ply:SetAmmo( clip, wep:GetPrimaryAmmoType())

		if t and  t.deselfunc then
			t.deselfunc(ply, wep)
		end

		wep:CalculateWeaponStats()
		wep:SendWeaponStats()
        -- ============================
             wep:SaveParts("ammo",false)

        -- ============================
	end
end

concommand.Add("cstm_deselectammo", CSTM_DeselectAmmo)