
TOOL.Category		= "Customizable Weaponry"
TOOL.Name			= "#tool.ammopackspawner.name"
TOOL.Command		= nil
TOOL.ConfigName		= ""

TOOL.ClientConVar["caliber"]			= "9x19MM"
TOOL.ClientConVar["amount"]			= "20"
CreateConVar("sbox_maxammopacks", "10", {FCVAR_REPLICATED, FCVAR_ARCHIVE, FCVAR_NOTIFY})

cleanup.Register("ammopacks")

if CLIENT then
	language.Add("tool.ammopackspawner.name", "Ammo pack spawner")
	language.Add("tool.ammopackspawner.desc", "Lets you spawn packs of ammo that you can set up.")
	language.Add("tool.ammopackspawner.0", "LEFT click to spawn an ammo pack with selected customization; RIGHT click to spawn ammo pack with a random caliber and a random ammo amount.")
	language.Add("tool.ammopackspawner.calibertype", "Caliber type")
	language.Add("tool.ammopackspawner.ammoamt", "Amount of ammo")
	language.Add("SBoxLimit_ammopacks", "Ammo pack limit reached.")
end 

function TOOL:LeftClick(trace)
	if ( trace.Entity && trace.Entity:IsPlayer() ) then return false end
	
	if (CLIENT) then 
		return true
	end
	
	local ply = self:GetOwner()
	local caliber = self:GetClientInfo("caliber")
	local amt = self:GetClientNumber("amount")
		
	if ( trace.Entity:IsValid() && trace.Entity:GetClass() == "cstm_ammopack" && trace.Entity.pl == ply ) then
		trace.Entity.Caliber = caliber
		trace.Entity.Amount = amt
		return true
	end
	
	if ( !self:GetSWEP():CheckLimit( "ammopacks" ) ) then return false end

	local ang = trace.HitNormal:Angle()
	ang.pitch = ang.pitch + 90

	local ammo = MakeAmmoPack(ply, ang, caliber, amt)
	ammo:SetPos(trace.HitPos - trace.HitNormal * ammo:OBBMins().z)
	
	undo.Create("Ammo pack")
		undo.AddEntity(ammo)
		undo.SetPlayer(ply)
		undo.SetCustomUndoText("Undone ammo pack")
	undo.Finish()
		
	ply:AddCleanup("ammopacks", ammo)
	
	return true
end

function TOOL:RightClick(trace)
	if ( trace.Entity && trace.Entity:IsPlayer() ) then return false end
	
	if (CLIENT) then 
		return true
	end
	
	local ply = self:GetOwner()
		
	if ( trace.Entity:IsValid() && trace.Entity:GetClass() == "cstm_ammopack" && trace.Entity.pl == ply ) then
		trace.Entity.Caliber = table.Random(Calibers).ammo
		trace.Entity.Amount = math.random(5, 120)
		return true
	end
	
	if ( !self:GetSWEP():CheckLimit( "ammopacks" ) ) then return false end

	local ang = trace.HitNormal:Angle()
	ang.pitch = ang.pitch + 90

	local ammo = MakeAmmoPack(ply, ang, table.Random(Calibers).ammo, math.random(5, 120))
	ammo:SetPos(trace.HitPos - trace.HitNormal * ammo:OBBMins().z)
	
	undo.Create("Ammo pack")
		undo.AddEntity(ammo)
		undo.SetPlayer(ply)
		undo.SetCustomUndoText("Undone ammo pack")
	undo.Finish()
		
	ply:AddCleanup("ammopacks", ammo)
	
	return true
end

if (SERVER) then
	function MakeAmmoPack(pl, ang, ammo, amt)
		if ( IsValid( pl ) ) then
			if ( !pl:CheckLimit( "ammopacks" ) ) then return false end
		end
	
		local ent = ents.Create("cstm_ammopack")
		
		if not ent:IsValid() then 
			return false
		end
		
		ent:SetModel("models/Items/BoxMRounds.mdl")

		ent:SetAngles(ang)
		ent:Spawn()
		ent.Caliber = ammo
		ent.Amount = amt
		ent.pl = pl

		if IsValid(pl) then
			pl:AddCount("ammopacks", ent)
		end
		
		DoPropSpawnedEffect(ent)

		return ent
	end
	
	duplicator.RegisterEntityClass( "cstm_ammopack", MakeAmmoPack, "ang", "ammo", "amt")
end

function TOOL:UpdateGhostAmmoPack( ent, player )
	if ( !ent ) then return end
	if ( !ent:IsValid() ) then return end

	local tr 	= util.GetPlayerTrace(player)
	local trace 	= util.TraceLine(tr)
	
	if not trace.Hit then 
		return 
	end
	
	if (trace.Entity && trace.Entity:GetClass() == "cstm_ammopack" || trace.Entity:IsPlayer()) then
		ent:SetNoDraw(true)
		return
	end
	
	local Ang = trace.HitNormal:Angle()
	Ang.pitch = Ang.pitch + 90
	
	local min = ent:OBBMins()
	ent:SetPos(trace.HitPos - trace.HitNormal * min.z)
	ent:SetAngles(Ang)
	
	ent:SetNoDraw(false)
end


function TOOL:Think()
	if (!self.GhostEntity || !self.GhostEntity:IsValid()) then
		self:MakeGhostEntity("models/Items/BoxMRounds.mdl", Vector(0,0,0), Angle(0,0,0))
	end
	
	self:UpdateGhostAmmoPack( self.GhostEntity, self:GetOwner() )
end


function TOOL.BuildCPanel( CPanel )
	-- HEADER
	CPanel:AddControl("Header", {Text = "#tool.ammopackspawner.name", Description	= "#tool.ammopackspawner.desc"})	
	CPanel:AddControl("Slider", {Label = "#tool.ammopackspawner.ammoamt",
									 Description = "",
									 Type = "Integer",
									 Min = 5,
									 Max = 120,
									 Command = "ammopackspawner_amount"})
	
	CPanel:AddControl("ComboBox", {Label = "#tool.ammopackspawner.calibertype",
									MenuButton = "0",
									Options = list.Get("CalibersToPick")})
end

for k, v in pairs(Calibers) do
	list.Set("CalibersToPick", v.name, {ammopackspawner_caliber = v.ammo})
end
