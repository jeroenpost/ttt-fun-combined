local FS = {}

FS["CSTM_M14"] = "weapons/m14/m14-1.mp3"
FS["CSTM_SilencedShot2"] = "weapons/g3/g3-1.mp3"
FS["CSTM_SilencedShot4"] = "weapons/m4a1customizable/m4a1-1.mp3"
FS["CSTM_SilencedShot5"] = "weapons/g36c/g36c_sil.mp3"
FS["CSTM_SilencedShot7"] = "weapons/silenced1.mp3"
FS["CSTM_G2CONTENDER"] = {"weapons/g2contender/g2-1.mp3", "weapons/g2contender/g2-2.mp3", "weapons/g2contender/g2-3.mp3" }
FS["CSTM_M98B"] = "weapons/m98/m98-1.mp3"
FS["CSTM_Deagle"] = "weapons/deagles/deagle-1.mp3"
FS["CSTM_M16A4"] = "weapons/m16a4/m16-1.mp3"
FS["CSTM_M4A1"] = "weapons/m4a1customizable/m4a1_unsil-1.mp3"
FS["CSTM_TAR21"] = "weapons/tar21/tar21-1.mp3"
FS["CSTM_SCOUT"] = "weapons/scout/scout_fire-1.wav"
FS["CSTM_PPSH"] = "weapons/ppsh/ppsh-1.mp3"
FS["CSTM_SPAS12"] = "weapons/spas/spas-1.mp3"
FS["CSTM_SCARH"] = "weapons/scarh/scarh-1.mp3"
FS["CSTM_44MAGNUM"] = "weapons/44magnum/magnumfire.mp3"
FS["CSTM_Masada"] = "weapons/masada/masada_unsil.mp3"

local tbl = {channel = CHAN_WEAPON,
    volume = 1,
    soundlevel = 120,
    pitchstart = 100,
    pitchend = 100}

for k, v in pairs(FS) do
    tbl.name = k
    tbl.sound = v

    sound.Add(tbl)
end

if CLIENT then
    local RS = {}
    RS["CSTM_MASADA.Silencer_Off"] = "weapons/masada/masada_silencer_off.mp3"
    RS["CSTM_MASADA.Silencer_On"] = "weapons/masada/masada_silencer_on.mp3"
    RS["CSTM_MASADA.Clipout"] = "weapons/masada/masada_clipout.mp3"
    RS["CSTM_MASADA.Clipin"] = "weapons/masada/masada_clipin.mp3"
    RS["CSTM_MASADA.Boltpull"] = "weapons/masada/masada_boltpull.mp3"
    RS["CSTM_MASADA.Deploy"] = "weapons/masada/masada_deploy.mp3"

    RS["Magnum.chamberout"] = "weapons/44magnum/magnumchamberout.mp3"
    RS["Magnum.chamberin"] = "weapons/44magnum/magnumchamberin.mp3"
    RS["Magnum.clipout"] = "weapons/44magnum/magnumclipout.mp3"
    RS["Magnum.clipin"] = "weapons/44magnum/magnumclipin.mp3"
    RS["Magnum.deploy"] = "weapons/44magnum/magnumdeploy.mp3"
    RS["SCARH.MagOut"] = "weapons/scarh/scarh_magout.mp3"
    RS["SCARH.MagIn"] = "weapons/scarh/scarh_magin.mp3"
    RS["SCARH.MagTap"] = "weapons/scarh/scarh_magtap.mp3"
    RS["SCARH.BoltRel"] = "weapons/scarh/scarh_boltrel.mp3"
    RS["SCARH.Foley"] = "weapons/scarh/scarh_foley.mp3"
    RS["Weapon_g2.draw"] = "weapons/g2contender/Draw.mp3"
    RS["Weapon_g2.hammer"] = {"weapons/g2contender/Cock-1.mp3", "weapons/g2contender/Cock-2.mp3"}
    RS["Weapon_g2.open"] = "weapons/g2contender/open_chamber.mp3"
    RS["Weapon_g2.shell"] = {"weapons/g2contender/pl_shell1.mp3", "weapons/g2contender/pl_shell2.mp3", "weapons/g2contender/pl_shell3.mp3", "weapons/g2contender/pl_shell4.mp3"}
    RS["Weapon_g2.shellin"] = "weapons/g2contender/Bullet_in.mp3"
    RS["Weapon_g2.close"] = "weapons/g2contender/close_chamber.mp3"
    RS["Weapon_M14.Clipout"] = "weapons/m14/m14_clipout.mp3"
    RS["Weapon_M14.Clipin"] = "weapons/m14/m14_clipin.mp3"
    RS["Weapon_M14.Boltpull"] = "weapons/m14/m14_boltpull.mp3"
    RS["Weapon_M98.Boltlock"] = "weapons/m98/m98_boltlock.mp3"
    RS["Weapon_M98.Boltback"] = "weapons/m98/m98_boltback.mp3"
    RS["Weapon_M98.Boltpush"] = "weapons/m98/m98_boltpush.mp3"
    RS["Weapon_M98.draw"] = "weapons/m98/m98_deploy.mp3"
    RS["Weapon_M98.Clipout"] = "weapons/m98/m98_clipout.mp3"
    RS["Weapon_M98.Clipin"] = "weapons/m98/m98_clipin.mp3"
    RS["Weapon_DEAGS.Lclipout"] = "weapons/deagles/deagle_lclipout.mp3"
    RS["Weapon_DEAGS.Rclipout"] = "weapons/deagles/deagle_rclipout.mp3"
    RS["Weapon_DEAGS.Lclipin"] = "weapons/deagles/deagle_lclipin.mp3"
    RS["Weapon_DEAGS.Rclipin"] = "weapons/deagles/deagle_rclipin.mp3"
    RS["Weapon_DEAGS.Magbash"] = "weapons/deagles/deagle_magbash.mp3"
    RS["Weapon_DEAGS.Sliderelease"] = "weapons/deagles/deagle_sliderelease.mp3"
    RS["Weapon_DEAGS.Deploy"] = "weapons/deagles/shh.mp3"
    RS["Weapon_DEAGS.Hammerback"] = "weapons/deagles/deagle_hammerback.mp3"
    RS["Weapon_M16A4.Deploy"] = "weapons/m16a4/m16a4_deploy.mp3"
    RS["Weapon_M16A4.MagOut"] = "weapons/m16a4/m16a4_magout.mp3"
    RS["Weapon_M16A4.MagIn"] = "weapons/m16a4/m16a4_magin.mp3"
    RS["Weapon_M16A4.MagTap"] = "weapons/m16a4/m16a4_magtap.mp3"
    RS["Weapon_M16A4.BoltPull"] = "weapons/m16a4/m16a4_boltpull.mp3"
    RS["Weapon_M16A4.BoltRelease"] = "weapons/m16a4/m16a4_boltrelease.mp3"
    RS["Weapon_M4CU.Silencer_on"] = "weapons/m4a1customizable/m4a1_silencer_on.mp3"
    RS["Weapon_M4CU.Silencer_off"] = "weapons/m4a1customizable/m4a1_silencer_off.mp3"
    RS["Weapon_M4CU.Clipout"] = "weapons/m4a1customizable/m4a1_clipout.mp3"
    RS["Weapon_M4CU.Clipin"] = "weapons/m4a1customizable/m4a1_clipin.mp3"
    RS["Weapon_M4CU.Magtap"] = "weapons/m4a1customizable/m4a1_magtap.mp3"
    RS["Weapon_M4CU.Boltpull"] = "weapons/m4a1customizable/m4a1_boltpull.mp3"
    RS["Weapon_M4CU.Deploy"] = "weapons/m4a1customizable/m4a1_deploy.mp3"
    RS["Weapon_M4CU.BoltRelease"] = "weapons/m4a1customizable/m4a1_boltrelease.mp3"

    RS["Weapon_xm8.Cliplock"] = "weapons/xm8/safety.mp3"
    RS["Weapon_xm8.Clipout"] = "weapons/famf1/famas_clipout.mp3"
    RS["Weapon_xm8.Clipin"] = "weapons/famf1/famas_clipin.mp3"
    RS["Weapon_xm8.Cliptap"] = "weapons/f2000/f2000_cliptap.mp3"
    RS["Weapon_xm8.Boltpull"] = "weapons/xm8/Boltpull.mp3"
    RS["Weapon_xm8.Draw"] = "weapons/xm8/Cloth.mp3"
    RS["Weapon_xm8.Lock"] = "weapons/xm8/safety.mp3"
    RS["Weapon_scout.clipout"] = "weapons/scout/scout_clipout.wav"
    RS["Weapon_scout.clipin"] = "weapons/scout/scout_clipin.wav"
    RS["Weapon_scout.Bolt"] = "weapons/scout/scout_bolt.wav"
    RS["Weapon_ppsh.clipout"] = "weapons/ppsh/ppsh_clipout.mp3"
    RS["Weapon_ppsh.clipin"] = "weapons/ppsh/ppsh_clipin.mp3"
    RS["Weapon_ppsh.cliprelease"] = "weapons/ppsh/ppsh_cliprelease.mp3"
    RS["Weapon_ppsh.Boltpull"] = "weapons/ppsh/ppsh_boltpull.mp3"

    RS["Weapon_SP.Pump"] = "weapons/spas/spas_pump.mp3"
    RS["Weapon_SP.Pumpforward"] = "weapons/spas/spas_pumpforward.mp3"
    RS["Weapon_SP.Pumpback"] = "weapons/spas/spas_pumpback.mp3"
    RS["Weapon_SP.Insertshell"] = {"weapons/spas/spas_insertshell1.mp3", "weapons/spas/spas_insertshell2.mp3", "weapons/spas/spas_insertshell3.mp3"}


    local tbl = {channel = CHAN_WEAPON,
        volume = 1,
        soundlevel = 70,
        pitchstart = 100,
        pitchend = 100}

    for k, v in pairs(RS) do
        tbl.name = k
        tbl.sound = v

        sound.Add(tbl)
    end

    print("loaded sounds 2")
end