if SERVER then
   AddCSLuaFile( "shared.lua" )

--resource.AddFile("materials/models/weapons/v_models/green_black/greenblack1.vmt")
--resource.AddFile("materials/models/weapons/v_models/green_black/greenblack2.vmt")
--resource.AddFile("materials/models/weapons/v_models/green_black/greenblack.vmt")

--resource.AddFile("models/weapons/v_green_black.mdl")
--resource.AddFile("models/weapons/w_green_black.mdl")
--resource.AddFile("sound/weapons/green_black1.mp3")
--resource.AddFile("sound/weapons/green_black2.mp3")
--resource.AddFile("sound/weapons/green_black3.mp3")
--resource.AddFile("sound/weapons/green_black4.mp3")
--resource.AddFile("sound/weapons/green_black5.mp3")
--resource.AddFile("materials/vgui/ttt_fun_killicons/green_black.png")

end
   
SWEP.HoldType			= "pistol"
SWEP.PrintName			= "OP GreenBlack"
SWEP.Category			= "TTT-FUN"
SWEP.Slot				= 2

if CLIENT then
   			
   SWEP.Author				= "GreenBlack"
   SWEP.SlotPos			= 2
   SWEP.Icon = "vgui/ttt_fun_killicons/green_black.png"
	killicon.Add( "green_black", "vgui/spawnicons", color_white )
	SWEP.WepSelectIcon = Material( "greenblack.png" )
	SWEP.BounceWeaponIcon = false
	SWEP.DrawWeaponInfoBox = false
end

   SWEP.EquipMenuData = {
      name = "NyanGun",
      type = "item_weapon",
      desc = "Shoots Nyan Cats"
   };
SWEP.Base				= "weapon_tttbase"

SWEP.Spawnable = true
SWEP.AdminSpawnable = true

SWEP.Kind = WEAPON_PISTOL
SWEP.WeaponID = AMMO_DEAGLE

SWEP.Primary.Ammo       = "AlyxGun" 
SWEP.Primary.Recoil			= 0.01
SWEP.Primary.Damage = 200
SWEP.Primary.Delay = 0.05
SWEP.Primary.Cone = 0.05
SWEP.Primary.ClipSize = 100000
SWEP.Primary.ClipMax = 180000
SWEP.Primary.DefaultClip = 100000
SWEP.Primary.Automatic = true
SWEP.Primary.NumShots = 10
SWEP.HeadshotMultiplier = 4

SWEP.Secondary.Ammo       = "AlyxGun" 
SWEP.Secondary.Recoil			= 0.05
SWEP.Secondary.Damage = 100
SWEP.Secondary.Delay = 1
SWEP.Secondary.Cone = 0.08
SWEP.Secondary.ClipSize = 3
SWEP.Secondary.ClipMax = 3
SWEP.Secondary.DefaultClip = 3
SWEP.Secondary.Automatic = false

SWEP.AutoSpawnable      = false
SWEP.AmmoEnt = "item_ammo_revolver_ttt"
SWEP.Primary.Sound = Sound( "weapons/green_black1.mp3" )

SWEP.UseHands			= true
SWEP.ViewModelFlip		= true
SWEP.ViewModelFOV		= 75
SWEP.ViewModel			= "models/weapons/v_green_black.mdl"
SWEP.WorldModel			= "models/weapons/w_green_black.mdl"

SWEP.IronSightsPos = Vector(5.14, -2, 2.5)
SWEP.IronSightsAng = Vector(0, 0, 0)
--SWEP.IronSightsPos 	= Vector( 3.8, -1, 3.6 )
--SWEP.ViewModelFOV = 49

function SWEP:PrimaryAttack(worldsnd) 

   self.Weapon:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
   self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )

   if not self:CanPrimaryAttack() then return end

   if not worldsnd then
      self.Weapon:EmitSound( self.Primary.Sound, self.Primary.SoundLevel )
   elseif SERVER then
      sound.Play(self.Primary.Sound, self:GetPos(), self.Primary.SoundLevel)
   end

   self:ShootBullet( self.Primary.Damage, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )
   -- local tr = self.Owner:GetEyeTrace()
   -- local effectdata = EffectData()
   -- effectdata:SetOrigin( tr.HitPos )
   -- effectdata:SetNormal( tr.HitNormal )
  --  effectdata:SetMagnitude( 1 )
  --  effectdata:SetScale( 1 )
  --  effectdata:SetRadius( 1 )
  --  util.Effect( "Sparks", effectdata )
    
   self:TakePrimaryAmmo( 1 )

   local owner = self.Owner 
   if not IsValid(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

   owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )
end

SWEP.Headcrabs = 0
SWEP.NextSecond = 0

function SWEP:SecondaryAttack()
    if  self.NextSecond > CurTime() then return end
    if not self:CanPrimaryAttack() then return end
       self.Weapon:SetNextPrimaryFire( CurTime() + 0.1 )
   self.Weapon:SetNextSecondaryFire( CurTime() + 0.5 )

    self.NextSecond = CurTime() + 0.2

    self.Headcrabs = 0
    self:EmitSound( "weapons/green_black2.mp3" )

    local tr = self.Owner:GetEyeTrace()
   
    if  not SERVER then return end
    
    local ent = ents.Create( "npc_headcrab" )
    ent:SetPos( tr.HitPos + self.Owner:GetAimVector() * -46 )
    ent:SetAngles( tr.HitNormal:Angle() )
    ent:SetHealth( 20000 )
    ent:SetPhysicsAttacker(self.Owner)
    ent:Spawn()

    
    
end

SWEP.NextReload = 0
SWEP.Bottles = 0

function SWEP:Reload()

    if  self.NextReload > CurTime() then return end
       self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
   self.Weapon:SetNextSecondaryFire( CurTime() + self.Secondary.Delay )
    
    timer.Create("giveAmmoWaterBottleGreen",1,0,function()
        if IsValid(self) then
            self:SetClip1( self:Clip1() + 1 )
        end
    end)

     self.NextReload = CurTime() + 0.2
    
    
    self:ShootEffects( self )
    
    if( self.Bottles > 4 ) then
        self:EmitSound( "weapons/green_black4.mp3" )
    return end

    self:EmitSound( "weapons/green_black3.mp3" )
local tr = self.Owner:GetEyeTrace()
    self.Bottles = self.Bottles + 0
    

    if (!SERVER) then return end

    local ent1 = ents.Create("prop_physics") 
	local ang = Vector(0,0,1):Angle();
	ang.pitch = ang.pitch + 90;
	ang:RotateAroundAxis(ang:Up(), math.random(0,360))
	ent1:SetAngles(ang)
	ent1:SetModel("models/props/cs_office/water_bottle.mdl")
	--local pos = position
	--pos.z = pos.z - ent1:OBBMaxs().z
	ent1:SetPos( tr.HitPos + self.Owner:GetAimVector() * -46  )
	ent1:Spawn()
    timer.Simple(3,function()
        if SERVER then
            if not magnitude then magnitude = 35 end
            local tr = self.Owner:GetEyeTrace()
            local ent = ents.Create( "env_explosion" )

            ent:SetPos( ent1:GetPos()  )
            ent:SetOwner( self.Owner  )
            ent:SetPhysicsAttacker(  self.Owner )
            ent:Spawn()
            ent:SetKeyValue( "iMagnitude", magnitude )
            ent:Fire( "Explode", 0, 0 )

            util.BlastDamage( self, self:GetOwner(),  ent1:GetPos(), 50, 300 )
            if not soundy then
                ent:EmitSound( "weapons/big_explosion.mp3" )
            else
                ent:EmitSound(soundy)
            end
        end

        ent1:Remove()

    end)
end
 


