if SERVER then
   AddCSLuaFile( "shared.lua" )

--resource.AddFile("materials/models/weapons/v_models/green_black/greenblack1.vmt")
--resource.AddFile("materials/models/weapons/v_models/green_black/greenblack2.vmt")
--resource.AddFile("materials/models/weapons/v_models/green_black/greenblack.vmt")

--resource.AddFile("models/weapons/v_green_black.mdl")
--resource.AddFile("models/weapons/w_green_black.mdl")
--resource.AddFile("sound/weapons/green_black1.mp3")
--resource.AddFile("sound/weapons/green_black2.mp3")
--resource.AddFile("sound/weapons/green_black3.mp3")
--resource.AddFile("sound/weapons/green_black4.mp3")
--resource.AddFile("sound/weapons/green_black5.mp3")
--resource.AddFile("materials/vgui/ttt_fun_killicons/green_black.png")

end
   
SWEP.HoldType			= "pistol"
SWEP.PrintName			= "BlackGreen"
SWEP.Category			= "TTT-FUN"
SWEP.Slot				= 2

SWEP.CanDecapitate= true
   SWEP.Author				= "GreenBlack"
   SWEP.SlotPos			= 2
   SWEP.Icon = "vgui/ttt_fun_killicons/green_black.png"


	SWEP.BounceWeaponIcon = false
	SWEP.DrawWeaponInfoBox = false

SWEP.Base				= "weapon_tttbase"

SWEP.Spawnable = true
SWEP.AdminSpawnable = true

SWEP.Kind = WEAPON_PISTOL
SWEP.WeaponID = AMMO_DEAGLE

SWEP.Primary.Ammo       = "AlyxGun" 
SWEP.Primary.Recoil			= 0.05
SWEP.Primary.Damage = 25
SWEP.Primary.Delay = 0.3
SWEP.Primary.Cone = 0.03
SWEP.Primary.ClipSize = 150
SWEP.Primary.ClipMax = 1800
SWEP.Primary.DefaultClip = 150
SWEP.Primary.Automatic = true

SWEP.HeadshotMultiplier = 4

SWEP.Secondary.Ammo       = "AlyxGun" 
SWEP.Secondary.Recoil			= 0.05
SWEP.Secondary.Damage = 1
SWEP.Secondary.Delay = 1
SWEP.Secondary.Cone = 0.08
SWEP.Secondary.ClipSize = 3
SWEP.Secondary.ClipMax = 3
SWEP.Secondary.DefaultClip = 3
SWEP.Secondary.Automatic = false

SWEP.AutoSpawnable      = true
SWEP.AmmoEnt = "item_ammo_revolver_ttt"
SWEP.Primary.Sounds = { Sound( "weapons/blackgreen1.mp3" ), Sound( "weapons/blackgreen2.mp3" ), Sound( "weapons/blackgreen3.mp3" ), Sound( "weapons/blackgreen4.mp3" ) }
SWEP.ReloadSounds = { Sound( "weapons/blackgreen_parched.mp3" ), Sound( "weapons/blackgreen_parched2.mp3" ) }
SWEP.BrickSound =  Sound( "weapons/blackgreenbrick.mp3");
SWEP.brickSounds = { Sound( "weapons/blackgreen_brick2.mp3" ), Sound( "weapons/blackgreen_bricktalk.mp3" ), Sound( "weapons/blackgreen_bricktalk2.mp3" ) }

SWEP.randomSounds= {  Sound( "weapons/blackgreen_weapons.mp3" ), Sound( "weapons/blackgreen_drop.mp3" ), Sound( "weapons/blackgreen_bricktalk2.mp3" ) }

SWEP.brick = "models/props/de_inferno/cinderblock.mdl"

SWEP.UseHands			= true
SWEP.ViewModelFlip		= true
SWEP.ViewModelFOV		= 75
SWEP.ViewModel			= "models/weapons/v_green_black.mdl"
SWEP.WorldModel			= "models/weapons/w_green_black.mdl"

SWEP.IronSightsPos = Vector(5.14, -2, 2.5)
SWEP.IronSightsAng = Vector(0, 0, 0)
--SWEP.IronSightsPos 	= Vector( 3.8, -1, 3.6 )
--SWEP.ViewModelFOV = 49

function SWEP:PrimaryAttack(worldsnd) 

   self.Weapon:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
   self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )

   if not self:CanPrimaryAttack() then return end

   if SERVER then
      sound.Play( table.Random(self.Primary.Sounds), self:GetPos(), self.Primary.SoundLevel)
   end

   self:ShootBullet( self.Primary.Damage, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )

   self:TakePrimaryAmmo( 1 )

   local owner = self.Owner 
   if not IsValid(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

   owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )
end

SWEP.Headcrabs = 0
SWEP.NextSecond = 0

function SWEP:SecondaryAttack()
    if  self.NextSecond > CurTime() then return end
    if not self:CanPrimaryAttack() then return end
       self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
   self.Weapon:SetNextSecondaryFire( CurTime() + self.Secondary.Delay )

    self.NextSecond = CurTime() + 5

    if( self.Headcrabs > 1 ) then
        if SERVER then   sound.Play( table.Random(self.randomSounds), self:GetPos(), self.Primary.SoundLevel) end

    return end
    self.Headcrabs = self.Headcrabs + 1

    if SERVER then sound.Play( self.BrickSound, self:GetPos(), self.Primary.SoundLevel) end

    local tr = self.Owner:GetEyeTrace()
    self.Weapon:EmitSound(   table.Random(self.brickSounds) )

    if  not SERVER then return end

    self:SpawnBrick();

    

end

function SWEP:SpawnBrick()

    local tr = self.Owner:GetEyeTrace()

    if  not SERVER then return end



    spos = tr.HitPos + self.Owner:GetAimVector() * -46

    local headturtle = self:SpawnNPC2(self.Owner, spos, "npc_headcrab")

    headturtle:SetNPCState(2)

    local turtle = ents.Create("prop_dynamic")
    turtle:SetModel(self.brick)
    turtle:SetPos( spos )
    turtle:SetAngles(Angle(0,-90,0))
    turtle:SetParent(headturtle)

    --headturtle:SetCollisionGroup(COLLISION_GROUP_WEAPON)

    headturtle:SetNWEntity("Thrower", self.Owner)
    --headturtle:SetName(self:GetThrower():GetName())
    headturtle:SetNoDraw(true)
    headturtle:SetHealth(40)

    if SERVER then  sound.Play(  table.Random(self.brickSounds) , self:GetPos(), self.Primary.SoundLevel) end


    if math.Rand( 1,5) == "4" then
        turtle:Ignite(100,1)
        turtle:SetHealth(75)
    end

    --headturtle:SetCollisionGroup(COLLISION_GROUP_WEAPON)

    headturtle:SetNWEntity("Thrower", self.Owner)
    --headturtle:SetName(self:GetThrower():GetName())
    headturtle:SetNoDraw(true)
    headturtle:SetHealth(15)
    headturtle:EmitSound(   table.Random(self.brickSounds) )
    timer.Simple(9,function()
        if IsValid( headturtle ) then
            self:ReplaySound( headturtle )
        end

    end)
end

function SWEP:ReplaySound( ent )
    if not IsValid( ent ) then return end

    if SERVER then sound.Play(  table.Random(self.brickSounds) , ent:GetPos(), self.Primary.SoundLevel) end

    timer.Simple(9,function()
        if IsValid( ent ) then
            self:ReplaySound( ent )
        end

    end)
end



SWEP.NextReload = 0
SWEP.Bottles = 0

function SWEP:Reload()

    if  self.NextReload > CurTime() then return end
       self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
   self.Weapon:SetNextSecondaryFire( CurTime() + self.Secondary.Delay )
    
    timer.Create("giveAmmoWaterBottleGreen",5,0,function()
        if IsValid(self) then
            self:SetClip1( self:Clip1() + 5 )
        end
    end)

     self.NextReload = CurTime() + 3
    
    
    self:ShootEffects( self )

    if( self.Bottles > 4 ) then

        if SERVER then sound.Play(  table.Random(self.randomSounds) , self:GetPos(), self.Primary.SoundLevel) end

        return end

    if SERVER then  sound.Play(  table.Random(self.ReloadSounds) , self:GetPos(), self.Primary.SoundLevel) end

    local tr = self.Owner:GetEyeTrace()
    self.Bottles = self.Bottles + 1
    

    if (!SERVER) then return end

    local ent1 = ents.Create("prop_physics") 
	local ang = Vector(0,0,1):Angle();
	ang.pitch = ang.pitch + 90;
	ang:RotateAroundAxis(ang:Up(), math.random(0,360))
	ent1:SetAngles(ang)
	ent1:SetModel("models/props/cs_office/coffee_mug2.mdl")
	--local pos = position
	--pos.z = pos.z - ent1:OBBMaxs().z
	ent1:SetPos( tr.HitPos + self.Owner:GetAimVector() * -46  )
	ent1:Spawn()
    
end




function SWEP:SpawnNPC2( Player, Position, Class )

    local NPCList = list.Get( "NPC" )
    local NPCData = NPCList[ Class ]

    -- Don't let them spawn this entity if it isn't in our NPC Spawn list.
    -- We don't want them spawning any entity they like!
    if ( not NPCData ) then
        if ( IsValid( Player ) ) then
            Player:SendLua( "Derma_Message( \"Sorry! You can't spawn that NPC!\" )" );
        end
        return end

    local bDropToFloor = false

    --
    -- This NPC has to be spawned on a ceiling ( Barnacle )
    --
    if ( NPCData.OnCeiling and Vector( 0, 0, -1 ):Dot( Normal ) < 0.95 ) then
        return nil
    end

    if ( NPCData.NoDrop ) then bDropToFloor = false end

    --
    -- Offset the position
    --


    -- Create NPC
    local NPC = ents.Create( NPCData.Class )
    if ( not IsValid( NPC ) ) then return end

    NPC:SetPos( Position )
    --
    -- This NPC has a special model we want to define
    --
    if ( NPCData.Model ) then
        NPC:SetModel( NPCData.Model )
    end

    --
    -- Spawn Flags
    --
    local SpawnFlags = bit.bor( SF_NPC_FADE_CORPSE, SF_NPC_ALWAYSTHINK)
    if ( NPCData.SpawnFlags ) then SpawnFlags = bit.bor( SpawnFlags, NPCData.SpawnFlags ) end
    if ( NPCData.TotalSpawnFlags ) then SpawnFlags = NPCData.TotalSpawnFlags end
    NPC:SetKeyValue( "spawnflags", SpawnFlags )

    --
    -- Optional Key Values
    --
    if ( NPCData.KeyValues ) then
        for k, v in pairs( NPCData.KeyValues ) do
            NPC:SetKeyValue( k, v )
        end
    end

    --
    -- This NPC has a special skin we want to define
    --
    if ( NPCData.Skin ) then
        NPC:SetSkin( NPCData.Skin )
    end

    --
    -- What weapon should this mother be carrying
    --

    NPC:Spawn()
    NPC:Activate()

    if ( bDropToFloor and not NPCData.OnCeiling ) then
        NPC:DropToFloor()
    end

    return NPC
end