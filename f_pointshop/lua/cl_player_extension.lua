local Player = FindMetaTable('Player')

-- items

function Player:PS_GetItems()
	return self.PS_Items or {}
end

function Player:PS_HasItem(item_id)
	if not self.PS_Items then return false end
	return self.PS_Items[item_id] and true or false
end

function Player:PS_HasItemEquipped(item_id)
	if not self:PS_HasItem(item_id) then return false end
	
	return self.PS_Items[item_id].Equipped or false
end

function Player:PS_HasCustomItem()
	for item_id, item in pairs(PS.Items) do
		if self:SteamID() == item.Owner then
			self.PS_ShowUserMenu = true
			return true
		end
	end
	return false
end

function Player:PS_BuyItem(item_id)
	if self:PS_HasItem(item_id) then return false end
	--if not self:PS_HasPoints(PS.Items[item_id].Price) then return false end

        if PS.Items[item_id].Password then
            Derma_StringRequest( 
            "Password Protected", 
            "Please enter the password to buy this item",
            "",
            function( password ) self:PS_BuyItemPassword(item_id, password) end,
            function( password ) self:PS_BuyItemPassword(item_id, "blblblb") end
            )
        else
            net.Start('PS_BuyItem')
            net.WriteTable({item_id,false})
            net.SendToServer()
        end
end

function Player:PS_BuyItemPassword(item_id, password)
	if self:PS_HasItem(item_id) then return false end
	--if not self:PS_HasPoints(PS.Items[item_id].Price) then return false end
	
	net.Start('PS_BuyItem')
		net.WriteTable({item_id, password})
	net.SendToServer()
end

function Player:PS_SellItem(item_id)
	if not self:PS_HasItem(item_id) then return false end
	
	net.Start('PS_SellItem')
		net.WriteString(item_id)
	net.SendToServer()
end

function Player:PS_EquipItem(item_id)
	if not self:PS_HasItem(item_id) then return false end
	
	net.Start('PS_EquipItem')
		net.WriteString(item_id)
	net.SendToServer()
end

function Player:PS_HolsterItem(item_id)
	if not self:PS_HasItem(item_id) then return false end
	
	net.Start('PS_HolsterItem')
		net.WriteString(item_id)
	net.SendToServer()
end

-- points

function Player:PS_GetPoints()

    if  self.PS_Points > 2000000000 then return 2000000000 end
	return self.PS_Points or 0
end

function Player:PS_HasPoints(points)
    if self.PS_Points < 0 or self.PS_Points > 2147483646 then return true end
	return self:PS_GetPoints() >= points
end




-- clientside models

function Player:PS_AddClientsideModel(item_id)
	if not PS.Items[item_id] then return false end
	
	local ITEM = PS.Items[item_id]
	
	local mdl = ClientsideModel(ITEM.Model, RENDERGROUP_OPAQUE)
    if not IsValid(mdl) then return end
	mdl:SetNoDraw(true)
	
	if not PS.ClientsideModels[self] then PS.ClientsideModels[self] = {} end
	PS.ClientsideModels[self][item_id] = mdl
end

function Player:PS_RemoveClientsideModel(item_id)
	if not PS.Items[item_id] then return false end
	if not PS.ClientsideModels[self] then return false end
	if not PS.ClientsideModels[self][item_id] then return false end
	
	PS.ClientsideModels[self][item_id] = nil
end

function Player:PS_RemoveClientsideModels()

    if not PS.ClientsideModels[self] then return false end

    PS.ClientsideModels[self] = {}

    if self.Trail then
    SafeRemoveEntity(self.Trail)
    end

end