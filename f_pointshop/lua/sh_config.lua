PS.Config = {}

-- Edit below

PS.Config.DataProvider = 'json'

PS.Config.Branch = "https://raw.github.com/adamdburton/pointshop/master/" -- Master is most stable, used for version checking.
PS.Config.CheckVersion = false -- Do you want to be notified when a new version of Pointshop is avaliable?

PS.Config.ShopKey = "F3" -- F1, F2, F3 or F4, or blank to disable
PS.Config.ShopCommand = 'ps_shop' -- Console command to open the shop, set to blank to disable
PS.Config.ShopChatCommand = '!shop' -- Chat command to open the shop, set to blank to disable

PS.Config.NotifyOnJoin = true -- Should players be notified about opening the shop when they spawn?

PS.Config.PointsOverTime = true -- Should players be given points over time?
PS.Config.PointsOverTimeDelay = 1 -- If so, how many minutes apart?
PS.Config.PointsOverTimeAmount = 50 -- And if so, how many points to give after the time?

PS.Config.AdminCanAccessAdminTab = false -- Can Admins access the Admin tab?
PS.Config.SuperAdminCanAccessAdminTab = true -- Can SuperAdmins access the Admin tab?

PS.Config.CanPlayersGivePoints = true -- Can players give points away to other players?
PS.Config.DisplayPreviewInMenu = true -- Can players see the preview of their items in the menu?

resource.AddFile("materials/camos/hax_hud.png")

PS.Config.Categories = {
    [1] = { "hats", "hats-donator", "hats-member", "hats_superdonator", "hats_megadonator", "hats_supermember",  "hats_megamember", "hats_legendmember", "hats-special" },
    [2] = { "playermodels", "playermodels-donator", "playermodels-donator2", "playermodels-member", "playermodels-superdonator", "playermodels_megadonator"},
    [3] = { "powerups", "powerups_member","powerups_donator","powerups_donatorplus","powerups_superdonator","powerups_megadonator" },
    [4] = { "trails", "trails-member", "trails-donator", "trails_legendmember", "trails_donatorplus", "trails_superdonator"},
    [5] = { "customizeable_weapons", "customizeable_weapons_ammo", "customizeable_weapons_internal", "customizeable_weapons_sights","customizeable_weapons_su" },
    [6] = { "weapons_special","aa_donator_customs","weapons_special_member"},
    [7] = { "weapons", "weapons-member", "weapons-donator", "weapons-donator2",  "weapons-superdonator", "weapons-megadonator" },
    [8] = { "perma-weapons","perma-weapons-member",  "perma-weapons-donator", "perma-weapons-donator2", "perma-weapons-superdonator", "perma-weapons-megadonator",  "perma-weapons-legendmember" },

    [9] = { "member", "hats_supermember", "hats_megamember", "hats_legendmember", "perma-weapons-legendmember"},
    [10] = { "donator",       "perma-weapons-donator",   "trails-donator", "powerups_donator", "playermodels-donator", "hats-donator", "weapons-donator", },
    [11] = { "donator_plus", "perma-weapons-donator2",  "powerups_donatorplus", "playermodels-donator2", "weapons-donator2",},
    [12] = { "donator_super", "perma-weapons-superdonator", "powerups_superdonator", "playermodels-superdonator", "weapons-superdonator", },
    [13] = { "donator_mega", "perma-weapons-megadonator",  "powerups_megadonator", "playermodels_megadonator", "weapons-megadonator", "hats_megadonator" },
    [14] = { "custom_db_official","custom_db_single", "custom_db" },
    [15] = { "custom_private_weapons","custom_weapons", "custom_playermodels" }
}

PS.Config.PointsName = "Points"
PS.Config.SortItemsBy = "Name"

-- Edit below if you know what you're doing

PS.Config.CalculateBuyPrice = function(ply, item)
	-- You can do different calculations here to return how much an item should cost to buy.
	-- There are a few examples below, uncomment them to use them.

    if( item.IsCustomDB)then
        return item.Price
    end

    if string.sub(item.Name,1,3) == "cm_" or string.sub(item.Name,1,5) == "upgr_" then
        if  gb.is_donator( ply ) then
            return item.Price
        else
            return math.Round(item.Price*0.25)
        end
    end

	-- Everything half price for admins:



	
	-- 25% off for the 'donators' groups
	if gb.is_donator( ply )  then return math.Round(item.Price * 0.5) end

     return item.Price
end

PS.Config.CalculateSellPrice = function(ply, item)
    
       local priceItem = PS.Config.CalculateBuyPrice( ply, item)

       if  gb.is_donator( ply ) then
            return  math.Round((item.Price * 0.5) * 0.75)
       end
	

	return math.Round(priceItem * 0.75) -- 75% or 3/4 (rounded) of the original item price
end