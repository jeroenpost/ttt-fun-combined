ITEM.Name = 'JetPack'
ITEM.Price = 350000
ITEM.Model = 'models/xqm/jetengine.mdl'
ITEM.Skin = 'camos/camo49'
ITEM.Bone = 'ValveBiped.Bip01_Spine2'

function ITEM:OnEquip(ply)
    ply:PS_AddClientsideModel(self.ID)
    ply:GiveRocketBoots(10)
end


function ITEM:OnHolster(ply)
    ply:PS_RemoveClientsideModel(self.ID)
end

function ITEM:ModifyClientsideModel(ply, model, pos, ang)
    model:SetModelScale(0.5, 0)
    pos = pos + (ang:Right() * 7) + (ang:Forward() * 6)

    return model, pos, ang
end
