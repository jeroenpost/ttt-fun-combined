ITEM.Name = "Perma Gas Mask"
ITEM.Price = 10000000
ITEM.Material = 'materials/vgui/ttt_fun_pointshop_icons/gasmask.png'
--ITEM.WeaponClass = 'weapon_ttt_protection_suit'
ITEM.Description = "Gas wont have effect on you anymore. Permanent use"
ITEM.SingleUse = false

function ITEM:OnEquip(ply)
	-- ply:Give(self.WeaponClass)
	ply.GasProtection = true
	--ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply.GasProtection = false
	
end

function ITEM:OnHolster(ply)
	ply.GasProtection = false
end