ITEM.Name = '+200% Jump'
ITEM.Price = 990000
ITEM.Material = 'vgui/ttt_fun_pointshop_icons/jumpboots.png'

function ITEM:OnEquip(ply)
    if ply:GetJumpPower() < 600 then
	    ply:SetJumpPower(600)
    end
    timer.Simple(10,function()
        if IsValid(ply) then
            if ply:GetJumpPower() < 600 then
                ply:SetJumpPower(600)
            end
        end
    end)
    ply.HasJumpBoots = true
end 

