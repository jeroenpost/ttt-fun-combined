ITEM.Name = '+35% Speed'
ITEM.Price = 5000000
ITEM.Material = 'vgui/ttt_fun_pointshop_icons/runningshoes.png'
--ITEM.WeaponClass = 'weapon_ttt_spidermangun_gb'

function ITEM:OnEquip(ply)
    if ply:GetNWInt("walkspeed") < 250 then
        ply:SetNWInt("walkspeed",250)
    end
    if ply:GetNWInt("runspeed") < 425 then
        ply:SetNWInt("runspeed", 425)
    end
    timer.Simple(10,function()
        if IsValid(ply) then
            if ply:GetNWInt("walkspeed") < 250 then
                ply:SetNWInt("walkspeed",250)
            end
            if ply:GetNWInt("runspeed") < 425 then
                ply:SetNWInt("runspeed", 425)
            end
        end
    end)
end

