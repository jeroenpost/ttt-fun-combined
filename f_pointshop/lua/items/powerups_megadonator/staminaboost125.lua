ITEM.Name = '+75% Stamina'
ITEM.Price = 2500000
ITEM.Material = 'vgui/ttt_fun_pointshop_icons/fun_bull.png'
ITEM.Description = [[Speeds up the regen of your stamina]]

function ITEM:OnEquip(ply)
    if ply:GetNWInt("staminaboost",0) < 75  then
        ply:SetNWInt("staminaboost",0.75)
    end
end 
