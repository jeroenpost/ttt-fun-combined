ITEM.Name = 'Lemongun'
ITEM.Price = 1200
ITEM.Material = 'vgui/ttt_fun_pointshop_icons/lemongun.png'
ITEM.WeaponClass = 'weapon_lemonrifle'
ITEM.SingleUse = true

function ITEM:OnBuy(ply)
	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end 