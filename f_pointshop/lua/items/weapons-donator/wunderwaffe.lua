ITEM.Name = 'Wunderwaffe'
ITEM.Price = 15000
ITEM.Model = "models/wunderwaffe.mdl"
ITEM.WeaponClass = 'wunderwaffe'
ITEM.SingleUse = true

ITEM.AdminOnly = false

function ITEM:OnBuy(ply)
	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end