ITEM.Name = 'Health Grenade'
ITEM.Price = 2000
ITEM.Material = 'vgui/ttt_fun_pointshop_icons/smokegrenade.png'
ITEM.WeaponClass = 'weapon_ttt_healthgrenade'
ITEM.SingleUse = true

function ITEM:OnBuy(ply)
	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end