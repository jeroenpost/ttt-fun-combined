ITEM.Name = 'Stun Gun'
ITEM.Price = 5000
ITEM.Material = 'vgui/ttt_fun_pointshop_icons/ump45.png'
ITEM.WeaponClass = 'weapon_ttt_stungun'
ITEM.SingleUse = true


function ITEM:OnBuy(ply)
	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end
