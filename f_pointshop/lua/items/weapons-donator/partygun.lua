ITEM.Name = 'Party Gun'
ITEM.Price = 6000
ITEM.Material = 'vgui/ttt_fun_pointshop_icons/partygun.png'
ITEM.WeaponClass = 'weapon_gb_partygun'
ITEM.SingleUse = true

function ITEM:OnBuy(ply)
	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end