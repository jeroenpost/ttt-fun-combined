ITEM.Name = 'Laugh Poison'
ITEM.Price = 6000
ITEM.Material = 'vgui/ttt_fun_pointshop_icons/laughterpoison.png'
ITEM.WeaponClass = 'weapon_gb_laughpoison'
ITEM.SingleUse = true

function ITEM:OnBuy(ply)
	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end