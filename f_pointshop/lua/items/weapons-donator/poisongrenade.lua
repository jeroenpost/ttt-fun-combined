ITEM.Name = 'Poison Grenade'
ITEM.Price = 4000
ITEM.Material = 'vgui/ttt_fun_pointshop_icons/grenade.png'
ITEM.WeaponClass = 'weapon_ttt_gasgrenade'
ITEM.SingleUse = true

function ITEM:OnBuy(ply)
	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end