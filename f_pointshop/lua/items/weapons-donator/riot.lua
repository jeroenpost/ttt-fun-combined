ITEM.Name = 'Riot Shield'
ITEM.Price = 500
ITEM.Material = 'vgui/ttt_fun_pointshop_icons/riot_shield.png'
ITEM.WeaponClass = 'weapon_ttt_riot'
ITEM.SingleUse = true

function ITEM:OnBuy(ply)
	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end