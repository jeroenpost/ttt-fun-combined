ITEM.Name = 'Chainsaw'
ITEM.Price = 1400
ITEM.Material = 'vgui/ttt_fun_pointshop_icons/chainsaw.png'
ITEM.WeaponClass = 'doom3_chainsaw'
ITEM.SingleUse = true

ITEM.AdminOnly = false

function ITEM:OnBuy(ply)
	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end