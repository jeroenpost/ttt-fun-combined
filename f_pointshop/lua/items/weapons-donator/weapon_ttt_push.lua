ITEM.Name = 'Physicsgun'
ITEM.Price = 1950
ITEM.Model = "models/weapons/w_physics.mdl"
ITEM.WeaponClass = 'weapon_ttt_push'
ITEM.SingleUse = true

function ITEM:OnBuy(ply)
	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end