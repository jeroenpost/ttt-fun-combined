ITEM.Name = 'Galil'
ITEM.Price = 1000
ITEM.Material = "materials/vgui/ttt_fun_pointshop_icons/galil.png"
ITEM.WeaponClass = 'weapon_ttt_galil'
ITEM.SingleUse = true

ITEM.AdminOnly = false

function ITEM:OnBuy(ply)
	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end