ITEM.Name = 'Cannibal'
ITEM.Price = 1000
ITEM.Material = 'vgui/ttt_fun_pointshop_icons/cannibal.png'
ITEM.WeaponClass = 'weapon_ttt_cannibalism'
ITEM.SingleUse = true

function ITEM:OnBuy(ply)
	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end