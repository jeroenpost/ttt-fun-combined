ITEM.Name = 'Nyan Gun'
ITEM.Price = 3000
ITEM.Material = 'vgui/ttt_fun_pointshop_icons/nyancat.png'
ITEM.WeaponClass = 'weapon_nyangun'
ITEM.SingleUse = true

function ITEM:OnBuy(ply)
	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end