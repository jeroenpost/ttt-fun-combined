ITEM.Name = 'M14 ERB'
ITEM.Price = 1500000
ITEM.Material = 'VGUI/entities/cstm_rif_m14ebr'
ITEM.WeaponClass = 'cstm_rif_m14ebr'
ITEM.Description = [[Can be equipped with:
Sights:
- Ballistic
- EoTech
- Kobra
- Rifle Reflex
- AimPoint
- Laser

Attachments:
- Grenade Launcher
- Silencer
- Bipod
- Front Grip
- Heavy Barrel
- Light Barrel
- Heavy Frame
- Egronomic Handle
- CustomStock
- Light/Heavy Bolt
- Gas Direction]]


include "camo_customizeable.lua"
ITEM.DontShow = false