ITEM.Name = 'Deagle'
ITEM.Price = 750000
ITEM.Material = 'materials/vgui/ttt_fun_pointshop_icons/deserteagle.png'
ITEM.WeaponClass = 'cstm_pistol_deagle'
ITEM.Description = [[Can be equipped with:
Sights:
- Reflex
- Laser

Attachments:
- Egronomic Handle
- Burst Converter
]]


include "camo_customizeable.lua"
ITEM.DontShow = false