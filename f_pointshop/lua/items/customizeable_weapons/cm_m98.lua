ITEM.Name = 'M98B'
ITEM.Price = 1500000
ITEM.Material = 'VGUI/entities/cstm_snip_m98'
ITEM.WeaponClass = 'cstm_snip_m98'
ITEM.Description = [[Can be equipped with:
Sights:
- Ballistic
- EoTech
- Kobra
- Rifle Reflex
- AimPoint
- Laser

Attachments:
- Silencer
- Bipod
- Front Grip
- Heavy Barrel
- Light Barrel
- Heavy Frame
- Egronomic Handle
- CustomStock
- Light/Heavy Bolt
- Gas Direction]]


include "camo_customizeable.lua"
ITEM.DontShow = false