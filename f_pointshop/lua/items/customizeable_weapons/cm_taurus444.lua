ITEM.Name = 'Taurus Model 444'
ITEM.Price = 750000
ITEM.Material = 'VGUI/entities/cstm_pistol_taurus444'
ITEM.WeaponClass = 'cstm_pistol_taurus444'
ITEM.Description = [[Can be equipped with:
Sights:
- Acog
- Laser

Attachments:
- Silencer
]]


include "camo_customizeable.lua"
ITEM.DontShow = false