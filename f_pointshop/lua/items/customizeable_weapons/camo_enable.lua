ITEM.Name = "Enable Camo's"
ITEM.Price = 2000000
ITEM.Material =   "camos/camo1.vmt"
--ITEM.WeaponClass = 'gb_camo_test'
ITEM.SingleUse = false
ITEM.Description = "Buy this to enable Camo's on Customizeable Weapons"
ITEM.AdminOnly = false

function ITEM:OnEquip(ply,modifications )
	 ply.hasCustomizeableCamos = true
end

function ITEM:OnSell(ply)
    ply.hasCustomizeableCamos = false
end
