ITEM.Name = 'HK MX8'
ITEM.Price = 1500000
ITEM.Material = 'VGUI/entities/cstm_rif_xm8'
ITEM.WeaponClass = 'cstm_rif_xm8'
ITEM.Description = [[Can be equipped with:
Sights:
- Elcan
- EoTech
- Kobra
- Rifle Reflex
- AimPoint
- Laser

Attachments:
- Silencer
- C-mag
- Bipod
- Front Grip
- Heavy Barrel
- Light Barrel
- Heavy Frame
- Egronomic Handle
- CustomStock
- Light/Heavy Bolt
- Gas Direction]]

include "camo_customizeable.lua"
ITEM.DontShow = false
