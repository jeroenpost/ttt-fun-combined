ITEM.Name = 'P90'
ITEM.Price = 1250000
ITEM.Material = 'materials/vgui/ttt_fun_pointshop_icons/p90.png'
ITEM.WeaponClass = 'cstm_smg_p90'
ITEM.Description = [[Can be equipped with:
Attachments:
- Silencer
- Heavy Barrel
- Light Barrel
- Heavy Frame
- Egronomic Handle
- CustomStock
- Light/Heavy Bolt
- Gas Direction]]

include "camo_customizeable.lua"
ITEM.DontShow = false