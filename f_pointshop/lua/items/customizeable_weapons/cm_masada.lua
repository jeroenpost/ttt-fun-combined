ITEM.Name = 'Magpul Masada'
ITEM.Price = 1750000
ITEM.Material = 'VGUI/entities/cstm_rif_m14ebr'
ITEM.WeaponClass = 'cstm_rif_masada'
ITEM.Description = [[Can be equipped with:
Sights:
- Kobra
- EOTech
- AimPoint
- Ballistic
- Acog
- Laser

Attachments:
- Silencer
- Grenade Launcher
- Heavy Barrel
- Light Barrel
- Heavy Frame
- Egronomic Handle
- CustomStock
- Light/Heavy Bolt
- Gas Direction]]

include "camo_customizeable.lua"
ITEM.DontShow = false


