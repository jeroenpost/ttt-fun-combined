ITEM.Name = 'M4A1'
ITEM.Price = 1250000
ITEM.Material = 'VGUI/entities/cstm_rif_m4a1'
ITEM.WeaponClass = 'cstm_rif_m4a1'
ITEM.Description = [[Can be equipped with:
Sights:
- EoTech
- Kobra
- Rifle Reflex
- AimPoint
- Acog
- Elcan
- Laser

Attachments:
- Cmag
- Grenade Launcher
- Silencer
- Bipod
- Front Grip
- Heavy Barrel
- Light Barrel
- Heavy Frame
- Egronomic Handle
- CustomStock
- Light/Heavy Bolt
- Gas Direction]]


include "camo_customizeable.lua"
ITEM.DontShow = false