ITEM.Name = 'UMP45'
ITEM.Price = 1250000
ITEM.Material = 'materials/vgui/ttt_fun_pointshop_icons/ump45.png'
ITEM.WeaponClass = 'cstm_smg_ump45'
ITEM.Description = [[Can be equipped with:
Sights:
- EoTech
- AimPoint
- Laser

Attachments:
- Silencer
- Heavy Barrel
- Light Barrel
- Heavy Frame
- Egronomic Handle
- CustomStock
- Light/Heavy Bolt
- Gas Direction]]

include "camo_customizeable.lua"
ITEM.DontShow = false