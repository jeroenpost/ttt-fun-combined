ITEM.Name = 'Spas-12'
ITEM.Price = 1250000
ITEM.Material = 'VGUI/entities/cstm_shotgun_spas12'
ITEM.WeaponClass = 'cstm_shotgun_spas12'
ITEM.Description = [[Can be equipped with:
Sights:
- EoTech
- Kobra
- Rifle Reflex
- AimPoint
- Acog
- Elcan

Attachments:
- Heavy Barrel
- Light Barrel
- Heavy Frame
- Egronomic Handle
- CustomStock
- Light/Heavy Bolt
- Gas Direction]]


include "camo_customizeable.lua"
ITEM.DontShow = false