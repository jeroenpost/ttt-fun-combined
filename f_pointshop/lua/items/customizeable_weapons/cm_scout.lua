ITEM.Name = 'Scout'
ITEM.Price = 750000
ITEM.Material = 'materials/vgui/ttt_fun_pointshop_icons/scout.png'
ITEM.WeaponClass = 'cstm_sniper_scout'
ITEM.Description = [[Can be equipped with:

Attachments:
- Heavy Barrel
- Light Barrel
- Heavy Frame
- Egronomic Handle
- CustomStock
- Light/Heavy Bolt
- Gas Direction
]]

include "camo_customizeable.lua"
ITEM.DontShow = false