ITEM.Name = 'SG552'
ITEM.Price = 1250000
ITEM.Material = 'materials/vgui/ttt_fun_pointshop_icons/sg552.png'
ITEM.WeaponClass = 'cstm_sniper_sg552'
ITEM.Description = [[Can be equipped with:
Attachments:
- Bipod
- Silencer
- Heavy Barrel
- Light Barrel
- Heavy Frame
- Egronomic Handle
- CustomStock
- Light/Heavy Bolt
- Gas Direction]]

include "camo_customizeable.lua"
ITEM.DontShow = false