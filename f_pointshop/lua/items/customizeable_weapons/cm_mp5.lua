ITEM.Name = 'MP5'
ITEM.Price = 1250000
ITEM.Material = 'materials/vgui/ttt_fun_pointshop_icons/mp5.png'
ITEM.WeaponClass = 'cstm_smg_mp5'
ITEM.Description = [[Can be equipped with:
Sights:
- EoTech
- AimPoint
- Laser

Attachments:
- Silencer
- Heavy Barrel
- Light Barrel
- Heavy Frame
- Egronomic Handle
- CustomStock
- Light/Heavy Bolt
- Gas Direction]]

include "camo_customizeable.lua"
ITEM.DontShow = false
