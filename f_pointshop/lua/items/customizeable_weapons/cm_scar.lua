ITEM.Name = 'FN SCAR-H'
ITEM.Price = 1250000
ITEM.Material = 'VGUI/entities/cstm_rif_scarh'
ITEM.WeaponClass = 'cstm_rif_scarh'
ITEM.Description = [[Can be equipped with:
Sights:
- Kobra
- EOTech
- AimPoint
- Ballistic
- Acog
- Laser

Attachments:
- Bipod
- Silencer
- Heavy Barrel
- Light Barrel
- Heavy Frame
- Egronomic Handle
- CustomStock
- Light/Heavy Bolt
- Gas Direction]]

include "camo_customizeable.lua"
ITEM.DontShow = false