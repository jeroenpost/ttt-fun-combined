ITEM.Name = 'XM1014'
ITEM.Price = 1250000
ITEM.Material = 'materials/vgui/ttt_fun_pointshop_icons/xm1014.png'
ITEM.WeaponClass = 'cstm_shotgun_xm1014'
ITEM.Description = [[Can be equipped with:
Sights:
- EoTech

Attachments:
- Heavy Barrel
- Light Barrel
- Heavy Frame
- Egronomic Handle
- CustomStock
- Light/Heavy Bolt
- Gas Direction]]

include "camo_customizeable.lua"
ITEM.DontShow = false