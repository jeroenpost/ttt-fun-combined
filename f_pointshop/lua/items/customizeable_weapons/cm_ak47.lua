ITEM.Name = 'AK47'
ITEM.Price = 1250000
ITEM.Material = 'materials/vgui/ttt_fun_pointshop_icons/ak47.png'
ITEM.WeaponClass = 'cstm_rif_ak47'
ITEM.Description = [[Can be equipped with:
Sights:
- Kobra
- AimPoint
- Acog
- Laser

Attachments:
- Silencer
- Heavy Barrel
- Light Barrel
- Heavy Frame
- Egronomic Handle
- CustomStock
- Light/Heavy Bolt
- Gas Direction]]

include "camo_customizeable.lua"
ITEM.DontShow = false