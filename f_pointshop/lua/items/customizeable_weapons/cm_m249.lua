ITEM.Name = 'M249 Para'
ITEM.Price = 1250000
ITEM.Material = 'materials/vgui/ttt_fun_pointshop_icons/m249.png'
ITEM.WeaponClass = 'cstm_rif_m249'
ITEM.Description = [[Can be equipped with:
Sights:
- EoTech
- Kobra
- Rifle Reflex
- AimPoint
- Laser

Attachments:
- Silencer
- Bipod
- Front Grip
- Heavy Barrel
- Light Barrel
- Heavy Frame
- Egronomic Handle
- CustomStock
- Light/Heavy Bolt
- Gas Direction]]


include "camo_customizeable.lua"
ITEM.DontShow = false