ITEM.Name = 'FiveSeven'
ITEM.Price = 750000
ITEM.Material = 'materials/vgui/ttt_fun_pointshop_icons/fiveseven.png'
ITEM.WeaponClass = 'cstm_pistol_fiveseven'
ITEM.Description = [[Can be equipped with:
Sights:
- Reflex
- Laser

Attachments:
- Silencer
- Auto Convert
- Egronomic Handle
]]

include "camo_customizeable.lua"
ITEM.DontShow = false