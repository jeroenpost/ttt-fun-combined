ITEM.Name = 'P228'
ITEM.Price = 750000
ITEM.Material = 'materials/vgui/ttt_fun_pointshop_icons/p228.png'
ITEM.WeaponClass = 'cstm_pistol_p228'
ITEM.Description = [[Can be equipped with:
Sights:
- Reflex
- Laser

Attachments:
- Silencer
- Burst Convert
- Egronomic Handle
]]

include "camo_customizeable.lua"
ITEM.DontShow = false