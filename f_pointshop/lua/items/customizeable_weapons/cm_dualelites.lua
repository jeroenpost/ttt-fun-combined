ITEM.Name = 'Dual Elites'
ITEM.Price = 1250000
ITEM.Material = 'materials/vgui/ttt_fun_pointshop_icons/elites.png'
ITEM.WeaponClass = 'cstm_pistol_dualelites'
ITEM.Description = [[Can be equipped with:
Attachments:
- Silencer
]]

include "camo_customizeable.lua"
ITEM.DontShow = false