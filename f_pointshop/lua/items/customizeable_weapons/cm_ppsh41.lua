ITEM.Name = 'PPSH-41'
ITEM.Price = 750000
ITEM.Material = 'VGUI/entities/cstm_smg_ppsh'
ITEM.WeaponClass = 'cstm_smg_ppsh'
ITEM.Description = [[Can be equipped with:
Sights:
- Elcan
- Laser

Attachments:
- Silencer
- Heavy Barrel
- Light Barrel
- Heavy Frame
- Egronomic Handle
- CustomStock
- Light/Heavy Bolt
- Gas Direction]]

include "camo_customizeable.lua"
ITEM.DontShow = false


