ITEM.Name = 'Dual Deagles'
ITEM.Price = 1250000
ITEM.Material = 'VGUI/entities/cstm_pistol_dualdeagles'
ITEM.WeaponClass = 'cstm_pistol_dualdeagles'
ITEM.Description = [[Can be equipped with:
- Different ammotypes
]]

include "camo_customizeable.lua"
ITEM.DontShow = false