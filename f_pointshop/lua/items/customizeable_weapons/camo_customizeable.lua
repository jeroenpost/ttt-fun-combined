
ITEM.DontShow = true

function ITEM:Modify(modifications)
    Derma_StringRequest("Camo Number", "What camo do you want? Use the camotester to find the number. Example: 52 or 23", modifications.camo, function(camo)
        local camo = tonumber(camo)
        if camo and camo < 1000 and camo > 0 then
            modifications.camo = camo
            PS:SendModifications(self.ID, modifications)
        end
    end)
end

function ITEM:OnModify(ply, modifications)
    ply:StripWeapon(self.WeaponClass)
    self:OnEquip(ply, modifications) -- adds the item back again, with new mods
    ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnEquip(ply, modifications)

    if not ply.customcamo then ply.customcamo = {} end
    if not modifications then modifications = {} end
    ply.customcamo[self.WeaponClass] = modifications.camo or 0
    ply:Give(self.WeaponClass)
end

function ITEM:OnSell(ply)
    ply:StripWeapon(self.WeaponClass)
end