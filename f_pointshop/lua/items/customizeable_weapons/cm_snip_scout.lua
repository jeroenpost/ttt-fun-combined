ITEM.Name = 'Fun Scout'
ITEM.Price = 125000
ITEM.Material = 'VGUI/entities/cstm_snip_scout'
ITEM.WeaponClass = 'cstm_snip_scout'
ITEM.Description = [[Can be equipped with:
Sights:
- Kobra
- EOTech
- AimPoint
- Ballistic
- Acog
- Laser

Attachments:
- Silencer
- Bipod
- Heavy Barrel
- Light Barrel
- Heavy Frame
- Egronomic Handle
- CustomStock
- Light/Heavy Bolt
- Gas Direction]]


include "camo_customizeable.lua"
ITEM.DontShow = false