ITEM.Name = 'AWP'
ITEM.Price = 1250000
ITEM.Material = 'materials/vgui/ttt_fun_pointshop_icons/awp.png'
ITEM.WeaponClass = 'cstm_sniper_awp'
ITEM.Description = [[Can be equipped with:
Attachments:
- Silencer
- Heavy Barrel
- Light Barrel
- Heavy Frame
- Egronomic Handle
- CustomStock
- Light/Heavy Bolt
- Gas Direction]]

include "camo_customizeable.lua"
ITEM.DontShow = false
