ITEM.Name = 'TMP'
ITEM.Price = 1250000
ITEM.Material = 'materials/vgui/ttt_fun_pointshop_icons/tmp.png'
ITEM.WeaponClass = 'cstm_smg_tmp'
ITEM.Description = [[Can be equipped with:
Sights:
- Reflex
- Laser

Attachments:
- Heavy Barrel
- Light Barrel
- Heavy Frame
- Egronomic Handle
- CustomStock
- Light/Heavy Bolt
- Gas Direction]]

include "camo_customizeable.lua"
ITEM.DontShow = false