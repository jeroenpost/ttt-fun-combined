ITEM.Name = 'Glock 18'
ITEM.Price = 750000
ITEM.Material = 'materials/vgui/ttt_fun_pointshop_icons/glock18.png'
ITEM.WeaponClass = 'cstm_pistol_glock18'
ITEM.Description = [[Can be equipped with:
Sights:
- Reflex

Attachments:
- Silencer
- Egronomic Handle
]]

include "camo_customizeable.lua"
ITEM.DontShow = false