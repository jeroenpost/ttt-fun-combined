ITEM.Name = 'G2 Contender'
ITEM.Price = 750000
ITEM.Material = 'VGUI/entities/cstm_pistol_g2contender'
ITEM.WeaponClass = 'cstm_pistol_g2contender'
ITEM.Description = [[Can be equipped with:
Sights:
- Kobra
- Rifle Reflex
- EOTech
- AimPoint
- Ballistic
- Acog
- Laser
]]

include "camo_customizeable.lua"
ITEM.DontShow = false