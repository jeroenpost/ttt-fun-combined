ITEM.Name = 'Traitor Dog'
ITEM.Price = 50000
ITEM.Material = "materials/vgui/ttt_fun_pointshop_icons/dog_transformer_traitor.png"
ITEM.WeaponClass = 'dog_transformer_traitor'
ITEM.SingleUse = true

ITEM.AdminOnly = false

function ITEM:OnBuy(ply)
	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end