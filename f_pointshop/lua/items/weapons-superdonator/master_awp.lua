ITEM.Name = 'Master AWP'
ITEM.Price = 9000
ITEM.Material = 'vgui/ttt_fun_pointshop_icons/awp.png'
ITEM.WeaponClass = 'master_awp'
ITEM.SingleUse = true


function ITEM:OnBuy(ply)
	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end
