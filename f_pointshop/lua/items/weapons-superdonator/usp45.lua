ITEM.Name = 'USP45'
ITEM.Price = 5000
ITEM.Material = "materials/vgui/ttt_fun_pointshop_icons/usp45.png"
ITEM.WeaponClass = 'weapon_ttt_sipistol'
ITEM.SingleUse = true

ITEM.AdminOnly = false

function ITEM:OnBuy(ply)
	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end