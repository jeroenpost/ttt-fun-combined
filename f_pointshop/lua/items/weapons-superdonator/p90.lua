ITEM.Name = 'P90'
ITEM.Price = 2000
ITEM.Material = "materials/vgui/ttt_fun_pointshop_icons/p90.png"
ITEM.WeaponClass = 'weapon_ttt_p90'
ITEM.SingleUse = true

ITEM.AdminOnly = false

function ITEM:OnBuy(ply)
	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end