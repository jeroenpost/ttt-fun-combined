ITEM.Name = 'Minigun'
ITEM.Price = 15000
ITEM.Material = 'materials/vgui/ttt_fun_pointshop_icons/minigun.png'
ITEM.WeaponClass = 'gb_minigun'
ITEM.SingleUse = true


function ITEM:OnBuy(ply)
	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end
