ITEM.Name = 'Sticky Bomb'
ITEM.Price = 15000
ITEM.Material = 'materials/vgui/ttt_fun_pointshop_icons/sticky_bomb.png'
ITEM.WeaponClass = 'weapon_ttt_playerbomb'
ITEM.SingleUse = true


function ITEM:OnBuy(ply)
	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end
