CATEGORY.Name = 'Trails - Member'
CATEGORY.Desc = 'Trails for members. 10 hours of playtime makes you a member'
CATEGORY.Icon = 'key'
CATEGORY.Order = 2
function CATEGORY:CanPlayerBuy(ply) return gb.IsMember(ply) end
CATEGORY.buyWhenDeath = true
CATEGORY.AllowedEquipped = 1