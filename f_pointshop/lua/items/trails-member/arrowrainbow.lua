ITEM.Name = 'ArrowRainbow Trail'
ITEM.Price = 30000
ITEM.Material = 'trails/arrowrainbow.vmt'

function ITEM:OnEquip(ply, modifications)
	ply.TrailPSARainbow = util.SpriteTrail(ply, 0, modifications.color, false, 15, 1, 4, 0.0333, self.Material)
	
end

function ITEM:OnHolster(ply)
	SafeRemoveEntity(ply.TrailPSARainbow)
end

function ITEM:Modify(modifications)
	PS:ShowColorChooser(self, modifications)
end

function ITEM:OnModify(ply, modifications)
	SafeRemoveEntity(ply.TrailPSARainbow)
	self:OnEquip(ply, modifications)
end
