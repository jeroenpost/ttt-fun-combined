ITEM.Name = 'Happy Trail'
ITEM.Price = 30000
ITEM.Material = 'trails/happy.vmt'

function ITEM:OnEquip(ply, modifications)
	ply.TrailPSHappy = util.SpriteTrail(ply, 0, modifications.color, false, 15, 1, 4, 0.0333, self.Material)
end

function ITEM:OnHolster(ply)
	SafeRemoveEntity(ply.TrailPSHappy)
end

function ITEM:Modify(modifications)
	PS:ShowColorChooser(self, modifications)
end

function ITEM:OnModify(ply, modifications)
	SafeRemoveEntity(ply.TrailPSHappy)
	self:OnEquip(ply, modifications)
end
