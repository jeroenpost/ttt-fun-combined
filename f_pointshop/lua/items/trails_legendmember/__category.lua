CATEGORY.Name = 'Trails - Legend Member'
CATEGORY.Desc = 'Trails for LEGEND members'
CATEGORY.Icon = 'key'
CATEGORY.Order = 2
function CATEGORY:CanPlayerBuy(ply) return gb.IsLegendMember(ply) end
CATEGORY.buyWhenDeath = true
CATEGORY.AllowedEquipped = 1