ITEM.Name = 'Golden Trail'
ITEM.Price = 350000
ITEM.Material = 'camos/camo7.vmt'

function ITEM:OnEquip(ply, modifications)
    if not modifications then modifications = {} end
    SafeRemoveEntity(ply.Trail) ply.Trail = util.SpriteTrail(ply, 0, modifications.color or Color(255,255,255,100), false, 15, 1, modifications.length or 2, 0.0333, self.Material)
end

function ITEM:OnHolster(ply)
    SafeRemoveEntity(ply.Trail)
end

function ITEM:Modify(modifications)
    Derma_StringRequest("Length", "How long do you want your trail to be? Enter a value between 0.1 and 5", "", function(leng)
        local leng = tonumber(leng)
        if leng and leng < 5.1 and leng > 0.09 then
            modifications.length = leng
        end
        PS:ShowColorChooser(self, modifications)
    end)
end

function ITEM:OnModify(ply, modifications)
    SafeRemoveEntity(ply.Trail)
    self:OnEquip(ply, modifications)
end
