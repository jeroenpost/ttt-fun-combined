ITEM.Name = '+25% Stamina'
ITEM.Price = 150000
ITEM.Material = 'vgui/ttt_fun_pointshop_icons/fun_bull.png'
ITEM.Description = [[Speeds up the regen of your stamina]]

function ITEM:OnEquip(ply)
    if ply:GetNWInt("staminaboost",0) < 25  then
        ply:SetNWInt("staminaboost",0.25)
    end
end 
