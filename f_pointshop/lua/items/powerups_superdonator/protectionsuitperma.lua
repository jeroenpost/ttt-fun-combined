ITEM.Name = 'Perma Protection Suit'
ITEM.Price = 125000
ITEM.Material = 'vgui/ttt_fun_pointshop_icons/protectionsuit.png'
--ITEM.WeaponClass = 'weapon_ttt_protection_suit'


function ITEM:OnEquip(ply)

	 ply:GiveEquipmentItem( EQUIP_PROTECTIONSUIT )
	ply.hasProtectionSuit = true 
	--ply:SelectWeapon(self.WeaponClass)
     hook.Add("TTTBeginRound","GiveProtectionSuit"..ply:SteamID(), function()
         timer.Simple(0.5,function()
             if not IsValid(ply) then return end
             ply:GiveEquipmentItem( EQUIP_PROTECTIONSUIT )
             ply.hasProtectionSuit = true
         end)
     end)
end

function ITEM:OnSell(ply)
	ply.hasProtectionSuit = false
	
end

function ITEM:OnHolster(ply)
	ply.hasProtectionSuit = false

end