ITEM.Name = 'Leonidas Trail'
ITEM.Price = 50000
ITEM.Material = 'trails/leonidas.vmt'

function ITEM:OnEquip(ply, modifications)
	ply.TrailPSLeo = util.SpriteTrail(ply, 0, modifications.color, false, 15, 1, 4, 0.0333, self.Material)
end

function ITEM:OnHolster(ply)
	SafeRemoveEntity(ply.TrailPSLeo)
end

function ITEM:Modify(modifications)
	PS:ShowColorChooser(self, modifications)
end

function ITEM:OnModify(ply, modifications)
	SafeRemoveEntity(ply.TrailPSLeo)
	self:OnEquip(ply, modifications)
end
