ITEM.Name = 'MusicalNotes'
ITEM.Price = 25000
ITEM.Material = 'trails/musicalnotes.vmt'

function ITEM:OnEquip(ply, modifications)
	ply.TrailPSPlasma = util.SpriteTrail(ply, 0, modifications.color, false, 15, 1, 4, 0.0333, self.Material)
end

function ITEM:OnHolster(ply)
	SafeRemoveEntity(ply.TrailPSPlasma)
end

function ITEM:Modify(modifications)
	PS:ShowColorChooser(self, modifications)
end

function ITEM:OnModify(ply, modifications)
	SafeRemoveEntity(ply.TrailPSPlasma)
	self:OnEquip(ply, modifications)
end
