ITEM.Name = 'Laser Trail'
ITEM.Price = 10000
ITEM.Material = 'trails/laser.vmt'

function ITEM:OnEquip(ply, modifications)
	ply.TrailPSLaser = util.SpriteTrail(ply, 0, modifications.color, false, 15, 1, 4, 0.0333, self.Material)
end

function ITEM:OnHolster(ply)
	SafeRemoveEntity(ply.TrailPSLaser)
end

function ITEM:Modify(modifications)
	PS:ShowColorChooser(self, modifications)
end

function ITEM:OnModify(ply, modifications)
	SafeRemoveEntity(ply.TrailPSLaser)
	self:OnEquip(ply, modifications)
end
