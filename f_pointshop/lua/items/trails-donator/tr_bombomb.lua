ITEM.Name = 'Bombomb'
ITEM.Price = 15000
ITEM.Material = 'sprites/store/bombomb.vmt'

function ITEM:OnEquip(ply, modifications)
    if ply.Trail then
         SafeRemoveEntity(ply.Trail)
    end
	ply.Trail = util.SpriteTrail(ply, 0, modifications.color, false, 15, 1, 4, 0.0333, self.Material)
end

function ITEM:OnHolster(ply)
	SafeRemoveEntity(ply.Trail)
end

function ITEM:Modify(modifications)
	PS:ShowColorChooser(self, modifications)
end

function ITEM:OnModify(ply, modifications)
	SafeRemoveEntity(ply.Trail)
	self:OnEquip(ply, modifications)
end
