ITEM.Name = 'Tube Trail'
ITEM.Price = 30000
ITEM.Material = 'trails/tube.vmt'

function ITEM:OnEquip(ply, modifications)
	ply.TrailPSTube = util.SpriteTrail(ply, 0, modifications.color, false, 15, 1, 4, 0.0333, self.Material)
end

function ITEM:OnHolster(ply)
	SafeRemoveEntity(ply.TrailPSTube)
end

function ITEM:Modify(modifications)
	PS:ShowColorChooser(self, modifications)
end

function ITEM:OnModify(ply, modifications)
	SafeRemoveEntity(ply.TrailPSTube)
	self:OnEquip(ply, modifications)
end
