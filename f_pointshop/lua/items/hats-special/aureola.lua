ITEM.Name = 'Aureola'
ITEM.Price = 10
ITEM.Model = 'models/apocmodels/sonicgenerations/ring.mdl'
ITEM.Attachment = 'eyes'

function ITEM:OnEquip(ply, modifications)
	ply:PS_AddClientsideModel(self.ID)
end

function ITEM:OnHolster(ply)
	ply:PS_RemoveClientsideModel(self.ID)
end

function ITEM:ModifyClientsideModel(ply, model, pos, ang)
	model:SetModelScale(0.7, 0)
	pos = pos + (ang:Forward() * -2.5) + (ang:Up() * 10)
	ang:RotateAroundAxis(ang:Right(), 90)
	
	return model, pos, ang
end
