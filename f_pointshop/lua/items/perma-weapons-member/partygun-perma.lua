ITEM.Name = 'Party Gun'
ITEM.Price = 60000
ITEM.Material = "materials/vgui/ttt_fun_pointshop_icons/partygun.png"
ITEM.WeaponClass = 'weapon_gb_partygun'


function ITEM:OnEquip(ply)
	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end