ITEM.Name = 'EMSSS MK-II'
ITEM.Price = 46000
ITEM.Model = "models/weapons/w_shot_g3super90.mdl"
ITEM.WeaponClass = 'emsss_mkii'
ITEM.Skin = "camos/camo44"

ITEM.AdminOnly = false

function ITEM:OnEquip(ply)
	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end