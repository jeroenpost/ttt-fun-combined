ITEM.Name = 'Perma Mario Fireballs'
ITEM.Price = 10000
ITEM.Material = 'vgui/ttt_fun_pointshop_icons/mario_fireball.png'
ITEM.WeaponClass = 'weapon_real_fireball'
ITEM.SingleUse = false




function ITEM:OnEquip(ply)
    ply:StripWeapon('weapon_zm_improvised')
	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
    
	ply:StripWeapon(self.WeaponClass)
	ply:Give('weapon_zm_improvised')
end
