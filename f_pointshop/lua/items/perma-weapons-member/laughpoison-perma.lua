ITEM.Name = 'Laugh Poison'
ITEM.Price = 60000
ITEM.Material = "materials/vgui/ttt_fun_pointshop_icons/laughterpoison.png"
ITEM.WeaponClass = 'weapon_gb_laughpoison'


function ITEM:OnEquip(ply)
	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end