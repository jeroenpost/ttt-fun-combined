ITEM.Name =  "Black Carver"
ITEM.Price = 100000
ITEM.Model  = 'models/morrowind/daedric/shortsword/w_daedric_shortsword.mdl'
ITEM.WeaponClass = 'custom_black_carver'

function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end