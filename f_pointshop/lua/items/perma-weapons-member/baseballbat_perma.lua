ITEM.Name = 'Perma Baseball Bat'
ITEM.Price = 5000
ITEM.Material = 'vgui/ttt_fun_pointshop_icons/baseball_bat.png'
ITEM.WeaponClass = 'weapon_baseballbat'


function ITEM:OnEquip(ply)
	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end