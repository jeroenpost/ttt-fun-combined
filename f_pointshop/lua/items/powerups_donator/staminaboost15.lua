ITEM.Name = '+15% Stamina'
ITEM.Price = 50000
ITEM.Material = 'vgui/ttt_fun_pointshop_icons/fun_bull.png'
ITEM.Description = [[Speeds up the regen of your stamina]]

function ITEM:OnEquip(ply)
    if ply:GetNWInt("staminaboost",0) < 15  then
        ply:SetNWInt("staminaboost",0.15)
    end
end 
