ITEM.Name = 'Burst Converter'
ITEM.Price = 500000
ITEM.Material = 'VGUI/entities/cstm_part_burstfire'
ITEM.Class = 'burstconvert'
ITEM.CanPlayerSell = false
ITEM.Description = [[Converts the weapon to fire in 3-round bursts.
Increases rate of fire by 40%
Semi-automatic not usable.
Increases recoil by 15%]]

function ITEM:OnEquip(ply, modifications)
    timer.Simple(1,function()
        if not IsValid(ply) then return end
        if not ply:HasInternalPart( self.Class ) then
            ply:PickUpInternalPart( self.Class )
        end
    end)
end

function ITEM:OnSell(ply)
   ---- ply:RemoveInternalPart( self.Class )
end
function ITEM:OnHolster(ply)
    self:OnSell(ply)
end