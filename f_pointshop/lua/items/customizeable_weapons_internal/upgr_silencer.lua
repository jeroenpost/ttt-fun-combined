ITEM.Name = 'Silencer'
ITEM.Price = 750000
ITEM.Material = 'vgui/silencer.png'
ITEM.Class = 'silencer'
ITEM.CanPlayerSell = false
ITEM.Description = [[Silence your shots]]

function ITEM:OnEquip(ply, modifications)
    timer.Simple(1,function()
        if not IsValid(ply) then return end
        if not ply:HasAttachment( self.Class ) then
            ply:PickUpAttachment( self.Class )
        end
    end)
end
function ITEM:OnBuy(ply)
    ply:ChatPrint("Press Reload (R) + WALK (Alt) to attach to a supported weapon")
end
function ITEM:OnSell(ply)

end
function ITEM:OnHolster(ply)

end