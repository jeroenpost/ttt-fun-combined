CATEGORY.Name = 'Attachments'
CATEGORY.Desc = 'Grips, Bipods, Burstconverters, etc. Cannot be sold when bought.'
CATEGORY.Icon = 'emoticon_smile'
CATEGORY.Order = 3
CATEGORY.NotAllowedUserGroups = {}
CATEGORY.IsSubcategory = true
CATEGORY.SubcategoryChild = "customizeable"
