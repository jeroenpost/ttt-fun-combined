ITEM.Name = 'Heavy Barrel'
ITEM.Price = 500000
ITEM.Material = 'VGUI/entities/cstm_part_hbar'
ITEM.Class = 'hbar'
ITEM.CanPlayerSell = false
ITEM.Description = [[Increases damage by 10%.
Slightly increases aimed accuracy.
Increases recoil by 15%.]]


function ITEM:OnEquip(ply, modifications)
    timer.Simple(1,function()
        if not IsValid(ply) then return end
        if not ply:HasInternalPart( self.Class ) then
            ply:PickUpInternalPart( self.Class )
        end
    end)
end

function ITEM:OnSell(ply)
   -- ply:RemoveInternalPart( self.Class )
end
function ITEM:OnHolster(ply)
    self:OnSell(ply)
end