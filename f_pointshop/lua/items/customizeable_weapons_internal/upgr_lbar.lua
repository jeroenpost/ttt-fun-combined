ITEM.Name = 'Light Barrel'
ITEM.Price = 500000
ITEM.Material = 'VGUI/entities/cstm_part_lbar'
ITEM.Class = 'lbar'
ITEM.CanPlayerSell = false
ITEM.Description = [[Decreases recoil by 15%
Decreases damage by 10%
Slightly decreases aimed accuracy.]]

function ITEM:OnEquip(ply, modifications)
    timer.Simple(1,function()
        if not IsValid(ply) then return end
        if not ply:HasInternalPart( self.Class ) then
            ply:PickUpInternalPart( self.Class )
        end
    end)
end

function ITEM:OnSell(ply)
   -- ply:RemoveInternalPart( self.Class )
end
function ITEM:OnHolster(ply)
    self:OnSell(ply)
end