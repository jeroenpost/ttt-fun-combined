ITEM.Name = 'C-Mag'
ITEM.Price = 750000
ITEM.Material = 'VGUI/entities/upgr_cmag'
ITEM.Class = 'cmag'
ITEM.CanPlayerSell = false
ITEM.Description = [[Increases magazine size to 100 rounds.
Decreases reload speed by 20%]]

function ITEM:OnEquip(ply, modifications)
    timer.Simple(1,function()
        if not IsValid(ply) then return end
        if not ply:HasAttachment( self.Class ) then
            ply:PickUpAttachment( self.Class )
        end
    end)
end

function ITEM:OnSell(ply)
   -- ply:RemoveInternalPart( self.Class )
end
function ITEM:OnHolster(ply)
    self:OnSell(ply)
end