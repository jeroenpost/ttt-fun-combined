ITEM.Name = 'Heavy bolt'
ITEM.Price = 500000
ITEM.Material = 'VGUI/entities/cstm_part_heavybolt'
ITEM.Class = 'heavybolt'
ITEM.CanPlayerSell = false
ITEM.Description = [[Slightly increases aimed accuracy.
Decreases rate of fire by 10%]]

function ITEM:OnEquip(ply, modifications)
    timer.Simple(1,function()
        if not IsValid(ply) then return end
        if not ply:HasInternalPart( self.Class ) then
            ply:PickUpInternalPart( self.Class )
        end
    end)
end

function ITEM:OnSell(ply)
   -- ply:RemoveInternalPart( self.Class )
end
function ITEM:OnHolster(ply)
    self:OnSell(ply)
end