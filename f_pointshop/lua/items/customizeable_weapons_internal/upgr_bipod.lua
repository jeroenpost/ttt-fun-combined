ITEM.Name = 'Bipod'
ITEM.Price = 500000
ITEM.Material = 'VGUI/entities/upgr_bipod'
ITEM.Class = 'bipod'
ITEM.CanPlayerSell = false
ITEM.Description = [[Decreases maximum spread from continuous fire by 50%
Decreases spread increase from movement by 35%
Decreases recoil by 20%]]

function ITEM:OnEquip(ply, modifications)
    timer.Simple(1,function()
        if not IsValid(ply) then return end
        if not ply:HasAttachment( self.Class ) then
            ply:PickUpAttachment( self.Class )
        end
    end)
end

function ITEM:OnSell(ply)
    ply:RemoveAttachment( self.Class )
end
function ITEM:OnHolster(ply)
    self:OnSell(ply)
end