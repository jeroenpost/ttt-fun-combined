ITEM.Name = 'TMP'
ITEM.Price = 30000
ITEM.Material = "materials/vgui/ttt_fun_pointshop_icons/tmp.png"
ITEM.WeaponClass = 'weapon_ttt_siltmp'


ITEM.AdminOnly = false

function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end