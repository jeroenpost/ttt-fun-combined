ITEM.Name = 'Perma Space Shotgun'
ITEM.Price = 24000
ITEM.Material = "materials/vgui/ttt_fun_pointshop_icons/spaceshotgun.png"
ITEM.WeaponClass = 'weapon_bulk_canon'


function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end 