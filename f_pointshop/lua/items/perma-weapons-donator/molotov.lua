ITEM.Name = 'Molotov'
ITEM.Price = 50000
ITEM.Material = "materials/vgui/ttt_fun_pointshop_icons/molotov.png"
ITEM.WeaponClass = 'weapon_ttt_molotov'


ITEM.AdminOnly = false

function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end