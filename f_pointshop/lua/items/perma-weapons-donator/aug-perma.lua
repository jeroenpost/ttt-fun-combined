ITEM.Name = 'Aug'
ITEM.Price = 30000
ITEM.Model = "models/weapons/w_rif_aug.mdl"
ITEM.WeaponClass = 'weapon_ttt_aug'
ITEM.Skin = "camos/camo14"
ITEM.AdminOnly = false

function ITEM:OnEquip(ply)

    ply:Give(self.WeaponClass)
    ply:SelectWeapon(self.WeaponClass)
    local wep = ply:GetWeapon( self.WeaponClass )
    wep:SetCamo( 14 )
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end