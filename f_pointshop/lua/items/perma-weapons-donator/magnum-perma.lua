ITEM.Name = '.44 Magnum'
ITEM.Price = 20000
ITEM.Material = "materials/vgui/ttt_fun_pointshop_icons/magnum.png"
ITEM.WeaponClass = 'weapon_ttt_44mag'


ITEM.AdminOnly = false

function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end