ITEM.Name = 'UMP45'
ITEM.Price = 30000
ITEM.Material = "materials/vgui/ttt_fun_pointshop_icons/ump45.png"
ITEM.WeaponClass = 'weapon_ttt_stungun'


ITEM.AdminOnly = false

function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end