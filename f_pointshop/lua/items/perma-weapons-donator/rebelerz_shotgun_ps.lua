ITEM.Name = "Rebelerz Shotgun"
ITEM.Price = 5000
ITEM.Material  = 'vgui/ttt_fun_pointshop_icons/m3.png'
ITEM.WeaponClass = 'custom_rebelerz_shotgun'

ITEM.Owner =  	"STEAM_0:0:144762878"

function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end