CATEGORY.Name = 'Perma Weapons for Donators'
CATEGORY.Icon = 'bomb'
CATEGORY.IsSubcategory = true
CATEGORY.SubcategoryChild = "permaweapons"
CATEGORY.Desc = 'Permanent weapons for Donators. Type !donate for info!'
CATEGORY.Icon = 'key'
function CATEGORY:CanPlayerBuy(ply) return gb.is_donator(ply) end
CATEGORY.Order = 3
CATEGORY.buyWhenDeath = true
CATEGORY.AllowedEquipped = 15