ITEM.Name = "Cross Grave"
ITEM.Cost = 350000
ITEM.Model = "models/props_c17/gravestone_cross001a.mdl"
ITEM.Bone = 'ValveBiped.Bip01_Spine2'
ITEM.Tooltip = "Gravedigger."

function ITEM:OnEquip(ply, modifications)
    ply:PS_AddClientsideModel(self.ID)
end

function ITEM:OnHolster(ply)
    ply:PS_RemoveClientsideModel(self.ID)
end


function ITEM:ModifyClientsideModel(ply, model, pos, ang)
    model:SetModelScale( 0.264706, 0 )
    local PlyModel = ply:GetModel()

    pos = pos + (ang:Forward() * 1) + (ang:Up() * 0) + (ang:Right() * 6.11765)
    ang:RotateAroundAxis(ang:Right(), -90)
    ang:RotateAroundAxis(ang:Up(), 90)
    ang:RotateAroundAxis(ang:Forward(), 0)

    return model, pos, ang
end