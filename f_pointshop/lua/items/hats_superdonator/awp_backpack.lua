ITEM.Name = 'Awp Backpack'
ITEM.Price = 75000
ITEM.Model = 'models/weapons/w_snip_awp.mdl'
ITEM.Bone = 'ValveBiped.Bip01_Spine2'

function ITEM:OnEquip(ply, modifications)
    ply:PS_AddClientsideModel(self.ID)
end

function ITEM:OnHolster(ply)
    ply:PS_RemoveClientsideModel(self.ID)
end

function ITEM:ModifyClientsideModel(ply, model, pos, ang)
    model:SetModelScale(0.8, 0)
    pos = pos + (ang:Forward() * 0) + (ang:Up() * 2) + (ang:Right() * 5)
    ang:RotateAroundAxis(ang:Right(), -30)

    return model, pos, ang
end