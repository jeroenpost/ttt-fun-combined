ITEM.Name = 'Ballistic 12x'
ITEM.Price = 750000
ITEM.Material = 'VGUI/entities/upgr_ballistic'
ITEM.Class = 'ballistic'
ITEM.CanPlayerSell = false
ITEM.Description = [[Greatly increases aim zoom.
Magnification levels can be adjusted with the mouse wheel.
Is very disorienting when engaging targets at close range.]]

function ITEM:OnEquip(ply, modifications)
    timer.Simple(1,function()
        if not IsValid(ply) then return end
        if not ply:HasAttachment( self.Class ) then
            ply:PickUpAttachment( self.Class )
        end
    end)
end

function ITEM:OnSell(ply)
   -- ply:RemoveInternalPart( self.Class )
end
function ITEM:OnHolster(ply)
    --self:OnSell(ply)
end