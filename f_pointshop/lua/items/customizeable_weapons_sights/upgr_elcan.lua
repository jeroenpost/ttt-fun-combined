ITEM.Name = 'ELCAN C79'
ITEM.Price = 750000
ITEM.Material = 'VGUI/entities/upgr_elcan'
ITEM.Class = 'elcan'
ITEM.CanPlayerSell = false
ITEM.Description = [[Provides 3.4x magnification.
Has back up sights that can be used by double tapping your USE KEY while aiming.
Can be disorienting when engaging targets at close range.]]

function ITEM:OnEquip(ply, modifications)
    timer.Simple(1,function()
        if not IsValid(ply) then return end
        if not ply:HasAttachment( self.Class ) then
            ply:PickUpAttachment( self.Class )
        end
    end)
end

function ITEM:OnSell(ply)
   -- ply:RemoveInternalPart( self.Class )
end
function ITEM:OnHolster(ply)
    self:OnSell(ply)
end