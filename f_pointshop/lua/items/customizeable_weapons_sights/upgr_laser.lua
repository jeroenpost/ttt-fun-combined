ITEM.Name = 'LaserSight'
ITEM.Price = 500000
ITEM.Material = 'VGUI/entities/upgr_lasersight'
ITEM.Class = 'laser'
ITEM.CanPlayerSell = false
ITEM.Description = [[Decreases hip-fire spread by 60%
Decreases spread increase from movement by 30%
Decreases spread increase speed from continuous fire by 15%]]

function ITEM:OnEquip(ply, modifications)
    timer.Simple(1,function()
        if not IsValid(ply) then return end
        if not ply:HasAttachment( self.Class ) then
            ply:PickUpAttachment( self.Class )
        end
    end)
end

function ITEM:OnSell(ply)
    ply:RemoveAttachment( self.Class )
end
function ITEM:OnHolster(ply)
    self:OnSell(ply)
end