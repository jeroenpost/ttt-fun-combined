CATEGORY.Name = 'Sights'
CATEGORY.Desc = 'Sights for Customizeable Weapons. Cannot be sold once bought.'
CATEGORY.Icon = 'emoticon_smile'
CATEGORY.Order = 2
CATEGORY.NotAllowedUserGroups = {}
CATEGORY.IsSubcategory = true
CATEGORY.SubcategoryChild = "customizeable"
