ITEM.Name = 'EoTech 557'
ITEM.Price = 750000
ITEM.Material = 'VGUI/entities/upgr_eotech557'
ITEM.Class = 'eotech'
ITEM.CanPlayerSell = false
ITEM.Description = [[Provides a bright red sphere-like reticle to enhance aiming.
Slightly increases aim zoom.]]

function ITEM:OnEquip(ply, modifications)
    timer.Simple(1,function()
        if not IsValid(ply) then return end
        if not ply:HasAttachment( self.Class ) then
            ply:PickUpAttachment( self.Class )
        end
    end)
end

function ITEM:OnSell(ply)
    ply:RemoveAttachment( self.Class )
end
function ITEM:OnHolster(ply)
    self:OnSell(ply)
end