ITEM.Name = 'Superman'
ITEM.Price = 500
ITEM.Material = "vgui/ttt_fun_pointshop_icons/superman.png"
ITEM.WeaponClass = 'superman'
ITEM.SingleUse = true

function ITEM:OnBuy(ply)
	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end