ITEM.Name = 'Traitor'
ITEM.Price = 7500
ITEM.Material = 'vgui/ttt/sprite_traitor.vmt'
ITEM.OneUse = true
ITEM.SingleUse = true
ITEM.buyWhenDeath = true

function ITEM:CanPlayerBuy(ply)

    local timebetween = os.time() - (ply:GetPData( "lasttraitorbuy", 1) * 1)
    if timebetween < 600 then
        ply:PS_Notify("You can only buy traitor once every 10 minutes. Please wait "..(600-timebetween).." seconds")
        return false
    end

    return canPlayerBuySpecialRound(ply, false)

end

function ITEM:OnBuy(ply)
    ply:SetPData( "lasttraitorbuy", os.time())
    ply:SetPData( "IPDIforceTnr", 1)
	print( ply:Nick().." bought Traitor")
	ply:PS_Notify("You will be traitor next round")
end

function ITEM:OnHolster(ply)
--hook.Remove("TTTBeginRound", ply:UniqueID() .. "_traitor")
end

function ITEM:OnSell(ply)
--hook.Remove("TTTBeginRound", ply:UniqueID() .. "_traitor")
end