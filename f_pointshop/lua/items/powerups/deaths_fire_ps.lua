ITEM.Name = "Deaths Trail"
ITEM.Price = 50
ITEM.Material = 'camos/custom_camo32.vmt'

ITEM.Owner =  	"STEAM_0:0:144762878"


function ITEM:OnEquip(ply, modifications)
    timer.Create("healthregen"..ply:SteamID(),1,0,function()
        autohealfunction(ply)
    end)
    if not modifications then modifications = {} end
    SafeRemoveEntity(ply.Trail)
    ply:SetNWInt("runspeed", 400)
    ply:SetNWInt("walkspeed",275)
    ply:SetJumpPower(250)
    ply.Trail = util.SpriteTrail(ply, 0,  Color(255,255,255,255), false, 15, 1, modifications.length or 4, 0.0333, self.Material)
end

function ITEM:OnHolster(ply)
    SafeRemoveEntity(ply.Trail)
end

function ITEM:Modify(modifications)
    Derma_StringRequest("Length", "How long do you want your trail to be? Enter a value between 0.1 and 15", "", function(leng)
        local leng = tonumber(leng)
        if leng and leng < 15.1 and leng > 0.09 then
            modifications.length = leng
        end
        PS:ShowColorChooser(self, modifications)
    end)
end

function ITEM:OnModify(ply, modifications)
    SafeRemoveEntity(ply.Trail)
    self:OnEquip(ply, modifications)
end
