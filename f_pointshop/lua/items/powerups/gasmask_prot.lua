ITEM.Name = "Gas Mask"
ITEM.Price = 5000
ITEM.Material = 'materials/vgui/ttt_fun_pointshop_icons/gasmask.png'
--ITEM.WeaponClass = 'weapon_ttt_protection_suit'
ITEM.Description = "Gas wont have effect on you anymore. Has to be renewed every map."
ITEM.SingleUse = true

function ITEM:OnBuy(ply)
	-- ply:Give(self.WeaponClass)
	ply.GasProtection = true
	--ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply.GasProtection = false
	
end

function ITEM:OnHolster(ply)
	ply.GasProtection = false
end