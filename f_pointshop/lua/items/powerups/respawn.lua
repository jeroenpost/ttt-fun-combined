ITEM.Name = 'Respawn'
ITEM.Price = 7500
ITEM.Material = 'vgui/ttt_fun_pointshop_icons/respawn.png'
ITEM.OneUse = true
ITEM.SingleUse = true
ITEM.buyWhenDeath = true


function ITEM:CanPlayerBuy( ply )
    
    if GetGlobalInt("repawenedPointShop"..ply:SteamID()) > CurTime() then
         ply:PS_Notify("You can buy respawn every 15 minutes")
        return false
    end
    if specialRound and specialRound.isSpecialRound then
         ply:PS_Notify("You cannot buy Respawn in a special round")
        return false
    end
    return true
end

function ITEM:OnBuy(ply)
	
	    ply:SetGhost(false)
		ply:SetTeam( TEAM_TERROR )
   		ply:Spawn()
		SetGlobalInt("repawenedPointShop"..ply:SteamID(), CurTime() + 900)
		
		print( ply:Nick().." bought respawn")

        if SERVER then
            DamageLog("RESPAWN: " .. ply:Nick() .. " [" .. ply:GetRoleString() .. "] bought respawn from the pointshop ")
        end
	
		-- Remove status after every round
		--hook.Add("TTTEndRound", "removeRespawnedStatus", function()
		   --for k,v in pairs(player.GetAll()) do
		--		v.repawenedPointShop = false
		--		v.repawenedPointShop = nil
		--    end
		--end)
	
	
end

function ITEM:OnEquip(ply)
    ply:SetNWBool("repawenedPointShop", true)
    end

function ITEM:OnHolster(ply)
--hook.Remove("TTTBeginRound", ply:UniqueID() .. "_detective")
end

function ITEM:OnSell(ply)
--hook.Remove("TTTBeginRound", ply:UniqueID() .. "_detective")
end
