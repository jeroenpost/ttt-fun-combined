ITEM.Name = 'Gestures (permanent)'
ITEM.Price = 50000
ITEM.Material = 'vgui/ttt_fun_pointshop_icons/gestures.png'
ITEM.buyWhenDeath = true
ITEM.Description = [[You can Dance, and do other gestures after buying this
Type: show_gestures in console to see all gestures]]

function ITEM:OnEquip(ply)
	ply.canGesture = true	
end

function ITEM:OnBuy( ply )
	ply.canGesture = true
	ply:ConCommand("show_gestures")
end

function ITEM:OnHolster(ply)
	ply.canGesture = false
end

function ITEM:OnSell(ply)
	ply.canGesture = false
end
