ITEM.Name = '+25% Jump Boots'
ITEM.Price = 2500
ITEM.Material = 'vgui/ttt_fun_pointshop_icons/jumpboots.png'
ITEM.SingleUse = true

function ITEM:OnBuy(ply)
    if ply:GetJumpPower() < 200 then
	    ply:SetJumpPower(200)
    end
    ply.HasJumpBoots = true
end 

