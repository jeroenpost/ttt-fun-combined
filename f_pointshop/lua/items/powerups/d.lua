ITEM.Name = 'Detective'
ITEM.Price = 50
ITEM.Material = 'vgui/ttt_fun_pointshop_icons/detective.png'
ITEM.OneUse = true
ITEM.SingleUse = true
ITEM.buyWhenDeath = true

function ITEM:CanPlayerBuy(ply)


       if GetGlobalInt("ttt_rounds_left", 6) < 4 then
       ply:PS_Notify("The next round will be special, you cannot buy detective now")
	return false
       end

       for k,v in pairs(player.GetAll()) do
           if v.BoughtBossThisRound == true then
               ply:PS_Notify("Someone bought a bossround for next round. Try again next round")
               return false
           end
       end


    return true
end


function ITEM:OnBuy(ply)
    ply:SetPData( "IPDIforceDnr", 1)
	print( ply:Nick().." bought Detective")
	ply:PS_Notify("You will be detective next round")
end
