ITEM.Name = 'The Infected'
ITEM.Price = 17500
ITEM.Material = 'vgui/ttt_fun_pointshop_icons/theinfected.png'
ITEM.OneUse = true
ITEM.SingleUse = true
ITEM.buyWhenDeath = true

function ITEM:CanPlayerBuy(ply)

    return canPlayerBuySpecialRound(ply)
end

function ITEM:OnBuy(ply)
    ply.BoughtBossThisRound = true
    ply.GotTheInfected = true;
    print( ply:Nick().." bought The Infected")
    ply:PS_Notify("You will be The Infected next round!")

    refundPlayersWithSpecialStuff()

end

