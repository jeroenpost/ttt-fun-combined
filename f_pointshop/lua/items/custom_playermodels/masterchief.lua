ITEM.IsPlayerModel = true  ITEM.Name = 'Masterchief'
ITEM.Price = 50
ITEM.Model = 'models/halo4/spartans/masterchief_player.mdl'

ITEM.Owner = 	"STEAM_0:0:44819727"
--ITEM.Skin = 'camos/custom_camo7'

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end

function ITEM:OnEquip(ply, modifications)
	if not ply._OldModel then
		ply._OldModel = ply:GetModel()
	end
	hook.Remove("TTTBeginRound", ply:UniqueID() .. "_setLowerHealthPlayerModel")
	timer.Simple(1, function() if not IsValid(ply) then return end ply.PlayerModel = self.Model end) ply.PlayerModel = self.Model ply:SetModel( ply.PlayerModel )
end

function ITEM:OnHolster(ply)
	if ply._OldModel then
		ply:SetModel(ply._OldModel)
	end
	hook.Remove("TTTBeginRound", ply:UniqueID() .. "_setLowerHealthPlayerModel")
end