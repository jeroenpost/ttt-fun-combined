ITEM.IsPlayerModel = true
ITEM.Name = 'Moon Knight'
ITEM.Price = 5000
ITEM.Model = 'models/player/moon_knight/slow_v2.mdl'

ITEM.Owner = 	"STEAM_0:0:50080297"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end


function ITEM:OnEquip(ply, modifications)
	if not ply._OldModel then
		ply._OldModel = ply:GetModel()
	end

	timer.Simple(5, function()  ply.PlayerModel = self.Model ply:SetModel( ply.PlayerModel ) end) ply.PlayerModel = self.Model ply:SetModel( ply.PlayerModel )
end

function ITEM:OnHolster(ply)
	if ply._OldModel then
		ply:SetModel(ply._OldModel)
	end

end