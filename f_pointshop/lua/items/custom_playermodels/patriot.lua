ITEM.IsPlayerModel = true  ITEM.Name = 'Patriot'
ITEM.Price = 50
ITEM.Model = 'models/avengers/iron_man/patriot_player.mdl'

ITEM.Owner = 	"STEAM_0:1:51258820 "

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner or ply:SteamID() == "STEAM_0:0:56086754" or ply:IsSuperAdmin() then
        return true
    end

    return false
end
 

function ITEM:OnEquip(ply, modifications)
	if not ply._OldModel then
		ply._OldModel = ply:GetModel()
	end
	hook.Remove("TTTBeginRound", ply:UniqueID() .. "_setLowerHealthPlayerModel")
	timer.Simple(1, function()  ply.PlayerModel = self.Model end) ply.PlayerModel = self.Model ply:SetModel( ply.PlayerModel )
end

function ITEM:OnHolster(ply)
	if ply._OldModel then
		ply:SetModel(ply._OldModel)
	end
	hook.Remove("TTTBeginRound", ply:UniqueID() .. "_setLowerHealthPlayerModel")
end