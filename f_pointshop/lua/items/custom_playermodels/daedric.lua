ITEM.IsPlayerModel = true
ITEM.Name = 'Deadric'
ITEM.Price = 50
ITEM.Model = 'models/player/daedric.mdl'

ITEM.Owner = 	"STEAM_0:0:144762878"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner or ply:IsSuperAdmin() then 
        return true
    end

    return false
end


function ITEM:OnEquip(ply, modifications)
	if not ply._OldModel then
		ply._OldModel = ply:GetModel()
	end
	hook.Remove("TTTBeginRound", ply:UniqueID() .. "_setLowerHealthPlayerModel")
	timer.Simple(1, function()  ply.PlayerModel = self.Model end) ply.PlayerModel = self.Model ply:SetModel( ply.PlayerModel )
    ply.dealextradamage = 1.15
    ply:ChatPrint("Your playermodel doest register headshots. You deal 15% more damage")

    hook.Add("PlayerDeath", "daedrick"..ply:SteamID(),function( victim, weapon, killer )
        if IsValid(ply) and not ply:IsGhost() and SERVER and killer == ply  then
            if not killer.nextdeadrickhealth then killer.nextdeadrickhealth = 0 end
            if killer:Health() < 150 and  killer.nextdeadrickhealth < CurTime() then
                killer.nextdeadrickhealth = CurTime() + 15
                killer:SetHealth(killer:Health() + 30)
                if killer:Health() > 150 then  killer:SetHealth(150) end
            end
        end
    end)
end

function ITEM:OnHolster(ply)
	if ply._OldModel then
		ply:SetModel(ply._OldModel)
    end
    ply.dealextradamage = 1
	hook.Remove("TTTBeginRound", ply:UniqueID() .. "_setLowerHealthPlayerModel")
end
