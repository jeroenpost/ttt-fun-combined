ITEM.IsPlayerModel = true  ITEM.Name = 'Sauron'
ITEM.Price = 2000
ITEM.Model = 'models/koz/lotr/sauron/sauron.mdl'

ITEM.Owner = "STEAM_0:0:39767777"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end



function ITEM:OnEquip(ply, modifications)
	if not ply._OldModel then
		ply._OldModel = ply:GetModel()
	end
	hook.Remove("TTTBeginRound", ply:UniqueID() .. "_setLowerHealthPlayerModel")
	timer.Simple(1, function()  ply.PlayerModel = self.Model end) ply.PlayerModel = self.Model ply:SetModel( ply.PlayerModel )
end

function ITEM:OnHolster(ply)
	if ply._OldModel then
		ply:SetModel(ply._OldModel)
	end
	hook.Remove("TTTBeginRound", ply:UniqueID() .. "_setLowerHealthPlayerModel")
end
