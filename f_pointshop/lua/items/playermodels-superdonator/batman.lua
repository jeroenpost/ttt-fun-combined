ITEM.IsPlayerModel = true
ITEM.Name = 'Batman'
ITEM.Price = 250000
ITEM.Model = 'models/Batman/slow/jamis/mkvsdcu/batman/slow_pub_v2.mdl'

function ITEM:OnEquip(ply, modifications)
	if not ply._OldModel then
		ply._OldModel = ply:GetModel()
	end
	timer.Simple(1, function()  ply.PlayerModel = self.Model end) ply.PlayerModel = self.Model ply:SetModel( ply.PlayerModel )
end

function ITEM:OnHolster(ply)
	if ply._OldModel then
		ply:SetModel(ply._OldModel)
	end
end