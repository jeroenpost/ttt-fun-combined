ITEM.Name = 'Bomb Head Skully'
ITEM.Price = 50000
ITEM.Model = 'models/Combine_Helicopter/helicopter_bomb01.mdl'
ITEM.Attachment = 'eyes'
ITEM.Skin = "camos/camo14"

function ITEM:OnEquip(ply, modifications)
	ply:PS_AddClientsideModel(self.ID)
end

function ITEM:OnHolster(ply)
	ply:PS_RemoveClientsideModel(self.ID)
end

function ITEM:ModifyClientsideModel(ply, model, pos, ang)
	model:SetModelScale(0.5, 0)
    model:SetMaterial("camos/camo14")
	pos = pos + (ang:Forward() * -2)
	ang:RotateAroundAxis(ang:Right(), 90)
	
	return model, pos, ang
end
