ITEM.Name = 'Ze Explosive'
ITEM.Price = 350000
ITEM.Model = 'models/props_c17/oildrum001_explosive.mdl'
ITEM.Skin = "camos/camo18"
ITEM.Follower = 'explosive'

function ITEM:OnEquip(ply, modifications)
	local ent = ply:Fo_CreateFollower( self.Follower )
    ent:SetMaterial("camos/camo18")
end

function ITEM:OnHolster(ply)
	ply:Fo_RemoveFollower( self.Follower )
end