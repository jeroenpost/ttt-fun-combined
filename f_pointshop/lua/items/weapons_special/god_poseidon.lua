ITEM.Name = "Poseidon's Trident"
ITEM.Price = 25000
ITEM.Material = "materials/vgui/ttt_fun_pointshop_icons/poseidons_fork.png"
ITEM.WeaponClass = 'weapon_gb_poseidon'
ITEM.SingleUse = true

ITEM.AdminOnly = false

function ITEM:OnBuy(ply)
	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end