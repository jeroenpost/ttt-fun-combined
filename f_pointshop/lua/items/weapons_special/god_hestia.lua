ITEM.Name = "Hestia's Kiss of Enlightment"
ITEM.Price = 50000
ITEM.Material = "materials/vgui/ttt_fun_pointshop_icons/hestias_kiss_of_enlightment.png"
ITEM.WeaponClass = 'weapon_gb_hestia'
ITEM.SingleUse = true

ITEM.AdminOnly = false

function ITEM:OnBuy(ply)
	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end