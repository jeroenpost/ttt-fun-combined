ITEM.Name = "Megamelon"
ITEM.Price = 1000000
ITEM.Model = "models/props_junk/watermelon01.mdl"
ITEM.WeaponClass = 'gb_megamelon'
ITEM.SingleUse = true
ITEM.Description = [[Extremely OP weapon
2000 damage / 10 Melons per shot
unlimited ammo]]

ITEM.AdminOnly = false

function ITEM:CanPlayerBuy( ply )

    if specialRound.isSpecialRound then
        return false,"Special Round, you cant buy this now!"
    elseif GetRoundState() != ROUND_ACTIVE then
    return false,"Wait untill the round starts!"
    elseif GetGlobalString("nukeweapon_used") == "yes" then
        return false,"A nukeweapon was already used this map"
    elseif GetGlobalString("rdminatorPointShop"..ply:SteamID()) != "yes" then
    return true
    else
        return false,"You can only use a Nukeweapon once a map"
    end

end


function ITEM:OnBuy(ply)
    ply:SetRole(ROLE_TRAITOR)
    for k,v in pairs(player.GetAll()) do
        if ply != v then
        if not v:Alive() then
            v:SetTeam( TEAM_TERROR )
            v:SetRole(ROLE_DETECTIVE)
            v:Spawn()
        end
        v:SetRole(ROLE_DETECTIVE)
        end
    end
    timer.Simple(0.01,function() SendFullStateUpdate() end)

    for k,v in pairs(player.GetAll()) do
        if v.BoughtDOrT == true then
            v:PS_Notify("Someone bought a nukeweapon, since you bought D or T you get a refund")
            v:ChatPrint("Someone bought a nukeweapon, since you bought D or T you get a refund")
            v:PS_GivePoints(7500)
        end
    end

    SetGlobalString("nukeweapon_used","yes")
    SetGlobalString("rdminatorPointShop"..ply:SteamID(), "yes")
	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end