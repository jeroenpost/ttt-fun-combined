ITEM.Name = 'Zeus Thunder'
ITEM.Price = 25000
ITEM.Material = "materials/vgui/ttt_fun_pointshop_icons/zeus_thunder.png"
ITEM.WeaponClass = 'weapon_gb_zeus'
ITEM.SingleUse = true

ITEM.AdminOnly = false

function ITEM:OnBuy(ply)
	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end