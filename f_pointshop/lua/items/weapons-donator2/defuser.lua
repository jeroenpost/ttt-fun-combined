ITEM.Name = 'Defuser'
ITEM.Price = 5000
ITEM.Material = 'vgui/ttt_fun_pointshop_icons/defuser.png'
ITEM.WeaponClass = 'weapon_ttt_defuser'
ITEM.SingleUse = true



function ITEM:OnBuy(ply)
	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end
