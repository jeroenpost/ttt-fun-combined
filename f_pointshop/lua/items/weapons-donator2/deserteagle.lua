ITEM.Name = 'Deserteagle'
ITEM.Price = 1500
ITEM.Material = "materials/vgui/ttt_fun_pointshop_icons/deserteagle.png"
ITEM.WeaponClass = 'weapon_zm_revolver'
ITEM.SingleUse = true

ITEM.AdminOnly = false

function ITEM:OnBuy(ply)
	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end