ITEM.Name = 'Elites'
ITEM.Price = 1200
ITEM.Material = "materials/vgui/ttt_fun_pointshop_icons/elites.png"
ITEM.WeaponClass = 'weapon_ttt_elites'
ITEM.SingleUse = true

ITEM.AdminOnly = false

function ITEM:OnBuy(ply)
	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end