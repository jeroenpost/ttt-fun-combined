ITEM.Name = 'Bianchi FA-6'
ITEM.Price = 2000
ITEM.Material = 'vgui/ttt_fun_pointshop_icons/bianchi.png'
ITEM.WeaponClass = 'weapon_bianchi'
ITEM.SingleUse = true

function ITEM:OnBuy(ply)
	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end