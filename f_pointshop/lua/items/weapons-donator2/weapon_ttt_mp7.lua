ITEM.Name = 'H&K MP7'
ITEM.Price = 800
ITEM.Material = 'vgui/ttt_fun_pointshop_icons/mp7.png'
ITEM.WeaponClass = 'weapon_ttt_mp7'
ITEM.SingleUse = true

function ITEM:OnBuy(ply)
	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end