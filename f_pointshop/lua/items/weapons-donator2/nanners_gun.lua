ITEM.Name = 'Nanners Gun'
ITEM.Price = 25000
ITEM.Material = 'materials/vgui/ttt_fun_pointshop_icons/nanners_gun.png'
ITEM.WeaponClass = 'gb_nanners'
ITEM.SingleUse = true

function ITEM:OnBuy(ply)
	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end
