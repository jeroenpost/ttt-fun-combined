ITEM.Name = 'AK47 Gold'
ITEM.Price = 5000
ITEM.Material = "materials/vgui/ttt_fun_pointshop_icons/ak47.png"
ITEM.WeaponClass = 'gb_gold_ak'
ITEM.SingleUse = true

ITEM.AdminOnly = false

function ITEM:OnBuy(ply)
	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end