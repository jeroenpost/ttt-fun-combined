ITEM.Name = 'Scout'
ITEM.Price = 1250
ITEM.Material = "materials/vgui/ttt_fun_pointshop_icons/scout.png"
ITEM.WeaponClass = 'weapon_zm_rifle'
ITEM.SingleUse = true

ITEM.AdminOnly = false

function ITEM:OnBuy(ply)
	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end