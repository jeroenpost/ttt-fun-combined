ITEM.Name = 'NES Zapper'
ITEM.Price = 2000
ITEM.Material = 'vgui/ttt_fun_pointshop_icons/zapper.png'
ITEM.WeaponClass = 'weapon_zapper'
ITEM.SingleUse = true

function ITEM:OnBuy(ply)
	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end