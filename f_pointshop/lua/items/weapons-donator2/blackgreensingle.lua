ITEM.Name = 'BlackGreen'
ITEM.Price = 50000
ITEM.Material = 'materials/vgui/ttt_fun_pointshop_icons/green_black.png'
ITEM.WeaponClass = 'black_green'

ITEM.SingleUse = true
function ITEM:OnEquip(ply, modifications)
         
	
        ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end
