ITEM.Name = "Warden's Big Finger"
ITEM.Price = 50
ITEM.Material  = 'vgui/ttt_fun_pointshop_icons/ak47.png'
ITEM.WeaponClass = 'custom_wardens_big_finger'

ITEM.Owner =  	"STEAM_0:1:57650756"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end

function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end