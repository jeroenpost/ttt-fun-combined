ITEM.Name =  "Orb's Sniper"
ITEM.Price = 60
ITEM.Material = 'materials/vgui/ttt_fun_pointshop_icons/sg550.png'
ITEM.WeaponClass = 'custom_orbs_sniper'

ITEM.Owner =  "STEAM_0:1:53508693"

function ITEM:CanPlayerBuy( ply )

    if ply:SteamID() == self.Owner or ply:IsSuperAdmin() then
        return true
    end

    return false
end


function ITEM:OnEquip(ply)

    ply:Give(self.WeaponClass)
    ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
    ply:StripWeapon(self.WeaponClass)
end