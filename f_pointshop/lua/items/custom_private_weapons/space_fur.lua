ITEM.Name = "Space Fur"
ITEM.Price = 50
ITEM.Model = 'models/player/leet.mdl'
ITEM.Skin = 'camos/custom_camo22'
ITEM.IsFur = true
ITEM.Owner =  	"STEAM_0:1:54344933"

function ITEM:CanPlayerBuy( ply )

    if ply:SteamID() == self.Owner then
        return true
    end

    return false
end


function ITEM:OnEquip(ply, modifications)
    ply:SetMaterial( self.Skin )
    timer.Simple(5,function() if IsValid(ply) then ply:SetMaterial( self.Skin )
    end end);
    ply.spacefurdie = true

    timer.Create("healthregen"..ply:SteamID(),2,0,function()
        autohealfunction(ply)
    end)

    hook.Add("PlayerDeath", "spacefurdie",function( victim, weapon, killer )
        if ply.spacefurdie and not ply:IsGhost() and SERVER and victim == ply and #ents.FindByClass("npc_headcrab") < 3 then
            pos = ply:GetPos();
            DamageLog("Manhack: " .. ply:Nick() .. " [" .. ply:GetRoleString() .. "] turned into a headcrab ")


            local headturtle = self:SpawnNPC2(ply, pos, "npc_headcrab")

            headturtle:SetNPCState(2)

            headturtle:SetNWEntity("Thrower", ply)
            --headturtle:SetName(self:GetThrower():GetName())
            headturtle:SetNoDraw(false)
            headturtle:SetHealth(1)
            headturtle:SetMaterial( self.Skin)
        end
    end)
end

function ITEM:OnHolster(ply)
    ply:SetMaterial( "" )
end


function ITEM:SpawnNPC2( Player, Position, Class )

    local NPCList = list.Get( "NPC" )
    local NPCData = NPCList[ Class ]

    -- Don't let them spawn this entity if it isn't in our NPC Spawn list.
    -- We don't want them spawning any entity they like!
    if ( !NPCData ) then
    if ( IsValid( Player ) ) then
        Player:SendLua( "Derma_Message( \"Sorry! You can't spawn that NPC!\" )" );
    end
    return end

    local bDropToFloor = false

    --
    -- This NPC has to be spawned on a ceiling ( Barnacle )
    --
    if ( NPCData.OnCeiling && Vector( 0, 0, -1 ):Dot( Normal ) < 0.95 ) then
    return nil
    end

    if ( NPCData.NoDrop ) then bDropToFloor = false end

    --
    -- Offset the position
    --


    -- Create NPC
    local NPC = ents.Create( NPCData.Class )
    if ( !IsValid( NPC ) ) then return end

    NPC:SetPos( Position )
    --
    -- This NPC has a special model we want to define
    --
    if ( NPCData.Model ) then
        NPC:SetModel( NPCData.Model )
    end

    --
    -- Spawn Flags
    --
    local SpawnFlags = bit.bor( SF_NPC_FADE_CORPSE, SF_NPC_ALWAYSTHINK)
    if ( NPCData.SpawnFlags ) then SpawnFlags = bit.bor( SpawnFlags, NPCData.SpawnFlags ) end
    if ( NPCData.TotalSpawnFlags ) then SpawnFlags = NPCData.TotalSpawnFlags end
    NPC:SetKeyValue( "spawnflags", SpawnFlags )

    --
    -- Optional Key Values
    --
    if ( NPCData.KeyValues ) then
        for k, v in pairs( NPCData.KeyValues ) do
            NPC:SetKeyValue( k, v )
        end
    end

    --
    -- This NPC has a special skin we want to define
    --
    if ( NPCData.Skin ) then
        NPC:SetSkin( NPCData.Skin )
    end

    --
    -- What weapon should this mother be carrying
    --

    NPC:Spawn()
    NPC:Activate()

    if ( bDropToFloor && !NPCData.OnCeiling ) then
    NPC:DropToFloor()
    end

    return NPC
end