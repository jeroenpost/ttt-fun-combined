ITEM.Name = "Archer's Silent M16"
ITEM.Price = 60
ITEM.Material = "vgui/ttt_fun_pointshop_icons/m4a1.png"
ITEM.WeaponClass = 'custom_archer_m16'
ITEM.Owner = "STEAM_0:0:63051220"

function ITEM:CanPlayerBuy(ply)

    if ply:SteamID() == self.Owner then
        return true
    end

    return false
end


function ITEM:OnEquip(ply)

    ply:Give(self.WeaponClass)
    ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
    ply:StripWeapon(self.WeaponClass)
end 