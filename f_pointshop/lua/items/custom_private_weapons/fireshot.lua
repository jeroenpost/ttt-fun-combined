ITEM.Name =  "FireShot"
ITEM.Price = 60
ITEM.Model = 'models/weapons/w_gbip_scoub.mdl'
ITEM.WeaponClass = 'custom_fireshot'


ITEM.Owner =  "STEAM_0:0:50080297"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end

function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end