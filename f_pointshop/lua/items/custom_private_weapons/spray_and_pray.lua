ITEM.Name =  'Spray and Pray'
ITEM.Price = 60
ITEM.Material = 'materials/vgui/ttt_fun_pointshop_icons/mac10.png'
ITEM.WeaponClass = 'custom_spray_and_pray'
ITEM.CanPlayerSell = false


ITEM.Owner = "STEAM_0:1:52228125"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end




function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
     ply:SetNWInt("runspeed", 380)
     ply:SetNWInt("walkspeed",250)
     ply:SetJumpPower(250)
     ply.hasProtectionSuit = true

end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
    ply:SetNWInt("runspeed", 320)
    ply:SetNWInt("walkspeed",220)
    ply:SetJumpPower(160)
    ply.hasProtectionSuit = true
end
