ITEM.Name = 'Fallen Angel'
ITEM.Price = 10
ITEM.Model = 'models/apocmodels/sonicgenerations/ring.mdl'
ITEM.Attachment = 'eyes'

ITEM.Owner =  	"STEAM_0:0:29205173"

function ITEM:CanPlayerBuy( ply )

    if ply:SteamID() == self.Owner then
        return true
    end

    return false
end


function ITEM:OnEquip(ply, modifications)
	ply:PS_AddClientsideModel(self.ID)
    if ply:GetNWInt("runspeed") < 340 then
        ply:SetNWInt("runspeed", 340)
    end
    if ply:GetNWInt("walkspeed") < 250 then
        ply:SetNWInt("walkspeed", 250)
    end

    timer.Create("healthregen"..ply:SteamID(),2,0,function()
        autohealfunction(ply)
    end)
    ply.hasProtectionSuit = true

end

function ITEM:OnHolster(ply)
	ply:PS_RemoveClientsideModel(self.ID)
end

function ITEM:ModifyClientsideModel(ply, model, pos, ang)
	model:SetModelScale(0.75, 0)
    model:SetMaterial('camos/custom_camo2')
	pos = pos + (ang:Forward() * -2.5) + (ang:Up() * 10)
	ang:RotateAroundAxis(ang:Right(), 90)
    model:Fo_AttachParticles( "dark_clouds" )


	
	return model, pos, ang
end
