ITEM.Name = "The Stick of Truth"
ITEM.Price = 50
ITEM.Model = "models/weapons/w_stunbaton.mdl"
ITEM.WeaponClass = 'custom_senpie_nator'

ITEM.Owner =  	"STEAM_0:1:43567907"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end

function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end