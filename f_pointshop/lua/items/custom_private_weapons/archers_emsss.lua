ITEM.Name = "Archer's Emsss"
ITEM.Price = 6000
ITEM.Model = "models/weapons/w_shot_g3super90.mdl"
ITEM.WeaponClass = 'custom_archers_emsss'
ITEM.Owner = "STEAM_0:0:63051220"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end


function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end 