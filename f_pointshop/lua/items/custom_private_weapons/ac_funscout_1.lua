ITEM.Name = "False Fingers"
ITEM.Price = 5000
ITEM.Model = "models/weapons/w_gbip_scoutrifle.mdl"
ITEM.WeaponClass = 'cstm_funscout_1'

ITEM.Owner =  	"STEAM_0:1:48618627"

function ITEM:CanPlayerBuy( ply )

    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end

function ITEM:OnEquip(ply)
    ply:GiveEquipmentItem( EQUIP_PROTECTIONSUIT )
    ply.hasProtectionSuit = true
	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end
