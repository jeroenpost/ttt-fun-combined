ITEM.Name = "Silent's Saxophone"
ITEM.Price = 50
ITEM.Model = 'models/weapons/c_models/c_bugle/c_bugle.mdl'
ITEM.WeaponClass = 'custom_silents_sax'

ITEM.Owner =  	"STEAM_0:1:73414858"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end

function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end