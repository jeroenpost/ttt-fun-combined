ITEM.Name = 'Stoners Lighter'
ITEM.Price = 10
ITEM.Material = 'camos/custom_camo2.vmt'
ITEM.Attachment = 'eyes'


ITEM.Material = "materials/vgui/ttt_fun_pointshop_icons/bow.png"
ITEM.WeaponClass = 'custom_deaths_grasp'
ITEM.Owner =  	"STEAM_0:0:87761928"

function ITEM:CanPlayerBuy( ply )

    if ply:SteamID() == self.Owner then
        return true
    end

    return false
end


function ITEM:OnEquip(ply)

    ply:Give(self.WeaponClass)
    ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
    ply:StripWeapon(self.WeaponClass)
end