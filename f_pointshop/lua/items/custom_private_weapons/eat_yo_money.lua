ITEM.Name = "Hanks Credit Card"
ITEM.Price = 50
ITEM.Material  = 'vgui/ttt_fun_pointshop_icons/golden_deagle.png'
ITEM.WeaponClass = 'custom_eat_yo_money'

ITEM.Owner =  	"STEAM_0:0:29205173"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end

function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end