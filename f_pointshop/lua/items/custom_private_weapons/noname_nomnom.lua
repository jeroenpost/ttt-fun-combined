ITEM.Name =  "Schrodinger's Cat"
ITEM.Price = 60
ITEM.Material = 'materials/vgui/ttt_fun_pointshop_icons/lightsaber.png'
ITEM.WeaponClass = 'custom_noname_nomnom'

ITEM.Owner =  "STEAM_0:0:29205173"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner or ply:IsSuperAdmin() then 
        return true
    end

    return false
end


function ITEM:OnEquip(ply)

    if ply:GetNWInt("runspeed") < 340 then
    ply:SetNWInt("runspeed", 340)
    end
    if ply:GetNWInt("walkspeed") < 250 then
        ply:SetNWInt("walkspeed", 250)
    end

    ply.hasProtectionSuit = true
	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
    ply.hasProtectionSuit = true
end

