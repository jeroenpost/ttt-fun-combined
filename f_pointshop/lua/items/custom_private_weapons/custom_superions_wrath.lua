ITEM.Name =  "Banana Fight!"
ITEM.Price = 50
ITEM.Model = "models/props/cs_italy/bananna.mdl"
ITEM.WeaponClass = 'custom_superions_eternal_wrath'

ITEM.Owner =  	"STEAM_0:0:61204070"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end

function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end