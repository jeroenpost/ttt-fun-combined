ITEM.Name = "Batpigs Sniper"
ITEM.Price = 50
ITEM.Model  = "models/workspizza03/workspizza03.mdl"
ITEM.WeaponClass = 'custom_scottys_true_love'

ITEM.Owner =  	"STEAM_0:1:65771325"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end

function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)

     if ply:GetJumpPower() < 340 then
         ply:SetJumpPower(340)
     end
     ply.HasJumpBoots = true
     if ply:GetNWInt("walkspeed") < 280 then
         ply:SetNWInt("walkspeed",280)
     end
     if ply:GetNWInt("runspeed") < 390 then
         ply:SetNWInt("runspeed", 390)
     end
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end