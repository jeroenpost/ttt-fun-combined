ITEM.Name = "Revenge"
ITEM.Price = 60
ITEM.Material  = 'vgui/ttt_fun_pointshop_icons/defibrillator.png'
ITEM.WeaponClass = 'custom_bagel_defib'
ITEM.Owner =  	"STEAM_0:1:102159443"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner or ply:IsSuperAdmin() then
        return true
    end

    return false
end



function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end