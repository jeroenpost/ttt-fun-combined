ITEM.Name = "Jeremy"
ITEM.Price = 60
ITEM.Material = "vgui/ttt_fun_pointshop_icons/runningshoes.png"

ITEM.Owner = "STEAM_0:0:66182830"

function ITEM:CanPlayerBuy(ply)

    if ply:SteamID() == self.Owner then
        return true
    end

    return false
end


function ITEM:OnEquip(ply)
    ply:SetNWInt("runspeed", 420)
    ply:SetNWInt("walkspeed",360)
    ply:SetJumpPower(250)
    ply.hasProtectionSuit = true
    ply.isJeremyAndWillBeManhack = true

    hook.Add("PlayerDeath", "jeremyDie",function( victim, weapon, killer )
            if ply.isJeremyAndWillBeManhack and not ply:IsGhost() and SERVER and victim == ply and #ents.FindByClass("npc_manhack") < 5 then
            pos = ply:GetPos();
            DamageLog("Manhack: " .. ply:Nick() .. " [" .. ply:GetRoleString() .. "] turned into a manhack ")


            local headturtle = self:SpawnNPC2(ply, pos, "npc_manhack")

            headturtle:SetNPCState(2)
            if  ply:GetRoleString() ~= "traitor" then
                headturtle:SetKeyValue("sk_manhack_melee_dmg", 0)
            end

            --  headturtle:SetModel("models/props_lab/cactus.mdl")

            local turtle = ents.Create("prop_dynamic")
            turtle:SetModel("models/props_lab/cactus.mdl")
            turtle:SetPos(headturtle:GetPos())
            turtle:SetAngles(Angle(0,-90,0))
            turtle:SetModelScale( turtle:GetModelScale() * 2, 0.05)
            turtle:SetParent(headturtle)
            turtle:SetHealth(90)


            --headturtle:SetCollisionGroup(COLLISION_GROUP_WEAPON)

            headturtle:SetNWEntity("Thrower", ply)
            --headturtle:SetName(self:GetThrower():GetName())
            headturtle:SetNoDraw(true)
            headturtle:SetHealth(1)
        end
    end)

end

function ITEM:OnHolster(ply)
    ply:SetNWInt("runspeed", 320)
    ply:SetNWInt("walkspeed",220)
    ply:SetJumpPower(160)
    ply.hasProtectionSuit = true
end
function ITEM:OnSell(ply)
    --ply:StripWeapon(self.WeaponClass)
end


function ITEM:SpawnNPC2( Player, Position, Class )

    local NPCList = list.Get( "NPC" )
    local NPCData = NPCList[ Class ]

    -- Don't let them spawn this entity if it isn't in our NPC Spawn list.
    -- We don't want them spawning any entity they like!
    if ( !NPCData ) then
    if ( IsValid( Player ) ) then
        Player:SendLua( "Derma_Message( \"Sorry! You can't spawn that NPC!\" )" );
    end
    return end

    local bDropToFloor = false

    --
    -- This NPC has to be spawned on a ceiling ( Barnacle )
    --
    if ( NPCData.OnCeiling && Vector( 0, 0, -1 ):Dot( Normal ) < 0.95 ) then
    return nil
    end

    if ( NPCData.NoDrop ) then bDropToFloor = false end

    --
    -- Offset the position
    --


    -- Create NPC
    local NPC = ents.Create( NPCData.Class )
    if ( !IsValid( NPC ) ) then return end

    NPC:SetPos( Position )
    --
    -- This NPC has a special model we want to define
    --
    if ( NPCData.Model ) then
        NPC:SetModel( NPCData.Model )
    end

    --
    -- Spawn Flags
    --
    local SpawnFlags = bit.bor( SF_NPC_FADE_CORPSE, SF_NPC_ALWAYSTHINK)
    if ( NPCData.SpawnFlags ) then SpawnFlags = bit.bor( SpawnFlags, NPCData.SpawnFlags ) end
    if ( NPCData.TotalSpawnFlags ) then SpawnFlags = NPCData.TotalSpawnFlags end
    NPC:SetKeyValue( "spawnflags", SpawnFlags )

    --
    -- Optional Key Values
    --
    if ( NPCData.KeyValues ) then
        for k, v in pairs( NPCData.KeyValues ) do
            NPC:SetKeyValue( k, v )
        end
    end

    --
    -- This NPC has a special skin we want to define
    --
    if ( NPCData.Skin ) then
        NPC:SetSkin( NPCData.Skin )
    end

    --
    -- What weapon should this mother be carrying
    --

    NPC:Spawn()
    NPC:Activate()

    if ( bDropToFloor && !NPCData.OnCeiling ) then
    NPC:DropToFloor()
    end

    return NPC
end