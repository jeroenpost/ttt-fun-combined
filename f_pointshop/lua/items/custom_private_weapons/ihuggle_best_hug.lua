ITEM.Name = "iHuggle's Best Hug"
ITEM.Price = 50
ITEM.Model = "models/weapons/w_pist_deagle.mdl"
ITEM.WeaponClass = 'custom_ihuggle_best_hug'

ITEM.Owner =  	"STEAM_0:1:17001489"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end

function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end