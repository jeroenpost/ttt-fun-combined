ITEM.Name = "Private's Shield of Protection"
ITEM.Price = 50
ITEM.Material  = 'camos/custom_camo16'
ITEM.WeaponClass = 'custom_bong_bubbles'

ITEM.Owner =  	"STEAM_0:0:52862820"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end

function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end