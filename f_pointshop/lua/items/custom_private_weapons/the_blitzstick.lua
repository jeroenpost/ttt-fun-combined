ITEM.Name = "The Blitzstick"
ITEM.Price = 60
ITEM.Model = "models/weapons/w_stunbaton.mdl"
ITEM.WeaponClass = 'custom_blitzstick'
ITEM.Owner = "STEAM_0:0:50750904"

function ITEM:CanPlayerBuy(ply)

    if ply:SteamID() == self.Owner then
        return true
    end

    return false
end


function ITEM:OnEquip(ply)

    ply:Give(self.WeaponClass)
    ply:SelectWeapon(self.WeaponClass)
    ply:SetNWInt("runspeed", 400)
    ply:SetNWInt("walkspeed",250)
    ply:SetJumpPower(250)


    timer.Create("checkWeawonblitzstick",5,0,function()
        if IsValid(ply) and not ply:HasWeapon(self.WeaponClass) then
            ply:SetNWInt("runspeed", 400)
            ply:SetNWInt("walkspeed",250)
            ply:SetJumpPower(250)
        end
    end)

end

function ITEM:OnSell(ply)
    ply:StripWeapon(self.WeaponClass)
end 