ITEM.Name = "inSANE medic"
ITEM.Price = 50
ITEM.Model = "models/morrowind/daedric/shortsword/w_daedric_shortsword.mdl"
ITEM.WeaponClass = 'custom_insane_medic'

ITEM.Owner =  	"STEAM_0:0:41259488"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end

function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end