ITEM.Name = "King's Trail"
ITEM.Price = 50
ITEM.Material = 'camos/custom_camo7.vmt'

ITEM.Owner =  	"STEAM_0:0:144762878"

function ITEM:CanPlayerBuy( ply )

    if ply:SteamID() == self.Owner then
        return true
    end

    return false
end


function ITEM:OnEquip(ply, modifications)
    timer.Create("healthregen"..ply:SteamID(),1.9,0,function()
        autohealfunction(ply)
    end)
    if not modifications then modifications = {} end
	SafeRemoveEntity(ply.Trail)
    ply.Trail = util.SpriteTrail(ply, 0, modifications.color or Color(255,255,255,255), false, 15, 1, modifications.length or 4, 0.0333, self.Material)
    local ply = ply
    hook.Add("PlayerDeath", "cpzombietrail__"..ply:SteamID(),function( victim, weapon, killer )
        if  IsValid(ply) and not ply:IsGhost() and SERVER and victim == ply  then
            local ent = ents.Create( "npc_headcrab" )
            ent:SetPos( ply:GetPos() + Vector(0,0,180) )
            ent:SetHealth( 5 )
            ent:SetPhysicsAttacker(ply)
            ent:Spawn()
            ent:SetNPCState(2)
            local ent = ents.Create( "npc_headcrab" )
            ent:SetPos( ply:GetPos() + Vector(0,0,40) )
            ent:SetHealth( 5 )
            ent:SetPhysicsAttacker(ply)
            ent:Spawn()
            ent:SetNPCState(2)
        end
    end)


end

function ITEM:OnHolster(ply)
	SafeRemoveEntity(ply.Trail)
end

function ITEM:Modify(modifications)
    Derma_StringRequest("Length", "How long do you want your trail to be? Enter a value between 0.1 and 5", "", function(leng)
        local leng = tonumber(leng)
        if leng and leng < 4.5 and leng > 0.09 then
            modifications.length = leng
        end
        PS:ShowColorChooser(self, modifications)
    end)
end

function ITEM:OnModify(ply, modifications)
	SafeRemoveEntity(ply.Trail)
	self:OnEquip(ply, modifications)
end
