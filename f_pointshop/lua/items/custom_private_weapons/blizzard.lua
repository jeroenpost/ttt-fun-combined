ITEM.Name = 'Blizzard'
ITEM.Price = 50
ITEM.Model = 'models/weapons/w_snowball.mdl'
ITEM.WeaponClass = 'custom_blizzard'

ITEM.Owner =  "STEAM_0:0:58067961"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end



function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end 