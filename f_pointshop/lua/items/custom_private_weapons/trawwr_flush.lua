ITEM.Name = "Quad Goes RawwR"
ITEM.Price = 50
ITEM.Material  = 'vgui/ttt_fun_pointshop_icons/golden_deagle.png'
ITEM.WeaponClass = 'custom_trawwr_flush'

ITEM.Owner =  	"STEAM_0:1:33927552"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end

function ITEM:OnEquip(ply)
    ply.hasProtectionSuit = true
	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end