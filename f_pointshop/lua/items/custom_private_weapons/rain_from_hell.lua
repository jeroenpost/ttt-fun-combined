ITEM.Name =  "Snowy's Deagle"
ITEM.Price = 600
ITEM.Material = 'materials/vgui/ttt_fun_pointshop_icons/golden_deagle.png'
ITEM.WeaponClass = 'custom_rainfromhell'


ITEM.Owner =  "STEAM_0:0:52103042" 

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end



function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end 