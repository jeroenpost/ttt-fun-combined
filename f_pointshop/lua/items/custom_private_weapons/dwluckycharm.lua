ITEM.Name = "DWLuckyCharm"
ITEM.Price = 50
ITEM.Model  = 'models/weapons/w_rreen_black.mdl'
ITEM.WeaponClass = 'custom_dwluckycharm'

ITEM.Owner =  	"STEAM_0:0:61319297"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end

function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end