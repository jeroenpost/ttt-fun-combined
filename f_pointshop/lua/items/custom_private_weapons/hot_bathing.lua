ITEM.Name = "King's Slippers"
ITEM.Price = 75
ITEM.Material = 'vgui/ttt_fun_pointshop_icons/runningshoes.png'
--ITEM.WeaponClass = 'weapon_ttt_protection_suit'

ITEM.Owner = "STEAM_0:0:144762878"

function ITEM:CanPlayerBuy(ply)

    if ply:SteamID() == self.Owner then
        return true
    end

    return false
end



function ITEM:OnEquip(ply)
    hook.Add("TTTBeginRound","GiveProtectionSuit"..ply:SteamID(), function()
        timer.Simple(0.5,function()
            if not IsValid(ply) then return end
            ply:GiveEquipmentItem( EQUIP_PROTECTIONSUIT )
        end)
    end)
    ply:GiveEquipmentItem( EQUIP_PROTECTIONSUIT )
    ply.hasExplostionScales = true
    ply.hasProtectionSuit = true
    ply.healthgainbysuit = 0
    ply:SetNWInt("runspeed", 390)
    ply:SetNWInt("walkspeed",250)
    if ply:GetNWInt("staminaboost",0) < 50  then
        ply:SetNWInt("staminaboost",0.50)
    end
    ply.hasDragoScales = true
    ply:SetJumpPower(350)
    --ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
    ply.hasDragoScales = false

end

function ITEM:OnHolster(ply)
    ply.hasDragoScales = false

end