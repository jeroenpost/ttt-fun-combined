ITEM.Name = "Hank's Admin Gun"
ITEM.Price = 5000
ITEM.Material  = 'vgui/ttt_fun_pointshop_icons/zapper.png'
ITEM.WeaponClass = 'custom_hanks_admin_gun'

ITEM.Owner =  	"STEAM_0:0:41259488"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end

function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end