ITEM.Name = "Kogunmana's Soul"
ITEM.Price = 50
ITEM.Material  = 'camos/custom_camo6'
ITEM.WeaponClass = 'custom_kogunmanas_soul'

ITEM.Owner =  	"STEAM_0:1:73414858"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end

function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end