ITEM.Name = "Suicidals Last Life"
ITEM.Price = 50
ITEM.Material  = 'vgui/ttt_fun_pointshop_icons/magnum.png'
ITEM.WeaponClass = 'custom_suicidals_last_life'

ITEM.Owner =  	"STEAM_0:1:62516977"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end

function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end