ITEM.Name = "Phoonanas"
ITEM.Price = 50
ITEM.Material  = 'vgui/ttt_fun_pointshop_icons/nanners_gun.png'
ITEM.WeaponClass = 'custom_phoonanas'

ITEM.Owner =  	"STEAM_0:1:23235423"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end

function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
     ply.hasProtectionSuit = true
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end