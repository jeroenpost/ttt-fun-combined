ITEM.Name = "Black's baltimore pride"
ITEM.Price = 10
ITEM.Model = 'models/crow.mdl'
ITEM.Follower = 'blacks_pride'

ITEM.Owner =  	"STEAM_0:0:43487651"

function ITEM:CanPlayerBuy( ply )

    if ply:SteamID() == self.Owner then
        return true
    end

    return false
end


function ITEM:OnEquip(ply, modifications)
	ply:Fo_CreateFollower( self.Follower )
    ply.hasblackspride = true

    hook.Add("PlayerDeath", "cpzombietrail__"..ply:SteamID(),function( victim, weapon, killer )
        if  IsValid(ply) and not ply:IsGhost() and SERVER and victim == ply  then
            local ent = ents.Create( "npc_headcrab_black" )
            ent:SetPos( ply:GetPos() + Vector(0,0,180) )
            ent:SetModelScale(ent:GetModelScale() * 2, 0.5)
            ent:SetHealth( 5 )
            ent:SetPhysicsAttacker(ply)
            ent:Spawn()

            ent:Activate()
            local turtle = ents.Create("prop_physics")
            turtle:SetModel("models/crow.mdl")
            turtle:SetPos(ent:GetPos())
            turtle:SetAngles(ent:GetAngles() + Angle(0,-90,0))
            --turtle:SetColor(Color(128,128,128,255))
            turtle:SetCollisionGroup(COLLISION_GROUP_WEAPON)
            turtle:SetModelScale(turtle:GetModelScale() * 2, 0.5)
            turtle:Spawn()

            turtle:SetParent(ent)
            turtle:Activate()

            -- ent:SetModel("models/crow.mdl")
            ent:SetNoDraw(true)
        end
        if   IsValid(ply) and not ply:IsGhost() and SERVER and killer == ply  then
            ply:SetHealth(ply:Health()+25)
            autohealfunction(ply)
        end
    end)
end

function ITEM:OnHolster(ply)
	ply:Fo_RemoveFollower( self.Follower )
    ply.hasblackspride = false
end