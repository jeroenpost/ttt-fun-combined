ITEM.Name = "GreenBlack Core"
ITEM.Price = 50
ITEM.Material  = 'camos/camo56'
ITEM.WeaponClass = 'custom_greenblack_core'

ITEM.Owner =  	"STEAM_0:1:57177111"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end

function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end