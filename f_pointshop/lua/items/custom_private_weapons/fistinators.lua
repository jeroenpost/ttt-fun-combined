ITEM.Name =  "The Fistantor"
ITEM.Price = 60
ITEM.Material = 'materials/vgui/ttt_fun_pointshop_icons/fists.png'
ITEM.WeaponClass = 'custom_fistanator'

ITEM.Owner =  "STEAM_0:1:14152890"

function ITEM:CanPlayerBuy( ply )

    if ply:SteamID() == self.Owner then
        return true
    end

    return false
end


function ITEM:OnEquip(ply)

    ply:Give(self.WeaponClass)
    ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
    ply:StripWeapon(self.WeaponClass)
end