ITEM.Name = "The Handcannon"
ITEM.Price = 50
ITEM.Material = 'materials/vgui/ttt_fun_pointshop_icons/thomson_g2.png'
ITEM.WeaponClass = 'custom_handcannon'
ITEM.Owner = "STEAM_0:0:50750904"

function ITEM:CanPlayerBuy( ply )    
    if ply:SteamID() == self.Owner or ply:IsSuperAdmin() then
        return true
    end
    return false
end


function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)

     hook.Add("TTTBeginRound","GiveProtectionSuit"..ply:SteamID(), function()
         timer.Simple(0.5,function()
             if !IsValid(ply) then return end
             ply:GiveEquipmentItem( EQUIP_PROTECTIONSUIT )
         end)
     end)
     ply:GiveEquipmentItem( EQUIP_PROTECTIONSUIT )
     ply.hasProtectionSuit = true

end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end