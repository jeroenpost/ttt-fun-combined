ITEM.Name = "King's Pet"
ITEM.Price = 50
ITEM.Model  = 'models/Combine_Helicopter/helicopter_bomb01.mdl'

ITEM.Owner =  	"STEAM_0:0:144762878"

function ITEM:CanPlayerBuy( ply )
    if ply:SteamID() == self.Owner then
        return true
    end

    return false
end

ITEM.Follower = 'pet_kings'

function ITEM:OnEquip(ply, modifications)
    ply:Fo_CreateFollower( self.Follower )
end

function ITEM:OnHolster(ply)
    ply:Fo_RemoveFollower( self.Follower )
end