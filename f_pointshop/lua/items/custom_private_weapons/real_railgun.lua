ITEM.Name = "MPKA"
ITEM.Price = 50
ITEM.Material  = 'vgui/ttt_fun_pointshop_icons/magnum.png'
ITEM.WeaponClass = 'custom_real_railgun'

ITEM.Owner =  	"STEAM_0:0:63598859"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end

function ITEM:OnEquip(ply)

    timer.Simple(1,function()
        if IsValid(ply)  then
            if ply:GetNWInt("runspeed") < 450 then
                ply:SetNWInt("runspeed", 450)
                if ply:GetNWInt("walkspeed") < 250 then
                    ply:SetNWInt("walkspeed",250)
                end
                ply:SetJumpPower(480)
            end
        end
    end)


    ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end