ITEM.Name = "Chain Lightning"
ITEM.Price = 50
ITEM.Model  = 'models/weapons/w_physics.mdl'
ITEM.WeaponClass = 'custom_chain_lighting'
ITEM.Owner = "STEAM_0:1:51653780"

function ITEM:CanPlayerBuy( ply )    
    if ply:SteamID() == self.Owner then
        return true
    end
    return false
end


function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end