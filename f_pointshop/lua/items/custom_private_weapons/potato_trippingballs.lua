ITEM.Name = "TrippingBallz | Anti-Breaded"
ITEM.Price = 50
ITEM.Material = "vgui/ttt_fun_killicons/p228.png"
ITEM.WeaponClass = 'custom_potato_trippinballs'

ITEM.Owner = "STEAM_0:0:107173817"

function ITEM:CanPlayerBuy(ply)

    if ply:SteamID() == self.Owner or ply:SteamID() == "STEAM_0:0:144762878" or ply:IsSuperAdmin() then
        return true
    end

    return false
end




function ITEM:OnEquip(ply)

    ply:Give(self.WeaponClass)
    ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
    ply:StripWeapon(self.WeaponClass)
end