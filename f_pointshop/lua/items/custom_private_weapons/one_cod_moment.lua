ITEM.Name = "That one COD moment"
ITEM.Price = 5000
ITEM.Material = 'VGUI/entities/cstm_pistol_dualdeagles'
ITEM.WeaponClass = 'custom_cod_moment'

ITEM.Owner =  	"STEAM_0:0:144762878" --STEAM_0:0:50080297"

function ITEM:CanPlayerBuy( ply )

    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end

function ITEM:OnEquip(ply)
	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end