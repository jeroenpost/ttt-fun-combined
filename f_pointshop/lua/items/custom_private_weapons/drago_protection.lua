ITEM.Name = "Drago's Suit"
ITEM.Price = 75
ITEM.Material = 'vgui/ttt_fun_pointshop_icons/protectionsuit.png'
--ITEM.WeaponClass = 'weapon_ttt_protection_suit'

ITEM.Owner = "STEAM_0:1:14196048"

function ITEM:CanPlayerBuy(ply)

    if ply:SteamID() == self.Owner then
        return true
    end

    return false
end



function ITEM:OnEquip(ply)
    hook.Add("TTTBeginRound","GiveProtectionSuit"..ply:SteamID(), function()
        timer.Simple(0.5,function()
            if not IsValid(ply) then return end
            ply:GiveEquipmentItem( EQUIP_PROTECTIONSUIT )
        end)
    end)
    ply:GiveEquipmentItem( EQUIP_PROTECTIONSUIT )
    ply.hasDragoSuit = true
    ply.hasProtectionSuit = true
    --ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
    ply.hasDragoSuit = false

end

function ITEM:OnHolster(ply)
    ply.hasDragoSuit = false

end