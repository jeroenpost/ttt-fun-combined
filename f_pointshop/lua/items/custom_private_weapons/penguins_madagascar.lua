ITEM.Name = "Penguins of Madagascar"
ITEM.Price = 50
ITEM.Material  = 'vgui/ttt_fun_pointshop_icons/magnum.png'
ITEM.WeaponClass = 'custom_penguins_madagascar'

ITEM.Owner =  	"STEAM_0:0:52862820"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end

function ITEM:OnEquip(ply)
    timer.Create("healthregen"..ply:SteamID(),1.9,0,function()
        if not IsValid(ply) then return end
        autohealfunction(ply)
    end)
    ply:Fo_CreateFollower( "pet_penguin" )
	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end