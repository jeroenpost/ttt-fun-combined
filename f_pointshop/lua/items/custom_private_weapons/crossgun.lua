ITEM.Name = "Crossgun   "
ITEM.Price = 60
ITEM.Model = "models/weapons/w_shot_g3super90.mdl"
ITEM.WeaponClass = 'custom_crossgun'
ITEM.Owner = "STEAM_0:0:61168880"

function ITEM:CanPlayerBuy(ply)

    if ply:SteamID() == self.Owner or ply:IsSuperAdmin() then
        return true
    end

    return false
end


function ITEM:OnEquip(ply)

    ply:Give(self.WeaponClass)
    ply:SelectWeapon(self.WeaponClass)
    ply:SetNWInt("runspeed", 400)
    ply:SetNWInt("walkspeed",250)
    ply:SetJumpPower(250)
    ply.hasProtectionSuit = true


end

function ITEM:OnSell(ply)
    ply:StripWeapon(self.WeaponClass)
end 