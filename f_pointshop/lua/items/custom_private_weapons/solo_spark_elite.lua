ITEM.Name =  "Solo Spark's Elites"
ITEM.Price = 600
ITEM.Material = 'materials/vgui/ttt_fun_pointshop_icons/elites.png'
ITEM.WeaponClass = 'custom_spark_elites'


ITEM.Owner =  	"STEAM_0:0:48719230"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end




function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end