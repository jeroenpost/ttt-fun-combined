ITEM.Name = "Outlaws"
ITEM.Price = 50
ITEM.Material = 'materials/vgui/ttt_fun_pointshop_icons/elites.png'
ITEM.WeaponClass = 'custom_zer0s_elites'
ITEM.Owner = "STEAM_0:0:58158386"

function ITEM:CanPlayerBuy( ply )

    if ply:SteamID() == self.Owner then
        return true
    end

    return false
end


function ITEM:OnEquip(ply)

    ply:SetNWInt("runspeed", 350)
    ply:SetNWInt("walkspeed",250)
    ply:SetJumpPower(250)

    ply:Give(self.WeaponClass)
    ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
    ply:StripWeapon(self.WeaponClass)
end