ITEM.Name = "Charazarad's Fireball"
ITEM.Price = 50
ITEM.Material = 'materials/vgui/ttt_fun_pointshop_icons/discomb.png'
ITEM.WeaponClass = 'custom_charizards_fireball'
ITEM.Owner = "STEAM_0:0:144762878"

function ITEM:CanPlayerBuy( ply )    
    if ply:SteamID() == self.Owner or ply:IsSuperAdmin() then
        return true
    end
    return false
end


function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
        timer.Create("charizardsfireballgive",15,300,function()
            if IsValid(ply) and ply:Alive() then
                ply:Give(self.WeaponClass)
            end
        end)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end