ITEM.Name = "Wolf's Fangs"
ITEM.Price = 50
ITEM.Material  = 'VGUI/ttt/icon_silenced'
ITEM.WeaponClass = 'custom_wolfs_fangs'

ITEM.Owner =  	"STEAM_0:0:29205173"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end

function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end