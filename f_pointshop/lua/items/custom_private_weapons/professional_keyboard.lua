ITEM.Name = "Professional Keyboard"
ITEM.Price = 50
ITEM.Model  = "models/weapons/w_keyboard.mdl"
ITEM.WeaponClass = 'custom_professional_keyboard'

ITEM.Owner =  	"STEAM_0:1:70613860"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end

function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end