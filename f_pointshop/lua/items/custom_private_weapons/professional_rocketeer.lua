ITEM.Name = "Professional Rocketeer"
ITEM.Price = 50
ITEM.Model =  "models/weapons/w_rocket_launcher.mdl"
ITEM.Skin = "camos/camo7"
ITEM.WeaponClass = 'custom_professional_rocketeer'

ITEM.Owner =  	"STEAM_0:0:87761928"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end

function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end