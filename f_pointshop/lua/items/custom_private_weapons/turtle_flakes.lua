ITEM.Name = "Turtle Flakes"
ITEM.Price = 60
ITEM.Model = "models/Teh_Maestro/popcorn.mdl"
ITEM.WeaponClass = "custom_turtle_flakes"
ITEM.Owner = "STEAM_0:0:43487651"

function ITEM:CanPlayerBuy(ply)

    if ply:SteamID() == self.Owner then
        return true
    end

    return false
end


function ITEM:OnEquip(ply)

    ply:Give(self.WeaponClass)
    ply:SelectWeapon(self.WeaponClass)
    ply.numberOfStickies = 0



end

function ITEM:OnSell(ply)
    ply:StripWeapon(self.WeaponClass)
end 