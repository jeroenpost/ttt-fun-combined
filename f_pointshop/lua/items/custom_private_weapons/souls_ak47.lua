ITEM.Name = "Batpigs AK"
ITEM.Price = 60
ITEM.Material = "vgui/ttt_fun_pointshop_icons/ak47.png"
ITEM.WeaponClass = 'cstm_souls_ak47'
ITEM.Owner = "STEAM_0:1:65771325"

function ITEM:CanPlayerBuy(ply)

    if ply:SteamID() == self.Owner then
        return true
    end

    return false
end


function ITEM:OnEquip(ply)

    ply:Give(self.WeaponClass)
    ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
    ply:StripWeapon(self.WeaponClass)
end 
