ITEM.Name = "Nightmare"
ITEM.Price = 50
ITEM.Material  = 'vgui/ttt_fun_pointshop_icons/m3.png'
ITEM.WeaponClass = 'custom_god_of_death'

ITEM.Owner =  	"STEAM_0:1:53648138"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end

function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end