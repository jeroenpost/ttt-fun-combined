ITEM.Name = "Godslayer"
ITEM.Price = 60
ITEM.Model = "models/weapons/w_shot_g3super90.mdl"
ITEM.WeaponClass = 'custom_god_slayer'
ITEM.Owner = "STEAM_0:0:144762878"

function ITEM:CanPlayerBuy( ply )

    ply:SetNWInt("runspeed", 350)
    ply:SetNWInt("walkspeed",250)
    ply:SetJumpPower(400)
    ply.hasProtectionSuit = true

    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end


function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end 