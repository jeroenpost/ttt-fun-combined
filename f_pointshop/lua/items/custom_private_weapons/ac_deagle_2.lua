ITEM.Name = "Dark's Death"
ITEM.Price = 5000
ITEM.Material  = 'vgui/ttt_fun_pointshop_icons/golden_deagle.png'
ITEM.WeaponClass = 'ac_deagle_2'

ITEM.Owner =  	"STEAM_0:0:53251783"

function ITEM:CanPlayerBuy( ply )

    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end

function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end