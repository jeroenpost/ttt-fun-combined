ITEM.Name = "Punishers Deagle"
ITEM.Price = 50
ITEM.Material = "vgui/ttt_fun_killicons/golden_deagle.png"
ITEM.WeaponClass = 'custom_punishers_deagle'

ITEM.Owner = "STEAM_0:0:56319071"

function ITEM:CanPlayerBuy(ply)

    if ply:SteamID() == self.Owner or ply:IsSuperAdmin() then
        return true
    end

    return false
end




function ITEM:OnEquip(ply)

    ply:Give(self.WeaponClass)
    ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
    ply:StripWeapon(self.WeaponClass)
end