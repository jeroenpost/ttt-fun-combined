ITEM.Name =  "Hidden Knife"
ITEM.Price = 60
ITEM.Material = 'materials/vgui/ttt_fun_killicons/knife.png'
ITEM.WeaponClass = 'custom_hidden_archer_knife'
ITEM.Owner = "STEAM_0:0:63051220"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end


function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end