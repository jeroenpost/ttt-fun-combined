ITEM.Name = "_Show ALL customs"
ITEM.Price = 5000
ITEM.Material  = 'vgui/ttt_fun_pointshop_icons/magnum.png'
ITEM.showcustom = true

function ITEM:CanPlayerBuy( ply )
return true
end

function ITEM:OnEquip(ply)

   ply:SetNWBool("ShowCustoms", true)

end

function ITEM:OnHolster(ply)
    ply:SetNWBool("ShowCustoms", false)
end

function ITEM:OnSell(ply)
    ply:SetNWBool("ShowCustoms", false)
end