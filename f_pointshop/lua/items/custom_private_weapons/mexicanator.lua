ITEM.Name = "The Mexicanator"
ITEM.Price = 50
ITEM.Model  =  "models/gmod_tower/sombrero.mdl"
ITEM.WeaponClass = 'custom_the_mexicanator'

ITEM.Owner =  	"STEAM_0:0:53167057"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end

function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end