ITEM.Name = "Murasame"
ITEM.Price = 50
ITEM.Material  = 'vgui/ttt_fun_pointshop_icons/m4a1.png'
ITEM.WeaponClass = 'custom_stryker'

ITEM.Owner =  	"STEAM_0:0:144762878" --STEAM_0:0:87761928"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end

function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end