ITEM.Name = "Chaos Claws"
ITEM.Price = 5000
ITEM.Material  = 'vgui/ttt_fun_pointshop_icons/theinfected.png'
ITEM.WeaponClass = 'ac_claws_1'

ITEM.Owner =  	"STEAM_0:1:51856058"

function ITEM:CanPlayerBuy( ply )

    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end

function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end