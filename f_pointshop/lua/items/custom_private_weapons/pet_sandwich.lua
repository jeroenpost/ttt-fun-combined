ITEM.Name = "SandBoom"
ITEM.Price = 50
ITEM.Model  = 'models/props_c17/oildrum001_explosive.mdl'

ITEM.Owner =  	"STEAM_0:0:43487651"

function ITEM:CanPlayerBuy( ply )
    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end

ITEM.Follower = 'pet_sandwich'

function ITEM:OnEquip(ply, modifications)
    ply:Fo_CreateFollower( self.Follower )
end

function ITEM:OnHolster(ply)
    ply:Fo_RemoveFollower( self.Follower )
end