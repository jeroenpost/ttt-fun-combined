ITEM.Name = "Alice and Alexia"
ITEM.Price = 50
ITEM.Material  = 'vgui/ttt_fun_pointshop_icons/elites.png'
ITEM.WeaponClass = 'custom_alice_alexia'

ITEM.Owner =  	"STEAM_0:0:41259488"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end

function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end