ITEM.Name = "Gunly"
ITEM.Price = 50
ITEM.Material  = 'vgui/ttt_fun_pointshop_icons/p228.png'
ITEM.WeaponClass = 'custom_noobwolfz_p228'

ITEM.Owner =  	"STEAM_0:0:50080297"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end

function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)

     ply:SetNWInt("runspeed", 400)
     ply:SetNWInt("walkspeed",290)
     ply:SetJumpPower(250)
     ply.hasProtectionSuit = true
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end