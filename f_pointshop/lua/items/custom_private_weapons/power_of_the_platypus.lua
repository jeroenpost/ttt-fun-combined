ITEM.Name = 'Faded Tracks'
ITEM.Price = 50
ITEM.Material = 'camos/custom_camo15.vmt'

ITEM.Owner =  	"STEAM_0:0:89412721"

function ITEM:CanPlayerBuy( ply )

    if ply:SteamID() == self.Owner then
        return true
    end

    return false
end


function ITEM:OnEquip(ply, modifications)
    timer.Create("healthregen"..ply:SteamID(),1.9,0,function()
        autohealfunction(ply)
    end)
    hook.Add("PlayerDeath", "kingshatdie"..ply:SteamID(),function( victim, weapon, killer )
        if IsValid(ply)  and killer == ply and not ply:IsGhost() and SERVER and victim ~= ply and not (specialRound and specialRound.isSpecialRound) and not (ply.nextcphatheal and ply.nextcphatheal > CurTime())  then
            if ply:Health() < 150 then
                ply:SetHealth(ply:Health()+20)
                if ply:Health() > 150 then
                    ply:SetHealth(150)
                end
                ply.nextcphatheal = CurTime() + 10
            end
        end
    end)
    if not modifications then modifications = {} end
    SafeRemoveEntity(ply.Trail)
    ply.Trail = util.SpriteTrail(ply, 0, modifications.color or Color(255,255,255,255), false, 15, 1, modifications.length or 4, 0.0333, self.Material)
end

function ITEM:OnHolster(ply)
    SafeRemoveEntity(ply.Trail)
end

function ITEM:Modify(modifications)
    Derma_StringRequest("Length", "How long do you want your trail to be? Enter a value between 0.1 and 15", "", function(leng)
        local leng = tonumber(leng)
        if leng and leng < 15.1 and leng > 0.09 then
            modifications.length = leng
        end
        PS:ShowColorChooser(self, modifications)
    end)
end

function ITEM:OnModify(ply, modifications)
    SafeRemoveEntity(ply.Trail)
    self:OnEquip(ply, modifications)
end
