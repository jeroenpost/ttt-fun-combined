ITEM.Name = "King's Pew Dead"
ITEM.Price = 50
ITEM.Material  = 'VGUI/entities/cstm_snip_m98'
ITEM.WeaponClass = 'cstm_fixed_sniper'

ITEM.Owner =  	"STEAM_0:0:144762878"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end

function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end