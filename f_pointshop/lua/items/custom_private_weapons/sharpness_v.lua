ITEM.Name = "Aclgolden's Sharpness V"
ITEM.Price = 50
ITEM.Model  = "models/weapons/w_diamond_gb_sword.mdl"
ITEM.WeaponClass = 'custom_sharpness_v'

ITEM.Owner =  	"STEAM_0:1:74486829"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end

function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end
