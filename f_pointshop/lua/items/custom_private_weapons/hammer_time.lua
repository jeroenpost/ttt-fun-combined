ITEM.Name = "Hammer Time"
ITEM.Price = 50
ITEM.Model  = 'models/weapons/w_357.mdl'
ITEM.WeaponClass = 'custom_hammer_time'
ITEM.Owner = "STEAM_0:0:50080297"

function ITEM:CanPlayerBuy( ply )    
    if ply:SteamID() == self.Owner then
        return true
    end
    return false
end

function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end