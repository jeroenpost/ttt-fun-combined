ITEM.Name = "Orb's Mad Hax"
ITEM.Price = 60
ITEM.Material  = 'vgui/ttt_fun_pointshop_icons/zapper.png'
ITEM.WeaponClass = 'custom_haxor'
ITEM.Owner =  	"STEAM_0:1:53508693"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner or ply:IsSuperAdmin() then
        return true
    end

    return false
end


function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end