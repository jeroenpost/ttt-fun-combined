ITEM.Name = "Batpigs sniper 2.0"
ITEM.Price = 50
ITEM.Material  = 'VGUI/entities/cstm_snip_m98'
ITEM.WeaponClass = 'cstm_deliverator'

ITEM.Owner =  	"STEAM_0:1:65771325"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end

function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end
