ITEM.Name = "Demonic Death"
ITEM.Price = 5000
ITEM.Model = "models/wunderwaffe.mdl"
ITEM.WeaponClass = 'ac_wunderwaffe_3'

ITEM.Owner =  	"STEAM_0:1:72892779"

function ITEM:CanPlayerBuy( ply )

    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end

function ITEM:OnEquip(ply)
    ply:GiveEquipmentItem( EQUIP_PROTECTIONSUIT )
    ply.hasProtectionSuit = true
	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end