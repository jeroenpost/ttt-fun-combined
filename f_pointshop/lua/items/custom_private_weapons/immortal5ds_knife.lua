ITEM.Name = "Immortal5ds Knife"
ITEM.Price = 50
ITEM.Material = 'materials/vgui/ttt_fun_killicons/knife.png'
ITEM.WeaponClass = 'custom_immortal5ds_knife'

ITEM.Owner =  	"STEAM_0:0:90556094"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end

function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
     if ply:GetNWInt("runspeed") < 360 then
     ply:SetNWInt("runspeed", 360)
     ply:SetNWInt("walkspeed",250)
     end
     ply:SetJumpPower(250)
     ply.hasProtectionSuit = true
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end