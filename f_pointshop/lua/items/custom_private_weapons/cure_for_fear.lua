ITEM.Name =  "The Cure for Fear"
ITEM.Price = 60
ITEM.Material  =  'materials/vgui/ttt_fun_pointshop_icons/tmp.png'
ITEM.WeaponClass = 'custom_cure_for_fear'
 

ITEM.Owner = "STEAM_0:0:29205173"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end


function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end