ITEM.Name = "GreenBlack MKII"
ITEM.Price = 50
ITEM.Material  = 'vgui/ttt_fun_pointshop_icons/green_black.png'
ITEM.WeaponClass = 'custom_greenblack_mkii'

ITEM.Owner =  	"STEAM_0:0:40394891"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end

function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end