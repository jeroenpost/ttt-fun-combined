ITEM.Name = "Kowalski's Escape Plan"
ITEM.Price = 50
ITEM.Material  = 'vgui/ttt_fun_pointshop_icons/spidergun.png'
ITEM.WeaponClass = 'custom_professional_escaper'

ITEM.Owner =  	"STEAM_0:0:52862820" --STEAM_0:0:87761928"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end

function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end