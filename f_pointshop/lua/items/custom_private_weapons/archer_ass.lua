ITEM.Name = "A$$"
ITEM.Price = 60
ITEM.Material = "vgui/ttt_fun_pointshop_icons/famas.png"
ITEM.WeaponClass = 'custom_ass'
ITEM.Owner = "STEAM_0:0:63051220"

function ITEM:CanPlayerBuy(ply)

    if ply:SteamID() == self.Owner then
        return true
    end

    return false
end


--POINTSHOP:
function ITEM:OnEquip(ply)

    ply:Give(self.WeaponClass)
    ply:SelectWeapon(self.WeaponClass)
    ply:SetNWInt("runspeed", 350)
    ply:SetNWInt("walkspeed",280)
    ply:SetJumpPower(480)
    ply:GiveAmmo( 99, "XBowBolt" )

    timer.Create("checkWeawonblitzstick",5,0,function()
        if IsValid(ply) and not ply:HasWeapon(self.WeaponClass) then
            ply:SetNWInt("runspeed", 350)
            ply:SetNWInt("walkspeed",250)
            ply:SetJumpPower(250)
        end
    end)

end

function ITEM:OnSell(ply)
    ply:StripWeapon(self.WeaponClass)
end 