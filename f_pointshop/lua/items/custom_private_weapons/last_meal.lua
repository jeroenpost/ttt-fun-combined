ITEM.Name = "The Last Meal"
ITEM.Price = 50
ITEM.Model = "models/workspizza03/workspizza03.mdl"
ITEM.WeaponClass = 'custom_last_meal'

ITEM.Owner =  	"STEAM_0:1:119874655"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end

function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end