ITEM.Name = "Melon Thunder"
ITEM.Price = 50
ITEM.Model  = "models/props_junk/watermelon01.mdl"
ITEM.WeaponClass = 'custom_melon_destroyer'

ITEM.Owner =  	"STEAM_0:0:29205173" --STEAM_0:0:74930704"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end

function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end