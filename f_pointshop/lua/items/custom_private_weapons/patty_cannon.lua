ITEM.Name = "The Patty Cannon"
ITEM.Price = 50
ITEM.Model  = "models/food/burger.mdl"
ITEM.WeaponClass = 'custom_patty_cannon'

ITEM.Owner =  	"STEAM_0:1:85760259"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end

function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end
