ITEM.Name = "HeadShot Boom"
ITEM.Price = 50
ITEM.Material  = 'vgui/ttt_fun_pointshop_icons/scout.png'
ITEM.WeaponClass = 'custom_headshot_boom'

ITEM.Owner =  	"STEAM_0:0:87761928"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end

function ITEM:OnEquip(ply)
    ply:Give(self.WeaponClass)
    ply:SelectWeapon(self.WeaponClass)

    ply:SetNWInt("runspeed", 450)
    ply:SetNWInt("walkspeed",280)
    ply:SetJumpPower(250)


end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end