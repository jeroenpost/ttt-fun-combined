ITEM.Name = "Toxics MP7"
ITEM.Price = 50
ITEM.Material = 'materials/vgui/ttt_fun_pointshop_icons/mp7.png'
ITEM.WeaponClass = 'custom_toxics_mp7'



ITEM.Owner =  	"STEAM_0:1:56862166"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end



function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end