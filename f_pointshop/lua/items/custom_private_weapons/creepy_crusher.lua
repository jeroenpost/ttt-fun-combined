ITEM.Name = "The Creepy Crusher"
ITEM.Price = 50
ITEM.Model = "models/weapons/w_shot_francspas.mdl"
ITEM.WeaponClass = 'cstm_creepy_cruster'

ITEM.Owner =  	"STEAM_0:0:52852510"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end

function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end