ITEM.Name =  "Potato's Destroyer"
ITEM.Price = 60
ITEM.Material = 'vgui/ttt_fun_pointshop_icons/sg552.png'
ITEM.WeaponClass = 'custom_potatos_destroyer'


ITEM.Owner = "STEAM_0:1:27887237"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end



function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end