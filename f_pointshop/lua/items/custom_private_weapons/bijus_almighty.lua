ITEM.Name = "Orb's UMP"
ITEM.Price = 50
ITEM.Material  = "vgui/ttt_fun_killicons/ump45.png"
ITEM.WeaponClass = 'custom_bijus_almighty_push'

ITEM.Owner =  	"STEAM_0:1:53508693"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end

function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end