ITEM.Name = "Death's Fur"
ITEM.Price = 50
ITEM.Model = 'models/player/leet.mdl'
ITEM.Skin = 'camos/custom_camo26'
ITEM.IsFur = true
ITEM.Owner =  	'STEAM_0:0:144762878'

function ITEM:CanPlayerBuy( ply )

    if ply:SteamID() == self.Owner then
        return true
    end

    return false
end


function ITEM:OnEquip(ply, modifications)
    ply:SetMaterial( self.Skin )
    timer.Simple(5,function() if IsValid(ply) then ply:SetMaterial( self.Skin )
    end end);

    if ply:GetNWInt("runspeed") < 480 then
        ply:SetNWInt("runspeed", 480)
    end
    if ply:GetNWInt("walkspeed") < 250 then
        ply:SetNWInt("walkspeed", 250)
    end

end

function ITEM:OnHolster(ply)
    ply:SetMaterial( "" )
end

