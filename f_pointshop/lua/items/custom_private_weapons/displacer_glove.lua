ITEM.Name =  "Displacer Glove"
ITEM.Price = 60
ITEM.Material = 'vgui/ttt_fun_pointshop_icons/fists.png'
ITEM.WeaponClass = 'custom_displacer_glove'


ITEM.Owner =  	"STEAM_0:0:29205173"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end



function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)

     ply:SetJumpPower(500)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end