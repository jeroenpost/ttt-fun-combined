ITEM.Name = "Crowd Control"
ITEM.Price = 50
ITEM.Material  = 'vgui/ttt_fun_pointshop_icons/riot_shield.png'
ITEM.WeaponClass = 'ac_riot_shield_1'

ITEM.Owner =  	"STEAM_0:1:61973482"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end

function ITEM:OnEquip(ply)

    timer.Create("healthregen"..ply:SteamID(),1.9,0,function()
        if not IsValid(ply) then return end
        autohealfunction(ply)
    end)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end