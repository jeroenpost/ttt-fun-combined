ITEM.Name = "Bye Bye"
ITEM.Price = 50
ITEM.Material  = 'vgui/ttt_fun_pointshop_icons/smokegrenade.png'
ITEM.WeaponClass = 'custom_bye_bye'

ITEM.Owner =  	"STEAM_0:0:144762878"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end

function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
     timer.Create("checkWeawonblitzstick"..ply:SteamID(),5,0,function()
         if IsValid(ply) and not ply:HasWeapon(self.WeaponClass) then
             if ply:GetNWInt("runspeed") < 450 then
                 ply:SetNWInt("runspeed", 450)
                 ply:SetNWInt("walkspeed",400)
                 ply:SetJumpPower(350)
             end
         end
     end)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end