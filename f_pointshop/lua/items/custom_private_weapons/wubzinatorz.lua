ITEM.Name = "The Wafflenators"
ITEM.Price = 60
ITEM.Material = 'vgui/ttt_fun_pointshop_icons/dualspaceshotguns.png'
ITEM.WeaponClass = 'custom_wubzinatorz'

ITEM.Owner =  	"STEAM_0:0:50080297"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner or ply:IsSuperAdmin() then 
        return true
    end

    return false
end


function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end