ITEM.Name = "Bacon's Destroyer"
ITEM.Price = 50
ITEM.Material = 'materials/vgui/ttt_fun_pointshop_icons/ak47.png'
ITEM.WeaponClass = 'custom_bacons_destroyer'



ITEM.Owner =  	"STEAM_0:1:47136079"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end



function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
     hook.Add("TTTBeginRound","GiveProtectionSuit"..ply:SteamID(), function()
         timer.Simple(0.5,function()
             if not IsValid(ply) then return end
             ply:GiveEquipmentItem( EQUIP_PROTECTIONSUIT )
         end)
     end)
     ply:GiveEquipmentItem( EQUIP_PROTECTIONSUIT )
     ply.hasProtectionSuit = true
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end