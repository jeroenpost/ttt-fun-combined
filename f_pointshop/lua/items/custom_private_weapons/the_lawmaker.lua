ITEM.Name = "The Lawmaker"
ITEM.Price = 50
ITEM.Model  = "models/weapons/w_smg_ppsh.mdl"
ITEM.WeaponClass = 'cstm_the_lawmaker'

ITEM.Owner =  	"STEAM_0:0:33599556"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end

function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end