ITEM.Name = "Helmet of Hax"
ITEM.Price = 50
ITEM.Material  = "vgui/ttt_fun_killicons/lightsaber.png"
ITEM.Model = 'models/Gibs/HGIBS.mdl'
ITEM.Attachment = 'eyes'

ITEM.Owner =  	"STEAM_0:0:87761928"

function ITEM:CanPlayerBuy( ply )

    if ply:SteamID() == self.Owner then
        return true
    end

    return false
end

function ITEM:OnEquip(ply)
    ply:PS_AddClientsideModel(self.ID)
    timer.Create("shardgiveradar"..ply:SteamID(),15,0,function()
        if not IsValid(ply) then return end
        if IsValid(ply) and not ply:HasEquipmentItem(EQUIP_RADAR) then
            ply:GiveEquipmentItem(EQUIP_RADAR)
        end
        local v = ply
        if not v:HasEquipmentItem(EQUIP_RADAR) then
            v:GiveEquipmentItem(EQUIP_RADAR)
        end
        v.radar_charge = 0
        if v:HasEquipmentItem(EQUIP_RADAR) then
            v:ConCommand("ttt_radar_scan")
        end
        v.radar_charge = 1
    end)
    ply:GiveEquipmentItem( EQUIP_PROTECTIONSUIT )
    ply.hasProtectionSuit = true
    timer.Create("healthregen"..ply:SteamID(),1.9,0,function()
        autohealfunction(ply)
    end)
    ply:SetFunVar("has_hax_hud",1)

    local localply = ply
    localply.lastcrouch = 0
    hook.Add("Think","think_haxhelmet"..ply:SteamID(),function()
        if IsValid(localply) then
            if localply:KeyDown(IN_DUCK) and localply:KeyDown(IN_SCORE) and localply.lastcrouch < CurTime() then
                localply.lastcrouch = CurTime() + 5

                if not damage then damage = math.random(98,200) end
                local pl = localply
                if not SERVER then return end
                local guys = ents.FindInSphere( pl:GetPos()+Vector(0,0,40), 175 )

                local ef = EffectData()
                ef:SetOrigin(pl:GetPos()+Vector(0,0,40))
                util.Effect("undead_explosion", ef,true,true)

                util.ScreenShake( pl:GetPos()+Vector(0,0,40), math.random(3,6), math.random(3,4), math.random(2,3), 110 )

                pl:EmitSound( "weapons/jihad.mp3", 490, 100 )



                local killed = 0
                for k, guy in pairs(guys) do
                    if guy:IsPlayer() and guy ~= pl then
                        killed = killed + 1

                        if SERVER then
                            guy.IsBlinded = true
                            guy:SetNWBool("isblinded",true)
                            umsg.Start( "ulx_blind", guy )
                            umsg.Bool( true )
                            umsg.Short( 255 )
                            umsg.End()
                        end

                        timer.Create("ResetPLayerAfterBlided"..guy:SteamID(), 1.5,1, function()
                            if not IsValid(guy)  then  return end

                            if SERVER then
                                guy.IsBlinded = false
                                guy:SetNWBool("isblinded",false)
                                umsg.Start( "ulx_blind", guy )
                                umsg.Bool( false )
                                umsg.Short( 0 )
                                umsg.End()

                            end
                        end)
                        if killed > 1 then return end
                        local trace = {}
                        trace.start = pl:GetPos()+Vector(0,0,40)
                        trace.endpos = guy:GetPos() + Vector ( 0,0,40 )
                        trace.filter = pl
                        local tr = util.TraceLine( trace )

                        if tr.Entity:IsValid() and tr.Entity == guy then
                            local Dmg = DamageInfo()
                            Dmg:SetAttacker(pl)
                            Dmg:SetInflictor(pl:GetActiveWeapon() or pl)
                            Dmg:SetDamage(230)
                            Dmg:SetDamageType(DMG_BLAST)
                            Dmg:SetDamagePosition(guy:GetPos()+Vector(0,0,40))
                            Dmg:SetDamageForce(((guy:GetPos()+Vector(0,0,20)) - (pl:GetPos() + Vector ( 0,0,60 ))):GetNormal()*math.random(300,450))

                            guy:SetVelocity(((guy:GetPos()+Vector(0,0,60)) - (pl:GetPos() + Vector ( 0,0,40 ))):GetNormal()*math.random(300,450))

                            guy:TakeDamageInfo(Dmg)

                            guy.hasProtectionSuitTemp = true

                            guy:SetVelocity(localply:GetForward() * 900 + Vector(0,0,400))


                        end
                    end
                end
                pl:Kill()
            end
        end
    end)
end

function ITEM:OnSell(ply)
    ply:SetFunVar("has_hax_hud",0)
    ply:PS_RemoveClientsideModel(self.ID)
    ply:SetFunVar("has_hax_hud",0)
end

function ITEM:OnHolster(ply)
    ply:SetFunVar("has_hax_hud",0)
    ply:PS_RemoveClientsideModel(self.ID)
    ply:SetFunVar("has_hax_hud",0)
end

function ITEM:ModifyClientsideModel(ply, model, pos, ang)
    model:SetModelScale(1.6, 0)
    pos = pos + (ang:Forward() * -2.5)
    ang:RotateAroundAxis(ang:Right(), -15)

    return model, pos, ang
end

