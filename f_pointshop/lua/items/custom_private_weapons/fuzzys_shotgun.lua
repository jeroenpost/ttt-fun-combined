ITEM.Name = "Fuzzy's Rigged Shotgun"
ITEM.Price = 50
ITEM.Material = 'materials/vgui/ttt_fun_pointshop_icons/m3.png'
ITEM.WeaponClass = 'custom_fuzzy_shotgun'

ITEM.Owner = "STEAM_0:0:37766519"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end



function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end 

