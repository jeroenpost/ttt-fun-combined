ITEM.Name = "Drakes Talons"
ITEM.Price = 50
ITEM.Model = "models/weapons/w_knife_t.mdl"
ITEM.WeaponClass = 'custom_drakes_talons'

ITEM.Owner =  	"STEAM_0:0:74314089"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end

function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end