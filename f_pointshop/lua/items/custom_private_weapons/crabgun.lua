ITEM.Name = "Crabgun"
ITEM.Price = 50
ITEM.Model = "models/weapons/w_shot_g3super90.mdl"
ITEM.WeaponClass = 'custom_crabgun'
ITEM.Owner = "STEAM_0:1:34087944"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end


function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end 