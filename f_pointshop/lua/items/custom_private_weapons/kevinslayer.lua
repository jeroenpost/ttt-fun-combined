ITEM.Name =  "Orb's Shotgun"
ITEM.Price = 60
ITEM.Model  = "models/weapons/w_shot_g3super90.mdl"
ITEM.WeaponClass = 'custom_kevinslayer'
 
ITEM.Owner = "STEAM_0:1:53508693"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end


function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end