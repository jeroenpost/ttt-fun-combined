ITEM.Name = "Phoenix Magnum"
ITEM.Price = 5000
ITEM.Material  = 'vgui/ttt_fun_pointshop_icons/magnum.png'
ITEM.WeaponClass = 'ac_deagle_1'

ITEM.Owner =  	"STEAM_0:0:45999260"

function ITEM:CanPlayerBuy( ply )

    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end

function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)

     hook.Add("PlayerDeath", "phoenixDie"..ply:SteamID(),function( victim, weapon, killer )

         if IsValid(ply) and  not ply:IsGhost() and SERVER and victim == ply  then
             pos = ply:GetPos();

             local ent = ents.Create("cse_ent_shortgasgrenade")
             ent:SetOwner(ply)
             ent.Owner = ply
             ent:SetPos(ply:GetPos())
             ent:SetAngles(Angle(1,0,0))
             ent:Spawn()
             ent.timer = CurTime() + 0.01
             timer.Simple(7,function()
                 if IsValid(ent) then
                     ent.Bastardgas:Remove()
                     ent.Entity:Remove()
                 end
             end)
         end
     end)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end