ITEM.Name = "The Kratos Slayer"
ITEM.Price = 50
ITEM.Model  = "models/weapons/w_shot_xm1014.mdl"
ITEM.WeaponClass = 'cstm_kratos_slayer'

ITEM.Owner =  	"STEAM_0:0:38365986"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end

function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end