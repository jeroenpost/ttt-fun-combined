ITEM.Name = "Hail Cpzombie"
ITEM.Price = 50
ITEM.Material  = 'materials/vgui/ttt_fun_killicons/golden_deagle.png'
ITEM.WeaponClass = 'custom_rezonate'
ITEM.Owner = "STEAM_0:0:51359123"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end


function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end