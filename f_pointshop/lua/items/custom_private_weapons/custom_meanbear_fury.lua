ITEM.Name = "Manbearphoenix's Fury"
ITEM.Price = 50
ITEM.Material  = 'vgui/ttt_fun_pointshop_icons/scout.png'
ITEM.WeaponClass = 'custom_manbear_fury'

ITEM.Owner =  	"STEAM_0:0:44614432"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end

function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end