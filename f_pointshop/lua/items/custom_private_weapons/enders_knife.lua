ITEM.Name = "Ender Knife"
ITEM.Price = 60
ITEM.Model = "models/weapons/w_knife_t.mdl"
ITEM.WeaponClass = 'custom_ender_knife'
ITEM.Owner = "STEAM_0:0:29999933"

function ITEM:CanPlayerBuy(ply)

    if ply:SteamID() == self.Owner then
        return true
    end

    return false
end


function ITEM:OnEquip(ply)

    ply:Give(self.WeaponClass)
    ply:SelectWeapon(self.WeaponClass)
    ply:SetNWInt("runspeed", 400)
    ply:SetNWInt("walkspeed",250)
    ply:SetJumpPower(250)


end

function ITEM:OnSell(ply)
    ply:StripWeapon(self.WeaponClass)
end 