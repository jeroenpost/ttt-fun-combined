ITEM.Name = "Adreneline Shot"
ITEM.Price = 60
ITEM.Material  = 'vgui/ttt_fun_killicons/fists.png'
ITEM.WeaponClass = 'custom_adreneline_shot'
ITEM.Owner = "STEAM_0:0:60683353"

function ITEM:CanPlayerBuy(ply)

    if ply:SteamID() == self.Owner then
        return true
    end

    return false
end


function ITEM:OnEquip(ply)

    ply:Give(self.WeaponClass)
    ply:SelectWeapon(self.WeaponClass)

end

function ITEM:OnSell(ply)
    ply:StripWeapon(self.WeaponClass)
end 