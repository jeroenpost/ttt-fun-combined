ITEM.Name = "Jeremy, The Cacti King"
ITEM.Price = 60
ITEM.Model = "models/props_lab/cactus.mdl"
ITEM.WeaponClass = 'custom_cactus'
ITEM.Owner = "STEAM_0:1:53508693"

function ITEM:CanPlayerBuy(ply)

    if ply:SteamID() == self.Owner then
        return true
    end

    return false
end


function ITEM:OnEquip(ply)

    ply:Give(self.WeaponClass)
    ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
    ply:StripWeapon(self.WeaponClass)
end 