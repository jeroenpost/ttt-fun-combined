
ITEM.Name = "Hand-held Supernova"
ITEM.Price = 50
ITEM.Model  = 'models/weapons/w_357.mdl'
ITEM.WeaponClass = 'custom_hand_held_nuking_device'
ITEM.Owner = "STEAM_0:1:119874655"

function ITEM:CanPlayerBuy( ply )
    if ply:SteamID() == self.Owner then
        return true
    end
    return false
end

function ITEM:OnEquip(ply)

    ply:Give(self.WeaponClass)
    ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
    ply:StripWeapon(self.WeaponClass)
end

