ITEM.Name = "Spartan Jihad"
ITEM.Price = 60
ITEM.Model  = 'models/weapons/w_c4.mdl'
ITEM.WeaponClass = 'custom_you_wont_come_again'
ITEM.Owner =  	"STEAM_0:0:50080297"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner or ply:IsSuperAdmin() then
        return true
    end

    return false
end



function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end