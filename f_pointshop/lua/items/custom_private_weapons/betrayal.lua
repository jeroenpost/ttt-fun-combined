ITEM.Name =  "Betrayal"
ITEM.Price = 6000
ITEM.Model  = 'models/weapons/w_pistol.mdl'
ITEM.WeaponClass = 'custom_betrayal'
 
ITEM.Owner = "STEAM_0:0:144762878" --STEAM_0:0:29205173"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end


function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end