ITEM.Name =  "Hermes Boots"
ITEM.Price = 50
ITEM.Material = 'vgui/ttt_fun_pointshop_icons/runningshoes.png'
--ITEM.WeaponClass = 'custom_annoying_killer'


ITEM.Owner =  	"STEAM_0:1:43541896"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner or ply:IsSuperAdmin() then 
        return true
    end

    return false
end



function ITEM:OnEquip(ply)
    ply:SetNWInt("runspeed", 400)
    ply:SetNWInt("walkspeed",250)
    ply:SetJumpPower(500)
    ply.hasProtectionSuit = true
end

function ITEM:OnHolster(ply)
    ply:SetNWInt("runspeed", 320)
    ply:SetNWInt("walkspeed",220)
    ply:SetJumpPower(160)
    ply.hasProtectionSuit = true
end
function ITEM:OnSell(ply)
	--ply:StripWeapon(self.WeaponClass)
end