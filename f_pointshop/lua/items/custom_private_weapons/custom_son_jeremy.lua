ITEM.Name = "Son of Jeremy"
ITEM.Price = 60
ITEM.Model = "models/props/de_inferno/cactus2.mdl"
ITEM.WeaponClass = 'custom_son_of_jeremy'
ITEM.Owner = "STEAM_0:1:14152890"

function ITEM:CanPlayerBuy(ply)

    if ply:SteamID() == self.Owner then
        return true
    end

    return false
end


function ITEM:OnEquip(ply)

    ply:Give(self.WeaponClass)
    ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
    ply:StripWeapon(self.WeaponClass)
end 