ITEM.Name =  "The Warlock"
ITEM.Material = 60
ITEM.Model = 'models/staff/staff.mdl'
ITEM.WeaponClass = 'custom_warlock'


ITEM.Owner = "STEAM_0:1:51423619"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end


function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end