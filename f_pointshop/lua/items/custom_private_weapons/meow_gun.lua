ITEM.Name = "Meow Gun"
ITEM.Price = 60
ITEM.Material = "vgui/ttt_fun_pointshop_icons/golden_deagle.png"
ITEM.WeaponClass = 'custom_meow_gun'
ITEM.Owner = "STEAM_0:0:144762878"

function ITEM:CanPlayerBuy(ply)

    if ply:SteamID() == self.Owner then
        return true
    end

    return false
end


function ITEM:OnEquip(ply)
    if ply:GetNWInt("runspeed",400) < 400 then
      ply:SetNWInt("runspeed", 400)
    end
    ply:SetNWInt("walkspeed",250)
    ply:SetJumpPower(250)

    ply:Give(self.WeaponClass)
    ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
    ply:StripWeapon(self.WeaponClass)
end 