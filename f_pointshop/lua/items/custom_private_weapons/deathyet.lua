ITEM.Name = "Deathyet"
ITEM.Price = 50
ITEM.Material  = 'vgui/ttt_fun_pointshop_icons/mp7.png'
ITEM.WeaponClass = 'custom_deathyet'

ITEM.Owner =  	"STEAM_0:1:51617207"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end

function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end