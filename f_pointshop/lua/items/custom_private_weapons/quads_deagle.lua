ITEM.Name = "Quad's Deagle"
ITEM.Price = 60
ITEM.Material = "vgui/ttt_fun_pointshop_icons/golden_deagle.png"
ITEM.WeaponClass = 'custom_quads_deagle'
ITEM.Owner = "STEAM_0:0:50080297"

function ITEM:CanPlayerBuy(ply)

    if ply:SteamID() == self.Owner then
        return true
    end

    return false
end


function ITEM:OnEquip(ply)

    ply:Give(self.WeaponClass)
    ply:SelectWeapon(self.WeaponClass)

    ply:SetNWInt("runspeed", 350)
    ply:SetNWInt("walkspeed",250)
    ply:SetJumpPower(250)
    ply.hasProtectionSuit = true
end

function ITEM:OnSell(ply)
    ply:StripWeapon(self.WeaponClass)
end 