ITEM.Name = "Honk's Fire"
ITEM.Price = 50
ITEM.Model = 'models/player/leet.mdl'
ITEM.Skin = 'camos/custom_camo40'
ITEM.IsFur = true
ITEM.Owner =  	"STEAM_0:0:144762878"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end

function ITEM:OnEquip(ply, modifications)


    timer.Create("honkfire"..ply:SteamID(),0.2,0,function()
          if  IsValid(ply) and not ply:IsGhost() and ply:Alive() and SERVER then
            local flame = ents.Create("env_fire");
            					flame:SetPos(ply:GetPos() + Vector( 0, 0, 1 ));
            					flame:SetKeyValue("firesize", "10");
            					flame:SetKeyValue("fireattack", "10");
            					flame:SetKeyValue("StartDisabled", "0");
            					flame:SetKeyValue("health", "1");
            					flame:SetKeyValue("firetype", "0");
            					flame:SetKeyValue("damagescale", "1");
            					flame:SetKeyValue("spawnflags", "128");
            					flame:SetPhysicsAttacker(ply)
            					flame:SetOwner( ply )
            					flame:Spawn();
            					flame:Fire("StartFire",0);
            					timer.Simple(0.7,function() if IsValid(flame) then flame:Remove() end end)
          end
    end)

end

function ITEM:OnHolster(ply)
    timer.Remove("honkfire"..ply:SteamID());
end

