ITEM.Name = "Fuzzy's Baby Maker"
ITEM.Price = 50
ITEM.Material  = 'vgui/ttt_fun_pointshop_icons/minigun.png'
ITEM.WeaponClass = 'custom_babe_maker'

ITEM.Owner =  	"STEAM_0:0:144762878"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end

function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end