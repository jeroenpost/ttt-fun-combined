ITEM.Name = "Royal Flush"
ITEM.Price = 500
ITEM.Material  = 'materials/vgui/ttt_fun_pointshop_icons/aug.png'
ITEM.WeaponClass = 'custom_royal_flush'

ITEM.Owner =  	"STEAM_0:1:59141882"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end


function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end