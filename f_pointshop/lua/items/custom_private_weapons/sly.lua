ITEM.Name = "Sly"
ITEM.Price = 50
ITEM.Material  = 'vgui/ttt_fun_pointshop_icons/famas.png'
ITEM.WeaponClass = 'custom_sly'

ITEM.Owner =  	"STEAM_0:1:67523356"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end

function ITEM:OnEquip(ply)

    ply:SetNWInt("runspeed", 390)
    ply:SetNWInt("walkspeed",250)
    ply:SetJumpPower(250)
    ply.hasProtectionSuit = true
	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end