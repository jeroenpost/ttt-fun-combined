ITEM.Name = 'Hat of Jeremy'
ITEM.Price = 50
ITEM.Model = "models/props_lab/cactus.mdl"
ITEM.Follower = 'custom_jeremy_hat'
ITEM.Attachment = 'eyes'

ITEM.Owner =  	"STEAM_0:1:53508693"

function ITEM:CanPlayerBuy( ply )

    if ply:SteamID() == self.Owner or ply:IsSuperAdmin() then
        return true
    end

    return false
end

function ITEM:OnEquip(ply, modifications)
	ply:Fo_CreateFollower( self.Follower )
    if ply:GetJumpPower() < 500 then
        ply:SetJumpPower(500)
    end
    timer.Create("jeremyhathealth"..ply:SteamID(),2,0,function()
        autohealfunction(ply)
    end)
    ply.HasJumpBoots = true
    ply:PS_AddClientsideModel(self.ID)
end

function ITEM:OnHolster(ply)
	ply:Fo_RemoveFollower( self.Follower )
    ply:PS_RemoveClientsideModel(self.ID)
end

function ITEM:ModifyClientsideModel(ply, model, pos, ang)
    --model:SetModelScale(1.6, 0)
    pos = pos + (ang:Forward() * -2.5) + (ang:Up() * 5.2)
    --ang:RotateAroundAxis(ang:Right(), 90)

    return model, pos, ang
end