ITEM.Name = "Picture of Death"
ITEM.Price = 50
ITEM.Model= "models/MaxOfS2D/camera.mdl"
ITEM.WeaponClass = 'custom_picture_of_Death'

ITEM.Owner =  	"STEAM_0:0:81532617"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end

function ITEM:OnEquip(ply)
    ply:GiveEquipmentItem( EQUIP_PROTECTIONSUIT )
    ply.hasProtectionSuit = true
	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnHolster(ply)
    ply:StripWeapon(self.WeaponClass)
end
function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end