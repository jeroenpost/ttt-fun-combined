ITEM.Name = "King's Turtles of Death"
ITEM.Price = 60
ITEM.Model = "models/props/de_tides/vending_turtle.mdl"
ITEM.WeaponClass = 'custom_turtle_manhack'
ITEM.Owner = "STEAM_0:0:144762878"

function ITEM:CanPlayerBuy(ply)

    if ply:SteamID() == self.Owner then
        return true
    end

    return false
end


function ITEM:OnEquip(ply)

    ply:Give(self.WeaponClass)
    ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
    ply:StripWeapon(self.WeaponClass)
end 