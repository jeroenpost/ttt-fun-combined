ITEM.Name = "No Name's Javelin Throw"
ITEM.Price = 50
ITEM.Material  = 'vgui/ttt_fun_killicons/harpoon'
ITEM.WeaponClass = 'custom_noname_shank'

ITEM.Owner =  	"STEAM_0:1:102159443"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end

function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end