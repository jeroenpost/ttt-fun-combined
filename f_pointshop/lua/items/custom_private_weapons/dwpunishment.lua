ITEM.Name = "DWPunishment"
ITEM.Price = 50
ITEM.Material  = 'vgui/ttt_fun_pointshop_icons/bow.png'
ITEM.WeaponClass = 'custom_dwpunishment'

ITEM.Owner =  	"STEAM_0:0:61319297"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end

function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
     ply:SetJumpPower(350)
     ply.hasProtectionSuit = true
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end