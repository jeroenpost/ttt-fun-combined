ITEM.Name = "Josh's Silent Scout"
ITEM.Price = 50
ITEM.Material = 'materials/vgui/ttt_fun_pointshop_icons/scout.png'
ITEM.WeaponClass = 'custom_joshscout'
ITEM.CanPlayerSell = false

ITEM.Owner =  "STEAM_0:0:50080297"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end


function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end 