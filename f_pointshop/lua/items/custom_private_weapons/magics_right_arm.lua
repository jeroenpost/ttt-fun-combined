ITEM.Name = "Magic's Meal Time"
ITEM.Price = 50
ITEM.Material  = 'vgui/ttt_fun_pointshop_icons/magnum.png'
ITEM.WeaponClass = 'custom_magics_right_arm'

ITEM.Owner =  	"STEAM_0:0:82905466"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end

function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
     if ply:GetNWInt("runspeed") < 450 then
         ply:SetNWInt("runspeed", 450)
         ply:SetNWInt("walkspeed",400)
         ply:SetJumpPower(350)
     end
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end