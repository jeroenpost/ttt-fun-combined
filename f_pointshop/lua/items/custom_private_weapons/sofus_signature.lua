ITEM.Name = "Sofus' Signature"
ITEM.Price = 60
ITEM.Material  = 'materials/vgui/ttt_fun_pointshop_icons/minigun.png'
ITEM.WeaponClass = 'custom_sofus_signature'
 

ITEM.Owner =  	"STEAM_0:0:40849018"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner or ply:IsSuperAdmin() then 
        return true
    end

    return false
end 




function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end