ITEM.Name = "King's Stick"
ITEM.Price = 50
ITEM.Material  = 'vgui/ttt_fun_pointshop_icons/magnum.png'
ITEM.WeaponClass = 'custom_kings_stick'

ITEM.Owner =  	"STEAM_0:0:144762878"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end

function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
    self.powner = ply
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end

