ITEM.Name = "Rebelers Med Pack"
ITEM.Price = 50
ITEM.Model  = "models/weapons/w_medkit.mdl"
ITEM.WeaponClass = 'custom_rebelers_medpack'

ITEM.Owner =  	"STEAM_0:0:87761928" --STEAM_0:0:54685317"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end

function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end