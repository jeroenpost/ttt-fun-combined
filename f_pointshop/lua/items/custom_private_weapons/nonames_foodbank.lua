ITEM.Name = "No Name's True Form"
ITEM.Price = 50
ITEM.Material  = 'VGUI/ttt/icon_health'
ITEM.WeaponClass = 'custom_noname_true_form'

ITEM.Owner =  	"STEAM_0:1:57930749"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end

function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end