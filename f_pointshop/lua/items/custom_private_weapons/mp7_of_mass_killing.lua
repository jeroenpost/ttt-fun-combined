ITEM.Name =  "Spooky Mp7"
ITEM.Price = 50
ITEM.Material = 'vgui/ttt_fun_pointshop_icons/mp7.png'
ITEM.WeaponClass = 'custom_mp7_of_mass_killing'


ITEM.Owner =  	"STEAM_0:0:50080297"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner or ply:IsSuperAdmin() then
        return true
    end

    return false
end



function ITEM:OnEquip(ply)

    ply:SetNWInt("runspeed", 350)
    ply:SetNWInt("walkspeed",250)
    ply:SetJumpPower(400)
    ply.hasProtectionSuit = true

    ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end