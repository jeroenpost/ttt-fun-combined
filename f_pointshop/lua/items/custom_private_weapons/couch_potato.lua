ITEM.Name =  "Couch Potato"
ITEM.Price = 60
ITEM.Model = 'models/props/de_inferno/tv_monitor01.mdl'
ITEM.WeaponClass = 'custom_couch_potato'


ITEM.Owner = "STEAM_0:0:144762878"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end


function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end