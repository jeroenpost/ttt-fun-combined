ITEM.Name = "Crescent Thorns"
ITEM.Cost = 50
ITEM.Model = "models/rubyscythe.mdl"
ITEM.Bone = 'ValveBiped.Bip01_Spine2'

ITEM.Owner =  	"STEAM_0:0:29205173"

function ITEM:CanPlayerBuy( ply )

    if ply:SteamID() == self.Owner then
        return true
    end

    return false
end


function ITEM:OnEquip(ply, modifications)
    ply:PS_AddClientsideModel(self.ID)
   hook.Add("PlayerDeath", "crescent_die"..ply:SteamID(),function( victim, weapon, killer )
        if  IsValid(ply) and not ply:IsGhost() and SERVER and victim == ply  then
            local pos = ply:GetPos();

            local ent = ents.Create("crescent_thorns_ent")
           -- ent:SetOwner(ply)
           -- ent.Owner = ply
            ent:SetPos(ply:GetPos())
            ent:SetAngles(Angle(1,0,0))
            ent:Spawn()

        end
    end)

end

function ITEM:OnHolster(ply)
    ply:PS_RemoveClientsideModel(self.ID)
end


function ITEM:ModifyClientsideModel(ply, model, pos, ang)
    model:SetModelScale( 1, 0 )
    local PlyModel = ply:GetModel()

    pos = pos + (ang:Forward() * 1) + (ang:Up() * 0) + (ang:Right() * 6.11765)
    ang:RotateAroundAxis(ang:Right(), -45)
    ang:RotateAroundAxis(ang:Up(), 90)
    ang:RotateAroundAxis(ang:Forward(), 0)

    return model, pos, ang
end