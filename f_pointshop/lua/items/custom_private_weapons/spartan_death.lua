ITEM.Name = "Power"
ITEM.Price = 50
ITEM.Material  = 'vgui/ttt_fun_pointshop_icons/sparta_kick.png'
ITEM.WeaponClass = 'custom_spartan_death'

ITEM.Owner =  	"STEAM_0:1:64973878"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end

function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end