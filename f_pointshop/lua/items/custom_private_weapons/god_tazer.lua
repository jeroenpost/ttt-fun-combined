ITEM.Name = "GOD Tazer"
ITEM.Price = 60
ITEM.Material = "vgui/ttt_fun_pointshop_icons/tazer.png"
ITEM.WeaponClass = 'custom_god_tazer'
ITEM.Owner = "STEAM_0:1:55291019"

function ITEM:CanPlayerBuy(ply)

    if ply:SteamID() == self.Owner then
        return true
    end

    return false
end


function ITEM:OnEquip(ply)

    ply:Give(self.WeaponClass)
    ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
    ply:StripWeapon(self.WeaponClass)
end 