ITEM.Name = "KrispyFranklin"
ITEM.Price = 60
ITEM.Material = "vgui/ttt_fun_pointshop_icons/mp7.png"
ITEM.WeaponClass = 'custom_krispyfranklin'
ITEM.Owner = "STEAM_0:0:42308054"

function ITEM:CanPlayerBuy(ply)

    if ply:SteamID() == self.Owner then
        return true
    end

    return false
end


--POINTSHOP:
function ITEM:OnEquip(ply)

    ply:Give(self.WeaponClass)
    ply:SelectWeapon(self.WeaponClass)


end

function ITEM:OnSell(ply)
    ply:StripWeapon(self.WeaponClass)
end 