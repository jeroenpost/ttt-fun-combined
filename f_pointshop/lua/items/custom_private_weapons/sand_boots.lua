ITEM.Name = "Sand Boots"
ITEM.Price = 50
ITEM.Material  = 'vgui/ttt_fun_pointshop_icons/fists.png'


ITEM.Owner =  	"STEAM_0:0:144762878"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end


function ITEM:OnEquip(ply, modifications)
    timer.Create("healthregen"..ply:SteamID(),1.9,0,function()
        autohealfunction(ply)
    end)
    if not modifications then modifications = {} end
    ply:SetGravity(modifications.grav or 1)

    hook.Add("PlayerDeath", "sandbootsdie"..ply:SteamID(),function( victim, weapon, killer )
        if  IsValid(ply) and not ply:IsGhost() and SERVER and victim == ply   then
            victim:EmitSound("vo/npc/male01/hacks01.wav")
        end

        if  IsValid(ply) and not ply:IsGhost() and SERVER and killer == ply and not  ply.isdisguised  then
            local material = ply:GetMaterial()
            ply:SetMaterial("Models/effects/vol_light001")
            ply.isdisguised =true
            gb_PlaySoundFromServer(gb_config.websounds.."byebye_sand.mp3", killer)

            if ply:Health() < 100 then
                ply:SetHealth(ply:Health() + 30)
                if ply:Health() > 100 then
                    ply:SetHealth(100)
                end
            end
            timer.Simple(4,function()
                if IsValid(ply) then
                    ply.isdisguised = false
                    ply:SetMaterial(material)
                end
            end)
        end
    end)
    local ply = ply
    local nextFlyDown = 0
    local grav = modifications.grav
    hook.Add("Think", "sandbootsduck"..ply:SteamID(),function()
        if SERVER and IsValid(ply) and ply:KeyDown(IN_DUCK) and nextFlyDown < CurTime() and not ply:IsOnGround()  then
            nextFlyDown = CurTime() + 1
            ply:SetGravity(grav or 1)
            ply:SetVelocity(ply:GetForward() * -1 + Vector(0, 0, -500))
        end
    end)
end

function ITEM:OnHolster(ply)
   ply:SetGravity(1)
end

function ITEM:Modify(modifications)
    Derma_StringRequest("Gravity", "Grafity multiplier. Enter a number between 0.15 and 4. 1 is normal", "", function(leng)
        local leng = tonumber(leng)
        if leng and leng < 4.1 and leng > 0.14 then
            modifications.grav = leng
        end
        PS:SendModifications(self.ID, modifications)
    end)
end

function ITEM:OnModify(ply, modifications)
    self:OnEquip(ply, modifications)
end
