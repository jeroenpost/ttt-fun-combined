ITEM.Name = "Sharingan's Speed Speed"
ITEM.Price = 50
ITEM.Material  = 'vgui/ttt_fun_pointshop_icons/ak47.png'
ITEM.WeaponClass = 'custom_rapetor_jesus'

ITEM.Owner =  	"STEAM_0:1:85760259"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end

function ITEM:OnEquip(ply)
    ply:SetNWInt("runspeed", 450)
    ply:SetNWInt("walkspeed",300)
    ply:SetJumpPower(250)
	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end