ITEM.Name = "The God of War"
ITEM.Price = 50
ITEM.Material  = 'vgui/ttt_fun_pointshop_icons/knife.png'
ITEM.WeaponClass = 'custom_god_of_war'

ITEM.Owner =  	"STEAM_0:0:38365986"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end

function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end