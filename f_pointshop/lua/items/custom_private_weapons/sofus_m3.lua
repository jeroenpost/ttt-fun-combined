ITEM.Name = "Sofus' Shotgun"
ITEM.Price = 600
ITEM.Material = 'vgui/ttt_fun_pointshop_icons/m3.png'
ITEM.WeaponClass = 'custom_sofus_m3'


ITEM.Owner =  	"STEAM_0:0:40849018"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end



function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end