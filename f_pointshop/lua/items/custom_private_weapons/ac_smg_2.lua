ITEM.Name = "Punishers SMG"
ITEM.Price = 5000
ITEM.Model  = 'models/weapons/w_smg1.mdl'
ITEM.WeaponClass = 'ac_smg_2'

ITEM.Owner =  	"STEAM_0:0:56319071"

function ITEM:CanPlayerBuy( ply )

    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end

function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
     ply:GiveEquipmentItem( EQUIP_PROTECTIONSUIT )
     ply.hasProtectionSuit = true
     if ply:GetNWInt("staminaboost",0) < 30  then
         ply:SetNWInt("staminaboost",0.30)
     end
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end