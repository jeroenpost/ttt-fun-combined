ITEM.Name = "Eyars Bow"
ITEM.Price = 60
ITEM.Material = "vgui/ttt_fun_pointshop_icons/bow.png"
ITEM.WeaponClass = 'custom_eyars_bow'
ITEM.Owner = "STEAM_0:0:63151790"

function ITEM:CanPlayerBuy(ply)

    if ply:SteamID() == self.Owner then
        return true
    end

    return false
end


function ITEM:OnEquip(ply)

    ply:Give(self.WeaponClass)
    ply:SelectWeapon(self.WeaponClass)

    timer.Create("EyarsBowCheck",1,0,function()
        if IsValid(ply) and ply.numberOfStickies and ply.numberOfStickies < 15 then
            ply:Give(self.WeaponClass)
        end
    end)

end

function ITEM:OnSell(ply)
    ply:StripWeapon(self.WeaponClass)
end 