ITEM.Name = "Travelever's Destroyer"
ITEM.Price = 60
ITEM.Material = 'materials/vgui/ttt_fun_pointshop_icons/galil.png'
ITEM.WeaponClass = 'custom_travellers_destroyer'
ITEM.Owner = "STEAM_0:0:42086900"


function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end

function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end