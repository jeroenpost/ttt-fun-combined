ITEM.Name = "Gum's on your shoe"
ITEM.Price = 1500000
ITEM.Model = "models/Teh_Maestro/popcorn.mdl"
ITEM.WeaponClass = 'custom_poofer_gum'
ITEM.Owner = "STEAM_0:0:32034132"

function ITEM:CanPlayerBuy(ply)

    --if ply:SteamID() == self.Owner then
        return true
    --end

   -- return false
end


function ITEM:OnEquip(ply)

    ply:Give(self.WeaponClass)
    ply:SelectWeapon(self.WeaponClass)
    ply.numberOfStickies = 0

    timer.Create("PooferGum",1,0,function()
        if not IsValid(ply) then return end
        if not ply.numberOfStickies then ply.numberOfStickies = 0 end
        if IsValid(ply) and ply.numberOfStickies < 15 and not ply:HasWeapon(self.WeaponClass) and ply:Alive() then

            ply:Give(self.WeaponClass)
            ply.numberOfStickies = ply.numberOfStickies + 1
        end
    end)

end

function ITEM:OnSell(ply)
    timer.Destroy("PooferGum");
    ply:StripWeapon(self.WeaponClass)
end

function ITEM:OnHolster(ply)
    timer.Destroy("PooferGum");
end