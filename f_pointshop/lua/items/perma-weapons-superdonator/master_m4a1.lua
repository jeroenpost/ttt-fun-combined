ITEM.Name = 'Master M4A1'
ITEM.Price = 125000
ITEM.Material = "materials/vgui/ttt_fun_pointshop_icons/m4a1.png"
ITEM.WeaponClass = 'master_m4a1'
ITEM.SingleUse = false

ITEM.AdminOnly = false

function ITEM:OnEquip(ply)
	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end