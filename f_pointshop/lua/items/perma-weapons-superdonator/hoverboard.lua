ITEM.Name = 'Hoverboard'
ITEM.Price = 125000
ITEM.Material = 'materials/vgui/ttt_fun_pointshop_icons/hoverboard.png'
ITEM.WeaponClass = 'hoverboard'
ITEM.CanBuyMultipleTimes = true

function ITEM:OnEquip(ply)
	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnBuy(ply)
	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end
 