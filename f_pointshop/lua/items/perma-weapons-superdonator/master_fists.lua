ITEM.Name = 'Master Fists'
ITEM.Price = 125000
ITEM.Material = 'vgui/ttt_fun_pointshop_icons/fists.png'
ITEM.WeaponClass = 'master_fists'
ITEM.SingleUse = false

ITEM.AdminOnly = false

function ITEM:OnEquip(ply)
	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end