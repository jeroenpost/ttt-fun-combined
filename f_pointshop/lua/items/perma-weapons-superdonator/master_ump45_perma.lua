ITEM.Name = 'Master UMP45'
ITEM.Price = 150000
ITEM.Material = "materials/vgui/ttt_fun_pointshop_icons/ump45.png"
ITEM.WeaponClass = 'master_ump45'
ITEM.SingleUse = false

ITEM.AdminOnly = false

function ITEM:OnEquip(ply)
	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end