ITEM.Name = "Lapua"
ITEM.Price = 2500000
ITEM.Model  = 'models/weapons/w_snip_m24gb.mdl'
ITEM.WeaponClass = 'custom_archer_sniper'


function ITEM:OnEquip(ply)

    ply:Give(self.WeaponClass)
    ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
    ply:StripWeapon(self.WeaponClass)
end