ITEM.Name = 'Perma Nanners Gun'
ITEM.Price = 250000
ITEM.Material = 'materials/vgui/ttt_fun_pointshop_icons/nanners_gun.png'
ITEM.WeaponClass = 'gb_nanners'

function ITEM:OnEquip(ply)
	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end
