ITEM.Name = 'Master M3'
ITEM.Price = 125000
ITEM.Material = "materials/vgui/ttt_fun_pointshop_icons/m3.png"
ITEM.WeaponClass = 'master_m3'
ITEM.SingleUse = false

ITEM.AdminOnly = false

function ITEM:OnEquip(ply)
	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end