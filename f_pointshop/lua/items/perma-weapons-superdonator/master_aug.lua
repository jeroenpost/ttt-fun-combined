ITEM.Name = 'Master Aug'
ITEM.Price = 250000
ITEM.Material = "materials/vgui/ttt_fun_pointshop_icons/aug.png"
ITEM.WeaponClass = 'master_aug'

function ITEM:OnEquip(ply)
	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end
