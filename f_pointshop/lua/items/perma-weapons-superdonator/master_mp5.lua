ITEM.Name = 'Master MP5'
ITEM.Price = 125000
ITEM.Material = "materials/vgui/ttt_fun_pointshop_icons/mp5.png"
ITEM.WeaponClass = 'master_mp5'
ITEM.SingleUse = false

ITEM.AdminOnly = false

function ITEM:OnEquip(ply)
	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end