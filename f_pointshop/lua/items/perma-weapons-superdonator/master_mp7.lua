ITEM.Name = 'Master MP7'
ITEM.Price = 125000
ITEM.Material = 'vgui/ttt_fun_pointshop_icons/mp7.png'
ITEM.WeaponClass = 'master_mp7'
ITEM.SingleUse = false

ITEM.AdminOnly = false

function ITEM:OnEquip(ply)
	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end