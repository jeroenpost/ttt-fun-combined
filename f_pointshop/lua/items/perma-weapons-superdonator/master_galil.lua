ITEM.Name = 'Master Galil'
ITEM.Price = 250000
ITEM.Material = "materials/vgui/ttt_fun_pointshop_icons/galil.png"
ITEM.WeaponClass = 'master_galil'

function ITEM:OnEquip(ply)
	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end
