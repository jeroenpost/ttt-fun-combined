ITEM.Name =  "Assassin's Knife"
ITEM.Price = 1000000
ITEM.Material = 'materials/vgui/ttt_fun_killicons/knife.png'
ITEM.WeaponClass = 'custom_archers_knife'

function ITEM:OnEquip(ply)

    ply:Give(self.WeaponClass)
    ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
    ply:StripWeapon(self.WeaponClass)
end