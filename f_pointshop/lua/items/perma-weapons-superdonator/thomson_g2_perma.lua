ITEM.Name = 'Thomson G2'
ITEM.Price = 1750000
ITEM.Material = 'vgui/ttt_fun_pointshop_icons/thomson_g2.png'
ITEM.WeaponClass = 'weapon_ttt_thomson_g2'

function ITEM:OnEquip(ply)
	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end
