ITEM.Name = 'Dubstepgun'
ITEM.Price = 500000
ITEM.Material = 'materials/vgui/ttt_fun_pointshop_icons/dubstepgun.png'
ITEM.WeaponClass = 'weapon_dubstep3'


function ITEM:OnEquip(ply, modifications)
         
	
	if modifications.color ~= nil then
		ply.dubstepcolor = modifications.color
	else
            ply.dubstepcolor = false
        end

        ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end

function ITEM:OnHolster(ply)
     ply.dubstepcolor = false
end

function ITEM:Modify(modifications)
		PS:ShowColorChooser(self, modifications)
end

function ITEM:OnModify(ply, modifications)
    self:OnHolster(ply)
    self:OnEquip(ply, modifications) -- adds the item back again, with new mods
end
