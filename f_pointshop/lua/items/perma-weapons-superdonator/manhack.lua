ITEM.Name = 'Manhack Gun'
ITEM.Price = 50000
ITEM.Material = 'materials/vgui/ttt_fun_pointshop_icons/manhackgun.png'
ITEM.WeaponClass = 'gb_manhack'

function ITEM:OnEquip(ply)
	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnBuy(ply)
	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end
