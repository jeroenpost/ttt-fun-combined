ITEM.Name = 'Perma Master Molotov Cocktail'
ITEM.Price = 500000
ITEM.Material = "materials/vgui/ttt_fun_pointshop_icons/molotov.png"
ITEM.WeaponClass = 'weapon_ttt_mastermolotov'

function ITEM:OnEquip(ply)
	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end
