ITEM.Name = "Barreta"
ITEM.Price = 5000000
ITEM.Model  = 'models/weapons/w_pist_m9g.mdl'
ITEM.WeaponClass = 'custom_barreta'

function ITEM:OnEquip(ply)

    ply:Give(self.WeaponClass)
    ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
    ply:StripWeapon(self.WeaponClass)
end