ITEM.Name = 'RocketJump'
ITEM.Price = 500000
ITEM.Model =  "models/weapons/w_rocket_launcher.mdl"
ITEM.WeaponClass = 'gb_rocketjump'
ITEM.SingleUse = false

ITEM.AdminOnly = false

function ITEM:OnEquip(ply,modifications )

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end
