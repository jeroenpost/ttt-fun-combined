ITEM.Name = 'P228 of LIGHTNING'
ITEM.Price = 50000
ITEM.Model = "models/weapons/w_pist_p228.mdl"
ITEM.Skin = "camos/camo43"
ITEM.WeaponClass = 'weapon_ttt_p228'
ITEM.SingleUse = false

ITEM.AdminOnly = false

function ITEM:OnEquip(ply,modifications )

	 ply:Give(self.WeaponClass)
	 ply:SelectWeapon(self.WeaponClass)
    local wep = ply:GetWeapon( self.WeaponClass )
    wep:SetPrintName( self.Name )
    wep:SetCamo( 43 )
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end
