ITEM.Name = 'G2 Cstm'
ITEM.Price = 500000
ITEM.Model =   "models/weapons/w_cstm_g2.mdl"
ITEM.WeaponClass = 'cstm_pistol_g2contender_col'
ITEM.SingleUse = false

ITEM.AdminOnly = false

function ITEM:OnEquip(ply,modifications )

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end
