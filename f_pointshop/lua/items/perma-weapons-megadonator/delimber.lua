ITEM.Name = 'The Delimber'
ITEM.Price = 500000
ITEM.Model =  "models/morrowind/daedric/shortsword/w_daedric_shortsword.mdl"
ITEM.WeaponClass = 'gb_delimber'
ITEM.SingleUse = false

ITEM.AdminOnly = false

function ITEM:OnEquip(ply,modifications )

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end
