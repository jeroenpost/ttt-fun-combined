ITEM.Name = "ACR"
ITEM.Price = 5000000
ITEM.Model  = 'models/mass_effect_3/weapons/sniper_rifles/m-13_Raptor.mdl'
ITEM.WeaponClass = 'custom_acr'

function ITEM:OnEquip(ply)

    ply:Give(self.WeaponClass)
    ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
    ply:StripWeapon(self.WeaponClass)
end