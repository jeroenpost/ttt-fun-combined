ITEM.Name = 'CROWbar'
ITEM.Price = 50000
ITEM.Model =   "models/weapons/w_crowbar.mdl"
ITEM.WeaponClass = 'gb_crowbar'
ITEM.SingleUse = false

ITEM.AdminOnly = false

function ITEM:OnEquip(ply,modifications )

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end
