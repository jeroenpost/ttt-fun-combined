ITEM.Name = 'The Crippler'
ITEM.Price = 500000
ITEM.Model =  "models/weapons/w_nessbat.mdl"
ITEM.WeaponClass = 'gb_crippler'
ITEM.SingleUse = false

ITEM.AdminOnly = false

function ITEM:OnEquip(ply,modifications )

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end
