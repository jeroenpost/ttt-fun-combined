ITEM.Name = 'Drone'
ITEM.Price = 500000
ITEM.Model = "models/Combine_Scanner.mdl"
ITEM.WeaponClass = 'sent_drone_ttt'
ITEM.SingleUse = false

ITEM.AdminOnly = false

function ITEM:OnEquip(ply,modifications )
    if not modifications then modifications = {} end
    ply.DroneName = modifications.text or nil
    ply.DroneColor = modifications.color or nil
	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end

function ITEM:Modify(modifications)
    Derma_StringRequest("Text", "Name your drone", "", function(text)
        modifications.text = string.sub(text, 1, 20)
        PS:ShowColorChooser(self, modifications)
    end)
end