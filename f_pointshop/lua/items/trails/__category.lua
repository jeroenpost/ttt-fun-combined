CATEGORY.Name = 'Trails'
CATEGORY.Desc = 'Leave a trail behind you when you are walking'
CATEGORY.Icon = 'rainbow'
CATEGORY.buyWhenDeath = true
CATEGORY.AllowedEquipped = 1