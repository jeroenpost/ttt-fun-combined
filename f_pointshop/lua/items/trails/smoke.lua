ITEM.Name = 'Smoke Trail'
ITEM.Price = 25000
ITEM.Material = 'trails/smoke.vmt'

function ITEM:OnEquip(ply, modifications)
	ply.TrailPSSmoke = util.SpriteTrail(ply, 0, modifications.color, false, 15, 1, 4, 0.0333, self.Material)
end

function ITEM:OnHolster(ply)
	SafeRemoveEntity(ply.TrailPSSmoke)
end

function ITEM:Modify(modifications)
	PS:ShowColorChooser(self, modifications)
end

function ITEM:OnModify(ply, modifications)
	SafeRemoveEntity(ply.TrailPSSmoke)
	self:OnEquip(ply, modifications)
end
