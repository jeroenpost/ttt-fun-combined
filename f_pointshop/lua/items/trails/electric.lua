ITEM.Name = 'Electric Trail'
ITEM.Price = 1500
ITEM.Material = 'trails/electric.vmt'

function ITEM:OnEquip(ply, modifications) if ply:Team() ~= TEAM_HUMAN and  ply.ShowTrailAsProp == true then return end
	ply.ElectricTrail = util.SpriteTrail(ply, 0, modifications.color, false, 15, 1, 4, 0.0333, self.Material)
end

function ITEM:OnHolster(ply)
	SafeRemoveEntity(ply.ElectricTrail)
end

function ITEM:Modify(modifications)
	PS:ShowColorChooser(self, modifications)
end

function ITEM:OnModify(ply, modifications)
	SafeRemoveEntity(ply.ElectricTrail)
	self:OnEquip(ply, modifications)
end