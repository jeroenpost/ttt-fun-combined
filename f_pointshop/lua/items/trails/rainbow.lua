ITEM.Name = 'Rainbow Trail'
ITEM.Price = 12000
ITEM.Material = 'trails/rainbow.vmt'

function ITEM:OnEquip(ply, modifications)
	ply.TrailPSRainBow = util.SpriteTrail(ply, 0, modifications.color, false, 15, 1, 4, 0.0333, self.Material)
end

function ITEM:OnHolster(ply)
	SafeRemoveEntity(ply.TrailPSRainBow)
end

function ITEM:Modify(modifications)
	PS:ShowColorChooser(self, modifications)
end

function ITEM:OnModify(ply, modifications)
	SafeRemoveEntity(ply.TrailPSRainBow)
	self:OnEquip(ply, modifications)
end
