ITEM.Name = 'Buckshot Ammo'
ITEM.Price = 500
ITEM.Model =    Model("models/items/boxbuckshot.mdl")
ITEM.WeaponClass = 'item_box_buckshot_ttt'
ITEM.SingleUse = true

ITEM.AdminOnly = false

function ITEM:OnBuy(ply,modifications )

	 ply:Give(self.WeaponClass)
     ply:EmitSound("npc/turret_floor/click1.wav"     )
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end
