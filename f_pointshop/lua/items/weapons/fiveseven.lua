ITEM.Name = 'FiveSeven'
ITEM.Price = 750
ITEM.Material = "materials/vgui/ttt_fun_pointshop_icons/fiveseven.png"
ITEM.WeaponClass = 'weapon_zm_pistol'
ITEM.SingleUse = true

ITEM.AdminOnly = false

function ITEM:OnBuy(ply)
	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end