ITEM.Name = 'Camo Tester'
ITEM.Price = 50
ITEM.Material =   "camos/camo43.vmt"
ITEM.WeaponClass = 'gb_camo_test'
ITEM.SingleUse = true

ITEM.AdminOnly = false

function ITEM:OnBuy(ply,modifications )

	 ply:Give(self.WeaponClass)
	 ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end
