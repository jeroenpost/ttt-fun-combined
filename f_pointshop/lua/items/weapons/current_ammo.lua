ITEM.Name = 'Ammo Refill'
ITEM.Price = 2500
ITEM.Model = "models/Items/BoxBuckshot.mdl"
ITEM.SingleUse = true

function ITEM:OnBuy(ply)
        
    for k, v in pairs(ply:GetWeapons()) do

        if v.Primary and v.Primary.DefaultClip and v.Primary.DefaultClip > 5 then
            ply:SetAmmo(v.Primary.DefaultClip, v:GetPrimaryAmmoType())
        end
    end
	 
end

function ITEM:OnEquip(ply)
   self:OnBuy( ply )
	 
end