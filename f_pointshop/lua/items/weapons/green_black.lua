ITEM.Name = 'GreenBlack'
ITEM.Price = 1200
ITEM.Material = 'vgui/ttt_fun_pointshop_icons/green_black.png'
ITEM.WeaponClass = 'green_black'
ITEM.SingleUse = true

ITEM.AdminOnly = false

function ITEM:OnBuy(ply)
	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end