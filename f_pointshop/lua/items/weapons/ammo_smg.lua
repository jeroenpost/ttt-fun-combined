ITEM.Name = 'SMG Ammo'
ITEM.Price = 500
ITEM.Model =    Model("models/items/boxmrounds.mdl")
ITEM.WeaponClass = 'item_ammo_amg1_ttt'
ITEM.SingleUse = true

ITEM.AdminOnly = false

function ITEM:OnBuy(ply,modifications )

	 ply:Give(self.WeaponClass)
     ply:EmitSound("npc/turret_floor/click1.wav"     )
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end
