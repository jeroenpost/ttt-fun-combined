ITEM.IsPlayerModel = true
ITEM.Name = 'Geth'
ITEM.Price = 25000
ITEM.Model = 'models/voxelzero/player/geth.mdl'

function ITEM:OnEquip(ply, modifications)
	if not ply._OldModel then
		ply._OldModel = ply:GetModel()
    end
    timer.Simple(1, function()  ply.PlayerModel = self.Model end) ply.PlayerModel = self.Model ply:SetModel( ply.PlayerModel )

end

function ITEM:OnHolster(ply)
	if ply._OldModel then
		ply:SetModel(ply._OldModel)
	end
end
