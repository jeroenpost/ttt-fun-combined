ITEM.Name = "WunderWaffe DG-3"
ITEM.Price = 1000000
ITEM.Model = "models/wunderwaffe.mdl"
ITEM.WeaponClass = 'wunderwaffe_v2'
ITEM.Skin = "camos/camo43"

function ITEM:OnEquip(ply)
    ply:GiveEquipmentItem( EQUIP_PROTECTIONSUIT )
    ply.hasProtectionSuit = true
	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end