ITEM.Name = '.44 Magnum'
ITEM.Price = 200
ITEM.Material = 'vgui/ttt_fun_pointshop_icons/magnum.png'
ITEM.WeaponClass = 'weapon_ttt_44mag'
ITEM.SingleUse = true

function ITEM:OnBuy(ply)
	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end