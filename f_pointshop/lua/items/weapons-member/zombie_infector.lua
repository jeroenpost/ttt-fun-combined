ITEM.Name = 'Zombie Infector'
ITEM.Price = 2000
ITEM.Material = 'vgui/ttt_fun_pointshop_icons/zombie_infector.png'
ITEM.WeaponClass = 'weapon_ttt_zombieinf'
ITEM.SingleUse = true

function ITEM:OnBuy(ply)
	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end