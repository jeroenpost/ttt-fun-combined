ITEM.Name = 'XM1014'
ITEM.Price = 1000
ITEM.Material = "materials/vgui/ttt_fun_pointshop_icons/xm1014.png"
ITEM.WeaponClass = 'weapon_zm_shotgun'
ITEM.SingleUse = true

ITEM.AdminOnly = false

function ITEM:OnBuy(ply)
	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end