ITEM.Name = 'Puncake'
ITEM.Price = 2550
ITEM.Material = 'vgui/ttt_fun_pointshop_icons/puncake.png'
ITEM.WeaponClass = 'gb_puncake'
ITEM.SingleUse = true

function ITEM:OnBuy(ply)
	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end