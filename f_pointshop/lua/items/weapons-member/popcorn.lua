ITEM.Name = 'Popcorn'
ITEM.Price = 750
ITEM.Model = 'models/teh_maestro/popcorn.mdl'
ITEM.WeaponClass = 'weapon_popcorn'
ITEM.SingleUse = true

function ITEM:OnBuy(ply)
	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end