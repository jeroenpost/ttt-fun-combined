ITEM.Name =  "Black Carver"
ITEM.Price = 1000
ITEM.Model  = 'models/morrowind/daedric/shortsword/w_daedric_shortsword.mdl'
ITEM.WeaponClass = 'custom_black_carver'
ITEM.SingleUse = true


function ITEM:OnBuy(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end