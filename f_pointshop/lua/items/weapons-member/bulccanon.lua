ITEM.Name = 'Space Shotgun'
ITEM.Price = 1200
ITEM.Material = "materials/vgui/ttt_fun_pointshop_icons/spaceshotgun.png"
ITEM.WeaponClass = 'weapon_bulk_canon'
ITEM.SingleUse = true

function ITEM:OnBuy(ply)
	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end 