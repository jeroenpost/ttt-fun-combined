ITEM.Name = 'BabyGrenade'
ITEM.Price = 2000
ITEM.Material = 'vgui/ttt_fun_pointshop_icons/babyboom.png'
ITEM.WeaponClass = 'gb_babygrenade'
ITEM.SingleUse = true

function ITEM:OnBuy(ply)
	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end