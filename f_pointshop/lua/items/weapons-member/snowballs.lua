ITEM.Name = 'Snowballs'
ITEM.Price = 50
ITEM.Material = 'vgui/ttt_fun_pointshop_icons/snowball.png'
ITEM.WeaponClass = 'snowball_thrower'
ITEM.SingleUse = true

function ITEM:OnBuy(ply)
	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end