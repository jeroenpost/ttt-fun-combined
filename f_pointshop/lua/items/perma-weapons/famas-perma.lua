ITEM.Name = 'Red Camo Famas'
ITEM.Price = 3000
ITEM.Model =  "models/weapons/w_rif_famas.mdl"
ITEM.WeaponClass = 'weapon_ttt_famas'
ITEM.Skin = "camos/camo2"

ITEM.AdminOnly = false

function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
     local wep = ply:GetWeapon( self.WeaponClass )
     wep:SetPrintName( self.Name )
     wep:SetCamo( 2 )
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end