ITEM.Name = 'Minecraft Sword'
ITEM.Price = 35000
ITEM.Material = "materials/vgui/ttt_fun_pointshop_icons/minecraftsword.png"
ITEM.WeaponClass = 'minecraft_sword'
ITEM.SingleUse = false

function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end