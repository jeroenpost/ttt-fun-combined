ITEM.Name = "Harry Potter's Cloak"
ITEM.Price = 5000
ITEM.Material  = 'vgui/ttt_fun_pointshop_icons/fists.png'
ITEM.WeaponClass = 'custom_harry_potters_cloak'

ITEM.Owner =  	"STEAM_0:0:144762878"


function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)

    timer.Create("checkcloak"..ply:SteamID(),3,0,function()
        if not IsValid(ply) then return end
        local wep = ply:GetActiveWeapon()
        if wep or not wep.GetClass then
            ply:SetMaterial("")
            return
        end
        if wep:GetClass() != self.WeaponClass then
            ply:SetMaterial("")
         return
       end
    end)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end