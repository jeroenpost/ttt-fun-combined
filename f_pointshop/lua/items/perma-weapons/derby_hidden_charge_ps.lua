ITEM.Name = "Derby's Hidden Charge"
ITEM.Price = 5000
ITEM.Model = "models/props/cs_office/microwave.mdl"
ITEM.WeaponClass = 'custom_derbys_hidden_charge'

ITEM.Owner =  	"STEAM_0:0:144762878"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end

function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end
