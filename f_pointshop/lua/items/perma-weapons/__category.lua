CATEGORY.Name = 'Weapons - Perma use'
CATEGORY.Icon = 'gun'
CATEGORY.id = "permaweapons"
CATEGORY.Desc = 'You spawn every round with the weapons you buy here'
CATEGORY.Order = 2
CATEGORY.buyWhenDeath = true
CATEGORY.AllowedEquipped = 15