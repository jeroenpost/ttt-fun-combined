ITEM.Name = 'Perma Lightsaber'
ITEM.Price = 2500
ITEM.Material = 'vgui/ttt_fun_pointshop_icons/lightsaber.png'
ITEM.WeaponClass = 'lightsaber'


function ITEM:OnEquip(ply)
    ply:StripWeapon('weapon_zm_improvised')
	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
    
	ply:StripWeapon(self.WeaponClass)
	ply:Give('weapon_zm_improvised')
end
