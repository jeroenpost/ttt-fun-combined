ITEM.Name = 'XM1014'
ITEM.Price = 2000
ITEM.Material = "materials/vgui/ttt_fun_pointshop_icons/xm1014.png"
ITEM.WeaponClass = 'weapon_zm_shotgun'

ITEM.AdminOnly = false

function ITEM:OnEquip(ply)
	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end