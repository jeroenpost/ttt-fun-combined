ITEM.Name = "Black's Frosted Fury"
ITEM.Price = 50
ITEM.Model  = 'models/weapons/w_frozen_acr.mdl'
ITEM.WeaponClass = 'custom_frozen'


ITEM.Owner =  "STEAM_0:0:82905466"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end


function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end