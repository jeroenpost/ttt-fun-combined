ITEM.Name = 'Perma JR mod/admin Deagle'
ITEM.Price = 55000
ITEM.Material = 'materials/vgui/ttt_fun_pointshop_icons/golden_deagle.png'
ITEM.WeaponClass = 'gb_jr_deagle'
ITEM.CanPlayerSell = false


function ITEM:CanPlayerBuy( ply )
    
    if ply:GetUserGroup() == "jr_mod" or ply:GetUserGroup() == "jr_admin"    then 
        return true
    end

    return false

end

function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end