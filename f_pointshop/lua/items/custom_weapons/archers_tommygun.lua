ITEM.Name = "Archer's Tommygun"
ITEM.Price = 60
ITEM.Model = "models/weapons/w_tommy_gun.mdl"
ITEM.WeaponClass = 'custom_archer_tommygun'
ITEM.Owner = "STEAM_0:0:63051220"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner or ply:IsUserGroup("superadmin") then 
        return true
    end
 
    return false
end


function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end 