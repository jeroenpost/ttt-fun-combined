ITEM.Name = "The Black Mamba"
ITEM.Price = 50
ITEM.Model  = 'models/mass_effect_3/weapons/sniper_rifles/m-13_Raptor.mdl'
ITEM.WeaponClass = 'custom_black_mamba'


ITEM.Owner = "STEAM_0:1:50394180"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end



function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end