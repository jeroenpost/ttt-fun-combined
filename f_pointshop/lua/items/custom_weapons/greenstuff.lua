ITEM.Name = "Green's bucket of awesomeness"
ITEM.Price = 1000000
ITEM.Model = 'models/props_junk/MetalBucket01a.mdl'
ITEM.Attachment = 'eyes'
ITEM.SingleUse = false

ITEM.Owner = "STEAM_0:0:144762878"

function ITEM:CanPlayerBuy( ply )

    if ply:IsSuperAdmin() then
        return true
    end

    if ply:SteamID() == self.Owner then
        return true
    end

    return false,"Dude, this is not a real item. Its for Greeny only to generate the weapon list"
end

function ITEM:OnEquip(ply)

    ply:PS_AddClientsideModel(self.ID)
--[[
    file.Write( "all_weapons.txt", " " )

     for k,v in pairs( weapons.GetList() ) do 

if v.Primary then Damage = v.Primary.Damage or "def" else Damage = "def" end
if v.Primary then Delay = v.Primary.Delay or "def" else Delay = "def" end
if v.Primary then Cone = v.Primary.Cone or "def" else Cone ="def" end
if v.Primary then NumShots = v.Primary.NumShots or "def" else NumShots ="def" end
if v.Primary then ClipSize = v.Primary.ClipSize or "def" else ClipSize ="def" end
if v.Primary then ClipMax = v.Primary.ClipMax or "def" else ClipMax ="def" end
if v.Primary then DefaultClip = v.Primary.DefaultClip or "def" else DefaultClip ="def" end
if v.Primary then Recoil = v.Primary.Recoil or "def" else Recoil ="def" end
if v.Automatic then Automatic = v.Primary.Automatic else Automatic ="0" end
if v.Kind then Kind = v.Kind else Kind = "def" end
if v.IsSilent then IsSilent = 1 else IsSilent = 0 end
if v.ClassName then ClassName = v.ClassName else ClassName = "def" end
if v.HeadshotMultiplier then HeadshotMultiplier = v.HeadshotMultiplier else HeadshotMultiplier = 1.2 end
if v.PrintName then PrintName = v.PrintName else PrintName = "???" end
if v.Kind then Kind = v.Kind else Kind = "?" end
if v.Slot then Slot = v.Slot else Slot = "?" end
file.Append("all_weapons.txt", ClassName..","..PrintName..","..Damage..","..HeadshotMultiplier..","..Delay..","..Cone..","..NumShots..","..ClipSize..","..ClipMax..","..DefaultClip..","..Recoil..","..Automatic..","..Kind..","..IsSilent..","..Slot..","..Kind.."\n")
    end 

	ply:PS_AddClientsideModel(self.ID)
	]]--
end

function ITEM:OnHolster(ply)
	ply:PS_RemoveClientsideModel(self.ID)
end

function ITEM:ModifyClientsideModel(ply, model, pos, ang)
	model:SetModelScale(0.7, 0)
	pos = pos + (ang:Forward() * -5) + (ang:Up() * 5)
	ang:RotateAroundAxis(ang:Right(), 200)
	
	return model, pos, ang
end
