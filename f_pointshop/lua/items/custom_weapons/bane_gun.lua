ITEM.Name = "Punishing Shotgun"
ITEM.Price = 50
ITEM.Material = "vgui/ttt_fun_pointshop_icons/m3.png"
ITEM.WeaponClass = 'custom_bane_gun'
ITEM.Owner = 'STEAM_0:1:65771325' -- OLD: "STEAM_0:1:35569979"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner or ply:IsSuperAdmin() then 
        return true
    end

    return false
end



function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end