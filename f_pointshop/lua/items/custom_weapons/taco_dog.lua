ITEM.Name = "Fly's Enforcer"
ITEM.Price = 50
ITEM.Model  = 'models/weapons/w_trh_92fs.mdl'
ITEM.WeaponClass = 'custom_taco_dog'

ITEM.Owner =  	"STEAM_0:0:61204070"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end

function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end