ITEM.Name = 'RifleKind2'
ITEM.Price = 6000
ITEM.Model = 'models/w_rif_l2552.mdl'
ITEM.WeaponClass = 'custom_riflekind2'
ITEM.CanPlayerSell = false


ITEM.Owner =  	"STEAM_0:1:36044530"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end



function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end