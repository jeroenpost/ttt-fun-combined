ITEM.Name =  "Olorin"
ITEM.Price = 600
ITEM.Model  = 'models/morrowind/gandal/mace/w_gandal_mace.mdl'
ITEM.WeaponClass = 'custom_olorin'
 

ITEM.Owner =  "STEAM_0:0:52688983" 

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end


function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end