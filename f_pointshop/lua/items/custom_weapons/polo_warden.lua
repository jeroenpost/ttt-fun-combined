ITEM.Name = "The Glaive"
ITEM.Price = 50
ITEM.Model = 'models/glaive/world/glaive.mdl'
ITEM.WeaponClass = 'custom_polo_glaive'

ITEM.Owner =  "STEAM_0:0:39767777" 

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner then 
        return true 
    end

    return false
end


function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end