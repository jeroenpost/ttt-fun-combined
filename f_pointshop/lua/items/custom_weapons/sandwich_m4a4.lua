ITEM.Name = "M4A4 || Asiimov"
ITEM.Price = 500
ITEM.Model  = "models/weapons/w_m4a4asim.mdl"
ITEM.WeaponClass = 'custom_sandwich_m4a4'
ITEM.Owner = "STEAM_0:1:74486829"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end

function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end
