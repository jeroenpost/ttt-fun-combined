ITEM.Name = "Pikachu's Lighting Bolt"
ITEM.Price = 50
ITEM.Model = 'models/weapons/w_snip_hex.mdl'
ITEM.WeaponClass = 'custom_pikachus_bold'
ITEM.CanPlayerSell = false


ITEM.Owner =  "STEAM_0:0:144762878"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end

function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end 