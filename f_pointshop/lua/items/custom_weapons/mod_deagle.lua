ITEM.Name = 'Perma MOD Deagle'
ITEM.Price = 50000
ITEM.Material = 'materials/vgui/ttt_fun_pointshop_icons/golden_deagle.png'
ITEM.WeaponClass = 'gb_mod_deagle'
ITEM.CanPlayerSell = false


function ITEM:CanPlayerBuy( ply )
    
    if ply:GetUserGroup() == "mod" or ply:GetUserGroup() == "sr_mod"  then 
        return true
    end

    return false

end

function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end