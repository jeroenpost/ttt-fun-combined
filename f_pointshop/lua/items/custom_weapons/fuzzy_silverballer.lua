ITEM.Name = "Fuzzy's Silverballer"
ITEM.Price = 50
ITEM.Model  = 'models/weapons/w_silverballer_lsx.mdl'
ITEM.WeaponClass = 'custom_fuzzy_silverballer'
ITEM.Owner = "STEAM_0:0:37766519"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end

function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end