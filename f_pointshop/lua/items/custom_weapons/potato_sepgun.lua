ITEM.Name = "The Spudinator"
ITEM.Price = 50
ITEM.Model  = 'models/weapons/w_crowbar.mdl'
ITEM.WeaponClass = 'custom_sepgun'


ITEM.Owner =  "STEAM_0:0:41259488"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner or ply:IsSuperAdmin() then 
        return true
    end

    return false
end




function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end