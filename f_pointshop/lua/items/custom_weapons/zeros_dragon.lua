ITEM.Name = "Zero's Dragon"
ITEM.Price = 6000
ITEM.Model = 'models/weapons/w_pist_zeagle.mdl'
ITEM.WeaponClass = 'custom_zero_dragon'

ITEM.Owner =  	"STEAM_0:0:41259488"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end



function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end