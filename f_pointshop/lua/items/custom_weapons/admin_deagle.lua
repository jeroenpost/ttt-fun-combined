ITEM.Name = 'Perma ADMIN Deagle'
ITEM.Price = 75000
ITEM.Material = 'materials/vgui/ttt_fun_pointshop_icons/golden_deagle.png'
ITEM.WeaponClass = 'gb_admin_deagle'
ITEM.CanPlayerSell = false


function ITEM:CanPlayerBuy( ply )
    
    if ply:GetUserGroup() == "admin" or ply:GetUserGroup() == "sr_admin" or ply:GetUserGroup() == "head_admin" or ply:GetUserGroup() == "manager" or ply:GetUserGroup() == "marcopolo"    then 
        return true
    end

    return false

end

function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end