ITEM.Name = "ACR"
ITEM.Price = 500
ITEM.Model  = 'models/mass_effect_3/weapons/sniper_rifles/m-13_Raptor.mdl'
ITEM.WeaponClass = 'custom_acr'
ITEM.Owner = "STEAM_0:0:63051220"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end

function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end