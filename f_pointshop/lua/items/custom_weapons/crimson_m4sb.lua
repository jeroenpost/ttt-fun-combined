ITEM.Name = "Silent M4"
ITEM.Price = 50
ITEM.Model = 'models/weapons/w_rif_m4kk.mdl'
ITEM.WeaponClass = 'custom_m4sb'

ITEM.Owner = "STEAM_0:0:144762878"

function ITEM:CanPlayerBuy( ply )
    
    if ply:SteamID() == self.Owner then 
        return true
    end

    return false
end



function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end 

