
    ITEM.Name = "Soda Mask"
    ITEM.Model = "models/props_junk/PopCan01a.mdl"
    ITEM.Price = 125000
    ITEM.Attachment = 'eyes'

    function ITEM:OnEquip(ply, modifications)
        ply:PS_AddClientsideModel(self.ID)
    end

    function ITEM:OnHolster(ply)
        ply:PS_RemoveClientsideModel(self.ID)
    end

    function ITEM:ModifyClientsideModel(player, entity, pos, ang)

        pos = pos + (ang:Up() * 4)
        ang:RotateAroundAxis(ang:Right(), -90)

        return entity, pos, ang

    end

