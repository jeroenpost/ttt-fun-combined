ITEM.IsPlayerModel = true
ITEM.Name = 'Rin'
ITEM.Price = 120000
ITEM.Model = 'models/rin.mdl'

function ITEM:OnEquip(ply, modifications)
	if not ply._OldModel then
		ply._OldModel = ply:GetModel()
    end
    ply:PrintMessage( HUD_PRINTTALK, "You have playermodel Rin. This model doesnt have a head to shoot at, so you deal 25% more damage")
    hook.Add("EntityTakeDamage","playermodelDamageIncreaseRin", function(ent, dmginfo)
        if ent:IsPlayer() and ent:GetModel() == 'models/rin.mdl' then
            dmginfo:ScaleDamage( 1.25 )
        else
            return
        end
    end)
	timer.Simple(1, function()  ply.PlayerModel = self.Model end) ply.PlayerModel = self.Model ply:SetModel( ply.PlayerModel )
end

function ITEM:OnHolster(ply)
	if ply._OldModel then
		ply:SetModel(ply._OldModel)
	end
end