ITEM.Name = "Megapulsator"
ITEM.Price = 50
ITEM.Model  =  "models/weapons/w_rif_famas.mdl"
ITEM.Skin = "camos/camo7"
ITEM.WeaponClass = 'donator_megapulsator'

function ITEM:CanPlayerBuy( ply )
    return false,"You can only get this weapon through the donation form"
end

ITEM.CanPlayerSell = function() return false,"You cannot sell custom weapons" end

function ITEM:OnEquip(ply)

	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end