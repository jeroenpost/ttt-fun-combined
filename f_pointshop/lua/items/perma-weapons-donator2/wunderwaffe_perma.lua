ITEM.Name = 'Wunderwaffe'
ITEM.Price = 125000
ITEM.Model = "models/wunderwaffe.mdl"
ITEM.WeaponClass = 'wunderwaffe'
ITEM.SingleUse = false

ITEM.AdminOnly = false

function ITEM:OnEquip(ply)
	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end