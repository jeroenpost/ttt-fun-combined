ITEM.Name = "USP-45"
ITEM.Price = 195000
ITEM.Material = "materials/vgui/ttt_fun_pointshop_icons/usp45.png"
ITEM.WeaponClass = 'weapon_ttt_sipistol'


ITEM.AdminOnly = false

function ITEM:OnEquip(ply)
	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end