ITEM.Name = "Tommygun"
ITEM.Price = 3000000
ITEM.Model = "models/weapons/w_tommy_gun.mdl"
ITEM.WeaponClass = 'custom_archer_tommygun'


function ITEM:OnEquip(ply)

    ply:Give(self.WeaponClass)
    ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
    ply:StripWeapon(self.WeaponClass)
end 