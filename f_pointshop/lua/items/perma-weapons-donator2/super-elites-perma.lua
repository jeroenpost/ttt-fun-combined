ITEM.Name = 'Super Elites'
ITEM.Price = 96000
ITEM.Material = "materials/vgui/ttt_fun_pointshop_icons/elites.png"
ITEM.WeaponClass = 'super_elites'


ITEM.AdminOnly = false

function ITEM:OnEquip(ply)
	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end