ITEM.Name = 'Flamethrower'
ITEM.Price = 250000
ITEM.Material = 'vgui/ttt_fun_pointshop_icons/flamethrower.png'
ITEM.WeaponClass = 'gb_flamethrower'



function ITEM:OnEquip(ply)
	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end
