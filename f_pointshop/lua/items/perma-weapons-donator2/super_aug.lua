ITEM.Name = 'Super Aug'
ITEM.Price = 90000
ITEM.Material = "materials/vgui/ttt_fun_pointshop_icons/aug.png"
ITEM.WeaponClass = 'super_aug'

function ITEM:OnEquip(ply)
	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end
