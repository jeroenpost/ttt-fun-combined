ITEM.Name = 'Dual Space Shotgun'
ITEM.Price = 95000
ITEM.Material = "materials/vgui/ttt_fun_pointshop_icons/dualspaceshotguns.png"
ITEM.WeaponClass = 'gb_dual_space_shotgun'


ITEM.AdminOnly = false

function ITEM:OnEquip(ply)
	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end