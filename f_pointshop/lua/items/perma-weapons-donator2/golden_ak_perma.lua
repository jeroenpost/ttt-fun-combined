ITEM.Name = 'AK47 Gold'
ITEM.Price = 25000
ITEM.Material = "materials/vgui/ttt_fun_pointshop_icons/ak47.png"
ITEM.WeaponClass = 'gb_gold_ak'

ITEM.AdminOnly = false

function ITEM:OnEquip(ply)
	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end