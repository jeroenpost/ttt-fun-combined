ITEM.Name = 'Super Famas'
ITEM.Price = 90000
ITEM.Material = "materials/vgui/ttt_fun_pointshop_icons/famas.png"
ITEM.WeaponClass = 'super_famas'

function ITEM:OnEquip(ply)
	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end
