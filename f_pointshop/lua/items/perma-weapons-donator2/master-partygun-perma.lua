ITEM.Name = 'Master Party Gun'
ITEM.Price = 150000
ITEM.Material = "materials/vgui/ttt_fun_pointshop_icons/partygun.png"
ITEM.WeaponClass = 'master_partygun'


function ITEM:OnEquip(ply)
	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end