ITEM.Name = 'P90'
ITEM.Price = 90000
ITEM.Material = "materials/vgui/ttt_fun_pointshop_icons/p90.png"
ITEM.WeaponClass = 'weapon_ttt_p90'

function ITEM:OnEquip(ply)
	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end
