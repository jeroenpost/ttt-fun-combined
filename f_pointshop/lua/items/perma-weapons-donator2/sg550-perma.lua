ITEM.Name = "SG-550"
ITEM.Price = 95000
ITEM.Material = "materials/vgui/ttt_fun_pointshop_icons/sg550.png"
ITEM.WeaponClass = 'weapon_ttt_sg550'


ITEM.AdminOnly = false

function ITEM:OnEquip(ply)
	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end