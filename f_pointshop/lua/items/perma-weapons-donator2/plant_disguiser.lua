ITEM.Name = "Plant Disguiser"
ITEM.Price = 95000
ITEM.Material = "materials/vgui/ttt_fun_pointshop_icons/plant-disguiser.png"
ITEM.WeaponClass = 'gb_plant_disguiser'


ITEM.AdminOnly = false

function ITEM:OnEquip(ply)
	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end