ITEM.Name = 'Super Glock18'
ITEM.Price = 70000
ITEM.Material = "materials/vgui/ttt_fun_pointshop_icons/glock18.png"
ITEM.WeaponClass = 'super_glock'

function ITEM:OnEquip(ply)
	 ply:Give(self.WeaponClass)
	ply:SelectWeapon(self.WeaponClass)
end

function ITEM:OnSell(ply)
	ply:StripWeapon(self.WeaponClass)
end
