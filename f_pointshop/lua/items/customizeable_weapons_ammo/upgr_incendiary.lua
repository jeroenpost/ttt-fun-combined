ITEM.Name = 'Incendiary'
ITEM.Price = 500000
ITEM.Model = "models/Items/BoxMRounds.mdl"
ITEM.Class = 'incendiary'
ITEM.Description = 'Incendiary'
ITEM.CanPlayerSell = false
ITEM.Description = [[Ignites hit target for 6 seconds.
Only ignites targets that are within the weapon's 50% effective range.
Loses ignitive ability after penetrating or ricocheting off a surface.
Deals 70% damage.
Decreases penetrative effectiveness by 25%]]

function ITEM:OnEquip(ply, modifications)
    timer.Simple(1,function()
        if not IsValid(ply) then return end
        if not ply:HasAmmoType( self.Class ) then
            ply:PickUpAmmoType( self.Class  )
        end
    end)
end

function ITEM:OnSell(ply)
    ply:RemoveAttachment( self.Class )
end
function ITEM:OnHolster(ply)
    self:OnSell(ply)
end