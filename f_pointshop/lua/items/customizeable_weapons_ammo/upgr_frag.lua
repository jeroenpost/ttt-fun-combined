ITEM.Name = 'Frag'
ITEM.Price = 500000
ITEM.Model = "models/Items/BoxMRounds.mdl"
ITEM.Class = 'frag'
ITEM.CanPlayerSell = false
ITEM.Description = [[Fires out a single very accurate explosive round.
Greatly affected by gravity.
Much slower than regular bullets.]]

function ITEM:OnEquip(ply, modifications)
    timer.Simple(1,function()
        if not IsValid(ply) then return end
        if not ply:HasAmmoType( self.Class ) then
            ply:PickUpAmmoType( self.Class  )
        end
    end)
end

function ITEM:OnSell(ply)
    ply:RemoveAttachment( self.Class )
end
function ITEM:OnHolster(ply)
    self:OnSell(ply)
end