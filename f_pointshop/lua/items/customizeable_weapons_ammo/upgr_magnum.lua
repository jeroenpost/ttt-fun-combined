ITEM.Name = 'Magnum'
ITEM.Price = 500000
ITEM.Model = "models/Items/BoxMRounds.mdl"
ITEM.Class = 'magnum'
ITEM.CanPlayerSell = false
ITEM.Description = [[Increases damage by 25%
Increases penetrative effectiveness by 10%
Increases recoil by 25%]]

function ITEM:OnEquip(ply, modifications)
    timer.Simple(1,function()
        if not IsValid(ply) then return end
        if not ply:HasAmmoType( self.Class ) then
            ply:PickUpAmmoType( self.Class  )
        end
    end)
end

function ITEM:OnSell(ply)
    ply:RemoveAttachment( self.Class )
end
function ITEM:OnHolster(ply)
    self:OnSell(ply)
end