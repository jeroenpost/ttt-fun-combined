ITEM.Name = 'BirdShot'
ITEM.Price = 500000
ITEM.Model = "models/Items/BoxMRounds.mdl"
ITEM.Class = 'birdshot'
ITEM.CanPlayerSell = false
ITEM.Description = [[Fires out 20 pellets.
Decreases recoil by 50%.
Greatly decreases accuracy.
Decreases penetrative effectiveness by 50%]]

function ITEM:OnEquip(ply, modifications)
    timer.Simple(1,function()
        if not IsValid(ply) then return end
        if not ply:HasAmmoType( self.Class ) then
            ply:PickUpAmmoType( self.Class  )
        end
    end)
end

function ITEM:OnSell(ply)
    ply:RemoveAttachment( self.Class )
end
function ITEM:OnHolster(ply)
    self:OnSell(ply)
end