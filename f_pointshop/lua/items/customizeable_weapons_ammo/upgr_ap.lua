ITEM.Name = 'Armor Piercing'
ITEM.Price = 500000
ITEM.Model = "models/Items/BoxMRounds.mdl"
ITEM.Class = 'ap'
ITEM.CanPlayerSell = false
ITEM.Description = [[Deals 130% damage to armored NPCs, players and props.
Increases penetrative effectiveness by 15%
Deals 70% damage to unarmored NPCs or players.]]



function ITEM:OnEquip(ply, modifications)
    timer.Simple(1,function()
        if not IsValid(ply) then return end
        if not ply:HasAmmoType( self.Class ) then
            ply:PickUpAmmoType( self.Class  )
        end
    end)
end

function ITEM:OnSell(ply)
    ply:RemoveAmmoType( self.Class )
end
function ITEM:OnHolster(ply)
    self:OnSell(ply)
end