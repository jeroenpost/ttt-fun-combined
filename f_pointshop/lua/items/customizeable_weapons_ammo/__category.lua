CATEGORY.Name = 'Ammotypes'
CATEGORY.Desc = 'Check the website on the different ammotypes. Cannot be sold when bought'
CATEGORY.Icon = 'emoticon_smile'
CATEGORY.Order = 4
CATEGORY.NotAllowedUserGroups = {}
CATEGORY.IsSubcategory = true
CATEGORY.SubcategoryChild = "customizeable"
