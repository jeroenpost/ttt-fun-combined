PS = {}
PS.__index = PS

PS.Items = {}
PS.ItemsClient = {}
PS.Categories = {}
PS.ClientsideModels = {}

-- validation

function PS:ValidateItems(items, ply)
	if type(items) ~= 'table' then return {} end
	
	-- Remove any items that no longer exist
	for item_id, item in pairs(items) do
		if not self.Items[item_id] then
			--items[item_id] = nil
        end
	end
	
	return items
end

function PS:ValidatePoints(points)
	if type(points) != 'number' then return 5000 end

    return points >= 0 and points or 0
end

-- Utils

function PS:FindCategoryByName(cat_name)
	for id, cat in pairs(self.Categories) do
		if cat.Name == cat_name then
			return cat
		end
	end
	
	return false
end

-- Initialization

function PS:Initialize()
	if SERVER then self:LoadDataProviders() end
	if SERVER and self.Config.CheckVersion then self:CheckVersion() end
end

-- Retry function


nextPsItemsCheck = 0





-- Loading

function PS:LoadItems()

    if SERVER then

        self:LoadItemsFromDB() --get all the items from the database
    end
	local _, dirs = file.Find('items/*', 'LUA')
	
	for _, category in pairs(dirs) do

        if without_customs then
            if string.find(string.lower(category), "custom") and not string.find(string.lower(category), "customi")  then
               continue
            end
        end

		local f, _ = file.Find('items/' .. category .. '/__category.lua', 'LUA')
		
		if #f > 0 then
            CATEGORY = {}

            CATEGORY.Name = ''
            CATEGORY.Icon = ''
            CATEGORY.Order = 0
            CATEGORY.AllowedEquipped = -1
            CATEGORY.AllowedUserGroups = {}
            CATEGORY.heavyserver = false
            CATEGORY.CanPlayerSee = function() return true end
			
			if SERVER then AddCSLuaFile('items/' .. category .. '/__category.lua') end
			include('items/' .. category .. '/__category.lua')
			
			if not PS.Categories[category] then
                if (heavyserver and CATEGORY.heavyserver) or not CATEGORY.heavyserver then
				        PS.Categories[category] = CATEGORY
                else
                    continue
                end

			end

                local files, _ = file.Find('items/' .. category .. '/*.lua', 'LUA')

                for _, name in pairs(files) do
                    if name ~= '__category.lua' then
                        if SERVER then AddCSLuaFile('items/' .. category .. '/' .. name) end

                        ITEM = {}

                        ITEM.__index = ITEM
                        ITEM.ID = string.gsub(name, '.lua', '')
                        ITEM.Category = CATEGORY.Name
                        ITEM.Price = 0

                        -- model and material are missing but there's no way around it, there's a check below anyway

                        ITEM.AdminOnly = false
                        ITEM.AllowedUserGroups = {} -- this will fail the #ITEM.AllowedUserGroups test and continue
                        ITEM.SingleUse = false
                        ITEM.NoPreview = false

                        ITEM.CanPlayerBuy = true
                        ITEM.CanPlayerSell = true
                        ITEM.heavyserver = false

                        ITEM.OnBuy = function() end
                        ITEM.OnSell = function() end
                        ITEM.OnEquip = function() end
                        ITEM.OnHolster = function() end
                        ITEM.OnModify = function() end
                        ITEM.ModifyClientsideModel = function(ITEM, ply, model, pos, ang)
                            return model, pos, ang
                        end

                        include('items/' .. category .. '/' .. name)

                        if ITEM.DontShow then
                            continue
                        end
                        if not ITEM.Name then
                            ErrorNoHalt("[POINTSHOP] Item missing name: " .. category .. '/' .. name .. "\n")
                            continue
                        elseif not ITEM.Price then
                            ErrorNoHalt("[POINTSHOP] Item missing price: " .. category .. '/' .. name .. "\n")
                            continue
                        elseif not ITEM.Model and not ITEM.Material then
                            ErrorNoHalt("[POINTSHOP] Item missing model or material: " .. category .. '/' .. name .. "\n")
                            continue
                        end

                        -- precache

                        if ITEM.Model then
                            util.PrecacheModel(ITEM.Model)
                        end

                        -- item hooks
                        local item = ITEM

                        for prop, val in pairs(item) do
                            if type(val) == "function" then -- although this hooks every function, it doesn't matter because the non-hook functions will never get called
                                hook.Add(prop, 'PS_Item_' .. item.Name .. '_' .. prop, function(...)
                                    for _, ply in pairs(player.GetAll()) do
                                        if ply:PS_HasItemEquipped(item.ID) then -- hooks are only called if the player has the item equipped
                                            item[prop](item, ply, ply.PS_Items[item.ID].Modifiers, unpack({...}))
                                        end
                                    end
                                end)
                            end
                        end

                        self.Items[ITEM.ID] = ITEM

                        ITEM = nil
                    end
                end

		end
	end
end

random_custom_weapons = {}
custom_db_weapons = {}
PS.ItemsClient = {}

function PS:LoadItemsFromDB()

    local numberofitems = 0

    if without_customs then return end

    print("Getting Custom weapons sell from database")

    http.Post( gb_json.settings.url, {['key'] = gb_json.settings.key,
        ['action']="get_custom_db_weapons" },
        function( body, len, headers, code )
            local results = util.JSONToTable( body )

        for _, t in ipairs( results ) do

            if not t.category or t.category == "" then continue end

            t.ps_price = tonumber( t.ps_price )
            t.ps_price_single = tonumber( t.ps_price_single )

            ITEM = nil
            ITEM2 = nil
            ITEMCLIENT = nil
            ITEM = {}
            ITEM2 = {}
            ITEMCLIENT = {}

            ITEM.Category = "Customs 4 Sale by Owner. Perma Use"
            ITEM.Price = 0

            ITEM.AdminOnly = false
            ITEM.AllowedUserGroups = {} -- this will fail the #ITEM.AllowedUserGroups test and continue
            ITEM.SingleUse = false
            ITEM.NoPreview = false


            ITEM.CanPlayerBuy = true
            ITEM.CanPlayerSell = function() return false,"You cannot sell custom weapons" end
            ITEM.heavyserver = false
            ITEM.Owner = t.owner
            ITEM.IsCustomDB = true

            ITEM.OnBuy = function(ply)

            end
            ITEM.OnSell = function() end
            ITEM.ParentId = t.ps_item_id
            ITEM.OnEquip = function(ply)

            end
            ITEM.OnHolster = function() end
            ITEM.OnModify = function() end
            ITEM.ModifyClientsideModel = function(ITEM, ply, model, pos, ang)
                return model, pos, ang
            end

            ITEM.WeaponClass = t.weaponclass
            ITEM.Name = t.name
            ITEM.ID = "custom_db_"..t.ps_item_id
            ITEM.Model = t.model
            ITEM.Material = t.material
            ITEM.Price = t.ps_price

            numberofitems = numberofitems + 1
            table.insert(random_custom_weapons, t.weaponclass)

            if not ITEM.Name then
                ErrorNoHalt("[POINTSHOP] Item missing name: /" .. name .. "\n")
                continue
                elseif not ITEM.Price then
                ErrorNoHalt("[POINTSHOP] Item missing price: /" .. name .. "\n")
                continue
                elseif not ITEM.Model and not ITEM.Material then
                ErrorNoHalt("[POINTSHOP] Item missing model or material: /" .. name .. "\n")
                continue
            end

            -- precache

            if ITEM.Model and ITEM.Model != "" then
                util.PrecacheModel(ITEM.Model)
            end

            -- item hooks
            local item = ITEM

            for prop, val in pairs(item) do
                if type(val) == "function" then -- although this hooks every function, it doesn't matter because the non-hook functions will never get called
                    hook.Add(prop, 'PS_Item_' .. item.Name .. '_' .. prop, function(...)
                        for _, ply in pairs(player.GetAll()) do
                            if ply:PS_HasItemEquipped(item.ID) then -- hooks are only called if the player has the item equipped
                                item[prop](item, ply, ply.PS_Items[item.ID].Modifiers, unpack({...}))
                            end
                        end
                    end)
                end
            end

                custom_db_weapons[ITEM.ID] = ITEM.ID

            if t.ps_price > 9 then
            PS.Items[ITEM.ID] = ITEM
            ITEMCLIENT = table.Copy(ITEM)

                ITEMCLIENT.OnBuy = true
                ITEMCLIENT.OnEquip = true
                ITEMCLIENT.OnSell = false
                ITEMCLIENT.OnHolster = true
                ITEMCLIENT.OnModify = true
                ITEMCLIENT.CanPlayerSell = false
                ITEMCLIENT.CanPlayerBuy = true
                ITEMCLIENT.ModifyClientsideModel = false
                PS.ItemsClient[ITEMCLIENT.ID] = ITEMCLIENT
            end


            if t.ps_price_single > 9 then
                ITEM2 = table.Copy(ITEM)


                ITEM2.SingleUse = true
                ITEM2.Price = t.ps_price_single

                if t.owner == "STEAM_0:0:59872576" then
                     ITEM2.Category = "Customs 4 Sale"
                else
                    ITEM2.Category = "Single Use Customs by Owner"
                end
                ITEM2.ID = "custom_db_"..t.ps_item_id.."_single"

                -- item hooks
                local item = ITEM2

                for prop, val in pairs(item) do
                    if type(val) == "function" then -- although this hooks every function, it doesn't matter because the non-hook functions will never get called
                        hook.Add(prop, 'PS_Item_' .. item.Name .. '_' .. prop, function(...)
                            for _, ply in pairs(player.GetAll()) do
                                if ply:PS_HasItemEquipped(item.ID) then -- hooks are only called if the player has the item equipped
                                    item[prop](item, ply, ply.PS_Items[item.ID].Modifiers, unpack({...}))
                                end
                            end
                        end)
                    end
                end

                PS.Items[ITEM2.ID] = table.Copy(ITEM2)


            ITEM2.OnBuy = false
                ITEM2.OnEquip = true
                ITEM2.OnSell = true
                ITEM2.OnHolster = true
                ITEM2.OnModify = true
                ITEM2.CanPlayerSell = false
                ITEM2.CanPlayerBuy = true
                ITEM2.ModifyClientsideModel = false
            PS.ItemsClient[ITEM2.ID] = ITEM2
            end


            end
            print("Loaded "..numberofitems.." custom sale weapons")
            end)


end