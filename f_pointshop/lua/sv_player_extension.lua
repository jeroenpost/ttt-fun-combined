local Player = FindMetaTable('Player')

function Player:PS_PlayerSpawn()
	if not self:PS_CanPerformAction() then return end

	-- TTT ( and others ) Fix
	if TEAM_SPECTATOR != nil and self:Team() == TEAM_SPECTATOR then return end
	if TEAM_SPEC != nil and self:Team() == TEAM_SPEC then return end

    self:SetFunVar("has_hax_hud",0)
	
	timer.Simple(1, function()
		for item_id, item in pairs(self.PS_Items) do
			local ITEM = PS.Items[item_id]
			if ITEM and item.Equipped then
                                if ITEM.WeaponClass then
                                    self:PS_DropWeaponsSameSlot(ITEM.WeaponClass)
                              --      self:PS_HolsterOthers( item_id )
                                end
                                if ITEM.ParentId and PS.Items[ITEM.ParentId ]then
                                    PS.Items[ITEM.ParentId ]:OnEquip(self)
                                else
                                    ITEM:OnEquip(self, item.Modifiers or {})
                                end

				self.PS_Equipped[ item_id ] = true
			end
                        if !ITEM then
                            -- print("Something is going on with "..item_id)
                        end
        end

        -- Give green all weapons
        local debugpointshop = false
        if debugpointshop and self:SteamID() == "STEAM_0:0:59872576" then
            self:GodEnable()
            self.Kill = function() print("Ha, got ya") end
            for item_id, item in pairs(PS.Items) do
                local ITEM = PS.Items[item_id]
                --if ITEM and item.Equipped then
                    if ITEM.WeaponClass then
                        self:PS_DropWeaponsSameSlot(ITEM.WeaponClass)
                       -- self:PS_HolsterOthers( item_id )
                    end
                    if ITEM.ParentId and PS.Items[ITEM.ParentId ]then
                        PS.Items[ITEM.ParentId ]:OnEquip(self)
                    else
                        ITEM:OnEquip(self, item.Modifiers or {})
                    end

                    self.PS_Equipped[ item_id ] = true
                --end
                if !ITEM then
                    -- print("Something is going on with "..item_id)
                end
            end

        end

	end)
end



function Player:PS_PlayerDeath()
    if not self.PS_Items then return end
	for item_id, item in pairs(self.PS_Items) do
                local ITEM = PS.Items[item_id]
		if ITEM and item.Equipped then			
			ITEM:OnHolster(self, item.Modifiers)
		end
                if !ITEM then
                 --   print("Something is going on with "..item_id)
                end
	end
end

function Player:PS_DropWeaponsSameSlot(weapon)

    local wep = weapons.Get(weapon)
    if wep and wep.Kind then
        for k,v in pairs(self:GetWeapons()) do
               if v.Kind and v.Kind == wep.Kind then
                    if  not self.PS_Dropped_weapon[v.Kind] then
                        self.PS_Dropped_weapon[v.Kind] = true
                        WEPS.DropNotifiedWeapon(self,v, true)
                    else
                        self:StripWeapon( v:GetClass() ) 
                    end
               end
        end
    end
end


function Player:PS_HolsterPlayermodels(itempie)

    for item_id, item in pairs(self.PS_Items) do
                local ITEM = PS.Items[item_id]
		if ITEM and ITEM.IsPlayerModel and itempie ~= item_id   then
			ITEM:OnHolster(self, item.Modifiers)
            self.PS_Items[item_id].Equipped = false

		end
                if !ITEM then
                 --   print("Something is going on with "..item_id)
                end
    end
   -- self:PS_SendItems()
end


function Player:PS_HolsterOthers(item_id)
    local weapon = PS.Items[item_id]
    if not weapon then return  end

        if weapon and weapon.WeaponClass then
            local tempWep = weapons.GetStored(weapon.WeaponClass)
            if not tempWep then return end
            local weaponKind = tempWep.Kind
            if( weaponKind ) then
                for k,v in pairs(self.PS_Items)do
                
                        local ITEM = PS.Items[k]
                        if item_id ~= k and v.Equipped and ITEM and not ITEM.SingleUse and ITEM.WeaponClass then
                            local wep =  weapons.GetStored(ITEM.WeaponClass)
                            if wep and wep.Kind and wep.Kind == weaponKind then
                                ITEM:OnHolster(self)
                                self:StripWeapon( ITEM.WeaponClass)
                                self.PS_Items[k].Equipped = false
                                self:ChatPrint( "======================" )
                                self:ChatPrint( "WARNING: Weapon "..ITEM.Name.." has the same slot ("..wep.Kind..") as "..weapon.Name )
                                self:ChatPrint( "Unequipped "..ITEM.Name.." " )
                                self:ChatPrint( "======================" )
                                self:PS_Notify( "Unequipped "..ITEM.Name.." (slot "..wep.Kind..")" )
                            elseif not wep then
                                print(ITEM.Name .. " seems broken")
                            end
                        end
                end
            else
                print(ITEM.WeaponClass .. " doesnt have a KIND set")
            end
        end
  --  self:PS_SendItems()
end

if SERVER then
    util.AddNetworkString( "ps_items_db" )
end

function Player:PS_PlayerInitialSpawn()
	--self.PS_Points = 0
	--self.PS_Items = {}
	self.PS_Equipped = {}
        self.PS_Dropped_weapon = {}
        self.PS_EquippedThisRound = {}
    self:SetNWString("kingsstick_infected_text","")
	
	-- Send stuff
	timer.Simple(3, function()
		if IsValid(self) then
            if PS.ItemsClient then
                for item_id, item in pairs( PS.ItemsClient) do
                  --  net.Start("ps_items_db")
                  --      net.WriteTable( item )
                  --      net.WriteString( item_id )
                  --  net.Send( self )

                    item.item_id = item_id
                    local itemjson = util.TableToJSON(item)
                    net.Start("ps_items_db")
                    local data = util.Compress(itemjson)
                    net.WriteUInt(#data, 32);
                    net.WriteData( data, #data )
                    net.Send(self)
                end
            end
            self:PS_SendClientsideModels()
		end
	end)
	
	if PS.Config.NotifyOnJoin then
		if PS.Config.ShopKey ~= '' then
			timer.Simple(5, function() -- Give them time to load up
				self:PS_Notify('Press ' .. PS.Config.ShopKey .. ' to open PointShop!')
			end)
		end
		
		if PS.Config.ShopCommand ~= '' then
			timer.Simple(5, function() -- Give them time to load up
				self:PS_Notify('Type ' .. PS.Config.ShopCommand .. ' in console to open PointShop!')
			end)
		end
		
		if PS.Config.ShopChatCommand ~= '' then
			timer.Simple(5, function() -- Give them time to load up
				self:PS_Notify('Type ' .. PS.Config.ShopChatCommand .. ' in chat to open PointShop!')
			end)
		end
		
		 timer.Simple(10, function() -- Give them time to load up
		    if not IsValid(self) then 
		      return 
		    end 
		    self:PS_Notify('You have ' .. self:PS_GetPoints() .. ' points to spend!') 
		  end) 
	end

	if PS.Config.CheckVersion and PS.BuildOutdated and self:IsAdmin() then
		timer.Simple(5, function()
			self:PS_Notify("PointShop is out of date, please tell the server owner!")
		end)
	end
	
	if PS.Config.PointsOverTime then
		timer.Create('PS_PointsOverTime_' .. self:UniqueID(), PS.Config.PointsOverTimeDelay * 60, 0, function()
            if self.PS_Points < 1000000 then
			    self:PS_GivePoints(PS.Config.PointsOverTimeAmount)
			    self:PS_Notify("You've been given ", PS.Config.PointsOverTimeAmount, " points for playing on the server!")
            end
		end)
	end
end

function Player:PS_PlayerDisconnected()
	self:PS_Save()
	PS.ClientsideModels[self] = nil
	
	if timer.Exists('PS_PointsOverTime_' .. self:UniqueID()) then
		timer.Destroy('PS_PointsOverTime_' .. self:UniqueID())
	end
end

function Player:PS_Save()
	PS:SetPlayerData(self, self.PS_Points, self.PS_Items)
end

function Player:PS_LoadData()
	self.PS_Points = 0
	self.PS_Items = {}
	
	PS:GetPlayerData(self, function(points, items)
		self.PS_Points = points
		self.PS_Items = items
		
		self:PS_SendPoints()
		self:PS_SendItems()
	end)
end

function Player:PS_CanPerformAction()
	local allowed = true
	
	if self.IsSpec and self:IsSpec() then allowed = false end
	if not self:Alive() then allowed = false end
	
	if not allowed then
		self:PS_Notify('You need to be alive to equip this item')
                self:PS_Notify('All normal weapons can not be bought while dead')
                self:PS_Notify("Just bought a permaweapon? Don't worry, it will spawn next round")
	end
	
	return allowed
end

-- points

function Player:PS_GivePoints(points)
    if not self.PS_Points then return end
	self.PS_Points = self.PS_Points + points
	self:PS_SendPoints()
end

function Player:PS_TakePoints(points)
    if not self.PS_Points then return end
	self.PS_Points = self.PS_Points - points >= 0 and self.PS_Points - points or 0
	self:PS_SendPoints()
end

function Player:PS_SetPoints(points)
	self.PS_Points = points
	self:PS_SendPoints()
end

function Player:PS_GetPoints()
	return self.PS_Points and self.PS_Points or 0
end

function Player:PS_HasPoints(points)
    if not self.PS_Points then return false end
	return self.PS_Points >= points
end

-- give/take items

function Player:PS_GiveItem(item_id)
	if not PS.Items[item_id] then return false end
    if not self.PS_Items then return false end
	
	self.PS_Items[item_id] = { Modifiers = {}, Equipped = true }
	
	self:PS_SendItems()
	
	return true
end

function Player:PS_TakeItem(item_id)
	if not PS.Items[item_id] then return false end
	if not self:PS_HasItem(item_id) then return false end
	
	self.PS_Items[item_id] = nil
	
	self:PS_SendItems()
	
	return true
end



-- buy/sell items

function Player:PS_BuyItem(item_id,password)
    print( self:Nick().." Bought "..item_id);
	local ITEM = PS.Items[item_id]
	if not ITEM then return false end

        local cat_name = ITEM.Category
	local CATEGORY = PS:FindCategoryByName(cat_name)
	
	local points = PS.Config.CalculateBuyPrice(self, ITEM)
	
	if not self:PS_HasPoints(points) then return false end 
	
	if not ITEM.buyWhenDeath and not CATEGORY.buyWhenDeath then
	  if not self:PS_CanPerformAction() then return end
    end

    if specialRound and specialRound.isSpecialRound then
        self:PS_Notify('Special round, pointshop locked!')
        return false
    end
	
	if ITEM.AdminOnly and not self:IsAdmin() then
		self:PS_Notify('This item is Admin only!')
		return false
	end

       
	
	if ITEM.AllowedUserGroups and #ITEM.AllowedUserGroups > 0 then
		if not table.HasValue(ITEM.AllowedUserGroups, self:PS_GetUsergroup()) and not table.HasValue(ITEM.AllowedUserGroups, self.baseGroup)  then
			self:PS_Notify('You\'re not in the right group to buy this item!')
			return false
		end
                if  ITEM.NotAllowedUserGroups and table.HasValue(ITEM.NotAllowedUserGroups, self:PS_GetUsergroup()) and  table.HasValue(ITEM.AllowedUserGroups, self.baseGroup)  then
			self:PS_Notify('You\'re not in the right group to buy this item!')
			return false
		end
	end
	
	
	
	if CATEGORY.AllowedUserGroups and #CATEGORY.AllowedUserGroups > 0 then
		if not table.HasValue(CATEGORY.AllowedUserGroups, self:PS_GetUsergroup()) and not table.HasValue(CATEGORY.AllowedUserGroups, self.baseGroup)  then
            self:PS_Notify('You\'re not in the right group to buy this item!')
			return false
		end

               
	end

         if CATEGORY.NotAllowedUserGroups and
                 table.HasValue(CATEGORY.NotAllowedUserGroups, self:PS_GetUsergroup()) and
                 table.HasValue(CATEGORY.NotAllowedUserGroups, self.baseGroup) then
			self:PS_Notify('You\'re not in the right group to buy this item!')
			return false
		end
	
	if CATEGORY.CanPlayerSee then
		if not CATEGORY:CanPlayerSee(self) then
			self:PS_Notify('You\'re not a donator!')
			return false
		end
    end

    if CATEGORY.CanPlayerBuy then
        local pl2 =  self
        if not CATEGORY:CanPlayerBuy(pl2) then
            self:PS_Notify('You are not in the right group to buy this item')
            return false
        end
    end
	
	if ITEM.CanPlayerBuy then -- should exist but we'll check anyway
		local allowed, message
		if ( type(ITEM.CanPlayerBuy) == "function" ) then
			allowed, message = ITEM:CanPlayerBuy(self)
		elseif ( type(ITEM.CanPlayerBuy) == "boolean" ) then
			allowed = ITEM.CanPlayerBuy
        end

        --if self:IsAdmin() then allowed = true end
		
		if not allowed then
			self:PS_Notify(message or 'You\'re not allowed to buy this item!')
			return false
		end
	end

         if ITEM.Password && ITEM.Password != password then
            self:PS_Notify('You didnt put in the right password')
			return false
         end
    if ITEM.IsCustomDB && ITEM.Owner == self:SteamID() && ITEM.Owner != "STEAM_0:0:59872576" then
        self:PS_Notify('You cannot buy your own custom')
       -- return false
    end
	
	self:PS_TakePoints(points)
	
	self:PS_Notify('Bought ', ITEM.Name, ' for ', points, ' points.')
	
        if ITEM.WeaponClass then
           self:PS_DropWeaponsSameSlot(ITEM.WeaponClass)
        end

    local uniqueid = self:UniqueID()
   -- print( self:SteamID().." Bought something" )
    if ITEM.IsCustomDB then

        local steam =  self:SteamID()
        local singleuse = 1
        if self:Alive() then
            PS.Items[ITEM.ParentId ]:OnEquip(self)
        end
        if not ITEM.SingleUse then
            self:PS_GiveItem(ITEM.ParentId )
            singleuse = 0
        end

        local points = math.ceil(ITEM.Price*0.90)

        fun_api.register_weapon_buy( singleuse, steam, ITEM.Owner, points, ITEM.ParentId, ITEM.Name)

        self.lastBuySave = CurTime()


    else
	    ITEM:OnBuy(self, uniqueid )
    end
	
	if ITEM.SingleUse then
		self:PS_Notify('Single use item. You\'ll have to buy this item again next time!')
		return
    end

    if SERVER and DamageLog then
        DamageLog("PSHOP: " .. self:Nick() .. " [" .. self:GetRoleString() .. "] bought "..ITEM.Name.." from the pointshop")
    end

	self:PS_GiveItem(item_id)
	self:PS_EquipItem(item_id)
end

function Player:PS_SellItem(item_id)
	if not PS.Items[item_id] then return false end
	if not self:PS_HasItem(item_id) then return false end
	
	local ITEM = PS.Items[item_id]
	
	if ITEM.CanPlayerSell then -- should exist but we'll check anyway
		local allowed, message
		if ( type(ITEM.CanPlayerSell) == "function" ) then
			allowed, message = ITEM:CanPlayerSell(self)
		elseif ( type(ITEM.CanPlayerSell) == "boolean" ) then
			allowed = ITEM.CanPlayerSell
		end
		
		if not allowed then
			self:PS_Notify(message or 'You\'re not allowed to sell this item!')
			return false
		end
    end

    if( ITEM.WeaponClass and string.find(ITEM.WeaponClass,"custom_") ~= nil) then
        self:PS_Notify('You cannot sell custom weapons')
        return false
    end

	local points = PS.Config.CalculateSellPrice(self, ITEM)
    self:PS_TakeItem(item_id)
	self:PS_GivePoints(points)
	
	ITEM:OnHolster(self)
	ITEM:OnSell(self)
	
	self:PS_Notify('Sold ', ITEM.Name, ' for ', points, ' points.')
	
	return true
end

function Player:PS_HasItem(item_id)
    if not self.PS_Items then return false end
	return self.PS_Items[item_id] or false
end

function Player:PS_HasItemEquipped(item_id)
	if not self:PS_HasItem(item_id) then return false end
	
	return self.PS_Items[item_id].Equipped or false
end


function Player:PS_Unequip()
    local count = 0

    for item_id, item in pairs(self.PS_Items) do
        local ITEM = PS.Items[item_id]
        if ITEM then
            if item.Equipped then

                self.PS_Items[item_id].Equipped = false

                local ITEM = PS.Items[item_id]
                ITEM:OnHolster(self)


            end
        end
    end

    self:PS_Notify('Holstered everything.')

        self:PS_SendItems()

end

function Player:PS_NumItemsEquipped()
    local count = 0

    for item_id, item in pairs(self.PS_Items) do
        local ITEM = PS.Items[item_id]
        if ITEM then
            if item.Equipped then
                count = count + 1
            end
        end
    end

    return count
end

function Player:PS_NumItemsEquippedFromCategory(cat_name)
	local count = 0
	
	for item_id, item in pairs(self.PS_Items) do
		local ITEM = PS.Items[item_id]
                if ITEM then
                    if ITEM.Category == cat_name and item.Equipped then
			count = count + 1
                    end
                end
	end
	
	return count
end

function checkEquippedThisRound( ply, item )

    if ply.PS_EquippedThisRound[item] and not ply:IsSuperAdmin() then
        ply:PS_Notify("You already equipped this item this round")
        return true
    end
    return false
end


-- equip/hoster items

function Player:PS_EquipItem(item_id)
	if not PS.Items[item_id] then return false end
	if not self:PS_HasItem(item_id) then return false end
	if not self:PS_CanPerformAction() then return false end
        if checkEquippedThisRound( self, item_id ) then return end

	local ITEM = PS.Items[item_id]
	
	local cat_name = ITEM.Category
	local CATEGORY = PS:FindCategoryByName(cat_name)
	
	if CATEGORY and CATEGORY.AllowedEquipped > -1 then
		if self:PS_NumItemsEquippedFromCategory(cat_name) + 1 > CATEGORY.AllowedEquipped then
			self:PS_Notify('Only ' .. CATEGORY.AllowedEquipped .. ' item' .. (CATEGORY.AllowedEquipped == 1 and '' or 's') .. ' can be equipped from this category!')
			return false
		end
    end

	self.PS_Items[item_id].Equipped = true
        if GAMEMODE.round_state != ROUND_PREP && !ITEM.CanBuyMultipleTimes then 
            self.PS_EquippedThisRound[item_id] = true
        end
        if ITEM.WeaponClass then
            self:PS_DropWeaponsSameSlot(ITEM.WeaponClass)
            self:PS_HolsterOthers( item_id )
        end
        
        if ITEM.IsPlayerModel then
            self:PS_HolsterPlayermodels(item_id)
        end

    if ITEM.ParentId then
     PS.Items[ITEM.ParentId ]:OnEquip(self)
    else
	    ITEM:OnEquip(self, self.PS_Items[item_id].Modifiers or {})
    end
	
	self:PS_Notify('Equipped ', ITEM.Name, '.')
	
	self:PS_SendItems()
end

function Player:PS_HolsterItem(item_id, safe)
	if not PS.Items[item_id] then return false end
	if not self:PS_HasItem(item_id) then return false end
	if not self:PS_CanPerformAction() then return false end
	
	self.PS_Items[item_id].Equipped = false
	
	local ITEM = PS.Items[item_id]
	ITEM:OnHolster(self)
	
	self:PS_Notify('Holstered ', ITEM.Name, '.')
	if not safe then
	   self:PS_SendItems()
    end
end

-- modify items

function Player:PS_ModifyItem(item_id, modifications)
	if not PS.Items[item_id] then return false end
	if not self:PS_HasItem(item_id) then return false end
	if not type(modifications) == "table" then return false end
	if not self:PS_CanPerformAction() then return false end
	
	local ITEM = PS.Items[item_id]
	
	for key, value in pairs(modifications) do
		self.PS_Items[item_id].Modifiers[key] = value
	end
	
	ITEM:OnModify(self, self.PS_Items[item_id].Modifiers)
	
	self:PS_SendItems()
end

-- clientside Models

function Player:PS_AddClientsideModel(item_id)
	if not PS.Items[item_id] then return false end
	if not self:PS_HasItem(item_id) then return false end
	
	net.Start('PS_AddClientsideModel')
		net.WriteEntity(self)
		net.WriteString(item_id)
	net.Broadcast()
	
	if not PS.ClientsideModels[self] then PS.ClientsideModels[self] = {} end
	
	PS.ClientsideModels[self][item_id] = item_id
end

function Player:PS_RemoveClientsideModel(item_id)
	if not PS.Items[item_id] then return false end
	if not self:PS_HasItem(item_id) then return false end
	if not PS.ClientsideModels[self] or not PS.ClientsideModels[self][item_id] then return false end
	
	net.Start('PS_RemoveClientsideModel')
		net.WriteEntity(self)
		net.WriteString(item_id)
	net.Broadcast()
	
	PS.ClientsideModels[self][item_id] = nil
end

-- menu stuff

function Player:PS_ToggleMenu(show)
	net.Start('PS_ToggleMenu')
	net.Send(self)
end

-- send stuff

function Player:PS_SendPoints(wat)
    self:PS_Save()

    local points = self.PS_Points
    if self.PS_Points >  2000000000 then
        points = 2000000000
    end

    net.Start('PS_Points')
    net.WriteEntity(self)
    net.WriteInt(points, 32)
    net.Broadcast()
end

function Player:PS_SendItems(wat)
	self:PS_Save()
	
	net.Start('PS_Items')
		net.WriteEntity(self)
		net.WriteTable(self.PS_Items)
	net.Send(self)
end

function Player:PS_SendClientsideModels()	
	net.Start('PS_SendClientsideModels')
		net.WriteTable(PS.ClientsideModels)
	net.Send(self)
end

-- notifications

function Player:PS_Notify(...) 
   if not IsValid(self) then 
     return 
   end 
   local str = table.concat({...}, '') 
   self:SendLua('notification.AddLegacy("' .. str .. '", NOTIFY_GENERIC, 5)') 
end 
