if SERVER then
    AddCSLuaFile()
end
local function DrawTarget( tgt, size, offset, no_shrink)

   local scrpos = tgt.pos:ToScreen() -- sweet
   local sz = (IsOffScreen(scrpos) and (not no_shrink)) and size/2 or size

   scrpos.x = math.Clamp(scrpos.x, sz, ScrW() - sz)
   scrpos.y = math.Clamp(scrpos.y, sz, ScrH() - sz)

   surface.DrawTexturedRect( scrpos.x - sz, scrpos.y - sz, sz * 2, sz * 2)

   if sz == size then

      local text = math.ceil(LocalPlayer():GetPos():Distance(tgt.pos))
      local w, h = surface.GetTextSize(text)

      if offset then
         surface.SetTextPos(scrpos.x - w/2, scrpos.y + (offset * sz) - h/2)
         surface.DrawText(text)
      else
         surface.SetTextPos(scrpos.x - w/2, scrpos.y - h/2)
         surface.DrawText(text)
      end

      if tgt.t then
         text = string.FormattedTime(tgt.t - CurTime(), "%02i:%02i")
         w, h = surface.GetTextSize(text)        
         surface.SetTextPos(scrpos.x - w / 2, scrpos.y + sz / 2)
         surface.DrawText(text)

      elseif tgt.nick then
         text = tgt.nick
         w, h = surface.GetTextSize(text)    

         surface.SetTextPos(scrpos.x - w / 2, scrpos.y + sz / 2)
         surface.DrawText(text)

      end
   end
end



local painst = Material("vgui/ttt_fun_killicons/death_station.png", "unlitgeneric")

hook.Add("HUDPaint", "DrawPSnTicon", function()

	if not LocalPlayer() then return end	

	--PAIN STATION ICON

	if LocalPlayer():IsTraitor() then
		surface.SetMaterial(painst)
		surface.SetTextColor(200, 55, 55, 0)
		surface.SetDrawColor(255, 255, 255, 150)
		for _, v in pairs( ents.FindByClass( "ttt_death_station" ) ) do
			DrawTarget({pos=v:GetPos()}, 24, nil, true)
		end
    end


end )


