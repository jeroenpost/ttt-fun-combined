if SERVER then
AddCSLuaFile( "shared.lua" )
--resource.AddFile("materials/vgui/ttt_fun_killicons/death_station.png")

--resource.AddFile("models/props/cs_office/microwave.dx80.vtx.")
--resource.AddFile("models/props/cs_office/microwave.dx90.vtx.")
--resource.AddFile("models/props/cs_office/microwave.mdl")
--resource.AddFile("models/props/cs_office/microwave.phy")
--resource.AddFile("models/props/cs_office/microwave.sw.vtx")
--resource.AddFile("models/props/cs_office/microwave.vvd")
--resource.AddFile("sound/weapons/big_explosion.mp3")


   
end

SWEP.HoldType = "normal"
   SWEP.PrintName = "Death Station"
   SWEP.Slot = 6

if CLIENT then


   SWEP.ViewModelFOV = 10

   SWEP.EquipMenuData = {
      type = "Poison",
      desc = [[ The Death Station is the opposite of 
	 the Health Station, it Kills instead of Healing. 
	 
	 You need to be hurt to be able to use it.]]
   };

   SWEP.Icon = "vgui/ttt_fun_killicons/death_station.png"
end

SWEP.Base = "weapon_tttbase"

SWEP.ViewModel          = "models/weapons/v_crowbar.mdl"
SWEP.WorldModel         = "models/props/cs_office/microwave.mdl"

SWEP.DrawCrosshair      = false
SWEP.Primary.ClipSize       = -1
SWEP.Primary.DefaultClip    = -1
SWEP.Primary.Automatic      = true
SWEP.Primary.Ammo       = "none"
SWEP.Primary.Delay = 1.0

SWEP.Secondary.ClipSize     = -1
SWEP.Secondary.DefaultClip  = -1
SWEP.Secondary.Automatic    = true
SWEP.Secondary.Ammo     = "none"
SWEP.Secondary.Delay = 1.0

SWEP.Kind = WEAPON_EQUIP
SWEP.CanBuy = {ROLE_TRAITOR} -- Traitor Weapon
SWEP.LimitedStock = false -- True = You can buy only once per round

SWEP.AllowDrop = false

SWEP.NoSights = true

function SWEP:OnDrop()
   self:Remove()
end

function SWEP:PrimaryAttack()
   self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
   self:DSDrop()
end
function SWEP:SecondaryAttack()
   self.Weapon:SetNextSecondaryFire( CurTime() + self.Secondary.Delay )
   self:DSDrop()
end

local throwsound = Sound( "Weapon_SLAM.SatchelThrow" )

function SWEP:DSDrop()
   if SERVER then
      local ply = self.Owner
      if not IsValid(ply) then return end

      if self.Planted then return end

      local vsrc = ply:GetShootPos()
      local vang = ply:GetAimVector()
      local vvel = ply:GetVelocity()
      
      local vthrow = vvel + vang * 200

      local ds = ents.Create("ttt_death_station")
      if IsValid(ds) then
         ds:SetPos(vsrc + vang * 10)
         ds:Spawn()

         ds:SetPlacer(ply)

         ds:PhysWake()
         local phys = ds:GetPhysicsObject()
         if IsValid(phys) then
            phys:SetVelocity(vthrow)
         end   
         self:Remove()

         self.Planted = true
      end
   end

   self.Weapon:EmitSound(throwsound)
end


function SWEP:Reload()
   return false
end

function SWEP:OnRemove()
   if CLIENT and IsValid(self.Owner) and self.Owner == LocalPlayer() and self.Owner:Alive() then
      RunConsoleCommand("lastinv")
   end
end

if CLIENT then
   function SWEP:Initialize()

      return self.BaseClass.Initialize(self)
   end
end

function SWEP:Deploy()
   if SERVER and IsValid(self.Owner) then
      self.Owner:DrawViewModel(false)
   end
   return true
end
 
function SWEP:DrawWorldModel()
end

function SWEP:DrawWorldModelTranslucent()
end

