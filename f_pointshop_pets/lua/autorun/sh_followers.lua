local Player = FindMetaTable( "Player" )
local Entity = FindMetaTable( "Entity" )

fo = {}

fo.SmoothAnimations	= false				// TURN THIS SETTING OFF TO DISABLE FAST ANIM, IMPROVES SERVER PERFORMANCE.
fo.ShowFirstPersonParticles = true		// Setting this to false will remove particle attachments from the owner's point of view.
fo.ShowFirstPersonFollower = true		// Setting this to false will disable the player's follower model from being show to him.

fo.EnableSounds = true					// Set this to false to disable sounds.
fo.SoundsMinTime = 15
fo.SoundsMaxTime = 60

fo.EnableAllParticles = true			// Set this to false to disable particles.

fo.ViewOffset = Vector( 0, 0, 20 );		// Offset when spectating a follower.
fo.Speed = 100;							// Overall speed.
fo.ForwardSpeed = 100					// Set the forward speed when controlling the entity.
fo.SideSpeed = 60						// Set the side speed of the controlled follower.
fo.UpSpeed = 20							// DOES NOT WORK YET LOL.
fo.Acceleration = 20;

fo.AllowIdleOrder = true;				// Enable/Disable the IDLE order.
fo.AllowChaseOrder = true;				// Enable/Disable the CHASE order.
fo.AllowControlOrder = true;			// Enable/Disable the CONTROLLED order.
fo.AllowControlCollitions = true;		// Enable/Disable the collision on pets when controlled.

fo.IdleOffset = Vector( 0, 0, 50 );		// Offset when idling.
fo.IdleTimer = 3						// Timer between idle commands for the player, so its not spammed.

fo.ChaseCircleSpeed = 150;				// Speed at which the ent circles around another entity.
fo.ChaseTimer = 3						// Timer between chase commands.

fo.Origin = Vector( 0, 0, 60 )			// Offset when following the player.
fo.Forward = -10
fo.Right = -20
fo.Up = 10
fo.Wobble = 4;
fo.WobbleSpeed = 1.5;
fo.MinSpeed = 0;
fo.MaxSpeed = 150;
fo.SpeedMult = 3;

// STATES
fo.STATE_IDLE			= 1
fo.STATE_FOLLOW 		= 2
fo.STATE_CHASE			= 3
fo.STATE_CONTROLLED		= 4

function Player:Fo_GetSpecEntity()
	return self:GetNWEntity( "fo_SpecEnt" );
end

function Player:Fo_ShouldSpec()
	local ent = self:Fo_GetSpecEntity()
	if ( IsValid( ent ) ) and ( ent:Fo_GetState() == fo.STATE_CONTROLLED ) then return true end
	return false;
end

function Entity:Fo_GetOwner()
	return self:GetNWEntity( "fo_PetOwner" );
end

function Entity:Fo_GetState()
	return self:GetNWInt( "fo_State" );
end

function Entity:Fo_Speed()
	return self.fo_Speed or 0;
end

function Entity:Fo_SetSpeed( value )
	self.fo_Speed = value 
end


// Sets the angle of the entity.
function Entity:Fo_SetAngles( offset )
	
	if ( not self.angWeight ) then self.angWeight = 0 end
	
	local state = self:Fo_GetState();
	local ply = self:Fo_GetOwner()
	local vel = self:GetVelocity()
	local speed = vel:LengthSqr() * 0.0005
	
	self:Fo_SetSpeed( speed )
	
	if speed > 1.5 then
		self.angWeight = math.Approach( self.angWeight, 1, FrameTime() * 3 )
	else
		self.angWeight = math.Approach( self.angWeight, 0, FrameTime() * 2.5)
	end
	
	offset = offset or 0
	
	local angMove = Angle( 0, vel:Angle().y + offset, 0 )
	local angStop = self:GetAngles();
	
	if ( state == fo.STATE_CONTROLLED ) then
		angMove = Angle( vel:Angle().p, vel:Angle().y + offset, 0 );
	end
	
	if ( IsValid( ply ) ) and ( ( state == fo.STATE_FOLLOW ) or ( state == fo.STATE_CONTROLLED ) ) then
		angStop = Angle( 0, ply:GetAngles().y + offset or 0, 0 )
	end
	
	self:SetAngles( LerpAngle( self.angWeight, angStop, angMove ) )
	
	return speed, self.angWeight

end

// When controling an entity.
hook.Add( "SetupMove", "Fo_SetupMove", function( ply, move )

	if ( ply:Fo_ShouldSpec() ) then
		
		move:SetUpSpeed( 0 );
		move:SetForwardSpeed( 0 );
		move:SetSideSpeed( 0 );
		move:SetVelocity( Vector( 0, 0, 0 ) );
	
	end

end )


// TF2 MODELS?
// models/bots/scout/bot_scout.mdl