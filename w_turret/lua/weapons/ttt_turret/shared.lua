
if SERVER then
    AddCSLuaFile( "shared.lua" )

    --resource.AddFile("materials/vgui/ttt/icon_turret.vtf");
    --resource.AddFile("materials/vgui/ttt/icon_turret.vmt");

end


SWEP.HoldType = "knife"
SWEP.ViewModelFOV = 74
SWEP.ViewModelFlip = false
SWEP.ShowViewModel = true
SWEP.ShowWorldModel = false
SWEP.ViewModel = "models/weapons/v_Grenade.mdl"
SWEP.WorldModel = "models/weapons/w_eq_smokegrenade.mdl"
SWEP.IronSightsPos = Vector(7.212, -5.41, 1.148)
SWEP.IronSightsAng = Vector(-4.016, -0.575, 28.114)

SWEP.VElements = {
    ["VTurret"] = { type = "Model", model = "models/Combine_turrets/Floor_turret.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(4.519, 1.417, 16.611), angle = Angle(-175.362, -44.231, 7.531), size = Vector(0.416, 0.416, 0.416), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
}
--SWEP.WElements = {
--        ["WTurret"] = { type = "Model", model = "models/Combine_turrets/Floor_turret.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(-2.806, 7.787, 19.087), angle = Angle(0, -39.237, -156.344), size = Vector(0.5, 0.5, 0.5), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
--}

SWEP.PrintName    = "Turret"
SWEP.Slot         = 6

if CLIENT then


    SWEP.SlotPos = 5
    SWEP.ViewModelFlip = false


    SWEP.Icon = "vgui/ttt_fun_killicons/turret"


end

SWEP.WElements = {
    ["turret"] = { type = "Model", model = "models/Combine_turrets/Floor_turret.mdl", bone = "ValveBiped.Bip01_R_Foot", rel = "", pos = Vector(19.545, -0.456, 12.272), angle = Angle(27.614, -25.569, 86.931), size = Vector(1,1,1), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
}

SWEP.Base               = "gb_base"
SWEP.Primary.NumShots       = 3
SWEP.DrawCrosshair      = false
SWEP.Primary.ClipSize       = -1
SWEP.Primary.DefaultClip    = -1
SWEP.Primary.Automatic      = true
SWEP.Primary.Delay = 0
SWEP.Primary.Ammo       = "none"
SWEP.Secondary.ClipSize     = -1
SWEP.Secondary.DefaultClip  = -1
SWEP.Secondary.Automatic    = true
SWEP.Secondary.Ammo     = "none"
SWEP.Secondary.Delay = 0

SWEP.Kind = WEAPON_EQUIP
SWEP.CanBuy = {ROLE_TRAITOR}
SWEP.LimitedStock = false
SWEP.WeaponID = AMMO_TURRET
SWEP.AllowDrop = true

SWEP.DeploySpeed = 2


if SERVER then

    function SWEP:PrimaryAttack()
        self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
        self.Weapon:SetNextSecondaryFire( CurTime() + self.Secondary.Delay )

        if self.Owner:GetRole() ~= ROLE_TRAITOR then
            self.Owner:PrintMessage( HUD_PRINTCENTER, "Only traitors can place turrets" )
            self:Remove()
        else

            if SERVER then self:SpawnTurret(); self:Remove() end
        end

    end

    function SWEP:SpawnTurret()

        if SERVER then DamageLog("Turret: " .. self.Owner:Nick() .. " [" .. self.Owner:GetRoleString() .. "] placed a turret ")
        end
        local ply = self.Owner
        local tr = ply:GetEyeTrace()
        if !tr.HitWorld then return end
        if tr.HitPos:Distance(ply:GetPos()) > 128 then return end
        local Views = self.Owner:EyeAngles().y
        local ent = ents.Create("npc_turret_floor")
        ent:SetOwner(ply)
        ent.fingerprints = self.fingerprints
        ent:SetPos(tr.HitPos + tr.HitNormal)
        ent:SetAngles(Angle(0, Views, 0))
        ent:Spawn()
        ent.IsTurret = true
        ent:SetPhysicsAttacker(self.Owner)
        ent:SetTrigger(true)
        ent.IsTurret = true
        ent:Activate()
        local entphys = ent:GetPhysicsObject();
        if entphys:IsValid() then
            entphys:SetMass(entphys:GetMass()+200)
        end

        self.Owner:StripWeapon("weapon_ttt_turret")
    end

end
function SWEP:Deploy()
    self:SecondaryAttack()
end

function SWEP:Equip()
    self.Weapon:SetNextPrimaryFire( CurTime() + (self.Primary.Delay * 1.5) )
    self.Weapon:SetNextSecondaryFire( CurTime() + (self.Secondary.Delay * 1.5) )
end

function SWEP:OnRemove()
    if CLIENT and IsValid(self.Owner) and self.Owner == LocalPlayer() and self.Owner:Alive() then
        RunConsoleCommand("lastinv")
    end
end

hook.Add("ShouldCollide", "TurretCollides", function(ent1, ent2)
    if ent1.IsTurret then
        if (ent2:IsPlayer() and ent2:GetRole() == ROLE_TRAITOR)  then
            return false
        end
    end

    if ent2.IsTurret then
        if (ent1:IsPlayer() and ent1:GetRole() == ROLE_TRAITOR) then
            return false
        end
    end

end)

hook.Add( "EntityTakeDamage", "TurretDamage", function( ent, dmginfo )
    if ent:IsPlayer() and not ent:IsGhost() and IsValid(dmginfo:GetInflictor()) and dmginfo:GetInflictor():GetClass() == "npc_turret_floor" then
        if ent:IsActiveTraitor() then
            --dmginfo:ScaleDamage(0)
            --return
        end
        dmginfo:ScaleDamage(3.5)
    end
end)

function SWEP:OnDrop()
    self:Remove()
end



SWEP.EquipMenuData = {
    type = "item_weapon",
    name = "Turret",
    desc = [[
Spawn a turret to shoot innocents]]
};