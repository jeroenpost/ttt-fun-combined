SWEP.PrintName		= "Molotov Cocktail"
SWEP.ViewModelFOV	= 75
SWEP.ViewModelFlip	= false
SWEP.ViewModel		= "models/weapons/v_molotov.mdl"
SWEP.WorldModel		= "models/props/cs_militia/bottle02.mdl"
SWEP.Base           = "gb_base"
SWEP.ReloadSound	= ""
SWEP.Kind = WEAPON_NADE
SWEP.Slot = 4
SWEP.SlotPos = 1
SWEP.CanBuy = { ROLE_TRAITOR }
SWEP.Icon   = "vgui/ttt_fun_killicons/molotov.png"
SWEP.AllowDrop = true
SWEP.EquipMenuData = {
   type = "Moltov",
   desc = "Burn something with this"
};
SWEP.DrawWorldModel = false
SWEP.DrawViewModel 	= true
SWEP.DrawShadow		= true

SWEP.HoldType		= "grenade"

SWEP.Spawnable		= true
SWEP.AdminSpawnable	= true

SWEP.WElements = {
	["bottle2"] = { type = "Model", model = "models/props/cs_militia/bottle02.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(3.181, 2.273, 2.273), angle = Angle(0, 0, -170.795), size = Vector(0.833, 0.833, 0.833), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
}

SWEP.Primary.ClipSize		= 4
SWEP.Primary.DefaultClip	= 4
SWEP.Primary.Automatic		= false
SWEP.Primary.Ammo			= "grenade"

SWEP.Secondary.ClipSize		= -1
SWEP.Secondary.DefaultClip	= -1
SWEP.Secondary.Automatic	= false	
SWEP.Secondary.Ammo			= "none"



function SWEP:PrimaryAttack()
	if !self:CanPrimaryAttack() then return end
	self:SetNextPrimaryFire(CurTime() + 1)
	self:SetHoldType( self.HoldType or "pistol" )
	self.Owner:EmitSound("WeaponFrag.Throw", 100, 100)
	self:SendWeaponAnim(ACT_VM_THROW)
	self.Owner:SetAnimation(PLAYER_ATTACK1)
	
	if (CLIENT) then return end

		local ent = ents.Create("ent_molotov_c")
		ent:SetPos(self.Owner:EyePos() + (self.Owner:GetAimVector() * 16))
		ent:SetAngles(self.Owner:GetAngles())
		ent:SetPhysicsAttacker(self.Owner)
		ent:SetOwner( self.Owner )
		ent:Spawn()
		
		local entobject = ent:GetPhysicsObject()
		entobject:ApplyForceCenter(self.Owner:GetAimVector():GetNormalized() * math.random(1500,2000))
	
	self:TakePrimaryAmmo(1)
	
end

function SWEP:SecondaryAttack()

end

function SWEP:Reload()
	self.Weapon:DefaultReload(ACT_VM_DRAW)
end

function SWEP:Think()

end

if SERVER then
--resource.AddFile("models/weapons/v_molotov.dx80.vtx")
--resource.AddFile("models/weapons/v_molotov.mdl")
--resource.AddFile("models/weapons/v_molotov.xbox.vtx")
--resource.AddFile("models/weapons/v_molotov.dx90.vtx")
--resource.AddFile("models/weapons/v_molotov.sw.vtx")
--resource.AddFile("models/weapons/v_molotov.vvd")
--resource.AddFile("sound/weapons/molotov/fire_ignite_2.wav")
--resource.AddFile("materials/models/weapons/w_molotov/molotov.vmt")
--resource.AddFile("materials/models/weapons/w_molotov/rag.vtf")
--resource.AddFile("materials/models/weapons/w_molotov/rag.vmt")
--resource.AddFile("materials/models/weapons/w_molotov/molotov.vtf")
--resource.AddFile("materials/vgui/ttt_fun_killicons/molotov.png")
--resource.AddFile("materials/vgui/entities/weapon_molotov_c.vmt")
--resource.AddFile("materials/vgui/entities/weapon_molotov_c.vtf")

end
