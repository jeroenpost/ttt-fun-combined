
if SERVER then
	AddCSLuaFile()
	
	--resource.AddFile( "sound/slenderambient.wav" )
	--resource.AddFile( "sound/slenderclose.wav" )
	--resource.AddFile( "sound/slenderlaugh.wav" )
	--resource.AddFile( "sound/spawn.wav" )
	--resource.AddFile( "sound/death.wav" )
	--resource.AddFile( "sound/iseeyou.wav" )
end

DEFINE_BASECLASS( "base_ai" )

ENT.Base = "base_ai" 
ENT.Type = "ai"
 
ENT.PrintName		= "Slenderman"
ENT.Author			= "NECROSSIN"
ENT.Contact			= ""
ENT.Purpose			= ""
ENT.Instructions	= ""

ENT.Category		= "Slender"

ENT.Spawnable = true
ENT.AdminOnly = true

 
ENT.AutomaticFrameAdvance = true
ENT.RenderGroup = RENDERGROUP_BOTH


ENT.SearchRange = 4000
 
//Initialize
function ENT:Initialize()

	if SERVER then
		self:SetModel( "models/player/lordvipes/slenderman/slenderman_playermodel_cvp.mdl" )
	 
		self:SetHullType( HULL_HUMAN )
		self:SetHullSizeNormal();

		self:SetSolid( SOLID_BBOX ) 
		self:SetMoveType( MOVETYPE_STEP )
		
		self:SetCollisionGroup( COLLISION_GROUP_DEBRIS )
		self:SetTrigger(true)
	 
		self:CapabilitiesAdd( bit.bor(CAP_MOVE_GROUND,CAP_OPEN_DOORS) )
	 
		self:SetNPCState( NPC_STATE_ALERT )
	 
		self:SetMaxYawSpeed( 3490 )
		
		self.StartTime = CurTime() + 2
		self.TouchEnt = nil
		self.NextTouch = 0
		self.TargetVisible = false
		self.Target = nil
		self.Seek = false
		
		self:DropToFloor()
		
		local ent = ents.Create( "slender_amb" )
		ent:SetPos(self:GetPos())
		ent:SetOwner(self)
		ent:SetParent(self)
		ent:Spawn()
		ent:Activate()
		
		self.Ghosting = false
		
		
		local bones = {
			["Bip01 Head"] = { scale = Vector(0.931, 0.931, 0.931), pos = Vector(0, 0, 0), angle = Angle(0, 0, 0) },
			["Bip01 R Thigh"] = { scale = Vector(0.83, 0.83, 0.83), pos = Vector(0, 0, 0), angle = Angle(0, 0, 0) },
			["Bip01 L Clavicle"] = { scale = Vector(1, 1, 1), pos = Vector(0, 0, -1.178), angle = Angle(0, 0, 0) },
			["Bip01 R Clavicle"] = { scale = Vector(1, 1, 1), pos = Vector(0, 0, -0.029), angle = Angle(-0.774, 0, 0) },
			["Bip01 R Forearm"] = { scale = Vector(1, 1, 1), pos = Vector(0, 0, 0), angle = Angle(0, 82.232, 0) },
			["Bip01 L Thigh"] = { scale = Vector(0.834, 0.834, 0.834), pos = Vector(0, 0, 0), angle = Angle(0, 0, 0) },
			["Bip01 Neck"] = { scale = Vector(0.69, 0.69, 0.69), pos = Vector(0, 0, 0), angle = Angle(0, 0, 0) }
		}
		
		for k, v in pairs( bones ) do
			local bone = self:LookupBone(k)
			if (!bone) then continue end
			self:ManipulateBoneScale( bone, v.scale  )
			self:ManipulateBoneAngles( bone, v.angle  )
			self:ManipulateBonePosition( bone, v.pos  )
		end

		
	end	
end
 
 

function ENT:PhysicsCollide( data, physobj )
end
 
function ENT:PhysicsUpdate( physobj )
end

if SERVER then

function ENT:SpawnFunction( pl, tr )

	if !IsValid(pl) then return end
		
	local ent = ents.Create( self.ClassName )
	ent:SetPos(tr.HitPos)
	ent:Spawn()
	ent:Activate()
	 
	return ent
end

	local schdStart = ai_schedule.New( "slenderStart" )
	--schdStart:EngTask( "TASK_WAIT", 	2 )
	

	local schdChase = ai_schedule.New( "slenderChase" )
	
	schdChase:AddTask( "FindTarget", 		0 )
	schdChase:EngTask( "TASK_GET_PATH_TO_ENEMY", 	0 )
	schdChase:EngTask( "TASK_USE_SMALL_HULL", 				0 )
	schdChase:EngTask( "TASK_FACE_ENEMY", 	0 )
	schdChase:EngTask( "TASK_RUN_PATH", 			0 )
	
	
	local schdChase2 = ai_schedule.New( "slenderChase2" )
	
	schdChase2:AddTask( "PlaySequence", { Name = "walk_all", Speed = 1 } )
	--schdChase2:EngTask( "TASK_STOP_MOVING", 			0 )


function ENT:TaskStart_FindTarget( data )

	local target = self:GetClosestTarget()
	if IsValid(target) then
		self:SetEnemy( target, true )
		self:UpdateEnemyMemory( target, target:GetPos() )
		self:TaskComplete()
		return
	end

	
	self:SetEnemy( NULL )	

end

function ENT:Task_FindTarget( data )
end

 
function ENT:Attack(target)
	if (self.NextAttack or 0) > CurTime() then return end
	self.NextAttack = CurTime() + 1

	local dmginfo = DamageInfo()
	dmginfo:SetDamageType(DMG_ACID)
	dmginfo:SetDamagePosition(target:GetPos())
	dmginfo:SetAttacker(self)
	dmginfo:SetInflictor(self)
	dmginfo:SetDamage(9999999999)
	dmginfo:SetDamageForce(90000 * (target:GetPos()-self:GetPos()):GetNormal()) 
	
	target:TakeDamageInfo(dmginfo)

	self.Entity:EmitSound("slenderlaugh.wav")
	
end

 
 function ENT:OnTakeDamage(dmg)
 end 
 
 function ENT:StartTouch ( hitEnt)

	if self.NextTouch > CurTime() then return end

	if hitEnt:IsValid() and hitEnt:IsPlayer() then
 		self:Attack(hitEnt)
		self.NextTouch = CurTime() + 1
	end
 end
 
function ENT:SelectSchedule()
	if self.StartTime > CurTime() then
		--self:StartSchedule( schdStart )
	else			
		local target = self:GetClosestTarget()
			if IsValid(target) then
				self.Target = target
				
				if self:EnemyVisible(target) then
					self:StartSchedule( schdChase2 )
					self.TargetVisible = true
					self.Seek = false
				else
					self:StartSchedule( schdChase2 )
					self.TargetVisible = false
				end
			end
	end
 
end

function ENT:Think()
	local ct = CurTime()
	self:DoGhosting(self.Target)	
	self:NextThink(ct)
	
end

function ENT:DoGhosting(target)
	
	if !IsValid(target) then return end
	
	local dest = target:GetPos()+vector_up
	local cur = self.Entity:GetPos()
	
	local dir = (dest-cur):GetNormal()
	local ang = dir:Angle()
	
	/*if self:EnemyVisible(target) then 
		if self.Ghosting then
			if math.random(3) == 1 then self:EmitSound("iseeyou"..math.random(2,10)..".wav") end
			self.Ghosting = false				
			//self:DropToFloor()			
		end
	
		if dir.z > 0 then dir.z = 0 end
			
		ang = dir:Angle()
			
		self.Entity:SetAngles(Angle(0,ang.y,ang.r))
		
		local final = cur + dir * 4
			
		self:SetPos(final)
	
		return 
	end*/
	
	if !self.Ghosting then
		if math.random(3) == 1 then self:EmitSound("iseeyou"..math.random(2,4)..".wav") end
		self.Ghosting = true
		
	end	
	
	if (self.NextDecal or 0) < CurTime() then
		
		for i=1,2 do
			local tr = {}
			tr.start = self.Entity:GetPos()+vector_up*30*i-dir*30
			tr.endpos = self.Entity:GetPos()+vector_up*30*i-dir*50
			tr.filter = self.Entity
			--tr.mask = MASK_NPCWORLDSTATIC
					
			local trace = util.TraceLine(tr)
					
			if trace.Hit then
				util.Decal("MetalStain"..math.random(3,8),trace.HitPos + trace.HitNormal,trace.HitPos - trace.HitNormal)
			end
			
			local tr1 = {}
			tr1.start = self.Entity:GetPos()+vector_up*30*i+dir*30
			tr1.endpos = self.Entity:GetPos()+vector_up*30*i+dir*50
			tr1.filter = self.Entity
			--tr1.mask = MASK_NPCWORLDSTATIC
				
			local trace1 = util.TraceLine(tr1)
				
			if trace1.Hit then
				util.Decal("MetalStain"..math.random(3,8),trace1.HitPos + trace1.HitNormal,trace1.HitPos - trace1.HitNormal)
			end
		end
			self.NextDecal = CurTime() + 0.2
	end
	
	self.Entity:SetAngles(Angle(0,ang.y,ang.r))
		
	local final = cur + dir * 20

	self:SetPos(final)
	
end

end

function ENT:EnemyVisible(target)
	
	if !IsValid(target) then return true end
	
	local tr = {}
	
	tr.start = self:GetPos()+vector_up*10
	tr.endpos = target:GetPos()+vector_up*10
	tr.filter = {self.Entity}
	
	local trhull = {}
	trhull.start = self.Entity:GetPos()+Vector(0,0,5)
	trhull.endpos = target:GetPos()+Vector(0,0,5)
	trhull.mins = self.Entity:OBBMins()
	trhull.maxs = self.Entity:OBBMaxs()
	trhull.filter = self.Entity
	
	local trace = util.TraceLine(trhull)
	local trace2 = util.TraceLine(tr)
	
	local result = true
	
	if trace.Hit then
		
		if trace.HitWorld then result = false end
		
		if trace.Entity and !trace.Entity:IsPlayer() then result = false end
	
	end
	
	if trace2.Hit then
		
		if trace2.HitWorld then result = false end
		
		if trace2.Entity and !trace2.Entity:IsPlayer() then result = false end
	
	end
	
	//check if he got stuck
	local hull = {}
	
	hull.start = self.Entity:GetPos()+Vector(0,0,2)
	hull.endpos = self.Entity:GetPos()+Vector(0,0,2)
	hull.mins = self.Entity:OBBMins()
	hull.maxs = self.Entity:OBBMaxs()
	hull.filter = self.Entity
				
	local trhull = util.TraceHull( hull )
	
	if trhull.Hit then result = false end
	
	
	return result
	
end

function ENT:GetClosestTarget()
	local Closest = self.SearchRange or 1000
	local dist = 0
	local Ent = nil
		for k, v in pairs(player.GetAll()) do
			dist = v:GetPos():Distance( self.Entity:GetPos() )
				if( dist < Closest) then
					if v:IsPlayer() and v:Alive() then
						Closest = dist
						Ent = v
					end
				end
		end

	return Ent

end
if CLIENT then
function ENT:Draw()
	self:DrawModel()
end

function ENT:SetRagdollBones( bIn )

	self.m_bRagdollSetup = bIn
 
end
 
function ENT:DoRagdollBone( PhysBoneNum, BoneNum )
 
end

end

function ENT:SetAutomaticFrameAdvance( bUsingAnim )
 
	self.AutomaticFrameAdvance = bUsingAnim
 
end
 
if SERVER then
function ENT:UpdateTransmitState()
	return TRANSMIT_ALWAYS
end
end

