
if SERVER then
	AddCSLuaFile("shared.lua")
end

ENT.Type = "anim"
ENT.RenderGroup         = RENDERGROUP_TRANSLUCENT

--add nessesary decals
game.AddDecal( "MetalStain1",					"decals/decalstain013a" );
game.AddDecal( "MetalStain2",					"decals/decalstain014a" );
game.AddDecal( "MetalStain3",					"decals/decalstain006a" );
game.AddDecal( "MetalStain4",					"decals/decalstain009a" );
game.AddDecal( "MetalStain5",					"decals/decalstain005a" );
game.AddDecal( "MetalStain6",					"decals/decalstain007a" );
game.AddDecal( "MetalStain7",					"decals/decalstain003a" );
game.AddDecal( "MetalStain8",					"decals/decalstain008a" );

//Note: These sounds are from slender, and the hidden: source beta 4a so all credits goes to whoever made them

function ENT:Initialize()
	if SERVER then
		self.Entity:DrawShadow(false)
	end
	if CLIENT then
		self.Ambient = CreateSound( LocalPlayer(), "slenderambient.wav" )
		self.Close = CreateSound( LocalPlayer(), "slenderclose.wav" )
		self.Breath = CreateSound( self, "spawn.wav" )
	end
end

if SERVER then
function ENT:Think()
	if !IsValid(self:GetOwner()) then
		self:Remove()
	end
end	
end

if CLIENT then

function ENT:Think()
	local max = 2000
	
	if IsValid(self:GetOwner()) then
		max = self:GetOwner().SearchRange or 2000
	end
	
	local pos = LocalPlayer():GetPos()
	
	local dist = pos:Distance(self:GetPos())
	
	if self.Ambient then
		self.Ambient:PlayEx(math.Clamp((max+10-dist)/max,0,1), 100)
	end
	if self.Close then
		self.Close:PlayEx(math.Clamp((dist+10-dist)/dist,0,1), 100)
	end	
	
	if self.Breath then
		self.Breath:PlayEx(1,100)
	end
	
end



function ENT:Draw()

end

end

