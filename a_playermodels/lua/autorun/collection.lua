if SERVER then
    AddCSLuaFile()
end
local function AddPlayerModel22( name, model )
    list.Set( "PlayerOptionsModel", name, model )
    player_manager.AddValidModel( name, model )
end

local function AddNPC( name, machinename, model, health, type, category, class)
    local NPC = { 	Name = name,
        Class = class,
        Model = model,
        Health = health,
        KeyValues = { citizentype = type },
        Category = category	}
    list.Set( "NPC", machinename, NPC )
end


AddPlayerModel22(  "Lightning", "models/player/lightning.mdl"   )
AddPlayerModel22( "James Bond", "models/player/bond.mdl" )
AddPlayerModel22( "Saddam", "models/jessev92/player/misc/saddam.mdl" )
AddPlayerModel22(     "Tyrel", 	"models/player/tyrael/tyrael.mdl" )
AddPlayerModel22(     "Divii's Werewolf", 	"models/player/stenli/lilith.mdl" )
 AddPlayerModel22( "Corvo", 	"models/player/corvo.mdl" );
 AddPlayerModel22( "US Soldier Masked", "models/minson97/bo2/seal_masked.mdl" )
 --AddPlayerModel22(  "Atlas", 			"models/bots/survivor_mechanic.mdl" )
 AddPlayerModel22( "batman", "models/Batman/slow/jamis/mkvsdcu/batman/slow_pub_v2.mdl" )
 AddPlayerModel22( "Daedra", 	"models/player/daedric.mdl" );
 AddPlayerModel22(  "Darkrai","models/player/player_darkrai/darkrai.mdl" )
 AddPlayerModel22(  "dogggy", "models/glayer_dog.mdl" )
 AddPlayerModel22(  "dog2", "models/blacky_dog.mdl" )
 AddPlayerModel22(  "dokuro chan",     "models/player/dokuro_chan_pm.mdl" )
 AddPlayerModel22( "DragonKnightFemale", "models/nikout/mass_effect_2/dragon_knight_female.mdl" )
 AddPlayerModel22( "DragonKnightMale", "models/nikout/mass_effect_2/dragon_knight_male.mdl" )
 AddPlayerModel22(  "geth", "models/voxelzero/player/geth.mdl" )
 AddPlayerModel22(  "Hatsune Miku", "models/player/miku.mdl" )
 AddPlayerModel22( "Juggernaut", "models/dota_heroes/juggernaut_player_v1.mdl" );
 AddPlayerModel22(  "Kasane teto", "models/player/kasane_teto.mdl" )
AddPlayerModel22( "Sauron", "models/koz/lotr/sauron/sauron.mdl" )
AddPlayerModel22( "Gandalf", "models/koz/lotr/gandalf/gandalf.mdl" )
AddPlayerModel22( "Moon Knight", "models/player/moon_knight/slow_v2.mdl" )
AddPlayerModel22( "The Hidden", "models/player/hidden/hidden.mdl" )
AddPlayerModel22( "Monster", 		"models/player/weird.mdl" );
AddPlayerModel22( "Lucario", "models/smashbros/lucario_player/lucario_player.mdl" )
AddPlayerModel22( "Neo Heavy", "models/player/neo_heavy.mdl" );
AddPlayerModel22(  "Obama",                   "models/obama/Obama.mdl" )
AddPlayerModel22( "OptimusPrime", "models/player/optimus_prime.mdl" )
AddPlayerModel22(  "Iron Patriot", 				"models/avengers/iron_man/patriot_player.mdl" )
AddPlayerModel22(  "Pedo Bear", 				"models/player/pbear/pbear.mdl" )
AddPlayerModel22( "Phazo Samus", 	"models/player/phazosamus.mdl" );
AddPlayerModel22(  "Wekser", "models/player/pil/re1/wesker/wesker_pil.mdl" )
AddPlayerModel22(  "Slender", 					"models/player/lordvipes/slenderman/slenderman_playermodel_cvp.mdl" )
AddPlayerModel22(  "Snivy", 					"models/player/snivy.mdl" )
--AddPlayerModel22( "Sonic", "models/player/sonic.mdl" );
--AddPlayerModel22( "Miles_'Tails'_Prower", "models/player/tails.mdl" );
AddPlayerModel22(  "Venom", 						"models/player/venom.mdl" )
AddPlayerModel22(  "Yahtzee", 						"models/player/Yahtzee.mdl" )

AddPlayerModel22(  "Dude", 						"models/player/dude.mdl" )
AddPlayerModel22(  "Faith", 						"models/player/faith.mdl" )
AddPlayerModel22(  "Harry Potter", 						"models/player/harry_potter.mdl" )
AddPlayerModel22(  "Zelda", 						"models/player/zelda.mdl" )

AddPlayerModel22(  "Rin","models/Rin.mdl")
AddNPC("Rin","npc_rin","models/rin.mdl","100",4,"GB NPCs","npc_citizen");
AddNPC("Zelda","npc_zelda","models/player/zelda.mdl","100",4,"GB NPCs","npc_citizen");
