-- Variables that are used on both client and server

  SWEP.PrintName = "Hoverboard Spawner";
   SWEP.Slot = 1;
   SWEP.SlotPos = 1;

SWEP.Author			= ""
SWEP.Contact			= ""
SWEP.Purpose			= ""
SWEP.Instructions		= ""

SWEP.ViewModel			= "models/weapons/c_toolgun.mdl"
SWEP.WorldModel			= "models/weapons/w_toolgun.mdl"
SWEP.AnimPrefix			= "python"


SWEP.UseHands = true
SWEP.Spawnable				= true
SWEP.AdminSpawnable			= true
SWEP.Kind = WEAPON_EQUIP
--SWEP.CanBuy = {ROLE_DETECTIVE,ROLE_TRAITOR} -- only traitors can buy
SWEP.Base   = "weapon_tttbase"
SWEP.Icon = "vgui/ttt_fun_pointshop_icons/hoverboard.png"
SWEP.ViewModelFlip = false

-- Be nice, precache the models
util.PrecacheModel( SWEP.ViewModel )
util.PrecacheModel( SWEP.WorldModel )

-- Todo, make/find a better sound.
SWEP.ShootSound			= Sound( "Airboat.FireGunRevDown" )



SWEP.Primary = 
{
	ClipSize 	= 1,
	DefaultClip = 1,
	Automatic = false,
	Ammo = "none"
}

SWEP.Secondary = 
{
	ClipSize 	= -1,
	DefaultClip = -1,
	Automatic = false,
	Ammo = "none"
}

SWEP.CanHolster			= true
SWEP.CanDeploy			= true


local HoverboardTypes = {}

table.insert( HoverboardTypes, {
	bonus = {
		twist = 6,
		flip = 6,
		speed = 6,
		jump = 6,
		turn = 6
	},
	model = "models/squint_hoverboard/hotrod.mdl",
	name = "HotRod",
	rotation = 90,
	driver = Vector( 1.5, 0, -1.5 ),
	effect_1 = {
		effect = "plasma_thruster_middle",
		position = Vector( 11, 22, 0 ),
		normal = Vector( 0, 1.85, 0.90 ),
	},
	effect_2 = {
		effect = "plasma_thruster_middle",
		position = Vector( -11, 22, 0 ),
		normal = Vector( 0, 1.85, 0.90 ),
	},
	effect_3 = {
		effect = "plasma_thruster_middle",
		position = Vector( 11, 27.5, 0 ),
		normal = Vector( 0, 1.85, 0.90 ),
	},
	effect_4 = {
		effect = "plasma_thruster_middle",
		position = Vector( -11, 27.5, 0 ),
		normal = Vector( 0, 1.85, 0.90 ),
	},
	effect_5 = {
		effect = "plasma_thruster_middle",
		position = Vector( 11, 30, 0 ),
		normal = Vector( 0, 1.85, 0.90 ),
	},
	effect_6 = {
		effect = "plasma_thruster_middle",
		position = Vector( -11, 30, 0 ),
		normal = Vector( 0, 1.85, 0.90 ),
	},
	effect_7 = {
		effect = "trail",
		position = Vector( 8.5, 40, -2.5 ),
	},
	effect_8 = {
		effect = "trail",
		position = Vector( -8.5, 40, -2.5 ),
	},
	files = {
		"materials/models/squint_hoverboard/hotrod/hotrod.vmt",
		"materials/models/squint_hoverboard/hotrod/hotrod.vtf",
	}
} )




-- loop board types
for _, hbt in pairs( HoverboardTypes ) do

	-- add to list
	list.Set( "HoverboardModels", hbt[ 'model' ], {} );
	
	if ( SERVER ) then
	
		-- send model
		--resource.AddFile( hbt[ 'model' ] );
		
		-- send other files
		if ( hbt[ 'files' ] ) then
		
			for __, f in pairs( hbt[ 'files' ] ) do
			
				--resource.AddFile( f );
			
			end
		
		end
		
	end
	
	-- precache
	util.PrecacheModel( hbt[ 'model' ] );

end



function SWEP:PrimaryAttack( trace )

	-- do shit
	local result, hoverboard = self:CreateBoard( trace );
	
if SERVER then
	self.Owner:StripWeapon("hoverboard");
end
	-- done
	return result;

end


function SWEP:RightClick( trace )

	-- do shit
	local result, hoverboard = self:CreateBoard( trace );
	
	-- client result
	if ( CLIENT ) then
	
		return result;
		
	end
	
	-- validate board
	if ( IsValid( hoverboard ) ) then
	
		-- owner
		local pl = self:GetOwner();
	
		-- check distance
		local dist = ( hoverboard:GetPos() - pl:GetPos() ):Length();
		
		-- make sure its relatively close?
		if ( dist <= 256 ) then
		
			-- had to delay it to avoid errors
			timer.Simple( 0.25, function( h, p )
			
				-- validate
				if ( IsValid( h ) && IsValid( p ) ) then
				
					-- bam!
					h:SetDriver( p );
					
				end
				
			end, hoverboard, pl );
		 
		end
	
	end
	
	return result;

end



function SWEP:CreateBoard( trace )

	-- get owner
	local pl = self:GetOwner();
	local trace = pl:GetEyeTraceNoCursor();

	-- client is done
	if ( CLIENT ) then
	
		return true;
		
	end


		 



	
	-- get values
local board = HoverboardTypes[ math.random(#HoverboardTypes)]

	local model = board["model"];
	print(model)
	local mcontrol = 0;
	local shake = 0;
	local trailsize = math.Clamp( 5, 0, 10 );
	local height = math.Clamp( 55, 36, 100 );
	local viewdist = math.Clamp( 128, 64, 256 );
	local trail = Vector( 128, 128, 255 );
	local boost = Vector( 128, 255, 128 );
	local recharge = Vector( 255, 128, 128 );
	
	-- get attributes
	local attributes = {
		[ 'speed' ] = math.Clamp( 10, 0, 10 ),
		[ 'jump' ] = math.Clamp( 5, 0, 10 ),
		[ 'turn' ] = math.Clamp( 10, 0, 10 ),
		[ 'flip' ] = math.Clamp( 5, 0, 10 ),
		[ 'twist' ] = math.Clamp( 5, 0, 10 )
	};
	
	-- set angle
	local ang = pl:GetAngles();
	ang.p = 0;
	ang.y = ang.y + 180;
	
	-- position
	local pos = trace.HitPos + trace.HitNormal * 32
	
	-- create hoverboard
	local hoverboard = MakeHoverboard( pl, model, ang, pos, mcontrol, shake, height, viewdist, trailsize, trail, boost, recharge, attributes );
	
	-- validate
	if ( !hoverboard ) then
	
		return false;
		
	end
	
	-- create undo
	undo.Create( "Hoverboard" );
		undo.AddEntity( hoverboard );
		undo.SetPlayer( pl );
	undo.Finish();
	
	-- all done
	return true, hoverboard;

end


function SWEP:Reload( trace )

end



function SWEP:Think( ) 

end


if ( SERVER ) then

	function MakeHoverboard( pl, model, ang, pos, mcontrol, shake, height, viewdist, trailsize, trail, boost, recharge, attributes )
	
		
		-- create
		local hoverboard = ents.Create( "modulus_hoverboard" );
		
		-- validate
		if ( !hoverboard:IsValid() ) then
		
			return false;
			
		end
		
		-- storage
		local boardinfo;
		
		-- loop boards
		for _, board in pairs( HoverboardTypes ) do
		
			-- find selected
			if ( board[ 'model' ]:lower() == model:lower() ) then
			
				-- save
				boardinfo = board;
				break;
			
			end
			
		end
		
		-- validate
		if ( !boardinfo ) then
		
			return false;
			
		end
		
		-- just incase
		util.PrecacheModel( model );
		
		-- setup
		hoverboard:SetModel( model );
		hoverboard:SetAngles( ang );
		hoverboard:SetPos( pos );
		
		-- default rotation
		hoverboard:SetBoardRotation( 0 );
		
		-- check rotation
		if ( boardinfo[ 'rotation' ] ) then
		
			-- get value
			local rot = tonumber( boardinfo[ 'rotation' ] );
		
			-- update
			hoverboard:SetBoardRotation( tonumber( boardinfo[ 'rotation' ] ) );
			
			-- change angles
			ang.y = ang.y - rot;
			hoverboard:SetAngles( ang );
			
		end
		
		-- spawn
		hoverboard:Spawn();
		hoverboard:Activate();
		
		-- default position
		hoverboard:SetAvatarPosition( Vector( 0, 0, 0 ) );
		
		-- check position
		if ( boardinfo[ 'driver' ] ) then
		
			-- get position
			--local x, y, z = unpack( string.Explode( " = ", boardinfo[ 'driver' ] ) );
			local pos = boardinfo[ 'driver' ]--Vector( tonumber( x or 0 ), tonumber( y or 0 ), tonumber( z or 0 ) );
			
			-- update
			hoverboard:SetAvatarPosition( pos );
		
		end
		
		-- loop info
		for k, v in pairs( boardinfo ) do
		
			-- check for effects
			if ( k:sub( 1, 7 ):lower() == "effect_" && type( boardinfo[ k ] == "table" ) ) then
			
				-- get effect table
				local effect = boardinfo[ k ];
				
				-- get position
				--local x, y, z = unpack( string.Explode( " = ", effect[ 'position' ] ) );
				local pos = effect[ 'position' ]--Vector( tonumber( x or 0 ), tonumber( y or 0 ), tonumber( z or 0 ) );
				
				-- get name
				local name = effect[ 'effect' ] or "trail";
				
				-- get normal
				local normal;
				if ( effect[ 'normal' ] ) then
				
					--local x, y, z = unpack( string.Explode( " = ", effect[ 'normal' ] or "" ) );
					normal = effect[ 'normal' ]--Vector( tonumber( x or 0 ), tonumber( y or 0 ), tonumber( z or 0 ) );
					
				end
				
				-- get scale
				local scale = effect[ 'scale' ] or 1;
				
				-- add it
				hoverboard:AddEffect( name, pos, normal, scale );
			
			end
		
		end
		
		-- controls
		hoverboard:SetControls( math.Clamp( tonumber( mcontrol ), 0, 1 ) );
		
		-- boost shake
		hoverboard:SetBoostShake( math.Clamp( tonumber( shake ), 0, 1 ) );
		
		-- hover height
		hoverboard:SetHoverHeight( math.Clamp( tonumber( height ), 36, 100 ) );
		
		-- view distance
		hoverboard:SetViewDistance( math.Clamp( tonumber( viewdist ), 64, 256 ) );
		
		-- spring
		hoverboard:SetSpring( 0.21 * ( ( 72 / height ) * ( 72 / height ) ) );
		
		-- trail info
		trailsize = math.Clamp( trailsize, 0, 10 ) * 0.3;
		hoverboard:SetTrailScale( trailsize );
		hoverboard:SetTrailColor( trail );
		hoverboard:SetTrailBoostColor( boost );
		hoverboard:SetTrailRechargeColor( recharge );
		
		-- make sure no one is hacking the console vars
		local count = 0;
		local points = GetGlobalInt( "HoverPoints" );
		
		-- loop
		for k, v in pairs( attributes ) do
		
			-- available points
			local remaining = points - count;
			
			-- clamp
			v = math.Clamp( v, 0, math.min( 10, remaining ) );
			
			-- update
			attributes[ k ] = v;
			
			-- increment count
			count = count + v;
		
		end
		
		-- find bonuses
		for k, v in pairs( boardinfo[ 'bonus' ] or {} ) do
		
			-- check bonus
			if ( attributes[ k ] ) then
			
				-- add it
				attributes[ k ] = attributes[ k ] + tonumber( v );
				
			end
		
		end
		
		-- attributes
		local speed = ( attributes[ 'speed' ] * 0.1 ) * 20;
		hoverboard:SetSpeed( speed );
		local jump = ( attributes[ 'jump' ] * 0.1 ) * 250;
		hoverboard:SetJumpPower( jump );
		local turn = ( attributes[ 'turn' ] * 0.1 ) * 25;
		hoverboard:SetTurnSpeed( turn );
		local flip = ( attributes[ 'flip' ] * 0.1 ) * 25;
		hoverboard:SetPitchSpeed( flip );
		local twist = ( attributes[ 'twist' ] * 0.1 ) * 25;
		hoverboard:SetYawSpeed( twist );
		local roll = ( ( flip + twist * 0.5 ) / 50 ) * 22;
		hoverboard:SetRollSpeed( roll );
		
		-- all done
		--DoPropSpawnedEffect( hoverboard );
		--pl:AddCount( "hoverboards", hoverboard );
		
		-- store
		hoverboard.Creator = pl:UniqueID();
		
		-- return
		return hoverboard;
		
	end
	
end

if SERVER then

--resource.AddFile("models/modulus/player_hull.mdl")
--resource.AddFile("models/modulus/player_hull.vvd")
--resource.AddFile("models/modulus/player_hull.phy")
--resource.AddFile("models/modulus/player_hull.dx90.vtx")
--resource.AddFile("models/modulus/player_hull.dx80.vtx")
--resource.AddFile("models/modulus/player_hull.sw.vtx")
--resource.AddFile("models/cloudstrifexiii/boards/longhoverboard.vvd")
--resource.AddFile("models/cloudstrifexiii/boards/regularhoverboard.phy")
--resource.AddFile("models/cloudstrifexiii/boards/trickhoverboard.mdl")
--resource.AddFile("models/cloudstrifexiii/boards/regularhoverboard.dx90.vtx")
--resource.AddFile("models/cloudstrifexiii/boards/regularhoverboard.mdl")
--resource.AddFile("models/cloudstrifexiii/boards/longhoverboard.phy")
--resource.AddFile("models/cloudstrifexiii/boards/longhoverboard.dx90.vtx")
--resource.AddFile("models/cloudstrifexiii/boards/trickhoverboard.dx90.vtx")
--resource.AddFile("models/cloudstrifexiii/boards/regularhoverboard.sw.vtx")
--resource.AddFile("models/cloudstrifexiii/boards/trickhoverboard.sw.vtx")
--resource.AddFile("models/cloudstrifexiii/boards/trickhoverboard.vvd")
--resource.AddFile("models/cloudstrifexiii/boards/longhoverboard.sw.vtx")
--resource.AddFile("models/cloudstrifexiii/boards/longhoverboard.mdl")
--resource.AddFile("models/cloudstrifexiii/boards/trickhoverboard.phy")
--resource.AddFile("models/cloudstrifexiii/boards/longhoverboard.dx80.vtx")
--resource.AddFile("models/cloudstrifexiii/boards/regularhoverboard.dx80.vtx")
--resource.AddFile("models/cloudstrifexiii/boards/trickhoverboard.dx80.vtx")
--resource.AddFile("models/cloudstrifexiii/boards/regularhoverboard.vvd")
--resource.AddFile("models/ut3/hoverboard.dx90.vtx")
--resource.AddFile("models/ut3/hoverboard.sw.vtx")
--resource.AddFile("models/ut3/hoverboard.dx80.vtx")
--resource.AddFile("models/ut3/hoverboard.mdl")
--resource.AddFile("models/ut3/hoverboard.phy")
--resource.AddFile("models/ut3/hoverboard.vvd")
--resource.AddFile("models/dav0r/hoverboard/hoverboard.dx90.vtx")
--resource.AddFile("models/dav0r/hoverboard/hoverboard.sw.vtx")
--resource.AddFile("models/dav0r/hoverboard/hoverboard.dx80.vtx")
--resource.AddFile("models/dav0r/hoverboard/hoverboard.mdl")
--resource.AddFile("models/dav0r/hoverboard/hoverboard.phy")
--resource.AddFile("models/dav0r/hoverboard/hoverboard.vvd")
--resource.AddFile("models/squint_hoverboard/hoverboard.dx90.vtx")
--resource.AddFile("models/squint_hoverboard/asltd.phy")
--resource.AddFile("models/squint_hoverboard/hotrod.sw.vtx")
--resource.AddFile("models/squint_hoverboard/hoverboard.sw.vtx")
--resource.AddFile("models/squint_hoverboard/hoverboard.dx80.vtx")
--resource.AddFile("models/squint_hoverboard/hotrod.vvd")
--resource.AddFile("models/squint_hoverboard/hoverboard.mdl")
--resource.AddFile("models/squint_hoverboard/hotrod.dx90.vtx")
--resource.AddFile("models/squint_hoverboard/asltd.vvd")
--resource.AddFile("models/squint_hoverboard/asltd.dx90.vtx")
--resource.AddFile("models/squint_hoverboard/hoverboard.phy")
--resource.AddFile("models/squint_hoverboard/asltd.sw.vtx")
--resource.AddFile("models/squint_hoverboard/hotrod.mdl")
--resource.AddFile("models/squint_hoverboard/asltd.dx80.vtx")
--resource.AddFile("models/squint_hoverboard/hotrod.dx80.vtx")
--resource.AddFile("models/squint_hoverboard/hoverboard.vvd")
--resource.AddFile("models/squint_hoverboard/asltd.mdl")
--resource.AddFile("models/squint_hoverboard/hotrod.phy")
--resource.AddFile("models/jaanus/stuntboard.vvd")
--resource.AddFile("models/jaanus/scopesboard.mdl")
--resource.AddFile("models/jaanus/truehoverboard_1.dx90.vtx")
--resource.AddFile("models/jaanus/truehoverboard_2.dx80.vtx")
--resource.AddFile("models/jaanus/scopesboard.dx80.vtx")
--resource.AddFile("models/jaanus/truehoverboard_1.sw.vtx")
--resource.AddFile("models/jaanus/scopesboard.phy")
--resource.AddFile("models/jaanus/truehoverboard_2.mdl")
--resource.AddFile("models/jaanus/stuntboard.sw.vtx")
--resource.AddFile("models/jaanus/truehoverboard_2.phy")
--resource.AddFile("models/jaanus/scopesboard.vvd")
--resource.AddFile("models/jaanus/stuntboard.mdl")
--resource.AddFile("models/jaanus/truehoverboard_2.vvd")
--resource.AddFile("models/jaanus/truehoverboard_2.sw.vtx")
--resource.AddFile("models/jaanus/truehoverboard_1.phy")
--resource.AddFile("models/jaanus/truehoverboard_1.vvd")
--resource.AddFile("models/jaanus/truehoverboard_1.dx80.vtx")
--resource.AddFile("models/jaanus/scopesboard.dx90.vtx")
--resource.AddFile("models/jaanus/stuntboard.dx80.vtx")
--resource.AddFile("models/jaanus/truehoverboard_1.mdl")
--resource.AddFile("models/jaanus/stuntboard.phy")
--resource.AddFile("models/jaanus/truehoverboard_2.dx90.vtx")
--resource.AddFile("models/jaanus/stuntboard.dx90.vtx")
--resource.AddFile("models/jaanus/scopesboard.sw.vtx")

--resource.AddFile("materials/models/cloudstrifexiii/boards/normalboard.vmt")
--resource.AddFile("materials/models/cloudstrifexiii/boards/longboard.vmt")
--resource.AddFile("materials/models/cloudstrifexiii/boards/normalboard.vtf")
--resource.AddFile("materials/models/cloudstrifexiii/boards/trickboard.vmt")
--resource.AddFile("materials/models/cloudstrifexiii/boards/longboard.vtf")
--resource.AddFile("materials/models/cloudstrifexiii/boards/trickboard.vtf")
--resource.AddFile("materials/models/squint_hoverboard/asltd/asltd.vtf")
--resource.AddFile("materials/models/squint_hoverboard/asltd/asltd.vmt")
--resource.AddFile("materials/models/squint_hoverboard/squint_hoverboard.vmt")
--resource.AddFile("materials/models/squint_hoverboard/hotrod/hotrod.vtf")
--resource.AddFile("materials/models/squint_hoverboard/hotrod/hotrod.vmt")
--resource.AddFile("materials/models/squint_hoverboard/squint_hoverboard.vtf")
--resource.AddFile("materials/models/hoverboard/boardmap.vtf")
--resource.AddFile("materials/models/hoverboard/boardmap.vmt")
--resource.AddFile("materials/models/hoverboard/boardtransparency.vmt")
--resource.AddFile("materials/models/hoverboard/boardnormal.vtf")
--resource.AddFile("materials/models/hoverboard/boardtransparency.vtf")
--resource.AddFile("materials/models/kamikazi/half_pipe.vmt")
--resource.AddFile("materials/models/kamikazi/fun_box.vmt")
--resource.AddFile("materials/ut3/hoverboard_normal.vtf")
--resource.AddFile("materials/ut3/hoverboard.vmt")
--resource.AddFile("materials/ut3/hoverboard.vtf")
--resource.AddFile("materials/ut3/hoverboard_mask.vtf")
--resource.AddFile("materials/modulus_hoverboard/glow.vmt")
--resource.AddFile("materials/modulus_hoverboard/deathicon.vtf")
--resource.AddFile("materials/modulus_hoverboard/deathicon.vmt")
--resource.AddFile("materials/modulus_hoverboard/trail.vmt")
--resource.AddFile("materials/jaanus/bawrd.vmt")
--resource.AddFile("materials/jaanus/enginefuck.vmt")
--resource.AddFile("materials/jaanus/stuntboard.vtf")
--resource.AddFile("materials/jaanus/stuntboard.vmt")
--resource.AddFile("materials/jaanus/enginefuck.vtf")
--resource.AddFile("materials/jaanus/huvaburd.vtf")
--resource.AddFile("materials/jaanus/bawrd.vtf")
--resource.AddFile("materials/jaanus/huvaburd.vmt")
--resource.AddFile("materials/fun_box.vtf")
----resource.AddFile("materials/vgui/ttt/hoverboard.png")


end

if CLIENT then
    local matScreen 	= Material( "models/weapons/v_toolgun/screen" )
    local txBackground	= surface.GetTextureID( "models/weapons/v_toolgun/screen_bg" )

    -- GetRenderTarget returns the texture if it exists, or creates it if it doesn't
    local RTTexture 	= GetRenderTarget( "GModToolgunScreen", 256, 256 )

    surface.CreateFont( "GModToolScreen", {
        font	= "Helvetica",
        size	= 60,
        weight	= 900
    } )


    local function DrawScrollingText( text, y, texwide )

        local w, h = surface.GetTextSize( text  )
        w = w + 64

        local x = math.fmod( CurTime() * 400, w ) * -1

        while ( x < texwide ) do

            surface.SetTextColor( 0, 0, 0, 255 )
            surface.SetTextPos( x + 3, y + 3 )
            surface.DrawText( text )

            surface.SetTextColor( 255, 255, 255, 255 )
            surface.SetTextPos( x, y )
            surface.DrawText( text )

            x = x + w

        end

    end

    --[[---------------------------------------------------------
        We use this opportunity to draw to the toolmode
            screen's rendertarget texture.
    -----------------------------------------------------------]]
    function SWEP:RenderScreen()

        local TEX_SIZE = 256
        local mode = GetConVarString( "gmod_toolmode" )
        local oldW = ScrW()
        local oldH = ScrH()

        -- Set the material of the screen to our render target
        matScreen:SetTexture( "$basetexture", RTTexture )

        local OldRT = render.GetRenderTarget()

        -- Set up our view for drawing to the texture
        render.SetRenderTarget( RTTexture )
        render.SetViewPort( 0, 0, TEX_SIZE, TEX_SIZE )
        cam.Start2D()

        -- Background
        surface.SetDrawColor( 255, 255, 255, 255 )
        surface.SetTexture( txBackground )
        surface.DrawTexturedRect( 0, 0, TEX_SIZE, TEX_SIZE )

        -- Give our toolmode the opportunity to override the drawing


        surface.SetFont( "GModToolScreen" )
        DrawScrollingText("TTT-FUN's Hoverboards", 64, TEX_SIZE )



        cam.End2D()
        render.SetRenderTarget( OldRT )
        render.SetViewPort( 0, 0, oldW, oldH )

    end
end