
-- info
ENT.Type 		= "vehicle";
ENT.Base 		= "base_anim";
ENT.PrintName 		= "Hoverboard";
ENT.Author 		= "% Software";
ENT.Information 	= "UT Hoverboard";
ENT.Category 		= "% Software";
ENT.Spawnable 		= false;
ENT.AdminSpawnable 	= false;

-- thrusters
ENT.ThrusterPoints = {
	{ Pos = Vector( -46, 0, 0 ), Diff = 24, Spring = 3 },

	{ Pos = Vector( -24, 13, 24 ) },
	{ Pos = Vector( 24, 13, 24 ) },
	{ Pos = Vector( -24, -13, 24 ) },
	{ Pos = Vector( 24, -13, 24 ) },
	--Vector( 0, 32, 24 ),
	--Vector( 0, -32, 24 ),
	--Vector( 12, 18, 24 ),
	--Vector( 12, -08, 24 ),
	--Vector( -12, 08, 24 ),
	--Vector( -12, -08, 24 ),
	--Vector( 0, 0, 24 ),

};

function ENT:SetupDataTables()
	self:NetworkVar( "Float", 0, "BoostShake" )
	self:NetworkVar( "Float", 1, "BoardVelocity" )
	self:NetworkVar( "Float", 2, "MaxLength" )
	self:NetworkVar( "Float", 3, "TrailScale" )
	self:NetworkVar( "Float", 4, "BoardRotation" )

	self:NetworkVar( "String", 0, "HoverHeight" ) -- Stupid limits!
	self:NetworkVar( "String", 1, "ViewDistance" )
	self:NetworkVar( "String", 2, "EffectCount" )

	self:NetworkVar( "Bool", 0, "DarkInner" )

	self:NetworkVar( "Vector", 0, "TrailColor" )
	self:NetworkVar( "Vector", 1, "TrailBoostColor" )
	self:NetworkVar( "Vector", 2, "TrailRechargeColor" )

	self:NetworkVar( "Entity", 0, "ScriptedVehicle" )

	if ( SERVER ) then
		/*self:SetBoardRotation( 0 )
		self:SetHoverHeight( 2 )
		self:SetViewDistance( 0 )*/
	end
end

-- network vars
--AccessorFuncNW( ENT, "effects", "EffectCount", false );
--AccessorFuncNW( ENT, "boardvelocity", "BoardVelocity", 0, FORCE_NUMBER );
--AccessorFuncNW( ENT, "rotation", "BoardRotation", 0, FORCE_NUMBER );
/*
AccessorFuncNW( ENT, "hoverheight", "HoverHeight", 0, FORCE_NUMBER );
AccessorFuncNW( ENT, "viewdistance", "ViewDistance", 0, FORCE_NUMBER );*/

--AccessorFuncNW( ENT, "boostshake", "BoostShake", 1, FORCE_NUMBER );
--AccessorFuncNW( ENT, "trailscale", "TrailScale", 1, FORCE_NUMBER );
/*AccessorFuncNW( ENT, "trailcolor", "TrailColor", Vector( 128, 128, 255 ) );
AccessorFuncNW( ENT, "trailboostcolor", "TrailBoostColor", Vector( 255, 128, 128 ) );
AccessorFuncNW( ENT, "trailrechargecolor", "TrailRechargeColor", Vector( 255, 255, 128 ) );*/


function ENT:GetDriver( )

	--return self:GetNWEntity( "Driver", NULL );
	return self:GetOwner();

end

hook.Add("CalcView", "hoverboards_bitch", function( pl, pos, ang, fov )

	local ent = pl:GetNWEntity( "ScriptedVehicle" )
	if ( !IsValid( ent ) ) then return end

	local dir = pl:GetAimVector()
	
	local face = dir:Angle()
	face:RotateAroundAxis( Vector( 0, 0, 1 ), ent:GetBoardRotation() )
	dir = face:Forward()

	local pos = ent:GetPos() + Vector( 0, 0, 64 ) - ( dir * tonumber( ent:GetViewDistance() ) )
	local speed = ent:GetVelocity():Length() - 500;

	-- shake their view
	if ( ent:IsBoosting() && speed > 0 && ent:GetBoostShake() == 1 ) then

		local power = 14 * ( speed / 700 );

		local x = math.Rand( -power, power ) * 0.1;
		local y = math.Rand( -power, power ) * 0.1;
		local z = math.Rand( -power, power ) * 0.1;

		pos = pos + Vector( x, y, z );

	end

	-- the direction to face
	--local face = ( ( self:GetPos() + Vector( 0, 0, 40 ) ) - pos ):Angle();
	
	-- get the proper force to apply
	--face:RotateAroundAxis( Vector( 0, 0, 1 ), self:GetBoardRotation() );*/

	-- trace to keep it out of the walls
	local tr = util.TraceHull( {
		start = ent:GetPos(),
		endpos = pos,
		mask = MASK_NPCWORLDSTATIC,
		mins = Vector( -4, -4, -4),
		maxs = Vector( 4, 4, 4)
	} );

	-- setup view
	local view = {
		origin = tr.HitPos + tr.HitNormal,
		angles = dir:Angle(),
		fov = fov,

	};

	return view;

end )


local function Move( pl, mv )

	-- get the scripted vehicle
	--if ( !pl.GetScriptedVehicle ) then /*print("NOOOOOOOOOOOOOOOOOOOOOOOOO")*/ return end
	local board = pl:GetNWEntity("ScriptedVehicle");

	-- make sure they are using the hoverboard
	if ( !IsValid( board ) || board:GetClass() != "modulus_hoverboard" ) then

		-- if not, exit
		return;

	end

	-- set their origin
	mv:SetOrigin( board:GetPos() );

	-- prevent their movement
	return true;

end
hook.Add( "Move", "Hoverboard_Move", Move );


function ENT:IsGrinding( )

	return self.Entity:GetNWBool( "Grinding" );

end


function ENT:Boost( )

	return self.Entity:GetNWInt( "Boost" );

end

function ENT:IsBoosting( )

	return self.Entity:GetNWBool( "Boosting" );

end

function ENT:GetThruster( index )

	local pos = self:LocalToWorld( self.ThrusterPoints[ index ].Pos );

	-- get distance and dir
	local dist = ( self:GetPos() - pos ):Length();
	local dir = ( pos - self:GetPos() ):GetNormalized();

	-- rotate
	dir = dir:Angle();
	dir:RotateAroundAxis( self:GetUp(), self:GetBoardRotation() );
	dir = dir:Forward();

	-- return
	return self:GetPos() + dir * dist;

end


hook.Add( "UpdateAnimation", "Hoverboard_UpdateAnimation", function( pl )

	-- get the scripted vehicle
	--if ( !pl.GetScriptedVehicle ) then /* print("NOOOOOOOOOOOOOOOOOOOOOOOOO")*/ return end
	local board = pl:GetNWEntity("ScriptedVehicle");

	-- make sure they are using the hoverboard
	if ( !IsValid( board ) || board:GetClass() != "modulus_hoverboard" ) then

		-- if not, exit
		return;

	end

	-- copy pose parameters
	local pose_params = { "head_pitch", "head_yaw", "body_yaw", "aim_yaw", "aim_pitch" };
	for _, param in pairs( pose_params ) do

		if ( IsValid( board.Avatar ) ) then

			local val = pl:GetPoseParameter( param );
			board.Avatar:SetPoseParameter( param, val );

		end

	end

end )