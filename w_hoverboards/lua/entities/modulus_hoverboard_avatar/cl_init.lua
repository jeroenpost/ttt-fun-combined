
// shared file
include( 'shared.lua' );


/*------------------------------------
	Initialize
------------------------------------*/
function ENT:Initialize( )
	
end


/*------------------------------------
	Draw
------------------------------------*/
function ENT:Draw( )
	
	if ( !IsValid( self.Board ) || !IsValid( self.Board:GetDriver() ) ) then
	
		return;
		
	end
	
	// draw
	self:DrawModel();
	
end


/*------------------------------------
	DrawTranslucent
------------------------------------*/
function ENT:DrawTranslucent( )

	// draw opaque
	self:Draw();

end


/*------------------------------------
	Think
------------------------------------*/
function ENT:Think( )

	// update player
	local pl = self:GetNWEntity( "Player" );
	self.Player = pl;
	
	// update board
	//local board = self:GetOwner();
	local board = self:GetNWEntity( "Board" );
	self.Board = board;
	
	// think fast
	self.Entity:NextThink( CurTime() + 0.1 );
	
	return true;
	
end
