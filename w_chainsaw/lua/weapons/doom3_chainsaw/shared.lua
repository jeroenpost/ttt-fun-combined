////////////////////////////
/////CHAINSAW SWEP /////
/////////by CMasta/////////
////////////////////////////

if ( SERVER ) then

	AddCSLuaFile( "shared.lua" )
	
	
	
--resource.AddFile( "sound/weapons/doom_chainsaw/knife_deploy2.mp3")
--resource.AddFile( "sound/weapons/doom_chainsaw/knife_stab3.mp3")
--resource.AddFile( "sound/weapons/doom_chainsaw/knife_stab1.mp3")
--resource.AddFile( "sound/weapons/doom_chainsaw/knife_deploy1.mp3")
--resource.AddFile( "sound/weapons/doom_chainsaw/knife_slash1.mp3")
--resource.AddFile( "sound/weapons/doom_chainsaw/knife_idle.mp3")
--resource.AddFile( "sound/weapons/doom_chainsaw/knife_stab2.mp3")
--resource.AddFile( "sound/weapons/doom_chainsaw/knife_hitwall1.mp3")
--resource.AddFile( "models/weapons/v_chain_s.mdl")
--resource.AddFile( "materials/models/weapons/v_models/d3_chainsaw/chainsaw_ref.vtf")
--resource.AddFile( "materials/models/weapons/v_models/d3_chainsaw/chainsaw.vmt")
--resource.AddFile( "materials/models/weapons/v_models/d3_chainsaw/chain.vmt")
--resource.AddFile( "materials/vgui/ttt_fun_killicons/chainsaw.png")
	
	SWEP.HoldType			= "shotgun"
	
end
SWEP.PrintName			= "Chainsaw"
SWEP.SlotPos			= 7

if ( CLIENT ) then

				
	SWEP.Author				= "CMasta -ZsN-"

	SWEP.Slot				= 1
	
	SWEP.ViewModelFOV		= 60
	SWEP.IconLetter			= "x"

end

 SWEP.EquipMenuData = {
      type  = "item_weapon",
      name  = "Chainssaw",
      desc  = "Saw players to a bloody death"
   }

SWEP.Spawnable				= true
SWEP.AdminSpawnable			= true
SWEP.Kind = WEAPON_EQUIP
SWEP.CanBuy = {ROLE_DETECTIVE,ROLE_TRAITOR} -- only traitors can buy
SWEP.Base   = "weapon_tttbase"
SWEP.Icon = "vgui/ttt_fun_killicons/chainsaw.png"
SWEP.ViewModelFlip = false

-----------------------Main functions----------------------------
 
function SWEP:Think() -- Called every frame
end

sounds = {
	"deploy1",
	"deploy2",
	"hitwall1",
	"idle",
	"slash1", 
	"stab1",
	"stab2",
	"stab3"
	}

function SWEP:Initialize()
	for k, v in pairs ( sounds ) do
		util.PrecacheSound( "weapons/doom_chainsaw/knife_"..v..".mp3" )
	end
end

SWEP.bones = { "ValveBiped.Bip01_Head1", "ValveBiped.Bip01_R_Hand", "ValveBiped.Bip01_R_Forearm", "ValveBiped.Bip01_R_Foot", "ValveBiped.Bip01_R_Thigh", "ValveBiped.Bip01_R_Calf",  "ValveBiped.Bip01_R_Elbow", "ValveBiped.Bip01_R_Shoulder"
    , "ValveBiped.Bip01_L_Hand", "ValveBiped.Bip01_L_Forearm", "ValveBiped.Bip01_L_Foot", "ValveBiped.Bip01_L_Thigh", "ValveBiped.Bip01_L_Calf",  "ValveBiped.Bip01_L_Elbow", "ValveBiped.Bip01_L_Shoulder" }

function SWEP:PrimaryAttack()
local tr = self.Owner:GetEyeTrace()
	tr.endpos = self.Owner:GetPos() + (self.Owner:GetAimVector() * 150)
local length = (self.Owner:GetShootPos() - tr.Entity:GetPos()):Length()
	
	
	if length <= 195 then
		if tr.Entity:IsValid() and tr.Entity:IsNPC() or tr.Entity:IsPlayer()  or tr.Entity:GetClass()=="prop_ragdoll"  then
			self.Weapon:EmitSound("weapons/doom_chainsaw/knife_stab"..math.random( 1, 3 )..".mp3")
			self.Weapon:SendWeaponAnim( ACT_VM_HITCENTER )
			util.BlastDamage(self.Owner, self.Owner, tr.HitPos, 4, math.random( 30, 95 ) )
			local effectdata = EffectData()
				effectdata:SetOrigin( tr.HitPos )
            timer.Simple(0.01,function()

                local bone = table.Random(self.bones)
                bone = tr.Entity:LookupBone( bone )
                if bone then
                    self:detachLimb(tr.Entity,bone)
                end

            end)

				for i=1, 5 do
					util.Effect( "bloodsplash_effect", effectdata )
				end
		elseif !tr.HitWorld then
			self.Weapon:EmitSound("weapons/doom_chainsaw/knife_hitwall1.mp3")
			self.Weapon:SendWeaponAnim( ACT_VM_HITCENTER )
			util.BlastDamage(self.Owner, self.Owner, tr.HitPos, 4, math.random( 20, 50 ) )			
			
			local Pos1 = tr.HitPos + tr.HitNormal
			local Pos2 = tr.HitPos - tr.HitNormal
				util.Decal( "ManhackCut", Pos1, Pos2 )
		end
	else
		self.Weapon:EmitSound( "weapons/doom_chainsaw/knife_slash1.mp3" )
		self.Weapon:SendWeaponAnim( ACT_VM_MISSCENTER )
	end
	self.Weapon:SetNextPrimaryFire( CurTime() + 0.8 )
end
 
function SWEP:SecondaryAttack()
	return false
end

function SWEP:Reload()
	return false
end

function SWEP:Deploy()
	self.Weapon:EmitSound( "weapons/doom_chainsaw/knife_deploy"..math.random( 1, 2 )..".mp3" )
	self.Weapon:SendWeaponAnim( ACT_VM_DRAW )
	timer.Create( "IdleSound", 2.99, 0, function()
	if self.Weapon then
	     	self.Weapon:EmitSound( "weapons/doom_chainsaw/knife_idle.mp3")
			end
     end)
end

function SWEP:detachLimb( Ply, Normal )

    Ply:ManipulateBoneScale(Normal, vector_origin)

    local ED = EffectData()
    ED:SetEntity( Ply ) --Player Entity
    ED:SetNormal( self.Owner:GetForward() )
    ED:SetScale( Normal )
    ED:SetOrigin( Ply:GetPos() )
    util.Effect( 'limbremove', ED ) --Effect Gib

end

function SWEP:Holster()
	timer.Destroy( "IdleSound" )
	self.Weapon:StopSound( "weapons/doom_chainsaw/knife_idle.mp3" )
	return true
end
-------------------------------------------------------------------

------------General Swep Info---------------
SWEP.Author   = "CMasta -ZsN-"
SWEP.Contact        = ""
SWEP.Purpose        = "Cut something... maybe some legs or arms"
SWEP.Instructions   = "Pull out,\naim,\nruuuun cheater cheater cheater cheater ruuuun"
SWEP.Spawnable      = true
SWEP.AdminSpawnable  = true
-----------------------------------------------

------------Models---------------------------
SWEP.ViewModel      = "models/weapons/v_chain_s.mdl"
SWEP.WorldModel		= "models/weapons/w_physics.mdl"
-----------------------------------------------

-------------Primary Fire Attributes----------------------------------------
SWEP.Primary.Delay			= 1 	--In seconds
SWEP.Primary.Recoil			= 0		--Gun Kick
SWEP.Primary.Damage			= 1	--Damage per Bullet
SWEP.Primary.NumShots		= 1		--Number of shots per one fire
SWEP.Primary.Cone			= 10 	--Bullet Spread
SWEP.Primary.ClipSize		= -1	--Use "-1 if there are no clips"
SWEP.Primary.DefaultClip	= -1	-- Number of shots in next clip
SWEP.Primary.Automatic   	= true	--Pistol fire (false) or SMG fire (true)
SWEP.Primary.Ammo         	= "none"	--Ammo Type
-------------End Primary Fire Attributes------------------------------------
 
-------------Secondary Fire Attributes-------------------------------------
SWEP.Secondary.Delay		= 0
SWEP.Secondary.Recoil		= 0
SWEP.Secondary.Damage		= 0
SWEP.Secondary.NumShots		= 0
SWEP.Secondary.Cone			= 0
SWEP.Secondary.ClipSize		= -1
SWEP.Secondary.DefaultClip	= -1
SWEP.Secondary.Automatic   	= false
SWEP.Secondary.Ammo         = "none"
-------------End Secondary Fire Attributes--------------------------------