if CLIENT then
   
   SWEP.Author 		= "GreenBlack"   
   
end
SWEP.PrintName	= "ADMIN Deagle"
SWEP.Base			= "gb_master_deagle"
 SWEP.Icon = "vgui/ttt_fun_killicons/golden_deagle.png"
SWEP.Kind 			= WEAPON_PISTOL
SWEP.Slot = 1
SWEP.SlotPos 	= 1
SWEP.Primary.Recoil	= 1
SWEP.Primary.Damage = 50
SWEP.Primary.Delay 	= 0.45
SWEP.Primary.Cone 	= 0.009
SWEP.Primary.ClipSize 		= 25
SWEP.Primary.ClipMax 		= 75
SWEP.Primary.DefaultClip 	= 25
SWEP.Primary.NumShots		= 1
SWEP.Primary.Automatic 	= true
SWEP.HeadshotMultiplier = 4
SWEP.AmmoEnt 		= "item_ammo_revolver_ttt"