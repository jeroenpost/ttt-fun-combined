if CLIENT then
   
   SWEP.Author 		= "GreenBlack"   

end
SWEP.PrintName	= "Golden Deagle"
SWEP.Base			= "gb_master_deagle"
 SWEP.Icon = "vgui/ttt_fun_killicons/golden_deagle.png"
SWEP.Kind 			= WEAPON_PISTOL
SWEP.Slot = 1
SWEP.SlotPos 	= 1
SWEP.Primary.Recoil	= 1.25
SWEP.Primary.Damage = 75
SWEP.Primary.Delay 	= 0.65
SWEP.Primary.Cone 	= 0.001
SWEP.Primary.ClipSize 		= 15
SWEP.Primary.ClipMax 		= 90
SWEP.Primary.DefaultClip 	= 15
SWEP.Primary.NumShots		= 1
SWEP.Primary.Automatic 	= true
SWEP.HeadshotMultiplier = 3
SWEP.Primary.Ammo = "AlyxGun"
SWEP.AmmoEnt = "item_ammo_revolver_ttt"

SWEP.CanBuy = {ROLE_DETECTIVE, ROLE_TRAITOR}

   SWEP.EquipMenuData = {
      name = "Golden Deagle",
      type = "item_weapon",
      desc = "Badass deagle"
   };
