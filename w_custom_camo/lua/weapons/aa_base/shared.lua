SWEP.Base = "gb_base"

--SWEP.HasFireBullet = true
--SWEP.HasFlare = true
--SWEP.HasFly = true
---SWEP.HasHealshot = true
--SWEP.CanDecapitate= true

-- HEALSHOT
function SWEP:HealShot(timebetween, owner)
    if  CLIENT or not IsValid(self.Owner) then return end
    if not timebetween then timebetween =2 end

    local tr = self.Owner:GetEyeTrace()
    local hitEnt = tr.Entity
    if not hitEnt.lastgothealfromhealshot then hitEnt.lastgothealfromhealshot = 0 end

    if hitEnt:IsPlayer() and self.Owner:Health() < 150 and (hitEnt.lastgothealfromhealshot + timebetween) < CurTime()then
        if not owner then
            self.Owner:SetHealth(self.Owner:Health() + 10)
            self.Owner.lastgothealfromhealshot = CurTime()
        else
            hitEnt:SetHealth(self.Owner:Health() + 10)
            hitEnt.lastgothealfromhealshot = CurTime()
        end
    end
end



-- Secondary
SWEP.NextSecondary = 0
function SWEP:ShootSecondary(damage)
    if self.NextSecondary > CurTime() then
        return
    end
    if not damage then damage = 25 end
    self.NextSecondary = CurTime() + 0.75
    self:EmitSound(  "weapons/usp/usp1.wav", 100 )
    self:ShootBullet( damage, 0.02, 1, 0.003 )
    self.Owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )
end


function SWEP:NormalShootBullet( dmg, recoil, numbul, cone )

    self:SendWeaponAnim(self.PrimaryAnim)

    self.Owner:MuzzleFlash()
    self.Owner:SetAnimation( PLAYER_ATTACK1 )

    if not IsFirstTimePredicted() then return end

    local sights = self:GetIronsights()

    numbul = numbul or 1
    cone   = cone   or 0.01

    local bullet = {}
    bullet.Num    = numbul
    bullet.Src    = self.Owner:GetShootPos()
    bullet.Dir    = self.Owner:GetAimVector()
    bullet.Spread = Vector( cone, cone, 0 )
    bullet.Tracer = 4
    bullet.TracerName = self.Tracer or "Tracer"
    bullet.Force  = 10
    bullet.Damage = dmg

    self.Owner:FireBullets( bullet )

    -- Owner can die after firebullets
    if (not IsValid(self.Owner)) or (not self.Owner:Alive()) or self.Owner:IsNPC() then return end

    if ((game.SinglePlayer() and SERVER) or
            ((not game.SinglePlayer()) and CLIENT and IsFirstTimePredicted())) then

        -- reduce recoil if ironsighting
        recoil = sights and (recoil * 0.6) or recoil

        local eyeang = self.Owner:EyeAngles()
        eyeang.pitch = eyeang.pitch - recoil
        self.Owner:SetEyeAngles( eyeang )
    end

end

-- Sound from server

function SWEP:PlaySoundFromServer( soundfile, ply )
   if SERVER then
       if not IsValid(ply) then return end
       local orgin_ents = ents.FindInSphere(ply:GetPos(),350)
       local filter = RecipientFilter();
       for k,v in pairs( orgin_ents ) do
           if v:IsPlayer() then
             filter:AddPlayer( v );
           end
       end

       umsg.Start("playremotesound", filter)
       umsg.Entity(ply)
       umsg.String(soundfile)
       umsg.End()
   end
end


-- Power Hit
SWEP.NextPowerHit = 0
function SWEP:PowerHit( nodmg, power, range )
    if self.NextPowerHit > CurTime() then return end
    self.NextPowerHit = CurTime() + 0.3

    if not power then power = 12000 end
    if not range then range = 120 end

    local tr = util.TraceLine( {
        start = self.Owner:GetShootPos(),
        endpos = self.Owner:GetShootPos() + self.Owner:GetAimVector() * range,
        filter = self.Owner
    } )

    if ( not IsValid( tr.Entity ) ) then
        tr = util.TraceHull( {
            start = self.Owner:GetShootPos(),
            endpos = self.Owner:GetShootPos() + self.Owner:GetAimVector() * range,
            filter = self.Owner,
            mins = self.Owner:OBBMins() / 3,
            maxs = self.Owner:OBBMaxs() / 3
        } )
    end

    if ( tr.Hit ) then self.Owner:EmitSound( "Flesh.ImpactHard" ) end

    if ( IsValid( tr.Entity ) and SERVER  ) then
        self.NextPowerHit = CurTime() + 5
        if not nodmg then
        tr.Entity:TakeDamage( 25, self.Owner )
        end
        tr.Entity.hasProtectionSuitTemp = true

        tr.Entity:SetVelocity(self.Owner:GetForward() * power + Vector(0,0,400))
    end


end


SWEP.hadtripmines = 0
function SWEP:TripMineStick()
    if SERVER then
        local ply = self.Owner
        if not IsValid(ply) then return end


        local ignore = {ply, self.Weapon}
        local spos = ply:GetShootPos()
        local epos = spos + ply:GetAimVector() * 80
        local tr = util.TraceLine({start=spos, endpos=epos, filter=ignore, mask=MASK_SOLID})

        if tr.HitWorld and self.hadtripmines < 3 then

            local mine = ents.Create("npc_tripmine")
            if IsValid(mine) then


                local tr_ent = util.TraceEntity({start=spos, endpos=epos, filter=ignore, mask=MASK_SOLID}, mine)

                if tr_ent.HitWorld then

                    local ang = tr_ent.HitNormal:Angle()
                    ang.p = ang.p + 90

                    mine:SetPos(tr_ent.HitPos + (tr_ent.HitNormal * 3))
                    mine:SetAngles(ang)
                    -- mine:SetModel("models/dynamite/dynamite.mdl")

                    mine:SetOwner(ply)
                    mine:Spawn()
                    --  mine:SetModel("models/dynamite/dynamite.mdl")
                    -- mine:SetHealth( 10 )

                    function mine:OnTakeDamage(dmg)
                        self:Remove()
                    end

                    -- mine:Ignite(100)
                    --  print( mine )
                    if SERVER then DamageLog("Tripmine: " .. self.Owner:Nick() .. " [" .. self.Owner:GetRoleString() .. "] placed a tripmine ")
                    end

                    mine.fingerprints = self.fingerprints

                    self:SendWeaponAnim( ACT_SLAM_TRIPMINE_ATTACH )

                    local holdup = self.Owner:GetViewModel():SequenceDuration()

                    timer.Simple(holdup,
                        function()
                            if SERVER then
                                self:SendWeaponAnim( ACT_SLAM_TRIPMINE_ATTACH2 )
                            end
                        end)

                    timer.Simple(holdup + .1,
                        function()
                            if SERVER then
                                if self.Owner == nil then return end
                                if self.Weapon:Clip1() == 0 and self.Owner:GetAmmoCount( self.Weapon:GetPrimaryAmmoType() ) == 0 then
                                --self.Owner:StripWeapon(self.Gun)
                                --RunConsoleCommand("lastinv")
                                self:Remove()
                                else
                                    self:Deploy()
                                end
                            end
                        end)


                    --self:Remove()
                    self.Planted = true

                    self.hadtripmines = self.hadtripmines + 1

                    if not self.Owner:IsTraitor() then
                        self.Owner:PrintMessage( HUD_PRINTCENTER, "You lost 75 health for placing a tripmine as inno" )
                        self.Owner:SetHealth( self.Owner:Health() - 75)
                    end

                end
            end
        end
    end
end

-- Color material set

-- TRANSPARANT:
    -- "engine/occlusionproxy"

--list.Add( "OverrideMaterials", "models/wireframe" )
--list.Add( "OverrideMaterials", "debug/env_cubemap_model" )
--list.Add( "OverrideMaterials", "models/shadertest/shader3" )
--list.Add( "OverrideMaterials", "models/shadertest/shader4" )
--list.Add( "OverrideMaterials", "models/shadertest/shader5" )
--list.Add( "OverrideMaterials", "models/shiny" )
--list.Add( "OverrideMaterials", "models/debug/debugwhite" )
--list.Add( "OverrideMaterials", "Models/effects/comball_sphere" )
--list.Add( "OverrideMaterials", "Models/effects/comball_tape" )
--list.Add( "OverrideMaterials", "Models/effects/splodearc_sheet" )
--list.Add( "OverrideMaterials", "Models/effects/vol_light001" )
--list.Add( "OverrideMaterials", "models/props_combine/stasisshield_sheet" )
--list.Add( "OverrideMaterials", "models/props_combine/portalball001_sheet" )
--list.Add( "OverrideMaterials", "models/props_combine/com_shield001a" )
--list.Add( "OverrideMaterials", "models/props_c17/frostedglass_01a" )
--list.Add( "OverrideMaterials", "models/props_lab/Tank_Glass001" )
--list.Add( "OverrideMaterials", "models/props_combine/tprings_globe" )
--list.Add( "OverrideMaterials", "models/rendertarget" )
--list.Add( "OverrideMaterials", "models/screenspace" )
--list.Add( "OverrideMaterials", "brick/brick_model" )
--list.Add( "OverrideMaterials", "models/props_pipes/GutterMetal01a" )
--list.Add( "OverrideMaterials", "models/props_pipes/Pipesystem01a_skin3" )
--list.Add( "OverrideMaterials", "models/props_wasteland/wood_fence01a" )
--list.Add( "OverrideMaterials", "models/props_foliage/tree_deciduous_01a_trunk" )
--list.Add( "OverrideMaterials", "models/props_c17/FurnitureFabric003a" )
--list.Add( "OverrideMaterials", "models/props_c17/FurnitureMetal001a" )
--list.Add( "OverrideMaterials", "models/props_c17/paper01" )
--list.Add( "OverrideMaterials", "models/flesh" )

-- phx
--list.Add( "OverrideMaterials", "phoenix_storms/metalset_1-2" )
--list.Add( "OverrideMaterials", "phoenix_storms/metalfloor_2-3" )
--list.Add( "OverrideMaterials", "phoenix_storms/plastic" )
--list.Add( "OverrideMaterials", "phoenix_storms/wood" )
--list.Add( "OverrideMaterials", "phoenix_storms/bluemetal" )
--list.Add( "OverrideMaterials", "phoenix_storms/cube" )
--list.Add( "OverrideMaterials", "phoenix_storms/dome" )
--list.Add( "OverrideMaterials", "phoenix_storms/gear" )
--list.Add( "OverrideMaterials", "phoenix_storms/stripes" )
--list.Add( "OverrideMaterials", "phoenix_storms/wire/pcb_green" )
--list.Add( "OverrideMaterials", "phoenix_storms/wire/pcb_red" )
--list.Add( "OverrideMaterials", "phoenix_storms/wire/pcb_blue" )

--list.Add( "OverrideMaterials", "hunter/myplastic" )
--list.Add( "OverrideMaterials", "models/XQM/LightLinesRed_tool" )

-- "engine/occlusionproxy" -- TRANSPARANT

function SWEP:SetColorAndMaterial( color, material)
    if SERVER then
        self.Weapon:SetColor(color)
        self.Weapon:SetMaterial(material) --models/shiny"))
        if not self.Owner or not self.Owner.GetViewModel then return end
        local vm = self.Owner:GetViewModel()
        if not IsValid(vm) then return end
        vm:ResetSequence(vm:LookupSequence("idle01"))
    end
end

-- Color/materialreset
function SWEP:ColorReset()
    if SERVER then
    --    self:SetColor(Color(255,255,255,255))
     --   self:SetMaterial("")
     --   timer.Stop(self:GetClass() .. "_idle" .. self:EntIndex())
    elseif CLIENT and IsValid(self.Owner) and (self.Owner.GetViewModel) and IsValid(self.Owner:GetViewModel()) then
        self.Owner:GetViewModel():SetColor(Color(255,255,255,255))
        self.Owner:GetViewModel():SetMaterial("")
    end
end

-- player swap teleport
SWEP.NextSwap = 0
function SWEP:SwapPlayer()
    if self.NextSwap < CurTime() then
        self.NextSwap = CurTime() + 0.2
        local tr = self.Owner:GetEyeTrace()
        local hitEnt = tr.Entity

        if hitEnt:IsPlayer()  then
            local pos =  self.Owner:GetPos()
            self.Owner:SetPos(hitEnt:GetPos())
            hitEnt:SetPos(pos)
            local ef = EffectData()
            ef:SetEntity(self.Owner)
            util.Effect("swap_effect", ef,true,true)
            self:EmitSound("ambient/energy/zap5.wav")
           self.NextSwap = CurTime() + 11
        end

    end
end

SWEP.NextSwap = 0
-- Swap players and props
function SWEP:SwapPlayerProp()
    if self.NextSwap < CurTime() then
        self.NextSwap = CurTime() + 0.2
        local tr = self.Owner:GetEyeTrace()
        local hitEnt = tr.Entity

        if IsValid( tr.Entity) and not  tr.Entity:IsWorld()  then

            local phys = tr.Entity:GetPhysicsObject()
            if not phys:IsValid() then return end
            if phys:GetVolume() and phys:GetVolume() > 100000 then
                self.Owner:PrintMessage( HUD_PRINTCENTER, "Prop is too big" )
                return
            end

            local pos =  self.Owner:GetPos()
            self.Owner:SetPos(hitEnt:GetPos())
            hitEnt:SetPos(pos)
            local ef = EffectData()
            ef:SetEntity(self.Owner)
            util.Effect("swap_effect", ef,true,true)
            self:EmitSound("ambient/energy/zap5.wav")
            self.NextSwap = CurTime() + 11
        end
    end
end

-- Flame Shotf
function SWEP:MakeAFlame()
    if not self.Owner:IsValid() or CLIENT then return end

    if self.NextFlame and self.NextFlame > CurTime() then
        return
    end
    self.NextFlame = CurTime() + 0.20

    local tr = self.Owner:GetEyeTrace()
    local effectdata = EffectData()
    effectdata:SetOrigin(tr.HitPos)
    effectdata:SetNormal(tr.HitNormal)
    effectdata:SetScale(1)
    -- util.Effect("effect_mad_ignition", effectdata)
    util.Decal("FadingScorch", tr.HitPos + tr.HitNormal, tr.HitPos - tr.HitNormal)

    local tracedata = {}
    tracedata.start = tr.HitPos
    tracedata.endpos = Vector(tr.HitPos.x, tr.HitPos.y, tr.HitPos.z - 10)
    tracedata.filter = tr.HitPos
    local tracedata = util.TraceLine(tracedata)
    if SERVER then
        if tracedata.HitWorld then
            local flame = ents.Create("env_fire");
            flame:SetPos(tr.HitPos + Vector(0, 0, 1));
            flame:SetKeyValue("firesize", "10");
            flame:SetKeyValue("fireattack", "10");
            flame:SetKeyValue("StartDisabled", "0");
            flame:SetKeyValue("health", "10");
            flame:SetKeyValue("firetype", "0");
            flame:SetKeyValue("damagescale", "5");
            flame:SetKeyValue("spawnflags", "128");
            flame:SetPhysicsAttacker(self.Owner)
            flame:SetOwner(self.Owner)
            flame:Spawn();
            flame:Fire("StartFire", 0);
        end
    end
end

-- Blind
SWEP.NumBlinds = 0
SWEP.Nextblind = 0
function SWEP:Blind( timerrr, victim, color )
    if not timerrr then timerrr = 3 end
    if self.Nextblind > CurTime() then return end
    self.Nextblind = CurTime() +3
    local trace = self.Owner:GetEyeTrace()
    if (trace.HitNonWorld) or victim then

        local victim = victim or trace.Entity
        if ( not victim:IsPlayer()) then return end

        self.NumBlinds = self.NumBlinds + 1
        if SERVER then
            victim.IsBlinded = true
            victim:SetNWBool("isblinded",true)
            umsg.Start( "ulx_blind", victim )
            umsg.Bool( true )
            umsg.Short( 165 )
            umsg.End()
        end
        if isfunction(victim.AnimPerformGesture) then
           victim:AnimPerformGesture(ACT_GMOD_TAUNT_PERSISTENCE);
        end
        if (SERVER) then

            timer.Create("ResetPLayerAfterBlided"..victim:SteamID(), timerrr,1, function()
                if not IsValid(victim)  then  return end

                if SERVER then
                    victim.IsBlinded = false
                    victim:SetNWBool("isblinded",false)
                    umsg.Start( "ulx_blind", victim )
                    umsg.Bool( false )
                    umsg.Short( 0 )
                    umsg.End()

                end
            end)
            return true
        end
    end
end

-- Freeze
function SWEP:Freeze(slowspeed,resetafter, ply)
    local tr = self.Owner:GetEyeTrace()

    if SERVER then
        if not slowspeed then slowspeed = 50 end
        if not resetafter then resetafter = 3 end

        local ply = ply or tr.Entity
        if ply:IsPlayer() or ply:IsNPC()  then

            if not ply.oldspeedmul then
                ply.oldspeedmul =  ply:GetNWInt("speedmul")
            end
            if ply.lastfreeze and ply.lastfreeze > CurTime() then
                return
            end
            ply.lastfreeze = CurTime() + 5
            ply.runslowed =true
             ply:SetNWInt("speedmul", slowspeed)
             ply:SetNWInt("speedmul",slowspeed)
            ply.oldwalkspeed = ply:GetWalkSpeed()
            ply.oldrunspeed = ply:GetRunSpeed()
            ply:SetWalkSpeed( slowspeed )
            ply:SetRunSpeed( slowspeed )

            timer.Create("NormalizeRunSpeedFreeze"..ply:UniqueID() ,resetafter,1,function()
                if IsValid(ply) and  ply.oldspeedmul  then
                    if tonumber(ply.oldspeedmul) < 220 then ply.oldspeedmul = 260 end

                    if not (ply.oldrunspeed) or tonumber(ply.oldrunspeed) < 260 then ply.oldrunspeed = 260 end
                    if not ply.oldwalkspeed or tonumber(ply.oldwalkspeed) < 220 then ply.oldwalkspeed = 260 end

                    ply.runslowed =false
                     ply:SetNWInt("speedmul", ply.oldspeedmul)
                    ply.oldspeedmul = false
                    ply:SetRunSpeed(ply.oldrunspeed )
                    ply:SetRunSpeed(ply.oldwalkspeed )

                end
            end)

        end
    end
end

-- FLY
SWEP.flyammo = 5
SWEP.nextFly = 0
function SWEP:Fly( soundfile, remote )
    if self.nextFly > CurTime() or CLIENT then return end
    self.nextFly = CurTime() + 0.30
    if self.flyammo < 1 then return end

    if not soundfile then
        if SERVER then self.Owner:EmitSound(Sound("weapons/ls/lightsaber_swing.mp3")) end
        if SERVER then self.Owner:EmitSound(Sound("player/suit_sprint.wav")) end
    else
        if SERVER then
           if remote then
               local soundfile = gb_config.websounds..soundfile
               gb_PlaySoundFromServer(soundfile, self.Owner)
           else
               self.Owner:EmitSound(Sound(soundfile))
           end
        end
    end
    self.flyammo = self.flyammo - 1
    self.Owner:SetAnimation(PLAYER_ATTACK1)
    local power = 400
    if math.random(1,5) < 2 then power = 100
    elseif math.random(1,15) < 2 then power = -500
    elseif math.random(1,15) < 2 then power = 2500
    else power = math.random( 350,450 ) end

    if SERVER then self.Owner:SetVelocity(self.Owner:GetForward() * power + Vector(0, 0, power)) end
    timer.Simple(20, function() if IsValid(self) then self.flyammo = self.flyammo + 1 end end)
    --timer.Simple(0.2,self.SecondaryAttackDelay,self)
end

-- Think HEAL
SWEP.NextHeal = 0
function SWEP:HealSecond()
    if CLIENT then return end
    autohealfunction(self.Owner)
end

-- Bolt

function SWEP:FireBolt( ignitetime, damage, cone)
    if not damage then damage = 0 end
    local pOwner = self.Owner;

    if ( not IsValid(pOwner)  ) then return;   end

    if ( SERVER ) then
            local vecAiming = pOwner:GetAimVector()
        if cone then
            vecAiming		= pOwner:GetAimVector() + Vector(math.Rand(-cone,cone),math.Rand(-cone,cone),math.Rand(-cone,cone));
        end
        local vecSrc		= pOwner:GetShootPos();

        local angAiming;
        angAiming = vecAiming:Angle();

        local pBolt = ents.Create ( "crossbow_bolt" );
        pBolt:SetPos( vecSrc );
        pBolt:SetAngles( angAiming );
        pBolt.Damage = damage --self.Primary.Damage;
        pBolt.m_iDamage = damage

        pBolt.AmmoType = "crossbow_bolt";
        pBolt:SetOwner( pOwner );
        pBolt:Spawn()

        if ignitetime and ignitetime > 0 then
            pBolt:Ignite(ignitetime,20)
        end

        if ( pOwner:WaterLevel() == 3 ) then
            pBolt:SetVelocity( vecAiming * 1500);
        else
            pBolt:SetVelocity( vecAiming * 3500 );
        end
    end
    --  pOwner:ViewPunch( Angle( -2, 0, 0 ) );
end

-- Flare

SWEP.flares = 20
SWEP.NextFlare = 0

local function flareRunIgniteTimer(ent, timer_name)
    if IsValid(ent) and ent:IsOnFire() then

        if ent:WaterLevel() > 0 then
            ent:Extinguish()
        elseif CurTime() > ent.burn_destroy then

            ent:Extinguish()
            if ent.willRemove  then
                ent:SetNotSolid(true)
                ent:Remove()
            end

        else
            -- keep on burning
            return
        end
    end

    timer.Destroy(timer_name) -- stop running timer
end

local function ScorchUnderRagdoll(ent)
    if SERVER then
        local postbl = {}
        -- small scorches under limbs
        for i = 0, ent:GetPhysicsObjectCount() - 1 do
            local subphys = ent:GetPhysicsObjectNum(i)
            if IsValid(subphys) then
                local pos = subphys:GetPos()
                util.PaintDown(pos, "FadingScorch", ent)

                table.insert(postbl, pos)
            end
        end
    end
    -- big scorch at center
    local mid = ent:LocalToWorld(ent:OBBCenter())
    mid.z = mid.z + 25
    util.PaintDown(mid, "Scorch", ent)
end

local function flareIgniteTarget(att, path, dmginfo)
    local ent = path.Entity

    -- ent.BelongsTo = ragdolled / tazed
    if not IsValid(ent) or ent.BelongsTo then return end

    if SERVER then
        local dur = ent:IsPlayer() and 5 or 10
        -- disallow if prep or post round
        if ent:IsPlayer() and (not GAMEMODE:AllowPVP()) then return end
        ent:Ignite(dur, 100)
        ent.ignite_info = { att = dmginfo:GetAttacker(), infl = dmginfo:GetInflictor() }

        if ent:IsPlayer() then
            timer.Simple(dur + 0.1, function()
                if IsValid(ent) then
                    ent.ignite_info = nil
                end
            end)

        elseif ent:GetClass() == "prop_ragdoll" then

            ScorchUnderRagdoll(ent)
            if not att.burnedBody then
                ent.willRemove = true
                att.burnedBody = true
            end

            local burn_time = 3
            local tname = Format("ragburn_%d_%d", ent:EntIndex(), math.ceil(CurTime()))

            ent.burn_destroy = CurTime() + burn_time

            timer.Create(tname,
                0.1,
                math.ceil(1 + burn_time / 0.1), -- upper limit, failsafe
                function()
                    flareRunIgniteTimer(ent, tname)
                end)
        end
    end
end

function SWEP:ShootFlare()
    if self.flares < 1 or self.NextFlare > CurTime() then return end
    self.NextFlare = CurTime() + 5
    self.Weapon:EmitSound("Weapon_USP.SilencedShot")
    self.Weapon:SendWeaponAnim(ACT_VM_PRIMARYATTACK)
    self.flares = self.flares - 1
    if IsValid(self.Owner) then
        self.Owner:SetAnimation(PLAYER_ATTACK1)

        self.Owner:ViewPunch(Angle(math.Rand(-0.2, -0.1) * self.Primary.Recoil, math.Rand(-0.1, 0.1) * self.Primary.Recoil, 0))
    end
    local cone = 0.001
    local bullet = {}
    bullet.Num = 1
    bullet.Src = self.Owner:GetShootPos()
    bullet.Dir = self.Owner:GetAimVector()
    bullet.Spread = Vector(cone, cone, 0)
    bullet.Tracer = 1
    bullet.Force = 2
    bullet.Damage = 15
    bullet.TracerName = self.Tracer
    bullet.Callback = flareIgniteTarget

    self.Owner:FireBullets(bullet)
end

--PLay sound on hit

function SWEP:PlaySoundOnHit( sound )
    local tr = self.Owner:GetEyeTrace()
    if SERVER then
        local entz = tr.Entity
        if entz:IsPlayer() or entz:IsNPC() then
            entz:EmitSound( sound )
        end
    end

end

--Disorentate

function SWEP:Disorientate()
    local tr = self.Owner:GetEyeTrace()
    if SERVER then
        local entz = tr.Entity
        if (entz:IsPlayer() or entz:IsNPC()) and  isfunction(entz.SetEyeAngles) then
            if (self.Owner.NextDisOrentate and self.Owner.NextDisOrentate > CurTime()) then return end
            self.Owner.NextDisOrentate = CurTime() + 4
            local eyeang = entz:EyeAngles()
            local j = 10
            eyeang.pitch = math.Clamp(eyeang.pitch + math.Rand(-j, j), -10, 10)
            eyeang.yaw = math.Clamp(eyeang.yaw + math.Rand(-j, j), -10, 10)
            entz:SetEyeAngles(eyeang)
        end
    end

end

-- STAB
SWEP.nextstab = 0
function SWEP:StabShoot(soundy, damage, fired)
    if not damage then damage = 45 end
    if self.nextstab > CurTime() then return end
    self.nextstab = CurTime() + 1

    self.Owner:LagCompensation(true)
    if soundy and SERVER then
        self.Owner:EmitSound(soundy)
    end


    local spos = self.Owner:GetShootPos()
    local sdest = spos + (self.Owner:GetAimVector() * 80)

    local kmins = Vector(1,1,1) * -10
    local kmaxs = Vector(1,1,1) * 10

    local tr = util.TraceHull({start=spos, endpos=sdest, filter=self.Owner, mask=MASK_SHOT_HULL, mins=kmins, maxs=kmaxs})

    -- Hull might hit environment stuff that line does not hit
    if not IsValid(tr.Entity) then
        tr = util.TraceLine({start=spos, endpos=sdest, filter=self.Owner, mask=MASK_SHOT_HULL})
    end

    local hitEnt = tr.Entity

    -- effects
    if IsValid(hitEnt) then
        self.Weapon:SendWeaponAnim( ACT_VM_HITCENTER )
        if fired then
            hitEnt:Ignite(1,1)
        end
        local edata = EffectData()
        edata:SetStart(spos)
        edata:SetOrigin(tr.HitPos)
        edata:SetNormal(tr.Normal)
        edata:SetEntity(hitEnt)

        if hitEnt:IsPlayer() or hitEnt:GetClass() == "prop_ragdoll" then
            util.Effect("BloodImpact", edata)
        end
    else
        self.Weapon:SendWeaponAnim( ACT_VM_MISSCENTER )
        self.Owner:ViewPunch( Angle( rnda,rndb,rnda ) )
    end

    if SERVER then
        self.Owner:SetAnimation( PLAYER_ATTACK1 )
    end


    if SERVER and tr.Hit and tr.HitNonWorld and IsValid(hitEnt)  then
        if hitEnt:IsPlayer() and not hitEnt:IsGhost() then
            if not self.KnifeUsed then self.KnifeUsed = 1 end
            self.KnifeUsed = self.KnifeUsed + 1
            if self.Owner:Health() < 150 then
                self.Owner:SetHealth(self.Owner:Health()+10)
            end
            -- knife damage is never karma'd, so don't need to take that into
            -- account we do want to avoid rounding error strangeness caused by
            -- other damage scaling, causing a death when we don't expect one, so
            -- when the target's health is close to kill-point we just kill

            local dmg = DamageInfo()
            if not damage then damage = 25 end
            dmg:SetDamage(damage)
            dmg:SetAttacker(self.Owner)
            dmg:SetInflictor(self.Weapon or self)
            dmg:SetDamageForce(self.Owner:GetAimVector() * 5)
            dmg:SetDamagePosition(self.Owner:GetPos())
            dmg:SetDamageType(DMG_SLASH)

            hitEnt:DispatchTraceAttack(dmg, spos + (self.Owner:GetAimVector() * 3), sdest)

        end
    end
    local owner = self.Owner
    self.Owner:LagCompensation(false)
    if not IsValid(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end


    self.Owner:SetFOV( 100, 0.2 );
    timer.Simple(0.3, function() if IsValid(self) and IsValid(self.Owner) then self.Owner:SetFOV( 0, 0.5 ) end end)

    self.Owner:SetAnimation(ACT_RELOAD_PISTOL)
    self.Weapon:SendWeaponAnim(ACT_SHOTGUN_RELOAD_FINISH)
    owner:ViewPunch( Angle( -10, 0, 0 ) )
end

-- RocketJump
SWEP.rflyammo = 15
SWEP.rnextFly = 0
function SWEP:rFly()
    if self.rnextFly > CurTime() or CLIENT then return false end
    self.rnextFly = CurTime() + 0.30
    if self.rflyammo < 1 then return end

    self.rflyammo = self.rflyammo - 1
    self.Owner:SetAnimation(PLAYER_ATTACK1)

    if SERVER then self.Owner:EmitSound(Sound(self.Primary.Sound )) end
    --if SERVER then self.Owner:EmitSound(Sound("player/suit_sprint.wav")) end

    local ent = ents.Create( "env_explosion" )
    if( IsValid( self.Owner ) ) then
        ent:SetPos( self.Owner:GetShootPos() )
        ent:SetOwner( self.Owner )
        ent:SetKeyValue( "iMagnitude", "0" )
        ent:Spawn()

        ent:Fire( "Explode", 0, 0 )
        ent:EmitSound( "physics/body/body_medium_break2.wav", 350, 100 )
    end


    if SERVER then self.Owner:SetVelocity(self.Owner:GetForward() * -900 ) end
    timer.Simple(20, function() if IsValid(self) then self.rflyammo = self.rflyammo + 1 end end)
    return true
    --timer.Simple(0.2,self.SecondaryAttackDelay,self)
end

-- ZEDTIME
SWEP.NextZedPress = 0
function SWEP:ZedTime(soundy)
    if self.NextZedPress > CurTime() then return end
    self.NextZedPress = CurTime() + 0.5
    if self.ZedTimeUsed then
        self.Owner:PrintMessage( HUD_PRINTCENTER, "Already used this round" )
        return

    end
    if  (self.Owner.NextZedTimeCustomUse and self.Owner.NextZedTimeCustomUse > CurTime()) then
         self.Owner:PrintMessage( HUD_PRINTCENTER,  math.floor(self.Owner.NextZedTimeCustomUse - CurTime()).." seconds left" )
        return
    end

    self.ZedTimeUsed = true;
    if SERVER then
        self.Owner:GiveEquipmentItem(EQUIP_ZEDTIME)
    end
    self.Owner.NextZedTimeCustomUse = CurTime() + 600

    if soundy then
    local soundfile = gb_config.websounds..soundy
    gb_PlaySoundFromServer(soundfile)
    end

    if CLIENT then
        net.Start("FourSecondSlow")
        net.SendToServer()
        if not soundy then
            soundy = "zt_enter.mp3"
        end



        timer.Simple(2.5,function()
            local sound = "zt_exit.mp3"
            local soundfile = gb_config.websounds..sound
            gb_PlaySoundFromServer(soundfile)
        end)
    end
end


--DetonationTime

SWEP.NextDetTime = 0
SWEP.DetTime = 20
function SWEP:SetDetTimer()
    if self.NextDetTime > CurTime() then
        return end

    self.NextDetTime = CurTime() + 0.2

    if self.DetTime == 10 then
        self.DetTime = 20
    elseif self.DetTime == 20 then
        self.DetTime = 30
    elseif self.DetTime == 30 then
        self.DetTime = 45
    elseif self.DetTime == 45 then
        self.DetTime = 60
    elseif self.DetTime == 60 then
        self.DetTime = 90
    elseif self.DetTime == 90 then
        self.DetTime = 180
    else
        self.DetTime = 10
    end

    self.Owner:PrintMessage(HUD_PRINTCENTER,"Detonation Time: "..self.DetTime.." seconds")

end

-- NormalPrimaryAttack
function SWEP:NormalPrimaryAttack(worldsnd)

    self.Weapon:SetNextSecondaryFire( CurTime() + self.Primary.Delay )
    self.Weapon:SetNextPrimaryFire( CurTime() + self.Primary.Delay )

    if not self:CanPrimaryAttack() then return end

    if not worldsnd then
        self.Weapon:EmitSound( self.Primary.Sound, self.Primary.SoundLevel )
    elseif SERVER then
        sound.Play(self.Primary.Sound, self:GetPos(), self.Primary.SoundLevel)
    end

    self:ShootBullet( self.Primary.Damage, self.Primary.Recoil, self.Primary.NumShots, self:GetPrimaryCone() )

    self:TakePrimaryAmmo( 1 )

    local owner = self.Owner
    if not IsValid(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

    owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )
end

SWEP.nexttracer = 0
-- Tracer
function SWEP:ShootTracer( tracername)
   -- AR2Tracer - Self-descripting. Visible from all directions except under it
   -- AirboatGunHeavyTracer - Makes a bigger version of the Pulse-rifle Tracer.
   -- AirboatGunTracer - Similar to the Pulse-rifle Tracer, only visible from above.
   -- Tracer - 9mm pistol tracer
   -- StriderTracer - Similar to AR2 tracer
   -- HelicopterTracer - Similar to GunshipTracer effect
   -- GunshipTracer - 2x the size of the pulse-rifle tracer
   -- LaserTracer - The tool tracer effect from the Tool Gun
    -- LaserTracer_thick
    -- manatrace
    if self.nexttracer > CurTime() then return end
    self.nexttracer = CurTime() + 0.3
    local cone = self.Primary.Cone
    local bullet = {}
    bullet.Num = self.Primary.NumShots
    bullet.Src = self.Owner:GetShootPos()
    bullet.Dir = self.Owner:GetAimVector()
    bullet.Spread = Vector(cone, cone, 0)
    bullet.Tracer = 1
    bullet.Force = 2
    bullet.Damage = 0
    bullet.TracerName = tracername
    self.Owner:FireBullets(bullet)
end

-- Frag Round
function SWEP:ShootFrag( damage)
    if SERVER then
        local ent = ents.Create("round_frag")
        local AimVec = self.Owner:GetAimVector()
        local EyeAng = self.Owner:EyeAngles()
        local Offset = 0

            Offset = EyeAng:Up() * -5
        if not damage then
            damage = self.Primary.NumShots * self.Primary.Damage
        end

        ent:SetPos(self.Owner:GetShootPos() + AimVec * 30 + Offset)
        ent:SetAngles(self.Owner:EyeAngles())
        ent:Spawn()
        ent:SetOwner(self.Owner)
        ent:GetPhysicsObject():SetVelocity(AimVec * 10000)
        ent.Owner = self.Owner
        ent.Damage = damage
    end
    self.Weapon:SendWeaponAnim(ACT_VM_PRIMARYATTACK)
end



-- Jihad
SWEP.NextJihad = 0
function SWEP:Jihad( soundfile, magnitude)
    if self.NextJihad > CurTime() then
        return
    end
    self.NextJihad = CurTime() + 5
    if not magnitude then magnitude = 30 end

    local effectdata = EffectData()
    effectdata:SetOrigin( self.Owner:GetPos() )
    effectdata:SetNormal( self.Owner:GetPos() )
    effectdata:SetMagnitude( magnitude )
    effectdata:SetScale( 1 )
    effectdata:SetRadius( 150 )
    util.Effect( "Sparks", effectdata )
    self.BaseClass.ShootEffects( self )

    -- The rest is only done on the server
    if (SERVER) then
        local owner = self.Owner
        timer.Create(owner:SteamID().."jihad",2,1, function() if IsValid(self) then self:JihadBoom(owner) end end )
        if soundfile then
            local soundfile = gb_config.websounds..soundfile
            gb_PlaySoundFromServer(soundfile, self)
        else
            owner:EmitSound( "weapons/jihad.mp3", 490, 100 )
        end
    end
end


function SWEP:JihadBoom(owner)
    local k, v
    if not ents or not SERVER then return end
    local ent = ents.Create( "env_explosion" )
    if( IsValid(owner) ) then
        ent:SetPos( owner:GetPos() )
        ent:SetOwner( owner )
        ent:SetKeyValue( "iMagnitude", "275" )
        ent:Spawn()
        owner:Kill( )
        self:Remove()
        ent:Fire( "Explode", 0, 0 )
        ent:EmitSound( "weapons/big_explosion.mp3", 511, 100 )
    end
end


-- Check for customs
function SWEP:has_customs(ply)

    if not ply then  ply =  self.Owner end
    for k, v in pairs( ply:GetWeapons() ) do
        if string.find(v:GetClass(),"custom_") then
            ply:PrintMessage( HUD_PRINTCENTER, "You can't use this when you have custom weapons" )
            if SERVER then
                ply:DropWeapon(self)
            end
            return true
        end
    end
    return false
end

-- Normal REload
function SWEP:NormalReload()
    if ( self:Clip1() == self.Primary.ClipSize or self.Owner:GetAmmoCount( self.Primary.Ammo ) <= 0 ) then return end
    self:DefaultReload(self.ReloadAnim)
    self:SetIronsights( false )
end

-- CANNIBAL
SWEP.NextReload = 0
SWEP.numNomNom = 2

function SWEP:Cannibal( eattime, length )
    if  self.numNomNom  > 0 and self.NextReload < CurTime() then
        if not eattime then eattime = 6.1 end
        if not length then length = 80 end

        local tracedata = {}
        tracedata.start = self.Owner:GetShootPos()
        tracedata.endpos = self.Owner:GetShootPos() + (self.Owner:GetAimVector() * 100)
        tracedata.filter = self.Owner
        tracedata.mins = Vector(1,1,1) * -10
        tracedata.maxs = Vector(1,1,1) * 10
        tracedata.mask = MASK_SHOT_HULL
        local tr = util.TraceLine({start = self.Owner:EyePos(), endpos = self.Owner:EyePos() + self.Owner:EyeAngles():Forward() * length, filter = self.Owner})

        local ply = self.Owner
        self.NextReload = CurTime() + 0.8
        if IsValid(tr.Entity) then
            if tr.Entity.player_ragdoll then
                self.numNomNom = self.numNomNom -1

                self:SetClip1( self:Clip1() - 1)

                self.NextReload = CurTime() + 5
                timer.Simple(0.1, function()
                    ply:Freeze(true)
                    ply:SetColor(Color(255,0,0,255))
                end)
                self.Weapon:SendWeaponAnim(ACT_VM_PRIMARYATTACK)

                timer.Create("GivePlyHealth_"..self.Owner:UniqueID(),0.5,6,function() if(IsValid(self)) and IsValid(self.Owner) then
                    if self.Owner:Health() < 175 then
                        self.Owner:SetHealth(self.Owner:Health()+5)
                    end
                end end)
                self.Owner:EmitSound("ttt/nomnomnom.mp3")
                timer.Simple(3,function()
                    if not IsValid(ply) then return end
                    self.Owner:EmitSound("ttt/nomnomnom.mp3")
                end)
                timer.Simple(eattime, function()
                    if not IsValid(ply) then return end
                    ply:Freeze(false)
                    ply:SetColor(Color(255,255,255,255))
                    tr.Entity:Remove()
                --self:Remove()
                end )
            end
        end

    end

end

-- Explosion

SWEP.NextBoom = 0

function SWEP:MakeExplosion(soundy, magnitude)

    if self.NextBoom > CurTime() then return end
    self.NextBoom = (CurTime() + 1.5)
    if SERVER then
        if not magnitude then magnitude = 35 end
        local tr = self.Owner:GetEyeTrace()
        local ent = ents.Create( "env_explosion" )

        ent:SetPos( tr.HitPos  )
        ent:SetOwner( self.Owner  )
        ent:SetPhysicsAttacker(  self.Owner )
        ent:Spawn()
        ent:SetKeyValue( "iMagnitude", magnitude )
        ent:Fire( "Explode", 0, 0 )

        util.BlastDamage( self, self:GetOwner(), self:GetPos(), 50, 75 )
        if not soundy then
            ent:EmitSound( "weapons/big_explosion.mp3" )
        else

            ent:EmitSound(soundy)
        end
    end

    --  self:TakePrimaryAmmo( 1 )
    self.Owner:SetAnimation( PLAYER_ATTACK1 )
    local owner = self.Owner
    if not IsValid(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

    owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )
end


-- Sphereexplosion
function SWEP:SphereExplosion( damage )
    if not damage then damage = math.random(10,80) end
local pl = self.Owner
if not SERVER then return end
local guys = ents.FindInSphere( pl:GetPos()+Vector(0,0,40), 130 )

for k, guy in pairs(guys) do
    if guy:IsPlayer() and guy ~= pl then

        local trace = {}
        trace.start = pl:GetPos()+Vector(0,0,40)
        trace.endpos = guy:GetPos() + Vector ( 0,0,40 )
        trace.filter = pl
        local tr = util.TraceLine( trace )

        if tr.Entity:IsValid() and tr.Entity == guy then
            local Dmg = DamageInfo()
            Dmg:SetAttacker(pl)
            Dmg:SetInflictor(pl:GetActiveWeapon())
            Dmg:SetDamage(damage)
            Dmg:SetDamageType(DMG_BLAST)
            Dmg:SetDamagePosition(pl:GetPos()+Vector(0,0,40))
            Dmg:SetDamageForce(((guy:GetPos()+Vector(0,0,20)) - (pl:GetPos() + Vector ( 0,0,60 ))):GetNormal()*math.random(300,450))

            guy:SetVelocity(((guy:GetPos()+Vector(0,0,60)) - (pl:GetPos() + Vector ( 0,0,40 ))):GetNormal()*math.random(300,450))

            guy:TakeDamageInfo(Dmg)
        end
    end
end

local ef = EffectData()
ef:SetOrigin(pl:GetPos()+Vector(0,0,40))
util.Effect("undead_explosion", ef,true,true)

util.ScreenShake( pl:GetPos()+Vector(0,0,40), math.random(3,6), math.random(3,4), math.random(2,3), 110 )

pl:EmitSound("ambient/explosions/explode_"..math.random(7,9)..".wav",90,math.random(80,110))
end


-- THROW ATTACK

function SWEP:throw_attack (entitys, power)
    local tr = self.Owner:GetEyeTrace();
    if not power then power = 100 end

    self:EmitSound(self.Primary.Sound)
    self.BaseClass.ShootEffects (self);
    if (not SERVER) then return end;

    local  ent = ents.Create (entitys);

    ent:SetPos (self.Owner:EyePos() + (self.Owner:GetAimVector() * 25));
    ent:SetAngles (self.Owner:EyeAngles());
    ent:SetPhysicsAttacker(self.Owner)
    ent.Owner = self.Owner

    ent:Spawn();
    ent.Mag = (self:Clip1() * 2 ) + 7

    local phys = ent:GetPhysicsObject();
    local shot_length = tr.HitPos:Length();
    phys:ApplyForceCenter (self.Owner:GetAimVector():GetNormalized() * power);

    cleanup.Add (self.Owner, "props", ent);
    undo.Create ("Thrown model");
    undo.AddEntity (ent);
    undo.SetPlayer (self.Owner);
    undo.Finish();
    return ent
end


function SWEP:DropHealth(ent, model)
    if self.NextHealthDrop > CurTime() then return end
    self.NextHealthDrop = CurTime() + 5
    local healttt = 200
    if self.healththing and SERVER then
        if Entity(self.healththing).GetStoredHealth then
            healttt = Entity(self.healththing):GetStoredHealth()
        else
            healttt = 200
        end
        Entity(self.healththing):Remove()
    end
    self:HealthDrop(ent,healttt,model)
end


SWEP.NextHealthDrop = 0

local throwsound = Sound( "Weapon_SLAM.SatchelThrow" )

function SWEP:HealthDrop(ent, healttt, model)
    if SERVER then
        local ply = self.Owner
        if not IsValid(ply) then return end

        local vsrc = ply:GetShootPos()
        local vang = ply:GetAimVector()
        local vvel = ply:GetVelocity()

        local vthrow = vvel + vang * 200

        local health = ents.Create(ent)
        if model then
            health.model = model
            health:SetModel(model)
        end
        if IsValid(health) then
            health:SetPos(vsrc + vang * 10)
            health:Spawn()
            --health:SetModel( "models/props_lab/cactus.mdl")
            health:SetModelScale(health:GetModelScale()*1,0)
            health:SetPlacer(ply)
            --health.healsound = "ginger_cartman.mp3"

            health:PhysWake()
            local phys = health:GetPhysicsObject()
            if IsValid(phys) then
                phys:SetVelocity(vthrow)
            end
            if model then
                health:SetModel(model)
                timer.Simple(0.5,function()
                    if IsValid(health) then
                        health:SetModel(model)
                    end
                end)
            end
            --  health:SetModel( "models/props_lab/cactus.mdl")
            health:SetStoredHealth(200)
            health.setOwner = self.Owner

            self.healththing = health:EntIndex()

            self.Planted = true
        end
    end

    self.Weapon:EmitSound(throwsound)
end


SWEP.NextSuperman = 0
function SWEP:Superman()
    if self.NextSuperman < CurTime() then
        self.NextSuperman = CurTime() + 5
        self.Owner:SetVelocity(self.Owner:GetForward() * 1000 + Vector(0,0,1000))
        self.hasSuperman = true
        self:EmitSound("ambient/wind/wind_hit3.wav")
    end
end

-- Superrunsped
function SWEP:SuperRun()
    local ply = self.Owner
    if not IsValid(ply) then return end
    if not ply.nextsuperspeed then ply.nextsuperspeed = 0 end
    if ply.nextsuperspeed > CurTime() then return end
    ply.nextsuperspeed = CurTime() + 60
    ply.runslowed = true
    ply.oldspeedmul = ply:GetNWInt("speedmul", 260)
    ply:SetNWInt("speedmul", 850)
    ply:SetRunSpeed(850)
    ply:SetWalkSpeed(850)
    ply:SetJumpPower(900)
    ply.oldrunspeed = ply:GetRunSpeed()
    ply.oldwalkspeed = ply:GetWalkSpeed()
    ply.oldjumppower = ply:GetJumpPower()

    timer.Create("NormalizeRunSpeedFreezesdf"..ply:SteamID() ,5,2,function()
        if IsValid(ply)   then
            ply.runslowed =false
            ply:SetNWInt("speedmul", ply.oldspeedmul or 260)
            ply:SetJumpPower(ply.oldjumppower or 200)
            ply:SetRunSpeed( ply.oldrunspeed or 300)
            ply:SetWalkSpeed(ply.oldwalkspeed or 240)
        end
    end)
    return
end

--- Throw like Knife
SWEP.nextsecondaryThrowLikeKnife = 0
function SWEP:ThrowLikeKnife(throwent, delay, damage)
    if self.nextsecondaryThrowLikeKnife > CurTime() then return end
    if not delay then delay = 0.4 end
    if not throwent then throwent = "normal_throw_knife" end
    self.nextsecondaryThrowLikeKnife = CurTime() + delay
    if SERVER then
        if not damage then damage = self.Primary.Damage end
        local ply = self.Owner
        if not IsValid(ply) then return end

        ply:SetAnimation( PLAYER_ATTACK1 )

        local ang = ply:EyeAngles()

        if ang.p < 90 then
            ang.p = -10 + ang.p * ((90 + 10) / 90)
        else
            ang.p = 360 - ang.p
            ang.p = -10 + ang.p * -((90 + 10) / 90)
        end

        local vel = math.Clamp((90 - ang.p) * 5.5, 550, 800)

        local vfw = ang:Forward()
        local vrt = ang:Right()

        local src = ply:GetPos() + (ply:Crouching() and ply:GetViewOffsetDucked() or ply:GetViewOffset())

        src = src + (vfw * 1) + (vrt * 3)

        local thr = vfw * vel + ply:GetVelocity()

        local knife_ang = Angle(-28,0,0) + ang
        knife_ang:RotateAroundAxis(knife_ang:Right(), -90)

        local knife = ents.Create(throwent)
        if not IsValid(knife) then return end
        knife:SetPos(src)
        knife:SetAngles(knife_ang)

        knife:Spawn()
        knife.owner = self.Owner

        knife.Damage = damage

        knife:SetOwner(ply)

        local phys = knife:GetPhysicsObject()
        if IsValid(phys) then
            phys:SetVelocity(thr)
            phys:AddAngleVelocity(Vector(0, 1500, 0))
            phys:Wake()
        end

        return knife

    end
end
