SWEP.Base = "gb_camo_base"
SWEP.ViewModelFOV	= 54
SWEP.HoldType = "pistol"
SWEP.Icon = "vgui/ttt_fun_killicons/p228.png"
SWEP.PrintName = "P228"
SWEP.ViewModel		= "models/weapons/cstrike/c_pist_p228.mdl"
SWEP.WorldModel		= "models/weapons/w_pist_p228.mdl"
SWEP.Kind = WEAPON_PISTOL
SWEP.Slot = 1
SWEP.AutoSpawnable = true
SWEP.Primary.Delay       = 0.14
SWEP.Primary.Recoil      = 1.9
SWEP.Primary.Automatic   = true
SWEP.Primary.Damage      = 20
SWEP.Primary.Cone        = 0.025
SWEP.Primary.Ammo        = "smg1"
SWEP.Primary.ClipSize    = 45
SWEP.Primary.ClipMax     = 90
SWEP.Primary.DefaultClip = 45
SWEP.Primary.Sound       = Sound( "Weapon_Glock.Single" )

SWEP.Primary.Sound = Sound( "Weapon_Glock.Single" )
SWEP.IronSightsPos = Vector( -6, -6, 2.6 )

SWEP.HeadshotMultiplier = 1.75
