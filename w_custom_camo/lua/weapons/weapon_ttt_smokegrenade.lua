SWEP.Base = "gb_camo_base_grenade"
SWEP.ViewModelFOV	= 54
SWEP.HoldType = "grenade"
SWEP.PrintName = "Smoke Grenade"
SWEP.ViewModel		= "models/weapons/cstrike/c_eq_smokegrenade.mdl"
SWEP.WorldModel		= "models/weapons/w_eq_smokegrenade.mdl"
SWEP.AutoSpawnable = true
SWEP.Primary.Delay		    = 1
SWEP.Primary.Sound          = Sound("npc/waste_scanner/grenade_fire.wav")

function SWEP:GetGrenadeName()
return "ttt_smokegrenade_proj"
end

function SWEP:PrimaryAttack()
    if SERVER then
        DamageLog("Smoke: " .. self.Owner:Nick() .. " [" .. self.Owner:GetRoleString() .. "] threw a grenade")
    end
    self.BaseClass.PrimaryAttack(self)
end
