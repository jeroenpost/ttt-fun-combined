SWEP.Base = "gb_camo_base"
SWEP.ViewModelFOV	= 54
SWEP.HoldType = "slam"
SWEP.PrintName = "MedKit"
SWEP.ViewModel		= "models/weapons/c_medkit.mdl"
SWEP.WorldModel		= "models/weapons/w_medkit.mdl"
SWEP.Slot = 6
SWEP.Kind = WEAPON_EQUIP1
SWEP.Primary.ClipSize		= 175
SWEP.Primary.DefaultClip	= 175
SWEP.Primary.Automatic		= false
SWEP.Primary.Ammo			= "none"
SWEP.AutoSpawnAble = true
SWEP.Secondary.ClipSize		= -1
SWEP.Secondary.DefaultClip	= -1
SWEP.Secondary.Automatic	= false
SWEP.Secondary.Ammo			= "none"

SWEP.HealAmount = 10 -- Maximum heal amount per use
SWEP.MaxAmmo = 175 -- Maxumum ammo

local HealSound = Sound( "items/smallmedkit1.wav" )
local DenySound = Sound( "items/medshotno1.wav" )

SWEP.NextHealll = 0

function SWEP:Initialize()

    self:SetHoldType( "slam" )

    if ( CLIENT ) then return end

    timer.Create( "medkit_ammo" .. self:EntIndex(), 1, 0, function()
        if ( !IsValid( self.Owner ) ) then return end
        if ( self:Clip1() < self.MaxAmmo ) then self:SetClip1( math.min( self:Clip1() + 2, self.MaxAmmo ) ) end
    end )

end

function SWEP:Reload()
    self:ShootSecondary()
end

function SWEP:PrimaryAttack()



    local tr = util.TraceLine( {
        start = self.Owner:GetShootPos(),
        endpos = self.Owner:GetShootPos() + self.Owner:GetAimVector() * 64,
        filter = self.Owner
    } )

    local ent = tr.Entity

    if ( CLIENT ) or ent.NextHeal and ent.NextHeal > CurTime() then return end

    local need = self.HealAmount
    if ( IsValid( ent ) ) then need = math.min( 175 - ent:Health(), self.HealAmount ) end

    if ( IsValid( ent ) && self:Clip1() >= need && ( ent:IsPlayer() || ent:IsNPC() ) && ent:Health() < 175 ) then


    ent:SetHealth( math.min( 175, ent:Health() + need ) )
    ent:EmitSound( HealSound )
    ent.NextHeal = CurTime() + 5

    self:SendWeaponAnim( ACT_VM_PRIMARYATTACK )

    self:SetNextPrimaryFire( CurTime() + self:SequenceDuration() + 5 )
    self.Owner:SetAnimation( PLAYER_ATTACK1 )

    -- Even though the viewmodel has looping IDLE anim at all times, we need this to make fire animation work in multiplayer
    timer.Create( "weapon_idle" .. self:EntIndex(), self:SequenceDuration(), 1, function() if ( IsValid( self ) ) then self:SendWeaponAnim( ACT_VM_IDLE ) end end )

    else

        self.Owner:EmitSound( DenySound )
        self:SetNextPrimaryFire( CurTime() + 5 )

    end

end

function SWEP:SecondaryAttack()



    local ent = self.Owner

    if ( CLIENT ) or ent.NextHeal and ent.NextHeal > CurTime() then return end

    local need = self.HealAmount
    if ( IsValid( ent ) ) then need = math.min( 175 - ent:Health(), self.HealAmount ) end

    if ( IsValid( ent ) && self:Clip1() >= need && ent:Health() < 175 ) then

    ent:SetHealth( math.min( 175, ent:Health() + need ) )
    ent:EmitSound( HealSound )
    ent.NextHeal = CurTime() + 5
    self:SendWeaponAnim( ACT_VM_PRIMARYATTACK )

    self:SetNextSecondaryFire( CurTime() + self:SequenceDuration() + 5 )
    self.Owner:SetAnimation( PLAYER_ATTACK1 )

    timer.Create( "weapon_idle" .. self:EntIndex(), self:SequenceDuration(), 1, function() if ( IsValid( self ) ) then self:SendWeaponAnim( ACT_VM_IDLE ) end end )

    else

        ent:EmitSound( DenySound )
        self:SetNextSecondaryFire( CurTime() + 5 )

    end

end

function SWEP:OnRemove()

    timer.Stop( "medkit_ammo" .. self:EntIndex() )
    timer.Stop( "weapon_idle" .. self:EntIndex() )

end

function SWEP:Holster()

    timer.Stop( "weapon_idle" .. self:EntIndex() )

    return true

end

function SWEP:CustomAmmoDisplay()

    self.AmmoDisplay = self.AmmoDisplay or {}
    self.AmmoDisplay.Draw = true
    self.AmmoDisplay.PrimaryClip = self:Clip1()

    return self.AmmoDisplay

end