SWEP.Base = "gb_camo_base"

SWEP.fwsound = {Sound('fireworks/firework_launch_1.mp3'),Sound('fireworks/firework_launch_2.mp3') }
SWEP.fwsound = {Sound('fireworks/firework_explosion_1.mp3'),Sound('fireworks/firework_explosion_2.mp3'),Sound('fireworks/firework_explosion_3.mp3')}

local function MakeFirework( pl, Ang, Pos, Col, FuseTime, FWScale, AutoUnfreeze, ExplodeTime, FlameColor, DamageExplode, CollisionExplode, TrajectoryRand, BlastDamage, Key )

    -- if ( !pl:CheckLimit( 'fireworks' ) ) then return false end

    local Firework = ents.Create( 'sent_firework' )
    if !Firework:IsValid() then return false end

    Firework:SetAngles( Ang )
    Firework:SetPos( Pos )
    Firework:SetupData( Col, FuseTime, FWScale, AutoUnfreeze, ExplodeTime, FlameColor, DamageExplode, CollisionExplode, TrajectoryRand, BlastDamage, Key )
    Firework.pl = pl
    Firework:Spawn()

    local Phys = Firework:GetPhysicsObject()

    if Phys:IsValid() then

        Phys:EnableMotion( false )

    end

    return Firework

end

hook.Add("ShouldCollide", "Fireworkscollide", function(ent1, ent2)
    if ent1.IsFirework  then
        if (ent2:IsPlayer() and ent1.pl != ent2) then
            return false
        end
    end

    if ent2.IsFirework then
        if (ent1:IsPlayer() and ent2.pl != ent1) then
            return false
        end
    end

end)

SWEP.NextFireWork = 0
SWEP.NumFirework = 12
SWEP.NextDance = 0
function SWEP:FireWorkie( spawnting )
    if self.NextFireWork > CurTime() or self.NumFirework < 1 then return end
    self.NextFireWork = CurTime() + 0.3
    self.NumFirework = self.NumFirework - 1
    trace = self.Owner:GetEyeTrace()
    timer.Simple(20,function()
        if IsValid(self) then
            self.NumFirework = self.NumFirework + 1
        end
    end)


    if ( trace.Entity and trace.Entity:IsPlayer() ) then return false end
    if (CLIENT) then return true end

    if self.NextDance < CurTime() then
        self.NextDance = CurTime() + 60
        local danceMoves = {ACT_GMOD_TAUNT_ROBOT,ACT_GMOD_TAUNT_DANCE,ACT_GMOD_TAUNT_MUSCLE}

        for k,victim in pairs(player.GetAll()) do
            local  danceMove = table.Random(danceMoves)
            if (!victim:IsPlayer()) then return end
            victim:AnimPerformGesture(danceMove);

        end
    end

    local ply = self.Owner
    local Ang = trace.HitNormal:Angle() + Angle( 90, 0, 0 )


    local ExplosionCol = HSVToColor( math.Rand(0,360), math.Rand( 0.9, 1 ), math.Rand( 0.75, 1 ) )

    r	= ExplosionCol.r
    g	= ExplosionCol.g
    b	= ExplosionCol.b

    local FlameCol = HSVToColor( math.Rand(0,360), math.Rand( 0.9, 1 ), math.Rand( 0.75, 1 ) )

    flame_r	= FlameCol.r
    flame_g	= FlameCol.g
    flame_b	= FlameCol.b


    local key			= 999

    local FuseTime			=		3
    local ExplodeTime		=		4
    local Scale				=		math.Clamp( 1, 0.1, 3 )
    local Col				=		Color( r, g, b )
    local FlameColor		=		Vector( flame_r, flame_g, flame_b )
    local AutoUnfreeze		=		true
    local DamageExplode		=		false
    local CollisionExplode	=		true
    local TrajectoryRand	=		math.Clamp( 10, 0, 30 )
    local BlastDamage		=		0

    if ( trace.Entity:IsValid() and trace.Entity:GetClass() == 'sent_firework' and trace.Entity.pl == ply ) then

        trace.Entity:SetupData( Col, FuseTime, Scale, AutoUnfreeze, ExplodeTime, FlameColor, DamageExplode, CollisionExplode, TrajectoryRand, BlastDamage, key )

        return true

    end

    -- if ( !self:GetSWEP():CheckLimit( 'fireworks' ) ) then return false end

    local Firework = MakeFirework( ply, Ang, trace.HitPos + trace.HitNormal, Col, FuseTime, Scale, AutoUnfreeze, ExplodeTime, FlameColor, DamageExplode, CollisionExplode, TrajectoryRand, BlastDamage, key )

    undo.Create( 'Firework' )
    undo.AddEntity( Firework )
    undo.SetPlayer( ply )
    undo.Finish()

    return true

end
