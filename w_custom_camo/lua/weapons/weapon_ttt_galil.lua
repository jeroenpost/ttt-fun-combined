SWEP.Base = "gb_camo_base"
SWEP.ViewModelFOV	= 54
SWEP.HoldType = "smg"
SWEP.PrintName = "Galil"
SWEP.ViewModel		= "models/weapons/cstrike/c_rif_galil.mdl"
SWEP.WorldModel		= "models/weapons/w_rif_galil.mdl"
SWEP.AutoSpawnable = true

SWEP.HoldType = "ar2"
SWEP.Slot = 2

if CLIENT then

    SWEP.Author = "GreenBlack"

    SWEP.Icon = "vgui/ttt_fun_killicons/galil.png"
end

SWEP.Spawnable = true
SWEP.AdminSpawnable = true
SWEP.Kind = WEAPON_HEAVY
SWEP.Primary.Damage = 16
SWEP.Primary.Delay = 0.13
SWEP.Primary.Cone = 0.00
SWEP.Primary.ClipSize = 35
SWEP.Primary.ClipMax = 105
SWEP.Primary.DefaultClip	= 35
SWEP.Primary.Automatic = true
SWEP.Primary.Ammo = "smg1"
SWEP.AmmoEnt = "item_ammo_smg1_ttt"
SWEP.AutoSpawnable = true
SWEP.Primary.Recoil = 2.2
SWEP.Primary.Sound = Sound("weapons/galil/galil-1.wav")

SWEP.HeadshotMultiplier = 1.2

SWEP.IronSightsPos = Vector( -6.4, -6.38, 2.5 )
SWEP.IronSightsAng = Vector( 0, 0.00, 0 )