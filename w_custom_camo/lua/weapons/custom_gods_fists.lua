
SWEP.Author			= "Spastik"
SWEP.Instructions	= "Left click to scratch."
SWEP.Contact		= ""
SWEP.Purpose		= "To scratch backs and provide itching relief."
SWEP.Category = "S&G Munitions"

SWEP.ViewModelFlip	= false
SWEP.PrintName="God's Fists";
SWEP.Slot=0;
SWEP.SlotPos=1;
SWEP.DrawAmmo=true;
SWEP.DrawCrosshair=true;
SWEP.Icon = "vgui/entities/the backscratcher"
SWEP.Spawnable			= true
SWEP.AdminSpawnable		= true
SWEP.Kind = WEAPON_EQUIP3
SWEP.NextStrike = 0;
SWEP.Secondary.Ammo = "none"
SWEP.HoldType = "fist"
SWEP.ViewModelFOV = 48
SWEP.ViewModelFlip = false
SWEP.ViewModel = "models/weapons/v_fists.mdl"
SWEP.WorldModel = "models/weapons/w_fists.mdl"
SWEP.ShowViewModel = true
SWEP.ShowWorldModel = true
SWEP.ViewModelBoneMods = {}

SWEP.VElements = {
	["claw+"] = { type = "Model", model = "models/gibs/Antlion_gib_medium_3a.mdl", bone = "Bip01 R Hand", rel = "Palm", pos = Vector(5.908, 3.181, -1.364), angle = Angle(-15.341, -84.887, -13.296), size = Vector(0.5, 0.5, 0.5), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["gaurder"] = { type = "Model", model = "models/props_c17/playground_swingset_seat01a.mdl", bone = "Bip01 R Arm2", rel = "amguard", pos = Vector(0.455, -0.456, 4.091), angle = Angle(0, 180, 180), size = Vector(0.5, 0.5, 0.5), color = Color(255, 255, 255, 255), surpresslightning = false, material = "Debug/env_cubemap_model_translucent_fountain", skin = 0, bodygroup = {} },
	["Palm"] = { type = "Model", model = "models/gibs/Antlion_gib_Large_2.mdl", bone = "Bip01 R Hand", rel = "", pos = Vector(5.908, 2.273, 1.363), angle = Angle(-5.114, -9.205, 66.476), size = Vector(0.5, 0.5, 0.5), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["amguard"] = { type = "Model", model = "models/gibs/Scanner_gib01.mdl", bone = "Bip01 R Arm2", rel = "", pos = Vector(11.364, 1.363, -0.456), angle = Angle(-180, -5.114, 5.113), size = Vector(0.549, 0.379, 0.947), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["grip"] = { type = "Model", model = "models/props_c17/utilityconnecter005.mdl", bone = "Bip01 R Hand", rel = "", pos = Vector(4.091, -1.201, 0.455), angle = Angle(-3.069, -15.341, -101.25), size = Vector(0.435, 0.435, 0.435), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["claw2"] = { type = "Model", model = "models/gibs/Antlion_gib_medium_3.mdl", bone = "Bip01 R Hand", rel = "Palm", pos = Vector(5, 0.455, -0.456), angle = Angle(115.568, -178.978, 91.023), size = Vector(0.5, 0.5, 0.5), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["claw"] = { type = "Model", model = "models/gibs/Antlion_gib_medium_3a.mdl", bone = "Bip01 R Hand", rel = "Palm", pos = Vector(5, -2.274, -2.274), angle = Angle(15.34, -84.887, -25.569), size = Vector(0.5, 0.5, 0.5), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
}
SWEP.WElements = {
	["claw+"] = { type = "Model", model = "models/gibs/Antlion_gib_medium_3a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "Palm", pos = Vector(4.091, 2.273, -1.364), angle = Angle(-15.341, -84.887, -15.341), size = Vector(0.379, 0.379, 0.379), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["arm guard"] = { type = "Model", model = "models/gibs/Scanner_gib01.mdl", bone = "ValveBiped.Bip01_R_Forearm", rel = "", pos = Vector(6.817, -0.456, -0.456), angle = Angle(-180, 3.068, -27.615), size = Vector(0.435, 0.435, 0.435), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["Palm"] = { type = "Model", model = "models/gibs/Antlion_gib_Large_2.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(2.273, 0.455, 0.455), angle = Angle(1.023, -13.296, -56.25), size = Vector(0.379, 0.379, 0.379), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["guarder"] = { type = "Model", model = "models/props_c17/playground_swingset_seat01a.mdl", bone = "ValveBiped.Bip01_R_Forearm", rel = "", pos = Vector(5, -1.364, -3.182), angle = Angle(-1.024, 3.068, 23.523), size = Vector(0.379, 0.379, 0.379), color = Color(255, 255, 255, 255), surpresslightning = false, material = "Debug/env_cubemap_model_translucent_fountain", skin = 0, bodygroup = {} },
	["grip"] = { type = "Model", model = "models/props_c17/utilityconnecter005.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "Palm", pos = Vector(0.455, 0.455, -2.274), angle = Angle(0, 0, 0), size = Vector(0.321, 0.321, 0.321), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["claw2"] = { type = "Model", model = "models/gibs/Antlion_gib_medium_3.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "Palm", pos = Vector(5, 0.455, -0.456), angle = Angle(115.568, -178.978, 91.023), size = Vector(0.379, 0.379, 0.379), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["claw"] = { type = "Model", model = "models/gibs/Antlion_gib_medium_3a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "Palm", pos = Vector(4.091, -1.364, -1.364), angle = Angle(19.431, -91.024, -15.341), size = Vector(0.379, 0.379, 0.379), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
}
-------------Primary Fire Attributes----------------------------------------
SWEP.Primary.Delay			= 0.2 	--In seconds
SWEP.Primary.Recoil			= 0		--Gun Kick
SWEP.Primary.Damage			= 25	--Damage per Bullet
SWEP.Primary.NumShots		= 1		--Number of shots per one fire
SWEP.Primary.Cone			= 0 	--Bullet Spread
SWEP.Primary.ClipSize		= -1	--Use "-1 if there are no clips"
SWEP.Primary.DefaultClip	= 0	--Number of shots in next clip
SWEP.Primary.Automatic   	= false	--Pistol fire (false) or SMG fire (true)
SWEP.Primary.Ammo         	= "none"	--Ammo Type
SWEP.Secondary.Automatic 	= true
SWEP.Base = "aa_base"

SWEP.Kind = WEAPON_MELEE
SWEP.Slot = 0
function SWEP:Precache()
end

function SWEP:PreDrop(yes)
    if yes then self:Remove() end
end
SWEP.Sounds = { "dubstep.s1","dubstep.s2","dubstep.s3" }

function SWEP:OnDrop()
    self:Remove()
end

SWEP.playsound = false
SWEP.SoundObject = false
SWEP.playsound = false
SWEP.Damage = 5
SWEP.LastSoundRelease = 0
SWEP.RestartDelay = 1
SWEP.Volume = 0
SWEP.Influence = 0
SWEP.OPMode = false
SWEP.ModeReady = true
SWEP.ShakesNormal = 0
SWEP.Shakes = SWEP.ShakesNormal
SWEP.TotalShakes = 0
SWEP.Charged = false
SWEP.DoShakes = false
SWEP.danceMoves = {ACT_GMOD_TAUNT_ROBOT,ACT_GMOD_TAUNT_DANCE,ACT_GMOD_TAUNT_MUSCLE}
SWEP.lastActivate = 0

function SWEP:Deploy()
    self:SetNWString("shootmode","Normal")

    self.playsound = Sound( table.Random(self.Sounds))
end

function SWEP:PrimaryAttack()
    if self.Weapon.nextreload2 and self.Weapon.nextreload2 > CurTime() then return end
    self.Weapon.nextreload2 = CurTime() + 1

    self:EmitSound("weapons/explode4.wav")
    self.Weapon:SetNextPrimaryFire( CurTime() + 1 )

    self:ShootBullet( 12, 2, 12, 0.085 )

    self:TakePrimaryAmmo( 1 )

    local owner = self.Owner
    if not IsValid(owner) or owner:IsNPC() or (not owner.ViewPunch) then return end

    owner:ViewPunch( Angle( math.Rand(-0.2,-0.1) * self.Primary.Recoil, math.Rand(-0.1,0.1) *self.Primary.Recoil, 0 ) )
    return
end


SWEP.HadReload = false
function SWEP:Reload()
    if self.HadReload then return end


    local trace = self.Owner:GetEyeTrace()
    if (trace.HitNonWorld) then
        local victim = trace.Entity
        if CLIENT or ( not victim:IsPlayer()) or (specialRound and specialRound.isSpecialRound) then return end

        self.HadReload = true
        local v = victim
        if v:InVehicle() then
            local vehicle = v:GetParent()
            v:ExitVehicle()
        end
        v:SetMoveType(MOVETYPE_WALK)
        local tcolor = team.GetColor( v:Team()  )

        local trail = util.SpriteTrail(v, 0, Color(tcolor.r,tcolor.g,tcolor.b), false, 60, 20, 4, 1/(60+20)*0.5, "trails/smoke.vmt")


        v:SetVelocity(Vector(0, 0, 2048))


        timer.Simple(2.5, function()
            if !ents or !SERVER then return end
            local ent = ents.Create( "env_explosion" )
            if( IsValid( self.Owner ) ) then
                ent:SetPos( v:GetPos() )
                ent:SetOwner( self.Owner )
                ent:SetKeyValue( "iMagnitude", "150" )
                ent:Spawn()
                ent:Fire( "Explode", 0, 0 )
                ent:EmitSound( "weapons/big_explosion.mp3", 511, 100 )
            end
            timer.Simple(0.1, function()

                trail:Remove()

            end)
        end)

    end

end


function SWEP:SecondaryAttack()
    self.Weapon:SetNextSecondaryFire( CurTime() + 0.049 )
    self:Dupstep()
end

SWEP.lastActivate = 0

function SWEP:Dupstep()
    if self.lastActivate < (CurTime() -0.8) then
        self.lastActivate = CurTime()
        self.trace = self.Owner:GetEyeTrace()
        self.target = self.Owner
        self.tracepos = self.Owner:GetPos()
        self:makeThemDance()
    end

    local tr, vm, muzzle, effect
    vm = self.Owner:GetViewModel( )
    tr = { }
    tr.start = self.Owner:GetShootPos( )
    tr.filter = self.Owner
    tr.endpos = tr.start + self.Owner:GetAimVector( ) * 4096
    tr.mins = Vector( ) * -2
    tr.maxs = Vector( ) * 2
    tr = util.TraceHull( tr )

    if( tr.Entity:IsValid() and ( tr.Entity:IsPlayer() or tr.Entity:IsNPC())) then
        if SERVER then tr.Entity:Ignite(0.005)
        end
    end
    local bullet = {}
    bullet.Num 		= 1
    bullet.Src 		= self.Owner:GetShootPos()
    bullet.Dir 		= self.Owner:GetAimVector()
    bullet.Spread 	= Vector( aimcone, aimcone, 0 )
    bullet.Tracer	= 1
    bullet.TracerName =  "ToolTracer"
    bullet.Force	= 10
    bullet.Damage	= 8
    bullet.AmmoType = "Buckshot"
    self.Owner:FireBullets( bullet )
    self.Weapon:SetNextPrimaryFire( CurTime() + 0.05 )





    self.Weapon:EmitSound( "player/pl_fallpain3.wav" )


    if self.Shakes == self.ShakesNormal then
        if self.DoShakes == true then
            --RunConsoleCommand("shake")
        end
        self.Shakes = 0
    end
    self.Shakes = self.Shakes + 1
    self.TotalShakes = self.TotalShakes + 1

    --	MsgN(self.TotalShakes) --Debug Feature
    if self.TotalShakes == 270 or self.TotalShakes == 280 or self.TotalShakes == 290 then
        --RunConsoleCommand("shake")
        --RunConsoleCommand("shake")
        --RunConsoleCommand("shake")
    end
    if self.TotalShakes == 822 then
        --self.DoShakes = false
    end
    if self.TotalShakes == 1010 then
        --self.DoShakes = true
    end



    if self.TotalShakes >= 120 then
        if self.Shakes != self.ShakesNormal then

        if self.Shakes == self.ShakesNormal - 1 then
            tr.start = self.Owner:GetShootPos( )
            effect = EffectData( )
            effect:SetStart( tr.StartPos )
            effect:SetOrigin( tr.HitPos )
            effect:SetEntity( self )
            effect:SetAttachment( vm:LookupAttachment( "muzzle" ) )
            util.Effect( "LaserTracer", effect )
        end
        --if (false && self.Shakes <= 10 ) then
        --	local props = ents.GetAll()
        --	for _, prop in ipairs( props ) do
        --		if(prop:GetPhysicsObject():IsValid()) then
        --		prop:GetPhysicsObject():ApplyForceCenter( Vector( 0, 0, ((self.ShakesNormal*0.73) * prop:GetPhysicsObject():GetMass() ) ) )
        --	end
        --	end
        --	local Players = player.GetAll()
        --		for i = 1, table.Count(Players) do
        --		local ply = Players[i]
        --			ply:SetVelocity(Vector(0,0,50))
        --		end
        --end
        end





    end
end

SWEP.NextDamageTake = 0
function SWEP:makeThemDance()

    if(!IsValid(self)) then return end
    local trace = self.trace
    local target = self.target
    local tracepos = self.tracepos
    danceMove = table.Random(self.danceMoves)



    if(IsValid(self) && IsValid(target)) then
    local entstoattack = ents.FindInSphere(target:GetPos(),450)
    numberOfItems = 0
    if entstoattack != nil then
    for _,v in pairs(entstoattack) do
        if(IsValid(v)) then

            v:SetColor(Color(math.random(100,255), math.random(100,255), math.random(100,255)))
            if ( v:IsPlayer() && v != self.Owner ) then
            if SERVER && (!v.LastDance or v.LastDance < CurTime() - 5) then
            v.LastDance = CurTime()
            v:AnimPerformGesture(danceMove);
            end

            if( false && SERVER && self.NextDamageTake < CurTime() ) then
            self.NextDamageTake = CurTime() + 0.5
            local dmg = DamageInfo()
            dmg:SetDamage(5)
            dmg:SetAttacker(self.Owner)
            dmg:SetInflictor(self.Weapon)
            dmg:SetDamageType(DMG_SLASH )
            v:TakeDamageInfo(dmg)
            end
            end

            if IsValid(v:GetPhysicsObject())  && numberOfItems < 20 then
            numberOfItems = numberOfItems + 1
            v:GetPhysicsObject():ApplyForceCenter( Vector( 0, 0, ((self.ShakesNormal*0.73) * v:GetPhysicsObject():GetMass() ) ) )
            v:GetPhysicsObject():AddVelocity( Vector(math.random(-100,100), math.random(-100,100), math.random(-100,100)) )
            end

        end
    end
    end
    end
end

/********************************************************
	SWEP Construction Kit base code
		Created by Clavus
	Available for public use, thread at:
	   facepunch.com/threads/1032378
	   
	   
	DESCRIPTION:
		This script is meant for experienced scripters 
		that KNOW WHAT THEY ARE DOING. Don't come to me 
		with basic Lua questions.
		
		Just copy into your SWEP or SWEP base of choice
		and merge with your own code.
		
		The SWEP.VElements, SWEP.WElements and
		SWEP.ViewModelBoneMods tables are all optional
		and only have to be visible to the client.
********************************************************/

SWEP.NextCheckSound = 0

function SWEP:Think ()
    if self.Owner:KeyDown (IN_RELOAD) then

    else
        if not self.Owner:KeyDown (IN_ATTACK) then
            self.TotalShakes = 0
            self.OPMode = false
            self.shakes = 0
            self.Charged = false
        end
        self.ModeReady = true
    end

    self.LastFrame = self.LastFrame or CurTime()
    self.LastRandomEffects = self.LastRandomEffects or 0



    if self.NextCheckSound < CurTime() and self.Weapon:GetNWBool ("SoundPlaying") && !self.Owner:KeyDown(IN_ATTACK) then
    self.NextCheckSound = CurTime() + 1
    self.SoundPlaying = false
    self.Weapon:SetNWBool ("SoundPlaying", self.SoundPlaying)
    for k, v in pairs( self.Sounds ) do
        self.Weapon:StopSound( Sound( v) )
    end
    end

    self.LastFrame = CurTime()
    self.Weapon:SetNWBool ("on", self.SoundPlaying)

end


function SWEP:EndSound ()
    if self.SoundObject then
        self.SoundObject:Stop()
        self.Shakes = 0
        self.TotalShakes = 0
    end

    for k, v in pairs( self.Sound ) do
        self.Weapon:StopSound( Sound( v) )
    end
end

function SWEP:Initialize()

	// other initialize code goes here

	if CLIENT then

		self:SetHoldType( self.HoldType or "pistol" )
		// Create a new table for every weapon instance
		self.VElements = table.FullCopy( self.VElements )
		self.WElements = table.FullCopy( self.WElements )
		self.ViewModelBoneMods = table.FullCopy( self.ViewModelBoneMods )

		self:CreateModels(self.VElements) // create viewmodels
		self:CreateModels(self.WElements) // create worldmodels
		
		// init view model bone build function
		if IsValid(self.Owner) then
			local vm = self.Owner:GetViewModel()
			if IsValid(vm) then
				self:ResetBonePositions(vm)
				
				--[[// Init viewmodel visibility
				if (self.ShowViewModel == nil or self.ShowViewModel) then
					vm:SetColor(Color(255,255,255,255))
				else
					// we set the alpha to 1 instead of 0 because else ViewModelDrawn stops being called
					vm:SetColor(Color(255,255,255,1))
					// ^ stopped working in GMod 13 because you have to do Entity:SetRenderMode(1) for translucency to kick in
					// however for some reason the view model resets to render mode 0 every frame so we just apply a debug material to prevent it from drawing
					vm:SetMaterial("Debug/hsv")			
				end]]--
			end
		end
	end
end
function SWEP:Holster()
	
	if CLIENT and IsValid(self.Owner) then
		local vm = self.Owner:GetViewModel()
		if IsValid(vm) then
			self:ResetBonePositions(vm)
		end
	end
	
	return true
end

function SWEP:OnRemove()
	self:Holster()
end

if CLIENT then

	SWEP.vRenderOrder = nil
	function SWEP:ViewModelDrawn()
		
		local vm = self.Owner:GetViewModel()
		if !IsValid(vm) then return end
		
		if (!self.VElements) then return end
		
		self:UpdateBonePositions(vm)

		if (!self.vRenderOrder) then
			
			// we build a render order because sprites need to be drawn after models
			self.vRenderOrder = {}

			for k, v in pairs( self.VElements ) do
				if (v.type == "Model") then
					table.insert(self.vRenderOrder, 1, k)
				elseif (v.type == "Sprite" or v.type == "Quad") then
					table.insert(self.vRenderOrder, k)
				end
			end
			
		end

		for k, name in ipairs( self.vRenderOrder ) do
		
			local v = self.VElements[name]
			if (!v) then self.vRenderOrder = nil break end
			if (v.hide) then continue end
			
			local model = v.modelEnt
			local sprite = v.spriteMaterial
			
			if (!v.bone) then continue end
			
			local pos, ang = self:GetBoneOrientation( self.VElements, v, vm )
			
			if (!pos) then continue end
			
			if (v.type == "Model" and IsValid(model)) then

				model:SetPos(pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z )
				ang:RotateAroundAxis(ang:Up(), v.angle.y)
				ang:RotateAroundAxis(ang:Right(), v.angle.p)
				ang:RotateAroundAxis(ang:Forward(), v.angle.r)

				model:SetAngles(ang)
				//model:SetModelScale(v.size)
				local matrix = Matrix()
				matrix:Scale(v.size)
				model:EnableMatrix( "RenderMultiply", matrix )
				
				if (v.material == "") then
					model:SetMaterial("")
				elseif (model:GetMaterial() != v.material) then
					model:SetMaterial( v.material )
				end
				
				if (v.skin and v.skin != model:GetSkin()) then
					model:SetSkin(v.skin)
				end
				
				if (v.bodygroup) then
					for k, v in pairs( v.bodygroup ) do
						if (model:GetBodygroup(k) != v) then
							model:SetBodygroup(k, v)
						end
					end
				end
				
				if (v.surpresslightning) then
					render.SuppressEngineLighting(true)
				end
				
				render.SetColorModulation(v.color.r/255, v.color.g/255, v.color.b/255)
				render.SetBlend(v.color.a/255)
				model:DrawModel()
				render.SetBlend(1)
				render.SetColorModulation(1, 1, 1)
				
				if (v.surpresslightning) then
					render.SuppressEngineLighting(false)
				end
				
			elseif (v.type == "Sprite" and sprite) then
				
				local drawpos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
				render.SetMaterial(sprite)
				render.DrawSprite(drawpos, v.size.x, v.size.y, v.color)
				
			elseif (v.type == "Quad" and v.draw_func) then
				
				local drawpos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
				ang:RotateAroundAxis(ang:Up(), v.angle.y)
				ang:RotateAroundAxis(ang:Right(), v.angle.p)
				ang:RotateAroundAxis(ang:Forward(), v.angle.r)
				
				cam.Start3D2D(drawpos, ang, v.size)
					v.draw_func( self )
				cam.End3D2D()

			end
			
		end
		
	end

	SWEP.wRenderOrder = nil
	function SWEP:DrawWorldModel()
		
		if (self.ShowWorldModel == nil or self.ShowWorldModel) then
			self:DrawModel()
		end
		
		if (!self.WElements) then return end
		
		if (!self.wRenderOrder) then

			self.wRenderOrder = {}

			for k, v in pairs( self.WElements ) do
				if (v.type == "Model") then
					table.insert(self.wRenderOrder, 1, k)
				elseif (v.type == "Sprite" or v.type == "Quad") then
					table.insert(self.wRenderOrder, k)
				end
			end

		end
		
		if (IsValid(self.Owner)) then
			bone_ent = self.Owner
		else
			// when the weapon is dropped
			bone_ent = self
		end
		
		for k, name in pairs( self.wRenderOrder ) do
		
			local v = self.WElements[name]
			if (!v) then self.wRenderOrder = nil break end
			if (v.hide) then continue end
			
			local pos, ang
			
			if (v.bone) then
				pos, ang = self:GetBoneOrientation( self.WElements, v, bone_ent )
			else
				pos, ang = self:GetBoneOrientation( self.WElements, v, bone_ent, "ValveBiped.Bip01_R_Hand" )
			end
			
			if (!pos) then continue end
			
			local model = v.modelEnt
			local sprite = v.spriteMaterial
			
			if (v.type == "Model" and IsValid(model)) then

				model:SetPos(pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z )
				ang:RotateAroundAxis(ang:Up(), v.angle.y)
				ang:RotateAroundAxis(ang:Right(), v.angle.p)
				ang:RotateAroundAxis(ang:Forward(), v.angle.r)

				model:SetAngles(ang)
				//model:SetModelScale(v.size)
				local matrix = Matrix()
				matrix:Scale(v.size)
				model:EnableMatrix( "RenderMultiply", matrix )
				
				if (v.material == "") then
					model:SetMaterial("")
				elseif (model:GetMaterial() != v.material) then
					model:SetMaterial( v.material )
				end
				
				if (v.skin and v.skin != model:GetSkin()) then
					model:SetSkin(v.skin)
				end
				
				if (v.bodygroup) then
					for k, v in pairs( v.bodygroup ) do
						if (model:GetBodygroup(k) != v) then
							model:SetBodygroup(k, v)
						end
					end
				end
				
				if (v.surpresslightning) then
					render.SuppressEngineLighting(true)
				end
				
				render.SetColorModulation(v.color.r/255, v.color.g/255, v.color.b/255)
				render.SetBlend(v.color.a/255)
				model:DrawModel()
				render.SetBlend(1)
				render.SetColorModulation(1, 1, 1)
				
				if (v.surpresslightning) then
					render.SuppressEngineLighting(false)
				end
				
			elseif (v.type == "Sprite" and sprite) then
				
				local drawpos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
				render.SetMaterial(sprite)
				render.DrawSprite(drawpos, v.size.x, v.size.y, v.color)
				
			elseif (v.type == "Quad" and v.draw_func) then
				
				local drawpos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
				ang:RotateAroundAxis(ang:Up(), v.angle.y)
				ang:RotateAroundAxis(ang:Right(), v.angle.p)
				ang:RotateAroundAxis(ang:Forward(), v.angle.r)
				
				cam.Start3D2D(drawpos, ang, v.size)
					v.draw_func( self )
				cam.End3D2D()

			end
			
		end
		
	end

	function SWEP:GetBoneOrientation( basetab, tab, ent, bone_override )
		
		local bone, pos, ang
		if (tab.rel and tab.rel != "") then
			
			local v = basetab[tab.rel]
			
			if (!v) then return end
			
			// Technically, if there exists an element with the same name as a bone
			// you can get in an infinite loop. Let's just hope nobody's that stupid.
			pos, ang = self:GetBoneOrientation( basetab, v, ent )
			
			if (!pos) then return end
			
			pos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
			ang:RotateAroundAxis(ang:Up(), v.angle.y)
			ang:RotateAroundAxis(ang:Right(), v.angle.p)
			ang:RotateAroundAxis(ang:Forward(), v.angle.r)
				
		else
		
			bone = ent:LookupBone(bone_override or tab.bone)

			if (!bone) then return end
			
			pos, ang = Vector(0,0,0), Angle(0,0,0)
			local m = ent:GetBoneMatrix(bone)
			if (m) then
				pos, ang = m:GetTranslation(), m:GetAngles()
			end
			
			if (IsValid(self.Owner) and self.Owner:IsPlayer() and 
				ent == self.Owner:GetViewModel() and self.ViewModelFlip) then
				ang.r = -ang.r // Fixes mirrored models
			end
		
		end
		
		return pos, ang
	end

	function SWEP:CreateModels( tab )

		if (!tab) then return end

		// Create the clientside models here because Garry says we can't do it in the render hook
		for k, v in pairs( tab ) do
			if (v.type == "Model" and v.model and v.model != "" and (!IsValid(v.modelEnt) or v.createdModel != v.model) and 
					string.find(v.model, ".mdl") and file.Exists (v.model, "GAME") ) then
				
				v.modelEnt = ClientsideModel(v.model, RENDER_GROUP_VIEW_MODEL_OPAQUE)
				if (IsValid(v.modelEnt)) then
					v.modelEnt:SetPos(self:GetPos())
					v.modelEnt:SetAngles(self:GetAngles())
					v.modelEnt:SetParent(self)
					v.modelEnt:SetNoDraw(true)
					v.createdModel = v.model
				else
					v.modelEnt = nil
				end
				
			elseif (v.type == "Sprite" and v.sprite and v.sprite != "" and (!v.spriteMaterial or v.createdSprite != v.sprite) 
				and file.Exists ("materials/"..v.sprite..".vmt", "GAME")) then
				
				local name = v.sprite.."-"
				local params = { ["$basetexture"] = v.sprite }
				// make sure we create a unique name based on the selected options
				local tocheck = { "nocull", "additive", "vertexalpha", "vertexcolor", "ignorez" }
				for i, j in pairs( tocheck ) do
					if (v[j]) then
						params["$"..j] = 1
						name = name.."1"
					else
						name = name.."0"
					end
				end

				v.createdSprite = v.sprite
				v.spriteMaterial = CreateMaterial(name,"UnlitGeneric",params)
				
			end
		end
		
	end
	
	local allbones
	local hasGarryFixedBoneScalingYet = false

	function SWEP:UpdateBonePositions(vm)
		
		if self.ViewModelBoneMods then
			
			if (!vm:GetBoneCount()) then return end
			
			// !! WORKAROUND !! //
			// We need to check all model names :/
			local loopthrough = self.ViewModelBoneMods
			if (!hasGarryFixedBoneScalingYet) then
				allbones = {}
				for i=0, vm:GetBoneCount() do
					local bonename = vm:GetBoneName(i)
					if (self.ViewModelBoneMods[bonename]) then 
						allbones[bonename] = self.ViewModelBoneMods[bonename]
					else
						allbones[bonename] = { 
							scale = Vector(1,1,1),
							pos = Vector(0,0,0),
							angle = Angle(0,0,0)
						}
					end
				end
				
				loopthrough = allbones
			end
			// !! ----------- !! //
			
			for k, v in pairs( loopthrough ) do
				local bone = vm:LookupBone(k)
				if (!bone) then continue end
				
				// !! WORKAROUND !! //
				local s = Vector(v.scale.x,v.scale.y,v.scale.z)
				local p = Vector(v.pos.x,v.pos.y,v.pos.z)
				local ms = Vector(1,1,1)
				if (!hasGarryFixedBoneScalingYet) then
					local cur = vm:GetBoneParent(bone)
					while(cur >= 0) do
						local pscale = loopthrough[vm:GetBoneName(cur)].scale
						ms = ms * pscale
						cur = vm:GetBoneParent(cur)
					end
				end
				
				s = s * ms
				// !! ----------- !! //
				
				if vm:GetManipulateBoneScale(bone) != s then
					vm:ManipulateBoneScale( bone, s )
				end
				if vm:GetManipulateBoneAngles(bone) != v.angle then
					vm:ManipulateBoneAngles( bone, v.angle )
				end
				if vm:GetManipulateBonePosition(bone) != p then
					vm:ManipulateBonePosition( bone, p )
				end
			end
		else
			self:ResetBonePositions(vm)
		end
		   
	end
	 
	function SWEP:ResetBonePositions(vm)
		
		if (!vm:GetBoneCount()) then return end
		for i=0, vm:GetBoneCount() do
			vm:ManipulateBoneScale( i, Vector(1, 1, 1) )
			vm:ManipulateBoneAngles( i, Angle(0, 0, 0) )
			vm:ManipulateBonePosition( i, Vector(0, 0, 0) )
		end
		
	end

	/**************************
		Global utility code
	**************************/

	// Fully copies the table, meaning all tables inside this table are copied too and so on (normal table.Copy copies only their reference).
	// Does not copy entities of course, only copies their reference.
	// WARNING: do not use on tables that contain themselves somewhere down the line or you'll get an infinite loop
	function table.FullCopy( tab )

		if (!tab) then return nil end
		
		local res = {}
		for k, v in pairs( tab ) do
			if (type(v) == "table") then
				res[k] = table.FullCopy(v) // recursion ho!
			elseif (type(v) == "Vector") then
				res[k] = Vector(v.x, v.y, v.z)
			elseif (type(v) == "Angle") then
				res[k] = Angle(v.p, v.y, v.r)
			else
				res[k] = v
			end
		end
		
		return res
		
	end
	
end

