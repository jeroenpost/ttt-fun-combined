SWEP.Base = "gb_camo_base"
SWEP.ViewModelFOV	= 54
SWEP.HoldType = "ar2"
SWEP.PrintName = "Irifle"
SWEP.ViewModel		= "models/weapons/c_irifle.mdl"
SWEP.WorldModel		= "models/weapons/w_irifle.mdl"

SWEP.Primary.Delay		    = 1
SWEP.Slot = 3
SWEP.Kind = WEAPON_HEAVY

SWEP.Primary.Damage = 22
SWEP.Primary.Delay = 0.2
SWEP.HeadshotMultiplier = 1
SWEP.Primary.Cone = 0.03

function SWEP:PrimaryAttack()
   self:ShootTracer( "AR2Tracer")
   self:NormalPrimaryAttack()

end

function SWEP:SecondaryAttack()
    self:Fly("weapons/mortar/mortar_explode1.wav");
end
