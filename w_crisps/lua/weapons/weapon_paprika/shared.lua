if SERVER then

   --resource.AddFile("materials/vgui/ttt_fun_killicons/chips.png")

--resource.AddFile("materials/models/weapons/gbcrowbar/chips.vmt")
--resource.AddFile("materials/models/grisps/chips.vmt")
--resource.AddFile("materials/vgui/entities/weapon_paprika.vmt")
--resource.AddFile("models/crisps/gaprika.mdl")
--resource.AddFile("models/weapons/v_gbprika.mdl")
--resource.AddFile("sound/crisps/eat.mp3")





end
SWEP.PrintName          = "Paprika Crisps"
SWEP.Slot               = 1
SWEP.Base				= "gb_base"
SWEP.Author             = "GreenBlack"
SWEP.Purpose            = "For the laughs"
SWEP.Instructions       = "Left click to hit other players \nRight click to throw and damage other players \nReload to eat for health"
SWEP.Category			= "Other"
 
SWEP.Spawnable = true
SWEP.AdminSpawnable = true

SWEP.Kind = WEAPON_EQUIP
SWEP.Icon = "vgui/ttt_fun_killicons/chips.png"

SWEP.CanBuy = { ROLE_DETECTIVE, ROLE_TRAITOR }
SWEP.LimitedStock = true

  SWEP.Slot				= 6
   SWEP.SlotPos = 8

   SWEP.EquipMenuData = {
      name = "Crisps 25 health",
      type = "item_weapon",
      desc = "Left click to hit other players \nRight click to throw and damage other players \nReload to eat for health"
   };

SWEP.WElements = {
	["chiips"] = { type = "Model", model = "models/crisps/gaprika.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(1.363, 2.273, -0.456), angle = Angle(-13.296, 0, 0), size = Vector(0.5, 0.5, 0.5), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
}

SWEP.ViewModel = Model("models/weapons/v_gbprika.mdl")
SWEP.WorldModel = Model("models/crisps/gaprika.mdl")

SWEP.Primary.Clipsize = -1
SWEP.Primary.DefaultClip = -1
SWEP.Primary.Automatic = false
SWEP.Primary.Ammo = "none"
SWEP.ViewModelFlip = false
SWEP.Secondary.Clipsize = -1
SWEP.Secondary.DefaultClip = -1
SWEP.Secondary.Automatic = false
SWEP.Secondary.Ammo = "none"
SWEP.reloaded = false

function SWEP:Think()
end

function SWEP:Initialize()
	util.PrecacheSound("weapons/iceaxe/iceaxe_swing1.wav")
	util.PrecacheSound("physics/body/body_medium_impact_hard1.wav")
	util.PrecacheSound("physics/body/body_medium_impact_hard2.wav")
	util.PrecacheSound("physics/body/body_medium_impact_hard3.wav")
	util.PrecacheSound("physics/body/body_medium_impact_hard4.wav")
	util.PrecacheSound("physics/body/body_medium_impact_hard5.wav")
	util.PrecacheSound("physics/body/body_medium_impact_hard6.wav")
	util.PrecacheSound("weapons/knife/knife_slash1.wav")
	util.PrecacheSound("crisps/eat.mp3")
end

function SWEP:Reload()
if ( self.Owner:IsValid() && SERVER && self.reloaded == false ) then
self.reloaded = true
self.Owner:SetHealth(self.Owner:Health()+25)
self.Owner:EmitSound("crisps/eat.mp3")
self.Owner:StripWeapon("weapon_paprika")
end

end

function SWEP:Deploy()
end

function SWEP:PrimaryAttack()
	
	self.Weapon:SendWeaponAnim( ACT_VM_HITCENTER )
	
	local tr = {}
	tr.start = self.Owner:GetShootPos()
	tr.endpos = self.Owner:GetShootPos() + ( self.Owner:GetAimVector() * 50 )
	tr.filter = self.Owner
	tr.mask = MASK_SHOT
	local trace = util.TraceLine( tr )

	self.Weapon:SetNextPrimaryFire(CurTime() + 0.5)

	if ( trace.Hit ) then

		if trace.Entity:IsPlayer() or string.find(trace.Entity:GetClass(),"npc") or string.find(trace.Entity:GetClass(),"prop_ragdoll") then
			bullet = {}
			bullet.Num    = 1
			bullet.Src    = self.Owner:GetShootPos()
			bullet.Dir    = self.Owner:GetAimVector()
			bullet.Spread = Vector(0, 0, 0)
			bullet.Tracer = 0
			bullet.Force  = 1
			bullet.Damage = 20
			self.Owner:FireBullets(bullet)
			self.Weapon:EmitSound( "physics/body/body_medium_impact_hard" .. math.random(1, 6) .. ".wav" )
		else
			bullet = {}
			bullet.Num    = 1
			bullet.Src    = self.Owner:GetShootPos()
			bullet.Dir    = self.Owner:GetAimVector()
			bullet.Spread = Vector(0, 0, 0)
			bullet.Tracer = 0
			bullet.Force  = 1
			bullet.Damage = 20
			self.Owner:FireBullets(bullet)
			self.Weapon:EmitSound("weapons/knife/knife_slash1.wav")
		end
		
	end
end

function SWEP:SecondaryAttack()
if (!SERVER) then return end
self.Weapon:SendWeaponAnim( ACT_VM_MISSCENTER )
self.Weapon:EmitSound("weapons/iceaxe/iceaxe_swing1.wav")

	local ent = ents.Create("sent_paprika")
	local pos = self.Owner:GetShootPos()
	ent:SetPos(pos)
	ent:SetAngles(self.Owner:EyeAngles())
	ent:SetPhysicsAttacker( self.Owner )
	--ent.owner = self
	ent:Spawn()
	ent:Activate()
	ent:SetOwner(self.Owner)
	ent:SetPhysicsAttacker(self.Owner)

self.Weapon:SetNextSecondaryFire(CurTime() + 0.5)
self.Weapon:SetNextPrimaryFire(CurTime() + 1)

	local tr = self.Owner:GetEyeTrace()
	
	local phys = ent:GetPhysicsObject()
	if (phys:IsValid()) then
		phys:Wake()
		phys:SetMass(1)
		phys:EnableGravity( true )
		phys:ApplyForceCenter( self:GetForward() *2000 )
		phys:SetBuoyancyRatio( 0 )
	end
 
	local shot_length = tr.HitPos:Length()
	ent:SetVelocity(self.Owner:GetAimVector():GetNormalized() *  math.pow(shot_length, 3))
	
	self.Owner:StripWeapon("weapon_paprika")
	
end
