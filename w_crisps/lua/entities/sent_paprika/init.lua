AddCSLuaFile( "cl_init.lua" )
AddCSLuaFile( "shared.lua" )

include('shared.lua')


function ENT:Initialize()
	
	self:SetModel( "models/crisps/gaprika.mdl" )
	self:PhysicsInit( SOLID_VPHYSICS )
	self.Entity:SetMoveType( MOVETYPE_VPHYSICS )
	self.Entity:SetSolid( SOLID_VPHYSICS )
	local phys = self.Entity:GetPhysicsObject()
	self.NextThink = CurTime() +  1

	if ( phys:IsValid() ) then
		phys:Wake()
		phys:SetMass( 10 )
	end

	self:GetPhysicsObject():SetMass( 2 )
end

function ENT:Think()
end

function ENT:Disable()
	self.PhysicsCollide = function() end
end

function ENT:PhysicsCollide( data, phys )
	
	local Ent = data.HitEntity
	if !(IsValid( Ent ) || Ent:IsWorld()) then return end

	if Ent:IsWorld() then
			self:Disable()

	elseif Ent.Health then
		if not(Ent:IsPlayer() || Ent:IsNPC() || Ent:GetClass() == "prop_ragdoll") then 
			util.Decal( "ManhackCut", data.HitPos + data.HitNormal, data.HitPos - data.HitNormal )
		end
        Ent.hasProtectionSuit = true
		Ent:TakeDamage( self.damage or 45, self:GetOwner() )

        if self.poison then
            timer.Create(Ent:EntIndex().."takepoisondamage",self.poisondelay or 0.5,self.poisontimes or 10,function()
                local owner =  self:GetOwner()
                if IsValid(Ent) and IsValid( owner ) then
                  Ent:TakeDamage( self.poisondamage or 5, owner )
                  owner:SetHealth(self:GetOwner():Health()+ (5))
                    if owner:Health() > 150 then
                        owner:SetHealth(150)
                    end
                      if (Ent:IsPlayer() || Ent:IsNPC() || Ent:GetClass() == "prop_ragdoll") then
                      local effectdata = EffectData()
                      effectdata:SetStart( data.HitPos )
                      effectdata:SetOrigin( data.HitPos )
                      effectdata:SetScale( 1 )
                      util.Effect( "BloodImpact", effectdata )
                      end
                end
            end)
        end

		if (Ent:IsPlayer() || Ent:IsNPC() || Ent:GetClass() == "prop_ragdoll") then 
			local effectdata = EffectData()
			effectdata:SetStart( data.HitPos )
			effectdata:SetOrigin( data.HitPos )
			effectdata:SetScale( 1 )
			util.Effect( "BloodImpact", effectdata )
		end
	end

	self.Entity:SetOwner( nil )
	
end

function ENT:Use( activator, caller )
	self.Entity:Remove()
	if ( activator:IsPlayer() ) then
        if activator:Health() < 150 then
            if self.lower then
                activator:SetHealth(activator:Health()+ (self.heal or 10))
            else
		activator:SetHealth(activator:Health()+ (self.heal or 25))
                end
        end
        gb_check_max_health(activator)
		activator:EmitSound("crisps/eat.mp3")
	end
end
