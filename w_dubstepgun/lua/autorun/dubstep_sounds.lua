sound.Add(
{
    name = "dubstep.s1",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 80,
    sound = "sr4normal.mp3"
})
sound.Add(
{
    name = "dubstep.s2",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 80,
    sound = "basscannon.mp3"
})
sound.Add(
{
    name = "dubstep.s3",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 80,
    sound = "sr4popstar.mp3"
})
