AddCSLuaFile( )
SWEP.PrintName         = "Dubstep Gun"
SWEP.Spawnable        = true
SWEP.AdminSpawnable     = false
SWEP.DrawCrosshair    = true
SWEP.AdminOnly		= false
SWEP.Category         = "MrDuffdude's Stuff"
SWEP.Author        = "MrDuffdude"
SWEP.Purpose        = "Shoot dubstep"
SWEP.Instructions    = "Left click for dubstep, Right click for zoom!"
SWEP.Slot        = 5
SWEP.SlotPos        = 5
SWEP.ViewModel        = Model( "models/thunder_gun.mdl" )
SWEP.WorldModel        = Model( "models/thunder_gun.mdl" )
SWEP.Primary.Automatic        = true
SWEP.Primary.ClipSize        = -1
SWEP.Primary.DefaultClip    = -1
SWEP.Primary.Ammo    = "dubstep"
SWEP.Secondary.Ammo    = "none"
SWEP.ViewModelFOV    = 70
SWEP.HoldType        = "pistol"
SWEP.playsound = false
SWEP.Damage = 1
SWEP.LastSoundRelease = 0
SWEP.RestartDelay = 1
SWEP.Volume = 0
SWEP.Influence = 0
SWEP.OPMode = false
SWEP.ModeReady = true
SWEP.ShakesNormal = 0
SWEP.Shakes = SWEP.ShakesNormal
SWEP.TotalShakes = 0
SWEP.Charged = false
SWEP.DoShakes = false
SWEP.Base = "weapon_tttbase"
SWEP.Kind = WEAPON_HEAVY
SWEP.SoundObject = false

SWEP.EquipMenuData = {
    type="Weapon",
    name="DubstepGun",
    desc="WUB WUB WUB\nReload for other song (random)"
}

SWEP.Icon = "vgui/ttt_fun_killicons/dubstepgun.png"
SWEP.CanBuy = {ROLE_DETECTIVE}

SWEP.Sound = { "dubstep.s1","dubstep.s2","dubstep.s3" }

SWEP.trace = 0
SWEP.target = 0
SWEP.tracepos = 0

SWEP.HoldType = "pistol"
SWEP.ViewModelFOV = 70
SWEP.ViewModelFlip = false
SWEP.ViewModel = "models/weapons/v_pistol.mdl"
SWEP.WorldModel = "models/weapons/w_pistol.mdl"
SWEP.ShowViewModel = true
SWEP.ShowWorldModel = true
SWEP.ViewModelBoneMods = {
    --	["ValveBiped.Bip01"] = { scale = Vector(1,1,1), pos = Vector(4.126, -5.079, 5.396), angle = Angle(0, 0, 0) }
}


SWEP.VElements = {
    ["element_name"] = { type = "Model", model = "models/dubstepgun.mdl", bone = "ValveBiped.Bip01", rel = "", pos = Vector(-5.909, -23.182, 29.545), angle = Angle(-1.024, 0, -103.295), size = Vector(1.343, 1.343, 1.343), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
}
SWEP.WElements = {
    ["element_name"] = { type = "Model", model = "models/dubstepgun.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(15.909, -0.456, -3.182), angle = Angle(-7.159, -88.977, -180), size = Vector(1.911, 1.911, 1.911), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
}

SWEP.Irons = {
    Normal = {
        Pos = Vector( -12.5, -4, 4.5 ),
        Ang = Vector( 0 ),
        BlendTime = 0,
    }
}

function SWEP:Deploy()

    if IsValid(self.Owner) and self.Owner.dubstepcolor and CLIENT then
        self.VElements2 = {
            ["element_name"] = { type = "Model", model = "models/dubstepgun.mdl", bone = "ValveBiped.Bip01", rel = "", pos = Vector(-5.909, -23.182, 29.545), angle = Angle(-1.024, 0, -103.295), size = Vector(1.343, 1.343, 1.343), color = self.Owner.dubstepcolor, surpresslightning = false, material = "", skin = 0, bodygroup = {} }
        }
        self.WElements2 = {
            ["element_name"] = { type = "Model", model = "models/dubstepgun.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(15.909, -0.456, -3.182), angle = Angle(-7.159, -88.977, -180), size = Vector(1.911, 1.911, 1.911), color = self.Owner.dubstepcolor, surpresslightning = false, material = "", skin = 0, bodygroup = {} }
        }

        self.VElements = table.FullCopy( self.VElements2 )
        self.WElements = table.FullCopy( self.WElements2 )

    end



    --util.ScreenShake( Vector( 0,0,0 ), 5, 5, 10, 5000 )
end



function SWEP:Initialize( )
    self:SetHoldType( self.HoldType or "pistol" )
    self.playsound = Sound( table.Random(self.Sound))
    if CLIENT then


        // Create a new table for every weapon instance
        self.VElements = table.FullCopy( self.VElements )
        self.WElements = table.FullCopy( self.WElements )
        self.ViewModelBoneMods = table.FullCopy( self.ViewModelBoneMods )

        self:CreateModels(self.VElements) // create viewmodels
        self:CreateModels(self.WElements) // create worldmodels

        // init view model bone build function
        if IsValid(self.Owner) then
            local vm = self.Owner:GetViewModel()
            if IsValid(vm) then
                self:ResetBonePositions(vm)

                // Init viewmodel visibility
                if (self.ShowViewModel == nil or self.ShowViewModel) then
                vm:SetColor(Color(255,255,255,255))
            else
                // we set the alpha to 1 instead of 0 because else ViewModelDrawn stops being called
                vm:SetColor(Color(255,255,255,1))
                // ^ stopped working in GMod 13 because you have to do Entity:SetRenderMode(1) for translucency to kick in
                // however for some reason the view model resets to render mode 0 every frame so we just apply a debug material to prevent it from drawing
                vm:SetMaterial("Debug/hsv")
                end
            end
        end

    end
end

function ignite()

end

SWEP.danceMoves = {ACT_GMOD_TAUNT_ROBOT,ACT_GMOD_TAUNT_DANCE,ACT_GMOD_TAUNT_MUSCLE}
SWEP.lastActivate = 0

function SWEP:PrimaryAttack()

    if self.lastActivate < (CurTime() -0.8) then
        self.lastActivate = CurTime()
        self.trace = self.Owner:GetEyeTrace()
        self.target = self.Owner
        self.tracepos = self.Owner:GetPos()
        self:makeThemDance()
    end

    if ! self.Weapon:GetNWBool ("SoundPlaying") then
    if !self.playsound then self.playsound = Sound( table.Random(self.Sound)) end
    self.SoundPlaying = true
    self.Weapon:SetNWBool ("SoundPlaying", true)
    self.Weapon:EmitSound( self.playsound, 450 )
    end

    if self.Shakes == self.ShakesNormal then
        if self.DoShakes == true then
            --RunConsoleCommand("shake")
        end
        self.Shakes = 0
    end
    self.Shakes = self.Shakes + 1
    self.TotalShakes = self.TotalShakes + 1

    --	MsgN(self.TotalShakes) --Debug Feature
    if self.TotalShakes == 270 or self.TotalShakes == 280 or self.TotalShakes == 290 then
        --RunConsoleCommand("shake")
        --RunConsoleCommand("shake")
        --RunConsoleCommand("shake")
    end
    if self.TotalShakes == 822 then
        --self.DoShakes = false
    end
    if self.TotalShakes == 1010 then
        --self.DoShakes = true
    end

    local tr, vm, muzzle, effect
    vm = self.Owner:GetViewModel( )
    tr = { }
    tr.start = self.Owner:GetShootPos( )
    tr.filter = self.Owner
    tr.endpos = tr.start + self.Owner:GetAimVector( ) * 4096
    tr.mins = Vector( ) * -2
    tr.maxs = Vector( ) * 2
    tr = util.TraceHull( tr )

    if self.TotalShakes >= 120 then
        if self.Shakes != self.ShakesNormal then
        effect = EffectData( )
        effect:SetStart( tr.StartPos )
        effect:SetOrigin( tr.HitPos )
        effect:SetEntity( self )
        effect:SetAttachment( vm:LookupAttachment( "muzzle" ) )
        util.Effect( "ToolTracer", effect )
        if self.Shakes == self.ShakesNormal - 1 then
            tr.start = self.Owner:GetShootPos( )
            effect = EffectData( )
            effect:SetStart( tr.StartPos )
            effect:SetOrigin( tr.HitPos )
            effect:SetEntity( self )
            effect:SetAttachment( vm:LookupAttachment( "muzzle" ) )
            util.Effect( "LaserTracer", effect )
        end
            --if (false && self.Shakes <= 10 ) then
            --	local props = ents.GetAll()
            --	for _, prop in ipairs( props ) do
            --		if(prop:GetPhysicsObject():IsValid()) then
            --		prop:GetPhysicsObject():ApplyForceCenter( Vector( 0, 0, ((self.ShakesNormal*0.73) * prop:GetPhysicsObject():GetMass() ) ) )
            --	end
            --	end
            --	local Players = player.GetAll()
            --		for i = 1, table.Count(Players) do
            --		local ply = Players[i]
            --			ply:SetVelocity(Vector(0,0,50))
            --		end
            --end
        end


        if( tr.Entity:IsValid() and ( tr.Entity:IsPlayer() or tr.Entity:IsNPC())) then
            if SERVER then tr.Entity:Ignite(0.005)		end
            local bullet = {}
            bullet.Num 		= 1
            bullet.Src 		= self.Owner:GetShootPos()
            bullet.Dir 		= self.Owner:GetAimVector()
            bullet.Spread 	= Vector( aimcone, aimcone, 0 )
            bullet.Tracer	= 0
            bullet.Force	= 10
            bullet.Damage	= 3
            bullet.AmmoType = "Buckshot"
            self.Owner:FireBullets( bullet )



        end


    end
end

SWEP.NextDamageTake = 0

function SWEP:makeThemDance()

    if(!IsValid(self)) then return end
    local trace = self.trace
    local target = self.target
    local tracepos = self.tracepos
    danceMove = table.Random(self.danceMoves)



    if(IsValid(self) && IsValid(target)) then
    local entstoattack = ents.FindInSphere(target:GetPos(),450)
    numberOfItems = 0
    if entstoattack != nil then
    for _,v in pairs(entstoattack) do
        if(IsValid(v)) then

            v:SetColor(Color(math.random(100,255), math.random(100,255), math.random(100,255)))
            if ( v:IsPlayer() && v != self.Owner ) then
            if SERVER && (!v.LastDance or v.LastDance < CurTime() - 5) then
            v.LastDance = CurTime()
            v:AnimPerformGesture(danceMove);
            end

            if( false && SERVER && self.NextDamageTake < CurTime() ) then
            self.NextDamageTake = CurTime() + 0.5
            local dmg = DamageInfo()
            dmg:SetDamage(5)
            dmg:SetAttacker(self.Owner)
            dmg:SetInflictor(self.Weapon)
            dmg:SetDamageType(DMG_SLASH )
            v:TakeDamageInfo(dmg)
            end
            end

            if IsValid(v:GetPhysicsObject())  && numberOfItems < 20 then
            numberOfItems = numberOfItems + 1
            v:GetPhysicsObject():ApplyForceCenter( Vector( 0, 0, ((self.ShakesNormal*0.73) * v:GetPhysicsObject():GetMass() ) ) )
            v:GetPhysicsObject():AddVelocity( Vector(math.random(-100,100), math.random(-100,100), math.random(-100,100)) )
            end

        end
    end
    end
    end
end


function SWEP:SecondaryAttack()
    if(Zoom == 0) then
        if(SERVER) then
            self.Owner:SetFOV( 50, 1 )
        end
        Zoom = 1
    else if(Zoom == 1) then
        if(SERVER) then
            self.Owner:SetFOV( 30, 1 )
        end
        Zoom = 2
    else
        if(SERVER) then
            self.Owner:SetFOV( 0, 1 )
        end
        Zoom = 0
    end
    end
end

function SWEP:Reload()
    self.Weapon:SetNWBool ("SoundPlaying", false)
    for k, v in pairs( self.Sound ) do
        self.Weapon:StopSound( Sound( v) )
    end

    self.playsound = Sound( table.Random(self.Sound))
end



SWEP.NextCheckSound = 0

function SWEP:Think ()
    if self.Owner:KeyDown (IN_RELOAD) then

    else
        if not self.Owner:KeyDown (IN_ATTACK) then
            self.TotalShakes = 0
            self.OPMode = false
            self.shakes = 0
            self.Charged = false
        end
        self.ModeReady = true
    end

    self.LastFrame = self.LastFrame or CurTime()
    self.LastRandomEffects = self.LastRandomEffects or 0



    if self.NextCheckSound < CurTime() and self.Weapon:GetNWBool ("SoundPlaying") && !self.Owner:KeyDown(IN_ATTACK) then
    self.NextCheckSound = CurTime() + 1
    self.SoundPlaying = false
    self.Weapon:SetNWBool ("SoundPlaying", self.SoundPlaying)
    for k, v in pairs( self.Sound ) do
        self.Weapon:StopSound( Sound( v) )
    end
    end

    self.LastFrame = CurTime()
    self.Weapon:SetNWBool ("on", self.SoundPlaying)



end

function SWEP:Holster()
    self:EndSound()
    if SERVER && IsValid(self.Owner) then
    self.Owner:SetFOV( 0, 0 )
    end
    if CLIENT and IsValid(self.Owner) then
        local vm = self.Owner:GetViewModel()
        if IsValid(vm) then
            self:ResetBonePositions(vm)
        end
    end

    return true
end

function SWEP:OnDrop()
    self:Holster()
end
function SWEP:OnRemove()
    self:Holster()
end


function SWEP:OwnerChanged() self:EndSound() end

function SWEP:EndSound ()
    if self.SoundObject then
        self.SoundObject:Stop()
        self.Shakes = 0
        self.TotalShakes = 0
    end

    for k, v in pairs( self.Sound ) do
        self.Weapon:StopSound( Sound( v) )
    end
end

if CLIENT then

    SWEP.vRenderOrder = nil
    function SWEP:ViewModelDrawn()
        if !IsValid(self.Owner) then return end
        local vm = self.Owner:GetViewModel()
        if !IsValid(vm) then return end

        if (!self.VElements) then return end

        self:UpdateBonePositions(vm)

        if (!self.vRenderOrder) then

        // we build a render order because sprites need to be drawn after models
        self.vRenderOrder = {}

        for k, v in pairs( self.VElements ) do
            if (v.type == "Model") then
                table.insert(self.vRenderOrder, 1, k)
            elseif (v.type == "Sprite" or v.type == "Quad") then
                table.insert(self.vRenderOrder, k)
            end
        end

        end

        for k, name in ipairs( self.vRenderOrder ) do

            local v = self.VElements[name]
            if (!v) then self.vRenderOrder = nil break end
            if (v.hide) then continue end

            local model = v.modelEnt
            local sprite = v.spriteMaterial

            if (!v.bone) then continue end

            local pos, ang = self:GetBoneOrientation( self.VElements, v, vm )

            if (!pos) then continue end

            if (v.type == "Model" and IsValid(model)) then

                model:SetPos(pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z )
                ang:RotateAroundAxis(ang:Up(), v.angle.y)
                ang:RotateAroundAxis(ang:Right(), v.angle.p)
                ang:RotateAroundAxis(ang:Forward(), v.angle.r)

                model:SetAngles(ang)
                //model:SetModelScale(v.size)
                local matrix = Matrix()
                matrix:Scale(v.size)
                model:EnableMatrix( "RenderMultiply", matrix )

                if (v.material == "") then
                    model:SetMaterial("")
                elseif (model:GetMaterial() != v.material) then
                model:SetMaterial( v.material )
                end

                if (v.skin and v.skin != model:GetSkin()) then
                model:SetSkin(v.skin)
                end

                if (v.bodygroup) then
                    for k, v in pairs( v.bodygroup ) do
                        if (model:GetBodygroup(k) != v) then
                        model:SetBodygroup(k, v)
                        end
                    end
                end

                if (v.surpresslightning) then
                    render.SuppressEngineLighting(true)
                end

                render.SetColorModulation(v.color.r/255, v.color.g/255, v.color.b/255)
                render.SetBlend(v.color.a/255)
                model:DrawModel()
                render.SetBlend(1)
                render.SetColorModulation(1, 1, 1)

                if (v.surpresslightning) then
                    render.SuppressEngineLighting(false)
                end

            elseif (v.type == "Sprite" and sprite) then

                local drawpos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
                render.SetMaterial(sprite)
                render.DrawSprite(drawpos, v.size.x, v.size.y, v.color)

            elseif (v.type == "Quad" and v.draw_func) then

                local drawpos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
                ang:RotateAroundAxis(ang:Up(), v.angle.y)
                ang:RotateAroundAxis(ang:Right(), v.angle.p)
                ang:RotateAroundAxis(ang:Forward(), v.angle.r)

                cam.Start3D2D(drawpos, ang, v.size)
                v.draw_func( self )
                cam.End3D2D()

            end

            end

            end

            SWEP.wRenderOrder = nil
            function SWEP:DrawWorldModel()

                if (self.ShowWorldModel == nil or self.ShowWorldModel) then
                    self:DrawModel()
                end

                if (!self.WElements) then return end

                if (!self.wRenderOrder) then

                self.wRenderOrder = {}

                for k, v in pairs( self.WElements ) do
                    if (v.type == "Model") then
                        table.insert(self.wRenderOrder, 1, k)
                    elseif (v.type == "Sprite" or v.type == "Quad") then
                        table.insert(self.wRenderOrder, k)
                    end
                end

                end

                if (IsValid(self.Owner)) then
                    bone_ent = self.Owner
                else
                    // when the weapon is dropped
                    bone_ent = self
                end

                for k, name in pairs( self.wRenderOrder ) do

                    local v = self.WElements[name]
                    if (!v) then self.wRenderOrder = nil break end
                    if (v.hide) then continue end

                    local pos, ang

                    if (v.bone) then
                        pos, ang = self:GetBoneOrientation( self.WElements, v, bone_ent )
                    else
                        pos, ang = self:GetBoneOrientation( self.WElements, v, bone_ent, "ValveBiped.Bip01_R_Hand" )
                    end

                    if (!pos) then continue end

                    local model = v.modelEnt
                    local sprite = v.spriteMaterial

                    if (v.type == "Model" and IsValid(model)) then

                        model:SetPos(pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z )
                        ang:RotateAroundAxis(ang:Up(), v.angle.y)
                        ang:RotateAroundAxis(ang:Right(), v.angle.p)
                        ang:RotateAroundAxis(ang:Forward(), v.angle.r)

                        model:SetAngles(ang)
                        //model:SetModelScale(v.size)
                        local matrix = Matrix()
                        matrix:Scale(v.size)
                        model:EnableMatrix( "RenderMultiply", matrix )

                        if (v.material == "") then
                            model:SetMaterial("")
                        elseif (model:GetMaterial() != v.material) then
                        model:SetMaterial( v.material )
                        end

                        if (v.skin and v.skin != model:GetSkin()) then
                        model:SetSkin(v.skin)
                        end

                        if (v.bodygroup) then
                            for k, v in pairs( v.bodygroup ) do
                                if (model:GetBodygroup(k) != v) then
                                model:SetBodygroup(k, v)
                                end
                            end
                        end

                        if (v.surpresslightning) then
                            render.SuppressEngineLighting(true)
                        end

                        render.SetColorModulation(v.color.r/255, v.color.g/255, v.color.b/255)
                        render.SetBlend(v.color.a/255)
                        model:DrawModel()
                        render.SetBlend(1)
                        render.SetColorModulation(1, 1, 1)

                        if (v.surpresslightning) then
                            render.SuppressEngineLighting(false)
                        end

                    elseif (v.type == "Sprite" and sprite) then

                        local drawpos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
                        render.SetMaterial(sprite)
                        render.DrawSprite(drawpos, v.size.x, v.size.y, v.color)

                    elseif (v.type == "Quad" and v.draw_func) then

                        local drawpos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
                        ang:RotateAroundAxis(ang:Up(), v.angle.y)
                        ang:RotateAroundAxis(ang:Right(), v.angle.p)
                        ang:RotateAroundAxis(ang:Forward(), v.angle.r)

                        cam.Start3D2D(drawpos, ang, v.size)
                        v.draw_func( self )
                        cam.End3D2D()

                    end

                    end

                    end

                    function SWEP:GetBoneOrientation( basetab, tab, ent, bone_override )

                        local bone, pos, ang
                        if (tab.rel and tab.rel != "") then

                        local v = basetab[tab.rel]

                        if (!v) then return end

                        // Technically, if there exists an element with the same name as a bone
                        // you can get in an infinite loop. Let's just hope nobody's that stupid.
                        pos, ang = self:GetBoneOrientation( basetab, v, ent )

                        if (!pos) then return end

                        pos = pos + ang:Forward() * v.pos.x + ang:Right() * v.pos.y + ang:Up() * v.pos.z
                        ang:RotateAroundAxis(ang:Up(), v.angle.y)
                        ang:RotateAroundAxis(ang:Right(), v.angle.p)
                        ang:RotateAroundAxis(ang:Forward(), v.angle.r)

                        else

                            bone = ent:LookupBone(bone_override or tab.bone)

                            if (!bone) then return end

                            pos, ang = Vector(0,0,0), Angle(0,0,0)
                            local m = ent:GetBoneMatrix(bone)
                            if (m) then
                                pos, ang = m:GetTranslation(), m:GetAngles()
                            end

                            if (IsValid(self.Owner) and self.Owner:IsPlayer() and
                                    ent == self.Owner:GetViewModel() and self.ViewModelFlip) then
                                ang.r = -ang.r // Fixes mirrored models
                            end

                        end

                        return pos, ang
                    end

                    function SWEP:CreateModels( tab )

                        if (!tab) then return end

                        // Create the clientside models here because Garry says we can't do it in the render hook
                        for k, v in pairs( tab ) do
                            if (v.type == "Model" and v.model and v.model != "" and (!IsValid(v.modelEnt) or v.createdModel != v.model) and
                            string.find(v.model, ".mdl") and file.Exists (v.model, "GAME") ) then

                            v.modelEnt = ClientsideModel(v.model, RENDER_GROUP_VIEW_MODEL_OPAQUE)
                            if (IsValid(v.modelEnt)) then
                                v.modelEnt:SetPos(self:GetPos())
                                v.modelEnt:SetAngles(self:GetAngles())
                                v.modelEnt:SetParent(self)
                                v.modelEnt:SetNoDraw(true)
                                v.createdModel = v.model
                            else
                                v.modelEnt = nil
                            end

                            elseif (v.type == "Sprite" and v.sprite and v.sprite != "" and (!v.spriteMaterial or v.createdSprite != v.sprite)
                            and file.Exists ("materials/"..v.sprite..".vmt", "GAME")) then

                            local name = v.sprite.."-"
                            local params = { ["$basetexture"] = v.sprite }
                                    // make sure we create a unique name based on the selected options
                            local tocheck = { "nocull", "additive", "vertexalpha", "vertexcolor", "ignorez" }
                            for i, j in pairs( tocheck ) do
                                if (v[j]) then
                                    params["$"..j] = 1
                                    name = name.."1"
                                else
                                    name = name.."0"
                                end
                            end

                            v.createdSprite = v.sprite
                            v.spriteMaterial = CreateMaterial(name,"UnlitGeneric",params)

                            end
                        end

                    end

                    local allbones
                    local hasGarryFixedBoneScalingYet = false

                    function SWEP:UpdateBonePositions(vm)

                        if self.ViewModelBoneMods then

                            if (!vm:GetBoneCount()) then return end

                            // !! WORKAROUND !! //
                            // We need to check all model names :/
                            local loopthrough = self.ViewModelBoneMods
                            if (!hasGarryFixedBoneScalingYet) then
                            allbones = {}
                            for i=0, vm:GetBoneCount() do
                                local bonename = vm:GetBoneName(i)
                                if (self.ViewModelBoneMods[bonename]) then
                                    allbones[bonename] = self.ViewModelBoneMods[bonename]
                                else
                                    allbones[bonename] = {
                                        scale = Vector(1,1,1),
                                        pos = Vector(0,0,0),
                                        angle = Angle(0,0,0)
                                    }
                                end
                            end

                            loopthrough = allbones
                            end
                            // !! ----------- !! //

                            for k, v in pairs( loopthrough ) do
                                local bone = vm:LookupBone(k)
                                if (!bone) then continue end

                                // !! WORKAROUND !! //
                                local s = Vector(v.scale.x,v.scale.y,v.scale.z)
                                local p = Vector(v.pos.x,v.pos.y,v.pos.z)
                                local ms = Vector(1,1,1)
                                if (!hasGarryFixedBoneScalingYet) then
                                local cur = vm:GetBoneParent(bone)
                                while(cur >= 0) do
                                    local pscale = loopthrough[vm:GetBoneName(cur)].scale
                                    ms = ms * pscale
                                    cur = vm:GetBoneParent(cur)
                                end
                                end

                                s = s * ms
                                        // !! ----------- !! //

                                if vm:GetManipulateBoneScale(bone) != s then
                                vm:ManipulateBoneScale( bone, s )
                                end
                                if vm:GetManipulateBoneAngles(bone) != v.angle then
                                vm:ManipulateBoneAngles( bone, v.angle )
                                end
                                if vm:GetManipulateBonePosition(bone) != p then
                                vm:ManipulateBonePosition( bone, p )
                                end
                                end
                        else
                            self:ResetBonePositions(vm)
                        end

                    end

                    function SWEP:ResetBonePositions(vm)

                        if (!vm:GetBoneCount()) then return end
                        for i=0, vm:GetBoneCount() do
                            vm:ManipulateBoneScale( i, Vector(1, 1, 1) )
                            vm:ManipulateBoneAngles( i, Angle(0, 0, 0) )
                            vm:ManipulateBonePosition( i, Vector(0, 0, 0) )
                        end

                    end

                    /**************************
                    Global utility code
                    **************************/

                    // Fully copies the table, meaning all tables inside this table are copied too and so on (normal table.Copy copies only their reference).
                    // Does not copy entities of course, only copies their reference.
                    // WARNING: do not use on tables that contain themselves somewhere down the line or you'll get an infinite loop
                    function table.FullCopy( tab )

                        if (!tab) then return nil end

                        local res = {}
                        for k, v in pairs( tab ) do
                            if (type(v) == "table") then
                                res[k] = table.FullCopy(v) // recursion ho!
                            elseif (type(v) == "Vector") then
                                res[k] = Vector(v.x, v.y, v.z)
                            elseif (type(v) == "Angle") then
                                res[k] = Angle(v.p, v.y, v.r)
                            else
                                res[k] = v
                            end
                        end

                        return res

                    end

                end

