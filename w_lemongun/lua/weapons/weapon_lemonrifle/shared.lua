-- Variables that are used on both client and server
if SERVER then

--resource.AddFile("sound/weapons/lemonar2/empty.wav")
--resource.AddFile("sound/weapons/lemonar2/fire1.wav")
--resource.AddFile("sound/weapons/lemonar2/altfire.wav")
--resource.AddFile("models/weapons/v_lrifle.mdl")
--resource.AddFile("materials/models/weapons/v_lrifle/v_irifle_core.vmt")
--resource.AddFile("materials/models/weapons/v_lrifle/lemon2.vtf")
--resource.AddFile("materials/models/weapons/v_lrifle/v_irifle_core.vtf")
--resource.AddFile("materials/models/weapons/v_lrifle/v_irifle_mask.vtf")
--resource.AddFile("materials/models/weapons/v_lrifle/lemon1.vtf")
--resource.AddFile("materials/models/weapons/v_lrifle/lemon1.vmt")
--resource.AddFile("materials/models/weapons/v_lrifle/v_irifle_normal.vtf")
--resource.AddFile("materials/models/weapons/v_lrifle/lemon2.vmt")
--resource.AddFile("materials/models/weapons/v_lrifle/v_irifle.vmt")
--resource.AddFile("materials/models/weapons/v_lrifle/v_irifle.vtf")
--resource.AddFile("materials/vgui/ttt_fun_killicons/lemongun.png")
end

SWEP.PrintName			= "Lemon Rifle"				// 'Nice' Weapon name (Shown on HUD)	
SWEP.Slot				= 2							// Slot in the weapon selection menu
SWEP.SlotPos			= 1							// Position in the slot
SWEP.Icon				= "vgui/ttt_fun_killicons/lemongun.png"

SWEP.Base 				= "gb_base"

SWEP.ViewModelFlip		= false
SWEP.ViewModel 			= "models/weapons/v_lrifle.mdl"
SWEP.WorldModel 			= "models/weapons/w_irifle.mdl"
SWEP.HoldType				= "ar2"
SWEP.Spawnable			= true
SWEP.AdminSpawnable		= false

SWEP.Primary.Sound 		= Sound("weapons/lemonar2/fire1.mp3")
SWEP.Primary.Reload 		= Sound("weapons/lemonar2/empty.mp3")
SWEP.Primary.Recoil		= 0.6
SWEP.Primary.Damage		= 14
SWEP.Primary.NumShots		= 1
SWEP.Primary.Cone			= 0.016
SWEP.Primary.Delay 		= 0.14

SWEP.Primary.ClipSize		= 45					// Size of a clip
SWEP.Primary.DefaultClip	= 45					// Default number of bullets in a clip
SWEP.Primary.ClipMax = 90 
SWEP.Primary.Automatic		= true				// Automatic/Semi Auto
SWEP.Primary.Ammo			= "AR2"
SWEP.AmmoEnt = "item_ammo_revolver_ttt"

SWEP.Secondary.ClipSize		= -1					// Size of a clip
SWEP.Secondary.DefaultClip	= -1					// Default number of bullets in a clip
SWEP.Secondary.Automatic	= false				// Automatic/Semi Auto
SWEP.Secondary.Ammo		= "none"


SWEP.LimitedStock = false
SWEP.AllowDrop = true
SWEP.AutoSpawnable = true
SWEP.Spawnable = true
SWEP.AdminSpawnable = true
SWEP.Kind = WEAPON_HEAVY

SWEP.IronSightsPos 		= Vector (-5.8777, -11.2377, 1.1921)
SWEP.IronSightsAng 		= Vector (6.1388, -0.2003, 0)


function SWEP:Precache()

    	util.PrecacheSound("weapons/lemonar2/fire1.mp3")
end

function SWEP:SecondaryAttack()
if self.NoSights or (not self.IronSightsPos) then return end
--if self:GetNextSecondaryFire() > CurTime() then return end
self.Weapon:EmitSound( Sound("weapons/lemonar2/altfire.mp3") )
self:SetIronsights(not self:GetIronsights())

self:SetNextSecondaryFire(CurTime() + 0.3)
end