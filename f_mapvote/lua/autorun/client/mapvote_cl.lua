if not CLIENT then return end
print("loading mapvote")
--if not SERVER then return end

vgui.Register("DHTTPImageButton", {
    Init = function(self)
        self:SetDrawBackground(false)
        self:SetDrawBorder(false)
        self:SetCursor('hand')
        self.m_Image = vgui.Create("HTML")
        self.m_Image:Dock(FILL)
        self:SetText("")
        self:SetColor(Color(255, 255, 255, 255))
    end,

    SetImage = function(self, url)
        self.m_Image:SetHTML( [[<img src="]] .. url .. [[" width="128" height="128"">]] )
    end
}, "DButton")



gb_mapvote.ClientInit = function()

    gb_mapvote.playerAvatars = {}
    gb_mapvote.Winnertext = "No Votes yet"


    gb_mapvote.ResetImageLocations()
    gb_mapvote.userList = {}
    gb_mapvote.voteCount = {}
    gb_mapvote.playerAvatars = {}
    gb_mapvote.SecondsLeft = gb_mapvote.countdown_seconds
end

gb_mapvote.ResetImageLocations = function()
    gb_mapvote.imageLocationsLeft = {}
    gb_mapvote.imageLocationsLeft[99] = 302
    gb_mapvote.imageLocationsLeft[0] = 550
    gb_mapvote.imageLocationsLeft[1] = 78
    gb_mapvote.imageLocationsLeft[2] = 236
    gb_mapvote.imageLocationsLeft[3] = 394
    gb_mapvote.imageLocationsLeft[4] = 552
    gb_mapvote.imageLocationsLeft[5] = 78
    gb_mapvote.imageLocationsLeft[6] = 236
    gb_mapvote.imageLocationsLeft[7] = 394
    gb_mapvote.imageLocationsLeft[8] = 552

    gb_mapvote.imageLocationsTop = {}
    gb_mapvote.imageLocationsTop[99] = 115
    gb_mapvote.imageLocationsTop[0] = 65
    gb_mapvote.imageLocationsTop[1] = 315
    gb_mapvote.imageLocationsTop[2] = 315
    gb_mapvote.imageLocationsTop[3] = 315
    gb_mapvote.imageLocationsTop[4] = 315
    gb_mapvote.imageLocationsTop[5] = 530
    gb_mapvote.imageLocationsTop[6] = 530
    gb_mapvote.imageLocationsTop[7] = 530
    gb_mapvote.imageLocationsTop[8] = 530
end




gb_mapvote.SetImageLocations = function(vote, posTop, posLeft)
    if (gb_mapvote.voteCount[vote] == 7 or
            gb_mapvote.voteCount[vote] == 14 or
            gb_mapvote.voteCount[vote] == 21) then
        gb_mapvote.imageLocationsTop[vote] = posTop + 18
        gb_mapvote.imageLocationsLeft[vote] = posLeft - (18 * 6)
    else
        gb_mapvote.imageLocationsLeft[vote] = posLeft + 18
    end
end


gb_mapvote.GetPlayerAvatars = function()
    gb_mapvote.ResetImageLocations()

    for _, v in ipairs(player.GetAll()) do
        local id = v:SteamID()
        local vote = 0
        local posTop = gb_mapvote.imageLocationsTop[0]
        local posLeft = gb_mapvote.imageLocationsLeft[0]
        gb_mapvote.playerAvatars[id] = vgui.Create("AvatarImage", gb_mapvote.mapvoteFrame)
        gb_mapvote.playerAvatars[id]:SetSize(16, 16)
        gb_mapvote.playerAvatars[id]:SetPlayer(v, 16)
        gb_mapvote.playerAvatars[id]:SetPos(posLeft, posTop)
        gb_mapvote.SetImageLocations(vote, posTop, posLeft)
    end
end




gb_mapvote.getVotesForMap = function()
    gb_mapvote.ResetImageLocations()
    local data2 = util.JSONToTable( net.ReadString() )
    local data = {}
    data[1] = data2[2]
    data[2] = data2[1]

    if not IsValid(winnerText) then return end
    winnerText:SetText(data[2])
    winnerText:SizeToContents()

    for _, v in ipairs(player.GetAll()) do
        local id = v:SteamID()
        vote = (data[1][id] or 0)
        gb_mapvote.voteCount[vote] = (gb_mapvote.voteCount[vote] or 0) + 1
        local posTop = gb_mapvote.imageLocationsTop[vote]
        local posLeft = gb_mapvote.imageLocationsLeft[vote]
        if (IsValid(gb_mapvote.playerAvatars[id])) then
            gb_mapvote.playerAvatars[id]:SetPos(posLeft, posTop)
        end
        gb_mapvote.SetImageLocations(vote, posTop, posLeft)
    end
end


net.Receive("gb_mapvote_getVoteCount", gb_mapvote.getVotesForMap)




gb_mapvote.StartTimer = function()

    timer.Create("gb_mapvote_CountDownTimer", 1, (gb_mapvote.SecondsLeft+5), function()

        if (gb_mapvote.countDowntext:IsValid()) then
            gb_mapvote.countDowntext:SetText(gb_mapvote.SecondsLeft .. "")
            if gb_mapvote.SecondsLeft > 15 then
                gb_mapvote.countDowntext:SetTextColor(Color(0, 255, 0, 255))
            elseif gb_mapvote.SecondsLeft < 16 and gb_mapvote.SecondsLeft > 5 then
                gb_mapvote.countDowntext:SetTextColor(Color(255, 128, 0, 255))
            else
                gb_mapvote.countDowntext:SetTextColor(Color(255, 0, 0, 255))
            end
        end
        --Remove the frame
        if gb_mapvote.SecondsLeft < 1 then
            if gb_mapvote.mapvoteFrame:IsValid() then gb_mapvote.mapvoteFrame:Remove() end
        end
        if gb_mapvote.SecondsLeft < 1 then
            gb_mapvote.SecondsLeft = 0
        end
        gb_mapvote.SecondsLeft = gb_mapvote.SecondsLeft - 1
    end)
end



gb_mapvote.StartMapVote = function()

    gb_mapvote.ClientInit()
    LocalPlayer():ChatPrint("Vote for the next map!")
    voteNumbersMap = {}

    gb_mapvote.mapselection = net.ReadTable()

    gb_mapvote.mapvoteFrame = vgui.Create("DFrame")
    gb_mapvote.mapvoteFrame:SetPos(200, 100)
    gb_mapvote.mapvoteFrame:SetSize(790, 600)
    gb_mapvote.mapvoteFrame:SetTitle("")
    gb_mapvote.mapvoteFrame:SetVisible(true)
    gb_mapvote.mapvoteFrame:SetDraggable(true)
    gb_mapvote.mapvoteFrame:ShowCloseButton(true)

    gb_mapvote.mapvoteFrame.Paint = function()
        draw.RoundedBox(8, 0, 0, gb_mapvote.mapvoteFrame:GetWide(), gb_mapvote.mapvoteFrame:GetTall(), Color(0, 0, 0, 252))
    end

    gb_mapvote.mapvoteFrame:Center()
    gb_mapvote.mapvoteFrame:MakePopup()

    --TitleText
    surface.CreateFont("totleText", { font = "Arial", size = 28, weight = 700, })
    gb_mapvote.titleText = vgui.Create("DLabel", gb_mapvote.mapvoteFrame)
    gb_mapvote.titleText:SetSize(400, 28)
    gb_mapvote.titleText:SetText("Vote for next map")
    gb_mapvote.titleText:SetPos(60, 5)
    gb_mapvote.titleText:SetFont("totleText")
    gb_mapvote.titleText:SetTextColor(Color(255, 255, 0, 255))

    --StayOnThisMap button
    if server_id < 49 or server_id > 60 then
        if GetGlobalInt("gb_mapvote.ShowStayOnMap") == 1 and #gb_mapvote.mapselection > 3 then
            button = vgui.Create("DButton", gb_mapvote.mapvoteFrame)
            button:SetPos(300, 50)
            button:SetText("Stay on this map")
            button:SetSize(150, 60)
            button.DoClick = function()
                net.Start("gb_mapvote_castvote")
                net.WriteInt(99,32)
                net.WriteString(tostring(LocalPlayer():SteamID()))
                net.SendToServer()
            --votedForTextmap:SetText("Stay on this map")
            end
        end
    end
    --StayONThisMap number of votes
    voteNumbersMap[99] = vgui.Create("DLabel", gb_mapvote.mapvoteFrame)
    voteNumbersMap[99]:SetSize(128, 40)
    voteNumbersMap[99]:SetText("")
    voteNumbersMap[99]:SetPos(310, 115)
    voteNumbersMap[99]:SetTextColor(Color(255, 255, 0, 255))

    --Countdowntext
    surface.CreateFont("countDownTest", { font = "Arial", size = 30, weight = 700, })
    gb_mapvote.countDowntext = vgui.Create("DLabel", gb_mapvote.mapvoteFrame)
    gb_mapvote.countDowntext:SetSize(138, 40)
    gb_mapvote.countDowntext:SetText((gb_mapvote.countdown_seconds or 30))
    gb_mapvote.countDowntext:SetPos(650, 5)
    gb_mapvote.countDowntext:SetFont("countDownTest")
    gb_mapvote.countDowntext:SetTextColor(Color(0, 255, 0, 255))

    -- You have voted for text
    votedForText = vgui.Create("DLabel", gb_mapvote.mapvoteFrame)
    votedForText:SetSize(300, 20)
    votedForText:SetText("Not voted yet:")
    votedForText:SetPos(550, 45)

    surface.CreateFont("winnerText", { font = "Arial", size = 20, weight = 700, })


    --Winning map text
    winnerText2 = vgui.Create("DLabel", gb_mapvote.mapvoteFrame)
    winnerText2:SetSize(200, 20)
    winnerText2:SetText("Winning map:")
    winnerText2:SetTextColor(Color(255, 255, 255, 255))
    winnerText2:SetPos(65, 45)

    winnerText = vgui.Create("DLabel", gb_mapvote.mapvoteFrame)
    winnerText:SetSize(200, 40)
    winnerText:SetText("")
    winnerText:SetTextColor(Color(255, 0, 0, 255))
    winnerText:SetPos(65, 65)
    winnerText:SetFont("winnerText")


    for i = 1, #gb_mapvote.mapselection do --(mapcount)

        if (i < 5) then
            posTop = 160
            posLeft = ((i * 158) - 140) + 60
        else
            posTop = 375
            posLeft = ((158 * (i - 4)) - 140) + 60
        end



        local button = vgui.Create("DHTML",gb_mapvote.mapvoteFrame)

        button:OpenURL("https://ttt-fun.com/sites/all/modules/custom/ttt_fun_maps/ttt-fun-mapicons/" .. gb_mapvote.mapselection[i] .. ".png")

        button:SetSize(160, 160)

        button:SetPos(posLeft, posTop)
        button:SetText("")
        button:SetScrollbars(false)

        local button = vgui.Create("DImageButton",gb_mapvote.mapvoteFrame)
        button:SetSize(130, 130)
        button:SetColor(255,255,255,0)

        button:SetPos(posLeft, posTop)
        button:SetText("")

        button.DoClick = function()
            net.Start("gb_mapvote_castvote")
            if game.GetMap() == gb_mapvote.mapselection[i] then
                net.WriteInt( 99,32)
                net.WriteString(tostring(LocalPlayer():SteamID() ))
            else
                net.WriteInt(i, 32)
                net.WriteString( tostring(LocalPlayer():SteamID()))
            end
            net.SendToServer()
        --votedForTextmap:SetText(Mapvote.mapselection[i])
        --frame:Remove()
        end

       local button2 = vgui.Create("DLabel", gb_mapvote.mapvoteFrame)
        button2:SetSize(138, 20)
        button2:SetText(gb_mapvote.mapselection[i])
        button2:SetPos(posLeft, posTop + 130)
        button2.DoClick = function()
            net.Start("gb_mapvote_castvote")
            net.WriteInt( i,32)
            net.WriteString(LocalPlayer():SteamID())
            net.SendToServer()
        --frame:Remove()
        --votedForTextmap:SetText(Mapvote.mapselection[i])
        end

        voteNumbersMap[i] = vgui.Create("DLabel", gb_mapvote.mapvoteFrame)
        voteNumbersMap[i]:SetSize(128, 40)
        voteNumbersMap[i]:SetText("")
        voteNumbersMap[i]:SetPos(posLeft, posTop + 150)
        voteNumbersMap[i]:SetTextColor(Color(255, 255, 0, 255))
    end

    gb_mapvote.GetPlayerAvatars()
    gb_mapvote.StartTimer()
end



net.Receive("gb_mapvote_getVoteCount", gb_mapvote.getVotesForMap)
net.Receive("gb_mapvote_startMapVote", gb_mapvote.StartMapVote)

