AddCSLuaFile()
print("GreenBlacks Map Vote v4 loaded!")

Mapvote = {}
Mapvote.mapselection = {}
voteCountDisplay = {}
Mapvote.votes = {}
Mapvote.doneVoting = false

mapvoteFrame = {}
countDowntext = {}
votedForText = {}
voteNumbersMap = {}
winnerText = {}

SetGlobalInt( "mapreloads", 0)

--TODO: Clean up this code. A lot.
if GetConVarString("gamemode") == "terrortown" and SERVER then
	util.AddNetworkString("mapvote_prompt")
	util.AddNetworkString("mapvote_castvote")
	util.AddNetworkString("mapvote_getVoteCount")

	--Instantiate our variables and tables.
	Mapvote.votes = {}
	Mapvote.playerlist = {}
	Mapvote.voted = false
	Mapvote.voting = false
	Mapvote.maps = {}
	Mapvote.rtvScheduled = false
	MapMaterial = {}

         apptime = RealTime( )
                print( "Apptime is:"..apptime )
                if apptime && apptime < 30 then 
                        print("Will Change LEvel in 30 secs")
                    timer.Simple(30, function()
                       RunConsoleCommand("changelevel",  table.Random(Mapvote.maps) )
                    end)

                end


        


	Mapvote.Initialize = function()
		--Establish CVars. They are grouped according to the set they were introduced with.
		if not ConVarExists("mapvote_randommaps") 			then CreateConVar("mapvote_randommaps",8,{ FCVAR_ARCHIVE }) end
		if not ConVarExists("mapvote_oldestmaps") 			then CreateConVar("mapvote_oldestmaps",0,{ FCVAR_ARCHIVE }) end
		if not ConVarExists("mapvote_shuffle") 				then CreateConVar("mapvote_shuffle",1,{ FCVAR_ARCHIVE }) end
		if not ConVarExists("mapvote_cycle_delay")			then CreateConVar("mapvote_cycle_delay",1,{ FCVAR_ARCHIVE }) end
		if not ConVarExists("mapvote_rtv") 					then CreateConVar("mapvote_rtv",1,{ FCVAR_ARCHIVE }) end
		if not ConVarExists("mapvote_rtv_ratio") 			then CreateConVar("mapvote_rtv_ratio",0.35,{ FCVAR_ARCHIVE }) end
		if not ConVarExists("mapvote_rtv_minrounds") 		then CreateConVar("mapvote_rtv_minrounds",2,{ FCVAR_ARCHIVE }) end
		if not ConVarExists("mapvote_rtv_minplayers") 		then CreateConVar("mapvote_rtv_minplayers",1,{ FCVAR_ARCHIVE }) end
		if not ConVarExists("mapvote_rtv_forcevote") 		then CreateConVar("mapvote_rtv_forcevote",0,{ FCVAR_ARCHIVE }) end
		if not ConVarExists("mapvote_start_timeleft") 		then CreateConVar("mapvote_start_timeleft",0,{ FCVAR_ARCHIVE }) end
		if not ConVarExists("mapvote_start_roundsleft") 	then CreateConVar("mapvote_start_roundsleft",1,{ FCVAR_ARCHIVE }) end
		if not ConVarExists("mapvote_cycle_forcechange")	then CreateConVar("mapvote_cycle_forcechange",1,{ FCVAR_ARCHIVE }) end
                if not ConVarExists("mapvote_max_reloads")	then CreateConVar("mapvote_max_reloads",2,{ FCVAR_ARCHIVE }) end
		--if not ConVarExists("mapvote_rtv_anonymous") then CreateConVar("mapvote_rtv_anonymous",0,{ FCVAR_REPLICATED, FCVAR_ARCHIVE }) end --That was probably a bad idea for a feature.

		--Create the file containing the map whitelist.
		--This code was originally from the maplist for the game creation menu, and adapted for a more robust map menu.
		if file.Exists("ttt_mapcycle.txt", "DATA") == false then
			local g_MapList = {}

			for k, v in pairs( file.Find( "maps/*.bsp", "GAME" ) ) do
		
				-- Don't add useless maps
				local lowername = string.lower( v )
				if (string.find( lowername, "^cs_") or string.find( lowername, "^de_") or string.find( lowername, "^clue") or string.find( lowername, "^xmas") or string.find( lowername, "^ttt_")) then
		
					local Mat = nil
					local name = string.gsub( v, ".bsp", "" )
				
					--The map icons are added as clientside assets in case a custom GUI wants to include a preview or something.
					
					--Mat = "materials/vgui/ttt_fun_map_icons/"..name..".png"
                                       -- resource.AddFile(Mat) 


					g_MapList[ v ] = { Material = Mat, Name = name}
				end
			end 
			print("First run, built custom maplist(CSS and TTT)")
			print("Writing to maplist file data/ttt_maplist.txt")
			for k,v in pairs(g_MapList) do
				file.Append("ttt_mapcycle.txt", v.Name.."\n")
			end
		end

		Mapvote.maps = string.Explode("\n",file.Read("ttt_mapcycle.txt", "DATA"))
		table.remove(Mapvote.maps) --When reading the file there is a blank entry at the end; remove that.
		
		local mCount = table.Count(Mapvote.maps)	

                
		
		---Shuffeling,shuffeling
		for i=1,mCount do
				local current = table.remove(Mapvote.maps,math.random(mCount))
				table.insert(Mapvote.maps,current)
	    end  
               
	    

		if not table.HasValue(Mapvote.maps,game.GetMap()) then
			print("Current map not on rotation! Adding to map list.")
			table.insert(Mapvote.maps,1,game.GetMap())
		end

		--Pop the current map to the end of the map list. Oldest maps first.
		local curMap = table.remove(Mapvote.maps,table.KeyFromValue(Mapvote.maps,game.GetMap()))
		table.insert(Mapvote.maps,curMap)

		--file.Write("ttt_mapcycle.txt", "")
		--for k,v in pairs(Mapvote.maps) do
		--	file.Append("ttt_mapcycle.txt", v.."\n")
		--end

		--print("Server map list:")
		--ßPrintTable(Mapvote.maps)

		--Generate the voting selection, first by inserting the oldest maps, and then by adding a random selection of maps.
		Mapvote.mapselection = {}
			
		
		
		--Check to make sure the counts are acceptable, so we don't get stuck in an infinite loop.
		local oCount = 0 --GetConVarNumber("mapvote_oldestmaps")
		local rCount = 16 --GetConVarNumber("mapvote_randommaps")
		if oCount > mCount or oCount < 0 then
			oCount = mCount
			print("Warning! mapvote_oldestmaps is out of bounds, clamping oldest map count to "..oCount.."")
		end
		--for i=1,oCount do
		--	if Mapvote.maps[i] == nil then break end
		--	table.insert(Mapvote.mapselection,Mapvote.maps[i])
		--end
		
		if rCount > mCount - oCount or rCount < 0 then
			rCount = mCount - oCount
			print("Warning! mapvote_randommaps is out of bounds, clamping random map count to "..rCount.."")
			rCount = 0
			Mapvote.mapselection = Mapvote.maps --Since all maps then are being loaded, just set the selection to the map table.
		end
		for i=1,rCount do
			local id = table.Random(Mapvote.maps)
			local maxtries = 5000000+1000000*rCount --An escape should we become stuck in an infinite loop.
			local try = 0
			while (table.HasValue(Mapvote.mapselection, id) or id == curMap) and not (try > maxtries)  do
				id = table.Random(Mapvote.maps)
				try = try + 1
			end
			if try > maxtries then 
				print("Unable to resolve new random maps after "..maxtries.." attempts, skipping.")
				print("Consider lowering mapvote_randommaps, or adding more maps to the rotation.")
			end
                        
              if file.Exists("maps/"..id..".bsp","GAME") then        	
                   
                      resource.AddFile("materials/vgui/ttt_fun_map_icons/"..id..".png")
                       print("Image for map "..id.." found and added!")
                  
			 
              
			       table.insert(Mapvote.mapselection,id)
		      else
			  --Remove the map
				table.remove(Mapvote.maps,table.KeyFromValue(Mapvote.maps,id))
				print("Removed map "..id.." from list")
				file.Write("ttt_mapcycle.txt", "")
				for k,v in pairs(Mapvote.maps) do
					file.Append("ttt_mapcycle.txt", v.."\n")
				end
			  
			  end
		end
                      resource.AddFile("materials/vgui/ttt_fun_map_icons/noimage.png") 

		local MapCount = table.Count(Mapvote.mapselection)

		--Shuffle the selection
		if GetConVarNumber("mapvote_shuffle") == 1 then
			for i=1,MapCount do
				local current = table.remove(Mapvote.mapselection,math.random(MapCount))
				table.insert(Mapvote.mapselection,current)
			end
		end

		print("Votemap map selection:")
		PrintTable(Mapvote.mapselection)
	end
	hook.Add( "Initialize", "mapvote_Initialize", Mapvote.Initialize )


        function getWinningMapName()
          winnerID = table.GetWinningKey(Mapvote.votes)
	   if winnerID == 99 then
	      winnerMap = "Stay on this map"
	   else
	      winnerMap = Mapvote.mapselection[winnerID]
	   end
             return winnerMap
         end





        --RECEIVE VOTES
	--[[The following hooks and functions are the actual voting component]]--
	Mapvote.ReceiveVote = function(len,ply)
		if  Mapvote.voting == true then
                        
                        local data = net.ReadTable()
			local mapID = data[1]
                        local userId = data[2]

                   if  GetGlobalInt( "mapreloads") > (GetConVarNumber("mapvote_max_reloads") or 2)  && mapID == 99 then return end

                    if mapID <= table.Count(Mapvote.mapselection) and mapID >= 1 or mapID == 99 then --Make sure the data received is in a valid range.
			
                    
                       didVote = 0
                        if Mapvote.playerVotes[userId]  then
                           Mapvote.votes[Mapvote.playerVotes[userId]] = Mapvote.votes[Mapvote.playerVotes[userId]] - 1

                            --Send it to the client
                            -- net.Start("mapvote_getVoteCount")
                            --  net.WriteTable({Mapvote.playerVotes, getWinningMapName()}) 
                            -- net.Broadcast() 
							didVote = Mapvote.playerVotes[userId]
                        end
                       Mapvote.votes[mapID] = (Mapvote.votes[mapID] or 0) + 1 --Increment mapID the vote was for, or start tally.
		                 Mapvote.playerVotes[userId] = mapID

				
						if IsValid(ply) and didVote != Mapvote.playerVotes[userId]  then
							for k,v in pairs(player.GetAll()) do
							
									if mapID == 99 then
									  v:PrintMessage( HUD_PRINTTALK , ply:Nick().." voted for: Stay On this map ("..Mapvote.votes[mapID].." votes)")
									else
										
									  v:PrintMessage( HUD_PRINTTALK , ply:Nick().." voted for: "..Mapvote.mapselection[mapID].." ("..Mapvote.votes[mapID].." votes)")
									  
									end

									--Send it to all players
								  net.Start("mapvote_getVoteCount")
									 net.WriteTable( {Mapvote.playerVotes, getWinningMapName()}) 
								  net.Broadcast()                  
							end
						end
                               
                        table.insert(Mapvote.playerlist, ply)

                        return 
		      end
		end
	end
	net.Receive( "mapvote_castvote", Mapvote.ReceiveVote )





	--Should start a vote? Function
	--Return true if a vote should be started. 
	Mapvote.ShouldStartVote = function()
		if Mapvote.voted == true or Mapvote.doneVoting == true then 
                   Mapvote.EndVote()
                return false end
		
		--Check if the server presently meets the requirements for RTV.
		local plyCount = table.Count(player.GetAll())
		if (GetConVarNumber("mapvote_rtv") or 1) == 1 and (GetConVarNumber("ttt_round_limit") - GetGlobalInt("ttt_rounds_left")) >= (GetConVarNumber("mapvote_rtv_minrounds") or 2) then
			if Mapvote.rtvScheduled == true then 
				print("Starting Mapvote due to RTV")
                               Mapvote.StartVote()
				return true 
			end
		end
	
		--MAXTIME IS IN MINUTES! IMPORTANT DETAIL!
		local maxtime = GetConVarNumber("ttt_roundtime_minutes") or 10
		if (GetConVarNumber("mapvote_start_timeleft") or 0) == 0 then	
			if (GetConVarNumber("ttt_haste") or 0) == 1 then
				maxtime = (GetConVarNumber("ttt_haste_starting_minutes") or 5) + table.Count(player.GetAll())*(GetConVarNumber("ttt_haste_minutes_per_death") or 0.5)
			end
			maxtime = maxtime + GetConVarNumber("ttt_preptime_seconds")/60 + GetConVarNumber("ttt_posttime_seconds")/60
		else
			maxtime = GetConVarNumber("mapvote_start_timeleft") or 15
		end
		
		if (GetConVarNumber("mapvote_start_roundsleft") or 1) >= GetGlobalInt("ttt_rounds_left", 6) or math.floor((GetGlobalInt("ttt_time_limit_minutes") or 75) - (CurTime()/60)) <= maxtime then
		     Mapvote.StartVote()
                       return true
		end
		
		return false
	end
	

        --Start The Vote
	--Start the vote itself.
	Mapvote.StartVote = function()
		Mapvote.voting = true
        Mapvote.doneVoting = true
		Mapvote.playerlist = {}
        Mapvote.playerVotes = {}
		
		timer.Create( "endthemapcoteandChange", 21,1, Mapvote.EndVote ) --(GetConVarNumber("ttt_posttime_seconds")+31),1, Mapvote.EndVote)
		  
		net.Start("mapvote_prompt")
		net.WriteTable(Mapvote.mapselection)
                if( GetGlobalInt( "mapreloads") > (GetConVarNumber("mapvote_max_reloads") or 2) ) then
                    net.WriteString( "0" )
                else
                    net.WriteString( "1" )
                end
		net.Broadcast()
		hook.Add( "TTTDelayRoundStartForVote", "shouldwedelaytheroundprepatestateEND",Mapvote.EndVote )
            
	end
	
	
        hook.Add( "TTTDelayRoundStartForVote", "shouldwedelaytheroundprepatestate",Mapvote.ShouldStartVote )
        --hook.Add( "TTTDelayRoundStartForVote", "mapvote_CheckForVote", function() if Mapvote.ShouldStartVote() == true then Mapvote.StartVote() end end )


	Mapvote.ForceMapChange = function(delay) timer.Simple(delay,function() game.LoadNextMap() end) end
	hook.Add("TTTEndRound","mapvote_ForceMapchange", function()
		if Mapvote.voted == true and (Mapvote.rtvScheduled == true or GetConVarNumber("mapvote_cycle_forcechange") == 1) then
			Mapvote.ForceMapChange(GetConVarNumber("mapvote_cycle_delay") or 5)
		end
	end)



        --Done Voting, changemap or stay
	Mapvote.EndVote = function()

		if Mapvote.voting == true then
			local nextmap = 99 
              if  (table.Count(Mapvote.playerVotes) or 0) == 0 then
				nextmap = 1
			  else
				nextmap	= table.GetWinningKey(Mapvote.votes) or 1
		      end
			  
			 if nextmap == 99  then
                            SetGlobalInt( "mapreloads", GetGlobalInt("mapreloads") + 1)
                           
			    local nextMAPje = game.GetMap()
				for k,v in pairs(player.GetAll()) do
				   v:PrintMessage( HUD_PRINTTALK, "We stay on this map for another "..(GetConVarNumber("ttt_round_limit")).." rounds")
			     	v:PrintMessage( HUD_PRINTTALK, "("..(Mapvote.votes[99] or 0).."/"..(table.Count(Mapvote.playerVotes) or 0)..") votes.")
			    end

      			--RunConsoleCommand("nextlevel",nextMAPje)
				SetGlobalInt("ttt_rounds_left", GetConVarNumber("ttt_round_limit"))
				
				--RunConsoleCommand("nextlevel",nextMAPje)
				
				--Reset all variables
				print(nextMAPje)
				Mapvote.votes = {}
				Mapvote.voting 	= false
				Mapvote.voted = false
				Mapvote.voting = false
				Mapvote.rtvScheduled = false
				secsLeftMapCote = GetConVarNumber("ttt_posttime_seconds")+20
				Mapvote.playerlist = {}
                                Mapvote.playerVotes = {}
                                Mapvote.doneVoting = false
                                playerAvatars = {}

                                --Shuffle the maps again
                                local MapCount = table.Count(Mapvote.mapselection)

                                if GetConVarNumber("mapvote_shuffle") == 1 then
                                        for i=1,MapCount do
                                                local current = table.remove(Mapvote.mapselection,math.random(MapCount))
                                                table.insert(Mapvote.mapselection,current)
                                        end
                                end
			 end
			 
			 if (nextmap != 99) then

			   Mapvote.voting 	= false
			   Mapvote.voted	= true
                           Mapvote.doneVoting = true
			 
				 
				for k,v in pairs(player.GetAll()) do
					v:PrintMessage( HUD_PRINTTALK, "The winning map is "..Mapvote.mapselection[nextmap].."!")
					v:PrintMessage( HUD_PRINTTALK, "("..(Mapvote.votes[nextmap] or 0).."/"..table.Count(Mapvote.playerVotes)..") votes.")
				end		
				local nextmapid = table.KeyFromValue(Mapvote.maps,Mapvote.mapselection[nextmap])--Convert the ID from mapselection to the map registry.
				
				--Here we push the winning map to the top of the map list.
				local winningMap = table.remove(Mapvote.maps,(nextmapid or 1))
				print(winningMap)
				table.insert(Mapvote.maps,1,winningMap)				
		
				--file.Write("ttt_mapcycle.txt", "")
				--for k,v in pairs(Mapvote.maps) do
				--	file.Append("ttt_mapcycle.txt", v.."\n")
				--end
                           timer.Simple(1,  function() 
							   RunConsoleCommand("changelevel",winningMap)
							    end)
			
		    end
		
			
		end
	end
	hook.Add("TTTBeginRound","mapvote_EndMapVote", Mapvote.EndVote)
	
        --RTV
	--Handles the playertable for RTV.
	Mapvote.RTV = function(ply, text)
		if (GetConVarNumber("mapvote_rtv") or 0) != 1 then
			ply:PrintMessage( HUD_PRINTTALK, "RTV is disabled on this server." )
			return
		end
		if Mapvote.voting == true then
			ply:PrintMessage( HUD_PRINTTALK, "A vote is in progress." )
			return
		end
		if Mapvote.voted == true then
			ply:PrintMessage( HUD_PRINTTALK, "A vote has already been taken." )
			return
		end
		if Mapvote.rtvScheduled == true then
			ply:PrintMessage( HUD_PRINTTALK, "A RTV has already passed." )
			return
		end
		if table.HasValue(Mapvote.playerlist, ply) then
			ply:PrintMessage( HUD_PRINTTALK, "You have already RTV'd." )
			return
		end
			
		table.insert(Mapvote.playerlist,ply) 
		
		local rtvCount = table.Count(Mapvote.playerlist)
		local plyCount = table.Count(player.GetAll())
		local votesNeeded = math.ceil(plyCount*(GetConVarNumber("mapvote_rtv_ratio") or 0.35))
		if plyCount < GetConVarNumber("mapvote_rtv_minplayers") then
			votesNeeded = GetConVarNumber("mapvote_rtv_minplayers")
		end

		if rtvCount >= votesNeeded then	Mapvote.RTVPassed( nil ) end
		for k,v in pairs(player.GetAll()) do
			v:PrintMessage( HUD_PRINTTALK, ply:Name() .. " Wants to change the map! Type !rtv to rock the vote ("..rtvCount .."/"..votesNeeded..").")
		end			
	end
	
	Mapvote.RTVPassed = function(msg_override)
		Mapvote.rtvScheduled = true
		local message = "RTV passed! A vote will occur after "
	
		if (GetConVarNumber("ttt_round_limit") - GetGlobalInt("ttt_rounds_left") + 1) >= (GetConVarNumber("mapvote_rtv_minrounds") or 2) then
			message = message.."this round."
		else
			message = message..(GetConVarNumber("mapvote_rtv_minrounds") or 2) - ((GetConVarNumber("ttt_round_limit") - GetGlobalInt("ttt_rounds_left"))) .." rounds."
		end
		
		if (GetConVarNumber("mapvote_rtv_forcevote") or 0) == 1 then
			message = "RTV passed!"
			Mapvote.StartVote()
			--timer.Simple(GetConVarNumber("ttt_preptime_seconds") or 20, function() Mapvote.EndVote() end)
		end
		
		for k,v in pairs(player.GetAll()) do
			v:PrintMessage( HUD_PRINTTALK , msg_override or message )
		end
	end
	
	hook.Add("PlayerSay","mapvote_CheckRTV",function(ply,text,team)
		if string.lower(text) == "!rtv" or string.lower(text) == "rtv" then 
			Mapvote.RTV(ply,text)
		end
	end)
	
	--If a player disconnects, invalidate their RTV petition.
	hook.Add("PlayerDisconnected","mapvote_RTVDisconnect",function(ply)
		if voting == false and voted == false then
			if table.HasValue(Mapvote.playerlist,ply) then
				table.remove(Mapvote.playerlist,ply)
				return
			end
			
			local rtvCount = table.Count(Mapvote.playerlist)
			local plyCount = table.Count(player.GetAll())
			local votesNeeded = math.ceil(plyCount*(GetConVarNumber("mapvote_rtv_ratio") or 0.40))
			if plyCount < GetConVarNumber("mapvote_rtv_minplayers") then
				votesNeeded = GetConVarNumber("mapvote_rtv_minplayers")
			end
			
			if rtvCount >= votesNeeded then Mapvote.RTVPassed("A player has disconnected and the RTV has passed.") end		
		end
	end)
end





  playerAvatars = {}


if CLIENT then



function getVoteCount( isFirst )


    data = net.ReadTable()
   
   if IsValid(winnerText) then
              
        imageLocationsLeft = {}
        imageLocationsLeft[99] = 302
        imageLocationsLeft[0] = 550
        imageLocationsLeft[1] = 78
        imageLocationsLeft[2] = 236
        imageLocationsLeft[3] = 394
        imageLocationsLeft[4] = 552
        imageLocationsLeft[5] = 78
        imageLocationsLeft[6] = 236
        imageLocationsLeft[7] = 394
        imageLocationsLeft[8] = 552
                        
        imageLocationsTop = {}
        imageLocationsTop[99] = 115
        imageLocationsTop[0] = 65
        imageLocationsTop[1] = 315
        imageLocationsTop[2] = 315
        imageLocationsTop[3] = 315
        imageLocationsTop[4] = 315
        imageLocationsTop[5] = 530
        imageLocationsTop[6] = 530
        imageLocationsTop[7] = 530
        imageLocationsTop[8] = 530
                        
       userList = {}
       voteCount = {}

  -- Set the player avatars
   if not playerAvatars[ LocalPlayer():UniqueID() ] then
     for _, v in ipairs( player.GetAll() ) do
        id = v:UniqueID()
        vote = 0
        voteCount[ vote ] = (voteCount[vote] or 0) + 1
        posTop = imageLocationsTop[vote]
        posLeft = imageLocationsLeft[vote]
        playerAvatars[id] = vgui.Create("AvatarImage", mapvoteFrame)
        playerAvatars[id]:SetSize(16, 16)
        playerAvatars[id]:SetPlayer( v, 16 )
        playerAvatars[id]:SetPos(posLeft, posTop)
        
        if( voteCount[vote] == 7 or voteCount[vote] == 14 or voteCount[vote] == 21) then
           imageLocationsTop[vote] = posTop + 18
           imageLocationsLeft[vote] = posLeft - (18*6)
        else
           imageLocationsLeft[vote] = posLeft + 18
        end 
     end
   end
  



        winnerText:SetText( data[2] )
        winnerText:SizeToContents()

         for _, v in ipairs( player.GetAll() ) do
             id = v:UniqueID() 

             vote = (data[1][id] or 0)
             voteCount[ vote ] = (voteCount[vote] or 0) + 1
             posTop = imageLocationsTop[vote]
             posLeft = imageLocationsLeft[vote]
             if(IsValid( playerAvatars[id] ) ) then
               playerAvatars[id]:SetPos(posLeft, posTop)
             end
             if( voteCount[vote] == 7 or voteCount[vote] == 14 or voteCount[vote] == 21) then
                imageLocationsTop[vote] = posTop + 18
                imageLocationsLeft[vote] = posLeft - (18*6)
             else
                imageLocationsLeft[vote] = posLeft + 18
             end  


         end
   end
end

 net.Receive( "mapvote_getVoteCount", getVoteCount ) 
  
MapVoteMenu = function()
   
    secsLeftMapCote = GetConVarNumber("ttt_posttime_seconds")+20
	
	print("Map vote started!\nMap Selection:")
	LocalPlayer():ChatPrint("Vote for the next map!")
	playerAvatars = {}
	Mapvote.mapselection = net.ReadTable()
        Mapvote.showExtend = net.ReadString()
	local mapcount = table.Count(Mapvote.mapselection)
	PrintTable(Mapvote.mapselection)
	
        mapvoteFrame = vgui.Create("DFrame") 	
	mapvoteFrame:SetPos(200, 100)              
	mapvoteFrame:SetSize(790, 600)    
	mapvoteFrame:SetTitle("") 
	mapvoteFrame:SetVisible(true)             
	mapvoteFrame:SetDraggable(true)      
	mapvoteFrame:ShowCloseButton(true)   
    --mapvoteFrame:SetScreenLock(true)     
    
	--mapvoteFrame:SetBackgroundBlur( true )	
	mapvoteFrame.Paint = function()
	draw.RoundedBox( 8, 0, 0, mapvoteFrame:GetWide(), mapvoteFrame:GetTall(), Color( 0, 0, 0, 252 ) )
	end
	mapvoteFrame:Center() 
	mapvoteFrame:MakePopup()    

   --TitleText
   surface.CreateFont("totleText",{ font = "Arial", size = 28, weight = 700,})
   titleText = vgui.Create("DLabel",mapvoteFrame) 
   titleText:SetSize(400,28)
   titleText:SetText( "Vote for next map" )	
   titleText:SetPos( 60, 5)
   titleText:SetFont("totleText")
   titleText:SetTextColor(Color(255,255,0,255))   
   
   --StayOnThisMap button
   if Mapvote.showExtend == "1" then
        button = vgui.Create("DButton",mapvoteFrame)
        button:SetPos(300,50)
        button:SetText("Stay on this map")
        button:SetSize(150,60)
        button.DoClick = function()
         net.Start("mapvote_castvote")
          net.WriteTable({99,LocalPlayer():UniqueID()})
          net.SendToServer()
          --votedForTextmap:SetText("Stay on this map") 
        end  
   end
   --StayONThisMap number of votes			  
   voteNumbersMap[99] = vgui.Create("DLabel",mapvoteFrame)
   voteNumbersMap[99]:SetSize(128,40)
   voteNumbersMap[99]:SetText( "" )	
   voteNumbersMap[99]:SetPos( 310, 115)
   voteNumbersMap[99]:SetTextColor( Color(255, 255, 0, 255) )

   --Countdowntext
   surface.CreateFont("countDownTest",{ font = "Arial", size = 30, weight = 700,})
   countDowntext = vgui.Create("DLabel",mapvoteFrame) 
   countDowntext:SetSize(138,40)
   countDowntext:SetText( secsLeftMapCote )	
   countDowntext:SetPos( 650,5)
   countDowntext:SetFont("countDownTest")
   countDowntext:SetTextColor(Color(0,255,0,255))
   
   -- You have voted for text				  
   votedForText = vgui.Create("DLabel",mapvoteFrame) 
   votedForText:SetSize(300,20)
   votedForText:SetText( "Not voted yet:" )	
   votedForText:SetPos( 550,45)

   surface.CreateFont("winnerText",{ font = "Arial", size = 20, weight = 700,})

  -- votedForTextmap = vgui.Create("DLabel",mapvoteFrame) 
  -- votedForTextmap:SetSize(200,40)
  -- votedForTextmap:SetText( "You did not vote" )	
  -- votedForTextmap:SetPos( 550,60)
  -- votedForTextmap:SetTextColor(Color(255,0,0,255))
  -- votedForTextmap:SetFont("winnerText")
  
   --Winning map text	
   winnerText2 = vgui.Create("DLabel",mapvoteFrame) 
   winnerText2:SetSize(200,20)
   winnerText2:SetText( "Winning map:" )	
   winnerText2:SetTextColor(Color(255,255,255,255))
   winnerText2:SetPos( 65,45)
   
   winnerText = vgui.Create("DLabel",mapvoteFrame) 
   winnerText:SetSize(200,40)
   winnerText:SetText( "" )	
   winnerText:SetTextColor(Color(255,0,0,255))
   winnerText:SetPos( 65,65)
   winnerText:SetFont("winnerText")
	

	for i = 1,8 do --(mapcount)

              if( i < 5 ) then
                  posTop = 160
                  posLeft = ((i*158) - 140) + 60
              else
                  posTop = 375
                  posLeft = ((158*(i-4) ) - 140) + 60
              end
	
	   

		local button = vgui.Create("DImageButton",mapvoteFrame)                
                      button:SetSize(130, 130)
                    if file.Exists("materials/vgui/ttt_fun_map_icons/"..Mapvote.mapselection[i]..".png","GAME") then
                      button:SetImage("vgui/ttt_fun_map_icons/"..Mapvote.mapselection[i]..".png")
                    else
                      button:SetImage("vgui/ttt_fun_map_icons/noimage.png")
                    end
		      button:SetPos( posLeft,posTop)	 		
		      button:SetText(Mapvote.mapselection[i])
                      button.DoClick = function()			
                        net.Start("mapvote_castvote")
			net.WriteTable({i,LocalPlayer():UniqueID()}) 
			net.SendToServer()   
                        --votedForTextmap:SetText(Mapvote.mapselection[i])                        
			--frame:Remove()
		      end

                 button2 = vgui.Create("DLabel",mapvoteFrame) 
		 button2:SetSize(138,20)
                 button2:SetText(Mapvote.mapselection[i])	
                 button2:SetPos(  posLeft, posTop+130)	
                 button2.DoClick = function()
			net.Start("mapvote_castvote")
			net.WriteTable({i,LocalPlayer():UniqueID()}) 
			net.SendToServer()
			--frame:Remove()
                        --votedForTextmap:SetText(Mapvote.mapselection[i])    
		 end
			
		 voteNumbersMap[i] = vgui.Create("DLabel",mapvoteFrame)
		 voteNumbersMap[i]:SetSize(128,40)
		 voteNumbersMap[i]:SetText( "" )	
		 voteNumbersMap[i]:SetPos( posLeft, posTop+150)
                 voteNumbersMap[i]:SetTextColor( Color(255, 255, 0, 255) )
                		
	end 
    
         secsLeftMapCote = (secsLeftMapCote or GetConVarNumber("ttt_posttime_seconds")+23) -1

   --Countdown Timer
        timer.Create("mapvoteCouts", 1, 30,function()
           mapv = getMapVotes()          
		    secsLeftMapCote = secsLeftMapCote - 1
           
           if(countDowntext:IsValid() ) then 
              countDowntext:SetText( (secsLeftMapCote or 28).."" )
              if secsLeftMapCote > 15 then
                  countDowntext:SetTextColor(Color(0,255,0,255))
              elseif secsLeftMapCote < 16 and secsLeftMapCote > 5 then
                   countDowntext:SetTextColor(Color(255,128,0,255))
              else
                   countDowntext:SetTextColor(Color(255,0,0,255))
              end
           end
		   --Remove the frame
		   if secsLeftMapCote < 1 then
		    if mapvoteFrame:IsValid() then mapvoteFrame:Remove() end
		   end
        end)
   

  end
  
  




  Mapvote.SetMapVoteGUI = function(f)
          timer.Simple(0.1,function() MapVoteMenu = f end)
  end

  --[[Deprecated]]--
  function SetMapVoteGUI(f)
          Mapvote.SetMapVoteGUI(f)
  end

  timer.Simple(1,function() net.Receive("mapvote_prompt",MapVoteMenu) end)
end



function getMapVotes()
  return Mapvote.votes
end

