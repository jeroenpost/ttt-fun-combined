"AddonInfo"
{
	"name" "The Bulk Cannon"
	"author_name" "author"
	"info" "-Read this!-
Babel Industries and TS Industries are proud to present to you the Bulk Cannon, the world's first Break-Action Revolving Automatic Shotgun. Baras sounds lame, though.

Here's the official description from TS Ind.:
"7 12 Gauge shells can be fired rapidly in 1.7 seconds, thanks to revolver mechanism,assisted by electric magnet spinning force.
The rail serves as magnet conductor to accelerate the speed of the pellets, making them go twice as fast; Easily making any tough enemy as weak as balsa wood.
The Bulk Cannon is a perfect close range weapon,but it takes quite a while to reload. The heat coming up from the rail conductor strongly glows in the dark - this could compromise your cover if you're planning any ambushes in dark areas."


--Credits--
Permission was given by Teh Snake to use his skin:
http://www.gamebanana.com/css/skins/114023 

Swepping/Hexing/Distribution: Chris / Babel Ind.


--Group--
Babel is finally back after overcoming some issues. If you enjoy this weapon, why not join the official Babel Industries steam group?
http://steamcommunity.com/groups/BabelInd
I posts previews of upcoming weapons, and announce when they're on the 'shop."
}